
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Iconography of वराहमिहिर](https://manasataramgini.wordpress.com/2006/09/05/iconography-of-varahamihira/){rel="bookmark"} {#iconography-of-वरहमहर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/05/iconography-of-varahamihira/ "3:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

वराहमिहिर was a Hindu encyclopedist who lived in the 500s of the CE era. He may have descended from a family of शाकद्वीपी ब्राह्मणस्, as suggested by the mihira epithet in his name combined with वराह. This is also suggested by his tendencies for solar-system worship which appears to have been a trait of the शाकद्वीपी ब्राह्मणस् or Iranian Hindu brahmins. He covers an extraordinary range of topics in his encyclopedic works ranging from astronomy, mathematics, rituals, yavana जातक-s, augury, sooth-saying of various kinds, and worship of Hindu gods through idols. In the same breadth he introduces some of the best known trigonometric formulae like (sinx)^2 + (cosx)^2 = 1; the double angle expansions like cos2\*x=1-2*(sinx)^2; the factorial and its relationship to combinations,  as well as the basics of Hindu iconography. Thus, it is not suprising when we find trigonometry mensuration and agamic issues pertain to idols being linked even the works of the later day नम्बूतिरि mathematician and तान्त्रिक Chennas Narayanan Nambutiripad.

In his बृहत् saMhitA he gives a succinct section on construction of icons of gods, which gives a good picture of what was the iconographic situation around, 500-600 CE, the culmination of the gupta period. Many of these topics are dealt with in great detail in the पुराणस् and Agamas of the various streams, but वराहमिहिर's account appears to give a summary of what was popular, such that it can be compared with surviving temples of the period, like the glorious Udayagiri complex.

The deities who were iconographically depicted in वराहमिहिर's times were:

 1. rAma the son of dasharatha; 2) bali the son of virochana, the asura. 3) विष्णु with 8 arms etc 4) balabhadra or the संकर्षण 5) sAmba 6) pradyumna 7) एकानंशा (3-7 appear to represent a vaiShNava set parallel to the set expounded in the iconographic chapters of the पाञ्चरात्र tantras) 8)4-headed brahmA 9) कुमार or skanda 10) indra 11) rudra, holding uma by his side 12) the buddha 13) the naked jain ascetic 14) sUrya 16) लिङ्ग 17) revanta 18) yama 19) वरुण 20) kubera 21) विनायक 22) sapta-मातृक

Appended below are the relevant verses along with some notes and my translation (the text follows the one provided freely by Indologist Michio Yano and one published by obscure press in the drAviDa country of which the title pages are worn off):

[५७।३० दशरथ-तनयो रामो बलिश् च वैरोचनिः शतं विंशम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३० द्वादश-हान्या शेषाः प्रवर-सम-न्यून-परिमाणाः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
rAma, son of dasharatha, and bali, son of virochana, should be made 120 \[digits tall]. The heights of other images, i.e. large, medium and small, are less by 12 digits in succession (108, 96 and 84).

[५७।३१ कार्यो अष्टभुजो भगवांश् चतुर्-भुजो द्विभुज एव वा विष्णुः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३१ श्रीवत्स-अङ्कित-वक्षाः कौस्तुभ-मणि-भूषित-उरस्कः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
The idols of विष्णु must be made with eight, four or two arms. His chest is marked with the श्रीवत्स and is adorned with the Kaustubha gem.

[५७।३२ अतसी-कुसुम-श्यामः पीत-अम्बर-निवसनः प्रसन्न-मुखः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३२ कुण्डल-किरीट-धारी पीन-गल-उरःस्थल-अंस-भुजः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
He colored like a deep colored flax flower (deep blue), with yellow robes, having a calm and happy face. He wears ear-rings, a crown and has a muscular neck, chest, shoulders and arms.

[५७।३३ खड्ग-गदा-शर-पाणिर् दक्षिणतः शान्तिदश् चतुर्थ-करः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३३ वाम-करेषु च कार्मुक-खेटक-चक्राणि शङ्खश् च]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
\[In the eight handed form] the four on the right side must have the sword, mace, arrow and the peace-giving mudra. The four left hands have a bow, shield, discus and conch.

[५७।३४ अथ च चतुर्भुजम् इच्छति शान्तिद एको गदा-धरश् च-अन्यः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[57.34 दक्षिण-पार्श्वे hy एवं वामे शङ्खश् cha chakraM cha]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
In case a four armed idol is desired, the two hands on the right side will bear the peace mudra (=abhaya) and a mace. On the left he bears a conch and discus.

[५७।३५ द्विभुजस्य तु शान्ति-करो दक्षिण-हस्तो अपरश् च शङ्ख-धरः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३५ एवं विष्णोः प्रतिमा कर्तव्या भूतिम् इच्छद्भिः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
If a two armed idol is made then the right hand has the shanti-mudra and the left holds a conch. Thus, the image of the विष्णु is to be made when wealth is desired

[५७।३६ बलदेवो हल-पाणिर् मद-विभ्रम-लोचनश् च कर्तव्यः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३६ बिभ्रत् कुण्डलम् एकं शङ्ख-इन्दु-मृणाल-गौर-तनुः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
baladeva should be with a hala in his hand, with his eyes intoxicated with beer. He should be shown wearing a single ear-ring, his body being as white as conch, the moon and lotus.

[५७।३७ एकानंशा कार्या देवी बलदेव-कृष्णयोर् मध्ये]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३७ कटि-संस्थित-वाम-करा सरोजम् इतरेण छ-उद्वहती]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
The goddess एकानम्शा should be placed between baladeva and कृष्ण. Her left hand is placed on the hip and the other hand holds a lotus.

[५७।३८ कार्या छतुर्भुजा या वाम-कराभ्यां स-पुस्तकं कमलम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३८ द्वाभ्यां दक्षिण-पार्श्वे वरम् अर्थिष्व् अक्ष-सूत्रं च]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
If she is made with four arms, then her left hands hold a book and a lotus. Her two right hands show the boon-giving mudra to the votaries and a rosary.

[५७।३९ वामो अथ वा-अष्ट-भुजायाः कमण्डलुश् चापम् अम्बुजं शास्त्रम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।३९ वर-शर-दर्पण-युक्ताः सव्य-भुजाः स-अक्ष-सूत्राश् च]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
\[If in the] eight armed form, in the left hands she has a कमण्डलु, a bow, a lotus, and a book. In the right ones, boon-giving mudra, an arrow, a mirror and a rosary.

[५७।४० शाम्बश् च गदा-हस्तः प्रद्युम्नश् चाप-भृत् सुरूपश् च]{style="font-style:italic;font-weight:bold;color:#99cc00;"}\
[५७।४० अनयोः स्त्रियौ च कार्ये खेटक-निस्त्रिंश-धारिण्यौ]{style="font-style:italic;font-weight:bold;color:#99cc00;"}\
shAmba should be made with a mace in his hand; pradyumna, is shown in a handsome form, with a bow in his hand. Their wives are also made with sword and shields in their hands.

[५७।४१ ब्रह्मा कमण्डलु-करश् चतुर्-मुखः पण्कज-आसन-स्थश् च]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४१ स्कन्दः कुमार-रूपः शक्ति-धरो बर्हिकेतुश् च]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
brahmA is made with four faces, a कमण्डलु in his hand and seated on a lotus. skanda is made as a youth, holding a spear in his hand and with peacock feathers.

[५७।४२ शुक्ल-चतुर्-विषाणो द्विपो महेन्द्रस्य वज्र-पाणित्वम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४२ तिर्यग् ललाट-संस्थं तृतीयम् अपि लोचनं चिह्नम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
The great indra is \[made] with a white, four tusked elephant, and a thunderbolt in his hand. He should also be show with the mark of a third eye placed horizontally on the forehead.

[५७।४३ शम्भोः शिरसि-इन्दुकला वृष-ध्वजो अक्षि च तृतीयम् अपि च-ऊर्ध्वम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४३ शूलं धनुः पिनाकं वाम-अर्धे वा गिरिसुता-अर्धम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
shiva \[is made] with the cresent moon on the head, and a bull-banner, as also the third eye, placed vertically on the forehead. He has a trident in one hand and a पिनाक bow in the other. He may be shown as having the daughter of the mountains for his left half.

[५७।४४ पद्म-अङ्कित-कर-चरणः प्रसन्न-मूर्तिः सुनील-केशश् च]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४४ पद्मासन-उपविष्टः पिता-इव जगतो भवति बुद्धः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
His hands and feet are marked with lotuses, he is of happy form and good black hair. Thus the buddha is depicted in the padmAsana pose, showing a fatherly attitude to the world.

[57.45 आजानु-lamba-बाहुः श्रीवत्स-अङ्कः प्रशान्त-मूर्तिश् cha]{style="font-weight:bold;font-style:italic;"}\
[57.45 dig-वासास् तरुणो रूपवांश् cha कार्यो अर्हतां देवः]{style="font-weight:bold;font-style:italic;"}\
His idol \[is shown] with long arms reaching to knees, with the shrivatsa mark and of serene appearance. Thus is the god of the arhats \[jainas] made as a space-clad, handsome youth.

[५७।४६ नासा-ललाट-जङ्घ-ऊरु-गण्ड-वक्षांसि च-उन्नतानि रवेः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४६ कुर्याद् उदीच्य-वेषं गूढं पादाद् उरो यावत्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
ravi should be shown with a prominent nose, forehead, calves, thighs, cheeks and chest. He should be made with the dress of northerners, covering the feet and chest entirely.

[५७।४७ बिभ्राणः स्व-कर-रुहे पाणिभ्यां पङ्कजे मुकुट-धारी]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४७ कुण्डल-भूषित-वदनः प्रलम्ब-हारो ऽवियङ्ग-वृतः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
He holds two lotus sprouts in his hands and wears a crown. His face is decorated with ear-rings, he bears a long necklace and is girdled with an Iranian sacred-thread.

[५७।४८ कमल-उदर-द्युति-मुखः कञ्चुक-गुप्तः स्मित-प्रसन्न-मुखः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४८ रत्न-उज्ज्वल-प्रभा-मण्डलश् च कर्तुः शुभकरो अर्कः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
His face is lustrous as the inner surface of the lotus petal, he covered with a leather jacket and face is happy and smiling. He has a halo with the bright glitter of gems, such a depiction of the sun is beneficient to the maker.

[५७।४९ सौम्या तु हस्त-मात्रा वसुदा हस्त-द्वय-उच्छ्रिता प्रतिमा]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।४९ क्षेम-सुभिक्षाय भवेत् त्रि-चतुर्-हस्त-प्रमाणा या]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
An image just one cubit \[in height] is good, an idol two cubits (hands) in height gives weath. An idol 3 and 4 cubits in height respectively confer well-being and abundance of food.

[५७।५० नृप-भयम् अत्यङ्गायां हीन-अङ्गायाम् अकल्यता कर्तुः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५० शात-उदर्यां क्षुद्-भयम् अर्थ-विनाशः कृश-अङ्गायाम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
If the image has extra limbs \[than prescribed] the maker suffers the wrath of kings, while lack of \[appropriate] limbs causes illness.

[५७।५१ मरणं तु सक्षतायां शस्त्र-निपातेन निर्दिशेत् कर्तुः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५१ वाम-अवनता पत्नीं दक्षिण-विनता हिनस्त्य् आयुः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
If the idol is split the maker will die by injury from weapons. If the left side is dented then the maker's wife suffers, if the right side is dented the maker life is shortened.

[५७।५२ अन्धत्वम् ऊर्ध्व-दृष्ट्या करोति चिन्ताम् अधोमुखी दृष्टिः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५२ सर्व-प्रतिमास्व् एवं शुभ-अशुभं भास्कर-उक्त-समम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
If \[the idol's] eyes look upwards it causes blindness, if it looks downwards it causes worries. These \[precautions are applicable \]to both the images of other gods as well as the sun.

[५७।५३ लिङ्गस्य वृत्त-परधिं दैर्घ्येण-आसूत्र्य तत् त्रिधा विभजेत्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५३ मूले तच् चतुरस्रं मध्ये त्व् अष्टाश्रिं वृत्तम् अतः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
The circumference of the लिङ्ग is equal to its height and its \[height] is divided into three equal parts. The root \[lowest part] should be quadrilateral, the middle part is octagonal and the uppermost one, cylindrical.

[५७।५४ चतुरस्रम् अवनि-खाते मध्यं कार्यं तु पिण्डिका-श्वभ्रे]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५४ दृश्य-उच्छ्रायेण समा समन्ततः पिण्डिका श्वभ्रात्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
The quadrilaterial part should be buried in the earth, the middle one, in the cavity of the pedestal; and the width of the pedestal around the cavity must be equal to the visible height of the लिङ्ग (1/3 of circumference).

[५७।५५ कृश-दीर्घं देशघ्नं पार्श्व-विहीनं पुरस्य नाशय]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५५ यस्य क्षतं भवेद् मस्तके विनाशाय तल्-लिङ्गम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
\[A लिङ्ग] made thin and tall will destroy the country, one without \[with defective] sides with destroy forts. One with a crack in the top part will cause destruction.

[५७।५६ मातृ-गणः कर्तव्यः स्व-नाम-देव-अनुरूप-कृत-चिह्नः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५६ रेवन्तो अश्व-आरूढो मृगया-क्रीडा-आदि-परिवारः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[The band of goddesses (7/8 मातृका-s)](https://manasataramgini.wordpress.com/2006/08/07/the-circle-of-matri-s/) are made with the form and insignia of the gods whose names they bear [1) brAhmi, 2) वैष्णवी, 3) माहेश्वरी, 4) कौमारी, 5) ऐन्द्री, 6) वाराही 7) चामुण्डा /8) महालक्ष्मी/]. [revanta](https://manasataramgini.wordpress.com/2006/08/31/the-deva-revanta-mythology-iconography-history-and-ritualism/) (the Sun's son) should be shown seated on horseback with his attendants as hunters and acrobats.

[५७।५७ दण्डी यमो महिषगो हंस-आरूढश् च पाशभृद् वरुणः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५७ नर-वाहनः कुबेरो वाम-किरीटी बृहत्-कुक्षिः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
The rod wielding यमा should be shown riding a water-buffalo and वरुण mounted on a swan, with a noose in his hand. The man-borne kubera has a beautiful crown and is pot-bellied.

[५७।५८ प्रमथाधिपो गजमुखः प्रलम्ब-जठरः कुठारधारी स्यात्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[५७।५८ एकविषाणो बिभ्रन् मूलककन्दं सुनीलदलकन्दम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
The lord of the pramathas with an elephant head and a pot-belly holds an axe. He has a single tusk and holds a radish bulb with deeply colored leaves and root.


