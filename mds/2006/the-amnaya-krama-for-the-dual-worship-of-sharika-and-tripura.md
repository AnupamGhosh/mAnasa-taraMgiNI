
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The आम्नाय-krama for the dual worship of शारिका and त्रिपुरा](https://manasataramgini.wordpress.com/2006/10/21/the-amnaya-krama-for-the-dual-worship-of-sharika-and-tripura/){rel="bookmark"} {#the-आमनय-krama-for-the-dual-worship-of-शरक-and-तरपर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/21/the-amnaya-krama-for-the-dual-worship-of-sharika-and-tripura/ "10:55 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R and I finally got to moving ahead with the initial part of laying out the formulae related to the dual worship of शारिका and त्रिपुरा that was prevalent with the Himalayan tantrics. The शारिका-त्रिपुरा krama of aghora-shiva is related in terms of the formulae to दक्षिण-मूर्ति virachita उद्धार-kosha, a famous work on mantra शास्त्र that contains most of these formulae. The aghora-shiva here is the south-face of rudra or दक्षिण-मूर्ति. Many similar or same formulae are also found in the well-known देवी-rahasya tantra. The central manifestation of the shakti are शारिका and त्रिपुरा, and then there are the faces of the cardinal directions comprised of the mantras of the आम्नाय krama. Note that in this procedure the base त्रिपुरा mantra is derived from कादि-विद्या and not the हादि-विद्या.

उच्छिष्ट gaNapati:\
हृदय:\
[OM AH गः शान्तिं कुरुष्व mAM ह्रीं ब्रूं huM स्वाहा ||]{style="font-weight:bold;font-style:italic;"}\
उपहृदय:\
[rAga siddhi siddhi सर्वार्थं me प्रसादय प्रसादय huM ja ja स्वाहा |]{style="font-weight:bold;font-style:italic;"}\
[\
OM AH गः huM स्वाहा | OM वरदकराय स्वाहा | OM वक्त्रैकदंष्ट्रे vighnesha huM फट् स्वाहा | OM गणपत्यै स्वाहा | OM शन्तिङ्करि ब्रुं हारिणेश Dakini huM फट् स्वाहा | O]{style="font-weight:bold;font-style:italic;"}[M जः mesi huM kuru वं OM anye होः ja ja स्वाहा |]{style="font-weight:bold;font-style:italic;"}

[पूर्वाम्नाय]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
सन्गीत-yogini or श्यामला, the prime minister:\
[OM क्रीं श्रीं prIM prIM छ्रीं उच्छिष्ट-चाण्डालिनि देवी महापिशाचिनि मातङ्गी देवी क्रीं ठः ह्रीं ठः aiM ठः स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

Then विनायक:\
[OM श्रीं ह्रीं क्लीं glauM gaM गणपतये vara varada sarva-जनं me वशमानय स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

Then कुमार:\
[OM ह्रां कुमाराय नमः स्वाहा||]{style="font-weight:bold;font-style:italic;"}

कौमार त्र्याक्षरि rahasya:\
[OM ह्रां ङं||]{style="font-weight:bold;font-style:italic;"}

त्र्यंबक मृत्युञ्जय विद्या:\
[OM जुं saH haM saH mAM पालय पालय saH haM saH जुं OM ||]{style="font-weight:bold;font-style:italic;"}

[दक्षिणाम्नाय]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
बगलामुखी:\
[OM ह्लीम् bagalamukhi देवी सर्वदुष्टानाम् वाचं मुखं पदं stambhaya (2) जिह्वां कीलय (2) बुद्धीम् विनाशय (2) ह्लीं OM स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

The commander of the armies- the boar-faced दण्डनायिका वाराही:\
[aIM glauM laM aIM namo भगवती वार्ताली वाराही devate वरहामुखी aiM glauM ठः ठः स्वाहा ||]{style="font-weight:bold;font-style:italic;"}\
[\
]{style="font-weight:bold;font-style:italic;"}Then the वटुक:\
[OM क्लीं vIM ruM ध्रुं ध्रीं ह्रीं वटुक-भैरवाय नमः स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

[पष्चिमाम्नाय]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
कुब्जिका:\
[OM श्रीं prIM kubjike देवी ह्रीं ठः स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

Then इन्द्राक्षी:\
[OM क्रीं क्रीं hUM hUM glauM ह्लीं sह्रां aiM इन्द्राक्षी vajra-haste श्रीं ह्रीं glauM hUM hUM क्रीं क्रीं OM aiM फट् स्वाहा ||]{style="font-style:italic;font-weight:bold;"}

Then विष्णु:\
[OM namo नारायणाय]{style="font-weight:bold;font-style:italic;"}

Then indra:

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/indra.png){width="75%"}
```{=latex}
\end{center}
```



भुवनेश्वरी:\
[OM ह्रीं श्रीं क्लीं bhuvaneshvaryai नमः ||]{style="font-weight:bold;font-style:italic;"}

haihaya:\
[OM prIM छ्रीं क्लीं व्रों AM क्रीं aiM ह्रां hUM फट् कार्तवीर्यार्जुनाय स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

[उत्तराम्नाय]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
महातुरी:\
[OM त्रुं त्रौं त्रों महातुर्यै नमः ||]{style="font-weight:bold;font-style:italic;"}

लक्ष्मी:\
[OM ह्रीं श्रीं लक्ष्मी mahA]{style="font-weight:bold;font-style:italic;"}[लक्ष्मी ]{style="font-weight:bold;font-style:italic;"}[ sarva-काम-prade sarva-सौभाग्य-दायिनि अभिमतं प्रयच्छ sarve sarvagate sarva-svarupe sarva-durjaya-vimochini ह्रीं सौः स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

Then shAradA:\
[OM ह्रीं क्लीं saH namo bhagavatyai शारदायै ह्रीं ठः ठः स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

Then bhavAnI:\
[OM श्रीं श्रीं OM ह्रीं श्रीं श्रीं hUM फट् स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

vajrayogini:\
[OM ह्रीं vajrayoginyai स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

Chinnamasta:\
[OM ह्रीं श्रीं छ्रीं Chinnamastake छ्रीं फट् स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

वज्रवैरोचनी:\
[श्रीं ह्रीं ह्रीं aiM vajravairochani ह्रीं ह्रीं फट् स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

नीलसरस्वती:\
[OM aim hUM tAre फट् स्वाहा | OM ह्रीं aiM hUM तारिणी फट् स्वाहा स्त्रीं | OM स्त्रीं aiM hUM निलसरस्वती फट् स्वाहा | OM ह्रां स्त्रीं ह्रीं aiM ह्रूं उग्रतारे फट् स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

ज्वालामुखी:\
[ OM ह्रीं श्रीं ज्वालामुखी mama शत्रून् भक्षय भक्षय huM फट् स्वाहा ||]{style="font-weight:bold;font-style:italic;"}

[ऊर्ध्वाम्नाय]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
The महामन्त्र of शारिका:\
[OM ह्रीं श्रीं hUM फ्रां AM AM शारिकायै नमः ||]{style="font-weight:bold;font-style:italic;"}

The base mantra of त्रिपुरा:\
[OM श्रीं ह्रीं क्लीं aiM सौः | OM ह्रीं श्रीं | ka e i la ह्रीं | ha sa ka ha la ह्रीं | sa ka la ह्रीं | सौः aiM क्लीं ह्रीं श्रीं ||]{style="font-style:italic;font-weight:bold;"}

The expanded "crest-jewel" त्रिपुरा mantra:\
[श्रीं ह्रीं सौः tripure | aiM क्लीं सौः भैरवी | OM ह्रीं श्रीं हिङ्गुले | श्रीं ह्रीं OM चण्डिके | चामुण्डे ha sa ka la ह्रीं | sa ka la श्रीं नारसिंही | ऐन्द्री aim ह्रीं क्लीं | क्लीं ह्रीं श्रीं देवी हैमवतीश्वरी श्रीं | श्रीं ह्रीं क्लीं मृडानि | aiM वारुणि | ka e i la ह्रीं durge | श्रीं ह्रीं क्लीं aiM सौः | OM ह्रीं श्रीं श्री महात्रिपुरसुन्दरी ||]{style="font-weight:bold;font-style:italic;"}

प्रासाद:\
[ह्सौः ||]{style="font-weight:bold;font-style:italic;"}

पञ्चाक्षरी विद्या:\
[नमः शिवाय ||]{style="font-weight:bold;font-style:italic;"}


