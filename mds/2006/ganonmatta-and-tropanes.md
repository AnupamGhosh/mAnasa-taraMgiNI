
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [गणोन्मत्त and tropanes](https://manasataramgini.wordpress.com/2006/08/15/ganonmatta-and-tropanes/){rel="bookmark"} {#गणनमतत-and-tropanes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/15/ganonmatta-and-tropanes/ "8:06 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/datura.jpg){width="75%"}
```{=latex}
\end{center}
```




We had heard of the tale of that medieval luminary of our community, appayya दीक्षित's experiment with the tropane alkaloids. The story has it that he was seized with the urge to enter that state of ecstatic abandon, in which the hideous goblins of the retinue of rudra, prance around, herded by the terrible elephant-headed one. appayya is supposed to have drunk a decoction of the famed dhattura plant (One can learn about them in this excellent [botany site](http://waynesword.palomar.edu/ww0703.htm#Datura)) and gone into 13 days of गणोन्मत्त or the madness of the भूत गणस्, where he pranced around like an agent of The god, much to the horror of his wife. But then he is supposed to have returned to normalcy there after.

While we had no urge whatsoever to emulate him, we were driven by our chemical zeal to isolate the tropane alkaloids from dhattura (not for use of course). We achieved this through multiple methods in the hey days of our "practical chemistry". We assembled our co-conspirators, the muni, Mn (who was later to become a great experimental scientist but at that point was somewhat inferior to us- the course of fate has been strange indeed!) and Mis-creant, who shared our interest in both chemistry and the arcana of the रसायन (probably we were only two who saw ourselves as inheritors of the tradition of the tattva-shambhara तन्त्रं). We knew of a patch of deep purple dhatturas in the vicinity and also for variety collected some Brugmansia or the tree dhattura that grew in the vicinity of the वानर parvata. We obtained a good load of seeds of the former and flowers of the latter, but the most successful isolations were from the former. The first technique was the रसायन one resembling the tantric methodology. Knowing that tropanes, especially hyoscyamine which was dominant in our dhattura-s were soluble in lipids we decided to use the ghee extraction technique. We made a haridra impregnated ghee, in which the haridra served as our indicator to show that the basic tertiary amine had been extracted. Having pounded the seeds we poured our haridra-impregnated ghee and extracted for 3 days. The brow-red haridra indicated the extraction of the alkaloid into the घृत. The next technique which we used was a more direct and better one for purification. In this we used something called the dry-cleaning kerosene and extracted the alkaloids again for 3 days. Then we filtered out the kerosene and dried it as the dry-cleaners do. This gave us yellowish crystals of alkaloid. Mn then made a crude electrophoresis apparatus and we found separation of some alkaloids from our extracted crystals. We used the bromine decolorization test to check for tertiary amine presence. Sadly there is no strong test like ninhydrin for tertiary amines we were aware of.


