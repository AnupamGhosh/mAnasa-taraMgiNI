
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Flogging dead Germans](https://manasataramgini.wordpress.com/2006/07/24/flogging-dead-germans/){rel="bookmark"} {#flogging-dead-germans .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 24, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/24/flogging-dead-germans/ "5:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Recent events have exposed a peculiar bunch of Europeans- they appear on face to be sincere students of Indic issues, but deep within are Hindu-haters. This group is dominanted by people of Germanic origin, but also includes a range of other Europeans, their well-mixed American cousins, and Japanese and Lankan imitators. The list is a long one but the some examples include Witzel, Wolpert, Kenoyer, Falk, Pollock, Southworth, Ringe, Dreyer, Scharfe, Olivelle, Einoo, Marui, Zimmer, Tosi, Nakatani and so on.

Many Hindus anguished by their anti-Hindu actions and have taken to flogging Germans and their fellow travellers starting from Schopenhauer, through Mueller down the above-mentioned modern disciples of theirs. Like other Hindus, we are largely in agreement with the characterization of these modern Indologists (not Schopehauer) as detractors of Hindus. Despite being foreigners, and not practicing Hindus they interfere in internal Hindu affairs (the textbook case), and Hindu politics taking the side to the traditional Abrahamistic enemies of the Hindus. In some cases their connections are open- inciting Isaist, Mohammedan and Dalit operations against Hindus. In other cases they are more subtle, engaging in denigrating Hindu systems (e.g. Goldman) or mis-representing Hindu politics to create negative caricature of Hindus and their state (most of this group of Indologists), or providing a biased forum for such Hindu baiters (e.g. Wujastyk). These were studied by an otherwise confused Hindu pamphleteer, Malhotra, who proposed the theory of U-turns. As an operational theory this is fine but it misses the central point. Or perhaps Malhotra is well aware of this but is fearful of its deepest implications. Despite the framework offered by Malhotra, we are disappointed to note that most of the Hindu elite that bothers to look into these matters is flogging dead horses, or more precisely dead Germans. Earlier they used to flog Mueller, and now even more disappointingly Schopenhauer! In this process the Hindus are being lost in the स्थूल, entirely missing the सूक्ष्म.

These obssessions with the स्थूल are making us flagrantly repeat our old words here. Schopenhauer deserves a deep study-- I did this several years ago in secondary school, and it led to understand the most important challenge faced by the Hindus from their vairi-s. Schopenhauer by any stretch of imagination was one of Europe's most important intellectual figures. Karl Popper, whose clarity in scientific philosophy is of highest standing, declared that there were more good ideas in Schopenhauer than in any other philosopher except Plato. Plato was a inseparable product of the heathen Greek system. But what were Schopenhauer's influences. He himself admits the following: ["I owe what is best in my own development to the impression made by Kant's works, the sacred writings of the Hindus, and Plato." ]{style="font-weight:bold;"}In another place he remarks that the sacred Hindu texts were "[the consolation of my life]{style="font-weight:bold;"}". Now, it is well known that Schopenhauer profoundly influenced the most important of European intellectuals- good and bad- including Darwin, Freud, Wittgenstein, Einstein and Popper. Given this start, why do we have the modern Indologists so often profoundly anti-Hindu rather than acknowledge the ultimate contribution of Hindu thought to modern European thought. They try to explain it by claiming that they have a better grasp of the subject now unlike the romantic ancestors of theirs and can see the[ cul de sac]{style="font-style:italic;"} that the Hindus really were. But the real cause is far deeper- the monotheisitic Isaistic custodians of "Western Civilization" feared the return to the heathen roots with the influences of Plato and still worse the Hindus leaking back in. Hence they worked behind the scenes to expunge such influences and the modern Indologists are actually the foot soldiers of this Isaistic counter assault. It is a small wonder they make common cause with the Marxist and Mohammedan ideological cousins when it comes to Hindus.


