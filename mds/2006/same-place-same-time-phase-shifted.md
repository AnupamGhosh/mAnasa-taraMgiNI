
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Same place same time phase shifted](https://manasataramgini.wordpress.com/2006/01/01/same-place-same-time-phase-shifted/){rel="bookmark"} {#same-place-same-time-phase-shifted .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/01/same-place-same-time-phase-shifted/ "8:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Phase shifted by an year we were there forced to spend time with the wild Northerners, more precisely characterized as the fallen brahmins and क्षत्रियस्. Luckily for us we had our own inner circle with whom we could explore the further mysteries of whippos and the secret applications. We remained there like a पाशुपत verily, concealing the brahman, observing the unmatta of others, behaving like a वृक्ष ourselves. The secret formula goes as badhira Akrandayati.

We returned and saw two कृत्यास्. One was headed for the yonder man. The other was headed for us. Jx had left the land, the कृत्या moving away from him came over to us.


