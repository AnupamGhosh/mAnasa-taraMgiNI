
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Meiolaniid turtles](https://manasataramgini.wordpress.com/2006/03/01/meiolaniid-turtles/){rel="bookmark"} {#meiolaniid-turtles .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/01/meiolaniid-turtles/ "6:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/meio_skeleton.jpg){width="75%"}
```{=latex}
\end{center}
```

Meiolaniids are amongst the most fascinating turtles. The first remains of them were found by an Australian collector Bennett who correctly identified them as turtles and sent them to the obnoxious anti-Darwinian anatomist Richard Owen in England. Owen wrongly thought they were a lizard and described them as such. It is clear that Owen had made a huge anatomical blunder. However, the Darwinian anatomist, Thomas Huxley, showed that it was a turtle of the cryptodiran clade. After him many puzzled over the affinities of the meiolaniid turtles, but two decades of studies by the great turtle expert Eugene Gaffney showed that Thomas Huxley was essentially correct. The prolific South American fossil hunter Florentino Ameghino, found a related turtle from the Eocene beds of Argentina, suggesting that they might have once had a widespread. Gaffney and Paul Sereno prospected for new skull material in Howe's Island near Australia and found several skulls of recent provenance from the Pleistocene. These, with other Australian material give a reasonable insight into the evolution of these great turtles.

The Meiolaniid turtles are characterized by the presence of an impressive cranial ornamentation of horns and frills. They may be huge cow-like horns in some species like a Queensland [Meiolania]{style="font-style: italic"}, lateral conical horns in [Ninjemys]{style="font-style: italic"}, low rugosities in [Warkalania ]{style="font-style: italic"}and ceratopsian-like frill, eye horns and nasal horn in [Niolamia argentina]{style="font-style: italic"}. The Meiolaniids where characterized by a long tail with a rather dramatic spiked club in certain cases, like [Ninjemys]{style="font-style: italic"}. They are generally large land turtles over 6-8 ft in length, as in the case of [Niolamia]{style="font-style: italic"}, [Ninjemys ]{style="font-style: italic"}and the Queensland [Meiolania]{style="font-style: italic"}. Thus, in essence, the Meiolaniids where pretty close in a general structural sense to the glyptodonts (extinct South American Xenarthrans) and the ankylosaurs. The Meiolaniids are found from the Eocene of South America, the Oligocene and Miocene of various Australian localities and Pleistocene of various Australian mainland and Island sites, like Howe' Island, Walpole Island and New Caledonia. This has been taken to mean that the Meiolaniids emerged in the Late Cretaceous when Australia and South America were still connected via Antartica and got widely distributed over the southern continents. They then appear to have moved from the main land as chains of volcanic islands rose and sunk moving out into the sea. I suspect that their growth in size may have happened even before the extinction of at K/T to develop defenses against predatory dinosaurs, like the ankylosaurs. Giant predatory birds that emerged in South America and possibly even Australia (like [Bullockornis ]{style="font-style: italic"}the gigantic predatory duck) may have kept them large. What is not clear is why they suddenly became extinct in the recent past after lasting for almost a 65 Myrs. We believe this extinction was caused by the arrival of Homo sapiens on some of these islands Vanuatu. Gaffney's work shows that they lie in the more basal radiations of Cryptodira and form a clade with the Sinemyids and the crown group Cryptodirans termed centrocryptodira. The big question is about their Mesozoic past- would we ever know anything of it?

Update: August 16, 2010: New work by White et al. ([https://doi.org/10.1073/pnas.1005780107](https://doi.org/10.1073/pnas.1005780107){rel="nofollow"}) indeed suggests that the extinction of the meiolaniids on Vanuatu was likely due to Homo sapiens.


