
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [shakra virachita laghu कुमार स्तोत्रं](https://manasataramgini.wordpress.com/2006/10/21/shakra-virachita-laghu-kumara-stotram/){rel="bookmark"} {#shakra-virachita-laghu-कमर-सततर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/21/shakra-virachita-laghu-kumara-stotram/ "8:40 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

laghu कुमार stotrasya शतक्रतुः ऋषिः | अनुष्टुप् छन्दः | skando devata ||\
OM ह्रां \~NaM\
deva-देवं नमस्यामि कुमारं वरमच्युतं |\
कार्त्तिकेयं दुराराध्यं vahni-तेजः समुद्भवं || 1 ||\
उमा शङ्करजं भीमं गङ्गाया जठरे धृतं |\
shakti-हस्तं विषालाक्षं षण्मुखं दीप्त-तेजसं || 2 ||\
भक्तानुकंपिनं दातं ब्रह्मण्यं वरदं प्रभुं |\
kAka-पक्ष-धरं शान्तं शिखण्डक-विभूषितं || 3 ||\
रक्ताम्बरं महाबाहुं मयूर-वरगामिनं|| 4 ||\
घण्टा-प्रियं गणाध्यक्षं महाबल-पराक्रमं |\
deva-सेनापतिं देवं sarva-loka-हितेप्सया || 5 ||

skanda-dhyAnaM\
कार्त्तिकेयं महाभागं mayuroparisamstitham |\
tapta-काञ्चन वर्णाभं शक्तिहस्तं वरप्रदं ||\
द्विभुजं शत्रुहन्ता cha नानालञ्कार भूषितम् |\
prasanna-वदनां devam कुमारं पुत्रदायकं ||


