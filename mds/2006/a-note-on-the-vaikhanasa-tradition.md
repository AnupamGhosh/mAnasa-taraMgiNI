
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on the वैखानस tradition](https://manasataramgini.wordpress.com/2006/02/16/a-note-on-the-vaikhanasa-tradition/){rel="bookmark"} {#a-note-on-the-वखनस-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/16/a-note-on-the-vaikhanasa-tradition/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The वैखानसस् appear to represent an archaic branch of वैष्णवस् who are distinct from the more popular and numerous पाञ्चरात्रिन्स्. The वैखानसस् however were and are still influential in few south Indian temples including the world's most famous shrine, वेन्कटाद्रि (Other examples being the विष्णु temple of वाणमामलै). The वैखानसस् belong to a कृष्ण yajurvedic shAkhA founded by the descendent of the ancient ऋषि of the आङ्गिरस clan, विखानस्. The वैखानसस् seem to have adopted a form of vaiShnavism early on, and are probably even amongst the earliest of the वैष्णवस् alluded to in the नारायणीय section of the महाभारत. They possess many unique tantras of their own. Another unique collection associated with them is the वैखानस mantra पाठ, which in addition to the yajurvedic गृह्य mantras (cf. Apastamba मन्त्रपाठ) includes a striking collection of mantras used in the private वैखानस ritual, especially in connection to विष्णु. Even their गृह्य sUtra includes a section on the iconic worship of विष्णु. Their main tantras pertaining to the installation and worship of idols are the भृगु, atri, kAshyapa and मरीचि संहितास्. The भृगु work mainly convers temple rituals. atri too covers these topics at great depth including the design of temples. kAshyapa's Agama is mainly in sutraic form. Like many other great traditions of India we notice medieval historical references to the वैखानस Agamas from both the extreme north of India- काश्मीर and the drAviDa and Andhra countries to the south. It finds mention in both the literary works of Sanskrit writers from drAviDa, like दण्डिन् and jayanta भट्ट of काश्मीर.

The वैखानसस् describe five fold states of विष्णु, which is parallel to the पाञ्चरत्रिc doctrine. 1) विष्णु is the sarva व्यापिन्, the primary source, the eternal state from which all arise. 2) From him differentiates the puruSha, which is a distinct entity or the property of existence, termed consciousness. 3) From विष्णु also emanates satya, the which is the matter comprising the universe. 4) acyuta is the time-invariant aspect of all matter. 5) aniruddha is the ultimate atomic particle of all existence. These five states of विष्णु are represented by the वैखानसस् as the 5 vedic fires: 1) गार्हपत्य, 2) आहवनीय, 3) दक्षिणाग्नि, 4) अन्वाहार्य and 5) sabhya. While विष्णु is their primary deity, the वैखानसस्, like the early पाञ्चरात्रिन्स् worship the rest of the Hindu pantheon with a series of elaborate oblations and तर्पणंस्.

The वैखानस tantras usually dialate upon: 1) कर्षण- construction of shrines 2) प्रतिष्ठ- installation of idols of gods and goddess 3) पूजा- worship of the idols 4) स्नापन- the abhisheka or bathing of idols 5) utsava- festivals and processions 6) प्रायश्चित्त- expiatory rites relating to utsavas and errors in rituals. Interestingly for all these rites the वैखानस priests are organized as per the traditional vedic division into होता-s, उद्गाता-s and adhvaryu-s and brahmA-s deploying the 4 vedic संहितास्. All the rituals for the above procedures depend upon oblations in the vedic fires.

The bhr\^igu, मरिची and atri संहितास् in particular go into different aspects of architecture of वैखानस विष्णु temples, while other fragments cover chitra karma or painting of pictures of deities. The basic plan of a temple is termed the vimAna. The atri saMhitA enumerates 96 different plans of विमानस्, which are described as belong to the several basic classes termed brahmA, विष्णु, rudra, indra, soma and those of various brahmins. The paddhati of इशानशिव mishra, the siddhAnta tantric, also mentions 96 विमानस् for the construction of shiva temples, many of which match the वैखानस set implying that basic hindu temple architectures had an early common ancestor in the form of the proto-agAma. The old विष्णु temples over South India represent many of these 96 vimAna forms suggesting the wide use of the वैखानस tradition before their take over by the पाञ्चरत्रिन्स्.

5 different idols are made for the वैखानस temple. The dhruva idol is the permanent idol of विष्णु whom may be in standing (Tirupati), sleeping (Shrirangam) or sitting (Urakam, Kanchi) poses and display [vIra ]{style="font-style:italic;"}(heroic), [yoga ]{style="font-style:italic;"}(yogic) or [bhoga ]{style="font-style:italic;"}(relaxing or amorous) disposition. Stone, copper, clay or wood may be used for the dhruva idol. The next idol is the kautuka idol, made of gems, stone, copper, silver, gold or wood and it 1/3 to 5/9 the size of the dhruva. This idol is used for all offerings. The [स्नापक ]{style="font-style:italic;"}idol is smaller than the kautuka and is used for abhisheka and made similarly. The next idol is the utsava idol always made of metal and is used in festival processions. The final idol is the [bali ]{style="font-style:italic;"}made of shiny metal and is daily taken around the central shrine when offerings are made to indra and other devas, as well as jaya and vijaya. For matsya and कूर्म idols no [kautuka ]{style="font-style:italic;"}is made and the oblations are made into the गार्हपत्य. In case of various fiery and watery वराह-s the offerings are made in the sabhya fire. वैखानस-s make only simple नृसिंह images and install him with offerings in the आहवनीय. trivikrama and भार्गव are installed with oblations in the अन्वाहार्य fire. For रामचन्द्र and balabhadra the दक्षिणग्नि is used, while for देवकीपुत्र and कल्की the पौन्डरीक fire is used. For garuDa and विष्वक्सेन the आहवनीय is used.


