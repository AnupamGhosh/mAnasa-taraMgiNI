
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [agnichayana Altars](https://manasataramgini.wordpress.com/2006/01/23/agnichayana-altars/){rel="bookmark"} {#agnichayana-altars .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 23, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/23/agnichayana-altars/ "7:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A number of archaic altars shapes existed for the agnichayana of which the practical geometric knowledge is largely lost in the surviving shrauta tradition.

The most famous which are still performed are: 1) the broad winged षड्पत्रिक shyenachiti or the gliding eagle altar with 6 feathered wings. There is a variant of this with wings entirely made of 6 rows of parallelogram bricks.

 2.  the drawn wing पङ्चपत्रिक shyenachiti or the diving eagle altar with 5 feathered wings.

 3.  The पीथचिति- In the form a square with 3 rectangular paths leading out on 3 of the sides (this is nearly extinct, only a single Andhra ritualist, whose पत्नी is also a qualified soma ritualist, knows this.

 4.  The पुरुषचिति- in the form of a standing man with arms extended to the sides.

 5.  The कूर्मचिति- in the form of a turtle

The less known ones:

 6.  The कञ्कचिति- in the form of a flying heron

 7.  The alajachiti- in the form of an alaja bird with four furrow like paths leading out in the cardinal directions.

 8.  praugachiti- in the form of an equilateral triangle

 9.  द्रोणचिति- in the form of a droNa kalasha

 10.  rathachakrachiti- in the form of a spoked wheel

 11.  परिचाय्यं- a circular altar

 12.  श्मशानचिति- in the form resembling a tomb (श्मशान described in the yajurvedic burial rite for the bones of a dead clansman)

All the altars have 5 layers of bricks.


