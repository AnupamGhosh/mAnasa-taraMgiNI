
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Strange and frightening conversation](https://manasataramgini.wordpress.com/2006/07/30/strange-and-frightening-conversation/){rel="bookmark"} {#strange-and-frightening-conversation .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 30, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/30/strange-and-frightening-conversation/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I received a call and we set off for the evening. The women-folk wanted to make their stock of powders and pickles for the quarter and had spent the day collecting their ingredients. As we converged at the pre-destined venue. While the women milled away at the kitchen, Marc and me had planned to re-live some youthful fancies that we much missed- we had planned some experiments. R had told Marc of the दीपावल्ली vacation of एकादशान्त, when she and me had experimented extensively with what in bhArata were known as snake-tablets. Marc obsessed over them and wanted to make his own snake tablets. While he was certain he had reached a recipe he did not have all the substances at home for the synthesis. Instead we decided to settle for other stuff that we thought might also entertain the kids usefully while the women milled away their powders. We first decided to make "hydrogen bombs"- basically exploding mixtures of oxygen and hydrogen trapped in soap suds following electrolysis. We also wanted to make pure silicon. Many years back during the same दीपावली vacation R and I had made use of our stash of Mg wire to isolate silicon, but we ended up with a somewhat dangerous result with our test-tube exploding in the vigorous heating. But Marc now managed this step with great ease and then let it cool. The moment of reckoning came when we dumped the mixture into the acid solution and watched the spectacular pyrotechnics as the silicon precipitated to the bottom.

R unable to stay out of all this dropped the powders and joined in. Towards the end while the kids were sent away to play or study, and we were eating, the conversation turned to the sleeping disease \[For reasons of confidentiality I cannot say why the conversation went to that topic, hence elements of the narrative may appear disconnected]. I recalled the founding Mogol tyrant Babur may have suffered from it but spectacularly recovered. We wondered what history might have been if he had died or been paralyzed, like most victims normally are. However, the classical sleeping disease (encephalitis lethargica) swept the world during 1917 to 1928 in the form of a great epidemic that killed numerous thousands of peoples. It was identified as a novel brain disease by Constantin von Economo, the famous Austrian neuro-scientist, and has been the focal point of the well-known narrative of Oliver Sacks. At this point Sharada made a very strange comment: "I have been interested in this disease even while I was an intern. Your(R's) father told me something that I did not believe at all. He said that victims of a particularly severe धूमवती मारण attack showed symptoms similar to encephalitis lethargica, and long befored Sacks the tantrics knew that there was one counter that could help- Atmagupta. He could not at that point show me any patients. However, several years later when he was visiting bhArata, (X) showed both of us a patient who was in a condition very similar to encephalitis lethargica due to "witch-craft". Of course they did not know what prayoga was deployed on the victim, but we were definitely reminded of the similarity with the supposed effects of धूमवती prayoga. Over the years we have become aware of at least one other victim of "witch-craft" with such effects but he could not try out Atmagupta because of actual scorn of the other physicians"

This striking statement by Sharada reminded me of something that was particular resonant. One of the victims of a confirmed धूमावती attack (whose account was narrated to me) started displaying frequent coprolalia, followed by phase of voice change and finally catatonia. The victim's parents tried to resort to a variety of psychiatric diagnosis but failed. Interestingly the victim as reversed to the normal state by a vaiShNava prayoga whose details are kept secret. The same informer narrated a second account as an example of the deadliest of the धूमवती attacks, where her "head-falling prayoga" was combined with her dreaded daughter rakta-ज्येष्टा. The symptoms of this were dramatic : there was a simulation of both streptococcal infection and subsequent encephalitis lethargica. Our informer used the secret Ashvina prayoga along with its specific ओषधि-s (which include ashvagandha, Atmagupta, khadira amongst others) to save the victim of this attack. A third victim of a similar धूमवती attack was the sister of MM as a part of the generational conflicts. It is indeed confirmed that catatonia was pronounced in her. She never recovered fully though she was relieved for her torpor by the counter-prayoga.

Sharada was convinced that विष-s were deployed in the deadly धूमवती prayoga. But my informer was evasive on the matter, though not denying the fact that विष's may be in use. I had the same suspicion in the event that killed the young सेनानी's relative. R was shaken to hear all this and the excitement of the evening dampened to silence for some reason. Only Sharada seemed to be excited about it and continued conversing with me.


