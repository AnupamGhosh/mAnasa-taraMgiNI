
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Oldest archaeological signs of ritual?](https://manasataramgini.wordpress.com/2006/12/04/oldest-archaeological-signs-of-ritual/){rel="bookmark"} {#oldest-archaeological-signs-of-ritual .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 4, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/04/oldest-archaeological-signs-of-ritual/ "6:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

When did human rituals begin? As a ritualist and evolutionary explorer this question has naturally attracted my attention. My own contention is that the genetico-memetic complex of ritual probably goes back to our common ancestor with the African Great Apes or even beyond to the common ancestor of the Great Apes. In this context, the Bushmen of Africa are of particular interest because they are the earliest branching line of humans alive today. Sadly, the ritual and mythology of the Bushmen has not been objectively studied by professionals of mythology and ritual, and the Bushmen themselves are in a quite precarious situation in many parts of southern Africa. There is a lot to be learnt from Bushmen of which I will touch upon a fascinating sample towards the end.

Recently there was an announcement by a team from the Oslo University that they had found the oldest signs of human ritual, which they dated back to the Middle Stone Age, around 70,000 years ago. I am not sure how they managed to get this date that they claim. Their central claim is that the people there seem to have established a cult object in the form of a python head with marking on it in a cave. Near this cult object they are supposed to have deposited special stone tools (scrappers and spear points) made from stones of different colors and specifically put only those with one color into the fire. Yes, may be this is sign of a ritual, but one can hardly be sure of the date given that Bushmen apparently use the area in their religious practice to the current epoch. Especially, the elephant and giraffe paintings they observe in the cave may be of a later epoch.

Some time last year when I was being hammered by the कृत्या, Elleberger and Lockley uncovered a remarkable piece of ritual art of the Bushmen from Lesotho. In a Lesotho cave the Bushmen made cave paintings of ornithopod dinosaur tracks from the Jurassic as also their trackmakers. These reconstructions are apparently reasonably accurate drawings of bipedal dinosaur! This is something the 18th and 19th century European biologists failed to establish in their first attempt. This shows the acute sense of relationship between tracks and the animals that the Bushmen have developed over time. Interestingly, one of these reconstucted dinosaurs appears to have inspired the form of a mythological being termed //Khwai-hemm that appears in certain rituals.

As an aside, these dinosaur-inspired beings remind me of the देवता-s of the अकाश भैरवं in our tradition. From the behind mist, the dinosaurine inspirations for forms of आकाश bhairava, आशुगरुड, sharabha and पक्षिराज become apparent, [as R had earlier speculated](http://manollasa.blogspot.com/2006/05/arimaspas-dinosaurs-griffins-and.html).


