
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Mapusaurus roseae and the theropods of the South](https://manasataramgini.wordpress.com/2006/04/01/mapusaurus-roseae-and-the-theropods-of-the-south/){rel="bookmark"} {#mapusaurus-roseae-and-the-theropods-of-the-south .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/01/mapusaurus-roseae-and-the-theropods-of-the-south/ "5:19 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In my earliest youth I was fascinated by an obscure Late Cretaceous dinosaur [Carcharodontosaurus]{style="font-style:italic;"}, which was known only from fragmentary remains from the Sahara, described by the famous German paleontologist Ernst Stromer. The few remains suggested that it was an enormous predator and probably had links to the allosaurs of the Jurassic. In the 1990s some thing dramatic happened. First, a gigantic carnivore of the tetanuran lineage, [Giganotosaurus ]{style="font-style:italic;"}was described from the late Cretaceous of Patagonia, Argentina. Shortly after this Paul Sereno recovered more complete remains of [Carcharodontosaurus ]{style="font-style:italic;"}from the Sahara. It became clear that these two were unified into a single clade of large tetanuran theropods, that had emerged out of the more primitive allosauroid stock of the Jurassic. These were the first large predators from the Cretaceous of the Southern Continents (the products of Gondwanaland breakup). Over the years it has become clear the theropod faunas of the South were were different from those of the North during the Cretaceous.

In both the South and the North, the middle to small predatory guilds were prominently occupied by deinonychosaurs. It needs to be seen if some of these might have actually dispersed using their limited flight abilities like the [Buiteraptor]{style="font-style:italic;"}. In the north there were other major coelurosaurian lineages like the oviraptorosaurs, therizinosaurs, and ornithomimosaurs of which there is not much evidence in the South. The top predatory guilds in the north were taken up the coelurosaurian clade the tyrannosaurs. In contrast in the south across various predatory guilds we had a prominent presence of the abelisauroids. For example in the small range, we had [Iloklesia]{style="font-style:italic;"}, [Noasaurus, ]{style="font-style:italic;"}fragmentary [Ligabueino ]{style="font-style:italic;"}and [Masiakasaurus ]{style="font-style:italic;"}(an odd piscivorous or insectivorous abelisauroid). The medium to large range was occupied by several abelisaurs such as [Carnotaurus, Abelisaurus and Aucasaurus]{style="font-style:italic;"} (South America), [Indosuchus, Indosaurus and Rajasaurus]{style="font-style:italic;"} (India), [Majungatholus ]{style="font-style:italic;"}(Madagascar), [Rugops ]{style="font-style:italic;"}(Africa).

The largest theropod guilds are however, occupied by the Carcharodontosaurs. Recently Coria and Currie have uncovered a new carcharodontosaur that promises to through new light on the phylogeny of these beasts. Importantly, it appears that the carcharodontosaurs are distinct from [Acrocanthosaurus ]{style="font-style:italic;"}of the North American Early-Middle Cretaceous. The latter is closer to [Allosaurus. ]{style="font-style:italic;"}This suggests that by the middle to late Jurassic there was an explosive radiation of Allosauroid lineages. [Allosaurus ]{style="font-style:italic;"}proper, the Asian Siraptorids ([Sinraptor]{style="font-style:italic;"} and [Yangchuanosaurus]{style="font-style:italic;"}), [Monolophosaurus]{style="font-style:italic;"}, and [Neovenator]{style="font-style:italic;"}. In the Cretaceous the surivors of these lineages founded new clades. There seems to be larger clade within allosauroid including the Sinraptorids followed by a crown group of [Neovenator ]{style="font-style:italic;"}and the Carcharodontosaurs. On the other hand there was another clade with [Allosaurus ]{style="font-style:italic;"}and [Acrocanthosaurus]{style="font-style:italic;"}.


