
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [मयूराष्टकं](https://manasataramgini.wordpress.com/2006/04/16/mayurashtakam/){rel="bookmark"} {#मयरषटक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/16/mayurashtakam/ "4:58 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

मयूर was a famous poet in the court of हर्षवर्धन, most of whose works are lost. The मयूराष्टकं one of his fine शृन्गार works survives as a reasonably complete fragment. He is said to have composed it on bANa's wife, who enraged by its double entendres, laid an अभिचार on mayura that resulted in a skin disease. He is said to have composed the chaNDI-shataka to चाण्डिका and was relieved of the अभिचार. It is an excellent piece of शृन्गार for a beginner and illustrates some concepts discussed by Anandavardhana.

oM नमः श्री-hari-हराभ्यां

[एषा का प्रस्तुतां गी प्रचलित-नयना हंसलीला व्रजन्ती]{style="font-style:italic;color:#66ff99;"}\
[द्वाउ हस्तौ कुञ्कुमार्द्रौ कनक-विरचित ॥ऊ.........]{style="font-style:italic;color:#66ff99;"}\
[॥ऊं गांगेगता सा बहु-कुसुम-युता बद्धवीणा हसन्ती]{style="font-style:italic;color:#66ff99;"}\
[[ताम्बूलम् वामहस्ते मदन-वशगता गूह्य शालां प्रविष्टा ॥१॥]{style="color:#66ff99;"} ]{style="font-style:italic;"}(Chandas: स्रग्धरा)

[एषा का भुक्त-मुक्ता प्रचलित-नयना स्वेद-लग्नाङ्ग-वस्त्रा]{style="font-style:italic;color:#66ff99;"}\
[प्रत्युषे याति बाला मृग इव चकिता सर्वतश् शंकयन्ती]{style="font-style:italic;color:#66ff99;"}\
[केनेदं वक्त्र-पद्मं स्फुर-दधर-रसं षट्पद् एनैव पीतं]{style="font-style:italic;color:#66ff99;"}\
[स्वर्गः केनाद्य भुक्तो हर-नयन-हतो मन्मथः कस्य तुष्टः ॥२॥]{style="font-style:italic;color:#66ff99;"} (Chandas: स्रग्धरा)

[एषा का स्तन-पीन-भार-कठिना मध्ये दरिद्रावती]{style="font-style:italic;color:#66ff99;"}\
[विभ्रान्ता हरिणी विलोल-नयना संत्रस्त यूथोद्गता]{style="font-style:italic;color:#66ff99;"}\
[अंतः-स्वेद-गजेन्द्र-गण्ड-गलिता संलीलया गच्छति]{style="font-style:italic;color:#66ff99;"}\
[दृष्ट्वा रूपम् इदं प्रियाङ्ग-गहनं वृद्धो ऽपो कामायते ॥३॥]{style="font-style:italic;color:#66ff99;"}(Chandas: शार्दूलविक्रीडित)

[वामेना वेष्टयन्ती प्रविरल-कुसुमं केश-भारं करेण]{style="font-style:italic;color:#66ff99;"}\
[प्रभ्रष्टं चोत्तरीयं रतिपतित-गुणां मेखलां दक्षिणेन]{style="font-style:italic;color:#66ff99;"}\
[ताम्बूलं चोद्वहन्ती विकसित-वदना मुक्त-केशा नरागा]{style="font-style:italic;color:#66ff99;"}\
[निष्क्रान्ता गुह्य-देशाण् मदन-वशगता मारुतं प्रार्थयन्ती ॥४॥]{style="font-style:italic;color:#66ff99;"}(Chandas: स्रग्धरा)

[एषा का नव-यौवना शशिमुखी कान्ता पथी गच्छति]{style="font-style:italic;color:#66ff99;"}\
[निद्रा-व्याकुलिता विघूर्ण-नयना संपक्वबिम्बाधरा]{style="font-style:italic;color:#66ff99;"}\
[केशैर् व्याकुलिता नखैर् विदलिता दन्तैश् च खण्डीकृता]{style="font-style:italic;color:#66ff99;"}\
[केनेदं रति-राक्षसेन रमिता शार्दूलविक्रीडिता ॥५॥]{style="font-style:italic;color:#66ff99;"}(Chandas: शार्दूलविक्रीडित)

Who is this lovely one advancing along the path, face glowing\
moon-like, in the bloom of youth,\
bewildered with lack of sleep, her eye rolling, her lower lip like a\
ripe pomegranate fruit,\
Bewildered by her dishevlled locks, bearing marks of finger nails and teeth,\
How is this ? By a rAkShasa in love, imitating the sporting tiger, has\
she been loved.

(Sporting tiger= शार्दुल विक्रीडिता, शार्दुलविक्रिडित is also the\
name of the Chandas in which the poem has been composed)

[एषा का परिपूर्ण-चन्द्र-वदना गौरी-मृगा क्षोभिनी]{style="font-style:italic;color:#66ff99;"}\
[lIlA-matta-gajendra-hamsa-गमना .............e........]{style="font-style:italic;color:#66ff99;"}\
[निःश्वासाधर-गन्ध-शीतल-मुखी वाचा मृदूल्लासिनी]{style="font-style:italic;color:#66ff99;"}\
[स श्लाघ्यः पुरुशस् स जीवति वरो यस्य प्रिया हीदृशी ॥६॥]{style="font-style:italic;color:#66ff99;"}(Chandas: शार्दूलविक्रीडित)

[एषा का जघन-स्थली सुललिता प्रोन्-मत्त-कामाधिका]{style="font-style:italic;color:#66ff99;"}\
[भ्रू-भञ्गं कुटिलं त्व् अनङ्ग-धनुषः प्रख्यं प्रभाचन्द्रवत्]{style="font-style:italic;color:#66ff99;"}\
[राका-चन्द्र-कपोल-पञ्कज-मुखी क्षामोदरी सुन्दरी]{style="font-style:italic;color:#66ff99;"}\
[वीणी-दण्डं इदं विभाति तुलितं वेलद्-भुजं गछ्छति ॥७॥]{style="font-style:italic;color:#66ff99;"}(Chandas: शार्दूलविक्रीडित)

[एषा का रति-हाव-भाव विलसच्-चन्द्राननं बिभ्रती]{style="font-style:italic;color:#66ff99;"}\
[गात्रं चंपक-दाम-गौर-सदृशं पीन-स्तनालम्बिता]{style="font-style:italic;color:#66ff99;"}\
[पद्भ्यां संचरती प्रगल्भ हरिणी संलीलया स्वेच्छया]{style="font-style:italic;color:#66ff99;"}\
[[किं cऐषा गगनाङ्गना भुवितले संपादिता ब्रह्मणा ॥८॥]{style="color:#66ff99;"} ]{style="font-style:italic;"}(Chandas: शार्दूलविक्रीडित)

[iti श्री-मयूराष्टकं समाप्तम् ]{style="font-style:italic;"}


