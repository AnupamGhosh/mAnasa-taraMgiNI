
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief history of shubhakara सिंह](https://manasataramgini.wordpress.com/2006/05/01/a-brief-history-of-shubhakara-simha/){rel="bookmark"} {#a-brief-history-of-shubhakara-सह .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/01/a-brief-history-of-shubhakara-simha/ "5:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The mahA-चीनाचार्य, shubhakara सिंह, a nAstIka of the second वर्ण, was a remarkable, even if forgotten, figure in history largely forgotten in his homeland India. He was a descendent of the इक्ष्वाकु chieftain अमृतोदन whose clan held a small kingdom in what is the border between modern Madhya Pradesh and Orissa. They were overthrown by the राष्ट्रकूट army and as a consequence fled to ओड्डियान पीठ in central Asia. Here they established themselves as rulers of ओड्डियान. It was here that shubhakara सिंह was born. His father believed he saw the signs of divine favor in him. Just then the brutal onslaught of the terror called Islam was spreading into central Asia under the directives of the Umayyad Kalifs. shubhakara सिंह was put in command of his army at the tender age of 10 years and led his troops to a great victory against the marauding Moslems. The chIna records mention how the kingdom of ओड्डियान repulsed the attack of the Moslems. At the age of 13 his father died and he was declared राजन् of ओड्डियान. He was a popular ruler, but this sparked an intercine conflict with his brothers in course of which he was wounded on his head by the blow of a chakra and an arrow. He however crushed his brothers and regained total control of the kingdom. After this intercine war he is said to have lost interest in the kingdom, and released his defeated brothers and handed the kingdom to the eldest of them. He decided to take सन्न्यास prematurely. But his mother gave him an ancestral pearl, which the symbol of inheritence of the ओड्डियान throne, before he left the palace. He travelled south to saindhava, where he wandered on the shore studying the nAstIka tantras of the bauddhamata. After rigorous japa and yoga he became a great tantric. He found a ship of vaishyas setting forth on a trading expedition around the coast of India to the far East. They agreed to take him along on the journey, and on the ship he kept performing various japa-s to 7 crore देवता-s.

It was said that a white light emanated from his mouth as he was performing his secret yoga. Once when wind did not blow for 3 days he is said to have allowed the ship to go a long way with his mantra prayoga-s. Then the merchants were attacked by Moslem pirates. He performed another prayoga and the deva-s sent another band of pirates that destroyed the Arab pirates. The ship then found his way by sea to Orissa, where he saw a राजन् who had married his sister. He discussed various tantric issues with his brother-in-law, who then came to terms with his renunciation. He then proceeded to Nalanda and studied the highest of the bauddha tantras under the master of the धरणिस्, dharmagupta. shubakara सिंह was initiated into the path of the great yoga by which he could suddenly attain enlightenment. Having attained enlightenment he wandered on कुक्कुट-parvata where he saw महाकाश्यप in meditation. He cut his over grown hair and the lokeshvara placed his hand on his head providing him highest bliss. He spent the monsoons in गृध्रकूट where a lion lead him to a cave, where he saw a vision of the shAkyamuni, the founder of the nAstIka mata.

In his ancestral land in Malva the people were suffering from a major drought and called upon shubhakarasimha to help them. He performed a great sahasrabhuja lokeshvara prayoga by invoking him in the sUrya मण्डल and apparently brought down rain to relieve the land. He then engaged in debates with आस्तीकस् and other नास्तीकस् like the jainas and hInayAna bauddhas in different parts of जम्बुद्वीप. He is said to have spread the bauddhamata with great fervor in his debating spree.


