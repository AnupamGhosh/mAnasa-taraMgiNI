
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the midst of the राक्षसस्](https://manasataramgini.wordpress.com/2006/06/19/in-the-midst-of-the-rakshasas/){rel="bookmark"} {#in-the-midst-of-the-रकषसस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 19, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/19/in-the-midst-of-the-rakshasas/ "4:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having set the spot for the rendezvous with the स्वस्त्री, and having made preparations for the life in the wild we moved of to the abyss. We wished we were accompanied by the learned muni or the knower of spiders, but we were soon in surrounded by the माम्स-devouring राक्षसस् and राक्षसिस्, कृष्ण-कणिष्ठ and others. We roved endlessly being the साक्षि for all, tormented constantly by the shiva-yuvati of महारुद्र. Then we briefly experienced the oneness with अकुलवीर --- the essence of the गङ्गा showered upon us as it does eternally on the terrifying अकुलवीर. We passed by a number of श्मशानस् feeling the oneness with अकुलवीर on account of which we had transcended the dvandvas. We were अकुलवीर, holding the musala and the skull-bowl filled with soma. On our neck were the skulls of the evolutes of प्रकृति passing through the numerous upheavals. We then moved forward and performed आचमनं at the pure riverine waters- we beheld नीलसरस्वती, in her untrammeled charge -- with all her fury eating the rocks and rivers. Then we saw kubjeshvara conjoined with कुब्जिका. Having seen all, we descended slowly downwards towards to पृथिवि, tormented by rAkShasa-गण and others, we kept wandering around with our कापल as our only companion.\
"The end of the rope was reached,\
the rope then snapped.\
The bricks started to fall;\
where there was once a house,\
now there was a ruin;\
the sounds ceased to play."


