
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [जातवेदसे... and श्रेधी व्यवहार](https://manasataramgini.wordpress.com/2006/03/11/jatavedase-and-shredhi-vyavahara/){rel="bookmark"} {#जतवदस-and-शरध-वयवहर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/11/jatavedase-and-shredhi-vyavahara/ "6:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The hymn RV 1.99 is a peculiar hymn in many ways:

[जातवेदसे सुनवाम सोमम्-अरातीयतो नि दहाति वेदः ।]{style="font-weight:bold;color:#ff0000;"}\
[स नः पर्षदति दुर्गाणि विश्वा नावेव सिन्धुं दुरितात्यघ्निः ॥]{style="font-weight:bold;color:#ff0000;"}

It is addressed to agni जातवेदस्: We extract soma for जातवेदस्; may he the knower consume the wealth of the evil enemies. May agni tide us over our troubles, through difficulties as in a boat across the river.

The hymn is the one सूक्तं in the ऋग् that comprises of a single ऋक्. It also re-appears in the tantras as the celebrated invocation of दुर्गा. It may be combined with other tantric incantations in the prayoga of शूलिनी. Likewise, in the triple prayoga combining sharabha, शूलिनी and प्रत्यङ्गिरा, it is in fact the joint attacking and protecting module which forms the left wing, with the ugra-कृत्या ऋक् forming the right-wing of the same prayoga with a principal role in back-hurling. The stuhi श्रुतं ऋक् form the central destroying module.

However, both shaunaka's बृहद्-devata (3.130) and कात्यायन's सर्वानुक्रमणी have a peculiar tradition regarding this hymn जातवेदसे...: It is supposed to be the only listed सुक्तं of a special khila known as the kashyapa khila, which comprised of 1000 सुक्तं-s composed to agni-जातवेदस् and also known as the grand glorification of agni. shaunaka then quotes shlokas of the ancient authority on vedic particulars, शाकपूणि that the 1000 सूक्तं-s followed an arithmetic progression including the जातवेदसे hymn of having 1 to 1000 ऋक्स् in them. Then शाकपूणि calculates the value of the series 1+2+3+...+1000=(1000*1001)/2=500,500. Thus, the shadowy vedic sage शाकपूणि was one of the earliest to provide the correct demonstration of the formula for sum of an arithmetic series. This is clearly older than Pythagoras by any stretch of imagination and at the lower bound the same age as the Rhind Papyrus of Egypt.

It should be Hindus had a long-standing fascination with series (known in सन्स्कृत् as श्रेधी) of various types from the earliest vedic period. In the तैत्तिरीय saMhita (7.2.11-20) there are numerous simple arithmetic series associated with the "multiplicator", ascending and descending offerings. One of these is a power series of 10: 10^2, 10^3, 10^4, 10^5, 10^6, 10^7, 10^8, 10^9, 10^10,1 0^11, 10^12. This series also occurs in the तैत्तिरीय ब्राह्मण 3.8.16 and is repeated verbatim in several hindu works like the रामायण to exaggerated size of the rAkShasa or वानर army (e.g. yuddha काण्ड 3.24-28). The पञ्चविम्श ब्राह्मण 18.3 gives the series: 12, 24, 48, 96, ...196608, 393216 showing that the early Hindus also worked on geometric series. पिङ्गल in the Chandas शास्त्र provides a formula for the sum of a geometric series. पिङ्गल also provides early accounts of the binomial expansion series. This early fascination with the series for its own sake is unique to the Hindus, beginning well before any Egyptian mention of it and developing in an algebraic sense well beyond that of their peers. This suggests that it was an original development of the Hindus. In the classical period of Hindu mathematics, beginning with हलायुद्ध, passing through the pinnacle of the illustrious आर्यभट, this fascination for the series was formalized, with the term श्रेधी meaning progression being adopted. shredhi-phala or संकलित is used for sum of series the formulae for it and nth terms of the series were all obtained. The formulae are also given form composite series of the form: tn=r\*tn-1+b or r\*tn-1-b\
The binomial expansions were provided as meru-prasthas or what were known in the West as Pascal's triangles. Finally, the great Nambuthiri mathematicians developed the trignometric series expansions and the concept of series with limit n-\>infinity (kha-hara) to obtain definite integrals.


