
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Tyrannosaurs and morphological evolution in coelurosaurs](https://manasataramgini.wordpress.com/2006/02/11/tyrannosaurs-and-morphological-evolution-in-coelurosaurs/){rel="bookmark"} {#tyrannosaurs-and-morphological-evolution-in-coelurosaurs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/11/tyrannosaurs-and-morphological-evolution-in-coelurosaurs/ "5:27 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The recent find of the tyrannosaur [Guanlong ]{style="font-style:italic;"}going back to the late Jurassic, around 156-161 Myr is of interest when placed in the context of the oldest bird, [Archaeopteryx]{style="font-style:italic;"}, which from a slightly later Jurassic layer dating to around 150 Myr. [Guanlong ]{style="font-style:italic;"}also suggests that the other primitive tyrannosaurs which were roughly coeval with [Archaeopteryx]{style="font-style:italic;"}, like [Stokesosaurus ]{style="font-style:italic;"}and [Aviatyrannus ]{style="font-style:italic;"}were approximately in the same morphological grade. The current phylogenetic analysis (by number of researchers) supports a more basal position for the tyrannosaurs in the coelurosaur clade, with several successive clades, namely the ornithomimosaurs and the therizinosaur-oviraptorosaur clade forming the sister-group of a crown assemblage comprising of scansoriopterygids-deinonychosaurs (=troodontids+dromeosaurs)-birds. This would mean that the major lineages of coelurosaurs had definitely split up by the late Jurassic and probably occurred in the early Jurassic. The lower jaw which forms the basis of the species [Eshanosaurus]{style="font-style:italic;"}, a supposed therizinosaur from the early Jurassic of Yunnan, if real, might suggest that this happened even earlier.

The upshot of all this, assuming that there are no major problems with the current phylogeny, is that the coelurosaurian morphology evolved at very different rates. The crown group of the scansoriopterygid-deinonychosaur-bird appears to have reached their basic specialized morphological condition rather early on. The tyrannosaurs in contrast appear to have lingered on in a generalist mophological state till the very late Cretaceous. This is sort of consistent with the fact that even in the Cretaceous, perhaps until quite late the tyrannosaurs retained their generalist ancestral morphology as seen in [Dilong]{style="font-style:italic;"}, [Eotyrannus ]{style="font-style:italic;"}and [Dryptosaurus]{style="font-style:italic;"}, only growing a larger in size. The [Archaeopteryx ]{style="font-style:italic;"}condition appears to be primitive for the deinonychosaur-bird clade, with experiments seen in dromeosaurs like [Microraptor]{style="font-style:italic;"}, that further developed the leg feathers to become the "4-winged fliers".


