
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Attaining अथर्वण mantra ज्ञान](https://manasataramgini.wordpress.com/2006/02/06/attaining-atharvana-mantra-gyana/){rel="bookmark"} {#attaining-अथरवण-mantra-जञन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/06/attaining-atharvana-mantra-gyana/ "6:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Acquiring the atharvaveda either requires you to be born as a scion of the atharvan clan or acquire it through a separate upanayanam. The key to this new upanayanam is the need for the student to chant the सूक्तं known as त्रिषप्तीयम् (AV-S 1.1 or AV-P 1.6). Only by chanting the त्रिषप्तीयम् does the power of the atharvanic mantras enter him so that he can use them. Thus the त्रिषप्तीयम् is also used before commencing practice of atharvanic recitation or स्वाध्यायः. For deploying an atharvanic spell for protection one needs the first mantra of the त्रिषप्तियम् to be recited. In the atharvanic upanayanam there is a special ritual apparently not seen in other vedic traditions. The father or teacher ties feathers of a शूक (parrot), a sAri (myna) and a कृश (Wren warbler) with a yellow string around the students neck. Then these are offered in the sacrificial fire with the त्रिषप्तीयं. In a more gory shaunaka rite of the past the tongues of these birds were similarly tied and eaten. This qualifies him to start acquiring atharvanic vidya.\
AV-S 1.1\
[ये त्रिषप्ताः परियन्ति विश्वा रूपाणि बिभ्रतः /]{style="color:#99cc00;"}\
[वाचस्पतिर् बला तेषां तन्वो अद्य दधातु मे //१//]{style="color:#99cc00;"}\
Three times seven that go around, bearing all forms;\
let वाचस्पति put their powers into my body's \[parts]

[पुनर् एहि वछस्पते देवेन मनसा सह /]{style="color:#99cc00;"}\
[वसोष्पते नि रमय मय्य् एवास्तु मयि श्रुतम् //२//]{style="color:#99cc00;"}\
Come here again वाचस्पति with the mind of the devas;\
lord of riches, make it stay in me, in myself the shruti.

[इहैवाभि वि तनूभे आर्त्नी इव ज्यया /]{style="color:#99cc00;"}\
[वाचस्पतिर् नि यछतु मय्य् एवास्तु मयि श्रुतम् //३//]{style="color:#99cc00;"}\
Just here stretch on, like the two ears of the bow with the bowstring;\
Let वाचस्पति hold in me, in myself the shruti.

[उपहूतो वाछस्पतिर् उपास्मान् वाछस्पतिर् ह्वयताम् /]{style="color:#99cc00;"}\
[सं श्रुतेन गमेमहि मा श्रुतेन वि राधिषि //४//]{style="color:#99cc00;"}\
वाचस्पति is called upon, on us let वाचस्पति call;\
may we united with the shruti and not parted from the shruti.

The key rahasyas here are: the jaws are the Artni-s of the bow (1.1.2) and the vocal cords are the jya or the bow-string.\
The first mantra gives the most reduced form of the sounds of संस्कृत. It is from this most condensed set described as 3*7=21 that all sounds of the shruti are derived by knowning this (as the mantravit-s know it, not the plebeian) one gains the profound insight of the shruti. Hence the kaushika calls it the मेधाप्रजनन mantra.

Thus we have:\
...............

 1.  a -> A

 2.  i-\>I

 3.  u-\>U

 4.  ऋ->ॠ, L\^i, ॡ

 5.  e

 6.  ai

 7.  o

 8.  au = all above svara\
...............

 9.  aH=visarga\
...............

 10.  ya, 11) ra, 12) la, 13) va =अन्थःस्थ (half vowels)\
...............

 14.  ka-\>kha, ga, gha

 15.  c-> cha, ja, jha

 16.  T-\>Tha, Da, Dha-\>La

 17.  ta-> tha, da, dha

 18.  p-\>pha, ba, bha= all above sparsha\
...............

 19.  sa-\>sha, Sha = sibilants\
...............

 20.  ha = ऊष्मन्\
...............

 21.  M->अनुनासिक+ma, na, Na, ञ, ङ = nasals\
...............

Thus the atharvan tradition condenses all the sounds of Chandas to a set of 21 that are invoked to enter you in the त्रिषप्तीय rite. This marks the beginning of Hindu linguistics where the principle of homologous condensation was recognized. This is summed up by पतञ्जलि in his महाभाष्य thus:\
[अवर्णाकृतिर् उपदिष्टा सर्वं अवर्ण-कुलं ग्रहीष्यति, तथेवर्णाकृतिः तथोवर्णाकृतिः ।]{style="color:#99cc00;"}\
The theoretical form of the a sound, when being taught, with contain the whole family of a-like sounds, so also with the theoretical forms of i and u sounds. Thus, the principle of homologous reduction stems from the root of the vedic tradition itself and made explicit in atharvanic education.


