
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [अथर्वणिc संध्योपासन vidhi](https://manasataramgini.wordpress.com/2006/06/06/atharvanic-samdhyopasana-vidhi/){rel="bookmark"} {#अथरवणc-सधयपसन-vidhi .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/06/atharvanic-samdhyopasana-vidhi/ "5:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The practioners of the fourth veda are dying creed. As we may be amongst the very last of the practioners and some who are learning it in a recent revival do not known of how the अथर्वण संध्य needs to be done we provide some details. As this is a common question asked by the atharvan revivalists, with time I hope to provide a detailed protocol of atharvan संध्य, atharvan उपाकर्म and some other important para-vedic rites of the atharvan-s like the skanda याग or the धूर्त homa. Here is an outline for the संध्य

  - The AV संध्य is done seated in the Urdhva-जाह्नुः वीरासन.

  - It is to be done in the north or east of the place of residence and facing the sun.

  - before it one purifies himself with the आपोहिष्ठिय ablution.

  - One sips water (आचमनं) with the formula jIvA stha (AV-S19.69.1-4)

  - One sprinkles water on oneself with formula Apo hi ष्ठ (AV-S1.5). One touches ones knees when the incantation yas-यक्षयाय jinvata is uttered.

  - One says agnishcha mA manyushcha and suryashcha mA manyushca purificatory formulae.

  - One performs three प्रानायाम-s with the 7 व्याहृति-s+gAyatrI+brahma-shiras.

  - One performs आचमनं as above again.

  - One stands up and throws water upwards with the formula अयोजाल (AV-S19.66)

  - One faces the sun and chants the formula as per the संध्य:\
morning- हरिः सुपर्ण (AV-S19.65.)\
noon- ud u त्यं + ud वयं तमसः (AV-S20.47.13+AV-S7.53.7)\
evening-ud ghed abhi (AV-S20.7)

  - One then performs gAyatrI japa 8, 11, 12, 15, 100 or 1000 times.

  - Then one stands up again and utters the formula pashyema शरदः (AV-S19.67)

  - Then one chants still standing AV-S19.70, 19.71, 12.1.18, 10.5.37, 19.16, 19.72


