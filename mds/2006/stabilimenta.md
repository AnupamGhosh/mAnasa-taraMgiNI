
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Stabilimenta](https://manasataramgini.wordpress.com/2006/06/15/stabilimenta/){rel="bookmark"} {#stabilimenta .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/15/stabilimenta/ "4:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R and I like naturalists for almost a century before us were fascinated by stabilimenta of spider webs and wondered what their role was. R independently of the great Eisner had a belief that stabilimenta might have role in warning flying or moving animals from running into webs and pulling them down. Her conjecture came from the observation that stabilimenta containing webs are more easily seen by vertebrates like us than the other webs, into which we used to run into all the time and occasionally get spiders in our hair. I had my initial doubts about it because I wondered if that were the case would they not attract spider killers like wasps or humming birds. Before we became aware of Eisner's brilliant studies which provided strong evidence that stabilimenta indeed served as a warning for flying vertebrates in day time I made an observation supporting R's conjecture. I noticed that some orb spinning araneids strung the carcasses of their victims in the form of a straight line giving a stabilimentum like appearance. These spiders also kept their webs up during the day and might have convergently evolved a tactic for signaling their web's presence.

More recent studies suggest that stabilimenta of some araneid orbs spinners may help them in some way (perhaps distraction) to escape parasitoid wasps that attack them. Other studies also indicate that the stabilimenta may reflect at wavelengths normally not seen by insects. We wonder if all these represent a second round of fine tuning by natural selection to fix "holes" in their primary selective function. Finally we were also puzzled by the stabilimenta like decorations used by 3D lattice web spiders at the bottom of their webs. Is there some role in protection from parasitoid wasps?


