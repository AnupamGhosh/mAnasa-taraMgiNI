
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The left-handed अष्ठ-mukha गण्डभेरुण्ड नृकेशरि mantra](https://manasataramgini.wordpress.com/2006/03/11/the-left-handed-ashtha-mukha-gandabherunda-nrikeshari-mantra/){rel="bookmark"} {#the-left-handed-अषठ-mukha-गणडभरणड-नकशर-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/11/the-left-handed-ashtha-mukha-gandabherunda-nrikeshari-mantra/ "6:00 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is said that there are two emanations of the mantra of the most terrible अष्ठ-mukha गण्डभेरुण्ड नृसिम्ह (AMGN)- a दक्षिण and वाम form. The right-handed form is said to lie in the text termed the bhIma-tantra which is a upa-tantra of the ananta-vijaya saMhitA of the विष्णु-यामल tradition. The left-handed form is said to like in the text termed वामा-tantra which is also an upa-tantra of the ananta-vijaya saMhitA. Not much is know of the full extant of these texts ( if they survive in an degree at all they are obviously kept a close secret) but fragments relevant to ritual performance have been transmitted to those in the know. It is however clear that the left-handed vidya has propagated considerably in various guises. AMGN is said to have 8 heads of the leonine form with saber teeth, whereas two heads of the dinosaurian form (which are apparently concealed as per the वाम mantra). He has 16 hands with a garland of skulls and holds a chakra, trident, गदा, sword, bow and arrow, in addition to various other weapons. The mantra was narrated thus by महादेव to इशानी when she embraced him and kissed him:\
[\
वेदानाम् आदिमं चैव अर्धेन्धु-बिन्दु-भूषितं ।]{style="font-style:italic;color:rgb(255,0,0);"}\
[पश्चाद् अष्टाननयेति पिङ्गोर्ध्व-केश-वर्त्मने ॥]{style="font-style:italic;color:rgb(255,0,0);"}\
[चतुर्विंशतिनेत्राय तदनु षोडशभुजाय ।]{style="font-style:italic;color:rgb(255,0,0);"}\
[कृष्ण-जीमूत-वपुषे कपाल-मालानेक धारिणे ॥]{style="font-style:italic;color:rgb(255,0,0);"}\
[अध्यान्त-क्रूर-चित्ताय अर्धेन्दु-दंष्ट्रिणे ।]{style="font-style:italic;color:rgb(255,0,0);"}\
[मारय मारय कारय कारय गर्जय गर्जय तर्जय तर्जय शोषय शोषय सप्तसागरान् बन्ध बन्ध नागाष्टकान् गृह्ण गृह्ण शत्रून् ह हा हि ही हु हु हे है हो हौ हं हः च अस्त्रं च अग्नि ललना॥]{style="font-style:italic;color:rgb(255,0,0);"}

This formula's origin in the brahminical विष्णु यामल tradition is supported by the use of the term "वेदानां आदिमं" in describing the प्रणव in the mantra. This allusion to the प्रणव, the leader of the veda, has found its way into the चीनाचारा tantras unaltered showing the direct lateral transfer of this mantra from the आस्तीक Hindu sources. The famed sadhaka of चीनाचार, matidhvaja shribhadra, became a knower of these mantras after acquiring them from a brahmin savant from Bihar. He journeyed to the court in Xanadu of the great Khan Kublai, the grandson of Chingiz Kha'Khan. The Kha'Khan appointed him to contruct a new script for the Mongol language. In course of this he initiated the Kha'Khan into the mantras of vama-AMGN and he became its practioner. From there it became popular in the secret realm of चीनाचार.


