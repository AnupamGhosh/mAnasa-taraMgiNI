
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Alick Walker](https://manasataramgini.wordpress.com/2006/01/28/alick-walker/){rel="bookmark"} {#alick-walker .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 28, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/28/alick-walker/ "7:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Late Alick Walker was an English gentleman who studied archosaurian anatomy. He was a retiring kind of person who is said to have put in tremendous effort to study in great depth the anatomy of reptiles. He is said to have been very different from today's researcher. He did not care to publish to much, nor did he compete agressively and violently like most modern scientists. He instead quitely did his work with great focus and intensity. He gathered enormous amounts of facts for science's sake and gave them freely to other scientists. His most important work was the study of the anatomy of the primitive crocodile [ष्फेनोसुछुस्]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}. His magisterial work on Sphenosuchus contains a deep understanding of the basic anatomical unity of the archosaurian form- be it a bird or crocodile. His anatomical notes are central to understanding the early evolution of crocodiles and their immediate sister groups the Rauisuchians, Ornithosuchians, Prestosuchians and Aetosaurs. However, he did not study the later dinosaurs too carefully. This lead to an erroneous idea that birds had descended from early crocodiles rather than being dinosaurs. But towards the end of his life he was resigned to accept that birds were indeed dinosaurs.


