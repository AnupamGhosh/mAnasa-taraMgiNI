
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [trishiro-bhairava](https://manasataramgini.wordpress.com/2006/03/04/trishiro-bhairava/){rel="bookmark"} {#trishiro-bhairava .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 4, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/04/trishiro-bhairava/ "3:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/trishirobhairavas.0.jpg){width="75%"}
```{=latex}
\end{center}
```



Images of the hallowed trishirobhairava from काश्मीर्, whose अवतार puruSha is दत्तात्रेय. These idols were originally from temples of trishiro bhairava near परिहासपुर. They show the left side ugramukha which is (rudra murti) and may have a कपाल, कमण्डलु and अक्षमाल in the hands. The same iconography is retained for दत्तात्रेय but, the कापाल of the trishirobhairava form is not seen. The temples were destroyed and the idols damaged by the Moslems and their current status is unclear. The representation of the deity as trishiro-bhairava and दत्तात्रेय were both supposed to have dogs beside them, but their presence in these idols is unknown.



