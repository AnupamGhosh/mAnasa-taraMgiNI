
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [श्री श्यामोल् rAm's exposition of महाचीनक्रम](https://manasataramgini.wordpress.com/2006/02/25/shri-shyamol-rams-exposition-of-mahachinakrama/){rel="bookmark"} {#शर-शयमल-rams-exposition-of-महचनकरम .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 25, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/25/shri-shyamol-rams-exposition-of-mahachinakrama/ "6:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While महाचीनक्रम is a not a path of tantra usually followed by the shrauta-स्मार्त ritualist, it was once secretly practiced by several, including brahminical पण्डितस्. Some believe such पण्डितस् practicing महाचीनक्रम where fallen brahmins, often associated with the nAstIka matas, but others believed that they had reached a state of "siddha-hood" that made them transcend the sacral and sacrilegious state. श्री श्यामोल् rAm whom we introduced earlier was an exponent of महाचीनक्रम and his उपासन deities included महाचीनक्रम-tArA, एकजटा, and a form of बृहस्पति known as मङ्जुघोश. Some details of his texts and साधनस् were recorded even though these साधनस् may no longer be practiced. One notable collection was the साधन-shata-पञ्चाशिका that was collected by the great वामाचारी and कापालिक, कृष्णपाद. He then gave it to his equally great student शाश्वतवज्र. He then gave it to his student अभयाकरगुप्त who transmitted it to a nAstIka student in the nalanda university and he himself settled in the hills of हिमाचल् pradesh where he had other students.

महाचीनक्रम-tArA is also described as प्रेतारूढा. She stands on corpse of man slain in battle, in the प्रत्यालीधा pose, with the left foot extended and the right drawn back (some also call it the वीरभद्रासन). She holds in her right hands a sword and a cleaver and in her right hands a blue lotus and a skull. She of dark blue complexion and has sharp long fangs, a beautiful tongue that she sticks out and 3 eyes. She has well-formed and firm breasts and her tawny hair is tied up in a single knot. She wears a tiger skin skirt, eight snake bands, a garland of severed heads, and the following ornaments of bones: a श्वेतास्थि paTTa- a white bone hair-band; अस्थिकुण्डल, asthi-कण्ठाभरणं, asthimekhala (waist-band) and asthiruchka (bracelets). She utters a terrifying laugh.

The yoga of महाचीनक्रम-tArA is done thus:

  - The साधक may choose an appropriate श्मशान for the purpose and retire there at night with his दूती.

  - There he sits on the पञ्च muNDa seat with his दूती and meditates upon 3 vajras radiating rays pervading the 3 states of existence.

  - Then he draws in the rays and meditates upon the empty void of spanda and mutters the mantra: oM शून्यताज्ञान vajra स्वभावात्मको .aham |

  - Then he sees the red syllable aH in the sky, which transform into a red lotus.

  - Then he sees the white syllable tAM on top of the red lotus, which then transforms into a skull cup.

  - Then the sun appears in the middle of the skull cup. In the middle of it appears a sun-spot that becomes the dark colored bIJa hUM.

  - Then the bIja hUM transforms into a cleaver, which itself is adorned by the hUM bIja.

  - The cleaver then transforms into the साधक performing the yoga.

  - The साधक then identifies completely with महाचीनक्रम-tArA. It is critical that the साधक has transcended अहम्कार at this point else the साधान could damage him.\
Then he mutters the mantra oM ह्रीं स्त्रीं hUM फट् and enters japa.


