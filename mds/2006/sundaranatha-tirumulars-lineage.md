
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सुन्दरनाथ (तिरुमूलर्)'s lineage](https://manasataramgini.wordpress.com/2006/08/25/sundaranatha-tirumulars-lineage/){rel="bookmark"} {#सनदरनथ-तरमलरs-lineage .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 25, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/25/sundaranatha-tirumulars-lineage/ "4:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the preamble to the तिरुमन्तिरं तिरुमूलर् (verses ) gives his lineage thus (translated only loosely during conversation with my clansmen who know the drAviDa भाष/transliterated from a reading in an old book possessed by them):

[n\^an\^dhi अरुळ्पेऱ्‌ऱ न्^आधरै n\^ADiDin]{style="font-style:italic;font-weight:bold;"}\
seek the नाथ-s (tantric adepts) who have attained the grace of nandin,

[न्^अन्^धिकळ् न्^आल्वर् chivayOka mAmuni]{style="font-weight:bold;font-style:italic;"}\
The 4 nandins, and then the muni शिवयोगी,

[मन्ऱू thozudha padhanychali viyAkramar]{style="font-style:italic;font-weight:bold;"}\
पतञ्जलि of the holy temple and व्याघ्रपाद,

[एन्ऱिवर् ennO डेण्मरु mAmE ]{style="font-weight:bold;font-style:italic;"}(TM 67)\
and with I complete you count to eight.

Thus we have 4\*nandin-s -> shivayogi -> पतञ्जलि -> व्याघ्रपाद -> सुन्दरनाथ (तिरुमूलर्).\
This fascination with nandin prevades the whole of the tirumantiram

For example we have in the preamble:\
[n\^an\^dhi aruLAlE न्^आथनाम् pErpeRROm]{style="font-weight:bold;font-style:italic;"}\
By the grace of nandin I attained the name amongst his नाथ-s,

[n\^an\^dhi aruLAlE न्^आधनै n\^ADinOm]{style="font-style:italic;font-weight:bold;"}\
By nandin's grace I sought the first नाथ,

[n\^an\^dhi अरुळाव dhencheyum n\^ATTinil]{style="font-style:italic;font-weight:bold;"}\
What can I not do with nandin's grace.

[n\^an\^dhi वज़िकाट्ट न्^आनिरुन्^ dhEnE (TM 68)]{style="font-weight:bold;font-style:italic;"}\
With nandin showing the way I remain here in the \[guru-lineage].

Further we find विनायक apparently called nandin's son in the introductory invocation:\
[n\^an\^dhi मकन्ऱनै न्यानक् kozun\^dhinai]{style="font-weight:bold;font-style:italic;"}\
[pun\^dhiyil वैथ्थडि pORRukin REnE]{style="font-weight:bold;font-style:italic;"}\
nandin's son, a bloom of knowledge\
I meditate on him and worship his feet.

We do not know for certain if this preamble of the tirumantiram was added later, but my sources believe that on linguistic grounds it should be considered an integral part of the text. This tradition of attributing a lineage to nandi does not follow the tradition of teachers of several well know tantras including श्रिविद्या, where we see illustrious matsyendra नाथ. In a sense these lineages vaguely resemble, in concept, those stated for the krodhabhairava सूत्रस्, siddha-योगेश्वरी-mata, the glorious विश्वाद्य तन्त्रं, which expound the mysterious योगिनी-जाल of विश्वा and the famed योगिनी-जाल-shambara. However no tradition as that of तिरुमूलर् is seen in any सन्स्कृत् text. Nevertheless, the क्रोधानल saMhitA of the krodhabhairava tradition is said to have been founded by नन्दिनाथ and transmitted by him. Of which only the binduvijaya is supposed to survive in fragments. Amongst the drAviDa सिद्धान्तिन्-s there is a belief that a part of the raurava Agama was transmitted by nandin; however, a lineage akin to that given by tirumular is not attested there to the best of my knowledge.

But the tirumantiram names nine tantras as a part of the tradition it received from nandin. These are described in verse 63 of the preamble as:\
पेऱ्‌ऱन्^अल् आकमङ् [कारण]{style="font-weight:bold;"}N^ [kAmikam]{style="font-style:italic;"}\
uRRanal [वीरम्]{style="font-weight:bold;"} uyar[chin\^dham]{style="font-weight:bold;"} [vAdhuLam]{style="font-weight:bold;"}\
maRRav [वियामळ]{style="font-weight:bold;"} mAkuN^[kAlOththara]{style="font-weight:bold;"}n^\
thuRRan\^aR [chuppiran]{style="font-weight:bold;"}y chollu [makuDam]{style="font-weight:bold;"}E (TM 63)

All the tantras named here belong to the Urdhva-srotas emanating from इशान or the upper head of the five-faced deva. They are divided into the 10 shiva Agamas and 18 rudra Agamas, both of which are seen in the nandin tradition of tirumular. They are:\
principal शिवागमस्: 1) कारण 2) कामिका 4) chintya 8) suprabheda ; principal रुद्रागमस्: 3) vIra 5) वातुल 6) vimala 9) मकुट उपागम of the किरण tantra stream 7) kallottara.

It should be noted that within the tirumantiram a particular hierarchy of the siddhAnta tantras is implicit, with the above 9 being primary. This, however, is not mentioned in the surviving Sanskrit tradition. It again points to the fact that tirumular's lineage was probably an early branch of the siddhAnta tantric tradition that possibly had its unique set of chosen tantras amongst the core shiva tantras and rudra tantras, which it considered primary. Thus, we encounter this verse:

Akamam ओन्पान् अथिलान n\^AlEzu\
There are nine Agamas that emerged in the ancient time\
mOkamil n\^AlEzu muppEtha muRRuTan\
Then they split up into three paths\
vEkamil vEthAn\^tha चिथ्थान्^थ meymaiyon\
To display the truth of vedAnta and siddhAnta\
RAka मुटिन्^थ varunychuththa chaivamE (TM1429)\
That is the secret knowledge of shuddha shaiva

The genuine connection of the siddhAnta Agamas and the tradition of तिरुमूलर् is established beyond doubt in the discussion of the celebrated पञ्चाक्षरी विद्या (नमः शिवाय) seen in the verse 890 onwards. Here तिरुमूलर् reaches the height of his work, describing at length the extraordinary विकृतिस् of the पञ्चाक्षरी and the numerous yantras derived from it. He also explains the harihara yantra, establish the archaic nature of this yantra/mantra and bringing memories of the ancient हरिहरालयस् of Angkor and Vietnam. In fact few of the yantra-s derived from the पञ्चाक्षरी विद्या mentioned by tirumular are of utmost importance as an independent evidence because the Sanskrit originals are lost (though their fragments are very much retained in mantra manuals).

Taken together with references to other incidents veiled by mysticism, we may infer that around 500 AD Kashmirian, or Kashmir-trained shaiva tantric like सुन्दरनाथ entered the drAviDa desha and seeded a tamil tradition. For this they claimed a distinctive lineage from nandin, which they might have inherited from a lost Sanskrit tradition related to krodhabhairava or derived it afresh in the drAviDa lands. They appear to have originally considered as authoritative a set of 8 principal Agamas and 1 उपागम of the siddhAnta srotas, and were also influenced heavily by the श्रीकुल tradition --- of which we do not know which tantra-s they followed. However, from the form of the पञ्चदशि which is given by तिरुमूलर् we may conclude that they were affiliated with कादिमत form of the श्रीविद्या mantra, unlike most later Kashmirians and Himachalas who tended to follow the हादिमत. They were clearly vedic in affiliation, in that they held the veda as the highest authority and respected the vedic याग-s, though for all practical purposes the Agamic religion dominated their thought. The important point to note here is that from an early period there was a symbiosis between various shaiva and shAkta tantric schools. Of the latter the dominant ones were कुब्जिकामत, श्रीकुल, कालिकुल, and त्रिका.


