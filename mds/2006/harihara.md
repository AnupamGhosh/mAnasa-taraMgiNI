
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [harihara](https://manasataramgini.wordpress.com/2006/07/25/harihara/){rel="bookmark"} {#harihara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 25, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/25/harihara/ "5:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/harihara_ruins.png){width="75%"}
```{=latex}
\end{center}
```



