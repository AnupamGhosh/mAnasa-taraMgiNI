
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [From परीक्षित्'s window](https://manasataramgini.wordpress.com/2006/04/22/from-parikshits-window/){rel="bookmark"} {#from-परकषतs-window .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/22/from-parikshits-window/ "10:58 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From the top of the fort, from the window we watched the desolate landscape cloaked in the moist haze of indra sprinklings. The last cry of the bird had been heard for the day. The अमात्य and the shachiva were caught in their own troubles due the passing of the वृद्ध. On the shore Jx sat still unable to come to terms with the fact that he was free. His mantri-s were also caught in the same troubles as our mantri-s. The muni had retreated into his cave. We reminisced of our brief conversation with him on the eve of the tragedy. The muni shared my concerns at that point and our minds went back to the fateful clear night when the PM was rattling under a घोराभिचार. He saw how that very day we had taken the worst turn with respect to the issue of खाण्डव. Having sized the situation we knew it was alright for us to die, but the muni had to escape to continue the fight. We spoke then of that step into the कालछिद्र. We did not know what was happening to our arrows in the great raNa; we knew then knew what had befallen the brave इक्ष्वाकु of ayodhya, when he stood before the fierce भरद्वाज warrior who was raging on that fateful day.

We looked again from the window. We saw that our yauvana सेनानि had conquered indra जाल I and II completely and paid us a tribute from those raids after placing our flags. The other yoddha along with Hayastanika were still being needled without stop by the single दुष्ट shatru. Our सेनानि decided to tackle the सुत्रान्त front where one fierce vairi and another mild one had come up against us. He skillfully re-organized the troops to strengthen our front. But then our time of reckoning had come. We watched; the feeling was like how कृष्नदेव might felt before Kovilkonda or rAma rAya before Talikota or हम्मीर महापत्र behind Kondavidu. Many shatru-s had combined forces against us. We were weakened like the आचार्य struck by navagupta; the धूमवती agents continued to assail us non-stop and follow us like a shadow. But we were born with the karma of the warrior and had to fight, even if like the सूतपुत्र without the armor and earrings. We hoped to smite the dasyu in that great battle.


