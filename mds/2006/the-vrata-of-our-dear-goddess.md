
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The vrata of our great goddess](https://manasataramgini.wordpress.com/2006/01/28/the-vrata-of-our-dear-goddess/){rel="bookmark"} {#the-vrata-of-our-great-goddess .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 28, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/28/the-vrata-of-our-dear-goddess/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We bow humbly to the 6th part of प्रकृति. On the 6th bright lunar day or for pediatric purposes on the 6th day after childbirth he performs the vrata to षष्ठी

ध्यानं\
[सुपुत्रदां च शुभदां दया-रूपां जगत्-प्रसू ।]{style="color:#99cc00;"}\
[श्वेत-चम्पक-वर्णाभां रत्न-भूषण-भूषितां ।]{style="color:#99cc00;"}\
[पवित्र-रूपां परमां देवसेनां परां भजे ॥]{style="color:#99cc00;"}

He does japa, homa and तर्पण with the following षष्ठी mantra:\
[प्रणव+भुवनेश्वरि+षष्ठी-देव्यै+अग्नि प्रिया ॥]{style="color:#99cc00;"}

Thus meditating using the dhyAna, the worshipper should place a flower on ones own head. Then again meditating and muttering the above मूल mantra the votary offers पाद्य, arghya and आचमनीय, and sandal-paste, flowers, incense, lamp waving, naivedyam of food, delightful bhojya roots and fruits with the पङ्चोपचार formulae. The one should do japa of the मूल mantra. Finally one should utter the below stotra of kShatriya priyavrata connected with the kauthuma school of the सामवेद and offer devote worship. Then one may ritually dismiss the deity asking her to be favorable, pleased and boon-giving.

[नमो देव्यै महादेव्यै सिद्ध्यै शान्त्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[शुभायै देवसेनायै षष्ठ्यै देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[वरदायै पुत्रदायै धनदायै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[सुखदायै मोक्षदायै षष्ठ्यै देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[सृष्ट्यै षष्ठाशरूपायै सिद्धायै च नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[मायायै सिद्धयोगिन्यै षष्ठी-देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[सारायै सारदायै छ परादेव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[बालाधिष्ठातृ-देव्यै च षष्ठी-देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[कल्याणदायै कल्याण्यै फलदायै छ कर्मणाम्।ह् ।]{style="font-weight:bold;color:#99cc00;"}\
[प्रत्यक्षायै स्वभक्तानां षष्ठ्यै देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[पूज्यायै स्कन्द-कान्तायै सर्वेषां सर्व-कर्मसु ।]{style="font-weight:bold;color:#99cc00;"}\
[देव-रक्षण-कारिण्यै षष्ठी-देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[शुद्ध-सत्त्व-स्वरूपायै वन्दितायै नृणां सदा ।]{style="font-weight:bold;color:#99cc00;"}\
[हिम्सा-क्रोध-वर्जितायै षष्ठी-देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[धनं देहि प्रियां देहि पुत्रं देहि सुरेश्वरि ।]{style="font-weight:bold;color:#99cc00;"}\
[मानं देहि जयं देहि द्विषो जहि महेश्वरि ।]{style="font-weight:bold;color:#99cc00;"}\
[धर्मं देहि यशो देहि षष्ठी-देव्यै नमो नमः ।]{style="font-weight:bold;color:#99cc00;"}\
[देहि भुमिम्।ह् प्रजां देहि विद्यां देहि सुपूजिते ।]{style="font-weight:bold;color:#99cc00;"}\
[कल्याणं च जयं देहि षष्ठी-देव्यै नमो नमः ॥]{style="font-weight:bold;color:#99cc00;"}

[॥ इति प्रियव्रत विरचित श्री षष्ठी-देवी स्तोत्रं समाप्तं ॥]{style="color:#99cc00;"}


