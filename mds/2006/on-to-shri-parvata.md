
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On to श्री-parvata](https://manasataramgini.wordpress.com/2006/11/29/on-to-shri-parvata/){rel="bookmark"} {#on-to-शर-parvata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 29, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/29/on-to-shri-parvata/ "7:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/x/blogger2/6438/855/320/318420/कुमारgRiha.png){width="75%"}
```{=latex}
\end{center}
```



[The core altars of a कुमार-गृह. The green elements mark the fire altars]{style="font-style:italic;"}

He sat in the place where the first of the pieces had fallen. In his mind's eye h remembered the tale of the great grahanatha: "तारकासुर the accursed enemy of the gods wore a लिङ्ग around his neck that protected him from death as long as it was there. As he fought विष्णु and वीरभद्र their weapons failed on him after prolonged and fierce duels. Finally the one born of the arrow-forest charged forth to meet the daitya. After a prolonged duel he finally struck the daitya with his charmed shakti, from which none escape. The shakti shattered the लिङ्ग into 5 fragments and the pierced the daitya-s broad chest slaying him. The pieces of the लिङ्ग fell on पृथिवि and each became a maha-लिङ्ग in which The god is worshiped." It was near the first of these pieces he had constructed a कुमार गृह. After having performed the kha-chakra-dharaNi he offered the oblations to the bull-headed nandikeshvara at the appropriate altar. Then he moved over to the altar of पिलिपिच्छिका and offered seven oblations: पिलिपिच्छिका, रेवती, शकुनी, षष्ठि, जातहारिणि, मुखमण्डिका and देवसेना on the पञ्चमि day. Even as the last oblation hit the fire a terrifying graha named परिप्रकाश arose, filling the air with awe that only the masters of the महामन्त्र know. He muttered the graha-स्तोत्रं was transported to the great mound of श्री-parvata by that awful graha. Atop श्रीपर्वत he remained lying at night performing the complete cycle of the kha-chakra-dhyAna. He joined the siddhaugha.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/x/blogger2/6438/855/320/994496/nakShatra_nyAsa2.png){width="75%"}
```{=latex}
\end{center}
```

\
The start- and end-point of the kha-chakra-dhyAna


