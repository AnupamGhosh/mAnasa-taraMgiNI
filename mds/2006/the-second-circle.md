
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The second circle](https://manasataramgini.wordpress.com/2006/05/08/the-second-circle/){rel="bookmark"} {#the-second-circle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 8, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/08/the-second-circle/ "6:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had sought she who dwells in the midst of the kiri chakra. Our आचार्य's आचार्य the one learned in the great atharvan lore with all its परिशिष्ठ-s, kalpa-s ब्राह्मण and उपनिषद्स् retired to live in the most picturesque land south of लाट pradesha, with forests largely inhabited by niShAda tribes. There on the banks of the Damanganga river he settled down with the forest providing him the quietude and ओषधी-s for his rites. We decided after the great battle of तीक्ष्ण jvara to proceed to the sylvan slopes of Damanganga to spend time in tapasya and acquisition of the विद्यास् of the goat-headed one and she who dwells in the midst of the kiri chakra. We got into the train that bore us on the long journey and then we used a ratha to reach the silent Ashrama on the banks of the Damanganga to go with our acts. The setting was most splendid. We found that path through which षष्ठिनाथ and his wife आर्यमेखला had traversed the regions infested with fierce किराटस् and bhilla-s to reach the shores skirting the सहयाद्री's. There we went to the ancient कुमार गृह, we as the sun was reaching the western peg. The pleasant breezes were blowing from the west and we sat by the chaitya. The amaraugha of his includes rudra, himavat's pretty daughter, agni and गङ्ग. The मानवौघ began with विश्वामित्र and haimavatI, passing through, ब्रह्मास्कन्द, through षष्ठिनाथ and आर्यमेखला. From him to संभद्र and दिव्यमेखला. Then the मानवौघ split into the ogha of the adhoretas who moved along the paschima मयूख and पूर्णगिरि पीठ and ogha of the Urdhvaretas that dwelt in प्राच्य मयूख and then moved to कुमार tapovana. We joined the adhoretasas, being of this world.

As we were seated there we saw the amaraugha before our eyes- it was like an immense flash of उल्लास. The amaraugha entered our mind. We then beheld the immense chaturashra: The great mantras stirred the रसार्णव like the deva-s and the daityas stirring the ocean with मन्धार:\
[ॐ नमो भगवते रुद्राय स्कन्दाय हुं फट् ।\
ॐ नमो हैम्वत्यै विशाखाय हुं फट् ।\
ॐ रं नमो जातवेदसे शाखाय हुं फट् ।\
ॐ नमो जाह्नव्यै नेजमेशाय हुं फट् ।\
ॐ नमो नन्दिकेश्वराय ।]{style="font-weight:bold;font-style:italic;color:#ff6600;"}

Then the deva chakra emerged by the grace of the amaraugha. It was the supreme mantra chakra endowed with 24 spokes as it turned.

[ॐ नमो ब्रह्मणे नन्दिसेन लोहिताक्ष घण्टकर्ण कुमुदमाली सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो भगवते रुद्राय शत-महेन्द्रजाल धृक्-सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो छित्राय छित्रगुप्तायोन्मथ-प्रमथ सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो महद्द्युतिकराय सुभ्राज-भास्वर सहिताय कुमाराय स्वाहा नमः ।\
ॐ नम ओषधिपतये मणि-सुमणि सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो वैश्वानराय ज्वाल-जिह्व-ज्योति सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो ऽम्शायादित्याय परिघ-वात-भीम-दहति-दहन सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो महेन्द्राय वज्रहस्तायोत्क्रोश-पञ्चक सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो विष्णवे चक्र-विक्रम-सन्क्रम सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो ऽश्विभ्यां वर्धन-नन्दन सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो धात्रे कुन्द-कुसुम-कुमुद, डम्बराडम्बर सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो त्वष्ट्रे चक्रानुचक्र सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो मित्राय सुव्रत-सत्यसन्ध सहिताय कुमाराय स्वाहा नमः ।,\
ॐ नमो विधात्रे सुप्रभ-शुभकर्मा सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमः पूष्णे पणित्रक-कलिक सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो वायवे बलातिबल सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो वरुणाय घसातिघस तिमिङ्गल-मुखौ सहिताय कुमाराय स्वाहा नमः ॥\
ॐ नमः पर्वताय सुवर्छातिवर्छा सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो स्खंभाय काञ्चन-मेघमाली सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमो मनवे स्थिरातिस्थिर सहिताय कुमाराय स्वधा नमः ।\
ॐ नमो विन्ध्यायोश्चित्राग्निश्रिङ्गाद्रि-ध्रितौ सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमस् समुद्राय संग्रह-विग्रह गदा-धृतौ सहिताय कुमाराय स्वाहा नमः ।\
ओम् नमो भगवत्या उमायै पुष्पदन्त-शङ्कुकर्ण सहिताय कुमाराय स्वाहा नमः ।\
ॐ नमः काल-महोरगाय जय-महाजय नागौ सहिताय कुमाराय स्वाहा नमः ।\
]{style="font-weight:bold;font-style:italic;color:#99ff99;"}

Then we saw the देवी chakra of the great goddesses led by the most splendid चतुश्पथनिकेता, केतकी, बृहदम्बालिका, the dolphin-faced शिशुमारमुखी, लोहमेखला and the terrifying ghostly goddess वेतालजननी. Then we saw the bitch-headed सरमा and her companions including लोहितायनी.

The word of the amaraugha flashed through us. We saw in urdhva-त्रिकोण:

[ॐ ह्रां ह्रीं ह्रूं ह्रैं ह्रौं ह्रः शरवनभवाय षण्मुखाय शक्तिधराय शिखिवाहनाय कुमाराय रक्तवर्णाय कुक्कुटध्वजाय ॐ फाट् ॥\
ॐ षं षण्मुखाय वज्र-शक्ति-चापधराय सर्व दुष्टान् प्रहरय प्रहरय हुं फट् स्वाहा ॥\
ॐ क्रौञ्चगिरि-प्रहरणाय महाप्रलय-कालाग्निरुद्र पुत्राय कं क्षं कं फट् ॥]{style="font-weight:bold;font-style:italic;color:#ff6666;"}

We went atop the hill reciting the mantra-s of भद्रशाख: We experienced the whole spectrum and beyond. It was high truth realized through the विद्या of the गौहस्फान्द. Then only mantra that came naturally was the षडक्षरि. At one moment we saw without any effort all the immense pleasures of the संगम of the तिक्ष्ण-tripatha स्त्री-चूडामणि and\
the prathama-sundarI of the divyaugha. It was all within us- the second circle had been completed. Then we were burnt by the fires which were the torments of raurava and the hardships of tapasya. We enjoyed the highest foods and then we were performing tapasya eating grass and leaves. We saw the rahasya bindu in the middle-- the glimpse of the महाविद्या, the प्रियं धामन् of the deva-s.


