
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [varmashiva](https://manasataramgini.wordpress.com/2006/06/22/varmashiva/){rel="bookmark"} {#varmashiva .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/22/varmashiva/ "4:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

["ततो देवा रुद्रं नापश्यंस् ते देवा रुद्रं ध्यायन्ति ततो देवा ऊर्ध्वबाहवः स्तुन्वन्ति]{style="font-style:italic;color:rgb(255,0,0);font-weight:bold;"}[[[[\
]{style="font-weight:bold;"}]{style="font-style:italic;"}]{style="font-weight:normal;"}]{style="font-family:Arial Unicode MS;font-size:85%;"}[ब्रह्मैकं त्वं द्वि-त्रिध्-ओर्ध्वम् अधश् च त्वं शान्तिश् च त्वं पुष्टिश् च त्वं तुष्टिश् च त्वं हुतम् अहुतं विश्वम् अविश्वं दत्तम् अदत्तं कृतं अकृतं परं अपरं परायणं चेति]{style="color:rgb(255,0,0);font-weight:bold;font-style:italic;"}[[ ]{style="font-weight:normal;"}]{style="font-family:Arial Unicode MS;font-size:85%;"}["]{style="font-style:italic;color:rgb(255,0,0);font-weight:bold;"}

The circumstances and the details of the migration of my ancestors from the southern country to the northern zone, where they stayed for almost 4-5 centuries is poorly known. But inscriptional records and tantric ogha-valli-s record one member of my clan from the period just after the initial migration. While not much is known of others, this member of my clan, nephew to my direct male ancestor, attained considerable fame in a particular sense. The vaidiki "स्मार्त-s" were always the source of intellectual capital for diverse sectarian matas through their defections to those paths. We have जयराशी, the brahmin exponent of the चार्वाक-mata, others like वासुबन्धु of the आघमर्षण kaushika gotra from पुष्पपुर (now Peshawar in the Moslem Terrorist State) benefitted the bauddha mata, and रामानुज expanded श्री-vaiShNavism. This clansman of mine, varmashiva, was likewise a case of brain-drain, but into the shaiva-mata. While I generally tend to identify myself with संकर्षण in jayanta's play, Agama-Dambara, I still have my sympathies for varmashiva for certain reasons. Inscriptions records associated with the वोदामयूता temple of rudra clearly mention his pravara as being:\
[प्र्]{.f4 style="font-style:italic;color:rgb(255,102,102);"}[[अवर पङ्चकान्वितो वत्स-भार्गव सुगोत्र-मण्डितः। भार्गव- च्यावनक- आप्नवानकैर्- आउर्व्वनाम-जमदग्निभिर् युतः ॥]{style="color:rgb(255,102,102);"}\
]{style="font-style:italic;"}His father, my ancestor's brother was वसावण hailing from the village of सिंहपल्ली. Born in the north in the region of पञ्च-गौड he was in Gujarat in very turbulent times after the Ghaznavid storm. He learnt the atharva-veda at अणहिलपाटक, the great पाशुपत center. He also studied the वैशेषिक and nyAya of which he remained a great advocate till the end and wrote a treatise building on the पदार्थ-dharma-संग्रह. He performed the पाशुपत vrata as per the अथर्वण आदेशः and became inclined towards the shaiva mata. He then proceeded to the drAviDa desha in दक्षिण patha where he encountered bauddhas and is said to have blasted them in fiery polemics. It was there that he completed the study of various shaiva tantras of the bhairava, भूत, garuDa, वाम and siddhAnta srotas and became firmly established in the siddhAnta stream. varmashiva became a great proponent of the siddhAnta tantras, developing a school parallel to that of king bhoja-deva of the परमार-s, with a nyAya-वैशेइषिक orientation. He then journeyed to Kashmir, where debated with towering scholars of the लाकुलीश कालामुख, trika and श्रीविद्या schools and his scholarly and independent philosophical abilties was much respected by his rivals. He was honored by आनन्दराज the lohara king of Kashmir. He also was widely respected for his tAntric abilities in the ख़्षुद्र विद्या-s acquiring the यक्षिणि, चूर्ण, chetaka, डामरिका and डावी prayoga-s (latter for invoking terrifying agents that can cause a variety of rogas in course of उच्छाटन prayoga-s). varma then settled in the temple of वोदामयूता, where he was appointed as the head of a shaiva मठ. There he acted in un-orthodox ways. He took as his primary student, a brilliant शूद्र named मूर्तिगण. मूर्तिगण was initiated into all the tantric rahasya-s by varmashiva and became a great tantric himself. He was made the preceptor of the Rathod king शूरपाल, despite being शूद्र on account of his learning. He succeeded वर्ंअशिव as the lord of the मठ. At that time a नम्बूथिरि from the chera country, इशानशिव, journeyed to वोदमयूता and impressed by मूर्तिगण became his student. He then succeeded मूर्तिगण and became an illustrious shaiva tantric himself.[\
]{style="font-style:italic;"}


