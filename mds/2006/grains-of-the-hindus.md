
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Grains of the Hindus](https://manasataramgini.wordpress.com/2006/04/19/grains-of-the-hindus/){rel="bookmark"} {#grains-of-the-hindus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/19/grains-of-the-hindus/ "6:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Driven by the curse of खाण्डव we were wandering. We decided to drown our sorrows in an early dinner at the dimly lit haunt of yore. We spoke about the traditional grains of the Hindus-- we of course debated the issue of which of these were of Indo-Aryan origin and which were non-Aryan. We did of course finally get into in the issue of language X of Masica. A new linguistic concept came to us, but I shall not go into any of that and only record the list of grains we drew up lest we forget it in the future when we revisit these matters:

वृहि: rice. It is also known by other names like शालि and टण्डुल.

yava: Indologists translate this as barley. But in modern Indian languages we have jowar which is a derivative of yava. It is a proto-Indo-European word meaning grain, so it might have meant grain or even jowar that was traditionally cultivated in India from the earliest agricultural phase.

गोधूम: Wheat. It's cognates are seen in many Asian languages unrelated to IE, e.g Dravidian and Semitic. It is a much travelled word though the exact source of it is unclear.

मसूर: The Masur lentil. Spread from India to Africa but its linguistic origins in India are mysterious.

mudga: The green gram or Mung bean.

mAsha: The black gram or the urad dal.

tila: Sesame. The main oil seed of the Hindus, occurs in black, white and red colors, grows extensively in India.

चणक: The chick-pea, may have been used as food for horses.

अणव: Ragi millet (It may mean other millets in local customs throughout India)

प्रियङ्गु: Foxtail Millet

kodrava: Kodo Millet

मयुष्ठक: a kidney bean or black-eyed bean= [Vigna acontifolia]{style="font-style:italic;"}.

आढकी/ तुवरी : tuvar dal

कलाय: pea

kulattha: horsegram

भञ्ग: hemp

कृष्णचनक

valla

क्शुमा

लट्व

बरठी

गवीधुक

siddhArtha

धान्यक

श्यामाक


