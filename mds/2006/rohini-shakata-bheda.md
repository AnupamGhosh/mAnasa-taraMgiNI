
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [रोहिणी शकट bheda](https://manasataramgini.wordpress.com/2006/09/04/rohini-shakata-bheda/){rel="bookmark"} {#रहण-शकट-bheda .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 4, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/04/rohini-shakata-bheda/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The [रोहिणी शकट](https://manasataramgini.wordpress.com/2003/08/17/rohinis-cart/) bheda is a peculiar conjunction mentioned repeatedly in Hindu astronomy. It is technically defined in the sUrya-siddhAnta (8.13) thus:\
[वृषे सप्तदशे भागे यस्य याम्यो ऽंशकद्वयात् ।]{style="font-weight:bold;font-style:italic;color:#ff9900;"}\
[विक्षेपो ऽभ्यधिको भिन्द्याद् रोहिण्याः शकटं तु सः ॥]{style="font-weight:bold;font-style:italic;color:#ff9900;"}\
In the constellation of वृष (Taurus), at 17th degree, a planet of which the latitude is a little more than 2 degrees, south, will split the cart of रोहिणी.

In his बृहत् saMhitA, वराहमिहिर mentions that the planets that are considered for this conjunction are arka-nandana (the son of the sun, the inauspicious shanaishchara- Saturn), rudhira (the red Mars) or शिखी (a tufted one or a comet). varahamihira himself cites the Atharvavedic astronomer वृद्ध garga (author of the atharvanic नक्षत्र hymns, although I was unable to locate a direct reference to the event in the surviving सूक्त-s and nivid-s of garga from the atharvan saMhitA as well as नक्षत्र kalpa) as the authority who described this type of conjunction. The शकटभेद is believed by the Hindus to be a harbinger of immense destruction or the flooding of the world by the ocean. The medieval Maharatta astronomer गणेश दैवज्ञ correctly noted that the शकटभेद by the Moon was relatively common but that by Saturn or Mars was not possible in the current yuga and might possibly occur/ed in some very distant yuga.

The शकटभेद by Mars or Saturn is a rather remarkable conjunction, which would, based on the sUrya siddhAnta definition, essentially place these planets in between the triangle formed by alpha, epsilon and gamma Tauri. Most western scholars have believed that this is a typical fanciful Hindu exaggeration that was never observed actually in the sky. However, Hindu astronomers never claimed to have observed it themselves, and गणेश दैवज्ञ even clearly states that it is not possible in the current yuga and may be something of a distant yuga. The pioneer of Hindu astro-chronology, SB Dixit in the late 1800s began an investigation astronomical references of the Hindus with the objective of dating them. With the then available ephimerides of the Maharatta astronomers he calculated a शकटभेद by Saturn in 5371 BC, but with modern planetary data this was shown to be unreal.

More recently Indian astronomer Vahia and his coworkers have used modern ephimerides and the commercial program Skymap to show that शक्टभेद by Saturn is indeed unlikely to have really happened since 10,000 BCE. However, they provide evidence that शकटभेद by Mars did happen 4 times between 10,000 BCE and now. The dates they arrive at are: 5284 BCE (7287 BP), 9339 BCE (11,342 BP), 9371 BCE (11,374 BP) and 9860 BCE. Based on this they conclude that शकटभेद was based on genuine observations. They argue that given that it was known to Hindu astronomers that शकटभेद-s by the two planets were not at all observed in their yuga and their correlation with global catastrophes was of little general astrological value, it must be a real tradition. Vahia et al go ahead to make an even bolder claim that these dates for शकटभेद by Mars generally overlap with periods to flooding caused by rises in sea level as can be judged from recently available sea-level records. These sea-level changes occured over relatively prolonged periods. So it is not clear if there were sharp correlations. But it is not impossible that some ancient traditions formed around particular conjunctions. If this claim were true then the शकटभेद will be one of the oldest recorded events surviving in Hindu tradition. It is already known that Hindu tradition preserves memory of fairly ancient conjunctions like the one observed at the start of the traditional kali yuga. The planetary conjunction of 3102 BCE in the region of the constellation of Pisces can be verified with modern ephimerides and there is not much doubt that this was observed rather than back-calculated, because it was in fact not a particularly close conjunction in any case.

Thus, the contention of von Dechend and de Santillana regarding preservation of astronomical knowledge in the language of myth over great stretches of time seems to be supported by Hindu astronomical observations.


