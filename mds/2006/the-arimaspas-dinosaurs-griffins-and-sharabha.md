
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Arimaspas, dinosaurs, griffins and sharabha.](https://manasataramgini.wordpress.com/2006/05/20/the-arimaspas-dinosaurs-griffins-and-sharabha/){rel="bookmark"} {#the-arimaspas-dinosaurs-griffins-and-sharabha. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 20, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/20/the-arimaspas-dinosaurs-griffins-and-sharabha/ "6:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A persistant Indo-Iranian motif is that of the griffin, occuring in India as the sharabha. The sharabha may be described as an imaginary animal or as an incarnation of rudra in that form. The famous शर्भास्त्र weapon and the शरभास्त्र mantra invoke this form of rudra. The form of विष्णु known as the गण्डभेरुन्द नृसिंह is also inspired by the same beast. The greek chroniclers recorded the griffins as being associated with an Iranian tribe on the steppe known as the Arimaspas. They were described as great horsemen, as their name suggests and are supposed to have fought the griffins. The griffins are also described as fighting the Amazons. The griffins are supposed to have guarded gold in central Asia from the Arimaspas. The Pazyryk graves which have been studied over the past century in detail have gold articles, griffin depictions and contain the bodies of female steppe warriors. This suggests that the tales of the Arimaspa and the Amazons are confused Greek accounts for Indo-Iranian tribes of the far east, from whose midst the reports of the griffins first started emerging.

A Greek account tells us that a certain Aristeas was possessed by Apollo and as result had a flight to the land of the Arimaspas, beyond which he declared was the land of the griffins. As per his account, and that of Pausanias, the Sakas (Scythians) were to the North East of Asia, followed by the Issedones, followed by the Arimaspas, beyond whom was the land of the griffins, and beyond them were the Hyperboreans. This suggests that the griffins were located per this account in the regions of Central Asia closer to Mongolia. The Greek accounts also mention that in the temple of Artemis in Alpheios in Elis, she was depicted as riding a griffin. Philostratus declared that griffins were actually from India, and that the Indian Sage Larkas had declared that they were worshipped in India. In the budhnahis of the Iranians too the griffin is mentioned as the largest of the birds.

Many, many years ago R and I were discussing the dinosaurian gods of Hindus, when R came up with the idea that sharabha, the form of rudra is the primary dinosaurian deity. She proposed the idea that the sharabha and the griffin have a common origin inspired on dinosaurian skeletons from Central Asia and India. In support of it she pointed that the sharabha लिन्ङ्ग-s worshipped in her regions were dinosaurian eggs obtained from the Narmada region and pointed a bunch of paintings of sharabha from temples and prints in her region where he is depicted in an unmistakeably dinosaurian form. A couple of years later we noticed that an author A. Mayor had reached a similar conclusion on the possibility of griffins being inspired by the nests of [Protoceratops ]{style="font-style:italic;"}in central Asia which matched the location of the griffins in the Greek accounts. We also concluded that the forms of rudra, ahirbudhnya (an independent deity associated with rudra and the marut-s in the veda) and tumburu are also dinosaurian deities.

What really struck me as remarkable about this fossil connection of the griffins and sharabha is that the ancients, especially Hindus, accurately reconstructed a bird-like animal from the skeletons and saw it as either a bird or a close relation thereoff. However, the modern scientists, despite the pioneering work of Thomas Huxley, failed to see this obvious point, till the late John Ostrom showed through his studies on [Deinonychus ]{style="font-style:italic;"}that birds are dinosaurs.

[This is one the prints from R's regions showing a sharabha in a pretty dinosaurian form]{style="font-style:italic;color:rgb(51,255,51);"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/sharabha.jpg){width="75%"}
```{=latex}
\end{center}
```




