
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The महाप्रलय](https://manasataramgini.wordpress.com/2006/05/14/the-mahapralaya/){rel="bookmark"} {#the-महपरलय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 14, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/14/the-mahapralaya/ "4:33 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/Merapi.0.jpg){width="75%"}
```{=latex}
\end{center}
```



The Hindu kingdom of Mataram in Yavadvipa, associated with the famous tantra of the वज्रमण्डल, came to an end in 1006. It was take to its peak by the राजन् named: मकुट-vamsha-vardhana इशान kula-sUrya. Due to the recent revival of the volcano that ended Mataram, I was reminded of this and also the need to look more deeply into this tantra (some of whose mantras were recovered on golden plates inscribed in a ruined Indonesian temple) when the chance arises . The inscription on the Calcutta stone states that in 928 shaka (1006 CE) there was a tremendous महाप्रलय and a strike of fire which ended the kingdom of Mataram. The whole of the यवद्वीप is said to have become a क्षिर सागर. It was described as corresponding to the end of the kalpa described in the Hindu texts. The क्षिर सागर is most likely to be a description of the [tephra (volcanic ash)](http://volcanoes.usgs.gov/Products/Pglossary/tephra.html) that was spewed all over by the volcanic eruption of Merapi. In a recent conversation with the two JSs I learnt that a tephral layer has been recovered in Indonesia with the charcoal from it C14 dated to between 900-1100 CE. This must definitely mark the क्षिर-सागर created by महाप्रलय of Merapi in 1006 CE that ended the Hindu kingdom of Central Java.


