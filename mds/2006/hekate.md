
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Hekate](https://manasataramgini.wordpress.com/2006/08/16/hekate/){rel="bookmark"} {#hekate .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/16/hekate/ "4:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/hekate.jpg){width="75%"}
```{=latex}
\end{center}
```



Another point I shall consider is the role of the great goddess Hekate in Neoplatonic thought. Hekate in Neoplatonism plays a strikingly parallel role to mAyA in उत्तरमीमांस and प्रकृति in सांख्य. She is described in the Chaldaean oracles to have emerged from the "primary entity/being" (the Neoplatonic equivalent of puruSha or brahman) and separates the two "fires"- the "purely intellectual fire" that belongs to the primary entity/being and the material fire from which the cosmos arises. She separated the two fires in the form of a membrane (again parallel to the mAyA of उत्तरमीमांस). The universe is seen as her emanation. In iconography she is seen as three-headed and the mistress of the 3 paths and may be compared to the goddess त्रिपुरा of श्रीविद्या. An old orphic hymn to her is of considerable interest (the original Greek text is provided along with an adaptation of the translation by Adam Forrest; we note a few words in the Greek text for comparison with संस्कृतं)

Einodian Hekatên, klêizô, Trihoditin Erannên,\
Hekatê of the Path, I praise Thee, Lovely Lady of the three paths,\
\[Note the verb Klêizô; it is the cognate of the Sanskrit root shravas, which is used for loud praise in the सूक्त-s of the ऋग्वेद]\
Ouranian, Chthonian, te kai Einalian, Krokopeplos.\
Celestial, Chthonian, and Marine One, Lady of the Saffron Robe.\
\[Ouranian compare with वरुण as the lord of the sky]

Tymbidian, Psychais Nekyôn meta bakcheuosan,\
Sepulchral One, celebrating the Bakchic Mysteries among the ghosts of the Dead,

Perseian, Philerêmon, agallomenên elaphoisi.\
Daughter of Persês, Lover of Solitude, rejoicing in deer.\
\[elaphoisi: deer]

Nykterian, Skylakitin, amaimaketon Basileian.\
Nocturnal One, Lady of the Dogs, invincible Queen.\
\[Nykterian: Night, the cognate of Sanskrit nakta; both epithets of being with deer and dogs are parallel to rudra being described similarly]

Thêrobromon, Azôston, aprosmachon Eidos echousan.\
She of the roar of the Beast, Unfettered One, having an irresistible Form.\
\[apros-machon: compare with aprati]

Tauropolon, Pantos Kosmou Klêidouchon, Anassan,\
Bullherder, protector of the Keys of All the Universe, Mistress,\
\[Tauro-polon: polon is cognate of Sanskrit pAla as in गोपाल= cow herder]

Hêgemonên, Nymphên, Kourotrophon, Ouresiphoitin.\
Guide, Bride, Nurturer of Youths, Mountain Wanderer.

Lissomenos, Kourên, teletais hosiaisi pareinai,\
I pray Thee, Maiden, to be present at our hallowed rites of initiation,\
\[Kourên- supposed to be a identification with the awful Persephone]

Boukolôi eumeneousan aei kecharêoti thymôi.\
Always bestowing Thy graciousness upon the Boukolos.


