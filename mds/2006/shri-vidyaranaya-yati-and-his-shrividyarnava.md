
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [श्री विद्यारणय yati and his श्रीविद्यार्णव](https://manasataramgini.wordpress.com/2006/03/26/shri-vidyaranaya-yati-and-his-shrividyarnava/){rel="bookmark"} {#शर-वदयरणय-yati-and-his-शरवदयरणव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/26/shri-vidyaranaya-yati-and-his-shrividyarnava/ "11:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The श्रीविद्यार्णव is an excellent tAntric manual that was used as textbook for mantra शास्त्र instruction amongst certain brahminical schools. It contains 2 halves called पूर्व and uttara ardha-s of 18 chapters called श्वासस् each. It runs into 1459 devanagari print pages to give an idea of its size. It is a digest for a wide range of material from ancient tantras that appear to be lost. One of these is the कुक्कुटेश्वर tantra, whose prayogas are still remembered by several practioners even if only in fragments. The other archaic tantras cited by it include : 1) कुलार्णव 2) वामकेश्वर 3) योगिनी हृद्य 4) tantra-rAja 5) फेट्कारिणि 6) the यामलस् 7) बृहद्नीला 8) siddha-सरस्वती 9) पिङ्गल mata 10) पाञ्च्रात्र संहितास् 11) कुलप्रकाश 12) दक्शिणामुर्ति saMhitA 13) bhairava tantras 14) कालिमत. Interestingly it also cites digests of vaiShNava tantric material outside of the classical पाञ्चरात्र. These include 1) shrikrama नारायणीय and 2) बृहत् नारायणीय. It also cites secondary tantric encyclopedia such as the shAradA-तिलकं. In its general form the श्रिविद्यार्णव is a relatively late digest coming at the end of a long line of such texts including प्रपञ्चसार, shAradA तिलकं, mantramahodadhi, बृहत्-तन्त्रसार, मन्त्रार्णव and तन्त्रसारसंग्रह.

It is attributed to श्री विद्यारणय yati, a sannyAsi. We had a discussion on this text and the author. A few उपासकस् from South India believe that this is the same as the guru of harihara and bukka the revivers of the Hindu struggle against the army of Islam. However, R believes that he was a northerner from the Himalayas. Another southern स्मार्त settled in वारानसि also believes that he was seen near प्रयाग् and वाराण्सि where he initiated many into the fold. I suspect we know very little about this विद्यारणय and it is quite possible that he was indeed distinct from the South Indian विद्यारण्य. There are several identical sections between mantra mahodadhi and श्रीविद्यार्णव, tenuously supporting an origin in the वारानसि region, although we cannot be sure if they were necessarily close in time. Of course I ignore the Indological consensus on this matter, for it hot air and conceit passing as scholarship.

The argument by the northerners regarding his origins go thus: He himself records in the section on guru-shishya choice that he was a student of प्रगल्भाचर्य himself the student of the great brAhmin विष्णुशर्मन्. Thus we have:\
[विष्णुशर्मन्->प्रगल्भाचार्य->विद्यारण्य यति]{style="font-weight:bold;color:#ff0000;"}\
Clearly, this is not the nyAya savant प्रगल्भाचार्य of the later nyAya tradition in Navadvipa. Further, R informs me that in Himachal Pradesh and Kashmir tradition has it that विद्यारण्य yati completed his work in 1070 AD after the death of the great tantric क्षेमराज of the trika school. विद्यारण्य yatI's extensive knowledge of vaiShNava material of an archaic type is believed to connect him to the Himalayan tantric वैष्णवस् whose famed आचार्य was श्री महाबल paNdita from श्रीनगर in Kashmir with followers in both Jambupura (Jammu) and Kashmir. One lineage from him was:\
[महाबल->आचार्य पाण्दित->श्री कृष्ण पण्डित-> लक्ष्मण देशिकेन्द्र]{style="font-weight:bold;color:#ff0000;"}(the illustrious tantric who compiled the prayogas of the shAradA तिलकं).\
As an aside in another direction लक्ष्मण deshikendra was also seen as the successor for the syncretic trika-kaula schools of tantricism:\
[वसुगुप्त (ओफ़् शिव सूत्र फ़मे)->सोमानन्द-> उत्पलदेव -> (रामकण्ठ?)-> लक्ष्मण देशिकेन्द्र->अभिनवगुप्त (०फ़् तन्त्रालोक फ़मे)।]{style="font-weight:bold;color:#ff0000;"}

Some dispute this lineage as being a created by राघवभट्ट the commentator on deshikendra's works. deshikendra put together the prayogas of the shArada तिलकं by 1010 AD ( Note that Kashmir had scored a spectacular victory against Mahmud Ghaznavi under the rajan संग्राम deva at that time and was safe from Mohammedan pestilence for a while). विद्यारण्य yati cites the work of लक्ष्मण deshikendra extensively, so he must be after him. Hence, it is believed that there was a distinct lineage from deshikendra that included विष्णु sharman and lead to विद्यारण्य yati. At least this is not inconsistent with the traditional date for the श्रीविद्यार्णव amongst the northerners. Its emphasis on Kashi is another point to note- it is believed that this may have a link to utpaladeva's ancestors hailing from Kashi in the days of rajan ललितादित्य.

The muni provided me with a South Indian story for the श्रीविद्यार्णव: विद्यारण्य was meditating in central India (near a बगलामुखि sthala) when a ब्रह्मराक्शस attacked him. He pacified it with food and water and it lead him to sage कृश्ण द्वैपायन who was making an unseen journey to the city of कालभैरव. विद्यारणय apparently asked व्यास for the tantra and was revealed the श्रीविद्यार्णव. Another more earthy explanation by the southerners is that विद्यारण्य went to Kashi to learn from the stated प्रगल्भाचर्य. We find this a little strange, given that the Matha at Shringeri is believed to have anyhow traditionally followed the प्रपञ्चसार, the tantric manual edited by shankara भगवत्पाद. So he would have anyhow been initiated by his local teachers into this rather than go to Kashi for his tantric lessons. We find no evidence for a प्रगल्भाचार्य. But it is pointed out that it is not uncommon to have two tantric teachers the same time.

What ever the case- the evidence is somewhat in favor of the northern origins of this विद्यारण्य yati, though I am not sure that a directional connection with लक्ष्मण deshikendra can be made without further investigation. What ever the tradition which is accepted it is the mantra mahodadhi that borrows from श्रीविद्यार्णव and not the other way around.


