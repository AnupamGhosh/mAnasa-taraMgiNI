
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Purussaurus and crocodylian recursiveness](https://manasataramgini.wordpress.com/2006/01/29/purussaurus-and-crocodylian-recursiveness/){rel="bookmark"} {#purussaurus-and-crocodylian-recursiveness .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 29, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/29/purussaurus-and-crocodylian-recursiveness/ "7:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The 5 foot skull of [Purussaurus ]{style="font-style:italic;"}the giant nettosuchid caiman of the Miocene epoch from the Amazon system. One of the largest ever crocodiles

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/hello/133/1300/400/purussaurus.jpg){width="75%"}
```{=latex}
\end{center}
```



Studies on the snout morphology of crocodylians points to 5 recurrent skull types repeatedly evolving their midst: 1) The generalized skull type as seen in [Crocodylus niloticus]{style="font-style:italic;"} the Nile crocodile. 2) Blunt-nosed skull as seen in [Osteolamus tetrapsis]{style="font-style:italic;"} the African dwarf crocodile. 3) The long-slender snouted crocodile as seen in gharial and [Tomistoma ]{style="font-style:italic;"}the false gharial. 4) The deep skull with laterally compressed knife-like serrated teeth (ziphodont) as seen in the pristichampsines. 5) The duck-faced crocodiles, like [Purussaurus ]{style="font-style:italic;"}and [Mourasuchus, ]{style="font-style:italic;"}which have broad long duck-bill like snout and eyes placed in posterior end of the long skull. These skulls may be deep as in [Purussaurus ]{style="font-style:italic;"}or very flat as in [Mourasuchus]{style="font-style:italic;"}.


