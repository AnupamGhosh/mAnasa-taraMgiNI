
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [श्रीविद्या beyond the पञ्चदशी and classical phase](https://manasataramgini.wordpress.com/2006/07/23/shrividya-beyond-the-panchadashi-and-classical-phase/){rel="bookmark"} {#शरवदय-beyond-the-पञचदश-and-classical-phase .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 23, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/23/shrividya-beyond-the-panchadashi-and-classical-phase/ "6:37 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Not much is remembered of the श्री-kula traditions before the universal sweep by the पञ्चदशी and structurally related mantras, like manu विद्या or skanda विद्या. These may be termed the classical श्रीविद्या mantras and examples include:\
The manmatha विद्या or कादि विद्या:\
ka e I la ह्रीं | ha sa ka ha la ह्रीं | sa ka la ह्रीं\
This celebrated mantra is seen even in tiru-मन्तिरं of सुन्दरनाथ (तिरुमूलर्) the celebrated siddha who migrated from Kashmir to the drAviDa desha.\
The लोपामुद्र विद्या or the हादि विद्या:\
ha sa ka la ह्रीं | ha sa ka ha la ह्रीं | sa ka la ह्रीं\
The skanda विद्या (with 18 syllables):\
ha sa ka la ह्रीं | ha sa ka sa ka la ह्रीं | sa ha ka ha ka ह्रीं |\
The manu विद्या (with 18 syllables):\
ka ha e I la ह्रीं | ha ka e I la ह्रीं | sa ka e I la ह्रीं\
Now even within the classic mantras mainly the कादिमत and to a smaller extant the हादिमत dominate.

However, there are two independent mantras of श्रीकुल that appear to have existed as parallel श्रीविद्या traditions before the पञ्चदशी-like formulae became universal. Only fragments of these texts now survive giving the minimal sketch of mantraic deployment. One of them appears to parallel the archaic कुब्जिका tradition, where the mantras are combined with five vaktra mantras for shiva. This is also called the नित्यक्लिन्ना विद्या. The second appears to be a stand alone form of त्रिपुरसुन्दरी. While in both these versions the 8 bhairavas are present, not all of the 16 नित्या-s are invoked (at least in the surviving fragments). वाराही (दन्डनथा) is however worshipped as in the pa\~chadashi based forms.

It appears that सुन्दरनाथ (तिरुमूलर्) appears to have been aware of these parallel forms as he mentions a shakti-पीठं त्रिपुरा chakra, which from the sketchy details seems to be chakra that goes along with the above version.


