
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The younger shatarudriya](https://manasataramgini.wordpress.com/2006/02/21/the-younger-shatarudriya/){rel="bookmark"} {#the-younger-shatarudriya .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/21/the-younger-shatarudriya/ "7:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A gentleman had asked if there might be a substitute for the shatarudriya, that could recited by someone who was not initiated into the path of the veda or eligible to receive the vedic rahasya. Me, the muni and और्वशेयी wondered over what would be such a perfect substitute for the shatrarudriya. Without any doubt we concluded that it should be the hymn that might be termed the 'younger shatarudriya' -one of the oldest shiva hymns of the early post-vedic period. This hymn is remarkable in that it gives a glimpse into one of the earliest layers of the post-atharvashiras shaiva stream and needs a detailed study. We hope to do that in the near future as we get the chance.\
[" कटंकटाय चण्डाय नमः पचपचाय च । नमः सर्ववरिष्ठाय वराय वरदाय च ॥ "]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

Salutations to one who is the rattling wind, the impetous one and the one who cooks all. Salutations to the foremost of all beings, one who is the boon, and the giver of boons.[]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}


