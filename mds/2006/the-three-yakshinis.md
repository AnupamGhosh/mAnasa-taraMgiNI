
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The three यक्षिनिस्](https://manasataramgini.wordpress.com/2006/01/30/the-three-yakshinis/){rel="bookmark"} {#the-three-यकषनस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 30, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/30/the-three-yakshinis/ "7:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After long we spoke to द्रोणजा of fair complexion. We marvelled at how she remained resplendent firing the imagination of admirers as she did 15 years ago, when she was at the height of her glory. She was then like Ardvi Sura Anahita coursing down from the heights of Hukairya to the foaming expanses of Vouru Kasha. jara and roga had not weathered the beauty of और्वशेयी, who could still raise the flutter in many a heart. But we touched wood because we did not want our "evil eye" to be cast on her. और्वशेयी pointed to an old scrap of paper she had preserved from long. She said: "here is the manas that should not be lost. The context may be forgotten but the core substance should always remain. I said: "It recurrs indeed and dominates life. Now the context returns". She said: "It was quite night in the land of the Qutb Shah, when this truth indeed dawned on you. You saw the beginning of the truth." Then she added invoke the 3 यक्षिनिस्. Under the parigha वृक्ष we invoked all three of them. They came with the sweet smiles illuminating the horizon. First was the delicate and elegant valli-यक्षिनि. The second was the imperial रोहिणि-यक्षिनि. The third was the तुरङ्ग-यक्षिनि. We flew in their midst to the dvipa on gola sarvarovara...


