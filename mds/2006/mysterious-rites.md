
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [mysterious rites](https://manasataramgini.wordpress.com/2006/11/20/mysterious-rites/){rel="bookmark"} {#mysterious-rites .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 20, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/20/mysterious-rites/ "7:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The village school master, and master of the mantra-शास्त्र possessed many a mysterious mantra. The most mysterious was a manuscript of a tantric from around 900 AD from Uttaranchal which had in an archaic script a whole collection of mantra-s, some found no where else.\
The पिशाचि prayogas: 1) पिशाची कर्णगृह्या 2) पिशाची औलुम्बिनी 3) पिशाची कृष्णमुखी 4) kharamukhI\
The पिशाच prayogas: 1) पिशाच पिलुपाल 2) कर्ण-पिशाच 3) कृष्ण पिशाच 4) कृष्ण-sAra पिशाच 5) munidhara पिशाच 6) alagupta\
Other prayogas:

 1.  महिषानन prayoga (agents of धूमावती) 2) क्रोधवाराही prayoga 3) छिन्नमस्ता prayoga 4) महामाया 5) गण chakra vidhi 6)कुरुकुल्ला and वाराही मारण prayogas.


