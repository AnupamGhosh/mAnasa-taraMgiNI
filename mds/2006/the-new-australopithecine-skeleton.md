
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The new australopithecine skeleton](https://manasataramgini.wordpress.com/2006/09/23/the-new-australopithecine-skeleton/){rel="bookmark"} {#the-new-australopithecine-skeleton .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 23, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/23/the-new-australopithecine-skeleton/ "4:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/australopithecus_tibia.jpg){width="75%"}
```{=latex}
\end{center}
```



R and I discussed the new find of [Australopithecus afarensis ]{style="font-style:italic;"}by Alemseged et al with some excitement. It is not so common that we get relatively complete remains of the australopithecines. This particular one from the Hadar formation is the fossil of a juvenile female specimen that is believed to have been 3 yrs at death based on a chimpanzee model. The studies are still preliminary and I am sure this find will advance the understanding of the apes of the human lineage considerably in the coming years. Yet a few features are striking: 1) The shape of the scapula is very gorilla-like, and strikingly so in the angle of the scapular spine. 2) The manual phalanges are long and curved. 3) The semi-circular canals are very similar to that of the chimpanzees and gorilla and are not indicative of a decoupled head from trunk as seen in humans. 4) The face is prognathus and quite like the chimpanzee and the brain-size in the same range as the surviving non-human African apes. Yet being a juvenile it provides a new data point in terms of brain size and suggests that the Australopithecus afarensis was probably born with a relatively large brain, but it did not grow rapidly as in humans and finally attained the just the size of non-human apes. 5) The leg is similar to humans, with the shape of the tibia in particular strongly indicative of a bipedal stance. The attachment of the tibialis anterior muscle also seems to closely resemble modern humans.

This melange of characters is peculiar especially given that it combines the semi-circular canals and scapular architecture typical of gorilla and chimpanzee with human-type hind-limbs. The one thing it tells us is that we do not precisely understand the correlation between morphology and function for the gorilla-type scapula -- the gorilla is a predominant knuckle-walker while evidence strongly favors a bipedal gait for[ A.afarensis]{style="font-style:italic;"}. As the authors suggest these morphologies may have more to do with an arboreal involving climbing with the hands being deployed above the heads. Taken together these suggest that this australopithecine was probably a capable tree-climber that principally used its arms to move about in the trees. Thus, it might have lead a life on the ground like modern apes with increased bipedal locomotion relative to them, but might scampered up trees to escape predators.

The hyoid bone which is surprisingly preserved is more like a gorilla or a chimp than that of a human or orangutan. Its morphology suggests that it allowed for large air sacs. Such air sacs are there in both great apes and lesser apes. Amongst the gibbons the larger species but not smaller ones have voluminous air sacs, whereas in the great apes all except humans have prominent air sacs. These air sacs open via thin-walled duct just above the vocal membrane and are also supplied with their own platysma muscle. This appears to be the primitive condition in apes and was probably lost twice in the smaller gibbons and Homo. The air-sacs are probably used to generate a loud vocalization without needing to breathe too much air. In the gorilla it is clear that the chest-thumping is used to release the air from the sacs to produce the requisite sound effect. Thus, its presence in [A.afarensis]{style="font-style:italic;"} suggests that it was lost relatively late in the line leading to humans. It is indeed interesting that we have the most elaborate language amongst the apes while losing the air sacs. So the loss of the air sacs may have indeed been coupled with the origin of a more elaborate vocal communication system in the form of language of [Homo]{style="font-style:italic;"}, which probably reflects a shift away from extended call sequences. This event it is now clear happened only after the [A.afarensis]{style="font-style:italic;"} grade in human evolution.

The lack of enlarged canines in [A.afarensis ]{style="font-style:italic;"}may indicate a lowered physical component in male dominance. In an accompanying note Wood suggests that[ A.afarensis]{style="font-style:italic;"} may have instead used its air sacs to shout down opponents. However, given that other apes with large canines also have comparable air sacs makes this idea rather fanciful.


