
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The herb grids of the कुब्जिका mata](https://manasataramgini.wordpress.com/2006/02/03/the-herb-grids-of-the-kubjika-mata/){rel="bookmark"} {#the-herb-grids-of-the-कबजक-mata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 3, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/03/the-herb-grids-of-the-kubjika-mata/ "5:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/hello/133/1300/400/oShadhi_grid.jpg){width="75%"}
```{=latex}
\end{center}
```



A stream of the कुब्जिका tradition has evolved out of the more archaic atharvanic tradition. One of the most important links between the कुब्जिका mata and the older atharvan tradition is the [कुब्जिका उपनिषद्](https://manasataramgini.wordpress.com/2005/04/23/the-tantric-manual-of-the-atharvan-angiras/ "The tantric manual of the atharvan-a~Ngiras"). कुब्जिका is identified with प्रत्यङ्गिरा or अथर्वण भद्रकाली the spirit of the atharva veda. The कुब्जिका mata also preserves certain other connections to the atharvan tradition in the form of the medico-magical use of herbs. The कुब्जिका traditon deploys two famous herb grids that are reminiscent of the herb collection used in the atharvanic उपाकर्म or दीक्श rite, the herb collection used for consecrating and offering during the अथर्वण महाशान्ति and the most exalted and awful धूमावती rite of the अथर्वण kalpa. The first of these is the 4X4 grid. These within the squares of this grid are placed the following 18 ओषधिस्:\
भृञ्गराज (16), सहदेवी (3), मयूरशिखा (8), पुत्रङ्जीव (2), कृतन्जली (7), अधःपुष्पा (14), रुदन्तिका (11), कुमारी (8), रुद्रजट (10), विष्णुक्रान्त (6), श्वेतार्क (4), लज्जालुका (9), मोहलता (6), कृष्णधत्तूर (12), गोरक्ष(1), कर्कटी (15), मेषशृञ्गी (5) and स्नुही (13).\
They are coded thus:\
1= चन्द्रमाः; 2=पक्षौ; 3=वहनयः; 4=वेदः; 5=bANa; 6=रसाः+ऋतवः; 7=muni/अब्धिः; 8=नागाः+वसवः; 9=ग्रहाः; 10=dik; 11=shiva; 12=ravi/sUrya; 13=tridasha; 14=manu; 15=तिथयः; 16=ऋत्विजः

गोरक्ष(1)=[Adansonia digitata]{style="font-style:italic;"}\
पुत्रङ्जीव (2)=[Putranjiva roxburghii]{style="font-style:italic;"}\
सहदेवी (3)=[Conyza cinerea]{style="font-style:italic;"}\
श्वेतार्क (4)= [Calotropis procera]{style="font-style:italic;"}\
मेषशृञ्गी (5)= [Gymnema sylvestra]{style="font-style:italic;"}\
विष्णुक्रान्त (6)=[Clitoria ternatea, Evolvulus alsinoides]{style="font-style:italic;"} (?)\
मोहलता (6) =[Bergenia ligulata]{style="font-style:italic;"}\
कृतन्जली (7)=[ Mimosa pudica]{style="font-style:italic;"}\
मयूरशिखा (8)= [Adiantum caudatum]{style="font-style:italic;"}\
कुमारी (8)=[Aloe barbadensis/ Aloe vera]{style="font-style:italic;"}\
लज्जालुका (9)= [Biophytum sensitivum]{style="font-style:italic;"}\
रुद्रजट (10)=[Aristolochia indica]{style="font-style:italic;"}\
रुदन्तिका (11)=[Cressa cretica]{style="font-style:italic;"}\
कृष्णधत्तूर (12)=[Datura fastuosa]{style="font-style:italic;"}\
स्नुही (13)= [Euphorbia species (E.neriifolia or E.nivulia)]{style="font-style:italic;"}\
अधःपुष्पा (14)=[Trichodesma indicum]{style="font-style:italic;"}\
कर्कटी (15)=[Trichosanthes anguina]{style="font-style:italic;"}\
भृञ्गराज (16)= [Eclipta alba\
]{style="font-style:italic;"}\
Using this grid and numerical system various prescriptions may be coded by the practioner. Thus, we are told of a prescription thus: [त्रि-दशाक्षेश-भुजगैर् लेपात् स्त्री सूयते सुखं ॥]{style="font-weight:bold;color:#ff6600;"}[\
]{style="font-weight:bold;font-style:italic;color:#ff9900;"}Which means: For easy delivery a woman may use a paste made from tri (3); dasha (10); अक्षि (eyes=2); isha (rudra=11); bhujaga (nAga=8).\
Some applications might have a magical slant: [तिथि-दिग्-युग-बाणैश् च गुटिका तु वशङ्करी । भक्ष्ये भोज्ये तथा पाने दातव्या गुटिका वशे ॥]{style="font-weight:bold;color:#ff6600;"}\
This means: a pill made from tithi (15); dik (10); yuga (ages=4); and bANa (5) can be used for वशिकरण. This pill should be given in eatables, food-stuff and drinks for the purpose of वशिकरण.

[ऋत्विग्-ग्रहाक्षि-शैलैश् च शस्त्र-स्तम्भे मुखे धृता ।]{style="font-weight:bold;color:#ff6600;"}\
For protection against palsy on the face caused by stambhana prayogas one may use ऋत्विक्(16); graha (9); अक्षि (eyes=2); shaila (mountains=7).

The second grid is a larger one of 6X6 squares (know as the grid of brahmA-rudra-indra), with 36 plants: (1) हरीतकी (2) अक्षि (3) धात्री (4) मरीच (5) पिप्पली (6) श्ल्फा (7) vahni (8) षुण्ठि (9) green पिप्पली (10) गुडूची (11) वचा (12) निम्बका (13) वासक (14) शतमूलि (15) saindhava (16) sindhu-वारक (17) कण्टकारी (18) गोक्षुरक (19) bilva (20) पौनर्नवा (21) balA (22) एरण्डमुण्डी (23) ruchaka (24) भृन्गराज (25) क्षर (26) पर्पट (27) धन्याक (28) जीरक (29) षतपुष्पी (30) जवानिका (31) विड्ङ्ग (32) khadira (33) कृतमाल (34) हरिद्रा (35) वचा (36) siddhArtha

It seems वचा is reused twice in the grid. The plants are dried, ground to powder, treated with soma and mixed with jaggery into modaka, or mixed with ghee and some tailas for use. The recommended doses are from .5 karsha to 1 pala by weight.


