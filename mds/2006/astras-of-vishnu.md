
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [astras of विष्णु](https://manasataramgini.wordpress.com/2006/12/17/astras-of-vishnu/){rel="bookmark"} {#astras-of-वषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/17/astras-of-vishnu/ "6:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The powers of the great god, who envelops the universe with his 3 strides, dwelling in the ocean of existence sleeping on the snake of time, watched upon by the epitome of the female kind, are put forth as deadly 105 astras.

The head of them is the chakra known as sudarshana:\
ध्यातं सकृद् भगवानेक कोट्यघौघम् हरत्यरं |\
sudarshanasya तद्दिव्यं bhargo devasya धीमहि ||

The pravartaka astras of विष्णु:

 1.  ब्रह्मास्त्र 2) daNDa chakra 3) कालचक्र 4) dharmachakra 5) विष्णुचक्र 6) indra chakra 7) वज्रास्त्र 8) त्रिशूल 9) brahmashiras 10) ऐषीकास्त्र 11) मोदकी 12) शिखरी 13) dharma-pAsha 14) काल-pAsha 15) वरुण-pAsha 16) Adyo.ashani 17) anyo.ashani 18) पिनाकं 19) नारायणास्त्र 20) पाषुपतास्त्र 21) आग्नेयास्त्र 22) दयितास्त्र 23) वायवास्त्र 24) हयग्रीवास्त्र 25) क्रौञ्चास्त्र 26) प्रथमा shakti 27) द्वितीया shakti 28) कञ्कालास्त्र 29) musala 30) कापालस्त्र 31) कञ्कणी 32) वैद्याधरास्त्र 33) असिरक्तास्त्र 34) गान्धर्वास्त्र 35) प्रस्वापनास्त्र 36) प्रशमनास्त्र 37) सौर्यास्त्र 38) दर्पणास्त्र 39) शोषणास्त्र 40) सन्तापनास्त्र 41) विलापनास्त्र 42) मदनास्त्र 43) कन्दर्पास्त्र 44) कर्दर्पदयितास्त्र 45) पैषाचास्त्र 46) तामसास्त्र 47) सौमनास्त्र 48) सम्वर्तास्त्र 49) मौसलास्त्र 50) सत्यास्त्र 51) मायाधरास्त्र 52) मायाधरास्त्र 53) घोरास्त्र 54) ratyastra 55) अघोरास्त्र 56) सौमास्त्र 57) त्वाSट्रास्त्र 58) आरुणास्त्र 59) भगास्त्र 60) शीतेश्वस्त्र 61) मानवास्त्र 62) raudrAstra\
The nivartaka astras of विष्णु :

 1.  satyavadastra 2) सत्यकीर्त्यस्त्र 3) धृष्टास्त्र 4) रभसास्त्र 5) प्रतिहारतरास्त्र 6) पराङ्मुखास्त्र 7) अवाङ्मुखास्त्र 8) लक्षास्त्र 9) विषमास्त्र 10) दृढनाभास्त्र 11) सम्हारकास्त्र 12) दशाक्षास्त्र 13) दशशीर्श 14) shatodara 15) पद्मनाभास्त्र 16) महानाभास्त्र 17) दुन्दुनाभास्त्र 18) धृतिमाल्यास्त्र 19) रुचिरास्त्र 20) पितृसौमनसास्त्र 21) विधूतास्त्र 22) मकरास्त्र 23) करवीरसमास्त्र 24) धनास्त्र 25) धान्यास्त्र 26) ज्यौतिषास्त्र 27) कृशनास्त्र 28) नैराश्यास्त्र 29) विमलास्त्र 30) योगन्धरास्त्र 31) विनिद्रास्त्र 32) नैद्रास्त्र 33) प्रमथनास्त्र 34) सार्चिमाल्यस्त्र 35) कामरूपास्त्र 36) कामरुच्यस्त्र 37) मोहनास्त्र 38) अवरणास्त्र 39) जृंभकास्त्र 40) सर्वनाभकास्त्र 41) कृशाश्वतनयास्त्र 42) संधानास्त्र 43) वारुणास्त्र

Most of these are discharged from the great bow known as the शार्ङ्ग


