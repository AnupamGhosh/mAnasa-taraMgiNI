
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The shaivas: the पाशुपत-s](https://manasataramgini.wordpress.com/2006/05/10/the-shaivas-the-pashupata-s/){rel="bookmark"} {#the-shaivas-the-पशपत-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 10, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/10/the-shaivas-the-pashupata-s/ "5:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The early evolution of the shaiva fold is a topic of tremendous interest (I am only grazing the surface with regards to a few points here. One may see some other notes on this topic elsewhere on this site). We obviously have no strong traces of its para-vedic elements. But there are several texts that pertain to the branching point of shaiva from within the old vedic tradition. These texts include chiefly the rudra पञ्चवक्त्र mantra-s and the श्वेताश्वतर of the कृष्ण yajurvedic tradition and the atharvashiras and the नीलरुद्र of the atharvavedic tradition. Many principal elements of the श्वेताश्वतर and the नीलरुद्र derive from the vedic rudra सूक्तं-s seen in the ऋग्, yajur and atharvan संहितास्. In the महाभारत we already have sections that are clearly of a generalized shaiva origin and express their clear connection to the vedic tradition including the importance of the shatarudriya hymns borrowed from the classical vedic ritual tradition. In addition, they provide an important hymn resembling the shatarudriya in many ways, which is the अजानन virachita rudra stuti. The पौराणिc, classical and inscriptional evidence one can discern that the shaiva fold had already differentiated into multiple distinct streams.

While the naming system of these streams is varied and sometimes confused the following can be made out:

 1.  पाशुपत (this name is almost an invariant). 2) कापालिक (this stream may be called the saumya or सोमसिद्धान्तिन्-s after their connection with the soma sacrifice. They may in some accounts also be be called महाव्रत-dhara). 3) The लाकुल after their founder लाकुल or लाकुलीश (this name is often misspelt in different sources, and may often be known by their later name कालामुख-s). Some source may also call them महाव्रत-dhara). 4) The tantric shaiva-s. This group originally further diversified to 5 lineages or srotas with their texts being: the भूत, वाम, bhairava, garuDa and siddhAnta tantras. The originally shAkta tantric lineage, the trika tantras or the सिद्धयोगेश्वरी-mata shaivized (most likely in the Tamil country and spread to Kashmir where it underwent an efflorescence) and added to the tantric shaiva stream. The popular bhakti stream of shaiva was closely linked to the लाकुलीश stream and appears to have been a public ofshoot for the general populace supported by the लाकुल आचार्यस्. To a lesser extant it incorporated the पाशुपत-s too. The "Elephanta" caves built by the kalachuri king कृष्ण rAja (around mid 500 AD) and the closely linked finalized shiva पुराण are closely linked to the लाकुलीश shaivas. dakSiNa-केदारेश्वर in Karnataka is another major temple built under the instigation of the later लाकुल-s, the कालामुख-s. The लाकुल-s were a pan-India phenomenon. Seen in many spots in south India down to the Tamil country, in सोमनाथ in Gujarat, Mathura (both major centers) and Kashmir.

The लाकुल-s were clearly related to the पाशुपत-s and may have shared a common ancestral development and then diverged. Their ancestor seems to be a very early shaiva branch with clear links to the vedic texts. The पाशुपत sUtra itself is attributed to the founder of लाकुल-s, लाकुलीश. लाकुलीश's disciple musalendra wrote a work termed हृद्यप्रमाण of which only fragments survive in a later लाकुल commentary. Another successor of लाकुलीश, a कौण्डिन्य composed the पञ्चार्थ भाष्य of the पाशुपत सूत्रस्. These works seem to be chiefly the texts of the लाकुल-s and later कालामुखस् and are principally highly ascetic in nature. The common follower is merely asked to perform japa of the पञ्चाक्षरि. The लाकुल ascetic may follow many strange ways. He may if he wishes wander entirely naked or with just a कौपीन. He has ash baths and performs penances. He meditates in rudra shrines and may perform अट्टहास or sing and dance loudly showing lewd gestures. He sleeps on ashes and worships the लाकुलौघ आचार्यस् starting with लाकुलीश when bathing. He utters हुडुकार 3 times before uttering the पञ्चाक्षरि. He shuns women entirely and observes strict celibacy. The path is only for dvijas. If he touches a woman he must purify himself with प्राणायामस् and the rudra gAyatrI. They also may perform प्रणव japa to gain greater asceticism. Thus, the लाकुल shaiva is different from the classical tantric shaiva-s in that he observes entirely orthodox vedic elements and strict celibacy despite the outward facade of "contrary behavior".

The classical पाशुपतस् in contrast seem to derive directly from none other श्वेताश्वतर and we get information about their early history chiefly from the analysis of the most ancient version of the पाशुपत vrata provided in the परिशिष्ट 40 of the atharvan literature. Their links to the atharva tradition is also established by the use of a particular rudra mantra that is unique to Atharvavedins (though not recorded in any extant saMhitA):\
रुद्रं क्रुद्धाशनिमुखं देवानां ईश्वरं paraM |\
shveta-पिङ्गलं devesham prapadye शरणागतः ||

yasya युक्ता rathe सिंहा व्याघ्राश् cha विषमाननाः |\
tam अहं पौण्डरीकाक्षं देवं आवाहये शिवं ||

The पाशुपत-s also deploy the atharvedic dvaya rudra formulae for the ghee offerings.


