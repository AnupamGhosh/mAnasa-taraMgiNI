
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The peculiar mythology behind the मायावामन mantras](https://manasataramgini.wordpress.com/2006/04/16/the-peculiar-mythology-behind-the-mayavamana-mantras/){rel="bookmark"} {#the-peculiar-mythology-behind-the-मयवमन-mantras .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/16/the-peculiar-mythology-behind-the-mayavamana-mantras/ "6:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The हादि mata votaries from the North also preserved a range of kramas, now largely reduced to auxilliary rites associated as आवर्ण invocations of their primary rite of त्रिपुरसुन्दरी. This follows the basic pattern seen in the श्रीविद्यार्णव's procedures. Many of these are strikingly interesting such as the invocation of the terrific graveyard-ranging बहुरूप bhairava and his wife, a special mantra of the supreme Anandabhairava, the 8-handed अमृतसूर्य invocation and the formulae derived from the kaTha yajurveda. Here, however I briefly allude to a remarkable "left-handed" form of विष्णु and his mythology.

On the dark 14th day, the terrifying काल bhairava wanted a sacrifice. The 8 yoginis, who had emanated from the 8 मातृका-s realizing that they would reign supreme if the sacrifice is made, decided to do so. They extracted अम्बिका from the cave within सदाशिव, where she resides and made उमा herself the sacrificial pashu for the bhairava. shiva realized that his heart had become empty without पार्वती, and they say shiva without shaktI is but a shava! विष्णु was observing what had come to pass and acted rapidly. From विष्णु emanated the terrible संकर्षण. From him emerged the emanation of नारायण known as मायावामन. He had eight arms and was of yellow complexion. He wore a red robe and a golden headgear. He held the हलायुध, daNDa, sunanda and other terrible weapons. From garuDa emanated a ram, which became the वाहन of मायावामन. He was of the form of a youth. His mAyA shakti manifested as 4 beautiful space-clad goddesses, कर्पूरी, कस्तूरी, कुञ्कुमी and चन्दनी, who were महामोहिनी-s, from whose delusion no one was spared. From the right of विष्णु the manifestation pertaining to the great अनुष्टुभ् mantra (of the avyakta), नृसिंह emerged with a loud roar. मायावामन and his retinue and नृसिंह rushed at the yogini-s and challenged them to a fight. They destroyed the mighty weapons of the yogini-s with their astras, but their bodies were impregnable to strikes with any weapon. At this point they generated a terrifying goddess शिवदूती, who devoured the yogini-s and asked shiva to remember his true nature. As soon as shiva thought of his true self ever-conjoint with the hidden shaktI, from मायावामन emerged another terrifying goddess known as कालसंकर्षणी, who then retrieved पार्वती from the yogini-s and restored her to sadashiva. Thus, कालसंकर्षणी and शिवदूती became the guardians of पार्वती. पर्वती in honour of नारायण's serviced wore the images of मायावामन and नृसिंह as her earrings.

मायावामन sporting with his 4 space-clad goddess are worshipped along with नृसिंह in the मायावामन rite which can achieve वशिकरण and total delusion of foes. The goddesses can draw यक्षिणी-s and राक्षसी-s of diverse kinds to the साधक. The पाङ्चरात्रिc tantra मायावामनिका is devoted to this emanation of नारायण.


