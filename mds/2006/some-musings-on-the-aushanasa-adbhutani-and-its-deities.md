
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some musings on the aushanasa अद्भुतानी and its deities](https://manasataramgini.wordpress.com/2006/03/26/some-musings-on-the-aushanasa-adbhutani-and-its-deities/){rel="bookmark"} {#some-musings-on-the-aushanasa-अदभतन-and-its-deities .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/26/some-musings-on-the-aushanasa-adbhutani-and-its-deities/ "5:30 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The work on omens attributed to उशना काव्य the भार्गव (narrated by काव्य to नारद the काण्व) is a well-known hindu work that formed the basis for many such future works that are abundant in Sanskrit literature. In thrust and content it is related to another text covering a similar topic the अद्भुतानीय परिशिष्ठ of the पारस्कर गृह्य sUtra. Not surprisingly we find formulaic repetitions of the omens mentioned in it in the रामायण, महाभारत, पुराणस् and some काव्यस्. It also finds mention in the बृहत् saMhitA of वराह mihira, the mideaval Hindu natural historian. The modern western critic has found ample material in the quaint superstitions of the अद्भुतानी to lampoon the Hindus and their primitive ways. However, it is little accepted that these omens were an integral part of the belief system of the Western intellectual heroes, who today are placed on strange pedestals outside their natural context. Thus, in Greece we have Pythagoras, Democritus the atomist and Socrates have all asserted the importance of these omens and held firm belief in them. In fact the Greeks had a text similar to the अद्भुतानी which was composed by Aristander of Macedonia. The Romans similarly mention their own अद्भुतानी text, the Ostentarium Tuscum, which was also in use amongst the Etruscans, a non-Indo-European people. Parallel to वराह mihira we also encounter a similar compilation of prodigia (as अद्भुतानी were referred to by the Romans) in Pliny's text Natural Historia. We also have other Roman compilations of the same in the works of Valerius Maximus and Julius Obsequens. Thus, one may conclude that the belief in अद्भुतानी and their prognostic nature was firmly rooted throughout the pagan Indo-European world- east and west as also their non-IE neighbors. Interestingly, the basic system behind these omens was pretty similar across these ancient peoples, suggesting their common origin in the Eurasiatic world, followed by divergences, by incorporating local systems.

Idea here is to merely examine some general features pertaining to the relative era of their composition in the world of Sanskrit literature, not their contents in depth.

  - traditional नक्षत्र based months as well as zodiacal constellations are used: e.g magha, श्रवण, and सिंहा (Leo) are mentioned. The planets budha (Mercury), shukra (Venus), बृहस्पति (Jupiter) are mentioned

  - temples termed chaityas and associated holy trees (chaitya वृक्षस्) are mentioned.

  - idols of gods, लिङ्गस् and pictures of gods (चित्राणि) and seats of deities (आयतनानी) are mentioned.

  - volcanic eruptions from ponds, lakes and mountains, cyclones and earthquakes are mentioned.

  - Several traditional holy fig trees of the Hindus are mentioned: प्लक्ष ([Ficus infectoria)]{style="font-style:italic;"}, ashvatta ([Ficus religiosa]{style="font-style:italic;"}), udumbara ([Ficus glomerata]{style="font-style:italic;"}), nyagrodha (banyan, [Ficus benghalensis]{style="font-style:italic;"}) all together and the knowledge of their not having regular flower morphology is implicit.

  - The 3 dvija वर्णस् are mentioned, and the old equivalence of dasyus and shudras is implicit.

  - indra is the prominent deity (also called पाकशासन) and the annual public indra festival and the erection of the indra flag is alluded to.

  - The other gods mentioned include: पितामह/prajApati, soma, dharma, yama, agni (=पावक), वात (=वायु), kubera along with मणिभद्र and the यक्षस्, vAsudeva (meaning विष्णु), वरुण, महादेव (=rudra), skanda-विशाख, vishvakarman (=त्वष्ट) and gandharvas.

  - The goddesses mentioned are: इन्द्राणी, वरुणानी, भद्रकाली, महाबला, वीरमाता.

  - As an protection against prognostications of the अद्भुतानी the text recommends the performance of the महाशान्ती rite with offerings to indra, rudra and the vishvedevas.

The mention of the deities skanda-विशाख, महाकाली (along with वीरमाता and महाबला), point to the latest vedic period where these para-vedic elements were making their entry. This is also consistent with the use of the term vAsudeva for विष्णु along with पितामह for prajApati. The non-mention of later पौराणिc deities like the full-blown विनायक place this text before the tantro-पौराणिc expansion of the pantheon. The prominence of indra and his public festival is still the mainstay. In these features text matches the core epic period, where some of the same deities are mentioned the first time. The chaityas, लिङ्गस् and pictures and idols also point to the latest vedic period when these elements were coming into the mainstream from pAra-vedic streams of Indo-Aryan culture. Some of these elements may be of very archaic para-vedic provenance, going back to the Indus Valley period. While the fig trees are important in the vedic ritual itself (with the ashvatta as the tree of indra), their association with chaityas as chaitya वृक्षस् is possibly an IVC element. We have several IVC pictograms showing the horned deity associated with the ashvatta, a stylized shrine with an ashvatta tree, or depictions of the nyagrodha. There is also an interesting IVC seal with the female form of the horned deity being worshipped in the middle of the ashvatta tree by the male one. This is likely to be the precursor of the tantric वट यक्षिनि.

In my opinion the important point here is that the para-vedic elements enter the mainstream in a major way only in the latest vedic period. This means that the core vedic period was potentially 1) spatially disconnected from the paravedic IVC and 2) in part temporally disconnected too. This punctures the pet ऋवेद well after IVC hypothesis of the mainstream Indologist (as an aside).


