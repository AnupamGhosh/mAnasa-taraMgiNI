
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Wanderings in ओड्डियान](https://manasataramgini.wordpress.com/2006/09/26/wanderings-in-oddiyana/){rel="bookmark"} {#wanderings-in-ओडडयन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 26, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/26/wanderings-in-oddiyana/ "5:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Long ago the great old vaiShNava wandered in the hallowed ओड्डियान पीठ, when he reached an old desolate प्रासद towards the evening hour. There he seated himself and concentrated ठं bIja. Then the divine boar-faced mistress of the 5th lunar digit graced him. She of most beautiful waist appeared there with curly locks and bearing two shining tusks on her snout. Her awesome hips were draped by a most excellent blue and golden vesture and she held the still-bloody pestle with which she had smashed the skull of विशङ्ग. She who is called the सञ्केता, who is seated in the आज्ञाचक्र of the enlightened ones, had lustrous bangles that shone like rings of light and her shapely breasts, like jewel pots, were smeared with sweet-smelling saffron. The glorious सञ्केता also held a हलायुध that was washed with the blood of daityas slain by her in combat. The twang of her deadly bow filled the sky with thunder-like roars and their reverberations. On सञ्केता's car fluttered banners bearing buffaloes, deer and lions. In front of her marched a vanguard of dreaded bhairava-s, dark as collyrium in complexion, with tawny locks of hair and led by the four handed terrifying चण्डोच्छण्द. She was followed by an army of boar-faced goddesses with shapely bodies colored like the dark bark of तमाल tree ([Xanthochymus pictorius]{style="font-style:italic;"}). They had tawny hair and ruthless eyes, and held terrible weapons similar to those of समयसञ्केता. Smoke and flames shot forth from their snouts. and the curved tusks illuminated the darkness like crescent moons.\
They rode on charging buffaloes, while a few of these sow-faced goddesses diffused away guarding the flanks prancing around with their swords and shields. They advanced before them a fleet of siege engines known as तलवृन्त-s that formed a barricade for their division.

To her right side was a mighty goddess संपत्करी with firm breasts and effulgent complexion like that of the midday sun, charging on a violent armored elephant. She held a double-edged long-bladed sword washed by the gore, lymph and fat of दानवस् butchered by her in battle. Between her and the glorious सञ्केता was the splendid goddess अतिवरितविक्रान्ति on a white swift-moving horse. She was of fair complexion, with long flowing locks and was armed with a lasso, an attacking hook with which she used pull down daitya warriors in battle and a death-dealing rod.

To her left was the exceedingly dark-complexioned सङ्गीतयोगिनी. She had a haughty air about her and her armor glowed with the color of the rising sun. Her pretty face was accentuated in its beauty by the little drops of sweat that appeared on it and her charming eyes danced as she surveyed her hordes. सङ्गीतयोगिनी's car was decorated with banners of parrots and she stood in its midst holding arrows and a terrible bow, filling the firmament with twangs, that mingled with the jingle of her bangles to make a noise pleasing to the gods. सचिवेशी had two full quivers of deadly arrows beside her and led a huge horde of archer-goddesses.\
One who sees this experiences bliss.


