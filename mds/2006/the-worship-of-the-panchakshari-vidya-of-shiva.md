
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The worship of the पञ्चाक्षरी विद्या of shiva](https://manasataramgini.wordpress.com/2006/09/20/the-worship-of-the-panchakshari-vidya-of-shiva/){rel="bookmark"} {#the-worship-of-the-पञचकषर-वदय-of-shiva .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 20, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/20/the-worship-of-the-panchakshari-vidya-of-shiva/ "4:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Late in the evening the my clansmen and me left by an uncomfortable bus journey through the drAviDa country to the outpost of aunts N and H. Their clansmen had been killed by fiery अभिचारस् of their shatrus, but they took the help of our learned co-ethnic, a master of the mantra-शास्त्र to save themselves. While in गवलकुण्ड he was a student of the descendent of that great exponent of samaya-विद्या of the east. Then he had come to know numerous tantriks and became a great exponent of the mantra शास्त्र. His network of mantra-vid-s was immense and he had an enormous collection of mantra manuscripts. At my teacher's behest, I had obtained from him an old copy of the mantra-mahodadhi of महीधर, the savant of वाराणसी and the shArada-tilaka तन्त्रं. In his secular existence he was a village school master, who taught all subjects in his ramshackle school from around 9.30 AM to 3.30 PM 5 days a week. In his free-time he was also a medicine-man doling out minor tantric preparations for free. Form the outpost of our aunts' we went to see this savant in the village of Karur, arriving there late at night. The next two days we spent in deep discussion of the mantra-शास्त्र and पद्धती-s, left alone by our disinterested clansmen. He had given the same line of my clan as aunts N and H several copies of texts but they were sadly lost when a mantra performance of theirs back-fired on them.

It was this savant who introduced me to the worship of the पञ्चाक्षरी विद्या of shiva. The rite is very simple and has nothing unusual about its performance but is of considerable significance for the tantric realization of the पञ्चाक्षरी. The पञ्चाक्षरी is the first mantra a स्मार्त child is taught, when it is introduced to the script of the देवभाष. The मूल-mantra itself itself is from the yajurveda found in all the saMhita-s embedded within the 8th section of the शतरुद्रीय. The tantric संपुटीकरण-s with bIja-s must proceed only after one has performed the ritual mentioned here.


  - 0-0-0-0-

One first prepares a stone or clay altar called the पीठ. It should be circular or square and on it one inscribes the picture of a frog. The one utters the incantation:\
मण्डूकादि-para-तत्वान्त-पीठ-देवताभ्यो नमः |

Then into the पीठ one invokes the goddess of it, the first eight from वामा to सर्वभूतदमनि in clockwise order in the periphery and manonmani in the center:\
OM वामायै नमः | OM ज्येष्ठायै नमः | श्रेष्ठायै नमः | OM raudryai नमः | OM काल्यै नमः | OM बलविकरिण्यै नमः | OM balapramathinyai नमः | OM सर्वभूतदमन्यै नमः | OM manonmanyai नमः ||

Then one prepares the yantra shown below by inscribing it on metal (gold in best case), stone, baked clay or wood.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/shiva_panchakshari.jpg){width="75%"}
```{=latex}
\end{center}
```



Then one places the yantra in a copper or brass receptacle and performs an abhisheka of the yantra with ghee, milk followed by a cleansing water bath. Then one wipes the yantra with a clean cloth. Then one decorates the पीठ with flowers and installs the yantra on it uttering the incantation:\
OM namo bhagavate sakala-गुणात्म-shakti-युक्तायानन्ताय योगपीठात्मने नमः ||\
Then one offers पाद्यं, अर्घ्यं, गन्धं, दूपं, दीपं and पुष्पं with OM नमः शिवाय to shiva in the yantra||

Then concentrating on the hexagram of the yantra one worships the first आवर्ण by invoking the faces of shiva in the 4 cardinal direction and zenith. While doing so one holds flowers in ones joined palms in the अङ्जली-mudra:\
OM ईशानाय नमः (zenith) | OM तत्पुरुषाय नमः (East) | OM अघोराय नमः (South) | OM वामदेवाय नमः (North) |OM सद्योजाताय नमः (West) ||\
One then offers the flowers with the formula:\
OM अभीष्ट-सिद्धिं me dehi शरणागत-vatsala| भक्त्या samarpaye तुभ्यं प्रथामावर्णार्चनं ||

Then one worships the second आवर्ण comprising of the कला goddesses in the zone outside the hexagram but inside its circumcircle holding flowers similarly, starting from the east, proceeding clockwise:\
OM निवृत्यै नमः |OM प्रतिष्ठायै नमः |OM विद्यायै नमः |OM शान्त्यै नमः | OM शान्त्यतीत्यै नमः ||\
Then one offers the flowers with the formula:\
OM अभीष्ट-सिद्धिं me dehi शरणागत-vatsala| भक्त्या samarpaye तुभ्यं द्वितीयावर्णार्चनं ||

Then one worships the emanations of shiva termed the vighnesha-s in the middle of the eight-petals of the अष्ट-dala-chakra starting from East proceeding clockwise similarly holding flowers:\
OM अनन्ताय नमः | OM सूक्ष्माय नमः | OM शिवोत्तमाय नमः | OM एकनेत्राय नमः | OM एकरुद्राय नमः | OM त्रिमूर्तये नमः | OM श्रीकण्ठाय नमः | OM शिखण्डिने नमः |\
Then one offers the flowers with the formula:\
OM अभीष्ट-सिद्धिं me dehi शरणागत-vatsala| भक्त्या samarpaye तुभ्यं त्रितीयावर्णार्चनं ||

Then at the tips of the eight petals of the अष्टदलचक्र one worships the agents of shiva holding flowers from East clockwise:\
OM उमायै नमः | OM चण्डेश्वराय नमः |OM nandine नमः |OM महाकालाय नमः |OM गणेशाय नमः |OM वृषभाय नमः |OM भृङ्गारित्ये नमः |OM स्कन्दाय नमः ||\
Then one offers the flowers with the formula:\
OM अभीष्ट-सिद्धिं me dehi शरणागत-vatsala| भक्त्या samarpaye तुभ्यं चतुर्थावर्णार्चनं ||

Then in the directions one worships the gods:\
OM laM इन्द्राय नमः (E) | OM raM agnaye नमः (SE) | OM yaM यमाय नमः (S) |OM क्षं निरृतये नमः (SW)|OM वं वरुणाय नमः (W)| OM yaM वायवे नमः (NW)| OM कुं कुबेराय नमः (N)| OM haM ईशानाय नमः (NE)||\
Then one offers the flowers with the formula:\
OM अभीष्ट-सिद्धिं me dehi शरणागत-vatsala| भक्त्या samarpaye तुभ्यं पङ्चमावर्णार्चनं ||

Then one worships the weapons of the gods (the thunderbolt, spear, cudgel, sword, lasso, hook-spike, mace, trident)\
OM वं वज्राय नमः (S) | OM shaM shaktyai नमः (SE) | OM daM दण्डाय नमः (S) | OM खं खद्गाय नमः (SW)| OM pAM पाशाय नमः (W) | OM aM अङ्कुषाय नमः (NW)| OM gaM गदायै नमः (N)| OM triM त्रिशूलाय नमः (NE) |\
Then one offers the flowers with the formula:\
OM अभीष्ट-सिद्धिं me dehi शरणागत-vatsala| भक्त्या samarpaye तुभ्यं षष्टावर्णार्चनं ||

Then one offers दीपं, दूपं, गन्धं, पुष्पं and नैवेद्यं of honey and पायस. Then one performs japa of the mantra:\
oM नमः शिवाय ||\
Then one offers 50 oblations in the fire with this mantra with पलाश samidh-s dipped in honey and पायस. Then offer 5 तर्पणं-s with the mantra. He who does so is not harmed by bhava nor sharva nor ईशान, nor are his possessions harmed by The god.


