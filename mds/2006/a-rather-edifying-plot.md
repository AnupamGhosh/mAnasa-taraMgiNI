
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A rather edifying plot](https://manasataramgini.wordpress.com/2006/02/12/a-rather-edifying-plot/){rel="bookmark"} {#a-rather-edifying-plot .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/12/a-rather-edifying-plot/ "5:12 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There has been much abuzz in our मण्डल on the genetic support for Aryan invasion and allied issues of racial differentiation of human populations. In this regard, most of us in the know, now clearly see that the thing called the Aryan invasion is about the origins of the IEan's of India and there is little doubt that IEans originated outside India. The more interesting issue is the matter of racial relationships. In this regard it is clear that the massive autosomal data is going to bear on us in many ways. All of us in our मण्डल obviously turn to the now famous Shriver et al paper from [Human Genomics]{style="font-style:italic;"} last year. It is just a sample of things that can be done. Below is the plot they generated of the first and second principle components of their data of 11,555 SNPs from 12 different human populations:

 1.  Upper Caste Hindus 2) Lower Caste Hindus /(I need to see how good their definitions are)

 3.  Spanish

 4.  Japanese

 5.  Altaians (Mongolic people of Central Asian Altai of Russia)

 6.  Chinese

 7.  Mbuti- the pygmies of Zaire, Africa.

 8.  Burunge- A Tanzanian tribe speaking an Afro-Asiatic/Semitic language of the southern branch

 9.  Mende- A West African (Sierra Leone) tribe of Niger-Congo (Bantuoid) speaker. Their leader Kisimi Kamala invented a script for their language in the 1800s.

 10.  Quechua- The south American Andean people, the Inca peoples

 11.  Nahua- A native American people found in Mexico and central America

 12.  Nasioi- a Papuan tribe from the island of Bougainville

What the plot shows is that distinct clusters are formed:

 1.  The Quechua and Nahua are thoroughly mixed with not much differentiation between them.

 2.  The Chinese, Japanese and Altaians form a clear cluster with noticeable overlap, though there is some differentiation in their populations.

 3.  The Spanish, Upper and Lower Caste Indians form a close cluster approximately comparable to the Chinese, Japanese and Altaians in clustering.

 4.  The 3 Africans form three distinct clusters with no overlap but are generally closer to each other than anyone else.

 5.  The Papuans lie all on their own.

Thus the racial definition from physical anthropology is partly reproducible through molecular results, but there are issue missed byt it:

 1.  The Hindu-Spanish cluster is apparently congruent with the Caucasian physical type.

 2.  The Chinese-Japanese-Altaian cluster is congruent with the Mongoloid physical type.

 3.  The Native American cluster is indicative of a Native American physical type that was previously linked to Mongoloid in physical classification, but given their long isolation are likely to indeed be distinctive.

 4.  The Blacks are a more diverse group than many imagine. In fact the distinct African groups have had a long history of diversification through isolation.

Of course the isolation of Papuans is not a matter of surprise. Further developments of such studies are going to be of greatest importance in understanding recent human biodiversity and evolution, and probably provide information complementary and beyond those inferred from uniparental markers.

Now in terms of linguistics, it appears that in Africa the divergence of the major groups also corresponded to the break up of the languages families like Niger-Congo, Afro-Asiatic and the like. So is it possible that the diversification of the Mongoloid cluster in Asia corresponds to the divergence of Sino-Burman, Altaic and Japanese? Finally the issue of Indo-European and Caucasian diversification comes up. Here is where the future data will be of most interest to address the IE and Aryan invasion issues.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/some_populations_pca.jpg){width="75%"}
```{=latex}
\end{center}
```




