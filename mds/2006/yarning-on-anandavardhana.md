
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [yarning on Anandavardhana](https://manasataramgini.wordpress.com/2006/04/13/yarning-on-anandavardhana/){rel="bookmark"} {#yarning-on-anandavardhana .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 13, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/13/yarning-on-anandavardhana/ "6:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Anandavardhana was a remarkable figure. In my Sanskrit learning I gave him the skip because the theory and practice of अलंकार never struck a chord in me in my youth (I also had a great excuse). My mother spoke of a lot of theory and practice of अलंकार literature but I realized soon that her way of conveying it was not at all for me. In any case my then आचार्य found my excuse convincing and maintained that we could continue with analysis of the yajur vedic prose when I returned. But I had some vague knowledge of Anandavardhana from the other learned ब्राह्मणस् who were disciples of my original aged आचार्य. My teacher only said I should return to देवीशतक when I got the chance. Mis-creant who had taken Sanskrit formally in the college mentioned some terrifying text called ध्वन्यालोक and said she had a copy of the देवीशतक. But shortly there after I had to observe a major niyamas of for acquisitions of the rahasyas, and was totally cut off from all oral and even written communitication with her and never got the examining the text.

The other day in the train due to those unusual combination of events the mention of the देवीशतक came up like a flashback from the past. I understand from my sources, but have not really investigated it, that Anandavardhana was in addition to his well-known commonly repeated achievements a great kaula tantrik. He is supposed to have followed the mantra of the हादिमत as was common to his regions. He also seems to have simultaneously obtained siddhi of the formula of भानवि kaulini. His shAkta disposition becomes clear in the little fragments of the देवीशतक that I obtained, which are truely a masterpiece of Sanskrit poetry. On tasting the देवीशतक I realized that for a groundling like me this is enough to see the heights that अलंकार and chitra काव्य might have reached. I do not go to the कीराटार्जुनीय or such, the देवीशक्त and the मयुराSटकं are enough. No doubt it is said that it was composed after Anandavardhana performed a स्वप्नवाराही prayoga.

नतापनीत-क्लेशायाः सुरारी-jana-तापनी |\
na तापनी tanur यस्यास् तुल्या नादीनतापनी ||

Of whom, removing the impurities of her votaries, burning the enemy folk of the gods, the body is not unlike the sun, she grants success. This shloka is a good example to the yamaka (actually a double of them) Note the sound na-tA-pa-nI-ta * 2 and na-tA-pa-nI * 2


