
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on Shahji and Sambhaji the brother of Shivaji-III](https://manasataramgini.wordpress.com/2006/07/06/some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-iii/){rel="bookmark"} {#some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-iii .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/06/some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-iii/ "5:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Mustafa Khan's invasions]{style="font-weight:bold;font-style:italic;color:#009900;"}\
Shah Jahan called on Adil Shah and Qutb Shah to destroy the Hindus of the South and bring the whole of Hind under Islam. First, Mustafa Khan attacked the Vijayanagaran army lead by the Nayaka Shivappa who had liberated Ikkeri (1645). The Hindus fought with great fury and inflicted severe losses on the Moslems in the battle of Sagar. But Mustafa Khan who greatly hated the Hindus was strengthened by reserves and he fell back on Shivappa Nayaka, routing him in the second battle of Ikkeri. But Shriranga III began his counter-operations right away and captured Vellore. The Moslems were alarmed and the Qutb Shah and Adil Shah made a common cause and dispatched a large army of Jihad against Shriranga. Shahji refused to join this army eventhough he was asked to. At that time his young son Shivaji in Maharashtra instead started making preparations to take a critical fort of Kondana near Pune from the Moslems. His other son Sambhaji started secret negotiations with Hindu Palegars to look upto Shahji and not side the Moslem invasionary force. Mustafa Khan struck rapidly and captured Vellore. But as soon as Mustafa Khan returned to Bijapur, Shahji and Sambhaji secretly provided intelligence and help to Shriranga and he was able to defeat the Moslems and recapture Vellore. Furious, the Sultan sent Mustafa Khan along with Afzal Khan and Asad Khan to destroy Shiranga. Shahji was ordered to help the invasion and threatened with arrest if he sided with the Hindus. Shriranga contacted Shahji and asked him to open negotiations for peace with Mustafa. Shahji duely did so and was trying to buy time for the Vijayanagarans, when Shriranga thought he might succeed by launching a preemptive strike on the Moslems. However, he was mistaken-- while outwardly the Moslems seemed to be negotiating peace as per Shahji's moves, they were themselves preparing to strike. So Shriranga's element of surprise was completely blown off and the Moslems slaughtered his forces in the battle and seized Vellore. However, Shahji helped him escape with life. While the Adil Shahi army was tied with Shriranga, Shahji's son Shivaji captured the Kondana fort from the Moslems, while his elder son Sambhaji quite deposed a Moslem palegar in the Raichur Doab and was bringing territory under his control in contrivance with the local Hindu population.

The brahmins met at the Tirupati temple and bank-rolled a Hindu army using temple revenues under the surviving Nayakas to counter the Moslem depredations. Alarmed at the growing Hindu counter-attack the Sultans ordered a major offensive with two Jihadi armies under Mustafa Khan and Afzal Khan from Bijapur and the zealous Mir Jumla from Hyderabad. The Hindus at first fought the Moslem army at Virinchipuram, where despite their defeat staved off the Moslem army by inflicting heavy losses on them. The surviving Nayaka, Rupa Nayaka went over to Jinji and from this excellent fort began operations against the Moslems with the Tirupati funds. He kept hitting the Moslem armies repeatedly and kept retreating to his fort. Sambhaji kept providing the Nayaka secretly with intelligence and was thus coming of his own in conceiving a nationalist Hindu cause, much as his brother was in Maharashtra. The brahmins at the Tirupati meet also decided to take a second course of action and selected a set of expert tantriks to perform a series of अभिcआर rites on the Sultan of Bijapur. The अभिचार had its due effect and the Sultan's limbs were paralyzed. Mustafa Khan and Mir Jumla enraged over the developments launched a major attack on Jinji. Shahji and Sambhaji were asked by Mustafa Khan to join him against the Nayaka at Jinji. Shahji proved an obstructionist and kept interfering by shielding various Nayakas and delaying encounters. Mustafa Khan furious over these actions had him arrested with the help of Shahji's treacherous relative Baji Ghorpade when the former was offguard due to a wild party. Shahji was put in chains by Afzal Khan and taken with him to Bijapur. Mustafa himself was targetted by a मारण prayoga laid by the brahmins and is said to have died in a week there after. But his successor Muhammad Khan continued the siege and finally killed Rupa Nayaka and captured Gingi.

[Go to part-IV](https://manasataramgini.wordpress.com/2006/07/07/some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-iv/)


