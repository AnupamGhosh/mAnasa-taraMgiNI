
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [वीरगणपति](https://manasataramgini.wordpress.com/2006/03/05/viraganapati/){rel="bookmark"} {#वरगणपत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/05/viraganapati/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/ganapati3.jpg){width="75%"}
```{=latex}
\end{center}
```



My माताश्री always draws or paints her own images for her साधनस्. The most striking of these which is used is the अश्वारुढ वीरगणपति. It was of course copied from the original work by a Maharatta artist which was done for the student of the famed भास्करराय मखीन्द्र. When at home I have always used this one for the rite of उग्रविनायक.


