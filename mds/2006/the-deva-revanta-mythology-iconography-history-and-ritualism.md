
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The deva revanta: Mythology, iconography, history and ritualism](https://manasataramgini.wordpress.com/2006/08/31/the-deva-revanta-mythology-iconography-history-and-ritualism/){rel="bookmark"} {#the-deva-revanta-mythology-iconography-history-and-ritualism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 31, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/31/the-deva-revanta-mythology-iconography-history-and-ritualism/ "4:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/revanta.jpg){width="75%"}
```{=latex}
\end{center}
```



Like कार्त्तिकेय, revanta, a once popular deity, has faded from the Hindu mind, with decline of Indic civilization. However, in the south his successor ectype शास्तृ is still worshiped in the folk religion.  We have long wanted collate a file on this deva and revive his rites as we are amongst the few still adhering to his worship. We have been made aware of some other works on him but we have not read those.\
[Mythology]{style="font-weight:bold;font-style:italic;"}\
The पुराणस् narrate the core myth of the origin of the god revanta thus:\
विवस्वान् was given सन्ज्ञा as his wife by त्वस्टा vishvakarman. vivasvan begot the twins yama and यमी, and manu vaivasvata through her. However, सन्ज्ञा was unable to endure the excessive fiery lustre of विवस्वान् and decided to leave him and go to the regions of the uttara kuru. She created a replica of herself in the form ChAyA and placed her with विवस्वान् before retiring in the form of a mare. विवस्वान् took ChAyA too be सन्ज्ञा and begot on ChAyA, the inauspicious shanaishchara, the daughter तपती and manu सावर्णी. One day ChAyA got into a conflict with the death-like yama vaivasvata as she mis-treated her foster son. Thus विवस्वान् realized that ChAyA was not सन्ज्ञा and through his दूरदृष्टि located her in the uttara kuru regions as a mare. So he assumed the form of a stallion and reached there. They copulated first via their nostrils, spawning the ashvins and then internally spawning revanta. He was born with a sword and bow, riding a horse and bearing a quiver full of arrows. विवस्वान् said to revanta that he shall be the lord of the guhyaka tribe of यक्षस् and यक्षिणी-s. He mentioned that at the time of forest fires and volcanic eruptions, and when dasyus and म्लेच्छस् devastate the land the mortal need to invoke revanta to obtain relief.

Versions of this tale are narrated in the विष्णु (VP 3.2) and मार्कण्डेय (ंक्P 78) पुराण-s. The matsya (MP 11.2) and agni (AP 272.3; 273 in MBLD edition) पुराण-s narrates a variant in which विवस्वान् has a separate wife राज्ञी through whom he fathers revanta. The devi भागवतं mentions revanta as riding on उच्छैश्रवस् to visit विष्णु, when लक्ष्मी gets fascinated by the horse and fails to pay attention to विष्णु. As a result he cursed लक्ष्मी to became a mare and generates the haihaya dynasty in this account (DB 6.17-18).

[Iconography and history]{style="font-weight:bold;font-style:italic;"}\
वराहमिहिर states that idols of revanta should be shown on horseback with attendents hunting and sporting. This depiction of revanta is seen throughout the early medieval period throughout Greater India from Afghanistan (as reported by Priyatosh Banerjee) to the Vanga country. Typically he is shown with hunting attendants and is invariably accompanied by a dog. As with sUrya he is also shown with high-legged shoes and usually wields a sword and bow. revanta in his classical form appears to be attested (prior to 1100 of the CE)  only in the zone from the northwest to the east. His southern most spread in the classical form is seen Orissa. However, there are indicators that revanta once had a pan-Indian presence in some form comparable to the late vedic/"itihasic" deities such as कार्त्तिकेय, the पाञ्चरत्रिc tetrad and the rapidly evolving विनायक. revanta's iconography is strikingly similar to that of the deity popular in महारष्ट्र, कर्नाटक and Andhra known as खण्डोब or मल्लण्ण. He is depicted as riding a horse with a a dog accompanying him and holding a sword. Further, खण्डोब is known as a manifestation of मार्तण्ड-bhairava and an explicit connection with the solar cult through this link is seen in some खण्डोब temples of Maharashtra where he is shown with a solar emblem. Thus, it is likely that revanta evolved into the local खण्डोब cult after the 12th century CE in middle India. In this process खण्डोब derivative appears to have lost connection with the classical revanta. A parallel variant appears to have reached the deep south early in the form of the comparable equestrian deity शास्तृ also known as Arya (tamiL: चात्तान्/अय्यनार्). In this case he has been made the son of rudra and मोहिनी but there is a faint memory of his original roots in the form of a poorly known parallel tradition that sees him as a son of sUrya. In addition the mantra-शास्त्र pertaining to शास्तृ remembers the link by naming the ऋषि of the शास्तृ मूलमन्त्र as revanta (at tradition still known in his famous shrine at the shabari hill). In addition the companions of शास्तृ, like the [karappu-chuvami-s](https://manasataramgini.wordpress.com/2007/06/16/shasta-in-the-dravida-country/ "shAstA in the drAviDa country") appear to be parallels of the equestrian companions with whom the classical revanta is often depicted.  In the north and west he has a prominent presence in Rajasthan and Gujarat which also correspond with the western zone of the [old saura sect](https://manasataramgini.wordpress.com/2005/11/21/saura-mata/). A major temple to revanta was built by kalachuri ratnadeva-II in Madhya Pradesh, but appears to have been destroyed now. Given this and the similarities in depiction of sUrya and revanta, it is possible that the शाखद्वीपि brahmins had an initial role in spread of the revanta worship. The epithet revant and its Avestan cognate raevant have been widely used for Vedic and Avestan deities including mitra in both the texts (see below). Given this it is possible that the name itself could have even emerged in the para-avestan Iranian Hindus.

[Worship of revanta]{style="font-weight:bold;font-style:italic;"}\
There are several other references to worship of revanta in the पुराणस् and associated texts.

  - The agni पुराण (AP 116.22) in describing the oblations to be made during the pilgrimage to the holy spot of गया mentions that offering of rice balls to the 12 Aditya-s, agni, indra and [रेवन्त]{style="font-weight:bold;font-style:italic;color:#0000ff;"}free on from the hold of disease and one attains heavenly pleasures. The garuDa पुराण (GP 1.86.23 ) gives a homologous description of the rite at गया, but in this the offerings to revanta are made to obtain good horses.

  - The agni पुराण states that making a gift of a horse consecrated to revanta decorated with gold removes the fear of death (AP 211.28).

  - In the grand पौराणिक coronation of a king (typically used for honorary kShatriya-s), where the entire Hindu pantheon is invoked to imbue the राजन् with power, revanta is invoked along with कुमार, विनायक, वीरभद्र and nandI in one series of incantations (AP 219.18)

  - The अश्वायुर्वेद of शालिहोत्र states that periodically in the Ashvina month on the full-moon day the अश्वशान्ति rite for horses should be done. Amongst deities worshipped are revanta, for whom a lotus and pitcher with water are placed on the altar and 100 oblations with sesame, rice, ghee and white mustard are made (शालिहोत्रीय अश्वशान्तिः verse 6). A homolgous rite is mentioned in dhanvantari's हयायुर्वेद (verse 6) which states that oblations should be made for the safety of horses to revanta and tonic be prepared during the rite of the sarala, nimba, guggulu, सर्षप, tila, वचा and हिञ्गु extracts ground into butter. The Kashmirian नीलमत पुराण (NMP 394) also mentions that the owners of horses should make oblations to revanta.

[mantra prayogas]{style="font-weight:bold;font-style:italic;"}\
The worship of revanta deploys the tantric mantras and tantricized ऋग्वेदिc formulae. First one meditates on revanta riding a horse with a sword and bow and an excellent quiver. He is accompanied by his dog and his horse borne पार्षदस्.

The one utters his gAyatrI:\
[ॐ अश्वप्रीयाय विद्महे सूर्यपुत्राय धिमही । तन्नो खड्गिः प्रचोदयात् ॥]{style="color:#0000ff;"}

Then one utters his names offering flowers:\
[१) ॐ रेवन्ताय नमः २) ॐ वैवस्वताय नमः ३) ॐ संज्ञापुत्राय ४) ॐ अश्वप्रियाय नमः ४) ॐ अश्ववाहनाय नमः ५) ॐ अश्वपतये नमः ६) अश्वविद्याधिष्ठाय नमः ७) ॐ श्रेष्ठाय नमः ८) ॐ निशङ्गिने नमः ९) ॐ धनुर्-हस्ताय नमः १०) ॐ चर्म-धृते नमः ११) ॐ सुवर्ण- बाण- तुण- समन्विताय नमः १२) ॐ उग्र-तेजसाय नमः १३) सुर्य-रेतसोद्ध्भवाय नमः १४) ॐ मृगव्याधाय नमः १५) ॐ दस्यु-तस्कर-म्लेच्छादि-उपद्रव- नाशकाय नमः १६) ॐ यौवनाय नमः १७) ॐ अश्व-पालाय नमः १८) गुह्यकानां-पत्ये नमः १९) ॐ आरण्यचारिणे नमः २०) ॐ सुवर्ण-कवचिने नमः २१) यमानुजाय नमः]{style="color:#0000ff;"}

Then one performs तर्पणं-s or oblations with the महामन्त्र of revanta:\
[रां रीं रुं तानाशिरं पुरोळाशम् । रूं रें रैं इन्द्रेमं सोमं श्रीणीहि । रों रौं रं रेवन्तं हि त्वा शृणोमि । रं स्वाहा रेवन्तं पुजायामी तर्पयामी नमः ॥]{style="color:#0000ff;"}

One shall than install the yantra of revanta by invoking his परिवार of eight shakti-s:\
[ॐ रां दीप्तायै नमः । ॐ रीं सूक्ष्मायै नमः । ॐ रुं जयायैनमः । ऒं रूं भद्रां नमः । ऒं रें विभूत्यै नमः । ऒं रैं विमलायै नमः । ऒं रों अमोघायै नमः । ऒं रौं विद्युतायै नमः ।]{style="color:#0000ff;"}

Then one shall meditate upon the syllable raM at the center of the yantra. Then tantric महामन्त्र of revanta shall emanate from that:

[रां रीं रुं रूं रें रैं रों रौं रं असिधराय इषुधन्विने हुं फट् स्वाहा ॥]{style="color:#0000ff;font-weight:bold;font-style:italic;"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/revanta.1.jpg){width="75%"}
```{=latex}
\end{center}
```




