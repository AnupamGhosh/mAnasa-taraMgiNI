
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [von Nopsca's Jurassic deja vu](https://manasataramgini.wordpress.com/2006/06/15/von-nopscas-jurassic-deja-vu/){rel="bookmark"} {#von-nopscas-jurassic-deja-vu .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/15/von-nopscas-jurassic-deja-vu/ "5:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/Nopsca.jpg){width="75%"}
```{=latex}
\end{center}
```



Franz Baron von Nopsca of the Austria-Hungary empire (1877-1933) was one of those outliers in the bell-curve whose life makes an interesting story. From an early age I was inspired by his scientific virtuosity, while all the time being repulsed by the rest of his character. von Nopsca was an aristocrat who began the study of dinosaurian anatomy after the discovery of dinosaurian bones by his sister in their family estate. Few had the luxury like him to study dinosaurs literally in their backyard with all the comforts of a castle. I dreamt through most of my childhood of enjoying this envious condition of von Nopsca. von Nopsca, aged 17, took the bones to a great geologist, who asked him to study them himself. von Nopsca plunged into this study and soon emerged as one of the foremost paleobiologists of his time with far-reaching ideas on dinosaurian evolution. Beyond this he was a linguist and ethnographer who made a deep study of Albania. However, his political activities were progressively decadent. He was initially involved in liberating Albania from the grip of the Osman Turks. He tried to become the king of Albania himself. He then became fascinated with Albania Islamists himself and served as a spy for the Austro-Hungarian empire. In his subsequent career he showed pre-Nazi tendencies. Finally, he lost all his wealth due to the World War aftermath and had to sell his precious fossils to make ends meet. He was involved trying to seduce a rich American woman to profit her wealth and then in a homosexual affair with a Moslem servant from the Balkans, whom he killed in fit of depression. Finally killed himself thereafter.

He came up with theories far ahead of his times: 1) Dinosaurs with high metabolic rates like those of birds. 2) Origin of birds from fast-running cursorial dinosaurs 3) Dinosaurs providing parental care. 4) During his studies on geology of the Balkan region resulted in a plate tectonic theory with elements of the modern geological theory. Most strikingly though he discovered that the Hateg dinosaurs from his backyard such as the iguanodontian [Rhabdodon]{style="font-style:italic;"}, [Telmatosaurus ]{style="font-style:italic;"}(sauropod) and [Struthiosaurus ]{style="font-style:italic;"}(nodosaurid ankylosaur) were dwarfed forms and speculated that they had dwarfed due to the island dwarfing effect occuring on the Hateg island. Romania's Hateg region was an island in the late Cretaceous. This idea received much support from the recent findings of Weishampel et al and there is evidence that these island dinosaurs may actually have retained the primitive conditions that had long disappeared in the late Cretaceous on large land masses.

More recently Sander, Mateus et al reported a new sauropod [Europasaurus ]{style="font-style:italic;"}from the Jurassic, when the part of Germany where it was found was an island. By histological analysis of the bones they suggest that adults were indeed much smaller than typical sauropods and that [Europasaurus ]{style="font-style:italic;"}was dwarfed similar to Nopsca's examples. It is lodged between the Camarasaurus and Brachiosaurus+titanosauriform clade, and looks much like a dimunitive hybrid of the two. Thus Nopsca's idea of dwarfing on islands may indeed be more prevalent than previously known.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/Europasaurus.jpg){width="75%"}
```{=latex}
\end{center}
```




