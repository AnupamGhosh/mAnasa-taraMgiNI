
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [New anomuran crustacean](https://manasataramgini.wordpress.com/2006/03/13/new-anomuran-crustacean/){rel="bookmark"} {#new-anomuran-crustacean .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 13, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/13/new-anomuran-crustacean/ "4:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/Kiwa_hirsuta.jpg){width="75%"}
```{=latex}
\end{center}
```



The jewel of the clan of the foes of the foremost of the अमावसुस् brought to my attention that a remarkable new anomuran crustacean was discovered. Anomurans are a sister group of the crabs or the Brachyurans. Whereas the Brachyurans have a very conservative body plan- the traditional bauplan of the "classical" crabs, the anomurans show a great diversity of body shapes. Within the anomurans we have a clade formed by the squat lobsters (galatheids and chirostylids) and the porcelain "crabs" (Porcellanids). Then there is the clade formed by the hermit crabs and king crabs (Paguroidea) with the former dwelling in molluscan shells. Then there is the basal clade of Hippoids or the mole crabs. Given the frequent occurrence of the "crab"-like morphology within the Anomuran clade it is likely that this morphology was primitive to the whole clade combining the Brachyurans and Anomurans, and that the Anomurans have diverged into various different forms.

The new Anomuran, [Kiwa hirsuta]{style="font-style:italic;"} was found in hydrothermal vents near the Easter Island is marked by number of unusual features. It has degenerate eyes, white body and is extremely hirsute with a dense fur of setae on its chelicipeds and walking legs. The setae bear a dense growth of filamentous bacteria sulfur-utilizing bacteria. The exact role of these bacteria is unclear but it appears to be related to suriving in the sulfur-rich environments of the hydrothermal vents either to provide nutrients or to detoxify the emissions from the vent. Since it was found eating mussels, it is does not seem to rely entirely on the bacteria for food, unlike the vestimentiferans. Other Anomurans from hydrothermal vents, [Paragiopagurus]{style="font-style:italic;"}, a hermit crab and [Shinkaia]{style="font-style:italic;"}, a galatheid squat lobster also have such setal mats that bear bacterial colonies on them. Herein, we may be seeing the associations that of bacteria and animals that only underline my contention that the bacteria are the primary engines of biochemical novelty with the other lifeforms merely using them directly or indirectly. It is conceivable that there is indeed lateral gene transfer between these bacteria and the animals.

Phylogenetic analysis suggests that [Kiwa hirsuta]{style="font-style:italic;"} is indeed a sister group to the clade including the squat lobsters, porcellain crabs, to the exclusion of the obscure South American freshwater Aeglids. Sadly the authors did not use[ Lomis hirta]{style="font-style:italic;"}, the hairy stone crab in their phylogeny, which also belongs to the Anomuran clade.\
*[]{style="font-size:100%;"}*


