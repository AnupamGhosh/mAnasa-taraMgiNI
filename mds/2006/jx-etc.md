
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Jx etc](https://manasataramgini.wordpress.com/2006/07/17/jx-etc/){rel="bookmark"} {#jx-etc .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/17/jx-etc/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We heard from Jx that through the साधन-s his preceptor had bestowed on him he had completely repulsed the अभिचार. He was finally relieved from the flight of the crane: it was confirmed, but it had taken a severe toll on him. Most ill-effects except one on which the vairi had invested the most had been reversed. MM was considerably relieved that her mantras had rescued him. We had seen how the the siddhi of कात्यायनी had visited MM, and later the most hallowed skanda had also visited her, so we did not doubt her abilties at all. We had also known how the शचीव having obtained the रक्षोहा agni from her had tided over some troubles. However, the vairi was far more accomplished than her in the विद्या-s, and somewhere down the lane her own distractions did not allow her to grow further. PM was not as accomplished as the vairi, but was able to hold on with his yoga for a while, till he sadly entered into the strife arising from petty matters dragging all of us with that into the sapping battle, which terminated his span of life somewhat prematurely.

We were still burning with curiousity about mantras that slipped away in the राधेय-like failure of ours. We invoked the two splendid यक्षिणि-s उल्लासमेखल and nilalohita-मेखला. यक्षिणि-s instantly gave us the answer: 1) Back to the mantras of your fathers of yore 2) aryaman in the tenth hexad of the 6th.

We went back and saw it all, and saw what had happened.


