
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Gansus and the early ornithuromorph/euornithan evolution](https://manasataramgini.wordpress.com/2006/07/01/gansus-and-the-early-ornithuromorpheuornithan-evolution/){rel="bookmark"} {#gansus-and-the-early-ornithuromorpheuornithan-evolution .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/01/gansus-and-the-early-ornithuromorpheuornithan-evolution/ "5:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/gansus_yanornis.jpg){width="75%"}
```{=latex}
\end{center}
```



Several new specimens of the previously enigmatic bird [Gansus ]{style="font-style:italic;"}throw light on the evolution of early ornithuromorph birds. There appear to be the following evolutionary grades and clades recognizable to different degrees of clarity amongst the Mesozoic birds. The primitive radiation of birds appear to have included the very deinonychosaur/Epidendrosaur-like forms such [Archaeopteryx]{style="font-style:italic;"}, [Rahonavis ]{style="font-style:italic;"}and [Jeholornis ]{style="font-style:italic;"}followed by the emergence of birds with shortened pygostylic tails. Interestingly a pygostyle-like structure appears to have also evolved in the Oviraptorosaur Nomingia. Subsequently the pygostylians spawned two great radiations the enantiornithines and the ornithuromorphs/euornithes which included the modern birds. The enantiornithines spanned a wide ecological niche and were dominant birds throughout the later Mesozoic, however for reasons unclear to us they all became extinct in the great K/T event.

Feduccia, otherwise known for his senseless rants, proposed a model that the early ornithuromorphs were aquatic or semi-aquatic-- his shore birds. The new fossils of [Gansus ]{style="font-style:italic;"}combined with the phylogeny suggest that many of the early clades of ornithuromorphs, such as [Hongshanornis]{style="font-style:italic;"}, the [Yanornis-Yixianornis-Songlingornis]{style="font-style:italic;"} clade, the Hesperornithids, Ichthyornithids and [Gansus ]{style="font-style:italic;"}were all aquatic, whereas only [Apsaravis ]{style="font-style:italic;"}is convincingly terrestrial. One must also revisit the somewhat later [Vegavis ]{style="font-style:italic;"}from Antarctica in light of the aquatic early ornithuromorph hypothesis. Examining the ornithuromorph radiation we note that the most primitive member of this clade [Patagopteryx ]{style="font-style:italic;"}appears to be a terrestrial bird, even secondarily flightless. So together with [Apsaravis ]{style="font-style:italic;"}we have at least 3 major ecological niches amongst the better preserved members of the early ornithuromorph radiation- a flightless land form (which might have repeatedly happened in early avian evolution), a volant desert living form and several aquatic forms with different degrees of aquatic adaptation. When we go to the neornithes, we have the basal-most branch of the ratites, which are largely flightless and clearly all terrestrial cursorial forms. The next most basal branch, which are the earliest branching, neognaths are the Galloanserae. In the latter clade we have the late Cretaceous form [Vegavis ]{style="font-style:italic;"}an aquatic form- the authors of that bird even claim it is nested within Anseriforms or the ducks.

Further in the rest of the neornithine radiation we have several major aquatic lineages such as :\
Gaviformes: Loons; Podicipediformes+Phoenicopteriformes: A potential Grebe flamingo clade; A possible Procellariiformes + Sphenisciformes(albatrosses+ petrels) and penguin clade; Ciconiiformes (storks); Gruiformes (cranes); Charadriiformes (plovers). There is no evidence that these forms are all monophyletic or were aquatic by virtue of their common ancestor being aquatic. In fact many of the adaptations in them seem to be convergent for aquatic life. Hence it is quite possible that we have sampled only the proverbial tip of the more basal ornithuromorph iceberg and there were multiple aquatic radiations in them, just as in the neornithines. It is much easier to develop aquatic adaptations, especially given that many birds translocate to food rich shore zones than to lose them repeatedly and become terrestrial. So I believe the ideas presented by Lamanna and colleagues in the [Gansus ]{style="font-style:italic;"}paper regarding an aquatic ancestor for the neornithine radiation is not well-supported. But most workers seem to agree that reasons why the neornithines were the only dinosaurs to survive the K-T event remain still very unclear --- did it have anything to do with the aquatic mode of life?.

Another interesting feature is the presence of the unusual predentary bone (otherwise seen in the Ornithischian dinosaurs) in the basal aquatic ornithuromorph birds like [Herperornis, Parahesperornis and Ichthyornis]{style="font-style:italic;"} (noted by Larry Martin) and the more primitive [Hongshanornis]{style="font-style:italic;"}. This leads to the distinct, but unconfirmed, idea that there were actually two basal aquatic ornithuromorph radiations. One of predentary-containing forms such as the above and the other without including [Yanornis, Yixianornis and Songlingornis]{style="font-style:italic;"}. The skulls of Gansus and Ambiortus, if and when found might clarify this issue.


