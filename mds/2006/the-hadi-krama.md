
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The हादि krama](https://manasataramgini.wordpress.com/2006/04/15/the-hadi-krama/){rel="bookmark"} {#the-हद-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/15/the-hadi-krama/ "6:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In South India outside of the chera country the knowledge of हादि विद्या has largely declined. I recall a gentleman telling me that हादि विद्या is only used for अभिचार purposes. Of course this is an erroneous view. I was going through R's manual used for the हादि krama- pretty distinctive in terms of its वाराही and मातङ्गी formulae. But the हादि पञ्च krama involves the worship of आद्या-काली in the morning, tArA at the mid-day ritual, छिन्नमस्ता at the sunset rite, बगलामुखी at the nocturnal rite and हादी-विद्या at the mid-night rite. The worship of tArA at the mid-day ritual is shared by the हादि-mata with the सादि-mata.

Interestingly the हादि followers also accept the formula which is believed to be associated with the शान्खायन tradition of the ऋग्वेद:

[कामो योनिः कमला वज्रपाणिर् गुहा हसा मातरिश्वाभ्रं इन्द्रः ।]{style="font-style:italic;color:rgb(255,153,102);"}\
[पुनर् गुहा सकला माया च पुरुच्येषा विश्वमाता ऽऽदि-विद्या ॥]{style="font-style:italic;color:rgb(255,153,102);"}

Interestingly it is also at the heart of the महाचीनाचार tantra, the कालचक्र.

[ज्झ्रीं महाचण्डे तेजः सङ्कर्षिणि कालमन्थने हः ॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}


