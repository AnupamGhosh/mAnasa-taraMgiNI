
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The monkey-eating eagles](https://manasataramgini.wordpress.com/2006/09/10/the-monkey-eating-eagles/){rel="bookmark"} {#the-monkey-eating-eagles .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 10, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/10/the-monkey-eating-eagles/ "5:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From when I was a kid one part of the रामायण that thrilled me was the battle between the eagle-king जटायु and रावण in the दण्डक forest

[तत् च अग्नि सदृशम् दीप्तम् रावणस्य शरावरम् ।]{style="color:#99cc00;"}\
[पक्षाभ्याम् च महातेजा व्यधुनोत् पतगेश्वरः ॥]{style="color:#99cc00;"} 3-51-14\
That lord of the eagles blew off the flame-like armor of Ravana with both of his wings.

[काञ्चन उरः छदान् दिव्यान् पिशाच वदनान् खरान् ।]{style="color:#99cc00;"}\
[तान् छ अस्य जव संपन्नान् जघान समरे बली ॥]{style="color:#99cc00;"} 3-51-15\
The eagle also cut down the the पिशाच-faced mules yoked to the car of Ravana which were covered in glowing armor.

[संपन्नम् कामगम् पावक अर्चिषम् ।]{style="color:#99cc00;"}\
[ maNi सोपान chitra अङ्गम् बभञ्ज cha महारथम् ||]{style="color:#99cc00;"} 3-51-16\
Then the eagle shattered that great chariot decorated with gems glowing like the sacrificial fire with all its parts.

[पूर्ण चन्द्र प्रतीकाशम् छत्रम् च व्यजनैः सह ।]{style="color:#99cc00;"}\
[पातयामास वेगेन ग्राहिभी राक्षसैः सह ॥]{style="color:#99cc00;"} 3-51-17\
The eagle swiftly smashed the parasol with decorations glowing like a full moon, and the fans together with the demons operating them.

[सारथेः च अस्य वेगेन तुण्डेन च महत् शिरः ।]{style="color:#99cc00;"}\
[पुनः व्यपाहरत् श्रीमान् पक्षिराजो महाबलः ॥]{style="color:#99cc00;"} 3-51-18\
Again mighty and regal king of the birds tore off the big head of the charioteer of रावण with his beak.

For those who know of eagles there is no surprise in the above description of वाल्मीकि narrating the battle between रावण and जटायू. Eagles are born killers taking down victims of all sizes. The golden eagle ([Aquila chrysaetos]{style="font-style:italic;"}) is the most notorious of these. In Central Asia they were used by Turko-Mongols to guard their lifestock and are known to kill wolves outright in fierce aerial attacks. Elsewhere golden eagles have been known to kill cows and deer. In the Mediterranean the golden eagles have adopted an unusual method of killing the strongly armored tortoises -- they catch them and drop them onto rocks to shatter them. They fly after the falling tortoises so that they do not miss them as they bounce of. It is said that the Greek poet Aeschylus was killed when an eagle dropped a tortoise on his bald pate.

But the most enigmatic of the great killer eagles are the so called monkey-eating eagles. Different versions of these are found in most places where primates are found and specialize in killing primates in tropical and equatorial forest habitats. In South America we have the Harpy eagle ([Harpia harpyja]{style="font-style:italic;"}) which feeds on sloths and New world monkeys, in Africa we have the crowned eagle ([Stephanoaetus coronatus]{style="font-style:italic;"}) which specializes on a variety of monkeys including the large mangabey, in Madagascar there was the recently extinct ([Stephanoaetus mahery]{style="font-style:italic;"}) that preyed on lemurs, and in Phillipines we have the mighty Phillipine eagle (*Pithecophaga jefferyi*) that in addition to monkeys also eats civets, large snakes and [Varanus ]{style="font-style:italic;"}lizards. The fact that in different tropical niches there has been a primate specializing eagle is interesting. These eagles appear to be pinnacle of dinosaurian evolution being able to tackle highly intelligent mammalian prey in rather extraordinary niches. Primates are rather abundant in these niches but not easy prey, even for most mammalian predators. But the eagles have evolved a unique set of abilities to track primates on the forest floor or on trees and control their flight in the dense forests to take their prey. Most of these eagles are relatively long-lived birds living up to 60-70 years and are often monogamous for life bearing a single offspring once in every few years. As a result their populations are low and are currently very rare birds. They also have a peculiar breeding practise of getting fresh green twigs to their large nests. Some speculate this twigs might contain anti-parasitic properties and help to rid the nests of parasites.

A recent study by McGraw et al throws considerable light on the predatory behavior of the African crowned eagle. By studying bones in their nests they were able to show that about half of their entire prey is exclusively monkeys. Of this about a third were the relatively rare ground dwelling mangabeys that could weigh upto 11 Kgs. The distributions suggested that the eagles specifically targeted these larger monkeys. Injuries suggested the eagles rapidly killed the monkey by puncturing their skulls with their talons. The scraps on nearly all shoulder bones suggested that the eagle pinned them down by the shoulder, before/while puncturing the skull. Then they tore the prey to pieces and transported the pieces to their nests. The beak marks suggest that they broke through the eye sockets or base of the skull to eat the brain and broke of the tops of long bones to extract marrow. This clinches the idea of Berger and Clarke that the famous australopithecine, the Taung Child, was a killed by an eagle, most probably the same [Stephanoaetus coronatus]{style="font-style:italic;"}. The injuries to the Taung skull are identical to those seen in the monkey victims and the mammalian bone assemblage produced in the Taung cave was most probably made by a large nesting eagle. Thus, our early ancestors appear to have been prey for this large African eagle and it would be of interest to see how this might have affected primate evolution.

In New Zealand, which was an avian paradise, before the coming of the destructive Maoris, we encounter another giant eagle, the Haast's eagle that specialized in hunting the largest birds of the island, the moa. Interestingly, though it is one of the largest eagles ever known, it evolved recently through an explosive increase in size from the smallest eagle, the little eagle ([Hieraaetus morphnoides]{style="font-style:italic;"}) found in New Guinea and Australia. In Australia there is the fairly large Wedge-tail eagle, which working in groups has been known to kill adult kangaroos. Thus, in practically every continent we have a specialized major eagle predator. However, strangely in India, the land of garuDa and जटायू, where there is an abundance of monkeys in the tropical forests we encounter no monkey-eating eagles. Why this is so is a mystery. May be we need to investigate if there were at least fossil forms.


