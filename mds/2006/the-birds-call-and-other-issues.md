
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The bird's call and other issues](https://manasataramgini.wordpress.com/2006/02/18/the-birds-call-and-other-issues/){rel="bookmark"} {#the-birds-call-and-other-issues .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 18, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/18/the-birds-call-and-other-issues/ "10:40 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The songs and the calls of the only surviving dinosaurs around us range from the prosaic to the rich. They were the inspiration for many forms of art and science. In the long past when we were whiling away time in the vicinity of the wooded nallah in जम्बुद्वीप, our attention was drawn to several corvids. The corvids came in many colors which our limited vision could only partly appreciate. The corvids we saw were : the common crow, the large-billed crow, the treepie, the golden oriole, the black drongo, the greater racket-tailed drongo and the black-billed magpie. Occasionally we noticed that the large-billed crow or the magpie may imitate the calls of other birds rather than its own. We noticed this behavior often when a predator like a cat was in sight. Recently Goodale and Kotagama documented this behavior in depth in the greater racket-tailed drongo in Lanka. The GRTD lives in mixed species flocks, where it may go along with other tropical birds on an average from 12 other species with about 40 individuals. The drongos are agressive corvids that often attack other animals. When threatened by predators the drongos were found to mob them and emit alarm calls variety of a variety of birds and also the calls of eagles and hawks. Thus the drongo appears to not only warn its own species but also others. It might use the calls of predators like eagles and hawks to add to the tension.

The drongo, the crow, and other non-corvid birds like the parrot and the small green bee-eater show extra-ordinary cognitive abilities that appear to have convergently evolved to that seen in the mammals. An old acquaitance of ours believes that the bee-eater may have a 'mind' to figure out what the predator may be thinking. His experiments to suggest this are indeed interesting, but I am not sure they clinch the case. What ever the case for long though we have made the "connection" with our dinosaurian cognitive rivals in simple and complex ways.


