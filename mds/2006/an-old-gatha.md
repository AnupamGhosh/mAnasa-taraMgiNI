
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An old gAtha](https://manasataramgini.wordpress.com/2006/05/09/an-old-gatha/){rel="bookmark"} {#an-old-gatha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 9, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/09/an-old-gatha/ "6:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

During the train journey we talked about ancient gAtha-s going back to the PIE period. Some of these are clear cut sayings that were probably present from the earliest days of the Indo-European crown group. An early American scholar pointed the presence of an archaic Indo-European maxim that occurs in classical Latin as: "[quos deus pedere vult dementat prius]{style="font-style:italic;"}". In the महाभारत (repeatedly) and the रामायण a similar statement occurs as a gAtha:\
["yasmai देवाः प्रयच्छन्ति पुरुषाय पराभवं | ]{style="font-style:italic;"}[\
बुद्धिं तस्या .अपकर्षन्ति so वाचीनानि pashyati || "\
]{style="font-style:italic;"}For which ever man the deva-s prepare the overthrow, they take away his mind, and he sees things inverted.\
[]{style="display:block;" title="Remove Formatting from selection"}\
The implications of this Indo-European unity are not perceived by modern European and surprisingly even most modern Hindus. I place the idea that this is because the modern Hindu mind was de-Indo-Europeanized by colonization by the de-Indo-Europeanized Christian European.


