
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [shri श्यामोल् rAm the वामाचारिन्](https://manasataramgini.wordpress.com/2006/02/23/shri-shyamol-ram-the-vamacharin/){rel="bookmark"} {#shri-शयमल-ram-the-वमचरन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 23, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/23/shri-shyamol-ram-the-vamacharin/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R, narrated to me a remarkable tale that with her due acquiscience I present. Some details are left out due to the directive of the narrator. मोलाराम् was a remarkable man from the realm of pradyumna shAh deva. He was a renowned artist, a poet, a diplomat and a shAkta उपासक of a high order. It is said that he acquired the विद्यास् of the dasha महाविद्या of whom he was a great worshipper (especially of बगलामुखी, ugra-tArA and छिन्नमस्ता). He apparently was secretly a student of a mysterious tantric of the वामाचार path, who was a brahmin known as श्यामोल् rAm, with the initiate name मुण्डमालिनिनाथ.

He it is said lived in alone in a house in the forest along with his दूती close to an obscure spot known as शम्बुला-mokSha तीर्थ on slopes of the near the shrine of चिन्नमस्ता in Chintapurni. शम्बुला was a pious woman who was the wife of the diseased brahmin shrotrasena. She was one day being borne away by a दानव, when she worshipped indra, even as अपाल आत्रेयी had done. Pleased with her devotion, indra slew the दानव with an arrow. The arrow having pierced the demon entered the ground and created a stream. Here, he instructed शम्बुला to bring her husband and bathe him. He was duely relieved of his disease by the grace of the thunderer. Over the ages this तीर्थ has largely been forgotten, but श्यामोल् rAm sincerely offered his rahasya तर्पण-s there.

R's ancestor was a yajurvedic दीक्षित, who was apparently returning to his home from the Uttaranchal region, and he was in the vicinity of the Monkey fort, when he got the news of the invasion of the गूर्खास्. गूर्खास् was marching with utmost ferocity and were ravaging the land, when R's ancestor took shelter in the temporary hut where श्यामोल् rAm was residing. A few gurkhA's it is said surrounded the श्यामोल् rAm and began taunting him. He cast a spell of धूमावती on them and they were terrified and retreated. But wanting to deal with श्यामोल् rAm futher they came with reinforcements. But before they could reach him the गोर्खा capital was attacked by the Chinese invasionary force and they were forced to retreat. Many felt it was due to श्यामोल् rAm's siddhi. Later it is said that the British official Malcolm Gardner wanted to find a Moslem brigand and approached श्यामोल् rAm humbly. He using a दूरदृष्टि prayoga pointed to where the turushka was hiding and he was duely captured and killed. He also saved molA rAm during the renewed gUrkhA attack when they killed pradyumna shAh, his patron.

श्यामोल् rAm it is said had made many artistic clay idols of shiva and उमा and placed them around his house in the forest in little shrines. They were all worshiped regularly by people who would come to see him. He was a वामाचारिन्, who possessed the विद्यास् of certain tantras such as the फेट्कारिणि tantra, the मुण्डमाल tantra, the अक्षोबय tantra and the हयग्रीव tantra. R's ancestor acquired several shAkta prayogas from him. His most important साधानस् were those of चिन्नमस्ता in the महाविद्या series and वाराही-वज्रयोगिनी.


