
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Who is he?](https://manasataramgini.wordpress.com/2006/03/24/who-is-he/){rel="bookmark"} {#who-is-he .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 24, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/24/who-is-he/ "6:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/hybrid_indian.jpg){width="75%"}
```{=latex}
\end{center}
```



We had a discussion with K, ST and R about some web sites they were reading about obtaining the average facial type of a given population. They pointed me to a great free morphing software that did it- SQIRLZ MORPH. I decided to try my hand at creating the average Hindu young male face. I am not sure if this is really an average for nothing statistical has been done. My data set consisted mainly of cricketers whose mugshots were available face on. To it was added a Middle aged Dravidian actor provided by ST. There were a total of 11 distinct males in this data set: 5 from South India, 5 from North India and one from Bengal. The new face that emerges is reasonably clear but because I did not get properly positioned mugshots and because they were of very different sizes, there is some lack of clarity. In any case, this profile produces a reasonably close approximation of the Hindu Caucasoid face, which we encounter with some changes in our midst. The experiments on the average Hindu female may prove too distracting to conduct for now.


