
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Under पूर्व](https://manasataramgini.wordpress.com/2006/11/03/under-purva/){rel="bookmark"} {#under-परव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 3, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/03/under-purva/ "5:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The duel drew out with the adversaries for a while. It was slow moving with neither side being able to cut lose or declare victory. यक्षिणी-s and कृत्या-s flew by. We gathered our armor but knew that the राक्षसी could come after we feel asleep and would try to break our armor. We decided to try to intercept that duly. Tomorrow we would know how the struggle will play out.


