
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Delusions](https://manasataramgini.wordpress.com/2006/01/12/delusions/){rel="bookmark"} {#delusions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/12/delusions/ "6:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is fashionable these days for some parents to describe their daughters by the following terms:

 1.  Modern 2) Forward 3) City-type 4) Progressive 5) Modern but Indian. 6) Modern but Spiritual. The simple message is that one must be beware of such women- they are verily living graveyards for the deluded.

The Hindu youth have been deluded in many ways by the waves of mlecchaic subversion lashing the Indian shores. These are affecting both the sexes in drastic ways but in the case of daughters often Hindu parents live a disconnected world of their own making, much as चाचा Nehru imagined that he was the center of the world. These deluded Hindu youth are characterized by the following fancies: 1) In movies they watch entire lives compressed into 1-3 hr capsules. Every emotion is intense and instant. Sex happens right away, sexual romance is instant, heroes and heroines achieve their goals in that brief span of the movie. As a result the youthful watcher believes in instant gratification. Everything he or she wants should come immediately or else they will throw tantrums. 2) They are much richer than the older generation, so they lose track of the value of money and are willing to spend it on vices. 3)Macho Moslem men and handsome men and women are projected as their ideals. So they strive to emulate these, in the process losing their own sense of identity and also get secularized. 4) Their peer pressure guilds prevent them from doing anything profound and drive them to wallow in shallow thoughts, cheap entertainment and low-grade humor.

As a result the average deluded Hindu youth of today is not a pleasant company


