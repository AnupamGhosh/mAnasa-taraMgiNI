
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Kosambi and numerical analysis of the महाभारत](https://manasataramgini.wordpress.com/2006/05/06/kosambi-and-numerical-analysis-of-the-mahabharata/){rel="bookmark"} {#kosambi-and-numerical-analysis-of-the-महभरत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/06/kosambi-and-numerical-analysis-of-the-mahabharata/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Various kinds of numerical analysis on the महाभारत need to be carried out. The pioneers in this were the two unlikeable characters DD Kosambi and E Kulkarni. The former was definitely a very learned man, with whom I share intellectual and some what ascetic inclinations. Ironically, his father was the mahA-nAstIka who had converted to the bauddha heresy and studied none other than the archaic parama-पाषण्ड बुद्धघोष at great length. बुद्धघोष had called upon bauddha-s to not to attend narrations or recitals of the AkhyAna-s like the महाभारत and the रामायण. DD Kosambi had inherited many of the tendencies of his father like being a Marxist who saw Indian history through the delusions of the red-book, in addition to the bauddha heresy. Yet as an intellectual and Sanskrit scholar he was way ahead of the rest. We went to the same college, though I was not born when he died. He was thrown out of that college, where he was a mathematics lecturer, because his intellectual leanings did not go well with the plebeians around him (I can fully well appreciate that given the kind of students who attend this well-known college). I recall sitting in the lecture hall where he used to teach, when I was reading his statistical analysis of the महाभारत. It was a pioneering work for its time, using distribution of upa-parvan length and syllable statistics to study the concordance between the critical edition initiated by Sukthankar and the पर्वसंग्रह section. The implications of this study, and a more modern overview, need to be narrated at some point. In several ways Damodar Kosambi was like Rahul Sankrityayan another Marxist historian-- both were genuine scholars whose intellectual breadth should be appreciated. Both were however deluded simultaneously by the bauddha heresy and the red-book of the bearded German. Thus, all their scholarship took grotesque final shapes, though there is a lot of useful primary data in their works. In this they differ from the empty pots like Thapar and the other allied communist historians, including the degraded brahmin Subrahmanyan.


