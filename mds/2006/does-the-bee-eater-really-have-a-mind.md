
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Does the bee eater really have a mind ?](https://manasataramgini.wordpress.com/2006/02/19/does-the-bee-eater-really-have-a-mind/){rel="bookmark"} {#does-the-bee-eater-really-have-a-mind .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 19, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/19/does-the-bee-eater-really-have-a-mind/ "5:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our acquaintance and his field team concluded the following. They observed (like many others before them) that the little green bee eaters, a reasonably frequent bird in the haunts of the Maharatta country, which builds nests as tunnels, avoids visiting its nest frequently if it sights a predator. If an intruder came near their nest and could see the nest then the bird avoid reduce the frequency of their visits to the nest considerably. However, if the intruder positions himself such that he cannot see the nest, then the bird does not bother to reduce its visits and continue going in as though nothing happened. Further, let us say that the bird saw the intruder first standing in a position from where he can see the nest. Then if he moves to a position where he cannot see the nest; the bird still avoids visiting the nest frequently as though he were directly in a position from where he could see the nest.

Thus our acquaintance interpretted these observations to mean that birds have minds: 1) In the first case the bird perceives whether the intruder can see the nest or not. Does this mean that the bird is sensing the intruder's mind? We cannot say for certain. It is possible that the bird definitely has a good sense of geometry and can discern objects in the line of sight of its nest and those not. This is enough for it to make a judgement. 2) The second case is more subtle- does it imply a mind? One can say that the bird remembers the original position of the intruder and realizes that he can be a threat even if he is currently not in a position to see the nest. Thus, a good memory for "faces" and positions on a map are enough to explain the observations on bee eaters. Definitely sophisticated reasoning or decision-making for a bird, but I think calling it a mind as my acquaintance did is upto the beholder.

Nevertheless we would all agree that the birds pretty sophisiticated in their mental apparatus.


