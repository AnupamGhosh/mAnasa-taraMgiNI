
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Najash rionegrina and Yurlunggur camfieldensis](https://manasataramgini.wordpress.com/2006/04/22/najash-rionegrina-and-yurlunggur-camfieldensis/){rel="bookmark"} {#najash-rionegrina-and-yurlunggur-camfieldensis .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/22/najash-rionegrina-and-yurlunggur-camfieldensis/ "6:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

During the flight with the यक्षिणि we grew interested the origins of the giant madstoiids of Australia. There have been several major recent advances in snake phylogeny and evolution. In molecular terms the snakes were shown to belong to a lizard clade termed Toxicofera with other lizard lineages such as a clade formed by agamids, chameleonids and iguanids and another clade formed by varanids, xenosaurids, helodermatids and Anguids. It was clear that toxin production emerged in the common ancestor of this clade and was elaborated further in individual lineages. Within Toxicofera the varanids do not show any particularly close relationship with snakes but instead group with the other lizard clades mentioned above to the exclusion of the snakes. This destroyed a long held view that varanids and snakes were sister groups (the conventional view was actually contested by anatomist Mahendra a while ago, but nobody bothered to read his works).

Mike Lee had proposed many outrageous theories on reptilian evolution that were published in high places. These included the claim for a connection between turtles and pareiasaurs and more recently the claim that snakes and mosasaurs, which are extinct varanid lizards, are related. However, the molecular phylogenies have falsified both of Lee's claims. But the Lee camp including Scanlon and Cadwell is still trying to project a modified version of their theory in the form of the aquatic origins of snakes. Rieppel has been a fierce critic of Lee and has engaged in demolishing the latter's claims in various directions.

Most recently Scanlon reported a remarkably complete skull of the snake Yurlunggur camfieldensis of the madstoiid clade. They assumed enormous sizes and appear to have ranged from .5 meters to 8 meters in length. I am not certain of the madstoiid monophyly. But assuming that, they were distributed throughout the Gondwanaland from the Cretaceous and were also probably found in southern Europe. They appear to have peaked between the late Cretaceous to early Eocene, evidently making it past the K/T event unlike many other forms. In Australia alone they appear to have lingered past the Eocene and became extinct only around 50,000 years suggesting a possible role for the coming of the first savages to Australia. They clearly led a life similar to the boiids. Scanlon claimed that his phylogenetic analysis showed [Yurlunggur ]{style="font-style:italic;"}to be really primitive and close to the base of snakes. But the South Americans recently discovered a new snake [Najash ]{style="font-style:italic;"}and claim that their phylogenetic analysis shows that it is the most primitive snake. I suspect that choosing between the position of the madstoiids between the Rieppel and the Lee camps is harder. I do suspect that the latter may have point in their claim that the most primitive snakes were large-mouthed. I also feel the Scanlon phylogeny may be better on the matter of snakes (leaving out the mosasaur claims).


