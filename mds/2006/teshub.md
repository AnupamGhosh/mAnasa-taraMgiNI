
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Teshub](https://manasataramgini.wordpress.com/2006/04/10/teshub/){rel="bookmark"} {#teshub .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 10, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/10/teshub/ "6:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/teshub2.jpg){width="75%"}
```{=latex}
\end{center}
```



Two images of the Hittite deity Teshub: with a trident and axe, mounted on a bull, with the bull-head gear.

