
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [New wave](https://manasataramgini.wordpress.com/2006/10/23/new-wave/){rel="bookmark"} {#new-wave .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 23, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/23/new-wave/ "11:57 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The जानश्रुतेय birds flew by and said: "The spider has run around the web twice since that fateful night of the 3-front war. 2 bow-shots away from your seat you saw the deadly one. It was the same place where your then सेनानी immersed in sorrow narrated his defeat against the vaiShNava on the Northern plain. The कृत्या-s were kept aside from reaching you by the eye of mitra and वरुण. Now the काल of वृत्र has begun. The front called kosha could be weakened. You thought your defenses were firm, but you are not seeing the agent of धूमावती in your midst!"

What was dispatched O birds ?

They said: "That कृत्या, which does not retreat and the frightful पिशाची. It will be an all out attack with many fronts being targetted. In this regard we shall give you संध्या sandesha: kosha, हिरण्य, चतुरङ्ग, तिक्ष्णाम्लक, कृत, nidra. There are मारण-s too!"

The muni and Mn who was our main hopes was also attacked. The muni is being subject to the same attacks, while Mn will turn become विपरीत.


  - -o--o--

Since they watch we shall weave in a crooked path. The birds gave us the signal and asked to us take stock: 4 भारतस्+ extenders of the virile तैत्तिरीयिन्+fallen one+चित्रका are the capital. Of them 3 are odd. The उक्ष-daNDa has already been sent to you; so only 8 are left. Of the 3 odd ones 2 have already fallen and one will most probably fall in due course. The ज्येष्ठ-उपभ्रातर् has been neutralized though he is now free from the ग्राही. The कनिष्ठ has gone विपरीत and he is writhing in the ग्रहण. But he has two advantages over you -- he has conquered the ratha and kosha. But, as you always knew it is best to forget him. There 2 unknowns and they will remain unknowns in the foreseeable future (but could be intangibly helpful). So let it be known, there is only one who can take the throne if you perish in battle.

What has befallen him?

As we said when we flew by the last time he came under severe attack the same time you did. They sent the उक्ष-daNDa his way to. The muni has escaped with the aid of his prayogas from that which chased him like ब्रह्महत्या chasing the bhairava. Remember real yuddha-s are not affairs of few days so they rage on. The अमात्य tried several prayoga-s on you behalf. Most of them were broken by the भ्रातृव्यस् and have had minimal success. The week after you spoke with the pretty अयोनिजा in the mountains near the lake of rudra, when the PM was seized by a horrible ग्राही that was signaling the end of his life, two innocous visitors arrived. You and the muni felt that they were probably a passing cloud and the cell-phone at hand was the big issue. You did prayoga-s but they were not directed at the correct address. The seemingly innocous visitors saw that and deposited a packet on the morning of the fateful day when you, the muni and the said कनिष्ठ were in an unlikely alignment. The अमात्य and शचीव were right there and could have seen it, but in their characteristic foolishness were having a ball of time with the said कनिष्ठ. But such is fate and it was never reported. Also note friends are only for times of mirth, only clansmen for times of destitution.

शूद्र alone will escape the breaking of the fruits.


