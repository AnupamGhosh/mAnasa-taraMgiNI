
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The वीतिहोत्रस् and कशिराजस्](https://manasataramgini.wordpress.com/2006/04/11/the-vitihotras-and-kashirajas/){rel="bookmark"} {#the-वतहतरस-and-कशरजस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/11/the-vitihotras-and-kashirajas/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Now we shall narrate the great आख्यानं of the वीतिहोत्रस् and the काशिराजस्.\
The वीतहव्य the descendent of सहस्रार्जुन, had restored the glory of the haihayas after it was destroyed by रामो भार्गव. He and his numerous sons the वीतहव्यस् were well-versed in the vedas and the science of weapons, and were fierce warriors. They planned a great raid on the kingdom of kAshI, when haryashva was its ruler. Having forded the yamuna, वीतहव्य and his sons swept into the territory of the काशिस् by surprise. haryashva was caught with a small division and surrounded by the vitihotras. In the battle that followed they surrounded haryashva and riddled him with arrows killing him. Having collected a large booty of wealth and cattle they returned to haihaya territory. haryashva's son sudeva ascended the throne and a righteous ruler restored the glory of kAshi after a while. But this only attracted the वीतहव्यस् to invade the kashi realms again. In the battle that followed, the haihayas slew sudeva and having laid waste to his capital itself returned to their domains in the vatsa country \[the region developed by the भार्गवस्, the erstwhile priests of the haihayas, before the दत्तात्रेयस्]. Realizing the threat from the वीतहव्य and his brother तालजन्घ's sons the next king of the काशिस् built elaborate fortifications in his revived capital named वाराणसि, with the help of the great indra. Under दिवोदास's reign the kingdom teemed with prosperity and trade.

A son of the king दिवोदास, धन्वन्त्री, became a great physician and brought good health to his citizens. He promulgated a great school of medicine and taught a great medical text to punarvasu Atreya. Seeing the growing power of the काशिराज and the wealth of his kingdom, the वीतहव्यस् and तालजन्घस् prepared for war again. The haihayas with their tremendous military abilities cut through the kAshI defenses and besieged their capital at वाराणसि. दिवोदास defended it for 3.5 years relentless, at the end of which he was hard-pressed due to losses of men and animals. His treasury was also empty and unable to prosecute the war further, he simply retreated to पयू भरद्वाज's abode. The वीतहव्यस् conquered वाराणसि and annexed the kAshI kingdom.

He piteously appealed to the आङ्गिरस् for help. The भरद्वाज said to दिवोदास that he would perform a याग by which दिवोदास would have a smashing son, a great rAjarShi, who would revive the fortunes of his clan. After this the भरद्वाज performed a sacrifice with the objective of bestowing a mighty son to दिवोदास. दिवोदास's son was named pratardana, which means one who smashes his enemies to pieces. He was taught the mantras and the science of weapons by the भरद्वाज and he mastered this lore by the time he was 13 years of age. Then the आङ्गिरस् summoned him and put on a special armor charmed by the yuddha सूक्तं on him and gave him a mighty bow and sword. With this the prince ascended his chariot and with a sword on his waist, shield in arm, he shone like the star of day. He was praised by the ब्राह्मणस् and मागधस्.

Thus, he went to the presence of his father, who believing his enemies were already dead दिवोदास installed his son as the युवराज and asked him to raise a force to destroy the वीतिहोत्रस्. pratardana soon revived the kAshi army and reorganized it into a mighty fighting force. He suddenly forded the गङ्ग in the height of summer and attacked the वीतहव्य outposts. Having destroyed and set fire to them he advanced past the yamuna and attacked the haihaya kingdom itself. The haihayas issued forth from their capital in their thousands like ants from a nest. They surrounded pratardana with their many chariots and archers poured arrows on him like torrents. The showers of haihayas arrows were like various clouds pouring rain on the breast of the himavat mountains. Never since the days of कार्तवीर्य had the haihayas fought with such fury. The kAshI warriors were shaken by the shower, but pratardana was unmoved and discharged the weapon known as the lighting fire of indra. Storms of arrows broke forth and destroyed the weapons of the haihayas. The pratardana assailed them with numerous भाल्लस्. Each भाल्ल beheaded a vitihotra and drove them in confusion. pratardana continuing to assail them slew thousands of haihayas, and the field was strewn with beheaded corpses of the तालाजन्घ and वीतहव्य princes. pratardana's army surrounded the haihayas and killed each one of them and the who field was like a forest of किंसुक trees devastated by wood-cutters.

A single वीतहव्य prince escaped from the carnage and ran pell-mell towards the ashrama of his old enemies the भार्गवस्. The भार्गव ऋषि agni-aurva sheltered him and included him his clan by marriage as he was great knower of the veda. pratardana noticed the foot-prints of the वीतहव्य and followed them to the भार्गव's abode. He asked the भार्गव's students to call their teacher. The भृगु came out and gave the prince the due ceremonial reverance and asked him the purpose of his visit. He asked for the वीतहव्य and said that by killing him he would avenge the past entirely. The भृगु said there is no kShatriya in his Ashrama. pratardana touched the भृगु's feet and said: "By this I am definitely successful because the last वीतहव्य has been rid of his very वर्ण by the force of my attack". The भृगु declared that was indeed the case and agreed to invoke the devas on behalf of the काशिस् and dismissed the prince. वीतहव्य's descendents became the kevala भार्गव-s. Many great ब्राह्मणस् arose in his clan --- गृत्समद, shaunaka, यास्क and others.

pratardana दैवोदासि went to the sattra of the नैमिषीयस् and asked the following question:\
"If a ritualist seated in the sadas should call attention to a flaw in the ritual that was passed over or if one of the priests himself noted this later then how can one correct the flaw?" They were silent; अलीकयु वाचस्पत्य was their brahmA priest and he said that "I do not know that but I will ask the aged जातूकर्ण्य, teacher of the ritualists of the past" . He asked जातूकर्ण्य if it could be corrected by an oblation or by repetition of the mantra. He replied "the mantra should be recited again". अलीकयु asked again "should one recite in full the whole shastra, nigada or the याज्य recitation?" जातूकर्ण्य replied "so much as is erroneous only need be repeated, a ऋच, or an अर्धर्चं, or a पाद, or pada or वर्णं."


