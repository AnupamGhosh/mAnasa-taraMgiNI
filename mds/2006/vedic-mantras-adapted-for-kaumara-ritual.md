
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [vedic mantra-s adapted for कौमार ritual](https://manasataramgini.wordpress.com/2006/10/18/vedic-mantras-adapted-for-kaumara-ritual/){rel="bookmark"} {#vedic-mantra-s-adapted-for-कमर-ritual .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 18, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/18/vedic-mantras-adapted-for-kaumara-ritual/ "6:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/कुमार_mantra_vedic.0.png){width="75%"}
```{=latex}
\end{center}
```



[कुमारम् माता युवतिः समुब्धं गुहा बिभर्ति न ददाति पित्रे ।]{style="font-style:italic;color:#ff9900;"}\
[[अनीकम् अस्य न मिनज् जनासः पुरः पश्यन्ति निहितम् अरतौ ॥]{style="color:#ff9900;"}\
]{style="font-style:italic;"}The young mother keeps the boy hidden in secret, nor gives him to the father;\
Men see his undiminishing radiance only when he is set up on the fuel sticks \[reeds].

In the vedic context it refers to agni hidden within his "mother": the kindling wood block, who is hidden away from the father: the fire-drill. He become visible only when placed on the fire-sticks in the altar. The term aratau has been somewhat obscure to some commentators. It is a synonym of aratni, which means a unit length (probably ell), which is a metaphor for the samidh which might be an aratni long.

Its relationship to the kaumara worship is explained thus: 1) the two words कुमार and guha that come in the mantra are epithets of the god. Further, as per the kaumara myth, कुमार was born secretly, hence his name is guha, which is also the theme in this formula. 2) The young mother (माता युवतिः who holds कुमार secretly is interpretted as either the goddess गङ्गा or the forest mother sharavana. 3) The aratau are interepretted as the ell-long reeds: the sharas on which कुमार was placed and became visible to all.

[कम् एतं त्वं युवते कुमारम् पेषी बिभर्षि महिषी जजान ।]{style="font-style:italic;color:#ff9900;"}\
[[पूर्वीर् हि गर्भः शरदो ववर्धा पश्यं जातं यद् असूत माता ॥]{style="color:#ff9900;"}\
]{style="font-style:italic;"}What is this\[child], swaddled in cloth, you bear young lady? the mighty queen gave birth to him.\
The embryo grew through many autumns. I saw him when his mother bore him.

In the vedic parlance two different mothers of agni are being alluded to. The young lady who was carrying him swaddled in cloth and the queen mother who originally bore him.

In the kaumara parlance the young mother is again [गङ्गा or the reed forest mother](http://manollasa.blogspot.com/2005/07/skanda-ganas.html), while the महिषी is taken to mean the great mother उमा.

[हिरण्यदन्तं शुcइवर्णम् आरात् क्षेत्राद् अपश्यम् आयुधा मिमानम् ।]{style="font-style:italic;color:#ff9900;"}\
[[ददानो अस्मा अमृतं विपृक्वत् किम् माम् अनिन्द्राः कृणवन्न् अनुक्थाः ॥]{style="color:#ff9900;"}\
]{style="font-style:italic;"}I saw him from afar in his abode, bright-complexioned and with golden teeth, hurling his weapons; When I have offered him soma, how can the non-indra-worshippers and ones-without mantras attack me?

In the vedic sense agni protects the यजमान from the dasyu-s, when he offers oblations to the deva.

In the kaumara sense again कुमार is seen here- hurling his weapons, drinking अमृत from the gods and keeping away the enemies of indra.\
[\
]{style="font-style:italic;color:#ff9900;"}[वृषा जजान वृषणं रणाय तमु cइन् नारी नर्यं ससूव ।]{style="font-style:italic;color:#ff9900;"}\
[[प्र यः सेनानीर् अध नृभ्यो अस्तीनः सत्वा गवेषणः स धृष्णुः ॥]{style="color:#ff9900;"}\
]{style="font-style:italic;"}The bull sired the bull for the sake of battle, and the firm mother bore forth the manly warrior;\
He who is the commander of the armies, the fore most of the manly, he is the strong hero, brave and ready for war.

In the vedic context, it describes indra as the son of dyaus and aditi [पृथिवि], who becomes the commander of the forces leading the gods in conflict.

In the कौमार sense many points are seen as critical: 1) the epithet सेनानी- commander, is specifically that of कुमार. 2) The bull that sires him from the sake of battle is obviously seen as rudra (also described as वृष in the veda) for the sake of the battle with the enemies of the gods. The firm mother who bears him is seen as उमा.

  - 

  - [अर्भको न कुमारको ऽधि तिष्ठन् नवं रथम् ।]{style="font-style:italic;color:#ff9900;"}\
[[स पक्षन् महिषम् मृगम् पित्रे मात्रे विभुक्रतुम् ॥]{style="color:#ff9900;"}\
]{style="font-style:italic;"}He, while yet a small boy, mounted his new chariot; He at his mother and father's behest burnt the mighty buffalo.

In the vedic parlance indra is explained as slaying the buffalo even when he was still a young boy and just born.

In the kaumara context this is seen as an allusion to the कुमार mounting his chariot for war at his parent's behest to slay the demon महिष. The vedic story of indra burning the buffalo is obscure. However, it is possible it had some connection with the tale of कुमार killing महिष.

[\
]{style="font-style:italic;"}[अग्निरिव मन्यो त्विषितः सहस्व सेनानीर् नः सहुरे हूत एधि ।]{style="font-style:italic;color:#ff9900;"}\
[हत्वाय शत्रून् वि भजस्व वेद ओजो मिमानो वि मृधो नुदस्व ॥]{style="font-style:italic;color:#ff9900;"}\
Blazing like agni, oh over-powering anger, you are invoked as the commander of the army;\
Slay our foes, distribute their weath and give us might to scatter our enemies.

This is from the famous vedic manyu सूक्तं.

In the atharva vedic tradition the ऋषि of this hymn is said to be ब्रह्मास्कन्द. In the skanda याग of gopatha and the शाण्मुख-kalpa this hymn is supposed be used for the worship of कुमार. It contains an epithet of कुमार- सेनानी and the manyu सूक्तं describes the commander of deva armies as agni conjoint with the maruts (sons of rudra).

The adaptation of these vedic hymns for कुमार worship is not particular surprising, especially given that agni's 'birth' in the veda provides a parallel for the birth of kumara the son of agni (who may be identified with rudra). Indeed in the shukla yajurvedic tradition, the shatapata ब्राह्मण calls a manifestation of rudra to be कुमार. Many other elements of the kaumara myth are paralleled in the veda. For example, agni is elsewhere described as having 7 mothers and the constellation of agni is कृत्तिका \[the mother of पौराणिc कुमार]. The sons of rudra even in the vedic parlance- the maruts were the leaders of the deva सेना (RV 10.103.8):\
"[]{#2M}[देवसेनानाम्]{style="font-weight:bold;font-style:italic;"} अभिभञ्जतीनां []{#2N}जयन्तीनां []{#2O}[maruto]{style="font-weight:bold;font-style:italic;"} yantvagram". The नेजमेष hymn in the RV khila is a direct reference to the deity, but it is in the context of the birth-rites.


