
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The blight of Monotheism](https://manasataramgini.wordpress.com/2006/07/02/the-blight-of-monotheism/){rel="bookmark"} {#the-blight-of-monotheism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 2, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/02/the-blight-of-monotheism/ "5:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Moving out of the frustration from the futile discussion on the thermal stability of acyl monophosphates I began filling Marc up on what I had recently learnt from the two hours I had spent with a garrulous Spanish gentleman. He yarned endlessly and voluntarily about trypanosomes, Mexican warlords, the need for Spanish as a first language in the US and the disappointing results of the Spanish-American conflicts. Suddenly Marc brought up the issue of a book that is being widely discussed: [God Against the Gods: The History of the War Between Monotheism and Polytheism]{style="font-style:italic;color:rgb(255, 0, 0);"} by a Jewish author Jonathan Kirsch. I had the chance to briefly read through it while waiting for a plane and could see how it does have an important message for the West. Many in the West are slaves and they do not even know it. They have been mentally []{style="font-family:Garamond;"}imprisoned by the monotheistic dogma of the Abrahamisms. And it is very bold of Kirsch to tell the story as it really is. Of course this is one of the few clear attempts in the West to tell the some of the story of how the Abrahamisms have brought imprisonment to their minds and death and indescribable terror to millions of victims of the infected European mind. Truely the Abrahamisms can be described memetic viruses that have not only damaged millions of minds but also spilt over claiming more lives than an epidemic of a genetic virus. Interestingly, this must also be considered in light of Hermann Sommers' idea that many of the arch prophets of Abrahamism, especially the last two, were victims of psychiatric conditions. So may be the particularly virulent monotheisms, Isaism and Mohammedanism emerged from biologically diseased prophetic minds and then spread purely as a memetic infection.

However, these discussions were first brought out with tremendous clarity by none other than the two vaishya चूडामणिस्- Ram Swarup and Sitaram Goel. They had seen all the dimensions of the problem and with their approach one can understand why the virulent sectarian forms of vaiShNavism and लिङ्गायतिस्म् did not evolve into the explosive and dangerous monotheisms that we encounter in the middle east. The evidence for the difference is very much there for all to see. Especially when one compares India to the the wastelands of the diseased mental landscape such as the Terrorist State of Pakistan, Iran and the fetid sewage called Saudi Arabia. []{style="font-family:Garamond;"}


