
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The bhinnayoni विद्या](https://manasataramgini.wordpress.com/2006/07/02/the-bhinnayoni-vidya/){rel="bookmark"} {#the-bhinnayoni-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 2, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/02/the-bhinnayoni-vidya/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Since I was asked of the famed bhinna yoni विद्या, which powers the mahA मातृका न्यास I have written it down. Of course its प्रकट as प्रत्यङ्गिरा needs the inter-twining with the atharvamukha, of which the bhinna yoni is itself a प्रकट रूप. Its manifestation as परमेश्वरी is can be seen in the stand alone bhinna yoni विद्या, though a practioner of the bhrigu-आङ्गिरस shruti notes the deep concordance with the atharvamukha constantly. Its manifestation as मालिनी, as stated from the mouth of trishiro-bhairava is in the form of the mahordhva लिङ्ग and the laghu-yoni. "Oh girl of pretty hips, whose firm breasts are the mounds of manmatha-कला, how many श्रिकण्ठस् and shiva-yuvati-s manifest when we triangulate the great upward लिङ्ग with 3 equal sides, 6-fold times ? There is one more shiva-yuvati than those of the श्रिकण्ठस्. He who knows this knows the विद्या"

Of course any one trying to make use of the महामन्त्र given below without the rahasya योगिनी-s goes no where.

oM na ऋ ॠ L\^i ॡ tha cha dha I Na u U ba ka kha ga gha ञ i a va bha ya Da Dha Tha jha ङ ja ra Ta pa cha la A saH ha Sha क्ष ma shaM ta e ai o au da pha ह्रीं

The दूती-s of the नादि vidya are conjoined with the rudra-s of शब्दराशी.


