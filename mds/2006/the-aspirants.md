
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The aspirants](https://manasataramgini.wordpress.com/2006/06/12/the-aspirants/){rel="bookmark"} {#the-aspirants .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/12/the-aspirants/ "5:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

RM had a charismatic appearance and could present a himself as an intellectual. He was not a स्वयं प्रकाश but picked up knowledge from different generally reticent sources and presented to his circle of largely ignorant friends. He widely lectured to youngsters about the Hindu ways of mythology, philosophy, exercise and ritual performance. Most of his crowd was bored and clueless, forced to attend RM's earnest lectures at their equally clueless parents behest. N ran a similar show of teaching the veda, more as a socializing process, also participated in the above sessions. But RM disapproved of his teaching the veda in such a non-serious way but still enjoyed the social intercourse N generated through his activities. I kept out of all of this despite several requests because I dwelt in the middle of the रसार्णव as the साक्षि but not the कर्ता. The one linked to me gave me input to the outer social world, these were स्थूल, or mundane links. But R was the link to the सूक्ष्म or the tattvas that we sought to witness as the साक्षि.

RM noticed that in his vast flotsam mass, even as the mass with which we travelled on the नदी on the boat, there were two peculiar ones. Knowing that they were beyond the crowd, and that they might benefit from a more advanced course, RM sent them to R. R in turn sent one of them to me under peculiar circumstances. We waited at the square where we had deluded many with the moha conferred on us by महागणपति. There were saw our accquaintence kanduka-netra of the second वर्ण, who was a practical master of the madana-शास्त्र with that rasping accent like a stick rattling on a railing. He went around us 5 times without seeing us and finally converged on us. We talked to kanduka-netra as though we were an unmatta --- he still saw through to see [me ]{style="font-style:italic;color:rgb(255,0,0);"}behind all that. There were two others with us during this meeting. One only saw us as the unmatta, while the second one saw through us. I said to that second one, three words one in the द्रमिड भाष, one in the महारट्टि language and one in deva भाष. The second one replied in deva भाष. We had gotten the data we wanted and bade them good bye to eat their abhojya food. We promptly reported this to R, who said: "Wait till the mouse comes to the trap, and let not the cat go after the mouse". The lock and key had fitted.


