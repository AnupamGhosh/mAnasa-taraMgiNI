
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [rudra नमस्करं](https://manasataramgini.wordpress.com/2006/04/19/rudra-namaskaram/){rel="bookmark"} {#rudra-नमसकर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/19/rudra-namaskaram/ "5:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some of the kaTha-s record a praise of rudra when ever they see evidence for rudra in nature:\
[[\
]{style="color:rgb(0,0,0);"}]{style="color:rgb(51,204,0);"}[दंष्ट्रा-करालं दिवि नृत्यमानं हुताश-वक्त्रं ज्वलनार्क-रूपं ।]{style="font-style:italic;color:rgb(51,204,0);"}\
[सहस्र-पादाक्षि-शिरोभि युक्तं भवन्तम्-एकं प्रणमामि रुद्रं ॥]{style="font-style:italic;color:rgb(51,204,0);"}\
[नमो।अस्तु सोमाय सु-मध्यमाय नमो।अस्तु देवाय हिरण्यबाहो ।]{style="font-style:italic;color:rgb(51,204,0);"}\
[नमोऽग्नि-चन्द्रार्क-विलोचनाय नमोऽम्बिकायाः पतये मृडाय ॥]{style="font-style:italic;color:rgb(51,204,0);"}


