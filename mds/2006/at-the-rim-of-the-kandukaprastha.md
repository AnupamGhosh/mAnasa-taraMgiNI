
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [At the rim of the kandukaprastha](https://manasataramgini.wordpress.com/2006/09/11/at-the-rim-of-the-kandukaprastha/){rel="bookmark"} {#at-the-rim-of-the-kandukaprastha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/11/at-the-rim-of-the-kandukaprastha/ "10:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We stood accompanied by three others at the rim of the kandukaprastha. They were: the fallen brahmin, whose eyes were fixed on the exceedingly rare तीक्ष्ण-tripatha स्त्री ratna-s, much like those of the bauddha बिक्षु, which were set on the serving ladies in the Kashmirian monastery. Then there was the ब्राह्मण who followed the vaiShNava mata, whose nose was verily like the hooked beak of the carrier of his chosen god. Then there was the शूद्र, who was indeed much like a heaped mass of collyrium. His hair stuck out like needles, like सूचीरोमन्. We walked along the rim of the kandukaprastha towards the foot of कप्याद्रि and then sat down at the nether end. Suddenly an animated discussion broke out on yuddha of द्वादशनन्तर. The rest of the three showed their weapons as we watched. Then the fallen prathama and the श्रीवैष्णव went away and only the शूद्र remained with me. He was terrified by the weapons of the former two and asked me if I would accompany him to train at the धूर्तशाला.

I did not want to but I acquiesced nevertheless for reasons unknown to me. Everyday of the hot sweltering summer the शूद्र used to come and take me along to the धूर्तशाला. We returned just after noon, and I ate my afternoon meals in solitude. The in the evenings I would mount my ashva and ride upto the धूर्तपीठ, just beyond the shrine of the elephant-headed one. At the same time the vaiShNava and the पतनीय used to go to the ऋक्षशाला. One evening, taking my faithful old ashva I strayed in the direction of the shrine of चण्डिका. I first passed the shrine of The god. Then I passed a shrine of madhumativallabha, which had pictures of the nine नाथस् starting from the hallowed matsyendra. Then I passed the shrine of the fiery kapi. Shortly, there after I was lost in my own internal solitude, when I head someone call out my name in a rattling accent. With my reverie broken, I looked up and saw the powerful ज्येष्ठदन्त hrasvaroman standing on our path with his fast ashva. He asked how we spent our time were during the days. I replied: "In the धूर्तशाला with the कृष्ण-शूद्र". A smile of satisfaction seemed to pass his face. I returned the same query to him. He said with a tinge of triumph "I go to the ऋक्षशाला, and thereafter in the afternoons, to the रसशाला". I smiled and asked him who were his भ्रातृव्य-s there and how they were doing.

ज्येष्ठदन्त hrasvaroman replied: "Oh foreign brahmin, I am happy not see you at those places, else you might have discomfited all of us. There are of course पतनीय and vaiShNava who offer stiff resistance. There is also the तीक्ष्ण tripatha-स्त्रीरत्न who makes us very uncomfortable with her yuddha-kaushala, but thankfully she deludes the former two with her [mukha-वक्ष-saundarya]{style="font-style:italic;"}, while I who has suppressed काम am not affected by that. But above all of them is that co-ethnic of yours, the fierce foreign brahmin, राजक, the first son of the brilliant ब्रिन्दा. He is unstoppable- he beat even that शिरोमणि of the local brahmins, he burns us down in the training sessions like rudra at the end of a kalpa. He finds errors in all of us and is never stumped by any astra sent at him. Our local brahmins were exceeding proud before his coming, but even they are forced to respect him these days, and he has spread terror on the क्षेत्र even as ब्रिन्दा had done in her days amongst our parents."

I said: "Indeed the son of ब्रिन्दा is more than a mouthful for anyone. I have heard much of his yuddha-नैपुण and in the brief encounter with him could feel his scorching fury. But then in the company of the शूद्र in the धूर्तशाला, why do I need to worry myself over this co-ethnic of mine -- after all it is the rich and the famous who need to be threatened by his ugra-tejas."\
h.j: "Oh वाजरत्नायन, while I am happy that you are not there at the ऋक्षशाला or रसशाला to add to our woes in the युद्धाङ्गण, why is that you have choosen to go to the धूर्तशाला? . What goes on at the धूर्तशाला? "\
I: "Oh mighty hrasvaroman, while my father is of moderate means, enought to provide me with an ashva and good food, we do not have the riches to waste on ऋक्षशाला-s. Further, I have wielded astra-s even as a kid, and have even survived even the aforesaid son of ब्रिन्दा, albeit in a short encounter. So while I do not want to appear boastful, I do feel I am capable of survival without going to the ऋक्षशाला. I am not as ambitious as you all are in the directions you wish to conquer, and I am content studying the veda-s and other rahasya-s of existence rather than clamor after the glories you all seek. Hence, I do not go to the glorious places. For reasons I do not know, I go with my faithful शूद्र fellow-traveller to the धूर्तशाला. It is not a pleasant place but like विश्वामित्र, we see the rich inner world even while being surrounded by extraordinarily boring and fallen characters."

Then we went our ways. I rode some distance towards the shrine of चण्डिका, and the proud Kartik passed us on his ashva. We then looped back and were riding ahead when और्वशेयी crossed our path on her ratha. We stopped to converse.\
She said: "My spashas have informed me that the yuddha of दशानन्तर was going to be intense and that there was a new highly feared warrior- the son of ब्रिन्दा."\
I said: "Oh Mis-creant, indeed I hear that at the ऋक्षशाला, the son of ब्रिन्दा is spreading terror. But you do not go to the ऋक्षशाला, just like me, so why break our heads with such information. Everything can be settled on the real field of battle. Plus, why do you need to fear anything Oh और्वशेयी."\
कलशजा said: "I am itching for an encounter with this राजक, the son of ब्रिन्दा. He had given me an existential challenge in the battle of राष्ट्र-संकुल that we fought when you were studying the पुराणस् instead of participating in that गविष्टि. I will scorch the son of ब्रिन्दा in the महायुद्ध".

Almost an year later I was standing in front of the stone walls about a 1/2 kilometer away from the upa-giri when the the news reached us of the fierce encounter. hrasvaroman had fought fiercely and destroyed पतनीय, तिक्ष्ण-tripatha-स्त्री and the vaiShNava in the संग्रम. Others like Igul, कुण्ड-भेदी, nAstIka and others were blown to bits by him. However, as he was advancing he was overthrown by the son of ब्रिन्दा in one of the fiercest संग्रांअ-s that had been ever witnessed. But even as he was bearing away his वाज, कलशयोनी destroyed him at the last the frontier. On that day we learnt to respect और्वशेयी's skills in battle for good.

Many years later the gods had sent people down their paths- it is said that indra gives every one their due fate. hrasvaroman, despite his defeat had done well enough in the संग्राम that he settled down as the युवराज of his father's jagir. He continued to enrich his estate despite having to paying tribute for 6 years to ब्रिन्दा's fiery son. The son of ब्रिन्दा after all his fiery exploits on the field was crushed by कलशजा, but she relinquished her conquests for higher quests. Hence, he got to keep most of his gains in combat and attain extraordinary heights. But then the force of fate omits none, and he had fallen in a pit of misfortune. He was wandering thus, when one day he ran into कलशजा's husband. और्वशेयी's husband took pity on him and pointed him our way. We saw the great warrior of old, now like the Wang Khan shorn of all his splendor after intersecting with the Khan of the Borjigins. We enquired of his mother, and his well-being and having let him rest allowed him to go his way.

Why had Brinda's son fallen? I pondered over this for some even as I was blocked from making contact with my clansmen like the atri, aunt Spring and Jx by the grip of the कृत्या. As I was in a reverie feeling like one in the desert of existence, I received a call from और्वशेयी. What follows is संध्या-\
कौण्डिण्या: Does the वार्ताली of the dreams favor you?\
I: Apparently not.\
कौण्डिण्या: Is that why you wander in the मरुक्षेत्र with no यक्षिणी in sight?\
I:Indeed. I took the winding path beyond पाताल-गङ्गा and returned then to my fortress. There was no यक्षिणी in sight other than fleeting glimpses of नीललोहित-मेखला who saved me on that fateful day.\
कौण्डिण्या: The only way out is the fire of prANa-रसायन! Seek it.\
I: Indeed but that does not shake off the कृत्या that is blocking me from my clansmen.\
कौण्डिण्या: The fate of our "compatriot" राजक the son ब्रिन्दा comes to mind.

  - -- end of the veil--\
I: How did it come to be?\
कौण्डिण्या: When we were still kids we sat on the top of the hill of चण्डिका as विवस्वान् plunged into the golden western depths. It was just the two of us- a cool malaya-marut blew around us and we saw the whole city of the founder of the Maharatta nation beneath us. Beside us were the two human skulls one nearly whole and the other a fragment. The spell of चण्डिका was protecting us from the दास्यू-s who crouched yonder. At one end of the mountain ridge was the hill of the वेताल-s were the one forsaken by the gods had fallen. I asked why had he fallen thus. Even beyond it lay the large pit where lived Vidrum the Lingayata. I asked how this mountain-scaling Vidrum was captured by the the पिशाचस् for 5 years. You then narrated the tale of the goblins of the वीरभद्र hill, which held the answer for all those events. I was hooked and sought to pursue this matters. One day when I had gone to see the "red" girl who behaved like a bartender, I stumbled upon MJ that mistress of apabhramsha who lived beside her in an opulent vihAra. MJ, who as you know mastered the lore of भूतडामर, showed me the tangled skein that connected the fallen of the forsaken one, with the capture of Vidrum. I followed the yarn further and found the trap that was waiting for ब्रिन्दासुत. You know well that I am the keeper of spiders. Ponder Oh वाजरत्नायन : Why did the पूषण् spell of the ancient atri bring up the venomous scorpion? Why did the car sent forth by the 10 sisters in aditi's lap not reach you? Why was the muni destroyed completely in the battle of दशानन्तर?\
I: The one who drinks the draught of soma in his head becomes amartya.


