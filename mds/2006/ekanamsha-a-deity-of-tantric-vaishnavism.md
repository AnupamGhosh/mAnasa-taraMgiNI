
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [एकानम्शा: a deity of tantric vaiShNavism](https://manasataramgini.wordpress.com/2006/02/04/ekanamsha-a-deity-of-tantric-vaishnavism/){rel="bookmark"} {#एकनमश-a-deity-of-tantric-vaishnavism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 4, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/04/ekanamsha-a-deity-of-tantric-vaishnavism/ "6:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/एकानम्शा.0.jpg){width="75%"}
```{=latex}
\end{center}
```



The core pantheon of the सात्त्वत branch of the पाञ्चरात्र texts is the chaturvyuha or the serial emanations of नारायण. The primary manifestation of नारायण is vAsudeva, from whom emerge संकर्षण and pradyumna. From pradyumna emerges aniruddha. This pantheon appears to be a unique feature of the yadu tribes (the सात्त्वत-s of the late vedic texts) that subsequently seems to have spawned the classical पाञ्चरात्र. However, the early texts suggest that there was another parallel system that appears to have largely passed out of subsequent vaiShNavism. That was the cult of the goddess [एकानम्शा]{style="font-weight:bold;font-style:italic;color:#3366ff;"}. Who is she ?

[नमस्ते ऽस्तु एकानम्शायै । योगकन्यायै नमो ऽस्तुते ।]{style="color:#0000ff;"}\
[विष्णोर् शरीरजाम् निद्रां विष्णोर् निर्देशकारिणीं । विवेश रूपिणी काली काल-मेघावगुण्ठिता । काली च कामरूपिणि सर्वहरस्य सखी तथ । महेन्द्र-विष्णु-भगिनी कालरात्रि च मोहिनि ।]{style="color:#0000ff;"}

Salutations be to एकानम्शा, salutation to the योगकन्या. She is Sleep, born of विष्णु's body and carries out the instructions of विष्णु. Of diverse forms, of the form of time, she is enveloped in black clouds. She is काली, able to assume any form, and the friend of the destroyer of all. She is the sister of the great indra and विष्णु, the night of total destruction between kalpas and the deluderess.

[Mythology of एकानम्शा]{style="font-weight:bold;font-style:italic;color:#ff0000;"}

In the days of yore deva विष्णु in the form of the terrifying नृसिंह had rent apart the entrails of the great daitya हिरण्यकशिपु at his own doorstead, after emerging from a pillar. After that विष्णु of a thousand heads retired to his eternal loka in the एकार्णव, where he lay in a deep slumber on the coils of the snake of time. But his sleep was not to last for long --- the leadership of the demons had passed on to the दानव कालनेमी- the wheel of death. The other devas came to him to seek his aid in the battle against the दानव emperor. From his forehead emerged the beautiful black goddess, एकानम्शा, veiled by a dark nebulosity. Then विष्णु arose and assumed a terrifying black form. His hair filled the skies like a dark nebulosity, his eyes shone like suns at the end of the kalpa and his body was covered with a deep yellow robes. He held in his 8 arms the famed शरङ्ग bow, the nandaka sword, the long-handled mace, the chakra, the लाङ्गल missile and other weapons, and a dagger in his belt. Mounted on तार्क्ष्यो अरिष्टनेमी, with his elder brother mahendra and he and other devas set forth for battle against कालनेमी and his demons. The goddess एकानम्शा proceeded with them to delude the demons with her mAyA.

A fierce battle with the demons ensued. indra fought a demon virochana and his several daityas, वरुण fought हेती, mitra fought प्रहेती, yama fought with कालनाभ the brother of कालनेमी, the ashvins fought with वृषप्रव and his hordes, rudra dealt with the vicious jambha and his armies. The six-headed son of deva rudra fought the terrifying demon क्रोधवास, while सविता fought bANa. indra and the devas used weapons known as bhusundi-s, chakra-s, maces, ऋष्टि-s (cone-headed spears), पट्टिषस् (flexible blades), shakti-s (broad-headed spears), ulmuka-s (fire-throwers), प्रास-s and axes. They shot missiles such as nistrimsha-s, भाल्लस् (broad-headed arrows) and hurled fiery clubs such as, parighas (dome-headed mace), mudgara-s (blade-sided mace), and bhindipala-s (electrical maces). The demons surrounded indra and attacked him with a shower of shakti-s, but wielding his vijaya bow he shot fiery भाल्लस् that destroyed the दानवस्' weapons before they left their hands. The devas started gaining the upper hand, when कालनेमि dispatched उत्कलासुर an expert in mAyA to delude the devas. But एकानंश countering his spells with her own, she attacked him directly. Bending her bow to a circle, she aimed a vastsadanta missile at his car and shattered the axle of his wheel. He jumped out deftly and aimed a शिलीमुख shara (toad-poisoned dart) at her. But she intercepted it in mid-air with an अङ्जलिक missile and pierced his palms with the painful naracha arrows. As a result utkala dropped his bow, but he quickly rallied back by hurling a गदा. She parried it her own mace and without giving him a chance pierced his neck with a नारायण missile and killed him.

Seeing him fall, the supporters of कालनेमि, mAlI, सुमाली and मलयवान् charged at विष्णु with their गदास् upraised. But garuDa flew deftly avoiding their strikes and विष्णु hurled his chakra and beheaded all three of them. कालनेमी himself charged at विष्णु with a hail of arrows, which विष्णु countered with a continous stream of bearded missiles from his शरङ्ग bow. Finally with an ardhachandrabha missile he cut in twain the bow of कालनेमी. कालनेमी now mounted on his lion, rushed with a tomara, shaktI, प्राश and a ऋष्टि. विष्णु for long parried the blows from these weapons with the लाङ्गल and finally destroyed them. कालनेमी hurled his final weapon, the terrifying त्रिशूल at विष्णु. But विष्णु deftly avoiding the strike, caught the weapon himself and hurled it back at कालनेमी. Unable to bear the त्रिशूल, the दानव and his वाहन were burnt to ashes by it.

Though it appeared that कालनेमी was dead towards the end of the ख़ृतयुग he was to re-appear before the beginning of the downward turn of the wheel time (the age of kali) as the evil king of the bhojas, कंस. There were six powerful दानवस्, grandsons of हिरण्यकशिपु, who threatened the world with their invincibility. The all-knowing विष्णु, entered them in a dream and stole their प्राणस् an kept them in the care of एकानम्शा for future use. Aeons later when कालनेमी reincarnated as कंस, विष्णु was asked by the ब्राह्मणस् and पृथिवि to intercede and save them from the दुष्ट क्षत्रियस्. विष्णु decided to duely incarnate and slay कंस, but this intelligence was leaked by नारद to कंस. So विष्णु directed एकानंशा to place those 6 दानव प्रानस् she had maintained in suspension in the womb of देवकी while she was in prison. Each time she gave birth कंस killed the child, thus riding the world of the 6 दानवस्. On the seventh occasion she brought the काल-sarpa or the संकर्षण as an embryo and placed it in the womb of रोहिणि, while she put the original embryo in रोहिणि's womb into देवकी's womb, where it miscarried. Then she converted herself to an embryo and entered यशोदा's womb. Finally, when vasudeva exchanged his child with that of यशोदा, he had actually brought back एकानंशा. When कंस tries to kill her, she slips from his hand flies to the sky and from there announces that his future killer still lives.

एकानम्शा is said to have been then sent by indra to live amongst the yadus, where vasudeva brought her up with great care. कृष्ण and balabhadra are said to have embraced her after returning to द्वारवती from their campaigns. She is said to have stood between संकर्षण and vAsudeva when she was worshiped by the yadus.

The हरिवंश then gives detailed account of the associated vaiShNava theology of the यादवस्: विष्णु is the axis of time. The time coils around him in the form of the काल-sarpa, shesha. It is this terrible snake that manifests as संकर्षण. At the end of the kalpa the snake spews flaming venom that destroys the loka, the main duty of संकर्षण. The निद्रा of विष्णु is the goddess एकानंशा, who manifests as the dark nights, the कालरात्रि-s between the kalpa. She is the friend of सम्कर्षण and envelops the world in darkness while he re-absorbs it. As the world manifests its components constantly keep emerging or return to विष्णु borne by the flaming waves of his breath. एकानंशा as the योगिनी, consumes all the entities that return. Every year, she seizes विष्णु with a sleep in the rainy season, when indra takes over his place as the supreme being and nourishes the world \[comparable of महामाय of the मार्कण्देय पुराण, who withdraws from विष्णु allowing him to kill madhu and कैटभ. In the kula tantric parlance she is स्वप्नवाराही, the goddess of yoganidra]. She is both the sister of indra and विष्णु and manifests as कात्यायनि or दुर्गा on the vindhya mountains where her principal shrine is present. Thus, the holy triad of संकर्षण, एकानंशा and नारायण was seen by the parama-bhAgavata अक्रूर. Her shrines are said to be all over भारतवर्श where she is offered sacrificial offerings of meat and wine.

[Historical worship of एकनम्शा]{style="font-weight:bold;font-style:italic;color:#ff9900;"}\
The astronomer वराहमिहिर recommends (बृहत् saMhitA, chapter 58) that temples should contain triads of संकर्षण holding the musala known as sunanda, a हलायुध and a स्थूणकर्ण weapon in his four hands. Then there should be एकानंशा and finally नारायण with a chakra, gada, शङ्ख and lotus. एकानंश may be of a two, four or eight handed form. In the two handed form she holds a lotus and one hand is on the hip. In the 4 handed form she holds a book, lotus and rosary. In the eight handed form she has a bow, arrow, lotus, mirror, book, rosary and abhaya-varada gestures. The two handed form is recommended by the Agama section of the विष्णुधर्मोत्तर (3.85) and the tamil text the परिपाटल् mentions that she should be flanked by संकर्षण and vAsudeva.

  - एकानंशा appears in coins of the provincial governors of the शुङ्गस् (e.g. the Besnagar silver coins from Vidisha) and also the yavanas like Agathocles around 180 BC. Subsequently a feudatory of the काण्वस्, वीरसेन, also depicts her on his coins. A possible late-maurya-शुङ्ग depiction of her with baladeva and vAsudeva is seen in sketch by a certain dabuka on the walls of a cave at Tikla.

  - The कुशानस्, vima khadphises, vima takto and kanishka built several temples of the सम्कर्षण-एकानंशा-नारायण triad idols in red sandstone with two armed एकानंशा-s between 50-200 AD. vima takto also built a pentad temple with all five deities including, the pradyumna and the aniruddha.

  - नृसिम्हगुप्त बालादित्य, the destroyer of the हूनस्, built a temple for the triad in magadha.

  - cave temple 27 of Ellora houses the triad

  - - The great rAja bhoja built a temple for the triad in Etah.

After that the deity appear to have vanished from the later traditions of vaiShNavism. The original tradition may be linked to सप्तरात्र rather than the parallel पाञ्चरत्र tradition that remained dominant in the later period.

I was prompted to investigate एकानम्शा in the connection of the origin of the विध्यावासिनि दुर्गा cult due to the photographic collection presented by NP Joshi in his Hindi pamphet entitled: देवी एकानंशा ki कुशाण कालीन् मूर्तियान्.


