
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The split forces and the प्रलाप](https://manasataramgini.wordpress.com/2006/09/09/the-split-forces-and-the-pralapa/){rel="bookmark"} {#the-split-forces-and-the-परलप .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 9, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/09/the-split-forces-and-the-pralapa/ "6:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The muni and me were in a sense like branches of the same tree. We wondered what might have happened if only the muni and me could have joined our forces on the battle field. Our coordination on the field was unmatched. We saw eye-to-eye in the conduct of campaigns and the like. The only difference was that we matured at different times. I grabbed the विद्यास् when they had to be acquired, but the muni ambulated in a random walk for long before he achieved them. This phase-shifted me and him and the locking of the वृषभ-शृङ्ग व्यूह never happened.\
...o...o...o...

After the forces we despatched under the command of GS had conquered the शुल्बारी daNDa and जीवमूलिय पञ्चकोण, we went to examine the spoils of the campaign. We examined it and were happy with it. Then we deployed कूटनीति to beseige the subtle द्वयं daNDa and सूत्रान्त and acquire an opening. Then we attacked both fronts simultaneously and captured them- both spectacular victories. Our young सेनानी also marched close to the fort of कांस्य vastra and acquired an advantageous position. It appeared that we were in the midst of completing the great conquests and were exultant in the sweet abhisheka of yasha. But at the back of our mind we knew that the workings of fate were not going to let us enjoy this too long. Suddenly, a new कृत्या, trouble some as the one which we had repulsed earlier with the mantras of the terrible विनायक came out of the hiding. Our शचीव and अमात्य told us that they were eager to strike against our shatru but we told them that timing was critical as they had botched up recent attempts. The कृत्या came through the open door like the very कृत्या that had sucked the ancient mighty clansman of ours into her योनी before he was destroyed by rudra. Over the night the कृत्या attacked us repeatedly and diminished our strength. We did not whether to directly attack the कृत्याकृत्-s or not. We then decided that we had to embark on new prayoga-s.


