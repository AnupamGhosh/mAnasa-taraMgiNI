
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The chakras of विष्णु](https://manasataramgini.wordpress.com/2006/02/14/the-chakras-of-vishnu/){rel="bookmark"} {#the-chakras-of-वषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 14, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/14/the-chakras-of-vishnu/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/sudarshana.jpg){width="75%"}
```{=latex}
\end{center}
```



From विष्णु emanate several chakra missiles, all of which may follow the deadly sudarshana and discs when used in battle against the दानवस् and राक्षसस्. They represent the sum of the deadly destructive fury of विष्णु. Likewise, the सम्पूर्ण chakra prayoga rite invokes these weapons to power the countering or attacking mantra missiles.\
The following are the chakras:

 1.  महारात्रिधर chakra: This powered by महामाया and is dark hued and terrifying with a single spoke.

 2.  उषश्-chakra: This is powered by वायु and agni and is of the color of dawn and with two spokes

 3.  udaya chakra: This is powered by the fury of the world-destroying venom of संकर्षण. It is the 3 spoked wheel

 4.  aishvarya chakra: This is powered by the all-prosperous pradyumna and has 4 spokes.

 5.  शक्तिमहाचक्र: This is powered by the blazing aniruddha and has 5 spokes

 6.  षडर-chakra: This is powered by keshava and has 6-spokes. It comes out from the sudarshana disc.

 7.  महासुदर्शन chakra: This is powered by पद्मनाभ and has 12 spokes. It is the destroyer of the asuras and the fiery atri दुर्वास.

 8.  सहस्रार chakra: This is the wheel powered विष्णु manifesting in the parame vyoman. It blazes with a 1000 spokes with all chakras, terrible शूलस्, musalas, हलायुधस्, गदास्, प्रास and other weapons surrounding it.


