
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The first circle](https://manasataramgini.wordpress.com/2006/05/07/the-first-circle/){rel="bookmark"} {#the-first-circle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 7, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/07/the-first-circle/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There we moved swiftly on our वाहन and placed it on the stalls. The two शूद्र-s कृष्णमुख and शूद्र-शिरोमणि met us at the अश्वशाल where we kept our horses. One of the शूद्र-s asked, O foreign brahmin, should we participate in a vigorous game of कन्दूक कृड in the yonder kandukaprastha that lies beyond the nAga-bila and at the foot hills of the वानर parvata. The other शूद्र said: this place is blessed with many women who are the handiworks of manmatha, yet why is it we are cursed to wander in these uninteresting assemblages with nothing more of interest than the aforesaid कॄड. Soon some others joined in and asked who among the young women was the greatest of the works of manmatha-कला. Just then madana's companion, the malaya-वात wafted bearing the fresh odors of the shaila-पुष्प-s captivating the minds with the presence of the wielder of the इक्षु-चाप. There was considerable unanimity in the assembly in picking one of the foremost of the स्त्री-चूडामणि-s. They described that one whose voice was like the one who is brought up in another's nest, whose tvacha was like the light of the star Ardra, whose eyes were like those of a तित्तिरी, whose each वक्षस् was like the सुवर्ण kalasha held by the awful विनायक. We knew this कांइनि better than the rest and uttered silently to ourselves the statement: "She said: "गौरी rests in [her abode]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"} guarded by nandin" ". We cannot talk more of this सखी, for in the world of men, people can connect the dots. Then they moved on to describe that other mohini on whom they were unanimous. Firstly, they declared she was unusually a तीक्ष्ण tri-patha स्त्री, while most स्त्री-s were komala tripatha-s or chatush-patha-s. Our curiousity was greatly roused regarding this स्त्री-ratna, so we asked our priya-सखी to lead us to her.

She was guarded by the [नभस्]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}[, ]{style="color:rgb(255,0,0);"}which we surveyed carefully and only after that could we sight her. She was definitely a trailokyasundari, the like of which one rarely encounters. She was to the eyes, what ghee is to jatavedas, with all the accourtments of ratipati in her- her blue- black locks the monsoonal clouds blown by the malaya-marut and her lips with the teeth his flag, and her सुश्रोणि his rathachakras. We wondered like उपहारवर्मन् in दण्डिन्'s novel as to what we were witnessing... Here ends the first circle.


