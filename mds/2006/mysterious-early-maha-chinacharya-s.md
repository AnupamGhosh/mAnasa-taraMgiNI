
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Mysterious early mahA-चीनाचार्य-s](https://manasataramgini.wordpress.com/2006/04/30/mysterious-early-maha-chinacharya-s/){rel="bookmark"} {#mysterious-early-maha-चनचरय-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 30, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/30/mysterious-early-maha-chinacharya-s/ "4:08 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is clear that the चीनस् have had contact with the Indians over a long period and have imbibed various forms of Hindu thought. It is also well-known that the bauddha influence on the chIna-s was profound from the early phase of the foreign expansion of the bauddha-mata. Especially, the emphasis on the desha-भाष as against the deva-भाष in the transmission of the early bauddha doctrine resulted in a profound change amongst the chIna-s, which corressponded to the shift from the peculiar literary chIna-भाश to the colloquial chIna-भाष, with incorporation of indic terminology in translation. We distinguish the spread of mahA-चीनाचार, from the early layer of bauddha spread, because unlike that layer, it used संस्कृतं as the main vehicle of transmission. While the chInA-s and their Indian teachers definitely used Chinese translations for explaining the texts to the native chIna-s, Sanskrit with its पणिनिअन् grammar was the principle ritual language. Secondly चीनाचार despite being borne in the garb of the nAstIka bauddha-mata principally focussed on Hindu deities and ritual beneath this garb. These features along with the biographical allusions regarding चीनाचार्यस् from India suggest that they were primarily ब्राह्मण or kShatriya scholars who had fallen to the nAstIka mata and carried their internal tantric tradition relatively unchanged, just like नागार्जुन, the nAstIka savant. This basic pattern lead me to investigate the issue of the earliest texts of mahA-चीनाचार (in the middle China) and the upa-चीनाचार-s (not of so much of later Tibet, but those of the Turko-Mongol khaganates of Central Asia).

From the muni's discussions with the vaiShNava who had journeyed beyond the hallowed ओड्डियाण पीठ along उत्तरापथ, I was made aware of one of the earliest texts of mahA-चीनाचार. चीनस् have reasonably recorded its history, but much still remains shrouded in mystery for the Sanskrit original of the tantric manual has been lost in India. It was composed by ब्राह्मण named in chIna-भाष as Chu Lue-yen. This brahmin was from Ujjain in India, and journeyed to China via the Hun khanate in central Asia in 230 CE; we do not know his Sanskrit name. He compiled a tantric manual in Sanskrit and also translated into chIna-भाष for the benefit of his students there. Only this translation/transliteration survives and is known by the chIna name: Mo-teng-ch'ieh ching. A quick examination of this text restored into Sanskrit by the learned vaiShNava showed that it has 2 parts. The first is the नक्षत्र krama विधानं, which provides a relatively simple vidhi for the worship of the नक्षत्र देवता-s and some oblations to them. The second part is the ShaD-mahA-mantra विधानं, which describes 6 mantra-s like those found in the उद्दामरेश्वर tantra, begining with the प्रणव and having agni-jArA as the terminal. It describes the vidhi for a version of the तन्त्रीक अग्निमुखं differing in details from the commonly prescribed one as per the bhargava tradition. The 6 mantra-s are used in offering oblations in the prescribed homa.

Between 290-300 CE a ब्राह्मण, only known by his chIna name, Fo-tu-ch'eng moved from Northern India to the Central Asian khanate of the Huns, early ancestors of the Chingizid Mongols of the later era. It was just at this time that the Shanyue of the Huns, Khan Ruhan (known in chIna translation as Liu Yuan) was uniting his forces for a huge expansionist campaign on the steppes. The ब्राह्मण Fo-tu-ch'eng, who was said to be an expert at the भूत tantras, dUra-दृष्टि-prayoga and mahendra-जाल tantra, arrived at the khan's camp and was received with much respect and honor. He seems to have left an immense impression on the चीनस् and हूनस्, who describe in amazement how he used mantras-s that deployed various यक्षिणि-s, चेटक-s, शाकिनिस् and डाकिनि-s as he wished. His dUra-दृष्टि prayoga has also been described: he is said to have smeared his palm with an आङ्जन and with an incantation seen things on it happening several yojana-s away. He is also said to have had a घण्ट prayoga, where he could use bells sounding in shrines to carry out prashna-s. The china-s and हून-s declare that there was never any one else who had his mantra siddhi-s ever amongst चीनाचार्य-s of any later age. He is said to have told the Hun Khan and his son the positions of their enemy armies with his prayoga-s. Most of his compendium of tantric formulae is now lost both in its original Sanskrit and Chinese translation.


