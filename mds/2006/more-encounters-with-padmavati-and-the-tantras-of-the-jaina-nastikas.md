
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [More encounters with पद्मावती and the tantras of the jaina नास्तिकस्](https://manasataramgini.wordpress.com/2006/12/20/more-encounters-with-padmavati-and-the-tantras-of-the-jaina-nastikas/){rel="bookmark"} {#more-encounters-with-पदमवत-and-the-tantras-of-the-jaina-नसतकस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 20, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/20/more-encounters-with-padmavati-and-the-tantras-of-the-jaina-nastikas/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RYjoXe6AktI/AAAAAAAAAAM/ZAQDBiDjp9A/s320/padubai.jpg){width="75%"}
```{=latex}
\end{center}
```



Continuing with our encounters with padubai (पद्मावती), we come to that expedition that opened to us the secrets of this deity. The our nआस्तीक astronomer acquaintance of the 3rd वर्ण had mentioned that there was a large idol of पद्मावती in the depression to the south of our city where also lay an old temple of rudra. That leader amongst the शूद्र-s told us right then that he recalled that there also lay an old aniconic rock of padubai. We desired to see this site and set out with our henchmen of the 3rd and 4th वर्ण-s. We rode along on our "ashva-s" and reached the said hamlet where वीरकल्स् of old cattle herders, including perhaps the ancestors of the Bhosles lay scattered. There शूद्र-श्रेष्ठ led us to a large scarlet-painted rock that was worshiped extensively by the local tribesmen and cowherds as padubai. The वैष्य pointed us to a ruined shrine where there was a large exquisite rock image of पद्मावती, which he claimed was installed by a jinendra named pradyumna-सूरि. We noted that on the pedestal of the image of the देवी was inscribed a peculiar mantra:\
[ॐ ह्रीं नमोऽस्तु भगवति पद्मावती एहि एहि सं वौषट् ॥]{style="font-weight:bold;font-style:italic;color:rgb(255, 153, 0);"}\
There was something about the icon --- one could feel the great goddess with many arms, holding many weapons inspire a sense of awe. I had the same feeling as what I felt only in the presence of another old idol of the great मन्त्रिनी, who is सन्गीत-योगिनी, at some other point in time. I kept this in mind and was to learn more of it some months later.

Both me and the muni had enjoyed the excellent hospitality of a remarkable jinendra during our expeditions to the north of the city of my birth in the कालामुख country. This jinendra was remarkable in that he doubled both as priest of temple of rudra and as an आचार्य of the nआस्तीक-s! As a result he was familiar with both traditions and was free from the rancor that characterized the medieval nAstika मन्त्रवादिन्-s who fought their fierce अभिचार battles with आस्तीकस्, bauddha नास्तीकस् and their digambara nआस्तीक cousins. It is well-known that that the jaina नास्तीकस् were tantrics right from their early days- the jaina hero kovalan of the draviDa epic शिलपधिकारं is described as performing different mantra prayogas. But their mantra-शास्त्र has remained more hidden than the famous activities of their bauddha rivals. One evening after an exploration we were returning to the house of the jinendra, when we noted him performing a homa with several mantras. This piqued our interest and we made inquiries and he was kind enough to reveal to us over some time period the details of the nआस्तीक मन्त्रशास्त्र. He was himself in the parampara of the following great nआस्तीक prayoga adepts who attained siddhi of the mantras of our goddess प्रत्यङ्गिरा as per the nआस्तीक traditions. Each one of them had composed tantric kalpa-s on the prayogas of श्री प्रत्यङ्गिरा देवी: 1) the ShaT-karma master सागर-chandra-सूरि 2) बालचन्द्र-सूरि 3) pradyumna-सूरि 4) devabhadra. Of great interest was the fact that नास्तीकस् admitted possessing a class of mantras termed आथर्वणी from which stem the प्रत्यङ्गिरा prayogas.

The jinendra explained that some of the main prayogas of great importance (other than प्रत्यङ्गिरा) were those of पद्मावती, अम्बिका and सरवती. But he also possessed some other really tantalizing विद्यास्: 1) शैवागमोक्त पद्मावती साधन with prayogas known as haMsa पद्मावती, mokSha-पद्मावती, rakta-पद्मावती and शबरी पद्मावती 2) कामेश्वरी mantra 3) भैरवी mantra 4) नित्या mantra 5) त्रिपुरा mantra 6) चक्रेश्वरी mantra 7) कुरुकुल्ला mantra 8) बटुक bhairava prayoga 9) स्वर्णाकर्षण bhairava prayoga 10) मृत्युञ्-jaya prayoga.\
The jaina forms of these prayogas are not identical but related to their आस्तीक counterparts in most cases. The jinendra explained that in the पद्मावती kalpa that he followed for his worship she is termed: त्वरिता, नित्या, त्रिपुरा, त्रिपुरभैरवी and कामसाधिनी. One of the prayogas for stambhana in the kalpa, with which an medieval jaina siddha had destroyed a Sufi subversionist invoked वार्ताली-वाराही. The mantra used in the kalpa for escaping ordeal inflicted by rulers is the rAja-श्यामला or सन्गीत-योगिनी mantra. The नित्यक्लिन्ना prayoga is provided for bewitching women. विद्या-s are also provided for chandeshvara, garuDa (for countering snakes) and कुरुकुल्ला.


