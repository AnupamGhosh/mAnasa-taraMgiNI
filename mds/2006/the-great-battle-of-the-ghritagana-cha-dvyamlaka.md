
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The great battle of the घृतगण cha द्व्याम्लक](https://manasataramgini.wordpress.com/2006/06/21/the-great-battle-of-the-ghritagana-cha-dvyamlaka/){rel="bookmark"} {#the-great-battle-of-the-घतगण-cha-दवयमलक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/21/the-great-battle-of-the-ghritagana-cha-dvyamlaka/ "4:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While we were still being relentless hammered by the shiva-युवती of महारुद्र, we decided to attend to the long-standing battle where the young yoddha was faltering. It was the glorious quest of what was code-named: "घृतगण cha द्व्याम्लक", a high prize that I had earned for since my earliest days in the सेनामुख, in the halcyon teens, filled with youthful intemperance. Now as a veteran of many bloody wars in which the devas had seen us through, I was still being rebuffed on that front. But then I remembered that R had prophesized long back that the path would not be easy. We called that most attractive jewel of the स्त्री-loka, Hayasthanika to aid us in the battle. While Hayasthanika in all due fairness was not capable of wielding the heaviest of weapons in the सेनामुख, she definitely had a whole array of skills that like those of सुशेण of the वानरस् that could be useful in completion of certain battles. We were hard-pressed by the राक्षसि that was dispatched against us, and she was threatening us with a chakra. Our young yoddha, however made a critical incision in the ranks with his excellent fighting early in the day. We had only a few hours to settle the battle and were simply not finding touch. Then we straightened our arm and lifted the bow invoking the deva-s to guide our astra-s. We fired a series of arrows into the enemy chinks, even as our young yoddha strategically made use of Hayasthanika's charms. I noticed however that they were faltering late in the day under our shatru's counter-attack and felt they might not last long. Our yoddha was in disarray himself seeing Hayasthanika on the retreat. It was at that point that we struck with series of rapid thrusts into the enemy ranks. Suddenly, by कार्तिकेय's aid the shatru was exposed and we decapitated him. Our flag flew over "घृतगण cha द्व्याम्लक". We beheld Hayasthanika in the blaze of victory: her sight was verily like the nectar flowing from the heavenly soma cup; her charms like the octet of mayura. The brief respite it gave from the hammering of the adverse edge of the downward-turning chakra of the kali age was like the single rain shower in the Namibian desert.


