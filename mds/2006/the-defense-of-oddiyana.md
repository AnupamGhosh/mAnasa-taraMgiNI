
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The defense of ओड्डियान](https://manasataramgini.wordpress.com/2006/06/05/the-defense-of-oddiyana/){rel="bookmark"} {#the-defense-of-ओडडयन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/05/the-defense-of-oddiyana/ "5:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The famed पीठ of ओड्डियान on the earthly plane, was the in kingdom of ओड्डियान, which was ruled by the clansmen of shubhakara-सिंह. The last days of this glorious पीठ under the constant assault of the Mohammedans poorly known, but we can piece together some details from the Chinese chronicles and the राजतरङ्गिणि of कल्हण. The heroic Hindu resistance in this region is largely forgotten but we provide an outline, which we hope to fill in the future.

  - In 670 CE the first wave of Jihadis reached close to Kabul (कूभ) but were repulsed.

  - In 710 CE the Mohammedans attacked the northern zone of the region associated with ओड्डियान, the province of Kapisa. This was an early full-fledged attack on Hindu territory, a few years before the invasion of Multan (मूलस्थान) by Mohammed ibn Qasim. The Moslem army was however beaten back by a special defense force instituted by the Hindus to protect Kapisa.

  - 713 CE, चन्द्रपीड, the कार्कोटक king of Kashmir defeated a Moslem attempt to invade Kashmir.

  - 713 CE ओड्डियान attacked by Moslems but they are repulsed.

  - 715 CE Arab and Tibetans forming an alliance invaded Ferghana and took it. The Chinese, Hindu and Zoroasterian alliance fought an 8 hour battle to defeat the Arabs. The Chinese massacred 1000 Arabs as they fell into their hands.

  - 715 CE Jihadis pour into Kashgar and ravage it completely.

  - 720 CE Ghazis attack ओड्डियान again. Young shubhakara सिंह defeats Moslem invaders in the great battle of ओड्डियान.

  - 741 CE Moslems launch another invasion of ओड्डियान- they are defeated again.

The Moslems are described as wearing black turbans like their successors the modern Taleban.


