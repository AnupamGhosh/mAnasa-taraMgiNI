
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The beginning of भार्गव tradition of भूत-DAmara](https://manasataramgini.wordpress.com/2006/11/13/the-beginning-of-bhargava-tradition-of-bhuta-damara/){rel="bookmark"} {#the-beginning-of-भरगव-tradition-of-भत-damara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 13, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/13/the-beginning-of-bhargava-tradition-of-bhuta-damara/ "5:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The preliminary section of the भूतडामरं with the vajra-मण्डल mantras and the सर्वापरस prayoga appear to be a distinct module that was acquired secondarily by current भूतडामरं. The beginning of the core section of the ancestral भार्गव tradition of the भूतडामरं and उन्मत्तभैरवं that was obtained from नीलरुद्र begins with the following rites and mantras. Their connections to the older atharvan tradition is reasonably transparent.

  - The section begins by saying that one must proceed to procure ओषधि-s (technically termed ओषधि ग्रहण vidhi by the भार्गव-s and अङ्गिरसस्) for medical and ritual use only by applying the appropriate mantra-s otherwise, instead of a भेषज one merely gets a pile of wood.

  - It advised that plants growing on termite nests, wells, at the base of shady trees, on chariot roads, groves of gods or graveyards be avoided for collection.

  - Plants that are damaged by fire, water or parasitic infestations, unseasonally growing, or trampled to the ground should be avoided. Plants should be collected only in the prescribed amount required for the user.

  - One gets up in the morning and worships rudra surrounded by his hordes of भूत-s. Then with worshipful attitude to The god one set forth for collection.

  - Going to the plant that one wishes to obtain one utters the spell:\
[OM वेतालाश्cअ पिषाचाश्च राक्षसाश्च सरीसृपा: |]{style="font-weight:bold;"}\
[apa sarpantu te sarve वृक्षादस्माच्छिवाज्ञया ||]{style="font-weight:bold;"}\
One thus invokes rudra to command the वेताल-s, पिशाच-s, राक्षसस् and reptiles to crawl away from the plant.\
Then one invokes the ओषधि with the following formula:\
[OM namaste .अमृतसंभूते bala-वीर्य-vivardhini |]{style="font-weight:bold;"}\
[बलमायुष्च me dehi पापान्मे त्राहि दूरतः ||]{style="font-weight:bold;"}\
The herbs, the descendents of soma, are asked to provide strength, fertility and long life and remove pApa-s

Then thinking of the mAyA and प्रत्यञ्गिरा bIja-s one utters the following incantation:\
[yena त्वां khanate brahmA yena त्वां khanate भृगु | yena indro.atha वरुणो yena त्वां upachakrame | तेनाहं खनिष्यामि mantra पूतेन पाणिना ||]{style="font-weight:bold;"}\
[mA te pAte मानिपाति mA te tejo.अन्यथाभवेत् |]{style="font-weight:bold;"}\
[atraiva तिष्ठ कल्याणि mama कार्यकरी bhava ||]{style="font-weight:bold;"}\
[OM ह्रीं क्ष्रौं स्वाहा | anena मूलिकां chedayet ||]{style="font-weight:bold;"}

Thus, as भृगु and brahman priest had dug herbs, as indra and वरुण had incised herbs thus purifying with mantras one collects the good herbs for the achievement of ones tasks.

The regular वशिकरण herbs are said to be: अपामार्ग, gorochana (a lump of taurine from cow's gall bladder), दण्डोत्पल roots, श्वेतापरजित roots, इन्दिवार lotus roots, banyan fig roots. विश्वामित्र is said to have expounded the वशिकरण chapter of the tantra.

An interesting receipe is given for destruction of lice and mites: The विडङ्ग and gandhotpala plants are extracted using gomutra. Then the extract is heated and colloidized with castor oil. This preparation is applied on the hair to destroy external parasites.

For alopecia the following is recommended: The hair is first treated with an extract of the javA flower in गोमूत्र. Then turmeric treated with alkali is mixed with equal amount of powder of the maricha root. Then the mixture is extracted with hot oil. The extract is mixed with the juice of the bimba flower and the resultant paste is applied on the head.

It also gives a long version of the महिषमर्दिनि formula used for agni-stambhana prayoga:\
OM ह्रीं महिषमर्दिनि laha laha hala hala kaTha kaTha stambhaya stambhaya अग्निं स्वाहा ||


