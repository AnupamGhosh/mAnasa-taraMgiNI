
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कुमार माल](https://manasataramgini.wordpress.com/2006/10/17/kumara-mala/){rel="bookmark"} {#कमर-मल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/17/kumara-mala/ "6:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/kaumAra_माल.jpg){width="75%"}
```{=latex}
\end{center}
```



