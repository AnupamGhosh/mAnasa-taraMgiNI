
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Indo-Aryans of the Middle East](https://manasataramgini.wordpress.com/2006/07/11/indo-aryans-of-the-middle-east/){rel="bookmark"} {#indo-aryans-of-the-middle-east .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/11/indo-aryans-of-the-middle-east/ "4:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Indo-Aryans of the second millenium Middle East have been a topic of continuing interest highlighted beginning with the works of Johannes Friedrich in 1929. Subsequently, Mironov and PE Dumont have also expanded upon this subject. These studies are of key importance to the Aryan problem, which may be briefly described as the question of time and place of origin of the Indo-Aryan languages of India and its speakers. In my opinion the traces of Indo-Aryan in the Middle East are indisputable; however, it is clear that only words and no complete Indo-Aryan language surives in these regions.The chief traces of IA in the Middle-East are the famous horse training manual of Kikkuli the Mitannian in the land of the Hittites, the Hittite-Mitanni treaty, and various texts from Syria, Nuzi and Mitanni documents. From the Indo-Aryan names of the royalty and the presence in the specialized horse-training manual it is suggestive that the Indo-Aryan population was relatively restricted to a mobile equestrian elite. The words appears to be a superstrate on the Hurrian (an old Asiatic language of possible Caucasian affinities) the language of the original people of Syria and the main consitituents of the Mitanni Kingdom.

[deities directly mentioned:]{style="font-weight:bold;"} mitra, वरुण, नासत्य-s, indra (Hittite-Mitanni treaty), lord of vasukhanni (mine of wealth?)\
[deities inferred from personal names:]{style="font-weight:bold;"} स्वर्दात (suvar= dyaus), indrota (indra), वायव (वायु), वसुदात (vasu-s); viryasoma (soma).\
[Horse/chariot-related names:]{style="font-weight:bold;"} वृद्धाश्व, त्विष्रथ, वार्धाश्व, ashvasani, yuga, vartana, marya\
[Numbers:]{style="font-weight:bold;"} aika, tera, pancha, satta, nava\
[colors:]{style="font-weight:bold;"} pingar\[l\]a, babhru, par\[l\]ita\
[vira/शूर-based names:]{style="font-weight:bold;"} वीरसेन, वीर्य, वीर्यसोम वीर्यवाज, वीर्यशूर, वीरजन, अवशूर, kal\[r]मशूर, क्षेमशूर.\
[वाज-based names:]{style="font-weight:bold;"} विर्यवाज, सत्तवाज, वाजिन्, मतिवाज.\
[ऋत based names: ]{style="font-weight:bold;"}अर्तधामन्, artamna, अर्तमान्य.

It is clear from whatever evidence we have that the Indo-Aryan speakers bringing this superstrate to the middle east: 1) were speakers of a language close to the vedic language. 2) Followed an IE religion close to vedic 3) were a heroic/military group rather than traders or pure ritualist priest (as against warrior-priests), as suggested by the numerous वाज, vIra/शूर based names. 4) Were highly equestrian in disposition (The Hittite ascendency can be directly linked to adoption of IA horse training techniques like that of Kikkuli. Modern day experiments of Kikkuli's method show that it is an effective 7 month training program for war-horses).

These traces of IA are very important because they form the only clearly attested archaeological evidence for an Indo-Aryan language prior to the Brahmi inscriptions in India. Clearly, the form of IA language in these texts is closer to Vedic rather than the earliest Brahmi inscriptions, providing evidence that a language close to Vedic was spoken as a regular language outside of ritual context around 1600-1200 BCE. Western Indologists interpret these traces of IA as a certain sign that the Rig Veda must be contemporaneous. They also see this is a certain sign that from the Central Asian homeland the vedic invasion of outer Asia took place in various directions with one branch reaching the middle east and only imposing itself as a superstrate, whereas in India, in the milieu of the dead Indus civilization, a branch was able to establish itself as the principal language of North India.

The Indo-centric authors argue that Middle Eastern IA phenomenon represents a symmetric westward outflow of IA warriors from an IVC locale. This mirrored the movements of IA within India where the archaeologically attested main population movements were from West to east with shift towards Eastern IVC sites and finally to the Ganga-Yamuna doab, and towards the South-West and South to sites in Gujarat and beyond. The correct model remains unclear to me, though I am actually quite willing to accept a secondary dispersal from India, chiefly driven by क्षत्रियस्. I also strongly suspect that this movement of Aryans to the middle east (from wherever it started) was critical in introducing Indo-Aryan philosophical and mathematical concepts of the Upanishadic era to Egypt and eventually Greece.


