
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Pauranic testimony and some time lines in Hindu tradition](https://manasataramgini.wordpress.com/2006/06/11/the-pauranic-testimony-and-some-time-lines-in-hindu-tradition/){rel="bookmark"} {#the-pauranic-testimony-and-some-time-lines-in-hindu-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/11/the-pauranic-testimony-and-some-time-lines-in-hindu-tradition/ "5:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I believe that despite all the noise on the topics the पुराणस् are at this point more enlightening on certain points than all the archaeology of the Indian subcontinent. In fact it appears important that the future archaeological studies in India, if they occur at all, must keep in the mind the Pauranic timelines for poorly known Hindu dynasties. At the outset I must make it clear that some well-known dates contested recently by Hindu historical revisionists are indeed correct. Where the real revision of Hindu dates is required is of an earlier era. The acceptable dates that I find emerging from the पुराणस् are:\
[Western: ]{style="font-style:italic;color:rgb(255,0,0);font-weight:bold;"}Alexander of Macedon, the yavana invader of India= 356-323 BCE. This date is important for it is indeed a sheet anchor intersection between the neo-yavana world and the Hindu world.\
[Hindu: ]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}महावीर the founder of the jaina mata467 BCE\
That is his entire life can be definitely mapped between this period.\
gautama buddha452 BCE\
[]{style="font-family:arial;"}[]{style="font-family:arial;"}chandragupta mauraya 288 BCE\
The nandas of magadha were definitely a major imperial power in eastern India at the time of the invasion of Alexander of Macedon. We can be sure that they came to power at least by 334 BCE and were ousted by chandragupta and kautilya at least by 312 BC.

However, there are corruptions in the पुराणस्. The main amongst these concerns the rise of the neo-magadhan powerof bimbisAra and अजातशत्रु. Obviously this event is well recorded in the bauddha associated vamsha-गाथस्. But we also should recall that important Hindu source, oft neglected as fiction, namely the वासवदत्त romance. Based on this we might conclude that bimbisAra's clan, whose founder was a minister of the बार्हद्रथस्, came to power by murdering the king of that line, arimjaya. The pradyotas in contrast appear to have been rulers of avanti, and might have been late-surviving descendents of the haihayas. In time a descendent of अजातशत्रु, udayin built the city of पाटलीपुत्र. So it is the reign of the bimbisAra dynasty that marks the so called "second urbanization of India". We through hindu source pin this event of udayin's rise with complete certainity between 450-390 BCE. This period indeed does correspond well to the archaeologically attested urbanization and iron smelting in the Magadhan region.

This then leads to the important point that most likely शिशुनाग, a minister of udayin, either overthrew him or son to seize the magadhan throne. Most likely he there after launched a great imperialist campaign of magadhan expansion, in which he conquered the इक्ष्वाकु-s, pradyota-s and vatsa-s. Thus we may picture the neo-magadhan expansion in the following stages: [1) rise under अजातशत्रु 2) conquest of much of north eastern and central India under शिशुनाग 3) reassertion of realm under nandas 4) pan-Indian empire under chandragupta and ashokavardhana 5) last great revival through counter-Helenistic conquests under the brahminical rulers पुष्यमित्र and agnimitra.]{style="font-weight:bold;color:rgb(51,204,0);font-style:italic;"}


