
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Comet C/2006 M4 Swan](https://manasataramgini.wordpress.com/2006/10/16/comet-c2006-m4-swan/){rel="bookmark"} {#comet-c2006-m4-swan .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/16/comet-c2006-m4-swan/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

"[On this day a fairly bright शिखी was seen coursing between the tail of the ऋक्ष and the पशुपाल at the evening time a मुहूर्त after sunset, on कृष्ण नवमी under पुष्य नक्षत्र]{style="font-weight:bold;font-style:italic;color:#ff0000;"}"

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/comet_swan.jpg){width="75%"}
```{=latex}
\end{center}
```



Yesterday, Mn as well as Marc and R mentioned their failure at picking the new comet in the sky. However, today a friend and I sighted C/2006 M4 (Swan) that was just at the fringe of naked eye visibility, close to the boundary of Canes Venatici. Using M92 and M13 as guides we estimated the comet at around magnitude 6.3-6.5. It was easily seen withour 20*70 binoculars and we were able to discern a tail with the 6″ reflector that we were using. On the whole the sky was spectacular from our high observing spot on the hill girt by dense forests and and a "haunted house" in the vicinity. After many days we saw the entirel unimpeded arc of the Milky Way that spanned a complete hemi-circular path in the sky. In the South-Western quarter is spanned Sagittarius, passed through zenith spanning the glorious Cygnus and ended in the North-eastern horizon crossing through Cassiopeia and Perseus. In the South-west dazzled with one of the finest of globular- M22, in the mid-sky on either side of Beta Cygni were the Dumbell nebula and the Ring Nebula. In the North East shone the Double Clusters and M34. M33 the Pinwheel Galaxy and M31 Andromeda were particular good on either side of Gamma Andromedae, the splendid double star, through the binoculars. We also spied the faint companions of the Andromeda Galaxy in Cassiopeia.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/swan_comet2.png){width="75%"}
```{=latex}
\end{center}
```



The comet has typical hyperbolic orbit passing into the inner solar system from below the plane of the ecliptic from its original home in the distant Oort's cloud.


