
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on Shahji and Sambhaji the brother of Shivaji-I](https://manasataramgini.wordpress.com/2006/07/05/some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-i/){rel="bookmark"} {#some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-i .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/05/some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-i/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In my childhood I was quite disappointed with our history books not providing any details what so ever, beyond even the merest mention, about the life and role of Sambhaji the brother of the founder of the Maharatta nation. So for long I wished to write a brief history Sambhaji, the brother of Shivaji, his contributions to the Hindu struggle and his early death on the battlefield. What ever we know of his struggle is mainly from Sanskrit chronicles like those of Paramanand and Jayaram Pandit (Maharatta Brahmin historians and contemporaries), supplemented by bakars and Farmans of the Mohammedans. The core of the narrative here of course follows Paramanand and Jayaram's efforts.

From the reliable genealogies of the Bhosles we known that Babaji Bhosle had two sons, the elder of them Maloji Bhosle being born in 1552 CE. His younger brother was Vithoji Bhosle. Maloji Bhosle and his brother gained some prominence as a restorer of ancient rudra temples that had gone into disuse by repairing them, building tanks for them and supporting worship (गृष्नेश्वर and shambhu महादेव; latter close to a temple built recently in Satara due to Chandrashekarendra Sarasvati, former आचार्य of the Kumbhaghonam मठ). Maloji had two sons Shahji and Sharifji and Vithoji eight sons all of whom fought under Shahji to resist the Mogol invasion of the Nizamshahi Sultanate. Shahji collaborated at first with the talented African slave Maliq Amber, who was sold to the Nizam Shah as a boy. The black warrior was remarkably innovative in his methods and Shahji closely observed him and learnt how to make maximum advantage of the terrain to counter numerically superior enemies with a small force. The Maliq started developing both land armies and a strong navy with fellow black slaves who had been sold to the Ahmednagar court.

Shortly, after Shahji began collaborating with Maliq Ambar, Sambhaji was born to Shahji and Jijabai, and he was named after one of their ancestral shiva temples. Shahji and Jijabai had 6 sons but of them only Sambhaji and Shivaji survived past puberty ([तेषां मध्ये शंभु-शिवौ द्वावेवान्-वय-वर्धनौ]{style="font-style:italic;color:rgb(255,0,0);"} -- they were the middle sons). The African warrior laid a trap for the invading forces of the Mogol Padishah Jahangir and the Adil Shah who was also fighting on their side. Finally, the Mogol and Adili forces were ambushed by Amber in Bhatavadi where Shahji also entered the fight on the side of the black warrior with his brother and cousins. Paramanand's Sanskrit poem on this battle gives a detailed account of the action that took place. The spectacular feats of Shahji in the battle resulted in a rout for the Mogols and their allies. In course of time he conquered the Pune region and made it his land. This also resulted in the jealousy of the African warrior towards him and Shahji wandered away, with both him and his young son Sambhaji entering Adil Shahi court and then the Mogol court when Shah Jahan ascended the throne.

But soon after that the Mogols conquered Devagiri and pressed the Nizam Shah close to extinction. Shahji for the first time made a proto-nationalistic attempt at this point. He with his young son Sambhaji (Shivaji was just born sometime back) decided to become the protector of a puppet Nizam Shah and resist the Mogols. For about 3 years he struggled against the Mogols and the Adil with a force of about 12,000 men that he had assembled along with a renegade Brahmin fighter Murar Jagdev. Shah Jahan was at the pinnacle of his prowess, he had destroyed the Nizam Shah and defeated the Portuguese. The Mogol depredations combined with failed monsoons had triggered famines in Maharashtra depopulating many human settlements. The Mogol Padishaw pressed hard on Shahji and in the early 1630s captured his wife and the young Shivaji. However, they somehow got free after one of Jijabai's relatives bribed a relatively mild Moslem officer who had arrested them. Finally, after much fighting Shah Jahan reduced Shahji's possessions to a mere 5 forts and deputed a force to collaborate with the Adil and the Qutb to destroy Shahji and returned to Delhi. Shahji was finally starved in the siege of Mahuli and was forced to surrender to Randulla Khan, the able Jihadi of the Adil Shah.

[Go to part II](https://manasataramgini.wordpress.com/2006/07/06/some-notes-on-shahji-and-sambhaji-the-brother-of-shivaji-ii/)


