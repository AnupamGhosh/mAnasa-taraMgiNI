
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The praise of rudra](https://manasataramgini.wordpress.com/2006/02/22/the-praise-of-rudra/){rel="bookmark"} {#the-praise-of-rudra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/22/the-praise-of-rudra/ "7:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[००१ नमस् ते देव-देवेश देवारि-बलसूदन ।]{style="color:#0000ff;"}\
[००२ देवेन्द्र-बल-विष्टम्भ देव-दानव-पूजित ॥]{style="color:#0000ff;"}\
Salutations to one who is lord of all the gods to the destroyer of the enemies of devas. One who is the inciter of the strength of the celestial chief. One who is adored by both gods and demons.

[००३ सहस्राक्ष विरूपाक्ष त्र्यक्ष यक्षाधिप-प्रिय ।]{style="color:#0000ff;"}\
[००४ सर्वतः-पाणि-पादान्त सर्वतोक्षिशिरोमुख ॥]{style="color:#0000ff;"}\
One who is thousand-eyed, one who is many-eyed, and one who is three-eyed. one who is the friend of the lord of the Yakshas. Your hands and feet extend in all directions to all places.

[००५ सर्वतः-श्रुतिमल्-लोके सर्वम् आवृत्य तिष्ठसि ।]{style="color:#0000ff;"}\
[००६ शङ्कु-कर्ण महा-कर्ण कुम्भ-कर्णार्णवालय ॥]{style="color:#0000ff;"}\
your eyes and head and mouth are in all directions. Your ears too are everywhere in the universe, and one who is yourself stands everywhere, one who is shaft-eared, one who is large-eared, and one who is pot-eared. One who is the receptacle of the Ocean.

[००७ गजेन्द्र-कर्ण गो-कर्ण पाणि-कर्ण नमो ।अस्तु ते ।]{style="color:#0000ff;"}\
[००८ शतोदर शतावर्त शतजिह्व शतानन ॥]{style="color:#0000ff;"}\
One whose ears are like those of the elephant, or of the bull, or like extended palms. Salutations to you! you has a hundred stomachs, a hundred revolutions, and a hundred tongues.

[००९ गायन्ति त्वां गायत्रिणो अर्छयन्त्य् अर्कम् अर्किणः ।]{style="color:#0000ff;"}\
[०१० ब्रह्माणं त्वां शतक्रतुम् ऊर्ध्वं खम् इव मेनिरे ॥]{style="color:#0000ff;"}\
One who is in the utterers of the gayatri chant, who sing your praises in uttering the Gayatri, and the chanters praise with arkas. You are the brahman power, a your worshipped by 100 sacrifices and as illuminated as the firmament above.

[०११ मूर्तौ हि ते महामूर्ते समुद्राम्बर-संनिभ ।]{style="color:#0000ff;"}\
[०१२ सर्वा वै देवता ह्य् अस्मिन् गावो गोष्ठ इवासते ॥]{style="color:#0000ff;"}\
One of mighty form, the Ocean and the Sky are your two forms. All the deities dwell in your form even as kine dwell within the fold.

[०१३ भवच्-छरीरे पश्यामि सोमम् अग्निं जलेश्वरम् ।]{style="color:#0000ff;"}\
[०१४ आदित्यम् अथ वै विष्णुं ब्रह्माणं च बृहस्पतिम् ॥]{style="color:#0000ff;"}\
In your body I behold soma, and agni, and the lord of the waters, and Aditya, and विष्णु, and brahmA, and बृहस्पति.

[०१५ भगवान् कारणं कार्यं क्रिया करणम् एव छ ।]{style="color:#0000ff;"}\
[०१६ असतश् च सतश् चैव तथैव प्रभवाप्ययौ ॥]{style="color:#0000ff;"}\
Illustrious one, you are cause and effect and action and instrument of everything unmanifest and manifest, and one who is origin and end.

[०१७ नमो भवाय शर्वाय रुद्राय वरदाय च ।]{style="color:#0000ff;"}\
[०१८ पशूनां पतये चैव नमो ऽस्त्व् अन्धकघातिने ॥]{style="color:#0000ff;"}\
Salutations to one who is that is the benevolent one, the archer and the terrible one and giver of boons. Salutations to one who is the lord of animals and one who is that art the slayer of andhaka.

[०१९ त्रि-जटाय त्रि-शीर्षाय त्रिशूल-वरपाणिने ।]{style="color:#0000ff;"}\
[०२० त्र्यम्बकाय त्रिनेत्राय त्रिपुरघ्नाय वै नमः ॥]{style="color:#0000ff;"}

Salutations one who is that has three matted locks, one who is that has three heads, one who is armed with an excellent trident; one who is that has three eyes and the destroyer of the triple city!

[०२१ नमश् चण्डाय मुण्डाय अण्डायाण्डधराय च ।]{style="color:#0000ff;"}\
[०२२ दण्डिने सम-कर्णाय दण्डि-मुण्डाय वै नमः ॥]{style="color:#0000ff;"}\
Salutations one who is terrible, of the form of a skull, the primoridal egg and also the bearer of the universal egg; one who holds a rod, one who is that has ears everywhere, and one who displays the skull and bones sign.


