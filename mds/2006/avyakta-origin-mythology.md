
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [avyakta origin mythology](https://manasataramgini.wordpress.com/2006/07/31/avyakta-origin-mythology/){rel="bookmark"} {#avyakta-origin-mythology .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 31, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/31/avyakta-origin-mythology/ "5:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The two tantric उपनिषद्स्, avyakta and नृसिंह-तापिनि are clear in their पाञ्चरात्र affinities. Yet the संख्य origin mythology of the avyakta is a pretty remarkable piece that is worth considering, for it is a rather distinctive expression of earliest stages of the universe. While in general concepts is similar to the thoughts expressed in other पाञ्चरत्र सांख्य origin mythologies, such as the लक्ष्मि-tantra, it different in the details. Of course it does not go into the details of the तञ्मात्रस् and their ontology like the account in the विष्णु पुराण, but its clarification of the early events in just two paragraphs is useful.

[पुरा किलेदं न किंचन्नासीन्, न द्यौर् नान्तरिक्शं, न पृथिवी ।]{style="font-weight:bold;font-style:italic;color:rgb(204,0,0);"}\
[केवलं ज्योती रूपम् अनाद्य्-अनन्तम् अनण्व्-अस्थूल-रूपम् अरूपं रूपवद् अविज्ञेयं ज्ञान-रूपम् आनन्दमयं आसीत् ।]{style="font-weight:bold;font-style:italic;color:rgb(204,0,0);"}

In the past there was neither the heavens, nor the mid-regions nor the earth. There was only an entity of the form of light, which was without a source nor end, it was neither an atomic particle nor a continous mass, it had not distinct form, yet had a certain distinctness, it was unknowable, yet of the was of the form of information, it was endowed with the "blissful" consciousness.

[तद् अनन्यत् तद् द्वेधाभूद्-धरितम् एकं रक्तम् अपरम् ।]{style="font-weight:bold;color:rgb(204,0,0);"}\
[तत्र यद् रक्तं तत् पुंसो रूपम् अभूत् ।]{style="font-weight:bold;color:rgb(204,0,0);"}\
[यद्-धरितं तन् मायायाः ।]{style="font-weight:bold;color:rgb(204,0,0);"}\
[tau समगच्छतः | ]{style="font-weight:bold;color:rgb(204,0,0);"}\
[तयोर्-वीर्यम् एवम् अनन्दत् ।]{style="font-weight:bold;color:rgb(204,0,0);"}\
[tad avardhata |]{style="font-weight:bold;color:rgb(204,0,0);"}\
[तदण्डम् अभूद्-धैमम् ।]{style="font-weight:bold;color:rgb(204,0,0);"}\
[तत् परिणममानम् अभूत् ।]{style="font-weight:bold;color:rgb(204,0,0);"}\
[ततः परमेष्ठी व्यजायत ॥]{style="font-weight:bold;color:rgb(204,0,0);"}

That one entity without a second then produced a pair, one 'yellow' and the other 'red'.\
The one which was red becme that form known as the puruSha.\
The one which was yellow became that form mAyA.\
The two united.\
Thus their generative power was satisfied.\
It expanded.\
It \[thus] became the glowing \[golden] egg (हिराण्यगर्भ).\
It underwent further evolution.\
From that emerged परमेष्टिन्.

  - The word for evolution in the सांख्य ontology, परिणमन is clearly used.

  - The first paragraph and the first portion of the second roughly maps to the philosophical account of origin mythology provided by the नासदीय सूक्तं or the ऋग्वेद. The second half of the second paragraph corresponds to the हिराण्यगर्भ सूक्तं of the ऋग्वेद. From these ancient starting points it is interesting to note that the Hindus developed several accounts of the origin of the universe each differing somewhat in their details. Examples of such are seen the बृगु स्मृति, the manu स्मृति, and the later tantric texts. So these ideas have hoary continuity in the Hindu world and should be considered the result of endogenous speculation on such topics.


