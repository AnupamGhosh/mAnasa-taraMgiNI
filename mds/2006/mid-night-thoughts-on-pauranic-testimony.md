
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [mid-night thoughts on Pauranic testimony](https://manasataramgini.wordpress.com/2006/06/10/mid-night-thoughts-on-pauranic-testimony/){rel="bookmark"} {#mid-night-thoughts-on-pauranic-testimony .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 10, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/10/mid-night-thoughts-on-pauranic-testimony/ "5:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

"Hindus have no historical sense"; "India is a [cul de sac";]{style="font-style:italic;"} "उपनिषद्स् and the buddha were contemporaneous and had Greek influence". I heard all this gibberish and more from my school textbooks and leftist colleagues. I had long back silently noticed the precision with which Hindus recorded dates on their stone and copper plate inscriptions, be it Sanskrit or a regional dialect. This immediately brought home to me in a flash that the white scholars and their fellow-travellers were looking for Hindu historicity in the wrong places. In fact, the texts which were believed to contain historical records were dismissed as being full of balony by the white scholars and their apers. I always had a certain respect for the पुराण-s and noticed that their genealogical testimonies were indeed surviving, even if corrupt fragments of hoary Hindu history, that actually recorded rulers of जम्द्बुद्वीप. Obviously there were many hoary kShatriya-s who were famous, like मन्धाता, trasadasyu, कार्तवीर्य, भगीरथ, bharata, raghu, रामचन्द्र, हिरण्यनाभ, युद्धिष्ठिर, कृष्ण etc. But then there were also many who were only names-- yet the पुराणस् faithfully preserved them and despite the corruptions in texts had a largely consistent flow. Why should these be preserved --- especially in certain cases like the बार्हद्रथस् with some numerical figures with each king even if his only mention was in these dynastic lists. It immediately struck me that the पौराणिc testimony is real hindu history of a hoary time. In course of my deep investigations of these genealogies I noticed that two white scholars had independently appreciated the importance of the पुराणस् as historical tradition and had paid most careful attention to their genealogies- Pargiter and Morton Smith. Still they had their biases- and got some key sheet anchors wrong. But much of their work is still useful and confirmed by my own studies. Ishwa Mishra is an internet raconteur who has also invested a lot of effort on this matter but many of his speculative forays are seriously misplaced despite the useful mass of information he collects.

The main point I emphasize is that the पुराणस् and not Pargiter, Smith, Mishra, Kak or for that matter Witzel or Parpola should be taken as standards for the genealogical testimony and dates. Most पुराणस् state that a gap of 1000, 1015, 1050, 1115 or 1150 years had elapsed between the nanda period and the महाभारत war. A figure of 1000-1100 years indeed likely to be correct figure for the core महाभारत event of the great war.


