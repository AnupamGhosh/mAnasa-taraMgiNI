
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The path of fire](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/){rel="bookmark"} {#the-path-of-fire .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In one of the most remarkable books written in all times de Santillana and von Dechend state:\
"[Fire is the equinoctial colure, the great circle that passes through the celestial poles intersecting the ecliptic at the point where it intersects the celestial equator.]{style="font-style:italic;"}" They also noted the importance of the fire sticks and fire drill in this symbolism. They indeed had seen the truth. One who know this will understand the hymns of सौचीक agni.

[mahat tad-उल्बंस्थविरं तदासीद् येना विष्टितः प्रविवेशिथापः ।\
विश्वा अपश्यद् बहुधा ते अग्ने जातवेदस् तन्वो देव एकः ॥]{style="color:#0000ff;"}(RV 10.51.2)\
\[gods to agni] Great was that dome (उल्बं), and firmly set, and it weighed-down \[folded] at the place where you entered the waters. One god alone, O agni जातवेदस्, saw your many forms all over.

[ko mA dadarsha कतमः sa devo yo me tanvo बहुधा paryapashyat |\
क्वाह मित्रावरुणा क्षियन्त्य् अग्नेर् विस्वाःसमिधो देवयानीः||]{style="color:#0000ff;"} (RV 10.51.2)\
\[agni asks the devas:] Which God had seen me? Who of all their multitude saw my forms in many dwellings? Where then are placed the fire sticks of agni that lead one of the northern path of the ecliptic (देवयान) O वरुण and mitra?

These cryptically allude exactly to what de Santillana and von Dechend mention as the armillary sphere of the sky with agni as the equinotical colure. The point where he enters the waters is the intersection with the celestial equator. They mention that the Aztecs took Castor and Pollux to be the fire sticks from which men learned to make fire.

[ऐछाम त्वा बहुधा जातवेदः प्रविष्टमग्ने अप्स्वोषधीषु ।\
तं त्वा यमो अचिकेच् चित्रभानो दशान्तरुष्याद् अतिरोचमानम् ॥]{style="color:#0000ff;"}

[वरूण on behalf of the devas said:] In many places, we searched for you O agni जातवेदस्, hidden in the plants and waters.Then yama piled you*, one with many-colored \[flames], blazing forth from your tenfold secret station,

  - (achikech: the same root as  chayana as in agni-chayana the piling of fire altar)

[होत्राद् अहं वरुण बिभ्यदायं नेदेव मा युनजन् नत्र देवाः ।\
तस्य मे तन्वो बहुधा निविष्टा एतम् अर्थं नचिकेताहम् अग्निः ॥]{style="color:#0000ff;"}

\[agni said:] O वरुण, I fled in fear from the sacrifice,so that gods do not in this manner yoke me*. Thus my forms we laid down in many places; I, agni saw this as my path.\
\*yoke me= enjoin to the sacrificial rite

[एहि मनुर् देवयुर् यज्ञकामो अरं कृत्या तमसि क्षेष्यग्ने ।\
सुगान् पथः कृणुहि देवयानान् वह हव्यानि सुमनस्यमानः ॥]{style="color:#0000ff;"}

[वरूण on behalf of the devas said:] Come O' agni who dwells in the darkness; manu devoted to the gods desiring to perform sacrifices,waits ready to do rituals. Make clear the paths leading to the northern course of the ecliptic, and bear oblations with good-will.

[अग्नेः पूर्वे भ्रातरो अर्थमेतं रथीवाध्वानमन्वावरीवुः ।\
तस्माद् भिया वरुण दूरमायं गौरो नक्षेप्नोरविजे ज्यायाः ॥]{style="color:#0000ff;"}

\[agni said:] My elder brothers first selected this path, a circuit, like the circumambulation of a charioteer. So,वरुण, I fled afar in fear, as the wild bull (Gaur) flees from the archer's bowstring. (The elder brothers of agni are the previous equinoctial points that slipped away from precession and thus "died")

[कुर्मस्त आयुर् अजरं यदग्ने यथा युक्तो जातवेदो नरिष्याः ।\
अथा वहासि सुमनस्यमानो भागं देवेभ्यो हविषः सुजात ॥]{style="color:#0000ff;"}

[वरूण on behalf of the devas said:] O agni जातद्वेदस् we establish for you undecaying life, so that when you are thus employed you are not harmed. So, well-born one you shall with good-will bear to the gods their share of oblations.

[प्रयाजान् मे अनुयाजांश्च केवलान् ऊर्जस्वन्तं हविषोदत्त भागम् ।\
घृतं चापां पुरुषं चौषधीनाम् अग्नेश्च दीर्घमायुरस्तु देवाः ॥]{style="color:#0000ff;"}

\[agni said:] Grant me the fore-offerings and post-offerings*, and the sole-offerings and the shares of the strengthening oblations. \[grant me] ghee, the waters, men and plants, and let me agni have long life, O gods, to Agni.

[तव प्रयाजा अनुयाजाश्च केवल ऊर्जस्वन्तो हविषः सन्तुभागाः ।\
तवाग्ने यज्ञो अयमस्तु सर्वस्तुभ्यं नमन्ताम् प्रदिशश् चतस्रः ॥]{style="color:#0000ff;"}\
[वरूण on behalf of the devas said:]\[agni said:] Yours are the fore-offerings and post-offerings*, and the sole-offerings and the shares of the strengthening oblations. O agni let this whole sacrificial rite belong to you, and let the four directions bow before thee.

\*The fore- and after-offerings are the pra-याजस् and anu-याज oblations made in the अप्री rite. The kevala is the agni-only offering in the main course of the ritual.

The mention of the archer shooting at the bull is important. It is an ancient motif in myth that is depicted in Egypt on the Dendera Zodiac. Here the goddess Satit shoots at Sirius which is the head of the Sothis cow! But in this instance the gaur bull is likely to be Taurus fleeing from the archer who is मृगव्याध (or could he gaur be another name for the antelope मृग?) for the Indo-Aryans.


