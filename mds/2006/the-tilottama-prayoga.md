
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The तिलोत्तमा prayoga](https://manasataramgini.wordpress.com/2006/03/09/the-tilottama-prayoga/){rel="bookmark"} {#the-तलततम-prayoga .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 9, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/09/the-tilottama-prayoga/ "6:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The भूत tantras recommend the worship of apasaras and provide the outlines for these rites. However, their proper deployment remains unknown except to one who has mastered the true rahasyas. The mantras of the practioners of the चीनाचार are nowhere in the league of these rahasya mantras. 8 principal apsaras may be invoked with apasara मण्डल though तिल्लोतमा may be offered a special sacrifice for the highest purpose. The apasaras who are invoked are:\
शशिदेवी, तिल्लोतमा, काञ्चनमाला, कुण्डलहारिणि, रत्नमाला, रम्भा, उर्वशी and भूशिणी.

The great तिल्लोतमा prayoga goes thus:\
The mula mantra is:\
[[](ci(21011,'vicinvat2B01m')){#iovmla16}]{style="font-size:1px;"}oM श्री तिलोत्तमा| विcइन्वतीम् आकिरन्तीम् अप्सरां साधुदेविनीम् |\
glahe कृता́ni गृह्णानाम् अप्सरां tAm iha huve || hUM स्वाहा

He then lays down 5 five feathers with the formula:\
[[](ci(16915,'paF100caco0D1E0101')){#iovpla16}]{style="font-size:1px;"}पञ्चचोडा upa दधाति | apsarasa evainam etA भूता अमूष्मिल् loka upa shere | Atho तनूपानीर् एवैता यजमानस्य | yaM द्विष्यात् tam upadadhad ध्यायेत् | एताभ्य एवैनं देवताभ्य A वृश्चति | ताजग् Artim आर्छति ||

With that he excludes his भ्रातृव्यस् from success. He then grinds a paste of 16 indra yava grains, 30 चणक (chickpeas) and mixes it with taila and prepares a वशिकरण lepa with that. Then he offers 5 oblations in the fire with a mixture of salt, tila, ghee, honey and milk.\
[]{style="font-size:1px;"}[[](ci(21011,'huve')){#iovmla16}]{style="font-size:1px;"}


