
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [साधन idols of the clan](https://manasataramgini.wordpress.com/2006/03/05/sadhana-idols-of-the-clan/){rel="bookmark"} {#सधन-idols-of-the-clan .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/05/sadhana-idols-of-the-clan/ "7:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A number of idols have been routinely used by my family in various उपासनस्. A few are detailed below:

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/ankusha_ganapati.jpg){width="75%"}
```{=latex}
\end{center}
```



The rudra family: The wooden कुमार on the left is our patron deity, the deity of the clan and the protector deity. The idol is routinely installed on the trienniel skanda याग performed as per the अथर्वण krama ordained by gopatha. The central idol is the bronze idol of uma and rudra that has been worshiped for a long time now by my clansmen for various festivals and receives regular रुद्रार्चन. The विनायक on the right with the ceremonial अङ्कुश, stone lamps and गजदीप is ceremonially bathed and deployed in several साधनस् including the regular chaturti-vrata. Receives regular अथर्वशीरसार्चन.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/sarasvati2_small.jpg){width="75%"}
```{=latex}
\end{center}
```



The sarasvati-series: The one on the left is the बृहद् नीला-सरस्वती, also known as भैषज्य सरस्वती- worshipped by the secret offerings known as वामदेव्य made as per the अथर्वण krama. It is a protector idol of the clan and the tassle of atharvanic bird feathers hang on the idol. One on the right is the विरिञ्ची-प्रेयस्या, also worshipped as महासरस्वती.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/mandala_small.jpg){width="75%"}
```{=latex}
\end{center}
```



The लक्ष्मीनारायण pair: used for worship with the mantra विष्णुमाय, the सहस्रनाम vidhi and नारायनार्चन. The saura मण्डल is used for the saura prayogas. Of course the other मण्डल and its details remain secret.


