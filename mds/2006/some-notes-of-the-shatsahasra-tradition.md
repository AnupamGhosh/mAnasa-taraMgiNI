
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes of the षट्साहस्र tradition](https://manasataramgini.wordpress.com/2006/07/10/some-notes-of-the-shatsahasra-tradition/){rel="bookmark"} {#some-notes-of-the-षटसहसर-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 10, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/10/some-notes-of-the-shatsahasra-tradition/ "3:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/वागीश्वरी.jpg){width="75%"}
```{=latex}
\end{center}
```



When in गवल्कुण्ड, R1 and A saved us from the pangs of hunger. R1, A and our clan had similar histories with peregrinations through the length of जम्बुद्वीप over time in the practice and quest of the mantra-शास्त्र. R1's clan had moved back recently to the Southern realms after 2.5 centuries in the land of the malla राजस्. R1 who treated us as son also opened our eyes to some remarkable matters- she us acquainted with her father, who was one the last holders of the विद्या's of the षट्साहस्र tradition (since R1 was cashing her brahminical check). Thus, I learnt some of these rahasyas that I wish to record as they are in danger of extinction.

  - Several forms of प्रत्यङ्गिरा are offered Ajya homas a combine पूर्णाहूती, and तर्पणं. The प्रत्यङ्गिरा-s invoked in this tradition are: 1) सिद्धलक्ष्मी (She dances on the hands of the hands of the frightful उग्रताण्डव bhairava who in turn stands on a preta. She smiles with her four heads and wears a garland of 108 skulls. She has 10 arms with a sword, mace, bell, varada mudra and wine cup and ज्ञान mudra, bow, arrow, lasso and a severed head. Her mantra encompasses the विद्या of bAlA, the daughter of कुलसुन्दरी). 2) मन्त्रमातृका. 3) मन्त्रडामारिका 4) सप्तकोटेश्वरी

  - Several सरस्वती-s are also offered oblations:1) मातृकासरस्वती 2) विद्याविद्येश्वरी 3) वाग्भवेश्वरी 4) वागीश्वरी (She has 4 arms and is seated on a सिंह in the ललितासन pose. She holds a hala, a rod and two spikes to spear the demons of tamas) 5) वैखरी (in the embrace of prajApati).

  - Then the oblations are made to the great चण्डकापालिनि and नित्याकाली

  - The 4 कुलसुन्दरी-s are worshiped: कुलवागीश्वरी, कामेश्वरी, वज्रेश्वरी and महात्रिपुरसुन्दरी. They are worshiped with the कुलवगीश्वरी, वज्रेश्वरी rahasya mantra and पञ्चदशि mantras

  - The mighty कुब्जिका, the mistress of the पस्चिमाम्नाय, is worshipped with the glorious 32 syllabled समयविद्या. She is mediated as seated on the lap of her spouse, महादेव as नवात्मन्. She is indescrible as one can only experience her upon 'seeing' the flash of the mantra.

  - The most peculiar विद्या of महान्तारी of 15 syllables is used with fire oblations in the Ajyahoma. The manifest form of महान्तारी for all those who know the विद्या emerges directly out of the अङ्गस् of कार्त्तिकेय. She has 6 heads and twelve arms as result.


