
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The vighnas](https://manasataramgini.wordpress.com/2006/01/17/the-vighnas/){rel="bookmark"} {#the-vighnas .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/17/the-vighnas/ "6:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We thought of the freedom of Jx and wondered what our fate might be. Our spasha reported that even before the खाण्डव event there was much fear that all were targets. The spasha stated that the खाण्डव event only took us into the center of the रणनदि. The spasha clarified that there was really no way out of it we had only two options: face the प्रशोष attack of the vairis or go out to battle in खाण्डवप्रस्थ. As we discussed the coming encounter with the अमात्य, the fate of Jx was mentioned again. It was no better but the devas had saved him. Then the अमात्य asked for a detailed account the events.

Oct 25 2003: The 1st fateful battle of the durga and पाषण्ड hills. Some key retrospective findings reported to us were: Even there the other पक्ष acted very similar as the current vairi. The moves were similar and they read some of our सेना's weaknesses.\
Nov 27 2003: The 2nd battle of पाषण्ड hill, the muni was providing cover. We scored a dramatic victory, but we noticed that the vairi was not finished and was regrouping.\
Nov 1 2003: The vairi-s overthrew the अमात्य and शचीव in a remarkably effective ambush.\
Nov 3 2003: We with the अमात्य and शचीव walked into the trap at deva hill.\
Nov 6 2003: The deva hill trap is pressed home disarming us temporarily.\
Nov 22 2003: We escape from the deva hill trap, due to our rapid action. The vairis were beaten but seem to have despatched a deadly prayoga at us.\
Dec 15 2003: R warns us that the worst is yet to come, she sights magician K of theEconomics department. K joins forces with the deva hill aris to lay a deadlier trap.\
Nov 23 2004: The खाण्डव war is imminent\
Nov 23 2004: We get all military preparation in place\
Nov 30 2004: Three front war begins. Heavy attack on two fronts were barely fended by us. The retrospective reports showed that we bore the day entirely due to the mantra prayoga.\
Dec 1 2004: We repelled the first attack.\
Jan 14-20 2005: The first victim is tied to the यूप like a pashu, but the sacrifices of his ancestors save him from certain death.\
Jan 27 2005: Major attack by vairis, we fight aided by the muni we balance the day.\
Feb 4th 2005: Another major attack by the vairis we stabilize the situation again.\
Feb 22nd 2005: Long term trap is laid by the vairis, and we walk into it.\
Feb 24th 2005: The two birds indradatta and विष्णुदत्त bore us the message, we might have walked into the trap. Jx who was being pinned down severely on his front also failed to see the trap and walked into it. The muni and Mn were shaken by they own attacks but no one saw the connections in the tangled web.\
March 10th: शचीव misreads the game and fails to deploy troops.\
March 17th 2005: Our mantras were neutralized and gods failed to bear us aid.\
March 24th: The vairis attacked us after we were lulled into pleasure by the great Hayasthanika.\
March 31st: The first great fateful battle, the forerunner of the future. It was a terrible showdown ironically not far from the battle ground of our "Gaugamela". It went almost like a repeat of the Gaugamela encounter. Here it was a massacre of our troops by the vairis who deployed the mysterious astra we had never seen before and was to haunt us for long. Over the next few days the battle raged and we saw the first glimpses of the devastation that was going to strike us in the near future.\
April 11th: We got a signal of the weakest link. The दण्डनायक was not likely to hold. We were to soon see how this opened the gates for the attack.\
May 14th: We again approached a maidan close to the पाशण्ड hill with our troops. They scored a potential victory and were celebrating. Our अमात्य and shachiva thought that the victory had finally settled the matter with the vairis. We were alone having dinner when we sighted a much larger force advancing. We tried to take a defensive position but faced a swift attack. We narrowly escaped the next morning with our lives.\
May 18th: We signed a truce pact and hoped to buy time for building our troops.\
June 7th: The vairis broke the truce plan and renewed attack by the कृत्या mode.\
June 13th: Second major कृत्या strike\
June 14th: The old witch whom we had countered in our heydays was surprised at the ferocity of the attack. She deployed certain prayogas to attempt to counter the कृत्या on our behalf.\
June 15th: मारण strike attempted.\
June 22nd: Our great rites were destroyed like those of विश्वामित्र by मारीच and सुबाहु.\
July 3rd: We deployed the great यक्षी to save ourselves from मारण at the nick of time.\
Aug 26th: Yet another मारण strike.

Finally the we got intelligence that the sammohana weapon was being deployed.


