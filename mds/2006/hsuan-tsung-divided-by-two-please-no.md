
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Hsuan Tsung divided by two : Please NO](https://manasataramgini.wordpress.com/2006/03/29/hsuan-tsung-divided-by-two-please-no/){rel="bookmark"} {#hsuan-tsung-divided-by-two-please-no .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 29, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/29/hsuan-tsung-divided-by-two-please-no/ "6:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We climbed up the turret there was a closely fought encounter going on the plain. We had sent a division to protect the home ground under an young and consentious captain, however, he was pretty inexperienced. The formidable Kappa having strengthen her fort well, ventured out her lair and gave me the code signal: "अभूरि taila amla anya amla". We saw that our captain launched a scorching assault but was trapped by a shatru. From our vantage point we noted that some म्लेच्छस् were covertly mining us. We started sending reinforcements to seal that breach but realize that still much fighting needs to be done on that front. We had sent a small division with some good guns to aid GS, our old acquaintence, but he was overwhelmed by an extraordinary attack by 5 म्लेच्छस् and our reinforcements could do nothing at all. We were left wondering if we could salvage any troops at all. We were concerned about the secret plan termed "पञ्च-कोणं cha शुल्बारि eka-कोणं" falling into the hands of the dasyus. Just then we saw that our mighty general कामराज, was stopped by an enemy division close to where our young captain was being pinned down in a संकुल yuddha. General कामराज broke through the enemy ranks and was wreaking havoc, when he was suddenly attacked by a second enemy division. We asked him to extricate himself and attack that second division code named indra -जाल 1, and sent our able servants to reinforce his ranks, as well as covered him with mantra prayogas. He scored a spectacular victory on the front indra जाल 1 and returned to tackle indra जाल 2 that he hopes control and then decimate it. We also dispatched a division to prepare for the future battle of rajjvanta.

The messengers were coming in with victorious news that our flag was flying over indra जाल 1 and that our troops were pressing hard on indra जाल 2. We were travelling back by the path where the राक्षसी had first gripped us during the terrible battle of खाण्डवप्रस्थ. There on the रणाङ्गण we saw the corpses of कृश्ण tvacha, dva saumyau, श्वेतडम्बर, क्षेत्रगु, मयाचुम्बिका and sarva-मदोल्लास. As we walked by each preta arose laughed and flew away. We placed the samid in the fire and cast the oblation. The यक्षिणिस् "OPJ", "SGYA", "LZ", "ME" and "LR" arose with each oblation. We kept sending them but each one appeard to explode into pieces and run away. Then we saw the vairi send the मरणाक्षण-- it gripped us, we were gripped by the terrible pasha from which no one has emerged on the other side. They said the यक्षिणिस् have become phena.


