
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On some दानस् and utsargas of the mediaeval Hindus](https://manasataramgini.wordpress.com/2006/09/24/on-some-danas-and-utsargas-of-the-mediaeval-hindus/){rel="bookmark"} {#on-some-दनस-and-utsargas-of-the-mediaeval-hindus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 24, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/24/on-some-danas-and-utsargas-of-the-mediaeval-hindus/ "11:10 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The dharma texts interminably dilate on the performance of a variety of dAna-s and utsarga-s to deserving people or for public benefit which ranged from elephants, cattle, horses, clothing, umbrellas, footware, toothbrushes, gold, gems, grains, sugar etc. These have all been discussed at length by the great scholar PV Kane in his voluminous studies on the dharma literature. However, there were a few points that we noted during our peregrinations through the laborious dharma literature that are of some interest:

1\) ***grAntha दानं*** is described in a few texts and the mediaeval authority हेमाद्रि, the यादव prime minister, dilates extensively on this, in his enormous volume on Hindu charity. Typically volumes of पुराण-s, इतिहास-s and dharma manuals were gifted to deserving individual or to मठ-s. In particular it was recommended that grantha dAna-s be made to temples of rudra, विष्णु or sUrya for the use of the public and said to be as meritorious as the gift of gold, land or cattle. This indicates the presence of Hindu public libraries associated with temples. Astronomical texts, the siddhAnta-s were also recommended as donations as per the agni-पुराण (211.61; per Nag press prints).


 2.  Both अपरादित्य, the shilahara king of Konkans (\~1120 CE) in his legal tract built on याज्ञ्यवल्क्य's स्मृति and हेमाद्रि also prescribe a dAna known as the ***आरोग्यशाला दानं***. This involved construction of hospitals that supplied free medication, and the provision to support a competent physician to man the आरोग्यशाला-s. This dAna was considered one of the highest dAna-s because it provided for everything by helping in successful achievement of the 4 puruShArtha-s. The स्कन्दपुराण provides an older presentation of the same point.


 3.  Making public water-resources, ***कुपादि दानं***, are considered amongst highest of meritorious activities. shabharasvamin mentions the ऋग्वेदिc mantra RV10.4.1 to provide shruti's authority on the matter. The mantra mentions the water shed set up in a arid zone to provide for way-farers. One is supposed to burn half of ones दुष्कर्म when one makes water resources for public use. The recommended water resources are कूप: wells, प्रपा: water-sheds in arid areas, apA-chakras: water-wheels, वापी: a water-tank with steps leading to the water, hrada: deep tank, पुश्करिनी: a pond and a तडाग: an artificial lake. There is some debate amongst Hindu authorities as to whether the maker of the water reservoir can use it as a member of the public. A water-shed may also be built in the temple and a nominal brahmin appointed to distribute drinking water to the general public.

After establishing the water-reservoir the donor should perform in the least the following ancient vedic fire sacrifice on a स्थण्डिल altar. He cooks barley in milk as an offering for deva वरुण. He then offers 8 oblations of this barley on the fire with the ऋक्स्: RV 4.1.4-5, 1.24.14, 1.25.19, 1.24.15, 8.42.3 and a final two using the formula known as "the greedy dog in the kennel" . These oblations are made starting from the west in each 8 directions on the altar in the anti-clockwise direction. Then in the middle of the altar he makes 2 oblations of milk with RV 10.81.3 and 1.22.17. Then he takes a bath (if a tank and larger) in that water body with the formula RV 7.89.5 to वरुण. If it is a pond or lake he then introduces fishes, terrapins and water fowl mentally invoking deva वरुण. Then he gives fee to the performing priests if he has employed them and feeds ब्राह्मणस्.

4\) ***वृक्षं*:** Tree plantation is an extremely meritorious karma as per Hindu authorities from the earliest times. The kaTha-s believed that trees should be treated like sons, while the padma पुराण gives a long lecture on the benefits of tree-planting. हेमाद्रि prescribes that the ashvattha ([Ficus religiosa]{style="font-style:italic;"}), ashoka, chinchini (tamarind) and दाडिमि (pomegranate) वृक्ष-s should be planted. Others recommend the mango, the banyan, kapittha (Jamun), bilva and amalaka (gooseberry) trees to be planted. For vandalizing a fruit-tree the ruler should administer the highest fine; for a flower tree the middle fine, for a bower a 100 coin fine and for desecrating grass and shrubs a one coin fine. हेमाद्रि also recommends plantation of a garden as an act of highest merit.

In the plantation and gardening karma he performs a fire rite with 3 oblations of cooked rice with formulae:\
OM विष्णवे स्वाहा | OM इन्द्राग्निभ्यां स्वाहा | OM विश्वकर्मणे स्वाहा ||\
Then he makes a series of oblations with the ऋक्स् of विश्वामित्र RV 3.8.6-10.\
then he goes watering the tree or garden with the formula of विश्वामित्र "vanaspate shatavalsho vi roha..." RV 3.8.6.


