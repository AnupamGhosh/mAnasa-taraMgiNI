
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The सर्वात्मन् hymn to विष्णु](https://manasataramgini.wordpress.com/2006/12/31/the-sarvatman-hymn-to-vishnu/){rel="bookmark"} {#the-सरवतमन-hymn-to-वषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 31, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/31/the-sarvatman-hymn-to-vishnu/ "7:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The सर्वात्मान् hymn to विष्णु shows some variations between recensions and traditions but form accepted amongst स्मार्तस् has 40 core shlokas corresponding to Mbh 12.47.23-63. This an extremely important hymn of विष्णु, comparable to the hymn to rudra by the goat-faced दक्ष. The ऋषि of सर्वात्मक महावैष्णव stotra is given as भगवान् व्यास, the Chandas is अनुष्टुभ्, the देवाता is सर्वात्मक mahA-विष्णु.

Its importance lies in the fact that it might represent one of the earliest post-vedic compositions to विष्णु, within an early vaiShNava lineage that was strongly affiliated with vedic ritualism. An important point to note is that the critical version of the hymn shared by all recensions and practiced by स्मार्तस् does not have any mention of the अवतार-s or the vAsudeva-centric elements of सात्त्वत that contributed to पाञ्चारत्र. So its original composition was possibly outside of these streams and was latter swallowed into the सात्त्वत context (as much of the vaiShNava material in the महाभारत belongs to the early सात्त्वत tradition). We suspect that there were 3 streams of early vaiShNavism: 2 associated with the कृष्ण yajurvedins: 1) The तैत्तिरीयस्, of which the वैखानस branch gave rise to an early vaiShNava system. 2) the कट्ःअस् who possibly incorporated an early अवतार model centered on यज्ञ-varaha (विष्णु स्मृति, विष्णु-dharma and विष्णु-dharmottara eventually emerged from this school). 3) The सात्त्वत-s who promulagated the chatur-व्यूह system and developed other aspects of the अवतार concept centered on कृष्ण. This was the precursor of the later पाञ्चरात्र. It is possible, but not certain, that this school might have had association with a shukla-yajurvedic shAkha called एकायन. This school was strong in Mathura and interacted with the early shaivas and कौमार-s, who also have a presence in the महाभारत. In later history all these three schools extensively exchanged concepts, espcially in terms of the अवतार concept.

हिरण्य-वर्णं yaM garbham-aditir daitya नाशनम् |\
एकं द्वादशधा जज्ञे tasmai सूर्यात्मने नमः ||1||\
shukle देवान् पितॄन् कृष्णे तर्पयत्यमृतेन yaH |\
yashcha rAjA द्विजातीनां tasmai सोमात्मने नमः ||2||\
The famous concept that in the bright fortnight the soma cup of the moon is drained by the deva, whereas in the dark fortnight by the manes is expressed here. The vedic idea of soma as rAja of the ब्राह्मणस् is repeated.

mahatas-तमसः pAre पुरुषं jvalanadyutim |\
yaM ज्ञात्वा मृत्युमत्येति tasmai ज्ञेयात्मने नमः ||3||\
[yaM बृहन्तं बृहत्युक्थे yam agnau yaM महाध्वरे | ]{style="font-weight:bold;font-style:italic;"}\
[yaM vipra सङ्घा गायन्ति tasmai वेदात्मने नमः ||4||]{style="font-weight:bold;font-style:italic;"}\
Here we find an identification of विष्णु as the agnichayana: the बृहद्-uktha is the collection of 80 त्रिऋचस् recited in the final part of the agnichayana. " you are the fire, the great sacrificial ritual is the reference to the agnichayana"

[ऋग्-यजुः साम धामानं दशार्ध हविराकृतिम् | ]{style="font-weight:bold;font-style:italic;"}\
[yaM sapta tantuM tanvanti tasmai यज्ञात्मने नमः || 5||]{style="font-weight:bold;font-style:italic;"}\
विष्णु is identified with the 3 vedic recitation, the 10 half oblations and the 7 thread from which the sacrifice is woven. Typically this is taken to mean 7 main Chandas: gAyatri, उष्णिक्, अनुSतुभ्, त्रिष्टुभ्, jagati, बृहति and pankti or the 7 व्याहृति calls.

[yaH सुपर्णो यजुर्नाम Chando गात्रस् त्रिवृच्छिराः | ]{style="font-weight:bold;font-style:italic;"}\
[rathantara बृहत्-यक्षस् tasmai स्तोत्रात्मने नमः || 6||]{style="font-weight:bold;font-style:italic;"}\
This idea, extensively developed in the ritual of the पाञ्चरात्र tantras, is the construction of the body of विष्णु by vedic mantras: सुपर्ण yajushes are his name, the Chandas the body, the त्रिवृत् the head, the बृहत् and rathanthara सामन्स् his body. The specific identification with yajur hymns is an aspect seen in वैखानसस् and some पाञ्चरात्रिc texts. This makes one wonder if the सर्वात्मन् was actually a production of the early वैखानसस्, who were one of the first of the more "vedically oriented" वैष्णवस्. This is supported by the five-fold manifestation of विष्णु mentioned below, which is an important tenet of [वैखानस thought](http://manollasa.blogspot.com/2006/02/note-on-vaikhanasa-tradition.html).

[yaH sahasra-save satre जज्ञे vishva-सृजाम्-ऋषिः | ]{style="font-weight:bold;font-style:italic;"}\
[हिरण्यवर्णः shakunis tasmai हंसात्मने नमः || 7||]{style="font-weight:bold;font-style:italic;"}\
विष्णु is called the ऋषि in the satra with the 1000-fold pouring in which the world was generated and equated to the famous vedic golden bird "हिराण्य पक्षं शकुनं".

पदाङ्गं सन्धिपर्वाणं स्वरव्यञ्जन लक्षणम् |\
यमाहुरक्षरं नित्यं tasmai वागात्मने नमः || 8||\
yashchinoti सतां सेतुमृतेनामृत योनिना |\
धर्मार्थव्यवहाराङ्गैस्तस्मै सत्यात्मने नमः || 9||\
yaM पृथग् धर्मचरणाः पृथग् dharma फलैषिणः |\
पृथग् धर्मैः samarchanti tasmai धर्मात्मने नमः || 10||\
yaM taM vyaktastham-अव्यक्तं vichinvanti महर्षयः |\
क्षेत्रे क्षेत्रज्ञमासीनं tasmai क्षेत्रात्मने नमः || 11||\
[yaM दृगात्मानमात्मस्थं वृतं षोडशभिर् गुणैः | ]{style="font-weight:bold;font-style:italic;"}\
[प्राहुः सप्तदशं साङ्ख्यास् tasmai साङ्ख्यात्मने नमः || 12|]{style="font-weight:bold;font-style:italic;"}|\
विष्णु is identified with सांख्य thought. विष्णु is called the 17th entity of सांख्य, enveloped the 16 attributes. Based on the commentary of नीलकण्ठ one might suggest that the 17th is the puruSha, while the 16 are प्रकृति + her evolutes (possibly 5 भूत-s, 5 karmendriya and 5 ज्ञानेन्द्रियस्)

[yaM विनिद्रा जितश्वासाः सत्त्वस्थाः संयतेन्द्रियाः | ]{style="font-weight:bold;font-style:italic;"}\
[ज्योतिः pashyanti युञ्जानास् tasmai योगात्मने नमः || 13||]{style="font-weight:bold;font-style:italic;"}\
विष्णु is identified with yoga here--explicitly yoga is describe as the control of breath (प्राणायाम) and withdrawal from sense-inputs.

अपुण्य पुण्योपरमे yaM punarbhava निर्भयाः |\
शान्ताः संन्यासिनो यान्ति tasmai मोक्षात्मने नमः || 14||\
yo.asau yuga-सहस्रान्ते प्रदीप्तार्चिर् विभावसु |\
सम्भक्षयति bhUtAni tasmai घोरात्मने नमः || 15||\
[सम्भक्ष्य सर्वभूतानि कृत्वा चैकार्णवं jagat | ]{style="font-weight:bold;font-style:italic;"}\
[बालः svapiti yashchaikas tasmai मायात्मने नमः || 16||]{style="font-weight:bold;font-style:italic;"}\
विष्णु is depicted as sleeping as an infant on the एकार्णव after devouring the universe and creating the pralaya. This form is recalled in पाञ्चरात्र and is encountered by मार्कण्डेय.

sahasra-shirase tasmai पुरुषायामितात्मने |\
चतुः समुद्रपर्याय योगनिद्रात्मने नमः || 17||\
ajasya नाभावध्येकं यस्मिन्विश्वं प्रतिष्ठितम् |\
पुष्करं पुष्कराक्षस्य tasmai पद्मात्मने नमः || 18||\
yasya केशेषु जीमूता नद्यः सर्वाङ्गसन्धिषु |\
कुक्षौ समुद्राश्चत्वारस्तस्मै तोयात्मने नमः || 19||\
युगेष्वावर्तते yo.अंशैर्दिनर्त्वनय हायनैः |\
sarga प्रलययोः कर्ता tasmai कालात्मने नमः || 20||

[brahma वक्त्रं bhujau क्षत्रं कृत्स्नम्-ऊरूदरं विशः | ]{style="font-weight:bold;font-style:italic;"}\
[पादौ यस्याश्रिताः शूद्रास् tasmai वर्णात्मने नमः || 21||]{style="font-weight:bold;font-style:italic;"}\
[यस्याग्निर्-आस्यं dyaur-मूर्धा खं नाभिश्-चरणौ क्षितिः | ]{style="font-weight:bold;font-style:italic;"}\
[सूर्यश्-चक्षुर् दिशः shrotre tasmai लोकात्मने नमः || 22||]{style="font-weight:bold;font-style:italic;"}\
The identification of विष्णु with the puruSha of the vedic puruSha सूक्तं is made in the the above 2 shlokas (This is a restatement of material from mantras 13/14/15 तैत्तिरीय version)

[विषये वर्तमानानां yaM taM वैशेषिकैर् गुणैः | ]{style="font-weight:bold;font-style:italic;"}\
[प्राहुर्-विषय गोप्तारं tasmai गोप्त्रात्मने नमः || 23||]{style="font-weight:bold;font-style:italic;"}\
There also appears to be an explicit mention of वैषेशिक thought here.

अन्नपानेन्धन mayo रसप्राणविवर्धनः |\
yo धारयति bhUtAni tasmai प्राणात्मने नमः || 24||\
paraH कालात्परो यज्ञात्परः sadasatoshcha yaH |\
अनादिरादिर्विश्वस्य tasmai विश्वात्मने नमः || 25||\
yo mohayati bhUtAni sneha रागानुबन्धनैः |\
sargasya रक्षणार्थाय tasmai मोहात्मने नमः || 26||\
आत्मज्ञानम् इदं ज्ञानं ज्ञात्वा पञ्चस्व vasthitam |\
yaM ज्ञानिनो.अधिगच्छन्ति tasmai ज्ञानात्मने नमः || 27||\
aprameya शरीराय sarvato.ananta चक्षुषे |\
अपार-परिमेयाय tasmai चिन्त्यात्मने नमः || 28||\
जटिने दडिने नित्यं lambodara शरीरिणे |\
कमण्डलुनिषङ्गाय tasmai ब्रह्मात्मने नमः || 29|

[शूलिने त्रिदशेशाय त्र्यम्बकाय महात्मने | ]{style="font-weight:bold;font-style:italic;"}\
[bhasma दृघोर्ध्व लिङ्गाय tasmai रुद्रात्मने नमः || 30||]{style="font-weight:bold;font-style:italic;"}\
The identification of विष्णु with rudra and is made here (The key attributes of rudra that are mentioned are the trident, rudra as tryambaka and bhasma and Urdhva लिङ्ग)

पञ्च भूतात्मभूताय भूतादि निधनात्मने |\
akrodha droha मोहाय tasmai शान्तात्मने नमः || 31||\
yasmin-सर्वं यतः सर्वं yaH सर्वं sarvatashcha yaH |\
yashcha sarvamayo नित्यं tasmai सर्वात्मने नमः || 32||\
vishvakarman namaste.astu विश्वात्मन् vishvasambhava |\
apavargo.asi भूतानां पञ्चानां परतः स्थितः || 33||\
namaste त्रिषु लोकेषु namaste परतस्त्रिषु |\
namaste दिक्षु सर्वासु त्वं hi सर्वपरायणम् || 34||\
namaste bhagavan विष्णो लोकानां प्रभवाप्यय |\
त्वं hi कर्ता हृषीकेश संहर्ता चापराजितः || 35||\
tena पश्यामि te दिव्यान्-भावान् hi त्रिषु vartmasu |\
tachcha पश्यामि tattvena yatte रूपं सनातनम् || 36||\
दिवं te शिरसा व्याप्तं पद्भ्यां देवी vasundharA |\
विक्रमेण trayo लोकाः पुरुषो.asi सनातनः || 37||\
अतसी पुष्प सङ्काशं पीतवाससम्-achyutam |\
ye namasyanti गोविन्दं na तेषां vidyate bhayam || 38||\
यथा विष्णुमयं सत्यं यथा विष्णुमयं हविः |\
यथा विष्णुमयं सर्वं पाप्मा me नश्यतां तथा || 39||\
त्वां प्रपन्नाय भक्ताय गतिमिष्टां जिगीषवे |\
यच्छ्रेयः पुण्डरीकाक्ष तद्ध्यायस्व surottama || 40||

iti विद्या tapo योनिरयोनिर्विष्णुरीडितः |\
वाग्यज्ञेनार्चितो देवः प्रीयतां मेजनार्दनः || 41||


