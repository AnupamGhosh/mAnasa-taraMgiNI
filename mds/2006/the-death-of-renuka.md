
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The death of रेणुका](https://manasataramgini.wordpress.com/2006/03/06/the-death-of-renuka/){rel="bookmark"} {#the-death-of-रणक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/06/the-death-of-renuka/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our ancestress रेणुका, the इक्ष्वाकु princess and wife of jamadagni, is supposed to have had a checkered career. She is supposed to have been adultrous having had an affair with a yadu prince of मार्तिकवत, which led to our illustrious ancestor jamadagni's wrath and the famous story concerning rAma that needs no repetition now. jamadagni was accused by some modern interlocutors as being unjust to her (but I realized their knowledge did not go beyond the tale narrated in the Amar Chitra Katha comics, where jamadagni merely say she had 'sinned' without any reference to what the sin was. After all this was for kids). But from the genuine versions of the tale we see this is entirely unjustified. jamadagni was pretty dutiful to रेणुका. The tale goes that the mighty jamadagni was practising archery, when she used to go and collect the arrows he used to shoot at the targets. She was distressed in course of this activity by the sun's heat, when jamadgni is supposed to have threatened to hit the sun with his great astras. This had the gods give रेणुका shade to help her with the sun. But there is not much doubt despite later day euphemisms that she did clearly have an affair with the prince of मार्तिकवत, and adultery as per Hindu law warranted punishment, ever since the day उद्दालक आरुणि saw his mother commit it. But many have asked me the question what happened to रेणुका after her son rAma restored her to jamadagni's grace. The history states that she indeed redeemed herself. The fierce वीतहव्यस्, protected by the Atreya's prayogas, bent on revenge, breached the defenses of jamadagni and attacked him while he was deep in meditation. While a once fiery archer, the old भृगु was now in meditation and his valiant sons were far away. रेणुका boldly picked up jamadagni's weapons and stood in the path of the वीतिहोत्रस्, तालझन्ग and तुण्डिकेरस् who rushed in with their bows upraised. They struck her their missiles and killed her. It is said she received 21 wounds before falling dead. Then they struck jamadagni with the दत्तात्रेय missiles. jamadagni's brahman power was vaporized by the weapons charged with the atri's brahman and he dropped dead. With that the Atreya had got back the brahman power of the atris that had been sucked out by our fiery ancestor aurva.


