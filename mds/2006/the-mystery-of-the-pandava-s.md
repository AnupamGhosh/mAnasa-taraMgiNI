
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mystery of the पाण्डव-s](https://manasataramgini.wordpress.com/2006/05/04/the-mystery-of-the-pandava-s/){rel="bookmark"} {#the-mystery-of-the-पणडव-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 4, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/04/the-mystery-of-the-pandava-s/ "5:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While the bhArata epic is a central to understanding the rise of the Indian state and the very culture of the Indian people, there are some mysterious issues associated with it which still leave a major gap in understanding this historical period. At its heart the epic represents the expansion of the kuru-पाञ्चाल realm over the whole of Northern India, establish a unified administrative and cultural zone. The sequence of events leading to this as painted in the mbh are thus: 1) We have the rise of the भारतस् under their eponymous founder the great conqueror bharata daushyanti who raises this line of the pUru kShatriya clan to prominence and pre-eminent rulers amongst the Indo-Aryans. 2) The dynastic successors of bharata daushyanti (not necessarily his biological lineage descendent) split up into two powerful states (subsequently the kuru and the पञ्चल) at the time of the brothers पुरुमीढ and अजामीढ. 3) The kuru and the पञ्चल were the pre-eminent rulers of much of north India jostling for territory with each other, as well as other Indo-Aryan states. 4) The period of the great bhArata war, where through marital alliances with पञ्चल, विराट, यादव and other smaller Aryan states a younger branch of the kuru-s, the पाण्डव-s, overthrew their older cousins the main-line kaurava-s. In the bloody bhArata war most north Indian rulers were annihilated, giving the surviving पाङ्डव-s total control of much of this swath of land. Subsequently, their successor janamejaya पारीक्षित through a formal ashvamedha rite and the sarpa sattra marking the destruction the nAga rivals made himself the emperor of the first unified empire of India, marking the efflorescence of the formal shrauta period of vedic culture.

Now in my investigations, the vedic saMhitA-s, ब्राह्मण-s and shrauta सूत्रस् largely confirm this sequence of events but leave one very mysterious gap vis-a-vis the bhArata. Firstly, in the core ऋग् saMhitA and the core parts of atharva saMhita there are no references the later kings of the bhArata . Instead there are allusions to early pUru kings, bhArata and his immediate successors पुरुमीढ and अजामिढ are seen. This suggests that much of the samhitA period predated or overlapped with early history of the pUru clan till the time of original, but not yet formal kuru-पाञ्चल split. The ब्राह्मणस् primarily talk of the core kuru-पाञ्चल period, explicitly using this "hyphenated" term. They also allude to the ancient historical गाथस् praising king bharata दौश्यान्ति as hoary figure, while refering to others like janamejaya पारीक्षित as more immediate or contemporary. In particular they detail the ashvamedha of janamejaya as a contemporary event and an epitome of the formal shrauta performance (e.g. aitareya ब्राह्मण 8.21.3, shatapatha ब्राह्मण 13.5.4.2 and some shrauta सूत्रस्). These ब्राह्मणस् and shrauta सूत्रस् also talk of several characters who figure in the core events of the महाभारत. These include for example: दृतराष्ट्र वैचित्रवीर्य , शिखण्डिन् याज्ञसेन, कृष्ण देवकीपुत्र, व्यास कृष्ण-द्वैपायन (consistently providing patro/matronymics as in the epic) and परीक्षित् the kaurava. The गृह्य सूत्रस् also recall the traditional narrators of the bhArata epic like वैषंपायन and jaimini explicitly in the context of the bhArata (e.g आश्वलायन गृह्य sUtra 3.4). Thus, at the face of it, it may seem that the tradition in the महाभारत is at least consistent with vedic tradition. Yet we see no mention what so ever throughout the vast vedic literature to the पाण्डवस्. Their descendents परीक्षित् and janamejaya are prominent late vedic figures, yet where are the पाण्डव-s in vedic literature? This is especially strange, given that the great epic tries to paint the पाण्डव राजसूय and ashvamedha as key historical events and even models for these shrauta performances. Yet, they are bypassed and those of their descendents are detailed.

While the silence of the vedic texts regarding the पाण्डवस् down to the shrauta सूत्रस् is striking, they are uniformly alluded to in all the major पुराणस्, अष्टाध्यायि and earliest mytho-historical works of the nAstIka matas- jaina and bauddha. This pattern strikes me as odd and requiring an explanation. The point is complicated by another observation of some interest: Several later Indic dynasties have claimed descent from the यादवस् of the bhArata period (e.g. चालुक्यस्, yadu kula राज्पूत्स्, यादवस्, abhiras so on). Yet, the number of Indic dynasties claiming पाण्डव origin are very few. This is especially strange given that their supposed descendents had such a prominent role in formalizing the vedic shrauta ritual which was so closely associated with royal prestige in ancient India. The only major dynasty with such a claim is the पाण्ड्य of Madhurai, whose very name bears this sign.

Simple proposals for solving this conundrum have hardly explained all the complicated issues associated with it. So, I will stop at this point and present some possible theories later.


