
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [yantra गणित](https://manasataramgini.wordpress.com/2006/09/17/yantra-ganita/){rel="bookmark"} {#yantra-गणत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/17/yantra-ganita/ "12:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

All polygons with n>4 are vector polygons because they occur as families of polygons i.e the polygon and its stars (or n-grams)

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/definitions.png){width="75%"}
```{=latex}
\end{center}
```



The considerations described here apply to regular polygons that are chiefly used in yantra construction. All regular polygons are described with the notation P {n,k}. P{n}, where k=1 is the regular convex polygon. Higher k results in stars. The polygons are inscribed in circles where the radius r=1 unit. Each polygon in the family has an edge-length, AB=l1; A'B'=l2 etc. The pairs of polygons for n>=5 are described by pairs of vectors whose diagonal flanking elements=1 (shown in the figure). The number of roots of the characteristic equation of the vector i.e. :\
DET \[Vector]=0 give the number polygons in the family i.e stars and the base convex polygon. Thus the maximum value of k=(n-1)/2 for odd polygons and k=(n-2)/2 for even polygons.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/5and6.png){width="75%"}
```{=latex}
\end{center}
```



Thus for a pentagon and hexagon we have quadratic characteristic equations and 2 polygons each in their families.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/7and8.png){width="75%"}
```{=latex}
\end{center}
```



Likewise for a heptagon and octagon we have cubic characteristic equations and 3 polygons each in their families.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/9.png){width="75%"}
```{=latex}
\end{center}
```



For a nonagon and decagon (not shown) we have 4th power equations and 4 polygons per family.\
Polygon P{n,k} contains within it successively smaller interior polygons including all the lower k polygons of the family i.e: P{n,k-1}, P{n,k-2}...,P{n}. Thus the nonagram P{9,4} include P{9,3}; P{9,2}; P{9}, while P{9,3} contains the remaining two etc. If n and k are relatively prime, e.g. P{5,2}, the n-gram is an unresolved continuous locus. If k is a factor of n e.g P{6,2},P{8,2} or P{9,3} the n-gram is a superposition of k convex regular polygons with number of sides = n/k. Thus P{6,2}= 2 equilateral triangles, P{8,2} = 2 squares and P{9,3}= 3 equilateral triangles.

The most striking property that was the deep rahasya known to the tantrics of yore was the relationship of the edges of a family of vector polygons. It was alluded to by the famed chera magician Narayanan Nambuthiripad. It had practical use if one need to know in advance the amount of metal inlay that was required for a yantra. This property was that the roots of the characteristic equations of give the squares of the sides of the family of polygons. Thus for P{6,k} we have roots of the characteristic equation= 1 and 3, which are respectively the squares of the sides of the hexagon and hexagram. For a P{5,k} polygon we get 3-phi for the pentagon and 2+phi for the pentagram, where phi is the golden ration (there is a typo in the panel 2 figure). These latter values obliquely also relates to the construction of the trapezium altar known as the श्मशान वेदी in yajur vedic parlance. The श्मशान-वेदी trapezium is made by constructing a quadrilateral by removing one vertex of a regular pentagon. The ratio of the parallel sides of the श्मशान vedi trapezium=phi. This is also known as Ptolemy's theorem in the yavana world. The importance of this equation is also in the fact that when a meditation on the yantra is done the साधक's spanda patterns while internalizing the yantra follow the values of the roots of the characteristic equation.

The characteristic equations also have a symmetry with respect to the number of sides of the polygon. Thus the pentagon is x^2-5\*x-5=0; heptagon is x^3-7\*x^2+14\*x-7=0\
This relationship for the heptagon was described by [Kepler](http://manollasa.blogspot.com/2005/08/johannes-kepler.html) with much amazement.


