
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [दण्डिन्'s पूर्वपीठिका of dasha-कुमार-charitra](https://manasataramgini.wordpress.com/2006/04/22/dandins-purvapithika-of-dasha-kumara-charitra/){rel="bookmark"} {#दणडनs-परवपठक-of-dasha-कमर-charitra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/22/dandins-purvapithika-of-dasha-kumara-charitra/ "5:23 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The 3rd stride of विष्णु as well as his snake is the enduring Hindus symbol of the axis that precesses with the passage of काल or time. Right from the ऋग्वेद this axial role of विष्णु is at the fore in all conceptions of him. We have alluded to this a few times before, see: [snake of विष्णु](https://manasataramgini.wordpress.com/2004/12/06/the-snake-of-vishnu/), [the Finnish epic](https://manasataramgini.wordpress.com/2005/09/05/the-finnish-epic/), and more obliquely in [fall of प्रःलाद](https://manasataramgini.wordpress.com/2005/08/07/the-fall-of-prahlada/). The element of the axis and its precession is very much a part of the "core mythology" of विष्णु: 1) the horn of matsya- the new axis being provided for the world after its shift due to precession. 2) The मन्दार mountain as the staff for the churning of the world ocean being borne by विष्णु on his back in the form of कूर्म. 3) वराह raising the "world plane" with his tusks- the world plane being the equatorial circle intersecting the ecliptic. It had sunk because the precession had pushed the old intersection with the ecliptic below the plane. 4) नृसिम्ह emerging by shattering the pillar- the old axis is broken by the flow of time by precession. 5) trivikrama with his three steps stabilizes the new world plane (equatorial circle), the ecliptic's high point (step 2) and the precessing axis (step 3). It is in this context that the great ऋक्ष जाम्बवन्त् is described as circumambulating trivikrama as he took his great strides. This represents the new axis being established and the constellation of the Great Bear (represented as the bear जाम्बवन्त्) making a circle of the north celestial pole. Indeed विष्णु himself is identified with the constellation of the शिशुमार or Draco associated with the old north pole from earlier period of Aryan memory.  As the knowledge of precession is very subtle (even alluded in the opening of the मैत्रयणि ब्राह्मण fragment by शाकायन्य who was well aware of these matters) the third step of विष्णु is secret and was only know to the great kavi-s.

In Indian architecture its depicted more directly on many occasions, but more subtly in the great iron pillar of the guptas. Before the iron pillar was uprooted by the Mohammedans and placed in the vicinity of their giant phallic signpost at Delhi, it graced the now derilict Hindu astronomical observatory at the विष्णुपाद-giri in the udayagiri complex. The iron pillar (as shown by Balasubramanian) was graced by a नक्षत्र-chakra on the top and was known as the विष्णु dhvaja-- depicting the great axis in front of the observatory located atop the hill. Thus, in the immortal pillar did chandragupta II विक्रमादित्य, one of the greatest rulers of bhArata commemorate the step of विष्णु and and world axis.

This was also very much in the mind of दण्डिन् when he composed his पूर्व पीठिका:

[ब्रह्माण्ड छत्र दण्डः]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[शत-धृति भवनाम्भो-रुहो नालदण्डः]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[क्षोणी-नौकूप दण्डः]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[क्षरद्-अमर-सरित्-पट्टिका-केतु-दण्डः]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[ज्योतिश्-चक्राक्ष दण्डस्-]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[त्रिभुवन विजयस्तम्भो ऽङ्घ्रि दण्डः]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[श्रेयस्-त्रैविक्रमस् ते वितरतु विबुध द्वेSइणां काल दण्डः ॥]{style="color:#0000ff;font-style:italic;font-weight:bold;"}

The rod of the umbrella cap of the universe,\
the reed stock for the lotus seat of the one of 100 sacrifices,\
the mast rod of the ship that is the earth\
the rod for banner, that is the heavenly stream (the Milky Way),\
the axis of the rotating star-wheel,\
the rod that is the victory pillar of the 3 worlds, may the foot \[of विष्णु] favor you,\
the rod that is the leg of trivikrama with which he strode to conquer the 3 worlds,\
is the rod of time (death) to the enemies of the devas.

Recall the ऋग्वेद: [अथ अब्रवीद् वृत्रं इन्द्रो हनिष्यन् । सखे विष्णो वितरं वि क्रमस्व ॥]{style="color:#0000ff;"}

Indra spoke as he set out to slay vitra: "friend विष्णु stride widely". In the Germanic world likewise we have the cognate of विष्णु, विडार् (=वितरं; compare with विष्णु=wide) taking his 3 steps before killing the Fenris wolf and the time of the world-end chaos or the राग्नरोक्. दण्डिन् of Kanchipuram was remembering a very ancient motif and artfully weaving his own name into the पूर्वपीठिका of dasha-कुमार-charitra, a literary work beyond compare. The whole work does not survive in its original. While I am told a medieval Telugu translation was made I am not much aware of it to say if it preserves more of the original.

[Balasubramanian and Dass paper on the iron pillar: a must read.](http://www.ias.ac.in/currsci/apr252004/1134.pdf)


