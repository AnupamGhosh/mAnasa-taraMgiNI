
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The sarpasattra: vedic and aitihasic.](https://manasataramgini.wordpress.com/2006/07/16/the-sarpasattra-vedic-and-aitihasic/){rel="bookmark"} {#the-sarpasattra-vedic-and-aitihasic. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/16/the-sarpasattra-vedic-and-aitihasic/ "5:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The sarpasattra as per the इतिहास was the gory sacrifice of janamejaya पारीक्शित done to avenge the death of his father परीक्षित् by the sarpas. The इतिहास states that the interaction between the पाण्डुस्, yadus and the sarpa-s was a long-standing and complicated one. arjuna first fathers a son इरावान्, through the sarpa princes उलूपी, the daughter of kauravya, the son of धृतराश्ट्र ऐरावत. कृष्ण देवकीपुत्र defeats a sarpa king कलिञ्ग according to the हरिवंश on the yamuna. Then arjuna the पाण्डु and कृश्न the yadu attack खाण्डवप्रस्थ the kingdom of the तक्षक the king of the nAga-s and defeat him to seize his land, with the vedic deities from indra to rudra fighting on the side of the sarpas. The पाण्डुस् build their new capital there. In the great war the half-sarpa son of arjuna comes to fight on his side and is killed, whereas तक्षक tries to aid कर्ण in killing arjuna. After the war the sarpa-s avenge themselves on परीक्षित्, called the grandson of arjuna, and kill him. Finally, this is avenged by janamejaya in his sarpa-sattra where he massacres the sarpas. The sarpa-s are saved from complete extinction by deva indra who sends the brAhmin आस्तीक to end the rite of janamejaya. In addition this battle, the भार्गव utanka fights तक्षक in the quest for the earrings of the queen.

In the veda, the sarpa-sattra is mentioned in the पञ्चविंश ब्राह्मण (25.15), the बौधायन shrauta sUtra (17.18) and the बौधायन गृह्य sUtra (3.10). It is said that the kings and princes of the sarpas performed it in order to gain firm grip over over the worlds or for obtaining poison. The sacrificer is asked to perform the sattra if he wishes similarly to gain a grip over the worlds. The सामवेदिc ब्राह्मण briefly describes the rite as having two अतिरात्रस् in the beginning and end of it, and two अग्निष्टोमस् in the middle of it. The only unpaired day in the annual sattra is the विषुवान् day. On the other days a 10 versed recitation is used but on the विषुवान् day the 12 versed chant with the sarpa सामन्स् are deployed. These are the आरण्यगेयगानंस् 2.a.1-10.

This leads us to another mysterious aspect of the sarpa-s found in the vaidika tradition, namely the sarpa-सूक्त-s attributed to the arbuda and his mother kadru, the queen of the sarpa-s (sarpa-राज्ञी). These सूक्त are: 1) RV 10.94 attributed to arbuda the son of kadru directed to the soma-pounding stones to prepare soma for indra. The sarpa arbuda is the foe-destroying snake probably related to the snake-poison arbudi weapon of atharvan tradition. In the RV, indra slew him with a snow-shower or an icicle. 2) RV 10.189 attributed the sarpa-राज्ञी kadru the mother of the serpentine तार्क्ष्य-s and the sun. It dappled cow in the सूक्त might be a metaphor for the dappled सर्पराज्ञी. Who might this सर्पराज्ञी associated with the sun be? The aitareya ब्राह्मण, explaining the deployment of this सूक्त during in the ritual snake-like creeping of the ऋत्क्विक्-s to altar, states that she is the earth (also repeated in the shatapatha ब्राह्मण) because the earth is queen of what creeps. It states that the earth was barren but when she saw this सूक्त, the dappled color entered entered and thus all plants and birds entered her. In the famous sarpa-balI incantation from the यजुष् tradition we hear that the sarpa-s are seen in the realms of light (the rochana; mentioned in above सूक्त) and the rays of the sun (सूर्यस्य rashmi). The same kadru is also offered an oblation with the incantation of kadrave नागमात्रे स्वाहा | in the sarpa-bali and the बोधायन ritual to rudra. Thus, from the context of the सूक्त this serpentine deity appears to be associated with the earth receiving the solar rays and becoming fertile. Thus, the sarpa-s as ऋषि-s and officiants has a wider presence in the veda.

Most remarkable aspect of the vedic rite done by the sarpa-s is the consistently preserved list of officiants seen in both the सामवेदिc and बौधायन texts. The brahmA priest is mentioned as धृतराष्ट्र ऐरावत, while the ब्राह्मनाच्छम्सिन् is तक्षक, and the adhvaryu is janamejaya. The बौधायन texts mention उपरीति तार्क्ष्य (the half brother of the khaga, अरिष्टनेमी तार्क्ष्य). The interesting feature is that the vedic sarpa sattra is done by the sarpa-s in the veda and is not the killing of the sarpa-s as in the इतिहास. It again has several of the circum-पाण्डु characters of the इतिहास except the पाण्डुस् themselves. This again highlights the big question we had asked earlier: Why are the vedic texts silent about the पाण्डुस्? The answer to this question is indeed central to some major historical transitions in India.


