
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [वेदाङ्ग ज्योतिष and other ramblings on early Hindu calendars](https://manasataramgini.wordpress.com/2006/12/02/vedanga-jyotisha-and-other-ramblings-on-early-hindu-calenders/){rel="bookmark"} {#वदङग-जयतष-and-other-ramblings-on-early-hindu-calendars .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 2, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/02/vedanga-jyotisha-and-other-ramblings-on-early-hindu-calenders/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The वेदाङ्ग़ ज्योतिष has been a strikingly difficult text to fathom despite its small size. It is the late vedic text that appears to be a quick and handy guide for calendrical calculations. It has attracted much attention of Western scholars, despite not understanding much of it. Yet they use it as a starting point to suggest how the Hindus were definitely astronomical idiots. This trend set by some of the early Euro-American scholars regarding Hindu astronomy is repeated to this date as a part of their program presenting the picture of "India as the *cul de sac*" (vide Michael Witzel, a German Indologist). The view of the early white Indologists on Hindu astronomy is epitomized by the American Indologist Whitney (the translator of the atharvaveda)'s comments on the VJ:[[ "And when we come to add that the Jyotisha (VJ) has no definable place in Sanskrit literature, or relation to the Vedic ceremonial.. we shall see that this famous datum, which has seemed to promise so much, has caused so much labour and discussion, and is even yet clung to by some scholars as the sheet-anchor of ancient Hindu chronology, is nothing but a delusive phantom."]{style="color:#008000;"}\
]{style="font-style:italic;color:#cc0000;"}While this and succeeding views of the white Indologists and their Japanese imitators have acquired wide currency, Hindu scholars have labored on interpreting the text correctly to a great measure. However, to give where credit is due an early European scholar Thibaut was fairly effective in interpreting a number of verses. The first Hindu scholar to make a major in road was S.B. Dikshit, who labored on this text not far from the place where I spent a good part of my youth. Dikshit's work unfortunately did not acquire wide readership because it was written in the Maharatti language. He was followed by the Northerner Chote Lala and Sudhakara Dvivedi, the paNDita from Kashi, who successfully interpreted a little more of the text, though they were locked in conflict between themselves. The great B.G. Tilak then further clarified some points. The final assault on the text along with a modern English translation was provided by the स्मार्त savant श्री kuppanna shAstrI, who more or less interpreted most of the text. If we can today understand some of the elegant devices of the VJ, it is only because of kuppanna shAstrI's enormous scholarship, setting the true standards for the kind of scholarship needed to tackle complex texts.

The vedic calendric knowledge which provides the background for the VJ needs some description. There is evidence that the vedic kavi-s were fully aware of the precession of the equinox, and they had memories of observations from the remote Indo-European past of at 4000 BCE or beyond. From the early times the Arya knew of the year being approximately 365 days-- the सामिधेनी hymns contain 360 syllables in the form of 15 gAyatrI-s + the oM भुर्भुवस्वरों forming the year by supplying the remaining days and its fraction. Two great vedic shrauta masters, the पञ्चाल prince babara प्रावाहणि and सार्वसेनि shaucheya declare the logic behind the पञ्चरात्र sacrifice. They say that the year, which is the ya\~jna, is complete only by adding the 5 days. They say that 4 days is too few while 6 days is two many so to complete the year we add 5 last days to the year . This is how the 365 day approximate year is laid out (TS 7.1.10). Right in the ऋग्वेद the शुनःशेप, the son of ajigarta notes that the intercalary month is used to correct for the imperfect match of the lunar and solar years (RV1.25.8).

Thus, the calendrical system presented by the VJ is not something present in isolation or borrowed from some Middle Eastern source (as some white authors foolishly propose) but it was based on what was considered common knowledge in the vedic sacrificial ritual. There are two extant ज्योतिष-s the yajur and ऋक् versions that differ some what in their contents but are over-all consistent. There is an अथर्वण ज्योतिष, but it is unrelated and mainly a manual for reckoning muhurta. Dating by precession gives the date of the VJ as being around 1300 BC because the it states that the winter solistice was in the beginning for श्रविष्ठा and summer in the mid-point of आश्लेषा. This is important because in the मैत्रायणि ब्राह्मण, शाखायन्य tells king बृहद्रथ that the winter solistice was in the middle of श्रविष्ठा (MBU 6.14). Thus, the lagadha was clearly aware of the change from the epoch time little before his times. Subsequently, the Hindu encyclopedist वराहमिहिर, who was an acute observer, writing around 550 AD states that the spring equinox fell 10′ East of Zeta Piscium in रेवती. An important observation made by Kuppanna Shastri concerns the intermediate period. We have a text of the nagna nAstika-s, termed sUrya-प्रज्ञाप्ति, where the same system of calendrics as the VJ is expounded. But the jaina-s correct the winter solstice to श्रवण, suggesting that they were after the VJ in the period when you would actually expect the jaina-s to be around. Thus, we have a continuous system of precessional corrections even in the period around the VJ which can be traced back to vedic antiquity. Hence, it is clear that the VJ was indeed composed in the period we can astronomically infer.

Interestingly, the total variation in day-light time is given as 6 मुहूर्त-s which corresponds approximately to the latitude of 35° N. This will be north of कुभा (kabul in modern Afghanistan), and might imply that the observations were inherited from the time the Aryas occupied the BMAC archaeological sites.

**----------------------**

**Some basics of the VJ:**\
The vedic measures of time as per VJ:\
[5 गुर्वक्षर-s \[long syllables]= 10 mAtrA-s\
]{style="font-family:courier new;"}[= ]{style="font-family:courier new;"} [1 काष्ठ\
]{style="font-family:courier new;"}[124 काष्ठा-s]{style="font-family:courier new;"} [=]{style="font-family:courier new;"} [1 कला\
]{style="font-family:courier new;"}[10 1/20 कला-s]{style="font-family:courier new;"} [=]{style="font-family:courier new;"} [1 नाडिका*\
]{style="font-family:courier new;"}[2 नाडीका-s]{style="font-family:courier new;"} [=]{style="font-family:courier new;"} [1 मुहूर्त\
]{style="font-family:courier new;"}[30 मुहूर्त-s]{style="font-family:courier new;"} [=]{style="font-family:courier new;"} [1 अहोरात्र (civil day)\
]{style="font-family:courier new;"}[366 days]{style="font-family:courier new;"} [=]{style="font-family:courier new;"} [1 संवत्सर= 12 mAsa-s\
]{style="font-family:courier new;"}[=]{style="font-family:courier new;"} [6 ऋतु-s= 2 ayana-s\
]{style="font-family:courier new;"}[5 years ]{style="font-family:courier new;"} [= ]{style="font-family:courier new;"} [1 yuga*]{style="font-family:courier new;"}\
This progression is an old vedic system we have in the तैत्तिरीय AraNyaka: "कला मुहूर्ताः काष्ठाश् चाहोत्राश् cha सर्ववशः | अर्धमासा mAsA ऋतवस् संवत्सरश् cha कल्पन्तां ||"

  -  Time is kept using a water-clock. The volume of water of weight 50 pala-s, at room temperature, is one Adhaka (a volume measure).\
4\*Adhaka = 1 droNA \[a volume]; 1/16 of Adhaka = 1 kudava;\
In a standard vedic water-clock 1 droNa- 3\*kudava is that volume of water that drains in one 1 नाडिका. Thus, the vedic water clock drained in 24 minutes or 1 नाडिका. The water-clock is mentioned in the काल सूक्तं of the भार्गव-s in atharva veda. Thus, both the time units and the water clock find mention in the veda, suggesting that the systems mentioned in the VJ was a standard aspect of the vedic time keeping.

* A yuga is defined as the "pairing (from the root yug- same as English yoke, Germanic \*yuminaz=Gemini)" or coming together of two celestial bodies, or their nodes or their apogees in the same place on the ecliptic. Approximately the moon and the sun meet in the same asterismal position in 5 years. Hence this is the basic yuga in vedic parlance. The years of the yuga were named in the veda as: संवत्सर, parivatsara, इदावत्सर, anuvatsara and idvatsara.

The yuga concept clarifies why the संवत्सर is described as 366 days, even though, even babara प्रावाहणि and सार्वसेनि shaucheya already knew in the days of the yajur veda that 366 was too much. In a yuga there are 366*5= 1830 days (yuga value) and this gives the required approximation for the yuga definition that is meeting of sun and moon in a नक्षत्र. It also gives a base to derive a number of other critical values for the period of the yuga easily:

 1.  Number of risings of श्रविष्ठा above the horizon (the नक्षत्र at the winter solstice) = yuga + 5 = 1835

 2.  Number of moon rises = yuga-62=1768

The Number of नक्षत्र-s traversed by the sun in 1830 days is 135. The number of ayanas of the moon 1 less than that number= 134.

The value of the yuga also can be used to relate to the following easily, as explained in the yajur ज्योतिष 31:

 1.  Number of सावन months in 1 yuga (that is the traditional months of the vedic ritual) = 61= 30 days

 2.  Number of synodic months in a yuga, i.e. the period between two new moons in a yuga = 62 = 29.51 days, a reasonable approximation (modern value= 29.530).

 3.  Number of sidereal months in yuga = 67 = 27.31 (modern value=27.32).

Given any 3 elements of the yuga that are not completely dependent on each other we can get every other element. This is an important computational device of the VJ. Thus, for example:\
Number of sidereal days (i.e. the time interval between two successive risings of a star) in a yuga = yuga +5 = 1835\
Thus, we have ratio of sidereal day : civil day = 0.99727 (modern value= 0.997269)\
So one can see that the for a vedic ritualist at 1300 BC the VJ gives decent quick approximations for key calenderical values using the yuga concept, and can hence be hardly called a primitive work.

***-----------------------------------------***

***titihis and नक्षत्र's for some key days***\
The युगारम्भ is given as when sun and moon are in the नक्षत्र of श्रविष्ठ in the bright fortnight of the month of magha. Then the determination of the पक्ष (कृष्ण/shukla- k/s) and approximate नक्षत्रस् (n) and tithis (t) at the beginning of each of the ayana-s of the sun in the yuga is given:\
1 n=श्रविष्ठा, t=1s; 2 n=chitrA, t=7s; 3 n=आर्द्रा, t=13s ;4 n=पूर्व-प्रोष्ठपदा, t=4k; 5 n=अनुराधा, t=10k; 6 n=अश्लेषा, t=1s; 7 n=ashvini, t=7s; 8 n=पूर्वाषाढा, t=13s; 9 n=uttara phalguni, t=4k; 10 n=रोहिणि, t=10k;\
These are days on which the solsticial sacrifices are performed.

The विषुव days or the equinoctial days are the other important ritual days. A formula is given for the number of पक्ष-s(p) and tithi-s(t) having elapsed from the beginning of the yuga to the nth विषुव (n):\
6*(2\*n-1)*(p+.5\*t); thus for विषुव 1 we have 6*पक्षस्+3\*tithis or it occurs on त्रितीय.\
The विषुव is declared as occurring in the shukla-पक्ष at the end of the तृतीय, नवमी and पौर्णंअस्य.

Rule for the tithi on which a ऋतु begins is thus explained: The number of ऋतुस् in a yuga is 30. The first ऋतु of the yuga is shishira, in the month of tapas, as stated in the yajurveda (TS 4.4.11.1), begins on shukla प्रथमा. The next ऋतु begins two tithis later on shukla त्रितीय, the next on shukla 5 panchami and so on till the 8th ऋतु begins on पौर्णमास्य. Then the ऋतु-s continue by the formula (1+2*(n-1)) modulo 15

**---------------------------------------**

**The logic of the peculiar division 124**\
Given that there are 62 synodic months in a yuga, we have 124 पक्ष-s in a yuga. Hence to keep the calculations as whole numbers the vedic day is divided into 124 parts or अंश-s. From the earlier table we have a day having 603 कला-s=74722 काष्ठ-s, the latter being divisible by 124. [1 काष्ठ=1.1563 seconds; 1 कला= 2.388 minutes; part of the day/amsha= 11.612 minutes].

The part of the day when the पक्ष ends is critical to determining the day when the sarcfices or इष्टि-s are performed. The VJ gives the following procedure to figure out the part of the day when a पक्ष ends:\
The duration of each पक्ष = yuga/124= 15 days -- 30 parts (amshas) of the day.\
Thus duration of each tithi =1 day -- 2 parts of a day.\
To find the part of the day the nth पक्ष ends:

 1.  For the nth पक्ष obtain x= n modulo 4

 2.  If x=1 then y=n+93; if x=2 then y=n+62; if x=3 then y=n+31; if x=0 then y=n

 3.  The number of parts of the day when the पक्ष ends= modulo (y,124).\
If the पक्ष ends before 31 parts then it ends before civil mid-day.\
E.g. 37th पक्ष- x= 37 modulo 4=1; y=37+93=130; So the पक्ष ends at 130 modulo 124=6. Thus it ends around 1 hour and 9.6 minutes of the day.

So one can see that the for a vedic ritualist at 1300 BC the VJ gives decent, rough and ready approximation for key values using the yuga concept, and can hence be hardly called a primitive work.


