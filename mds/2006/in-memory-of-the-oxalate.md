
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In memory of the Oxalate](https://manasataramgini.wordpress.com/2006/05/17/in-memory-of-the-oxalate/){rel="bookmark"} {#in-memory-of-the-oxalate .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/17/in-memory-of-the-oxalate/ "5:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A recent conversation brought to mind the our famed oxalate experiment. Oxalic acid first caught my fancy in the summer vacations of 6th class. I was intrigued by it and sought to extract if from the sorrel that grew abundantly in my late grandfather's garden. Then my greater aunt provided me with that lost text of chemistry, which introduced me to the possibility of making stains with silver oxalate. One day 3 years later I had gone to pay obeisance to वृषध्वज at the intersection beyond the buffalo pond. By some chance while return I ran into Mis-creant who was eager to have a look through my microscope. We decided to make the silver stain but ended up with an interesting result :-). I reasoned that silver oxalate or silver tartrate should be ideal substances for the purpose. Mis-creant had smuggled silver nitrate for us through a "senior" contact. Having made sure that there was no one at home we began our experiments. The nitrate was mixed with a [mixture ]{style="font-style:italic;color:rgb(255,0,0);"}of organic acids and we got a precipitate that we decided to heat in the kitchen gas stove to remove water. We left it there and heard a several minutes later a peculiar noise. We also noticed a strange discoloration on the kitchen ceiling which intrigued my parents to put it most mildly for a long time to come. The rest of the experiment is left to one's imagination.


