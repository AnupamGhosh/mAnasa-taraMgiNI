
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The southward passage](https://manasataramgini.wordpress.com/2006/04/09/the-southward-passage/){rel="bookmark"} {#the-southward-passage .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 9, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/09/the-southward-passage/ "7:37 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[*सूर्यं cअक्षुर् गच्छतु वातं आत्मा द्यां cअ गच्छ पृथिवीं cअ धर्मणा ।\
अपो वा गच्छ यदि तत्र ते हितम् ओषधीषु प्रतितिष्ठा शरीरैः ॥*]{style="color:#99cc00;"}\
(kaliyuga 5107, month: chaitra, shukla एकादशि, magha)\
Having reached the end of his span alloted by the son of विवस्वान् he has joined the line of the long past ones under the star of the manes. He has gone the way of the great and first of the atharvaN-s, भृगु. He has joined the line of च्यवान, दधीचि, बृहद्दिव, अप्नवान, vatsa, the fiery aurva, ऋचीक, jamadagni, rAma, kabandha, सोमाहूती, वाजरत्न, सोमशुष्म, vidarbha, sumantu, माण्डुक्य and other great atharvaN-s. But afterall he has met his gati in the upward journey of the sun.

With singing of the सामन्स् may he go southwards to reach his destination in sUrya, in वात, in dyaus, in पृथिवी, in the waters and the plants. And they hope the tree of भृगु may grow on...


