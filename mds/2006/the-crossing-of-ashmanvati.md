
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The crossing of अश्मन्वती](https://manasataramgini.wordpress.com/2006/11/26/the-crossing-of-ashmanvati/){rel="bookmark"} {#the-crossing-of-अशमनवत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 26, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/26/the-crossing-of-ashmanvati/ "4:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/x/blogger2/6438/855/320/606078/अश्मन्वती.jpg){width="75%"}
```{=latex}
\end{center}
```



In the days of our earliest memories we were transfixed by the sight of that part of the sky which has inspired and continued to inspire all discerning beholders. We drew that little cluster, the mothers of our god, and the three stars in a line, which form the arrow of The god. All this got imprinted in our minds. Over the ages the language of myth has preserved the passage of time through this region of the sky and beyond. Tilak first and de Santillana and von Dechend thereafter (the latter two are possibly plagiarists of Tilak, who failed to cite him appropriately) realized some of the underlying celestial secrets. We continue this journey of exploration through this part of the heavens.

We first briefly return those hymns of sauchika अग्नी, which is verily a repository of astronomical lore connected with precession in the typical language of the vedic mantra शास्त्र (as a tangential aside, those who understand the vedic mantra शास्त्र code, even if only in fragments, will see the concealment so typical of the tantric mantra शास्त्र that followed):

[साध्वी मकर् देववीतिं नो अद्य यज्ञस्य जिह्वाम विदाम गुह्याम् ।\
स आयुरागात् सुरभिर्वसानो भद्रामकर् देवहूतिं नो अद्य ॥ ऋV १०।५३।३]{style="color:#99cc00;"}

Today he made the food-offerings to the deva-s effective;we have now found the secret tongue of the यज्ञ. Today he has come, robed in good-scents, extending life, and had made our oblations to the deva-s effective. \[This celebrated mantra is recited in the midst of the long chant "you O agni, who is rudra ..." when the fire is brought to the main altar borne in a pot by the adhvaryu. The ब्राह्मण text of the तैत्तिरीयस् explains that the mouth the sacrifice, which is the year, is the equinotical colure, which was then in कृत्तिका. Hence, by analogy the finding of the tongue of the sacrifice is finding of the new position of the equinotical colure, which is agni, after it had precessed from its old position. This was alluded to in the [first सौचीक अग्नी hymn](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/)]

[तन्तुं तन्वन् रजसो भानुमन्विहि ज्योतिष्मतः पथो रक्ष धिया कृतान् ।\
अनुल्बणं वयत जोगुवामपो मनुर्भव जनया दैव्यं जनम् ॥ ऋV १०।५३।३]{style="color:#0000ff;"}

Spinning the thread, follow the sun in the heaven, guarding the stellar path that was found \[made] due to knowledge. Weave the regular pattern praised in the sacred rites, become manu, bring forth the heavenly folks. \[In the कौशीतकि tradition of the ऋग्वेद the hotar chants a mantra on the महाव्रत and विषुवान् days in which it is said that the sun is a spider that spins a web going six month north and six months south. The spinning in the first half of the mantra alludes to this, while the weaving in the second half alludes to the coming of days and nights in a regular pattern, which is also mentioned in the कौशीतकि mantra- KB 19.3. The gods are asked to watch upon the path of the sun that was earlier "disrupted" by precession]

[अक्षानहो नह्यतनोत सोम्या इष्कृणुध्वं रशना ओत पिंशत ।\
अष्टावन्धुरं वहताभितो रथं येन देवासोअनयन्नभि प्रियम् ॥ ऱ्‌V १०।५३।७]{style="color:#0000ff;"}

O soma drinkers, fasten that which is to the tied, prepare the ropes and decorate. Bring forth the 8 seats of the chariot and with which the gods lead forth that which we love. \[The deeper meaning of this mantra is still shrouded in mystery- e.g. why eight seats? Yet the axle and ropes here allude to the world frame being set in place]

[अश्मन्वती रीयते सं रभध्वम् उत्तिष्ठत प्र तरता सखायः ।\
अत्रा जहाम ये असन्न् अशेवाः शिवान् वयम् उत् तरेमाभि वाजान् ॥ ऱ्‌V १०।५३।८]{style="color:#0000ff;"}

अश्मन्वती flows by. Hold tight together, keep your self erect and cross \[the river], friends. There let us leave that which is not good, and we cross over to that which is auspicious. \[This is the single most important element of the सौचीक agni hymns in terms of a date. It clearly mentions crossing of the river अश्मन्वती to the other side where the auspicious lies. This means that the new equinoctial position laid on the other side the अश्मन्वती- the heavenly river the Milky Way. I prepared a simulacrum of the skies that inspired this hymn using the Cartes du Ciel program, the best freeware for the purpose. One can note that if the Milky was crossed by the equinoctial colure as indicated in the picture, the period was likely in a period after 4000 BC, possibly around 3700 BC by which the crossing was definitely over].

The final mantra of these series while extremely obscure gives a clue of what the period might have been:\
[गर्भे योषाम् अदधुर् वत्सम् आसन्य् अपीच्येन मनसोत जिह्वया ।\
स विश्वाहा सुमना योग्या अभि सिषासनिर्वनतेकार इज्जितिम् ॥ ऱ्‌V १०।५३।११]{style="color:#0000ff;"}\
The hymns appears to state that the ऋभु-s \[not named directly but indicated by brahminical tradition] secretly placed the young lady in "womb" and the calf in the jaw. Now this calf if the sun at the New Year, the jaw, as de Santillana and von Dechend show, is Taurus. Thus we have the crossing of the Milky Way, अस्मन्वती to enter Taurus. The योषा being placed in the garbha is more obscure. Possibly it means Virgo, the yuvati being deposed from her place at the top of the ecliptic and being moved to the descent \[Tilak mentions the Virgo full moon in the solsticial year beginning at an early era].

But the same celestial sector also appear more dramatically in the mythosphere as the place where rudra slaughters prajApati with his 3-fold arrow. The related issues we shall narrate if we get a chance later and then return to complete the tale of andhaka where an element of the astronomical language of myth survives. But for now we conclude with a much later litany to show how firmly planted these events were in Hindu thought.

पुष्पन्दन्त's shiva mahimna:\
[वियद् व्यापी तारागणगुणित फेनोद्गम रुछिः]{style="color:#0000ff;"}\
[प्रवाहो वारां यः पृषतलघु दृष्टः शिरसि ते ।]{style="color:#0000ff;"}\
[जगद्-द्वीपाकारं जलधि वलयं तेन कृतमिति]{style="color:#0000ff;"}\
[अनेनैवोन्नेयं धृत महिम दिव्यं तव वपुः ॥१७॥]{style="color:#0000ff;"}

The heavenly stream (the Milky Way) envelops the sky, with its beauty enhanced by the illumination of the foam by the groups of stars. It forms a ring around the universe making it look like an island. The same current is a mere trickle on you head. This itself shows the greatness of your celestial form.

Then again:\
[प्रजानाथं नाथ प्रसभमभिकं स्वां दुहितरं\
गतं रोहिद् भूतां रिरमयिषुमृस्ष्यस्य वपुषा ।\
धनुष्-पाणेर्यातं दिवमपि सपत्राकृतममुं\
त्रसन्तं तेऽद्यापि त्यजति न मृगव्याधरभसः ॥२२॥]{style="color:#0000ff;"}

O lord, once prajApati was attracted to his own daughter. When she fled becoming a red deer he took the form of a stag and chased her. You as a hunter, with a bow in hand \[went after him]. Struck by your arrow and greatly terrorized he is in the sky \[as a constellation].

In the picture above all these scenes are seen on the celestial dome.


