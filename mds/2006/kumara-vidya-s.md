
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कुमार विद्या-s](https://manasataramgini.wordpress.com/2006/10/10/kumara-vidya-s/){rel="bookmark"} {#कमर-वदय-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 10, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/10/kumara-vidya-s/ "5:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/kumara_tryakSharI.1.png){width="75%"}
```{=latex}
\end{center}
```



Of the मूर्ति-s of कुमार \[Note 1] that are worshiped by the educated कौमार साधक there are few having a special significance. The first of these is of great import for a dvija in devoted the याज्ञीकी perfection. It is the form known as अजारूढ. Here the great lord of the beautiful षष्ठि is meditated upon in the bindu of the मण्डल thus:\
द्विमुखं चाष्ट-बहुं cha श्वेतं वा श्यांअ-स्कन्धरं |\
स्रुवाक्षमालां खड्गं cha स्वस्तिकं दक्षिणे kare ||\
कुक्कुटं खेटकं वज्रं Ajya-पात्रं tu वामके |\
agnihotra-vidhi देवं agni-जात स्वरूपकं ||

His mantra in this form is:\
OM ह्रां ङं ||

This form of कुमार has 2 heads, 8 arms and is white in complexion with a dusky neck. In his right hands he holds a sruva, rosary, a straight-sword and shows the svastika mudra, in his left hands he holds a cock, a shield, a vajra, and Ajya-पात्र. He is visualized as making oblations in the fire rite. He is seated on a large well-fed goat.

Beside him is standing his गण वीरबाहु with a sword and shield. In the first आवर्ण is present the large Arya-deva, mounted on his horse, embracing the shapely goddesses, पूर्ण and पुष्कला, with enchanting smiles. He holds a वज्रदण्ड and a पट्टिशि, the long flexible razor. shAstA and his wives are surrounded by a band of hideous ghosts of skeletal forms. In the next आवर्ण stand two terrifying agents of skanda, namely स्कन्दापस्मर and skanda-graha. They both wield tridents are jet black in color. Outside his मण्डल, नारद the काण्व ऋषि is performing worship of कुमार.

In the गाङ्गेय form he is meditated upon thus:\
eka-वक्त्रं त्रिनयनं दाडिमी-kusuma-प्रभं |\
करण्ड-मकुटोपेतं कुक्कुट-dhvaja धारिणं ||\
नीलोत्पल-धरं vande मकरारूढं अवययं |\
परशुं पूर्ण-कुंभं cha savyahaste tu धारिणं ||\
अरणिं कुक्कुटं वामे गाञ्गेय-sama-रूपकं |\
His mantra in this form is:\
OM ह्रां घं ||

He is mediated as having a single head with 3 eyes of the deep scarlet color of a pomegranate flower. He bears a braided turban on his head and beside him is his cock-banner. He is adorned with blue water lilies and he is seated on a gharial. In his right hands he holds an axe and full pot, whereas in his left hands he holds a cock and the अरणि used to generate fire for the vedic rite.

In the first आवर्ण is present the beautiful riverine goddess जाह्नावी. In the next अवर्ण is present the pretty अरण्यानी of green color with a wreath of wild flowers on her head. She holds a fresh twig in her hand. In the next आवर्ण are present a series of goddess like सरमा, vinAtA, kadru and the like with heads of a bitch, female eagle, cobra etc.

The saurabheya form of कुमार is the form in which he behaved [roguishly,](http://manollasa.blogspot.com/2004/08/roguish-karttikeya-and-wives-of-gods.html) bewitched the goddesses and dallied with them. Hence his form is described accordingly with the accouterments of ratipati with which शृङ्गार is inflamed. He is meditated upon thus:\
chatur-वक्त्राष्ट-नयनं भुजाष्ट कमलासनं |\
कुन्ञ्चितं वाम्पादं cha सुस्थितं दक्षिणं पदं ||\
shaktyutpate पुष्प बाणां अभयं दक्षिणे kare |\
वज्रं चेक्षु dhanush-शूलं वरदं वामके दधं ||\
padma पुष्प-निभं chaiva saurabheya स्वरूपकं |

He has 4 heads, with 8 eyes, and 8 arms and is seated on a lotus. His left leg is bent while his right leg is straight. He is adorned with a spear, flower arrows, and the abhaya mudra in his right. In his left he holds a vajra, sugar-cane bow, trident and the varada mudra. His color is that of a red lotus.\
His formula is:\
OM क्लीं घं धूर्ताय नमः ||

In the first आवर्ण dwells the shakti of his brother विशाख drawn to skanda in amorous delight. In the next आवर्ण are circles of goddesses of incomparable beauty with the girdles around their waists splitting and their garments slipping off as they run in passion towards saurabheya skanda.

In the skanda-स्वामिन् form of कुमार he is flanked by two गण's on either side: On his left is स्रौष (a manifestation of shiva) and on his right is राज्ञ (a manifestation of sUrya) \[Note 2]. In his four arms he holds a spear, bow, vajra and arrow. He is mediated upon thus:\
eka-वक्त्रं त्रिनेत्रं cha hema-ratna-किरिटिनं |\
chatur-bhujair vajra shakti shara-कार्मुक धारिणं||\
udyat-भानु-सहस्राभं cha shukla-यज्ञोपवीतिनं |\
भूत-वेताल-गणाकीर्णं राज्ञ स्रौष सेवितं ||

He is worshipped with the mantra:\
OM ह्रां स्वामिने स्वाहा ||

Surrounding him in the आवर्ण are hordes of पिषाच-s and वेताल-s.

Note 1: The text detailing the उपासन of the कुमार मूर्ति-s is the obscure स्कन्दार्चन-kalpa-druma of which I am unaware of any complete manuscript though the hand-written transcripts, including those fragments used for the above account, amount to its core mantra expositions. The shri-tattva-nidhi of the Wodeyar of Mysore gives related कुमार-मूर्ति-s to those of SAKD with identical iconographic details although it lacks the form with स्रौष and राज्ञ. The कौमारागम also provides descriptions of some of these forms but they are very distinct in iconography. The forms described in the siddhAnta tantras- कामिक, suprabheda, कारण and amshubheda are also distinct from these. These forms constitute one form of worship, which is distinct from the mahachakra नीरूपणं of the great and mysterious मयुरशिखा and गौहास्फन्दा.

Note 2: Here the two attendants of कुमार indicate syncreticism with Iranian deities. In the Iranian pantheon mithra is flanked similarly by रश्णु razishta and sraosha, who are the cognates of राज्ञ and स्रौष of the Hindu world. This रूप of कुमार with his two attendants is also mentioned in the भविष्यत् पुराण.


