
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [naming of the initiates as per the classic siddhAnta tantras](https://manasataramgini.wordpress.com/2006/10/07/naming-of-the-initiates-as-per-the-classic-siddhanta-tantras/){rel="bookmark"} {#naming-of-the-initiates-as-per-the-classic-siddhanta-tantras .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 7, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/07/naming-of-the-initiates-as-per-the-classic-siddhanta-tantras/ "4:03 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The siddhAnta tantrics receive their names as per a fixed procedure: After the student receives mantra दीक्ष from the preceptor, s/he is taken to the central kumbha in which shiva has been invoked earlier. Then, the student casts flowers which s/he is holding in his hand on the kumbha. His name prefix is chosen based on a name or attribute of the पञ्चब्रह्म मूर्ति in whose direction the flowers fall. Top:ईशान, West: सद्योजात, East: tatpuruSha, South: aghora; North: वामदेव.

If the student is a ब्राह्मण he receives the suffix shiva (e.g. varma-shiva, aghora-shiva, ईशान-shiva etc), if he is a kShatriya its is deva (e.g. bhoja-deva, trilochana-deva, इशान-deva etc.), if he is a vaishya or शूद्र a suffix of गण is added (e.g मूर्तिगण, शंकर गण etc.). If the student is a woman then she receives and additional suffix shakti (e.g .त्र्यंबकदेवशक्ति).


