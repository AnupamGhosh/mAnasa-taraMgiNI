
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [भट्ट jayanta the पैप्पलादिन्](https://manasataramgini.wordpress.com/2006/04/21/bhatta-jayanta-the-paippaladin/){rel="bookmark"} {#भटट-jayanta-the-पपपलदन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/21/bhatta-jayanta-the-paippaladin/ "5:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I have always felt a close affinity to भट्ट jayanta, the great scholar from कश्मीर. He lived in the 800-900s of CE and was associated with the dictatorial king shankaravarman who कल्हणा describes as a corrupt despot. कल्हण informs us of his lack of education and culture by stating that this king spoke only apabhramsha, a language fit for drunkards (I told our Hindi teacher these very words, which she has not forgotten since :-), I hear) . Some how I have felt a unity with jayanta, though we are separated by space and time yet resonate. We have some biographical information about him from his son's notes and his own personal allusions. He was of the clan of the भरद्वाजस् and his ancestors were from the vanga country in the east. Then, from vanga they migrated to कश्मीर during the 600s. His clan followed the पैप्पलाद shAkha of the atharva veda and were ardent shrauta ritualists. The one of his ancestor's shakti-स्वामिन्, a worshipper of कुमार, was appointed as an officer in the court of the great ललितादित्य. Hailing from this long line of brahminical scholars, jayanta was not surprisingly a boy genius. At the age of ten he composed a comentary on पाणिनि, which was so lucid that he acquired the title of nava वृत्तिकार even before he had completed his education. He subsequently went on to master the atharva veda and its rituals, various dharma शास्त्रस्, and various tantras. His personal deity was rudra, but he was clearly a स्मार्त rather than sectarian. He soon became a great exponent of nyAya and came up with the concept that every thing that is "knowable" should also be formally definable. As the brahman is ultimately knowable it must be formally definable, unlike the advaita view that is knowable but indefinable. He then went on to build nyAya as the core philosophical system, and believed that the whole system of शास्त्रैc interpretation should rest on nyAya rather than advaita. He developed the concept of the atomic nature of sense perception. However, throughout he voiced his firm support for the importance of vedic ritualism over sectarian streams like shaiva and vaiShNava matas.

He ran a popular school with several students and composed 3 majors works on nyAya, of which the nyAya मङ्जरि and nyAya कलिका survive. He also wrote an interesting play for his students termed आगमडम्बर, which is a subtle satire on various दर्षनस् and their proponents. He is said to have composed his magnum opus nyAya मङ्जरि when he was sent away from his home and school to serve in the khasa country which was conquered by his king the despot shankaravarman.


