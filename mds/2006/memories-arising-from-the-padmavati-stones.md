
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [memories arising from the पद्मावती stones](https://manasataramgini.wordpress.com/2006/09/24/memories-arising-from-the-padmavati-stones/){rel="bookmark"} {#memories-arising-from-the-पदमवत-stones .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 24, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/24/memories-arising-from-the-padmavati-stones/ "6:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/padmavati2.jpg){width="75%"}
```{=latex}
\end{center}
```



As mentioned earlier in my explorations of the महरट्ट country with शूद्र-श्रेष्ठ, S and Mis-creant I came across several aniconic पद्मावती stones painted orange with a lead paint. However, in the exploration of Satmala we did see a couple of iconic पद्मवती images. One of them was still in worship and another was largely in neglect in a dung-ridden area. This image was strange in having a nAstIka arihant inscribed on her. A drAviDa nअस्तीक savant brought to my attention that they worshiped पद्मावती as a shasana देवी, that is an agent of a nAstIka tirthankara. He gave me a print of the jaina version of the deity that resembled the weathered image in part. This suggests that पद्मावती was worshiped by nAstIka-s as well as अस्तीक-s and tribes in that region. I wonder if the पद्मावती at Venkatadri is also a syncretized version of the old पद्मावती widely worshiped throughout the Deccan in the early medieaval period.

So what her origins? We get a clue for this in the archaic section of the gobhila स्मृति (1.11-12) that mentions that 13 mothers are worshiped:

 1.  गौरी (wife of rudra) 2) [पद्मावती ]{style="font-weight:bold;font-style:italic;"}3) शची (wife of indra) 4) & 5) मेधा and sAvitrI (the सरस्वती derived "veda" goddesses) 6) & 7) जया and विजया 8) देवसेना (wife of skanda) 9) & 10) स्वाहा and स्वधा (wives of agni) 11) धृति 12) तुष्टि 13) पुष्टि (the last 3 may or at least the last 2 may be लक्ष्मी like manifestations)\
Thus there was a distinct पद्मवती in an ancient list of goddesses.


  - 0-0-0-0-

As I stood atop the massif of the nine नाथा-s in the Satmala the wind was howling fiercely in the late evening hour. Various animals and birds made loud howling noises. Then to my mind came the yajur-vedic mantra:\
ehy अवक्रामन्न् áshastI rudrásya gÁणपत्यान् मयोभू́r éhi|\
urv àntárikSham ánv ihi svastí गव्यूतिर् ábhayAni कृण्व्áन् ||


