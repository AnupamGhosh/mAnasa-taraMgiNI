
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The AkhyAna of skanda-पत्नी](https://manasataramgini.wordpress.com/2006/10/19/the-akhyana-of-skanda-patni/){rel="bookmark"} {#the-akhyana-of-skanda-पतन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 19, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/19/the-akhyana-of-skanda-patni/ "5:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[ॐ ह्रीं षषठी देव्यै स्वाहा ॥]{style="font-weight:bold;font-style:italic;color:#99cc00;"}

The wandering सूत said: "Listen to the wonderous AkhyAna of skanda-पत्नी that relieves unborn and young kids from troubles. In days long past I was wandering in the fringes of the magadha, when I stumbled into the mighty brahmin, kashyapa, the son of मरीची. I asked him to narrate to me the tale of the wonderous exploits of skanda and his agents.

kashyapa said: "Listen सूत this ancient story of the great god कुमार, the commander of the deva forces and his agents. In the ancient kalpa, after the devas had drunk the अमृत, the asura-s smarting due to their outwitting sought to avenge themselves. The gods have won account of वाक् they said. We too shall strive with our वाक्, declared dundubhi the lord of the asuras. All the asuras put forth their mighty वाक् simulataneously and from the asura-वाक् was born the deadly asura-कन्या, दीर्घ-जिह्वी. The asuras then attacked the gods and a fierce battle broke out. The devas fought with their ranks comprised of the vasus, the rudras and Adityas, and their commander-in-chief being the six-headed skanda. He had with him his brothers and गण-s lead by shAkha, विशाख, nejamesha and the bull-headed nandI. At first the deva assault drove back the asuras to पाताल, but then they let out their secret agent- दीर्घ-जिह्वी. Of black color and with many strong arms and a long lolling tongue the asura-कन्या rushed on the deva army. She started eating up the weapons shot by the devas and licking them up with her long tongue. Seeing their army in disarray the deva-s huddled around their commander कुमार and asked him to aid them in battle.

कार्त्तिकेय said: "give me a boon- I wish to be represented amongst all of the देवता गण-s".\
The devas pronounced the प्रणाव "[ते देवा ऒं इत्यूछूः।]{style="color:#99cc00;"}". Immediately कुमार became one of each of the देवता गण-s. Of the vasu-s he became dhruva, of the rudra-s he became शङ्कर, of the Aditya-s he became ahaspati, who dominates the 13th month. Pleased with this कुमार decided to rid the world of दीर्घजिह्वी and the asuras, who were tormenting all. कुमार thought of his shaktI, the great षष्ठि देवी, and she emerged from this forehead in the form known as रेवती देवी. The mighty skanda-पत्नी cheered the deva army and advanced towards the asura army. Here she became a gigantic hyaena known as the शालावृकी and rushed towards दीर्घजिह्वी who was darting all over the battlefield. With her powerful jaws she grabbed the asura-कन्या and tearing her to shreds swallowed her up. The asura-s terrified by this frightful form of skanda-पत्नी rushed at her with all their weapons showering, arrows, shataghni-s and other भिन्दिपालस्. Then the frightful षष्ठि assumed the form known as shakuni देवी and she flew over the asura army hurling a shower of weapons that crashed from the firmament like blazing meteorites. Then she used a lighting weapon that brought a enveloped the daitya army in the crackle of thunderbolts. Finally, रेवती let loose a weapon that showered huge stones on the asuras. Assailed by these the enemies of the deva-s were smashed to smithereens and fled in disarray.

The दानवस् fearing an attack from skanda-पत्नी hid themselves in the wombs of various beings amongs their embryos. So रेवती becoming जातहारिणी at the behest of कुमार chased the demons in the wombs deluded them and killed them there. It is indeed the goblins of skanda under his wife's command who can hence destroy the embryos and young kids. Hence, the रेवती revealed to me \[kashyapa] the line of भेषज known as bAla-graha-chikitsa (paediatrics)."

[षष्ठी](https://manasataramgini.wordpress.com/2007/12/09/shashthi-suktam/ "षष्ठी sUktaM")\
[Agents of कुमार](https://manasataramgini.wordpress.com/2005/07/23/skanda-ganas/ "skanda गणs")


