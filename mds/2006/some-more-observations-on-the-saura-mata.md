
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some more observations on the saura mata](https://manasataramgini.wordpress.com/2006/10/21/some-more-observations-on-the-saura-mata/){rel="bookmark"} {#some-more-observations-on-the-saura-mata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/21/some-more-observations-on-the-saura-mata/ "4:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier compiled incomplete accounts of both the [saura mata](https://manasataramgini.wordpress.com/2005/11/21/saura-mata/) and its offshoots like the worship of [revanta](https://manasataramgini.wordpress.com/2006/08/31/the-deva-revanta-mythology-iconography-history-and-ritualism/). We initially wanted to work on the final part of the incomplete revanta essay dealing with the specific point of the evolution of shAstA from revanta and the relationship between their tantric prayogas. Then we realized we never put together the additional material from the various पुराणस् on the core saura mata that we originally intended. Since a few interlocutors had brought up saura issues and the original essay was too old we decided to put this part separately. This as such is common knowledge amongst Hindus educated in पुराण-s.

***Why did sAMba build sUrya temples?***\
This tale is a bit scandalous and often kept away from narratives (As a child I recall elders scrupulously avoiding this). As per the वराह पुराण (Vह्P 117 ) and भविष्य पुराण (भ्व्P 1.129) sAmba was said to be a paragon of good looks and attracted women. नारद, arrived at देवकी-putra's abode and mentioned that some of his many wives were flirting with his son sAMba. After an enquiry कृष्ण laid a spell on sAMba that he get a dreadful skin disease as a result of which the women would lose interest in him. sAmba invoked sUrya, who cured him of the disease, and told him that he would find an idol of sUrya for his worship. sAMba found this idol that was apparently crafted by vishvakarman from the kalpataru while he was having a bath in चन्द्रभाग. sAMba installed it in the forest of mitra at मूलस्थान. These पुराणस् also mention two other places where sAMba installed saura temples. One of these is said to be कालप्रिय on the south bank of the yamuna and the other is said to be sutira or मुण्डीर. The location of sutira as per tradition is in the eastern coast and is the same as the site where now the कोणार्क temple stands (in utkala). The site of मुण्डीर is generally believed to be the great [Modhera temple](http://ignca.nic.in/nl002206.htm) in लाट-pradesha. Both मुण्डीर and मूलस्थान were ravaged by the Mohammedan vandals on multiple occassions, but the former survives to date. There is a stand-alone पुराण by the name sAMba पुराण that exists in manuscript and corroborates the tales given the महापुराणस्.


