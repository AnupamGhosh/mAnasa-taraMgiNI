
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Weather and plants](https://manasataramgini.wordpress.com/2006/03/14/weather-and-plants/){rel="bookmark"} {#weather-and-plants .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 14, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/14/weather-and-plants/ "7:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/all_small.0.jpg){width="75%"}
```{=latex}
\end{center}
```



We were experiencing a peculiar weather pattern. There was an unusual spring like weather over the past few days -like the premature gift of indra, even before the विशुवान् day had arrived. We noted that a number of plants fired off their growth prematurely. We capture some this using a new technique which produced rather pleasing images.


