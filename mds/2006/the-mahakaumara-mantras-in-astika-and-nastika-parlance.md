
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The महाकौमर mantras in आस्तीक and nआस्तीक parlance](https://manasataramgini.wordpress.com/2006/09/06/the-mahakaumara-mantras-in-astika-and-nastika-parlance/){rel="bookmark"} {#the-महकमर-mantras-in-आसतक-and-nआसतक-parlance .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/06/the-mahakaumara-mantras-in-astika-and-nastika-parlance/ "4:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[॥ ऒं नमः कुमाराय ॥]{style="color:#0000ff;"}\
[नमोऽस्तु ते रक्तवर्णाय शक्तिधराय निलकण्ठवाहनाय महास्वमिने]{style="color:#0000ff;"}\
We bow to the great red-hued wielder of the shakti, who is seated atop a fierce peacock.

Sadly, many of the great tantras pertaining to the god कुमार appear to have been lost. However, the obscure tantric manuals like the स्कन्दार्चन-कल्पद्रूम and mahA-कौमार-kalpa preserve several of the mantras that are used, and those well-versed in the kaumara ritual use them to perform the various rites of कार्त्तिकेय. Amongst these mantra are the त्र्याक्षर महाविद्या that is recommended especially when one is a youth, the महाकुमार विद्या and the भूतडामर mantra that was mastered by the great Eastern कौमाराचार्य-s from the वङ्गदेश who latter settled in Bellary. Interestingly, variants of these mantras were adopted by the nआस्तीक-s early in the development of their tantric literature. These mantra-s in a bauddhized form appear in the मङ्जुश्री-मूल-kalpa one of the few Sanskrit originals of the tantras of the नास्तीकस् that survived in India. It was discovered by gaNapati shAstrI in the midst of the nambuthiris in early 1900s of CE (but in the discussion below I am following the text provided by the Vaidya edition). This appears to be one of the earliest tantric manuals of the bauddhas probably going back to the period around 500-600 CE at the earliest. In this tantra मङ्जुश्री is identified with कुमार and a bauddhized "myth" is created to incorporate him into the nआस्तीक fold. The tantra itself is composed in a peculiar vulgar Sanskrit: It is understandable, but probably not that of a well-educated brahmin tantrik.

मङ्जुश्री of the MMK is explicitly described as मङ्जुश्री कुमारभूत (the मङ्जुश्री who has become कुमार), right from the begining of the text and is repeatedly called that throughout the text. He is also called काऱ्‌त्तिकेया-मन्जुश्री and षण्मुख-bodhisattva repeatedly in the text. His मूल-mantra given in the text is clearly a bauddhized derivative of a kaumara mantra:\
[नमः सर्व-तथागतानाम्-अcइन्त्याप्रतिहत शासनानां ॐ र र स्मर । अप्रतिहत-शासन कुमार-रूपधारिण हूं हूं फट् फट् स्वाहा ॥]{style="color:#0000ff;"}(MMK2)\
This matches with the iconography of मङ्जुश्री कुमारभूत who is shown as कुमार.

After giving the महाकुमर-षण्मुख mantra (महाकुमार विद्या) the MMK provides the following narrative which includes the त्र्याक्षरी विद्या of कुमार and a bauddhized variant of the कौमार-भूतडामर mantra. It also alludes to the कौमार महामुद्र:

[भाषिता बोधिसत्त्वेन मङ्जुघोषेण नायिना ।]{style="color:#0000ff;"}\
[षड्विकारा मही कृत्स्ना प्रचचाल समन्ततः ॥]{style="color:#0000ff;"}\
It was said by the bodhisattva मङ्जुघोष, the leader, whose six-faced form shook the entire world

[हितार्थं सर्व सत्त्वानां दुष्ट सत्त्व निवारणम् ।]{style="color:#0000ff;"}\
[महेश्वरस्य सुतो घोरो वैनेयार्थम् इहागतः ॥]{style="color:#0000ff;"}\
To oppose the evil-doers for the sake of all beings' good, the terrible son of maheshvara came here to teach everyone.

skandam-अञ्गारकङ् chaiva graha चिह्नैः सुचिह्नितः |\
मङ्जु-भाषिणी tato भाषे करुणाविष्टेन चेतसा ||\
Well-adorned with the emblems of Mercury and other planets, he with a good voice, spoke to skanda with a consciousness filled with compassion.

[महात्मा बोधिसत्त्वो ।अयं बालानां हित कारिणः ।]{style="color:#0000ff;"}\
[सत्त्वछर्या यतः प्रोक्तो विछेरुः सर्वतो जगत् ॥]{style="color:#0000ff;"}\
This great boddhisattva for the cause of the good of children, expounded where ever beings wandered throughout the world.

[मुद्रा शक्ति यष्ट्यानु संयुक्तो स महात्मनः ।]{style="color:#0000ff;"}\
[आवर्तयति ब्रह्माद्यां किं पुनर् मानुषं फलम् ॥]{style="color:#0000ff;"}\
When combined with the mudra of of the great one, \[known as] the shakti of the rod it leads one to brahmA and others, let alone earthly \[human] fruits.

[कौमार-भित्तम्-अखिलं कल्यमस्य समासतः ।]{style="color:#0000ff;"}\
[कार्त्तिकेय-मङ्जुश्रीः मन्त्रो ऽयं समुदाहृतः ॥]{style="color:#0000ff;"}\
One would be completely free from all illness during the time of youth due to this mantra expounded by कार्त्तिकेयमङ्जुश्री.

[सत्त्वानुग्रह काम्यर्थं बोधिसत्त्व इहागतः ।]{style="color:#0000ff;"}\
[त्र्यक्षरं नाम हृदयं मन्त्रस्यास्य उदाहृतम् ॥]{style="color:#0000ff;"}\
For the sake of benefits for all the bodhitsattva came here to expound the three syllabled heart of the mantra.

[सर्व-सत्त्व-हितार्थाय भोगाकर्षण-तप्तरः ।]{style="color:#0000ff;"}\
[मुद्रया शक्तियष्ट्या तु विन्यस्तः सर्व कर्मिकः ॥]{style="color:#0000ff;"}\
For the good of all he engages in attracting enjoyment; with the mudra of shakti-यष्टि on complete all deeds.

[ऒं हूं जः ।]{style="color:#0000ff;"}\
[कौमार त्र्याक्षरी महाविद्या]

[एष मन्त्रः समासेन कुर्यान् मानुषकं फलम् ।]{style="color:#0000ff;"}\
This mantra completely accomplishes human fruits.

[नमः समन्त-बुद्धानां समन्तोद्योतित-मूर्तिनाम् ॥]{style="color:#0000ff;"}\
Salutations to all the buddha-s whose forms rise all over.

[ओम् विकृतग्रह हुं फट् स्वाहा ॥]{style="color:#0000ff;"}\
\[The कौमार भूतडामर विद्या]

[उप-हृदयं चास्य संयुक्तो मुद्रा-शक्तिना तथा ।]{style="color:#0000ff;"}\
[आवर्तयति भूतानि स-ग्रहां मातरां तथा ॥]{style="color:#0000ff;"}\
The deployment with the shakti mudra of this upa-हृद्यं (the sub-heart), averts भूतस्, possessing-goblins and mothers.

[सर्व-मुद्रित-मुद्रेषु विन्यस्ता सफला भवेत् ।]{style="color:#0000ff;"}\
[वित्रासयति भूतानां दुष्टाविष्टविमोचनी ॥]{style="color:#0000ff;"}\
Deploying it with the display of the mudra of mudras it becomes fruitful; it terrorizes all ghosts releases one from the possession of the evil \[ghosts]

[एष मङ्जुश्रियस्य कुमारभूतस्य कार्त्तिकेय-मङ्जुश्रीर्-नाम कुमारः अनुचरःसर्व-कर्मिकः । जप-मात्रेणैव सर्व-कर्माणि करोति, सर्व-भूतानि त्रासयति, आकर्षयति, वशमानयति, शोषयति, घातयति, यथेप्सितं वा विद्याधरस्य तत् सर्वं सम्पादयति ॥]{style="color:#0000ff;"}\
This is the emanation of मङ्जुश्री कुमारभूत, kumara named कार्त्तिकेय मङ्जुश्री who achieves all deeds. By merely japa of \[these mantras] one can achieve all deeds, terrify all goblins, attract, control, torment, slay and what ever the holder of these विद्या-s desires that is conferred by them.

Prior to this section a bauddhized version of the महाकुमार विद्या is given thus:\
[ॐ । कुमार महा-कुमार क्रीड क्रीड षण्मुख-बोधिसत्त्वानुज्ङात मयूरासन सञ्घोद्यत-पाणि रक्ताञ्ग रक्त-गन्धानुलेपन-प्रिय ख ख खाहि खाहि खाहि । हुं नृत्य नृत्य । रक्ता-पुष्पार्चित-मूर्ति समयमनुस्मर । भ्रम भ्रम भ्रामय भ्रामय भ्रामय। लहु लहु माविलम्ब सर्व-कार्याणि मे कुरु कुरु चित्र-रूप-धारिणे तिष्ठ तिष्ठ हुं हुं सर्व-बुद्धानुज्ङात स्वाहा ॥]{style="color:#0000ff;"}

Elsewhere the MMK provides a bauddhized version of the kaumara vighna-घातक विद्या:\
[हे हे महाक्रोध षण्मुख षट्चरण सर्व-विघ्न-घातक हूं हूं ।]{style="color:#0000ff;"}

The MMK provides a ध्यानं of कुमार that matches the classical ध्यानं used by the Astika tantriks:\
[कार्त्तिकेयश्-च मयूरासनः शक्त्युद्यत-हस्तः कुमार-रूपी षण्मुखः रक्ता-भास-मूर्तिः पीत-वस्त्रानिवस्तः ।]{style="color:#0000ff;"}\
कार्त्तिकेय is seated on the peacock with his hand bearing a shakti weapon, he is of a youthful with six heads and a glowing red form, clad in yellow robes.

A similar description is given by the MMK in describing the bauddha मण्डल where कुमार appears as the guardian of the second door:\
[द्वितीय-द्वार-समीपे कार्त्तिकेय-मङ्जुश्रीः मयूरासनः शक्तिपाणिः रक्तावभास-मूर्त्तिः पीत-वस्त्र-निवस्तोत्तरासञ्गिनः दक्षिण-हस्ते घण्टा-पताका-वसक्तः कुमार-रूपी मण्डलं निरीक्षमाणः ।]{style="color:#0000ff;"}\
Beside the second door, guarding the मण्डल is the कार्त्तिकेय form of मङ्जुश्री, seated on a peacock, shakti in hand, of red glowing form, wearing a yellow robe and upper garment, holding a bell and flag in his right hands and having a youthful form.

The MMK also goes on to appropriate the atharva veda for the bauddhas by claiming that it was summarized by मङ्जुश्री कुमारभूत:\
[नमः समन्त-बुद्धानाम्-अप्रतिहत शासनानाम् । तद्यथा:]{style="color:#0000ff;"}\
Salutations to all the buddhas whose ordinances are unassailable. Now the mantra:\
[ॐ ब्रह्म सुब्रह्म ब्रह्मवर्चसे शान्तिं कुरु स्वाहा ॥]{style="color:#0000ff;"}

[एष मन्त्रो महाब्रह्मा बोधिसत्त्वेन भाषितः ।]{style="color:#0000ff;"}\
This महाब्रह्मा mantra was expounded by the boddhisattva

[शान्तिं प्रजग्मुर्-भूतानि तत्-क्षणा-देव शीतला॥]{style="color:#0000ff;"}\
the beings attained peace; from this moment on the god is pacified.

[मुद्रा पञ्चशिखा युक्ता क्षिप्रं स्वस्त्ययनं भवेत् ।]{style="color:#0000ff;"}\
When combined with the पञ्चशिखा mantra one quickly attains well-being.

[आभिचारुकेषु सर्वेषु अथवो* चेद पठ्यते ।]{style="color:#0000ff;"}\
It is stated for all अभिचार rites in the atharva veda. [* note the vulgar prakritized form of the word atharva. The association of the brahma mantra with the atharva veda derives from the fact that it is the brahma veda and its mantra are the brahma mantras.]

[एष संक्षेपत उक्तो कल्पमस्य समासतः ॥]{style="color:#0000ff;"}\
This is indeed the abbreviated teaching of that text.

Thus, it appears that in course of the origin of the MMK's the subversion of the नास्तीकस् by the brahmins had begun. A brahmin educated in the rahasya of the kaumara-vidyas and other tantric mantras evidently flowed into the bauddha mata and incorporated these formulae into their fold. It is possible that he was an exponent of the atharva veda- both the worship of शाण्मुख in the form described in gopatha's धूर्त-kalpa of the atharvan school and the mention of the atharvan as the root teaching of the महाब्रह्मा support such a view. With this the revolution was set rolling. In the meantime after the 1200s of CE the mantramarga worship of कुमार faded away and the memories of the mantra became highly restricted to the practioners of the kaumara विद्यास्. The आस्तीक versions of the above षण्मुख mantras are critical in different circuits of the आवर्णस् of the worship of कुमार and his hordes in the षट्कोण-मण्डल (the heart of which contains the षडक्षरी विद्या-s only known to the आस्तीकस्).


