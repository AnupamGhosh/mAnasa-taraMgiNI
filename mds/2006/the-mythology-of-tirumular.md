
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mythology of तिरुमूलर्](https://manasataramgini.wordpress.com/2006/08/25/the-mythology-of-tirumular/){rel="bookmark"} {#the-mythology-of-तरमलर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 25, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/25/the-mythology-of-tirumular/ "5:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is clear from tirumantiram verses 337 onwards that तिरुमूलर् was an extreme sectarian shaiva. He cites several elements of shaiva mythology, where the aim is plainly to show the supremacy of shiva over other gods. He also sees a sadashiva higher than rudra in his world view, which is typical of this stream of thought and the sectarian शाक्तस् further go on to subsume the five brahma, विष्णु, rudra, mahesha and सदाशिव as the 5 pretas forming the cot of the transfunctional shaktI.

The sectarian elements apart, it is useful to compare the shiva पुराण with तिरुमूलर्'s mythology to identify the pan-Indian elements of shaiva mythology. The deeds of shiva are:

 1.  shiva sends agastya to correct the "balance" of the earth (the ancient astronomical myth of axial precession, is uniquely incorporated here into shaiva mythology)

 2.  The killing of Andhaka*

 3.  The beheading of दक्ष and his head replacement.*

 4.  Cutting of brahma's head and drinking of विष्णु's blood.*

 5.  Killing jalandhara with the chakra made from his toe*

 6.  Destruction of the tripura-s*

 7.  Killing of गजासुर*

 8.  The burning of yama (this is described as the yogic कुण्डलिनि fire arising from the मूलाधार and shooting out through the सहस्रार to burn yama) (?)

 9.  Killing of काम*

 10.  The winning of पार्वती from हिमवान्*

 11.  destruction of tripura-s again (mentioned further too)*

 12.  The pillar of fire and the quest of its ends by brahmA and विष्णु and the giving of the chakra and sword to विष्णु and brahmA respectively*

 13.  रावण being granted boons*

 14.  चन्डेश्वर raised to गण status for cutting his father's leg for having performed shiva-ninda*

 15.  routing the other gods during the destruction of the sacrifice of दक्ष*

 16.  He split the chakra of विष्णु into 3 parts and gave one part back to विष्णु, one to his wife and one he wore a cresent ornament

 17.  He suppressed the sea as the equine fire*

The majority of these myths are seen in the main sanskrit mythological traditions of shiva namely the shiva पुराण and the skAnda पुरान. A few tales of rudra find mention in the veda like the destruction of the tripura-s, the killing of Andhaka, the killing of prajApati/brahma, the destruction of the sacrifice. This suggests that rudra had a rich mythology right from the ancient past, like indra. The presence of most major elements in both the northern and southern traditions suggests that there was core pan-Indian shiva पुराण with several major tales (marked *) that had spread widely. The core shiva पुराण seems to have been shared by both the mantra मार्ग and लाकुलीश पाशुपतस्, but the extant version of the shiva पुराण seems to have been mainly preserved by the लाकुलीशस् rather than the mantra marga shaivas. The core shiva पुराण set in stone in the "Elephanta caves" around 550-600 CE, which is approximately contemporaneous with tirumular, suggesting that the core shiva पुराण text encompassing the tales of shiva were in place well before this period.


