
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The trayodashi rite of the kaTha दीक्षित-s](https://manasataramgini.wordpress.com/2006/08/15/the-trayodashi-rite-of-the-katha-dikshita-s/){rel="bookmark"} {#the-trayodashi-rite-of-the-katha-दकषत-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/15/the-trayodashi-rite-of-the-katha-dikshita-s/ "5:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The following is courtesy of Rajita, whom the deva-s have given me the fortune to know. I have only worked \[partially] on अक्षर-shuddhi of the mantra readings and put it together here with due permission.

The kaTha दीक्षित-s who were shrauta ritualists as well as well accomplished tantrics had a distinctive प्रदोषं rite on the trayodashi before महाशिवरात्री and that is described here. Interestingly they observed fast on trayodashi during the day time. The rite involves the worship of बटुक bhairava, paramashiva, sadashiva and his परिवार.

First a copper kalasha is taken and filled with sugar syrup. Walnuts are soaked in it this syrup and the kalasha is capped with a coconut with mango leaves. The kalasha is installed in the मण्डप and surrounded by 64 haridra bimba-s which are the योगिनीस्. Then in front of them clay idols of shiva, उमा, कुमार and गणनाथ are placed.

First the विनायक is meditated up with :\
[शुक्लाम्भर-dharam देवं shashi-varnam chathur-भुजं]{style="font-style:italic;font-family:arial;"}\
[prasanna वदनं ध्यायेत् sarva-vighnopa-shanthaye ||]{style="font-style:italic;font-family:arial;"}

Then he is invoked into his clay idol with the formula:\
[gaM गणपतये नमः |]{style="font-style:italic;font-family:arial;"}\
Then the japa of his mantra is done:\
[OM श्रीं ह्रीं क्लीं glauM gaM गणपतये vara-varada sarva-जनं me वशमानय स्वाहा ||]{style="font-style:italic;font-family:arial;"}\
Then a विनायक mudra is shown thus: the middle finger of the right hand is stretched out and slightly curved.\
Then the bali of a rice ball is offered with the formula:\
[gAM gIM gUM gaM गणपतये vara-varada sarva-जनं me वशमानय सर्वोपचार सहितं बलिं गृह्ण गृह्ण स्वाहा ||]{style="font-style:italic;font-family:arial;"}

Then one invokes बटुक bhairva into the syrup pot with walnuts with the formula:\
[oM baM बटुकाय नमः |]{style="font-style:italic;font-family:arial;"}\
Then one meditates on him with fair complexion, with deep blue clothing, with two hands holding a त्रिशूल and a skull with a yantra inscribed on it, with cobra garlands and surrounded by howling dogs.\
बटुक bhairava mantra is used during this meditation:\
[ह्रीं बटुकाय आपदुद्धारन kuru kuru बटुकाय ह्रीं ||]{style="font-style:italic;font-family:arial;"}\
homa may next be offered to बटुक bhairava with this mantra.\
Then the index finger and thumb and joined to show the बटुक mudra and then the following incantation is used to offer rice ball offerings:\
[ehyehi देवी-putra बटुकनाथ kapila जटा bhAra भासुर trinetra ज्वालामुख sarva-विघ्नान् नाशय नाशय सर्वोपचार सहितं बलिं गृह्ण गृह्ण स्वाहा ||]{style="font-style:italic;font-family:arial;"}

Then the योगिनीस् are invoked into the बिंब-s with the mantra: [yAM yoginibhyo नमः ||]{style="font-style:italic;font-family:arial;"}\
Then they are worshipped with the formula [aiM ह्रीं श्रीं ||]{style="font-style:italic;font-family:arial;"}\
Then the योगिनी mudra is shown: The middle finger, ring finger and thumbs are bent so that their tips meet to form a circle.\
Then the yoginis are offered bali with the formula:

*ऊर्ध्वं ब्रह्मण्डतो वा divi gaganatale भूतले niSkale वा पाताले वातले वा .anale वा salila pavanayor yatra kutra स्थिता वा क्षेत्रे पीठोपपीठादिषु cha कृतपदा dhupa -दीपादिकेन प्रीता देव्यः सदा नः shubha-bali-विधिना पान्तु वीरेन्द्र -वन्द्याः | yAM योगिनीभ्यः स्वाहा ||*

Then one meditates on paramshiva using the dhyAna:\
[nAnA-कल्पाभिरामापघनम्-abhi-मतार्थ-प्रदं सुप्रसन्नं |]{style="font-style:italic;font-family:arial;"}\
[पदंअस्थं पञ्चवक्त्रं स्फटिक-maNi-निभं पार्वतीशं नमामी ||]{style="font-style:italic;font-family:arial;"}

The he meditates on the mantra: [aiM ह्रीं श्रीं हसखफ्रें हसौः ||]{style="font-style:italic;font-family:arial;"} or [हौं ||]{style="font-style:italic;font-family:arial;"}

This is then the worship of सदाशिव and his clan is done.

One mediates on सदाशिव thus:

[bibhrad दोर्बिः कुटारं मृगं abhaya-varau suprasanno महेशः सर्वालङ्कार-दीप्तः sarasija-nilayo व्याघ्र-चर्मात्त्वासाः |]{style="font-style:italic;font-family:arial;"}\
[dhyeyo मुक्तापरागामृत-rasa-कलिताद्रि-प्रभः पञ्चवक्त्रस्-त्र्याक्षः कोटीरकोटी-घटित-tuhina-रोचिः कलोत्तुङ्ग-मौलिः ||]{style="font-style:italic;font-family:arial;"}

[sadyo वेदाक्षमाला .abhaya-वरदकरः kunda-मन्दार-gauro वामः काश्मीर-वर्णो .abhaya varada parashva .अक्षमाला-विलासी |]{style="font-style:italic;font-family:arial;"}\
[अक्ष-srag-veda-pAshA-.अङ्कुश-Damaruka-खट्वाङ्ग-शूलान् कपालं बिभ्राणो bhIMa-दंSट्रो .अञ्जन-ruchira-tanur-भीतिदश्चा .अप्यघोरः ||]{style="font-style:italic;font-family:arial;"}\
[vidyud-वर्णा .atha vedA .abhaya-varada कुठारान् dadhat पूरुषाख्यः प्रोक्ताः sarve त्रिनेत्रा विधृतमुख-चतुष्काश्-chatur-बाहवश्च |]{style="font-style:italic;font-family:arial;"}\
[मुक्ता-gauro .अभयेष्टाधिक-kara-kamalo .अघोरतः पञ्चवक्त्रस्-त्वीशो dhyeyo .ambujanmodbhava-muraripu-रुद्रेश्वराः स्युः शिवान्ताः ||]{style="font-style:italic;font-family:arial;"}

One then praises shiva saying:\
[vishva-ग्रासाय vilasat-काल-कूट-विषाशिने |]{style="font-style:italic;font-family:arial;"}\
[tat-कलङ्काङ्कित-ग्रीव-नीलकण्ठाय te नमः |]{style="font-style:italic;font-family:arial;"}

[दंष्ट्रा-करालं divi नृत्यमानं हुताश-वक्त्रं ज्वलनार्क-रूपं |]{style="font-style:italic;font-family:arial;"}\
[sahasra-पादाक्षि-shirobhi युक्तं bhavantam-एकं प्रणमामि रुद्रं ||]{style="font-style:italic;font-family:arial;"}\
[namo.astu सोमाय su-मध्यमाय namo.astu देवाय हिरण्य-bAho |]{style="font-style:italic;font-family:arial;"}\
[namo.agni-चन्द्रार्क-विलोचनाय namo.अम्बिकायाः pataye मृडाय ||]{style="font-style:italic;font-family:arial;"}

Then he is invoked with the five fold mantras.

Then there is recitation of the kaTha रुद्रीय, which begins with "tat-पुरुषाय vidhmahe..."

It is concluded with the oblation:\
[tasmai te deva भवाय शर्वाय pashupataya उग्राय देवाय mahate देवाय रुद्रायेशानायाशनये स्वाहा ||]{style="font-style:italic;font-family:arial;"}\
evidently from the kaTHa oral tradition (compare with similar तैत्तिरीय mantra).

Then उमा is worshipped with the formula:\
[ह्रीं namo brahma श्री-राजिते rAja पूजिते jaya vijaye गौरी गाण्धारी tribhuvana-वशंकरी sarva loka-वशंकरी su su du du ghe ghe वा वा ह्रीं स्वाहा ||]{style="font-style:italic;font-family:arial;"}

Then skanda is invoked with the formula: [ShaM षाण्मुखाय नमः ||]{style="font-style:italic;font-family:arial;"}\
He his worshiped with the mantra: [OM कं क्षं कं कार्तिकेयाय फट् ||]{style="font-style:italic;font-family:arial;"}

Finally क्षेत्रपाल is worshiped an offered bali.\
Then the soaked walnuts are offered as प्रसाद to break the fast.


