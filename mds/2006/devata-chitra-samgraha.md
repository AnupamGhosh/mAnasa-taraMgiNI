
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [देवता chitra संग्रह](https://manasataramgini.wordpress.com/2006/12/11/devata-chitra-samgraha/){rel="bookmark"} {#दवत-chitra-सगरह .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/11/devata-chitra-samgraha/ "5:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The eldest son of the great king of Nepal, पृथिवि नारायण शहा (who defeated the British and founded the Nepal Kingdom until its recent destruction by Marxists), प्रताप सिंह, compiled a voluminous work on the mantra शास्त्र during his brief life. As a companion to this a collection of pictures for the dhyAna-s on various deities was prepared by the artists in Nepal. A copy of this collection with R1's father is of considerable interest in terms of tradition Hindu dhyAna art of which not many old samples survive. That this Hindu tradition was very old is shown by the chitra-कला section of the विष्णुधर्मोत्तर. Here the भार्गव मार्कण्डेय gives a lecture on drawing and painting and specifically provides the principal लक्षणस् that should depicted in paintings of देवतास्. Most of the drawings in this collection are pretty exquisite and depict the following deities:

 1.  विनायक: 1 headed, 4 armed in भद्रासन with chakra and 3-looped pAsha.

 2.  sUrya

 3.  विष्णु: seated with his two wives standing on either side. The लक्ष्मि-s have a horizontal tilaka in addition to the bindu.

 4.  matsya: Strangely with 2 horns and a fish face but arms holding chakra and gada

 5.  कूर्म: shown like the turtle Chelydra serpentina, and grabbing a crocodile by the neck with the mouth. Having a peculiar chakra- probably close to how the real chakras were used with a central rod.

 6.  वराह: 8-arms with various weapons, globe on the tusks, in a war-like pose.

 7.  नृसिंह with two लक्ष्मि-s

 8.  वामन: young boy standing on moon.

 9.  रामो भार्गव: with excellent axe seated before a sacrificial fire. Shown with 3 hands- an iconographic peculiarity seen elsewhere in Nepal.

 10.  रामचन्द्र and brothers

 11.  संकर्षण: peculiar shaped hala and the musala called sunanda.

 12.  गोपीजन-vallabha

 13.  nAstIka siddhArtha: shown like a body-builder flanked by devas and ब्राह्मणस् on either side.

 14.  kalki: riding on a horse and slaughtering bearded तुरुष्क-s (or Meccan demons) and म्लेच्छ-s in combat.

 15.  hayagrIva

 16.  shiva: 5-headed with मृग and paraShu

 17.  sharabha: shown like a griffin from Western Asia.

 18.  कार्तवीर्यार्जुन: shown in a ratha with many arms and many bows discharging arrows.

 19.  kAli

 20.  tArA: shown just like a female shiva standing on shiva with a cleaver and lotus.

 21.  षोडशि: in the चिन्तामणि गृह with the पञ्च preta ma\~ncha

 22.  bhuvaneshvarI

 23.  bhairavI

 24.  छिन्नमस्ता: sky-clad, dancing on rati and काम with her 2 डाकिनिस् drinking the blood streams coming from her head.

 25.  धूमावती: on a chariot drawn by a large crow holding a winnowing basket and crow banner. Looking awful.

 26.  bagalamukhi

 27.  मातङ्गि: with a halberd and shield with crescent moon.

 28.  gaja-लक्ष्मि

 29.  दुर्गा: 10 handed killing महिष.

 30.  शीतला: young, voluptuous, sky-clad, holding a winnow basket and broom-stick with a sack carried on the head.


