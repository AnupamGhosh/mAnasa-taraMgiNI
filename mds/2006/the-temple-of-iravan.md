
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The temple of इरावान्](https://manasataramgini.wordpress.com/2006/06/08/the-temple-of-iravan/){rel="bookmark"} {#the-temple-of-इरवन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 8, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/08/the-temple-of-iravan/ "9:55 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In addition to the temples of draupadi अम्माळ् there are a cluster of temples of इरावान् close to the point the where my paternal ancestor first immigrated to the Dravida country. Our clanswoman had obtained a detailed account of the इतिहास of इरावान् from one of the most learned स्मार्त भारतियार्-s of that locality whose clan was associated with that of our ancestors for a while. Of course the स्मार्त narrative differs quite drastically from the narratives of the tevar and other tamil पारतियार्-s. It goes thus:


 1.  airavata was the great lord of the nAga-s and performed the first sarpa-sattra. His son was धृतराष्ट्र. His sons in turn were kauravya and others. kauravya became king of the nAga-s. His daughter was उलूपी the princess of the sarpa-s.


 2.  ayodo dhaumya of the भार्गव clan had a student named veda also of the भार्गव clan. veda's student was utanka also of the भार्गव clan. utanka at the behest of veda's wife had gone to obtain the magical ear-rings from the kShatriya पौष्य, but they were stolen from him on the way by the nAga chief तक्षक who was उलूपी's husband's brother.


 3.  To retrieve them utanka used spells to indra, such and "fire horse of indra". The nagas were terrified by the fire horse of indra and were scurrying around in terror when तार्क्ष्यो अरिष्टनेमी swept down from the firmament and attacked the husband of the nAga princess उलूपी. He was killed in the fight and eaten by the fierce दिवः shyena, the killer of elephants and turtles. The naga princess was thus widowed and led a forlorn existence thereafter.


 4.  ऋष्यश्रिङ्ग, the young उद्गातर्, lived alone in the forest with his father without any contact with women. He diligently spent all his time learning the rahasya gAnaM-s, the शक्वरी-s and the अरण्येगेय-s. One day the king of अङ्ग had sent a comely पुंश्चलि to entice him even as he was practicing the शक्वरी, which is verily the vajra of the god of the gods. He was fascinated by her breasts and alluring ball-game that she played and fell prey to his natural instincts to dally with her. Of their coitus was born the terrible rAkShasa, अलम्बुष was born, who got endowed with the might of the शक्वरी in his body.


 5. When arjuna पाण्डुपुत्र, in course of his exile for having interrupted युधिष्ठिर's dalliance with draupadi, was performing his पितृ-तर्पणं on the banks of the गङ्गा a terrific female uraga arose from the waters and catching him in her coils pulled him down to sarpa-loka. There she gave arjuna a sacrificial fire for him to perform his rites. He duely did so and asked why he had be been brought there. nAga woman told him that she was उलुपी the daughter of kauravya the snake-king. She told him that she was alone, with out mate and striken by काम's arrows was induced to pull him down to her abode. He protested that it was a sin for him do anything that broke his brahmacharya. She then told him that it would not be a major sin especially since he was being solicited by a mateless woman and was saving her life by giving her an offspring and asked him to dally with her. kauravya asked arjuna to take his daughter as his wife.


 6.  He duely did so staying over in her abode for a night and then next day she brought him back to the banks of the गङ्गा let him go.


 7.  Out of this pairing was born the handsome इरावान्. He was abandoned by his uncle तक्षक because in the great battle of खाण्डव, arjuna and his friend कृष्ण vAsudeva had brutally destroyed the realm of तक्षक disobeying indra and the deva-s orders.


 8.  उलूपी brought इरावान् up on her own and trained him with various weapons and asked him seek arjuna when he was in indra-loka. इरावन् duely did so and arjuna showering his affection on him asked him to aid the पाण्डुस् in the forthcoming war against the kaurava-s. इरावान् agreed and returned to his region. He obtained the horses bred with the founder of the yajur vedic school tittiri and built a cavalry division.


 9.  On the 8th day of the great war इरावान् fought a great cavalry battle with the gAndhAra division of shakuni under shakuni's six fierce clansmen. In a fierce battle that followed arjuna's son cut down five of the 6 gAndhAra heroes with his sword and mangled the 6th. duryodhana seeing his maternal clansmen savaged thus sent his great friend, ऋष्यश्रिञ्ग's son, the rAkshasa अलम्बुष to kill इरावान्.


 10.  अलम्बुष and इरावान् had a fiery encounter in which इरावान् cut his bow with a swift strike of his sword. He then struck अलम्बुष repeatedly with his battle axe, but the rAkShasa resorting to mAyA tactics confounded the son of arjuna. Finally, a nAga relative of उलूपी came to इरावान्'s aid, but he was devoured by the आर्ष्यश्रिञ्ग assuming the form of a garuDa. When इरावन् was confounded by the rAkShasa's mAyA, अलम्बुष swept down upon him and cut his head off with his sword.


