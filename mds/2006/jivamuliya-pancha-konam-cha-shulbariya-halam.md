
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [जीवमूलीय पञ्च-कोणं cha शुल्बारीय हलं](https://manasataramgini.wordpress.com/2006/08/12/jivamuliya-pancha-konam-cha-shulbariya-halam/){rel="bookmark"} {#जवमलय-पञच-कण-cha-शलबरय-हल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/12/jivamuliya-pancha-konam-cha-shulbariya-halam/ "5:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were ruing the crushing defeat on the field code-named जीवमूलीय पञ्च-कोणं cha शुल्बारीय हलं by the five म्लेच्छ attackers. We felt like Shivaji after the great fort of Purandar was taken by the ghazis. While we sat ruing our defeat and the loss of the small but strong force sent to wrest the पञ्चकोण and शुल्बारीय hala that we much desired, we wished that indra give us victory here. We knew that GS of boundless energy, the descendent of the great दीक्शित, was a man who had attained eko-मानुष आनन्दः. He was never happy though constantly seeking new गविष्टि-s for dhana. We hence thought of our शिष्य of great wealth again and hatched a plan for revenge. We told him that all wealth in the गविष्टि was deservedly his, though we wanted the yasha of jayam and more importantly the coveted पञ्चकोण and एककोण. We arranged for a secret force, small but fierce, to be gathered again. We were suffering from the crane-arrow plowing through us as a result of the disasterous खाण्डवन् battle, but GS promised to fight on our पक्ष. We equipped him with astra-s as we did before the गविष्टि-s in which he gained his महाधन and asked him to boldly advance with a raiding party of his own. His dauntless cousin of droNa-like tenacity joined him in the raid with another small raiding party. We then waited till GS and his troops surprised the म्लेच्छ enemies, who had nothing but deep hate and contempt for us. म्लेच्छ who thought we were completely annihilated laughed at first, but seeing GS and his cousin shake their ranks, were taken aback. At that critical point we dispatched our hidden force to join the fray in a merciless assault. भीमद्विष, the म्लेच्छ chief was killed right away and two more were slain after him. GS closed in capture the rest and we hope to seize the पञ्चकोण and एककोण from them.

\~*\~*\~*\~

The above was what we narrated before unexpectedly things took a new turn. We advanced to seize the पञ्चकोण and एककोण, when the म्लेच्छस् suddenly regrouped and counter-attacked. We asked the दीक्शित's descendent to continue the invasion, and that we will back him up with more troops. He and his cousin grouped together and continued with their ramshackle troops showing tremendous courage in face of an unexpected attack -- refusing to budge. Seeing the dreadful shower of the sharas hurled by the म्लेच्छस्, we asked GS to back off a little and seek cover. But he did not flinch. We positioned ourselves near a cleft in the rock all the time waiting for our opportune moment. Just then we saw the म्लेच्छ, विट्मुख, come into our range. We killed him outright like rAma killing vAlI. Then we came out of the cleft with our troops and fell on the काल-yavanas and routed them completely. We had जीवमूलीय पञ्च-कोणं cha शुल्बारीय हलं in our hands. indra had given them to us ! That was our victory.

On the other side, however, we were being pursued by a new threatening attack of the flight of the crane from which we were having no respite. We tasting both soma and the हालाहल.


