
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Mongolian रामायण](https://manasataramgini.wordpress.com/2006/12/13/mongolian-ramayana/){rel="bookmark"} {#mongolian-रमयण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 13, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/13/mongolian-ramayana/ "6:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vaiShNava journeying beyond the hallowed Oddiyana पीठ, where he had performed the worship of वाराही, and कर्णमोटिनि, arrived at the oasis of Khotan. Having obtained several manuscripts of various Indic texts in Central Asia he moved further ahead on horse to make a rendezvous with his Mongolian guide. Having mounted a fresh horse and penetrated deep into the steppes they arrived after a week at the remote outposts of what was once the organizing ground of the great Oirat emperors, Esen Taiji and Amasanji Taiji. There he found a guru in the lineage of the mysterious brahmin wanderer Zaya paNDita who had visited the courts of the Chingizid Kha'khans like Mangku and Kublai, journeying from Bihar to Tibet to Mongolia. On being introduced to the vaiShNava he invited with respect and showed him an aged manuscripts in the Oirat script. They were said to be translations and transcriptions of works with Zaya paNDita. Two of them contained the tale of रामचन्द्र. One of them was the इक्ष्वाकु वंश giving the history of the इक्ष्वाकु kings from manu down to the nAstIka siddhArtha, and his साख्य branch of इक्ष्वाकुस् of kapilavastu. In this the tale of रामचन्द्र was considerably developed.

The second was called the tale of king जीवक and his son. Here जीवक had 3 wives but no son. He was told by his mantra-वादिन्स् that he would need to get a flower of the audumbara tree and make his wives eat it for them to bear a son. He wandered to the oceans and found the said flower on an island in the middle of the ocean. He gave it to his wives and the eldest of them gave birth to a son called rAma. When he grew up he assumed kingship and ruled his people as the ideal king. He invited a buddha and let him spread the dharma.

Then a rAkShasa king from lanka came to distract the ऋषिस् in the land in the form of an antelope. They took the help of rAma and जीवक and they shot stones to blind the antelope's eyes and drove it away. In return the vipra-s conferred invincibility on rAma.

In the past the rAkShasa king of lanka had a daughter. The court soothsayer said that she would destroy lanka if alive. So they threw her into the sea in a chest. The chest floated to जम्बुद्वीप and was picked up by a tiller. The farmer brought up the girl sItA (Mongolian word for furrow used) and gave her to king rAma as a wife because of her great beauty.

In the meantime dasha-ग्रीव became emperor of lanka and was the lord of ten chiefs. He came to known from his sister that rAma had a beautiful wife and wanted to abduct her. So he sent मारीच in the form of an antelope to entice rAma away while he kidnapped sItA and took her to Lanka. Then rAma in search of her came to the kingdom of apes where he saw two apes fighting. One of them called for rAma's help and so on his behalf rAma killed the other ape vAlI. This surviving ape सुग्रीव gave his army under his commander hanumAn to rAma and aided him to retrieve sItA from Lanka.

rAma invaded Lanka with the army of apes and fought a fierce battle with the राक्षसस्. The राक्षसस् on their flying horses started fleeing north and riding through the steppes drawing rAma into an ambush. Then the great golden eagle garuDa helped rAma, as it had come to the aid of Chingiz Kha'Khan when he fought Shidurgo of the Hsia'Hsia. Flying down on the demons it took away their food reserves and blocked their paths with gusts of wind. Trapped thus they were forced to turn back and were met by rAma and the apes and slain. Then rAma decided to seek out dasha-ग्रीव and kill him. He had a palace in Lanka called Bolor Toli whose ruins were apparently still there when the navy of Kublai Kha'Khan had attacked Lanka. The Bolor Toli had a hundred mirrors and so no one could tell which was the real dasha-ग्रीव and which were his reflections. Thus he would keep evading his enemy and no one could shooting. The intelligent ape hanumAn detected the real dasha-ग्रीव and signaled to rAma who killed him with a single shot from his arrow. Then he returned to kapilavastu with sItA and happily ruled ever after.

Then two prayoga vidhis for hanumAn and सुग्रीव were given.

This Mongolian version is congruent to that of the naked jainas narrated in the महापुराण of पुष्पदन्त.


