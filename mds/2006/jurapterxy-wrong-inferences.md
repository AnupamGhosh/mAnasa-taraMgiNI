
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Jurapterxy- wrong inferences?](https://manasataramgini.wordpress.com/2006/03/16/jurapterxy-wrong-inferences/){rel="bookmark"} {#jurapterxy--wrong-inferences .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/16/jurapterxy-wrong-inferences/ "7:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/boersti_skin.jpg){width="75%"}
```{=latex}
\end{center}
```



Finally the fossil nicknamed Boersti in German was published today. It was officially named [Juravenator starki]{style="font-style:italic;"}. It is definitely an important fossil that throws much light on the early evolution of coelurosaurs, because Jurassic forms are generally scarce. But there are number of questions raised by this find. Phylogenetic analysis, performed by the authors clearly shows that it is a coelurosaur nested within the coelurosaurs. It unites with [Compsognathus, Huxiagnathus]{style="font-style:italic;"} and [Sinosauropteryx ]{style="font-style:italic;"}in forming the composgnathid branch of the tree, which is the next most basal lineage of coelurosaurs after the basal most tyrannosaurs. It is from the Solnhofen formations that also yielded the fossils of [Compsognathus ]{style="font-style:italic;"}earlier. The authors distinguish it from [Compsognathus ]{style="font-style:italic;"}though they hardly offer any explanations for this differentiation from Compsognathus. It is however from a slightly older formation than [Archaeopteryx ]{style="font-style:italic;"}or [Compsognathus]{style="font-style:italic;"}. They state that some integument was preserved around the tail and hind limbs which apparently showed no impressions what so ever of feathers which are seen other coelurosaurs such as [Sinosauropteryx]{style="font-style:italic;"}, a close relative of [Juravenator]{style="font-style:italic;"}. The authors interpret this as meaning that [Juravenator ]{style="font-style:italic;"}lacked feathers entirely and that the evolution of feathers in coelurosauria may have been "more complex" than believed. The authors and the चीनाचार्य Xing Xu go on to speculate that feathers may have evolved on multiple occassions. There are other speculations that coelurosaurs may have had feathers only seasonally.

I believe that these interpretations could possibly be flawed. Feathers do not preserve well and it is only the chance events of the Liaoning formation that really preserve feathers well. Even in Solnhofen the feathers of [Archaeopteryx ]{style="font-style:italic;"}have been preserved only in some of the specimens and other specimens lacking the feather impressions where even mistaken for Compsognathus. Also we should note that the enigmaitc Italian compsognathid [Scipionyx ]{style="font-style:italic;"}did not preserve any feathers even though it preserved several internal organs well. This raises the possibility that the [Jurapteryx ]{style="font-style:italic;"}preservation is just bad in terms of soft tissue and the absence of feathers is merely a preservational event. In light of this the foolish speculations like those of Xu and are just too far-fetched to believe and I really see no reason for a more complex history for coelurosaur feathers.

The preserved skin shows scales on the tail quite clearly. on the opposite side there are some fiber-like imprints. The authors believe that they might be tendons, but their location suggests that they are in probably outside of the scaly layer and probably feather-like fibers. So the matter of [Jurapteryx ]{style="font-style:italic;"}not having feathers is likely to be uncertain at best. In any case we can be sure that dinosaurian integumentation is far from completely understood. But sadly this will give the BANDITs (The moronic Feduccia band) more fuel for their fantasies.


