
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The countryside expeditions](https://manasataramgini.wordpress.com/2006/04/08/the-countryside-expeditions/){rel="bookmark"} {#the-countryside-expeditions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 8, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/08/the-countryside-expeditions/ "4:42 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The best remembered of our expeditions were those to the city of bhairava, the fort of tridents and the long-winded peregrinations through the Maharatta countryside.In the halcyon days Mis-creant, muscleman S, his शूद्र sycophant (शूद्र श्रेष्ठ=SS) and me were seated on our parked vehicles and yarning in the vicinity of a little shrine of a medieaval devotional vaiShNava cult descending from the महानभव् cult. There were some devotional singers doling out the tune of a medieaval saint of the Maharatta country, whose song roughly translated thus: "Our पाण्डुरण्ग़ is not like the goddess jAkhAI, jokhAI, मायाराणी or mhaisAbAI. Worship ईठोभा, do not worship gods like gaNapati and skanda or these AIs." I was struck by this parochial tune and asked SS who are these AIs and whether he happened to worship them. He replied, the AIs were most important in his peoples and they surely worshipped them. He told us of some cultic centers of his goddesses and gods. I had heard these from other local शूद्रस् and the Maharatta chief who had given us the land to live (due to his high regard for ब्राह्मणस्). So I was filled with an urge to explore these regions and know more and echoed them to Mis-creant, who as ever was always game for such adventures. She also came up with the bright idea that we might be able to couple it with our project to investigate the diversity of predaceous hemipterans of India, which was for long being contemplated. She also suggested that we take along S and SS as our bodyguard and guide- I knew they would come if they knew she was going to come along. Sure enough they agreed, if not for anything else just being around in the company of Mis-creant, and we found S a good listener and sounding board, whom we could bore endlessly with our newest theories on hemipteran evolution, geology of the Deccan plateau or history of local deities...

SS planned the details of the route. S prepared the armaments for the eventuality of self-defense in the lawless Indian countryside. He kept the artillery for himself and distributed other weapons to the rest of us and gave us briefing on their use. SS also brought some of his own weaponary, include a very effective secret Ayudha. I was pretty confident of their fighting skills for they had demolished a local politician's son and his host of rowdies, while coming to our defense. SS and S rode their shiny mobikes, while Mis-creant and myself very legally restricted ourselves with scooters. We headed early in the morning towards Bhugaon in the Mulshi Taluka of the Pune district. After a 2 hour drive we were at our destination. Parked suitably \~4 Km from the village and started our ascent to the pristine Bhugav hill atop which sits an old nearly forgotten shrine. Even before we ascended Bhugav hill, Mis-creant and myself recovered a number of microliths flaked of chalcedony cores in the gentle slope at the foot of the hill. This suggested that there was habitation in the obscure site from the Mesolithic period, possibly predating the rise of the Jorwe culture of Maharashra. However, I later realized that the presence of a microlithic background in the fully chalcolithic Harappan sites in India meant that such microlith makers could have lasted till later, and this could be the case in this site.

As we started climbing the we had a field day with the bugs- we bagged a few harpactorines, reduvines proper, Salyavatines- all predatory hemipterans or assassin bugs. It was the first time I saw a specimen of [Valentina]{style="font-style:italic;"}. We also saw some coreid bugs mating on a particular plant. At the top of the hill was a shrine of खण्डोबा. The main idol was a typical खण्डोबा with a sword and either side of him were his two wives-- म्हाळ्सा-AI and bANA-AI. In front of the temple under a shrub was a large stone painted completely bright orange with with a lead salt paint. We saw SS worshiping this deity with great reverence. On being asked who she was, SS said that she was padubAI (=पद्मावती in Skt.). Then we went down to the village where we saw another shrine completely dedicated to padubAI. On the side of the temple were found idols of भैरोबा (= bhairava in Skt) and jogubAI (=योगेश्वरी in Skt). There was also a vIra-kal of a brave hero who had died in the presumably in the pre-Islamic period. We camped on the hill that night and through my binoculars obtained a spectacular view of the the globular cluster [Omega Centauri, ]{style="font-style:italic;"}the like of which I have never had in recent times. We were also luck to use our vantage point to pick out the great 8th magnitude Centaurus A galaxy low in the sky. The Milky Way too was unforgettable, especially in the star rich regions near Crux.

The next day we travelled 20-30 kilometers to reach another village where there was another temple of padubAI -- she is just a large orange boulder. But here next to padubAI was another orange boulder, which was said to be tigeress-faced goddess वघोबा-bAI. Between them was a smaller rock which was said to be भैरोबा. We saw yet another shrine of padubAI and other deities. This opened our eyes to padubAI and other local deities that we shall detail in a subsequent work.\
[]{style="font-family:Verdana;font-size:85%;"}


