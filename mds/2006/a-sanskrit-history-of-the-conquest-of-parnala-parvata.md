
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A Sanskrit history of the conquest of पर्णाल-parvata](https://manasataramgini.wordpress.com/2006/05/19/a-sanskrit-history-of-the-conquest-of-parnala-parvata/){rel="bookmark"} {#a-sanskrit-history-of-the-conquest-of-परणल-parvata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 19, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/19/a-sanskrit-history-of-the-conquest-of-parnala-parvata/ "5:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

"Brahmins do not have it in them to fight" was a common refrain I had heard despite being brought up on the tales of the भृगु conquests of yore- र्^इचीक, jamadagni, rAma, agni aurva and all of them. I had almost internalized it. But my conversations with PR got me thinking about it from different angles till I discovered the truth. My learned आचार्य had a peculiar regard for PR: he would regularly come to recite the ऋग् veda when he taught it and would only talk with our आचार्य in Sanskrit. Otherwise, he spoke with none and had an air of mystery and power about him. I had occasionally seen them converse about performing a shrauta rite but, they would generally be secretive about it. He had some kind of secular job, because I had noted him and पिताश्री talk on some technical issues. That was only time he ever used the आन्ग्लीक भाष, else he spoke only either in Sanskrit, Maharatti or Tamil tongues. He was well-versed in the अष्टाध्यायी and used to cite sutra-s and demonstrate them with ease. Once my yajur vedic teacher asked me approach him on learning about ऋग् in practice with respect to the mysterious न्यूञ्ख and ninarda mode of recitation of mantra-s.

In course of this encounter PR narrated a bit of his history in addition to introducing me to the mysteries of न्यूञ्ख and ninarda recitations. He descended from Kondaji Ravlekar, a ब्राह्मण warrior who conquered the Panhala fort for the Maharatta king Shivaji, from the Moslems. This tale and the battles leading to it are described in a remarkable text पर्णाल-parvata-ग्रहणाख्यान by a certain Jayaram Kavi that was in his possession (his ancestors had taken it with them to Tanjore). The Moslems of Bijapur had taken Panhala from the Maharattas in 1660 CE after the great battle of Pavankhind, where the deadly Black African warrior Siddi Jawhar had pressed hard on Shivaji. Finally in 1672 Shivaji scored a string of victories against both the Mogols of Delhi and the Adil Shahi Moslems, such as the great battle of Salher (This is described in the PPG as the सालेरि parvata yuddha, where another ब्राह्मण warrior of Shivaji, Moro Tryambak Pant lead the Maharatta army to rout the Mogols in a spectacular assault. 30 principal Moslem officers and several thousand troops of theirs were destroyed by the attack of the Maharattas, in addition to serious injuries to Ikhlas Khan the imperial commander of Awrangzeb). Realizing that the Moslems were on the defensive Shivaji decided to strike hard early next year, assembled a body of fierce Maharatta fighters near Rajapur and asked them to take Panhala. He entrusted their leadership to Anaji Datto and the Rigvedi brahmin warrior Kondaji Ravlekar. Kondaji first dressed as Moslem and spied the fort. Then having obtained good intelligence, he chose a steep un-guarded mountain cliff for the attack. On the प्रदोषं day of March 6th 1673 in फाल्गुनि, Kondaji secretly climbed the mountain cliff with 60 commandos and launched a surprise attack on the Moslems. He then opened the fort doors and Anaji Datto rushed in with the main army and attacked Baboo Khan the Islamic commander. In the intense fight that followed Baboo Khan was beheaded and a traitorous brahmin who helped him with accounts Nagoji Pandit was also killed. Thus Panhala was conquered.


