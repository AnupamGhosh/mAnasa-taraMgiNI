
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The पञ्च vaktra chakra of the पूर्व srotas](https://manasataramgini.wordpress.com/2006/07/13/the-pancha-vaktra-chakra-of-the-purva-srotas/){rel="bookmark"} {#the-पञच-vaktra-chakra-of-the-परव-srotas .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 13, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/13/the-pancha-vaktra-chakra-of-the-purva-srotas/ "6:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/shiva.0.jpg){width="75%"}
```{=latex}
\end{center}
```



The पूर्व srotas of the tantras of rudra is comprised of the garuDa tantras, which encompass the glorious प्राणेश्वरी and मृत्युञ्जय विद्यास्. However, less known are the aspects of the regular worship of महादेव in this srotas. The most critical aspect of this the invocation of the five-fold shiva through the pa\~chavaktra chakra. Adhara of the worship is विष्णु, who shall be first invoked with the formula :\
[ॐ भूर् विष्णवे आदिभूताय सर्वाधाराय मूर्तये स्वाहा ॥]{style="font-style:italic;color:rgb(255,153,0);font-weight:bold;"}\
He is offered an oblation in the fire or a pouring of water.

The the five vedic vaktra mantras of the तैत्तिरीय ब्राह्मण (or in the case of members of the 4th वर्ण their equivalents as seen in the सनत्कुमार तन्त्रं) along with their tantric emanations are worshipped while meditating upon the मण्डल in the appropriate spots. The 3X13 mighty दूती-s or कला-s emanating from the faces of shiva are worshipped in order clustered as per their आम्नाय (to be detailed later).

Then the mystic gAyatrI of maheshvara is meditated upon by the सधाक to complete his उपासन .

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/shiva_gayatri.0.jpg){width="75%"}
```{=latex}
\end{center}
```




