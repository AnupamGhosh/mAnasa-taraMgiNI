
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Of the past](https://manasataramgini.wordpress.com/2006/10/20/of-the-past/){rel="bookmark"} {#of-the-past .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 20, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/20/of-the-past/ "6:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It was relatively late when we were sort of startled by R's call. She said we had a visitor from really ancient times- Igul. One forgets most males, at such a distance in time, but then some women are much harder to forget (the male brain seems to be wired so). I must be careful of what I say here for युधिष्ठिर cursed women that they will not keep their mouth shut. Anyhow I do not want to make it private as we do want to our पक्ष and the जानश्रुतेय पक्षी-s to see it. Anyhow, we say so little of the rest. We had great fun picking on R for calling us "native Indians", a term she can never give up. Our collective memories somehow wandered to that day when there was a certain competition in school where R was defending the वर्ण system. Igul was supposed to be her partner but she was clueless of everything and after all the debate was supposed to be about the evils of वर्ण. But now as all of us grow old and are no longer the youths we were the pratikriya changes one's perspectives. Igul we were pleased to note was consciously Hindu than ever before. Igul wondered, why me and R tend to yarn so much about the past --- school and college days as though neither of us ever grew up to be adults (in some cases even parents). This view of our juvenile freeze is shared with Igul by स्त्री, but our one other friend yarns like us endlessly on these days. I realized something really remarkably paradoxical at that point.\
Igul: School and college were great nice days, a faint nice blur to be forgotten now.\
स्त्री: Same\
R: They were bad days but should be talked about endlessly.\
Marc: Same\
Me: Same\
amAvasu: same\
The said women diagnosed us as suffering from a disease "juvenile freezure"...


