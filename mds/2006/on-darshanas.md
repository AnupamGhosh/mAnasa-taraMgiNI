
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On darshanas](https://manasataramgini.wordpress.com/2006/12/28/on-darshanas/){rel="bookmark"} {#on-darshanas .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 28, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/28/on-darshanas/ "6:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One thing many fellow traditional Hindus have found frustrating about me is my lack knowledge and lack of affinity for the advaita stream of vedanta, and more fundamentally me being a svatantra. My blasphemous views regarding vedas and their अपौरुषेयत्व are also hardly appreciated in their midst. I am not shaken by their views because I am pretty firm in my convictions on these matters arising from ongoing study of the originals, and याज्ञवल्य always stands out as beacon. These are indeed matters of worthy of deep historical study by any Hindu and it is advisable that they take nothing for granted. The key to their study is comparative philosophy -- a term first suggested by Brijendranath Seal, a modern वङ्ग scholar of early Hindu thought. Just like comparative linguistics is central to the understanding of Sanskrit, the language of Hindu thought, comparative philosophy is central to understanding the evolution of Hindu thought. It should be amply clear that the fundamental aspects of Hindu thought cannot be understood without comparative linguistics and philosophy, a point so blithely neglected by many traditional Hindus. Of course like most other sketches presented here the below ramblings are not completely worked out pieces of research.

How many आस्तीक darshana-s exist? If you ask this to an average educated Hindu he will probably give you the answer 6. This number is widely cited and has even acquired some kind of legitimacy. Early in my existence I felt there were perhaps only 3. Some years later I noticed that the great scholar from मिथिला गङ्गानथ झा was of a similar opinion. The pairing amongst the 6 darshanas is common knowledge amongst Hindus, but it is important to note that it goes beyond mere pairing --- the pair are actually two sides of a coin. Further, more importantly, there is definitely an evolutionary relationship between the core 3 and oft cited 6, which is key to understanding their development.

The core problem tackled Hindu and early Greek philosophies was what can be termed "the many-one problem". One way of stating it is that there are many identical, similar or congruent entities, that just appear to be multiple manifestations of one single prototype. In Sanskrit grammar we can reduce the various expressions of language to a small set of prototype rules. Similar we can classify the diversity of organisms to a few prototypes based on the principle of homology. Thus, many can be explained as few --- but, can this be taken to the minimal prototype (may be just one) which is the only reality, and also explain the multiplicity logically. To this end the Hindus applied themselves diligently.

We have सांख्य- it seems to have been there in the उपनिषद्स् because in 6.13 of the श्वेताश्वतर उपनिषद् we hear that सांख्य is the basis of mokSha. The basic premise of सांख्य's explanation-- mAyA, however, goes to the ancient Indo-Aryan past. It stems from a knowledge of the ways of gods! The gods exhibit mAyA- thus their one true or prototypic form appears in many diverse forms. This is true of indra in the ऋग्वेद:\
"[रूपम्-रूपं मघवा बोभवीति मायाह् कृन्वानस् तनुवं परि स्वां ।]{style="color:#99cc00;"}" (RV 3.53.8).\
In the इतिहासस् विष्णु and rudra put forth their mAyA by means of their [yoga]{style="font-weight:bold;"} to assume many forms. In the पुराण this mAyA is the great goddess माहाकाली. Right from the beginning it appears to have meant a means by which the deva or दानव creates many forms to "hide" his real one. Thus, the सांख्यन् solution appears to be actually a development of this original property of the divinities applied to describe the universe itself --- the one puruSha shrouded by mAyA creates a multiplicity that is seen as the world. This connection between the mAyA of devas and सांख्य ontology is very palpable in the explanation of संख्य expressed in the महाभारत:\
[अपां फेनोपमं लोकं विष्णोर्माया शतैर्-वृतम् ।]{style="color:#99cc00;"}\
[चित्त-भित्ति प्रतीकाशं नल सारमनर्थकम् ॥]{style="color:#99cc00;"} (Mbh-critical 12.290.57)\
"The universe is like the foam of water enveloped by hundreds of mAyA-s of विष्णु, like a painted wall and ephemeral as a hollow reed."

So we feel साम्ख्यन् thinking was always there in some form in the Indo-Aryan mind from its earliest stages. There is no evidence for this being well-developed in the Iranians- though I could be mistaken. The term mAyA as far as we can see, seems to be mainly an innovation of the Indo-Aryans, though the basic concept as applied to the gods may have existed elsewhere amongst Indo-Europeans. The idea that deva-s use their yoga to effect their mAyA suggests that it is an evolute of सांख्या. This comes out strongly in the इतिहास and पुराण --- one of the most famous expressions of this known to most hindus is the statement of कृष्ण in the bhagavad गीता:\
[नाहं प्रकाशः सर्वस्य योग-माया-समावृतः ।]{style="color:#99cc00;"}\
[मूढो।अयं नाभिजानाति लोको माम्-अजम्-अव्ययं ॥]{style="color:#99cc00;"} (BG7.25)\
"I am not manifest to all veiled by the yoga of my mAyA. This deluded world knows not me unborn and unlimited."\
Again at sunset on the 14th day of the Great War देवकी-putra turning to arjuna says: "arjuna, I have obscured the sun by the means of my yoga and the kauravas think it has set. Shoot jayadratha!"

Another poignant expression of the fact that the display of yoga is essentially the same as the display of mAyA is suggested by the story of the early भार्गव उशना काव्य given in Mbh 12.278 (Critical). काव्य was enraged with the deva-s because विष्णु had beheaded his mother who was a partisan of the asuras. काय्व used his [yoga ]{style="font-weight:bold;"}to enter into kubera, यक्ष who was the treasurer of the deva-s and stole his wealth. Furious kubera went to rudra and told him that काव्य used his yoga to enter kubera's body and having robbed him of his wealth came out of it and escaped. rudra himself of supreme yoga power raised his dreaded trident and sought काव्य with the intention of striking him. काव्य realized from a distance the intention of rudra of superior yoga powers and wondered whether he should flee or try some other trick. Then using his mighty yoga उशना काव्य, that physician of the daityas, became small and went and sat on the tip of rudra's trident. Unable to use his weapon he bent it with his arms to make it into the पिनाक bow! At this point उशना fell into shiva's hand, who promptly swallowed him and returned to perform his yoga meditation. The भार्गव wandered endlessly in rudra's stomach and was absorbed into shiva's body. As shiva had shut all his outlets in practice of yoga he was unable to find an exit. Unable to escape he repeatedly worshiped the terrible महादेव who asked him to emerge from his semen. Thus, did भृगु drop out when rudra saw him and raised his trident to kill him. But उमा intervened and asked him to spare the ब्राह्मण.

The point of note here is that the famous भार्गव magic of परकाय-pravesha is effected by means of yoga, which is strikingly parallel to the ability of devas to exhibit mAyA. This is further clarified in the great इतिहास in course of the description of yoga:

[ब्रह्माणम्-ईशम् वरदं च विष्णुम् । भवं च धर्मं च षडाननं च सो ब्रह्मपुत्रांश्-च महानुभावान् ॥ ५८]{style="color:#99cc00;"}\
[तमश्-च कष्टं सुमहद्-रजश्-च सत्त्वं च शुद्धं प्रकृतिं परां च ।]{style="color:#99cc00;"}\
[सिद्धिं च देवीं वरुणस्य पत्नीं तेजश्च कृत्स्नं सुमहच्-च धैर्यं ॥ ५९]{style="color:#99cc00;"}\
[नराधिपं वै विमलं सतारं विश्वांश्-च देवान् उरगान् पितॄंश् च ।]{style="color:#99cc00;"}\
[शैलांश्-च कृत्स्नानुदधींश्-च घोरान् नदीश्-च सर्वाः सवनन्घनांश्-च ॥ ६०]{style="color:#99cc00;"}\
[नागान्-नगान्-यक्ष-गणान्-दिशश्-च गन्धर्व-सङ्घान्-पुरुषान्-स्त्रियश्-च ।]{style="color:#99cc00;"}\
[परस्परं प्राप्य महान्-महात्मा विशेत योगी नचिराद्विमुक्तः ॥ ६१]{style="color:#99cc00;"}\
ंBह्(12.289.58-61)\
The high-souled yogI filled with greatness, can enter into and come out of, at will, brahmA the lord of all, the boon-giving Vishnu, bhava, dharma, the six-faced कुमार, the sons of brahmA, tamas that results in much trouble, rajas, sattva and the pure प्रकृती, which is the foremost, the goddess siddhi -- the wife of वरुण, and all energy, courage, the king, the sky with the stars, the universe, celestial snakes, the manes, mountains, all terrible oceans, all rivers, thick forests, serpents, plants, यक्ष bands, the directions gandharva bands, and both males and females.

Thus it is clear the yoga was precisely the practical means of "effecting mAyA" --- the yogI could literally enter into all possible रूपस्. Hence, yoga was probably associated with the very same concept of संख्यन् mAyA right from its origins. We specifically see that it was a means of entering that प्रकृति which is \[the cause of] mAyA as per संख्य. साम्ख्य and yoga appear to be two faces of the same darshana. The former stands for the siddhAnta or the theoretical framework and the latter for the practical means of achieving it. The earliest discoveries of importance in this direction were made by the Indo-Aryans probably in the late ब्राह्मण period of the vedic era. We have no clear evidence if these discoveries were known before of not, but some back ground knowledge was probably there in the medical investigation of the atharva veda. One discovery was that there was something peculiar about the problem of consciousness and that it could be tackled in a special way by eliding the subject-object distinction. The second discovery was that this issue of consciousness could be understood only by some special physiologically alterations connected to the nervous system. This latter realization led to the primitive "[कुण्डलिनि system](http://somasushma.googlepages.com/bhrigu_smriti_hs-1.pdf)" \[see the discussion in our note on the भृगु स्मृति]. This system's earliest traces in the late ब्राह्मण period are seen in the ideas of the भार्गव प्राचिनयोग्य and याज्ञवल्क्य. It was further developed in the भृगु स्मृति, but still these ideas were not yet formal. An early synthesis was attempted by पतञ्जलि in his system, but this was mainly an attempt to give yoga an independent darshana status by incorporating साम्ख्य as its internal siddhAnta in a slightly modified form. The real task of synthesis was taken up the tantras, which formalized the whole practical system with its physical angles, the "कुण्डलिनि system" and most importantly the mantra-शास्त्र which is the oldest tradition of the Hindus. It is this tantric synthesis that was one of the greatest achievements of Hindu thought in this direction, which until recently was neglected by most formal theoretical students of Hindu philosophies. The name of the great matsyendra, "the first guru of the kaliyuga", is to be remembered by all.

If mAyA appears to be an Indo-Aryan innovation, how then does mAyA's equivalent appear in Greek philosophy. An answer to this might solve a central issue in comparative philosophy.


