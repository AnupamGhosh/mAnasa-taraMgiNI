
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The 3 great impact craters of India](https://manasataramgini.wordpress.com/2006/10/15/the-3-great-impact-craters-of-india/){rel="bookmark"} {#the-3-great-impact-craters-of-india .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/15/the-3-great-impact-craters-of-india/ "6:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/lonar.0.jpg){width="75%"}
```{=latex}
\end{center}
```



 1.  Luna 2) Ramgarh 3) Lonar

