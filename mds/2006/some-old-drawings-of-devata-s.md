
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some old drawings of देवता-s](https://manasataramgini.wordpress.com/2006/05/01/some-old-drawings-of-devata-s/){rel="bookmark"} {#some-old-drawings-of-दवत-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/01/some-old-drawings-of-devata-s/ "5:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/deva1_small.jpg){width="75%"}
```{=latex}
\end{center}
```



While cleaning up, I found some old pictures R had drawn for me while in school. It brought me some nostalgia and with due permissions I scanned them to put them up. She drew much better later in life, but I was never able to get her to draw more like these again. It is sad that a great tapestry of the yogini-s, वाराही, मन्त्रिणी, अश्वारुढा and कामेश्वरी, in their war march, that she drew with considerable detail seems to be lost now.

In the first on the left is rudra with two arms with धनुः and इषु; then there is the lord of the gods indra, with a vajra and sword; to the right there is विष्णु with his usual weapons. She asked me to state that these pictures were dedicated to मोलाराम्, a famous shAkta artist from the Himalayas.


