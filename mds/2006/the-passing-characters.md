
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The passing characters](https://manasataramgini.wordpress.com/2006/05/14/the-passing-characters/){rel="bookmark"} {#the-passing-characters .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 14, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/14/the-passing-characters/ "6:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Fancying myself as the wandering व्रात्य I wandered in the मानुष लोकः. I noticed many leaving images in the eye and then in the brain. At face value these images are useless, but who knows, they may be the well-springs of future thoughts on the great moha. I appreciate दण्डिन् that jewel of the बृहत्चरण-s because of the word "bhramati". I wandered beside the river holding my कपाल given to silence observing all. Poor and rich men of various races. Men bereft of skills, very skilled but un-wanted and full of handy abilities. Scurrying like शैलूषस् and मागधस् and mankha-s they went about their business hoping to fill their bellies. There were women with the great beauty of yakSiNis and apsaras and others with the form of राक्षसिस् in ashoka-vana, also going about their existence. We had intersected in life with a friend who discovered humor in Telugu and Tamil films. We wondered if there were others in the world on that path --- it was not before long in our wanderings before we saw many such. We saw another northerner revelling in his chalices of mada. Another northerner was singing songs of तौरीय gAnaM, moving all hearers. We saw others reveling in their respective moha-s, the ephemeral world. Their jollity was much like the boat ride we were on.

We intersected with some and completely failed to do so with others. We used to play a private game with our मण्डल. We would choose the name of a gene from a chosen organism and ask the other person to tell us all s/he knew of the protein it coded. When one could not the other won a point. Only those who could play this game were our true friends, else our other true companions were clansman. As we were wandering we wished to play the game, but we had none by our side to do so. So we used our divya दृष्टि to connect with one who could and continued to do so. We then heard a group of drunken शैलूष-s, मागध-s and पुंष्cअलि-s making a lot of noise. We moved past all that as the व्रात्य learning of indra's gifts to the world of men. We then saw a महाजन, who was considered the eko-मानुष आनन्दः in out midst. He was seated on throne, with bone and stones about him, attended by his family, swilling various pAnaka-s and eating cullinary delights. We moved to him, who was endowed with all the splendor and humbly asked how is it that he had come to be thus favored by maghavan. He pointed to the tulA above the वृश्चिक. We had learned the lesson of the तुलाधर.


