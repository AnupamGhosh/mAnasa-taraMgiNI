
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The unmatta bhairava तन्त्रं of the संमोहन tradition](https://manasataramgini.wordpress.com/2006/11/11/the-unmatta-bhairava-tantram-of-the-sammohana-tradition/){rel="bookmark"} {#the-unmatta-bhairava-तनतर-of-the-समहन-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/11/the-unmatta-bhairava-tantram-of-the-sammohana-tradition/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

दानवेन्द्र realized that if he could get hold of the अमृत of the deva-s he would be victorious. For this he went over to the circle of मातृका-s headed by चक्रकरालिनी and invoked them to fight on his side. He also got the अनन्तवीर-s who had emanated from the 8 bhairavas starting from असिताङ्ग to कपाल to fight on his side. As a result the मातृका's and vIra-s attacked the deva-s and the great bhairava, and while they were fighting दानवेन्द्र stole their soma. He started to dance victoriously and the devas and bhairava seeing their shakti-s and vIra-s turn against them were stupefied. Then चक्रकरालिनी and the मातृका-s rushed to eat up the deva-s and bhairava. As they were opening their mouths and sticking out their tongues, rudra tried to save the situation. He created a mada-सागर. He then emanated a 4-petalled lotus into that मदसागर. On the 4 petals sat the 4 देवी-s जया, विजया, अजिता and अपराजिता, and in the middle sat the terrifying 4 faced emanation of महादेव- [tumburu](http://manollasa.blogspot.com/2005/02/tumburu-manifestation-of-rudra.html). They played melodiously on their वीणस् and captivated the मातृका-s and held them spell bound. When they were thus de-activated, the deva-s and tumburu attacked दानवेन्द्र and killed him.

tumburu and his 4 sister goddess revealed the महासम्मोहन saMhitA to महेशान, from him to sammohana (after whom the tantra is now known), from him to ananta, from him to त्रिमूर्ति, from him to dundubhi, from him to नीलकण्ठ. This is the amaraugha.

The lord of the vINa-शिखा became wild with passion on seeing the intoxicating beauty of दाक्षायणी-काली. A बीजाक्षर emanate from him then which manifested as the unmatta-bhairava of ghora रूप. The goddess after embracing the great bhairava asked him the विद्या of the महाघोर रूप of unmatta-bhairava. It was then that unmattabhairava propounded the जायातन्त्र and the unmatta-bhairava सूत्रस्. This was known to many students, but नीलरुद्र imparted it with all bhairava tantras to the भार्गवस्. The भृगु-s also received the संमोहन from him and the unmattabhairava or भूत-DAmara tantra as an upa-tantra of that. This is the मानवौघ.

In the surviving condition there are three tantras that appear to have had a common origin- they are उन्मत्तभैरवं, भूतडामरं (आस्तीक) and भूतडामरं (nआस्तीक). Both the आस्तीक and nआस्तीक versions in this case are close suggesting that not many changes beyond the obvious were made in either stream. It appears that this might have been a product of the symbiotic relationship between the आस्तीक-s and nआस्तीक-s. The उड्डीशं also appears to be a more divergent relative of the same, which is more firmly in the purely आस्तीक camp. From उड्डीश, in the Himalayan country was derived the well-known उड्डामरेश्वरं. We also suspect the उड्डीशं and unmatta भैरवं were the original आस्तीक versions that was taken up by nआस्तीक-s, and then the आस्तीकस् re-obtained this from them with minor modifications as their version of भूत-DAmara.

आस्तीक भूत-दांअरं and उन्मत्तभैरवं open with the lines:\
[व्योम-वक्त्रं महाकायं प्रललाग्नि-समप्रभं ।\
अभेद्य-भेद्यकं स्तौमि भूतडामर-नामकं ॥]{style="color:#99cc00;"}

Technically the face of rudra specifically being referred to here is इशान (vyoma being the Urdhva mukha), which in the usual system refers to face from which the siddhAnta tantras arose. But in the context of this tantra unmatta-bhairava is taken to be the emanation of इशान-shiva:

[त्रैलोक्याधिपति रौद्रं सुर-सिद्ध-नमस्कृतं ।\
उन्मत्त-भैरवं नत्वा पृच्चत्य्-उन्मत्त-भैरवी ॥]{style="color:#99cc00;"}

The first four mantras appear to belong to the vajra-series that were deployed in the vajra-मण्डल tantras used in certain shiva temples:\
[ऒं वज्र-ज्वालेन हन हन सर्व-भूतान् हूं फट् ॥]{style="color:#99cc00;"}[\
]{style="color:#99cc00;"}[भूत-DAmara mantra]

[OM vajra-mukhe sara-sara फट् ||]{style="color:#99cc00;"}[\
]{style="color:#99cc00;"}\[this is known as the विज्ञानाकर्षणी mantra]

[ॐ संघट्ट-संघट्ट मृतान् जीवय स्वाहा ॥]{style="color:#99cc00;"}[\
]{style="color:#99cc00;"}[मृत-संजीवनि mantra]

[ऒं हन हन सर्वं मारय मारय वज्र-ज्वालेन हूं फट् ॥]{style="color:#99cc00;"}[\
]{style="color:#99cc00;"}[महामारण mantra]

The mantra of सर्वाप्सरस prayoga of these tantras is thus given:\
[ॐ श्रीं तिलोत्तमा । श्रीं ह्रीं काञ्चन-माला । ॐ श्रीं हुं कुल-हारिणी ।]{style="color:#99cc00;"}[\
]{style="color:#99cc00;"}[ॐ हुं रत्नमाला । ॐ हुं रंभा । ॐ श्रीं उर्वशी । ॐ रमा भूषिणी ॥]{style="color:#99cc00;"}


