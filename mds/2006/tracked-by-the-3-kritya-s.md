
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Tracked by the 3 कृत्या-s](https://manasataramgini.wordpress.com/2006/05/13/tracked-by-the-3-kritya-s/){rel="bookmark"} {#tracked-by-the-3-कतय-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 13, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/13/tracked-by-the-3-kritya-s/ "5:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Mn, Jx and me assembled for the long awaited Kuriltai of the clan on the steppes. The only one missing from our old clan assembly was the muni, who was doing his time beside the somesha shrine where we spent the most liberated days of our existence when our aged clansmen who have now attained vaivasvata were still alive. Those days were our introduction to the सनातन dharma and aswell as the seeds of our future. She who is not a clanswoman but our companion in exploits like एकानंशा was also missing on account of her advanced level of maturity probably having surpassed us ruffians when we became a band. We were having this संगम after a gap of more than 10 years. We both realized how Jx had been freed from the grip of the अभिचार of Fourier's party by his yoga. Jx was doing pretty well and in good health. Mn and me were a touch nervous about the place were we held our Kuriltai for we were not free from the cloud-- it was the place that proved my Gaugamela on not one, but two occasions. Mn and me knew that if we were found together the बगलामुखी attack could be simultaneously launched on us. Of the clansmen Mn was rarely around these days so the whole संगंअ was as rare as a conjunction marking the begining of the kaliyuga per the Hindu calender. Mn's behavior was best approximated by that of a musalendra, while the muni who stood on the other shore thought and acted possibly much like us.

Given the rarity of the event we were savoring every moment of it, forgetting all about the कृत्या-s, behaving like the ruffians we naturally were. Our sakhi had said ["Mn and Jx lie in the middle because of their genes. The changing memes around them have made them move from the middle to the end. You lie at the other end. But together you all are back to the middle"]{style="font-style:italic;color:rgb(255,0,0);"}. I confirmed this and remarked to myself any one should be a fool to doubt the evolutionary theory. We were seeing it before us, like a कालरात्री, consuming all, like the punishing dharma-daNDa of deva वरुण, nay we could see ourselves rushing into its gaping mouth like सूतपुत्र, droNa and suyodhana. By virtue of their genes they were there and were here. I privately heard the news that shveta-शूद्र was deep trouble. The muni alone, we said in unison was the bearer of the weight, though one could never write of the Mn.

Mn remembered the "crane's flight" and strangely we saw the sign of the monstrous crane as we were ambling through. I allowed Mn to go ahead and tarried with Jx. To my horror I saw the 3 three come after me: अवचना, शुचिमुखा and लोहशृङ्खला. I parried अवचना with my protective विद्या's and made her vanish. But my horror अपरिक्लिन्ना rushed after me. Temporarily she left me to pursue Mn. I was tortured repeated by शुचिमुखी, but I kept her at bay. Me and Mn stuck to Jx as the umbrella of his protection also kept us safe. When, finally the kuriltai ended I stepped away hoping that the rahasya of atharvana भद्रकाली would keep me safe. I was attacked at that point by लोहशृङ्खला-- after a fiery attempt I broke past her with my own bala. While safely at the other end I saw the मूलकृत्या escape and hide under pillow. अवचना, अपरिक्लिन्ना and शुचिमुखी were still hovering around but were unable to completely breach my shield while they repeatedly kept poking.

The muni seeing the danger sent a जानस्रुतेय bird: "Most people think that "yAM कल्पयन्ती" is the rahasya of प्रत्यञ्गीरा, but the true मूल rahasya is the त्रिष्टुभ्. That is अथर्वण bhadra काली herself. It is the yoni of indra. You know that those who really know the atharvan lore know this rahasya. You know that three are the cats. indra is the emergent and indra is its striking power. Know it to be so. But between you and the कुण्ड lies the कृत्या. You are surrounded like abhimanyu, but those who know it can penetrate the chakra व्यूह."


