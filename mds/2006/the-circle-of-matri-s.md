
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The circle of मातृ-s](https://manasataramgini.wordpress.com/2006/08/07/the-circle-of-matri-s/){rel="bookmark"} {#the-circle-of-मत-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 7, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/07/the-circle-of-matri-s/ "5:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/mothers.jpg){width="75%"}
```{=latex}
\end{center}
```



The mighty bhairava stands in the midst of the circle of eight mothers (He is invoked by the formula: स्खां स्खीं स्खौं महाभैरवाय नमः||). On either side of the mothers are positioned the roaring वीरभद्र who emerged from the hair of The god and the frightful विनायक with a single tusk, both striking terror in the heart of the beholders. वीरभद्र is surrounded by several awful pramathas brandishing all manner of terrible weapons. The terrible विनायक is dancing performing a loud अट्टहास, holding a sharp axe in one of his stout arms. He is surrounded by the fearful goblins armed with all kinds of weapons. Each of the eight mothers is surrounded by her daughters. Together they form the complete circle of mothers. They are-

brAhmI's daughters: अक्षोद्या; अक्ष्कर्णी; राक्षसी; क्षपणक्षया; पिन्ङ्गाक्षी; अक्षया; क्षेमा.

रौद्री's daughters: iLA; ऋलावती; नीला; लञ्का; लञ्केश्वरी; लालसा; विमला.

कौमारी's daughters: हुताशना; विशालाक्षी; ह्रूञ्कारी; वडवामुखी; हाहारवा; क्रूरा; क्रोधा; bAlA; खरानना

वैष्णवी's daughters: सर्वज्ञा; तरला; tArA; ऋग्वेदा; हयानना; sArA; सारस्वयंग्राहा; शाशवती

वाराही's daughters: तालुजिह्वा; रक्ताक्षी; विद्युज्जिह्वा; कपालिनी; मेघनादा; प्रचङ्दोग्रा; कालकर्णी; कलिप्रिया

इन्द्रानी's daughters: champA; चम्पावती; प्रचम्पा; ज्वलितानना; पिशाची; पिचुवक्त्रा; लोलुपा;

चामुण्डा's daughters: पावनी, याचनी; वामनी; दामनी; बिन्दुवेला; बृहत्कुक्षी; विद्युतानना; विश्वरूपिणी

महालक्ष्मी's daughters: यमजिह्वा; जयन्ती; दुर्जया; यमान्तिका; बिडाली; रेवती; जया; विजया.

The hordes of mothers are all armed with deadly weapons like the gods from whom they have emerged.

There is only a single surviving fragment of the icongraphic representation of this circle of mothers from a now lost temple in Madhya Pradesh. The yantra of the circle of mothers is a simple one.


