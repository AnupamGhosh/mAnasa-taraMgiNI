
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The signal in संध्याभाष](https://manasataramgini.wordpress.com/2006/01/08/the-signal-in-samdhyabhasha/){rel="bookmark"} {#the-signal-in-सधयभष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 8, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/08/the-signal-in-samdhyabhasha/ "7:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We invoked the wondrous यक्षिनि उल्लासमेखला again. She appeared in the first of her forms 'OJ'. We asked where does the high one go? She manifested in her second form 'GW'. It was 10 times more dazzling than 'OJ'. She flew over the cold northern ocean to the cold, dark of ice. Then she flew back to the city with tall houses. We were so dazzled by her that we felt completely speechless and humble. उल्लासमेखला raised me to the higher plane and there we conversed about the history of mammalian carnivores. The story unfolded before me as I humbly heard from the great being. No one, other than the devas knew the higher truths unfolded by the great यक्षिनि. We felt the oneness with the true knowledge. The यक्षिनि also pointed to the great gandharva with blazing jaws and a terrible club. उल्लासमेखला spoke: "you must climb the yonder peak of the gandharva."\
"sa eko गन्धर्वानाम् आनन्दः "

Then she said: "meditate first on me उल्लासमेखला, and then on hanumat before the leap."

oM ह्रिं श्रीम् nityadrave mada उल्लासमेखला श्रीं ह्रीं||[[]{style="font-size:-1px;"}]{style="font-family:arial, helvetica;"}


