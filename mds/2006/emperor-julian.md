
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Emperor Julian](https://manasataramgini.wordpress.com/2006/07/27/emperor-julian/){rel="bookmark"} {#emperor-julian .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 27, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/27/emperor-julian/ "5:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many years ago when we were yapping away on the then latest production of the socialist academic Jared Diamond, Guns Germs Steel Blah blah, we turned our attention to two historical incidents which were reasonably faithfully described by him. One was the destruction of the Native South Americans by the Spanish conquistadors and the other was the cannibalizing of one Austronesian tribe the Moriori by another the Maori. Given his Marxist bent it is not at all surprising that he went on to equate the two events, and try to explain them through his universalist view of geographic contingency, which he repeats [ad nauseum]{style="font-style:italic;"} in that long-winding tome. The point he missed was that, despite the similarity in outcome, the Spanish destruction of the Native South Americans and Mexicans was something different -- the technological difference was merely a sideshow- in reality it was the result of the memetic infection of Isaism that had seized the European mind.

It was at this point that our mind went back to the one great Hero of Rome, for whom we had the highest respect for long. There were a some respectable emperors of Rome, like Julius Caesar, Augustos, Marcos Aurelius and the like, but of all of them we could relate most to the emperor Julian. He was the one man on whose fate the history of the world rested and swung the bad way, as though drawn by the downward turn of the wheel of the yuga-s. Isaism's career and as a violent Abrahamism that was to bring misery to the world began with the conversion of the Roman emperor Constantine-I. His sons Constans, Constantius and Contantine-II were even more fanatic proponents of Isaism, under whose reigns, it began terrorizing and suppressing the old religion of the gods. But most remarkably, the nephew of Constantine, Julian, was poised to reverse this evil, when fate snatched his life away. His parents and brothers, all had been killed by his cousin Constantius, and he was the only survivor of their clan other than former. Julian had been forcibly brought up as a Isaist, but right from an early age he repudiated this barbarous evil, and was a secret worshiper of the gods. Secretly reading the heathen literature he became a capable scholar and then was initiated into several secret pagan rites like the worship of Mithra (the Iranian cognate of our hallowed god mitra, whom we invoke daily with the ऋक्-s of विश्वामित्र) and the mother of the gods. When he was summoned by his cousin, most probably with the intention of killing him, he paused at Athens, where the Isaists had descrated the ancient temples and fire altars of the gods. But a cryptic heathen priest showed him that he had actually protected the shrines and the sacrificial fires still burned for the gods. This left a profound impression on him and he decided to restore the old religion which was being destroyed by the Isaists. After several victorious military campaigns against the Germans where his cousin tried to downsize him, he was declared emperor by his troops and restored the old rites to the gods. When finally he challenged his cousin, who died before the battle, and Julian came to power as the sole Roman emperor. In his short but remarkable reign he more or less reversed the damage of the Isaistic madmen.

Julian then fatefully went to war against the Iranians thinking he was a reincarnation of Alexander of Macedon. After initial successes in battle against the powerful Iranian army of Shapur, he pushed deep into Iranian territory like only one Roman emperor before him had done- Trajan. But here he fell into the Iranian trap. Shapur's troops cut off his supplies, and suddenly attacked the Roman army. As his left-wing was faltering he rushed to shore up the defenses, and in the hurry forgot to strap his armor properly. He received a spear thrust that pierced his ribs and punctured his liver and fell wounded. His physicians tried to suture his wound and he feeling slightly better wanted to lead his troops again, but his wound opened and he fell dead. With that the Romans were beaten into an ignomious retreat by the Iranian army. Julian was struck by an Isaist on the Roman side rather than an Iranian. At that moment was unleased the first of the most dreaded plagues on the world; the second was to start a few centuries later by taking down the Iranians.

Luckily several of Julian's writings survive and a person who is not tainted by an Abrahamism can get glimpses of the man. A true heathen alone today will immediately identify with Julian.

\*Firstly he realized that heathens needed name to call themselves in order to fight the "non-self" distinguishing Isaists. Thus he coined Hellenes, even as the only surviving heathens today, use Hindu to describe themselves in face of the Abrahamistic attacks.

\*He developed some order for the heathens so that they could withstand the attack of the well-organized Isaist predators.

\*He supported and developed Neo-Platonism as the philosophical foundation of the heathen religion. This is most remarkable because Neo-Platonism if correctly understood is remarkably parallel to the development of the Astika darshana-s in the Indic world, with close similarities to both साम्ख्य and vedAnta. Importantly, it encompassed a common philosophical frame work for Roman religious elements equivalent to the temple-Agamic, tantro-yogic and vedic sacrificial religion of India.

\*He identified himself with the both old sacrificial religion to the old deities and the "mystery" rituals of Neo-Platonic provenance. Here he was remarkably parallel to the highest brahmin savants of the medieval period who was simultaneously comfortable with the shrauta and tantric rituals.

\*He launched doctrinal attacks on the foundations of Isaism, rather than merely terrorizing them with his state apparatus and was pretty mild on them for all the vandalism of heathen temples and altars they had engaged in.

We can see his far-reaching pagan visions in his works:\
[\
"The Sun's resplendent deity I sing,]{style="font-style:italic;color:#ff9900;"}\
[ The beauteous offspring of almighty Jupiter,]{style="font-style:italic;color:#ff9900;"}\
[ Who, through the vivifying solar fount]{style="font-style:italic;color:#ff9900;"}\
[ Within his fabricative mind concealed,]{style="font-style:italic;color:#ff9900;"}\
[ A triad formed of splendid solar gods;]{style="font-style:italic;color:#ff9900;"}\
[ From whence the world's all-various forms emerged"]{style="font-style:italic;color:#ff9900;"}

\*offspring of the almighty Jupiter: nepot magnus Jovis and the famed epithet "divo नपात" or "asuro महोदिवः" immediately resonate to one linked to his cultural past.

  -  A triad formed of splendid solar gods: A heathen with an unbroken link to his past can immediately see the link to that ancient motif-- to the 3 Aditya's hymned by the ऋषि's of yore: mitra, वरुण and aryaman.

["Famed mystic bards of old, in sacred song,]{style="font-style:italic;color:#ff9900;"}\
[ By thee inspired, as the arrow-darting god,]{style="font-style:italic;color:#ff9900;"}\
[ Constant invoked thee, with resistless sway,]{style="font-style:italic;color:#ff9900;"}\
[ Because thy vigorous beams like arrows pierce,]{style="font-style:italic;color:#ff9900;"}\
[ And totally, whatever of measure void the world]{style="font-style:italic;color:#ff9900;"}\
[ Inordinate or dark contains, destroy."]{style="font-style:italic;color:#ff9900;"}

\*Old sages praise the arrow-darting god: We will immediately recall the praises of our ancestors to sharva who showers his arrows even as Apollo-- sharva=the arrow-darting god.

He was truly a great man with the true connection to the gods.


