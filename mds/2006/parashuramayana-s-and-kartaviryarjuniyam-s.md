
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [परशुरामायण-s and कार्तवीर्यार्जुनीयं-s](https://manasataramgini.wordpress.com/2006/11/05/parashuramayana-s-and-kartaviryarjuniyam-s/){rel="bookmark"} {#परशरमयण-s-and-करतवरयरजनय-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/05/parashuramayana-s-and-kartaviryarjuniyam-s/ "5:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The one great hero of our clan, the omnipresent and poly-faceted symbol of brahmin-hood lives a strange life in the Hindu mythosphere. रामो-bhargava has become an अवतार puruSha of नारायण but hardly enjoys the status of देवकी-putra or his name sake of the इक्ष्वाकु clan. There are no major mantra-s devoted to him, and even in the महाभारत where he makes a prominent appearance, only next the main protagonist अवतार-s, the देवकी-putra and baladeva, he is only once called an अवतार. Yet, starting from the महाभारत he has an increasingly prominent place in the Hindu mythosphere of the पौराणिc realm appearing all over over the place in addition to his main encounter with the haihaya king कार्तवीर्यार्जुन and his priest, datta the atri. He is seen roguishly barging into shiva' s abode and enters into a fight with विनायक where he is said to break his tusk. The fierce भार्गव also fights कुमार several times and is soundly beaten by the god. He is said to have received a mace from कुमार that he wielded with great ability in his many battles. He is seen transmitting श्रीविद्या in the form of the kaula kalpa-सूत्रस् from दत्तात्रेय to sumedhas. Elsewhere, he is shown teaching kalki the ways of war for the final destruction of the म्लेच्छ-s and dasyu-s who are overruning भारतवर्ष. Despite his vast popularity he does not receive the devotions like those रामचन्द्र and कृष्ण, nor his story widely followed like the रामायण or the हरिवंश.

Nevertheless, there are a series of circum-पौराणिc texts that go under the title परशुरामायण that provide a description of his exploits in epic proportions. Most of them are hardly known to the modern Hindu and the accounts are zealously held by the knowledgeable भार्गव families. The भृगुवंश, the भार्गव-rAma इतिहासं and भार्गव-रामायण are the main volume preserved by the भार्गव-s whose incomplete and crumbling manuscripts were written in an old Telugu script and hence I was unable to handle successfully with my poor epigraphic skills. However, the preservation of major portions of the भार्गव lore was effected by an obscure scholarly Maratha chieftain साबाजी pratap between the 1500-1600 CE who ruled a jAgIr in the regions south of Ahmednagar. साबाजि collected the above texts and some others like the agastya माहात्म्यं, दत्तात्रेय पुराण (from which I narrated a couple of accounts earlier), कार्तवीर्यार्जुनीयं and the closely related परशुरामायण. He compiled from these a trilogy titled: परशुराम-प्रताप, भार्गवार्चन-दीपिका and भृगु-वंश-महाकाव्य rendering in classical poetic form the prose of the older भृगुवंश.

I came across divergent versions of the tale of our ancestress रेणुका. We narrated the version of her death due to the sons of the haihaya earlier. This is followed by the दत्तात्रेय पुराण and भृगु-वंश (and its महाकाव्य). The भार्गवार्चन-दीपिका mentions another peculiar tale: जमदग्नी was killed by तालजञ्घ and others while he was performing japa. रेणुक readies to cremate him, and moved by the pangs of separation herself jumps into the funeral pyre. At this point indra rescues her from the flames and finds that her body is full of blisters. He sends a cooling shower on her and then appoints her as the deity of viral skin infections, commonly called शीतला.

When we were young we were read the remarkable tale of शान्दिमत् from the भृगुवंश: रामो-भार्गव had a spectacular golden gem-studded crown given by indra that he wore when campaigning against the 21 generations of kShatriya-s. After the blood bath was over and he had performed the यज्ञ to indra, he decided to place the glorious crown in a secret island as wealth for the भृगु-s. It is said for this purpose he chose the island of शान्दिमत् that was surrounded by "sea monsters", whales and sharks and inhabited by diverse sharabha-s and भेरुण्ड-s. It was there that he built a fort in the middle of the sea and deposited his crown amongst precious shells and pearls. The day I heard this tale I had a dream of the going to the shandimat island and recovering the wealth of my ancient clansman. I have had repeats of this dream and it has never ceased to fascinate me when I have had it. There is an inscription of a choLa king, where he claims to have recovered the crown of the भार्गव.


