
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Notes on early पान्चरात्र vaiShNavism](https://manasataramgini.wordpress.com/2006/03/27/notes-on-early-pancharatra-vaishnavism/){rel="bookmark"} {#notes-on-early-पनचरतर-vaishnavism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 27, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/27/notes-on-early-pancharatra-vaishnavism/ "6:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is likely that in the later period the वैष्णवस् were ignorant on why the name पाञ्चरत्र came into being. This ignorance of the meaning of the name is plainly seen in the explanation provided by the prashna saMhitA and padma saMhitA of पाञ्चरात्र, and the नारदीय पाञ्चरात्र has only a vague memory regarding the cause of the name. The कश्मिराचर्यस् of महाबल paNDita's school and विद्यारण्य yati cite a number of formulae of पाञ्चरात्र vaiShNavism. But the कश्मिराचर्यस् clearly explain the etymology by citing the vihagendra saMhitA and the सनत्कुमार saMhitA. विष्णु in course of the five night rite, in which puruSha नारायण was invoked, lectured details on the पाञ्चरात्रिc mantra शास्त्र to various recipients. As per the holy सनत्कुमार saMhitA, which only survives in fragment the recipients of the lectures on the 5 nights were rudra, prajApati, indra, the ऋषिस् and बृहस्पति. This knowledge was collected by the चित्रशिखण्डिन्स् and provided as the वैष्णवागमस्. The same text mentions that विष्णु also explained this knowledge (probably implying कृष्ण) directly to the सात्त्वत heroes like सात्यकि, अक्रूर and कृतवर्मन् which came to be the सात्त्वत branch of पाञ्चरत्र. The vihagendra saMhitA states that the five recipients were ananta (=nAga), garuDa (=vihagendra), विष्वक्सेन, rudra and prajApati, of which the vihagendra saMhitA is the collection of the lectures to garuDa. The Ishvara saMhitA mentions that the five recipients (at least in the ऋषि class) were ब्राह्मणस् of the clans : शाण्डिल्य, औपगायन, मौन्जायन, kaushika and भारद्वाज. In addition to पञ्चरात्र the हयग्रीव tantra remembers an independent tradition of tantric vaiShNavism termed सप्तरात्र, which was parallel to पाञ्चरात्र, and contained the trailokyamohana विष्णु tantra and हयग्रीव tantra amongst others.

The सनत्कुमार saMhitA is clearly divided into the 5 sections called रात्रस् with ten-eleven chapters in each, with each corresponding to the lecture of a night between विष्णु and the recieving deity or sage (of them the बृहस्पति ratra is lost). While it has some very late corruptions and extrapolations potentially attributable to the श्रीवैष्णवस् or माध्वस्, the सनत्कुमार saMhitA by its content and memory of the actual origin of the term is close to the source. The सनत्कुमार saMhitA assumes the coeaval presence of a text called नारद saMhitA (in part a potential precursor of the नारदीय पञ्चरात्र), mahendra saMhitA and padmodbhava saMhitA and the vihagendra saMhitA suggesting that these were composed together as a unit. The middle two could be internal citations to indra रात्र and prajApati रात्र (or more complete versions thereof).

The contents of the सनत्कुमार saMhitA fragment as we know it are pretty remarkable. The first section, the रात्र of rudra: 1) यागस् to deities: अग्नी याग, brahma याग, वैश्रवण याग, याग for pacifying the विनायक and the उच्छिष्ट ulka, श्री/लक्ष्मी oblations, the famed yaga to worship धूर्त skanda, saura प्रयोगं, rudra याग, yaga to pacify various shaktis, sacrifice for yama, indra याग, विष्णु याग, yaga for ashvins, worship of कामदेव, soma offering. गायत्रीस् for these deities and installation of their idols. 2) mantra-लक्षणं 3) great नारयण mantras and gAyatrI. माहासुदर्शन mantra prayoga, the vaiShNava श्मशान प्रयोगं to capture and deploy यक्षस् and यक्षिणिस्, rAkShasa श्मशान prayoga for sending राक्षसस् aided with sudarshana, garuDa mantra prayogas, sarpa prayogas 4) invocation of 1008 गणस् of विष्णु under विष्वक्सेन, 5) harinivedana 6) flowers for विष्णु worship 7) leaves for विष्णु worship 8) bathing rites for idols 9) public festivals, balis and related rites 10) construction of yantras of विष्णु.

The detailed tantric अग्निमुखं यागस् with 4 fires for नारायण and his व्यूहस् is provided in the brahma रात्र (this as far as my sources known is only available with some vaiShNava ब्राह्मणस् in श्रीरन्गं as a manuscript).

Most interestingly the सनत्कुमार saMhitA gives two forms of उपनयनं, the vedic form for dvijas and tantric अग्निमुखं version for women and शूद्रस्. On recieving that they may perform several rites, but the पाञ्चरत्रिc brahmin is still supposed to perform vaidika rites to various deities in addition to the internal पाञ्चरत्रिc sacrifices to vedic deities given in the rudra रात्र.


