
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Formulae for the rahasya प्राणायाम.](https://manasataramgini.wordpress.com/2006/07/09/formulae-for-the-rahasya-pranayama/){rel="bookmark"} {#formulae-for-the-rahasya-परणयम. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 9, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/09/formulae-for-the-rahasya-pranayama/ "5:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

प्राणम् me pAhi प्राणम् me jinva स्वाहा त्वा subhava सूर्याय |\
oM |\
अपानम् me पाह्यपानम् me jinva स्वाहा इत्वा subhava सूर्याय ||

