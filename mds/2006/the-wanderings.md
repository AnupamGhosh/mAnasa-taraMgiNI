
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The wanderings](https://manasataramgini.wordpress.com/2006/02/08/the-wanderings/){rel="bookmark"} {#the-wanderings .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 8, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/08/the-wanderings/ "6:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

yo vai रुद्रः sa भगवान् yash cha मृत्युस् tasmai vai namo-नमः ||

sa rudro vasu-vanir-vasudeye नमोवाके वषट्कारो .anu संहितः |\
tasyeme sarve यातव upa प्रशिषमासते |\
तस्यामू सर्वा नक्षत्रा vashe चन्द्रमसा saha ||

yo .agnau rudro yo .apsv antar ya ओषधीर् वीरुध Avivesha |\
ya imA विश्वा भुवनानि chaklape tasmai रुद्राय namo .astv agnaye |\
yo rudro .agnau yo rudro .apsv antar yo rudra ओषधीर् वीरुध Avivesha |\
yo rudra imA विश्वा भुवनानि chaklape tasmai रुद्राय namo-नमः |\
yo rudro .apsu yo rudra ओषधिषु yo rudro वनस्पतिषु |\
yena रुद्रेण jagad ऊर्ध्वं धारितं पृथिवी द्विधा त्रिधा धर्ता धारिता nAgA ye .अन्तरिक्षे tasmai रुद्राय vai namo-नमः |\
मूर्धानम् asya संसेव्याप्य् अथर्वा हृदयं cha yat\
mastiShkAd ऊर्ध्वं prerayaty अवमानो .अधिशीर्षतः ||

The god was our only companion-- we descended to पाताल and walked around the regions where we had seen yasha and roga. We began to reminesce: Then there was AshA, there was the इन्द्रजाल too. We flew like a bird that had never seen a cage, we were filled with the confidence of youth. Then we saw Hayastanika and her grandmother and started moving slowly towards the great श्मशान. We first passed the wooden bridge where numerous revellers were sporting by the river. Then we advanced to the path by the श्मशान. We were all by ourselves. There were trees and the rustling of their in the breeze. There was a little pond and a hollow beyond that and great grassy plains further beyond. There in silence we sat- we were seeing the विद्या. Towards us came those reduced to pretas. While those full of jIva ran away from us. That was the realm of bhavA-शर्वा, the भूतपति.


