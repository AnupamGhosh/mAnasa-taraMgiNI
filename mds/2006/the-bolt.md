
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Bolt](https://manasataramgini.wordpress.com/2006/06/11/the-bolt/){rel="bookmark"} {#the-bolt .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/11/the-bolt/ "5:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The news reached us that रक्षोहा agni had cleared 3 of the वृत्र and निरृति had gone back her way. We thought the clouds were clearing. But Fourier's man, who was feared as शूरवर्मन्, got the news too. Never had he been intimidated thus or his men faced such उच्छाटन-s. He accordingly released the deadly "crane arrow". Never was balance between life and death so clear.

Past mid-night we faced the terror of the crane. Never had anything been so directly threatening in our existence. As युधिष्ठिर said when yama visit yonder man we think he is not for us, but when he comes your own way we experience his real fear.


