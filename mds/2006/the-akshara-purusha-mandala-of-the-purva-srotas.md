
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The अक्षर puruSha मण्डल of the पूर्व srotas](https://manasataramgini.wordpress.com/2006/07/14/the-akshara-purusha-mandala-of-the-purva-srotas/){rel="bookmark"} {#the-अकषर-purusha-मणडल-of-the-परव-srotas .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 14, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/14/the-akshara-purusha-mandala-of-the-purva-srotas/ "6:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/shiva_puruSha.jpg){width="75%"}
```{=latex}
\end{center}
```



The five faced one manifests as the inactive matter at the origin, the all-pervading matter in the universe and the vaccuum from which all emerges. Hence is worshipped as:\
[शान्ताय नमः । सर्वगताय नमः । शून्याय नमः ।]{style="font-weight:bold;font-style:italic;color:rgb(255,153,0);"}\
These three states of the deva are stationed in the अक्षर puruSha मण्डल. The five short vowels are the faces and the 7 long vowels are the limbs of the body. They are conjoined with the nasal bindu. The visarga is the mighty astra, the त्रिशूल of tatpuruSha. All this is shiva and the core is his महामन्त्र [हौं]{style="font-weight:bold;color:rgb(255,0,0);"}.

To begin his worship the साधक shall perform the महामुद्रा which is form of the पस्चिमोत्तासन, with his hands grasping his feet and his head touching the feet. Then he shall perform the न्यासस् and meditate upon the मण्डल with the महामन्त्र.


