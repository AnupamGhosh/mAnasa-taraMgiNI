
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The iconography of diverse goddesses](https://manasataramgini.wordpress.com/2006/10/06/the-iconography-of-diverse-goddesses/){rel="bookmark"} {#the-iconography-of-diverse-goddesses .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/06/the-iconography-of-diverse-goddesses/ "5:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Wodeyar of Mysore, कृष्ण rAja, prepared a compendium of Hindu iconography from diverse classical Hindu sources. This collation called the श्री-tattva-निधी (STN) has become a useful standard for उपासक-s in amongst educated Hindus, especially from Southern India. Sadly, the full text is not easily accessible these days. It is useful to examine the succinct descriptions of iconography that he provides in his text.\
The STN is divided into 9 chapters:

 1.  shakti nidhi 2) विष्णु-nidhi including vaiShNava deities 3) shiva nidhi including shaiva deities 4) brahma, indra and the devas 5) planets, नक्षत्र-s, कृत्या-s and others 6) शलग्रामन्-s 7) लिङ्ग-s 8) Agamic and tantric issues 9) numbers, and scripts.

It begins with a long description of the iconography of चामुण्डा, who was the patron deity of the Mysore region as चामुण्डेश्वरी (At some point I will summarize all the deities whose icons he describes). Some are long while some are laconic. Some the less frequently encountered ones are:

[लोपामुद्रा]{style="font-weight:bold;"}:\
पाशाङ्कुशौ शरं चापं धारयन्तीं त्रिलोचनां | सूर्याभां chandra-चूडां cha लोपामुद्रां bhaje शिवां || सिन्धूर-वर्णः

[शीतला ]{style="font-weight:bold;"}(a terrifying female agent of The god, who is the deification of variola. As described here on the चण्डिका hill near my regions of youth I had noted an icon of the deity depicted riding a donkey, naked and bearing a pot of cooling drugs and a winnowing fan and black in color):\
ध्यायेच्च शीतलां देवीं रासभस्थां दिगम्बरां | मार्जनी-कलशोपेतां शूर्पालंक्र्^इत-मस्तकां || (कृष्णवर्णः).

[tulasI]{style="font-weight:bold;"} (It also provides the imagery for the demoness tulasI, who was conquered by विष्णु, there by allowing rudra to slay her husband, the demon शङ्कचूड. But here she is caled a देवी, worshiped as a deity of the basil plant):\
ध्यायेच्च तुलसीं देवीं श्यामां kamala-लोचनां | प्रसन्नां पद्मकह्लार-वराभय-chatur-भुजां ||1||\
किरीट-हार-केयूर-कुण्डलादि-विभूषितां | धवलां shuka-संयुक्तां padmAsana-निषेदुषीं ||2|| (श्याम वर्णः)

[तिरस्करिणी]{style="font-weight:bold;"} (A simple but beautiful image is given of our favor-bestowing goddess तिरस्करिणी, with whose hallowed mantra the stambhana prayoga of ari-s is destroyed.)[:]{style="font-weight:bold;"}\
नीलवाजि-समारूढा नीलाङ्गी खड्ग-धारिणी | nidra-निमीलद्-भुवनना-तिरस्करिणिकावतु || (नील-वर्णः)


