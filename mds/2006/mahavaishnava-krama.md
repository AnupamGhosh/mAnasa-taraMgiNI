
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [महावैष्णव krama](https://manasataramgini.wordpress.com/2006/08/05/mahavaishnava-krama/){rel="bookmark"} {#महवषणव-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/05/mahavaishnava-krama/ "6:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Both the avaykta and the नृसिंह-तापिनि explain that the yantra with the ugra विष्णु formula should be tied as an amulet. The नृसिंह-तापिनि explains the structure of the great yantra of विष्णु that simultaneously encompasses :1) the bIja of नृसिंह and the प्रणव. 2) the sudarshana chakra मूल mantra. 3) the नारायण अष्टाक्षरी 4) the vAsudeva द्वादशाक्षरी 5) the षोडश मातृक bIja-s 6) the ugra विष्णु formula.The formal worship of the yantra and विष्णु embodied in it may be done thus.

[ॐ गं गणपतये नमः ।]{style="color: #0000ff"}

First one does a सर्वाङ्ग न्यास thus:

 1.  head: OM 2) mouth: na 3) chest:mo 4) heart:nA 5) belly:rA 6) thighs:ya 7) knees: NA 8) feet: ya

Then one does करण्यास thus:

[ॐ न अङ्गुष्टाभ्यां नमः । मो भ तर्जनीभ्यां नमः । ग व मध्यमाभ्यां नमः । ते वा अनामिकाभ्यां नमः । सु दे कनिष्टिकाभ्यां नमः । वा य करतलकरपृष्ठाभ्यां नमः ॥]{style="color: #0000ff"}

Then mantra-न्यास:

[१) ॐ हृदयाय नमः २) विष् शिरसे स्वाहा ३) ण शिखायै वषट् ४) वे कवचाय हुं ५) न नेत्र-त्रयाय वौषट् ६) मः अस्त्राय फट् ॥]{style="color: #0000ff"}

Then he meditates on the fiery विष्णु as meditated upon by ahirbudhnya thus:

[अनादि निधनं देवं सर्व भूत भवोद्भवम् ।\
विद्युज्-जिह्वं महादंष्ट्रं स्फुरत्-केसर-मालिनम् ॥\
रत्नाङ्गदं समुकुटं हेम-केसर-भूषितम् ।\
कोणि-सूत्रेण महता काञ्चनेन विराजितम् ॥\
नीलोत्पल-दल-श्यामं रत्न-नूपुर-भूषितम् ।\
तेजस्-आक्रान्त-सकल-ब्रह्माण्डोदर-मण्डपम् ॥\
आवर्त-सदृशाकारैः संयुक्तं देह रोमभिः ।\
सर्व पुष्पैर्-योजिताञ्च धारयंश्-च महास्त्रजम् ॥]{style="color: #0000ff"}

He then worships विष्णु thus and offers flowers or water with the below formula:

[नमस्ते ।अस्तु जगन्नाथ नरसिंह वपुर्धर ।\
दैत्येश्वरेन्द्र संहारि नख-शुक्ति विराजित ।\
नख-मण्डल-सभिन्न हेम-पिङ्गल विग्रह ।\
नमो ऽस्तु पद्मनाभाय शोभनाय जगद्गुरो ।\
कल्पान्ताम्भोद-निर्घोष सूर्य-कोटि-समप्रभ ॥]{style="color: #0000ff"}

He then meditates on the chakra thus to destroy his sins and enemies:\
[वासुदेवस्य यछ्-छक्रं तस्य छक्रस्य ये त्वराः ।]{style="color: #0000ff"}\
[ते हि छिन्दन्तु पापान्-मे मम हिंसन्तु हिंसकान् ॥]{style="color: #0000ff"}

Then he installs the great vaiShNava yantra in front of him:

He then performs the invocation of the आवर्ण of विष्णु with flower or water offerings with the following incantations:

[ॐ ह्रौं इन्द्राय नमः\
ॐ ह्रौं छण्डाय नमः\
ॐ ह्रां अघोराय नमः\
ॐ वासुदेवासनाय नमः\
ॐ वासुदेव-मूर्तये नमः\
ॐ अं ॐ नमो भगवते वासुदेवाय नमः\
ॐ आं ॐ नमो भगवते सङ्कर्षणाय नमः\
ॐ अं ॐ नमो भगवते प्रद्युम्नाय नमः\
ॐ अः ॐ नमो भगवते अनिरुद्धाय नमः\
ॐ नारायणाय नमः\
ॐ तत्-सद्-ब्रह्मणे नमः\
ॐ ह्रां विष्णवे नमः\
ॐ क्ष्रौं नमो भगवते नरसिंहाय नमः\
ॐ भूः ॐ नमो भगवते वराहाय नमः\
ॐ कं टं पं शं वैनतेयाय नमः\
ॐ जं खं रं सुदर्शणाय नमः\
ॐ खं ठं फं षं गदायै नमः\
ॐ वं लं मं क्षं पाञ्चजन्याय नमः\
ॐ घं ढं भं हं श्रियै नमः\
ॐ गं डं वं सं पुष्ट्यै नमः\
ॐ धं षं वं सं वनमालायै नमः\
ॐ सं दं लं श्रीवत्साय नमः\
ॐ ठं छं भं यं कौस्तुभाय नमः\
ॐ गुरुभ्यो नमः\
ॐ इन्द्रादिदिक्पालेभ्यो नमः\
ॐ विष्वक्सेनाय नमः]{style="color: #0000ff"}

He first invokes विष्णु with the formula known as विष्णु-mAyA:

[अनाद्यन्त जगद्बीज  पद्मनाभ  नमो ।अस्तु ते  ।\
ॐ कालाय स्वाहा । ॐ कालपुरुषाय स्वाहा । ॐ कृष्णाय स्वाहा ।\
ॐ कृष्णरूपाय स्वाहा ।  ॐ चण्डाय स्वाहा । ॐ चण्डरूपाय स्वाहा  ।\
ॐ प्रछण्डाय स्वाहा । ॐ प्रछण्डरूपाय स्वाहा । ॐ सर्वाय स्वाहा ।\
ॐ सर्वरूपाय स्वाहा ।  ॐ नमो भुवनेशाय त्रिलोकधात्रे इह विटि सिविटि सिविटि स्वाहा ।  ॐ नमः अयोखेटये ये ये संज्ञापय ।]{style="color: #0000ff"}

[दैत्य-दानव-यक्ष-राक्षस-भूत-पिशाच-कूष्माण्डान्तापस्मार-कच्छर्धन-दुर्धराणाम्-एकाहिक-द्व्याहिक-त्र्याहिक-चातुर्थिक-मौहूर्तिक-दिन-ज्वर-रात्रि-ज्वर-सन्ध्या-ज्वर-सर्व-ज्वरादीनां-लूता-कीट-कण्टक-पूतना-भुजङ्ग-स्थावर-जङ्गम-विषादीनाम्-इदं शरीरं मम पथ्यं त्वं कुरु स्फुट स्फुट स्फुट प्रकोट लफट ॥]{style="color: #0000ff"}

[विकट-दंष्ट्र पूर्वतो रक्षतु । ॐ है है है है है । दिनकर-सहस्र-काल-समाहतो जय पश्चिमतो रक्ष । ॐ निवि निवि प्रदीप्त-ज्वलन-ज्वालाकार महाकपिल उत्तरतो रक्ष । ॐ विलि विलि मिलि मिलि गरुडि गरुडि । गौरी-गान्धारी-विष-मोह-विषम-विषं मोहयतु स्वाहा मां दक्षिणतो रक्ष । मां पश्य सर्व-भूत-भयोपद्रवेभ्यो रक्ष रक्ष ।  जय जय विजय तेन हीयते रिपु-त्रासाहङ्कृतवाद्यतो भयानुदभयतो ऽभयं दिशत्-अच्युतं  ।]{style="color: #0000ff"}

[तद्-उदरम्-अखिलं विशन्तु युग-परिवर्त-सहस्र-संख्येयो ऽस्तं-हंसम्-इव प्रविशन्ति रश्मयः  । वासुदेव-सङ्कर्षण-प्रद्युम्नाश्-चानिरुद्धकः  सर्व-ज्वरान्-मम-घ्नन्तु विष्णुर्-नारायणो हरिः । वासुदेव-सङ्कर्षण-प्रद्युम्नाश्-चानिरुद्धकः  सर्व-शत्रून्-मम-घ्नन्तु विष्णुर्-नारायणो हरिः ॥]{style="color: #0000ff"}

Then he invokes the trailokya-mohana विष्णु by meditating on the below incantation and offer a flower or water offering:\
[ॐ ह्रीं श्रीं क्लीं ह्रूं ॐ नमः । पुरुषोत्तम अप्रतिरूप लक्ष्मी-निवास जगत्-क्षोभण सर्व-स्त्री-हृदय-दारण त्रिभुवन-मदोन्मादनकर सुरासुरमनुज सुन्दरी जन मनांसि तापय तापय शोषय शोषय मारय मारय स्तम्भय स्तम्भय द्रावय द्रावय आकर्षय आकर्षय परम-सुभग सर्व-सौभाग्यकर सर्व-कामप्रद । हन हन चक्रेण गदया खड्गेन सर्व-बाणैर् भिन्धि भिन्धि पाशेन कुट्ट कुट्ट अङ्कुशेन ताडय ताडय तुरु तुरु किं तिष्ठसि तारय तारय यावत् समीहितं मे सिद्धं भवति ह्रीं ह्रूं फट् नमः ॥ॐ श्रीं श्रीः श्रीधराय त्रैलोक्यमोहनाय नमः ॥क्लीं पुरुषोत्तमाय त्रैलोक्यमोहनाय नमः ॥]{style="color: #0000ff"}

Then he invokes\
[ॐ विष्णवे त्रैलोक्यमोहनाय नमः ॥]{style="color: #0000ff"}

[ॐ श्रीं क्लीं त्रैलोक्यमोहनाय विष्णवे नमः ॥]{style="color: #0000ff"}

He then invokes the chakra and makes water offerings thus:

[ॐ छक्राय स्वाहा । ॐ विछक्राय स्वाहा । ॐ सुछक्राय स्वाहा । ॐ\
महाछक्राय स्वाहा ॥\
ॐ महाचक्राय असुरान्तकृत् हुं फट् ॐ हुं सहस्रार हुं फट् ॥]{style="color: #0000ff"}

He then meditates on the अष्टाक्षरी, द्वादशाक्षरी, शडक्षरी and finally the अनुष्टुभ्.

He then states the below formula and let the deva rest:\
[ॐ नमो भगवते महा-पुराषाय महा-भूतपतये सकल- सत्त्वभावि-व्रीडनिकर कमल-रेणूत्पल-निभ-धर्माख्य-विद्यया चरणारविन्दयुगल परमेष्ठिन् नमस्ते ॥]{style="color: #0000ff"}

