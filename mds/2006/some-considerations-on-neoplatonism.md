
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some considerations on Neoplatonism](https://manasataramgini.wordpress.com/2006/08/16/some-considerations-on-neoplatonism/){rel="bookmark"} {#some-considerations-on-neoplatonism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/16/some-considerations-on-neoplatonism/ "6:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I have always believed, since I was probably 11 years old, that it was important to study Neoplatonic thought and ritual. This is particularly important because it was one of the sister systems of our own, and was the only world philosophy with a reasonably comparable depth and scope. Importantly, as we are the only surviving heathens in the world with an unbroken link to our ancient traditions firstly of the shruti and then of the Agama-s, we alone, are in the unique position to study Neoplatonism from an insider's perspective, which is entirely lacking for the western students of Graeco-Roman thought. We need to look at history and philosophy NeoPlatonism from various angles: 1) How did it originate; how did the connections to ancient Greek and Hindu thought develop in Eurasia? 2) What does the end of Neoplatonism tell us about the dangers of Abrahamistic monotheism to modern Hindus? 3) What do the relationships between Neoplatonism and Hindu thought mean? 4) What is the common theme realized from Neoplatonism and Hindu thought in terms of their unifying role that transcended linguistic and regional differences?

We will touch upon a few of these now \[As a personal aside: While I began studying these issues since the beginning of my second decade, I must confess the process has been continuously on going- realization coming slowly, even as it does in the study of natural history and the making of life itself. At that point I had not yet gained a realization of the shruti itself. Then a couple of years later on the eve of a fierce battle, rather than practicing arms for the impending conflict, I turned to the सूक्त-s of हिरण्यस्तूप आङ्गिरस and gotamo राहुगण. All of a sudden the realization of the shruti started unfolding, the ज्ञान of the ancient अङ्गिर ऋषि-s was becoming clearer. Not all of this can be easily expressed or relayed to the uninitiated]

The first issue worth noting amongst the Neoplatonists is their convergence towards a concept similar to that of the shruti amongst the Hindus. The founder of the Neoplatonic synthesis Iamblichus provided an equivalent of the shruti ("the divine word") in the form of the Chaldaean Oracles, which were a series of verses composed in the Greek hexameter. The emperor Julian considered the entire Greek literature sacred, but he was by far the most inclusive of the Neoplatonists. The most typical position was to consider the divine word as comprising of the Chaldaean Oracles and the Timaeus of Plato. Typically, only those verses used by Iamblichus in his commentaries were used in discourse. Thus in a sense these verses chosen by Iamblichus became like the उपनिषद्-s chosen from within the shruti in the Hindu world. Thus even the yavana-s saw the need for a canon of texts that were considered a divine प्रमाण, just as the shruti in India. We will expand a little more and its historical implications a little later.


