
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The meditation and emanation of the पञ्चाक्षरी विद्या](https://manasataramgini.wordpress.com/2006/10/01/the-meditation-and-emanation-of-the-panchakshari-vidya/){rel="bookmark"} {#the-meditation-and-emanation-of-the-पञचकषर-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/01/the-meditation-and-emanation-of-the-panchakshari-vidya/ "5:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We described earlier how one installs the yantra of महादेव pertaining to the पञ्चाक्षरी विद्या and performs worship of it. Now we shall describe that meditation of and the mantra emanation from it.

  - As one starts the meditation of the पञ्चाक्षरी one sees the मण्डल of eight कला goddesses appearing in the eight directions:\
वामा- brilliant white complexion, (E); ज्येष्ठा -blood red, (SE); raudri- white again, (S); काली- bright yellow, (SW) ; कलविकरिणी- purple, (W); बलविकरिणी- shining gold, (NW); बलप्रमथनी-black, (N) सर्वभूतदमनी-light red, (NE). मनोन्मनी appear in the middle of the मण्डल with complexion of a bright blowing light. One then realizes मनोन्मनी within one self and the entire मण्डल of the कला goddesses surround you.

  - Then one sees the supreme अकुलवीर filling everything. He has the complex of a pure quartz crystal. He has 4 arms with हलायुध and त्रिशूल and the other two showing the abhaya and varada mudra-s. He has five heads with 3 eyes each and fills the chakras from the अनाहत to the मूलाधार including the minor लिङ्ग chakra (between मणिपूर and अनाहत) and महापद्म chakra(between स्वादिष्टान and मूलाधार). From each face emanates the पञ्चब्रह्म deity:\
tatpuruSha emanates with a single head and two arms in the east, with white complexion and 3 eyes holding a trident and skull.\
aghora in the south with black complexion, holding trident, bow, arrow, thunderbolt, mace, axe, खट्वाङ्ग and sword.\
सद्योजात in the west with yellow complexion, 4 hands and 4 heads, holding a bow, arrow and abhaya and varada.\
वामदेव in the north of light red complexion with 4 hands and 4 heads embracing and dallying with the 9 कला goddesses.\
ईशान at zenith, with white complexion 5 faces and 10 hands holding trident, bow, arrow, axe, thunderbolt, sword, खट्वाङ्ग, mace, skull, rosary

  - After realizing the पञ्चब्रह्म emanations he sees the following mantra emanate from the पञ्चाक्षरी:\
oM शिवः शिवाय नमः शिवः oM | ज्वालिन्यै स्वाहा | oM शिवात्मक महातेजः सर्वज्ञ प्रभुरावर्तय महाघोर पिङ्गल नमः | शिवाज्ञया हृदयं bandha bandha घूर्णय घूर्णय चूर्णय चूर्णय सूक्ष्म vajradhara वज्रपाश dhanur-वज्राशनि-vajra-sharIra mama शरीरम्-anupravishya सर्वदुष्टान् stambhaya stambhaya hUM ||


