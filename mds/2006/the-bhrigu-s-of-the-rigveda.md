
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The भृगु-s of the ऋग्वेद](https://manasataramgini.wordpress.com/2006/05/21/the-bhrigu-s-of-the-rigveda/){rel="bookmark"} {#the-भग-s-of-the-ऋगवद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/21/the-bhrigu-s-of-the-rigveda/ "2:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From the accounts of vedic tradition the भृगु-s were the foremost of the ऋषि-s. It appears that they were the founders of the fire ritual which is at the heart of the vedic religion. They are not authors of many mantra-s of the ऋग् veda, though their surrogate clan, the shaunahotra-s, have a मण्डल to themselves (RV2). Nevertheless, भृगु or the भृगु-s are mentioned 27 times by name in the RV. A single ancient member of the main-line भृगु clan उशना काव्य, the grandson भृगु, through his son kavi भार्गव, is mentioned by that name in 15 times in the RV. Another much latter main-line भृगु, jamadagni, is mentioned as contemporary by two of the great RV ऋषिस्, विश्वामित्र (who was an ally jamadgni) and वसिष्ठ. He is mentioned 9 times in the RV. Other भृगुस् are fleetingly mentioned by name, like अप्नवान (at least 2 times) and aurva, and, like उशना काव्य are clearly historical figures even for most of the RV composers.

भृगु/s are mentioned by name by most major clans of the RV from all three dvija वर्णस्:\
13 times by the other ancient brahminical clan the आङ्गिरस from whom most mainstream brahmins descended. 3 times by वसिष्ठ-s. 3 times by विश्वामित्र-s. 2 times by kShatriya hymn composers. 2 times the by ancient vaishya sages vatsapri and भालन्दन. 1 time by atri-s. 1 time by the shaunahotra-s 1 time by the later भृगु-s. भृगु may also be refered to by his generic name atharvan. The name atharvan in used 14 times in the RV. Seven (7) of these occurences are directly used in the sense of भृगु/s in connection to their ancient kindling of fire or ancient sacrifice. 4 occurences are in the sense of a contemporary atharvan priest performing a rite.

Many important themes emerge by examining the hymns mentioning भृगु-s in the RV:

 1.  A persistant motif which appears to be common knowledge amongst the vedic composers is that of मातरिश्वान् (वायु) bearing agni to भृगु-s.

 2.  agni is often discovered by the भृगु-s hidden cave or is brought to them from a hidden cave by मातरिश्वान्.

 3.  The भृगु-s are associated with the kindling of fire and the two births of agni, one of which appears to be within water. The fire within water is also a major theme with other भृगु sages like aurva. Several भृगु-s have fiery names: भृगु= cognate of fiery, phleugai etc; aurva- the fire in water; jamadagni- the feeder of fire.

 4.  The भृगु-s make a car for the gods.

The observation that most clans mention भृगु-s as the promulgator of the fire rite, suggests that they were ancient predecessors of the vedic rite. This is well-supported by the सुभेषज hymn and the सामिधेनी rite. However, this role might have even been performed by the later भृगु-s as the term 'गृणान' (गृणाति=teach) is used 5 times with jamadagni भार्गव, suggesting he proclaimed the fire rite to many even in his period.

RV 1.58.6 नोधा gautama\
[दधुष् ट्वा भृगवो मानुषेष्वा रयिं न चारुं सुहवं जनेभ्यः ।]{style="color:#99cc00;"}\
[होतारमग्ने अतिथिं वरेण्यं मित्रं न शेवं दिव्याय जन्मने ॥]{style="color:#99cc00;"}

The भृगुस् established you among the people, for the descendents of manu, like a beautiful treasure, pleasing to invoke; You, agni, as a messenger and and excellent guest, like an auspicious friend to the gods.

RV 1.60.1 नोधा gautama\
[वह्निं यशसं विदथस्य केतुं सुप्राव्यं दूतं सद्यो अर्थम् ।]{style="color:#99cc00;"}\
[द्विजन्मानं रयिमिव प्रशस्तं रातिं भरद् भृगवे मातरिश्वा ॥]{style="color:#99cc00;"}

मातरिश्वा brought, as a friend, to भृगु, the famed agni, the light of rituals, the protector , the swift messenger, with two births, (to be to him) as it were precious wealth.

RV 1.071.04 पराशर शाक्त्य (वसिष्ठ)\
[मथीद् यदीं विभृतो मातरिश्वा गृहे-गृहे श्येतो जेन्यो भूत् ।]{style="color:#99cc00;"}\
[आदीं राज्ञे न सहीयसे सचा सन्ना दूत्यं भृगवाणो विवाय ॥]{style="color:#99cc00;"}

When the all-pervading मातरिश्वा , incites agni, he becomes bright and manifest in every house; I, following \[the method of] भृगु, invoke agni to be messenger, as an ambassador to the emperor.

RV 1.120.05\
[प्र या घोषे भृगवाणे न शोभे यया वाचा यजति पज्रियो वाम् ।]{style="color:#99cc00;"}\
[प्र इषयुर् न विद्वान् ॥]{style="color:#99cc00;"}

The great is the chant that was uttered by घोषा, like that of भृगु, the chant with which the pajras worship you, who is like a mighty intelligent one.

RV 1.127.7 परुच्छेप दैवोदासि\
[द्विता यदीं कीस्तासो अभिद्यवो नमस्यन्त उपवोचन्त भृगवो मथ्नन्तो दासा भृगवः । अग्निरीशे वसूनां शुचिर्यो धर्णिरेषाम् ।]{style="color:#99cc00;"}\
[प्रियानपिधीन्र्वनिषीष्ट मेधिर आ वनिषीष्ट मेधिरह् ॥]{style="color:#99cc00;"}

भृगुस्, lauded him (agni) in both his forms, glorifying him, and saluting him, proclaim his praises; the भृगुस्, rubbing (the fire) for the oblation. The shining agni, who is the guardian of treasures, with power, the receiver of oblations, the wise one, may the wise one be pleased.

RV 1.143.04 (दीर्घतम औचाथ्य)\
[यमेरिरे भृगवो विश्ववेदसं नाभा पृथिव्या भुवनस्य मज्मना ।]{style="color:#99cc00;"}\
[अग्निं तं गीर्भिर् हिनुहि स्व आ दमे य एको वस्वो वरुणो न राजति ॥]{style="color:#99cc00;"}

Bring to his own abode with hymns that agni th all-knowning, whom the भृगुस् obtained from the navel of the earth; for, like वरुण, he reigns sole over wealth.

...\
RV 2.004.02 गृत्समद shaunahotra\
[इमं विधन्तो अपां सधस्थे द्वितादधुर् भृगवो विक्ष्वायोः ।]{style="color:#99cc00;"}\
[एष विश्वान्यभ्यस्तु भूमा देवानाम् अग्निर् अरतिर् जीराश्वः ॥]{style="color:#99cc00;"}

The भृगुस्, extracting agni, have twice made him manifest; (once) in the abode of the waters, and (once) among men; may that agni, with a fast horse, messenger of the gods, be the lord.

...\
RV 3.002.04 विश्वामित्र गाथिन\
[आ मन्द्रस्य सनिष्यन्तो वरेण्यं वृणीमहे अह्रयं वाजम् ऋग्मियम् ।]{style="color:#99cc00;"}\
[रातिं भृगूणाम् उशिजं कवि क्रतुम् अग्निं राजन्तं दिव्येन शोचिषा ॥]{style="color:#99cc00;"}

Desiring gain, we seek from the adorable, the benefactor excellent and vigorous; the wealth of भृगुस्, willing, of sagely deeds, agni shining forth with light that comes from heaven.

RV 3.005.10 विश्वामित्र गाथिन\
[उद् अस्तम्भीत् समिधा नाकम् ऋष्वोऽग्न्íर् भवन्न् उत्तमो रोcअनानां ।]{style="color:#99cc00;"}\
[यदी भृगुभ्यः परि मातरिश्वा गुहा सन्तं हव्यवाहं समीधे ॥]{style="color:#99cc00;"}

The mighty agni, the best of the heavenly lights, sustained the heaven with radiance, when मातरिश्व bore the bearer of oblations, (who was concealed) in a cave from the भृगुस्.\
...\
RV 4.7.1 वामदेव gautama\
[अयम् इह प्रथमो धायि धातृभिर् होता यजिष्ठो अध्वरेष्व् ईड्यः ।]{style="color:#99cc00;"}\
[यम् अप्नवानो भृगवो विरुरुचुर् वनेषु चित्रं विभ्वं विशे-विशे ॥]{style="color:#99cc00;"}

Here by ordainers was this god appointed the first hotar, and the adhvaryu who worships at the rites; whom apnavana, and the भृगुस् caused to shine bright and colored in the wood, spreading from home to home.

RV 4.7.4 वामदेव gautama\
[आशुं दूतं विवस्वतो विश्वा यश् चर्षणीर् अभि ।]{style="color:#99cc00;"}\
[आ जभ्रुः केतुम् आयवो भृगवाणं विशे-विशे ॥]{style="color:#99cc00;"}

विवस्वान्'s messenger, men have taken as their flag, swift; the ruler over all peoples, moving in each home as \[invoked] by भृगु.

RV 4.016.20 वामदेव gautama\
[एवेद् इन्द्राय वृषभाय वृष्णे ब्रह्माकर्म भृगवो न रथम् ।]{style="color:#99cc00;"}\
[नू चिद् यथा नः सख्या वियोषद् असन् न उग्रो ऽविता तनूपाः ॥]{style="color:#99cc00;"}

Hence we offer to the मिघ्त्यीन्द्र, the showerer, the acts of brahman and ritual, as the भृगुस् made the chariots, that he may never withdraw his friendship from us, and he the fierce one protect us.\
...\
RV 6.015.02 भरद्वाज बार्हस्पत्य\
[मित्रं न यं सुधितं भृगवो दधुर् वनस्पता वीड्यम् ऊर्ध्वशोचिषम् ।]{style="color:#99cc00;"}\
[स त्वं सुप्रीतो वीतहव्ये अद्भुत प्रशस्तिभिर् महयसे दिवे दिवे ॥]{style="color:#99cc00;"}

Like a friend to the भृगु-s, who established the benevolent agni, with upward flames in wood; be pleased with वीतहव्य, since you are magnified by his praise day after day.\
...\
RV 7.018.06 वसिष्ठ मैत्रावरुणि\
पुरोळा it turvasho यक्षुरासीद् राये मत्स्यासो निशिता अपीव\
श्रुष्टिं chakrur भृगवो druhyavashcha sakhA सखायमतरद् विषूचोः\
...\
RV 8.003.09 मेध्यातिथि काण्व\
tat त्वा यामि सुवीर्यं tad brahma पूर्वचित्तये\
येना yatibhyo भृगवे dhane hite yena प्रस्कण्वमाविथ

RV 8.003.16 मेध्यातिथि काण्व\
कण्वा iva भृगवः sUryA iva vishvamid धीतमानशुः\
इन्द्रं stomebhirmahayanta आयवः प्रियमेधासो asvaran

8.006.18\
ya indra यतयस्त्वा भृगवो ye cha तुष्टुवुः\
mamed ugra श्रुधी havam

RV 8.035.03 श्यावाश्व Atreya\
vishvair devais tribhir एकादशैर् इहाद्भिर् marudbhir भृगुभिः सचाभुवा\
सजोषसा उषसा सूर्येण cha सोमं pibatam अश्विना

RV 8.043.13 विरूप अङ्गिरस\
[उत त्वा भृगुवच्छुचे मनुष्वद् अग्न आहुत ।]{style="color:#99cc00;"}\
[अङ्गिरस्वद् धवामहे ॥]{style="color:#99cc00;"}\
agni one, to whom oblations are offered as by भृगु and by manu; we invoke you as the अङ्गिरसस्.

RV 8.102.04 prayoga भार्गव\
[और्वभृगुवच्छुचिमप्नवानवदा हुवे ।]{style="color:#99cc00;"}\
[अग्निं समुद्रवाससम् ॥]{style="color:#99cc00;"}

Like aurva, भृगु and like अपन्वान, I invoke the pure Agni, dwelling in the midst of the sea.\
...\
RV 9.101.13 prajApati वैश्वामित्र\
[प्र सुन्वानस्यान्धसो मर्तो न वृत तद् वचः ।]{style="color:#99cc00;"}\
[अप श्वानम् अराधसं हता मखं न भृगवः ॥]{style="color:#99cc00;"}

Let no mortal hear the sound of the effused Soma; away with the dog that offers no rituals, as the भृगु-s drove off makha (also seen the mantras of the भृगु sage इटन्त् भार्गव).

...\
RV10.014.06 ?\
अङ्गिरसो नः pitaro नवग्वा अथर्वाणो भृगवः सोम्यासः\
तेषां वयं sumatau यज्ञियानामपि bhadre सौमनसेस्याम

RV 10.039.14 घोष काक्षित्वती\
एतं वां स्तोममश्विनावकर्मातक्षाम भृगवो na ratham\
न्यमृक्षाम योषणां na marye नित्यं na सूनुन्तनयं दधानाः

RV 10.046.02 वत्सप्री भालन्दन\
imaM vidhanto अपां sadhasthe पशुं na नष्टं padairanu gman\
guhA chatantamushijo नमोभिरिछन्तो धीराभृगवोअविन्दन्

RV 10.046.09 वत्सप्री भालन्दन\
द्यावा यमग्निं पृथिवी जनिष्टामापस्त्वष्टा भृगवो yaM सहोभिः\
ईळेन्यं प्रथमं मातरिश्वा देवास्ततक्षुर्मनवे yajatram

RV 10.092.10 शार्यात मानव\
te hi प्रजाया abharanta vi shravo बृहस्पतिर्वृषभःसोमजामयः\
यज्ञैरथर्वा prathamo vi धारयद् देवादक्षैर्भृगवः saM chikitrire

RV 10.122.05 चित्रमहा वासिष्ठ\
त्वं दूतः prathamo वरेण्यः sa हूयमानो अमृतायमत्स्व\
त्वां marjayan maruto दाशुषो गृहे त्वां stomebhir भृगवो vi रुरुचुः


