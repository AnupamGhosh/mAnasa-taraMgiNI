
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कौमार विद्या](https://manasataramgini.wordpress.com/2006/01/16/the-kaumara-vidya/){rel="bookmark"} {#the-कमर-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/16/the-kaumara-vidya/ "7:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having paid respect with to rAja bhojadeva, who stands in the line of the teachers from षष्ठिनाथ we salute the god who is in the embrace of सूर्यसुनुदुहितृ.

The cryptic mantra goes thus:\
वाक् (holy speech) is the varman (armour)\
the कर्णबिन्द्वाढ्य (ear ornaments) go with the charama (last one)\
The Fish banner flutters (मीनकेतन)

salutations to कुमार.

We also worship that awful being, riding a horse or a violent elephant, embracing पूर्णा and पुष्कला along with his hosts like गोप्तृ


