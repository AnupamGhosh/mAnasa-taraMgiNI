
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [When chimps and men stopped mating](https://manasataramgini.wordpress.com/2006/07/03/when-chimps-and-men-stopped-mating/){rel="bookmark"} {#when-chimps-and-men-stopped-mating .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 3, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/03/when-chimps-and-men-stopped-mating/ "4:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Chimps and humans are so close that I had wondered since my school days when the ancestors of chimp and humans had stopped mating with each other. There were several rumors of human-chimp hybrids but they were just rumors. There was the long-suffering chimp named Oliver that had many behavioral peculiarities that apparently made him do many human-like acts and walk bipedally routinely. Then there were the reports of the giant chimpanzees from DR Congo which had large crests on their head and howled during full moons. These chimps suggested that there are still extant, in the dwindling modern Chimp population apes with a diversity of traits including those resembling humans.

Recently, Patterson and colleagues produced a remarkable data collection of sequence on chimps, humans, gorillas, orangs and macaques which throws light on the speciation of chimps and humans. What their data suggests is that human and chimp divergence across the genome varies between 84% to 147% of the average. Given that species divergence will be equal to or less than minimal genome divergence once corrected for multiple mutations, it becomes clear that the chimps and humans diverged over a long period of time, with some regions separating much earlier than others. Based on this they proposed that human chimp species divergence is definitely younger than 6.3 million years and most likely younger. Further they show that the X chromosome is much closer between chimps and humans than autosomes and is close to the genome minimum for divergence for most of its length. This is even after accounting for the fact the X to autosome effective population is 0.75 (XX females + XY males=3/4 X).\
This raises two issues that Patterson et al appear to solve with their data: 1) The low divergence time of chimp and human is at loggerheads with the fossil evidence suggesting that apes of the human line like [Sahelanthropus, Ardipithecus ]{style="font-style: italic"}and [Orrorin ]{style="font-style: italic"}(especially the former) are likely to have been 6.5-7.1 million yr range. This is clearly older than the maximum upper limit one can stretch the human chimp divergence under molecular clock assumptions and calibration with [Sivapithecus]{style="font-style: italic"}. 2) The X chromosomes alone of chimps and humans are much closer than expected, that too throughout their length, but not that of humans and gorillas or chimps and gorillas. So, there is something bringing their X-chromosomes much closer in time rather than a general slowdown of natural selection.

The solution appears to lie in the basic possibility that after the initial divergence human and chimpanzee ancestors started mating again and exchanging genes. Importantly Haldane had long ago proposed a rule: "If in hybrids of two species one sex is rare, missing or sterile then it is the heterogametic sex." Thus, after the initial chimp human separation it is likely that they started mating again but the Haldane rule kicked in and the males of such crosses were probably defective or sterile. The females in contrast would have remained fertile and the only way they could propagate the genes was by mating with males within the previously diverged chimp or human lines. The products of such matings will necessarily contain the contemporary shared X chromosome there by allowing much younger versions of it compared to other regions on autosomes that might have diverged earlier. Thus, all in all we may say that human and chimp line ancestral populations were mating down to between 4-5 million yrs BP at least.

Given the diversity of chimps we get a glimpse of today and the known diversity of humans it is quite possible that there were diverse sub-races, with differing behavioral and anatomical traits, with some overlap in terms of gene exchange between the human and chimp lines until relatively recently. Then Sahelanthropus and other apes might represent the early divergent phase after which renewed mating mixed the human and chimp lineages again.


