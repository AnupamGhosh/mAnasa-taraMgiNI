
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The master of विलिस्तेङ्गा](https://manasataramgini.wordpress.com/2006/12/12/the-master-of-vilistenga/){rel="bookmark"} {#the-master-of-वलसतङग .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/12/the-master-of-vilistenga/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One who is in the grip of the विद्या-s of the dreaded धूमावती and rakta-ज्येष्ठा will be released from them by the secret formula known as the "bonds of the देवी". The one who goes beyond that step attains that ancient and most secret विद्या known as the prayoga of विलिस्तेङ्गा. It was the secret of the atharvans of, that which arises from the herb which was known to भृगु and उशना काव्य. It is considered the crest jewel of the mighty विद्या-s of उशान काव्य himself -- indeed it is called the AsurI mAyA. Only the ब्राह्मण of high caliber who masters the deep rahasyas of the ritual practice can set himself on the path of becoming the master of the विलिस्तेङ्गा prayoga. He having changed his यज्ञोपवीत and performed his संध्य as per the rules of the atharvan lore moves in the southern direction. He performs the appropriate rites of the पैप्पलाद lore to the lord of the gods, with the सूक्तं that a knower of rahasya-s will attain identity to with indra. He is filled with the mAyA of indra. He then invokes निरृति into himself and if he has mastered the rahasya mantras he sends her away into a mule or nematode worm. He is then ready to invoke the विद्या of विलिस्तेङ्गा. Even as he makes the oblations to her he experiences bliss, like a long-lasting and unfading pleasure of maithuna with a most beautiful स्त्री. The very sight of विलिस्तेङ्गा provides him indescribable bliss that no one can experience in the regular world. Once विलिस्तेङ्गा joins him he is the master of prayogas. He becomes like a वृषभ filled with the virility of ten males and attracts beautiful women easily. He can cause instant stambhana of rivals and send deep bhaya into enemies.


