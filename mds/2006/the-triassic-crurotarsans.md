
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Triassic crurotarsans](https://manasataramgini.wordpress.com/2006/01/30/the-triassic-crurotarsans/){rel="bookmark"} {#the-triassic-crurotarsans .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 30, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/30/the-triassic-crurotarsans/ "6:12 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A phytosaur and the shuvosaurid [Effigia.]{style="font-style:italic;"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/effigia.0.jpg){width="75%"}
```{=latex}
\end{center}
```



The archosaurs of the crurotarsan clade (the crocodile line) had their heydays in the Triassic, taking up the dominance of the terrestrial and semi-aquatic ecosystems from the synapsid promammals. Chatterjee had claimed in the 1980s that the Triassic crurotarsan [Postosuchus]{style="font-style:italic;"}, was a precursor of [Tyrannosaurus]{style="font-style:italic;"}, the late Cretaceous coelurosaurian dinosaur. Likewise Chatterjee also claimed that other coelurosaurian clade, the ornithomimosaurs had an origin in the Triassic, after he discovered the toothless archosaur [Shuvosaurus]{style="font-style:italic;"}, which indeed looked like an ornithomimosaur. Examining his remains it became clear to me that the archosaurian forms represent convergences to a similar morphospace position amongst the crurotarsan rather than early dinosaur precursors. Thus, the crurotarsan [Postosuchus ]{style="font-style:italic;"}appears to have converged to a theropod carnivore-like condition and [Shuvosaurus ]{style="font-style:italic;"}to that of an ornithomimosaur, several million years prior to the dinosaurian equivalents.

The recent discoveries by Nesbitt point to other such convergences: [Effigia]{style="font-style:italic;"}, is another Shuvosaurid that is remarkably like an ornithomimosaur or an elaphrosaur in several ways-it clearly occupied an ecological niche close to them. The presence of other fragmentary versions such as [Sillosuchus ]{style="font-style:italic;"}suggest that the Shuvosaurids might have been a very widespread aspect of the ecosystem. The highly armored Aetosaurs evolved a state similar to the armored dinosaurs of the thryeophoran clade- the ankylosaurs and stegosaurs. Recently, the recovery a reasonable skeleton of [Revueltosaurus]{style="font-style:italic;"}, revealed that it was not an ornithischian dinosaur but a crurotarsan probably close to the Aetosaurs. Thus, the Revueltosaurs, seem to have anticipated the small ornithischians prior to their meteoric rise in the Jurassic. The mega-predatory crurotarsans- [Prestosuchus, Rauisuchus, Saurosuchus, Postosuchus, Tikisuchus and Ticinosuchus]{style="font-style:italic;"} repeatedly approximated the theropod condition. The ornithosuchids approximated the smaller coelurosaurs and probably even acquired a facultatively bipedal gait. Likewise the early crocodylomorphs also appear to have occupied a niche similar to the smaller dinosaurian predators. The forms like [Arizonasaurus ]{style="font-style:italic;"}and the Ctenosauriscids appear to have converged to the state similar to [Dimetrodon ]{style="font-style:italic;"}and related pelycosaurs, with giant sail-backs one one end and the spinosaurs amongst the dinosaurs. The phytosaurs appear to have occupied the later crocodilian morphospace as semi-aquatic predators.

P. Olsen and colleagues have studied the geology of the Triassic-Jurassic boundary in great detail and noticed the following: 1) There is an iridium spike in the boundary. 2) There is marked spike in the fern spores. 3) There is a rise in the number of theropod trackways.

This coincided with: 1) a complete extinction of the dominant Triassic crurotarsans except the crocodylomorphs 2) the rise in number of dinosaur genera and 3) enormous increase in size of the dinosaurs. This may mean that the dominance of the dinosaurs was ushered by a celestial impact event that knocked out most of the crurotarsans and made way for the rise of the dinosaurs. Clearly the basic archosaurian body plan appears to have been a constraint (la Gould's spandrel) that caused dinosaurs to iteratively fill in eco-morpho-space zones of the crurotarsan precursors. The surviving crurotarsans, the crocodiles appear to have largely taken up the eco-morpho-space released from the phytosaurs by their extinction. Though the notosuchid clade appears to have undergone considerable morphological diversification towards the end of the Jurassic. It seems the crocodiles were always survivor-opportunists, that took up all kinds of niches when ever a chance presented itself.

In light of this one wonders if at least part of Protoavis of Chatterjee may represent a crurotarsan imitation of the avian state?? Far fetched but not impossible.


