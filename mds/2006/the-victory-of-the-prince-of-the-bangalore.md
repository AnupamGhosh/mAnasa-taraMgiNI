
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The victory of the prince of the Bangalore](https://manasataramgini.wordpress.com/2006/07/10/the-victory-of-the-prince-of-the-bangalore/){rel="bookmark"} {#the-victory-of-the-prince-of-the-bangalore .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 10, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/10/the-victory-of-the-prince-of-the-bangalore/ "2:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

If am writing this it really means I am wasting time. Since I have done a lot of that over the past two days and feel guilty about I am just adding x to infinity --- something a person with my burden of responsibilities really should not be doing. I have met very few great men in my life. Others I have heard of and have attained that eko-मानुष आनन्दः. But one great man I met was before he attained eko-मानुष आनन्दः. Our clansman Mn introduced him to us -- he was slightly older than us but still in the same "strength" bracket and was an extraordinary whacker of the red cherry. In our brief encounters he demonstrated that he was destined for great deeds on the field- we felt like gauche brahmin boys competing with arjuna in the use of the bow. Great bullies whom we feared he trashed with least exertion. Even then he stated that he felt that his true mettle would be tested only against the cannonade of great Black bombardiers like Ambrose and Walsh.

Many years later and much history in between he rose to be the prince of Bangalore, and along with the prince of Vangipura led the Hindu armies to many glorious victories, especially against the peerless pirates of Southern Saxony led by emperor Slit-eyes. But the islands of the Black Emperors remained a stumbling post past which the Hindus had never been since the days before we or the prince of Bangalore was in this world. But now he had achieved despite defeats in the initial skirmishes. In that last battle, on a field where mines went off without warning, he alone lead his horses through the "Canonengebruell (to borrow from Heinrich Heine)". Sugriva and the Mohammedan mercenery fell early to the mines. After that even the prince of Vangipura and other sepoys fell easily to the steady bombardment of the Black artillery assisted by their Hindu mercenaries. The field brought fleeting memories of the quartets of great black gunners like Holding, Marshall and others tearing the Hindu armies to shreds in the battles of 1983 (some of our earliest memories).

But amidst all this carnage the prince of B'lore kept sniping getting down his quarry, aided only by the flimsiest of gunners, his old rifleman also from B'lore. Aided by the Sikh sapper he then blew up the defenses of the last of the black emperors sallying forth from his great Zimbabwe. In the second encounter on the same field the going was even worse but there the prince of B'lore alone stood, like रामचन्द्र slaughtering the रक्षस् in जनस्थान or like भीमसेन before अश्वत्तामन् on the morn of 15th day. Finally, the Hindus almost saw defeat, as mercenaries of their own kind on the side of the black emperor, retaliated on a large scale. But the old trusted field-gun of B'lore fired rather than tumbling or fumbling and conferred the most coveted victory on the prince of B'lore. Thus, the saffron flag flew on the islands of the Atlantic after a long long time.


