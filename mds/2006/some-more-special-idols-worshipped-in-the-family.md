
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some more special idols worshipped in the family](https://manasataramgini.wordpress.com/2006/03/12/some-more-special-idols-worshipped-in-the-family/){rel="bookmark"} {#some-more-special-idols-worshipped-in-the-family .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/12/some-more-special-idols-worshipped-in-the-family/ "7:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/chakraनारायण_frame.jpg){width="75%"}

[](http://photos1.blogger.com/blogger/2010/410/1600/chakraनारायण_frame.jpg)

chakra-नारायण: The icon of विष्णु combined with the sudarshana wheel. He is offered the combined तर्पनं of the उग्रविष्णु and the bahuchakra mantras.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/natarAja_frame.jpg){width="75%"}
```{=latex}
\end{center}
```



This a replica of the idol housed in the Madhurai shrine, the city with which one of the lineages of mine is associated. Previously they were kept concealed except on the days of the special offerings.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/kumara_gaNanAthAmba_frame.jpg){width="75%"}
```{=latex}
\end{center}
```



The mother of heroes and the mother of gods. In the vedas she is पृश्णि, the cow of the bull of the heavens, rudra. The geneatrix of the two gods कुमार and गणनाथ-- the lords of the ghostly hordes


