
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [saH जनासः इन्द्रः](https://manasataramgini.wordpress.com/2006/02/24/sah-janasah-indrah/){rel="bookmark"} {#sah-जनस-इनदर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 24, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/24/sah-janasah-indrah/ "6:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[य्áह् स्ह्áस्ह्वतो म्áह्य् éनो द्áधनन् áमन्यमनञ् छ्áर्व जघन ।]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[य्áः श्áर्धते नानुद्áदाति शृध्यां य्ó द्áस्योर् हन्ता स्á जनास íन्द्रः ॥]{style="font-weight:bold;font-style:italic;color:#99cc00;"}

He who struck the evildoers, before even they realized, by shooting his arrow;\
He who forgives not the boldness of those who provoke him, who kills the dasyu, he oh men is indra!

R brought to my attention the translation by Oldenburg, a grand old hero of the Indologists, of the hymn 4.19.7 by वामदेव gautama.\
\
[प्राग्र्úवो नभन्व्ò न्á व्áक्वा ध्वस्रा अपिन्वद् युवतीर् ऋतज्ञाः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[ध्áन्वान्य् áज्रां+ अपृणक् तृषाणां+ áधोग् íन्द्र स्तर्य्ò द्áंसुपत्नीः]{style="font-weight:bold;font-style:italic;color:#99cc00;"}

The Indologist, in his wretched flight of fantasy, and some other German scholars like him, such as Geldner thought that the greatest of the gods was supposed to be ravishing virgins according to this hymn. In fact, the British translator Griffith, who was much maligned recently by a philologist successor of Oldenburg, came closer to the truth.

The meaning of the mantra can be more correctly stated thus:\
He made virgins' breasts swell like bubbling springs flowing forth, \[of] the girls who know the ऋत; his rain bow slaked the thirst of the fields, \[so] indra has made the sterile ladies of the household to give milk.

This mantra as is clear from above is a spell for fertility of girls and women.

Some may have in mind the अहल्या-indra incident. However, that tale itself arose out of the thorough misunderstanding the ancient rites of the great god, where indra is called to drink soma using the सुब्रह्मण्य chant. This chant is highly enigmatic and has bhaktis that have never been understood outside the vedic tradition. The chief bhaktis of the सुब्रह्मण्य chant are:\
[सुब्रह्मण्यो३ं । सुब्रह्मण्यो३ं । सुब्रह्मण्यो३ं ॥]{style="color:#99cc00;"}\
[ॐ सुब्रह्मण्य]{style="color:#99cc00;"}\
[इन्द्रागच्छ । हरिव आगच्छ । हरिभ्यां आयाही । मेधातिथेर् मेष । वृषणश्वस्य मेन । गौरावस्कन्दिन् । *अहल्यायै जार* । कौशिक ब्राह्मण । कौशिक ब्रुवाण [गौतम ब्रुवाण] । देवा ब्रह्माण आगच्छातागच्छ ।]{style="color:#99cc00;"}

ahala means the unplowed, from which derives अहल्या, either an unplowed field or a virgin woman \[It may be recalled that in the sexual formulae of the ashvamedha the term "ahala" is used at the lady participating in the banter]. The gautama ब्रुवाण means the one who spoke to gautama and is an unconnected bhakti to the अहल्यायै jAra. Further, given that अहल्या if not the unplowed land, is a virgin woman, she cannot be gautama's wife. The जैमिनीय ब्राह्मण laconically states that this अहल्या is the female sage मैत्रेयी. So like the other bhaktis of the सुब्रह्मण्य litany, it might refer to an obscure tale where indra aided अहल्या maitreyi.


