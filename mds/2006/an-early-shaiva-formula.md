
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An early shaiva formula](https://manasataramgini.wordpress.com/2006/05/12/an-early-shaiva-formula/){rel="bookmark"} {#an-early-shaiva-formula .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/12/an-early-shaiva-formula/ "5:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Very little is preserved of the ancestral tantric shaiva lineage prior to its split into the five fold path. An early formula of the lineage that survives is seen in the garuDa tantra termed hara tantram, which itself does not survive as a full text as far as I know.

The distinctive core used to make the oblation for rudra in the fire is thus:

Amantrito .asi devesha गणैः सार्धं maheshvara |\
प्रातस्त्वां पूजयिष्यामि atra sannihito bhava |\
(Invitation to rudra with his agents to the rite)

oM hAM hIM हौं शिवाय नमः |

(the five-face oblations from सद्योजात to ईशान)\
oM ह्रां सद्योजातय स्वाहा |\
oM ह्रीं वामदेवाय स्वाहा |\
oM ह्रूं अघोराय स्वाहा |\
oM ह्रैं तत्पुरुषाय स्वाहा |\
oM ह्रौं ईशानाय स्वाहा |

oM hAM आत्मतत्त्वाय स्वाहा |\
oM hIM विद्यातत्त्वाय स्वाहा |\
oM hUM शिवतत्त्वाय स्वाहा |\
oM hAM स्वाहा |\
oM sarvebhyo devebhyo वौषट् |

पूरयपूरय मखव्रतं तन्नियमेश्वराय sarva-तत्त्वात्मकाय sarva-कारण-पालिताय oM hAM hIM hUM हैं हौं शिवाय नमः ||

The use of the 5-fold face formulae suggest the basic link to the vedic proto-shaiva stock.


