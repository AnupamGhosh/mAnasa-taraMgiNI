
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Activist Indologist](https://manasataramgini.wordpress.com/2006/01/31/the-activist-indologist/){rel="bookmark"} {#the-activist-indologist .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 31, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/31/the-activist-indologist/ "6:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Edgar Hoover had made leftism a matter of the past in American politics. Hunted mercilessly by Hoover and McCarthy they leftists found refuge in American academia from where they mainly concentrated on social issues taking the side of the Islamists, and cudgels against sociobiologists. Harvard University is home to several such academics, who wistfully hope that their utopian egalitarian society will be enforced. [Currently, the most leftist a party can get is the Democractic party. It is via this party they will reappear. ]{style="display:inline !important;float:none;background-color:#ffffff;color:#2c3338;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:1.2em;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}However, there was another aspect to their subterranean lives in academia. In my early days in this environment I remarked to a couple of friends (They were मैत्रवरुणि and kaushika :) ) that these leftist academics have a secret political role. They foster the American rapproachment of China and are a great subversionary force deployed against India. The leftist US academics have their helpers throughout the Leukosphere as well as yellow journalists of Indian rags.\
For example take a विट्-mukha like Khushwant Singh: "[I am not as much bothered by Pakistani Hindu haters as I am by Indian Muslim haters.]{style="font-style:italic;color:#ff0000;"}"

Or take an Italian Archeologist Maurizio Tosi, who belongs to this network of leftist Hindu haters, writes in his flawed English : ["... the LAW is our god, the LAW made by the humans for the humans. I know no god, I have no soul, I am food for the worms or.....the fishes if I die in India.....Since on January 1st 1643 the British Parliament executed for treachery the king Charles 1st Europe has made a choice/ NOBODY, least of all a dream of abstraction for fear of death like god, allah and the like, ought to be above our LAW. It might be a wrong law, but it is real. So if you choose to send your chjildren to our schools, to train them in a competitive environment of science....DO NOT bother us with the crap of your soulms and fears. Wer hjave enough of fanatics who exploit our liberality; Your freedom ends where mine begins....."\
]{style="font-style:italic;color:#000099;"}\
What is Tosi trying to say? If we cut the crap, we see that he is mouthing old Leukosupremacist fantasies in a diaphanous garb. Charles Murray, an American far-rightist raconteur, recently produced a voluminous tome "Human Accomplishment" which was nectar to the Leukospheric leaders. Its basic thesis was that European leukotestate gents were the epitome of human achievement in the arts and the sciences. Juxtapose this with a now famous statement of a garrulous Harvard philologist of German origin: "India is a [cul de sac]{style="font-style:italic;"}". Here is where one sees a remarkable convergence between the right and the left streams of the monotheistic Leukosphere: both hold a monotheistic view at heart- thus giving them an excellent chance to cooperate on common enemies. Hindus to them are a problem: They look different, they seem to have a civilization of their own, they have a large army, which even evicted Portugal with impunity, they even manage democracy that the Leukotestate supremacists pride themselves about, and even more galling is the fact that Hindus still preserve a very archaic form of Indo-European culture that the Leukotesta have lost. "This ought not to be true" they tell themselves. Here the leaders of the Leukosphere see a good opportunity to redirect their artsy leftists: cat leftist > anti.india. Thus, is born the activist Indologist, who is a saboteur for the leukospheric leaders in the crusade against pagan India.

"A-servin' of 'Er Majesty the Queen,\
Of all them [blackfaced crew]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
The finest man I knew\
Was our regimental bhisti, [Gunga Din]{style="font-weight:bold;font-style:italic;color:#cc33cc;"}."

Thus, another Anglical troubadour regally patronized his ever-faithful coolie. And sure enough like the legendary Gunga Din we have the supine drudges of the Indologist. I once received a high-sounding letter from a co-ethnic who asked us to indicate on a web site that Vedic Sanskrit was certainly younger than Hittite. I politely told him that this was an Indological fantasy, and while the Hittite lineage was definitely the earliest branching Indo-European, it did not make Hittite attested well after 2000 BC older than the ancestral ऋग्वेद. The Gunga Din of course declared that the great Indologist had told him so and he was on a mission to correct the rightist fantasies of Hindus. This leaves little doubt that we need to put such Indologists out of business for good.


