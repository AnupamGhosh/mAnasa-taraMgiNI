
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Theories for the bhArata conundrum](https://manasataramgini.wordpress.com/2006/05/05/theories-for-the-bharata-conundrum/){rel="bookmark"} {#theories-for-the-bharata-conundrum .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/05/theories-for-the-bharata-conundrum/ "5:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We earlier described the major problem termed the bhArata conundrum, where we notice a silence regarding the पाण्डवस् alone in the vedic textual layers, but they are prominently mentioned in all the later texts of Sanskrit and prakrit literature. The following plausible theories may be cited in this regard:

 1.  The vedic authors were of the kaurava party and hence purposely omitted references to the पाण्डुस्. The main problem with this theory is why do they mention the descendents of the पाण्डव-s as per the great epic in very positive light. In fact they do refer to दृतराष्ट्र वैचित्रवीर्य in negative light, just as the surviving version of the epic. They also refer to the पञ्चलस् like शिखण्डिन् in positive light.


 2.  Generational difference theory: The people with the same names as the महभारत characters are in fact people of an earlier generation, coincidentally having same names as the epic characters. While there is some evidence for this from the पौराणिc genealogies, the patronymics typically only match the characters of the great epic.


 3.  Clearly in linguistic as well as historical terms the consolidation of the vedic literature preceded the consolidation of the oral epic. So is it indeed possible that the पाण्डवस् were new comers superimposed on to the older vedic history with the composition of the bhArata epic? This theory implies that in the original sequence of events, the characters of the महाभारत, who find mention in the vedic literature, indeed comprise a real historical sequence as narrated earlier, but the पाण्डवस् were completely absent in it. The पाण्डवस् or their real descendents came later and captured power as a result of the bhArata war, and then superimposed themselves on the existing Indo-Aryan genealogies and historical epics by creating a new epic the महाभारत. What are the suggestions supporting this unusual contention beyond the silence of the vedic texts regarding the पाण्डवस्: (i) The पाण्डवस् are a younger branch of the kuru clan in the epic and are not actually पाण्डु's biological sons but claim divine origin from the principal vedic gods. (ii) The पाण्डवस् are associated with खाण्डव-prastha a fringe zone of the kuru realm as per the late vedic descriptions of the ब्राह्मणस्. (iii) Most traditional Indo-Aryan rulers as well as brahmin orthodoxy of the अञ्गिरसस् (कृप and droNa) stand by the kauravas in the war, whereas the major पाण्डव alliances are all through marriage.

Thus, it is possible in this theory that the पाण्डवस् were never a part of the vedic kuru-पाञ्चाल realm nor were they progenitors of its prominent kings like परीक्षित and janamejaya. They were actually outsiders, who (or their descendent) became active just [after ]{style="font-weight:bold;font-style:italic;"}the core vedic period was over. They then seized power through marriage alliances with prominent Indo-Aryan dynasties and ensuing the great bhArata war to oust the original kurus. Thus, they established their own dynasty in place of the original kurus. Then they tried to establish their legitimacy by many means. Firstly, they created an epic where they placed their founders, the original पाण्डवस्, as ancestors to the last of the pre-eminent kuru rulers known at that time who had great prestige in the vedic circles. Secondly, they obscured their own origins by using the device of divine origin and placing their ancestor पाण्डु within the kuru dynasty as a younger branch. Thirdly, they incorporated old astronomical observations to predate the bhArata war back to the hoary vedic period.

What remains unclear is whether they were really a younger branch of भारतस् or para-vedic outsiders. The latter is indeed very possible given the issue of the divine origins of the actual पाण्डव-s and the purely symbolic number 3+2, with two different mothers. That motif also occurs in the foundational myth of the Indo-Aryan पञ्च jana (3= anu, druhyu, pUru through शर्मिष्ठा and turvasha, yadu through देवयानी). This motif occurs outside India in the foundational myth of the Chingisid mongols (3+2 with different fathers but a common mother in this case, with the divine father being the wolf manifestation of the deity Koko Tengri himself). The main problem with this overall theory is that इतिहासस्, and the bhArata itself is mentioned in the vedic literature in itself albeit somewhat late. The only way out is that the पाण्डवस् inserted themselves into a pre-existing bhArata epic itself, while reducing the exploits of janamejaya and परीक्षित to a sideshow within it. If all this were true, perhaps we must look into the rise of [historical ]{style="font-weight:bold;font-style:italic;"}पण्डव-s for the cultural changes in the Indo-Aryan realm in post-vedic India.

An allied issue is the origin of the god सुब्रह्मण्य in the Indic pantheon (a historical analysis for a later day).


