
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Simplified होमा-विधानं](https://manasataramgini.wordpress.com/2006/08/06/simplified-homa-vidhanam/){rel="bookmark"} {#simplified-हम-वधन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/06/simplified-homa-vidhanam/ "5:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

oM gaM गणपतये नमः X 4 तर्पणं

He first kindles the fire on the स्थण्डिल with formula:

क्रव्यादम् अग्निं pra हिणोमि दूरं यमराज्ञो गछतु रिप्रवाहः |\
इहैवायम् itaro जातवेदा devebhyo हव्यं vahatu प्रजानन् || (RV 10.16.9)

He then sprinkles water around the fire with the formula:

OM पावक वैश्वानर इदमासनं अरणीगर्भ संस्कृत ||

Then his consciousness expands (chit) flows out through his forehead and enters the fire and as this happens he utters the formula:

OM तेजोरूप महाब्रह्मन् मुहूर्तास्-त्रिषु वैश्वानरं प्रतिबोधयामि ||

Then he endows the fire with "consciousness" with the formula:

OM वैश्वानरो na Utaya A pra यातु परावतः | agnir नः सुष्टुतीर् [स्वद्युतीर्] upa ||\
पृष्टो divi पृष्टो अग्निः पृथिव्यां पृष्टो विश्वा ओषधीरा vivesha |\
वैश्वानरः सहसा पृष्टो अग्निः sa no divA sa रिषः pAtu naktam || [ AV-S 6.35.1+RV 1.98.2]

Then he offers the following oblations to the deities:\
oM प्रजापतये स्वाहा | oM सोमाय स्वाहा | oM बृहस्पतये स्वाहा | oM अग्निषोमाभ्यां स्वाहा | oM इन्द्राग्नीभ्यां स्वाहा | oM द्यावापृथिवीभ्यां स्वाहा\
oM इन्द्राय स्वाहा | oM vishvebhyo देवेभ्यः स्वाहा | oM ब्रह्मणे स्वाहा | oM अद्भ्यः स्वाहा | oM ओषधि-वनस्पतिभ्यः स्वाहा | oM ग्रहाभ्यः स्वाहा | oM deva-देवताभ्यः स्वाहा | oM इन्द्राय स्वाहा | oM indra-पुरुषेभ्यः स्वाहा | oM यमाय स्वाहा | oM yama-पुरुषाय स्वाहा | oM sarvebhyo भूतेभ्यो दिवाचारिभ्यः स्वाहा | oM स्वधापितृभ्यः स्वाहा \[touch water] | oM ye भूताः pracharanti divA-नक्तं बलिं इच्छन्ति vitudasya प्रेष्याः | tebhyo बलिं पुष्टि-कामो ददामि mayi पुष्टिं पुष्टिपतिर् ददातु स्वाहा \[touch water]|\
oM आचाण्डालपतिर्-ददातु आचाण्डाल-patita-वायसेभ्यः स्वाहा ||

\[Here he might insert the core oblations of the rite with the mantra of a specific deity.]

........

भूर् agnaye cha पृथिव्यै cha mahate cha स्वाहा bhuvo वायवे चान्तरिक्षाय cha mahate cha स्वाहा suvar आदित्याय cha dive cha mahate cha स्वाहा भूर् bhuvas suvash candramase ca नक्षत्रेभ्यश् cha digbhyash cha mahate cha स्वाहा ||

He offers the final oblation with:\
sapta te agne samidhas sapta जिह्वास् sapta ऋषयस् sapta धाम प्रियाणि |\
sapta होत्रास् सप्तधा त्वा yajanti sapta योनीर् A पृणस्वा घृतेन स्वाहा || \[TS 1.5.3]

He shall put off the fire with the formula:

agne naya सुपथा राये अस्मान् विश्वानि deva वयुनानि विद्वान् |\
yuyodhyasmaj-जुहुराणमेनो भूयिष्ठां te nama उक्तिं vidhema || \[RV 1.189.1]


