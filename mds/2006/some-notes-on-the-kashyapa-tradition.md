
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on the kAshyapa tradition](https://manasataramgini.wordpress.com/2006/12/16/some-notes-on-the-kashyapa-tradition/){rel="bookmark"} {#some-notes-on-the-kashyapa-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/16/some-notes-on-the-kashyapa-tradition/ "9:32 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There is much reason to believe that the kAshyapa tradition is a real coherent tradition of magico-medical practices in post-Vedic India that survived in some form or the other in the midst of other traditions until recently. At some point, after we discovered the coherence of this tradition, we had thought of a 3-way collaboration with shAradA and R to produce a more scholarly piece on the various aspects of this tradition --- both in terms of the printed texts and what ever little information we could gather about unpublished manuscripts of this tradition. However, the travails from saMsAra-bandha-s have prevented us from doing this. In any case here are some notes on the topic.

Many tangled lines of our investigations over the years led to this body of unique सन्स्कृत् literature that we came to term the kAshyapa tradition.

 1.  The tracing the post-अथर्वणिc development of medicinal practices has lead most students to the obvious successors in the form of the great saMhitA-s of charaka and sushruta, which represent early high points in the development of Hindu medicine. However, more careful investigation also leads to the now incompletely preserved but fairly extensive text of early Hindu medicine the kAshyapa तन्त्रं (also called saMhitA, hence forth KT) that covers a wide range of topics especially pediatrics.

 2.  The investigation of the early कौमार mythology and liturgy leads to the same KT that has extensive chapters on kumara and his graha-s, रेवती, मुखमण्डिका and षष्ठि, and their pacification in the context of application of pediatric drugs. It provides the complete narrative of kAshyapa on how देवी रेवती dispatched by skanda destroyed the asura कन्या दीर्घजिह्वी and others.

 3.  The investigation of the early history of Hindu zoology --- the study of snakes, insects, arachnids and rodents leads to another kAshyapa saMhitA or the kAshyapa garuDa saMhitA (henceforth KS-G).

 4.  The study of the successors of the atharvaN tradition on toxicology and the link to the treatment of snake bites obviously leads to the same KS-G.

 5.  The search of remnants of the once extensive shaiva garuDa tantra-s also points to the KS-G which is the source book of several garuDa prayogas.

 6.  The common section on magico-medicinal practices presented in the tantric treatises of the इशानशिव mishra and the Chera magician chenas नारायणन् नम्बुथिरिपाड् also suggest that their source was the same KS-G.

As we can currently identify it this kAshyapa tradition covers the following texts: 1) The kAshyapa saMhitA, KS-G 2) The kAshyapa tantra/saMhitA- KT 3) The manuscript termed kaShyapa saMhitA of shiva-पार्वति संवाद, which covers several diseases like tuberculosis, leprosy, tumors and the like. 4) The काश्यपीय रोगनिदानं also on diverse diseases. The पैप्पलादस् in their assorted late productions preserve a text termed गरुडोपनिषत्. While not mentioning kAshyapa its contents suggest a close relationship with KS-G. The विष sections of इशानशिव paddhati and tantra-sAra-संग्रह also are derivatives of the kAshyapa works.

The KT is wider in its scope and its surviving portion covers a variety of medical topics under the following headings:

sUtra स्थानं: 1) लेहाध्याय- covers pediatric drugs which are administered by licking. 2) क्षिरोत्पत्ति- covers suckling children, problems relating to milk-borne infection, like those attributed to skanda grahas. 3) danta janmika- the treatment of conditions of children growing teeth and defects of milk teeth 4) चूडकर्णीय- the issue relating to piercing of ear-lobes. 5) स्नेहाध्याय- on lipids and oils. The identification and treatment of lipid metabolism related conditions. 6) sveda- sweat and techniques to induce sweating to remove toxins from the body. 7) उपकल्पनीय-mainly dealing with digestive problems. 8) vedana- a large section describing various types of morbidity and painful symptoms associated with all parts of the body. 9) चिकित्सासंपादनीय- mainly physicians qualities and qualifications 10) roga- detailed description of diseases and their causes. Notes the connection between blood physiology and bile production in liver. 11) लक्षण- physical examination of children for bodily defects and signs of disease. It also mentions 22 physio-personalities types.

vimAna-स्थान: 1) शिष्योपक्रम- qualifications of medical students and the aspect of medical education

shArIra स्थान: sense organs and संख्य thought. 1) अस्मान्गोत्रीय- sex and uterus 2) गर्भावक्रान्ति- a description of fetal development. 3) sharIra विषय- anatomy. Discusses amongst other things the arteries and veins and the sweat glands. 4) जातिसूत्रीय- discusses reproduction and child birth related issues.

इन्द्रीय स्थान: 1) ओSःअध बेषज covers a melange of topics including dreams caused by the seizure by agents of कार्त्तिकेय, and some properties of drugs.

चिकित्सा स्थानं: 1) jvara चिकित्सा- diagnosis of fevers. 2) गर्भिणीचिक्तिसा- attendance to diseases of pregnant women. 3) दुष्प्रजाताचिकित्सा- dealing with birth defects. 4) बालग्रह- deals with the stories of the emergence of the retinue of कुमार and various magico-medical means of dealing with seizure by the agents of कार्त्तिकेय, like रेवती, पूतना, मुखमण्डिका and षष्ठि. 5) प्लीहरोग- spleen disease. Correctly identifies that blood flows through the spleen sinuses. 6) उदावर्त- colon diseases. 7) राजयक्ष्म- tuberculosis and its treatment. Mentions a secret drug involving frog skin secretions in treating the condition. 8) gulma- classification and treatment of tumors. 9) कुष्ठ- diagnosis and treament of leprosy. 10) मूत्र-कृच्छ्र- urinary tract infections, complications of diabetes and their treatment. 11) द्विव्रण- classification, identification and treatment of boils and sores. 12) प्रतिश्याय 13) उरोघात- cardiac problems 14) shotha- inflammations and their treatment 15) कृमि- helminth infestations, especially of the alimentary tract. 16) मदात्याय- alcoholism. 17) phakka- polio diagnosis and management. 18) धात्री- nursing diseases

siddhi-स्थानं: 1) rAja पुत्रीया 2) tri-लक्षणा 3) vamana विरेचनीया 4) नस्तः कर्मीया 5) क्रिया 6) basti-कर्मीया 7)पञ्चकर्मीया 9) मङ्गल

कल्पस्थान: 1) धूप- fumigation and use of smokes in various procedures like disinfection and sedation. 2) lashuna-preparation of garlic for medical purposes. 3) कटु-taila- making of medicinal oils in connection to spleen disease. 4) ShaT- mainly concentrates on eye infections. 5) शतपुष्पा-शतावरी- describes preparations from fennel and aloe plants for different purposes. 6) रेवती- this kalpa contains an account from the kaumara lore relating to how कुमार and his agents destroyed the asuras. It the goes on to describe the magico-medical treatment for pediatric diseases attributed to agents of कुमार headed by रेवती. 7) bhojana kalpa- summary of basic nutritional and dietary issues. 8) विशेष- deals with सन्निपात jvara. 9) saMhitA- a summary kalpa on the kAshyapa tantra. It gives the following origin mythology for the tantra: Diseases first emerged from the wrath of rudra that destroyed the sacrifice of prajApati दक्ष. Then kashyapa produced this great saMhitA of the medical lore counter the ravages of disease. जीवक, the son of the भार्गव ऋशि ऋचिक mastered this medical lore of kAshyapa and systematized and summarized it as the kAshyapa tantra even as a young boy. But when he went to teach medicine the other vipra-s refused to take him seriously as he was just a boy. He then he took a bath in a lake formed by the गङ्गा and emerged as a serious elderly man. The ऋषिस् now learned from him calling him वृद्ध-जीवक. Subsequently the tantra was lost, but the यक्ष अनायास recovered the text and restored it. Then a descendant of जीवक of the वात्य्स clan of भार्गव-s pleased the यक्ष with rites and re-obtained the tantra and propounded it in the kali yuga.

khila: This is a large appendix that follows the main saMhitA and provides further technical details on many of the topics in the saMhitA as well as other topics like dermatology.

Thus, the KT resembles the more famous early medical saMhitA-s like those of charaka or sushruta. However, it is distinct in having a major body of kaumara material and several prayoga ritual practices that accompany the conventional medical practices typical of Hindu medicine. It provides one of the most detailed early accounts of pediatric practices and in this connection deploys several कौमार prayoga-s to the कौमार deities.


