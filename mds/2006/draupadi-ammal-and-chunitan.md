
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [draupadi अम्माळ् and चुनीतन्](https://manasataramgini.wordpress.com/2006/05/22/draupadi-ammal-and-chunitan/){rel="bookmark"} {#draupadi-अममळ-and-चनतन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/22/draupadi-ammal-and-chunitan/ "5:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A sub-set of the smarta brahmins of Gingee, Melaccheri Kadu and parts of the modern Telugu country upwards of Kalahasti, usually belonging to the great sect of बृहत्चरणं or rarely the वडमस् (northerners), are called पारतियार्-s. There are other पारतियार्-s belonging the middle caste vELALar-s. Both of them are associated with the draupadi अम्माळ् temples of the region, whose many patrons are vanniyar-s. The vanniyar-s are described by some as शूद्र or अवर्ण, but their and internal beliefs point to the possibility that they were actually vahni-kula राजपुत्र-s. Both the स्मार्त and vELALar पारतियार्स् are experts in the narration of the महाभारतं or the मकापारतं as they respectively call it. Their versions are obviously different. The vELALar version, is very popular with the vanniyars and they enact it in kuttu-s or Dravidian dance-dramas, though the स्मार्त version too is enacted by these actors. The vELALar form is ahistoric, anachronistic and full of local Dravidian deities. While the स्मार्तस् too incorporate some of these their narrative of the draupadi अम्माळ् kathai has an interesting consistency with the पौराणीc narratives.

The critical स्मार्त version recorded in full by माताश्री's clanswoman goes thus:

 1.  bhIma the second पाण्डव killed baka in the outskirts of ekachakra.

 2.  His terrified attendents and wives begged bhima to spare them. He agreed but asked them to leave ekachakra for good.

 3.  In the reign of king shuchadratha, 6 rulers after king janamejaya पारिक्षित, हस्तिनापुर was drowned by the unexpected flood. The king and his family fled the place with the surivors.

 4.  A colateral kuru prince वृष्णिमन् declared himself king of the kuru realm. But by then shuchadratha had reached kaushambi, where he established a new city and declared himself ruler again.

 5.  In the new kuru capital of kaushambi, sunitha succeeded shuchadratha

 6.  अचलाधिप, a rAkShasa and descendent of baka, attacks सुनीथ and ravages his kingdom. He has a hundred heads and he could not be killed if 99 of them are cut. Anyone who cut the 100th head would die instantaneously like the curse of वृद्धक्षत्र and jayadratha.

 7.  He is told by his ब्राह्मण-s that he has to invoke draupadi to incarnate again.

 8.  सुनीथ goes to the Himalayas and performs a fire ritual. As he casts the oblations in the fire, draupadi emerges from it holding a त्रिशूल, shakti and pAsha.

 9.  The draupadi asks सुनीथ to get her assistant yuddhapati (tamil: pOrmannan or pOttu-राजन्, a popular vanniyar deity) to help her in the campaign, and he duely does so.

 10.  After a fierce fight she cuts of all the heads and finally severs the 100th head with her त्रिशूल. But before it could fall to the ground yuddhapati grabs it and keeps holding it for enternity.

 11.  सुनीत founds the cult of draupadi by building her primal temple and in its आवर्ण there is yuddhapati with the achala's severed head. The Gingee temple and others in the region are said to be replicas of this original temple of सुनीथ.

In some versions pOtturAja comes out of लिङ्ग.


