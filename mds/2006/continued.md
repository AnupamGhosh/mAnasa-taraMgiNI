
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Continued](https://manasataramgini.wordpress.com/2006/02/12/continued/){rel="bookmark"} {#continued .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/12/continued/ "3:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}


  - Then we spoke of Messier and Mechain's objects in the sky.\
We simple yarned of the good old day when we conquered every Messier object with our small scope. Messier is supposed to have discovered 20 comets and 103 deep sky objects. Two of which are sort of spurious. He seems to have sparked the great hunt of W. Herschel.

  - Then we spoke on primate evolution\
First we spoke of the possibility of a primate-dermopteran-rodent evolutionary link. Superficially the plesiadapiforms seem to confirm such a link. But the jury is still out and we cannot be sure of this. Clearly, the recent analysis of skulls of the plesiadapiforms [Ignacius ]{style="font-style:italic;"}and [Carpolestes ]{style="font-style:italic;"}suggest that they were the most ancient precursors of the primates. The opposable thumb appears to be a synapomorphy of the euprimates. The basal most of the euprimate clade are the adapiforms, of which the lemurs of Madagascar and lorises seem to be an aberrant late surviving form. This followed by the the Omomyids from which arose the Haplorrhines with basal the tarsioids, followed by the anthropoids which split up into Platyrrhines (new world monkeys) and Catarrhines (old world monkeys+ apes). There are many mysteries in the evolution of catarrhines anthropoids alone. When did the ancestral Platyrrhines resembling the basal anthropoids from the Al Fayum Beds of Egypt reach South America Given that only they and the caviamorph rodents (which spawned *Phoberomys*) reached South America they seem to have floated across the narrower Paleo-Atlantic ocean, due to their small size on washed away branches perhaps. An amazing journey to make! What about the earliest Anthropoids like Eosimias: did Anthropoids first emerge in Asia or Africa What is the phylogenetic position of *Oreopithecus*. Did it convergently evolve to an ape-like state? We also wondered if the Platyrrhines and Catarrhines independently evolve larger brain sizes. This may have a bearing on when the South American monkeys separated.

What was the issue with the gigantic *Gigantopithecus*-- was it an extra-large orang like ape? *Gigantopithecus* like one of those legendary ऋक्षस् of the रामायण (जाम्बवान्) was the largest primate. It was probably over 10ft tall when it stood erect and weighed about 400kgs or more. Its over-engineered dentition suggests that it was the only bamboo eating ape. We also spoke of the giant baboons and geladas, [Theropithecus oswaldi ]{style="font-style:italic;"}and [Dinopithecus ingens]{style="font-style:italic;"}, which appear to have been large grass-eating monkeys that might have competed partly with the coeaval human ancestors.

  - Then we talked about a far-fetched idea as to whether the idea of प्रळयस् and other doomsday scenarios are linked to the ancient appreciation of the shortest Milankovitch cycle. As it is possible that knowledge of precession is a very ancient issue, may be there was an idea that the changing precessional position may be correlated with climatic change. The shortest Milankovitch cycle happens because the Earth may precess to the state where winter is closer an amphelion and summer closer to perihelion. When it happens the winters and summers are accentuated in their severity. This could have devastating climatic consequences.

  - Then we spoke of various issues related to Arthropods: like the chemical warfare by whip scorpions, bombardier beetles, cockroaches and Polyxenid millipedes. All these arthropods are very accurate in directing their spray the prey. We wondered about the ant [Thaumatomyrmex ferox]{style="font-style:italic;"} that decimates polyxenid millipedes unlike other ants which are repulsed or killed by these millipedes.


