
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some old drawings-II](https://manasataramgini.wordpress.com/2006/05/01/some-old-drawings-ii/){rel="bookmark"} {#some-old-drawings-ii .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/01/some-old-drawings-ii/ "5:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/devi_small.jpg){width="75%"}
```{=latex}
\end{center}
```



This was her piece on the goddesses- it is a 'combined' vision, though the drawings are entirely hers. It was done during the most dreary of all lectures, the geography class, during which even DoM, डमार्, तुण्डिक, दुष्ट-durmukha and others fell asleep.

From left to right: 1)Top: कर्णमोटिनि 2) down: गौरी 3) middle: षष्टि 4) down: त्वरिता 5)up: आद्यलक्ष्मी 6) down: कर्ण पिशाचिनी


