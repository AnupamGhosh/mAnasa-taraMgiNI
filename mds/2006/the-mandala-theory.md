
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The मण्डल theory](https://manasataramgini.wordpress.com/2006/02/02/the-mandala-theory/){rel="bookmark"} {#the-मणडल-theory .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 2, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/02/the-mandala-theory/ "6:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The ब्राह्मण of old had laid out the intricacies of artha; विष्णुशर्मन् explained that for dullards like us through the mouth of the jackal. And then दण्डिन्, that scaler of great literary pinnacles, had parodied the same. The gods in heaven and earth had given us an even share of luck and ill-luck, and both friends and foes. We learned the world of society is a मण्डल. There are friends of varying degrees in the inner rounds. Double-agents and neutral in the broad mid-regions and enemies at the periphery. Through the aid of the deva-s we suppressed and crushed some enemies, but others continue to torment us without a solution is sight. The biggest question for a man is often who is a friend and who is a enemy? Can a man really classify women in these categories? These are questions for which answers exist, but may not help in life.\
The basic truth is that most relationships, including friendship, depend on the fact that one man possesses what the other does not. Thus, a friend is one who gives what he has to another without being coerced to do so, and there an element of gratitude and willingness in this trading of favors. An enemy is one who forcefully takes what a man has for himself. Rival is one who exclusively contends for the same object of desire as you. A double-agent commits both friendly and inimical acts. All the above applies between males.

Between males and females interactions are fundamentally different. But there is a class of women similar to enemies who may be termed parasites. They suck males, just like spiders suck their mates. Friendship between a male and female is in reality different from that between two males. Many people find it difficult to accept this fact.


