
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The basic invocation of पद्मावती according the पद्मावती kalpa](https://manasataramgini.wordpress.com/2006/12/21/the-basic-invocation-of-padmavati-according-the-padmavati-kalpa/){rel="bookmark"} {#the-basic-invocation-of-पदमवत-according-the-पदमवत-kalpa .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/21/the-basic-invocation-of-padmavati-according-the-padmavati-kalpa/ "6:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The jinendra explained at length how the पद्मावती prayoga-s are performed as per the पद्मावती kalpa. Described below is the vidhi for पद्मावती आराधन:\
अथः सर्वावर्ण पूजा:\
[ऒं ह्रीं धरणेन्द्राय नमः]{style="color:#99cc00;"} -- eastern door; [ॐ ह्रीं अधच्छदनाय नमः]{style="color:#99cc00;"} -- southern door; [ॐ ह्रीं ऊर्ध्वच्छदनाय नमः]{style="color:#99cc00;"} -- western door; [OM ह्रीं]{style="color:#99cc00;"} [पद्मच्छदनाय नमः]{style="color:#99cc00;"} -- northern door

[ऒं लं इन्द्राय नमः]{style="color:#99cc00;"} (E) | [ऒं रं अग्नये नमः]{style="color:#99cc00;"} (SE) | [ॐ शम् यमाय नमः]{style="color:#99cc00;"} (S) | [ॐ षं नैरृत्याय नमः]{style="color:#99cc00;"} (SW) | [ऒं वं वरुणाय नमः]{style="color:#99cc00;"} (W) | [ऒं यं वायवे नमः]{style="color:#99cc00;"} (NW) | [ऒं सं कुबेराय नमः]{style="color:#99cc00;"} (N) | [ॐ हं ईशानाय नमः]{style="color:#99cc00;"} (NE) ||

[ऒं ह्रीं जये! नमः]{style="color:#99cc00;"} (E) | [ऒं ह्रीं विजये! नमः]{style="color:#99cc00;"} (S) | [ऒं ह्रीं अजिते! नमः]{style="color:#99cc00;"} (W) | [ऒं ह्रीं अपराजिते! नमः]{style="color:#99cc00;"} (W) | [ऒं ह्रीं जम्भे! नमः]{style="color:#99cc00;"} (SE) |[ऒं ह्रीं मोहे! नमः]{style="color:#99cc00;"} (SW) | [ऒं ह्रीं स्तम्भे! नमः]{style="color:#99cc00;"} (NW) |[ऒं ह्रीं स्तम्भिनि! नमः]{style="color:#99cc00;"} (NE) ||

[ॐ ह्रीं अनङ्गकमलायै नमः]{style="color:#99cc00;"} (E) | [ऒं ह्रीं पद्मगन्धायै नमः]{style="color:#99cc00;"} (SE) | [ऒं ह्रीं पद्मास्यायै नमः]{style="color:#99cc00;"} (S) | [ऒं ह्रीं पद्ममालायै नमः]{style="color:#99cc00;"} (SW) | [ऒं ह्रीं मदनोन्मादिन्यै नमः]{style="color:#99cc00;"} (W) | [ऒं ह्रीं कामोद्दीपनायै नमः]{style="color:#99cc00;"} (NW) |[ऒं ह्रीं पद्मवर्णायै नमः]{style="color:#99cc00;"} (N) | [ॐ ह्रीं त्रैलोक्यक्षोभिण्यै नमः]{style="color:#99cc00;"} (NE) ||

[ॐ ह्रीं ख़्शां प नमः]{style="color:#99cc00;"} (E) |[ॐ ह्रीं ख़्शीं द्मा नमः]{style="color:#99cc00;"} (S) | [ॐ ह्रीं ख़्शूं व नमः]{style="color:#99cc00;"} (W) | [ॐ ह्रीं ख़्शैं ती नमः]{style="color:#99cc00;"} (N) ||

core mantras:\
[ॐ ह्रीं नमोऽस्तु भगवति ! पद्मावति ! एहि एहि सं वौषट् ॥]{style="color:#99cc00;"} (आवाहन mantra) The देवी in invoked into the yantra or idol.\
[ॐ ह्रीं नमोऽस्तु भगवति ! पद्मावति ! तिष्ठ तिष्ठ स्वाहा ॥]{style="color:#99cc00;"} (स्तिथिकरण mantra) The देवी is positioned in the right place in idol or yantra.\
[ॐ ह्रीं नमोऽस्तु भगवति ! पद्मावति ! मम सन्निहिता भव भव वषट् ॥]{style="color:#99cc00;"} (सनिद्धिकरण mantra). She is made to settle down in one's place of worship\
[ॐ ह्रीं नमोऽस्तु भगवति ! पद्मावति ! गन्धादीन् गृह्ण गृह्ण नमः ॥]{style="color:#99cc00;"} (with 5 such formulae offer the पञ्चोपचार पूज)\
[ॐ ह्रीं नमोऽस्तु भगवति ! पद्मावति ! स्वस्थानं गच्छ गच्छ जः जः जः ॥]{style="color:#99cc00;"} (visarjana mantra) Ritual dismissal of the deity with प्राणायाम.

Then the महाजप is performed with the following mantra:\
[ऒं ह्रीं ह्रैं ह्स्क्लीं पद्मे ! पदम्कटिनि ! नमः ॥]{style="color:#99cc00;"}

Flower offerings are made with red करवीर and सवृन्त flowers.

The great षडक्षरी विद्या of पद्मावती is:\
[ॐ ह्रीं ह्रैं ह्स्क्लीं श्रीं पद्मे ! नमः ॥]{style="color:#99cc00;"}

The so-called त्र्यक्षरी विद्या of पद्मावती is:\
[ऒं ऐं क्लिं ह्सौः नमः ॥]{style="color:#99cc00;"} (note its relation with the bAlA mantra of श्रीविद्या)

The so-called एकाक्षरी विद्या of पद्मावती is:\
[ऒं ह्रीं नमः ॥]{style="color:#99cc00;"}

garuDa mantra:[ऒं ह्रां ह्रीं स्वाहा ॥]{style="color:#99cc00;"}\
सरस्वती mantra: [ऐं ह्रीं श्रीं क्लीं ह्सौं वद वद वाग्वादिनि ! भगवति सरस्वति ! तुभ्यं नमः ॥]{style="color:#99cc00;"} (The japa of these mantra may be done further for their specific viniyogas there after)

...0...

Along with the पद्मावती आराधन the jinendra also routinely performed a homa to सरस्वती and the श्रीविद्या krama as per the nAstIka traditions. The श्रीविद्या of the नास्तीकस् corresponds to the पञ्चदशाक्षरी of the कादि-mata and is given concealed within a remarkable mantraic स्तोत्रं known as the श्रीविद्या garbha स्तोत्रं (SGS). Their krama is relatively simple, done separately after the पद्मावती worship and coupled with the वाराही and rAja-श्यामला mantras from the पद्मावती kalpa. The SGS takes the place of the ललिता सहस्रनामं or trishati in their tradition.


