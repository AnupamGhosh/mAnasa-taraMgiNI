
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Killing of andhaka](https://manasataramgini.wordpress.com/2006/11/17/killing-of-andhaka/){rel="bookmark"} {#killing-of-andhaka .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/17/killing-of-andhaka/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having bowed to the one whose odd eye roasts the universe at the end of the kalpa we narrate his awful exploits.

When rudra and उमा were sporting on the eastern ridges of the mandhara, उमा closed the eyes of one who simply known as the god because even his name can terrify the uninitiated. When his eyes were closed by उमा's hand which had the complexion of coral and a golden lotus the universe turned exceedingly dark, for after all he is soma-सूर्याग्नि-lochana. उमा's hands heated by his eye on the forehead began to sweat copiously and resulted in divine conception due to contact with the god. A terrifying being manifested itself. It was blind, deformed, and black in complexion, with matted locks of hair on the head and fine down all over the body. It sang and laughed horribly, danced and put out its tongue like a serpent and roared fiercely. haimavatI aghast at the sight asked what it was. rudra said, you made it yourself by closing my eyes. She took her hands of his eyes and as the light came back the being looked even more hideous. rudra said: this awful being is andhaka, the blind one, who was born when you closed my eyes. Take care of him well as he is an object of our creation.

The daitya हिराण्याक्ष did not have a son and was performing austerities to gain one. shiva the granter of boons gave him andhaka to become his son. Thereafter in the clash between the demons and devas विष्णु in the form of the terrible वराह and नृसिंह slew हिराण्याक्ष and his brother हिरण्यकशिपु. There upon प्रह्लाद the mighty son of हिरण्यकशिपु took over the lordship of the daityas and made fun of andhaka as the blind and ugly one. Then andhaka performed a terrible rite where he offered all his flesh and blood to brahmA. Finally, when he was to offer himself as an oblation he received a boon of invincibility from brahmA, except under the condition where he will covet the most excellent epitome of the female kind. This it was said would bring his destruction. andhaka having gained eyes and beauty in addition came back and was declared lord of the asuras with the mighty प्रह्लाद becoming subservient to him. He conquered the universe and tormented the gods. He acquired thousands of beautiful women from the realms of the asuras, पृथिवि and the heavens. He engaged in intercourse with them on the beautiful banks of the rivers, mountains and other places, drank divine beverages along with them. He thus enjoyed time in magic mansions erected for him by the asura maya. He attacked leading Brahmins with fallacious arguments and derided vedic learning. He went around with his other daitya friends like the anindra प्रह्लाद destroying vedic rites. He built himself a large city in the मन्धार mountains and settled there with his दानवस् and daityas. His three ministers duryodhana, vighasa and hasti, saw the most beautiful woman in the company of a terrible being. He had serpents all around his body. He had a necklace of skulls and had a digit of the moon on his matted locks. He had a trident in his hand and was a great archer with a terrible bow and quiver full of deadly arrows. He had a terrible sword, and his body was smeared with ashes and he wore vestures of elephant and tiger hides. The elegantly dressed young epitome of the female kind was his companion and was embracing him. They were served by an ape-faced attendant of exceedingly terrible face and demeanor, who also held awful weapons in his rough and brawny arms.

andhaka smitten by passion asked his ministers to go and ask that ascetic to give up his wife to him. The asuras duly went to shiva and asked him who he was and ordered him to hand over his wife to them. rudra said that he neither new his parentage nor remembered his origins and said that the asuras may come and take what they wanted if they were able to fight him or his गण वीरक. Furious, andhaka went to seize पार्वती but was obstructed by वीरक who showered on him stones, trees, fire and serpents. The asura dispersing these weapons rushed at वीरक with his sword. वीरक shattered it to pieces and put andhaka to flight. He also beat back प्रह्लाद, bhaji and cut kujambha to two pieces.

rudra then told पार्वती that he had to leave to perform the महापाशुपत vrata because he had that regain his energies lost the due the sexual dalliance with उमा during the generation of skanda and विनायक. He asked उमा to stay within their cave with their attendant वीरक serving as the guard outside. andhaka avenge his defeat returned with a terrible daitya army and pounced on वीरक. They fought a terrible combat for 500 days on end, using swords, shakti-s, पट्टिशिस्, and maces, shooting numerous arrows like ardhachandrabha-s, कूर्म-mukhas, vatsadantas, hurling axes and iron balls. वीरक overwhelmed by the asura's weapons was buried within them. उमा at that point called the other gods to her aid. To aid her, their shaktis went forth to fight the daityas: the frightful goddesses like brAhmI with a rod, वैष्णवी with her usual weapons, इन्द्राणी of a thousand eyes, वारुणी, कौबेरी of hideous form and 100s of other goddesses came for to fight along with them. वीरक in the meantime regained consciousness and along with the goddess attacked the asuras. गौरी too joined the fight with the goddess and just then they saw shiva arrive from his vrata. The goddesses delighted at his fought fiercely and attacked the frontline division of the asuras who were commanded by gila. The asura routed by the goddesses retreated in disarray. पार्वती then sent the goddesses back and honored वीरक with hundreds of gifts.


