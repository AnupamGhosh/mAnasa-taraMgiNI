
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The origin of the royal bhosles](https://manasataramgini.wordpress.com/2006/11/26/the-origin-of-the-royal-bhosles/){rel="bookmark"} {#the-origin-of-the-royal-bhosles .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 26, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/26/the-origin-of-the-royal-bhosles/ "11:32 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The problem of the origin of the royal Bhosles, or the clan of Shivaji the reviver of Hindu fortunes, has interested me at various points due to a variety of reasons. I had come across the notorious stone inscription in Thanjavur that gave a peculiar genealogy that did not match any other genealogy from Maharashtra. During my peregrinations in the Maharatta country I came across tribal Bhosles who were cattle-herders, and many other tribal groups bearing traditionally Maharatta last names- e.g. the Shinde, the Holkar and the Gaikavad. There is evidence that these later Maharatta princes were originally शूद्रस् who attained royal status due their military distinction. So given the Thanjavur inscription, we did not find it impossible  that the royal Bhosles were also शूद्रस् who attained royal status as the latter Maharatta princes had done. Per say, this phenomenon was pretty widespread: In Andhra, in the years of the great struggle, we have the Kamma and Reddi warlords (self-described as शूद्रस्) attain royal status as a consequence of their valiant struggle against the Moslems (Though there are other lines of evidence that they were originally kShatriya-s who were merely reassuming their lost status). In support of such origins for the Bhosles we have the statement of Jadhavrao the father-in-law of Shahji. When Shahji's father, Maloji, asked him for his daughter's had he refused initially citing his higher status as result of his descent from the यादव clan who were originally rulers of Maharashtra before the Islamic deluge. The name bhosle is variously hyper-sanskritized: परमानन्द makes it बृषबल. A स्मार्त eulogist from Thanjavur made it भास्वत-kula. This suggests that it probably did not really have a Sanskrit original.

Yet, we have many assertions to the contrary claiming royal Bhosles to be a Rajputs of the Sisodia dynasty. This idea greatly fascinated me, because it would mean that many primary Hindu heroes of medieval times had a common origin- linking the Ranas Hammir, Kumbha, Sanga and Pratap with Shivaji -- a pretty remarkable thing if true. So what are the points that support the Rajput origins of Shivaji?

 1.  shiva-bhArata of परमानन्द: Shivaji and Shahji are of the इक्ष्वाकु lineage like the Sisodias

 2.  पर्नाल-parvata-ग्रहणाख्यान: Shivaji is a Sisodia

 3.  Bhushan the Hindi poet states the same.

 4.  Shahji in his letter to the Sultan Adilshah states he is a Rajput.

 5.  Islamic raconteur Khafi Khan describes Shivaji as a descendent of the Ranas of Chittor.

 6.  राधा-माधव-विलास-champu of kavi जयराम (\~1654 CE) describes Shivaji as descending from the Sisodias of Chittor.

 7.  An intelligence dispatch of the East India company from 28th Nov 1659 reports: "Sevagy, [a great Rashpoote]{style="font-style:italic;"} issues forth from his fort Rayguhr to strike blows on the Emperor, Duccan, Golconda and the Portuguese."

 8.  Colonel Tod and historian Ojha who had access to the Rajput वंश chronicles claimed that they apparently mention the Bhosles descending from Ajay Singh, the uncle of Vir Hammir.\
The proponents of the Rajput theory mention that after Lakshman Singh and Ari Singh died fighting Khalji, the Rajputs fled to the Aravalis. Here, Ajay Singh brought up Vir Hammir the son of his brother Ari Singh. When Ajay Singh faithfully handed the throne to Hammir, his sons were furious and fled to the Deccan. One of them Sajjan Singh then founded the Bhosle clan in Maharashtra. In Rajasthan, Vir Hammir liberated Mewar from the Moslems after defeating them in many battles.

The following are the main dynastic accounts given for Shivaji (names phonetically rendered):\
Kolhapur Royal chronicles:\
लक्श्मण सिंह -> sajjana सिंह (reaches Maharashtra in 1310 AD) -> दिलीप सिंह -> शिवाजी-I -> भोसाजी -> devaraj-jI -> ugrasena -> महुलाजी -> खेलोजी -> janako-jI -> सम्भाजी -> बाबाजी -> मालोजी -> शहाजि -> शिवजी Chatrapati

As per Tod's account from Rajput chronicles:\
ajaya सिंह -> sajjana सिंह -> दिलीप सिंह -> शिवाजी-I -> भोराजी -> devaraj-jI -> ugrasena -> महूल्जी-> खैलुजी -> janako-jI -> सत्तुजी -> सम्भाजी -> शिवजी Chatrapati

Satara Museum chronicles:\
लक्श्मण सिंह -> sajjana सिंह (reaches Maharashtra in 1310 AD) -> दिलीप सिंह -> सिंहजी-I -> भोसाजी -> devaraj-jI (came to south India) indrasen-jI -> शुभकृष्ण -> रूपाजी -> भुमीन्द्र-jI -> धापाजी -> बर्बट्जी -> खेलोजी -> jaya-कर्ण -> सम्भाजी-> बाबाजी -> मालोजी -> शहाजि -> शिवजी Chatrapati

Thanjavur stone inscription:\
येकोजी -> शरभोजी (came to south India) -> महासेन -> ekashiva -> रामचन्द्र -> भीमराय -> एकोजी -> वराह -> एकोजी II -> ब्रह्माजी -> शहाजी -> अम्बाजी -> परसोजी -> बाबाजी -> मालोजी -> शहाजि -> एकोजी or व्यान्कोजी (First Maharatta king of Thanjavur)

The Jintikars of Gwalior give the following genealogy:\
बख्ताजी (came to south India from north) -> नागोजी -> एकोजी/व्यान्कोजी -> बाबाजी -> मालोजी -> शहाजी -> सम्भाजी (elder brother of शिवाजी) -> उमाजी -> पर्सोजी (may not be real son) -> जिन्टिकर् bhosles

From royal Rajput documents with seals, termed sanads one can confirm the following:\
लक्श्मण सिंह -> ajaya सिंह -> sajjana सिंह -> दिलीप सिंह -> sidhoji -> bhairoji (भोसाजी?) -> devaraj-jI -> ugrasena -> शुबकृष्ण

All reliable Maharatta accounts confirm:\
बाबाजी -> मालोजी -> शहाजि -> शिवजी Chatrapati

The connection between the two is the major un-answered question. One might notice that most non-Shivaji lineages of Bhosles match with the Shivaji lineage's claims only starting from Babaji Bhosle the Patil of Verul, associated with the shiva temple of घृष्नेश्वर. Hence, one might argue that the Sisodia connection is fictitious. The opponents of the Sisodia theory state that Gagabhatta, the great brahmin scholar of from Kashi, created this genealogy to legitimize Shivaji as a kShatriya and give him the यज्ञोपवीत and savitrI.

However, there is another important point that supports this connection, albeit tenuously --- the origin of the royal Bhosles of Nagpur who rose to power under their brave leader Raghuji, whose wife had an insatiable taste for French wine. The royal Bhosles of Nagpur too claimed a Sisodia origin from Sajjan Singh. But they had definitely branched off from Shivaji's line before Babaji Bhosle; hence, we are already in the murky zone. The piece of evidence linking them Shivaji comes from a tale related to his death. Since Shambaji was under detention during Shivaji's death and for reasons that are unclear Rajaram was also not around, Sabaji Bhosle an ancestor of the royal Nagpur Bhosle performed the funerary rites. Shahu also briefly considered appointing the Nagpur Bhosle as his successor because he lacked a son, but eventually chose Rajaram's descendent as his successor, being a closer relative. This suggests that the royal Bhosle clans did consider each other related. Another Maharatta clan, the Ghorpades, also claim descent from the same Sisodia lineage and declare themselves as the elder line to the Bhosles. It is possible that their claim is a bit more solid based on preserved genealogies.

Recently, there has been a new theory that tries to ingeniously combine the cattle-herder origins with royal origins. This theory derived in part from the book "shikhar शिन्गणपूर्चा shambhu महारज्" in the मरहट्टि language has the following elements: 1) Bhosle is etymologically related to Hoysala. The Hoysalas were old rulers of parts of what is now southern Maharashtra, Karnataka and Tamilnad. 2) A chieftain of the Hoysala family बलियप्पा gopati, the grandson of mahabhillama was the ancestor of Shivaji. He was a major cattle-herder with numerous herds and built a temple for मल्लिकार्जुन in Shikhar-Shinganapur. The Bhosle surname was taken up by his descendants or herders unrelated to him but in his payroll. It was common even in the north for servants to take the surnames of Rajput masters. The royal Bhosles are direct descendents of बलियप्पा gopati, while the tribal ones might be those of his erstwhile herders. In support of this it is notable the मल्लिकार्जुन was a very important shrine for Shivaji --- he spent a while performing rites there --- he even wished to renounce the world and become a shaiva ascetic but his men forbade him from doing so. His brother Ekoji/Vyankoji built the Malleshvaram temple in Bangalore again for the same form of shiva. The लिङ्ग of घृष्नेश्वर also seems to be a representative of मल्लिकार्जुन. They were all equated with the South Indian equestrian manifestation of rudra: खण्डोब or मल्लण्ण.

Even today Shikhar-Shinganapur is a major temple for Maharatti pastoralists and it had several vIra-kals in the vicinity that depict carvings of fallen cattle-raiders, similar to those of the Tamil poems, and women worshiping rudra.


