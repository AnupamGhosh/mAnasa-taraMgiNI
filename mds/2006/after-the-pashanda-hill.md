
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [After the पाषण्ड hill](https://manasataramgini.wordpress.com/2006/04/12/after-the-pashanda-hill/){rel="bookmark"} {#after-the-पषणड-hill .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 12, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/12/after-the-pashanda-hill/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were trying to trace back the footsteps of besieger. We realized some of the details of what happened immediately after the पाषण्ड hill. We got out of पाषण्ड hill and were confronted by the second "born of soma". We brushed aside "born of soma-2" pretty easily. Shortly there after an incantation followed. We believed that out pitA had taken care of that, but it was only partly so. Shaken by the recent turn of events for the first time he told us that there might have been a mistake at that point and that he might acted on a mistaken track for a whole year. Now realizing that it had gone way beyond his level, he dumped the whole thing on us to take care of it our own. But somewhat ironically we are like two incomplete men each having only one key half- he all those brilliant strategies and us the theoretical knowledge of mantras.


