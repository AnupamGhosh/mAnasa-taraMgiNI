
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The wall of बगलामुखि](https://manasataramgini.wordpress.com/2006/03/18/the-wall-of-bagalamukhi/){rel="bookmark"} {#the-wall-of-बगलमख .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 18, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/18/the-wall-of-bagalamukhi/ "7:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/bagala.0.jpg){width="75%"}
```{=latex}
\end{center}
```




R told me that her amongst her chosen देवतास् whose mantras are perfect with her is बगलामुखी. Fourier of मण्गलग्राम, was initiated into बगलामुखी विद्यास् by the powerful chera magician. बगलामुखि deployments can be terrifying for the victim and there are many of them that are generally kept secret. Some बगलामुखि deployment that I have come across include: 1) the धत्तूर हरिताल lepana प्रयोगं- the yellow orpinient made from a mixture of धत्तूर bIja and turmeric which can used for stambhana and वशिकरण. 2) The मृत्तिक वृष्भ प्रयोगं -its effects can resemble the effects of certain धूमावती prayoga-s and can be mistaken for them. It is pretty debilitating. 3) The वाम कपाल प्रयोगं- can cause a severe stambhana of the victim. 4) The shava vastra मण्दूक प्रयोगं- can paralyze the tongue or throat of the shatru. 5) The आटरूषक प्रयोगं- can destroy attacking law enforcement agents. 6) इन्द्रावारुणिक प्रयोगं- can protect from being tortured by law enforcement agents. And there are several others.

by invoking बगलामुखी one may also seek the aid of some frightful agents of hers that might strike the victim with great force. Some of these include खर्वती and सर्वविच्चा. बगला may manifest in many forms which include ब्रह्मास्त्र-विद्या, त्रैलोक्यस्तम्भिनी, trimurtistambhini and सम्हारिक विद्या. Some of these are amongst the highest of the prayoga's deployed in the attacking mode. Our clansman who was pretty confident own mantras was hit by a बगलामुखि deployment of Fourier. He was relentlessly chased by बगला agents. Some would enter his office room, others his house and still others used to knock him even when he tried to take shelter in some temples. Finally while he was in the city of Gaya, he was attacked by a sarva-dehastambhana and nearly killed. He finally found some relief when he crossed the border and reached Nepal and took shelter at the feet of बगला herself. The भरद्वाज woman's PM was also attacked by a बगला deployment. He would undergo stambhana daily for a whole month and all his mantras was were rendered ineffective. Finally he with a helper deployed the saucer-breaking धूमा to save himself for some time. However, the second बगला attack gave him a room in the buffalo-rider's home.

[\~\~\~0\~\~\~]{style="font-weight:bold;color:rgb(255,102,0);"}

Mn was having a ball of a time with his many friends at that time. He visited the muni and me and declared that we should have a good time that evening. Earlier in the day our PM had made fun of Fourier's threats, as he thought he was completely protected by the shield of महारुद्र the "flight of the crane" would not affect him. Mn was PM's favorite and he asked Mn to keep out all fears of the hocus-pocus that Fourier dabbled with. While MM was the more cautious type, she too alluded to शूलिनि, वनदुर्गा, धूर्त and the like asked Mn to have a good time. Mn believed he had nothing to fear for he actually saw that MM had a siddhi of वनदुर्गा. शचीव was a little cautious, and asked them to stay away and probably even make peace with Fourier but they told him he was being too cowardly in face of Fourier's arrogance and indulgence in ugra karmas. The muni, me, Mn, J and our "श्री विद्या adept" moved to the outskirt of the city in a large ratha that had been lent to us by the patita-सावित्र, मैरेयपायिन्. We visited the देवस्थान where our श्री विद्या adept had attained the protection of धनाकर्षण bhairava when hit by the wrath of वरुण. Then till the sun went down we indulged in vigorous गुलिकाक्रीडा. Finally, Mn and I wandered off and arrived near the wall and on it was the "flight of the crane". Who is she he asked, humbly bowing to the great one, I answered his query. The next moment a mohana dart sprung out and issued forth, I was hit first, but it came away from me after some time and seized Mn. Mn had a total stambhana in his arm. MM was alarmed at this and took the refuge of the वनदुर्गा rite that also included the invocation of प्रत्यङ्गिरा. Mn was freed from the afflication and the back-hurl was complete. Fourier, his mighty putra and पौत्रिकास् were all hit hard and reeled under it.

Fourier had been demolished but not out and his full subsequent history is for a different day. We visited Fourier's putra and his wife in गवलकुण्ड. The descendent of the renowned exponent of shuddha-विद्या लक्ष्मीधर came to their aid freed them from the ill-effects of the back-hurl and also gave him several mantras that raised him to great power. Aged Fourier met मामी and declared his intention to renew the war even though his son and descendents had signed a peace treaty. Fourier smarting under the crushing strike from the प्रत्यङ्गिरा formulae which had completely destroyed him and his पत्नी, went back to the chera magician and asked of the higher yuddha-stambhini. The chera imparted him all the prayoga-s and took an oath that even if Fourier was sent to vaivasvata's regions due a future back-hurl he will personally teach Fourier's भ्रातृव्य's an unforgettable lesson. In the mean time Aunt Spring thought she was completely safe because of the famous "yajurvedin" who an early student of my learned teacher. Around the time my teacher had finished performing my first year rites, he had already imparted the "yajurvedin" the needful spells. With that he was largely safe. But then on that quiet night in म्लेच्छ desha, in क्रौञ्च द्वीप, out of the blue, that force of the yuddha-stambhini prayoga deployed Fourier's chera hit the "puruSha"; he almost crossed the bridge on the यमुना before being brought back by the "yajurvedin". The news of the strike reached us: "All नृ fruits on the tree will be bored by beetles".

We sensed the movements.


