
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Heading into the great unknown](https://manasataramgini.wordpress.com/2006/06/16/heading-into-the-great-unknown/){rel="bookmark"} {#heading-into-the-great-unknown .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 16, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/16/heading-into-the-great-unknown/ "5:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In a few hours we head into the great unknown. We thought last night that we had shaken of the "crane-arrow" after a costly expedient. Today we are not sure if this is so. Whatever the case, over the next three days we have to be in the wild, as in the old days of field work, and hope we are not struck by the arrow then.


