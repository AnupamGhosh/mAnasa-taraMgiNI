
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The शृङ्गार-परितुष्टीय sAra](https://manasataramgini.wordpress.com/2006/08/04/the-shringara-paritushtiya-sara/){rel="bookmark"} {#the-शङगर-परतषटय-sara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 4, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/04/the-shringara-paritushtiya-sara/ "4:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The शृङ्गार-परितुष्टीय sAra is viewed with intense embarrassment by the modern Hindu. At every level he or she would not like to believe that such texts were composed by their ancestors. I admit that literally translating some of its terms might sound embarrassingly absurd, but I suspect they are not supposed to sound as foolish as the literal translations are. But then I am not the kind of Hindu who is "modern" in outlook, and still linger on in the early centuries of the Hindu era. So I do not feel any coyness about much of this. Nor am I a pseudo-scientist filled with the zeal that characterizes Abrahmistic messianisms to cleanse my compatriots of so-called irrational thought. So here is its un-adulterated summary:

The vaiShNava formulae for women to bewitch men:

[ब्रह्मदण्डी वचा कुष्ठं प्रियङ्गवर्-नागकेशरं ।]{style="color:rgb(51,204,0);"}\
[दद्यात् ताम्बूल संयुक्तं स्त्रीणां मन्त्रेण तद्वशम् ।]{style="color:rgb(51,204,0);"}\
[ॐ नारायण्यै स्वाहा ॥]{style="color:rgb(255,0,0);font-weight:bold;font-style:italic;"}\
[ताम्बूलं यस्य दीयेत स वशी स्यात् सुमन्त्रतः ।]{style="color:rgb(51,204,0);"}\
[ॐ हरिः हरिः स्वाहा ॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

A woman wishing to fascinate a man will make a mixture of the herbs ब्रह्मदण्डी, वाचा कुष्ठ, प्रियङ्गु and नागकेशर, utter the above incantation to the goddess नारायणि and give the mixture as a तांबूल to her man. After she has given the mixture she shall utter the incantation to the god विष्णु (above).

[खञ्जरीटस्य मांसं तु मधुना सह पेषयेत् ।]{style="color:rgb(51,204,0);"}\
[ऋतु-काले योनि लेपात् पुरुषो दासतामियात् ॥]{style="color:rgb(51,204,0);"}

The woman should grind the खञ्जरीट flesh with honey and apply it to her vagina at the time of her menstrual flow. Her man would become her slave.

[दुरालभा वचा कुष्ठं कुङ्कुमञ् च शतावरी ।]{style="color:rgb(51,204,0);"}\
[तिल-तैलेन संयुक्तं योनि लेपाद् वशी नरः ॥]{style="color:rgb(51,204,0);"}

दुराल्लभ, वाचा, कुष्ठ, कुङ्कुम and शतवरी plants are ground into the oil of sesame seeds and applied as a lubricant to the vagina. This makes the woman bewitch her lover.

Formula and preparation for men to bewitch women:

[महासुगन्धिका मूलं शुक्रं स्तम्भेत् कटौ स्थितम् ।]{style="color:rgb(51,204,0);"}\
[ॐ नमः सर्वसत्त्वेभ्यो नमः सिद्धिं कुरु कुरु स्वाहा]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}\
[सप्ताभि-मन्त्रितं कृत्वा करवीरस्य पुष्पकम् ।]{style="color:rgb(51,204,0);"}\
[स्त्रीणामग्रे भ्रामयच्छ क्षणाद् वै सा वशे भवेत् ।]{style="color:rgb(51,204,0);"}\
[ब्रह्मदण्डीं वचां पत्रं मधुना सह पेषयेत् ।]{style="color:rgb(51,204,0);"}\
[अङ्ग लेपाच्छ वनिता नान्यं भर्तारमिच्छति ॥]{style="color:rgb(51,204,0);"}

He smears the extract of the root of the महासुगन्धिका plant on his pelvis and prevents premature ejaculation. He then utters the above incantation seven times and prepares an extract of the करवीर flowers. He then sprinkles this on the woman and in instantaneously bewitches her. He grinds a paste with honey of the ब्रह्मदण्डी, वाचा and patra plants and applies it on his member. His woman will never seek another man.

The aids of the goddesses:

[रोचना गन्ध पुष्पाणि निम्ब पुष्पं प्रियङ्गवः ।]{style="color:rgb(51,204,0);"}\
[कुङ्कमं चन्दनं चैव तिलकेन जगद् वशेत् ।]{style="color:rgb(51,204,0);"}\
[ॐ ह्रीं गौरी देवी सौभाग्यं पुत्रवशादि देहि मे स्वाहा ॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}\
[ॐ ह्रीं लक्ष्मी देवी सौभाग्यं सर्वं त्रैलोक्यमोहनम् स्वाहा ॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

One applies the paste of fragrant rochana flowers, neem flowers and प्रियङ्गु grains along with chandana and vermillion on ones fore head. One then makes oblations respectively to गौरी and लक्ष्मी for the sake of children and bewitching the 3 worlds.

[ॐ रक्तचामुण्डे [अमुकं] मे वशमानय आनय ॐ ह्रीं ह्रूं ह्रः फट् । ।]{style="color:rgb(255,102,0);font-weight:bold;font-style:italic;"}\
By applying a tilaka with her menstrual blood and gorochana a woman can subjugate any target by meditating on the above formula.

There is also a preparation for a woman who has even had several children to look like virgin. But this is garbled and needs further reconstruction.\
[\
ॐ नमः खड्ग-वज्रपाणये महा-यक्ष-सेनापतये स्वाहा । ॐ रुद्रं ह्रां ह्रीं वरशक्ता त्वरिता-विद्या । ॐ मातरः स्तम्भयत स्वाहा ॥]{style="font-weight:bold;color:rgb(255,0,0);font-style:italic;"}\
By meditating on the above formulae on the lord of the यक्ष army, rudra and the goddess त्वरिता one wards of thieves sneaking into one's domain.


