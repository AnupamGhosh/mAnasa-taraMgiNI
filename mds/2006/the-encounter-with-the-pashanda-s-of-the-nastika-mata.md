
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The encounter with the पाषण्ड-s of the nAstIka mata](https://manasataramgini.wordpress.com/2006/10/05/the-encounter-with-the-pashanda-s-of-the-nastika-mata/){rel="bookmark"} {#the-encounter-with-the-पषणड-s-of-the-nastika-mata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/05/the-encounter-with-the-pashanda-s-of-the-nastika-mata/ "5:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

With the due assent I decided to narrate the account of [**R1**](http://manollasa.blogspot.com/2006/07/some-notes-of-shatsahasra-tradition.html)'s father encounter with the vile पाषण्ड-s of the nAstIka mata- this is an illustrative example of some technicalities for the complete उच्छाटन of पषण्ड mantras. The other learned वृद्धा had also demolished the prayoga-s of such a पाषण्ड from the vanga country. The old R1F had journeyed deep into the were the practitioners of the fallen ways of महाचीनाचार and उपचीनाचार still wandered. In their midst he saw their mantra-s that are are thefts from savants the first वर्ण or gifts from patita-सावित्र-s of the bygone eras. Over the ages these चीनाचार्यस् and other पाषण्ड-s who practice their vile para-tantras have mellowed down as though due to the delusion of नारायण.

When the R1F in his younger days was on a journey to see the temple chakra-नारायण at the foot of the hima-parvata and then to gain the grace of श्री-प्रत्यङ्गिरा he came into conflict with a nAstIka पाषण्ड. The nAstIka's line of prayoga-s was thus:

  - He first defended himself with rites to mahA-प्रत्यङ्गिरा देवी and then invoked महाप्रतिसरा and her परिवार to protect him from any incoming attack (knowers of the मन्त्रास्त्र vidya know that this is a difficult configuration to pierce).

  - Then for his first strike at his ब्राह्मण foe he invoked चण्डरोषण, performing a dhyAna of him binding विष्णु, rudra and brahma with a pAsha.

  - Then he invoked the महाविद्या of the most terrifying vajra-ज्वालानलार्क performing a dhyAna conceiving विष्णु crushed under his left foot and लक्ष्मी under his right foot.

  - Then he invoked the awful deity trailokyavijaya by meditating on him placing his left foot on the head of shiva and with his right foot crushing the breasts of गौरी. To him he made oblation of rakta and then prepared a skull for burial with the name of R1F inscribed on it.

  - Then he escalated the attack further by performing a homa for परमाश्व imagining him crushing under his four feet indra, विष्णु, इन्द्राणी and लक्ष्मी. He also tried to cause धनक्षय to R1F by deploying the उच्छुष्म jhambhala prayoga.

A normal tantric, or a even a well-educated chera magician could be easily blown to smithereens by such a series of prayoga-s and his counter-prayoga-s may not even reach the attacker. Our female vairini of old days, Ktk, used to dable in these विद्या-s of चीनाचार. But R1F was fully ready for the occassion. He did face some problems due to the vairi prayoga-s that alerted him. He suffered धनक्षय and from severe ill-health putting him a difficult situation, but due to contacts in the Indian embassy he managed to hold out. Then with the help of a स्मार्त in the great temple of pashupati he got his act together. We, for reasons obvious to those in the know, cannot describe all the details of his line of action. He went to a silent southern corner of the great shrine of pashupati and began his rites:

  - He first performed a dUra-दृष्टि prayoga with the help of two emanations from the पञ्च-brahma mantras (ekarudra and siddheshvara) and figured out all the prayoga-s used by the nAstIka. He also noticed that the पाषण्ड was monitoring his movements with his own दूरदृष्टि prayoga. R1F quickly performed the मार्ताण्ड-bhairava prayoga and destroyed the bauddha's dUra-दृष्टि.

  - He then pierced the nAstika's protective shield by performing a homa with the संपुटीकरण of:\
नृसिम्ह-हयग्रीव + sudarshana विद्या + अष्टाक्षरी + हयग्रीवास्त्र + aghora-rudra + अघोरास्त्र

  - With the bauddha exposed he despatched at him the most terrifying अष्ट-mukha-गण्डभेरुण्ड ugra-नृसिंह prayoga, with the dhyAna on the नृसिम्ह with 20 hands, having 8 leonine heads and 2 dinosaur-like heads. This completely destroyed all the पाषण्ड-s prayoga-s.

  - Through his दूरदृष्टि he saw that the पाषण्ड was readying for a मारण strike with the स्वादिष्ठान-rakta-यमारी prayoga. He simply back-hurled it by deploying the secret प्रत्यङ्गिरा विद्या with a संपुटीकरण of the विद्या of our hallowed goddess ghora-rudra-कुब्जिका. The पाषण्द who was going to fill a pot in the kaushiki river was borne to the abode of vaivasvata.


