
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The hand of fate](https://manasataramgini.wordpress.com/2006/08/29/the-hand-of-fate/){rel="bookmark"} {#the-hand-of-fate .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 29, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/29/the-hand-of-fate/ "1:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After much struggle our forces fought their way to a strong position on the front of the द्वयं दण्डकाः. We were close to taking this much desired and important outpost after a really long drawn conflict. But, just then the news reached us on the खाण्डवन् front that something we always suspected from the fateful day of Nov 30, 2004 was true. It put us in an even more complicated mess on that front. What struck us in [hindsight ]{style="font-style:italic;"}was that how despite knowing everything fairly accurately before hand we kept committing mistake after mistake in our actions. We could today clearly see what the Hindus call the hand of fate dragging us into the morass, despite us having seen the bog from a over a mile away. It taught us that foresight and rational decisiveness does necessarily translate into the right path if the flow of fate is against it.


