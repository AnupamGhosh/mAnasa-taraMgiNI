
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पस्चिमाम्नायेश्वरी and shiva of the शिखा](https://manasataramgini.wordpress.com/2006/12/05/paschimamnayeshvari-and-shiva-of-the-shikha/){rel="bookmark"} {#पसचममनयशवर-and-shiva-of-the-शख .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 5, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/05/paschimamnayeshvari-and-shiva-of-the-shikha/ "6:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We first salute the master of the school, matsyendra and कुञ्कुमांबा who was his कुलाङ्गना without whom none of these mysteries would be known to us mortals.\
"[सिद्धमाता विभुः शब्दराशीति योन्यार्णवी वाग्विशुद्धासि वागेश्वरी]{style="color:#99cc00;"}"\
That provides the inspiration of the rudra-shaktI -- महासेनसम्भोगिनी. The one with pretty curves, who is the queen under the chinchini tree, manifests simultaneously in all her glory in the great ओड्डियान, जालन्धर, पूर्णगिरि and kAmarUpa. The very name कुब्जीशान causes the उल्लास that cannot be described in words, the leading steps on the kula path. Even a moment of revelation of कुब्जीशान is just like the drinking of the तीव्र soma, sweeted for the sacrificial rite. He is the glorious नवात्मन् like a thunder cloud, the wielder of the पिनाक bow. On his lap, like streak of brilliant lightning illuminating that roaring thunder cloud is our goddess, पस्चिमाम्नायेश्वरी, the queen of the समयविद्या. The one who can recite the दण्डकं recalls the words ["]{style="color:#ff0000;"}[त्वं नवात्मानदेवस्य चोत्सङ्गयानाश्रिता]{style="font-style:italic;color:#ff0000;"}["]{style="color:#ff0000;"}.\
From the 32 syllabled समयविद्या's शिखा mantra emanates स्वच्छन्द, who is the manifestation of the tantric aghora mantra. One who has stilled his mind for the expansion of the mighty समयविद्या sees a preta मञ्च. On that is seated lokeshvara, the mighty yogin bearing the universe in him. On him emerges a bright blue lotus. In the pericarp of that lotus is seen the terrific serpentine ananta, blazing forth with all-conquering power of heat and other radiations. He is the deadly कालसंकर्षण, armed with a musala and हलायुध. On his शिखा is seen an eight-petalled lotus. In that is the powerful shiva of the शिखा or स्वच्छन्द. From him emerge 32 brilliant मयूखस्, which are the energies of the samaya-विद्या. He is adorned by ornaments of various colors. He has 4 heads and 10 hands and inspires extreme terror. At the same time he is imbrued with the शृन्गार rasa and tightly hugs a देवी of dark color, the great दूती, with ornaments of all colors. In her manifests the power of the वषट्.

[तवाधारे मूले सह समयया लास्यपरया\
नवात्मानं मन्ये नव-रस-महाताण्डव-नटम् ।\
उभाभ्याम्-एताभ्याम्-उदय-विधिम्-उद्दिश्य दयया\
सनाथाभ्यां जज्ञे जनक-जननी-मज्-जगदिदम् ॥]{style="color:#99cc00;"} (SL 41)

In your मूलाधार (of कुलसुन्दरी) I meditate upon नवात्मन् dancing the great ताण्डव, along with समया performing her लास्य. This world came to own these two as generator and generatrix due to their grace for the act of the emergence of the world.

The great समयविद्या:\
[नमो भगवति श्री कुब्जिकायै ह्रां ह्रीं ह्रों ञ ङ ण न मे अघोरमुखि चां चीं किणि किणि विच्चे ॥]{style="color:#99cc00;"}


