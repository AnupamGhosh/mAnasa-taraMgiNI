
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The gAtha on the founder of our nation](https://manasataramgini.wordpress.com/2006/07/09/the-gatha-on-the-founder-of-our-nation/){rel="bookmark"} {#the-gatha-on-the-founder-of-our-nation .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 9, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/09/the-gatha-on-the-founder-of-our-nation/ "4:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[हिरण्येन पारिव्]{style="font-style:italic;color:rgb(255,153,102);"}[R\^i]{style="font-style:italic;color:rgb(255,153,102);"}[तान् कृष्णाञ् छुक्लदतो म्]{style="font-style:italic;color:rgb(255,153,102);"}[R\^i]{style="font-style:italic;color:rgb(255,153,102);"}[गान् ।]{style="font-style:italic;color:rgb(255,153,102);"}\
[मष्णारे भरतो ऽददाच् छतम् बद्वानि सप्त च ॥१॥]{style="font-style:italic;color:rgb(255,153,102);"}\
Cased in gold, black beasts with white tusks, at मष्णार bharata gave by the herds, seven hundred of them

[भरतस्यैष दौःषन्तेर् अग्निः साचीगुणे चितः ।]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
[यस्मिन् सहस्रम् ब्राह्मणा बद्वशो गा विभेजिरे ॥ २॥]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
This is the fire-alter of bharata दौःषन्ति piled in साचिगुण, at which 1000 brahmins shared herds of cattle.

[अष्टा-सप्ततिम् भरतो दौःषन्तिर् यमुनाम् अनु ।]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
[गङ्गायां v]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}[R\^i]{style="font-style:italic;color:rgb(255,153,102);"}[त्रघ्ने ऽबध्नात् पञ्च-पञ्चाशतं हयान् ॥ ३॥]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
Seventy eight \[horses] bharata दौःषन्ति gave at yamuna, on the गङ्ग for the slayer of वृत्र (indra) he tied 55 horses

[त्रयस्-त्रिंशच्छतं राजाश्वान् बद्ध्वाय मेध्यान् ।]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
[दौःषन्तिर् अत्यगाद् राज्ञो ऽमायान् मायवत्तरः ॥ ४॥]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
133 horses the king tied for the ashvamedha rite, दौःषन्ति surpassed other rulers, in power he is the most powerful.

[महाकर्म भरतस्य न पूर्वे नापरे जनाः ।]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
[दिवम् मर्त्य इव हस्ताभ्यां नोदापुः पञ्च मानवा ॥ ५॥]{style="font-style:italic;color:rgb(255,153,102);font-family:arial;"}\
The great acts of bharata neither men before nor after, as mortal the sky with his hands, the five peoples have not can achieved.

Thus, did दीर्घतमा the son of ममता proclaim the founder of our nation with the great abhisheka of indra, the supreme royal coronation.


