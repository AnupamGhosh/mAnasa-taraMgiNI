
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Prestosuchus](https://manasataramgini.wordpress.com/2006/01/28/prestosuchus/){rel="bookmark"} {#prestosuchus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 28, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/28/prestosuchus/ "7:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/pbp.gif){width="75%"}
```{=latex}
\end{center}
```

{target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/hello/133/1300/400/prestosuchus.jpg){width="75%"}
```{=latex}
\end{center}
```



The Prestosuchids appear to be a sister group to the proper rauisuchians, branching off after the ornithosuchians. They were top carnivores in the Gondwana regions. But the phylogenetic relationships of the suchian archosaurs remain far less understood than the ornithodiran clade. Prestosuchus from south America has a skull (above) that is a meter long and might have been upto 5 meters in length preying vigorously on Triassic synapsids, dinosaurs and other reptiles.


