
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The three-headed one](https://manasataramgini.wordpress.com/2006/01/25/the-three-headed-one/){rel="bookmark"} {#the-three-headed-one .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 25, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/25/the-three-headed-one/ "5:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

oM drAM दत्तात्रेय नमः\
[oM aiM क्रों क्लीं cलूं ह्रां ह्रीं ह्रूं सौः दत्तात्रेयाय स्वाहा ||]{style="font-weight:bold;font-style:italic;"}\
The three-headed one, also known as the terrifying trishiro-bhairava, wandered around carefree in the great forests of himaparvatas. With one of his arms was holding the most beautiful madhumati of sweet smiles in an embrace around her gold-girt slender waist. In his other hands he held a mace, a trident, the blazing sudarshana, and a conch. He was of fair complexion, youthful and energetic with ever-smiling faces. The delightful madhumati would occassionally cast her arms around him, like a jasmine creeper winding around a full-grown khadira tree, and she would bring her face close to his. Their eyes would meet, exchanging glances of the highest bliss. The great trimukha would hold her face with his hand, and it would seem like a lotus sprouting from a pond under the pleasant spring sun. In her hands, madhumati held a chalice of sweet maireya liqour, which she would playfully quaff. Occasionally she would pass the chalice full of mirth to trishiras and they would quaff together in eternal Ananda. The bIja eM was constantly resonating around madhumati even as she with her dazzling beauty concealed the three-headed one from the gaze of on lookers. They might stop in a secluded slyvan grotto and engage in कामक्रीड, नृत्य and अट्टहास. In the highest ecstasy of pleasure the mighty त्र्यानन offered madhumati the premaratna -- the gem of love, and having nothing greater to offer in return, madhumati, full of rasa, matched it by offering her own breasts. The incomparable त्रिमूर्ती clasping her breasts in his hands, gently placed a camphor flavored betel leaf between her pearl-like teeth. Then sporting with nails, polished like the mother-of-sea-pearl, he tickled the three folds in her mid-riff. Embraced by madhumati the trideva looked like the peaks of the Himalayas girt by snow and pine forests. The बीजस् AM, ह्रीं, क्रों, क्लीं, huM and oM filled the air. They spread the mAyA.

The great रामो भार्गव wandered there holding and axe that had been washed in the blood of the वीतिहोत्रस् over and over again. He could not have a glimpse of the trideva because of madhumati. She shrouded दत्तात्रेय like a mass of clouds blocking the vision of the high peak of कैलाश parvata. Through his intense meditation and grace of the deity the भार्गव could finally attain दत्तात्रेय and madhumati. They gave him audience even as they quaffed the maireya liquor. trimukha first showed him the yoni mudra and then the ज्वालिनि. The three-headed one was surrounded by 4 dogs. The भार्गव declared his familiarity with them and asked: "Oh Noble one what is the rahasya of the agni chaitanya?" The lover of madhumati said: "From raM it emanates. The splendid वागीश्वरी having taken a purifying bath after her menstrual flow is lying in joyous abandon on a soft couch. prajApati full of शृङ्गार rasa approaches her who is the embodiment of beauty and engages in passionate intercourse. Into her yoni he deposits his retas. That retas is vahni which endowed with the chaitanya of raM. He salutes that with the formula:\
[ह्रूं वःनि छैतन्याय नमः]{style="font-style:italic;color:#ff0000;font-weight:bold;"}\
He then offers आचमनं to the great ईश्वरी of vak. Then the great वैश्वानर shoots up as he utters:\
[चित्-पिङ्गल हन हन दह दह पच पच सर्वज्ञाज्ञापय स्वाहा]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
In the former days O rAma your illustrious ancestor, the foremost of the atharvans, भृगु saw many mantras. But this one which the primordial भृगु saw, who himself has sprung from the retas of prajApati, is the base of the new kalpa that is being enunciated. The dull-witted cannot grasp it:\
oM वाइश्वानर इहावह लोहिताक्ष सर्वकर्माणि साधय स्वाहा\
It is with it the act of sacrifice is achieved. He is then protected by the emanations of the 7 बीजस्:\
[स्र्यूं । श्र्यूं । ष्र्यूं । व्र्यूं । ळृर्यूं ।ऋर्यूं ।य्र्यूं ॥]{style="font-weight:bold;color:#ff0000;"}


