
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Erketu and the titanosaurs](https://manasataramgini.wordpress.com/2006/03/21/erketu-and-the-titanosaurs/){rel="bookmark"} {#erketu-and-the-titanosaurs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 21, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/21/erketu-and-the-titanosaurs/ "6:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The titanosaurs were the last great radiation of the sauropods. Earlier it was thought that the era of the sauropods was gone with the Jurassic. But over the past two decades the finds of the titanosaurs, show that this lineage sauropods attained considerable diversity and nearly world-wide spread through the Cretaceous. We had long-necked forms resembling the diplodocids of the Jurassic, like [Rapetosaurus ]{style="font-style:italic;"}and [Nemegtosaurus]{style="font-style:italic;"}. Then there were forms like [Argentinosaurus ]{style="font-style:italic;"}and [Paralititan ]{style="font-style:italic;"}which were the largest land animals that have ever walked on earth. There were reasonably shorter-necked forms like [Isisaurus ]{style="font-style:italic;"}from India and [Mendozasaurus ]{style="font-style:italic;"}which appears to have been dominant in the Andean zone. South America also had the enigmatic armored sauropods like [Saltasaurus, ]{style="font-style:italic;"}[Laplatasaurus]{style="font-style:italic;"} and perhaps [Agustinia]{style="font-style:italic;"}, which might have provided it protection against the mighty predators of the land like [Giganotosaurus ]{style="font-style:italic;"}and the abelisaurs. There was one form [Bonitasaura ]{style="font-style:italic;"}that evolved a beak convergent to the ornithopods. In this background the most recent titanosaurian addition is *Erketu ellisoni*, from Mongolia. This dinosaur with extremely extended cervicals is supposed to have had a estimated neck of around 8 meters. The sauropods for all their size are extremely enigmatic products of bioengineering. We do not understand much about how they got about life in the first place. The 8 meter neck like a hose-pipe might have allowed it to clean up plants over a wide area for certain, but how it managed with a neck nearly parallel to the horizontal and this long is not easily imagined- yet these animals lived and thrived at that.


