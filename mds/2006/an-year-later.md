
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An year later](https://manasataramgini.wordpress.com/2006/04/02/an-year-later/){rel="bookmark"} {#an-year-later .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 2, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/02/an-year-later/ "1:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The कृत्या had hit precisely an year back. We knew that the anniversary strike was in the making. Around 2.30 PM the कृत्या was seen again; as before, it rapidly breached all the kavachas and burnt its way through. It was like the classical प्रत्यङ्गिरस कृत्या described in the literature. The "Ajya तर्पणं" held it off temporarily. We were reviewing the indra जाल 2 front with our general when she struck again. This time we deployed "कलायसूप" against it but that did not work and it seized us violently. We had to finally use the "Asandi" to drive her away. We thought we had her under check, when she suddenly returned and could not be quelled by by "निम्बूक" or "mAsha". She raged for five hours and the धनाकर्शण bhairava spell was repulsed. Then finally with the गणनाथ a temporary control was effected.


