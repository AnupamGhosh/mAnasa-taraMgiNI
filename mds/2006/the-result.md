
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The result](https://manasataramgini.wordpress.com/2006/05/28/the-result/){rel="bookmark"} {#the-result .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 28, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/05/28/the-result/ "9:44 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

कृषमुख and परषुमुख organized their attack at us with the deep hate and hope of overthrowing us for good-- the महोच्छाटनं. We were enveloped by the कृत्या-s of धूमावती that they had sent. Our सेनानी had warned us well of what was approaching. We paused for a moment and decided that we must break up कृषमुख and परषुमुख as well as their spells as we had done to यूप-shiras and matsya-पुcछ-shiras in that great battle of द्वादश. In every respect we were having a [deja vu]{style="font-style:italic;"} with respect to that old battle. We laid the circle of jamadagni's daughter's रङ्कुटि ([Vicia tetrasperma]{style="font-style:italic;"}), in the middle we laid the अयोकण ([Polygonum convolvulus]{style="font-style:italic;"}) and deployed the निरृति-dispelling spell of our patriarchs, the भृगुस् of yore. When the words were uttered:[ "Seek the one who does not make offering nor rites; the may you follow the road of the thief and dacoit; seek that other man than us, that is your path; salutations be you, O goddess निरृति."\
]{style="font-style:italic;color:rgb(51,255,51);"}\
The कृत्यास् instantlly rebounded off us like a ball striking a hard flat wall. We then let fly that which was fashioned by उशना काव्य and kabandha आथर्वण, for it is said in the texts that the great astra is infallible as the spell of the atharvans of yore. The brahman of kabandha आथर्वण, known as the नैर्बाध्य, went forth:\
["Away from his home do I drive that fellow yonder, who as a भ्रातृव्य contends with us; through the oblation devoted to overpowering \[of foes] indra has smashed him to pieces."]{style="font-style:italic;color:rgb(51,255,51);"}

That brahman of the illustrious काव्य, known as the कृत्या परिहरणं, went forth as it went to kill the इक्ष्वाकु daNDa:\
["O agni, the destroyer in the war \[with foes], O win the battle; with the प्रतिहरणं we back-hurl the कृत्या upon sender."]{style="font-style:italic;color:rgb(51,255,51);"}

Thus, we invoked the dasyu-killing, the वृत्र-slaying, fort-shattering ones, as भृगु had done, as aurva had, as rAma had, and as kabandha आथर्वण.\
"अमन्यमानान् चर्वा जघान"

कृषमुख and परषुमुख were overthrown themselves and with that ended a major chapter, as the chapter of द्वादशान्त was ended.\
[]{style="font-style:italic;color:rgb(51,255,51);"}


