
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [siddhAnta tantrics and the mainstream brahminical path](https://manasataramgini.wordpress.com/2006/06/23/siddhanta-tantrics-and-the-mainstream-brahminical-path/){rel="bookmark"} {#siddhanta-tantrics-and-the-mainstream-brahminical-path .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 23, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/23/siddhanta-tantrics-and-the-mainstream-brahminical-path/ "5:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I have yarned a lot on this topic from different angles: [1) The shaivas: the पाशुपत-s](http://manollasa.blogspot.com/2006/05/shaivas-pashupata-s.html) [\
3)]{#114724324346937706} [dvijas and non-dvijas in the shaiva cults](http://manollasa.blogspot.com/2005/03/dvijas-and-non-dvijas-in-shaiva-cults.html) [2)]{#114724324346937706} [The कालामुखस्-I](http://manollasa.blogspot.com/2005/02/kalamukhas-i.html) [](http://manollasa.blogspot.com/2005/02/kalamukhas-i.html){#114724324346937706}[4)]{#114724324346937706} [कालामुखस्-II](http://manollasa.blogspot.com/2005/03/kalamukhas-ii.html)

But I was brought back to it because ST happened to introduce me to a confused brahmin woman who declared that her Dravidian guru had told her that the siddhanta shaivas had dispensed with the "brahminical strictures" and made their mata "more accessible" to all. This is not an isolated opinion because we see precisely this view in the opinions of several modern Dravidians who claim affiliation with the siddhAnta tantric tradition, apparently without really understanding the tradition too well. In fact nothing is farther from the truth in this regard when one approaches the original siddhAnta school. While शूद्रस् like मूर्तिगण the successor of varmashiva and teacher of brahmin इशानशिव held prominent places in the siddhAnta tradition, it did not mean that there was anykind of revolt against brahminical tradition, even if it was somewhat atypical.

In fact it must be pointed out that the siddhAnta shaivas repeatedly cite the authority of the now apparently lost uttara saMhitA of भार्गव thus:\
[इति वर्णाश्रमाचारान् मनसापि न लञ्घयेत् । यो यस्मिन्न् आश्रमे तिष्ठन् दीक्षितः शिवशासने । स तस्मिन्न् एव सम्तिष्ठेच् छिवधर्मं cअ पालयेत् ॥]{style="font-style:italic;color:rgb(255,153,0);"}\
"He should not even think of transgressing the varNAshrama practices. He should stand within (maintain) the order he was in when he attained shaiva initiation and at the same time maintain the practices of the shaiva path."

So the siddhAnta shaiva did not forsake his Ashrama or वर्ण when he attained दीक्ष into the shaiva path and was not a means of "revolting against" or "escaping" the ordinances of the Aryas. The important point to note was that the classical siddhAnta shaivas did not force a sectarian shaiva path on their general audience of followers. An examination of what survives of their extensively codified "dharma शास्त्र" the shivadharma and shivadharmottara (alluded in the above quoted from the भार्गवोत्तर as shivadharmam), which have not been edited and studied unlike the विष्णु dharmottara shows that the general followers were encouraged to maintain their existing life-style/profession and वर्ण-related observances, while accomodating the shaiva observances with that framework. In fact the basic dharma-शास्त्रिc injunctions are relating to other nitya issues are reiterated by the shiva-dharma. Secondly the siddhAnta tantric tradition did retain the पौराणिc hindu pantheon despite being sectarian shaiva-s. In fact their maintained a set of manuals termed the प्रतिष्ठ tantras for the installation and temple worship of the remaining Hindu patheon including कुमार, विनायक, विष्णु, brahma, शक्तीस् of various gods, मातृकास्, vedic gods reduced to lokapAla-s, planets and गणस्. These texts include the obscure manuscripts of पिङ्गलमत, देव्यामत, the collection of the demon maya and the किरण Agama. The atharvavedic shaivas also performed the whole series of अभिचार and counter अभिचार rites for rulers as per vedic injunctions. Thus, there is no evidence that the classical siddhAnta tantras were ever set up as a counter-current to the mainstream brahminical path.\
[\
]{#114724324346937706}


