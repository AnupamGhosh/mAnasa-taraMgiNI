
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [11 dances of the gods.](https://manasataramgini.wordpress.com/2006/10/01/11-dances-of-the-gods/){rel="bookmark"} {#dances-of-the-gods. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/10/01/11-dances-of-the-gods/ "11:22 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The सिलपधिकारं (6.44), the great champu written in the द्रमिड language, mentions 11 dances that, as far as I know, have no mention in the literature in deva-भाषा. We do not know if these were once prevalent over jambu-द्वीप and later became extinct. While many of the themes are clear it is hard to understand all the details from the archaic द्रमिड. Dixitar's translation clarifies most of them pretty well.

 1.  The कोडुकोट्टि: It was danced by shiva, with उमा keeping the tAla beside him, in the grave yard where भारती had danced with faultless rhythm and avoiding wrong tAla-s, when the great fire-tipped arrow had obeyed his command to destroy the tripura at the request of the devas.

 2.  The पाण्डरन्ग- This dance was done by shiva in the form of भारती (!) before the 4 faced brahma who was driving his car.

 3.  The alliyam- This was performed by the collyrium colored विष्णु after exposing the evil devices of कंस.

 4.  The mallu- This was performed by विष्णु after cutting the hands of बाणासुर.

 5.  The तुडि- this was performed by कुमार in the middle of the ocean after he killed the demon शूरपद्म, who was hiding in the middle of the ocean. \[As far as I know the killing of this demon termed शूरपद्म is a exclusively Dravidian development of the कुमार cycle. In all देवभाष AkhyAna-s, except recent ones back-translated from द्रमिळ, only तारक and not शूरपद्म is mentioned]

 6.  The कुडै- This was performed by कुमार after lowering the umbrella before the danavas routed by him in battle. [द्रमिड commentators state that कुमार shielded himself with his umbrella and danced in derision at having pounded तारक and his hordes.]

 7.  The कुडं- विष्णु danced this after striding through the city of बाणासुर.

 8.  The pEDi- काम turning into a hermaphrodite form is supposed to have danced this.

 9.  मरक्काल्- This was the dance of विष्णुमाया (महाकाली) when she sought to suppress the evil दानवस्.

 10.  पावै- This was the dance displayed by लक्ष्ंई when she faced the demons.

 11.  कडयं- This is the dance of इन्द्राणी in the fields outside the northern gates of the city.


