
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [माध्यमिका the average भारतीय नारी](https://manasataramgini.wordpress.com/2006/03/26/madhyamika-the-average-bharatiya-nari/){rel="bookmark"} {#मधयमक-the-average-भरतय-नर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/26/madhyamika-the-average-bharatiya-nari/ "1:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/average_nari4.jpg){width="75%"}
```{=latex}
\end{center}
```



[Three views of माध्यमिका with slightly different corrections for noise]{style="font-weight:bold;"}

Finally, we did get to the average भारतीय नारी business because ST maintained a database of our classmate's photos from a certain period in a quaint little thing called the autograph book. Since it might have looked inappropriate to prying parents or their spies in those halcyon days, ST sadly only had the mugs of the women. Initially, रामनुजवादिन्, K and the Sikh objected that this database may not be appropriate because there is a bias towards feminine ugliness in the "Science Stream" i.e those taking a combination of physics-chemistry-biology-mathematics. The ladies (ST and R) were furious and did not want to give us the database on hearing this. But we pacified them they were उत्तमरुपा-s amongst भारतीय नारी-s, without equals in the class (we weeded out some but are afraid to mention the reasons -- luckily R and ST happily participated). We were left with 16, which included the whole range from "style-मामीस्", the true सुरूपा-s, and some in my opinion borderline or even tending to दुर्मुखी. Then we pitched them into SQUIRLZ MORPH and out came the माध्य्मिका the average भारतीय नारी. All the guys said: "not bad". The ladies seemed to agree but felt they looked better. Finally, a subset of us are saffron Hindoos- so we asked how can the नारी go without a tilaka and accordingly gave माध्य्मिका one.

Below is the average using only 6 स्त्री-s of ब्राह्मण descent (1 from Tamil Nad, 2 Maharastra, 1 Himachal, 1 Uttar Pradesh, 1 Gujarat- pretty diverse sample).

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/brahmana_narI3.jpg){width="75%"}
```{=latex}
\end{center}
```




