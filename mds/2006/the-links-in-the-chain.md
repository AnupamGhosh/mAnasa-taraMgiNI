
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The links in the chain](https://manasataramgini.wordpress.com/2006/08/02/the-links-in-the-chain/){rel="bookmark"} {#the-links-in-the-chain .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 2, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/02/the-links-in-the-chain/ "5:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our ancestors went to the great asura medhira when they were in such a state. But this time we went to deva indra. We extolled the great वृत्रहन्: you who gave the yati-s to hyaenas, you who smashed the forts of the दासस्, you who slaughtered the arurmaghas, you who extirpated प्रह्लाद and the pulomas, we offer sacrifice to you. We wish to know what happended after we moved beyond the battle पाषण्द parvata. maghavan said you shall know thus:

The first of the battles actually occurred on वानर parvata, but that was an old story. If you wished to know about it ask your priya सखी about the "cool summer afternoon". The battle between durga parvata and पाषण्ड parvata was definitely hard fought. But you were forced to go beyond the town of pot-graves after that battle. After battle that was fought beyond the city of pot-graves, the अमात्य and शचीव thought they were seeing victory and wanted to press hard. But I bade you to stop near the cow-stall. There when you dismounted the ratha you saw the elusive Mercury close to the horizon. Then you composed this [ditty](http://manollasa.blogspot.com/2003/10/guillotines-fall.html). I made sure you came out on the otherside of the blade. But then I measure out things in different measures for all. The balance goes up and down. So I took 2 wicks, and showed them to the loka of मानुषस्. You मानुषस् however saw 8 wicks when there were only 2. Hence, they call me मायिन्- the wielder of इन्द्रजाल. I asked you all to chose one. You and those who lived beyond the pot-graves, and the खाण्डवन्स् chose one, while those at the foot of पाषण्ड's hill chose another. You all thought you had chosen different wicks. But to those who offered me sacrifices I did not yield up to मृत्यु right away, even though she was seeking them. Those who lived beyond the pot graves did not know how to perform याग-s as you did. But they offered the gods whatever they could. I showed to the आमात्य and शचीव that they lived firmly by dharma, despite being un-educated and not worth the thread slung on their shoulders. Hence, while the visitation of मृत्यु could not be avoided by them, we gave them the new twig. The dwellers of पाषण्ड hill chose the wick well and so they enjoyed its benefits. But they were limited by their own karma. That man whom you saw sitting in the midst of bones and stones had in times before chosen that wick and was rewarded with eko-मानुश-आनन्दः.

As those who lived beyond the pot-graves lived firmly by dharma, like the poor man in कुरुक्षेत्र, and was favored by the devas, we wished them good.

....\
Then ST told me of the rAkShasa dwelling in asi-parvata, where the muni and me had wandered in our days of youth. She said I wanted to tell you to proceed to asi-parvata but on its foot sits a rAkShasa, on account of which I have lapsed to in sorrow. Transmitting signal. "2 phantoms. 1 silver. 2 gold. gold sought. but phantom. take gold, trivikrama's daughter. Mn poked by crane again. आमात्य saves him with paste."


