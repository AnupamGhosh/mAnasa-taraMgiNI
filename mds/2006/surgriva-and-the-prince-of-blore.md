
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सुग्रीव and the prince of B'lore](https://manasataramgini.wordpress.com/2006/06/11/surgriva-and-the-prince-of-blore/){rel="bookmark"} {#सगरव-and-the-prince-of-blore .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 11, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/11/surgriva-and-the-prince-of-blore/ "2:39 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/dravida.jpg){width="75%"}
```{=latex}
\end{center}
```



The above illustrates the difference between the prince of B'lore (left) and सुग्रीव so strikingly.\
सुग्रीव:\
[Behind of wicket off=32.2% total=61.1%]{style="font-family:courier new;"}\
[front of wicket off =28.9%/]{style="font-family:courier new;"}

[front of the wicket =50.1% ]{style="font-family:courier new;"}\
prince of B'lore:\
[Behind of wicket off=15.8%total=44.2\
]{style="font-family:courier new;"}[front of wicket off =28.4%/\
]{style="font-family:courier new;"}[front of the wicket =55.2%]{style="font-family:courier new;"}

[What it shows is that सुग्रीव's ]{style="font-family:arial;"}[is]{style="font-family:arial;"}[[ success if he manages to slash away behind the wicket on the off without being pouched. This was precisely the area where we were forbidden to slash as youngsters learning the correct style :-) So सुग्रीव is truely special in being able to exploit this danger zone to perfection.]{style="font-family:arial;"}\
]{style="font-family:courier new;"}


