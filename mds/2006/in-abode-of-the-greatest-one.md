
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In abode of the greatest one](https://manasataramgini.wordpress.com/2006/01/26/in-abode-of-the-greatest-one/){rel="bookmark"} {#in-abode-of-the-greatest-one .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 26, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/26/in-abode-of-the-greatest-one/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[विश्वस्माद् इन्द्र उत्तरः ।]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
indra is greater than all!

[]{style="color:rgb(0, 0, 0);"}विश्वामित्रो ha वा indrasya प्रियं धामोप्जगाम शस्त्रेण cha व्रतचर्यया |\
विश्वामित्र indeed went to the dear abode of indra due to his vedic compositions and rites.

taM hendra उवाच विश्वामित्र वरं वृणीष्वेति |\
To him indra said, "विश्वामित्र, choose a boon".

sa होवाच विश्वामित्रस् त्वां eva vijAIyAM iti |\
विश्वामित्र said: "let me know you".

द्वितीयं iti | त्वाम् eveti | तृतीयं iti | त्वां eveti |\
i: \[choose] the second time. v: You only. i: \[choose] the third time. v: "You only"

taM hendra uvacha महांश्च महती चास्मि |\
To him indra said: "I am the great (male) and the great (female)"

devashcha देवी चास्मि |\
I am the god and the goddess

brahma cha brAhmaNI चास्मीति |\
I am \[the] brahma power \[male] the female brahma power.

tata u ha विश्वामित्रो विजिज्ञासां eva chakre |\
विश्वामित्र verily still worked to know more.

taM hendra उवाcऐतद् वा अहं asmi yad etad अवोचं yad वा ऋषे .ato भूयो.अतपास् tadeva tat स्याद् yad अहं iti |

To him indra said: "I am that which I have said but what is more, he who does no tapas may \[know what I am].

tad वा indro व्याहृतीर् Uche tA उपा.a.अप्ता Asanniti |\
Then indeed did indra proclaim the व्याहृतिस्. They sufficed for him.

Indeed the chant of the व्याहृतिस् is : bhur-bhuva-स्वरों |\
They fully describe the universe. Hence the shruti states: "yó víshvasya pratimaánam babhuúva"- the universe is the image of indra.


