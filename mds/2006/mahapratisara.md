
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [महाप्रतिसरा](https://manasataramgini.wordpress.com/2006/04/04/mahapratisara/){rel="bookmark"} {#महपरतसर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 4, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/04/mahapratisara/ "5:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/mahapratisarA_frame.jpg){width="75%"}
```{=latex}
\end{center}
```



The emanation of प्रत्यङ्गिरा who may be specifically combined with ऋक्षकर्णि and कर्णमोटिनि is चतुर्मुखा महाप्रतिसरा. Her incantation may be used to tie a charm on the body. She may also be invoked as emerging from प्रत्यङ्गिरा and combined with the latter's mantras in certain rites.

महाप्रतिसरा prayoga vidhi

He shall first worship विनायक meditating on him embracing his shaktI मदनावती

oM sarva विघ्नानुत्सारय huM फट् स्वाहा ||

He shall take water in his water pot with:

oM vajrodake huM फट् ||

He shall wash his feet with the formula:

oM ह्रीं स्वाहा ||

He shall sip water (Achamana X 3) with:

oM ह्रीं suvishuddhadharma सर्वपापानि शाम्याशेष विकल्पानपनय स्वाहा ||

He shall knot a strand of hair with the formula:

oM manidhari वज्रिणि शिखरिणि sarva वशम्करिणि कं huM फट् स्वाहा ||

He shall sprinkle water in the ritual area with hand or using a tuft of darbha grass with the formula:

oM रक्ष रक्ष hUM फट् स्वाहा ||

\[If he is not doing this immediately after a bath he should necessarily do the pApa पुरुषविधि]

Then न्यासा shall be performed thus:

oM महाप्रतिसरायै अङ्गुष्ठाभ्यां नमः /

oM तारिण्यै तर्जनीभ्यां नमः /

oM वज्रोदकायई मध्यमाभ्यां नमः /

oM उग्रतारायई अनामिकाभ्यां नमः /

oM एकजटायै कनिष्ठिकाभ्यां नमः /

oM पिङ्गोग्रैकजटायै करतलकरपृष्ठाभ्यां नमः /

oM महाप्रतिसरायै हृदयाय नमः /

oM तारिण्यै shirase स्वाहा /

oM वज्रोदकायई शिखायै वषट् /

oM उग्रतारायई कवचाय huM /

oM एकजटायै नेत्रत्रयाय वौषट् /

oM पिङ्गोग्रैकजटायै अस्त्राय फट् /

The yantra shall be inscribed using black lamp dye on a square white cloth about two completely extended spans in lengths. The four circles of the yantra termed:

महामन्त्रानुधारणि, महासस्रप्रमर्धनि, महामन्त्रानुत्सारिनि and षष्टी will be daubed with yellow turmeric. The circle termed महाप्रतिसरा will be daubed with red kumkuma and in its center huM फट् will be written.

Once the yantra is ready the yoni mudra will be shown before it.

Then arghya is offered 3X with:

ह्रीं मणिधरि वज्रिणि महाप्रतिसरे इदं अर्घ्यं स्वाहा ||

He shall meditate upon महाप्रतिसरा emerging from the third eye of प्रत्यङ्गिरा. She will be mediated as having 4 heads with the front head being golden in color, the side heads red in color and the back head blue in color. She has firm breasts and is raimented in a coppery hued garment. Each of her heads bears a third eye. She bears in her eight arms the weapons: chakra, pAsha, खड्ग, त्रिशूल, vajra, इषु, धनुः and parashu. She is seated in perfect padmAsana

From her four third eyes then emerge: महामन्त्रानुधारणि, महासस्रप्रमर्धनि, महामन्त्रानुत्सारिनि and षष्टी. The first 3 deities are in padmAsana bearing 4 heads. षष्ठि has 6 heads and is seated on a मयूर.

He will then worship with नमः the 4 above deities:

oM महामन्त्रानुद्धरण्यै नमः (South)

oM महासहस्रप्रमर्धन्यै नमः (East)

oM महामन्त्रानुत्सारिण्यै नमः (West)

oM षष्ट्यै नमः (North)

In the 4 corners of the भूपूर square the following are worshiped (they emerge from the above deities):

oM काल्यै नमः

oM कालरात्र्यै नमः

oM कालकण्ठ्यै नमः

oM महायशायै नमः

In the 4 doors the following are worshiped (from the above emerge the following deities):

oM वज्राङ्कुश्यै नमः

oM वज्रपाश्यै नमः

oM वज्रस्फोट्यै नमः

oM vajraveshyai नमः

Then the yantra is tied up with the yellow twine with the formula:

oM मणिधरि वज्रिणि महाप्रतिसरे रक्ष रक्ष hUM फट् स्वाहा ||

Then japa is done with the mantra (she may alternatively be mediated as vajra पञ्जर भासिता महाप्रतिसरा with two hands holding a chakra and खड्ग and just one head):

oM मणिधरि वज्रिणि महाप्रतिसरे huM huM फट् फट् स्वाहा ||

The same formula is inserted into the terminal part of the प्रत्यञ्गिरा rite with several oblations made into the fire of ghee, oil or tila.


