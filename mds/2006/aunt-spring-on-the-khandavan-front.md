
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Aunt Spring on the खाण्डवन् front](https://manasataramgini.wordpress.com/2006/07/22/aunt-spring-on-the-khandavan-front/){rel="bookmark"} {#aunt-spring-on-the-खणडवन-front .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/22/aunt-spring-on-the-khandavan-front/ "11:29 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

With Jx liberated from the ग्रहण, and Mn having stabilized his ratha, and ज्येष्ट-tanu having recovered from his head-shattering blow, aunt spring wanted to move ahead with operations on our front. The muni, a stickler for etiquette, felt that Mn and aunt spring were not observing the appropriate Achara and were hence out of his purview. We interceded with the muni to tell him that the cloud of the attack had not passed and we need to hold together rather than break apart in intercine arguments. The virile तैत्तिरीयिन् was mellowed by the events, even as the brusque शचीव of Mn and Jx was mellowed by the frightening direct hit of the भाइरव arrow he had taken in kAshI. Aunt spring tried to move rapidly on खाण्डव, even without consulting us, but the last of the victims was struck by a simultaneous उत्सादन and stambhana hit. The virile तैत्तिरीयिन् interceded rapidly and controlled the damage limiting it to a fixed duration stambhana. Aunt spring, who had been favored for long by bAlA, immediately rapidly advanced seeing the opportunity on the खाण्डव front. The खाण्डवन्स् showed that they were not cowed down and put up a strong attack. Soon the battle grew so intense that as expected we were also drawn into it. Since the आचार्य of the virile तैत्तिरीयिन् and ours was the same, there was not much different we could do, beyond deploying the अथर्वण श्रुती. But the allies of the खाण्डवन्स् made sure that we are constantly occupied on our usual battle front to tie up such a deployment.


