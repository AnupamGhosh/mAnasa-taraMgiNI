
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Watongia](https://manasataramgini.wordpress.com/2006/11/01/watongia/){rel="bookmark"} {#watongia .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 1, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/11/01/watongia/ "6:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The varanopids are a lineage of synapsids whose distinctness and wide distribution has come to light more recently, even though Romer described Varanops elegantly in his early monographs. More recently Reisz and Laurin, the experts on these topics, described a relatively large varanopid, Watongia, from the Oklahoma Chickasha formation. This synapsid was discovered by Olson in 1974 but was poorly understood until a few years ago. Watongia appears to share a hallmark feature of two other varanopids, namely the postorbital tuberosity if the authors' interpretation of the skull is correct. The vertebra nevertheless are very similar to Aerosaurus and Varanodon even if larger. The function of this tuberosity in Watongia, Aerosaurus and Varanodon is unclear. It could be a muscle attachment site, but I suspect that it might have had a soft tissue decoration, like a spike or spine that played a role in sexual selection. Watongia also has a distinct canine that is twice the size of the maxillary teeth. This suggests that the presence of the hypertrophied canine as a predatory device emerged relatively early in synapsid evolution, and was repeatedly used in so many synapsids lineage down to this day. The teeth were recurved strongly with anterior and posterior unserrated cutting edges.

A linear relationship between the size of the largest centrum of the vertebra and the body length has been postulated. Average body length in mm vs maximum centrum length in mm:

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger2/6438/855/320/synapsid_scaling.0.jpg){width="75%"}
```{=latex}
\end{center}
```



Smallest to largest: 1) [Varanops 2) Aerosaurus 3) Varanodon 4) Dimetrodon milleri 5) Watongia 6) Dimetrodon booneorum 7) Titanophoneus]{style="font-style:italic;"}\
Based on this scaling the length of Watongia is estimated as 2 meters. This is pretty large by Varanopid standards. Given that there are only large Caseids but no sphenacodontines in the Chickasha formation, it is clear that here the varanopids like [Watongia ]{style="font-style:italic;"}were the top predators. Thus, in different middle Permian assemblages different synapside lineage were top predators.\
In phylogenetic terms there is a clear monophyletic lineage within varanopids in the form of [Varanodon, Aerosaurus and Watongia]{style="font-style:italic;"}. The poorly studied [Archaeovenator ]{style="font-style:italic;"}appears to be the most basal member of the clade with [Elliotsmithia, Mesenosaurus and Mycterosaurus]{style="font-style:italic;"} falling in between it and the varanodontines, but with no strong support for their grouping.


