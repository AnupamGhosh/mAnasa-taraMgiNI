
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Knowing indra](https://manasataramgini.wordpress.com/2006/12/19/knowing-indra/){rel="bookmark"} {#knowing-indra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 19, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/12/19/knowing-indra/ "6:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The primary object of the Arya is to know indra:\
[सत्यं हीन्द्रः ।]{style="color:#0000ff;"}\
indra is verily the truth

[तं हेन्द्र उवाछ माम् एव विजानीहि ।]{style="color:#0000ff;"}\
And he told \[pratardana दैवोदासि] just perceive me

[एतद् एवाहं मनुष्याय हिततमं मन्ये यन् मां विजानीयात् ।]{style="color:#0000ff;"}\
What I consider to be most beneficial to a human is that he should perceive me.

[त्रिशीर्षाणं त्वाष्ट्रम् अहनम् ।]{style="color:#0000ff;"}\
I killed the 3-headed son of त्वष्टृ.

[अरुर्मघान् अहनम् ।]{style="color:#0000ff;"}\
I killed the arurmaghas.

[यतीन् सालावृकेभ्यः प्रायच्छम् ।]{style="color:#0000ff;"}\
I gave the yati-s to the hyenas.

[बह्वीः सन्धा अतिक्रम्य दिवि प्रह्लादीयान् अतृणहम् अन्तरिक्षे पौलोमान् पृथिव्यां कालकाञ्जान् ।]{style="color:#0000ff;"}\
Breaking many pacts, I smashed to pieces प्रह्लादीय-s in the heaven, the pauloman-s in the mid-region and the कालकञ्ज-s on earth.

[tasya me tatra na loma चनामीयत |]{style="color:#0000ff;"}\
I did not even lose a hair on my body in the process.

[sa yo mAM veda na ha vai tasya kena chana कर्मणा loma मीयते na steyena na भ्रूणहत्यया na मातृवधेन na पितृवधेन |]{style="color:#0000ff;"}\
When a person perceives me, nothing that he does- whether it is stealing, or performing an abortion, or killing his own father or killing his mother will ever make him lose a single hair of his body.

[नास्य पापं चक्रुषो मुखान् नीलं व्येतीति ॥१॥]{style="color:#0000ff;"}\
And when he has committed a sin his face does not lose its color.

Such are great acts of the god of the gods. The one who is able to see him transcends all.

[धिष्वा शवः शूर येन वृत्रम् अवाभिनद् दानुम् और्णवाभम् ।]{style="color:#0000ff;"}\
[अपावृणोर् ज्योतिर् आर्याय नि सव्यतः सादि दस्युर् इन्द्र ॥]{style="color:#0000ff;"}\
O mighty one, assume the power with which you tore to bits वृत्र, दानु and the spider-like demon और्णवाभ. You have shown light to the Aryans, on your left side O indra, you sank the dasyu.

[सनेम ये त ऊतिभिस् तरन्तो विश्वा स्पृध आर्येण दस्यून् ।]{style="color:#0000ff;"}\
[अस्मभ्यं तत् त्वाष्ट्रं विश्वरूपम् अरन्धयः साख्यस्य त्रिताय ॥]{style="color:#0000ff;"}\
May we with your aid cross over to \[victory] in that struggle for the world between the Aryans and dasyu-s. For तृत of our party you beheaded विश्वरूप, the son of त्वष्टृ.

[अस्य सुवानस्य मन्दिनस् त्रितस्य न्यर्बुदं वावृधानो अस्तः ।]{style="color:#0000ff;"}\
[अवर्तयत् सूर्यो न चक्रं भिनद् वलम् इन्द्रो अङ्गिरस्वान् ॥]{style="color:#0000ff;"}\
With his might growing with the invigorating soma offerings of trita he destroyed arbuda. Spinning his chakra like the blazing sun, indra cut down vala, praised by the अङ्गिरसस्.


