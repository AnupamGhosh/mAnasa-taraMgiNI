
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The संक्षिप्त-bhairava mantra](https://manasataramgini.wordpress.com/2006/07/23/the-samkshipta-bhairava-mantra/){rel="bookmark"} {#the-सकषपत-bhairava-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 23, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/23/the-samkshipta-bhairava-mantra/ "6:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/kAlAरी_gaHvara.jpg){width="75%"}
```{=latex}
\end{center}
```



The formulae of the fierce संक्षिप्त bhairava can be used to counter a range of मारण attacks. It is different from the अमृतेश्वर vidya, like that described in the netra tantra tradition. The latter is शान्तं, while the संक्षिप्त bhairava formulae are घोरं. It can also be used for मारण strikes when conjoined with महामारी, the mate of the fierce bhairava, and the mantra of yama. The भार्गव मार्कण्डेय is the ऋषि of the mantra of the terrifying संक्षिप्त bhairava. He is meditated upon holding महामारी in his grasp. He is dancing on preta-s lying on the श्मशान. His face is terrifying with 3 eyes and a crown of upward-standing hair. He has sharp fangs in his mouth. He has 8 arms, and holds a trident, the पिनाक bow and पाशुपत missile, a skull-topped खट्वाङ्ग, a lasso, sword, a chakra and a fire pot. His wife महामारी is of deep blue complexion, with long flowing locks of black hair and is embracing the fiery bhairava. The frightful yama is standing revertially worshiping them. Beyond him is standing the most bewitching pradhAna यक्षिणी, प्रमोदा with her यक्षिणी hordes.

The formal version of his mantra is the अनुष्टुभ्:\
[यमराज सदो।अमेय यमे दोरदयोदय । यद् अयो-निरय-क्षेय यक्षेय च निरामय ॥]{style="color:#99cc00;"}

The contains a symmetric structure within it known as the pratiloma-yamaka used by savants like Anandavardhana and दण्डिन्. This is characterized by what is known as the inverted terminal repeats:\
[[यमरा]{style="color:#ff9900;"}ja sa[दोमेय]{style="color:#ff0000;"} [yame do]{style="color:#ff0000;"}rada[योदय]{style="color:#009900;"} |]{style="font-weight:bold;font-style:italic;"} [[yad ayo]{style="color:#009900;"}-nira[ya-क्षेय]{style="color:#6600cc;"} [यक्षेय]{style="color:#6600cc;"} cha ni[रामय]{style="color:#ff9900;"} ||]{style="font-weight:bold;font-style:italic;"}

The mantras of महामरी and yama have similar symmentric structure:

[का-ली-मा-र-र-मा-ली-का- ली-न-मो-क्ष-क्ष-मो-न-ली ।]{style="color:#99cc00;"}\
[mA-mo-de-ta-ta-de-mo-mA-ra-क्ष-ta-ttva-ttva-ta-क्ष-ra ||]{style="color:#99cc00;"} (महामारी)

[ya-mA-pA-Ta-Ta-pa-mA-ya-mA-Ta-mo-Ta-Ta-mo-Ta-mA |]{style="color:#99cc00;"}\
[pA-mo-भू-ri-ri-भू-mo-pA-Ta-Ta-री-stva-stva-री-Ta-Ta ||]{style="color:#99cc00;"}\
[\
]{style="font-weight:bold;font-style:italic;"}These mantras create the yantra used in the prayoga known as a gahvara by virtue of their symmetries. The bewitching, most beautiful pradhAna यक्षिणी emanating from combined mantras is प्रमोदा worshipped with the mantra:\
[ऒं स्ठ्रीं ह्रींः महानग्न्यै हूं फट् स्वाहा ॥]{style="color:#99cc00;"}

The mantra is recommended by the great siddhAnta savants सद्योज्योती from Kashmir and ईशानशिव the नम्बूतिरि to be used in अभिचार. The mantra has widely spread in various forms across Asia and in the East Asian tradition a variant is used as mantra to the buffalo-riding vaivasvata. Among the nAstIka followers of the तथागत he is worshipped as vajrabhairava and the mantra was well-known in central Asia. Thus, two Mongol chiefs who attempted to revive the Chingizids in their declining era were practioners of the mantra. The first was Dayan Khan, the second was the renowned Mongol tantric, Khutuktai Sechen khongtaiji.

The mighty chera magicians (including the rival of PM) had fluency of the mantra and could counter prayogas aimed at them and attack others. We too were saved by the यक्षिणी from the grip of yama as we have narrated before.


