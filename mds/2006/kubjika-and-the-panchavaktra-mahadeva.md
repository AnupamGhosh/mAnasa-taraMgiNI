
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कुब्जिका and the पञ्चवक्त्र महादेव](https://manasataramgini.wordpress.com/2006/07/20/kubjika-and-the-panchavaktra-mahadeva/){rel="bookmark"} {#कबजक-and-the-पञचवकतर-महदव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 20, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/20/kubjika-and-the-panchavaktra-mahadeva/ "6:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/कुब्जिका1.jpg){width="75%"}
```{=latex}
\end{center}
```



The great tradition of पस्चिमाम्नाय goddess, our hallowed mother कुब्जिका, had a number of streams that are now largely forgotten. One of the older streams before its standardization in the tradition of the कुब्जिकामत and षट्साहस्र streams was the worship of कुब्जिका conjoined with the five headed shiva (distinct from the other representations where she is conjoined with shiva as नवात्मन् (also alluded to in the saundaryalahari; while mainly devoted to कुलसुन्दरी, 34, 39 and 41 refer to कुब्जिका) or has paramashiva above her) . This tradition is associated with ancient pre-matsyendra siddha guru-s like, भृग्वानन्दनाथ, राञ्गुलानन्दनाथ, सम्वर्तानन्दनाथ and their wives, and others whose feet are offered तर्पणं-s in the guru-मण्डल. Here she is worshipped with shiva showing all his five faces सद्योजात, वामदेव, tatpuruSha, aghora and ईशान. He holds a trident and a Damaru. The hallowed mother कुब्जिका has 6 faces; the cobras करोटक as a waist band, तक्षक as a mid-riff ornament, वासुकि as garland and the venomous cobra kulika as an ear ornament. The great tortoise is her ear stud. She holds in her arms as skull, a king-cobra, a crystal-bead rosary, skull-topped rod, a conch, a book, a trident, a mirror, a straight sword, a gem necklace, an अङ्कुष and a bow. She is of fair complexion like a young jasmine flower. The yantra of this worship is provided above.


