
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Atharvanic rudra gAyatrI](https://manasataramgini.wordpress.com/2006/03/22/the-atharvanic-rudra-gayatri/){rel="bookmark"} {#the-atharvanic-rudra-gayatri .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/22/the-atharvanic-rudra-gayatri/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Atharvanic tradition records a special rudra gAyatrI that might be used in regular japa in place of the traditional rudra sAvitrI. This mantra is:\
[तस्मै देवाय विध्महे । महादेवाय धीमहि । तन् नो रुद्रो ।अनुमन्यतां ॥\
]{style="font-weight:bold;color:rgb(255,0,0);"}This is known as the atharvashikha and is high mantra for those on the माहेश्वर path.[\
]{style="font-weight:bold;color:rgb(255,0,0);"}


