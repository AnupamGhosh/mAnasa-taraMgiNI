
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some words on the netra तन्त्रं](https://manasataramgini.wordpress.com/2006/06/24/some-words-on-the-netra-tantram/){rel="bookmark"} {#some-words-on-the-netra-तनतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 24, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/24/some-words-on-the-netra-tantram/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[त्र्यम्बकं यजामहे सुगन्धिं पुष्टि वर्धनम्]{style="font-weight:bold;font-style:italic;color:#99cc00;"}\
[[उर्वारुकम् इव बन्धनान् मृत्योर् मुक्षीय मामृतात् ]{style="color:#ff0000;"}\
]{style="font-weight:bold;font-style:italic;"}[\
]{style="font-weight:bold;font-style:italic;"}The netra tantra commented upon by the famous क्षेम-rAja, student of abhinava-gupta, is of some what later origin amongst the shaiva tantras. It is not listed as a primary text in any of the five srotas of the tantric shaiva मार्ग in support of this contention. However, it was definitely considered cannonical by the start of the syncretic trika period. While it has affinities with the garuDa tantras, there is evidence to believe that the netra tantra tradition was a parallel stream outside the five cannonical srotas that probably branched from an ancestor close to the garuDa srotas but evolved on its own in the midst of the kaTha yajurvedin ब्राह्मणस् of northern and north-western India. The main evidence for the connection to the garuDa srotas is from its emphasis on the मृत्युन्जय mantra which is also expounded in the former tantras. In support of this we note that क्षेमराज uses quotes from the garuDa srotas to explain several points in the netra तन्त्रं, and the mantra of त्र्यंबक rudra as अमृतेश्वर, which is the central theme of the tantra. The worship of मृत्युन्जय or अमृतेश्वर has a very precise iconography that is described in the dhyAna shloka given in the 3rd पटल (chapter) of the netra tantra. R brought to my attention a couple of idols of त्र्यंबक or अमृतेश्वर and his wife, one from what is now Himachal Pradesh (close to R's native village) and one from what is now Afghanistan (where apparently kaTha yajurvedins were once present). These exactly match the dhyAna sholkas supporting the importance of this stream as an independent one in N/NW India. Further, the kaTha ब्राह्मण तान्त्रिक-s use the important double prayoga, the aghora prayoga and the त्र्यंबक prayoga with the mantras of स्वच्छन्द bhairava and अमृतेश्वर. Importantly, the kaTha-s before reciting their shatarudriya recite the dhyAna sholkas of त्र्यंबक and aghora-- somewhat akin to the insertion the तैत्तिरीय yajurvedin-s make after the first रुद्रानुवाक in shaiva recitation.

अमृतेश्वर of the netra tantra stream is depicted with 4 arms, white in color, with 3 eyes and wearing a helmet/crown. He is seated on an इन्दिवार lotus, in the middle of which is a moon disc. In his right inner arm he holds a soma pot, and in his left inner arm a moon disc. The right outer arm shows the abhaya mudra and the left arm embraces अमृतश्री who is seated on his left thigh. She also has 4 arms and in her right and left inner arms carries a chakra and a शङ्ख, while her outer arms show abhaya and varada mudras. The main day for his worship is the अष्टमि of the autumnal navaratri, where his entire मण्डल along with the आवरण पूज is done.

The inner most आवर्ण has the उपासन of the four आम्नाय emanations of shiva and his wife: 1) सदाशिव associated with the सिद्धान्ताम्नाय 2) वामदेव associated with the वामाम्नाय 3) aghora or दक्षिणाङ्ग rudra associated with दक्षिणाम्नाय. 4) akula-vIra associated with the कुलाम्नाय.\
Then the next circle outwards has the worship of: 1) विष्णु and महालक्ष्मी 2) shiva and उमा 3) brahmA and सरसवती. 4) Then the मार्ताण्द bhairava emanation of अमृतेश्वर is invoked as अमृतसूर्य with 8 hands holding the weapons of the eight devas (see below). 5) Then bauddha mantras are inserted by some traditions here.\
In the next circle are worshipped: 1) कुमार कार्तिकेय 2) manmatha 3) soma ओषधिपति 4) विनायक.\
In the outer most circle the 10 deities are worshipped with their weapons: 1) indra+vajra 2) agni+shakti 3) yama+daNDa 4) निरृति+खड्ग 5) वरुण+pAsha 6) वायु+अङ्कुष 7) soma+gada 8)ईशान+त्रिशूल 9) विष्णु+chakra 10) prajApati+padma

For those of the second वर्ण, अमृतेश्वरी or अमृतलक्श्मी worshiped in the sword before going to do battle against one's shatru's. The followers of the atharvan tradition invoke her as प्रत्यङ्गिरा in the sword rite for क्षत्रियस्. The netra-tantric shaivas also have the rite of indra-rupa अमृतेश्वर which is performed in the भाद्रपद where अमृतेश्वर is identified with indra and worshiped in the indra pole prior to the end of the most important Hindu festival indramaha.


