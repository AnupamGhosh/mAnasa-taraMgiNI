
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On the name itself](https://manasataramgini.wordpress.com/2006/02/15/on-the-name-itself/){rel="bookmark"} {#on-the-name-itself .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/02/15/on-the-name-itself/ "5:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were passing noting the immersion of the folks in the pAsha of moha. We were trying to take the upward passage when we encountered the terrific कृत्या. The कृत्या was blocking our path even as the कृत्या of rudra that swallowed our ancient clansman ushanas. We evaded her narrowly and moved past despite sustaining a blow from it. The flow of internal surA from the ब्रह्मरान्ध्र quieted us even as a morbid यजमान who has been struck by the fury of soma is quieted by the external surA. धूम्रच्छाय asked us why this name?

Once in the halcyon days, we retired to our quiet haunt, having deftly broken away from the plebeians given to their alpa bhogas. There we came across a remarkable Sanskrit work, composed by the scholarly crest jewel of the second वर्ण- the चालुक्य king someshvara-deva III. While the readings were difficult, we immediately felt a kinship with this राजन्. In many ways he reminded me of his better known predecessor rAjA bhoja the परमार, who was also striking in his breadth of knowledge. The kindred feeling that I felt towards the rAjA was because of encyclopedic tendencies- he valued all branches of knowledge. More importantly, he was an astute observer- for after all, all science is observation and lively synthesizer. This was the intellectual Hindu spirit, which also produced the brahminical legalistic encyclopedias or the ritualistic encylopedias. The Hindu though often "self-absorbed" due making consciousness the axiom rather than the outgrowth, on occasion rose beyond to observe, accurately enumerate and systematize nature. In his systematization he discovered the principles of homology and "[परिणामं]{style="color:#ff0000;font-weight:bold;"}". His linguistic and intellectual cousin , the yavana sophist applied it to geometry, but the Hindu elucidated in a more general sense, when not in the inebriation of the axiom of Atman. This period of someshvara deva III was the last efflorescence of Hindu thought before the Mohammedan mayhem snuffed it out in tectonic rattles that followed.

This text the मानसोल्लास (aka अभिलषितार्थचिन्तमणि), has been much talked about, but hardly known to the decadent modern Hindus who epitomize the downward turn of the काल-chakra in the fourth मानुष yuga. While many have talked about it before, let me repeat some of this as a praise to the meteor of the चालुक्य firmament. The text as we have it owes its existence ot the unflagging efforts of a महराष्त्रन् brahmin, Shirgondekar, who edited the thing over several decades. It covers 100 chapters in 5 विंशति-s or 20 chapter divisions. A striking variety of matters are described in the मानसोल्लास. Just to give a flavor: 1) The lost wax procedure and metal casting. 2) Medicine and preparation of drugs. (These come in the sections termed रसायणं and धातु-वाद-रस्ण्यनं) 3) Sex. 4) Elephants and horses and their maintenance. 5) Fishes from the Indian ocean 6) Cooking including recipes for dishes including the idli and the मांस bhajjis. Many of our current favorites, like the dhosaka (dosai), mAsha वटक (the medu vadai) and पूरिक (the masala पूरी) and the amlaka pickle (आवक्काइ उर्गाइ) are laid out clearly. 7) construction of houses and temples. 8) sedentary games like cards and chess and physical games like water sports (toya-क्रीड). 9) Alcoholic revelry ([madira-पान-]{.a2}क्रीड) 10) bed, bath, footware, umbrellas and clothing. 11) deployment of mantras.

So in memory of the kShatriya's great text we decided to name our much lesser ramblings, expressing our continuity with the hindu intellectual tradition that is all but submerged.


