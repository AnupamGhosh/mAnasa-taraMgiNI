
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The battle of the train and bus towns](https://manasataramgini.wordpress.com/2006/04/29/the-battle-of-the-train-and-bus-towns/){rel="bookmark"} {#the-battle-of-the-train-and-bus-towns .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 29, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/29/the-battle-of-the-train-and-bus-towns/ "6:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As many issues remain still murky and we are still behind the haze we cannot narrate much. It was wild ride is all we can say. We knew that one of our greatest sapatna-s stood straight in our path. It was going to be a fight like the one between कर्ण and arjuna. Just as only one sword can reside in a scabbard, just as there can only be indra or वृत्र or just as there can be only one rAja in राष्ट्र it had to be one of us. The sapatna arranged things such that we had to attack from a very unfavorable position. We had no choice but to charge. We could not deploy anything at first. But we planned everything pretty well in terms of timing of the attack, and then found a moment to invoke the secret aid of the breaker of vala's stronghold: मञ्जुघोष, combined plainly with that great forgotten weapon: "the strike of शूलिनि". The speed of the maruts took us through the first attack scattering the कृष्ण-वर्ण-s and एकान्तिक-वादिन्-s. Then we drove away अजधाडिक, विट्शिरस्, बिडालश्मश्रु and shuchiroman. Then our great shatru repeatedly attacked trying to tie us with astras. We, however, invoked the mahA-vajra to pierce him. That thunder which had burnt the the enemies of trasadasyu, that might of इन्द्रावरुण bore us aid against him. This round of the गविष्टि was ours. Then we were moving away after having controlled the vile Damarushiras, when our old भ्रातृव्य attacked us violent with the rakta-घातक. He could not bear the fall of बिडालश्मश्रु and mahA-shatru in the open field. ("म्लेच्छस् horsemen are all around you."). Four fierce weapons pursue us- the रक्तघातक, the bhaya निरृति (we remembered the old नैरृत attacks), राक्षसि and ज्वालामुख. We needed to obtain the यक्षिनि-s बालचन्द्रिका and सुरतमञ्जरि to save ourselves now.


