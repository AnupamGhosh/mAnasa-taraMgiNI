
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Beauty in different degrees of recursiveness](https://manasataramgini.wordpress.com/2006/08/22/beauty-in-different-degrees-of-recursiveness/){rel="bookmark"} {#beauty-in-different-degrees-of-recursiveness .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 22, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/22/beauty-in-different-degrees-of-recursiveness/ "6:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/recursiveness2.jpg){width="75%"}
```{=latex}
\end{center}
```



