
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [trika and kaula teacher graph](https://manasataramgini.wordpress.com/2006/03/31/trika-and-kaula-teacher-graph/){rel="bookmark"} {#trika-and-kaula-teacher-graph .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 31, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/03/31/trika-and-kaula-teacher-graph/ "7:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/tantric_teachers.jpg){width="75%"}
```{=latex}
\end{center}
```



This is study in progress and will evolve with time

