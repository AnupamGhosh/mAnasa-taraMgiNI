
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The crocodile line](https://manasataramgini.wordpress.com/2006/01/28/the-crocodile-line/){rel="bookmark"} {#the-crocodile-line .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 28, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/01/28/the-crocodile-line/ "5:19 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/hello/133/1300/400/Mara_crocodile.jpg){width="75%"}
```{=latex}
\end{center}
```



The footage of the carnage by the giant Nile crocodiles on the Mara river was remarkably inspiring and it brought memories of a remarkable sight I had seen in the halcyon days: The cow being downed by a mugger in the Indian Nullah. These Nile crocodiles, the mugger and their cousins are all that remains an old line of archosaurs that in their glory once rivaled their cousins who still rule the skies-- the only surviving dinosaurs. While the dinosaurs justified the apellation "archosaur" for 135 million years of unbroken ecological dominance, the crocodile-line sporadically had its day. But their true period of ecological dominance was the Triassic. The carnage on Mara shows how this body design perfected in many basic ways even in the Triassic can hold its own perfectly fine in the world of the mammals. The Nile hunter takes down all sorts of mammalian prey with amazing ferocity and finese that would even give the felids, that mammalian epitome of carnivory, a run for their money. Wildebeest, Kudu, Zebra, the buffalo and others hunted in the yearly feast of the archosaur.

Reverse the time machine to the late Triassic of Durham, North Carolina, another predator of the crocodile-line, a rauisuchian poposaur related to [Postosuchus ]{style="font-style:italic;"}was stalking in vegetation where there was probably a faunal congregation similar to the events on the Mara. This poposaur was a verily a super-predator. almost a meter long head with a mouth armed with numerous meat-cutter knife teeth. These teeth could be lost in action and replaced by new ones througout life. Its body was covered with armour plating buttressed by dermal bone and its arms bore a thumb with a sharp claw for a pincer grip on the prey. The poposaur first encountered an ancient mammal relative the dicynodont which it attacked and tore up gulping it up. Next in encountered an armored temnospondyl amphibian, whose impressive armour failed to deter the attacker and was soon gobbled by the poposaur. In this feeding glut it next attacked one of the most heavily armored customers of the day-- its own cousin a aetosaur and with its bone crusher, meat cutter teeth broke through the armor of the aetosaur to make a meal of it. An ancient cousin of ours a protomammal- the traversodont cynodont [Plinthogomphodon herpetairus]{style="font-style:italic;"} came its way and the poposaur swallowed it whole as a snack to top its rich and varied catch for the day. Unsatisfied it seems to have attacked another cousin, an early agile crocodile related to [Saltoposuchus]{style="font-style:italic;"}. This fellow was to be its last victim-- with a quick lunge from an undergrowth the poposaur bit its victim on the neck slicing right through the bone of the cervical vertebra of the crocodile. However, before dying the agile crocodile appears to have bitten right into the heart of its attacker thereby delivering the [coup de grace]{style="font-style:italic;"} to the top predator of the era. Thus, in the cycle of life, the predator and prey lay dead one on top of the other. A few phytosaurs, which looked like the modern crocodiles came to make a meal of their carcasses.

Sues and Olsen described this remarkable assemblage from the Late Triassic of Durham, where the rauisuchian skeleton was uncovered and found to contain its last meals described above and a [Saltoposuchus-]{style="font-style:italic;"}like crocodile under it with a bite mark in its neck. Thus the Triassic was the high-point of the crocodile-line in a croc-eat-croc world: phytosaurs, ornithosuchids, prestosuchids, rauischids, aetosaurs and crocodylomorphs.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/junggarsuchus.0.jpg){width="75%"}
```{=latex}
\end{center}
```

\
*Junggarsuchus*, an early agile crocodylomorph with a fully erect gait


