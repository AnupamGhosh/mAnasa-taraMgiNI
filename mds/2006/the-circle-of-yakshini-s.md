
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The circle of यक्षिणी-s](https://manasataramgini.wordpress.com/2006/08/17/the-circle-of-yakshini-s/){rel="bookmark"} {#the-circle-of-यकषण-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 17, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/08/17/the-circle-of-yakshini-s/ "5:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We bow to the great goddess of the त्रोतलोत्तरं and pay worship to the great throng of यक्षिणी-s that emerge from her. The यक्षिणी-s are of diverse forms, all very wonderful and amazing. They bear many diverse facets of knowledge --- he who can acquire them all becomes sarva-व्यापिन्. But the knowledge is not easily acquired from the यक्षिणी. There are so many ways in which they slip away after it has been shown to you by her. After you land from the flight of the यक्षिणी, it may evaporate from you. Or in the delusion of the wonderous form of the यक्षिणी, the knowledge might escape from you. Some wonderous यक्षिणी-s carry what may be called "useless knowledge". It can be endlessly fascinating but cannot be acquired by those not fit for it, or knowing it takes you down a path a failure.


