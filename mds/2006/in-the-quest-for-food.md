
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the quest for food](https://manasataramgini.wordpress.com/2006/07/08/in-the-quest-for-food/){rel="bookmark"} {#in-the-quest-for-food .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 8, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/07/08/in-the-quest-for-food/ "6:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

भृगु, the founder of our race, had said that food was brahman. In that quest we set out to the city center. They say no journey in the company of a true friend is ever long. They say no wait in the company of a comely and amiable young woman is too long. But a journey in the company of a false friend or even the briefest wait in the company of a राक्षसि-like woman is a passage through naraka. We had made the long journey to city center in all these companies- with our friend with whom we were united in mind, with the apsara-like girl who bathes all the senses in soma-like bliss (verily called सोमानन्द), in the company of a पिषाचि when every nimesha of the मानुश feels like one of brahma. And then we were making it alone. When you make it alone, all you are left with are the reflections in the atmAn. As the reflections fleeted through the [chit]{style="font-style:italic;"}, or was it the [manas ]{style="font-style:italic;"}(we would never know?), we could identify with the प्रतिक्रिया that shuddhodana-putra had felt on his first journey outside the royal cocoon. It was all there roga, jara, मृत्यु. Yet on the other side there was the Ananda of the bhoga of anna, पान, काम इत्यादि being enjoyed by many. First the words of the wise eldest पाण्डु came to mind, but then the unity with वार्ताली was felt. It was that flow of प्रकृति who was manifest as वाराहि and the 8 mothers that we were a part of. We then attained bliss in food.


