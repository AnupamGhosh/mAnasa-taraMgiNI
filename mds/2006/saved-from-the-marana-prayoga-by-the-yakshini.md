
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Saved from the मारण prayoga by the यक्षिणि](https://manasataramgini.wordpress.com/2006/06/27/saved-from-the-marana-prayoga-by-the-yakshini/){rel="bookmark"} {#saved-from-the-मरण-prayoga-by-the-यकषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 27, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/27/saved-from-the-marana-prayoga-by-the-yakshini/ "9:31 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We have seen the hits of a few मारण prayoga-s in the past but never had we seen vaisvasvata's agents more closely than now. Some how the Northern journey is an opportunity for the मारण of the vairis. We were weakened by the effects of the tapta prayoga but were still completely functional. The night before we had performed the adhara-कुलानन्द rahas-तर्पण of the beautiful but terrifying यक्षिणि नीललोहित-मेखला. We fatefully ventured out to the आवर्ण-patha and were making our way for the midday oblations to सविता, when without warning we were struck by the terrifying devastambhini. The blow was so terrifying and so sudden that we simply could not protect ourselves all our varman-s were demolished thoroughly. We were being literally drowned in the waters and could see yama's agents bearing us away. For several minutes we were trashed by the waters of वैतरणी, any moment sliding towards our जलसमाधि. We just then realized that the only one who could physically bear us out of this deadly मारण prayoga was the great यक्षिणि, नीललोहित-मेखला, hovering yonder. We piteously called for her help. A fragment of the महास्त्र hit her leg, but she ably dodged it and still came to our aid. नीललोहित-मेखला gave us her arm, and holding to it like मार्कण्देय to shiva, we struggled out of वैतरणि with the yama दूतस् lashing at us. But the great नीललोहित-मेखला grew immensely heavy and pulled us out from the frothing abyss with her ivory-hued creeper-like arm endowed with the might of 800 elephants. We were bruised, bleeding and covered with the mire of yama's flow, but we had been pulled out by the most beautiful नीललोहित-मेखला from what would have been certain death. While we are reeling from the effects of this मारण that came closest to fruition, we are happy we are alive. You never know when the त्रिकोणोद्ध्भव mahA-यक्षिणि can save you, hence her sweet-bitter साधन is never in nought.


