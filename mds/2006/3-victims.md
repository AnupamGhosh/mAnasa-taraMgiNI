
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [3 victims](https://manasataramgini.wordpress.com/2006/04/08/3-victims/){rel="bookmark"} {#victims .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 8, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/04/08/3-victims/ "4:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The शचीव and and the अमात्य gave us the signals that three कृत्या's were dispatched. The strong PM, main victim of Fourier, seemed to be under what might be his final decline. The दूतस् of महिषवाहन were apparently approaching him. The muni confirmed this possibility. He had lived his life of yoga and his महाजप of the shatarudriya, and devotion to the vedas had given him the first 2 year respite when he was first struck by the vairiprayogas. But his mantra bala had run out and now he lay flattened. Mn, after his divergence from me was living a very similar life. Mn was the smartest amongst us, and was a remarkable schemer. His secretive शचीव performed several prayogas for him. So while faced with issues similar to us, he acted far more smartly. However, he was targetted by strike two. His शचीव did some rites to save his life, but his ratha was badly damaged. The third strike targetted us, we were intensely tormented by the piercing arrows of it. It was made clear that this was a strike from which there was no escape.

Again we reminded ourselves of Dattaji Shinde's words: "If I survive, I will fight again."


