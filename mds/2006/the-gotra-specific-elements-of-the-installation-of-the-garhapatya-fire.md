
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The gotra-specific elements of the installation of the गार्हपत्य fire](https://manasataramgini.wordpress.com/2006/09/25/the-gotra-specific-elements-of-the-installation-of-the-garhapatya-fire/){rel="bookmark"} {#the-gotra-specific-elements-of-the-installation-of-the-गरहपतय-fire .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 25, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/25/the-gotra-specific-elements-of-the-installation-of-the-garhapatya-fire/ "5:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}


  - The उद्गाता starts singing the rathanthara and ya\~jna-यज्ञीय sAman-s \[The जैमिनीयस् recite their samans in a low inaudible voice on the occassion of this rite].

  - The adhvaryu starts installing the गार्हपत्य from the fire he has just kindled with the अरणि-s on the संभार-s that he has laid down at the site of the गार्हपत्य altar. The संभार-s comprise of the following dravya-s- the पार्थिव set: 1) sand, 2) soil that is rich in salt, 3) soil from a rodent-excavated tunnel, 4) soil from a termite mound 5) soil dug by boars 6) sediment from the bed of a perennial lake 7) smooth pebbles 8) gold. The dru set: wooden pieces of 1) ashvattha 2) udumbara 3) पलाश 4) shami 5) विकङ्कट 6) fulgurites or a tree struck by lighting and 7) lotus leaves. He may at least get 5 of each of these.

  - As he installs the fire he recites formulae depending on the यजमान's origin\
If he is a भार्गव he chants:\
भृगुणां त्वा vratapate vratena दधामि |\
If he is an आङ्गिरस् he chants:\
अङ्गिरसां त्वा vratapate vratena दधामि |\
If he is of any other gotra he chants:\
आदित्यानां त्वा देवानां vratapate vratena दधामि |\
If he is a royal sacrificer he chants:\
वरुणस्य त्वा राज्ञो vratapate vratena दधामि |\
If he is an ordinary kShatriya he chants:\
indrasya त्वेद्रियेण vratapate vratena दधामि |\
If he is a vaishya he chants:\
manostva ग्रामण्यो vratapate vratena दधामि |\
If he is a version of the 4th वर्ण with vedic अधिकार (like रथकार or niShAda stapathi) he uses:\
ऋभुणं त्वा देवानां vratena दधामि |

  - Then the adhvaryu chants as he is making the fire burn the formulae beginning with यास्ते शिवास्तनुवो...

  - Then the यजमान worships the fire with the incantation सुगार्हपत्यो (TB 1.2.1) and repeats the gharmashiras incantations with the adhvaryu.

  - then the adhvaryu बर्हिष् around the गार्हपत्य fire.


