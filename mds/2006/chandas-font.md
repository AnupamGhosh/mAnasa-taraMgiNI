
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Chandas font](https://manasataramgini.wordpress.com/2006/06/06/chandas-font/){rel="bookmark"} {#chandas-font .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 6, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/06/06/chandas-font/ "5:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Sanskrit font created by Mihail Bayaryn is the best available now for Sanskrit word processing. In a combination with Itranslator of Svami Omkarananda Ashram it can now be used for processing Vedic Sanskrit of any type including atharvan, sAman and shukla yajurvedic svaras. Below is a sample of a screen shot of a तैत्तिरीय text processed by this method:\
Chandas font:\
[http://chandas.cakram.org/](http://chandas.cakram.org/ "http://chandas.cakram.org/"){.linkification-ext}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/vishnu_krama.jpg){width="75%"}
```{=latex}
\end{center}
```




