
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Satmala adventures](https://manasataramgini.wordpress.com/2006/09/15/the-satmala-adventures/){rel="bookmark"} {#the-satmala-adventures .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 15, 2006]{.entry-date}](https://manasataramgini.wordpress.com/2006/09/15/the-satmala-adventures/ "2:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/satmala.jpg){width="75%"}
```{=latex}
\end{center}
```




 1.  Chandvad (Chandradityapura)

 2.  Chandvad fort

 3.  चन्द्रेश्वरी cave temple

 4.  रेणुका temple

 5.  इन्द्राणी temple

 6.  indragiri durga (Indrai fort)

 7.  Koldher fort

 8. Rajdher fort

After a long bus journey the शूद्र, the feral brahmin, Mis-creant and myself entrenched ourselves in the lodge of the लाटस्. After enjoying a sumptous ghee-rich meal that in our youth we could eat without much thought we proceeded by means of the second bus to middle of nowhere town/village of Chandvad. It is all that remains of the once glorious township founded by the यादव king दृडप्रहार in 801 CE. He and his successors fortified the hill overlooking it to protect it against attacks. However, the strong fort was overcome by the Moslems twice under Alla-ad-din and Awrangzeb during their Jihads in central India. The Maharattas liberated the region during Shivaji's historic campaigns in the region in the 1660s. Finally it became the mint of Bajirao's general Malaharao Holkar. The British invaders under Thomas Hislop and McDowell conquered it in 1818 CE after the fierce war for India against the Maharattas. They ravaged the town and detonated the path to the fort top with bombs preventing an easy entry into the fort without scaling sheer rock.

We first spied a well-built but largely neglected temple of विष्णु just off the Chandvad township that was built by हेमाद्री the last great brahmin minister and encyclopaedist of the यादवस्. While we were good rock-climbers in those days we were put off by the final stretch and did not get into the Chandvad fort and instead moved along the slopes to enter the deep cave temple of चन्द्रविद्या. For Mis-creant and myself who had a deep connection with the tantrics of yore, we could sense a deep unity with those who had meditated in the cave, practising the hallowed पञ्चदशी and beyond. Then we moved towards the famous pass through which the Moslems attacked the land (which now contains the highway to Agra) after a 3 hour ardous trek (just to cover about 1.2 Km) through some extra-ordinary terrain to reach the remarkable cave temple of our royal ancestress, [रेणुका, the wife of जमदग्नी](http://manollasa.blogspot.com/2006/03/death-of-renuka.html)[.]{style="font-weight:bold;"} After the darshan of the large orange painted image of रेणुका, we climbed further uphill passing an abandoned shrine of the pastoral horse-riding deity खण्डोब. In the days of the Holkar it was a popular shrine of the winter full-moon festival of the deity, but now largely in disuse. There was the orange aniconic rock representing [[पदुबई (पद्मावती under a tree) ]{style="font-weight:bold;"}](http://manollasa.blogspot.com/2006/04/countryside-expeditions.html)as usual. We found a flat rock in the vicinity and placing our safety on the weapons carried by the शूद्र and the feral brahmin we retired for the day. Waking up before dawn we caught sight of the Centaurus A galaxy low on the southern sky through the binoculars and then scurried to the village for a wash and resumption of the campaign.

We packed our supplies and marched first about 1.6 Km from the township to reach the glorious summit of ऐन्द्री to visit the hoary temple of our hallowed goddess, the shaktI of the greatest of the gods, the wielder of the thunderbolt. We spent about 1/2 hour there as we had to attend to some tantric rituals, even as shudra and SS spied the path for our following adventure. We then followed a spur leading from the massif of ऐन्द्री to ascend the principal southern ridge of the Satmala through a difficult and hair-raising climb. After a long walk along the ridge we reached the glorious indragiri or the hill of indra. The top of the fort itself is not easily accessed but close to the pinnacle there were many points of interest. There were several deserted caves mostly defiled with dung of cattle and sheep, but all had a variety of carvings of the distant pre-यादव as well as yadava periods . Some of them had worn images of rudra and indra, while others had the images of either the clothed or the naked nAstika. In the lower path leading to the citadel was a more recent shrine of the mighty वानर. We traced the the path of the British assault on the area during the 1818 war in which they conquered the country. The evil Britons first attacked the hill of Tryambaka that adjoins the famous shrine of tryambakeshvara and conquered it to set a base there. From there a column under Briggs attacked the forts of Rajdher and Koldher on the northern ridge of the Satmala, while another column marching under MacKintosh attacked Indragiri. Briggs first took Rajdhair (which could be seen on the distant horizon) demolished the fortifications and burnt down the settlement of Maratha garrison there. Then Mackintosh detonated the large rampart and tower on Indragiri, as result the latter fort was also taken. Here too the Britons demolished the access to the fort making it a reasonably difficult climb to attempt the top.

We then retreated via Kothure back to our base. In the aforesaid village we saw a temple of the typical पाञ्चायतन type with rudra in the middle surrounded by विनायक, the transfunctional shaktI, विष्णु and sUrya. In organization it is similar to the one built by the Peshwas in their city. There is also another shrine probably of the same period with a large वानार idol.


