
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The श्मशान of kollagiri](https://manasataramgini.wordpress.com/2009/05/10/the-shmashana-of-kollagiri/){rel="bookmark"} {#the-शमशन-of-kollagiri .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 10, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/05/10/the-shmashana-of-kollagiri/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Beating our Damaru we advanced to the great ground of kollagiri;\
We recited the face mantra-s of the archer ranging on the high adri;\
In our आज्ञ chakra stood the bright पिनाकिन्, famed as para-shiva;\
In his embrace was clasped chit-parA, the fair queen of bhairava.

To transcend pain and pleasure and to rest in the of ocean of bliss;\
We translocated to अम्बिका's site, the मेलापक, in the manner of vIra-s;\
We raised our arm and displayed the Chomma of the two horns;\
We waited for her, who makes one a siddha, to come in our arms.


