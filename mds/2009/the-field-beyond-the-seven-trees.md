
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The field beyond the seven trees](https://manasataramgini.wordpress.com/2009/10/19/the-field-beyond-the-seven-trees/){rel="bookmark"} {#the-field-beyond-the-seven-trees .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 19, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/10/19/the-field-beyond-the-seven-trees/ "6:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Sporting with the दूती we reached the spot where seven trees stood in a row. Those were verily like the trees which had been denuded of their leaves by fist of the tawny ape वालिन्. Our दूती who was verily a madadrava said that beyond these trees was the spot of मृत्यु, where yama bore away those whose term had come. She added it was verily the raurava hell on earth, where, before completing their term, people suffered the tortures of vaivasvata's भृत्य-s. For them the coming of the महिषवाहन was not a cause of sorrow but the eternal relief from the ordeals of chitragupta. Riding our fleet ashva-s we reached the spot beyond the seven trees and the seat of धर्मराजन्-s play. At that far away spot were cooped up the रक्ष-s of भृगु, they who were sent forth during the destruction of दक्ष's adhvara. We went beyond it to the vast श्मशान. In our mind we intoned the words to the wielder of the पाशुपत missile. Beyond it was a vast field. The great kShatriya of light pulled down the नक्षत्र of the day even as the kShatriya of the night, who was praised by शुनःशेप in the days of yore, unfurled the ऋक्ष in the high skies. We saw the birds sweeping their heads to catch the magnetic lines and take of in the direction known by that great kShatriya with the pAsha. In that great field with knowledge of the pAsha-s of yama and the great asura prachetas we lay in unison with her whose hair was the like the array of monsoonal clouds of massed by indra against the wall of the सह्याद्रि. On one side was the gandharva known as मृत्यु on the other side was the gandharva विश्वावसु. But we held on to her whose stanau were like the pots of surA made ready for the ashvinau at the सौत्रामणि rite, and whose smell was like the यक्ष's saugandhika flower. Delighting in rati but ever aware of the buffalo-rider who was at hand we were drinking the honey of अनङ्ग's cup. In the state of the urdhvasravas we took up the mantra of the circle of the योगिनी-s headed by विलिस्तेङ्गा, चतुष्पथनिकेता and शिशुमारमुखी.


