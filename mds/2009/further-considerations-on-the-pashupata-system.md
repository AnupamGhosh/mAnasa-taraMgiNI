
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Further considerations on the पाशुपत system](https://manasataramgini.wordpress.com/2009/07/31/further-considerations-on-the-pashupata-system/){rel="bookmark"} {#further-considerations-on-the-पशपत-system .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 31, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/07/31/further-considerations-on-the-pashupata-system/ "6:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Though it is at the foundation of all the shaiva systems that survive to date, the old पाशुपत darshana has all but become extinct. Of the पाशुपत sUtra-s we have only 3 reported extant manuscripts thus far: two from वाराणसि and one from the वङ्ग country. Two of these manuscripts are fragmentary and the third though complete is poorly preserved. There appears to be another possible fragmentary manuscript from the tamil country that has not been studied at all (I have only seen the title and not examined it closely). Using two of the manuscripts our coethnic R. Ananthakrishna Shastri produced the first edition of the पाशुपत sUtra-s in the first half of the 1900s. A few years ago, the white indologist Bisschop produced several useful corrections based on the 2nd वाराणसि manuscript and other comparisons. These sUtra-s are the only testimony we have of the पाशुपत system that bridges the gap between the earlier layers \[footnote 1] and the rise of the लाकुलीश पाशुपत system and its later कालामुख successor. The sUtra-s as we have them come with the भाष्य of a certain कौण्डिन्य, who tradition records to be a successor of लकुलीश. This भाष्य also quotes a version of the sUtra-s that shows differences from the version transmitted as a मूल-सूत्रपाठ. This shows that the पाठ used by कौण्डिन्य was transmitted independently, but accompanied by the मूल-सूत्रपाठ, without any "gene conversion-like event" to equalize the two readings. Further, given the way the sUtra-s are organized to go along with the भाष्य, it is possible that some of the sUtra-s as we have them are only fragments of more complete sUtra-s that were extracted from an earlier version of the कौण्डिन्य भाष्य or some other pre-कौण्डिन्य भाष्य \[It is possible that the more complete sUtra-s resembled the sUtra-like prescriptions of the पाशुपत vrata in the AV परिशिष्ठ 40]. This is suggested by the form of certain sUtra-s, where they were clearly parts of longer coherent sUtra-s broken for commentarial purposes:\
[*मूत्र-पुरीषं नावेक्शेत् ।\
स्त्री-शूद्रं नाभिभाषेत् ।\
यद्य् अवेक्षेद् यद्य् अभिभाशेत् ।\
उपस्पृश्व ।\
प्राणायामं कृत्वा ।\
रौद्रीं गायत्रीं बहुरूपीं वा जपेत् ।*]{style="color:#99cc00;"}*

  -  Here the पाशुपत ascetic is prescribed not to see urine or feces and not to converse with women and शूद्र-s. If he does see the former two or talk or touch the latter two he performs प्राणायाम and does japa of the rudra gAyatrI or the बहुरूपी (i.e. aghora) mantra. We notice that in बौधायन dharma there is a single sUtra: [स्त्री-शूद्रैर् नाभिभाषेत मूत्र-पुरीषे नावेक्षेत]{style="color:#99cc00;"}. Similarly, the remaining sUtra-s cited above might also be combined into a single one. Thus, transmission of the sUtra-s as we have them today does seem to be affected by the tradition of commentary and are probably not the original form of them.

Despite these issues we can clearly see that the sUtra-s are of an archaic character and resemble in large part the kalpa sUtra-s with some features of the darshana sUtra-s. Yet unlike the kalpa or darshana sUtra-s they do not bear the name of any author. Though certain traditions mention लकुलीश as an author, there is absolutely nothing what so ever to support this in the form of internal evidence from the sUtra-s. Even कौण्डिन्य's commentary merely states the following:\
[तथा शिष्ट प्रामाण्यात् कामित्वाद् अजातत्वाच् च मनुष्य-रूपी भगवान् ब्राह्मण-कायम् आस्थाय कायावत-रणे अवतीर्ण इति । तथा पद्भ्याम् उज्जयिनीं प्राप्तः ।]{style="color:#99cc00;"}[\
]{style="color:#99cc00;"} i.e. shiva incarnated in the form of a man by entering the body of a ब्राह्मण in the forest of कायावत. Then he walked to Ujjain. This account matches those narrated in the पुरण-s and the कार्वण माहात्म्य where लकुलीश incarnates in कायावरोहण (modern Karvan village some distance north of Baroda). However, unlike the latter accounts the name लकुलीश is never mentioned even though in the subsequent lines कौण्डिन्य mentions that shiva as the ब्राह्मण imparted the शास्त्र to the student kushika. Only in the रत्नटीका on the गणकारिका (probably authored by भासर्वज्ञ) do we clearly see a mention of लकुलीश as the founder of the पाशुपत system. This raises questions regarding लकुलीश being the actual composer of the sUtra-s.

Using the पाशुपत sUtra-s one can look for key features of the पाशुपत ritual described in it in the earlier texts to trace the early evolution of the shaiva system.\
**1) yoga and सांख्य**: As we have discussed before on these pages the practice of yoga goes back to the earliest shaiva layer in the form of the श्वेताश्वतर and even there is accompanied by the philosophical framework provided by सांख्य. This connection to yoga continues through the atharvashiras and the epic period (MBH13.14 shiva is identified with the puruSha of सांख्य). yoga remained a persistent and central theme of shaiva practice which eventually gave rise to the yoga of the mantra मार्ग. Beginning around the time of the पाशुपत sUtra-s, elements of वैशेषिक are adopted by the system. Eventually nyAya-वैशेषिक comes to dominate the अतिमार्ग shaiva thought while सांख्य remains at the foundation of most mantra-मार्ग shaiva philosophy.

**2) The rite of bhasma स्नान:** The पाशुपत sUtra prescribes the practitioner to smear himself with ashes. This rite is also found in the atharvashiras and the AV-pari 40 suggesting that it was an old ritual of the shaiva system which probably evolved from the vedic ritual of smearing with ashes of a homa seen in the गृह्य daily offering. It is not specifically mentioned in the श्वेताश्वतर. It persisted amongst all later shaiva branches.

**3) Iconic and temple worship of shiva**: These are clearly present in the पाशुपत-sUtra-s. We see a similar mention of iconic shiva worship and temples in the पाशुपत vratra of the AV-pari 40. In both these texts we note that: a) the पाशुपत wears rudra निर्माल्य; b) he dances, sings, plays musical instruments and laughs loudly; c) he is asked to hangout at rudra shrines. However, the fire worship is a major component in the rite of the AV-pari 40, and one of the places the पाशुपत might hang out is a fire house (अग्न्यागर). In the account of the पाशुपत Ashrama in the Mbh13.14 (vulgate) we encounter the presence of large fire altars with kusha grass strew around them ([विशालैश् चाग्नि-शरणैर्-भूषितं कुश संवृतम्]{style="color:#99cc00;"}) and the mention of divine women singing songs. In the same section rudra himself is described as dancing, singing and playing instruments. There is no sign of iconic worship in the atharvashiras or the SU. The early iconic worship as suggested by the AV-pari 40 appears to have mirrored those found in the late गृह्य texts like those of वैखानस and बौधायन. Throughout later Hindu history shiva temples of पाशुपत-s were prominent monuments on the landscape of भारतवर्ष and even left their impression on the visiting nAstika चीनाचर्य-s. Interestingly, the survival of classical music and dance in दक्षिणपथ might have hugely benefited from the patronage they received from the कालामुख successors of the पाशुपत-s.

**4) Male ब्राह्मण-s only:** The original पाशुपत vrata appears to have been a solely male ब्राह्मण affair. This is attested in both the PअS and the AV-pari 40. The SU and AS also appear to be ब्राह्मण productions indicating that it arose specifically amongst ब्राह्मण vedic ritualists (not even other dvija-s). Only in the post-लकुलीश phase of its development it opened up to include the 3 other वर्ण-s. By the कालामुख phase we notice the involvement of women as patrons and dancers in the temples.

**5) Wearing of लिङ्ग:** We note this first in the PअS 1.6 [[*लिङ्गधारी]*]{style="color:#99cc00;"}. We see no mention of this in the AV-pari 40 or the earlier उपनिषद् texts. The earliest allusions to the लिङ्ग as an emblem of rudra appear in the great epic (अनुशासन parvan). It is hence conceivable that the PअS belongs to this period when लिङ्ग worship had come to the fore amongst the shaiva-s.

**6) Observance of govrata/pashuvrata:** This is mentioned in PअS 5.9 [[गोधर्मा मृगधर्मा वा]{style="color:#99cc00;"}]. It finds a mention in the epic अनुशासन parvan version of the पाशुपत practice (Mbh 13.14 vulgate) where it is described that some पाशुपत-s observe the govrata and others the मृगव्रत just as in the PअS. However, these vrata-s are not described in the AV-pari 40 or the earlier उपनिषद्-s.

**7)पञ्चब्रह्म:** A key feature of the पाशुपत sUtra-s is their link to the पञ्च brahma-mantra-s around which a number of comparisons can be made. Each of the five sections of the पाशुपत sUtra-s end with one of the brahma-mantra-s. However, the पञ्च brahma mantra-s have distinct origins. The rudra gAyatrI is part of the शतरुद्रीय of both the मैत्रायणीय and kaTha saMhitA-s, whereas the aghora (बहुरूपी) is part of the शतरुद्रीय of the मैत्रायणीय-s. These two mantra-s are used in these कृष्ण-yajurvedic traditions as a part of the shrauta ritual of the pacification of rudra after the 5th layer of the agni-chiti is laid out in a soma sacrifice. They are known by the technical names rudra sAvitrI and बहुरूपी in the पाशुपत system. Both of them are deployed in the AV-pari 40 पाशुपत ritual but the other 3 brahma mantra-s are not mentioned in this version of the ritual. The mantra of सद्योजात occurs as a कुमार mantra in the atharvavedic tradition as a part of the skanda-याग but is not deployed in rudra worship in any परिशिष्ठ tradition. Only in the तैत्तिरीय AraNyaka (महानारायणोपनिषत्) and its atharva-vedic counterpart the 5 brahma-mantra-s are seen as a group. In the former it is occurs after the लिङ्ग स्थापन mantra-s, but this group of लिङ्ग mantra-s is missing in the AV-महानारायण. In ritual terms, the first mention of the पञ्च brahma mantra-s is seen in the sections on iconic worship of rudra in the बौधायन गृह्य sUtra-s. Here, three rituals for the iconic worship of rudra are described (BGS 2.16-18). The first two for the installation and daily worship of rudra images cite several rudra mantra-s but not the पञ्चब्रह्म.Interestingly these have mantra-s similar to those deployed in the AV pari-40 पाशुपत rite. The third of these, termed ritual of the bathing of rudra (the precursor of the famous महान्यास) provides the deployment of the पञ्चब्रह्म: the सद्योजात mantra is used in आसनं, पाद्यं, अर्घ्यं, the वामदेव in सर्वोपचार पूज, the aghora mantra in installing the benign form of rudra, the tatpuruSha in japa and ईशान in seeking the grace of rudra. A further ritual for the pacification of rudra on अष्टमी evenings known as the ईशानकल्प (BGS 3.15) also does not mention the पञ्चब्रह्म even though it deploys several rudra mantra-s.The absence of the 5 brahma mantra-s as a pentad in the AV pari-40 पाशुपत system, but the presence of only the two older members of the brahma mantra-s in this system points to an internal evolution within the पाशुपत system. The brahma-mantra-s as a group are seen only in the बौधायन रुद्राभिषेक and the महानारायण उपनिषत्-s of the तैत्तिरीय tradition (i.e. TA) and the AV (without लिङ्ग) among the vedic texts. Hence, it appears quite possible that the brahma mantra-s as a group emerged in the context of the early iconic worship of rudra (not necessarily as a लिङ्ग) in the late vedic tradition after the foundation of the initial पाशुपत system. The पाशुपत sUtra-s specifically share only with the तैत्तिरीय AraNyaka both the brahma-mantra-s and the लिङ्ग worship. This suggests that the beginning of the पाशुपत sUtra tradition is later than that of the AV pari-40 and specifically emerged amongst the तैत्तिरीयिन्-s. To further test this conjecture let us look at the brahma-mantra-s as transmitted by the पाशुपत sUtra-s and its भाष्य:

PSउ:\
[*सद्योजातं प्रपद्यामि सद्योजाताय वै नमः ।\
भवे भवे नातिभवे भवस्व माम् भवोद्भवाय नमः ॥*]{style="color:#99cc00;"}*

  -  TA:

  - [सद्योजातं प्रपद्यामि सद्योजाताय वै नमो नमः ।\
भवे भवे नातिभवे भवस्व माम् भवोद्भवाय नमः ॥]{style="color:#99cc00;"}*\
कौण्डिन्य भाष्य:\
[*सद्योजातं प्रपद्यामि सद्योजाताय वै नमः ।\
भवे भवे नातिभवे भजस्व माम् भवोद्भवः ॥*]{style="color:#99cc00;"} (Regular अनुष्टुभ्)\
PSउ, TA:\
*[वामदेवाय नमो ज्येष्ठाय नमः श्रेष्ठाय नमो रुद्राय नमः कालाय नमः कलविकरणाय नमो बलविकरणाय नमो बलाय नमो बलप्रमथानाय नमः सर्वभूतदमनाय नमो मनोन्मनाय नमः ॥]{style="color:#99cc00;"}*\
कौण्डिन्य भाष्य:\
[वामदेवाय नमो ज्येष्ठाय नमो रुद्राय नमः कालाय नमः कलविकरणाय नमः। बलप्रमथानाय नमः सर्वभूतदमनाय नमो मनोन्मनाय नमः ॥]{style="color:#99cc00;"}

PSउ, TA:\
[अघोरेभ्योऽथ घोरेभ्यो घोर-घोरतरेभ्यः । सर्वेभ्यः सर्व-शर्वेभ्यो नमस् ते अस्तु रुद्ररूपेभ्यः ॥]{style="color:#99cc00;"}\
कौण्डिन्य भाष्य:\
[*अघोरेभ्योऽथ घोरेभ्यो घोरघोरतरेभ्यश् च ।\
सर्वतः शर्व सर्वेभ्यो नमस्ते ऽस्तु रुद्ररूपेभ्यः ॥*]{style="color:#99cc00;"} (Regular अनुष्टुभ्)\
PSउ, TA, कौण्डिन्य भाष्य:\
[*तत्पुरुषाय विद्महे महादेवाय धीमहि । तन्नो रुद्रः प्रचोदयात्

  - ]{style="color:#99cc00;"} PSउ, TA:\
[*ईशानः सर्व-विद्यानाम् ईश्वरः सर्वभूतानां ।\
ब्रह्माधिपतिर् ब्रह्मणोऽधिपतिर् ब्रह्मा शिवो मे अस्तु सदाशिवॊम् ॥*]{style="color:#99cc00;"}*

  -  कौण्डिन्य भाष्य:\
[*ईशानः सर्व-विद्यानाम् ईश्वरः सर्वभूतानां ।\
ब्रह्मणोऽधिपतिर् ब्रह्मा शिवो मे अस्तु सदाशिवः ॥*]{style="color:#99cc00;"} (Regular अनुष्टुभ्)

There is some variation even within the different transmissions of the तईत्तिरीय AraNyaka 10 but the version here is the most widely used oral tradition in भारतवर्ष. It is immediately apparent that the version in the पाशुपत sUtra-s is closest to the version in the TA 10. This further supports the specific relationship between the पाशुपत sUtra-s and the TA 10 पाशुपत tradition. This version of the mantra-s can also been seen in the लिङ्ग़ पुराण (2.27.245-256) in the context of the famous जयाभिषेक rite performed by a shaiva मन्त्रवादिन् for a राजन् desiring victory over his enemies. While all mantra-s are not given in full here, those that are closely match the PSउ form indicating that this transmission continued to exist in the shaiva world outside of the TA and the PSउ. In contrast, the version transmitted by the भाष्य is: 1) in the form of metrically regular अनुष्टुभ्-s, were relevant and 2) closer to other texts than the TA. In the case of the सद्योजात and aghora mantra-s the भाष्य form follows the reading in the धूर्त kalpa of the AV pari-20 and the मैत्रायणीय saMhitA respectively. Further, other major transmissions of the brahma mantra-s in the mantra-मार्ग shaiva and vaiShNava पाञ्चरात्र systems closely resemble the version in the भाष्य. In the early siddhAnta tantra the निःश्वास saMhitA, where the mantra-s are cited in full, they follow the भाष्य version. The Indonesian mantra manual (now extant only in Bali) also gives these mantra-s in a form close to the भाष्य version. The Indonesian manual is derived from a mantra-मार्ग shaiva source, most probably a siddhAnta tantra, supporting this version being transmitted by the सैद्धान्तिक-s. The version transmitted by the पाञ्चरात्रिक सनत्कुमार tantra also reads very close to the भाष्य version. Thus, कौण्डिन्य was not using the form given in the मूल-sUtra पाठ or the TA but his own version that has also widely been transmitted in successor texts. There is a hint that कौण्डिन्य was not a तैत्तिरीयक and did not know of that these mantra-s were part of the तैत्तिरीयक shruti. In explaining the sUtra 1.39 ([अत्रेदं ब्रह्म जपेत् ।]{style="color:#99cc00;"} in PSउ; [अत्रछेदम् ब्रह्म जपेत् ।]{style="color:#99cc00;"} in भाष्य) he says (for the word idam):\
[इदम् इति प्रत्यक्षे । नियोगे वा । इदम् एव ब्रह्म जपतव्यम् । ऋग्-यजुः-सामानीत्यर्थः ।]{style="color:#99cc00;"}*

  -  idam means in the current place an injunction (niyoga) to do japa of this \[i.e. सद्योजात] mantra and not a ऋक्, yajur or a sAman. Thus, it appears that he does not consider the सद्योजात mantra a vedic one. Elsewhere, he does consider it ऋक् in the sense of being a metrical formula (e.g. in भाषय to Psu 5.21) suggesting that in the above case he means it is not a mantra from one of the 3 veda-s supporting the idea that he was unfamiliar with the TA 10. Thus, interestingly कौण्डिन्य's transmission is independent of the PSउ, which belongs to the same tradition as TA 10.

In conclusion, we note that the पाशुपत sUtra-s are not the beginning of the पाशुपत system but are preceded by a number of pre-sUtra पाशुपत teachings. The textual comparisons show that PSउ पाशुपत darshana is closest to the version practiced in the माहाभरत and shows distinct links in the form the brahma mantra-s and the लिङ्ग worship with the तैत्तिरीय AraNyaka's last section. Though, the पाशुपत system arose early from the ancestral Indo-Aryan ascetic tradition, it appears to have interacted with the developing iconic worship of shiva, particularly in terms of incorporating the brahma mantra-s. In terms of the evolution of the brahma mantra-s, we note that the बहुरूपी and the rudra-gayatrI were recruited earlier and the सद्योजात mantra emerged in the iconic worship of कुमार. But the brahma mantra-s as group appears to have been put together in the late vedic period for the iconic worship of rudra. This tradition of worship with the पञ्चब्रह्म appears to have been particularly vigorous amongst the तैत्तिरीयक-s and was incorporated into their vedic corpus. It is from this तैत्तिरीयक tradition that the मूल-sUtra पाठ that we have appears to have emerged. But the presence of distinct sUtra and पञ्चब्रह्म readings in the कौण्डिन्य भाष्य indicate that there was a transmission independently of the तैत्तिरीयक-s that was partially normalized in the post-कौण्डिन्य transmission. The great epic pashupata system is likely to have been coeval or post-dated the Ur-पाशुपत sUtra-s but predated the कौण्डिन्य भाष्य with the narrative of shiva animating a ब्राह्मण's body.

  - ----------------------------------------------------------------- ------------------------------------------------------------------
  development                                                        text of first appearance
  बहुरूपी, रुद्रगायत्री                                                   कृष्ण yajurveda
  worship of rudra as a great god                                    नीलरुद्र, श्वेताश्वतर
  practice of yoga                                                   श्वेताश्वतर
  identification of rudra with puruSha of सांख्य. brahman of vedAnta   श्वेताश्वतर, kena
  rite of ash                                                        atharvashiras
  पाशुपत vrata                                                        atharvaveda परिशिष्ठ-40
  iconic worship of rudra                                            atharvaveda परिशिष्ठ-40, बौधायन गृह्यसूत्र-s, तैत्तिरीय AraNyaka 10
  singing, music, dancing                                            atharvaveda परिशिष्ठ-40
  bearing लकुट, daNDa, खट्वाङ्ग                                         atharvaveda परिशिष्ठ-40, ancestral संन्यासोपनिषत्
  पञ्चब्रह्म mantra-s                                                   तैत्तिरीय AraNyaka 10, बौधायन गृह्यसूत्र-s, AV-महनारायण, पाशुपत sUtra-s
  लिङ्ग worship/wearing                                               तैत्तिरीय AraNyaka 10, पाशुपत sUtra-s, महाभारत
  govrata/मृगव्रत                                                      पाशुपत sUtra-s, महाभारत, ancestral संन्यासोपनिषत्
  simulated madness/हुद्दुकार                                           पाशुपत sUtra-s, महाभारत, ancestral संन्यासोपनिषत्
  shiva animating ब्राह्मण's corpse                                    कौण्डिन्य भाष्य
  लकुलीश and his iconography                                          पुराण-s

  - ----------------------------------------------------------------- ------------------------------------------------------------------

footnote 1: In our reckoning the first proto-पाशुपत layer is represented by the late vedic material such as the श्वेताश्वतर (early first layer; of course the parallel नीलरुद्र is drawn from the AV-P saMhitA itself ) and atharvashiras (late first layer; perhaps also includes the सामवेदिc kena). The पाशुपत ritual of the AV-परिशिष्ठ appears in the form of sUtra-like prescriptions and might correspond to a pashupata practice immediately subsequent to the first layer. This was then followed by the पाशुपत system described in the great epic. The system in the AV-pari 40 also appears to represent a precursor of the system expounded in the पाशुपत sUtra-s. (see above for further discussion).


