
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the genes](https://manasataramgini.wordpress.com/2009/04/01/in-the-genes/){rel="bookmark"} {#in-the-genes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 1, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/04/01/in-the-genes/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

My माताश्री was reading out a piece of काव्य written by one of us. I remarked it must be composed by the muni. But she claimed it was written by me. This sort of puzzled me because I had no recollection of ever having written it and it had so much lexical concordance with some of the muni's pieces. She pointed to an annotation made on it by you would not believe whom (Did you guess it, since you read these pages) -- this made me feel a slightly embarrassed. But then it also marks the earliest evidence of the journey, which was to culminate in some of the greatest conquests thus far. Then I realized that the insipid versification and utter failure at alliteration did point to me, rather than a real kavI like the muni (He is a भरद्वाज, while am a mere शंयु :-)). Yet, the similarity in our words and metaphors was striking --whether we wrote on scientific or daiva themes. Then she read out a पाञ्चरात्रिc काव्य of the muni which completed this convergence of metaphor. While not as educated as me on the prayoga विषय-s of this branch of the mantra-शास्त्र, it became clear that our intuitive understanding of the very heart of पाञ्चरात्रिc mantra-s was congruent. Interestingly, the muni had also composed an elaborate श्रीकुल काव्य, which I seem to have unfortunately lost. Here again in his poem I could feel the congruency of the imagery of मन्त्रिणी, वाराही, bAlA or the most high त्रिपुरा in his words with that which only exists in my mind when enlivened by the said देवता. It then struck me that in the days of my versification I never shared it with the muni and I was not around when he began versifying. Yet the convergences of content and mantra vision (or देवता vision as the muni might tend to term it) that we realized post facto are quite striking. It was then I realized this vector derives from the genes.


