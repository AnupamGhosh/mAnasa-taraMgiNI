
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mouse, the rat and the wolf](https://manasataramgini.wordpress.com/2009/01/23/the-two-mice-and-the-wolf/){rel="bookmark"} {#the-mouse-the-rat-and-the-wolf .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 23, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/23/the-two-mice-and-the-wolf/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were chasing one mouse. One rat was chasing us and so was a wolf. For the mouse and the rat we placed pieces of cheese as baits. The wolf wanted to make us its meal. The mouse we were chasing sniffed the cheese. So by वरुण's eye we could see it. The rat that was chasing us also sniffed the cheese so we saw by वरुण's eye. Of the wolf we could only see the hazy form and hear the constant growling. The mouse we were chasing laughed at us thinking we had not seen it. But it was we who knew all about it. The rat knew we could see it but was bold enough to be undeterred. We did not know if might become a meal for the wolf sometime soon. If we did you will not hear from us here again. But before the wolf eats us, even if we were to be eaten we might talk a little: चण्डमहारोषण, महाकाल and atharva-शबरी.


