
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [nAstika notes](https://manasataramgini.wordpress.com/2009/01/31/nastika-notes/){rel="bookmark"} {#nastika-notes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 31, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/31/nastika-notes/ "7:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R1's father was not the kind like the prudish modern Hindu who was shy of laying out the realities of साधन and the subject matter of certain शास्त्र-s to me. Yet he was not exactly thrilled with the prospect of discussing nAstika tantra-s with me even in the privacy of his study. He had considerable practical knowledge about them [having faced them in action during his life in Nepal](https://manasataramgini.wordpress.com/2006/10/05/the-encounter-with-the-pashanda-s-of-the-nastika-mata/){target="_blank"}. But still he feared in those days that it might have a "corrupting" influence on a youngster like me :-) and perhaps he felt it was socially unacceptable and seen as irresponsible. Still he had an inkling that I was even then sort of beyond such dualities, at least in the realm of study. He had a collection of nAstika tantra-s he had amassed with a friend of his from the वङ्ग country, but generally kept it secret. Nevertheless, after giving me a basic introduction, he said studying them was up to me and it was best never discussed in public. I studied them to a degree laboring with their भाषा --- I could have benefited much from his erudition in संस्कृत and its vulgar derivatives \[e.g. the use of bhonti or bhoti for bhavanti or bhavati -- a clear vulgarity of language associated with the प्राच्य-s initially puzzled my unerudite mind]. Nor could I discuss the issues with two learned तान्त्रिक-s, scholars of the देवभाषा, about this matter because these tantra-s were like ordure to them. Nevertheless, R1's father did perceptively give me assistance in matters of संस्कृत in course of the process without ever explicitly discussing the material in these nAstika tantra-s. As I mentioned before, in my study I was learning of the vast web of lateral transfer between the nAstika and sectarian Astika sources and had a thought of putting it down more systematically. I learned at that point of the scholarly efforts of white scholars e.g. Sanderson and Dyczkowski along some similar lines and wondered if it was worth doing it at all, especially given the enormous magnitude of the task, and the insufficiency of our education. In any case I make a few notes even if the conceived synthetic overview is no where near.

In the चण्डमहारोषण tantra (CMT), a nAstika योगिनी tantra, the mantra for invocation of the देवता into the मण्डल is thus:\
[ॐ श्री-चण्डमहारोषण सर्वपरिवार-सहित-आगच्छ २ जः हुं वं होः अत्र मण्डले अधिष्टानं कुरु हूं फट् स्वाहा ॥]{style="color:#99cc00;"}\
Then the CMT adds: [अनेनाकृष्य प्रवेश्य बद्ध्वा वशिकृत्य पुयजेत् ॥]{style="color:#99cc00;"}\
The साधक here is actually conceives the mantra as attracting, causing to enter, binding and controlling the deity in the मण्डल. The bIja-s जः=आकृष्य:अङ्कुश; hUM=pravesha:pAsha; वं=बद्ध्वा:tAla; होः= वशिकृत्य:vajra. This action of actually capturing the deity into the मण्डल is something which seems to have rubbed on to the praxis of the Astika तान्त्रिक-s in Nepal and to a degree even in the chera country, where they conceive the deities as being captured and held in मण्डल-s (see note on gajapati, the Nepali tantrika). There is even an apocryphal myth in the hagiographies of अद्वैताचर्य शङ्कर who is imagined to have captured shAradA in Kashmir and installed her in a मण्डल in श्रिङ्गगिरि. However, this conception is generally absent in regular Astika मण्डल स्थापन (there is only entry of the deity).

The CMT also has a specific procedure of दीक्ष or अभिषेक for a female student:\
[स्त्रीणां तु मकुटाभिषेकं त्यक्त्वा सिन्धूराभिषेकं दद्यात् ।\
पट्ट-महादेवी-रूपां शिष्यां आलम्ब्य ।\
ॐ भगवति आविश आविश अस्या हृदये हुं फट् ।\
लौहादिकार्त्तिकां तस्या दक्षिण-हस्ते दद्यात् ।\
ऒं कार्त्तिके सर्वमाराणां माम्सं कर्तय कर्तय हुं फट् ।\
वामहस्ते नृकपालं दर्वादिकृतं दद्यात् ।\
ॐ कपाल सर्वशत्रूणां रक्तं धारय धारय हूं फट् ।\
ततो भगवती-मुद्रयोपवेश्य तदाकारेण चालम्ब्य ।\
ॐ हे श्री-द्वेष-वज्री सिद्धा त्वं हूं फट् ।\
एवं स्त्रियः कृष्णादि-वर्ण-भेदेनेन पञ्चयोगिनीनां नाम्नाभिषिञ्चेत् ।]{style="color:#99cc00;"}

Here the तान्त्रिक should lead the female aspirant dressed like a queen by his hand and invoke the भगवती into her heart (the consort of चण्डमहारोषण). Then he makes her hold a chopper in her right hand utters the mantra to cut the flesh of all the mAra-s. Then he gives a human skull and ritual ladle in her left hand and utters the mantra to hold the blood of all the enemies in the skull. Then he makes her bear the signs (pose) of the भगवती and holding her hand in hand he invokes one of the five योगिनी-s into her according to her complexion and bestows on her the name of that योगिनी.\
द्वेषवज्रि- black; mohavajri- white; pishunavajri- yellow; रागवज्रि-red; ईर्ष्यावज्रि- grey.

The CMT also gives a long list of female entities who should be worshiped as the forms of the bhagavati:\
[यावद् धि धृश्यते लोके स्त्री-रूपम् भुवनत्रये ।\
तन् मदीयं मतं रूपं नीछानीछ-कुलं गतं ॥\
देवी चासुरी चैव यक्षिणी राक्षसई तथा ।\
नागिनी भूतिनी-कन्या किन्नरी मानुषी तथा ॥\
गन्धर्वी नारकी छैव तिर्यक्-कन्याथ प्रेतिका ।\
ब्रह्माणी क्षत्रिणी वैश्या शूद्रा चात्यन्तविस्तारा ॥\
कायस्त्री राजपुत्री च शिष्टिनी करौत्तिनी ।\
वणिजिनी वारिणी वेशया च तरिणी चर्म-कारिणी ॥\
कुलत्रिणी हत्रिणी दोंबी चाण्डाली शवरिणी तथा ।\
धोबिनी सौण्डिनी गन्धवारिणी कर्म-कारिणी ॥\
नापिणी नटिनी कंस-कारिणी स्वर्ण-कारिणी ।\
कैवर्ती खटकी कुण्डकाऋइणी छापि मालिनी ॥\
कापालिनी शंखिनी चैव वरुडिनी च केमालिनी ।\
गोपाली काण्डकारी च कोचिनी च शिलाकूटी ॥\
थपतिनी केशकारी च सर्व-जाति-समावृता ।\
माता छ भगिनी भार्या मामिका भागिनेयिका ॥\
खुट्टिका छ स्वसा छैव अन्या छ सर्व-जातिनी ।\
व्रतिनी योगिनी छैव रण्डा छापि तपस्विनी ॥]{style="color:#99cc00;"}

This long list of "professions" is of some historical interest. It is dubious if women had all these professions -- some might are simply feminines of standard professions. This is probably true of some names like कायस्त्री and करौत्तिनी (female scribes or tax-collectors? unlikely. Females of scribes and tax-collectors). My discreet discussion with the paNDita suggested an emendation of कुलत्रिणी to कुलत्थ्रिणी: a maker or vendor of collyrium or an eye-shade. The हत्रिनी could be taken to be a murderess but more likely it is a form for हस्त्रिणी or an elephant-tender. The कपालिनी is reminiscent of the कापालिका-s encountered in संस्कृत plays.

[अत्रानुरक्ता ये तु सत्त्वाः सर्वदिक्षु व्यस्थिताः ।\
देवासुरा नरा नागास् ते ।अपि सिद्ध्यन्ति साधकाः ।\
अथैवं श्रुत्वा महेश्वरादयो देवा गौरी-लक्ष्मी-शची-रत्यादि-देवतीं ग्ऱ्‌^हीत्वा भावयितुं आरब्धाः ।\
अथ तत् क्षणं सर्वे तदैवं तन् मुहूतकं चण्डरोषण-पदं प्राप्ता विचरन्ति महीतले ।\
तत्र महेस्वरो वज्रशङ्करत्वेन सिद्धः । वासुदेवो वज्रनारायणत्वेन । देवेन्द्रो वज्रपाणित्वेन । कामदेवो वज्रानङ्गत्वेन । एवम्प्रमुखा गञ्गा-नदी-बालुका-समा देवपुत्राः सिद्धाः ।\
पञ्च-काम-गुणोपेताः सर्व-सत्त्वार्थकारकाः ।\
नानामूर्तिधराः सर्वे भूता मायाविनो जिनाः ।\
यथा पङ्कोद्भवं पद्मम् पञ्कदोषैर् न लिप्यते ।\
तथा रागन्योद्भूता लिप्यन्ते न च दोषकैः ॥]{style="color:#99cc00;"}

This represents a clear attempt of the nAstika to digest the Astika deities into his scheme by the process of "vajrification". The bodhisattva states that all the Astika deities headed by maheshvara along with their wives गौरी, लक्ष्मी, शची, rati etc (notice the vulgar bauddha भाशा: "देवतीं" among other vulgarities of the भाषा) have become at the very instance of practicing the चण्डमहारोषण teachings one with that deity. Then they moved about in the earthly sphere as वज्रशङ्कर, वज्रनारायण, वज्रपाणि, वज्रान्ङ्ग etc. Then the tantra adds that even though these deities are manifestations of mAyA they have become perfected beings by the practice of these teachings (जिनाह्). It then uses the analogy of the lotus: Even though the lotus is born from filth, it is not soiled by that filth. Likewise these deities and those who practice the teaching of चण्डमहारोषण, even though it is full of filthy and contra-normative sexual practices are not smeared by the faults from such practice.

The माला-mantra-s of चण्डमहारोषण are very common place mantra-s of this type and follow the general form such mantra-s in the Astika tradition. However, there are few elements that might be found in Astika saura-mantra-s and to a lesser degree in कौमार mantra-s (CM is often described as a कुमार or bAla). These points are of great importance in the evolutionary sense.\
मूल mantra-s:\
[ॐ चण्ड-महारोषण हूं फट् ॥\
ऒं अछल हूं फट् ॥\
ऒं हुं फट् ॥]{style="color:#99cc00;"}\
माला mantra-s:\
[ॐ ह्रां ह्रीं ह्रौं चण्ड-रूPए चट २ प्रचट २ कट्ट २ प्रस्फुर २ प्रस्फारय २ हन २ ग्रस २ बन्ध २ जम्भय २ स्तम्भय २ मोहय २ सर्वशत्रूनां मुखभन्धणं कुरु २ सर्व-डकिनीनां ग्रह-भूत-पिशाच-व्यादि-यक्षाणां त्रासय २ मर २ मारय २ रुरु-चण्ड-रुक् रक्ष २ [देवदत्तं] चण्डमहासेनः सर्वम् आज्ङपयति । ॐ चण्ड-महारोषण हुं फट् ॥]{style="color:#99cc00;"}\
The points of importance that emerge from this माला mantra are: 1) the bIja-s used at the beginning (i.e. ह्रां ह्रीं ह्रौं) are the bIja-s that mark the beginning of the saura mantra-s in Astika saura practice or its incorporation into shaiva, vaiShNava and स्मार्त practice. 2) The name of the primary deity of this tantra and the one invoked in this माला mantra is चण्ड-महारोषण which is a homolog of the name of the saura deity used in the primary saura तान्त्रिक practice: tejash-चाण्ड (चण्ड-ruk is a homonym). The same tejash-चण्ड has also been incorporated into shaiva practice as the equivalent of चण्डेश्वर when shiva is invoked as शिवसूर्य (e.g. in the works of the सैद्धान्तिक scholars somashambhu and aghorashiva). In the vaiShNava realm the worship of तेजश्चण्ड is seen in the saura काण्ड of the हयशीर्ष saMhitA of पाञ्चरात्र. The मूल mantra of CM also resembles one of tejash-चण्ड:[ॐ तेजश्-चण्डाय हुं फट् ॥]{style="color:#99cc00;"}Further, a name of CM, ruru-चण्ड-ruk or चण्ड-ruk (in साधन-माला 85 of प्रभाकर-कीर्ति) resembles names of sUrya (चण्ड-rochis). In the as yet unpublished chapter 13 of the CMT he is described as तीव्रतर again reminiscent of a name of sUrya: तीव्रभानु.

Finally, generic connections suggestive of a saura origin for चण्ड-महारोषण are suggested by the साधन-s of the साधन-माल. For example in SM-86 CM is invoked in a हुंकार emerging from the sun and in performing the japa of the first मूल-mantra his dhyana is given is as: "sUrya-प्रभा-मालिनम्-आत्मानं..." SM-87 and 88 similarly give an invocation of the deity from within the sUrya-मण्डल. e.g.: अन्तरिक्षे padmopari sUrya-मण्डलस्थं हुंकार-सम्भवं..." or "तस्यापि हृदये padma-सूर्यस्थं हुम्कार...").

Thus, the ultimate origins of the deity चण्ड-महारोषण perhaps lay in the saura realm of the Astika -- a lateral transfer of tejash-चण्ड. However, in the nAstika context, at least by the time of the CMT, the deity was incorporated into the system of the bauddha योगिनी tantra-s that appear to have been an exaggerated evolute of the non-dual practices of the shaiva-s. In fact the name used for the deity in the CMT, एकलवीर, is reminiscent of shiva as अकुलवीर in the homologous shaiva sphere. The presence of tejash-चण्ड is Astika texts predating (at least as per my reckoning) the nAstika CMT suggest that the lateral transfer was indeed from the Astika realm.

[कलशजा raised two distinct issues: Could it be mere convergence? After all चण्ड and the bIja-s are common in तान्त्रिक parlance and solar motifs are even more common. He is a neomorphic deity in any case and not a transparent absorption an Astika prototype, though he acquires the typical bhairava characteristics by his incorporation into the योगिनी-tantra, the CMT. Of course, I am willing to accept a better hypothesis that falsifies this one. I should point out that CM might have began his career as an attendant deity as suggested by the महाकरुण-garbhodbhava-मण्डल, which is not inconsistent with the tejash-चण्ड angle. The second issue is whether originally CM began in the west with a direct influence of the Iranian limb of the saura influence i.e. a conflation of Iranian roshana with संस्कृत रोषण. An interesting thought ... The name चण्डरुक् and makes one wonder]


