
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [balA to कामेश्वरी](https://manasataramgini.wordpress.com/2009/08/14/bala-to-kameshvari/){rel="bookmark"} {#bala-to-कमशवर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 14, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/08/14/bala-to-kameshvari/ "6:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

["मातर्-भण्ड-महादैत्य-सूनवो योद्धुम् आगताः ॥ ७७ ॥\
]{style="color:#99cc00;"} Mother, the great daitya भण्ड's sons have arrived to give battle.\
*[तैः समं योद्धुम् इच्छामि कुमारित्वात्-सकौतुका ।]{style="color:#99cc00;"}*[\
]{style="color:#99cc00;"} I wish to battle them due to my youthful enthusiasm.

  - [सफुरन्ताविव मे बाहू युद्ध-कण्डूययानया ॥ ७८ ॥\
]{style="color:#99cc00;"}* My arms throb with the itch for battle.\
*[क्रीडा ममैषा हन्तव्या न भवत्या निवारणैः]{style="color:#99cc00;"}*[\
]{style="color:#99cc00;"} Slaying them is my sport, so do not bar me with your prohibitions.|

  - [अहं हि बालिका नित्यं क्रीडनेष्व्-अनुरागिणी ॥ ७९ ॥\
]{style="color:#99cc00;"}* I am indeed a girl always delighting in sports.

  - [क्षणं रण-क्रीडया च प्रीतिं यास्यामि चैतसा ।\
]{style="color:#99cc00;"}* With this moment of battle-sport my mind will be delighted."


