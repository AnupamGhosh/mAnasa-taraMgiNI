
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [परशम्भुनाथ](https://manasataramgini.wordpress.com/2009/01/24/parashambhunatha/){rel="bookmark"} {#परशमभनथ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 24, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/24/parashambhunatha/ "7:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3467/3221369087_3e33f4ecc3.jpg){width="75%"}
```{=latex}
\end{center}
```



The tradition holds that the triad ravi, shashi and शुची are respectively shown in their पिण्ड form as the bIja-s: haM, saM and raM. They are combined with the great कुलयोगिनी manifesting as चित्परांबा and the bhairava manifest परशम्भुनाथ. Hence we get their mantra as OM haM saM raM चित्परांबा-परशंभुनाथाभ्याम् नमः |\
Or in their elemental forms we have for the bhairava: h-s-रौं and for the कुलयोगिनी: s-h-रौं.\
In the middle of the chakra dwells the conjoined pair of चित्परांबा and परशंभुनाथ. Around them are the 32 emanations of parA देवी seated on the laps the 32 bhairava-s emanating from परशंभुनाथ.

  - ----------------------------------------- -------------------------------------------
      **[भैरव]{style="color:#99cc00;"}**         **[योगिनी]{style="color:#99cc00;"}**
        [para]{style="color:#99cc00;"}              [परा]{style="color:#99cc00;"}
       [bhara]{style="color:#99cc00;"}              [भरा]{style="color:#99cc00;"}
        [chit]{style="color:#99cc00;"}            [छित्परा]{style="color:#99cc00;"}
      [महामाया]{style="color:#99cc00;"}         [महामाया-परा]{style="color:#99cc00;"}
        [सृष्टि]{style="color:#99cc00;"}           [सृष्टि-परा]{style="color:#99cc00;"}
        [इच्छा]{style="color:#99cc00;"}           [इच्छा-परा]{style="color:#99cc00;"}
       [स्तिथि]{style="color:#99cc00;"}          [stithi-parA]{style="color:#99cc00;"}
       [निरोध]{style="color:#99cc00;"}         [nirodha-parA]{style="color:#99cc00;"}
       [mukti]{style="color:#99cc00;"}          [mukti-parA]{style="color:#99cc00;"}
        [ज्ञान]{style="color:#99cc00;"}           [ज्ञान-परा]{style="color:#99cc00;"}
        [sat]{style="color:#99cc00;"}            [sati-parA]{style="color:#99cc00;"}
        [असत्]{style="color:#99cc00;"}           [asati-parA]{style="color:#99cc00;"}
      [sad-asat]{style="color:#99cc00;"}      [sad-asati-parA]{style="color:#99cc00;"}
       [क्रिया]{style="color:#99cc00;"}           [क्रिया-परा]{style="color:#99cc00;"}
       [Atman]{style="color:#99cc00;"}           [Atma-parA]{style="color:#99cc00;"}
     [इन्द्रियाश्रय]{style="color:#99cc00;"}     [इन्द्रियाश्रय-परा]{style="color:#99cc00;"}
        [गोछर]{style="color:#99cc00;"}         [gochara-parA]{style="color:#99cc00;"}
       [लोकमुख्य]{style="color:#99cc00;"}         [लोकमुख्या-परा]{style="color:#99cc00;"}
        [वेदवत्]{style="color:#99cc00;"}         [vedavat-parA]{style="color:#99cc00;"}
       [सम्विद्]{style="color:#99cc00;"}          [samvit-parA]{style="color:#99cc00;"}
      [कुण्डलिनी]{style="color:#99cc00;"}         [कुण्डलिनी-परा]{style="color:#99cc00;"}
       [सौषुंनी]{style="color:#99cc00;"}           [सौषुंनी-परा]{style="color:#99cc00;"}
      [प्राणसूत्र]{style="color:#99cc00;"}         [प्राणसूत्र-परा]{style="color:#99cc00;"}
       [स्यान्द]{style="color:#99cc00;"}           [स्यान्द-परा]{style="color:#99cc00;"}
       [मातृका]{style="color:#99cc00;"}           [मातृका-परा]{style="color:#99cc00;"}
      [स्वरोद्भव]{style="color:#99cc00;"}        [स्वरोद्भवा-परा]{style="color:#99cc00;"}
        [वर्णज]{style="color:#99cc00;"}           [वर्णजा-परा]{style="color:#99cc00;"}
        [शब्दज]{style="color:#99cc00;"}           [शब्दजा-परा]{style="color:#99cc00;"}
      [वर्ण-ज्ञात]{style="color:#99cc00;"}       [वर्ण-ज्ञात-परा]{style="color:#99cc00;"}
        [वर्गज]{style="color:#99cc00;"}           [वर्गजा-परा]{style="color:#99cc00;"}
       [संयोगज]{style="color:#99cc00;"}          [संयोगजा-परा]{style="color:#99cc00;"}
   [mantra-vigraha]{style="color:#99cc00;"}   [मन्त्र-विग्रहा-परा]{style="color:#99cc00;"}

  - ----------------------------------------- -------------------------------------------


