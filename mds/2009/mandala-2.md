
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [मण्डल-2](https://manasataramgini.wordpress.com/2009/01/11/mandala-2/){rel="bookmark"} {#मणडल-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 11, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/11/mandala-2/ "6:36 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3377/3187835525_fe35360c1c.jpg){width="75%"}
```{=latex}
\end{center}
```



