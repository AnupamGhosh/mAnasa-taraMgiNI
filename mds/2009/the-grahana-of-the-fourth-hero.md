
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The ग्रहण of the fourth hero](https://manasataramgini.wordpress.com/2009/08/26/the-grahana-of-the-fourth-hero/){rel="bookmark"} {#the-गरहण-of-the-fourth-hero .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 26, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/08/26/the-grahana-of-the-fourth-hero/ "5:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We learned that the अमात्य was caught up with completing the devotions to the terrifying विघ्नराज. We tried to reach the sachiva, but we could only reach MM. MM informed us that the sachiva had received the intelligence that the fourth hero had been attacked fiercely and was being pressed really hard in battle. The अमात्य of the unstoppable third hero had gone to make offerings at the parvata of rudra to help the first hero, who was locked in his अभिचार combat. This अमात्य first rushed to the help of the falling fourth hero. Then the news was relayed to our sachiva that he needed to enter the battle. So our sachiva entered the battle to aid the 4th hero -- he was in a stiff संअर्य.

We realized that a hit on the 4th hero was a like a direct hit on the communication center of the army in a yuddha. But we had to move to yuvapura, and if the communication center was gone we would not know anything of how the sachiva or the अमात्य fought the yuddha. Further due to us being in yuvapura the 4th hero could only be protected via the low-tech मन्त्रवाद of the sachiva and the अमात्य -- but they did what ever they knew well. Those who deploy कर्ण-यक्षिणी should be careful as she is not for the pashu-s. As we were advancing to the व्योमयान we briefly saw the यक्षिणी once -- we wondered why we should see her. After finishing with the व्योमयान, we went with one of our former warriors to get some food in what was an unusually good summer day in the cold wastes of yuvapura. We again saw the यक्षिनी briefly -- this raised our alarms a bit for we were to join a brief yuddha there after with some म्लेच्छादि. \[Many years ago when were still filled with youthful enthusiasm we decided to embark on our great quest. There we encountered a powerful म्लेच्छ warrior from the cold lands of the north. His skill with various shara-s left us with wonder. We realized that if we had to find a place amongst true yoddha-s we had to exceed him. Aided by the 1st hero who was then struggling in the grip of मोहिनी-s in balabaja-स्थान we reached शालाग्रम. About 18 hours after we reached there the deva-s gave us the shataghni, and once that was in our hand we knew how to find that way. With the shataghni we exceeded the said म्लेच्छ vIra of bhagadatta-like prowess]. After the yuddha of yuvapura, the guru of the said म्लेच्छ-vIra accepted our victory. We hence realized that the appearance of the यक्षिणी had nothing to do with this. Then we encountered three prathama-s who had decayed to the state of chaNDAla-s and were thus deservedly made servants of the म्लेच्छ-s. The यक्षिणी suddenly appeared and prevented us from being polluted by them.

We then retired and fell asleep. Around 2.00 AM we heard the कर्ण-यक्षिणी finally whisper in our ears -- we suddenly realized what was going on. She said that a मारण had been sent at the 4th hero. It was a peculiar astra like one dispatched by देवकी-putra in the days of yore. Neither the अमात्य nor the sachiva, nor the अमात्य of the 3rd hero nor other औषधप्रयोगिन्-s could figure out what this astra was. For sometime we were in dread, but we realized that only the स्वायुध of rudra could save the 4th hero. In the श्मशान we had said "namaste rudra manyave". We hoped that it might save. At the sight of the approaching मारण the troops of the 4th hero scattered like deer at the roar of a tiger and even the experienced अमात्य of the 3rd hero trembled in fear. Luckily the sachiva got the signal "namaste rudra manyave" and swung into action aided by some learned औषधप्रयोगिन्-s. They were able to stabilize the 4th hero like अनलासुर countered by ऋचीक aurva.


