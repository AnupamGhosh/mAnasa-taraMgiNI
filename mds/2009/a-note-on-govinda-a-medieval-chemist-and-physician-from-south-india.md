
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on govinda, a medieval chemist and physician from South India](https://manasataramgini.wordpress.com/2009/05/25/a-note-on-govinda-a-medieval-chemist-and-physician-from-south-india/){rel="bookmark"} {#a-note-on-govinda-a-medieval-chemist-and-physician-from-south-india .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 25, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/05/25/a-note-on-govinda-a-medieval-chemist-and-physician-from-south-india/ "7:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Years ago while examining the sarva-darshana-संग्रह it became clear that the rasa cult probably had a notable following in the Vijayanagaran territory. A visitor from the chera country had shown me a text titled the Ayurveda-प्रकाश-टीका composed by the medieval physician somadeva, which quoted from a text termed the रससार. The more recent vaidya named yadava-jI trikam-jI also quotes from the रससार in his exposition on rasa-विद्या. A few features of this text revealed some interesting details about the author of this रससार, who also seems to have composed another text the संनिपात-मञ्जरी on fevers. He was a ब्राह्मण, the son of सुरादित्य and the grandson of sahadeva. He is supposed to have been a student of धीरदेव, the son of अभयपाल from the region of अन्तर्वेदी. He also states that अभयपाल was a सारस्वत ब्राह्मण probably settled in that town. He calls himself a moda ज्ञाति, a mantra-siddha and a sarva-शास्त्रोविद्. The first term is unclear -- a rather speculative inference could be that is he was a follower of the moda shAkha of the atharvaveda. Alternatively, it can be emended to the term मोढज्ञाति, which is also not entirely clear in meaning. It appears to refer to a guild of brahminical teachers in northern India and is encountered in texts from Gujarat, Varanasi and Rajasthan. The place of his studies, अन्तर्वेदी is also defined by कल्हण as the land between the Ganga and the Yamuna with the capital of the वैश्वामित्र-s, कान्यकुब्ज as its central city. This is consistent with his teacher being a सारस्वत ब्राह्मण as they had a major epicenter in the city of कान्यकुब्ज. This is confirmed by the use of the term अन्तर्वेदी in inscriptions of राष्ट्रकूट and चन्द्रात्रेय (chandella) chiefs ruling the कान्यकुब्ज region. Another important place in this region was the great shaiva center of वोडामयूता which had a famous मठ of the सैद्धान्तिक-s. He also mentions the लिङ्ग of किरातेश्वर in Varanasi as his personal deity. These observations suggest that govinda was active in northern India in the Gangetic Doab probably in कान्यकुब्ज or kAshi. However, interestingly an examination of his text reveals the use of certain Tamil or Dravidian words which suggest that govinda was likely to have been from southern India. He also indicates that after studying Ayurveda and रसायन from धीरदेव he journeyed to Tibet (भोटदेश) to learn a mercurial technique termed रङ्गाकृष्टि from the Lamas by which amalgams are given various colors. This is described in the 9th chapter of the रससार and it interestingly mentions the use of ओषाधि-s such as काकाह्वा, काकमाची and काकतुण्डी in the "killing" of mercury. This link to the काकचन्दिश्वर-mata suggests that there was an earlier transmission of रङ्गाकृष्टि to Tibet during the absorption of shaiva tantric material by the bauddha-s. But this material was lost in India and regained by ब्राह्मण visitors such as govinda. We have no clear idea of when he lived but he was cited by तोडरमल्ल's panel of scholars, who were contemporaneous with the Mogol tyrant Akbar, and hence definitely prior to him. The somadeva who cites him was could have been before Akbar too.

One striking feature of govinda's work is the description of tetrodotoxins produced by four fishes \[tetraodontiforms] from the Indian ocean (Chapter 17, verses 36-38). These toxins are termed ahiphena-s by govinda and should not be confused with opium in the later texts. This to my knowledge is the earliest Hindu awareness of tetrodotoxins.

Following the classical tradition of रसायन, govinda expounds that the practice of रसायन is taken up along with a female partner (the काकिनी in other texts). The rasayogin and the रसाङ्गना set up the laboratory where they would conduct their experiments in the form a मण्डल of rasabhairava, an emanation of aghora. The rasabhairava or rasesha and his wife रसाङ्कुशी are installed in the center. There are several surrounding आवरण-s, which in order are:

 1.  Square: nandin, महाकाल, the demon कुम्भकर्ण and the monkey सुग्रीव with the skeletal भृगिन् probably in the middle holding the foot of shiva.

 2.  Octagon in E-S-W-N orientation: shukra, skanda, rudra, vayu, sadashiva, agni, उमा, and space (व्यापक)

 3.  Ten rasa-दूति-s or rasa-योगिनी-s: लेपिका, क्षेपिका, क्षारिका, रञ्जिका, लोहटी, बन्धकारी, भूचरी, मृत्युनाशिनी, विभूति and खेचरी. These are placed as an octagon with the last two on top and bottom.

 4.  Octagonal lotus in E-S-W-N orientation containing: mAkSika, vimala, shaila, chapala, rasaka, sasyaka, and two गन्धताल-s. These are the 8 vidyeshvara rudra-s, each of a different color with 5 heads with 3 eyes on each. They are blue-throated, with cresent moon ornaments and eighteen hands and bull-banners.

 5.  In the pericarp of this lotus are situated their shakti-s: मालिनी, hemashakti, parA, balA, अपरा, vajrashakti, कान्ति and परापरा.

In the middle of these circuits is situated the great rasa-bhairava: He is like pure quartz, with five heads, three eyes each, with flames shooting from them, a blazing tongue, locks and eye brows like flames, and difficult to look at. He stands on a corpse with a cresent moon on his head gear, wearing a tiger skin, a snake उपवीत, holding a bull-banner and has 18 arms with several weapons and a slight smile on his faces. He is invoked with the glorious mantra:\
*[ॐ ह्रीं क्रीं रसेश्वराय महाकालाय महाबलायाघोरभैरवाय वज्रवीर क्रोध-कङ्काल क्ष्लौः क्ष्लः ॥]{style="color:#99cc00;"}*\
The part after रसेश्वराय and ending with the two rasa bIja-s is the core rasesha mantra of 32 syllables. The whole mantra is said to correspond syllable for syllable to the aghora mantra and represent the emanation of rasabhairava from aghora:\
[अघोरेभ्यो ऽथ घोरेभ्यो घोर घोरतरेभ्यः । सर्वेभ्यस् सर्व-शर्वेभ्यो नमस्ते अस्तु रुद्र-रूपेभ्यः ॥]{style="color:#99cc00;"}

His shakti रसाङ्कुशी is conceived thus:\
She is well decked with jeweled ornaments, with a crescent moon on her headgear, is kindly faced, with three eyes and a blue throat. She shines like pure gold and is draped in yellow garments in the midst of white flywhisks and a pearl decked parasol. In her two right hands she holds a hook and rosary and a lasso and the abhaya mudra in her left hands. She is invoked by the glorious mantra:\
[अईं ह्रीं श्रीं क्लीं सौः ॥]{style="color:#99cc00;"}

Of the yantra-s actually used in the laboratory described by govinda most are obscure. Some of these are clarified in modern रसायन works such as भूदेव devasharman's bilingual manual but there is no guarantee that these modern interpretations are what govinda meant. Among them some are simple tools to process ores and metals, tongs, mortal and pestle and the like. Others appear to be more complex. A centrifuge-like device appears to be described as the chakra yantra; however, it is not clear if sufficiently high speeds could be achieved for being useful. The जलकूर्म and the स्थलकूर्म appear to be distillation devices and crucible heaters. The heating of a collyrium like black mercurial in one such device is supposed to yield the red रससिन्धूर. Another mysterious yantra is the अग्नीसोमाख्याय yantra which is used to heat a mixture of two salts to obtain an amalgam at the end. While रसायन is full of strange things to the modern mind, one unusual feature is the making of extracts from particular earthworms (भूनाग). The earthworms living near gold, silver or iron mines are used to extract what are known as sattva's of various metals.


