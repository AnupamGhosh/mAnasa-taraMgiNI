
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Nostalgia: entomofauna on the home turf](https://manasataramgini.wordpress.com/2009/11/24/nostalgia-entomofauna-on-the-home/){rel="bookmark"} {#nostalgia-entomofauna-on-the-home-turf .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 24, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/11/24/nostalgia-entomofauna-on-the-home/ "5:16 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2759/4130744153_b45e406036.jpg){width="75%"}
```{=latex}
\end{center}
```



The diversity in bhArata remains unmatched still

