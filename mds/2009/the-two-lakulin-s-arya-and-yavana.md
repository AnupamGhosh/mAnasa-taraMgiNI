
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The two lakulin-s: Arya and yavana](https://manasataramgini.wordpress.com/2009/06/06/the-two-lakulin-s-arya-and-yavana/){rel="bookmark"} {#the-two-lakulin-s-arya-and-yavana .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 6, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/06/06/the-two-lakulin-s-arya-and-yavana/ "6:30 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3401/3600576095_103d678383_o.png){width="75%"}
```{=latex}
\end{center}
```



The late white indologist Daniel Ingalls, whose Asana is now occupied by "der Narr von Deutschland", and who has spawned a brood of म्लेच्छ indologists who have been prominent in misinforming the uninformed regarding Hindu traditions \[footnote 1], once remarked:\
"Probably no two traditions of philosophy differ more widely than those of classical Greece and India. It is only when we force our way through the logical surface to the seemingly illogical practices and goals of an earlier age that we can see similarities between the two cultures. These similarities, however, are sometimes so striking that the Indian evidence may help our understanding of Greece and the Greek our understanding of India. One gains such a reciprocal understanding, I think, from examining what I shall call the cults, meaning by this the sum of the practices and goals as opposed to the philosophy, of the Greek Cynics and the Indian Pasupatas."\
Thus, in a contradiction-riddled fashion Ingalls introduces his discovery of the relationship between the पाशुपत shaiva-s and the Cynics (from kuon= dog). Another म्लेच्छ scholar Thomas Mcevilley after him came much closer in grasping the Arya-yavana homology, but his prolix tome is so full of factual errors (Ancient Dravidian शैवागम-s and a Dravidian origin for पाशुपत-s!) that it only tarnishes the skeleton of truth in his thesis. The problem, as we have repeatedly stated, is that someone not befuddled by the odious Ibrahimi fog and not lacking understanding of the ancient heathen thought (like most modern म्लेच्छ-s) should take up such studies.

That said we do agree that there is merit in Ingalls' hypothesis for a connection between पाशुपत and Cynics as graphically illustrated above by the images of the लकुटधारिन् founders. While Ingall's original work presents a number of contradictory ideas without laying them out as alternative hypotheses to be tested he does recognize many of the essential elements of this connection. Three general hypotheses might be proposed to understand this connection. 1) The out of Greece hypothesis: लकुलीश पाशुपत emerged due to Greek cynicism being transmitted to India and being absorbed into older shaiva systems. 2) The लकुलीश पाशुपत emerged in India and was adapted in via syncretism with older Greek thought. 3) The लकुलीश-s and cynics were products of an independent tradition absorbed by both Greeks and Hindus. Black Sea shamans, Middle Eastern Gilagameshists and the like have been proposed as this common source.

The main evidence supporting the first hypothesis goes thus:

  - There is no mention of लकुलीश as a great पाशुपत teacher in the महाभारत, which presents the first post-vedic material of पाशुपत-s. He first appears with his students in the पुराण-s.

  - The पाशुपत sUtra-s themselves do not clearly indicate that लकुलीश was their author, though the later tradition पाशुपत tradition holds this view.

  - There is inscription from the reign of chandragupta II विक्रमादित्य in Mathura (380CE) regarding a पाशुपताचर्य udita. He is mentioned to be the student of upamita who was the student of kapila. udita himself is described as 10th in line from kaushika. Some people have felt that this kaushika was the student of लकुलीश (but note लकुलीश is not mentioned in this inscription) so by assigning about 280 years for 10 generations they place लकुलीश in \~100 CE which is well after the Greek cynics. Some others state that there is an error in this estimate and there are two kaushika-s in the पाषुपत list and place लकुलीश around 200 BCE. But this is also after the first Greek cynics

  - The philological argument holds that लकुलीश is derived from Herakles:\
Herakles\>rakles>लकुलीश. In support of this it is stated that: 1) लकुलीश is not regularly derived from लकुट inside संस्कृत and 2) the word लकुलीश has many variants such as नकुलीश, lakulesha and lakulin. 3) An ectype of लकुलीश named musalendra has been stated to be his disciple. This name means bearer of a club, just like Herakles. The vedic rudra usually holds a bow rather than a club. Thus it is quite possible that the लकुलीश figure was adapted into the shaiva stream from a Greek cynical source.

Such syncretism is not surprising. We do know of the famous Greek bhAgavata Heliodorus who was a practitioner of some form of the पाञ्चरात्र sect. On the other hand we know that at Nemrud Dagh there is Iranian-Greek syncretism where there Iranian verethraghna is explicitly identified with Herakles. So it is not impossible that in the zone of Gandhara where there is much evidence for Indic-Greek-Iranian fusion that the cynic cult centered on Herakles was absorbed. Further, we do have evidence in the form of the yavana-जातक and the पौलीश siddhAnta of Indic absorption of Greek ideas \[Note how Paulos>पौलीश is similar to the proposed derivation of लाकुलीश from Herakles. We cannot be certain if this Paulos is the same of P. Alexandrinos the neo-Platonic astrologer].

Firstly, the key features for the out of India hypothesis are not apparent to many "main-stream" white Indologists and their fellow travelers as they tend to have ludicrous idée fixes on the dates of most Hindu texts -- but we shall not waste time with those discussions here. The main points in support of this hypothesis are:

  - There is a long history of पाशुपतं that predates the emergence of cynicism among the Greeks. We note that it begins in the late vedic period in the form of the पञ्चब्रह्म mantra-s of the महानार्यण उपनिषद्-s, and the श्वेताश्वतर and नीलरुद्र उपनिषद्-s. This is followed by the atharva-shiras, the पाशुपत ब्राह्मण and then by the पाशुपत section of the bhArata (e.g. the अजानन virachita rudra stuti). Probably a little after this emerged the पाशुपत सूत्राणि. Thus, even if, like much later tradition holds, the pashupata सुत्राणि were the work of लकुलीश, it is hardly the beginning of the पाशुपत tradition. Hence, the roots of the पाशुपत tradition is at least as old as 1200 BCE, which is certainly earlier than the 450-350 BCE time frame for the emergence of Greek cynicism. Hence, the origins of पाशुपत shaiva tradition were definitely not inspired by the Greek traditions.

  - Key elements of homopraxis that are believed to unite the पाशुपत-s and the cynics are: 1) theriomorphic behavior and 2) seeking of dishonor or अवमान. There are specific similarities especially in the latter in form of lewd behavior in public both among cynics and पाशुपत-s and behaving like madmen e.g. performing अट्टहास and हुड्डुकार. However, it should be noted that these elements are not entirely unique to the अतिमार्ग of the शैवशासन but in general of the अतिमार्ग matrix that includes both Astika-s and their nAstika cousins. For this we have to turn to the संन्यास उपनिषद्-s. In their current form many of these are later texts, but by they contain some genuinely archaic material \[To give credit where it is due the white Indologist Sprockhoff performed a detailed study of these and noted the homologies within them. Based on these homologies and the आरुणि text one may conclude that there were 1 or 2 ancestral pre-nAstika संन्यासोपनिषद्-s from which all the later ones descended. Interestingly, in the older period they tend not to use the word संन्यास that often. The older term for the man of the अतिमार्ग was परिव्राजक].

That the अतिमार्ग was not unique to the पाशुपत shaiva-s is suggested by a relatively early परिव्राजक text known as the आरुणि उपनिषद्, which appears to belong to the early vaiShNava अतिमार्ग. Here, the naked परमहंस utters two mantra-s as he wanders about:\
[ OM hi OM hi OM hi ||]{style="color:#99cc00;"} आरुऊ 11-12 \[which is like the हुड्डुकार of the shaiva]\
and\
[तद् विष्णोः परमं पदं सदा पश्यन्ति सूरयः । दिवीव चक्षुराततम् ॥\
तद् विप्रासो विपन्यवो जागृवांसः समिन्धते । विष्णोर् यत् परमं पदम् ॥]{style="color:#99cc00;"}\
\[which is like the पञ्चब्रह्म mantra of the shaiva ascetic].

The आरुणि text also states:\
[भिक्षार्थं ग्रामं प्रविशन्ति पाणि पात्रम् उदरपात्रं वा ॥]{style="color:#99cc00;"} आरुऊ११\
That is the परमहंस enters the village for alms and uses his hand or stomach as the begging bowl. Explaining this point the medieval exegete यादवप्रकाश (teacher of रामानुज the vaiShNava) states that a परिव्राजक eats food with his mouth like a cow. To this effect, a latter vaiShNava अतिमार्ग text, the नारद-परिव्राजकोपनिषद् states:\
[आस्येन तु यदाहारं गोवन्-मृगयते मुनिः । ण्पू१८२]{style="color:#99cc00;"}\
That is the vaiShNava ascetic eats food in theriomorphic fashion, like cow or some animal with his mouth.\
The same text adds:\
[बुधो बालकवत् क्रीडेत् कुशलो जडवच्चरेत् ।\
वदेदुन्मत्तवद् विद्वान् गोचर्यां नैगमश्चरेत् ॥]{style="color:#99cc00;"}\
The wise \[ascetic] plays like child and the clever one behaves like an idiot. The eloquent one \[speaks] like a madman and the scholar roams behaving like a cow.\
Thus theriomorphic and insane behavior is also part of the vaiShNava परमहंस's behavioral repertoire.

The bauddha majjhima निकाय shows how this behavioral complex of Indo-Aryan ascetics was adopted by the तथागत. In 1.79 of this Pali text we encounter the तथागत walking around on four legs observing a pashuvrata even as the one described in the earlier bhArata epic (somewhere in 5th parvan). Later in the majjhima निकाय (1.387) the तथागत is seen taking on two परिव्राजक-s one of whom is performing a govrata (the boomorphic conduct) and the other श्वचर्या (cynomorphic conduct).

These behaviors being part of the ancestral Indo-Aryan परिव्राजक's repertoire is confirmed by two statements made by बौधायन in the dharma sUtra in describing the mode of existence of the अवापचमानक (the ascetic who does not cook), which appears to include the vaiShNava वैखानस ascetic:\
[कृच्छ्रां वृत्तिम् असंहार्यां सामान्यां मृगपक्षिभिः ।\
तद् अहर्जन संभारां कषायकटुकाश्रयां ।\
परिगृह्य शुभां वृत्तिम् एतां दुर्जन वर्जितां ।\
वन-वासम् उपाश्रित्य ब्राह्मणो नावसीदति ॥\
मृगैः सह परिस्पन्दः संवासस्तेभिर् एव च ।\
तैर् एव सदृशि वृत्तिः प्रत्यषं स्वर्गलक्षणं ॥\
प्रत्यक्षं स्वर्गलक्षणं इति ॥]{style="color:#99cc00;"} BऔSउ 3.3.21-22

These citations by बौधायन appear to be from ancient lost texts which could very well be the precursor of the extant संन्यास उपनिषद्-s. It shows how the most difficult vrata observed by these vaikhanasa or brahma ascetics is one where they live like beasts or birds, eating raw and bitter and sour food just like them. They roam around with animals and live just like them amongst them. Thus, a key element which is present in some form in पाशुपत, vaiShNava and nAstika asceticism and also form a central element in Greek cynicism has its roots in the ancient Hindu asceticism but emerges only around 450-350 BC in Greece.

From the earlier statement from the ण्पू it also becomes clear that even the vaiShNava परिव्राजक assumed mannerisms of the insane. That this was an ancient trait of the परिव्राजक-s in general is supported by a sUtra in the वसिष्ठ dharma sUtra describing their behavior:\
[अनुन्मत्त उन्मत्त्वेषः ।]{style="color:#99cc00;"} VaSu 10.19\
Though sane he puts on the appearance of the insane. This same statement is recycled repeatedly in various संन्यास texts like जाबाल उपनिषद् and ण्पू suggesting that it is from the Ur-परिव्राजक text. Another ancient saying pertaining to परिव्राजक-s that is widely quoted in multiple texts is:\
[संमानाद् ब्राह्मणो नित्यम् उद्विजेत विषाद् इव ।\
अमृतस्य् एव चाकाङ्क्षेद् अवमानस्य सर्वदा ॥]{style="color:#99cc00;"}\
The ब्राह्मण is always averse to honor like poison and like elixir longs for dishonor. As in the case of the पाशुपत texts the vaiShNava ण्पू further adds how when the ascetic is reviled and tortured he attains yogic success (exactly parallels the पाशुपत sUtra 3.19). Thus, another core element of the पाशुपत behavior shared with the Greek cynics -- i.e. simulated madness and concomitant seeking of dishonor appears to have been a feature of the ancestral Indo-Aryan ascetic and not a unique feature of the former. Not surprisingly, such dishonor- or abuse- seeking is also encountered in the tales of even the nAstika ascetics like the nirgrantha, the tathagata and the गोशाल.

Finally we come to the club of लकुलीश and Herakles. At face value this feature is an important specific cognate of the respective founders of the systems under consideration. Yet, we must not forget that the carrying a rod or a staff is an ancient emblem of the Hindu ascetics, well before the emergence of लकुलीश in Hindu texts. This emblem is taken up by ascetics with a vedic mantra which is cited in many texts including संन्यास उपनिषद्-s and also the dharma sUtra-s (e.g. ण्पू 169 or BऔSउ 2.17.32):\
[सखा मा गोपाय । ओजह् सखायोऽसीन्द्रस्य वज्रोऽसि वार्त्रग्नः शर्म मे भव यत्पापं तन्निवारये ॥]{style="color:#99cc00;"}\
Now it should be noted the daNDa taken by the ascetic is explicitly identified as the vajra of indra. Interesting, the Greek cognate uagros is used for the club of Herakles. Thus, even in this case it is not impossible that the club is a cognate of the older staff held by परिव्राजक-s. Specifically, a खट्वाञ्ग or लकुट are mentioned as substitutes of a daNDa in the atharvaveda परिशिष्ठ 40's description of the accoutrements of a पाशुपत ascetic. Since most features of the पाशुपत-s that are believed to be specific cognates of the Greek cynics are integral archaic features of the Indo-Aryan ascetics that were inherited as a complex by various successor ascetic groups in the Indian milieu, the case for an Indic origin followed by a transfer to Greece only around 450 BCE appears entirely possible.

The last hypothesis involving a third party influence on both yavana-s and Arya-s is reminiscent of the posited by white Indologists to explain similarities between the mathematics of the शुल्बसूत्रस् and that of the yavana-s. This again comes from their misconceptions regarding the age of the earlier Indo-Aryan literature. As we have seen above, the essential elements that need explanations can be traced entirely inside Greek or Hindu tradition. Hence, this class explanations is useless because it merely multiplies the number entities needed in an explanation without adding any value.

Looking at the two main contenders we find that Greek hypothesis has the advantage of explaining the etymology of लकुलीश and providing an explanation for the club as against the ascetic's staff in the iconography of musalendra and लकुलीश. However, this is challenged by the mention of the लकुट relatively early in the AV-pari 40's account of the system. On the other hand, the Indic hypothesis has the advantage of providing continuity and context for the peculiarities of these systems, which is lacking in the case of the Greek cynics who appear relatively suddenly in their system. This probably was the basis for the confusion and contradiction displayed by Ingalls in his work -- he simultaneous suggests that there was no genetic link between the two and yet believes that they are related. He even adds that they "discovered each other" to explain the etymological link between Herakles and लाकुलीश. A more likely scenario is the following: The परिव्राजक asceticism arose in the Indo-Aryan world and the incipient shaiva- and vaiShNava- शासन-s both inherited it from their undifferentiated Indo-Aryan precursor. Thus वैखानस and पाशुपत ati-marga existed side by side before the tathagata or the nirgrantha. Around 500 BCE within the matrix of the परिव्राजक system emerged the nirgratha-s and bauddha-s who also inherited aspects of it to different degrees. Around the same time in the western limits of भारतवर्ष the yavana-s started appearing and participating in various Hindu systems. From then on right through the Hellenistic period catalyzed by the invasions of the barbarians of Macedon, yavana-s were actively involved in different Hindu systems. Around 500 BC some yavana-s most likely in the Northwest of भारतवर्ष adopted परिव्राजक practices from shaiva teachers. Given their syncretic nature they equated rudra (as श्रीकण्ठ) the founder of the पाशुपत system with Herakles. Their shaiva teachers appear to have absorbed the name, perhaps being struck by the convergence in terms of the लकुट held by both and the phonetic analogy in Herakles> लकुलीश. Thus, the figure लकुलीश emerged as an incarnation of shiva and a founder of the पाशुपत system. We note that many aspects of cynic philosophy, not just practices, arise from early अतिमार्ग shaiva thought and its larger परिव्राजक matrix and were transmitted to Greece to found classical cynicism. The memes of ati-मार्ग thinking appear to have then on propagated in the Greek world all the way to neo-Platonism. In a sense this is reminiscent of how अतिमार्ग memes permeate mantra-मार्ग शासन of the later era in India. Thus, in the cynic-पाशुपत homology we possibly encounter one of those lesser appreciated types of interactions of the pagan Indo-European world -- the two participants despite their apparent differences were able to rather intelligibly interface and take what they found valid suitable for their world view from the other and then move on with their own expressions. This is somewhat different from certain other interaction where one of the cultures simply acquires the whole complex from the other with hardly any local adaptation.


Footnote 1: In my investigation of the politics of indology I have found no evidence for Daniel Ingalls himself being a subversionist or engaging in willful misinformation. However, his students include notable म्लेच्छ subversionists who belong to a larger wave of academic misinformation which can be termed Freudism. My माताश्री reports that even in her college days one such वञ्चक was already implanted in bhArata who was engaging in indoctrinating the gullible in the desh. But another student Thurman appears to be more sympathetic to our traditions.


