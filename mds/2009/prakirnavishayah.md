
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [प्रकीर्णविषयाः](https://manasataramgini.wordpress.com/2009/09/09/prakirnavishayah/){rel="bookmark"} {#परकरणवषय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 9, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/09/09/prakirnavishayah/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The ever malignant विड्भुक् decided to take advantage of the absence of the sachiva who had gone to aid the 4th hero to attack the अमात्य. We intimated the अमात्य of the clause in the dharmasUtra-s regarding आततायिन्-s and that the time had come for observing it. The mantra of वामदेव gautama was uttered to invoke agni who is called रक्षोहा and sent forth like a dart shot by sharva. And then the mighty thunderer, the slayer of वृत्र was invoked to make the dasyu bend before the Arya. The विड्भुक् sent a चेटक at the Amatya. It was like a collyrium heap and gruesome with kusha-like hair sticking out from above the ears. It attacked first at night when the अमात्य as alone but the अमात्य bold defended against it despite the fear it raised. The अमात्य quickly realized what was happening and prepared the प्रतिक्रिया. The विड्भुक् sneakily sent out a tiraskarin to prevent the अमात्य from seeing the चेटक. But luckily pre-warned by experience and aided by the विनायक rite the अमात्य remained unfazed. But upon the second coming of the चेटक the अमात्य was prepared and unleashed the powerful counter-attack with brahmaskanda. The चेटक went up in vapor like a missile of सात्यकि ending the somadatta-s. The यक्षिणी नेत्रवती showed that the विढुक् was raging like duryodhana when the पाशुपतास्त्र fell on jayadratha on the 14th day.

We feared that कृशमुख might go to a fight with ST. But कृशमुख was held up in a great series of yuddha-s of his own and this did no happen (ST: I am sure he has quit the turf of अस्माल्लोक and he is unlikely to ever cross your path).


