
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [दीपोत्सव](https://manasataramgini.wordpress.com/2009/10/17/dipotsava/){rel="bookmark"} {#दपतसव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 17, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/10/17/dipotsava/ "6:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2714/4018813574_377b5d5ed4.jpg){width="75%"}
```{=latex}
\end{center}
```



