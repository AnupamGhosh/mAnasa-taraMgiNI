
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [दशदलानि](https://manasataramgini.wordpress.com/2009/04/19/dashadalani/){rel="bookmark"} {#दशदलन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/04/19/dashadalani/ "5:45 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3352/3455562455_7e7763d3f1.jpg){width="75%"}
```{=latex}
\end{center}
```



