
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A Prodigious lime](https://manasataramgini.wordpress.com/2009/11/15/a-prodigious-lime/){rel="bookmark"} {#a-prodigious-lime .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 15, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/11/15/a-prodigious-lime/ "8:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In course of their कृषि karman my folks encountered a prodigious lime (or a pair of them). Duly, they interpreted it as a visitation by the mighty विनायक. Given that an old sthala of the गाणपत्य तान्त्रिक-s lies about of mile from their dwelling, the visitation was not seen to be out of place.[![gan2](https://i0.wp.com/farm3.static.flickr.com/2646/4105312742_5dd514f295.jpg){.aligncenter height="336" width="448"}](http://www.flickr.com/photos/24766652@N05/4105312742/ "gan2 by somasushma, on Flickr")


