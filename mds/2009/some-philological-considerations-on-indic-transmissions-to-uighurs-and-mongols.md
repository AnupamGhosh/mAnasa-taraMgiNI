
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some philological considerations on Indic transmissions to Uighurs and Mongols](https://manasataramgini.wordpress.com/2009/07/10/some-philological-considerations-on-indic-transmissions-to-uighurs-and-mongols/){rel="bookmark"} {#some-philological-considerations-on-indic-transmissions-to-uighurs-and-mongols .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 10, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/07/10/some-philological-considerations-on-indic-transmissions-to-uighurs-and-mongols/ "5:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vaiShNava had brought my attention to a fragment of a text on वाराही साधन of the ब्राह्मण श्रीधर that was transmitted to both the Uighurs and the Chingizid Mongols. It described the योगिनी-s in the आवरण of eka-virA वज्रवाराही. It presented a number of interesting philological features that lead us to the study in greater detail of the material collected by Shogaito in his analysis of Uighur and Mongolian vocabularies:

  - --------- --------------- -------------
  Sanskrit   Uighur          Mongolian
  चक्रवेगा     cakira-a-vigi   cagr-a-bigi
  खण्डरोहा    kanta-roxi      खण्डरोहि
  महावीर्या   maxa-viry-a     mahA-biry-a
  सुविरा      suviri          subiri

  - --------- --------------- -------------

Other words:

  - --------- ------------ -----------
  Sanskrit   Uighur       Mongolian
  chitra     chitri       citiri
  ब्राह्मण     biraman      biraman
  kalpa      kalp         galb
  preta      prit/pirit   birit
  shloka     shlok        silüg
  vajra      vcir         vcir
  विरुपाक्ष    virupakshi   virubagsi

  - --------- ------------ -----------

Then we have the names of various deities:

  - ---------- ------------- -------------
  Sanskrit    Uighur        Mongolian
  महादेव       maxadivi      maqadivi
  Aditya      aditya        naran (Mon)
  soma        soma          saran (Mon)
  बृहस्पति      barxasuvadi   briqasbadi
  वैश्रवण       bishaman      bisman
  buddha      burxan        purghan
  garuDa      garudi        garudi
  asura       asuri         asuri
  gandharva   gandarvi      gandarwi
  महासेन       maxaseni      maqasini

  - ---------- ------------- -------------

E.g. of an Uighur text with recognizable words of Sanskrit origin (from an inscription near Beijing from the reign of Toghun Temür):\
अबितली shaki-munili sarva-वित्ञेलि\
akshobili vcirapaninïN besh ugush mantalï\
akshobi: अक्षोभ्य; सर्ववित्ञे: सर्वविज्ञा; shaki-muni: शाक्य-muni; mantal: मण्डल; vchirapani: वज्रपानि

One systematic rule concerning the endings appears to result in Sanskrit\>Uighur: a\>i. Shogaito observers that this is not the case with other Altaic languages like Mongolian if they directly acquire a word from Sanskrit or via a Tibetan intermediary. Interestingly, when Tocharian (an Indo-European language that geographically overlapped with Uighur before the destruction and absorption of the Indo-Europeans by the Turkic peoples) absorbs a word from Sanskrit we observe an a\>e at the terminal. Likewise when Tocharian absorbs a Sanskrit word as an "inanimate" gender it elides the terminal:\
e.g. शारिपुत्र>शारिपुत्रे; पारमिता> पारमित्;\
Thus, it appears that Uighur initially acquired its Sanskrit loans from Tocharian as the Turks over ran the Indo-Europeans (of mummy fame). However, it continued to retain the Tocharian type terminal transformations, which explains the transformation of the Sanskrit words that were subsequently absorbed by Uighur. The endings in Mongolian in turn indicate that it subsequently acquired several already transformed Sanskrit loans from Uighur.


