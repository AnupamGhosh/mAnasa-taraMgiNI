
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A famous clan of मन्त्रवादिन्-s](https://manasataramgini.wordpress.com/2009/04/26/a-famous-clan-of-mantravadin-s/){rel="bookmark"} {#a-famous-clan-of-मनतरवदन-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 26, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/04/26/a-famous-clan-of-mantravadin-s/ "6:47 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3187/3476508667_1f28a67617.jpg){width="75%"}
```{=latex}
\end{center}
```



There are certain strange events or what they call the déjà vu that give you an eerie feeling. In course of a conversation on the transmission of तान्त्रिक lineages between different parts of भारतवर्ष with R, she was mentioning the links on her maternal side that she had dug up recently. The connection to my medieval coethnic समरपुङ्गव दीक्षित and its symmetry with respect to my larger clan was rather surprising. So it may not be out of place to discuss this great clan of मन्त्रवादिन्-s among my coethnics. A genealogy up to certain point of the northern branch was produced by the historian madhava-कृष्ण-sharman. Unfortunately, at this stage I cannot produce a complete genealogy of the southern branch.

In the drAviDa country on the bank of the Pennar river in the Paanampalli village, south of Shivakanchi, in an अग्रहारं associated with the हस्तिगिरिनाथ temple, lived a family of notable shrauta ritualists and संस्कृत authors. They belonged to the Atreya gotra of vipra श्यावाश्व and were chaturvedin-s. Their family deity was the विष्णु standing atop the seven hills. One of the foremost amongst them was the ritualist समरपुङ्गव दीक्षित who had performed multiple soma याग-s and written a few works. His son was tirumala, and his son was श्रीनिकेतन दीक्षित were also सोमयाजिन्-s. The latter's son was श्रीनिवास दीक्षित, who after performing several सोमयाग-s in the drAviDa country had also acquired some knowledge of तान्त्रिक lore. He then headed to the northern country and reached the जालन्धर पीठ around 1575 CE even as the Mogol tyrant Akbar was prosecuting his bloody Jihad in Rajasthan. In the central part of the country रानी दुर्गावती and her son vIra-नारायण had been killed after a heroic struggle against the Moslems and the last great patrons of the सैद्धान्तिक shaiva-s had come to an end with that. The bundela-s were still defending the Hindu banner from their forest outpost at Orcha locked in a death-struggle with the army of Islam. But even in these trying times Hindus boldly moved from one part of bhArata to another as a part of their तीर्थ circuits. The main driving force for श्रीनिवास was to visit जालन्धर पीथ as it was one of the greatest तान्त्रिक क्षेत्र-s after the ओड्डियान पीठ, which had already been lost to the तुरुष्क-s.

There he encountered a powerful तान्त्रिक from the पञ्चनद named आचार्य sundara deshikendra who was a learned exponent of the kula path. After having mastered a vast body of tantric mantra-s श्रीनिवास was finally given दीक्ष in the कादि श्रीविद्या and the trika mantra-s by sundara deshikendra under the दीक्ष name विद्यानन्दनाथ. Giving श्रीनिवास a श्रीचक्र he asked him to settle in वाराणसी and expound the tantra-s of the कामेश्वरी. While वाराणसी was reeling under the charge of the तुरुष्क-s, sundara is supposed to have predicted that it will be revived by the time श्रीनिवास reached the city. Indeed remarkable events were unfolding in the background. The Mogol tyrant was undergoing a remarkable transformation \[read about it as detailed by [श्री sarvesha तिवारी](http://bharatendu.com/)] and he had elevated the Hindu तोडरमल्ल to handle the finances of his Ulus. The city that तोडरमल्ल bestowed his attention was वाराण्सी, which he renovated and made the center of his scheme of reviving Hindu scholarship. He attracted numerous learned ब्राह्मण-s to the city to participate in the production of an enormous medical and chemical encyclopedia among others. Having reached the city in 1581 CE, श्रीनिवास wrote a definitive commentary on the hallowed नित्याषोडशिकार्णव named the अर्थरत्नावली. He also composed a mantra manual on the worship of shiva titled the शिवार्चन-chandrika.

His son named जगन्निवास was sought by rAma शाह, the rAjpUt ruler of Chanderi. rAma शाह was the son of madhukara शाह and had fought the Moslems in many battles before finally seizing Chanderi after defeating the forces of the Mogol tyrant Akbar, which were lead by the Jihadis Ali Quli Khan and Jam Quli Khan. rAma शाह appointed the young जगन्निवास as his preceptor and tutor for the clan. But shortly thereafter an internecine fight broke out in the bundela clan -- वीरसिंह the other son of madhukara शाह killed Abu Fazl who had been sent to conquer Bundelkand by Akbar and strengthened himself in the old bundela capital of Orcha. This resulted in a conflict between rAma शाह and vIra सींह, when Jahangir interceded and had rAma शाह captured and imprisoned in Delhi. At this time जगन्निवास moved with rAma शाह's grandson bhArata शाह. In 1608 rAma शाह and Jahangir signed a truce in return for the former giving the latter troops. Upon his release rAma शाह moved to the province of Bar where he declared himself राजन्. Subsequently, in 1612 the warlike bhArata शाह succeeded rAma शाह. In 1616 he moved on Chanderi and captured it to found the second bundela dynasty. Here जगन्निवास helped with designing new forts which were built in Talbehat, Haraspur and Golakot the ruins of all of which are in the vicinity of Gwalior and some irrigation works. Additionally, जगन्निवास also helped in training bhArata शाह's son देवी सिंह along with his own sons शिवानन्द, जनार्दन and चक्रपाणि. This देवी सिंह then studied along with शिवानन्द as both of them shared great interest in a wide range of topics such as medicine, astronomy, संस्कृत literature, tantra-s and dharmaशास्त्र-s. When देवी सिंह ascended the throne of Chanderi at the young age of 16, he and शिवानन्द started indulging extensively in their intellectual pursuits. The राजन् was initiated into कादि श्रीविद्या by शिवानन्द.

Under the patronage of देवी सिंह, शिवानन्द started composing a variety of which include:

 1.  वैद्यरत्ना: A work on Ayurveda with an auto-commentary.

 2.  tithi-निर्णय, बालविवेक and मूहूर्त-ratna: works on time-keeping

 3.  व्यवहार-निर्णय and आचार-sindhu: dharma-शास्त्र works

 4.  संप्क्षेप-प्रायश्चित्त: a work on penances and atonements.

 5.  महाभारत सुभाषित संग्रह: didactic material from the महाभारत.

 6.  कारककोश, taddhita kosha, समास kosha, स्त्री-pratyaya kosha and सीम्ह siddhAnta दीपिका: all grammatical works

 7.  several stotra-s to various deities including an encoded श्रीकुल तान्त्रिक stotra titled the त्रिपुरा stotra.

 8.  महाविद्यालहरी: A तान्त्रिक poetic collection.\
Under his guidance his patron देवी सिंह also wrote a medico-tAntric volume titled the सिंह-siddhAnta-सुधानिधि.

 9.  Finally he composed his magnum opus the सिंह-siddhAnta-sindhu. This is beyond doubt the greatest tantric compendium ever composed in the medieval period and illustrates the enormous scholarship of this great ब्राह्मण. The work is definitely more comprehensive than the shArada tilaka, प्रपञ्चसार, श्रीविद्यार्णव, mantra-mahodadhi, mantra-प्रकाश, प्रपञ्चसार-संग्रह, बृहत्-tantra-sAra and पुरश्चर्यार्णव. It is the true course to be followed by a high-level स्मार्त tantric ritualist -- in 92 तरंग-s spanning 35310 sholka-s. It covers पाञ्चरात्र as well as shaiva mantra-शास्त्र and aims to cover all manner of mantra-s. Of the texts he quotes some apparently lost texts are of interest:\
\*guha-kalpa: कौमार rituals; \*rituals of the परिशिष्ठ-s of the Chandoga-s; *सौत्रमणि-tantra; *षडन्वय महारत्न: a trika-kaula work; *लक्ष्मीकुलार्णव: a श्रीकुल text with vaiShNava orientation; * A ऋग्वेदिc परिशिष्ठ of the बह्वृच-s.

His citations show his encyclopedic knowledge of siddhAnta tantra-s including some archaic ones like निश्वास saMhitA (पाशुपत elements of it), कालोत्तर, पिङ्गलामत and मोहचूडोत्तर. It is also clear that he had access to the pichumata (i.e. the real brahma-यामल rather than the text followed by the पण्टारम्-s of the Tamil country), विष्णु-यामल, देवी-यामल, bhairava-यामल and jayadratha यामल texts. He also shows knowledge of the Kashmirian हादिविद्या based श्रीचक्र ritual manual termed the ललितार्चन-चन्द्रिका. Another text of interest that he cites in the context of the कपालव्रत is the सोमसिद्धान्त. It is believed that such a कापालिक text might still be extant in वाराणसी through the grape-vine and manuscript catalogs but I have no confirmation for this. He also repeatedly cites a tantra called the मालिनीविजय, but from the citation it is clear that what is meant is that more precisely called the मालिनीविजयोत्तर. An unreferenced mention of seven crore विद्या-s and mantra-s (I guess) might be derived from the tantra-सद्भाव, but then some other tantra could also mention this figure. One of the kaula texts cited by him is the tantra termed कुलमूलावतर which is cited by various authors from the वङ्ग/कलिङ्ग, drAviDa and kAshI suggesting that it was a popular text. The हंसपारमेश्वर cited by शिवानन्द is enigmatic. Based on citations by the Kashmirian पाञ्चरात्रिक-s people have claimed that it is a vaiShNava tantra. However, it is cited on multiple occasions by shaiva authorities in the Kashmirian world and शिवानन्द's context favors a shaiva interpretation. So it is not clear if there were different हंसपारमेश्वर-s of both the shaiva and vaiShNava variety.

His vaiShNava tantra citations include the हयशीर्ष saMhitA showing that this text was indeed more widely distributed than many people have thought. Interestingly his नृसिंह kalpa appears to have been likely the overlapping or similar to the नृसिंह section of the "आञ्गिरस" kalpa of the atharvan-s from the कलीङ्ग country.

As the Mogol invasion of Bundelkhand progressed शिवानन्द moved to the court of अनूप-सिंह of Bikaner along with his brother जनार्दन and their sons. This अनूप सिंह, who was a great collector and preserver of manuscripts, gave them the grant of two villages and settled them there. But शिवानन्द's son श्रीनिकेतन settled in court of the rAja of Jaipur, the astronomer. When in Bikaner we find that शिवानन्द composed two श्रीविद्या manuals titled:

 10.  ललितार्चन-kaumudi

 11.  ललितार्चन-दीपिका

 12.  A manual of कार्तवीर्य and दात्तात्रेय prayoga-s known as the कार्तवीर्य-vidhi-ratna.

 13.  सपर्याक्रमदर्पण-stotra of unknown contents.\
His brother जनार्दन composed:

 1.  A worship manual for the lay votary in Hindi for the worship of विष्णु and लक्ष्मी: the लक्श्ंईनारायण्-पुजा-sAr.

 2.  An erotic work titled the शृङ्गार shataka.

 3.  His own mantra-शास्त्र manual though shorter than that of his brother's titled the mantra-चन्द्रिका.

 4.  महालक्श्ंई-स्तुती.\
The last notable scholar in this North Indian branch of the clan that I have been able to trace is नरसिंह-लाल् who composed:

 1.  A mantra manual for दुर्गा worship: दुर्गार्चन paddhati

 2.  A nitya-naimittika karma manual

 3.  sundarI-गोपाल पद्धती on the syncretic श्रीविद्या combined with the gopi-jana-vallabha mantra system of the late पाञ्चरात्रिक-s.\
Other works by this illustrious clan of तान्त्रिक-s like one on construction of an astrolabe by भैरवनाथ, टीका-s on पराशर's astronomy and the यन्त्रमालिका by paramasukha are found scattered over North India.


