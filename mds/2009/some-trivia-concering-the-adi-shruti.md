
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some trivia concering the Adi-shruti](https://manasataramgini.wordpress.com/2009/02/08/some-trivia-concering-the-adi-shruti/){rel="bookmark"} {#some-trivia-concering-the-adi-shruti .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 8, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/02/08/some-trivia-concering-the-adi-shruti/ "7:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Students of the veda often question each other on trivia to test their knowledge of the shruti. We have always believed that graphically depicting some of these produce a clear effect on the student. This presentation of trivia might be viewed in conjunction with an [earlier presentation](https://manasataramgini.wordpress.com/2007/10/27/the-mandala-graph-for-rishi-sharing/) that we had produced when we were in the mouth of कृत्या.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3486/3262607608_65182ed3d6.jpg){width="75%"}
```{=latex}
\end{center}
```



This is a basic summary of the RV by मण्डल. The height of the bar is the number of सूक्त-s while width is scaled as per the median सूक्त length in terms of number of ऋक्-s. One can note that शाकल (शाकल्य), the last redactor of the surviving text aimed at symmetry of मण्डल 1 and 10 between which the मण्डल follow a stepwise pattern in terms of length by number of सूक्त-s. But what is striking is that the median length of a सूक्त in terms of number of ऋक्-s is noticeably different between different मण्डल-s. We look at this below in greater detail.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm2.static.flickr.com/1387/3264672495_7c17f1dce5.jpg){width="75%"}
```{=latex}
\end{center}
```



This is a standard box-plot of the length distribution in terms of number of ऋक्-s of सूक्त-s in each of the 10 मण्डल-s of the RV. The median length is the point in the box, while the lower and upper quartiles bound the box and the whiskers give the complete range. The horizontal line is the median सूक्त length across the 10 मण्डल-s.

Some interesting points:

  - The two dramatic differences from the general trend are seen in the काण्व मण्डल(RV 8) and the पव्मान soma मण्डल (RV 9). The former has the greatest median सूक्त length and the latter the least median सूक्त length. Both are the top two most-densely connected मण्डल-s in the [ऋषि-sharing graph](https://manasataramgini.wordpress.com/2007/10/27/the-mandala-graph-for-rishi-sharing/), yet they are antipodes in terms of सूक्त length. Clearly, the 8th dominated by a काण्व core is different from all other मण्डल-s in with respect to length, including their आङ्गिरस cousins like the gotama-s and the भरद्वाज-s. The soma मण्डल is particularly striking because it contains the longest सूक्त and a few other particularly long सूक्त-s, yet the median length of a सूक्त in it is the shortest.

  - The गृसमद-s, विश्वामित्र-s, वामदेव gotama and atri-s never composed too long सूक्त-s (>25) though the first 3 had सूक्त-s of greater than or equal to overall median length. In contrast, the भरद्वाज-s and वसिष्ठ-s composed सूक्त-s some सूक्त-s of great length thought their median सूक्त length tended to be less than the overall median.

  - The मण्डल 1 and 10 despite having the same number of सूक्त-s differ in median सूक्त length, with RV 1 clearly exceeding the overall median.

The shortest सूक्त: RV 1.99 (1 ऋक्) This is supposed to be the locus of the insertion of the [secret श्रेधी-विद्या (or mysterious expansion ऋक्-s in the form of an arithmetic series)](https://manasataramgini.wordpress.com/2006/03/11/jatavedase-and-shredhi-vyavahara/).\
Longest सूक्त= RV 9.97(58 ऋक्-s)\
Median सूक्त length for RV across the 10 मण्डल-s=9

These features have implications for the nature of the old ritual and the tradition of redaction of the RV text. Clearly the tendency reasonable clustering of most books close to the overall median distribution suggest that the old Indo-Aryan ritual was centered around the recitation of सूक्त-s of about 7-9 ऋक्स् along with fire oblations. This might have also defined the preferred length that an extempore kavi could compose at a rite. The longer hymns in family books other than the काण्व-s are always related to specialized issues: e.g. the ashvamedha ritual hymns in RV1, the astronomical and philosophical hymn of दीर्घतमस् in RV 1, the medical toxin hymn of agastya in RV 1, the philosophical vaishvadeva hymns of prajApati विश्वामित्र in RV 3, the fire kindling and installation hymns of the भरद्वाज-s in RV 6 or वसिष्ठ's hymn of यतुधान-s etc in RV 7 are examples of such. This again supports the issue that the observed median length is a reflection of the extempore composition at the more archaic form of the shrauta ritual, whereas the longer hymns are for specialized purposes. Again, the notably shorter median length of the soma मण्डल indicates that the liturgy specifically used in the archaic soma rite typically featured shorter compositions centered on a size of 6 ऋक्-s. Thus there appears to have been a distinction between the compositions used in the fire ritual and those in the soma ritual. However, the soma मण्डल contains the longest सूक्त and some other long सूक्त-s that are clearly outliers. These सूक्त-s upon examination are clearly later redaction efforts that combine सूक्त-s or their fragments from different authors into a single long सूक्त. For example the longest सूक्त is a compilation of the सूक्त-s of short length by 11 वसिष्ठ-s and a single आङ्गिरस. These later redactions appear to mark the beginning of the move from the archaic short soma-specific composition to the classic shrauta liturgy of a draw-out soma recitation.

The काण्व-s who are exception, are also characterized by unusual meters and guru/laghu distributions in common meters like gAyatrI. This observation, taken together with the multiple features suggesting a closeness of the काण्व-s to the Iranian zone, implies that the काण्व-s probably had a distinct system of ritual performance. They organized compositions into longer सूक्त-s, which were possibly an archaic version of the longer shastra-recitation seen in the classical shrauta ritual, where multiple shorter सूक्त-s are strung together. Finally the difference between मण्डल 1 and 10 is reflected in fact that while मण्डल 1 is largely a collection of short family books, the terminal part of RV 10 is a collection of several short सूक्त-s of unclear provenance that probably formed a floating mass of tradition.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm2.static.flickr.com/1206/3264794055_a938f3d251.jpg){width="75%"}
```{=latex}
\end{center}
```



The frequency distribution of सूक्त-s by length in number of ऋक्-s across all 10 मण्डल-s. Note across all मण्डल-s a length of 5 ऋक्-s appears to be most common. The blue vertical line is the median सूक्त length across the entire RV saMhitA.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3523/3267448529_a66e4eeb24.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[Click here to magnify](http://farm4.static.flickr.com/3523/3267448529_a66e4eeb24_b.jpg)\
The frequency distribution of सूक्त-s by length in number of ऋक्-s in each of the 10 मण्डल-s.The the most frequent सूक्त size and the median सूक्त size are indicated for each मण्डल. A most frequent सूक्त length of 5 ऋक्-s appears to be most common individual मण्डल-s too. However, note the dramatic difference in the वामदेव(RV 4) and काण्व (RV 8) मण्डल-s.


