
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The gandharva flight](https://manasataramgini.wordpress.com/2009/09/06/the-gandharva-flight/){rel="bookmark"} {#the-gandharva-flight .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 6, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/09/06/the-gandharva-flight/ "6:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3418/3891424867_e6a6dfa66e.jpg){width="75%"}
```{=latex}
\end{center}
```



We saw परचितांबा and परमशंभु in our आज्ञ chakra. Like a gandharva we flew through time and space holding our दूती in embrace. We gazed on the hill of चण्डिका from afar. It was truly the hill of the भूत-s who dance along with भूतपति. On one end of the ridge was the pinnacle of the वेताल and the other end was the mound of चण्डिका. In between lay the circlet of stones of कपालीश-bhairava and rakta-चामुण्डा. Our chit rested in the mantra of the यामल -- the waves of existence merge into the bhairava and rebound resulting in his perception -- we behold the one conjoined with the shakti -- the लिङ्ग. We sense him who is known as vyomin and व्यापिन् by the सैद्धान्तिक-s. We descended to the ground ascended the steps towards Chatra of विनायक and then moved to the मण्डप of कालिका. Then having saluted the लिङ्ग of सोमाख्येश्वर that we had installed in the days of yore we moved with our दूती to the श्मशान of वेतालेश्वर where we had installed the लिङ्ग of रवीन्द्रेश्वर after battling the dasyu-s. There on the muNDa मञ्च we took our seat with our दूती as shakti who is offered उपचार. Thereafter she opened the way for the paramasukha that is not available to those trapped in the lower existence of dissipation. We remained absorbed in yoga of अभिसंबोधि witnessing dance of the hordes of वीरभद्र and भद्रकाली.


