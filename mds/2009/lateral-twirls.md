
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [lateral twirls](https://manasataramgini.wordpress.com/2009/02/27/lateral-twirls/){rel="bookmark"} {#lateral-twirls .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 27, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/02/27/lateral-twirls/ "2:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3304/3313206638_1e6c8c4036.jpg){width="75%"}
```{=latex}
\end{center}
```



