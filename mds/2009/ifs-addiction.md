
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [IFS addiction](https://manasataramgini.wordpress.com/2009/10/31/ifs-addiction/){rel="bookmark"} {#ifs-addiction .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 31, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/10/31/ifs-addiction/ "7:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3477/4065437461_368362e287.jpg){width="75%"}
```{=latex}
\end{center}
```

\
This is our 20th year with this addiction ...

