
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [मण्डल-1](https://manasataramgini.wordpress.com/2009/01/11/mandala-1/){rel="bookmark"} {#मणडल-1 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 11, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/11/mandala-1/ "5:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3463/3183602745_bfcbd606ef.jpg){width="75%"}
```{=latex}
\end{center}
```



