
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some sound changes in IA](https://manasataramgini.wordpress.com/2009/01/14/some-sound-changes-in-ia/){rel="bookmark"} {#some-sound-changes-in-ia .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 14, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/14/some-sound-changes-in-ia/ "5:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some neo-Indo-Aryan languages sound unusual to us because they are not our native tongue and very "discordant" with respect to संस्कृत. Hence, those "funny" words sort of make us sit up and try to figure out their evolutionary paths. Today we were amused by some reminiscences of usages in marAThi that was spoken in the land of our youth. This led us to some peculiarities in sound changes as this branch of Indo-Aryan, which probably included marAThI, gujarAtI and कोङ्कणी diverged from its ancestor.

  - ------------------------------------- ------------- -------------- -------------
               **Sanskrit**               **Marathi**   **Gujarati**   **meaning**
     *[इक्षु]{style="color:#99cc00;"}*          Us             Us         sugarcane
   *[उपलक्षयति]{style="color:#99cc00;"}*      ओळख्ने         ओळख्वु\~.n      recognize
     *[क्षण]{style="color:#99cc00;"}*          saN           khaN        festival

  - ------------------------------------- ------------- -------------- -------------

Thus even as marAThi and gujarAti-s ancestors broke off from the more northern radiations there was an unusual क्ष्\>s transition, with his change apparently happening to a greater degree to the South i.e. in marAThi.\
Now a semantic question why does संस्कृत क्षण give rise to saN/khan which means a festival while the former means a moment or instant. Apparently there are rare instances (even the notorious MW dictionary lists some) where क्षण is used for a festival in संस्कृत. Given that festivals were marked by the instant or moment of the moon entering a certain नक्षत्र or attaining a certain tithi or the holy मुहूर्त, it appears that word क्षण in a calendrical sense became equated with festival in this southern Indo-Aryan branch.


