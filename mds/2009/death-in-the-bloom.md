
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Death in the bloom](https://manasataramgini.wordpress.com/2009/11/26/death-in-the-bloom/){rel="bookmark"} {#death-in-the-bloom .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 26, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/11/26/death-in-the-bloom/ "4:34 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2537/4136351132_d817a99255.jpg){width="75%"}
```{=latex}
\end{center}
```



