
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Reviving memories again: the local flora](https://manasataramgini.wordpress.com/2009/11/25/reviving-memories-again-the-local-flora/){rel="bookmark"} {#reviving-memories-again-the-local-flora .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 25, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/11/25/reviving-memories-again-the-local-flora/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2651/4130747497_f94fe339f9.jpg){width="75%"}
```{=latex}
\end{center}
```

\
Of course now the tale of "फूल् Akhir phal banA" appears a distant joke despite all the nightmares in between :-)

