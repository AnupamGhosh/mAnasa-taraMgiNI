
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on खेचरी mudra](https://manasataramgini.wordpress.com/2009/07/02/some-notes-on-khechari-mudra/){rel="bookmark"} {#some-notes-on-खचर-mudra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 2, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/07/02/some-notes-on-khechari-mudra/ "6:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2452/3700634334_9854185ac8.jpg){width="75%"}
```{=latex}
\end{center}
```

\
A sagittal section through the head of a normal human.

Hy-hypothalamus;Pi-pituitary; Po-Pons; Me-Medulla oblongata; T-tongue; kh-खं, the place where the tongue should press in the खेचरी mudra; rd- rAja danta.

Having performed व्योमयान we translocated to the most beautiful city of ratnapura on the shores of the paschima mahodadhi and occupied a room on the समुद्रतट. There, while watching the purple sunset draped by the Russian volcano, we performed our संध्योपासन by pouring water from the raging waters of the सागर as arghya. We then curled up to watch a remarkable documentary by a जटाधारी English baron called Jim Mallinson, which was recommended to us by Sharada. In the documentary Mallinson shows a temple of the sapta-bhagini from the ancestral regions of Sharada and R, but the actual icons in there are nine in number. Since we had done some investigation of the sapta-भगिनी cults in the regions around the city of our youth, Sharada asked us about the extra two images. But we got sidetracked by something else. Sir Mallinson seems to be a pretty interesting guy as he stated in the documentary that he was both a paraglider and the student of a text which I understood to mean the खेचरी-विद्या. Checking him out on the web showed that he had indeed written a work on the खेचरी विद्या that appears to be out of print now. We shall record some notes on the खेचरी mudra here rather than talk about Mallinson's documentary or the sapta-भगिनी cult and its evolution.

The खेचरी mudra is a yogic practice of great antiquity emerging in late vedic stratum first represented in the मैत्रयणीयोपनिषद् the only surviving portion of the मैत्रायणीय ब्राह्मण in both manuscript and a precarious oral tradition. In this text the खेचरी mudra is expounded by शाकायन्य to the magadhan king बृहद्रथ along with proto-"कुण्डलिनि" yoga (ंऐऊ 6.20-21). The खेचरी mudra here is described as the great practice by which one has the experience of ब्रह्मैवाहमस्मि, a key teaching of vedAnta. The practice was incorporated into the early classical yoga of epic period. Subsequently, it was acknowledged by the तथागत as a means of achieving the state of a muni. It is clear that in one his sutta-s known as the नालक sutta (verse 38), in the महावग्ग of the सुत्तनिपात, the तथागत expounds the खेचरी mudra just as in the उपनिषद् as the means of achieve the state of knowledge i.e. that of the muni. Thus, it becomes clear that खेचरी was already well established in the yogic circles by the time of the तथागत. Hence, it is not surprising that it continued in its popularity with the emergence of the shaiva mantra-मार्ग. That it was present in the common ancestor of shaiva mantra मार्ग is clarified by the fact that it is described in the archaic text of the kula system the कौलज्ञान निर्णय as well as the सैद्धान्तिक tantra-s.

But it was in the kaula streams the खेचरी evolved into a distinct practice under that name with miraculous properties. In the कौलज्ञान निर्णय it is described as a yoga which destroys diseases. Here for the first time the meditation on a multiplicity of gustatory sensations while performing the mudra is alluded to: when the yogin perceives a sweet taste he is able to reverse graying of hair and wrinkles; when he perceives the taste of milk he becomes amartya; when he perceives ghee he becomes स्वराट्. In the trika kaula system we find that it is described in greater detail incorporated into a meditation on the central trika goddess parA. Here too it is a means of destroying disease and decrepitude. The 21'st chapter of the मालिनीविजयोत्तर states that the तान्त्रिक invokes parA देवी in the middle of a 16-spoked chakra, with the disc of he moon as its hub, in the midst of the void of the upper oro-nasal cavity -- this is what is precisely meant by the term khaga in MVUT:\
[षोडशारे खगे चक्रे चन्द्र-कल्पित-कर्णिके ।\
स्वरूपेण परां तत्र स्रवनीम् अमृतं स्मरेत् ॥ ंVऊट् २१।२]{style="color:#000080;"}\
Thus we see that the term खेचरी comes from the fact the tongue moves to the kha the space of the upper oro-nasal cavity. The tantra goes on indicate that in the initial practice of the mudra the साधक experiences a salty or iron taste (That this is default state can be easily verified by even merely moving the tongue backward in a regular person when having a clean mouth). By repeated practice the mudra he perceives a sweet taste and drinks that. Once he achieves the sweet taste he can perceive the tasting of all kinds of fluids he meditates upon, like blood, liquor, animal fat, ghee and milk.

In the subsequent yogic literature the खेचरी mudra is even described as a means of holding of retas. It is said that a yogin who practices it does not have an outflow of shukra even when he is in coitus with a beautiful woman:\
[तेनैषा खेचरी नाम मुद्रा सिद्ध-नमस्कृता ।\
खेछर्या मुद्रया यस्य विवरं लम्बिकोर्ध्वतः ॥\
बिन्दुः क्षरति नो यस्य कामिन्यालिङ्गितस्य च ।\
यावद् बिन्दुः स्थितो देहे तावन् मृत्यु-भयं कुतः ॥]{style="color:#000080;"} ध्यानबिन्दु 83-84\
Further, from the shaiva stream, the खेचरी also appears to have been transferred to the yoga of the पाञ्चरात्र vaiShNava system (See below).

The classical standalone खेचरी system came into its own relatively late, resting on the foundations of the श्रीकुल and the पश्चिमाम्नाय tantra-s. The खेचरीविद्या states that shiva first taught this system in various tantras such as the महाकाल saMhitA (पश्चिमाम्नाय), the glorious योगिनीजालशम्बर (श्रीकुल) and vishuddheshvara tantra-s. Then shiva adds that though these tantra-s cover the matter in different levels of details, it is the खेचरीविद्या text which would provide a devoted description of the procedure. The holy नित्याषोडशिकार्णव of the श्रीकुल again states that amongst the preceding tantra-s before the "अर्णव-ization" in the श्रीकुल stream were the YJS and Vइष्. Thus, the खेचरीविद्या in its classical form is an offshoot of the procedure proclaimed in the foundational kaula texts. It is here that the important mantra of the melaka is taught, which is supposed to be combined with the actual yogic practice of खेचरी:\
[ह्रीं भं सं षं [पं] फं सं क्षं ॥\
]{style="color:#000080;"} As per the खेचरीविद्या the practice of the mantra is a must for attaining खेचरी siddhI -- this point is repeatedly mentioned and the मन्त्रोद्धार is given before the yogic procedures. The mantra itself of the form given above was current amongst my coethnics who practiced it in Thanjavur. However, I have seen a variant form which replaces the ShaM with a paM. This might also correspond to a variant uddhara, which I have seen, but was not able to completely comprehend due to the lack of clarity in the varga being indicated.

Then the खेचरीविद्या text goes on to expound the practice of the खेचरी mudra itself. Here we encounter the version that involves cutting of the frenulum linguae, something not found in the earlier texts. The text states that after having cleaned the palate for week by rubbing the yogin incises frenulum by a hair diameter using a sharp scalpel shaped like a स्नुही leaf. He then rubs the area with a mixture of rock salt and the पथ्या plant. He repeats the procedure after 7 days and it is said that in 6 months he cuts of the frenulum. After that he wraps his tongue's tip with a cloth and stretches it. After carrying out this exercise for 6 years he would have stretched his tongue long enough for the mudra. The text warns that he should do it very gradually and if he rushes with it he destroys his organ. Then he practices the entry of tongue behind the soft palate into the oro-nasal cavity of the pharynx. With 3 years of practice he is able to press the wall of the cavity well above the anterior arch of the atlas vertebra and reach the base of sphenoidal sinus. The yogin then may practice manthana. If he has siddhi of the खेचरी mantra then he does not need churning, but if he can combine both then it is even better. In this he inserts a metal wire with a capsule at the end into to the nasal cavity with a thread and performing हृद्य प्राणायाम he churns the capsule gently while focusing on the center of his eyebrows. He does this once a month. Then keeps pressing with his tongue the base of the sphenoidal sinus and licking the surface with his tongue in the act called the drinking of अमृत. He focuses on the hypophysis and apparently stimulates it with the action of his tongue. When he tastes the अमृत he gradually attains the siddhi-s of खेचरी over several years of practice. These are described in hyperbolic terms: freedom from decrepitude, life stretched to 1000s of years, mastery of शास्त्र-s, knowledge of buried treasures and underground caves, दूरदृष्टि and दूरश्रवण, strength of 10000 elephants, success in chemistry and ability to repel अभिचार prayoga-s.

The vaiShNava version of खेचरी that I have encountered is from the yoga section of the शाण्डिल्य saMhitA. In this text the खं in the upper pharyngeal cavity is described as the seat of विष्णु. The yogin meditates on it and performs the खेचरी mudra largely as described in the खेचरी विद्या text. In this consideration of शाण्डिल्य, the खेचरी mudra contains a mention of the cutting of the frenulum linguae as in the more mature shaiva texts. It also uses yoga terminology relating to the chakra-s and the meditation of विष्णु in the सुशुम्ण typical of the yoga of the shaiva tantra-s. We see an evolution of the खेचरी and the associated yoga system within the shaiva tantra-s, but I have not encountered a parallel evolution in the पाञ्चरात्र texts. So it seems that खेचरी was transferred to the vaiShNava system relatively late from the shaiva system. However, I must stress that more careful examination of earlier vaiShNava saMhitA-s might change this picture.

To the outsider the hyperbole surrounding the खेचरी mudra appears to give the impression of the whole thing being bogus. Some others say it has nothing to do with regular anatomy, but some kind of perceived or "yogic" anatomy. Hence they argue that its effect should not be considered in terms of regular anatomy. While I have nothing to say of the great siddhi-s mentioned in connection with the mudra, I am aware of yogin-s both vaiShNava and स्मार्त (practicing श्रीकुला tantra-s) who have practiced it, albeit without cutting their frenulum linguae. It is of central importance for their practice along with the triple bandha and is the basis for entering the state of चिदानन्द or some say samAdhI. While I cannot speak for myself in this case, the reliable experience of the yogin-s suggests that the mudra has a very definitive physiological effect. These effects suggest that, though it might seem rather unbelievable, the खेचरी mudra might stimulate parts of the brain that are adjacent to where the action takes place. This I suspect might account for the experiential aspects of their practice. More controversially it is conceivable that somehow the practice stimulates the hypophysis to release lutenizing hormone. I have not tested this hypothesis in anyway, but one cannot rule it out. In conclusion I believe that the खेचरी is a real discovery of the Indo-Aryans that has been preserved as a central element of the tradition of yoga, while being refined through the ages. I must add that some practitioners of it do display some baffling capabilities (which insiders are well aware of) although none of this tested in any controlled way.


