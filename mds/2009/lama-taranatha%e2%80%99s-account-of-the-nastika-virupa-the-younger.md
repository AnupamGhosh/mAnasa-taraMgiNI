
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Lama तारनाथ's account of the nAstika विरूपा the younger](https://manasataramgini.wordpress.com/2009/11/03/lama-taranatha%e2%80%99s-account-of-the-nastika-virupa-the-younger/){rel="bookmark"} {#lama-तरनथs-account-of-the-nastika-वरप-the-younger .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 3, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/11/03/lama-taranatha%e2%80%99s-account-of-the-nastika-virupa-the-younger/ "7:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the kaccha country in the west lived a king named विभरट्ट during whose reign lived the siddha विरूपा. He had a daughter who became the queen of देवपाल. विभरट्ट had a temple that enshrined both bauddha and Astika images. The राजन् was a bauddha but his ministers were तीर्थिक-s. While building the temple the राजन् placed human-sized images of both तीर्थिक and bauddha deities in the same shrine. The bauddha-s asked him to build separate temples instead. The तीर्थिक-s wanted them together in the same shrine and the ministers agreed to this. The king asked विरूपा to do प्रतिष्ठ of the images. He did no ritual but simply said in a Bengali form of apabhramsha "Aisha Aisha". All the idols came out and assembled in the corridor of the temple. He then asked the deities to sit down and they all sat down on the floor of the corridor. He then took a pot of water and sprinkled it on the heads of the deities. The deities of the bauddha-s got up and walked into the shrine laughing aloud. The deities of the तीर्थिक-s remained seated outside with their heads hung low. This temple named अमृतकुम्भ still exists.


