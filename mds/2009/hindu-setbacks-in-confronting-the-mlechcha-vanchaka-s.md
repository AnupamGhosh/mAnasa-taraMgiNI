
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Hindu setbacks in confronting the म्लेच्छ वञ्चक-s](https://manasataramgini.wordpress.com/2009/03/07/hindu-setbacks-in-confronting-the-mlechcha-vanchaka-s/){rel="bookmark"} {#hindu-setbacks-in-confronting-the-मलचछ-वञचक-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 7, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/03/07/hindu-setbacks-in-confronting-the-mlechcha-vanchaka-s/ "7:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The news reached us through certain channels that the Hindu-s had met a defeat in their attempt to correct biases against them in the textbooks of the state of California. While our own life hangs in uncertainty and the कर्ण-yakSiNI in whose embrace we lay has been bound, we still felt it was in order to write a note around this matter, rather than precisely about it. We will begin by stating that Hindu-s have to be uncompromising on the following: 1) Rejection of that most reprehensible term South Asian and the geographical description "South Asia". This is a blatant attempt by the म्लेच्छ-s by the म्लेच्छ-s to destroy Hindu identity and deny ownership over their geography. Not without out reason do the detractors and revilers of Hindus delight in using "South Asian" and South Asia. Any Hindu organization should reject it and use जम्बुद्वीप and भारतवर्ष. They may in following म्लेच्छानुसार use Indian subcontinent, Greater India. 2) Hindus should not waste effort in emphasizing their racial distinctness as the basis their identity. At the same time there is no reason to deny their racial background or obstruct or stonewall studies pertaining to their genetic origins. Hindus should be willing to accommodate म्लेच्छ-s in their midst who come over to the dharma after completely rescinding their affiliation to the कीलितप्रेत-mata or other राक्षसमत-s . 3) Hindus should not claim to be monotheistic and should instead proudly proclaim their polytheism and educate their people in performing खण्डन of the एकस्वामित्व of the दुष्ट-s of the कीलितप्रेत-mata and other राक्षसाचरिन्-s.

The setbacks of Hindu-s in this effort, as in the past, stem from their lack of understanding of the म्लेच्छ-s and their systems. To understand the म्लेच्छ-s the Hindu-s need to figure out the different currents in the म्लेच्छ world. In this effort the biggest problem was seeing through "secular" face of the म्लेच्छ-s. The प्रेताचरिन्-s in the म्लेच्छ-desha can be made out relatively easily, but the Hindus generally failed to see through the nature of the secular variants. They tried in a futile attempt to link the secular subversionists like the Harvard indologist and his band of 50 वञ्चक-s with the प्रेताचरिन्-s. What they did not realize is that the म्लेच्छ system by design maintains two versions of its foundational dogma, the secular and the overtly प्रेताचारिन् variety to befuddle and trap their rivals. Not having seen through this trap the Hindu-s fell into right into it and were taken apart by the म्लेच्छ-s.


