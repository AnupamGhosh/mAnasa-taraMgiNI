
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [vertical twirls](https://manasataramgini.wordpress.com/2009/03/01/vertical-twirls/){rel="bookmark"} {#vertical-twirls .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 1, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/03/01/vertical-twirls/ "2:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3461/3318159314_6520d283c2.jpg){width="75%"}
```{=latex}
\end{center}
```



