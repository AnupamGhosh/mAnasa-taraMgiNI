
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नागार्जुन's head](https://manasataramgini.wordpress.com/2009/08/11/nagarjuna%e2%80%99s-head/){rel="bookmark"} {#नगरजनs-head .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 11, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/08/11/nagarjuna%e2%80%99s-head/ "6:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Remember नागार्जुन's head. The prince wanted to cut off नागार्जुन's head and advanced with a sword. He thought that no one knew of his ways. But when two men speak thinking they are alone राजन् वरुण hears as the third. The मन्त्रवादिन् said:\
[*इदम् इन्द्र शृणुहि सोमप यत् त्वा हृदा शोcअता जोहवीमि ।\
वृश्cआमि तं कुलिशेनेव वृक्षं यो अस्माकं मन इदं हिनस्ति ॥*]{style="color:#99cc00;"}*

  -  The भरद्वाज has spoken, the foot of the striding विष्णु is upon the सध्या's head. May he stride widely and thunderously. [उरुगायो।अद्भुतः]{style="color:#99cc00;"}


