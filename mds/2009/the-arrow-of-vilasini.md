
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The arrow of विलासिनी](https://manasataramgini.wordpress.com/2009/01/04/the-arrow-of-vilasini/){rel="bookmark"} {#the-arrow-of-वलसन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 4, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/04/the-arrow-of-vilasini/ "7:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It was just before दशान्त. We were on the brink of shaking off the shackles that had constrained us for several years. We were happy that rather than wasting our efforts on the mundane, which attracted our fellow travelers, we had spent some time studying the पुराण-s and even as the battle had just begun the सूक्त-s of the veda. We decided to set out on one great wandering that resembled the course of a comet. We got out of the कारागृह and accompanied by two chaturtha-s and rode up to a रसशाला some distance away. There the 3 of us ordered इक्षु-rasa, the spinach भृज्जिक and took our seats under a sprawling चिञ्चिणी tree, where we were to be favored by the देवी वक्रेश्वरी. For about half an hour we savored the food and drink talking of the past years that had flown past us like the great सुपर्ण in the quest for soma. I was the भार्गव, whose ancestors saw time, for the shruti says: "kAle .ayam अङ्गिरा devo अथर्वा cआधि तिष्ठतः ||". Hence, I saw the great Chidra that opened in the 4th dimension that my शूद्र companions did not perceive. After that session at the रसशाला we rose to depart. We all moved to our ashva-s when I saw the शूद्र durmukha vanishing for ever into the Chidra that was opening up. Knowing the nature of काल we smiled: "kAlo ha विश्वा bhUtAni kAle cअक्षुर् vi pashyati |".

We then crossed the shrine of The god and having worshiped his लिङ्ग proceeded to the उद्यान. There we espied her of यक्षिणी-like form with her thick kesha enveloping her skandha, stana-dvaya, marble-like पृष्ठ, even as the monsoon nebulosity spreading from the कोङ्कण. The sight of her stanau made one attain brahmaikya for on seeing them one said I am indra and you are विलिस्तेङ्गा. Her नास shone like a fair svaru of polished khadira wood attached to the यूप at an याग and her eyes sparkled like the कृत्तिका-s on a winter night. The sight of her nitamba-bimba made one lose the concept of time as thought eternally stationed in the विशुवान्. We had the सौभाग्य of the rati emerging from her klinnavoru-dvaya. In her अलिङ्गन and चुम्बनदान we felt that we were invincible. If only the grasp of the कालाजगर were as pleasurable as her bhujabandhana-s then who would verily fear काल at all? We had thus had a brush with the bhoga which few ever get to enjoy. In the end the wide snarling mouth of काल swallowed even this bhoga, verily like the invincible droNa, सूतपुत्र and भीष्म entering the mouth of the all consuming काल in the vision of savyasachi. At that moment we saw the two flows of rati -- the lower stream and the higher stream. We rushed by natural tendency to the lower stream but the intense pleasure of rati suddenly turned into the very fire of the wrath of rudra. Then looking up from the stream of pain we saw the aperture of the higher flow. But by what उपाय where we to get there? Was there a yAna? Or were we to perish in the wrath of sharva emanating at the destruction of the दक्षाध्वर?


