
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कुमार त्रिशती: commentary](https://manasataramgini.wordpress.com/2009/09/15/kumara-trishati-commentary/){rel="bookmark"} {#कमर-तरशत-commentary .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 15, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/09/15/kumara-trishati-commentary/ "6:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The [कुमार-त्रिशती](https://manasataramgini.wordpress.com/2009/09/15/kumara-trishati/), also known as the शत्रुंजय त्रिशती, the rahasya त्रिशती, the guhya षण्मुख-nAma-संगीति or the skanda-nAma-सागर had recently been only orally transmitted. In this sense it is reminiscent of the विनायक rahasya सहस्रनाम that was taught by भास्करराय मखीन्द्र or that secret collection which completes the साधन of वाराही. In past from the परमपीठ of rohitaka it had been transmitted to the पश्चिंऔघ in the two achala-s (in the modern Punjab and Rajasthan), to the पूर्वौघ in deep वङ्ग country and to the दक्षिणौघ in the सह्याद्रि-s (the modern Konkan). From there it was transmitted to the glorious massif of कुमार-parvata (modern Kukke-Subramanya) and the shores of the दक्षिण सागर (Tiruchendur of the drAviDa poet नक्कीरनार्). The दक्षिणौघ and pashchimaugha were lost over time. The कौमाराचर्य-s of the वङ्ग retreating from the demonic shashidhvaja-s came to Bellary in the कर्नाट country and established a tapovana near the might rock of महामुख (a granite prominence still seen in Bellary). Here the learned तान्त्रिक रामानुज deshika acquired the stotra and transmitted it.\
...\
Long ago we wended our way up to the hilly spur in the city of our youth on which the पेश्वा बालाजी नानासाहेब् had resided. Atop it among several other temples was a shrine of skanda which was installed by रघुनाथ-rav after his conquest of Attock --- the farthest north the महाराट्ट-s reached. But the installation of कुमार was repeatedly botched up by the mahArAShTrI ब्राह्मण-s who had little knowledge of the कौमार lore*.

 1.  The idol was broken by Moslems during the invasion of the Nizam of Hyderabad.

 2.  nAnA फड्नविस् made a new idol an installed it next and it broke up on its own an year later.

 3.  A new idol was again made by फड्नविस् and installed and insufficient rites were done at its installation. Several years later a lightning strike brought down the temple, so a new idol was made.

 4.  After the British conquest of India the temple was taken away from the पेश्वा-s and given to a local trust. In the late 1800s the metal crown made for the idol by माधव-rav fell down and broke the idol's hand. So a new idol was made for the 4th time, this time funded by some vaishya.

 5.  In the 1980s the right eye of one of the faces of the idol fell out and was lost.\
This was when the muni and I arrived at the top of the hill spur on the षष्टी after the दीपावली. The prashnaka had informed me that the idol was botched up and needed a महाशान्ती, but the complete ritual system was not available to the purohita at hand. We lent him what ever aid we could, but went away a bit dissatisfied and not having the complete thing ourselves. The great student of our Acharya finally set things right through a secret prayoga. We went back to hill and stood silent in the afternoon quiet. It was then that we started receiving the secret instructions in the त्रिशती to perform the lengthy पुरश्चरण to be able to understand the secret mantra embedded in it. Refraining from काम-bhoga we performed it for a while. The mantra flashed briefly like lightning but we were unable to see anything more --- in fact it looked like skanda vara given the ब्राह्मण-s in quest of व्याकरण. Then the mantra revealed itself in stages. We saw the most basic level when it revealed its power to us, even as we were attacked by मदघूर्णित and simultaneously by rasagola and darsha in the fifth year after द्वादशान्त. We then saw a vaiShNava who had attained a क्षुद्र siddhi during a climb to the कुमार-गृह and were way laid for a while trying to attain the same.

After years of trying to force attain the क्षुद्र siddhi and reaching nowhere we witnessed two apparitions of the deva. They were followed by dismal troughs in course of which we said to ourselves: "surely this मन्त्रशास्त्र is hocus-pocus and we should move on in life". But unlike shabara we did yet throw away our rosary into the commode. Then finally the veil of ignorance was pierced, at least partially, and beyond that is something a man does not speak about.

  -  I am aware of perhaps 3 exceptions including the one in the lineage of लक्ष्मण राणाडे who raised on objection to this statement :-).


