
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [वासिष्ठ लूतिकाशास्त्र](https://manasataramgini.wordpress.com/2009/01/20/vasishtha-lutikashastra/){rel="bookmark"} {#वसषठ-लतकशसतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 20, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/20/vasishtha-lutikashastra/ "7:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3399/3211542845_7fb6c18432.jpg){width="75%"}
```{=latex}
\end{center}
```



An year before द्वादशान्त, लूतिका-योषा, the muni and I journeyed into a remarkable world of spiders in the unattractive settings of our city. However, the journey was only completed by लूतिका-योष in the footsteps of her ancient ancestor.\
The tentative identification of वसिष्ठ's spiders are provided below to match the arrangement on the image above.

  - --------------------- ------------------- ---------------------- ------------------------
   त्रिमण्डला: Cyrtophora    श्वेता: Herennia       कपिला:Argyrodes      पीतिका: Chiracanthium
     आलविष: Phidippus     मूत्रविष: Linyphia       रक्ता: Florinda         कसना: Ordgarius
    सौवर्णिका:Opadometa     लाजवर्ण:Clubiona       जालिनी:Nephila       एणीपदी: Smeringopus
    कृष्णा: Latrodectes     अग्निवर्ण:Thymoites   काकाण्डा: Stegodyphus   मालागुणा: Poecilotheria

  - --------------------- ------------------- ---------------------- ------------------------


