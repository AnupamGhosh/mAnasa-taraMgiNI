
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The third enslavement?](https://manasataramgini.wordpress.com/2009/10/21/the-third-enslavement/){rel="bookmark"} {#the-third-enslavement .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 21, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/10/21/the-third-enslavement/ "6:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The coming of the मरून्मत्त-s resulted in our first loss of freedom. The leukotestate indologists and their xanthotestate fellow travelers from the eastern fringe of the world told us that it was no enslavement but a cultural enrichment (Hey, we at least get to call the zilebia and the samosa India food). After the म्लेच्छ-s had been packed out of our land ending our 2nd physical enslavement, our eminent नेताजि-s, who were usually prone to pulling Gandhis, at least tested आणवास्त्र-s. Most म्लेच्छ-s with whom I have spoken feel no remorse about the killing of the 220000 प्राच्य-s (minimum) with their आणवास्त्र-s. In fact, some of them expressed pride and all of them offered all kinds of self-serving justifications. When asked about their well-documented plan to keep dropping several आणवास्त्र-s on the प्राच्य-s the म्लेच्छ-s tell us of course they would have done it and there was nothing wrong in that plan. At least the mauryan publicly recanted his killing of 100,000 कलिङ्ग-s with mere shalya-s and showed remorse for the act for the rest of his existence. It is for reasons such as this we describe the म्लेच्छ-s as barbarians. These same म्लेच्छ-s endlessly pontificate to us about giving up आणवास्त्र-s --- verily only a fool who thinks himself to be a paNDita will use this famous म्लेच्छ-nyAya. But with the turning wheel of the kali things has come to such pass that it appears that we have verily become such fools. The truth is that some of our आणवास्त्र-s might have been damp fizzes, much like the occasional wet विSफोटक we would experience in the दीपोत्सव-s of yore. But the turbaned and shrouded नपुंसक-s who pass off as पूज्य नेताजि-s these days do not want the people to know any of this. Why is this the case? After all now a days we have no kShatriya-s ruling us but a mere गणराजय where the klaibya नीलशिरस्त्राण is elected by the masses. So should the klaibya not do what the masses want? That would be the case only if the masses really demand it. But what has happened is that the masses have been put to sleep with क्रीड and काम that they no longer bother if they really have an आणवास्त्र or not. To drive the nail in to the coffin the परमपूज्य नेताजि-s are repeatedly lying to the people of bhArata claiming they have the astra. In the mean time they are selling the राष्ट्र to those same म्लेच्छ-s who have murdered hundreds of thousands of people in a nimesha. Truly we are poised to enter our 3rd enslavement --- what does it matter if it is directly under the म्लेच्छ-s, their प्रेताचारिन् proxies or some regional opportunist like the chIna-s or the मरून्मत्त-s. Even the woman, who in the Kazakh entertainment unties the horse knot with her teeth, might feel safer than this forsaken woman called भारती.


