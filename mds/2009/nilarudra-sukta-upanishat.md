
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नीलरुद्र सूक्त (उपनिषत्)](https://manasataramgini.wordpress.com/2009/03/16/nilarudra-sukta-upanishat/){rel="bookmark"} {#नलरदर-सकत-उपनषत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 16, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/03/16/nilarudra-sukta-upanishat/ "12:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

नीलरुद्र is part of the AV-पैप्पलाद saMhitA (सूक्त 3 of काण्ड 14). It is also considered an उपनिषद् of the early पाशुपत-s equivalent to the श्वेताश्वतर of the कृष्ण yajurveda. In the now lost shrauta traditions of the atharvaN-s the brahmA recited the सूक्त mentally, standing beside the यजमान, when the fifth layer of the altar for the soma ritual was completed. In the much later तान्त्रिक tradition it is held that the भार्गव transmission of the जया-tantra and unmatta-bhairava sUtra-s of the वाम srotas are emanations of the core नीलरुद्र mantra-s. In अथर्वण विधान prayoga, नीलरुद्र can be combined with the rudra mantra-s of the उच्छुष्म kalpa for each of the viniyoga-s. A person who is not of the भार्गव clan, or uninitiated into the अथर्वण shruti needs to undergo a second atharvanic upanayana to perform prayoga-s with these mantra-s. However, if one desires to study it as an उपनिषद् as part of his shaiva study he may do so as is.

To my knowledge the published versions of the पैप्पलाद text and the नीलरुद्र lack svara-s. An Orissan recitation prepared during a veda-पारायण organized by the कुम्भघोणं (aka काङ्चि) मठ of the शङ्कर tradition also lacked svara-s and had peculiarities of the eastern dialect. The published versions which we have seen also have certain inaccuracies. Hence, as part of our preservation of atharvavedic material we prepared a version with accents and its supporting recitation. The उदात्त-s are marked in red and the rest of the svara-s use the usual AV notation deployed in the transmission of the vulgate AV text (not the Kashmirian पैप्पलाद). At some later time we might attempt to discuss some interesting aspect of vedic (and early IE) accents that come up as result of the study of this text.

The oral recitation(MP3)\
[oral नीलरुद्र](https://app.box.com/s/jbjewnvmuzzpsvoleyfb)\
The text with accents (PDF)\
[नीलरुद्र](https://manasataramgini.files.wordpress.com/2009/03/nilarudra.pdf)

[OM\
अपश्यं त्वावरोहन्तं दिवितः पृथिवीम् अव ।\
अपश्यम् अस्यन्तं रुद्रं नीलग्रीवं शिखण्डिनं ॥ १\
दिव उग्रो ऽवारुक्षः प्रत्यष्ठा भूम्याम् अधि ।\
जनासः पश्यतेमं नीलग्रीवं विलोहितं ॥ २\
एष ऐत्य् अवीरहा रुद्रो जलाष भेषजी ।\
वि ते क्षेपम् अनीनशद् वातीकारो व्य् एतु ते ॥ ३\
नमस्ते भव भामाय नमस्ते भव मन्यवे ।\
नमस्ते अस्तु बाहुभ्याम् उतो त इषवे नमः ॥ ४\
याम् इषुं गिरिशन्त हस्ते बिभर्श्य् अस्तवे ।\
शिवाम् गिरिश्रिताम् कृणु मा हिम्सीः पुरुषान् मम ॥ ५\
शिवेन वचसा त्वा गिरिशाछा वदामसि ।\
यथा नस् सर्वम् इज् जगद् अयक्ष्मं सुमनो असत् ॥ ६\
या त इषुश् शिवतमा शिवं बभूव ते धनुः ।\
शिवा शरव्या या तव तया नो मृळ जीवसे ॥ ७\
या ते रुद्र शिवा तनूर् अघोरापापकाशिनी ।\
तया नस् तन्वा शन्तमया गिरिशन्ताभि चाकशः ॥ ८\
नमो ऽस्तु नीलशिखण्डाय सहस्राक्षाय वाजिने ।\
अथो ये अस्य सत्वानस् तेभ्यो ।अहम् अकरं नमः ॥ ९\
नमांसि त आयुधायानातताय धृष्णवे ।\
उभाभ्याम् अकरं नमो बाहुभ्यां तव धन्वने ॥ १०\
प्रमुञ्च धन्वनः पर्य् उभयोर् आर्त्न्योर् ज्यां ।\
याश् cअ ते हस्त इषवः परा ता भगवो वपः ॥ ११\
अवतत्य धनुस् त्वं सहस्राक्ष शतेषुधे ।\
निशीर्य शल्यानां मुखा शिवो नश् शंभुर् आ चर ॥ १२\
असौ यस् ताम्रो अरुण उत बभ्रुर् विलोहितः ।\
ये चेमे अभितो रुद्रा दिक्षु श्रितास् सहस्रशो ऽवैषां हेळ ईमहे ॥ १३\
अदर्शं त्वावरोहन्तं नीलग्रीवं विलोहितं ।\
उत त्वा गोपा अदृशन्न् उत त्वोदहार्यः ।\
उतो त्वा विश्वा भूतानि तस्मै दृष्टाय ते नमः ॥ १४\
विज्यं धनुश् शिखण्डिनो विशल्यो बाणवान् उत ।\
अनेशन्न् अस्येषवश् शिवो अस्य निषङ्गथिः ॥ १५\
परि ते धन्वनो हेतिर् अस्मान् वृणक्तु विश्वतः ।\
अथो य इषुधिस् तवारे अस्मिन् नि धेहि तं ॥ १६\
या ते हेतिर् मीढुष्टम हस्ते बभूव ते धनुः ।\
तया त्वं विश्वतो अस्मान् अयक्ष्मया परि भुज ॥ १७]{style="color:#99cc00;"}


