
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [saura miscellany](https://manasataramgini.wordpress.com/2009/02/03/saura-miscellany/){rel="bookmark"} {#saura-miscellany .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 3, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/02/03/saura-miscellany/ "9:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3261/3255320824_c82933675c_b.jpg){width="75%"}
```{=latex}
\end{center}
```



The famous poet, मयूर of हर्षवर्धन's court, whose शृङ्गार work, the मयूराष्टक, we had earlier alluded is supposed to have composed the सूर्यशतक in the aftermath of the events following the former composition.

सूर्यशतक-1\
[जंभारातिइभ-कुम्भोद्भवं इव दधतः सान्द्र-सिन्दूर-रेणुं\
रक्ताः सिक्ता इवऔघैर् उदय-गिरि-तटी-धातु-धाराद्रवस्य\
आयान्त्या तुल्यकालं कमल-वन-रुछेवा ।अरुणा वो विभूत्यै\
भूयासुर् भासयन्तो भुवनम् अभिनवा भानवो भानवीयाः ॥]{style="color:#99cc00;"}

The imagery of the first simile with which the work opens puzzles me somewhat: I take the सिन्धूर reNu=scarlet dust i.e. an allusion to the dust motes in the rising sunbeams, which are likely to appear scarlet. Now they are described as sAndra, which in this context I mean to take wet. They are described as being on the frontal domes of the elephant of indra. So I took this to mean the mud bath that elephants take by spraying the mud from their trunks on to their heads. So it might seem that मयूर sees the spray of mud by the elephant onto its frontal domes as the motes in the sunbeams. I am not entirely sure of this simile though. The second simile is of streams of sprinkled blood flowing down the slopes of udaya-giri-like molten metal streams. The third simile compares the crimson luster of the rising sun with a water-lily conglomeration's glow, and states that the rays are simultaneous with the rising. While I ignored my lessons in संस्कृत, especially pertaining to काव्य, a word called उत्प्रेक्षा filtered in from somewhere. I guess this richly illustrates that उत्प्रेक्ष -- the synaesthetic tendencies of the poet.

सूर्यशतक-26\
[बिभ्राणः शक्तिम् आशु प्रशमित-बलवत्-तारकाउर्जित्यग्-उर्वीं\
कुर्वाणो लीलया ऽन्धः शिखिनम् अपि लसच्-चन्द्र-कान्तावभासं\
आदध्याद् अन्धकारे रतिम् अतिशयिनीम् आवहन् वीक्षणानां\
बालो लक्ष्मीम् अपारां अपर इव गुहो ऽहर्पतेर् आतपो वः ॥]{style="color:#99cc00;"}

This is a famous example of what the Hindus of yore called the श्लेष or paronomasia. The sentences are supposed to simultaneous refer to both sUrya and कुमार. We can work it out as below (given my कौमार predilection unlike my father's saura one, I give the कौमार form first):\
कुमार: Bearing a weighty shakti that instantly suppresses the might of the powerful तारक /\
sUrya: Bearing the strong energy that instantly suppresses the shine of the stars;\
कुमार: sportingly riding atop a peacock which verily flashes the moon-shaped \[tail spots]/\
sUrya:Effortlessly drowning the fire['s illumination] and verily the flashing glow of he moon;\
कुमार: bears immense pleasure to the eyes of the andhaka's enemy/\
sUrya: bears immense pleasure to the eyes given to darkness;\
The young rays of the sun, like a second guha, conveys unlimited prosperity/\
The young guha like a second sun's rays conveys unlimited prosperity.

The sUrya shataka is one of those saura works from the classical Indian saura tradition, the evolution of whose तान्त्रिक forms we shall consider in due course. A rough outline of the evolutionary scenario of the saura mata is presented here.

[\
]{style="color:#99cc00;"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3483/3249531830_5ff2d67da8.jpg){width="75%"}
```{=latex}
\end{center}
```



A picture from my parents' recent performance of a तान्त्रिक saura साधन.


