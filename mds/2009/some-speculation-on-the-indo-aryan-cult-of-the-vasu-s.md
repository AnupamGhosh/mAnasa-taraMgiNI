
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some speculation on the Indo-Aryan cult of the vasu-s](https://manasataramgini.wordpress.com/2009/11/10/some-speculation-on-the-indo-aryan-cult-of-the-vasu-s/){rel="bookmark"} {#some-speculation-on-the-indo-aryan-cult-of-the-vasu-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 10, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/11/10/some-speculation-on-the-indo-aryan-cult-of-the-vasu-s/ "7:42 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Parts of the below material were inspired by extended collaborative discussions with श्री KR.\
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2558/4096942791_83e4de895d.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[ The winter solstice/year beginning in the last part of श्रविष्ठ (1300 BCE)]{style="color:#33cccc;"}

**The three great categories of gods in the veda and beyond**\
Though absent in the extant Iranian texts, the oldest Indo-Aryan texts recognize the presence of three major categories of deva-s --- the vasu-s, rudra-s and Aditya-s. This triad is mentioned on a number of occasions in the ऋग्वेद. However, the ऋग्वेद does not explicitly enumerate the members of any of these three categories deva-s and only mentions some of the members of each category by name. In the origin mythology presented in RV 10.72 there is an allusion to 8 Aditya-s, but there is hardly any evidence that this number was uniformly intended throughout the RV. In the शतरुद्रीय of the yajurveda there is an allusion to thousands of rudra-s (a concept preserved in the shaiva tantra-s, especially of the Urdhva-srotas belonging to a much later era). The vasu who is explicitly named in the RV is agni. Three Aditya-s, mitra, वरुण and aryaman are frequently mentioned, but the other candidates of this category are hazy (beyond अंश and दक्ष who are named but rarely if ever made any offerings). In the ब्राह्मण texts the number, if not the identity, of the constituent deities in each category appears to have been standardized. The gAyatrI is associated with the vasu-s as they are 8 in number and so is the number of syllables in a foot of this meter. The त्रिष्टुभ् is associated with the rudra-s as they are 11 in number and so is the number of syllables in a foot of this meter. The jagati is associated with the Aditya-s as they are 12 in number and so is the number of syllables in a foot of this meter. Since then, the number of deities in each category has remained conserved in Hindu tradition. The constituents of the vasu category were explicitly laid out first in the shatapatha ब्राह्मण as being the deities who composed the world namely agni, वायु, पृथिवि, sUrya, अन्तरिक्ष, dyaus, soma and नक्षत्र-s (SB 11.6.3.6). Subsequent texts provided conceptually similar lists, while trying to systematize to world-forming deities to a greater extant by adjusting the names to include entities such light and water, and the Pole star as a representative of the नक्षत्र category.

The gopatha ब्राह्मण of the atharvaveda states:\
[अष्टौ वसव एकादश रुद्रा द्वादशादित्या वाग् द्वात्रिंशी स्वरस् त्रयस्-त्रिंशस् त्रयस्त्रिंशद् देवाः ।]{style="color:#000080;"}\
8 are the vasu-s; 11 are the rudra-s; 12 are the Aditya-s; वाक् is the 32nd and the accent is the 33rd. Thus there are 33 gods.

Like in the veda, these three categories of deva-s are repeatedly mentioned in the epics:\
E.g. in महाभारत:\
[ इहादित्याश्-cha रुद्राश्-cha vasavash-cha महर्षिभिः |]{style="color:#000080;"}

The tradition recorded in the रामायण counts the 33 gods (an Indo-European number, also known in the shruti) thus: 8 vasu-s+11 rudra-s+12 Aditya-s+2 ashvin-s:\
[अदित्यां जज्ञिरे देवास् त्रयस्त्रिंशद् अरिंदम ।\
आदित्या वसवो रुद्रा अश्विनौ च परंतप ॥]{style="color:#000080;"}

It appears plausible that the early vedic seers too intended these three categories to contain a comparable set of deities as those encountered in these 3 groups in later Hindu tradition of the इतिहास and पुराण. Following the lead of Hindu tradition the major ऋग् vedic deities can be placed in these three categories thus:\
Among the 8 vasu-s were the deities agni, वायु, soma, dyaus, पृथि(vi) (along with 3 others)\
Among the 12 Aditya-s were mitra, वरुण, aryaman, indra, विष्णु, bhaga, savitar, पुषण्, त्वष्टर्, विवस्वान्, अंश and दक्ष.\
Among the 11 rudra-s were rudra and 10 others.\
The main factor in favor of accepting the potential ऋग्वेदिc antiquity of this scheme is that it neatly accommodates all the major old vedic who are invoked and worshiped in shrauta rituals. But there are some contra-indications too:

 1.  The ashvin-s: were they in any of these categories or out of it? From the traditional view on the count of 33 deva-s and their distinctness from these 3 classes of deities it appears that they were probably outside of these 3 categories even in the ऋग्वेदिc world view. However, it should be noted that the RV acknowledges them as the sons of rudra, thus placing them in the rudra-class.

 2.  What about the marut-s, one of the most import group of ऋग्वेदिc deities? In the ऋग्वेद they are occasionally called rudra-s. They are sons of rudra and hence called rudriya-s. Hence, at face value it seems to me that they were intended to be in the 11 rudra-s i.e. rudra and his sons. In support of this we have a hymn of shantanu and देवापि in the RV which states (RV 10.98.1):\
[आदित्यैर् वा यद् वसुभिर् मरुत्वान् स पर्जन्यं शन्तनवे वृषाय ।]{style="color:#000080;"}\
Here the Aditya and vasu गण-s are mentioned, but instead of the tradition rudra-s we have marut-s supporting the idea that they were included in the rudra category. However, there is one inconsistency that questions this scheme: the maruts are 7 in number or 7 times 7. Their names are provided in the yajur texts as batches of 7 (e.g. KYV-TS 4.6.5). This does not fit in with the 11 count of the rudra-s making it unclear if the RV ऋशि-s uniformly intended them to be in the 11 rudra-s. Yet, other than rudra we do not have other names of the rudra-s in the older texts summing up to 11 (we have 8 names like those used in the शूलगव sacrifice; the shatapatha ब्राह्मण adds कुमार as the 9th). Certainly by the epic period the marut-s had been dissociated from rudra and were placed in a category of their own.

 3.  Further, within the vedic tradition itself we observe a number of different enumerations in an attempt to include the new god prajApati. For example in SB 11.6.3.5 याज्ञ्वल्क्य lists the 33 deva-s as being 12 Aditya-s+11 rudra-s+ 8 vasu-s + indra + prajApati. This would suggest that there was clearly some variability in the exact composition of the categories in the vedic period.

**indra and the marut-s : वासव and vasu-s**\
While members of each of the three categories were retained as well-defined entities in the post-vedic tradition, most of them were reduced to abstractions as they lost the dimensionality and prominence they enjoyed in the shruti. But the vasu category enjoyed a certain kind of curious prominence in the महाभारत and in a surprisingly under-appreciated form in a major sectarian system of the dharma. It is this issue that we shall look into it in greater detail here. The ऋग्वेद provides several names for indra such as: indra, shakra, वृत्रहन्, maghavan, shatakratu and the like. All these names are repeatedly used in the RV and are also frequently used in the इतिहास-s establishing the continuity between them and the shruti. However, in the इतिहास-s we observe a new, prevalent name for indra, namely वासव.\
E.g. in the रामायण:\
[अथ वा रामरूपेण वासवः स्वयम् आगतः ।\
]{style="color:#99cc00;"} E.g. in the महाभारत:\
[भीमसेनो महाबाहुः सवज्र इव वासवः ।\
]{style="color:#99cc00;"} This name is missing in the RV and nearly absent in the whole shruti. This is peculiar given that it has the same or greater prevalence than the vedic epithets in the इतिहास-s. The word वासव clearly connects indra with the vasu category. Yet neither in the veda nor in the इतिहास-s is he explicitly placed inside the vasu category. Now, in the veda indra is frequently described as being accompanied by the marut-s (i.e. मरुत्वान्) and sometimes by विष्णु. Both these associations are preserved in the इतिहास-s. E.g.:\
In the रामायण:\
[विजित्य तरसा लोकान् मरुद्भिर् इव वासवः ।;\
उपोपविष्टं सचिवैर् मरुद्भिर् इव वासवम् ।;\
वासवं परिवार्येव मरुतां वाहिनी स्थिता ।;]{style="color:#000080;"}\
In the महाभारत:\
[अभिगुप्तो महाबाहुर् मरुद्भिर् इव वासवः ।;\
]{style="color:#99cc00;"} All the above use a simile of indra accompanied by the marut-s.\
In the रामायण:\
[यथा शत्रुषु शत्रुघ्नो विष्णुना सह वासवः ।;\
लक्ष्मणेन सह भ्रात्रा विष्णुना वासवं यथा ।;]{style="color:#000080;"}\
In the महाभारत:\
[रणाजिरे वीतभयौ विरेजतुः समानयानाविव विष्णुवासवौ ।;]{style="color:#000080;"}\
All these are examples of the survival of the vedic इन्द्राविष्णू simile in the इतिहास.

Though we do not find the word वासव in the RV,  in the ऋक्-s of the वसिष्ठ-s alone do we find a clear association of indra with the vasu-s:\
[शं न इन्द्रो वसुभिर् देवो अस्तु शम् आदित्येभिर् वरुणः सुशंसः ।]{style="color:#000080;"} (RV 7.35.6)\
[यस्मिन्-निन्द्रो वसुभिर् मादयाते तम् अश्याम देवयन्तो वो अद्य|]{style="color:#99cc00;"} (RV 7.47.2)\
[इन्द्रो वसुभिः परि पातु नो गयम् आदित्यैर् नो अदित्ः शर्म यछ्तु । (]{style="color:#000080;"}RV 10.66.3)

While, this link to the vasu-s minor in the veda, it is prevalent in the इतिहास-s, consistent with the proliferation of the new name वासव in the latter texts.\
E.g. in the रामायण:\
[अभ्यषिञ्चन् नर-व्याघ्रं प्रसन्नेन सुगन्धिना ।\
सलिलेन सहस्राक्षं वसवो वासवं यथा ॥]{style="color:#000080;"}\
Here it is said that rAma was anointed with scents even as the vasu-s lustrated indra with the pure waters.\
E.g. in the महाभारत:\
[त्वं हि भीष्म महाबाहो वसूनां वासवोपमः ।;]{style="color:#000080;"}\
Here भीष्म is compared to वासव amongst the vasu-s.\
[परिवार्य रणे द्रोणं वसवो वासवं यथा ।;]{style="color:#000080;"}\
Here the warriors are supposed to surround droNa in battle like the vasu-s surrounding indra.\
[नमस्-कुर्वन्ति छ सदा वसवो वासवं यथा ।;]{style="color:#000080;"}\
Here the vasu-s are said to worship indra their leader.\
Thus, the link limited to the वसिष्ठ-s in the veda has become common currency in the two epics. This link to the vasu-s and the innovation of the name वासव in the इतिहास-s is accompanied by some other innovations. In the ऋग्वेद indra is described as killing several demons or दास-s such as वृत्र, namuchi, shambara among many others. The first three of these names are retained in the इतिहास-s and indra's battle with them is a common simile. Yet, despite the extensive list of foes slain by indra two new ones emerge in the इतिहास-s, who are not encountered in the veda: jambha and पाक. All these point to the possible existence of a para-vedic mythology of the vasu-s and indra that in the veda was only recorded, in part, by the वसिष्ठ-s, who were relative new-comers to the core vedic fold with links to the Iranian west.

**The vasu-s, भीष्म and vAsudeva\
** The vasu-s take the center stage in the opening plot of the Mbh in the birth of one the main heroes of the epic --- the पितामह भीष्म. In this tale the vasu-s, though great gods, are cursed by a mere ऋषि, वसिष्ठ Apava, and hence forced to take birth as humans. But their punishment is eased by the celestial river goddess गञ्गा who agrees to be their mother and helps them avoid the indignity of passing through a human womb on their earthly sojourn. Furthermore, she quickly relieves 7 of the 8 vasu-s by drowning them in her current as soon as they are born as sons of shantanu who takes गङ्गा as his wife. The last of them who remains behind as भीष्म has the option of immortality on earth, and finally gives up his life after the Great War upon being granted permission by vAsudeva to do so. Now the reason for which वसिष्ठ Apava curses the vasu-s is presented twice in completely different terms: 1) They are supposed to have conspired to steal the cow of वसिष्ठ at the behest of one of their wives. They do this because the wives of one of the vasu-s wishes to give the cow and its calf to the king उशीनर. The one who actually steals the cow, dyaus (who is described as indra-like), is cursed with the long stay on earth in the form of भीष्म. Now this cow-theft motif has been duplicated from an older tale involving वसिष्ठ, i.e. that of the theft of his cow by विश्वामित्र. Another version of this motif exists in the form of the theft of jamadagni's cow by the haihaya-s. This does not concern us here, but could well be the oldest version of the motif being alluded to in the atharvaveda. 2) They are supposed to have been cursed by वसिष्ठ for merely flying above his head while he performed his संध्या rite. It is also said that each of the vasu-s gave 1/8 of their power to गङ्गा due to the curse and that these fragments constituted one whole who was भीष्म.

Now this core motif makes a reappearance in the bhArata and the हरिवंश in another most familiar myth of the Hindu-s --- the birth of कृष्ण vAsudeva. He is also the 8th son, the first 7 of his siblings being killed shortly after they were born. He lives long on earth endowed with near immortality except for his "Achilles heel", but unlike the sterile भीष्म he is super-virile. The 8 sons here are not called vasu-s but his own name is vAsudeva and his father is vasudeva indicating that the link to the vasu-s was not far away. Right from the Mbh, we hear of the idea of multiple claimants to the position of the vAsudeva. In fact the jaina-s make this concept systematic by presenting a vAsudeva and a prati-vAsudeva in each cycle. In the case of कृष्ण देवकीपुत्र, the counter-claimant for vAsudeva-hood is the पौण्ड्र vAsudeva who was slaughtered by the former in his invasion of पुण्ड्र. This history with the striking parallel in the भीष्म and vAsudeva stories suggests that a notion was current in the Indo-Aryan world of the time that the vasu-s or their son would be born on earth \[footnote 1]. This chief of the vasu-s/ combination of the vasu-s/ vAsudeva born on earth was supposed to be a great hero of the age, while the remaining seven of them were probably supposed to return quickly. Given the depiction of indra as वासव with the vasu-s and the specific allusion that the vasu born as भीष्म was "indra-like", it is conceivable that vasu born on earth or the god vAsudeva was supposed be endowed with prowess like their chief indra. Finally, the link to the वसिष्ठ-s seen in the भीष्म myth suggests that there might be some continuity between this and the vasu-indra link of the RV --- the वसिष्ठ-s could have been major players in the vasu-centric cult throughout its evolution.

**The astronomical foundations of the vasu incarnation/vAsudeva myths\
** Despite the years of insistence to the contrary (sometimes nonsensical) on part of several mainstream Western Indologists and some of their Japanese fellow travelers, it is clear that the language of Hindu myths sparkles with astronomical allusions of a distant age. Here again we see the concealed within these seemingly meaningless myths a number of allusions to precession, which at least partial explain the matter of the vasu-s, vAsudeva and his ultimate connection with विष्णु. Where are the vasu-s in the sky? The तैत्तिरीय ब्राह्मण tells us:\
[अष्टौ देव वसवः सोम्यासः । चतस्रो देवीर् अजराः श्रविष्ठाः ।\
ते यज्ञं पान्तु रजसः पुरस्तात् ।संवत्सरीणं अमृतग्ग् स्वस्ति ।\
यज्ञं नः पान्तु वस्वः पुरस्तात् । दक्षिणतो। अभियन्तु श्रविष्ठः ।\
पुण्यन् नक्षत्रं अभि विसंशाम । मा नो अरातिर् अघशग्ंसा ऽगन्न ॥]{style="color:#000080;"}

The AV नक्षत्र kalpa 4.6 states:\
[श्रवणे विष्णुर् उच्यते श्रविष्ठा वसुदेवत्या]{style="color:#000080;"}

This indicates that constellation of the vasu-s was श्रविष्ठ (Delphinus). The mantra also suggests that in the तैत्तिरीय tradition the constellation of श्रविष्ठ has 4 stars. In contrast, in the atharvavedic tradition (नक्षत्र kalpa 2.1) श्रविष्ठ is said to be made up of 5 stars, thus being equivalent to the modern Delphinus. Now, in the older vedic layer of the yajurvedic texts and the 19th काण्ड of the vulgate atharvaveda we hear of the नक्षत्र-s beginning with कृत्तिका. This was not merely a fashion of listing the नक्षत्र-s but the actual order from the spring equinox because the AV19.7 clearly states that the ayana happened in magha (the summer solstice). The later vedic texts are aware of the precession from magha --- for example, in the kaushitaki ब्राह्मण 19.2 it is stated the winter solstice is associated with the magha or the तिष्य new moons. While both are allowed, तिष्य is considered more current than magha suggesting a movement away from magha. This movement brought श्रविष्ठ to the winter solstice, which started marking the beginning of the Aryan year. As a consequence we find several references to श्रविष्ठ in different texts:

 1.  The मैत्रायनीय ब्राह्मण उपनिषद् states that the winter solstice was in श्रविष्ठ (MBU 6.14). It apparently refers to the middle of श्रविष्ठ.

 2.  The वेदाङ्ग ज्योतिष states that the winter solistice was in श्रविष्ठ, evidently in implies the beginning of श्रविष्ठ.

 3.  In the स्कन्दोपाख्यान in the vanaparvan of the Mbh we see the statement: [धनिष्ठादिस्तदा कालो ब्रह्मणा परिनिर्मितः ।]{style="color:#000080;"}\
i.e. brahmA initiated time with धनिष्ठ (=श्रविष्ठ).

 4.  In Mbh 14.44.2 we encounter:[श्रविष्ठादीनि ऋक्षाणि ऋतवः शिशिरादयः ।]{style="color:#000080;"}\
i.e. the constellation list beginning with श्रविष्ठ while the seasons with the frigid season.\
Thus, we have several late vedic or post-vedic references to the नक्षत्र-s beginning with श्रविष्ठ. This, together with earlier references to the कृत्तिका-magha position, indicates that there was a clear knowledge of precession of the winter solstice into श्रविष्ठ. We also note that there is evidence for knowledge of the passage of the solstice within श्रविष्ठ (shatapatha and MBU 6.14). We posit these points are central to rise of the vasu incarnation myth. We may reconstruct the whole scenario speculatively thus:\
In the borderlands closer to the Iranian zone there existed a system of indra as the lord of the vasus, from fairly early in the Indo-Iranian history. The early signs of this system are what we observe in वसिष्ठ's mantra-s \[Incidentally, it should be noted that the वसिष्ठ in RV 10.66.3 is called वसुकर्ण वासुक्र!]. Over time they developed into the system with वासव as indra the lord of the vasu-s \[Footnote 2]. This para-vedic system was injected into the Indo-Aryan mainstream after the core saMhitA and ब्राह्मण periods but before the epic period. Around this period \~1500 BCE the sun entered श्रविष्ठ, the constellation of the vasu-s. The followers of the vasu system saw this as an important portent possibly resulting in the belief that a vasu might incarnate on earth. I suspect that this belief arose originally as an astronomical allegory in the language of myth of the entry of the sun into श्रविष्ठ, and as is usual the myth was interpreted by the more literal-thinking folks as implying a physical incarnation. As the sun coursed through the constellation over the next 300 years the myth was probably refined into a form that saw 7 of the vasu-s dying prematurely before the 8th would be born to live the life of a hero on earth. This expectation of the 8th vasu of the chief vasu was the central element behind both भीष्म and कृष्ण vAsudeva. This 8th vasu was in all likelihood considered the chief vasu or vAsudeva who was endowed with the qualities of वासव. This belief was responsible for multiple contenders for the vAsudeva of the age. As the sun moved to the last quarter of श्रविष्ठ around 1300-1200 BCE, it was tending towards the constellation of श्रवण, which belongs to विष्णु. This probably led to a parallel expectation of the incarnation of विष्णु. We posit that this belief arose among the सात्त्वत-s who had a vaiShNava streak in them. It was probably mixed up with the wider vasu/vAsudeva incarnation. Thus, the finally accepted vAsudeva, कृष्ण, acquired his link to विष्णु. The time of these events would be the window between 1500-1200 BCE, which is interestingly close to what is obtained as the age of the bhArata by the famous पौराणिc testimony of परीक्षित्-s birth. It is supposed to have happened 1050 or 1015 years before the rule of the nanda-s of Magadha who apparently ruled around 300 BCE.

**Epilogue**\
One may take this speculation further --- the name vasudeva, of his father, is merely a caulk to fill in an explanation for the name vAsudeva in the tradition of grammar. In light of this, one might note that as late as the महाभाष्य of पतञ्जलि there is the idea of vAsudeva as a deity who is distinct from the kShatriya. This suggests that there was possibly still some distinction between the concept of the divine vAsudeva and the human कृष्ण who had been accepted as vAsudeva. One may also speculate that the 4 part of श्रविष्ठ alluded to the yajurvedic system (chatasro देवी) inspired the 4 व्यूह-s in the cult of vAsudeva. Another more distant point is also worth noting here --- the early patron of पाञ्चरात्र is said to be the pUru king vasu uparichara who performed ritual as per the सात्त्वत vidhi \[Footnote 3]. He is named a vasu suggesting that he too could have been in the circle of contenders for the vasu incarnation. He was possibly seen as a precursor for the lord of the vasu-s --- the vAsudeva --- of whom he had a special vision of by his पाञ्चरात्रिc rituals.

The concealment of astronomical matter in "pure myth" or in history infused with myth is not uncommon. The great patriot of the Hindu nation, Tilak, and De Santillana and von Dechend more recently have labored in unearthing much in this regard. The latter authors propose that the Ragnarok with death of an old series of gods and the birth of a new one is has been inspired by a precessional event. We accept this core hypothesis and suggest a similar scenario here, wherein the notion of the coming of the vAsudeva might have been inspired by a mythic description of precession. Of course it is entirely possible that the notion remained in the realm of myth and was simply superimposed on the history of the hero of the age, कृष्ण देवकीपुत्र. Finally I must note that de Santillana and von Dechend mention something in passing that I have not been able to confirm but if true may be of interest in this context. The Greek original he cites goes thus:\
archaiotatos kai tOn oktON tOn prOtON legoumenOn थेऒन्.\
That Pan was one of the great 8 gods who existed before the rest. They try to connect this with the Eshmun of the Egyptians, the eighth god Sirius and the mysterious cry of the "death of the great god Pan heard on the voyage to Italy".

  - -----------------------------------------------------------------------

Footnote 1: KR goes as far as to state that भीष्म himself was a 3rd claimant for the vAsudeva post who probably lived a before the prime claimant देवकीपुत्र and his rival पौण्ड्रक. He lost out as कृष्ण was accepted as the vAsudeva and only a more negative version of the vasu-birth myth was retained for भीष्म.

Footnote 2: A similar concept was probably reused by ज़रथुष्ट्र in the formulation of the amesha spenta-s with ahura मज़्धा as their head.

Footnote 3: We should distinguish this old सात्त्वत text from surviving सात्त्वत tantra of the पाञ्चरात्रिक-s. The latter is a much later text of the classical तान्त्रिक age, though one can rule out that it contains some old survivals from the earlier era of पाञ्चरात्र.


