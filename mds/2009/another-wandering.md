
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Another wandering](https://manasataramgini.wordpress.com/2009/02/02/another-wandering/){rel="bookmark"} {#another-wandering .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 2, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/02/02/another-wandering/ "7:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We wandered on earth following the path of the heavenly stream,\
Towards the cemetery of किलिकिलारव, above which rose the heavenly archer.\
There the cry of the ape dvividha in the dark was still reverberating,\
Long after he had fallen to the club of the संकर्षण, his killer.

In our hands were a Damaru and a त्रिशूल,\
A कपाल hung in our sling and we were besmeared with ashes.\
We chanted the जराबोधीय and the secret raudra sAman-s,\
Praising the archer in the pursuit of brahmA

We tasted the soma streaming from our shiras,\
While reciting the old chant of कण्व the अञ्गिरस्.\
We walked past the periphery of the श्मशान,\
Feeling the presence of the sharva, bhava, इशान.

There we heard the sonorous music in अपभ्रंश\
Lo! We beheld a sammelana of प्रज्ञा-s of pretty smiles.\
In the joy of union they were singing vajra-गीति-s.\
In their midst were डाक-s observing the rite of बुद्धकपाल.


