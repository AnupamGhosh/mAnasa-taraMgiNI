
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [tryashra क्रीड](https://manasataramgini.wordpress.com/2009/09/28/tryashra-krida/){rel="bookmark"} {#tryashra-करड .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 28, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/09/28/tryashra-krida/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2465/3961148487_7b1edb46f7_o.png){width="75%"}
```{=latex}
\end{center}
```



