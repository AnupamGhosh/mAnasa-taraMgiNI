
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A नृसिंह for obtaining siddhi-s](https://manasataramgini.wordpress.com/2009/04/11/a-nrisimha-for-obtaining-siddhi-s/){rel="bookmark"} {#a-नसह-for-obtaining-siddhi-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 11, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/04/11/a-nrisimha-for-obtaining-siddhi-s/ "7:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R1's father had been lead by a descendant of the famous medieval तान्त्रिक लक्ष्मीधर to an extremely remote shrine of नृसिंह amongst ruins in the कलिङ्ग country. This shrine is in a place known today as Sonepur and is not far away from a temple of the most exalted योगिनी-जाल-shambara. R1's father and the descendant of the said tantrika stated that they attained siddhi of several mantra-s from the पाञ्चरात्र tantra-s when they performed brief japa before this लक्ष्मी-नृसिंह. I finally found a picture, albeit pretty hazy, of the same image.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3568/3431450017_48bb6b8b85_o.jpg){width="75%"}
```{=latex}
\end{center}
```




