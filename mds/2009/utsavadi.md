
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [उत्सवादि](https://manasataramgini.wordpress.com/2009/09/27/utsavadi/){rel="bookmark"} {#उतसवद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 27, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/09/27/utsavadi/ "6:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were engaged in a conversation with ekanetra on variety of topics ranging from the preceptorial genealogies of the medieval advaita वेदान्तिन्स् to the mudgara-swinging bludgeon-men of पराक्रम-बाहु in the "सिंहल-damila महायुद्ध". When, in the middle, ST chimed in that she was thinking of a paradox in traditional observances and wanted to know their textual bases in Hindu tradition. She alluded to events many solar cycles ago when we were still in our youth and filled with ignorance but full of the neophyte's ardor. In those past days, at this time of the year, we both used to cut class to pursue our respective extra-curricular pleasures even as the rest of the pashu-s labored with their pashu-शास्त्र-s. While I was seeking my mukti from the pashu-pAsha in the study of the deep rahasya-s that I hardly fully understood then, our friend ST was seeking mokSha from the शृङ्खल-bandha in the rAsa-क्रीड. She said: "I was observing a solemn नवरात्रि, but all of a sudden I was reminded of our days in the city of our youth and the क्रीड that used to be such great fun with the rest of the "gang". But suddenly it struck me how in my childhood the नवरात्रि was a solemn affair at home. I wondered if the profligate रासगोष्ठी was ever allowed in the शास्त्र-s." I did not share ST's closeness to the "gang" for after all like the advaitin we did not equate अहं with the sharIra that was the "gang". So I could not appreciate her sentiment in this regard, nevertheless the conversation veered to the tradition of the old Hindu madana-mahotsava-s. There is the famous [कामोत्सव](https://manasataramgini.wordpress.com/2004/09/01/the-festival-of-kama/) that is observed on chaitra shukla पक्ष, द्वादशी (chaturdashi according to some) and this is widely referred to in संस्कृत literature. But I recall that the परमार राजन् bhoja-deva mentions several others in his tome on शृङ्गार. But not having the original text of the राजन् before me I had to rely on my memory and certain other sources for this discussion. Hence, I must mention that many of them are missed in this discussion (but I might update them once if I feel inclined to look up the text again).

In the शारदि set of ratyutsava-s भोजराज mentions:

 1.  navapatrika; 2)भिसखादिक; 3) कन्दुकक्रीड; 4) यक्ष रात्रि; 5) कौमुदी जागर; 6)अभ्युष खादिक / इक्षु-भक्षिक; 7) चन्द्रिका-lalana; 8) haMsa-लीलावलोकन; 9) sarit-pulina-keli; 10) bali क्रीड.

Of these यक्ष रात्री is known to be over-night gaming festival that coincides with दीपावली/दीपोत्सव. Hence, if a festival corresponds to the नवरात्री it should be before it. In the दशकुमार charita of दण्डिन्, चन्द्रसेना states the following to mitragupta and कोशदास:\
[प्राप्तैवेयं भर्त्र्^ई दारिक कन्दुकावती कन्दुक क्रीडितेन देवीं विन्ध्यवासिनीम् आराधयितुम् । अनिशिद्ध दर्शना चेयम् अस्मिन् कन्दुकोत्सवे । सफलम् अस्तु युष्मc चक्षुः आगच्छतं द्रष्टुं ।]{style="color:#99cc00;"}*

  -  My king's daughter कन्दुकावती has come to worship the goddess विन्ध्यवासिनी. It is not forbidden to look at her during the ball-festival. May the object of you two's vision be attained.

Here we see a juxtaposition of the kandukotsva with the worship of विन्ध्यवासिनी, which might suggest that it coincided with the नवरात्रि. As per the descriptions, navapatrika happens when the meadows are lush with grass following the monsoons. On those green meadows the participants engage in drinking and काम keli. In the भिसखादिक the guys hold lotus stalks in their teeth while the girls bite them off. Going by the time allotted to the season of autumnal festivals these two were possibly slightly earlier in the year and therefore before नवरात्रि in my opinion. The remaining utsava-s that follow दीपावली, include कौमुदी जागर, which is also mentioned by वात्स्यायन as the कौमुदी mahotsava (also the name of a book of fiction by an early authoress विज्जका), a time suitable for initiating कामबन्ध-s. It survives to this date in parts of India where flavored milk is drunk along with revelry on terraces. The अभ्युष and इक्षु eating festivals appear to correspond to what in the drAviDa country is observed as the कार्त्तिक festival. The अब्युष (puffed-rice balls) is still made in the drAviDa country though the इक्षु is eaten on other occasions such as pongal. The festival चन्द्रिका-lalana was an overnight revelry when the moon was at its highest in the autumns. The haMsa-लीलावलोकन involved watching migratory water birds return to their nesting grounds in जम्बुद्वीप, whereas the sarit-pulina-keli was revelry on the sand banks of streams.\
In winter there were some more utsava-s like the blindman's buff festival and the peculiar shuka-सारिकालाप-abhyasana. In this latter festivity young men and women tried to teach parrots and mynahs to speak.

According to bhojadeva there was another remarkable festival in spring that might have overlapped with the spring नवरात्रि or according to others with the वट festival. The polymath हेमाद्रि also notes this festival but seems to assign a different date. This was called पञ्चालानुयान or भूत-मातृका or bhairavotsava. Here an image of भूतमातृका and in some cases bhairava was installed and worshipped. The image was then carried forth with young women following, decked in fine costumes. They engaged in amorous dances letting their garments slip, wearing of ghost-masks and श्मशानाभिनय (enactment of phantom apparitions from the cemetery). In spring bhojadeva mentions other utsava-s like:\
उदकक्ष्वेडिका involved young men and women sprinkling each other with colored water from pistons. This seems to have been incorporated in to होलिका in some parts of the land whereas in the महाराट्ट country the old उदकक्ष्वेडिका used to survive in my days as a vulgar form on a पञ्चमि day festival in spring.\
chutalatika involved young women approaching their male counterparts and striking one of them with a mango twig to chose them as their jAra.\
ashoka-dohada was a bacchanalic revelry for young women in which they ornamented their feet and embraced and kicked an ashoka tree. Then they spat chewed betel leaves and alcoholic beverages on the tree.\
Finally there was also some ratyotsava associated with the great indradhvaja festival according to certain traditions.


