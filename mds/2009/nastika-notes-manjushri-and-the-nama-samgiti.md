
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [nAstika notes: मञ्जुश्री and the nAma-संगीति](https://manasataramgini.wordpress.com/2009/08/07/nastika-notes-manjushri-and-the-nama-samgiti/){rel="bookmark"} {#nastika-notes-मञजशर-and-the-nama-सगत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 7, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/08/07/nastika-notes-manjushri-and-the-nama-samgiti/ "6:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The meteoric rise of मञ्जुश्री in the bauddha realm is not immediately apparently in his hazy beginnings. He appears to have been the focus of the syncretic absorption of several deities which can be seen in course of the evolution of the bauddha literature \[footnote 1]:

 1.  In the earliest bauddha lore he appears as a gandharva पञ्चशिख मञ्जुघोष who provides the celestial music during the visit of indra to the तथागत.

 2.  By the time of the mahAyAna texts he appears as a bodhisattva receiving nAstika teachings. Soon he rose to being a bodhisattva of level 10 like avalokiteshvara and वज्रपाणि who were also to rise greatly in stature in the future. But he does not represent the state of the complete buddha. It was in this phase that he absorbed elements of the deity बृहस्पति or वाचस्पति. However, in this stage the seeds of his future rise are seen in a teaching made by the तथागत to the nAstika ब्राह्मण शारिपुत्र, where he states that मञ्जुश्री is a progenitor of the bodhisattva-s and their guide.

 3.  In the major क्रिया tantra, the Arya-मञ्जुश्रीय मूल kalpa he becomes an important deity after absorbing कुमार as has been described earlier. In this text it is mentioned that as कुमार he will perform the deeds of the buddha in the world.

 4.  In the yoga tantra-s (the मञ्जुश्री ज्ञन-sattvasya परमार्थ nAma संगीति or the मञ्जुश्री nAma संगीति) and योगिनी tantra-s (e.g. कालचक्र तन्त्रं) he rises to be the Adibuddha and he is in the heart of the ShaD-buddha-s.In the yoga tantra phase he starts acquiring elements of the bhairava of the shaiva tantra-s and this absorption was critical for his rise as a major deity. This absorption to a degree enabled him to break free from the limitations of the bodhisattva mold which his earlier evolutionary history had placed him in. Now by imitating the supreme Astika देवता-s he could himself be presented as a supreme देवता by acquiring their attributes for free.

We now consider some aspects of the MNS to appreciate the universal descriptions of मञ्जुश्री:\
[*अवैवर्तिको ह्य् अनागामी खड्गः प्रत्येक-नायकः ।\
नाना-निर्याण-निर्यातो महाभूतैक-कारणः ॥ ५१*]{style="color:#0000ff;"}

Some of these names are clearly distinctive features of nAstika tradition, e.g. the rhino (खड्ग), which is considered a chief pratyekadbuddha by virtue of its solitary peregrination, named in this shloka. The term महाभूतैक-कारणः i.e. the single cause amongst the primal elements to me appears to be rather incongruous, if interpreted in its natural sense, with the basic bauddha principle of शून्यवाद. We indeed note that the nAstika आचार्य नरेन्द्रकीर्ति glosses the term to be "the pure thought of enlightenment (i.e. bodhi) amidst the 4 भूत-s" \[as translated from the surviving Tibetan translation of the Sanskrit original by the American scholar Wyman]. So rather than seeing मन्जु\~श्री as the कारण he actually see him as the state of bodhi in which one realizes the शून्यता of the महाभूत-s. This does appear like a contrived explanation for a term that would more naturally exist in a praise of a deity under the Astika framework.

The following names again are directly drawn from the brahminical substratum from which the nAstika-s drew their intellectuals:\
[*ब्रह्मविद् ब्राह्मणो ब्रह्मा ब्रह्मनिर्वाणम् आप्तवान् ।\
मुक्तिर् मोक्षो विमोक्षाङ्गो विमुक्तिः शान्तता शिवः ॥ ९५*]{style="color:#0000ff;"}*

  -  The word brahmavid is artificially explained by the nAstika-s as meaning knowing brahmA the deity.

The absorption of shiva:\
[*अमोघपाशो विजयी वज्रपाशो महाग्रहः ।\
वज्राङ्कुशो महापाशो वज्रभैरव भीकरः ॥ ६६\
क्रोधराट् षण्मुखो भीमः षड्नेत्रः षड्भुजो बली ।\
दंष्ट्राकराल कङ्कालो हलाहल शताननः ॥ ६७\
यमान्तको विघ्नराजो वज्रवेगो भयंकरः ।\
विघुष्टवज्रो हृद्वज्रो मायावज्रो महोदरः ॥ ६८\
कुलिशेशो वज्रयोनिर् वज्रमण्डो नभोपमः ।\
अचलैकजटाटोपो गजचर्मपटार्द्रधृक् ॥ ६९*]{style="color:#0000ff;"}

In these series we notice the name षण्मुख which is an inheritance from his कौमार syncreticism of the earlier stage. The names vajrabhairava (the "vajrified" bhairava), हलाहल, यमान्तक, विघ्नराज, गजचर्मपटार्द्रधृक् are all indicative of his absorption of shiva's attributes. The nAstika आचार्य चन्द्रभद्रकीर्ति explains हलाहल as the poison that makes the neck blue, making it pretty obvious what prototype he has in mind. Some of these absorptions of shiva, seen first in MNS, emerge as important emanations of मञ्जुश्री who are prominent in their own right as देवता-s of important tantric rituals and texts in the subsequent layers of nAstika productivity. The main among these is vajrabhairava also known as यमान्तक who is at the center of several साधन-s in later texts. The epithet describing the deity as holding a freshly skinned elephant has no mythological foundation in the earlier bauddha lore and is thus clearly a transfer of the image of shiva as the slayer of गजासुर.

At the same time we also see an absorption of विष्णु underway:

[*समन्तदर्शी प्रामोद्यस् तेजोमाली सुदर्शनः ।\
श्रीवत्सः सुप्रभो दीप्तिर् भा भासुरकर-द्युतिः ॥ १०२*]{style="color:#0000ff;"}

The ताथगत स्मृति explains that these names describe मञ्जुश्री as the unconquerable preserver who holds the blazing chakra in his hand and has the श्रीवत्स in the heart. Thus the identification with विष्णु is clear though he avoids mentioning विष्णु by name.\
The absorption of the famous features of विष्णु are also apparent in:

  - [त्रैलोक्यैक-क्रम-गतिर् व्योम-पर्यन्त-विक्रमः ।१५३अब्\
]{style="color:#99cc00;"}* One who covers the worlds in a stride and one who covers the sky with a stride.

In this phase of their evolution, the nAstika-s were still trying to accommodate and identify their innovations with the Astika देवता-s rather than depicting the latter as being crushed by the former:\
[*देवातिदेवो देवेन्द्रः सुरेन्द्रो दानवाधिपः ।\
अमरेन्द्रः सुरगुरुः प्रमथः प्रमथेश्वरः ॥ १४८*]{style="color:#0000ff;"}

In explaining this shloka the famed nAstika मन्त्रवादिन् and grammarian chandragomin states that देवातिदेव is विष्णु, the दानव lord is vemachitra and that pramathesvara is महादेव. The other epithets are pretty obvious in identifying him with indra and बृहस्पति. So nAstika tradition was fully alive to this identification with Astika देवता-s.

While the text repeatedly tries to present मञ्जुश्री as the compassionate Adi-buddha striving against the negativities in the form of the mAra-s, we do get a hint of him as a fierce warrior:\
[*वज्र-बाणायुधधरो वज्र-खड्गो निकृन्तनः ।\
विश्व-वज्रधरो वज्री एकवज्री रणं-जहः ॥ ७२*]{style="color:#0000ff;"}*

  -  He is described as holding the adamantine arrow and slashing sword with which he defeats enemy warriors on the battle field as an ekavajrin. The early nAstika commentators like स्मृति explain this shloka metaphorically as showing मञ्जुश्री battling the defilements coming in the way of the bauddha praxis. But it was the more literal meaning of this imagery that was to inspire one of the greatest warriors in the future \[Footnote 2].

Finally it should be noted that the MNS is the first major nAstika contribution in a fairly old Hindu tradition -- that of the सहस्रनाम-s. The earliest of these the 1008 names of विष्णु and shiva occur in the bhArata itself. In course of the evolution of the तान्त्रिक mantra-मार्ग such names started acquiring significance as a notable component of the उपासन of devata-s. It is quite likely the nAstika-s were acutely aware of the need to create similar liturgies like the सहस्रनाम-s they used in their पूर्वाश्रम as Astika-s. Hence in creating the MNS it is not surprising that they used some elements from the सहस्रनाम-s of Astika devata-s. They also have a long उत्तरभाग to the MNS which is like phalashruti of the सहस्रनाम-s. It is also interesting that phalashruti provide a long list of Astika देवता-s who will protect the nAstika if he performs the नामसंगीति. These include the invocation of प्रत्यङ्गिरा amongst others to guard the votary of मञ्जुश्री:\
[सर्व-विघ्न-विनायक-मारारि-महाप्रत्यङ्गिरा-महापराजिता स-रात्रिं-दिवं प्रतिक्षणं सर्वेर्यापथेषु रक्षावरण-गुप्तिं करिष्यन्ति ।]{style="color:#0000ff;"}

similarly\
[ब्रह्मेन्द्रोपेन्द्र-रुद्र-नारायण-सनत्कुमार-महेश्वर-कार्तिकेय-महाकाल-नन्दिकेश्वर-यम-वरुण-कुबेर-हारीती-दश-दिग्-लोकपालाश् च ]{style="color:#0000ff;"}etc...

\[Footnote 1] Also see:\
[nAstika notes-2](https://manasataramgini.wordpress.com/2009/02/16/nastika-notes-2/)\
[Some points regarding the Rewa stone inscription of मलयसिंह](https://manasataramgini.wordpress.com/2008/08/09/some-points-regarding-the-rewa-stone-inscription-of-malayasimha/)\
[The महाकौमर mantras in आस्तीक and nआस्तीक parlance](https://manasataramgini.wordpress.com/2006/09/06/the-mahakaumara-mantras-in-astika-and-nastika-parlance/)

\[Footnote 2]. In 1583 CE Nurhachi, a Jurchen chief, who was a descendant of the Jurchen-Mongol warlord Moengke Timur, in his early twenties, had a vision while reading the MNS that he was an incarnation of मञ्जुश्री as the fierce warrior. His father and grandfather had just been killed in attack by a Jurchen chief in the pay of the chIna-s. He apparently had nothing by a few weapons and armor left behind by his ancestors. But inspired by the idea of being मञ्जुश्री's incarnation he raised a force and led them to a spectacular string of victories in the steppes, unifying tribe after tribe. Finally in 1616 CE after 33 unbroken military successes he declared himself Khan with the title meaning "The Brilliant Khan Who Benefits All Peoples", reflective of his identity with मञ्जुश्री. His unified Jurchen tribes even acquired a new name with a folk etymology in which Manchu was equated with मञ्जुश्री. Then he led his troops to great victories against the chIna-s, the Koreans and other steppe tribes. He was finally killed by Christian mercenaries employed the chIna-s. But his son was to conquer the chIna-s and found the last great empire of the steppes that ruled chIna -- the Qing dynasty.


