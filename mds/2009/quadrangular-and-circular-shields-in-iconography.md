
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Quadrangular and circular shields in iconography](https://manasataramgini.wordpress.com/2009/03/06/quadrangular-and-circular-shields-in-iconography/){rel="bookmark"} {#quadrangular-and-circular-shields-in-iconography .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 6, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/03/06/quadrangular-and-circular-shields-in-iconography/ "6:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3386/3331859475_58e7200995_o.png){width="75%"}
```{=latex}
\end{center}
```



Pictures kindly provided by my friend VA are images from the Chola kingdom of the drAviDa country.\
From top:

 1.  महिषमर्दिनी(महालक्ष्मी); 2) कौशिकी(महासरस्वती); 3) महिषमर्दिनी(महालक्ष्मी); 4) महाकाली. It should be noted that this is one of the few rare depictions of the deity महासरस्वती the killer of शुंभ and निशुंभ. In the मार्कण्डेय पुराण महासरस्वती is aided by the मातृका goddess in this act. However, in most cases even the मातृका-s are iconographically combined with a generic महालक्ष्मी rather than the महासरस्वती whom they accompany in the above text.

Note the use of both quadrangular and circular shields in the Tamil country in Chola temples.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3355/3332747684_fbab5a862f_o.png){width="75%"}
```{=latex}
\end{center}
```



A modern print from the a fire-cracker box sold in the days of yore during दीपावली without any shield and an odd-looking शूल but no shield. Generally, one could find high-quality art on the cracker boxes which were again typically produced by artists from the drAviDa country. But the lack of the shield suggests loss of familiarity with real combat using swords.


