
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [योषणा](https://manasataramgini.wordpress.com/2009/03/18/yoshana/){rel="bookmark"} {#यषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 18, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/03/18/yoshana/ "6:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We came face to face with her, she who shone with the luster of a योगिनी. She cast an inescapable जाल on you, which was much like the पञ्जर of a powerful डाकिनी. She was verily a manifestation of the primeval cause of the most basic हर्ष. Her black hair framed her face verily like the sharan-निशा framing the bright soma in a female form. But unlike the cratered surface of the उपांशु her face was verily like a gleaming tusk of a mighty gaja with her black hair framing it like the blackness of the same ibha. Her grayish green eyes sparkled like that companion of beta Cygni that delights an observer of the sky. The tilaka on her forehead was like Antares illuminating the southern arc on a summer night. Her black eyebrows streaked across her marble-like face indeed like the dark lane across the galaxy Centaurus A. The sight of her face was like the mada of milk-effused sweetened soma in the ritual of the deva-s. Her sporting in the अर्णव of our manas was like the manifestation of the power of the mantra of asura वरुण seen by vipra गृत्समद. Her emergence from a bath was verily like the eye of mitra and वरुण rising over the great ocean; just as one daily beholds the daivi in that luminary, her presence was truly a sign of the amartya-s amongst मानुष-s -- an exemplary image in the canvas of genetic recombination, verily like a Karl Gauss.


