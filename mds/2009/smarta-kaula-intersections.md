
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [स्मार्त-kaula intersections](https://manasataramgini.wordpress.com/2009/07/13/smarta-kaula-intersections/){rel="bookmark"} {#समरत-kaula-intersections .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 13, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/07/13/smarta-kaula-intersections/ "6:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The उत्तरभाग of the ब्रह्माण्ड पुराण contains a large body of श्रीकुल तान्त्रिक material. Some तान्त्रिक-s mention that this section contains the dharma for the kaula practitioner. Other तान्त्रिक-s hold that the tame views presented here are still a sign of duality which a genuine practitioner of कुलाचर might transcend. Clearly, the emphasis here is on activities for a स्मार्त ब्राह्मण or one from any of the other वर्ण-s who is affiliated with स्मार्त authority, but wishes to perform साधन-s and उपासन-s prescribed in the kaula शास्त्र-s. This is thus the बृहस्पति dharma as adapted for a स्मार्त practicing the तान्त्रिक krama-s of the said शास्त्र-s. This should not be confused with the activities of a साधक who \*is* a kaula and considers other शास्त्र-s as pashu शास्त्र-s. For example regarding liquors we encounter the following injunctions:

[*पैष्टिकं तालजं कैरं माधूकं गुडसंभवम् ।\
क्रमान्-न्यूनतरं पापं तदर्द्धार्द्धार्द्धतस्तथा ॥\
क्षत्रियादि त्रिवर्णानाम् आसवं पेयम् उच्यते ।\
स्त्रीणाम् अपि तृतीयादि पेयं स्याद् ब्राह्मणीं विना ॥*]{style="color:#0000ff;"}

The liquors are generally classified as a beer, toddy, coconut toddy, mead and sugar cane liquor. A ब्राह्मण male is prohibited from drinking these. A woman from the remaining three वर्ण-s might drink liquors except beer and toddy. Here again a ब्राह्मण woman is proscribed from drinking any liquor.

The बृहस्पति dharma recognizes that in kaula rituals liquor is used. But here again it is rather clear regarding a स्मार्त ब्राह्मण being forbidden from using it:\
[*लक्ष्मीः सरस्वती गौरी चण्डिका त्रिपुरांबिका ।\
भैरवो भैरवी काली महाशास्त्री च मातरः ॥\
अन्याश्च शक्तयस् तासां पूजने मधु शस्यते ।\
ब्राह्मणस्तु विना तेन यजेद् वेदाङ्ग पारगः ॥*]{style="color:#0000ff;"}

The shakti-s mentioned include त्रिपुरांबिका, the bhairava-भैरवी यामल and काली among others, covering the श्रीकुल, कालिकुल and generically the भैर्वाचर derived from the bhairavatantra-s.

The same बृहस्पति स्मृति also recognizes the fact that in the kula ritual maithuna needs to be observed. In this context it gives two shloka mantra-s. The first of these to विष्णु and लक्ष्मी can be used by one even without शास्त्र-ज्ञान so that his acts of maithuna become mantra-karman-s:\
[*सर्वात्मको वासुदेवः पुरुषस्तु पुरातनः ।\
इयं हि मूलप्रकृतिर् लक्ष्मीः सर्व-जगत्-प्रसूः ।\
पञ्cआपञ्cआत्म-तृप्त्यर्थं मथनं क्रियतेतराम् ।*|]{style="color:#0000ff;"}

A person well-educated in the शास्त्र-s might observe maithuna as a part of kula worship; however, only with the स्वस्त्री and not on prohibited days using the second mantra:\
[*इयम् अंबा जगद्-धात्री पुरुषो ऽयं सदाशिवः ।\
पञ्cअविंशति तत्त्वानां प्रीतये मथ्यते 'धुना ॥*]{style="color:#0000ff;"}

The possibility of animal sacrifice and meat consumption in the context of भैरवाचर is recognized by similarly providing mantra-s to make them permissible acts. But here again meat is proscribed for a ब्राह्मण except in life and death circumstances. But the slaughter of the pashu for the shAkta ritual might be done by the other वर्ण-s affiliated with स्मार्त tradition using the shloka mantra if he wishes to be free of pApa:\
[*शिवोद्भवम् इदम् पिण्डमत्यथ शिवतां गतम् ।\
उद्बुध्यस्व पशो त्वं हि नाशिवः सञ्चिवो ह्यसि ॥\
ईशः सर्व-जगत्-कर्ता प्रभवः प्रलयस् तथा ।\
यतो विश्वाधिको रुद्रस्तेन रुद्रो ऽसि वै पशो ॥*]{style="color:#0000ff;"}

While retaining the proscriptions on ब्राह्मण-s while engaging the kula rituals, this बृहस्पति स्मृति is unorthodox in certain ways i.e. it makes attempts to accommodate Aryanizing tribals who build irrigation works, temples of shiva and विष्णु and set up अग्रहार-s. The स्मृति gives an example of such a meritorious tribal woman who was even given the दीक्ष of the शतरुद्रीय by a स्मार्त ब्राह्मण.\
[आं ह्रीं क्रों सङ्केतयोगिन्यै नमः ॥]{style="color:#0000ff;"}


