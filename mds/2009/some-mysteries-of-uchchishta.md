
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some mysteries of उच्छिष्ट](https://manasataramgini.wordpress.com/2009/01/14/some-mysteries-of-uchchishta/){rel="bookmark"} {#some-mysteries-of-उचछषट .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 14, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/14/some-mysteries-of-uchchishta/ "2:23 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In extant atharvavedic shrauta practice the उच्छिष्ट सूक्त (AV-vulgate 11.7; AV-P 16.82) is not deployed. However, सायण correctly understands its deployment in the old AV-based shrauta ritual. He cites the तैत्तिरीय ब्राह्मण 1.1.9 and states that this is the उच्छिष्ट of the offering to the goddess aditi made of rice. In the TB its stated that the उच्छिष्ट was assigned to aditi; earlier in the AV (AV-vulgate 11.3) there is a long recitation of यजुष्-s and 4 embedded ऋक्स् that is used in the celebrated ritual of the बृहस्पति sava in offering of odana or the rice dish. In this recitation itself the उच्छिष्ट is mentioned (AV-vul 11.3.21) and described in terms that are expanded in this सूक्त. Thus in the old atharvavedic sava it is likely that the odana recitation mainly of यजुष्-s was first deployed while the odana was taken up and offered and its उच्छिष्ट was then used along with the eponymous सूक्त.\
Recently a discussion of a word nya, probably a hapax legomenon in the AV, which is found in this सूक्त came to my attention.\
*[दृढो दृंह स्थिरो न्यो ब्रह्म विश्व-दृशो]{style="color:#99cc00;"}* [\[AV-vul: सृजो] *[दश ।]{style="color:#99cc00;"}*\
*[नाभिम् इव सर्वतश् चक्रम् उच्छिष्टे देवता आहिताः]{style="color:#99cc00;"}* \[AV-vul: श्रिताः] *[||]{style="color:#99cc00;"}*\
What does the word nya/nyas in this ऋक् mean?\
At this stage I am unable to fully grasp this mantra. सायण tells us that it is "नेतारस् तत्रत्याः प्राणिनः". So he derives it from the root nI. But is it actually derived from the root nyas in the sense of deposited. This might fight in well with the other constructs like दृम्ह=firm up; sthira=fix and hence nya=deposit. If so this might give some clarity about the mantra.


