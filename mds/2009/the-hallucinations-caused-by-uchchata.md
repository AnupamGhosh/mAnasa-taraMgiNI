
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The hallucinations caused by उच्चटा](https://manasataramgini.wordpress.com/2009/05/18/the-hallucinations-caused-by-uchchata/){rel="bookmark"} {#the-hallucinations-caused-by-उचचट .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 18, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/05/18/the-hallucinations-caused-by-uchchata/ "6:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The history of the rasatantra branch of the kaula system is a vast topic in itself with numerous texts and practitioners. We may, if we get the chance, provide some glimpses of this material here. The medieval shaiva lore preserved by the नाथ yogin-s in north India had the following tale about this matter (narrated in a text termed the भूति-प्रकरण relating to rasa-bandha. It might stem in part from an older कुब्जिका tantra):\
स्वच्छन्द bhairava manifesting from the southward aghora face (भूतिप्रकरण mentions the aghora mantra multiple times) had a discussion with कुब्जिका in the on the himAlaya on the secrets of रासायन. She having heard his vast 25,000 verse tantra (a usual tantric number) rewrote in a 12,000 verse text the mahodadhi. Then during her incarnation as उमा haimavati she again re-composed it in 6000 verses as the रसोपनिषद्. Her elder son, skanda then re-wrote a treatise in 6000 verses termed the rasa-tantra. An abbreviated rasa-tantra was then produced in 1000 verses by the गण वीरभद्र. shukra having learned it from rudra rewrote it in 6000 verses termed the रसावलोक. His rival बृहस्पति did the same in 8000 verses termed the rasachakra. Then चन्द्रात्रेय composed a रसाङ्कुष in 10000 verses and गणpati composed the rasodaya in 8000 verses.

This भुतिप्रकरण might contain early material of the rasa stream that was originally associated with the कुब्जिका tantra-s, but there is no guarantee that it is an early text as a whole. The other text that certainly contains some early material of the core rasa stream is the Anandakanda. But this again is clearly a composite text that was redacted in the medieval period by the rasa-yogin-s who followed the काकिनी मुक्ता from the महाराट्ट country and others who followed the वङ्ग nAstika कुक्कुरिपाद. It is ironic that the rasa school of the independent काकिनी मुक्ता (considered by some sister of the ascetic ज्ञानेश्वर, born 1279 CE) is largely forgotten among her coethnics, who tend to identify (conflate?) her with the devotional vernacular poetess. Other than the mention in the Anandakanda as renowned rasa-siddhA, the work of नित्यनाथ on rasa records her activity as the only female rasa-siddhA before her death at a young age in a flood in northern Maharashtra. Another hypothesis is that मुक्ता might have been a rasa-योगिनी of the tradition of the नाथ-s who was independently historicized in the महाराट्ट country in the context of ज्न्\~आनेश्वर and also in the southern country. What ever the case, her position as a successor of the नाथ yogins in the rasa tradition is rather clear. While pure early texts of the rasa tradition are not yet known to me, it does appear that two texts of the middle layer are available to us: the काकचण्डीश्वर-kalpa tantra and the काकचण्डेश्वरी-mata.

The काकचण्डेश्वरी-mata is a discourse of shiva given to his shakti काकचण्डी or काकचामुण्डा surrounded by योगिनी-s named क्रन्दनन्दी, महाकाली, भृङ्गचण्डी विनायकी, कपाली, कालरात्री, कालचन्द्रा, कालाम्बिका, कराली, कालकर्णी and काकचण्डेश्वरी. The देवी asks rudra to expound on the विद्या-s of रसायन and धातुवाद. The काकचन्देश्वर kalpa occurs as a kalpa of the former tantra or occurs as a standalone copy. This text has several interesting plants of which one is उच्चटा

[रासायनं प्रवक्ष्यामि यत्-सुरैर्-अपि दुर्लभं ।\
उच्चटेत्योषधी काचिज्-जायते पृथिवी-तले ॥]{style="color: #0000ff"}

I will narrate the chemical preparation that is even rare amongst the gods. It is known as उच्चटा, which grows on the land.

[वर्तुलं पर्णम् अस्यातश्च रूपम् एवं प्रदृष्यते ।\
एक नाला भवेत् सा तु वर्णेना कृष्ण नीलिका ॥]{style="color: #0000ff"}

It displays the following form: circular leaves, a single stalk and a dark bluish color.

[तस्याः पुष्पं भवेद् भुग्नं शुक-तुण्द-समप्रभं ।\
कन्दं कूर्म-प्रतीकाशं तस्या लक्षणम् ईदृशं ॥]{style="color: #0000ff"}

It is endowed with the following features: Its flowers are drooping and have the color of a parrot's beak. Its tuber is of the form of a tortoise

[उपर्यारुह्या तस्या अप्य् अन्तरिक्षं निरीक्षयेत् ।\
देव-यक्ष-गणाः सर्वे दृश्यन्ते नात्र संशयः ॥]{style="color: #0000ff"}

By it one can rise upwards and verily examine the sky. One can see without doubt all the deva-s and यक्ष-s.

[रसश्-च द्रवते देवि मधुरश्-च विशेषतः ।\
सादृश्यं सर्वलोहानां सकृल् लेपेन याति सः ॥]{style="color: #0000ff"}

O goddess it oozes a liquid that is particularly sweet. By making a paste of it and using everything appears as though made of metals. Or more literally, he makes everything to appear to like metal by using its paste.

[काञ्चनं कुरुते दिव्यं क्षिप्रं चैव च वह्निना ।\
मज्-जयित्वाञ्गुलिं तत्र लिहेत् तं तु सकृत् पुमान् ॥]{style="color: #0000ff"}

It rapidly becomes shiny golden in color \[when heated] by fire. A man must dip his finger into it and lick it.

[विमानानि छ सर्वाणि तत्र पत्रस्थिता नराः ।\
पश्यन्ति नात्र सन्देह ओषध्यास्तु प्रभावतः ॥]{style="color: #0000ff"}

All kinds of aircraft with men riding by their wings one sees without doubt under the influence of the drug.

What is pretty remarkable is that hallucination produced by using this herb is said to be one of flying on an aircraft or more generally flying in the sky where one sees celestials. In the least we know that Hindus at least hallucinated of being on a vimAna even if the vimAna-s like those of bhoja did not take off. Despite use of terms like loha and काञ्चन in this kalpa I believe that the primary thrust is about its psychotropic effects rather than anything else.


