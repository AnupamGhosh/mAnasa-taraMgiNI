
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The story of Mn](https://manasataramgini.wordpress.com/2009/10/29/the-story-of-mn/){rel="bookmark"} {#the-story-of-mn .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 29, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/10/29/the-story-of-mn/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We have referred to Mn under many disguises. He was and remains the master of concealment and image-building but the muni had seen through him early and summarily dismissed his disguises. The muni had formerly made him the topic of his hilarious one-liners in his study of English vocabulary. Why waste time on Mn one might ask? Well, in this declining age of the kali we need to understand the causes of why our foundations are being swept away. Importantly his tale offers an example of how buddhi does not necessarily mean viveka -- much like the old tale of the ब्राह्मण-s who brought the tiger back to life. Mn was enormously learned and intelligent --- before him all of us were like mere fools. Every object of learning he mastered with ease and always knew more on any subject than others around him. In the days of yore he applied himself to the practice of Hindu ways. He identified himself as a Hindu and performed his rituals regularly. He corrected the errors in our understanding of the yajurvedic अनुनासिक which had come due to the wrong instruction of the sachiva. The अमात्य was awfully scared of his great power of disguise and felt we might be fooled by it. But little did the अमात्य know that a fool we might have been, but not so great a fool. As Mn grew older he became less of Hindu and he spoke dismissively of the rituals of a ब्राह्मण. But now he was an Indian nationalist. He knew everything of geopolitics and how important it was for India to curb TSP and TSB. He foresaw trouble in Nepal and the Taleban, and he wanted India to conquer Shri Lanka! He endlessly spoke of the weapon systems of the Indian military and of missiles and nukes. Then Mn reached the shores of the म्लेच्छदेश a little after I had done the same. He was now a big man in every sense of the word. He now said different things. He now said ट्SPइअन्स् are very good people and we must not talk ill of them as they are our people. Amazingly, he had forgotten every iota of geopolitics and asked us why we waste time on "politics". He constantly genuflected before powerful म्लेच्छ-s and sang their tune what ever it may be. If a म्लेच्छ said something Mn would say it is cool. He spoke of how a Hindu should not criticize Mohammedanism and Isaism for it was a mean thing to criticize others religions while praising ones own. His relatives warned him about the evils of the religions of peace and love but Mn admonished them not to be mean. He had forgotten his gotra --- he even claimed he did not know what a gotra meant. He had forgotten his veda and he claimed it was something the पूजारि-s do in temples. No, Mn had not decreased even a delta x in his intelligence, but in his viveka he had regressed beyond where he was as child. He is not alone --- as ekanetra and the वङ्ग paNDita noted so many around us have gone the way of Mn.

People used think Mn was a non-conformist and I was conformist. But the wise अमात्य felt the reality was reverse. Indeed therein lies the answer to mysteries such as Mn.


