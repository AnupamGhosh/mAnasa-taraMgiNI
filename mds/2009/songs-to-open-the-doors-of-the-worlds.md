
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Songs to open the doors of the worlds](https://manasataramgini.wordpress.com/2009/03/08/songs-to-open-the-doors-of-the-worlds/){rel="bookmark"} {#songs-to-open-the-doors-of-the-worlds .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 8, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/03/08/songs-to-open-the-doors-of-the-worlds/ "6:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The 4 गान-s that comprise the vaishadeva series are based on the code words:\
rAT, विराट्, स्वराट्, संराट्. The first is for the conquest of पृथिवि, the second for the conquest of अन्तरिक्ष and the third and fourth for the conquest of dyaus. The first sAman opens the earthly door so that the यजमान might behold the vasu-s. The second opens the door of the atmosphere so that the यजमान might behold the rudra-s. The third and fourth sAmAn-s open the door of the sky so that the यजमान might behold the Aditya-s and other deva-s. The sAman-s end in oblations that state that after the end of the life of the यजमान "attains" these loka-s. I have repeatedly heard the colloquial tradition among स्मार्त ritualists that states that these mantra-s are a means of attaining these "upper" worlds of the gods upon the death of the यजमान. I was however puzzled by the need to go to पृथिवी when you are already there. If it was merely restricted to opening doors to go upwards with the funerary fire to the atmosphere and beyond then the colloquial belief is fine, but what about the earthly doors?

In reality these sAman-s should be taken in the same sense as the vedAntic teaching of the last part of तैत्तिरीयोपनिषत् 3.10 (the section beginning with अस्माल्-लोकात् pretya; Here too, the brahmavit sings a sAman as he attains universal consciousness upon his death). So what is meant here is that the यजमान experiences and expansion of consciousness to become part of the पृथिवी, अन्तरिक्ष and dyaus simultaneously. The image of the opening door is the related to this and the passage of his consciousness to encompass these realms. To the outside observer all this will appear to be either extremely bewildering or the usual mumbo-jumbo :-). But if one looks at the altered consciousness experienced by subjects under the influence N,N-dimethyltryptamine one can see that the themes of both the गान-s in the तैत्तिरीय and the Chandogya are closely reproduced even in people who have never encountered these texts (including the act of dying, which happens early in a DMT experience, and then the expansion of consciousness to occupy the worlds). The big question is how did the vipra-s attain experience of these states --was it via substance or a proto-yoga?


