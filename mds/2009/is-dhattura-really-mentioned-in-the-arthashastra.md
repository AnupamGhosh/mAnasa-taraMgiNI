
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Is धत्तूर really mentioned in the अर्थशास्त्र?](https://manasataramgini.wordpress.com/2009/01/26/is-dhattura-really-mentioned-in-the-arthashastra/){rel="bookmark"} {#is-धततर-really-mentioned-in-the-अरथशसतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 26, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/26/is-dhattura-really-mentioned-in-the-arthashastra/ "7:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the recent past people have generally believed that the अर्थशास्त्र represents one of the earliest mentions of धत्तूर (*Datura metel*) that is believed to be of pre-Columbian New World origin. Is the mention of the धत्तूर in the AS for true? It supposedly comes in the section on liquors and is believed to be mentioned in 2.25.33 along with a whole range of other plants. These plants are supposed to be additives to produce flavored drinks. The manuscripts used by Kangle in his edition of the AS have the word पत्तूर and not धत्तूर. One of his manuscripts termed G1 has it as वत्तूर. Kangle notes that G1 was copied from a source in the malayALaM script in which 'va' and 'pa' are close and can be conflated (also seen in Tamil grantha e.g. अवमानं for अपमानं). Hence, in all likelihood it was पत्तूर and not धत्तूर. The general belief is that पत्तूर is a distinct plant, namely *Achyranthes triandra* or *Alternanthera* or generically some member of Amaranthaceae. So it is unlikely that the toxic *Datura* was being added into the liquor of the अर्थशास्त्र.

Likewise, I have not seen any evidence for the mention of this plant in the two इतिहासस् or charaka, the earliest of the 3 great medical saMhitA-s of the Hindus. While the domain of artha is apparently lacking in unmatta, the कामसूत्र of वात्स्यायन does seem to have it. It is mentioned in 7.1.25 as a component of a concoction that is applied on the लिङ्ग (धत्तूरक-marica-पिप्पली-cऊर्णैर् madhu-mishrair lipta-लिङ्गस्य samprayogo वशी-करणम्). If there is any reality to this claim it might be the supposed erogenic role of tropanes. Likewise, both धत्तूर and पत्तूर are apparently mentioned in the medical saMhitA of sushruta. Thus, the कामसूत्र and the sushruta saMhitA happen to be the earliest surviving texts with a mention of the धत्तूर plant in the old world. The absence of evidence is not necessarily evidence for absence. However, it should be noted that the number of references to धत्तूर in the texts that temporally post-date these two texts only increases. In the Astika and nAstika tantric literature (as noted in the latter by the Eastern European scholar Siklos) the plant is very prominent along with much older plants that are already found in the atharvan literature (e.g. अपामार्ग). This might give us a means of fixing temporal window of the introduction of dhattura to India. Of course this later introduction would leave one uncertain about the psychoactive substance used by the vedic muni psychonauts. In the RV (10.136) the muni-s are mentioned as using a विष (also possibly AV-पैप्पलाद 5.17.3 where the word unmatta is used in connection with the muni), but in light of the above there are serious doubts if it is धत्तूर as otherwise assumed.


