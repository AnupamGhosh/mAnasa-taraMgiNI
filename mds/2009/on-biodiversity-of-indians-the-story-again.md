
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On biodiversity of Indians: the story again](https://manasataramgini.wordpress.com/2009/07/31/on-biodiversity-of-indians-the-story-again/){rel="bookmark"} {#on-biodiversity-of-indians-the-story-again .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 31, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/07/31/on-biodiversity-of-indians-the-story-again/ "6:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![world_races](https://manasataramgini.files.wordpress.com/2009/07/world_races.jpg "world_races"){width="75%"}

*[[A principle component analysis using a panel of 93 ancestry informative SNPs. Modified from :BMC Genetics 2009, Nassir et al. It shows how with these markers one can distinguish races such as African, Western Eurasian, Eastern Eurasian, Oceanian and Native American. But Indian are difficult to separate from Western Eurasians]{style="color:#99cc00;"}.]{style="color:#808000;"}*

The leukosphere and its sepoys headed by the subversionists of madhyama म्लेच्छ-desha of क्रौञ्चद्वीप have been consistently trying to erase the identity of Hindus by foisting upon us such despicable identity-masking descriptors such "South Asia". Yet the Hindu people comprise about a 6th of our species. Their genetics, linguistics and prehistory have been intensely studied, yet there remain many lacunae in our understanding of their origins. As of 2009 CE more than a decade of molecular studies has been completed on the Indian genomes. We shall briefly look at these findings to see if they can be reconciled with linguistic and archaeological data.

The genetic affinities of Indians as currently understood can be succinctly presented thusly:

 1.  The Indians as a whole form a rather distinctive group of humans that are well differentiated in genetic terms from Western (e.g. Europeans) as well as Eastern Eurasian peoples (e.g. Chinese, Japanese etc).

 2.  Yet, if we look at total ancestry based on the resultant signal from biparental markers we observe the Indians to be closer to Western Eurasian than to Eastern Eurasians.

 3.  However, if one looks at uni-parental markers we see the following pattern:\
3.1- The Y-chromosomal affinities of Indians are greater to Western-Eurasian than Eastern Eurasians.\
3.2- The Mitochondrial DNA affinities of Indians are greater to Eastern Eurasians than Western Eurasians.

 4.  Amongst Indians, irrespective of whether one looks at uniparental or biparental markers, the upper castes have greater affinity to Western Eurasians than to Eastern Eurasians. But this effect is most pronounced for Y-chromosomal data. Amongst Indians, highest affinity to Eastern Eurasians is seen only amongst tribals.

The cultural/linguistic situation may be summarized thusly:

 1.  The majority of Indians speak an Indo-European language of a single subfamily (Indo-Aryan), a language family they share with a large fraction of the Western Eurasians. Another major language family amongst Indians is Dravidian, which is unique to India.

 2.  Majority of Indians, irrespective of the language they speak, followed Indo-European cultural practices at least until recently and the survival of the most archaic Indo-European traditions is only seen in India.

 3.  Indo-Aryan as well as Dravidian shows evidence for a linguistic substratum from which each language acquired a large number of plant, animal and agricultural terms.

The chief archaeological issues might be summarized thusly:

 1.  India appears to have supported a microlithic hunter-gatherer population by around 30,000 y BP.

 2.  India appears to represents a zone of early independent domestication of various crops such as rice, millets, and certain lentils.

 3.  Farming and animal husbandry practices appear early in the NW of the subcontinent.

 4.  The same NW region shows a subsequent emergence of early regionalized large scale settlements starting around 5000-4500 BCE. By 2600 BCE these regional settlements in the NW had integrated into a massive culturally pretty uniform urban civilization -- the IVC/SSVC. This civilization flourishes rather undisturbed for about 700 years during which some materially connected Chalcolithic civilizations appear in the interior of the sub-continent. At its peak it spans from the banks of the Oxus to the banks of the Godavari

 5.  Starting around 1900 BCE to around 1000 BCE the IVC sites in the NW declined and the population moved carrying aspects of the culture to the Ganga-Yamuna Doab and into Gujarat. Eventually the urbanization considerably declined or vanished until a second vigorous urbanization around 600 BCE.

If viewed at this level of granularity a relatively consistent picture can be constructed. Both genetics and linguistics point to an intimate connection between the Indian people and Western Eurasians rather than with Eastern Eurasians, except of the mitochondrial DNA. Both Neolithic and chalcolithic developments on the Indian subcontinent begin in the Northwest of the subcontinent and spread inwards. Taken together these observations imply that there has been a flux of people from the NW in waves who have notably contributed to both the linguistic and cultural affinities of modern Indians. This flux appears to have been either male dominated or the incumbent males from NW practiced hypergyny with the native females belonging to an older population closer to the modern Eastern Eurasians. The correlation between caste hierarchy and the genetic affinities to western Eurasians strongly supports that the idea the carriers of the Indo-Aryan language were a major component of this human wave which spread developments from the NW into the subcontinent.

But this neat picture is definitely muddied when looks at several specifics:

 1.  The R1 lineages are major Y-chromosomal haplotypes shared between Indians and western Eurasians. However, the studies on the diversity of the R1 lineages seem to suggest that their diversity is greatest in India. This at face value would suggest an efflux rather than influx from Western Eurasia. However, the reality may be more complex -- involving a very early (pre-Indo-European efflux) followed by an influx of Indo-Aryan.

 2.  The IVC shows few signals that are compatible with it being Indo-Aryan e.g. rarity of depictions of horses which are central animals in the earliest Indo-Aryan text the ऋग्वेद. At the same time there is no evidence for a cataclysmic end of the IVC at the hands of the Indo-Aryans. Yet, the spread of the IVC and the linked inner chalcolithic culture closely overlap with the historical spread of Indo-Aryan. The age of the earliest Indo-Aryan texts, despite baseless claims of white Indologists to the contrary, cannot be placed after the core IVC period. This IVC vs Indo-Aryan discordance remains a major problem in Indian history.

 3.  The IVC graffiti continues to confuse -- is it a script or not remains unclear? Hence the corollary, namely the IVC language remains intractable. So despite certain recognizable Indic features we remain in the dark about the IVC/SSVC. I do suspect that the language X that appears as a substratum in Indo-Aryan and Dravidian might indeed be the original IVC but at some point it underwent a conversion to Indo-Aryan. When this happened remains entirely intractable yet.\
So we may call the glass either half-full or half-empty -- a personal choice. But in practical terms, we find Indian researchers exhibiting enormous intellectual insufficiency in handling these problems: Archaeologists display misunderstanding of comparative linguistics and textual studies. Linguists are non-existent or display a fundamental lack of understanding of the texts from which they get their languages. Biologists show a lack of understanding of texts, linguistics and culture. Add म्लेच्छ-s with agendas and the dead weight of conformity to this mix and the result is fog.


