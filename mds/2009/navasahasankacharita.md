
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नवसाहसाङ्कचरित](https://manasataramgini.wordpress.com/2009/04/14/navasahasankacharita/){rel="bookmark"} {#नवसहसङकचरत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 14, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/04/14/navasahasankacharita/ "6:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Long ago the Acharya had said that we should study the नवसाहसाङ्कचरित (NSC). But being truants we ignored his sage words. Years later we felt that we should relieve the enactments of the rAjpUt rulers we had played as kids and felt the need for authentic material. That is when we turned back to the नवसाहसाङ्कचरित, but with the आचार्य no longer around we are not sure that we could extract its sense in totality. The name of this काव्य is derived from साहसाङ्क, who is chandragupta-II विक्रमादित्य the pinnacle of gupta glory. The new साहसाङ्क, the protagonist of this काव्य, is सिन्धुराज the king of the परमार rAjpUt, whose dynasty was to be adorned by the great bhoja-deva. The author is a certain parimala padmagupta the son of मृगाङ्कदत्त. The epic is based on this historical character but imitates the रामायण or the Illiad in its core motif. The king सिन्धुराज with his minister रमाङ्गद were on a hunt in the vicinity of the narmada when the king shoots a deer of the नागिनी शशिप्रभा. This leads him to making an acquaintance with her, but before he could propose marriage to her, she vanishes on hearing an incorporeal voice and returns to her father. He then pursues her by jumping into the नर्मदा and passing through a subterranean grotto \[a very common Hindu motif found in the रामायण -- as the वानर-s were searching for sItA and also in the दशकुमार of दण्डिन्]. The goddess of the river tells him that he can win the नागिनी only if he can get the golden lotus from the king of the asura-s, who is interestingly name वज्राङ्कुश, and give it to the father of शशिप्रभा. The goddess also tells him to meet a ब्राह्मण named वङ्कु to get advice on his quest of the golden lotus. The king sends a message to शशिप्रभा of his intention to get the using a parrot who is actually a nAga cursed by a student of काण्व to become a parrot. Then king goes on his quest by first reaching वङ्कु's domain. There he is given a pomegranate by an ape. As soon as he accepts it's the monkey becomes a विद्याधर, शशिखण्ड the son of शिखण्डकेतु \[he had been cursed by वङ्कु to become an ape as he was trying to seize a maiden in the ब्राह्मण's Ashrama]. This शशिखण्ड then summons his own army of विद्याधर-s to aid the परमार राजन्. He also gives the king a vimAna that flies due to the विद्याधर's magic to set forth for the campaign. The parrot on delivering the message to the princess regains his form as the curse is lifted and returns to the king as रत्नचूड, a nAga. The ratna on his crest illumines the path for the army of सिन्धुराज to reach the asura capital of रत्नवती.

There the राजन् sent a message to the asura वज्राङ्कुश via रमाङ्गद asking him to hand over the golden lily to the परमार or else face war. वज्राङ्कुश refuses and states that he not the परमार would take शशिप्रभा. Duly the war breaks out between the asura-s one side and the विद्याधर-s, nAga-s and परमार-s on the other. In course of the fierce fight रमाङ्गद killed the asura's son while नवसाहसाङ्क himself killed वज्राङ्कुश by cutting his head off with a shara. Then he crowned रत्नचूड as the king of रत्नवती. With his minister bearing the golden water lily the king went to bhogavati the capital of the nAga-s. There he saw the temple of shiva known as हाटकेश्वर and he performed his worship there. Then was married to शशिप्रभा to whom he gives the golden lotus. Before they return to धार his father-in-law the nAga king gives him a स्फटिक लिङ्ग made by त्वष्टर्. He takes the लिङ्ग and first goes to ujjayini and worships a लिङ्ग of rudra there. Then he goes to धार his clan's capital and installs the लिङ्ग received from the nAga-s there.

Who are the हूण-s defeated by परमार-s in this work? I wonder if the conventional explanation is correct at all in this regard.

There are some motifs here that are reminiscent of दण्डिन्'s novel. In that work the feral ब्राह्मण is educated in the tantra-s by a ब्राह्मण whose life he saves from savage किराट-s. He then performs the worship of shiva and is told of a स्फटिक लिङ्ग near which lies the entry to a grotto leading to the netherworld. At the entrance to the cave he finds a copper plate with a prayoga inscribed on it. He performs the specified prayoga in the netherworld and acquires an असुरकन्या as his wife who gives him a jewel that he later gifts to his companion राजवाहन. Of course the overall similarity of themes to the बृहत्-katha is also quite obvious. Beyond the name "nava-साहसाङ्क" the परमार king is already identified with the विक्रमादित्य in many ways in this work: The return from the nAga world to ujjayini the capital of the old gupta monarch is one such point. This trend was to culminate in the legends around bhoja-deva the successor of सिन्धुराज \[e.g.:\
[अस्ति क्षितऊज्जयिनीति नाम्ना पुरी विहायस्य् अमरावतीव\
बबन्ध यस्यां पदम् इन्द्रकल्पः श्रीविक्रमादित्य इति क्षितीशः ।]{style="color:#99cc00;"}|\
or the statement:\
[अतीते विक्रमादित्ये गते ।अस्तं सातवाहने\
कविमित्रे विशश्राम यस्मिन् देवी सरस्वती ॥]{style="color:#99cc00;"}\
I.e. after the death of विक्रमादित्य and सातवाहन सरस्वती was now resting in सिन्धुराज, the protagonist of the novel.]

Interestingly, when the minister narrates the tale of the agnikula origins of the परमार-s he places it in the context वसिष्ठ-s struggle with the गाथिन. He also states that वसिष्ठ makes the first paramara using atharvanic spells:\
[अथाथर्वविदाम् आद्यः समन्त्राम् आहुतिं ददौ\
विकसद्-विकट-ज्वाला-जटिले जातवेदसि ॥]{style="color:#99cc00;"}

Thanks to the aid from paNDita somadeva we were able enjoy more aspects of this काव्य. Reading the verses again underscored the pleasantness of the dhvani of the vaidarbhi style to the years.\
[चक्षुस् तद् उन्मेषि सदा मुखे वः\
सारस्वतं शाश्वतम् आविर् अस्तु\
पश्यन्ति येनावहिताः कवीन्द्राः\
त्रिविष्टपाभ्यन्तर वर्ति वस्तु ॥]{style="color:#99cc00;"}\
संपूर्ण अनुवाद:\
चक्षुः=eye; tat= that; उन्मेषि=opening; सदा= always; mukha= face; वः= your;\
सारस्वतं=eloquence; शाश्वतम्=eternal; Avis= manifest; astu= be;\
pashyanti=behold; yena=by which; अवहिताः=attentive; कवीन्द्राः=chiefs among poets; त्रिविष्टप= celestial/heaven; abhyantara=inside; varti=abiding in/situated; vastu=essence/object/seat.

The text also has a stuti of हाटकेश्वर sung by the परमार king (provided unedited):\
[अन्तर्जतापिहितसोमसुरापगाय प्रच्छन्नशरशासनलोचनाय\
तीव्रव्रतग्लपितशैलसुतास्वरूप विज्ञाननर्मपटवे बटवे नमस्ते ॥]{style="color:#99cc00;"}

[अत्यादरानतसुरासुरमौलिरत्न नानामरीचिखचिताङ्घ्रिसरोरुहाय\
देहार्धवर्तिगिरिजाविहिताभ्यसूय सन्ध्याप्रणामविषमाञ्जलये नमस्ते ॥]{style="color:#99cc00;"}

[नीरन्ध्रसिन्धुजलसिक्तकपालमुक्त रत्नाङ्कुरस्य करणीं विधुरातनोति\
मौलौ सदैव भवतो भवभेदकर्तुर् निर्दग्धभास्करमहाय नमो ।अस्तु तस्मै ॥]{style="color:#99cc00;"}

[कन्दर्पदर्पशमनाय कृतान्तहर्त्रे कर्त्रे शुभस्य भुजगाधिपवेष्टनाय\
उर्वीमरुद्रविनिशाकरवह्नितोय याज्याम्बरोच्चवपुषे सुपुषे नमस्ते ॥]{style="color:#99cc00;"}

[नीरन्ध्रभूतिधवलाय गजेन्द्र-कृत्ति संवीतदेहकवली कृत पन्नगाय\
निर्दग्धदानवकुलाय विपत्क्षयैक कार्याय कारणनुताय नमो ऽस्तु तुभ्यम् ॥]{style="color:#99cc00;"}

[te te yam eva kila वाङ्मयसागरस्य pAraM गताः प्रणवम् Atmavido vadanti\
तस्मै समाहितमहर्षिविनिद्रहृद्य-हृत्पुण्डरीकविहितस्थितये नमस्ते ॥]{style="color:#99cc00;"}

[उत्तंसितेन्दुशकलाय कपालजूट सङ्घट्टितोर्मिमुखराम्बरनिर्झराय\
भस्माङ्गरागशुचये विकचोपवीत- व्यालेन्दुमौलिमणिदीधितये नमस्ते ॥]{style="color:#99cc00;"}

[नास्त्रं न भस्म न जटा न कपालदाम नेन्दुः सिद्धतटिनी न फणीन्द्रहारः\
नोक्षा विषं न दयितापि न यत्र रूपम् अव्यक्तम् ईश किल तद् दधते नमस्ते ॥]{style="color:#99cc00;"}


