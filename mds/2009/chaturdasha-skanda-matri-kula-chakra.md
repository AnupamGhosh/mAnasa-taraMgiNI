
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [chaturdasha skanda-मातृ-kula-chakra](https://manasataramgini.wordpress.com/2009/09/21/chaturdasha-skanda-matri-kula-chakra/){rel="bookmark"} {#chaturdasha-skanda-मत-kula-chakra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 21, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/09/21/chaturdasha-skanda-matri-kula-chakra/ "6:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2462/3943058473_577db3e628.jpg){width="75%"}
```{=latex}
\end{center}
```

\
कुमार with the kula of glorious goddesses in the holy city of Mathura

In the कौमार lore an important chakra is the 14 fold chakra in the center of which guha is worshipped. There are 3 major आवरण-s: 1) the chaturdasha-मातृका; 2) the कृत्तिका chakra; 3) भ्रातॄ chakra with कुमार residing in the bindu

The देवता-s of the chaturdasha-मातृका are:\
आवेशिनी, अश्रुमुखी, कुतूहली, एकस्तनी, जृम्भिणी, स्तम्भिनी, मोहिनी, कृष्णा, विशाखा, विमला, ब्रह्मरात्री, भ्रातृव्य-सङ्खी, इषुपतन्ती, अमोघा\
The देवता-s of the कृत्तिका chakra are:\
ambA, दुला, नितत्नी, अभ्रयन्ती, मेघयन्ती, वर्षयन्ती\
The देवता-s of the भ्रातृ chakra are:\
shAkha, विषाख and नेजमेष.\
The देवता of the bindu is:\
षण्मुख

This was first established in the days of yore in holy city of Mathura, the क्षेत्र which was then graced by several कुमारालय-s. There was first the old city of Mathura (the Mathura near पुष्पपुर, now Peshawar in the terrorist state) that lay near where the head of महिष when severed by skanda had fallen blocking the way to the uttara-kuru lands. Then a new Mathura arose in the land of the yadu-s and finally another Mathura arose in dakSiNApatha in the land of the पाण्ड्य-s. Each of them was the center of कुमारालय-s in yore. But the holy city of Mathura in yadu land was the great कौमार क्षेत्र of the past. Repairing there one revels in recounting and hearing the various sports of कुमार starting with his deeds as parvata-नाशक, the destroyer mountains. One recalls the account of the fiery son of agni impaling क्रौञ्च the son of himavat with his arrows and demolishing the peaks of the shveta-parvata with his shakti and sending other mountains flying off the earth. Then one recalls his marriage with देवसेना who is also known as षष्ठी. Having done so, one recalls his frightful roars and war-cries fiery deeds in battles with दानव-s and the overthrowing of the demonic प्रह्लाद. Then on a षष्ठी night he repairs to the kadamba-vana and invokes skanda with the secret mantra and the त्रिशती in which it is embedded. He then invokes the red लोहितायनी holding a deadly शूल. While a ब्राह्मण offers her red arka flowers, other वर्ण-s might offer her blood. He then invokes the twin deities मिञ्जिका and mi\~jinka, the fierce shishu, षष्ठी, agni, indra and विष्णु with oblations in his ritual fire. He then invokes the deva in the circle of glorious goddesses in the मण्डल outlined above. The chakra might also take the form of 3-dimensional मण्डल like a meru.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3510/3940521970_607415ef99.jpg){width="75%"}
```{=latex}
\end{center}
```




