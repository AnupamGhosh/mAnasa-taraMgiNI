
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Lost ऋक्-s, the shruti and शूद्र-s](https://manasataramgini.wordpress.com/2009/05/29/lost-rik-s-the-shruti-and-shudra-s/){rel="bookmark"} {#lost-ऋक-s-the-shruti-and-शदर-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 29, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/05/29/lost-rik-s-the-shruti-and-shudra-s/ "6:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A chaturtha asked me if mantra-s in the shruti were entirely inaccessible to him. I told that if looked hard enough in the texts to which he had अधिकार he could find enough to be moderately aware of the contents of the shruti if not its praxis, which indeed is largely the domain of the dvija-s. The vedic material embedded in the इतिहास-s, the हरिवंश, and the पुराण-s has been of some interest to me for a while, though I have never gotten around to systematically studying it by computational means. Kane and Apte have written on this topic, with the former precisely touching on this issue of the अधिकार of the शूद्र and the shruti. A learned clansmen, a student of the first veda, informed me that Apte is supposed to have had a student in the city of my youth who did such a study quite systematically for the great epic but I have never been able to access his dissertation on the same. There are, of course, a few famous examples, which we have covered at length on these pages before -- the hymn of the ashvin-s and the praise of indra. Some are intended quotations, while others like the spell of queen कौशल्या are a mixture of vedic and later elements. The intended quotations, as Apte points out, are labeled with the qualifiers such as:\
imAm श्रुतिं उदाहरेत्\
निदर्शनं चात्र bhavati\
ityapi श्रूयते श्रुतिः\
यस्तं veda sa veda vit\
In the old days I used to have a debate with some acquaintances over the possibility of a para-vedic tAntric system. It was in this context that I observed that the इतिहास-s and the हरिवंश cite vaidika mantra-s as such, but rarely, if ever, furnish a conventional तान्त्रिक mantra \[Footnote 1]. This to me indicated that the conventional तान्त्रिक system post-dates the इतिहास-s though they do overlap with the majority of the पुराण-s as they survive today. Hence, I tend take the stance that the तान्त्रिक mantra शास्त्र is largely an evolute of the late vaidika system rather than being a survivor of a para-vedic system \[Footnote 2].

Let us consider some well known citations from the bhArata. One chapter that is replete with citations is the lecture of कृष्ण to arjuna on the nature of अग्नीषोमौ and नारायण (vulgate MBH 12.343). It is definitely a highly interpolated section with at least 3 major players: 1) ब्राह्मण-s trying to assert their superiority in the brahma-क्षत्र alliance. 2) vaiShNava-s trying to establish नारायण-परत्वं and 3) shaiva-s trying to establish rudra-परत्वं. Yet all manuscripts contain the vedic citations in some form and the core of the narrative is actually that of agni-Shoma which is directly based on the vedic citations. This is evidenced by the original question of arjuna to कृष्ण:\
अग्नीषोमौ कथं पूर्वम् एकयोनी pravartitau |\
एष me संशयो जातस्तं chhindhi मधुसूदन ||\
How did agni-Shomau, in days of yore, get established in the same original source? Such a doubt has arisen, please clear it O मधुसूदन.

It is in answering this that कृष्ण explains the transformations of नारायण and cites some vaidika mantra-s as प्रमाण-s:\
nidarshanamapi hyatra bhavati:\
नासीदहो na रात्रिरासीत् | na सदासीन् नासदासीत्\
tama eva पुरस्ताद् abhavad विश्वरूपं ||

मन्त्रवादो.अपी hi bhavati:\
tvamagne यज्ञानां होता विश्वेषां hito devebhir मानुषे janeti ||

nidarshana.n चात्र bhavati:\
विश्वेषाम् agne यज्ञानां hoteti | hito devair मानुषैर् jagata iti ||

[1] stotras to तान्त्रिक deities might be found but they lack तान्त्रिक mantra-s; the हरिवंश might preserve proto-tAntric mantra elements in the context of एकानम्शा as noted before.\
[2] in more general terms by extension I also believe that Talageri's idea of the vaidika system being restricted to the pUru-s as being erroneous -- the composition of the RV might have been dominated by the pUru-s but this hardly means that the other Indo-Aryans and Iranians did not follow a "vaidika" system.

continued...


