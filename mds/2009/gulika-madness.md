
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [गुलिका-madness](https://manasataramgini.wordpress.com/2009/05/25/gulika-madness/){rel="bookmark"} {#गलक-madness .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 25, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/05/25/gulika-madness/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the long past days we were seated in a niche at the foot of the विष्ठा-parvata with the chaturtha and the feral ब्राह्मण. The chaturtha was caressing his रामपूरीय असिपुत्रिका while SS polished the handle of his hasta-नालिक. They were happy that they had made a great impression among the स्त्रियः with the display of their shastra-s. We had settled down to waste our time over गुलिका-क्रीड -- just watching it not playing it. Those days the game of गुलिका-क्रीड was enjoyed over 5 days, but nowadays it has contracted to a game of few hours, with पुम्श्चली-s and महानाग्निका-s thrown in good measure. In many ways we have to be thankful that this form of the game did not exist on the TV in our days. We were starved for गुलिका-क्रीड and saw at best few snippets. That was bad enough. Can we forget that day? We were not even in the game -- our dismal Hindu सेना had been routed in the preliminaries by the म्लेच्छ-s, though, before reeling out, we had inflicted a crushing blow on the Mohammedans. A great a battle was going on TV between the Saxon crusaders lead by Martin Crowe and blood-thirsty green-robed Ghazis with their ranks swelling with the many savages from the Khyber. In course of watching this gripping encounter we suddenly realized that our physics exam was supposed to be going on in some forbidding hall at the college. In panic we mounted our ashva and rode towards the समराङ्गण like अमरसिंह galloping on the ramparts of the तुरुष्क fort at Agra. We got in 40 minutes late, only to be accosted at the entrance of the hall by a fierce marATha yelling some nonsense in mahArAShTrI. After some effort a light-eyed ब्राह्मण friend came to our aid and helped us to get into the hall after repulsing that rapine marATha. In the remaining hour we had to tackle a whole maze of Newtonian mechanics and electronics which rather strained our already stressed cranium. We still wonder how we broke that व्यूह on that fateful day which featured a battle much like that between घटोत्कच and अश्वत्थामन् on the 14th night after the fall of jayadratha. We had already earned some censure for dropping out of med-school. Now, pulling the plug in physics would have buried us in true ignominy. A few years later the muni was showing signs of being worse than us. I could not but help commiserate with him, despite the rising wrath of the elders in the household, when he triumphantly declared: "Exams may come again but a match like this will not come again". Now this abbreviated गुलिका-क्रीड lasting for a whole month with all the gossip, including the fake IPL player, could have been fatal had it played out in our days. However, the muni and I would probably think we were very prophetic for our self-contained entertainment till my last evening on the shores of जम्बुद्वीप was this abbreviated form of the game.


