
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An account from Lama तारनाथ's history](https://manasataramgini.wordpress.com/2009/11/02/an-account-from-lama-taranatha%e2%80%99s-history/){rel="bookmark"} {#an-account-from-lama-तरनथs-history .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 2, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/11/02/an-account-from-lama-taranatha%e2%80%99s-history/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

During the reign of पञ्चमसिंह lived two brothers who were आचार्य-s of the तीर्थिक-a. One of them named दत्तात्रेय was supposed to teach the doctrine of samAdhi. The second named शङ्कराचार्य was a worshiper of महादेव. He chanted mantra-s on jar with a covered lid. From the jar महादेव would emerge up to the neck and teach the art of debate. In वङ्ग he entered into debates with the bauddha-s and defeated them repeatedly. The bauddha scholars wanted to call in धर्मपाल, chandragomin or चन्द्रकीर्ति to debate with शङ्कर, but the local paNDita-s did not want their prestige to go down with outsiders coming to save their face. They decided to take on शङ्कर himself and were defeated by him in debate. As a result 25 bauddha centers fell to the तीर्थिक and 500 bauddha-s gave up the संघ to become shaiva-s. Similarly, in ओडिविश appeared a disciple of शङ्कर called भट्टाचर्य. He had been made an expert in nyAya by the daughter of brahmA. A bauddha आचार्य कुलिशश्रेष्ठ approached him in debate proud over his strength of grammar and logic. But he was routed in the debate with भट्टाचर्य and as a result bauddha-s had to forfeit their slaves to the तीर्थिक-s. The तीर्थिक-s now occupied the temples of the bauddha-s. In the south were two mighty तीर्थिक polemicists, the ब्राह्मण named कुमार्लील and कणनाद (could it be Tamilized corruption of गणनाथ; the Tibetan is: gzegs-ma-sgra-sgrorg). The latter was a shaiva (पाशुपत) who observed the govrata. In many debates in the south they routed the successors of बुद्धपालित, bhavya, धर्मदास and दिग्नाग. Similarly not one belonging to the श्रावक संघ or a स्थविरवादिन् was able to stand them in a debate. As a result many of the संघा had to go over to the तीर्थिक ब्राह्मण-s and the latter robbed the संघ of its property.


