
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some aspects of वैखानस iconic worship and their parallels](https://manasataramgini.wordpress.com/2009/01/11/some-aspects-of-vaikhanasa-iconic-worship-and-their-parallels/){rel="bookmark"} {#some-aspects-of-वखनस-iconic-worship-and-their-parallels .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 11, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/01/11/some-aspects-of-vaikhanasa-iconic-worship-and-their-parallels/ "8:14 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The spread of iconoclastic Zarathushtran and Abrahamistic cults over Eurasia have all but destroyed most traces of ancient iconic worship. In the midst of this destruction, which relentlessly continues under the crescent and the cross banners, one tradition alone preserves an ancient strain of the Eurasian systems of iconic worship. This is the system of the वैखानस-s, a miniscule group of तैत्तिरीयक-s who are officiants at several major विष्णु temples of the drAviDa, Andhra and कर्नाट countries. The inner worship at richest shrine of modern bhArata, वेङ्कटाद्रि, represents one of those old unbroken वैखानस lineages, so also several temples in the drAviDa country like kapisthala and चित्रकूट (of Chidambaram). We had earlier pointed out that the वैखानस गृह्य sUtra system of domestic विष्णु worship, closely parallels vidhi-s for iconic worship of deities found in the बोधायन गृह्य sUtra and the atharvaveda परिशिष्ठ-s and represent the earliest surviving textual layer of iconic worship among the Indo-Aryans. The rituals of iconic worship followed in these texts suggested that a distinctive form of the iconic worship emerged in the late vedic period with the following key features: 1) ritual actions relating to bathing, installation and empowerment of the icon with specific vedic formulae, modeled after the ritual actions using यजुष् formulae deployed during a shrauta rite 2)vedic offerings made into the औपासन fire for invocation of a deity, resembling the regular non-iconic vedic household worship of deities. The वैखानस-s additionally also possess a system related to their household worship of विष्णु icons that is used specifically for worship of विष्णु installed in public temples. The close parallelism of the temple and household worship of विष्णु suggests that the earliest temple worship emerged within the vedic tradition as a mirror of the basic household system, even as the shrauta (\~temple) and गृह्य(\~household) systems mirror each other in the older vedic layers.

The uniqueness of the वैखानस temple worship is marked by the वैखानस mantra-prashna, which contains several accented mantra-s of clearly vedic nature that are found in no other यजुष् saMhitA, but used exclusively in temple worship. This feature suggests that the वैखानस system of temple worship is indeed ancient and that it predates the emergence of tantrika temple worship of the vaiShNava, shaiva and कौमार variety. Indeed we find precursors for the tantrika worship in the वैखानस system. The deployment of unique vedic mantra-s exclusively in the वैखानस temple worship suggests that these actual belong to the phase of worship of images in the oldest temples, the chaitya-s, which were present before the nagna and the tathagata made their advent. While the extant वैखानस mantra prashna might show some evidence for latter tampering, the core of it is archaic. The prashna-s 1-4 (वैखानस saMhitA) are mantra-s for the गृह्य rituals and parallel the बोधायन and Apastamba mantra prashna-s of other तैत्तिरीयक-s and the mantra ब्राह्मण of the सामवेदिन्-s. The prashna-s 5-8 are devoted to the iconic worship of विष्णु, his गण-s and other deva-s installed in the temple. The proper understanding of the temple worship using these mantra-s requires a knowledge of the saMhitA traditions of the वैखानस-s attributed to atri, भृगु, kAshyapa and मरीचि. I had the chance to study a ritual hand book compiled by a great medieval वैखानस ritualist known of नृसिम्ह वाजपेयिन्. This lucidly provides the core of temple worship in a very succinct form. The author is reputed by tradition to have been a spectacular triple scholar who was simultaneously well-versed in वैखानस temple ritual, vedic shrauta rituals (he performed the great वाजपेय himself) and tantrika mantra शास्त्र of various kinds. In the below descriptions we shall follow his description of ritual injunctions and accompanying mantra-s.

The वैखानस is enjoined to recite vedic mantra-s, perform प्राणायाम, japa, ब्रह्मयज्ञ and vaishvadeva homa similar to a स्मार्त before performing any temple action. But the वैखानस is distinguished by the recitation of a unique very late vedic सूक्त, Atma सूक्त. In addition to his daily पारायण with the Atma-सूक्त the वैखानस ritualist also deploys the same सूक्त just before he prepares to invoke विष्णु into the movable image.

Atma सूक्त:\
[आत्मात्मा परमान्तरात्मा मह्य्-अन्तरात्मा\
यश् चातिरात्मा]{style="color:#99cc00;"} \[drAviDa पाठ; आन्ध्रपाठ= चादिरात्मा] [sa tu]{style="color:#99cc00;"} [ some reciters= sa tano ] [no .antarAtmA\
व्यावेष्टि विश्व(ग्)ं सकलं बिभर्ति\
यो व्यक्त-पुण्यस् स-तुनः प्रधानः ॥ १ ॥\
प्राणः प्रणीतिस् स उदान आदिर्\
vara-do वराहो व्यानश् cha me syAt\
तपसाञ् च मूर्तिः कपिलो मुनीन्द्रो\
यश् चापानो हयशीर्षो नः ॥ २ ॥\
yat sarvam अश्नात्य् ajaras samagra(g)M\
shriyam Urja-युक्तां sa tu me samAnaH\
बलम् आसुरं यत् सततं निहन्ता\
ब्रह्मा बुद्धिर् मे गोप ईश्वरः ॥ ३ ॥\
सविता च वीर्यम् इन्दुश् च धातु\
रस-भूतम्-भूता भूतास् स-भूताः\
द्यौर् मे अस्तु मूर्धोदर-नाभो वा\
भूमिर् यथाङ्घ्रिर् ववृधे ऽहम् ईशः ॥ ४ ॥\
अस्थीनि मे स्युर् अथ पर्वताख्या\
भुजगाश् च केशा दिवि ये चरन्तः\
द्वौ नेत्र-रूपौ विथु पृश्नि मुख्यौ\
रुधिरं cha sAra(g)M सकलं cha toyam || 5 ||\
स्नायवो मे आसन् नदीर् भृगुर् मे हृदयम् अस्तु\
सर्वे अन्ये मुनयो ऽङ्ग-भूताः\
वेदा मे आस्यं जिह्वा मे सरस्वती\
दन्ता मरुत उपजिह्वा उपश्रुतिः ॥ ६ ॥\
वृषणौ मित्रा-वरुणाव् उपस्थः प्रजा-पतिर्\
आन्त्रा मे वेदाश् श्रुति-स्मृती मेदाधारणे\
स्वेदम् मे वर्षं मूत्र कोश(ग्)ं समुद्रं\
पुरीषं काञ्चनम् ॥ ७ ॥\
सावित्री गायत्री मर्यादा वेदी [वेदे इन् चेर्तैन् मनुस्च्रिप्त्स्]\
हृत्-पुण्डरीके विमले प्रविष्टस्\
सकलस् स-लक्ष्मीस् स-विभूतिकाङ्गो\
यत् सर्वं पुण्यं मय्य् अधिष्ठानम् अस्तु ॥ ८ ॥\
सर्वेषां देवानाम् आत्मकस् सर्वेषां\
मुनीनाम् आत्मकस् तपो-मूर्तिर् इह पुण्य-मूर्तिर् आसन् ॥ ९ ॥]{style="color:#99cc00;"}

An examination of the आत्मसूक्त shows that there is a mix of typical vedic and classical features suggesting its composition towards the end of the vedic productivity, like the नारायण सूक्त of the तैत्तिरीय-s: The accents show some differences between the reciters from the drAviDa and Andhra countries. Further, some accents are not consistent with the vedic accents just as in the case of late vaiShNava material in the तैत्तिरीय AraNyaka. There are words typical of the classical language such as सततं and shruti-स्मृति. Yet it shows vedic elements -- the underlying Chandas in large part is a त्रिष्टुभ् (I have arranged it to illustrate that). Importantly, it seems to have no mention of the vibhava-s or अवतार-s of the सात्वत system such as vAsudeva and his व्यूह-s. Instead it mentions only 3 अवतार-s explicitly: वराह -- identified with व्यान; kapila with व्यान; हयशीर्ष with अपान. Some modern वैखानस आचार्य-s state that the phrase "yat sarvam अश्नात्य् ajaras samagra(g)M" actually refers to नृसिंह. The only avatara-s that find mention in the floral worship of icons of various devata-s in the inner and surrounding shrines of a वैखानस temple, according to the core ritual are: kapila, वराह, नृसिंह, वामन/trivikrama and हयात्मक (हयशीर्ष) (the penultimate one having two separate images). There is no mention of the दशावतर-s in the core वैखानस daily ritual manuals. They are only found in the भृगु saMhitA's क्रियाखाण्ड. This suggests that the Atma-सूक्त preceded the classic पौराणिc period and was possibly also before the सात्वत system. It also suggests that original अवतार system as recognized by the वैखानस-s only had the above 5 अवतार-s. Of these it is not clear if kapila was originally considered an अवतार or merely a respected muni of the early वैखानस tradition.. It is also possible नृसिंह, वामन and trivikrama are considered among the 8 vidyeshvara-s of the वैखानस system -- indicated in the invocatory formula in the VMP: 5.1.139.9. These vidyeshvara-s might be an early attestation of a class of deities whose cognates were also adopted by the shaiva सैद्धान्तिक tantra-s most probably from their पाशुपत precursors.

The main function of the आत्मसूक्त appears to be the identification of the officiant with विष्णु in his cosmic form. This cosmic form of विष्णु is implied by the identification of विष्णु with the puruSha -- the puruSha सूक्त is recited after the आत्मसूक्त in वैखानस practice. In addition to the आत्मसूक्त the वैखानस-s have the ब्रह्मन्यास rite which serves an identical purpose and is performed before bathing the image of विष्णु. The ब्रह्मन्यास is not found in the वैखानस गृह्य sUtra. It is mentioned in a rather abbreviated form relative to the modern performance in the original saMhitA-s of atri, भृगु, kAshyapa and मरीचि. In modern performance is shows several तान्त्रिक elements that are found nowhere in the core saMhitA-s which instead use distinctive वैखानस-specific mantra-s. This suggests that brahma-न्यास was perhaps specifically derived with the emergence of the वैखानस temple ritual. It might have inspired the early evolution of tantrika mantra-मार्ग and has subsequently been overrun by laterally transferred elements of the तान्त्रिक mantra मार्ग.

The core brahma-न्यास comprises of अङ्ग-न्यास, bIja-dhyAna, kara-न्यास and ब्रह्मैक्वत्वं. While the modern performances have the usual formulae of हृदयाय नमः etc. there is no evidence they were used in the old वैखानस ritual. In fact they do not seem to appear in any of the old saMhitA-s, which only specify the basic mantra-s for each न्यास.\
हृदय (touching of heart): [brahma ब्रह्मान्तरात्मा brahma-पूतान्तरात्मा brahmaNi brahma-निष्ठो brahma-gupto gupto .aham asmi ||]{style="color:#99cc00;"}\
shiras: [द्यौर् द्यौर् असि सर्व इमे प्राणा स्थाणव आसन् ॥]{style="color:#99cc00;"}\
शिखा: [शिखे उद्वर्तयामि स-वेदास् स-मन्त्रास् सार्षास् स-सोमा देवाः परिवर्तयन्ताम् ॥]{style="color:#99cc00;"}\
kavacha: [देवानाम् अयुधैः परिबाधयामि ब्रह्मणो मन्त्रैर् ईशस्योजसा भृगूणाम् अङ्गिरसां तपसा सर्वम् आतनोमि ॥]{style="color:#99cc00;"}\
\[This kavacha formula might be translated approximately as: By the deva-s weapons I block \[the attackers], by the brahman power of mantra-s, by the manly strength of Isha(i.e. rudra) and by the heat of the भृगु-s and अङ्गिरस-s I pervade every thing. The last phrase is reminiscent of the yajurvedic incantation during the laying of the कपाल-s: [भृगूणां अङ्गिरसां तपसा तप्यध्वं]{style="color:#99cc00;"}.]\
astra:[नारायणाय विद्महे वासुदेवाय धीमहि । तन् नो विष्णुः प्रछोदयात् ॥]{style="color:#99cc00;"}\
[ s]{style="color:#99cc00;"}[उदर्शनम् अभिगृह्णामि । रविपाम् अभिगृह्णामि ॥]{style="color:#99cc00;"}\
दक्षिण netra: [सूर्यो ऽसि सूर्यान्तरात्मा चक्षुर् असि सर्वम् असि सर्वं धेहि ॥]{style="color:#99cc00;"}\
वाम netra: [चन्द्रो ऽसि यज्ञो ऽसि यज्ञायाधानम् असि यज्ञस्य घोषद् असि ॥]{style="color:#99cc00;"}\
Then the officiant carries out bIja-dhyAna by meditating upon the Adi-bIja: a\
Some वैखानस-s hold that the अकार bIja should be surrounded by OM-s -- this is found in the atri saMhitA and भृगु क्रियाधिकार. Then the ritualist performs करन्यास by invoking the देवता-s: आभुरण्य, vidhi, यज्ञं, brahmA, indra on the five fingers. Then he performs the brahmaikvatva dhyAna with the formula:\
[अन्तर् अस्मिन्न् इमे लोकाः अन्तर् विश्वं इदं जगत् । ब्रह्मैव भूतानां ज्येSठं तेन को ऽर्हति स्पर्धितुम् ॥]{style="color:#99cc00;"}\
This formula resembles the atharva vedic काण्ड तर्पण formula (AV-vulgate 19.22.21 and 19.23.30)

Now these वैखानस practices represent an early version of the eponymous rites performed in the tantrika system. But they can also be compared to two other rites: One earlier than all these rituals --- the vedic formulae for the identification of the body with the immortal देवता-s: the mantra-s beginning with "agnir me वाचिः श्रितः ..." (काठक 1.8.4-9). The second one is the shaiva ritual of न्यास, which in turns occurs as the short लघुन्यास and the long महान्यास. Such identifications of parts of the body with देवता-s are also seen in the medical saMhitA-s, like that of charaka. The लघुन्यास includes within it the vedic mantra-s beginning with agnir me वाचिः श्रितः. The modern लघुन्यास and महान्यास rites might be used in temples but there is no evidence to suggest that they were originally used thus. In fact they appear to be purely household rites for the worship of rudra by a यजमान and his family. We have no clear way of dating when these texts were composed relative to other texts either. However, tradition holds that there are two forms of महान्यास -- the तान्त्रिक version attributed to the rAkShasa रावण and the vaidika version attributed to बोधायन. The core of the महान्यास is entirely vaidika and so also the लघुन्यास and these न्यास-s are mainly performed by तैत्तिरीयक-s. We propose based on the parallels to the वैखानस ritual that these न्यास-s represent the proto-shaiva versions of their own न्यास-s. It is quite likely that this system of न्यास-s are also linked to the period of the emergence of the medical saMhitA-s when they identified parts of the body with deities and are the precursors of the classical तान्त्रिक न्यास-s. The तैत्तिरीय tradition may be seen as the nidus of their emergence. These early vaidika न्यास-s in turn emerged from the early shrauta ritual of संबन्ध with the agnir me वाचिः. But the early development of the temple ritual amongst the वैखानस-s resulted in these न्यास-s being incorporated into their larger system of public iconic worship.


