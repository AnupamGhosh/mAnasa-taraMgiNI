
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [vijayanagaran activities in श्री लन्का](https://manasataramgini.wordpress.com/2009/10/11/vijayanagaran-activities-in-shri-lanka/){rel="bookmark"} {#vijayanagaran-activities-in-शर-लनक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 11, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/10/11/vijayanagaran-activities-in-shri-lanka/ "6:46 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Since the vijayan colonization of लन्का several south India empires have extended their sway over लन्का. On occasions, the lankans too invaded or interfered with the mainland, most famous occasion being the invasion of पराक्रमबाहु. Due to some prominent historians underplaying or outright ignoring the vijayanagaran forays into लन्का, I too was misled into believing this was an unimportant facet of their military and cultural expansion. But an impressive collection of lankan gems that I had seen in the कर्नाट country from the vijayanagaran period kept coming to mind and culminated in more detailed look at the activities of vijayanagara in श्री लन्का. Further even the biased Islamic and Christian sources had recorded the activities of vijayanagara in लन्का, suggesting it was not something minor to be brushed aside.

  - कुमार कम्पण, the hero of Madhurai, the destroyer of the Army of Islam, had planned a large-scale invasion of श्री लन्का to bring it under vijyanagaran control. However, he died just before the invasion and it was called off. Nevertheless the lankan राजन् भुवनैकबाहु-V sent an embassy with gifts to maintain status quo with vijayanagara.

  - In 1385 CE विरुपाक्ष, was sent by his father harihara-II to bring लन्का to pay tribute. He invaded the island with an amphibious force and forced भुवनैकबाहु to accept vijayanagaran overlordship. He appears to have taken the state of Yazhpanam (Jaffna) and brought it under direct vijayanagaran rule and used it to exert pressure on भुवनैकबाहु. In support of this we note that a source mentioned by the learned historian SK Aiyangar, the नारायणीविलासं states that विरूपाक्ष set up a pillar of victory in लन्का. We note that around this time harihara-II took the grandiose title of पूर्व-pashchima-दक्षिण-समुद्राधीश्वर. Copper plates found at Alampundi in the drAviDa country state that विरूपाक्ष acting on behalf of his father conquered the सिंहल-s and brought gemstones for his father from there. The Portuguese sources also mention the conquest of लन्का by a hindu king whose name is hopelessly corrupted and state that the rulers of Jaffna were कर्नाट-s.

  - In 1424 CE देवराय-II came to power and appears to have strengthened the navy by appointing a special naval command under the नाविगड-prabhu. Initially, the naval force appears to have been active on the Andhra coast as देवराय dispatched ambitious naval expeditions against Burma. These invasions landed on the Pegu and Tenasserim coasts and forced the Mon kingdom to become a tributary of vijayanagara and possibly even under direct vijayanagaran control. In 1440 CE a conflict appears to have emerged over the lankan non-payment of tribute during देवराय-II's reign. As a consequence a lankan spice ship was captured and impounded by the vijayanagaran naval force. In response पराक्रमबाहु-II hoping to relive the memories of eponymous predecessor launched a naval expedition against the mainland and plundered Adhirampattinam. In response, देवराय sent his महाप्रधानि (prime minister) लक्ष्मण-दण्डनायक to punish the lankans. In 1442 CE he reconquered Adhirampattinam and launched an invasion into लन्का. The lankans were defeated in battle fought south of Jaffna in 1443CE. Originally लक्ष्मण intended to completely conquer लन्का and bring it under direct vijayanagaran rule. However, the alarming news of the great Jihad being launched deep into vijayanagaran territory by the Bahmanid sultan reached him just then. So he accepted the tributes of पराक्रमबाहु-II and returned to the capital to reorganize the army to fight the Moslem invasion. This is supported an inscription near Chengalapattu in the Tamil country that mentions देवराय having subjugated the सिंहल-s. Even an Islamic source (Abdur Razzak the Turk from Iran) and a Portuguese reports the same. Both these sources also report the subjugation of Pegu and Tenaserim in Southern Burma by देवराय. In the Burman accounts after the death of the king राजाधिराट् of Mons we notice that there was break before the king rAma-राजधिराट् takes the throne. This break corresponds to the vijayanagaran subjugation of the Burman coast, probably taking advantage of the internal conflict there.

  - With the death of देवराय-II the advancing Islamic Jihad pulverized the Vijayanagarans and their empire began shrinking rapidly even as they fought to barely stave off the annual jihads of the Bahmanids. In this phase Lanka had entirely slipped out of Vijayanagaran control.

  - Finally when Vijayanagara was revived under the great कृष्णदेवराय he decided revive the control of Lanka. For this he asked the powerful governor of Pudukotai, नरसिन्घरायर् to organize a force for looking into the Lankan affairs.

  - Around that time (1521CE) the सिंहल king विजयबाहु raised a cry for help as he was invaded by the Portuguese armada. The Christian terrorists asked him to convert to catholicism or face death and launched a massive attack on him. The सिंहल-s begged to the सामूथिरि of the chera country to come to his aid. In the past 20 years the सामूथिरि was raising a naval force to complement his army of skilled nAyar warriors to deal with the Christian threat from the west. The Christians had bombed Kodungallur in 1504CE and the सामूथिरि's navy had been engaged a fierce naval campaign in which he was facing the adverse edge of the Christian armada. To shore up his naval defenses the समूथिरि tried to form an alliance with the Osman Turk navy against the Christians. However, the Christians managed to make a landing at Kollam and demolished a shAstA temple and built a church atop it. But at this point the सामूथिरि deployed his force of elite nAyar warriors who made a clean sweep of the Portuguese fort at Kollam, exterminated the Christians and demolished their church. Soon after this strike the Christians signed a peace treaty with the Hindus to secure their trade in the west coast. But the king of Portugal sent an armada under Menezes to wage holy war on the Hindus. The Portuguese in a move to outflank the Hindus tried to conquer Lanka in an amphibious attack with several guns. With the call for help coming, कृष्णदेवराय who was the overlord of the सामूथिरि sent a force to aid in this battle against the Christians in Lanka. The सामूथिरि's navy and the vijayanagaran land force successfully thwarted the Portuguese attempt. However, soon after this there was an internecine war of succession in Lanka that resulted in the division of the land among the विजयबाहु's sons.

  - In 1525CE the Portuguese fleet decided to punish the chera-s for thwarting them at Lanka and set forth to attack Kozhikode. But the सामूथिरि had by then developed a sufficiently strong navy and won a major victory at sea against the Christians.

  - In 1533 CE taking advantage of the internecine war in Lanka the [general]{style="color:var(--color-text);"}[ from the ]{style="color:var(--color-text);"}[Tamil division of the empire नरसिन्घरायर् invaded Lanka and made it a tributary of कृष्णदेव rAya. These activities are supported by the Pudukotai inscription of नरसिन्घरायर् and the Piranmalai inscription mentioning कृष्णदेव sending a force during the time of the Portuguese invasion attempt.]{style="color:var(--color-text);"}

  - In 1564 CE before setting forth to face the Moslems in that fateful encounter रामराय dispatched a force of 20000 men under कृष्णप्पा नायक of Madhurai to bring Lanka under direct vijayanagaran control. He routed the Lankan king and also a large Portuguese force that tried to intervene and claim to be legitimate rulers of Lanka. He deposed the सिंहल राजन् and placed his brother vijaya गोपाल नायक as the viceroy of लन्का. But the victory was to be last flash of the greater Vijayanagara as Hampi itself was destroyed by the Mohammedans a few months later.

  - 1567CE vIra वसन्तराय, vijayanagaran general was still trying to enforce the empire's rule in Lanka, but with the empire facing an existential struggle against the army of Islam this hold was soon lost.


