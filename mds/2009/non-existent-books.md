
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Non-existent books](https://manasataramgini.wordpress.com/2009/02/07/non-existent-books/){rel="bookmark"} {#non-existent-books .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 7, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/02/07/non-existent-books/ "7:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Two books on arthropods have set standards for me that in a sense inspire but also cause despair. These are the "For Love of Insects" by the great Thomas Eisner and "Evolution of the Insects" by Grimaldi and Engel. If I were to live long enough, production of analogs of these works would mean that the janma has been completed as a Hindu's would say. But each of these has its own challenges: An Eisnerian tome implies the ability to write engaging prose in some language, which given my relatively under-developed faculty of language, seems a daunting climb. Grimaldi and Engel's volume is characterized by the clarity of illustrations -- something I believe to be essential for any biological work. The illustrations here are not only beautiful but also clear. Recently Holtz and Rey produced a comprehensive book on the Dinosauria. This book while having pretty pictures is not necessarily matched by clarity of the scientific illustrations, like those seen in Paul's works on dinosaurs. Who will be my illustrator, especially given my aesthetic obsessions? In any case, Grimaldi and Engel is a good model for my own imagined tome, because it too would deal with evolution and abound in phylogenetic depictions and illustrations of the diversity of the objects of my life's study. A book I realized should inspire at first sight -- just like a स्त्री with the right endowments can instantly inspire काम even in a self-controlled man. However, unlike the majority of such स्त्री-s who turn into राक्षसी-s upon संयोग, the book upon samyoga should inspire even greater thoughts. This is precisely what these two entomological tomes achieve.

Of course that is not all -- there is also the wish that we produce a work on the evolution of the mantra शास्त्र. For this there is no model yet. However, scanning publications on Sanskritic material over the ages good ideas and standards that are worthy of emulation come up: 1) The स्थालिपाक by Kolhatkar and Tachikawa, following the model of Staal's first volume of agni, and the यज्ञ पात्र-s produced by the Tilak vaidika संशोदन मण्डल (good model) in the city of my youth are models for depicting actual rites and ritual material photographically. H.G. Ranade's glossary of the shrauta ritual is also a decent model for the glossary at the end of the book. 2) The works on tantric texts, like the dissertation publications of S.Vasudeva and E.English are good models in terms of presentation. The former sets high standards technical soundness, which would be hard to achieve for most. The latter does a good job at illustrating mantra-deities (as an aside I would recommend E.English's work on the nAstika tantra-s for I was surprised to see that it discusses विलासिनी in the nAstika tantrika tradition. That account can be compared with my own study of the same presented here). Illustrations are very important even here in the proposed tome on the mantra-शास्त्र -- one needs a good artist to produce images of the mantra deities, yantra-s and mudra-s. A pop book "The tantric way" by Mookherji and Khanna has good illustrations but is pretty unsound in its text.

How can all this be achieved? This was the question we kept pondering about.


