
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सङ्केता](https://manasataramgini.wordpress.com/2009/07/17/sanketa/){rel="bookmark"} {#सङकत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 17, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/07/17/sanketa/ "6:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2644/3728932256_9445216214.jpg){width="75%"}
```{=latex}
\end{center}
```



OM sarva-डाकिनी-आवृतायै-सङ्केतयोगिन्यै नमः

