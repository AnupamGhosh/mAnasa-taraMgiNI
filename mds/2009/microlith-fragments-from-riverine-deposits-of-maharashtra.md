
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Microlith fragments from riverine deposits of महाराष्ट्र](https://manasataramgini.wordpress.com/2009/12/12/microlith-fragments-from-riverine-deposits-of-maharashtra/){rel="bookmark"} {#microlith-fragments-from-riverine-deposits-of-महरषटर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 12, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/12/12/microlith-fragments-from-riverine-deposits-of-maharashtra/ "6:13 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2666/4179314974_bcaa0bc668.jpg){width="75%"}
```{=latex}
\end{center}
```

\
The above are from the deposits of a tributary of the मूलक-गोमूत्रक (modern Mula-Mutha) that we picked up near Bhuleshvara:\
Row 1- recent and sub-fossil shells, and pebbles from the deposit bearing the microliths\
Row 2- flint cores.\
Row 3- fragments of agate microliths.\
Row 4- flakes produced during microlith manufacture.\
These blades appear to have been used with wooden hafts and stylistically belong to the middle of the temporal range of microlith manufacture which appears in India even before 30k years and continues as a background within chalcolithic sites of the Daimabad/Jorwe cultures. From a brief prospecting I hold these deposits contain a lower bound of 20 blades per cubic meter of deposit -- these fellows were enormously productive.


