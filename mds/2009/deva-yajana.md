
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [deva yajana](https://manasataramgini.wordpress.com/2009/06/14/deva-yajana/){rel="bookmark"} {#deva-yajana .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 14, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/06/14/deva-yajana/ "6:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We wandered like a bhogin in rati-given pleasure,\
Like a brahma-रक्ष we roamed, neither here nor there,\
We became a कपालिन् reciting the mantra-s of the 5 faces.\
On one side was life with a path leading to glory,\
On the other was the southern road, bloody and miry,\
The wise know the decision at the fork to not be ours.

Hoping not to become the target of the god's arrow we offered with the yajus of नाभानेदिष्ठा मानव:\
[एष ते रुद्र भागो यं निरयाचथास्तं जुषस्व विदेर्गोपत्यग़्ं रायस् पोषग़्ं सूवीर्यग़्ं संवत्सरीणाग़् स्वस्तिं ॥]{style="color:#99cc00;"}

The mixed soma that remains is the bhAga of rudra.\
Wandering but performing the soma याग in the mind, we said soma mixed with barley is that of the thunderer:\
[दिवः पृथिव्याः पर्यन्तरिक्षाथ् स्वाहा कृतम् इन्द्राय तं जुहोमि ।]{style="color:#99cc00;"}|

This the act of यज्ञ. We make over the यज्ञ to him who is the ya\~jna, meditating on the soma to swell. He who covers the worlds with his strides reverence:\
[विष्णो त्वं नो अन्तमः शर्म यच्छ सहन्त्य । प्रते धारा मधुष्चुत उथसं द्रुहते अक्षितं ॥]{style="color:#99cc00;"}

With the याग performed in our minds as भृगु did, as अप्नवान did, and as aurva did. He who does so experiences the वृष्टि of the marut-s whose call is the maithuna of eternal bhoga:\
[उदप्रुतो मरुतस्ताग़्ं इयर्त वृष्टिं ये विश्वे मरुतो जुनन्ति । क्रोशाति गर्दा कन्येव तुन्ना पेरुं तुञ्जाना पत्येव जाया ॥]{style="color:#99cc00;"}

Then even as the भृगु-s drove away makha he reverences the gods and absorb the luster of makha:\
[नमो ऽग्नये मखग्ने मखस्य मा यशो ऽर्यादित्य ।\
नमो रुद्राय मखग्ने नमस्कृत्या मा पाहि ।\
नम इन्द्राय मखग्न इन्द्रियं मे वीर्यं मा निर्वधीः ॥]{style="color:#99cc00;"}

He casts the spell saying:\
[प्रतूर्वन्-नेह्य्-अवक्रामन्-नशस्तीः\
रुद्रस्य गाणपत्याण्\
पूष्णा सयुजा सह]{style="color:#99cc00;"}

He sees the truth with:\
[मित्रः सग़्ंसृज्य पृथिवीं ।\
रुद्राः संभृत्य पृथिवीं ।]{style="color:#99cc00;"}


