
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The accomodation of women and शूद्र-s in the पाशुपत system](https://manasataramgini.wordpress.com/2009/08/03/the-accomodation-of-women-and-shudra-s-in-the-pashupata-system/){rel="bookmark"} {#the-accomodation-of-women-and-शदर-s-in-the-पशपत-system .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 3, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/08/03/the-accomodation-of-women-and-shudra-s-in-the-pashupata-system/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As we noted before the पाशुपत tradition was originally strictly restricted to ब्राह्मण males who were educated in the veda-s. They were celibate ascetics who kept away from regular social interaction. But by the time of the वामन पुराण we note that the पाशुपत-s were accommodating शूद्र-s (VअP 6.92). This is suggested by the महाव्रतिन् dhanada having the शूद्र अर्णोदर as a disciple. From inscriptional evidence in the southern country we note that women and शूद्र-s are included amongst patrons of the पाशुपत मठ-s. Is there any other evidence for their accommodation? It is seen that secondary shloka texts, resembling the vedic texts to which the शूद्र-s had no अधिकार, were composed in the पौराणिc period when the शूद्र-s were being accommodated. These texts provide the same material as the vedic mantra-s on which they are modeled making their purport accessible to the 4th वर्ण. Let us consider some examples briefly that are provided by लक्ष्मीधर and the लिङ्ग पुराण.. The first of this is a stand in for the पञ्चब्रह्म-mantra-s. Only 3 brahma mantra-s are represented here. I discussed this with R1's father. He speculated that perhaps the बहुरूपी and rudra-gayatrI are genuinely vaidika and cannot be paraphrased as पौरानिc shloka-s. Alternatively, their paraphrase has been lost.

[*वन्देअम् देवम् ईशानं सर्वेशं सर्वगं प्रभुम् ।\
ओम् ईशान नमस्ते ऽस्तु महादेव नमो ऽस्तु ते ॥१॥\
नमो ऽस्तु सर्व-विद्यानाम् ईशान परमेश्वर ।\
नमो ऽस्तु सर्व-भूतानाम् ईशान वृष-वाहन ॥२ ॥\
ब्रह्मणो ।अधिपते तुभ्यं ब्रह्मणे ब्रह्म-रूपिणे ।\
नमो ब्रह्माधिपतये शिवं मे ऽस्तु सदाशिव ॥३ ॥\
ओङ्कार-मूर्ते देवेश सद्योजात नमो नमः ।\
प्रपद्ये त्वां प्रपन्नो ।अस्मि सद्योजाताय वै नमः ॥४ ॥\
अभवे छ भवे तुभ्यं तथा नातिभवे नमः ।\
भवोद्-भव भवेशान मां भजस्व महाद्युते ॥५ ॥\
वामदेव नमस् तुभ्यं ज्येष्ठाय वरदाय च ।\
नमो रुद्राय कालाय कलनाय नमो नमः ॥६ ॥\
नमो विकरणायैव कालवर्णाय वर्णिने ।\
बलाय बलिनां नित्यं सदा विकरणाय ते ॥७ ॥\
बलप्रमथनायैव बलिने ब्रह्म-रूपिणे ।\
सर्व-भूतेश्वरेशाय भूतानां दमनाय च ॥८॥\
मनोन्मनाय देवाय नमस् तुभ्यं महाद्युते ।\
वामदेवाय वामाय नमस्तुभ्यं महात्मने ॥९॥\
ज्येष्ठाय चैव श्रेष्ठाय रुद्राय वरदाय च ।\
काल-हन्त्रे नमस् तुभ्यं नमस् तुभ्यं महात्मने ॥१०॥*]{style="color:#99cc00;"}

Similarly the following shloka-s attributed to brahmA are a शतरुद्रीय substitute for the 4th वर्ण to use. It has high frequency of overlap with the names of rudra in the शत्ररुद्रीय and also includes an abbreviated paraphrase of the पञ्चब्रह्म:

[*नमस्ते कालकालाय नमस्ते रुद्र मन्यवे ।\
नमः शिवाय रुद्राय शङ्कराय शिवाय ते ॥१॥\
उग्रो ऽसि सर्व-भूतानां नियन्तासि शिवो ऽसि नः ।\
नमः शिवाय शर्वाय शङ्करायार्त्ति-हारिणे ॥२॥\
मयस्कराय विश्वाय विष्णवे ब्रह्मणे नमः ।\
अन्तकाय नमस् तुभ्यम् उमायाः पतये नमः ॥३॥\
हिरण्यबाहवे साक्षाद् धिरण्यपतये नमः ।\
शर्वाय सर्वरूपाय पुरुषाय नमो नमः ॥४॥\
सदसद्व्यक्ति-हीनाय महतः कारणाय ते ।\
नित्याय विश्वरूपाय जायमानाय ते नमः ॥५॥\
जाताय बहुधा लोके प्रभूताय नमो नमः ।\
रुद्राय नीलरुद्राय कद्रुद्राय प्रछेतसे ॥६॥\
कालाय कालरूपाय नमः कालाङ्गहारिणे ।\
मीढुष्टमाय देवाय शितिकण्ठाय ते नमः ॥७॥\
महीयसे नमस् तुभ्यं हन्त्रे देवारिणां सदा ।\
ताराय छ सुताराय तारणाय नमो नमः ॥८॥\
हरिकेशाय देवाय शंभवे परमात्मने ।\
देवानां शंभवे तुभ्यं भूतानां शंभवे नमः ॥९॥\
शम्भवे हैमवत्याश् च मन्यवे रुद्र-रूपिणे ।\
कपर्दिने नमस्तुभ्यं कालकण्ठाय ते नमः ॥१०॥\
हिरण्याय महेशाय श्रीकण्ठाय नमो नमः ।\
भस्मदिग्ध-शरीराय दण्डमुण्डीश्वराय च ॥११ ॥\
नमो ह्रस्वाय दीर्घाय वामनाय नमो नमः ।\
नम उग्र-त्रिशूलाय उग्राय च नमो नमः ॥१२॥\
भीमाय भीमरूपाय भीम-कर्मकृताय ते ।\
अग्रेवधाय वै भूत्वा नमो दूरेवधाय छ ॥१३॥\
धन्विने शूलिने तुभ्यं गदिने हलिने नमः ।\
छक्रिणे वर्मिणे नित्यं दैत्यानां कर्मभेदिने ॥१४॥\
सद्याय सद्य-रूपाय सद्योजाताय ते नमः ।\
वामाय वाम-रूपाय वामनेत्राय ते नमः ॥१५॥\
अघोर-रूपाय विकटाय विकटशरीराय ते नमः ।\
पुरुष-रूपाय पुरुषैक-तत्पुरुषाय वै नमः ॥१६॥\
पुरुषार्थ-प्रदानाय पतये परमेष्ठिने ।\
ईशानाय नमस् तुभ्यम् ईश्वराय नमो नमः ॥१७॥\
ब्रह्मणे ब्रह्मरूपाय नमः साक्षाच्छिवाय ते ॥५*]{style="color:#99cc00;"}

This strange mantra is believed to be a rudra gAyatrI substitute althought it does not retain meter and has a grammatically strange form as it stands:\
*[ईशानाय कद्रुद्राय प्रचेतसे त्र्यंबकाय शर्वाय तन्नो रुद्रः प्रचोदयात् ॥]{style="color:#99cc00;"}*


