
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A mantra fossil from the vedo-tAntric transition in charaka](https://manasataramgini.wordpress.com/2009/10/16/a-mantra-fossil-from-the-vedo-tantric-transition-in-charaka/){rel="bookmark"} {#a-mantra-fossil-from-the-vedo-tantric-transition-in-charaka .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 16, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/10/16/a-mantra-fossil-from-the-vedo-tantric-transition-in-charaka/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The चिकित्सास्थान of the charaka saMhitA cites a formula to be recited during the compounding of the agada termed the महागन्धहस्ति (23.90):\
[*मम माता जया नाम जयो नामेति मे पिता ।\
सोहं जयजयापुत्रो विजये।अथ जयामि छ ।\
नमः पुरुषसिंहाय विष्णवे विश्वकर्मणे ।\
सनातनाय कृष्णाय भवाय विभवाय च ।\
तेजो वृषकपेः साक्षात् तेजो ब्रह्मेन्द्रोयोर् यमे ।\
यथा अ।हं नाभिजानामि वासुदेवपराजयं ।\
मातुश् च पाणिग्रहणं समुद्रस्य च शोषणं ।\
अनेन सत्यवाक्येन सिद्ध्यताम् अगदो ह्य् अयम् ।\
हिलि मिलि संस्पृष्टे रक्ष सर्व भेषजोत्तमे स्वाहा ॥*]{style="color:#99cc00;"}

The agada is a preparation that it specially used to kill or counter a disease-causing agent or poison and is contrasted from other drugs which help to provide nutrients or enhance particular functions. This महागन्धहस्ति formula is interesting in several ways. In its opening part it has a peculiar formulaic structure of X is the mother, Y is the father etc. This is a very old atharvanic formula and is seen right from an early atharvanic spell in the ऋग्वेद (RV10.97.9; इष्कृतिर् nAma vo माता) and similar forms in the atharvaveda. Interestingly, it is praised thus:\
[*यत्र च सन्निहितोऽयं न तत्र बालग्रहा न रक्षांसि ।\
न छ कार्मण वेताला वहन्ति नाथर्वण मन्त्राः ॥ (२३।८८)*]{style="color:#99cc00;"}*

  -  When it is applied or placed \[on a person] it is supposed to protect against बालग्रह-s, रक्ष-s, अभिचार rites, वेताल-s and atharvanic mantra-s. Thus, its structure and its "target", i.e. the AV mantra-s suggest that it belongs to a relatively early period, prior to the ascendancy of the classical तान्त्रिक material. Yet on the other hand we encounter elements that link it to the तान्त्रिक mantra शास्त्र such as the formula "hili mili". Moreover, we note that while it invokes a wide range of deities (i.e. विष्णु, bhAva=rudra?, वृषकपि, brahma, indra and yama), the most prominent deity here is विष्णु. Specifically his vibhava-s such as पुरुषसिंह (नृसिंह) and कृष्ण are mentioned and perhaps even वृषकपि here is identified with विष्णु. Also mentioned is vAsudeva, a name not found in the earlier vaidika layer of the vaiShNava शासन, but emerging in the वैखानस and पाञ्चरात्र traditions. There is hardly any mention of नृसिंह in the great epic which already contains early allusions to पाञ्चरात्र and वैखानस. However, नृसिंह is prominent alongside with vAsudeva and the व्यूह-s in the वैखानस mantra-पाठ. Hence, we suspect that this mantra from charaka belongs to the same layer as the vaiShNava mantra material of the वैखानस mantra-पाठ, which probably post-dates their precursors alluded to in the great epic. The mantra also preserves an old style Indo-European oath statement (सत्यवाक्य) albeit with a vaiShNava statement --- i.e. just as I do not know of the defeat of vAsudeva, and just as one does not take ones mother's hand in marriage and just as the ocean does not dry up so also let this drug be successful. This again suggests that the महागन्धहस्ति is a "fossil" mantra from the transitional period from the vaidika and tantrika mantra-शास्त्र-s along side other such examples we have previously pointed out, such as those of the AV परिशिष्ठ-s. This may also be significant in being one of the relatively early vaiShNava mantra-s to mention नरसिंह and gives a small window into the pre-classical mantra-s of the vaiShNava-शासन.


