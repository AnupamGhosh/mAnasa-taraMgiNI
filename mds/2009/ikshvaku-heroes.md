
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [इक्ष्वाकु heroes](https://manasataramgini.wordpress.com/2009/04/10/ikshvaku-heroes/){rel="bookmark"} {#इकषवक-heroes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 10, 2009]{.entry-date}](https://manasataramgini.wordpress.com/2009/04/10/ikshvaku-heroes/ "9:10 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3306/3429258565_ce66925ed5.jpg){width="75%"}
```{=latex}
\end{center}
```

\
My folks celebrating the इक्ष्वाकु heroes and their retinue.

