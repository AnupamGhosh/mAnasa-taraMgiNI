
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Residues of squares, sequence curiosities and parabolas galore](https://manasataramgini.wordpress.com/2018/06/19/residues-of-squares-sequence-curiosities-and-parabolas-galore/){rel="bookmark"} {#residues-of-squares-sequence-curiosities-and-parabolas-galore .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 19, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/06/19/residues-of-squares-sequence-curiosities-and-parabolas-galore/ "2:42 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**Squares and their residues**\
This is an exploration of number triangles in the same vein as some other such we have previously described . It resulted in some observations that seemed interesting to us. Some are perhaps trivial but some seem puzzling to us probably on account of our very meager mathematics.

Consider the triangle of squares  $T_s$ . The  $j^{th}$  row of  $T_s$  is comprised of squares of all integers  $1:j$ ; illustrated below is the tip of this triangle.

\begin{tabular}{|*{15}{r|}} \cline{1-1} 1 \\ \cline{1-2} 1 & 4 \\ \cline{1-3} 1 & 4 & 9 \\ \cline{1-4} 1 & 4 & 9 & 16 \\ \cline{1-5} 1 & 4 & 9 & 16 & 25 \\ \cline{1-6} 1 & 4 & 9 & 16 & 25 & 36 \\ \cline{1-7} 1 & 4 & 9 & 16 & 25 & 36 & 49 \\ \cline{1-8} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64\\ \cline{1-9} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64 & 81 \\ \cline{1-10} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64 & 81 & 100 \\ \cline{1-11} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64 & 81 & 100 & 121 \\ \cline{1-12} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64 & 81 & 100 & 121 & 144 \\ \cline{1-13} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64 & 81 & 100 & 121 & 144 & 169 \\ \cline{1-14} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64 & 81 & 100 & 121 & 144 & 169 & 196 \\ \cline{1-15} 1 & 4 & 9 & 16 & 25 & 36 & 49 & 64 & 81 & 100 & 121 & 144 & 169 & 196 & 225 \\ \cline{1-15} \end{tabular}

We then obtain the square residue triangle  $T_{sr}$  from it by following operation,  $T_{sr}\lbrack j,k\rbrack = T_{s}\lbrack j,k\rbrack \mod j$ . The first 15 rows of this triangle are shown below.

\begin{tabular}{|*{15}{r|}} \cline{1-1} 0 \\ \cline{1-2} 1 & 0 \\ \cline{1-3} 1 & 1 & 0 \\ \cline{1-4} 1 & 0 & 1 & 0 \\ \cline{1-5} 1 & 4 & 4 & 1 & 0 \\ \cline{1-6} 1 & 4 & 3 & 4 & 1 & 0 \\ \cline{1-7} 1 & 4 & 2 & 2 & 4 & 1 & 0 \\ \cline{1-8} 1 & 4 & 1 & 0 & 1 & 4 & 1 & 0 \\ \cline{1-9} 1 & 4 & 0 & 7 & 7 & 0 & 4 & 1 & 0 \\ \cline{1-10} 1 & 4 & 9 & 6 & 5 & 6 & 9 & 4 & 1 & 0 \\ \cline{1-11} 1 & 4 & 9 & 5 & 3 & 3 & 5 & 9 & 4 & 1 & 0 \\ \cline{1-12} 1 & 4 & 9 & 4 & 1 & 0 & 1 & 4 & 9 & 4 & 1 & 0 \\ \cline{1-13} 1 & 4 & 9 & 3 & 12 & 10 & 10 & 12 & 3 & 9 & 4 & 1 & 0 \\ \cline{1-14} 1 & 4 & 9 & 2 & 11 & 8 & 7 & 8 & 11 & 2 & 9 & 4 & 1 & 0 \\ \cline{1-15} 1 & 4 & 9 & 1 & 10 & 6 & 4 & 4 & 6 & 10 & 1 & 9 & 4 & 1 & 0 \\ \cline{1-15} \end{tabular}

**The rows of  $T_{sr}$ **\
If we look at each row of  $T_{sr}$ , we see a mirror symmetry upon excluding the last term which is always 0. Leaving out the final 0, the first and the last  $n$  elements of the  $j^{th}$  row are the squares  $1^2:n^2< j$ . Thus, the same squares  $1^2:n^2$  are found at the beginning of each row from  $j=n^2+1$  to  $j=(n+1)^2$ . After that a new square  $(n+1)^2$  gets added to this run of squares at the beginning and end of the row. The reason for this is rather obvious: All  $1^2:n^2< j$  will leave themselves behind as residues of the modulo operation with  $j$ .  $j^2$  will result in a residue in the form of the terminal 0 of that row. Then  $(j-1)^2, (j-2)^2 ... (j-n)^2$  for  $n^2< j$  will leave residues  $n^2:1^2$ , i.e., in the reverse order as the starting run. The middle part of each row between these square terms may or may not be square terms but will also symmetrically expand. The pattern of residues is unique for each row of  $T_{sr}$  but the basic symmetry of the parabolic ascent and descent is common to all. This is illustrated in Figure 1
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig1.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 1. Figure 1 shows 9 different rows of  $T_{sr}$ . The first row ---  $j$  divisible by 4; second row --- other numbers; third row --- primes. The successive values are alternately colored blue and red.*

We observe that for  $j$  divisible by 4 we get a supersymmetry, wherein each symmetric half of the row is further symmetric (Figure 1, row 1). All other  $j$  show a pseudo-supersymmetry (Figure 1, rows 2 and 3). All  $j$  also display a central parabolic region but its particular form varies from number to number.

**The columns of  $T_{sr}$ **\
Now, if we look at  $T_{sr}$  vertically, every column  $k$  starts with a 0. It eventually terminates in a continuous run of square terms with the value  $k^2$ . Thus, the first column terminates in a continuous run of 1s, the second column in a run of 4s, the third in a run of 9s, so on. In a given column, between the starting 0 and the first  $k^2$  term we observe several notable patterns:

 1.  Immediately after the starting 0 there is an ascending sequence of square terms. This becomes apparent from the second column, i.e.  $k=2$  onwards, where after 0 we get a 1. For  $k=3$ , the sequence is 1, 4 --- a new square term 4 is seen. 1,4 then continues as the initial ascending sequence of square terms till  $k=6$ . At  $k=7$ , we get a new square term 9; thus yielding the ascending sequence of square terms 1, 4, 9, which continues till  $k=12$ . At  $k=13$ , we get next square term 16, yielding the ascending sequence of square terms 1, 4, 9, 16. Thus, we derived the general formula for the number of terms  $n$  in this ascending run of square terms as:

 $$n=\textrm{round}(\sqrt{k})$ ; with the sequence of square terms being 1, 4, 9...  $\left(\textrm{round}(\sqrt{k})\right)^2$$ 

Thus, the new square term gets added when  $k=n^2-n+1$ , i.e. at 1, 3, 7, 13, 21, 31, 43, 57, 73, 91... (see below for more on this sequence)


 2.  The next pattern is more complicated. Starting from the largest and last square term of the initial ascending run of square terms of the column  $k$  (described above), we start seeing a series of descending runs, until we hit a terminal 0, which precedes the concluding continuous sequence of square terms  $k^2$ . To understand this let us examine the column  $k=7$  of  $T_{sr}$ . Below are its first 46 terms:\
0, 1, 4, 9, 5, 1, 10, 7, 4, 1, 15, 13, 11, 9, 7, 5, 3, 1, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, 49, 49, 49\
The first term is the starting 0. Terms 2, 3 and 4 are the 3 ascending square terms 1, 4, 9. Terms 44:46 are the first 3 of the terminal continuous run of square terms  $k^2=49$ . In between are the descending runs that are under consideration. From term 4 which is 9 we descend to 5 and then to 1 (terms 5 and 6 of above sequence); this is a descent by 4. Then we have 10, 7, 4, 1, which is a descent by 3. Then we have 11, 9, 7, 5, 3, 1, which is a descent by 2. Then we have 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0, which is a descent by 1. For all columns we have a descent by 1. For higher  $k$  we see the addition of new runs of descent by 2, 3, 4 ,so on. Thus, for column  $k=7$ , we have descents by 4, 3, 2, 1.


 3.  We notice that in  $T_{sr}$ , for a column  $k$ , these descents by 1, 2, 3... begin with a certain number that keeps increasing. The descents by each number continue till they hit 0 or the positive integer nearest to 0, which can be attained via such a descent. Thus, the descent by 1 sequence will always terminate in a 0. However, as we saw above, for  $k=7$ , the descent by 2 starts with 11; so the integer nearest to 0 that the run can reach is 1. Now if we look at the descent by 1 sequence, the first column,  $k=1$ , has just 0 before the continuous run of 1s; so we can take the start of the descent by 1 as 0. For  $k=2$  the descent by 1 begins with 1; for  $k=3$  it begins with 4; for  $k=4$  it begins with 7; for  $k=5$  it begins with 12. Similarly, we can identify the starting integers with which the descent by 2, 3, 4... begin for successive columns of  $T_{sr}$ . This is shown in table 1.

\begin{tabular}{|l|r|r|r|r|r|r|r|r|r|} \hline k \textbackslash d & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 \\ \hline 1 & 0 & & & & & & & & \\ 2 & 1 & 0 & & & & & & & \\ 3 & 4 & 1 & 0 & & & & & & \\ 4 & 7 & 4 & 1 & & & & & & \\ 5 & 12 & 7 & 4 & & & & & & \\ 6 & 17 & 10 & 6 & 4 & & & & & \\ 7 & 24 & 15 & 10 & 9 & & & & & \\ 8 & 31 & 20 & 13 & 12 & 9 & & & & \\ 9 & 40 & 25 & 18 & 13 & 11 & 9 & & & \\ 10 & 49 & 32 & 22 & 16 & 15 & 10 & 9 & & \\ 11 & 60 & 39 & 28 & 21 & 16 & 13 & 9 & 9 & \\ 12 & 71 & 46 & 33 & 28 & 19 & 18 & 11 & 8 & 9 \\ \hline \end{tabular}\
Table 1. The starting integer of each descent run by  $d=1, 2, 3...9$  (horizontal) for the first 12 columns of  $T_{sr}$  (vertical) are shown.

Thus, we get sequences of the starting integers of the descent by a given  $d$  in the successive columns of  $T_{sr}$ :\
 $d=1$ ; 0, 1, 4, 7, 12, 17... After the starting 0 here successive terms differ by 3, 3, 5, 5, 7, 7...\
 $d=2$ ; 0, 1, 4, 7, 10, 15... After the starting 0 here successive terms differ by 3, 3, 3, 5, 5, 5,7,7,7...\
 $d=3$ ; 0, 1, 4, 6, 10, 13...\
So on, as in Table 1. For the first three of these sequences, i.e. the starting integers of the descent by 1, 2 and 3 in the successive columns of  $T_{sr}$ , we were able to derive general formulae for the  $n^{th}$  term of these sequences:

 $$d=1; \; \left \lfloor \dfrac{n^2}{ 2} \right \rfloor+n$$ 

 $$d=2; \; \left \lfloor \dfrac{n(n+2)}{3}\right \rfloor-1$$ 

 $$d=3; \; \left\lfloor\dfrac{n(n+6)}{4}\right \rfloor$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig2.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 2. The number of edges in the graph are shown above it in red . The number of unit square cells in the graph are shown in blue below it.*

Remarkably, for the first of these sequences,  $d=1$ , we were able to find a clear geometric interpretation. It corresponds to the number of edges found in the alternating square-rectangle graph, which is formed by extending the previous element by a unit (Figure 2, panel 1). This sequence has been described in OEIS as appearing in several surprising contexts relating to packing of shapes and beyond. However, to our knowledge this is the first time it has been found emerging in the triangle  $T_{sr}$ . The second of these sequences corresponding to  $d=2$  may also be interpreted as the number of edges in a growing graph of unit squares (Figure 2, panel 2), but describing the growth pattern is less simple than what we see for the case of  $d=1$ . One can imagine that these sequences provide analogies for the growth of cell-layers under certain constraints. We are yet to find a clear geometric interpretation for the case of  $d=3$ . We have not yet been able to find general formulae for  $d \ge 4$ . It is something that mathematicians might be able to find.


 4.  Then we ask the question as to how many terms the total set of runs of descent in given column  $k$  of  $T_{sr}$  occupy. For  $k=1$ , it is obviously 0. For  $k=2$  the terms are 1,0; thus, the number of terms is 2. For  $k=3$ , the terms are 4, 3, 2, 1, 0; thus the number of terms are 5. For  $k=7$ , which we saw above, the total number of terms occupied by the runs of descent is 40. We derived the general formula for the total number of terms in the runs of descent for a column  $k$ :

 $$k^2-k-\textrm{round}(\sqrt{k})+1$$ 

This to our knowledge is a novel sequence whose significance in the context of  $T_{sr}$  has not been previously described.


 5.  From the above results it also becomes apparent that the total number of terms in a column  $k$  of  $T_{sr}$  before the terminal continuous run of square terms  $k^2$  is given by formula  $k^2-k+1$ . This sequence 1, 3, 7, 13, 21, 31, 43, 57, 73, 91, 111 ... is another famous sequence with a clear geometric significance. It is the number of cycles that exist in a pyramid graph (also known as a wheel graph). It has many other interesting properties (see OEIS), but to our knowledge, this is the first report of its occurrence in the context of  $T_{sr}$ .

Thus,  $T_{sr}$  is the mother of many interesting sequences that show up in other contexts with various geometric/topological implications.

**The overall structure of  $T_{sr}$ **
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig3.png){width="75%"}
```{=latex}
\end{center}
```$  to reveal the internal structure more clearly.*

The above-described row-wise and column-wise patterns in  $T_{sr}$  combine to give rise to a further intricate structure that becomes apparent if we visualize  $T_{sr}$  graphically by plotting each cell of the triangle in a different color (Figure 3). We see a central family of nested conics along the median of the triangle. This is flanked by further such curves along the medians of the two triangles formed by the median of  $T_{sr}$  and also their flanks. A mathematician might be able to provide the equations to describe these curves in  $T_{sr}$ .

Finally, if we take  $T_{sr}\lbrack j,k\rbrack$  up to a certain value of  $j, k$ , we can ask what are the frequencies of the various integers in it (Figure 4) ?
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig4.png){width="75%"}
```{=latex}
\end{center}
```

 values among the non-squares are marked.*

It is immediately apparent that the perfect squares occur at much higher frequencies than the non-squares. Why this is the case can be generally understood from the above discussion on the rows and columns of  $T_{sr}$ , which over-represent perfect squares. The frequencies of successive perfect squares decrease almost linearly (Figure 4, fit line). Among the non-squares, we observe that unusually low and unusually high frequencies are often seen for certain numbers just before or just after the perfect squares (Figure 4). The exact basis of this pattern is in need of an explanation.

**The linearization of  $T_{sr}$ **\
Another way to examine the structure of  $T_{sr}$  is to linearize it. This is done by concatenating successive rows to generate the sequence  $f$ . The first few terms of  $f$  are shown below:\
0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 4, 4, 1, 0, 1, 4, 3, 4, 1...\
We then plot  $f$  such that the first term is placed at 0 with  $f\lbrack n\rbrack$  being y-coordinate and  $n$  being the x-coordinate (Figure 5).

*[![Square_mod_Fig5](https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png){.alignnone .size-full .wp-image-11256 attachment-id="11256" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="Square_mod_Fig5" large-file="https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png" medium-file="https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png" orig-file="https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png" orig-size="1200,850" permalink="https://manasataramgini.wordpress.com/2018/06/19/residues-of-squares-sequence-curiosities-and-parabolas-galore/square_mod_fig5/" loading="lazy" sizes="(max-width: 1200px) 100vw, 1200px" srcset="https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png 1200w, https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png 150w, https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png 300w, https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png 768w, https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png 1024w"}](https://manasataramgini.files.wordpress.com/2018/06/square_mod_fig5.png)Figure 5. A plot of  $f$  with successive terms alternately colored red and blue. The bounding parabola (green) and the primary parabolic attractors (pink) are plotted over  $f$ .*

Given that  $f$  is created by linearizing a triangle it is bounded by a parabola (Figure 5) of the form:

 $$y=\dfrac{-1+\sqrt{1+8x}}{2}$$ 

Under this parabola the plot of  $f$  reveals a complex pattern, which is clearly non-random. There are curves, which might be termed attractors, on which the points tend to occur more frequently than elsewhere:

 1.  The most obvious attractors are straight lines of the form  $y=k^2$ ; where  $k \in \mathbb{N}$  (Figure 5).\
This relates to the preponderance of square terms in  $T_{sr}$  (Figure 4).


 2.  The next most visible attractors are the parabolic arcs that start from the bounding parabola and run towards the x-axis (Figure 5). We term these the primary parabolic attractors. The coordinates of the starting points of  $n^{th}$  parabolic attractor, (  $x\lbrack n\rbrack , y\lbrack n\rbrack$ ) are specified by the following algorithm:

Let  $r_1\lbrack j\rbrack = j^2;\; j=1, 2, 3...$ 

 $$r_2\lbrack k\rbrack =\frac{r_1\lbrack j\rbrack +r_1\lbrack j+1\rbrack }{2}$$ 

 $$r=\textrm{sort}(r_1,r_2)$$ 

 $$y\lbrack n\rbrack =2r\lbrack n\rbrack$$ 

 $$x\lbrack n\rbrack =y\lbrack n\rbrack \cdot r\lbrack n\rbrack$$ 

The  $n^{th}$  primary parabolic attractors themselves are defined by the equations of the form:

 $$y=a\lbrack n\rbrack -\sqrt{bx-c\lbrack n\rbrack }$$ 

We empirically determined that  $b=1.89$ .

 $$a\lbrack n\rbrack =\left \lceil \left( n+\frac{2}{3} \right)^2 \right \rceil$$ 

Thus,  $a$  is a sequence whose first few terms are: 3, 8, 14, 22, 33, 45, 59, 76, 94, 114, 137, 161, 187, 216, 246, 278, 313, 349, 387, 428, 470, 514, 561.\
We were unable to derive a general formula for  $c\lbrack n\rbrack$ . However, we empirically determined the first few terms of  $c$  to be: 1, 25, 50, 100, 100, 200, 200, 400, 480, 600, 700, 1000, 1100, 1400, 1550, 2100, 2000, 2700, 2800, 3250, 3250, 4200, 3900.\
Perhaps a mathematician could derive the a general formula for sequence  $c$  and also provide a means of deriving the value of  $b$  from first principles.


 3.  Finally, a closer look reveals secondary attractors in the form of fainter parabolic arcs and lines. We have not attempted to derive their equations. A general scheme for identifying them would be of interest.

In conclusion the humble  $T_{sr}$ , which can be derived using elementary school mathematics, spawns some interesting patterns and sequences that might uncannily appear in other places.

