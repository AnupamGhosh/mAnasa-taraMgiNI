
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The hearts and the intrinsic Cassinian curve of an ellipse](https://manasataramgini.wordpress.com/2018/07/06/the-hearts-and-the-intrinsic-cassinian-curve-of-an-ellipse/){rel="bookmark"} {#the-hearts-and-the-intrinsic-cassinian-curve-of-an-ellipse .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 6, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/07/06/the-hearts-and-the-intrinsic-cassinian-curve-of-an-ellipse/ "6:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**Introduction**

This investigation began with our exploration of pedal curves during the vacation following our university entrance exams in the days of our youth. It led to us discovering for ourselves certain interesting heart-shaped curves, which are distinct from the well-known limaçon of Etienne Pascal and its special case the cardioid. It also led us to find the intrinsic relationship between the ellipse and the Cassinian curve that is associated with every ellipse. We detail here those observations with the hindsight of multiple decades and the availability of excellent modern geometric visualization software (in this case Geogebra) since the days of our paper and pencil explorations (However, even then we had an excellent set of ellipse and circle templates that our father had gifted us and also an ellipse drawing tool which we had made inspired by the यवनाचार्य Proclus). We first lay the ground work with some basic results and concepts that provide the necessary background before delving into the heart of the topic under discussion.

**The eccentric circles theorem**

*Given a circle and a point inside it, the locus of the midpoint of the segment joining the said point to a moving point on the circle is another circle with radius half that of the given circle and with its center as midpoint of the given point and the center of the given circle.*
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_excentric_circle_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Let  $(x,y)$  be the coordinates of the point  $P$  on the given circle with center at origin  $O$  (Figure 1) and radius  $2a$ . The coordinates of the given point are  $F_1=(0, 2c)$ . Let the coordinates of the midpoint  $M$  of the segment  $\overline{F_1P}$  be  $(x_1,y_1)$ . From Figure 1 it is clear that:

 $x_1=\dfrac{x+2c}{2}, y_1=\dfrac{y}{2}$ ; thus  $x=2x_1-2c, y=2y_1$ .

By plugging these into the equation of the given circle  $x^2+y^2=4a^2$  we get:

 $$4x_1^2-8cx_1+4c^2+4y_1^2=4a^2$$ 

 $$x_1^2-2cx+c^2+y_1^2=a^2$$ 

 $$\therefore (x-c)^2+y^2=a^2 \rightarrow \textrm{Locus}(M)$$ 

Thus, the locus of  $M$  is a circle with  $A=(0,c)$  as center and radius of  $a$ :  $Q.E.D$ .

**The ellipse**

*Given a line  $d$  and a point  $F$  outside it, what will be the locus of all points such that the ratio of their distances from  $F$  and  $d$  respectively is a given constant value  $e_c$ ?*
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_conics_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


From the solution shown in figure we get the equation of this locus to be:

 $$x^2+(1-e_c^2)y^2-2h(1+e_c^2)y+(1-e_c^2)h^2=0$$ 

Being a quadratic curve it will be a conic. Specifically, when  $e_c< 1$  it is an ellipse; when  $e_c> 1$  it is a hyperbola and when  $e_c=1$  it is a parabola. This relates to why these curves are called conic sections. We can see that the distance from point  $F$  can be represented by the surface of an infinite bicone with vertex at  $F$ . The distance from line  $d$  can be represented by a plane containing  $d$ . The given ratio  $e_c$  specifies the inclination of this plane such that the angle by which the plane is inclined is  $\theta=\textrm{arctan}(e_c)$ . The intersection of this plane and the bicone generates the conic section, which when projected on the  $xy$  plane appears as the conic curve specified by the above equation (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_conics_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Thus, when the inclination of the plane is less than  $\pi/4$  it is an ellipse. When it is exactly  $\pi/4$  it is a parabola. When it is between  $\pi/4$  and  $\pi/2$  we get a hyperbola. Corresponding to this are the Greek terms ellipse: less than; para: equal; hyper: greater than. This number  $e_c=\tan(\theta)$  (where  $\theta$  is the angle of inclination of the generating plane) is the eccentricity of the conic and  $F$  is a focus of the conic.

By definition the bipolar equation of an ellipse is  $r_1+ r_2=2a$ . Here,  $r_1,r_2$  are the distances of a point on the ellipse from the two foci of the ellipse  $F_1, F_2$ .  $a$  is the semimajor axis of the ellipse.  $F_1$  is one of the foci of the ellipse ( for instance, as determined by the construction in Figure 2 and 3) then the second focus  $F_2$  is at a distance of  $2c$  from  $F_1$  along the major axis of the ellipse.  $c= e_c\cdot a$ . Further, it is easy to see that  $a^2-c^2=b^2$ , where  $b$  is the semiminor axis of the ellipse.

**The eccentric circles of an ellipse**

*Given an ellipse and a point  $P$  moving on it, 1) what is the locus of the foot of the perpendicular dropped from a focus of the ellipse to the tangent at  $P$ ? (i.e. locus of the intersection of the tangent at  $P$  and the line perpendicular to it from one of the foci. 2) What is the locus of the reflection of one of the foci on the tangent drawn to the ellipse at  $P$ .*
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_excentric_circles_fig4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


From Figure 4 it is clear that the  $\triangle F_1QP \cong \triangle PQR$  by the SAS test. Hence,  $\overline{F_1P}=\overline{PR}$ . By definition of ellipse  $\overline{F_1P}+\overline{F_2P}=2a$ . Thus,  $\overline{PR}+\overline{F_2P}=\overline{F_2R}=2a$ . Therefore, the locus of  $R$  is a circle  $c_1$  with center  $F_2$  and radius  $2a$ .

It is clear from the definition of  $R$  that  $Q$  is the midpoint (Figure 4) of  $\overline{F_1R}$ . Therefore, by the eccentric circle theorem applied to the above-defined circle  $c_1$  the locus of  $Q$  is a circle  $c_2$  with radius  $a$  and center as the midpoint of  $F_1, F_2$ , which is the center of the ellipse. Thus,  $c_2$  is the solution to the problem of the pedal curve of an ellipse with the pedal point as one of the foci.  $c_2$  is also the circumcircle of the given ellipse. These two circles  $c_1, c_2$  are the two eccentric circles of an ellipse.

**Construction of an ellipse using its eccentric circles**

Since the radii of both eccentric circles of an ellipse are defined by only the semimajor axis of the ellipse, the whole family of ellipses with the same semimajor axis will share the radii of the eccentric circles. Hence, we additionally need to define the foci to construct the ellipse given one or both of its eccentric circles.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_from_c2_fig5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


If we are given just the circumcircle  $c_2$  and a focus  $F_1$  then we can construct the required ellipse thus (Figure 5): Define focus  $F_1=(-c,0)$ . Draw a segment connecting  $F_1$  to  $P$ , a point moving on the circle  $c_2$ . Draw a perpendicular line to  $\overline{F_1P}$  at  $P$ . The envelop of all such lines would be our required ellipse.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_from_c1c2_fig6.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


If we are given both eccentric circles  $c_1$  and  $c_2$  then the construction is a little more involved but has interesting consequences (Figure 6). First define the foci  $F_1=(-c,0), F_2=(c,0)$ . Then draw circle  $c_2$  with origin as center and radius  $a$ . Draw circle  $c_1$  with  $F_1$  as center and radius  $2a$ . Let  $P$  be a moving point on circle  $c_2$ . Join  $F_1$  to  $P$ . Draw a line  $t$  perpendicular to segment  $\overline{F_1P}$  at  $P$ . With  $P$  as center draw a circle which passes through  $F_2$ . This circle cuts the circle  $c_1$  at points  $A$  and  $B$ . Join  $F_1$  to both  $A$  and  $B$ . The points where segments  $\overline{F_1A}$  and  $\overline{F_1B}$  intersect line  $t$  are  $C$  and  $D$  (Figure 6). The locus of points  $C$  and  $D$  as point  $P$  moves on  $c_2$  gives us the required ellipse (blue in Figure 6).

**The ellipse hearts**

Notably the above construction of an ellipse using both the eccentric circles yields a companion curve (purple in Figure 6). It usually assumes a heart-shaped form with a dimple or a cusp that superficially resembles the limaçon of Etienne Pascal. However, a closer examination reveals that it is not that curve and has a distinct shape; we term it the ellipse-heart because every given ellipse will have its unique ellipse-heart. From Figure 6 we can also see that the ellipse-heart can be defined for a given ellipse as the locus of the reflection of the point of tangency on the pedal line from one of the foci. Like the Descartes oval and its dual the limaçon, this ellipse-heart can be seen as the dual of the ellipse. Its shape can be described by the eccentricity  $e_c$  of the ellipse. For high eccentricity it shows a prominent dimple that tends towards a cusp as  $e_c \to 1$ . For  $e_c< \tfrac{1}{2\sqrt{3}}$  it becomes a convex oval that becomes a circle identical with the ellipse for  $e_c=0$ .

In order to derive the equation of this curve, we note that by the above definition of the eccentric circle  $c_1$  we have  $\overline{AC}=\overline{F_2D}$ . We also observe (Figure 6) that the ellipse-heart is obtained by subtracting  $\overline{F_2D}$  from the radius vector of the eccentric circle  $c_1$ . Now,  $F_2D$  is itself the radial vector of the ellipse with the focus as the pole. This allows us to define the polar equation of the ellipse using the focus as a pole as is done in celestial mechanics, where one star/planet is at the focus and another star/planet/moon is moving in an elliptical orbit around it. This equation of the ellipse is:

 $$r=\dfrac{\left(a^2-c^2\right)}{a\pm c\cdot\cos\left(\theta\right)}$$ 

Here the radial  $\angle{\theta}$  is known as the "true anomaly", as in the definition of elliptical orbits in the planetary theories of नीलकण्ठ सोमयाजिन् and Johannes Kepler. Given that  $\overline{AC} \; || \; \overline{F_2D}$  and in the opposite direction we can derive the equation of the ellipse-heart by subtracting the above radial vector from  $2a$ , the radial vector of the circle  $c_1$  with the appropriate signs. Thus, if the ellipse is:

 $r=\dfrac{\left(a^2-c^2\right)}{a- c\cdot\cos\left(\theta\right)}$ ,

then its ellipse-heart is:

 $$r=2a-\dfrac{\left(a^2-c^2\right)}{a+c\cdot\cos\left(\theta\right)}$$ 

While the square of the above equation has an indefinite integral, evaluating it is a bit complicated. Hence, we resorted to the expediency of numerically integrating it and arrived at the area of the ellipse heart  $A_h$  to be:

 $A_h=\pi (4a^2-3ab)$ , where  $a$  and  $b$  are respectively the semimajor and semiminor axis of the ellipse.

Thus,  $A_h= A_{c_1}-३आ_ए$ , where  $A_{c_1}$  is the area of the eccentric circle  $c_1$  and  $A_e$  that of the ellipse. Further we also get:

 $$\dfrac{A_h}{A_e}=4\dfrac{a}{b}-3$$ 

Thus, when  $\tfrac{a}{b}=\tfrac{5}{4}, \; e_c=\tfrac{3}{5}$ , i.e. the three ellipse parameters form a 3-4-5 right triangle then  $A_h=2\cdot A_e$ . This is a beautiful configuration (Figure 7). Finally, inspired by the above equation for the ellipse-heart we can also define a second ellipse-heart using parametric equations as:

 $$x=\left(2a-\dfrac{\left(a^2-c^2\right)}{a+c\cos\left(t\right)}\right)\cdot\cos\left(t\right), y=\left(2a-\dfrac{\left(a^2-c^2\right)}{a-c\cos\left(t\right)}\right)\cdot\sin\left(t\right)$$ 

This curve has a classic heart-shape (hotpink in Figure 7) for a part of the range of eccentricities of the ellipse. These curves may be considered bifocal like the ellipse, unlike the limaçons (including the cardioid) derived from the unifocal circle. The ellipse and the ellipse-hearts touch each other at the vertices of the ellipse. Figure 7 shows the relationships between a system of ellipses and their corresponding ellipse-hearts. They might define the outlines of certain leaves or the dehisced pod of the bastard poon tree.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_hearts_fig7.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad
```{=latex}
\end{center}
```


**The Cassini curve of an ellipse**

*Every ellipse is associated with a confocal Cassini curve sharing parameters with the ellipse.*

Even though the Cassini curves are well-known, that they are intrinsically associated with every ellipse does not seem to be common knowledge (at least as far as we know). This is despite the historical associations of a version of the Cassini curve, the Cassini oval. Hence, it excited us considerably when, in our youth, we discovered the two to be intimately linked. The curve itself was discovered by the astrologer and mathematician Giovanni Cassini in an interesting context: In the west, as in India, the transition from geocentricity to heliocentricity was neither immediate nor uncontested. Cassini, despite being a prodigious observational astronomer, believed that the sun went around the earth in an oval orbit, which was defined by one lobe of the curve now known as the Cassini ovals. However, later in his life he realized that he was totally wrong and that Kepler was right in describing the orbit of the earth around the sun as an ellipse rather than an oval.

Now, how is the Cassini curve associated with the ellipse? It arises from the following question: Given an ellipse with a point  $P$  on it, let points  $A$  and  $B$  be the feet of the perpendiculars dropped from the two foci of the ellipse  $F_1, F_2$  to the tangent at  $P$ . What would be the locus of the points of intersection  $F$  and  $E$  of the circles with radii  $r_1=F_1A$  and  $r_2=F_2B$  as  $P$  moves along the ellipse. We solved this thus:
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_ovals_fig8.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 8\quad
```{=latex}
\end{center}
```


From the above discussion it becomes clear that both  $A$  and  $B$  will lie on the circumcircle of the ellipse  $c_2$  (Figure 8). As both are pedal points they would define parallel lines  $\overleftrightarrow{AD}$  and  $\overleftrightarrow{BC}$  which form the rectangle  $ABCD$  inscribed in the circle  $c_2$ . From this rectangle (Figure 8) it becomes clear that the  $\overline{AF_1}=\overline{CF_2}=r_1$  and  $\overline{BF_1}=\overline{DF_1} =r_2$ . We then apply the well-known Euclidean intersecting chords theorem on  $AD$  and the major axis of the ellipse  $V_1V_2$ . Thus we get:

 $$r_1\cdot r_2=\overline{V1F_1}\cdot \overline{V2F_2}=(a-c)(a+c)=a^2-c^2=b^2$$ 

Thus, the product of the two pedal segments of an ellipse is a constant equal to the square of the semiminor axis:  $r_1\cdot r_2=b^2$ . Now, the bipolar equation of the form  $r_1\cdot r_2=b^2$  defines the Cassini curve. From this bipolar equation we can derive its Cartesian equation:

 $$\left((x - c)^2 + y^2 \right) \left((x + c)^2 + y^2\right) = (a^2 - c^2)^2=b^4$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_lemniscate_fig9.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 9\quad
```{=latex}
\end{center}
```


The form taken by the Cassini curve depends on the eccentricity of the ellipse. When  $e_c=0$ , the ellipse, circumcircle  $c_2$  and the Cassini curve all become a coincident circle. When  $e_c=\tfrac{1}{\sqrt{2}}$ , the curve crosses over to become the lemniscate of Bernoulli (Figure 9). When  $1> e_c> \tfrac{1}{\sqrt{2}}$  it becomes two separate oval loops and is the classic Cassinian oval. When  $\tfrac{1}{\sqrt{2}}> e_c> \tfrac{1}{\sqrt{3}}$  the curve is bilobed with central dimples but a single loop. When  $e_c \le \tfrac{1}{\sqrt{3}}$ , the curve takes the form of a single biaxially symmetric convex oval. For values close to the minimum of the range of  $e_c$  the Cassini curve approximates a single circle while close to the maximum it approximates two small circles. In conclusion, the eccentricity of the ellipse  $e_c$  is entirely sufficient describe the range of shapes adopted by the Cassini curve. Indeed, we can conceive, the three curves, namely the ellipse circumcircle  $c_2$ , the ellipse and the corresponding Cassini curve as three degrees of response to the eccentricity parameter. The circle  $c_2$  represents the  $0{th}$  degree in that it does not change at all with  $e_c$ . The ellipse represents the first degree response in that in flattens uniformly along the minor axis with increasing  $e_c$ . The Cassini curve represents the even more exaggerated second degree response in that it starts of by flattening along the minor axis even more rapidly than the ellipse. Thus it first dimples, then crosses over as the lemniscate and finally breaks apart into the two loops of the oval. Thus the first degree response is a conic while the second degree response is a toric section (i.e. a section through the torus as the Cassini curves). Figure 10 shows an animation of the evolving curve with changing  $e_c$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/ellipse_animation_fig10.gif){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 10\quad
```{=latex}
\end{center}
```


