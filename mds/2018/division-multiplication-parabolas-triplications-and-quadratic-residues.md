
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Division-multiplication parabolas, triplications, and quadratic residues](https://manasataramgini.wordpress.com/2018/09/30/division-multiplication-parabolas-triplications-and-quadratic-residues/){rel="bookmark"} {#division-multiplication-parabolas-triplications-and-quadratic-residues .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 30, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/09/30/division-multiplication-parabolas-triplications-and-quadratic-residues/ "5:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**Introduction**

Many strands of our investigations on conic-generating integer sequences, word fractals and cellular automaton models for pattern formation came together in an unexpected manner while investigating a simple integer sequence. While some of these connections have have been known before, others are to our knowledge unreported. We present our investigations in this regard below.

**The division-multiplication parabola sequence and other associated sequences**

Consider the following sequence  $f$ :

 $f\lbrack 1\rbrack =1$ ; thereafter if,

 $n < f\lbrack n-1\rbrack , f\lbrack n\rbrack =\left\lfloor\dfrac{f\lbrack n-1\rbrack }{n}\right\rfloor$ ;

else,  $n > f\lbrack n-1\rbrack$  and  $f\lbrack n\rbrack = n\cdot f\lbrack n-1\rbrack$ 

Following our initial investigation, a search of OEIS revealed that this sequence was reported therein by Amarnath Murthy (A076039). The first few terms of  $f$  are: 1, 2, 6, 1, 5, 30, 4, 32, 3, 30, 2, 24, 1, 14... A more detailed view of  $f$  for increasing values of maximum  $n$  is shown in Figure 1.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/09/div_mult_parabola_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


An examination of the sequence and its plot in Figure 1 reveals the following features:


 1.  The action of division and multiplication by  $n$  takes place alternately. Hence, the sequence alternately bounces between high and low values.


 2.  However, on the larger scale it becomes apparent that the sequence shows cycles, each with the same overall pattern but of increasing magnitude (Figure 1).


 3.  A closer examination of these cycles shows that within each cycle the high values climb to a maximum and then fall off to reach a value which is 1 more than the first low value of the next cycle. In contrast, within each cycle the low values keep linearly decreasing until reaching 1. Then they start again with a new starting value in the next cycle.


 4.  Each new cycle starts with a large jump in magnitude relative the previous cycle. We can define the start of each new cycle objectively as after when  $f\lbrack n\rbrack =n$ . We find that  $f\lbrack n\rbrack =n$  when  $n$  takes the values: 1, 2, 5, 14, 41, 122, 365, 1094, 3281, 9842, 29525, 88574, 265721, 797162, 2391485, 7174454... This sequence  $m$  can be described by the formula:

 $$m\lbrack n+1\rbrack =\dfrac{3\^n+1}{2}$ , where  $n=0,1,2,3...$$ 

We may also recursively define it as  $m\lbrack n\rbrack =3\cdot m\lbrack n-1\rbrack -1$ , from  $n=2$  onward after defining the first term  $m\lbrack 1\rbrack =1$ . We will show below that this sequence  $m$ , which appears in the context of the cycles of  $f$ , also appears in several other seemingly unrelated mathematical structures with interesting properties. It is also numerologically interesting that the 7th cycle corresponds to the approximate length of the earth year in days: 365.


 5.  The determination of the sequence  $m$  allow us to trivially prove that the length of each cycle is:

 $3\^n=1, 3, 9, 27, 81, 243...$ , where  $n=0, 1, 2, 3, 4, 5...$ . Thus, the expansion of the sequence is by the powers of 3.


 6.  The maximum value of each cycle is reached at the midpoint of each cycle and is attained when  $n$  is 1, 2, 3, 8, 21, 62, 183, 548, 1641, 4922, 14763, 44288, 132861, 398582, 1195743, 3587228... This novel sequence termed  $m_p$  can be described by the below formula:

 $$m_p\lbrack n\rbrack =\left \lfloor\dfrac{m\lbrack n\rbrack +2}{2}\right\rfloor = \left \lfloor\dfrac{3\^n+5}{4}\right\rfloor$$ 

 7.  The maximum value of each cycle is given by the novel sequence  $m_x$ : 1, 2, 6, 32, 231, 1922, 16836, 150152, 1347261, 12113042, 108980466, 980713472, 8826089091...  $m_x$  can be obtained by the formula:

 $$m_x= \left \lfloor\dfrac{3\^n+5}{4}\right\rfloor \cdot \left \lfloor \dfrac{3\^n+ 9}{8} \right \rfloor$  where  $n=0, 1, 2, 3, ...$$ 

 8.  Thus, the ratio  $\tfrac{m_x}{m_p}$  is the sequence 1, 1, 2, 4, 11, 31, 92, 274, 821, 2461, 7382... defined by the formula:

 $$\dfrac{m_x}{m_p}\lbrack n\rbrack =\left \lfloor \dfrac{3\^n+ 9}{8} \right \rfloor$$ 

This sequence can also be generated from  $m_p$  by the formula:

 $$\dfrac{m_x}{m_p}\lbrack n\rbrack =\left \lceil \dfrac{m_p\lbrack n\rbrack }{2} \right \rceil$$ 

We will see below that this sequence remarkably also emerges in another area of higher arithmetic.


 9.  Whereas the low values of  $f$  decrease linearly from their starting point in a cycle till they reach 1, the high values which they alternate with appear to be defined by successively larger parabolic arcs (Figure 2). We determined these parabolic arcs to have the general equation of the form:

 $$y= \dfrac{x}{2} \left(3 m\lbrack n-1\rbrack +1-x\right)$$ 

Here,  $n=2,3,4...$  and each successive arc spans the high values in the range  $x= m\lbrack n-1\rbrack +1$  to  $x= m\lbrack n\rbrack$ . Hence, we term sequence  $f$  the division-multiplication-parabola sequence (DMPS). Thus, in a sense our sequence is a "triessential" sequence with the number 3 being fundamentally linked to it in many ways.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/09/div_mult_parabola_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


The idea of this type of parabola first came to us over 23 years ago in the context of trying to understand if natural selection might tend to select oligo-functional proteins over both purely mono-functional ones and those that perform a large number of distinct functions. The idea is each new function gained by a protein results in an increased cost for the other functions it performs; however, there could be a net fitness gain from the compounding of the capacity to now perform multiple functions. This would result in an optimal point after which the performance in each function will deteriorate too much to have net fitness gain despite the compounding. The result would be a parabolic arc similar to that obtained in the DMPS.

In the remainder of this article we shall discuss the connections we have found between the sequences associated with the DMPS and other mathematical objects.

**The Mephisto-Waltz words**

A Mephisto-Waltz word is a word in a two-symbol alphabet that is generated by a triplicative substitution system with the rules  $0 \rightarrow 001, 1 \rightarrow 110$ . The growth is initiated with  $MW\lbrack 1\rbrack =0$ . Thus, we get the following words:

 $0; 001; 001001110; 001001110001001110110110001$  and so on. This can be graphically represented as the triplicative growth of circular cells with differentiation (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/09/div_mult_parabola_fig3_mw-e1538283343328.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


The Mephisto-Waltz words can also be used to generate another kind of pattern in the form of a square using Cantor's pairing function. Cantor's pairing function maps every ordered pair of non-negative integers  $(j,k)$  to a single unique integer  $n$ :

 $$n=j+\dfrac{(j+k)(j+k+1)}{2}$$ 

Now, if we pair every 'letter' in the Mephisto-Waltz word with every other 'letter' in it with the above Cantor function then get the square pattern shown in Figure 4. Since there are four distinct pairings possible between 0 and 1, i.e. (0,0), (0,1), (1,0) and (1,1), under the the Cantor function each cell of the ensuing square matrix can have one of four values, each shown in a different color.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/09/div_mult_parabola_fig4_mw_cantor.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


We can also apply a form of "epigenetic" information to the genetic encoding of a folding instruction in the two symbol Mephisto-Waltz word. It goes thus: We start out drawing a line segment in a certain direction. If we encounter a  $0$  we draw a line segment of fixed length in the same direction as we are currently oriented in. If we encounter  $1$  and it is in an even position (epigenetic information) then we turn by an angle of  $\tfrac{\pi}{2}$  and draw a line segment of the same fixed length in the new orientation. If we encounter  $1$  in an odd position in then we do the same but turning by  $-\tfrac{\pi}{2}$ . This results in the fractal "wave" illustrated in Figure 5.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/09/div_mult_parabola_fig5_mw_fractal.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


Mephisto-Waltz words show an intimate connection to sequences emerging from the DMPS: First, given that they have a triplicative growth pattern their length grows as  $3\^n$ , which is the same as the length of the cycles of the DMPS. Second, the numbers of zeros in the Mephisto-Waltz words grow as the sequence  $m=1, 2, 5, 14, 41, 122, 365$ , which defines the points of transition to the next cycle of the DMPS. The numbers of 1s in these words grow as  $m-1$ . Thus in a sense the sequence  $m$  captures the 'median' growth of these words.

**Standard order words in a 3-symbol alphabet**

Consider an alphabet with 3 symbols. We can denote those 3 alphabets by the numbers 1, 2, 3. Then we write the following words in the this alphabet:

1, 12, 12123, 12123123123123, 12123123123123123123123123123123123123123 ...

One observes that the process goes thus: we first write 1, then 12, which form the first two words. Then from the 3rd word onward we add  $3\^n$  letters (where  $n=1, 2, 3,...$ ) in the form of repeats of 123. Thus, the length of these words grows as 1, 2, 5, 14, 41, 122... which is the same as the sequence  $m$  defining the transitions to the next cycle in the DMPS.

Standard order means that a later symbol of the alphabet cannot appear in the word unless the symbol earlier to it has already appeared in the word. Thus, 2 cannot appear in the word unless 1 has already appeared. Likewise, 3 cannot appear in a word unless 1 and 2 have already appeared. Thus, if traverse through the above-generated 3-symbol words from the one to the next we can create  $n$ -letter words with a maximum of 3-symbols in standard order:

For 1-letter words we can only have: 1\
For 2-letter words we can only get: 11, 12\
For 3-letter words we get: 111, 112, 121, 122, 123\
For 4-letter words we get: 1111, 1112, 1121, 1122, 1123, 1211, 1212, 1213, 1221, 1222, 1223, 1231, 1232, 1233\
So on (OEIS: A278985). Thus, the number of  $n$ -letter standard order words we can get in a 3-symbol alphabet is the same as the sequence  $m=1, 2, 5, 14, 41, 122...$ .

**The number of quadratic residues of  $3\^n$ **

Quadratic residues were extensively studied by Carl Gauss in his celebrated *Disquisitiones Arithmeticae*. The quadratic residues for a number  $n$  are determined by considering all  $0 \le a < n$ . Then  $q= a^2 \mod n$ . Further, given that  $\left((n-a)^2 \right) \mod n= a^2 \mod n$ , we only need to compute the residues for  $0 \le a < \left \lfloor \dfrac{n}{2} \right \rfloor$ .

For example, if we take  $n=9$ , then we have the residues:  $0=(0^2) \mod 9; 1=(1^2) \mod 9; 4= (2^2) \mod 9; 0=(3^2) \mod 9; 7= (4^2) \mod 9$ . Thus, for  $n=9$ , the numbers 0, 1, 4, 7 are its quadratic residues. We observe that of the numbers from 0 to (9-1=8) the following are not represented: 2, 3, 5, 6, 8. These are the quadratic non-residues of 9.

Let us now consider the quadratic residues of the powers of 3 and count how many of them are there for each power:

 $$3^0: 0; n=1 \\ 3^1: 0, 1; n=2\\ 3^2: 0, 1, 4, 7; n=4\\ 3^3: 0, 1, 4, 7, 9, 10, 13, 16, 19, 22, 25; n=11\\ 3^4: 0, 1, 4, 7, 9, 10, 13, 16, 19, 22, 25, 28, 31, 34, 36, 37, 40, 43, 46, 49, 52, 55, 58, 61, 63, 64, 67, 70, 73, 76, 79; n=31$$ 

So on...

We see than the number of quadratic residues for the powers of 3 specify the sequence  $\tfrac{m_x}{m_p} \lbrack n\rbrack$  starting from the second term. This is the ratio of the maximum value to the midpoint of the cycle of the DMPS where it is attained.

We also noted that a sequence closely related to  $\tfrac{m_x}{m_p} \lbrack n\rbrack$  turns up in the graph theory. While  $\tfrac{m_x}{m_p}\lbrack n\rbrack =\left \lceil \tfrac{m_p\lbrack n\rbrack }{2} \right \rceil$  this sequence is defined as:

 $$\rho'\lbrack n\rbrack =\left \lfloor \dfrac{m_p\lbrack n\rbrack }{2} \right \rfloor$$ 

In graph theory a circuit is defined as graph in which by passing through adjacent vertices one can reach the same vertex. Now, if we specify that adjacent vertices should be colored using different colors, then we need a minimum of 4 colors to color all vertices of any circuit graph. The number of distinct configurations that a circuit graph with  $n$  vertices can be colored with 4 colors was shown to be  $\rho'\lbrack n\rbrack$  by Bernhart (OEIS: A006342). This sequence goes as: 0, 1, 1, 4, 10, 31, 91, 274, 820, 2461, 7381, 22144, 66430, 199291, 597871, 1793614 ... One can see that it differs from  $\tfrac{m_x}{m_p} \lbrack n\rbrack$  by 1 at every odd  $n$ , i.e.  $n=2k+1$ .

Thus, rather notably, a simple arithmetic procedure generates the DMPS, with associated sequences bearing connections to other seemingly unrelated mathematical objects pointing to the pervasiveness of certain combinatoric numbers.

