
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A problem from 600 CE and some curiosities of आर्यभट's कुट्टक algorithm](https://manasataramgini.wordpress.com/2018/04/02/a-problem-from-600-ce-and-some-curiosities-of-aryabha%e1%b9%adas-ku%e1%b9%ad%e1%b9%adaka-algorithm/){rel="bookmark"} {#a-problem-from-600-ce-and-some-curiosities-of-आरयभटs-कटटक-algorithm .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 2, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/04/02/a-problem-from-600-ce-and-some-curiosities-of-aryabha%e1%b9%adas-ku%e1%b9%ad%e1%b9%adaka-algorithm/ "3:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Around 600 CE in the examinations of one of the Hindu schools of mathematics and astronomy one might have encountered a problem such as below (given by भास्कर-I in his commentary on आर्यभट's आर्यभटीय):

[द्वयाद्यैः षट्-पर्यन्तैर् एकाग्रः यो 'वशिष्यते राशिः ।]{style="color:#0000ff;"}\
[सप्तभिर् एव स शुद्धो वद शीघ्रं को भवेद् गणक ॥]{style="color:#0000ff;"}

Quickly say, O mathematician, which number when divided by the numbers starting with 2 and ending in 6 (i.e 2:6) leaves 1 as the remainder, and is exactly divisible by 7?

This problem was given to illustrate the use of the कुट्टक algorithm first provided by आर्यभट. Before we actually solve the above problem we will briefly examine the कुट्टक. The कुट्टक is a general algorithm deployed to obtain integer solutions for the indeterminate linear equations of the form  $ax-by=c$ , where  $a, b, c$  are positive integers. Thus, it is essentially the problem of finding the coordinates of the the integer lattice point through which the line  $ax-by=c$  passes. Of the three constants in the equation,  $a$  and  $b$  are given;  $c$  is not given but we have to find the smallest  $c$  for which the equation can be solved in integers. From that we can build other valid  $c$  For computational simplicity (with no loss of generality) we take  $a$  to be the bigger number and  $b$  to be the smaller number. The below presentation of it follows the matrix representation of आर्यभट's operation given by the mathematician Avinash Sathye:

Let us consider as an example the following equation  $95x-25y=c$ . From  $a=95$  and  $b=25$  we can generate the below matrix which is the result of the कुट्टक procedure. We have written a function 'कुट्टक' in the R language that computes this matrix and few other details given  $a,b$ .

 $$K=\begin{bmatrix} 19 & 4 & 95 & NA \\ 5 & 1 & 25 & 3 \\ 4 & 1 & 20 & 1 \\ 1 & 0 & 5 & 4 \\ 0 & 1 & 0 & NA \\ \end{bmatrix}$$ 

The process is initiated with column  $K\lbrack ,3\rbrack$ . Write  $K\lbrack 1,3\rbrack =a$  and  $K\lbrack 2,3\rbrack =b$ . कुट्टक in Sanskrit means 'to powder', common translated as 'pulverizer'. We start 'pulverizing'  $a=95$  with  $b=25$ , which means finding the  $K\lbrack 3,3\rbrack =K\lbrack 1,3\rbrack \mod K\lbrack 2,3\rbrack$ . Then  $K\lbrack 4,3\rbrack =K\lbrack 2,3\rbrack \mod K\lbrack 3,3\rbrack$ . We iterate this procedure until we get  $K\lbrack n,3\rbrack =0$ ; that completes the column  $K\lbrack ,3\rbrack$ . We call  $n$  as the number of iterations for convergence. Thus, the matrix will have  $n$  rows and 4 columns. The quotient of the division of  $K\lbrack 1,3\rbrack \div K\lbrack 2,3\rbrack =3$  is written as  $K\lbrack 2,4\rbrack$ , that of  $K\lbrack 2,3\rbrack \div K\lbrack 3,3\rbrack =1$  as  $K\lbrack 3,4\rbrack$ , so on till convergence. Thus, the cells  $K\lbrack 4,1\rbrack$  and  $K\lbrack 4,n\rbrack$  will always be empty (NA in the R language).

Then we fill in  $K\lbrack n,2\rbrack =1$  and  $K\lbrack n-1,2\rbrack =0$ . There after we compute the remaining elements of this column working upwards from  $K\lbrack n-2,2\rbrack$  with the formula:

 $$K\lbrack j,2\rbrack =K\lbrack j+1,2\rbrack \cdot K\lbrack j+1,4\rbrack +K\lbrack j+2,2\rbrack$$ 

We then fill in  $K\lbrack n,1\rbrack =0$  and  $K\lbrack n-1,1\rbrack =1$  and complete the column starting  $K\lbrack n-2,1\rbrack$  upwards with the formula:

 $$K\lbrack j,1\rbrack =K\lbrack j+1,1\rbrack \cdot K\lbrack j+1,4\rbrack +K\lbrack j+2,1\rbrack$$ 

With that we have our कुट्टक matrix  $K$  and all the needfull stuff to solve the said indeterminate equation:

 1.  The greatest common divisor  $\textrm{GCD}(a,b)=K\lbrack n-1,3\rbrack$ . This is also the smallest positive  $c$  for which our indeterminate equation has integer solutions.

 2.  The least common multiple  $\textrm{LCM}(a,b)=K\lbrack 1,1\rbrack \cdot K\lbrack 2,1\rbrack \cdot K\lbrack n-1,3\rbrack$ 

 3.  The integer lattice points through which the line passes are obtained from  $(K\lbrack 2,2\rbrack ,K\lbrack 1,2\rbrack )$  by assigning the appropriate signs. Thus, for the above equation we have the solution  $95\times(-1)-25\times (-4) = 5$ . Thus,  $95x-25y=5$  will pass through the integer lattice at the point  $(-1,-4)$ 

 4.  If we enforce the need for positive solutions then we can use  $(K\lbrack 2,1\rbrack -K\lbrack 2,2\rbrack , K\lbrack 1,1\rbrack -K\lbrack 1,2\rbrack )$  to obtain the minimal integer solution:  $95 \times 4 -25 \times 15 =5$ . Thus,  $95x-25y=5$  will pass through the integer lattice at the point  $(4,15)$  in the first quadrant.

 5.  We can write the following relationship, which helps us to more generally get the lattice points through which the line  $ax-by=c$  passes even if the values of  $a$  and  $b$  are interchanged or for  $c$  other than the minimal  $c$ :

 $$b(K\lbrack 1,1\rbrack \cdot p+K\lbrack 1,2\rbrack \cdot q)-a(K\lbrack 2,1\rbrack \cdot p +K\lbrack 2,2\rbrack \cdot q)=K\lbrack n-1,3\rbrack \cdot q$$ 

Thus, if we set  $p=-1, q=1$ , we get  $(-4,-15)$  as further lattice points through which the line  $95x-25y=5$  passes.

Now we can tackle the original problem: Since it says that 7 divides the number  $r$  perfectly it can be written as  $r=7y$  where  $y$  will the y-coordinate of the lattice point. The numbers 2, 3, 4, 5, 6 leave a remainder of 1. Of them 4 is divisible by 2, and 6 by both 2 and 3. So all we need to consider are the numbers 4, 5, 6. Using the above कुट्टक or any other means we can show that  $\textrm{LCM}(4,5)=20$  and  $\textrm{LCM}(20,6)=60$ . Thus, we can write  $7y=60x+1$ , where  $x$  will the  $x$  coordinate of the integer lattice. Using कुट्टक on the equation  $7y-60x=1$  we get the matrix:

 $$K=\begin{bmatrix} 60 & 17 & 60 & NA\\ 7 & 2 & 7 & 8\\ 4 & 1 & 4 & 1\\ 3 & 1 & 3 & 1\\ 1 & 0 & 1 & 3\\ 0 & 1 & 0 & NA\\ \end{bmatrix}$$ 

From this we can compose  $(60-17) \times 7 - (7-2) \times 60 = 1$ . Thus, our number is  $r= 43 \times 7 = 301$ , which is divisible by 7 but leaves a remainder of 1 for all integers from 2:6. More generally, if we say that  $r \mod 2:6 \equiv 1$  then we can use  $K$  to compose the negative solutions  $r=-17 \times 7=-119$  or  $r=-77 \times 7 =-539$ . Such triplets of solutions correspond to symmetric lattice points along the line.

In the final part of this note we shall consider the following operation: Take a number  $n$  and perform the कुट्टक operation with it (i.e.  $a=n$ ) and all integers lesser than or equal to it (i.e.  $b=1:n$ ). Then we count the number of iterations it takes with each of these integers to reach convergence. From above it is clear that the minimum number of iterations for convergence will always be 3. We term the result the कुट्टक spectrum of a number and plot this spectrum for the numbers 120, 123, 127 and 128.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/04/kuttaka_spectrum.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


The कुट्टक spectrum displays several notable features:

 1.  It is pseudo-symmetric about the mid-point, i.e. either side of  $a/2$  is an approximate mirror image of the other side but they differ in "height" by one iteration.


 2.  The number of times the कुट्टक spectrum hits a minimum (i.e. converges in 3 iterations) is equal to the number of divisors of  $a$ ,  $D(n)$ . Thus, for a highly composite number, as defined by Ramanujan, we get the record number of minima in the कुट्टक spectrum for any number less than it. Thus, in our example the highly composite number  $a=120$  has 16 minima with the first six integers 1:6 giving a run of 6 successive minima. A minimally composite number like  $a=123=3 \times 41$  in our figure we get 4 minima, namely 1 and the number itself and its two prime factors. The prime number in our figure,  $a=127$ , as expected has only 2 minima.


 3.  The more composite a number the lower its mean value of iterations (red line in Figure 1) than other integers in its immediate neighborhood. Thus, the highly composite number 120 has the lowest mean value in our set. In contrast, the primes have higher mean values than the integers in their immediate neighborhood. This is can be seen with  $a=127$  in Figure 1.


 4.  A curious feature of the spectrum are the maxima, i.e. the value of  $b$  for which the maximum number of iterations are required for pulverizing  $a$  to convergence. For example the spectrum of  $a=128$  shows 2 maxima:  $b=79$  pulverizes it via the pathway: 128, 79, 49, 30, 19, 11, 8, 3, 2, 1, 0. The other one  $b=81$  via 128, 81, 47, 34, 13, 8, 5, 3, 2, 1, 0. One immediately notices that the convergence in each of these two cases enters the Golden ratio convergent sequence. This feature can be investigated further by examining the distribution of the values of  $a/b$  for those  $b$  which result in maxima in the कुट्टक spectrum for a given  $a$ . In order to have have clear discrimination of these fractional values of  $a/b$  corresponding to the spectral maxima we chose a set of relatively large  $a$ , namely all integers from 500 to 1000. We then determined the कुट्टक spectrum for each of those numbers and extracted the maxima for each  $a$  and plotted a distribution of the  $a/b$  values (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/04/kuttaka_maxima.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


First, the maxima always occur in second half of the spectrum (Figure 2), i.e.  $b> \tfrac{a}{2}$ . This makes sense because smaller  $b$  would reach close to  $a$  in the first division itself and could pulverize it to a relative small number. However,  $b> \tfrac{a}{2}$  would fit only once in  $a$  and could leave a relatively large remainder that could need more steps for pulverizing. Second, strikingly, the dominant peak in this distribution is the Golden ratio  $\phi$  (Figure 2), suggesting the maxima tend to occur where  $\tfrac{a}{b} \approx \phi$ . Indeed in our above example  $\tfrac{128}{79}=1.620253$ . This can be intuitively understood as the  $b$  which generates a maximum may be seen as a Golden cut of  $a$ : if  $b> \tfrac{a}{2}$  is too big then the remainder generated will be small and might be pulverized quickly. If  $b> \tfrac{a}{2}$  is too small then it will leave a big remainder relative to  $b$  which might be quickly pulverized in the next step. Thus, the  $\phi$  could give you the cut that is just right. The next dominant peak is at  $3-\phi \approx 1.381966$ . This is similar to  $\phi$  in its operation. These two are marked by a red dashed line in Figure 2.

There are further peaks in the distribution corresponding to other fractions on either side of  $\phi$  following a certain pattern of declining heights. Further they show the same symmetry as  $\phi$  and  $3-\phi$ , with each peak  $m$  having a counterpart  $3-m$ . We have thus far not been able to determine a more\
general expression describing all these peaks or prove why they should be peaks but we were able to account for a subset of them as corresponding to other quadratic irrational numbers (marked by grey dashed lines in Figure 2). These include:

 $$\sqrt{35/11} \approx 1.783765, 3-\sqrt{35/11} \approx 1.216235$$ 

 $$\sqrt{3} \approx 1.732051, 3-\sqrt{3} \approx 1.267949$$ 

 $$\sqrt{24/9} \approx 1.632993, 3-\sqrt{24/9} \approx 1.367007$$ 

 $$\sqrt{2} \approx 1.414214, 3-\sqrt{2} \approx 1.585786$$ 

Of these the pair  $\sqrt{2}, 3-\sqrt{2}$ , especially the former is not the best fit to the peak but given the breadth of that peak it is possible that more than one attractor fraction is merged in that peak. It would be a good mathematical quest to discover the general expression for the peaks, their dominance and prove why they tend to be peaks. There might be a subtle fractal structure to them that might become apparent at large values of  $a$ .

