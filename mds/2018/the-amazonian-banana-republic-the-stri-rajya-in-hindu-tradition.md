
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The amazonian banana republic: the स्त्री-राज्य in Hindu tradition](https://manasataramgini.wordpress.com/2018/05/06/the-amazonian-banana-republic-the-stri-rajya-in-hindu-tradition/){rel="bookmark"} {#the-amazonian-banana-republic-the-सतर-रजय-in-hindu-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 6, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/05/06/the-amazonian-banana-republic-the-stri-rajya-in-hindu-tradition/ "8:32 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![amazon_2](https://manasataramgini.files.wordpress.com/2018/05/amazon_2.jpg){width="75%"}

The शैव tradition shows a dichotomy with respect to the role of the sex in ritual and purity. The earlier अन्तिमार्ग or पाशुपत tradition focused on abstinence and the so-called "upward flow" or ऊर्ध्वरेतस्. This indeed the underlying idea behind the ithyphallic depiction of लकुलीश, the founder of one of the key अतिमार्ग traditions. However, within the शैव tradition there was another ambivalent practice with earlier roots in the shared pool of ascetic practices, which were also inherited by the वैष्णव-s (e.g. वैखानस-गृह्यसूत्र). This was the असिधारा-vrata. Here the practitioner engages in kissing and coital contact with his wife or another beautiful and sexually active woman without spilling his seed. Successful practice of this for a certain fixed period is said to confer rewards on the practitioner. This practice continued within the शैव-mantra-मार्ग both in the सैद्धान्तिक (e.g. in the मतङ्ग-पारमेश्वर tantra) and bhairava (e.g. Brahma-यामल) streams. Thus, the practice was likened to walking on the sword-edge. Unlike this practice, which still emphasized the non-spilling of seed, among the practices within the bhairava-srotas of the मन्त्रमार्ग the full-fledged sexual ritual with actual ejaculation developed with many variations in doctrine and praxis. The founder of one of central traditions within this stream, namely the kaula tradition, was the siddha मत्स्येन्द्रनाथ. A successor of his was another siddha गोरक्ष, who in certain late manifestations of the tradition is portrayed as superseding Matsyendra himself. This manifestation seemed to have been accompanied by a reversal to more abstinent practices and explicitly castigated the sexual activities of Matsyendra.

This is portrayed in famous story [we narrated earlier](https://manasataramgini.wordpress.com/2010/02/10/a-pan-indian-tale-of-the-natha-yogin-s/), which is widespread in the eastern reflexes of the नाथ tradition. Here, Matsyendra is described as going to a kingdom where only women existed, ruled by a female chief. It was termed the स्त्रिराज्य or कदलिराज्य (the banana-kingdom). There Matsyendra engaged in sex with the queen and was about to die from total loss of वीर्य, when his student गोरक्ष comes and saves him. This was the first time we learned of the स्त्रीराज्य. A tale similar to this one of Matsyendra was also incorporated into the hagiography of the अद्वैताचार्य शंकर presented in the माधवीय शंकर-digvijaya. These accounts were consistent with our next encounter with the term स्त्रीराज्य in the सूत्र-s of वात्स्यायन. There, in his sexual ontology, he says that the women of स्त्रीराज्य like violent actions in bed and also the use of कृत्रिम-लिङ्ग-s. Since then, we kept encountering the स्त्रीराज्य in a number of Hindu sources and it struck us that this was a parallel to the amazons, who are frequently mentioned in Greek lore. We had to visit an art museum, where we saw a modern imitation of a Classical sculpture of an amazon, probably one of the famous amazons featured in Greek legend (Top). The person, whom were showing the museum, remarked that the amazon had an "Indian" touch to her -- whether there was any truth to that or not -- it prompted us to revisit the topic leading to the current discursion on the स्त्रीराज्य.

While the amazons are frequently mentioned in the Greek epic and early literature, their counterpart, the स्त्रीराज्य, finds only a rare mention in the Hindu epic, the महाभरत. Yet a closer examination suggests that the inspiration for both probably stemmed from related steppe Iranic groups:

 1.  While there is no consensus it is most likely that the Greek word amazon does not have a Greek etymology. Rather, it is likely to have some kind of Iranic etymology such as ha-mazon, perhaps meaning a warrior band.

 2.  The Greek evidence from writers such as Herodotus associate them as mixing with the steppe Iranic groups like Scythians (शक तिग्रचूड) and spawning the Sarmatians (sairima). Consistent with this they are described as being experts of horse-borne archery.

 3.  The Roman leader Pompey records them as being in the army of Mithradata-VI the formidable Greco-Iranian king. The later Roman writer and general Ammianus mentions them as a neighboring tribe of the Iranic Alans (Aryans).

 4.  The archaeologist David Anthony notes that among the "Scythian-Sarmatian" warrior kurgans about 20% contain interred women in battle-suits like their male counterparts. Consistent with this, some Greek sources record the amazons being interred in large kurgans. This can also be placed in the context of Herodotus' account of the death of Cyrus, where he marches against an Eastern Iranic steppe kingdom of the Massagetae which was led by a queen Tomyris.

This suggests that indeed these steppe Iranics with female participation in warfare might have inspired the yavana legends about the amazons. They may have been more familiar to the early Greek sources than the Indic ones because they launched a series invasions in the direction of the Greek sphere and are even credited to have built some temples in the Greek sphere, which were subsequently centers of Greek worship.

On the Indian side of the evidence we find a further mention from the great Gupta age naturalist वराहमिहिर in his बृहत्संहिता:\
[दिशि पश्चिमोत्तरस्यां माण्डव्य-तुखार-ताल-हल-मद्राः ।]{style="color:#0000ff;"}\
[अश्वक-कुलूत-हलडाः स्त्रीराज्य-नृसिंहवन-खस्थाः ॥]{style="color:#0000ff;"} 14.22

He places the स्त्रीराज्य in the northwest along with several other tribes including the अश्वक, madra-s and the Tocharians. This is consistent with स्त्रीराज्य being associated with the steppe Iranics of the Northwest. In the second reference to स्त्रीराज्य by वात्स्यायन it is situated along with बाह्लिक (modern Balkh) again pointing to the northwest direction. This reference also mentions the स्त्रीराज्य women sequestering youths in their अन्तःपुर-s comparable to the a Greek tale regarding how the amazons reproduce by sequestering males from other tribes. The Chinese bauddha traveler-scholar Xuanzang and the Tang-Shu record a country Lang-ka-lo with its capital as Su-t'u-li-ssu-fa-lo which has be rendered by some as स्त्री-ईश्वर. It is explicitly stated as being under Iranian rule despite using ब्राह्मि script and having both bauddha-s and Hindus (hundreds of deva temples) on the way to the "Western woman country". This would suggest that all these sources recognized the same Northwestern land, likely associated with one or more steppe Iranic groups.

This position is also in line with the mention by कल्हण in the राजतरंगिणि of the स्त्रीराज्य. He describes स्त्रीराज्य as being invaded in course of the expansive conquests of the greatest Kashmirian emperor ललितादित्य to the north of Kashmir. He mentions स्त्रीराज्य as being conquered prior to the ललितादित्य conquering this Uttarakuru-s would again place it to the north and potentially in the steppes. Notably, he appears to attack it from the east crossing a desert which might have meant the southern reaches of the Takla Makan. In this regard we hear from the kavi who mixes the rasa-s of warlike and the erotic:

[तद् योद्धान् विगलद् धैर्यान् स्त्रीराज्ये स्त्रीजनो'करोत् ।]{style="color:#0000ff;"}\
[तुञ्गौ स्तनौ पुरस्कृत्य न तु कुम्भौ कवाटिनाम् ॥]{style="color:#0000ff;"} 4.173

Then the women-folk of स्त्रीराज्य made the valor of [ललितादित्य's] soldiers melt,\
by placing to fore their high breasts and not the frontal lobes of their elephants.

[स्त्री-राज्य-देव्यास् तस्याग्रे वीक्ष्य कम्पादि-विक्रियां ।]{style="color:#0000ff;"}\
[संत्रासम् अभिलाषं वा निश्चिकाय न कश्चन ॥]{style="color:#0000ff;"} 4.174

Seeing the emotions of trembling and the like exhibited by the queen of the स्त्रीराज्य in front of him (ललितादित्य), no one could say for certain if it was due to to fear or eros.

[एकम् ऊर्ध्वं नयद् रत्नम् अधः कर्षत् तथापरम् ।]{style="color:#0000ff;"}\
[बद्ध्वा व्यधान् निरालम्बम् स्त्रीराज्ये नृहरिं च सः ॥]{style="color:#0000ff;"} 4.185

By placing one magnetic gem which pulled it upwards, and another one which pulled it downwards, he (ललितादित्य) installed an idol of नृसिम्ह suspended in the air without support in the स्त्रीराज्य.

ललितादित्य's successor जयापीड is also mentioned as conquering the स्त्रीराज्य:

[चित्रं जितवतस् तस्य स्त्रीराज्ये मण्डलं महत् ।]{style="color:#0000ff;"}\
[इन्द्रिय-ग्राम-विजयं बह्व् अमन्यन्त भूभुजः ॥]{style="color:#0000ff;"} 4.587

After he conquered a large territory of the स्त्रीराज्य it is a wonder that other kings considered his conquest of the field of his (जयापीड's) senses \[ever greater].

[कर्ण-श्रीपटम् आबध्य स्त्रीराज्यान् निर्जिताद्-धृतम् ।]{style="color:#0000ff;"}\
[धर्माधिकरणाख्यं च कर्मस्थानं विनिर्ममे ॥]{style="color:#0000ff;"} 4.588

He established the office of the court of justice and hoisted therein the auspicious silk of कर्ण, which he had seized from the conquered स्त्रीराज्य.

Regarding his profligate successor ललितापीड we hear again from कलहण:

[अतृप्तः स्त्रीभिर् अल्पाभिर् उग्ररागः स पर्थिवः ।]{style="color:#0000ff;"}\
[जडं मेने जयापीडं स्त्रीराज्यान् निर्गतं जितात् ।]{style="color:#0000ff;"}|

The king (ललितापीड) with a raging passion and not satisfied with \[just] a few women considered जयापीड impotent for having left the स्त्रीराज्य after conquering it.

Thus, we see parallels of the Matsyendra story, where the king जयापीड is praised for having controlled his senses upon conquering स्त्रीराज्य. Notably, the कर्ण-श्री-पट, while obscure in meaning, reminds one of the Greek legends of Herakles and Theseus taking away the girdle of the amazonian queen Hippolyta. A further account of the स्त्रीराज्य in Hindu tradition is seen the Jaimini-अश्वमेधपर्वन्, which presents itself as a fragment of the महाभारत of Jaimini. However, as it has come down to us it is much reworked text with a वैष्णव focus. Here, the sacrificial horse reaches the स्त्रीराज्य in course of its wanderings and is taken by प्रमिला, the queen of स्त्रीराज्य. Arjuna challenges her to battle and after a brief archery encounter a celestial voice advises प्रमिला to give up and marry Arjuna. She releases the horse and accompanies Arjuna to हस्तिनापुर, where she waits for him till his period of celibacy for the अश्वमेध is complete. But in course of this account it informs us that there are no males in स्त्रीराज्य. The females are apparently left male-less due a curse of रुद्राणी. If the males go there they die in a month from the excessive and violent sex with the females of स्त्रीराज्य in line with the कामसूत्र-s comment in this regard ("[मासमात्रं स्त्रियं प्राप्य पश्चात् प्राप्नोति वैशसम् । ... तेनैव स्वेन लिङ्गेन प्रविशन्ति हुताशनम् ।]{style="color:#0000ff;"}"). This also reminds one of the Greek legend of the destruction of men by the sirens or Kirke during wanderings of Odysseus. There is indeed a reversal similar to that of Odysseus and Kirke in the Matsyendranatha tale where the women of [स्त्रीराज्य]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"} attack [गोरक्ष]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"} when he leaves with the former, [but by his yoga he turns them into birds](https://manasataramgini.wordpress.com/2010/02/10/a-pan-indian-tale-of-the-natha-yogin-s/).

Thus, we may infer both Greek and Hindu traditions had the memory of a land of females. In both traditions they were much embellished but it appears that the Greeks had much closer contact with the actual agents behind these legends. However, in both cases it seems they gave rise to a floating mass of legends, which were incorporated into various cycles in different ways. Given the relatively sparse occurrence of the स्त्रीराज्य in Hindu tradition, one may ask if they really encountered them or if were merely stories borrowed from some other group like the yavana-s. In this regard we may note the following:

 1.  The pre-Mauryan bronze mirrors described Vassilkov of Indian origin suggest some kind of a contact between the Hindus and the steppe Iranics. Moreover the alternative account of the death of Cyrus given by Ctesias, where the Indians are said to form an alliance with a central Asia group the दृबिक-s against the Achaemenids. These point to contacts between the Indians and the steppe long after the Indo-Aryans conquered and settled in India.

 2.  Stylistic similarities are seen between Northern Indian and horse-trappings (phalerae) and jewelry recovered from Sarmatian graves.

 3.  Recently Veeramah et al looked at ancient DNA extracted from individuals from Sarmatian and other graves from a wide swath of western Eurasia and studied their genetic affinities. Notably, one Sarmatian individual (labeled PR_10 in their study) from Russian Orenberg region (\~400-200 BCE) and a Crimean individual (labeled Ker_1) with Hunnic-style deformed skull from around 200-400 CE show evidence for Indian admixture. A preliminary examination (needs more careful confirmation) does suggest that this reflects a relatively recent Indian admixture with SNPs private to greater India rather than some ancient Indo-Iranian relationship. This would imply that there was direct contact with individuals of Indian origin so as to result in gene-flow.

In conclusion, we hence believe that there was some real knowledge of the steppe Iranics with female warriors among the Hindus. They were a distant group with which the authors of the texts might not have had close familiarity. Nevertheless, the direct experience of those who had journeyed to those regions likely formed the historical core of the information presented by Hindu authors, which was then subject to poetic elaboration. It is known that among the steppe groups both Iranic and later Turko-Mongol there was some degree of participation of women in warfare (down to the Mongol times and even after their conversion in the west to Mohammedanism). This was probably the root of both the Greek amazons and the Hindu स्त्रीराज्य.


