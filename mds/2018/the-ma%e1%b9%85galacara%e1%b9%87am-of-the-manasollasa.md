
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The मङ्गलाचरणम् of the मानसोल्लास](https://manasataramgini.wordpress.com/2018/12/01/the-ma%e1%b9%85galacara%e1%b9%87am-of-the-manasollasa/){rel="bookmark"} {#the-मङगलचरणम-of-the-मनसललस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 1, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/12/01/the-ma%e1%b9%85galacara%e1%b9%87am-of-the-manasollasa/ "8:15 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[In PDF format](https://manasataramgini.wordpress.com/wp-content/uploads/2018/12/manasollasa.pdf)

A conversation with a friend brought my mind back to the [मानसोल्लास](https://manasataramgini.wordpress.com/2006/02/15/on-the-name-itself/), the encyclopedia of the great चालुक्य emperor सोमेश्वर-deva. Below is the मङ्गलाचरणम् of that work. The last verse is where the author announces himself. There are the following caveats: 1) The text as I have it is unclear in parts and this seems to arise both from the printing and the underlying manuscripts that were used. In the unclear parts I have taken the readings most obvious to me. 2) The translation appended to verses is approximate and the reader should look at the original.

[अभीष्ट-फलदं सिद्धि-सिद्धमन्त्रं गणेश्वरम् ।]{style="color:#0000ff;"}\
[कर्ण-तालानिलोद्धूत-विघ्न-तूल-लवं नुमः॥]{style="color:#0000ff;"}\
We salute the गणेश्वर, the giver of desired fruits and the success-granting mantra, who blows away the cotton-tangles of obstacles with the draft from the beating of his ears.

[संवित्-सखी जयत्य् एका का 'पि शुद्धा सरस्वती ।]{style="color:#0000ff;"}\
[यया स्वतः प्रबुद्धानां प्रकाशो'तिप्रकाश्यते ॥]{style="color:#0000ff;"}\
The friend of consciousness conquers, who else is that one but the pure सरस्वती, by whose self the luster of the enlightened ones is caused to exceedingly shine forth.

[वन्दे भव-लता-बीजं लिङ्गरूपं महेश्वरम्।]{style="color:#0000ff;"}\
[सो 'व्यक्तम् अपि सुव्यक्तं यस्यान्तः सचराचरम् ॥]{style="color:#0000ff;"}\
I salute महेश्वर, the seed of the creeper of existence, in the form of a लिङ्ग, he who is unmanifest and also well-manifest and the \[cause of] the end of \[that manifestation] with all living and non-living bodies.

[कृष्ण कृष्ण हरे रक्ष मां विभो मां रमारमण मा विभुं कुरु ।]{style="color:#0000ff;"}\
[te hare narahare namo'stu me dehi deva padam अच्युताच्युतम् ॥]{style="color:#0000ff;"}\
कृष्ण! कृष्ण! Hari! protect me!\
Me O mighty one! O delighter of रमा make me powerful.\
O Man-lion! Hari! my obeisance is for you, O god Acyuta!\
confer on me the state from which one never falls.

[नौमि वेद-ध्वनि-वरं देवं धत्ते सदैव हि ।]{style="color:#0000ff;"}\
[नाभि-पद्मोदरे विष्णोः क्वणद्-भ्रमर-विभ्रमम् ॥]{style="color:#0000ff;"}\
I salute the god of the excellent chants of the Veda.\
Indeed, I always place myself in\
the humming of the buzzing bees,\
in the womb of विष्णु's navel-lotus.

[तं नमस् कुर्महे शक्रं देवानाम् अपि दैवतम् ।]{style="color:#0000ff;"}\
[यो लोचन-सहस्रेण विश्वकार्याणि पश्यति ॥]{style="color:#0000ff;"}\
I make my obeisance to him, [शक्र ]{style="font-weight:400;"}who is the god of the very gods,\
who with his thousand eyes beholds all the happenings of the universe.

[यः सन्ततं तत् तमः पटलं विदीर्य सावित्रिकं करशतैर् वहति प्र्काशम् ।]{style="color:#0000ff;"}\
[तं विश्व-रक्षण-पटुं परमेकम् आद्यम् आदित्यम् अद्भुत-विलासविधिं नमामि॥]{style="color:#0000ff;"}\
Continually rending that screen of darkness,\
the impeller, who with his hundred rays bears light,\
I salute him, the foremost one, skilled in protecting the world,\
the primal [आदित्य ]{style="font-weight:400;"}manifesting in a marvelous manner.

[स्थाणुर् यस्येच्छया जातः शरीरार्ध-भृत-प्रियः ।]{style="color:#0000ff;"}\
[अरिक्त-शक्तये तस्मै नमः कुसुम-धन्वने ॥]{style="color:#0000ff;"}\
By whose wish स्थाणु came to share half his body with his wife,\
Obeisance is for him of no mean power, the wielder of the flowery bow.

[चालुक्य-वंश-तिलकः श्री-सोमेश्वर-भूपतिः ।]{style="color:#0000ff;"}\
[कुरुते मानसोल्लासं शास्त्रं विश्वोपकारकम् ॥]{style="color:#0000ff;"}\
The forehead-mark of the चालुक्य-s, the king श्री Someshvara, has composed the encyclopedia मानसोल्लास for the benefit of the world.

