
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Making of a modern-day mantra-शास्त्र pamphlet](https://manasataramgini.wordpress.com/2018/06/10/making-a-modern-day-mantra-sastra-pamphlet/){rel="bookmark"} {#making-of-a-modern-day-mantra-शसतर-pamphlet .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 10, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/06/10/making-a-modern-day-mantra-sastra-pamphlet/ "1:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Originally, all mantra instructions were oral, keeping with the spirit of the श्रुति. But over time, starting probably around the Mauryan age, written manuals began to supplement the oral teachings. Thus, through the ages practitioners of the mantra-lore have made pamphlets for self-use, use by students or more general distribution. Until the English conquest, the Hindu, despite having familiarity with the printed mode of text dissemination, preferred the written mode. However, one of the signs of Hindu modernity was the production of printed and subsequently electronic texts. These mantra-manuals were originally written entirely in Sanskrit but with the decline of the use of आर्य-speech outside the ritual, bilingual manuals started making their appearance. What we present here is one such bilingual manual. It is made taking advantage of टेX, the famous and innovative typesetting system of Donald Knuth, which allows you to make nice-looking documents. The टेX distribution used is ंइख़्टेX२.9 and the typesetting engine is XएळटेX. The text is input using the टेXवोर्क्स् environment, which comes with ंइख़्टेX for editing and typesetting. The Devanagari and IAST conversion from an ITRANS input was achieved using the Sanscript converter ([http://www.learnsanskrit.org/tools/sanscript](http://www.learnsanskrit.org/tools/sanscript){rel="nofollow"}). The font for the Devanagari script is the Siddhanta font of Mihail Bayaryn. The manual presented here is a reasonably comprehensive discourse on "[The सावित्री and the उपासना of the Deva सवितृ](https://github.com/somasushma/tex-files/blob/master/pdf/Savitri.pdf)" (pdf format). It is primarily aimed at a serious student with some familiarity with the श्रुति.

The source .tex file may be obtained [here](https://github.com/somasushma/tex-files/blob/master/saavitrii/Savitri.tex).


