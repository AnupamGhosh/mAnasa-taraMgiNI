
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Convergence to a palindrome](https://manasataramgini.wordpress.com/2018/04/14/convergence-to-a-palindrome/){rel="bookmark"} {#convergence-to-a-palindrome .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 14, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/04/14/convergence-to-a-palindrome/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This is a brief account of a sequence we constructed inspired by Dattatreya Ramachandra Kaprekar. It is not known to us if he had discovered it in one of his obscure publications from a small town in the Maharatta country. In any case we explored this sequence independently upon hearing of some procedures he used in his work. Consider a number like  $n=100$ . Its reverse is  $r(n)=1$ . Then,  $n+r(n)=101$ . We find that 101 is a palindrome. Consider another case  $n=155$ , then  $n+r(n)=155+551=706$ . This is not a palindrome so we continue the same process  $n+r(n)=706+607=1313 \rightarrow n+r(n)=1313+3131=4444$ . Thus, after 3 iterations of the process we have a palindrome. Thus, if we take any number and perform this operation of adding it to its digital reverse iteratively till we get a palindrome then our sequence  $f$  is defined as the palindrome to which each number  $n$  converges. Thus,  $f\lbrack 100\rbrack =101; f\lbrack 155\rbrack =4444$ . One question which arose was whether there are  $n$  that never converge to a palindrome. Between 1:99 all numbers converge to a palindrome even if a large one. Hence, we explored 100:999 in greater detail. Figure 1 shows a plot of  $f\lbrack 100:999\rbrack$ 

![Palin_Fig1](https://manasataramgini.files.wordpress.com/2018/04/palin_fig1.png){width="75%"}(f\lbrack n\rbrack )$ 

Of these our experiments suggested that the following 13 numbers in the range 100:999 never converge to a palindrome (marked by red dots in Figure 1): 196, 295, 394, 493, 592, 689, 691, 788, 790, 879, 887, 978, 986. One can see that barring 790 all of them come in pairs. The maximum value attained by  $f\lbrack n\rbrack$  in this range is the 13 digit number: 8813200023188. This value is attained when  $n$  is 187, 286, 385, 484, 583, 682, 781, 869, 880, 968. Comparable to the non-converging cases, here all ten cases come as pairs. There is also an interesting pattern for both the maxima and the non-converging cases: we observe that in each century it comes one number earlier but at some point a "new line of descent" emerges which then perpetuates the same pattern along with the older one. The same maximum value is attained for  $n$  in the range 1:99, with  $f\lbrack 89\rbrack , f\lbrack 98\rbrack =16668488486661$ .

Omitting the single digit numbers, 11 is the primordial palindromic number and its multiples often tend to have a palindromic structure. Its multiples for  $k=1:9$  are the only double digit palindromes. Multiples of 11 are also found among the triple digit palindromes: 121, 242, 363, 484, 616, 737, 858, 979. For  $n=1:99$  all  $n> 4$  converge to a palindrome which is a multiple of 11. For  $n=100:999$ ,  $f\lbrack n\rbrack$  have 64 unique prime factors ranging from 2 to 18209090957. Notably, the largest number of  $f\lbrack n\rbrack$ , 641, are divisible by 11. Thus, the most frequent convergence even in this range is to a multiple of 11 (Figure 2).

![Palin_Fig2](https://manasataramgini.files.wordpress.com/2018/04/palin_fig2.png){width="75%"}Figure 2. The number of  $f\lbrack 100:999\rbrack$ , which are divisible by a given prime divisor from the set of all unique prime factors of  $f\lbrack 100:999\rbrack$ .

Figure 2 shows that other than than the first few primes (2, 3, 5, 7 etc) there are some anomalous standout values. Notable among these are 37 which with 3 reaches a palindrome  $3 \times 37= 111$  and other palindromic primes, most notably 101 and 131. The convergent might itself be a palindromic prime and for  $f\lbrack 100:999\rbrack$  we have 101, 727, 929, 181, 383, 787. As can be seen in Figure 1 (Violet points) the  $n$  for which these palindromic prime convergents are reached have a distinctive pattern of distribution reminiscent of the maxima and non-converging values.

Finally, this sequence is notable for the very large values that are attained amidst otherwise pedestrian values (Figure 3).

![Palin_fig3](https://manasataramgini.files.wordpress.com/2018/04/palin_fig3.png){width="75%"}(f\lbrack n\rbrack )$  scale. Mean of  $f\lbrack n\rbrack$  is the red line while the median is the blue line

This makes for an interesting distribution where extreme events on the right end are rare but enormous in magnitude. This is reflected in the difference of several orders of magnitude between the mean and the median of  $f\lbrack n\rbrack$ . However, at least the actual occurrence of these extreme values is quite regular (Figure 1).

