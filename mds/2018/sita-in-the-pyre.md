
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सीता in the pyre](https://manasataramgini.wordpress.com/2018/04/28/sita-in-the-pyre/){rel="bookmark"} {#सत-in-the-pyre .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 28, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/04/28/sita-in-the-pyre/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It was quiet, early Saturday evening. Vidrum, Jukuta, Sharvamanyu and Lootika were hanging out on the parkway of Somakhya's house. They had assembled there for some curricular preparation on differentiation. Somakhya and Lootika had covered the chain rule and the differentiation of product functions. Buoyed up the lessons their classmate-students felt that they could sally forth for the impending tests with confidence. But Somakhya put the damper on them: "Differentiation is the easy part and one can rake up the marks on the exam like [Gandulkar](https://manasataramgini.wordpress.com/2017/12/06/of-lives-of-men-of-times-of-men-iii/) smashing sixes off the Rainbow Republic bowlers. The real challenges will come with the rising balls of integration."

Lootika amplified the matter like an accompanying musician: "If you think you have mastered math, remember this tale in  Greek tradition: it was said that there was a mortal named Stentor who could yell with the voice of many men in unison. But once he attempted to contest with the god Hermes on hearing whose loud yell he died. Thus, when the integration section opens you may come up against some Hermesian howlers that our classmate Hemaling spends all day integrating."\
Somakhya: "In any case we will cover that tomorrow along with some vectors."

Vidrum flipping through the news and messages on his tablet chimed in: "I see some very disturbing news. A city man, Durjoy, has been arrested on the charges of trying to arrange aid to the socialist terrorists who have been trapped as part of the ongoing operation in Gondipura. Our classmate Manjukeshi who was supposedly taking preparatory lessons from him at that time has also been taken into custody!"\
Sharvamanyu: "Good job by the security forces."\
Jukuta checking out the news herself: "That is really sad. Was this Durjoy the senior student from the St. Stephens institution? He would come to hangout outside our school and was quite good to me in those days. I do not know if was really involved in anti-national activities. But it is ridiculous on their part to arrest Manjukeshi. I believe she should be innocent."\
Lootika: "I think he was a lout. One day he accosted me outside school but I scampered away on my bike in fear, thinking he might be an acolyte of [Shonit](https://manasataramgini.wordpress.com/2015/11/19/the-autumn-days/) who had harassed me and my sisters in our former school. But Sharva got some interesting "stones", from him, which he apparently found in Gondipura and gifted them to me. They turned out to be the fossil eggs and an ungual of the peculiar sauropod dinosaur Isisaurus "\
Sh: "Remember, he was always trying to seduce girls from our school. He left those fossils with me during an inter-school athletics event and never took them back. Then I gave them to you knowing you might find them interesting. Why do you think he was in Gondipura? I am sure it was not for dinosaur-hunting."\
Lootika: "Of course, now all that makes sense. I'd add Manjukeshi may not be innocent either."\
Vidrum: "Why do you think so? She is a nice girl. I know she was just taking classes from that Durjoy."\
Somakhya: "Vidrum, have you forgotten [the day you first introduced her to us](https://manasataramgini.wordpress.com/2016/07/28/nalika-s/). She did strike us as a fighter for social justice. After all let's not forget she used assemble for these lessons of ours whole of last semester. Until., well...I guess let it be."\
Jukuta: "I never really understood why she stopped coming. Somakhya don't kill me for this, but she simply told me that Lootika was a horrible person. She added that despite being a girl, she was perpetuating gender stereotypes and [horrible ]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}casteism. I tried to tell her that even though in school I had more than one tiff with Lootika, we are now grown up and in college and need to accept that she is truly brighter than all the girls in our class. She cut me off saying that I was either with her or with Lootika."\
Lootika: "But Jukuta you have not stopped on playing on both sides, right? Let me remind you we are under no obligation to have you here for these lessons."\
Vidrum: "Calm! Let us not get into one of those silly fights of you girls."\
Sharvamanyu: "I think it began with that article on the रामायण, which Manjukeshi authored for the college magazine, where she accused emperor राम of being a male chauvinist and a casteist. We castigated her for that and she got angry with Lootika for not supporting her silliness."

Jukuta: "I did read that article. I don't know much about the रमायण्, but if that incident of letting सीता burn is in the book it does sound awful, like a dowry death by burning, you see. Lootika, I know you and your sisters are the only girls who know these things written by Brahmins in Sanskrit. Is it really there in the book?"\
Lootika: "Jukuta, before anything else you must pronounce the रामायण correctly -- repeat after me. When my parents first told me the tale, they left out this part. But then, when I read it myself, in order to narrate it to my sister Vrishchika, I encountered it right there in the original. I must confess I read that part with some tension, as to what was going to happen, and even surprise. It did not feel right to me then and I asked my mother about it. She gave me the explanation that राम was an अवतार of the god विष्णु and that the अवातार ended the moment he performed the superhuman task of killing the रक्षस्-lord. Thus, beyond that point he behaved like an ordinary human and the act must be understood in that light. I then asked my father about it. He said that when सीता was abducted by रावण, the real सीता was taken away by the gods and replaced by a माया सीता. As per his interpretation, at the time of the fire ordeal, the god Agni incinerated the माया सीता and returned the real सीता to राम. So clearly, that incident was seen as bit unusual and rather than accept it without a question, people have come up with different explanations for it."

Jukuta: "But Lootika at the bottom-line would that still not be misogynistic, as Manjukeshi had written?"\
Lootika: "See, such words are neologisms, which make little sense in the context of old tradition. As a biologist I have learned not be fall prey to such emotionalistic constructs but coldly look at the hard realities on the ground. The sexes are always pitted in an inter-sexual conflict for maximizing their fitness. At same time, as we are eukaryotes, sex is institutionalized in us starting with the duplication into two paralogs of the ancient protein ऱेcआ and the HORMA protein acquired from bacteria more than 2.7 billion years ago. Like all other sexually reproducing organisms, we cannot make more copies of ourselves without the other sex. Hence, the games played by the players in this conflict should not be given any moralistic judgment. That said, if you read the original text, there is hardly anything of the kind the puerile Manjukeshi read in it. In fact, it can be read as not disallowing the option of her kind of unregulated sexuality: after all राम tells सीता is that she can chose any other mate she wants. So she is offered unfettered agency in the sexual domain and it is she who chooses to remain in the union with राम by proving her chastity via undergoing the ordeal by fire. Moreover, you see her not caving in but giving a dignified and firm public defense of her character when it was attacked and this you can take from a ब्राह्मणी who has actually read the text in the original language."\
Sharvamanyu: "Look at it from राम's perspective too. There was a direct affront on his manliness as a warrior in the abduction of सीता and he avenged it squarely. However, no man likes to be in doubt of his paternity or risk providing for his enemies' offspring. Therefore, he had very natural reasons to be uneasy in accepting his wife who had been taken into his enemy's custody for a while..."\
Somakhya: "See, this discussion we are having here is one of the reasons for epic's author to craft such a tale. Even though the रामायण, unlike our national epic the great भारत, is one of ideals, it has all these elements that bring out the grey shades of real life. If it were not for these, people would have had difficulty in having a discourse on dharma of the type as we are just having. When it comes to the knotted questions of dharma there are no black and white answers but only a difficult navigation though the shades in the grey zone. Incidents like this provide the framework for thinking about that path. But then the great भार्गव does not put things in the epic without a deeper metaphor, which only some grasp."\
Vidrum: "What is that deeper metaphor that you are alluding to? Does such exist in this particular incident?"\
Somakhya: "It does. But I'd let this spidery girl expound it if she wishes as it concerns her ancestors."

Lootika: "It was something I actually learned from Somakhya and that was when I came to a final understanding of the incident. Everything in the इतिहास of वल्मीकि is a reflection of deep elements of श्रुति. He has done so in order that people like you Vidrum and Jukuta can access the mysteries of religion that might be otherwise inaccessible. First, remember that the princess सीता born of the furrow in earth is transparently the humanized form of the ancient goddess of agriculture, who was praised by my ancestor वामदेव Gautama in the mantra:

[अर्वाची सुभगे भव सीते वन्दामहे त्वा ।]{style="color:#0000ff;"}\
[यथा नः सुभगाससि यथा नः सुफलाससि ॥]{style="color:#0000ff;"} RV 4.57.6

Be auspicious O सीता, come come close to us. We worship you,\
such that you confer us a good share \[of things] confer the good fruits \[of agriculture].

Second, my ancient clansmen, the Gotama-s, were the purohita-s of the Videgha-s, the clan of the Janaka-s to which सीता of the रामायण belonged. Evidently, they transmitted the cult of सीता to these kings. The ब्राह्मण on the सामिधेनि-s, the chants by which the fire is aroused in the श्रौत rite, preserves an anachronistic ancient legend of one of the founders of my clan, Gotama राहूगण, and his patron the founder of the Videha-s, Videgha माथव. In that tale, it is said, alluding to the Eastward migration of the Indo-Aryans, that the land the Videgha-s eventually occupied was initially rich in water but difficult to cultivate and habit. But when Gotama uttered an incantation, the god Agni is said to have burst forth from Videgha माथव's mouth and burnt the land of the सदानीर river making it habitable. It is this ancient legend that receives an epic reflection in the form of सीता's purification by the fire, representing that new land of the Videha-s and Kosala-s, along with the Gotama-s, to the east of the Kuru-पाञ्छल, becoming fit for habitation and the furrow of agriculture. राम who is the earthly manifestation of the great Indra then receives that सीता, even as my ancestor वामदेव says in his incantation:\
[इन्द्रः सीतां नि गृह्णातु ।]{style="color:#0000ff;"}\
May Indra set his hold on सीता (set down the furrow)."\
Somakhya: "More generally, it might be seen as representing the agricultural practice of burning leftover plant material after winter (the grip of वृत्र whose reflection in the epic is रावņअ) to get the fields ready for agriculture in spring under the fertilizing effect of Indra."\
Vidrum: "Interesting, but I guess such arcana might be beyond the lay user of the epics who might be satisfied with the social debate you brought up earlier."\
Lootika: "Of course this is mostly for the those who uphold the deeper language of tradition but we are just giving you a flavor of how overloaded the language of myth can be."

Jukuta: "But what would you say about the killing of जम्बुकुमार्, the शूद्र saint, by श्रि राम्? Is that not very casteist?"\
Sharvamanyu: "Who the hell is this जम्बुकुमार्? Never come across such in the रामायण."\
Lootika: "I believe she is referring to शम्बुक."\
Vidrum: "That is a tough one. What would you guys say.?\
Somakhya: "Again, not reading the original but going by the words of these social justice types can give you a wrong impression. Of course, it is a long story and we could sit here arguing both sides. for a while, like all these thorny points in the epic. Would you really like a story without such shades of grey, the tensions they create and the emotions they arouse? On the other hand there are at least three messages here. First, this शम्बुक was a शूद्र alright but he was not saint. He was aiming to ascend to and conquer the world of the gods, like the demon रौहिण; hence, it was imperative that their representative on earth रामचन्द्र ऐक्ष्वाकव nip such attempts in the bud. Remember that in the early days with the आर्य-s settled in the subcontinent of the Jambudvipa what was meant by शूद्र was some kind of enemy of the आर्य-s. Overtime they were defeated and absorbed into आर्य society. This leads to the second message -- it is actually one reflecting this social accommodation and change. The text clearly states: [भविष्यच् छूद्र-योन्यां हि तपश्चर्या कलौ युगे ॥]{style="color:#0000ff;"} It presents a doctrine that successively over the four yuga-s each वर्ण acquired the capacity to do tapas. Now this शम्बुक was doing it out of turn in the previous yuga and hence he was punished for breaking the rules. However, in a positive message it indicates that in our yuga the शूद्र has this option and the incident merely refers to the tensions of a bygone era. Thus, in the typical futuristic format of the पुराण-s, the text is presenting this change in social reality vis-a-vis the शूद्र against the background of the previous antagonism. Third, it delivers the message of conservatism for social change. Rapid social change by accommodation of those from without the fold presents dangers. Such elements are represented by शम्बुक, who wish to emulate आर्य practices, not for good purposes, but for conquering the daiva realm. Hence, such revolutionary tendencies have to be suppressed in favor more gradual accommodation over the yuga."

Jukuta: "Your uncompromisingly conservative outlook amazes me."\
Lootika: "You may appreciate it more as you age, though part of it is in the genes and we cannot do much about it. But whether you like our message or not remember that a love for facile, feel-good messages might turn you into a Durjoy, a Shonit or a Samikaran, who in the end might do more harm than good to society."\
Sharvamanyu: "And now Jukuta I hope you don't bring up the issues of animal rights in the incident of the killing of वालिन्."\
Jukuta: "Who is वालिन्?"\
Lootika: "The next time we assemble at my house I'll gift you a बाल-रामायण. I really think you need to be reading it more than any of this curricular stuff."


