
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on आम्रेडित-s in the ऋग्वेद and issues of word distribution](https://manasataramgini.wordpress.com/2018/05/14/a-note-on-a%e1%b9%83re%e1%b8%8dita-s-in-the-%e1%b9%9bgveda-and-issues-of-word-distribution/){rel="bookmark"} {#a-note-on-आमरडत-s-in-the-ऋगवद-and-issues-of-word-distribution .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 14, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/05/14/a-note-on-a%e1%b9%83re%e1%b8%8dita-s-in-the-%e1%b9%9bgveda-and-issues-of-word-distribution/ "6:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[स दर्शतश्रीर् अतिथिर् गृहे-गृहे]{style="color:#0000ff;"}\
[ vane-vane शिश्रिये तक्ववीर् iva ।]{style="color:#0000ff;"}\
[जनं-जनं जन्यो नाति मन्यते]{style="color:#0000ff;"}\
[विश आ क्षेति विश्यो विशं-विशम् ॥]{style="color:#0000ff;"} RV 10.91.2 by अरुण Vaitahavya

He, with visible auspiciousness, a guest in house after house,\
in forest after forest lurking like a hunting falcon,\
people after people, no people are overlooked by him,\
The clan among the clans, he dwells in clan after clan.

The आम्रेडित or the iterative is a duplicated compound like those seen in the above ऋक्: गृहे-गृहे etc. While the variety of आम्रेडित-s seen in the RV is no longer seen in modern Sanskrit, some forms have persisted from that time e.g. पुनः-पुनः or anyam-anyam. Their variants are also seen in other extant Indo-आर्यन् languages. This form of a compound is attested to my knowledge in the earliest branching lineage of Indo-European, Anatolian. It is also seen in Tocharian which probably branched off next. Forms equivalent to the "pra-pra" (forward and forward) found in the RV are also attested in Homeric Greek and are termed prepositional complements of verbs. However, the आम्रेडित, with the involvement of all elements of speech, is most developed only in Indo-Iranian. Old Iranian in the form of Avestan displays forms like:\
न्मान्ē-न्मान्ē = Skt: धाम्नि-धाम्नि; house after house (i.e. every household)\
वीसि-वीसि = Skt: विशे-विशे; clan after clan (i.e. every clan)

Given their parallels in Sanskrit, it is clear that this expanded system of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s was already present in the ancestral Indo-Iranian tradition. Here we briefly examine the [आम्रेडित-s ]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}in the ऋग्वेद. Given the temporal position of the RV, it likely presents a picture of the usage of such compounds close to the beginning of the Indo-Iranian tradition.

There can be anywhere between 1 to 4 [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s in a given ऋक्. The number of ऋक्स्-s with each number of आम्रेडित-s per ऋक् is shown in Table 1.

![AMreDita_tab](https://manasataramgini.files.wordpress.com/2018/05/amredita_tab.jpg){width="75%"}Table 1

The maximum number of आम्रेडित-s in the RV is four, which is seen in a single ऋक्, the one given in the opening lines of this article. By far the most common आम्रेडित is dive-dive (day after day), which occurs 47 times in the RV (the next most common, the ancient pra-pra, occurs only 12 times). A related usage ahar-अहः meaning the same as dive-dive occurs 6 times. Thus, the आम्रेडित used in the sense of 'daily occurrence' seems to be a characteristic feature of the Vedic language. Another temporal आम्रेडित is yuge-yuge (6x), which famously occurs in later literature in the भगवद्गीता. Whereas in that text it means 'eon after eon', in the RV it likely means some version of the पञ्चसंवत्सर-yuga, which is explained by Lagadha in the वेदाञ्ग ज्योतिष. Some other temporal आम्रेडित-s are also used on rare occasions in the RV: मासि-मासि (every month) and पर्वणा-पर्वणा (every fortnight).

Other [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s tell us about the old आर्यन् way of life and religion. Several of them indicate the sense 'in every household': dame-dame (6x), गृहे-गृहे/गृहं-गृहम् (7x), vastor-वस्तोः (2x). Others denote the sense 'in every clan': विशे-विशे and variants (7x), jane-jane/जनं-janam/जातो-जातः (5x). These meanings are shared with the most common Iranian आम्रेडित-s, suggesting that they were significant for the Indo-Iranians at large. Specifically, these terms indicate that the households and the clans were the primary organizational units of society. There is no mention of towns or even villages in any आम्रेडित-s although forests and trees are alluded to vane-vane/वृक्षे-वृक्षे. This is reflective of the semi-mobile life in the forest-steppe zone with its characteristic confederation of clans.

As we have noted before, [the people of the RV were a warlike people](https://manasataramgini.wordpress.com/2017/08/06/a-note-on-the-cow-the-horse-and-the-chariot-in-the-%E1%B9%9Bgveda/). Thus, the आम्रेडित-s meaning something like 'in every battle' or 'in every contest' are common: bhare-bhare (6x), वाजे-वाजे (4x), रणे-रणे (2x). There is also a शत्रोः-शत्रोः, i.e. 'of every enemy'. This indicates that frequent military encounters were a feature of the lives of the early Indo-आर्यन्स्.

We also have many terms relating to the Vedic religion as would befit a primarily religious text. First, we have देवं-devam/devasya-devasya/devo-देवः (10x) implying the worship of every god in the pantheon. We also here of groups or troops of gods: गणं-गणम्/शर्धम्-शर्धम् (3x). These are clear expression of the polycentric polytheism of the आर्य-s. Second, the soma- pressing and drinking sessions, which are part of the high Vedic rite, are alluded to in the terms made-made/sute-sute/some-some (7x). Third, rites and related terms are referred to multiply. The fire ritual: यज्ञे-यज्ञे and variants (8x); the ritual fires: agnim-agnim (3x); Ritual observances and actions: व्रातं-व्रातं (2x), karman-karman and variants (3x); ritual offerings: havir-हविः, samit-samit (2x); Incantations, recitations and invocations: धियं-धियं, गिरा-गिरा, have-have, brahma-brahma (5x)

To better understand the distribution of आम्रेडित-s in the RV we shall first take a detour to look at some basic statistics of the RV:

![RV_stats_tab](https://manasataramgini.files.wordpress.com/2018/05/rv_stats_tab.jpg){width="75%"}-s occurring per मण्डल

Figure 1 shows the number of words per hemistich (ardhark) with the alternate hemistichs colored in blue and red. The मण्डल boundaries are marked with vertical dotted lines.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/05/amredita_words_half_rk.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


This plot shows that the RV has a generally consistent average behavior across most मण्डल-s keeping with the relatively tight repertoire of Chandas (meters) used by most clans. However, the anomalies are also immediately apparent. The मण्डल-8 dominated by काण्व-s clearly stands out as having a distinct structure. Moreover, this pattern is also seen in the initial part of मण्डल-1 where multiple काण्व-s are represented. Further, in मण्डल-1 we see an anomalous spike corresponding to the ultra-long meter, the अत्यष्टि used by prince Parucchepa दैवोदासि. In मण्डाल-9, the soma-मण्डल we again see an anomaly. Here there is a strong separation of the short meters (गायत्री-s) in the first part and the long meters closer to the end. This reflects this distinct aggregation history of the मण्डल-9, which unlike the family books tends to collect the सूक्त-s used in the soma ritual from different clans around a काश्यप core. These anomalies again come out clearly in the first two panels of Figure 2, which show the average number of words per hemistich and the average number of words per सूक्त across the 10 मण्डल-s.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/05/amredita_plots.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad shows some of the statistics for the RV with relation to the आम्रेडित distribution.
```{=latex}
\end{center}
```


These features are related in part to peculiarities discussed by [in our earlier analysis of related issues pertaining to the RV](https://manasataramgini.wordpress.com/2009/02/08/some-trivia-concering-the-adi-shruti/). We observe that the मण्डल-s 8 and 9 have the lowest average number of words per hemistich, keeping with the dominance of the shorter meters in these मण्डल-s (panel 1). However, we can see that काण्व-s tend to compose long सूक्त-s; hence, they figure a higher number of words per सूक्त on an average (Panel 2). However, in contrast, मण्डल-9, which also has the lower average number of words per सूक्त, has no such compensation and is dominated by short सूक्त-s. These are peculiar to the soma ritual and the सामन्-s composed on them. We also note that the Atri-s and वसिष्ठ-s tend to compose more short सूक्त-s than the others. Barring these anomalies, the RV is quite uniform, especially in terms of the average number of words per hemistich. This gives us the general background to investigate the distribution of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s.

Panel 3 for Figure 2 shows the [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s in a sliding window of 2000 words through the length of the RV. The मण्डल boundaries are marked by a vertical dotted line. We observe that the [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s are not uniformly distributed. There are whole regions with a low count and others with notable spikes. We find that the मण्डल-s differ in their use of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s (Panel 4 of Figure 2): मण्डल-s 3 and 6 of the वैश्वामित्र-s and भारद्वाज-s are rich in [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s, whereas those of वामदेव, the वासिष्ठ-s and the soma मण्डल are particularly poor. In the case of the soma मण्डल, the metrical structure with a low average number of words per hemistich probably discriminates against [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s. However, in the case of the other मण्डल the difference in [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"} counts is in spite of their mostly average behavior in terms of word count per hemistich (compare panel 4 and panel 1).

To understand if this difference might have any significance, we simulated the distribution of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s as a random process using the total number of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s in the RV (Table 2). We created 10000 artificial sets corresponding to the size of each मण्डल, checked the number [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s reached in each replicate and computed the Z-scores for the observed number of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s and the probability of getting the observed number or more/less by chance alone (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2018/05/amredita_simulation.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


This experiment suggests that मण्डल-s 1 and 10 have more or less the average number of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s one would expect by chance alone. This probably reflects their composite nature rather than being the product of one dominant clan. However, मण्डल-s 3 and 6 have greater than expected number [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s (p=0.016 and 0.014 respectively), whereas मण्डल-s 7 and 9 have lower than expected number of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s (p=0.035 and 0.037 respectively). This observation suggests there was possibly a conscious difference in the poetic styles of the वैश्वामित्र-s and भारद्वाज#-s on one hand and the वासिष्ठ-s on the other, with the former showing a predilection for the use of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s. This makes one wonder if the reduced use of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s by the वसिष्ठ-s, who had some links to the Iranian side, represents a regional tendency also seen in the Avesta, which also uses a low number of [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}-s. As noted above the unique structure of the soma-मण्डल probably accounts for its low [आम्रेडित]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}count.


