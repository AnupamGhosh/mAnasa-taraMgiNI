
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mean hyperbola and other mean functions](https://manasataramgini.wordpress.com/2018/08/24/the-mean-hyperbola-and-other-mean-functions/){rel="bookmark"} {#the-mean-hyperbola-and-other-mean-functions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 24, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/08/24/the-mean-hyperbola-and-other-mean-functions/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Let  $a, b$  be two numbers such that,

 $$0\le a \le b$$ 

We use  $a, b$  to construct a specific rectangular hyperbola using one of the following methods:\
Method-I (Figure 1: this is based on an approach we [described earlier](https://manasataramgini.wordpress.com/2008/03/28/fancies-of-the-parabola-and-hyperbola/))
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/08/hyperbola_constr_mean2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```



 1.  Mark point  $C (-a, a)$ , which will be the center of the hyperbola to be constructed.

 2.  Draw two perpendicular lines through  $C$  respectively parallel to the x- and y-axes. These two lines are the asymptotes of the hyperbola.

 3.  Bisect the angle between these asymptotes to construct a line with slope  $m=1$  passing through  $C$ .

 4.  On this line mark the two foci of the hyperbola  $F_1, F_2$ , such that they are are equidistant from  $C$  and separated from each other by a distance of  $4\sqrt{ab-a^2}$ .

 5.  With  $F_1$  as center draw a circle with radius equal to  $2\sqrt{2(ab-a^2)}$ . This will be equal to the distance between the two vertices of the hyperbola.

 6.  Let  $Q$  be a point on the above circle. Draw lines  $\overleftrightarrow{F_1Q}, \overleftrightarrow{F_2Q}$ .

 7.  Draw the perpendicular bisector line of  $\overline{QF_2}$ . It cuts  $\overleftrightarrow{F_1Q}$  at point  $P$ .

 8.  The locus of  $P$  as  $Q$  moves on its circle gives you the required rectangular hyperbola (red in Figure 1).

Method-II (Figure 2)
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/08/hyperbola_constr_mean.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```



 1.  Mark point  $C (-a, a)$ , which will be the center of the hyperbola to be constructed.

 2.  Draw two perpendicular lines through  $C$  respectively parallel to the x- and y-axes. These two lines are the asymptotes of the hyperbola.

 3.  Bisect the angle between these asymptotes to construct two perpendicular lines with slope  $m= \pm 1$  passing through  $C$ .

 4.  On the line with slope  $m=1$  mark the two foci of the hyperbola  $F_1, F_2$ , such that they are are equidistant from  $C$  and separated from each other by a distance of  $4\sqrt{ab-a^2}$ .

 5.  On the line with slope  $m=-1$  mark a sequence of points and draw a sequence of coaxial circles, which will pass through each of those points on this line and also the two foci  $F_1, F_2$ .

 6.  Each of these circles will cut the two asymptotes on total of 4 points. On each side separately draw segments joining the respective points of intersection of each circle on the two asymptotes (Figure 2).

 7.  The envelope of these segments is the desired hyperbola.

The said hyperbola has the equation:

 $$y=\dfrac{a(x+b)}{x+a}$$ 

![hyperbola_mean](https://manasataramgini.wordpress.com/wp-content/uploads/2018/08/hyperbola_mean.png){width="75%"}Figure 3

One observes that its upper branch cuts the y-axis at  $(0,b)$  and by construction it is asymptotic to  $x=-a, y=a$  (Green curve in Figure 3). But what else notable about this specific hyperbola? Note the intersections of the following lines with the hyperbola (Figure 3):\
The line  $x=a$  intersects the hyperbola at  $y=\mu_A$ , the arithmetic mean of  $a,b$ .\
The line  $x=b$  intersects the hyperbola at  $y=\mu_H$ , the harmonic mean of  $a,b$ .\
The line  $x=y$  intersects the hyperbola at  $y=\mu_G$ , the geometric mean of  $a,b$ .

Thus, this hyperbola can be considered a mean-generating curve where the means of the two numbers  $a,b$  can be easily obtained by plugging certain nice values of  $x$ . It also furnishes the proof for the fact that the three primary means of two numbers lie on a single hyperbola between the point where the hyperbola cuts the y-axis, point  $B(0,b)$  and the asymptotic line passing through  $A(0,a)$ . One notices that the  $\mu_A$  cuts the segment  $\overline{AB}$  in half. The geometric mean  $\mu_G$  cuts  $\overline{AB}$  below  $\mu_A$  and the harmonic mean  $\mu_H$  cuts it below  $\mu_G$ . This geometry allows one to define two further means, which are inversions (reflections) of  $\mu_G$  and  $\mu_H$  respectively on the  $\mu_A$  line. We define these inversive means as  $\mu_{Gi}$  and  $\mu_{Hi}$ . We discover that the intersection of the line  $x=\tfrac{a^{3/2}}{\sqrt{b}}$  with the hyperbola generates  $y= \mu_{Gi} = \tfrac{a^{3/2}+b^{3/2}}{\sqrt{a}+\sqrt{b}}$ . Likewise, the intersection of  $x=\tfrac{a^2}{b}$  with the hyperbola generates  $y=\mu_{Hi}=\tfrac{a^2+b^2}{a+b}$ . Thus, we have 5 means with the arithmetic mean as the central mean bisecting the segment  $\overline{AB}$  with two means above it and two means below it (Figure 3).\
In trying to understand these additional means coming from our hyperbola we learned of the work in this regard by Derrick Lehmer-II. He defined two mean generating functions. The first of them is (orange in Figure 3):

 $$y=\dfrac{a^{x+1}+b^{x+1}}{a\^x+b\^x}$$ 

We notice that this function is a sigmoid curve having  $y=a$  and  $y=b$  as its asymptotes (Figure 3). We realized that this function of Lehmer generates the same means as the above-constructed hyperbola. When  $x=0$  we get  $\mu_A$ ; when  $x=-\tfrac{1}{2}$  we get  $\mu_G$ ; when  $x=-1$  we get  $\mu_H$ . The two other means  $\mu_{Gi}$  and  $\mu_{Hi}$  respectively emerge at  $x=\tfrac{1}{2}$  and  $x=1$ . Thus, the same symmetry as the with the hyperbola is recapitulated by this function (Figure 3).

Lehmer's second mean-generating function is (red curve in Figure 3):

 $$y= \left(\dfrac{a\^x+b\^x}{2}\right)^{\frac{1}{x}}$$ 

We observe that this curve is also bounded by the same asymptotes  $y=a$  and  $y=b$ ; however, it converges to them at very different rate. At  $x=1$  it generates  $\mu_A$ ; at  $x=-1$  we get  $\mu_H$  and it intersects the first mean-generating curve (Figure 3). At  $x=2$  we get the  $\mu_Q$ , i.e. the quadratic mean or the root mean squared (RMS). This mean cannot be obtained by a nice value of  $x$  in either the first mean-generating function or the mean hyperbola. What about the value of the second mean-generating curve at  $x=0$ ? To get that we need to do is to evaluate the below limit:

 $$\displaystyle \lim_{x \to 0} \left(\dfrac{a\^x+b\^x}{2}\right)^{\frac{1}{x}}$$ 

At first sight, it would seem that it is indeterminate because we get an ugly division by zero making it impossible to evaluate directly. Hence, this cannot be done and we have to take recourse to Johann Bernoulli's method (commonly called L'Hospital's method).

We first take the logarithm of the above expression to get:

 $$\dfrac{\log\left(\dfrac{ (a\^x + b\^x)}{2}\right)}{x}$$ 

Then we differentiate the numerator and the denominator. The denominator is reduced as  $\tfrac{d}{dx}x=1$ . For the numerator we get:

 $\dfrac{d}{dx}\left(\log\left(\dfrac{ (a\^x + b\^x)}{2}\right)\right) = \dfrac{a\^x \log(a) + b\^x \log(b)}{a\^x + b\^x}$ .

We next take the  $\lim {x \to 0}$  to get  $\log\left( \sqrt{ab}\right)$ . We then reverse the logarithm operation by exponentiation and evaluate the limit of the second mean-generating function at  $x=0$  to be  $\sqrt{ab}$ . Thus, Lehmer's second mean-generating function yields the geometric mean at  $x=0$ . Lehmer showed that there are thus two families of means of which one includes the inversions of the geometric and harmonic means on the arithmetic mean and the other which includes the quadratic mean or RMS. The only means that are common to both families are  $\mu_A, \mu_G, \mu_H$  and the two mean generating curves intersect at  $y=\mu_H$  (Figure 3). Thus, these 3 are the only fundamental means.

While these two families of means do not come together is there an operation where combining a fundamental and family-specific mean gives an interesting result:

Let  $a_0=1, b_0=1$  and  $n \in \mathbb{N}$ ,

 $$a_{i+1}=(\mu_Q(a_i, b_i\cdot \sqrt{n}))^2$$ 

 $$b_{i+1}=(\mu_G(a_i, b_i))^2$$ 

Then  $\dfrac{a_{i+1}}{b_{i+1}} \to \sqrt{n}$ \
This gives us a means of obtaining rational convergents for a given irrational square root. For example if we use  $n=6$  in the above procedure we get:

 $$\dfrac{7}{2}, \dfrac{73}{28}, \dfrac{10033}{4088}, \dfrac{200931553}{82029808}, \dfrac{2523338293565406}{1030148544608239}$$ 

The last of these yields  $\sqrt{6}$  correct to 11 places after the decimal point.

Also see:\
1) [Means and conics](https://manasataramgini.wordpress.com/2017/07/22/means-and-conics/)\
2) [The Hindu square root method](https://manasataramgini.wordpress.com/2016/10/21/chaos-in-the-iterative-hindu-square-root-method-of-the-gaṇaka-raja/)

