
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A poll on peoples' beliefs on reincarnation](https://manasataramgini.wordpress.com/2018/06/11/a-poll-on-peoples-beliefs-on-reincarnation/){rel="bookmark"} {#a-poll-on-peoples-beliefs-on-reincarnation .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 11, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/06/11/a-poll-on-peoples-beliefs-on-reincarnation/ "6:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Using the Twitter's poll tool we conducted a poll which gave the below results:\
Reincarnation happens:\
Believe so but there is no evidence for it: 52%\
Sure, have the evidence for it: 20%\
No, just mumbo-jumbo: 11%\
Do not know: 17%\
388 respondents\
Most of the people who are in my sphere on Twitter are Hindus from India and North America, with smaller numbers from East Asia and Europe.

Hence, the above result is quite in line with reincarnation being a widespread belief among Hindus. With all the caveats of sample size and this poll not being "scientific" and the like, it is still notable to us that the majority believe in it. 72% in one way or another. Some complained that the negative alternative was too strong. Others said that it was self-evident to them hence the alternatives did not capture that special experience of their self-evident knowledge of reincarnation. All those are valid but we think these alternatives mostly capture the "big picture".

In Hindu tradition, reincarnation makes its appearance in the late Vedic tradition in the आरण्यक-s and उपनिषत्-s while being conspicuously absent in the early layers of their tradition. Yet parallel beliefs are encountered among their cousins the yavana-s and in a muted form (perhaps due to Zarathushtra) among the Iranians (at least as far as the Zoroastrians are concerned). Thus, it could have been an older belief that came back to the mainstream in the later Vedic layers. It is central to the philosophies of the nagna and the तथागत suggesting that the belief in reincarnation had taken deep root by around 2500 YBP. Thus, Hindus holding such a belief today is unsurprising. Medieval Abrahamistic thinkers were terrified by this idea taking root in their peoples under [Hindu and ताथागत influences](https://manasataramgini.wordpress.com/2012/04/01/5030/) as they thought it to be damaging to their cults. However, this belief exists widely outside the Hindu world, even among peoples smothered by the hand of Abrahamistic delusions. The Turks in Turkey for example widely hold such beliefs; so also do Lithuanians and First Americans to name a few, who have not been in the Hindu sphere of influence.

Our poll and anecdotal conversations we have had over the years suggests that even though the majority of Hindus might not have seen evidence directly, i.e. either through their own experience or seeing cases first hand, they seem to believe in it. Hence, it is one of the few aspects of the Hindu religion that seems to rest in large part on belief. Yet, Hindus were always somewhat uncomfortable with this facet of their religion resting purely on belief. Thus, even before the days of the तथागत, we hear of an इक्ष्वाकु monarch performing experiments with criminals condemned to death in order the determine the "substance" that left the dead body to reincarnate in another. These experiments, however poorly conducted, were seen as providing evidence for a reincarnating substance and thus providing an empirical basis for the claim. Remarkably, even in our age, one of my own श्रुति teachers believed that such an experiment had been more recently repeated and the results of yore had been reproduced. Others in the past were less sanguine about such experiments and declared that they found evidence from more indirect sources. The most commonly stated evidence is that of people recalling texts or special knowledge at a very young age, apparently without any exposure. Also included in this class of evidence, is the recollection of events of past births that can then be verified independently. At least 77-78 people in our poll responded that they had evidence -- so we assume that quite a number of people feel that they have seen such signs even today.

Finally, in all our inquiries it has been apparent that to most people it is not clear as to what really reincarnates. This was apparent in the responses to the poll. One respondent said that he took reincarnation as an axiom of his worldview. However, it is not clear if a completely consistent worldview has emerged for him by taking reincarnation as an axiom. Inquiries with Hindu teachers who believe in reincarnation usually results in the following answer: There is something called a सूक्ष्म-शरीर. This is said to "envelop" the आत्मन् and keeps a record of the karman balance of an individual. As long as it is non-zero it results in the आत्मन् experiencing reincarnation. However, when that balance is zero the आत्मन् no longer has the experience of identification with a शरीर attains the state of मोक्ष. Of course, there are subtle variations on this theme depending on the teacher and also some more drastic differences with the bauddha-s and jaina-s. Yet they all seem to agree that there is something like the सूक्ष्म-शरीर that reincarnates. Some of the believers might even give a more precise account of that सूक्ष्म-शरीर and associate it with the manas or buddhi. Several modern believers (including some Hindu teachers) and investigators of reincarnation state that it is more physical. They say that birthmarks and developmental malformations correspond to lesions from the past birth.

Today it is thought that only चार्वाक-s hold that reincarnation does not happen. Many Hindus fear that if this belief was proven wrong then the Hindu religion might crumble. Whether this belief has a leg to stand on, or whether the Hindu religion will really crumble if it is shown to be false are different questions. Conversely, it was always seen as threatening to the 3 classical Abrahamistic counter-religions. Indeed, one of the accusations the church leveled against Giordano Bruno for his execution was a belief in reincarnation. There is also an even more fundamental question of whether the notion can be tested at all. Those will be discussed separately in a very different format if the gods are favorable to us. This is just a brief record of the beliefs on the matter of reincarnation that might provide a preamble to that discussion, whenever it happens.


