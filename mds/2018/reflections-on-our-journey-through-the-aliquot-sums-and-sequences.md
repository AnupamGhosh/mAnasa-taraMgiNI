
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Reflections on our journey through the aliquot sums and sequences](https://manasataramgini.wordpress.com/2018/07/27/reflections-on-our-journey-through-the-aliquot-sums-and-sequences/){rel="bookmark"} {#reflections-on-our-journey-through-the-aliquot-sums-and-sequences .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 27, 2018]{.entry-date}](https://manasataramgini.wordpress.com/2018/07/27/reflections-on-our-journey-through-the-aliquot-sums-and-sequences/ "4:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**The numerology of aliquot sums and perfect numbers\
**The numerology of the Pythagorean sages among the old yavana-s is one of the foundations of science and mathematics as we know it. One remarkable class of numbers which they discovered were the perfect numbers --- teleios as they termed it. What are these numbers? Let  $n$  be a number and  $d_j$  be all its proper divisors, i.e. those divisors of  $n$ , which are less than  $n$ . Then we can define an arithmetic function known as the aliquot sum  $s(n)$  thus,

 $$\displaystyle s(n)=\sum_{j=1}\^k d_j; d_k< n$$ 

For example, let us consider the number 10. Its divisors are 1, 2,5,10. Its proper divisors are 1, 2, 5. Hence,  $s(10)=1+2+5=8$ . Now, if  $s(n)=n$  then  $n$  is called a perfect number (termed पूर्णाङ्क in जगन्नाथ's Sanskrit edition of Euclid). From Figure 1, we can see that the numbers 6 and 28 are perfect numbers. The Pythagorean interest in them also becomes apparent from the fact that these numbers are associated with certain natural periodicities that have an old Indo-European significance. This becomes clear from their occurrence even in old Hindu tradition. The number 6 is associated with the 6 seasons in ब्राह्मण-s like the शतपथ-ब्राह्मण and encoded into the श्रौत altar. Similarly 28 is also encoded into the altar according to the शतपथ-ब्राह्मण and corresponds to the count of the नक्षत्र-s or days of the lunar month. The number 28 is also used in Vaidika tradition as one of the prescribed counts for the japa of the सवितृ गायत्री
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_perfect_numbers_figure1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Returning to the arithmetic, with our above example of  $s(10)$ , we can see that  $s(n)n$ . Such numbers are called abundant numbers. Now, a subset of the proper divisors of 20 add up to 20 (Figure 1). Hence, it is also a semi-perfect or a pseudo-perfect number. There is a rare set of abundant numbers for which no subset of their proper divisors add up to them; they are known as weird numbers. For example,  $s(70)=74$ ; hence, it is an abundant number. However, no subset of its proper divisors, 1, 2, 5, 7, 10, 14, 35 add up to 70. Hence, 70 is a weird number. The next weird number is 836. Thus, due their rarity majority of abundant numbers are semiperfect numbers.

One of the high points of yavana brilliance was the discovery of a general formula for perfect numbers. In Book 9, proposition 36 Euclid states:

*"ean apo monados hoposoioun arithmoi hexēs ektethōsin en tē diplasioni analogia, heōs hou ho sumpas suntetheis prōtos genētai, kai ho sumpas epi ton eskhaton pollaplasiastheis poiē tina, ho genomenos teleios estai."*

If as many numbers as we please beginning from an unit be set out continuously in double proportion, until the sum of all becomes prime, and if the sum multiplied into the last make some number, the product will be perfect.

In modern terms we can lay it out thus: If the sum of the series of the powers of 2, where the power are integers  $\ge 0$ , is a prime number then the product of that prime with the last power of 2 in the series is a perfect number:

 $$q=\displaystyle \sum_{j=0}\^n 2\^j$$ 

If  $q$  is a prime then  $q\cdot 2\^n$  is a perfect number. Let us take the first few examples.

 $$2^0+2^1=3\Rightarrow 2^1\times 3=6$$ 

 $$2^0+2^1+2^2=7\Rightarrow 2^2\times 7=28$$ 

 $2^0+2^1+2^2+2^3=15 \Rightarrow$  Not a prime

 $$2^0+2^1+2^2+2^3+2^4=31\Rightarrow 2^4\times 31=496$$ 

 $2^0+2^1+2^2+2^3+2^4+2^5=63\Rightarrow$  Not a prime

 $$2^0+2^1+2^2+2^3+2^4+2^5+2^6=127\Rightarrow 2^6\times 127=8128$$ 

After this point the perfect numbers become much less frequent and large.

 $$2^0...2^{12}=8191 \Rightarrow 33550336$$ 

 $$2^0...2^{16}=131071 \Rightarrow 8589869056$$ 

 $$2^0...2^{18}=524287 \Rightarrow 137438691328$$ 

Another way of expressing this is thus: Given a prime number  $p$  if  $M_p=2\^p-1$  is also a prime then  $P=2^{p-1}\cdot(2\^p-1)$  is a perfect number. The corresponding prime numbers  $M_p$  are today famous as the Mersenne primes. For all  $p < 100$ , the following  $p$  yield  $M_p$ : 2, 3, 5, 7, 13, 17, 19, 31, 61, 89\
The corresponding  $M_p$  are:\
3\
7\
31\
127\
8191\
131071\
524287\
2147483647\
2305843009213693951\
618970019642690137449562111

The corresponding perfect numbers are:\
6\
28\
496\
8128\
33550336\
8589869056\
137438691328\
2305843008139952128\
2658455991569831744654692615953842176\
191561942608236107294793378084303638130997321548169216

These primes  $M_p$  start getting huge and rare rapidly. We computed the above in a few seconds with a modern programming language and computer. However, some of them mark historic feats of arithmetic computation by the unaided human brain in the pre-computer era. For instance, Leonhard Euler computed the 8th  $M_p$  (10 digits) and the corresponding perfect number. The Russian village mathematician I.M. Pervushin went further by computing the 9th of these numbers (  $M_p= 19$  digits). With the Great Internet Mersenne Prime Search we now have 50 of them and the corresponding perfect numbers. This also helps us to see why the  $M_p$  always end in 1 or 7 and the corresponding perfect numbers end in 6 or 28. That the above are the only even perfect numbers was established by Euler. Unlikely as they seem, there has been no formal proof to show that no odd perfect numbers exist. The modern efforts also show that the perfect numbers are rarer than what the yavana sages thought them to be. The old Pythagorean numerologist Nicomachus states:

*"It comes about that even as fair and excellent things are few and easily numerated, while ugly and vile ones are widespread, so also the abundant and deficient numbers are found in great multitude and irregularly placed -- for the method of their discovery is irregular -- but the perfect numbers are easily enumerated and arranged with suitable order; for only one is found among the units, 6, only one among the tens, 28, and a third in the rank of the hundreds, 496 alone, and a fourth within the limits of the thousands, that is, below ten thousand, 8128."*

The above statement suggests that Nicomachus might have thought that for each decade there is one perfect number. Alternatively, he might have simply stopped counting with the greatest perfect number he knew. That it was the former is strengthened by Platonic siddha (to use Gregory Shaw's term), Iamblichus, even more explicitly claiming that there is one perfect number per decade. That, however, is clearly wrong as the next perfect number 33550336 is in the crores. In any case the modern confirmation of the real rarity of perfect numbers vindicates the philosophical analogy drawn from these numbers by the yavana siddha --- the scarcity of truly perfect things as opposed to the profusion of the supernumerary and the deficient. With respect to the easy enumeration of the perfect numbers Nicomachus and Iamblichus likely meant Euclid's proposition. This rarity of perfect numbers indicates that the sum of the reciprocals should converge to a constant:

 $$\displaystyle C_P=\sum_{j=1}\^n \dfrac{1}{P_j}= \dfrac{1}{6}+\dfrac{1}{28}+\dfrac{1}{496}+\dfrac{1}{8128}... \approx 0.20452014283893...$$ 

It would be immensely remarkable if this constant turns up somewhere in nature.

**Abundant numbers\
**In any case, this  $C_P$  provides a bound for the distribution of abundant numbers and thus, brings us to the point of whether Nicomachus' statement regarding the abundant numbers is really so? This was what we set out to investigate in our youth armed with a rather meager arithmetic knowledge and a computer. Computing the first few abundant numbers yields a sequence like below:\
12, 18, 20, 24, 30, 36, 40, 42, 48, 54, 56, 60, 66, 70, 72, 78, 80, 84, 88, 90, 96, 100, 102, 104, 108, 112, 114, 120, 126, 132, 138, 140, 144, 150, 156, 160, 162, 168, 174, 176, 180, 186, 192, 196, 198, 200, 204, 208, 210, 216...

A few rules become immediately apparent:

 1.  if  $P_j$  is a perfect number then  $k\cdot P_j$  is an abundant number for all  $k \ge 2$ . Thus,  $P_1=6$  initiates a line of abundant numbers 12, 18, 24, 30....  $P_2=28$  initiates 56, 84, 112...


 2.  We notice that beyond these a few abundant numbers emerge in the interstices e.g. 20, 88 etc. A notable class of these emergent abundant numbers are of the form  $2\^k\cdot p_j$ , where  $p_j$  is the  $j^{th}$  odd prime and  $k \ge 2$ . The first few of these are tabulated below.

![aliquot_table1](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_table1.jpg){width="75%"}

These are all shown as red points in panel 1 of Figure 2. One notices that for each jump of  $k$  we get a jump in these abundant numbers. Of these, some like 12, 56 and 992 are already accounted for as doubles of the perfect numbers 6, 28 and 496 but the rest are distinct. Their multiples, as with the perfect numbers, are also further abundant numbers.


 3.  A few further abundant numbers emerge in the interstices, which have a more complex description. They found new lineages of abundant numbers as their multiples continue to be abundant numbers. The first of these is  $70=2\times 5 \times 7$ . 70 is the product of successive odd primes 5, 7 with 2 leaving out 3, which would yield already accounted-for abundant numbers in a product with 2 via the perfect number 6. Another such is  $2002=2\times 7 \times 11 \times 13$ . Here 5 is left out because that will again result in already accounted-for abundants in a product with 7. Likewise, leaving out 7, we have  $1430=2\times 5\times 11\times 13$ . For further numbers in this category we need the next power of 2, e.g.  $9724=2^2 \times 11\times 13\times 17$ .


 4.  Till  $n=1000$  odd abundant numbers are very rare. The  $A\lbrack 232\rbrack =945$  is the first odd abundant number and till  $n=200000$  there are only 391 odd abundant numbers as opposed to 49090 even abundant numbers. Of these first 391 odd ones, 387 are divisible by 15. The remaining 4 which are not namely 81081, 153153, 171171, 189189 are divisible by 9009. All odd abundant numbers necessarily need to have the product of at least 3 distinct odd primes as a divisors. As with other abundant numbers, the multiples of odd abundant numbers are also abundant numbers. Based on the separation between the odd abundant numbers, we observed that the first of them 945 is the first of a lineage of odd abundant numbers, which are generated by the formula:  $A_o=3 \times (315+210k)$ , where  $k=0,1,2...51$ . Similarly, we observed that a related formula  $A_o=11 \times (315+210k)$ , where  $k=0,1,2...192$  produces a continuous run of 193 odd abundant numbers starting from 3465. After  $k=51$  and  $k=192$  respectively these formulae do not necessarily produce abundant numbers; nevertheless numbers emerging from these rules continue to remain enriched in odd abundant numbers. Of course, the other emergent odd abundant numbers might have further less-apparent rules. In any case, it appears the difference between two successive odd abundant numbers is most of the times divisible by  $2 \times 3 \times 5 =30$  and always divisible by the perfect number 6.

Thus, unlike what the yavana siddha-s thought, the abundant numbers are not entirely unruly. They have complex patterns, but can be described by some rules. The biggest bulk of them are children of perfect numbers --- like mortal descendants of the immortal gods. This leads us to the growth of the sequence of abundant numbers, which exhibits striking regularity (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_abundance_figure2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Given that the sum of the reciprocals of the perfect numbers converges to a constant  $C_P$  and given what we have seen above regarding the abundant numbers, they should grow at the upper bound by the equation  $y=\tfrac{x}{C_P} \approx 4.89x$  (the dark red line panel 1 of Figure 2). However, since the progeny of perfect numbers are not the only abundant numbers and several emerge by the other rules mentioned above  $A\lbrack n\rbrack$  should grow at a lower rate. During our initial foray into abundant numbers, by empirical examination we thought this growth rate might be exactly 4 (Figure 2, panel 1, light blue line). However, with better computers and computation of 49481 abundant numbers, we later realized that the rate was actually slightly more than 4 (Figure 2, panel 1, dark green line). Another way of visualizing the same is by defining the density of these numbers as the ratio of the number of abundant numbers  $\le$  to a certain number  $n$  to  $n$ ,

 $$D= \dfrac{\#A\le n}{n}$$ 

This is shown in Figure 2, panel 2, where we see  $D$  converging to a value between 0.248 and 0.247 (red and blue lines). This has been a topic of deep investigation in modern mathematics, starting with the likes of Sarvadaman Chowla and Paul Erdős, and indeed the above has proven to be the approximate bound of the density of abundant numbers. We were quite pleased to have semi-empirically arrived at it for ourselves. Thus, even as the odd and even numbers are distributed in a 1:1 ratio, after initial jitters the ratio of deficient to abundant numbers converges to a constant of  $\approx 4.04$  with new lineages of abundant numbers constantly emerging in the interstices of the established ones to maintain a linear growth at this rate.

**The basic features of the aliquot sequences\
**The process of obtaining aliquot sums of a number  $n$  can be applied iteratively (the aliquot map):  $s(n)=n_1 \rightarrow s(n_1)=n_2 \rightarrow s(n_2)=n_3...$ . The sequence  $ali(n)=n, n_1, n_2, n_3...$  is called the aliquot sequence of  $n$ . Thus, if we start with  $n= 20$  we get the sequence: 20, 22, 14, 10, 8, 7, 1, 0. More generally, by computing aliquot sequences for all numbers from 1:1000, we observe the following:

 1.  The number 1 converges in two steps to 0.

 2.  All primes converge in 3 steps,  $p, 1, 0$ .

 3.  Perfect numbers converge in 1 step to themselves.

 4.  Certain numbers converge to a perfect number. E.g. 25, 95, 119, 143, 417, 445, 565, 675, 685, 783, 909, 913... converge to 6; 608, 650, 652, 790 converge 496. We observe that these numbers have given the appellation "aspiring numbers".


 5.  Notably, 220 converges to 284 and 284 converges to 220.  $ali(562)=562, 284, 220$ . Thus, if from whatever starting point if one reaches 284 or 220 one cycles between them. Such a pair is termed a pair of amicable numbers and was probably known to Platonists as indicated by the [heathen Sabian from Harran, Thabit ibn Kurra](https://manasataramgini.wordpress.com/2006/08/28/the-makings-of-islamic-science/)'s knowledge of these numbers. Thabit discovered a rule to produce a lineage of these numbers, likely drawing on the rule to generate even perfect numbers: Let  $a=3\cdot 2^{n-1}-1, b=3\cdot 2\^n-1, c=9\cdot 2^{2n-1}-1$  and  $n\ge 2$ . If  $a,b,c$  are primes then we can compose the amicable pair as  $a_1=2\^n \cdot ab, a_2= 2\^n \cdot c$ . For  $n=2$  we get the pair 220, 284;  $n=4$  produces 17296, 18416;  $n=7$  produces the pair 9363584, 9437056. However, there are many more amicable numbers between these pairs which cannot be captured by this rule. For example, one can computationally show that 1184, 1210 are an amicable pair. Long after the days of Thabit, starting from Euler down to our times further rules have been found to capture more amicable pairs.


 6.  If perfect numbers are auto-cycles under the aliquot map, the amicable numbers can be considered 2-cycles. While we do not know if all higher cycles exist under the aliquot map, we can computationally find some of them. For example the pentad 12496, 14288, 15472, 14536, 14264 constitute a 5-cycle. Any number in this pentad will cycle through these values. Even more remarkable is this sequence 14316, 19116, 31704, 47616, 83328, 177792, 295488, 629072, 589786, 294896, 358336, 418904, 366556, 274924, 275444, 243760, 376736, 381028, 285778, 152990, 122410, 97946, 48976, 45946, 22976, 22744, 19916, 17716. These constitute a 28-cycle and any number in this 28-ad will cycle between these values. The lowest member of a 2- and greater cycle is always an abundant number.


 7.  Still other numbers will eventually converge to an odd prime and via that prime reach 0. There is a remarkable pattern in terms of the preference of the prime via which convergence occurs (Figure 3). 43 is the most preferred prime for convergence for  $n=1:1000$  (any links to [Heegner numbers](https://manasataramgini.wordpress.com/2017/03/18/euler-and-ramanujan-primes-near-integers-and-cakravala/) or twin primes?). We are not clear as to whether this trend survives with increasing  $n$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_convergence_p_figure3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```



 8.  If we leave out the starting  $n$  will every other integer be reached by rest of the aliquot trajectory of some  $n$ ? We can easily provide the answer to this question as no. For example,  $2=1+1$ . You cannot have two 1s as proper divisors, hence 2 can never be reached from any other number under the aliquot map. Similarly,  $5=4+1=2+3$ . But each of these sums leading to 5 cannot be a  $s(n)$  because we would leave out divisors 2 and 1. Thus, 5 can never be reached from any other number under the aliquot map. On the other hand, a number  $n=p+1$  (where  $p$  is a prime number) is never untouchable because  $s(p^2)=p+1$ . Similarly, a number  $n=p_o+3$  (where  $p_o$  is an odd prime) is never untouchable because  $s(2p_o)=p_o+3$ . Nevertheless, some numbers are not reached easily until one of the above configurations is encountered for the first time. For example,  $ali(1369=37^2)= 1369, 38, 22, 14, 10, 8, 7, 1, 0$  contains 38=37+1 for the first time. Similarly,  $ali(2209=47^2)=2209, 48, 76, 64, 63, 41, 1, 0$  contains 48=47+1 for the first time. But the number 52 lies a in sweet spot as as neither 51=52-1 nor 49=52-3 are primes and is untouchable from any other number under the aliquot map. These are some obvious examples of untouchability and touchability; Erdős has shown that ultimately there are an  $\infty$  of cases.

**Patterns in the convergence length of aliquot sequences\
**Are these the only convergence patterns seen in the aliquot sequences? Is there any further pattern to the number of iterations needed to converge? To address these questions we can define a further sequence  $f\lbrack n\rbrack$  where each element is the number of iterations taken by the integer  $n$  to converge. The plot of  $f\lbrack n\rbrack$  for the first 1000 elements is shown in Figure 4.

![aliquot_convergence_l\_figure4](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_convergence_l_figure4.png){width="75%"}Figure 4

The picture is quite remarkable --- the majority of values are rather pedestrian but from time to time there are huge eruptive values. The perfect numbers (  $f\lbrack n\rbrack =1$ ), 1 and the primitive aspiring numbers, e.g. 25 (  $f\lbrack n\rbrack =2$ ), and primes (  $f\lbrack n\rbrack =3$ ) form the lowest values of  $f$ .  $f\lbrack n\rbrack =3$  might also be reached by certain secondary aspiring numbers like 95 or 119. Of these the primes are the most common. It is quite obvious that even numbers have significantly longer convergence paths than odd numbers (  $p=10^{-10}$ ; Figure 5). We also observed that on an average the abundant numbers have significantly longer convergence paths than the remaining numbers (  $p=5.3 \times 10^{-9}$  for  $n=1:1000$ ). This holds even after the primes are removed from the non-abundant numbers (  $p= 1.9 \times 10^{-8}$  for  $n=1:1000$ ). Similarly, if we compare even abundant numbers and even non-abundant numbers, which occur in the roughly similar counts, the abundant even numbers still have significantly longer trajectories the non-abundant even numbers under the aliquot map (  $p=3.2 \times 10^{-7}$ ). Due to the relative rarity of odd abundant numbers, these effects are likely to persist beyond  $n=1000$ . The basic statistics for the convergence trajectory lengths for different types of numbers from  $n=1:1000$  are tabulated below and summed up in Figure 5.

\begin{tabular}{|l|r|r|r|} \hline Number & Median & Mean & Max \\ \hline Even & 12 & 21.7 & 749 \\ \hline Odd & 4 & 4.9 & 17 \\ \hline Abundant & 15 & 34.04 & 749 \\ \hline Non-abundant & 5 & 6.76 & 25 \\ \hline Non-abundant Non-p & 7 & 7.84 & 25 \\ \hline Even-abundant & 15 & 34.16 & 749 \\ \hline Even-non-abundant & 10 & 10.36 & 25 \\ \hline \end{tabular}

![Aliquot_abundant_l\_box_Figure5](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_abundant_l_box_figure5.png){width="75%"}Figure 5

During our initial foray into the first 200 terms of this sequence it appeared that certain integers, like 138, never converged under the aliquot map. Going on till  $n=1000$  more such terms appeared; however, better computation showed us that the aliquot sequences for some of these  $n$  indeed converge after a large number of steps after growing to monstrous values. It was then that we learned that the mathematicians have been studying this problem quite a bit computationally starting with Derrick Lehmer-II (the son of the father-son pair of arithmeticians), one of the doyens of computational mathematics. He was the first who, back in his days after what was a tough fight, showed that  $ali(138)$  indeed converges. For us, the hardest nut to crack for  $n=1:1000$  was  $f\lbrack 840\rbrack$ . It simply would not complete in our laptop at home even after running it for over a day with an efficient divisor finding function. Nor did it complete on our primary work station used for most of routine computations. Then we had to bring out our इन्द्रास्त्र, a 120 core machine that we use for big things. For that we had to first write a multi-threaded version, which was run on 100 cores of this machine and it completed  $f\lbrack 840\rbrack$  overnight. We term these large  $f\lbrack n\rbrack$  as the monster numbers. For  $n=1:1000$  there are 18 of them, which come in the below-tabulated families, all of which are abundant numbers descending from 6. After the first few terms the members of each family follow the same trajectory to convergence. The final prime via which they converge is termed  $p_c$ 

![aliquot_table6](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_table6.jpg){width="75%"}
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_monster_values_figure6.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad The evolution of the first members of each family.  $f_{840}$ : dark red  $f_{138}$ : cyan;  $f_{702}$ : dark blue;  $f_{720}$ : dark green;  $f_{858}$ : hot pink;  $f_{936}$ : gray.
```{=latex}
\end{center}
```


One notices that families  $f_{138}$  and  $f_{858}$  have a higher-order relationship because they converge via 59. However, we keep them as different families for, barring the last 8 elements of  $ali(n)$ , they followed very different trajectories (Figure 6). Their  $p_c$  is the 6th most common  $p_c$  for  $n=1:1000$ , with 45 numbers converging via it (Figure 3). However, 12 of them being monster values it is one  $p_c$  over-represented in the aliquot sequences attaining monster values. In contrast, the most common  $p_c$  in this range 43 (Figure 3) has only a single monster value (936) in this range.  $ali(840)$  shows the most monstrous behavior in this range (Figure 6). It reaches a maximum value of the behemoth number: 3463982260143725017429794136098072146586526240388. I felt please on reaching this number and experiencing the magnificence of  $ali(840)$  for myself. After that it remains in the high range for while with two prominent peaks before converging after 749 steps. Such a behavior, with a multiplicity of peaks before convergence, is also seen in the other monstrous families (Figure 6).

We wondered if there was any features of these sequences, which allowed their eruptive growth before finally converging. We observed the following:

 1.  They start as even abundant numbers, which means that they are statistically in the general group of numbers which are going to have longer trajectories to convergence.

 2.  Their trajectories tend to remain even, which again increases their probability of having a long convergence path. By specifically studying the first major eruptive case  $ali(138)$ , we see that it has a convergence trajectory of length 179. It remains even from  $ali(138)\lbrack 1\rbrack$  to  $ali(138)\lbrack 175\rbrack$ . Thus, remaining even is a key factor for a long convergence path.

 3.  The next key feature observation regarding their growth came from examining the behavior of the first two cases which show above average behavior. These are  $f(30)=16$  and  $f(102)=19$ . First, both are even abundant numbers satisfying the above condition. Then we note their behavior under the aliquot map:

 $$ali(30)=30, 42, 54, 66, 78, 90, 144, 259, 45, 33, 15, 9, 4, 3, 1, 0$$ 

 $$ali(102)=102, 114, 126, 186, 198, 270, 450, 759, 393, 135, 105, 87, 33, 15, 9, 4, 3, 1, 0$$ 

We see that both of them have a run of 7 continuous abundant numbers, which are multiples of the perfect number 6. This allows a monotonic increase. When they lose this divisor 6 in the 8th iteration they become odd and start falling rapidly and converge. We then looked if this pattern might play out in a truly eruptive example by taking the case of  $ali(138)$ . We see that from  $ali(138)\lbrack 1\rbrack =138$  to  $ali(138)\lbrack 30\rbrack =1467588$ , then  $ali(138)\lbrack 65\rbrack =19406148$  to  $ali(138)\lbrack 72\rbrack =348957876$  and again  $ali(138)\lbrack 95\rbrack =1013592$  to  $ali(138)\lbrack 117\rbrack =100039354704$  each number is a multiple of the perfect number 6. The repeated generation of a multiple of a perfect number under the aliquot map means there can be stretches of unhindered growth with one abundant number succeeding the previous one until that perfect number is lost among the divisors. It is the final loss of 6 and then 2 among the divisors that allows  $ali(138)$  to converge. In making these observations we had recapitulated Lehmer's key findings in the context of the growth of the most mysterious class of aliquot sequences we shall talk about next.

There are 12  $f\lbrack n\rbrack$ , for  $n \le 1000$ , which could not be determined because the corresponding  $ali(n)$  never converged in our computation (the blue dots in Figure 4). A search on the Internet reveals that deep computational efforts by various investigators have not seen an end to these sequences. They belong to 5 families which are known after Lehmer-II who first studied them. Within each family (listed below), after the first few terms the evolution converges to the same trajectory (Figure 7). These families are:

 $$f_{276}=276, 306, 396, 696$$ 

 $$f_{552}=552 ,888$$ 

 $$f_{564}=564,780$$ 

 $$f_{660}=660,828,996$$ 

 $$f_{966}=966$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2018/07/aliquot_lehmer5_figure7.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad  $f_{276}$ : dark red;  $f_{552}$ : dark blue;  $f_{564}$ : dark green;  $f_{660}$ : hot pink;  $f_{966}$ : cyan\
```{=latex}
\end{center}
```

All these numbers are abundant numbers descending from 6, which is a tendency they share with the above monster numbers that eventually converge. The frequencies of their largest prime factors are tabulated below:

\begin{tabular}{|r| r|r|r|r|r|r|r|r|r|} \hline p & 7 & 11 & 13 & 17 & 23 & 29 & 37 & 47 & 83 \\ \hline freq & 1 & 2 & 1 & 1 & 4 & 1 & 1 & 1 & 1 \\ \hline \end{tabular}

Notably, in this set except  $f_{564}$ , 4 of the five families have a member with 23 as the highest prime factor. It is now conjectured by mathematicians that these define the founding members of a class of aliquot sequences that would never converge but eventually shoot off to infinity. Examining the eruptive growth of these sequences, we found that the same features that cause the eruptive growth of the converging but monster  $n$  hold true here too. Thus, if we consider the first case  $ali(276)$ , which starts with a double of the first monster  $n=178$ , we notice that it starts off well with the perfect number 6 as the divisor for the first 5 terms successive terms ---  $ali(276)\lbrack 1\rbrack =276$  to  $ali(276)\lbrack 5\rbrack =1872$ . But then the sequence loses 6 in the 6th term a stutters for a few terms even going down from the high point it reached. But then in  $ali(276)\lbrack 9\rbrack =2716$  it acquires a new perfect number divisor 28, which drives the abundance of successive terms until the it reaches the giant 28 digit  $ali(276)\lbrack 170\rbrack =7165455981799192761913565252$ . Lehmer's study was the first which noted the role of such persistent divisors in driving up the aliquot sequences. Accordingly, the grand old man of arithmeticians, Richard Guy, called these 'drivers' and in addition to 2 and the perfect numbers defined a few more of such. What seems to happen is that in these apparently non-converging aliquot sequences the drivers drive the growth to some large number after which they are lost. But before the sequence can crash by attaining an odd number, a new driver emerges and pushes up its growth again. Thus, these sequences might not converge. On the other hand the case of the monstrous  $ali(840)$  with  $f=749$  tells us that even after many such runs there is some chance of hitting a number, which brings you crashing down though its even almost all the way through. Whatever the case, the aliquot sequence of these non-converging numbers tell us that there are several more numbers in their trajectories, which would similarly not converge. Hence, non-convergence will be encountered frequently as the starting numbers get large.

In conclusion, following along the lines of the ancients, we find that the aliquot sequences offer us some philosophical lessons and mysteries:

 1.  There is the predictable: The primes and perfects display very predictable and uniform behavior.

 2.  There is the somewhat predictable: like the amicable numbers, for some of which a rule was found as early as Thabit ibn Kurra. But not all of them can be captured by an easy rule.

 3.  There is the statistically tendency: We know that an even or abundant number is more likely to have a longer path to convergence though it is not clear at all if any rule can say which of them might be monstrous and which rather common place.

 4.  There is the unpredictable and mysterious: Why are some numbers "aspiring numbers" which converge to a perfect number? Is there any rule to describe them? Why is 43 the preferred prime for convergence at least for  $n=1:1000$ . Why do some numbers not converge? Can we predict which numbers will not converge? Do they really not converge at all? As far as I know these remain unanswered. Thus, a very simple arithmetic process gives rise to a whole range of unexpected behavior.

