
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on the Tantric state among the chIna-s and recovery of a lost वैनायक ritual](https://manasataramgini.wordpress.com/2012/01/15/a-note-on-the-tantric-state-among-the-china-s-and-recovery-of-a-lost-vainayaka-ritual/){rel="bookmark"} {#a-note-on-the-tantric-state-among-the-china-s-and-recovery-of-a-lost-वनयक-ritual .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 15, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/01/15/a-note-on-the-tantric-state-among-the-china-s-and-recovery-of-a-lost-vainayaka-ritual/ "8:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-sRPqhOm8Rbk/TxKOz3BdQZI/AAAAAAAACVI/NHufe1zHmVo/s400/chIna_विनायक.jpg){width="75%"}
```{=latex}
\end{center}
```



In 705 CE, amoghavajra was born in Samarkand to a ब्राह्मण teacher from either Prayag or Kashi and his Iranian wife. His father died when he was 10 years old and he moved to the चीनदेश with his mother and uncle. There he became a student of the great nAstika तान्त्रिक vajrabodhi, a ब्राह्मण from Kanchipuram, who settled in चीनदेश after a long journey in the east. From Kerala, vajrabodhi had gone to Shrilanka and from there he went to the Nicobar islands. From there he went to Sumatra and finally reached Kwangchow, where he became a guru of the chIna princess Tu-ku-kuei-fei. Along with amoghavajra and his Korean student Hyecho प्रज्ञाविक्रम he taught several aspects of the तान्त्रिक lore to numerous chIna students. In 730s amoghavajra succeeded vajrabodhi as the guru of the realm. In 741 CE the nationalist legalistic Han faction had an edict passed to kick out all Indian and Korean guru-s and students from China. In this period amoghavajra went to Java and then to India to learn under different आचार्यस् in the high universities of भारतवर्ष. In 746 CE he was called back to चीनदेश to perform तान्त्रिक rituals for the emperor of all chIna-s Xuan-zong and the princess Hua-yang was made his adopted daughter. In 751 CE the chIna army at peak of its military glory came to face to face with the Islamic jihad at Talas. The result was disastrous for the former and the Tang war machine met one of its worst defeats in a long time. This was quickly followed by another drubbing at the hands of the Qarluq Turks led by their yabghu Tun Bilge. On the other side of the chIna world, the Tang generals attempted and invasion of Thailand and met yet another crushing defeat at the hands of the rAjA of Thailand. This shook the Tang empire paving the way for the eventual rebellion of the Irano-Mongolian रोक्षन (An Lu-shan). In course of these events the defense of the Tang border zones came under prince Wumin of Xiping, who invited amoghavajra to perform अभिचारिक rituals. Fearing his तान्त्रिक rites, in course of his takeover of the chIna throne, रोक्षन's captured amoghavajra, while he was translating the tattva-संग्रह, a yoga tantra. Subsequently, his patron prince Wumin was also captured and killed by रोक्षन's son. But in 757 CE the chIna forces defeated the son of रोक्षन and killed him to restore Tang power, giving a chance for amoghavajra to escape. However, shortly thereafter the chIna-s suffered a defeat at the hand of the Uighur Turks and faced an attack from the Tibetans. At this point the Tang emperor Su-zong called upon amoghavajra to perform great तान्त्रिक rituals to purify the palace of the emperor and a जयाभिशेक for the survival of the Tang empire. In these rituals he deployed secret mantra-s to the विनायक यामल to prevent vighna-s from afflicting the chIna-s. After the Tang revived their fortunes in 760 CE, amoghavajra performed a complex तान्त्रिक ritual in which he crowned Su-zong as the chakravartin सार्वभौम. Shortly thereafter, he was joined by a ब्राह्मण from Kashmir named प्रज्ञाचक्र, whom he initiated into a secret ritual of the chatur-विनायक-मण्डल. In 765 CE he is supposed to have performed a long अभिचार rite on behalf of Su-zong right using the मञ्जुश्री कुमारभूत mantra to kill the Uighur Kha'khan. Thus, amoghavajra became a highly regarded तान्त्रिक and was declared minister of the realm of the chIna-s, and was declared प्रज्ञाकोश or the treasury of knowledge by the chIna emperor. Both amoghavajra and his successor प्रज्ञाचक्र were rAja-guru-s for Su-zong and his successor Dai-zong (on whose behalf amoghavajra had performed apotropaic rites when he was a general during the रोक्षन rebellion).

One of the secret विद्या-s that he promulgated were a set of rituals to गणpati and his shakti whose originals appear to have been lost in India. But they are of greatest significance in understanding the गाणपत्य ritual; hence we describe them here. An account of the chIna Hanguang mentions that he received दीक्ष in the secret साधन of गणpati and his shakti from amoghavajra in 747 CE. He mentions that the prayoga was so secret that neither the mantra-s nor the मण्डल and rites were recorded. The says the rites covered पुष्टिक, शान्तिक, वशीकरण, अभिचारिक, आकर्षण, and Ayuvardhana, i.e. a particular षट्कर्मन् encompassing both apotropaic, lifespan increasing and attacking rituals. He only briefly elaborates on the root bIja of विनायक being गः (a nAstika form of the original gaM from the Astika tradition). He then tries to provide a bauddha overlay to explain the nature of विनायक -- he claims that both विनायक and avalokiteshvara emerged as emanations from the dharmakAya (the pervasive body) of the cosmic buddha vairochana (the deity in वैरोचनाभिसंबोधि tantra). Then avalokiteshvara transformed into a female and became the shakti of विनायक to pacify him. Hence, they are worshiped as a couple. Jing-se in his vidhi provides a few further details of the transmission of amoghavajra: He says that the विनायक-s might be worshiped as: 1) the विनायक यामल -- where विनायक is conjoined with his shakti. In this depiction his shakti is clearly depicted in a proboscicephalic form. It is clear from the नवार्ण मूलमन्त्र of the original गाणपत्य tradition promulgated by the great आचार्य herambhasuta in India (the element "हस्तिपिशाचि likhe") that this is what was originally intended, although in the later period in India an entirely anthropomorphic shakti became the norm.

 2.  the षड्भुज विनायक -- this form is depicted above from the original drawing of vajrabodhi or a copy there of. Tradition holds that vajrabodhi was a skillful iconographic artist.\
3&4) are chaturbhuja विनायक-s.

 5.  The 4 seizing विनायक-s as ordained by the परिशिष्ठ of the atharvaveda and the मानव गृह्य sUtra of the मैत्रायणीय tradition. This was already taken up by the nAstika-s and incorporated into the सुबाहु-परिपृच्छ, a text from the [क्रिया tantra](https://manasataramgini.wordpress.com/2009/02/16/nastika-notes-2/) layer.

It is regarding this last version of the 4 विनायक-s that प्रज्ञाचक्र the student of amoghavajra records a remarkable ritual.This ritual is termed the ritual of the magical plates for the deva Amoda sUrya (विनायक). He first makes the circular heaven plate about 3-4 अङ्गुल-s in diameter and then the square earth plate with its side 7 अङ्गुल-s from a white fragrant wood on an auspicious day. He avoids seeing evil-doers when he is preparing and painting this yantra. He fixes two handles to the heaven plate and connects the two plates with an axial pin. On the heaven plate facing east he writes the bIja AM in a leaf and visualizes a red Amoda-विनायक on a jade seat with a red body and wrathful face. He is surrounded by many troops and holds a वैडूर्य stone and a spike. To the south in a leaf he writes the कूट jI and visualizes Amoda काम विनायक holding a modaka and a radish, seated on a ruby throne. He is surrounded by 18 crore hordes yelling like jackals. To the west in a leaf he inscribes ह्रीः and conceives Amoda soma विनायक with 104 thousand crore troops. He is seated on a tiger skin seat and is known as ekadanta. On the northern side in a leaf the कूट mA is inscribed and वागीश्वर गणेश is conceived seated on a and of the blue green color with his hands showing the asi-mudra seated on a black stone throne with कोटि-ayuta गण-s around him. In the center of this plate he may place an image of rudra. On the earth plate in the directions the following deities are inscribed indra (E); agni (SE); yama (S); निरृती (SW); वरुण (W); वायु (NW); वैश्रवण (N); rudra (NE). These are surrounded by the 28 नक्षत्र-s and 36 animals. He then displays the special विनायक mudra to call the for विनायक-s. Then he invites the 4 seizing विनायक-s with the mantra "[ऒं वक्रतुण्डादिपत्ये स्वाहा ॥]{style="color:#0000ff;"}". Then he deploys a certain वक्रतुण्ड माला mantra and displays a the vishva mudra. Then he shows the तेजोराशी mudra and invokes the deva-s in the earth plate and utters the incantation ("[ॐ शत्रुप्रमर्दिने स्वाहा ॥]{style="color:#0000ff;"}"). He then shows the कुण्ड-mudra and invokes the नक्षत्र-s and the 36 animals with a mantra that we are unable to decipher. In the chIna transliteration it goes: "OM chi-ri-an-gi-ru-ni-ye स्वाहा ||". He then shows the vajra mudra and invokes the multitude of गण-s of विनायक with a माला mantra. Then he shows the bhakti mudra.

He performs a digbandha by tying a boundary thread all around the ritual area and uttering the mantra: "[OM ki ye ye ye huM ||]{style="color:#0000ff;"}". To deploy the yantra he rotates the विनायक on the heaven plate to coincide with a deva on the earth plate. Then he casts and incantation for that particular prayoga. This mantra is not entirely decipherable and ends in [हुं हुं किलि किलि हुम् फट् स्वाहा ॥]{style="color:#0000ff;"}. It is accompanied by a display of the अङ्कुश and खड्ग mudra.

  - If he seeks to rise in stature then he may unites the sUrya विनायक with indra.

  - If he seeks to perform an अभिचार to cause fever to a shatru then he may unite the sUrya विनायक with agni.

  - If he seeks to get wealth then he may unite the soma विनायक with वैश्रवण.

  - If he wants to induce a person to love him he unites the soma विनायक with वैश्रवण and invokes the नक्षत्र-s thereafter

  - If he seeks to win a war then he unites the sUrya विनायक with rudra and invokes the hordes of rudra.

  - If one wants good crops he unites वागीश्वर विनायक with indra.

  - If he wants to remove bad dreams he unites sUrya विनायक with yama.\
Thus, there are numerous prayoga-s for different purposes. It is said that it was transmitted from India to China. The mantra manual ends with an equation of the गणpati-s on the heaven plate with the 4 seizing विनायक-s.\
This text was also subsequently transmitted to Japan which had vigorous [गाणपत्य tradition](https://manasataramgini.wordpress.com/2005/04/30/a-japanese-ganapatya-text/).

The recovery of this ritual from chIna sources is important because it belongs to the class of texts associated with the transition from the seizing विनायक-s to the classical विनायक-s. It is of interest to note that the text describes the 4 विनायक-s of being of the same basic substance. This is reminiscent of the the याज्ञवाल्क्य ritual stating that the 4 or 6 seizing विनायक-s are essential manifestation of a single विनायक, उमासुत.


