
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The मातृका krama](https://manasataramgini.wordpress.com/2012/12/20/the-matrika-krama/){rel="bookmark"} {#the-मतक-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 20, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/12/20/the-matrika-krama/ "7:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-KqTpK6jJqmU/UNdLLUTP71I/AAAAAAAACgE/O-cneayo4vc/s800/mAtR%255Eikas7.jpg){width="75%"}
```{=latex}
\end{center}
```



The worship of the मातृका-s lies at the root of the kaula system. We have earlier discussed the links of the मातृका worship with the कौमार system. The presence of female deities, later explicitly termed मातृका-s or योगिनी-s, in the retinue of rudra is alluded to even in the Vedic saMhitA-s. Indeed, those mantra-s are central to the मातृका krama, which will be described below. This मातृका krama is a basic ritual for the worship of the seven मातृका-s who constitute to the मातृकाणां-kula-chakra. The additional deities in the chakra are वीरभद्र or वीरक and rudra. The seven मातृका-s and वीरभद्र/वीरक are placed in an octagon around the central rudra.

He may perform the rite during the day in his देवगृह, or at night in a secluded spot or cremation ground. He procures a yoni-shaped lamp (योन्याकार दीप) and lights it with the tip of the lamp facing west. He himself is seated facing east. If he is accompanied by a consecrated (दीक्षिता) कुलाङ्गना, she sits facing west opposite to him. They then recite the following to initiate the rite:

[शुक्लाम्बरधरं विष्णुं शशि वर्णं चतुर्भुजम् ।]{style="color:#99cc00;"}\
[प्रसन्न वदनं ध्यायेत् सर्व विघ्नोप शान्तये ॥]{style="color:#99cc00;"}

[अथ मातृका-क्रमं करिष्यामि सिद्धिर् भवतु मे सदा ॥]{style="color:#99cc00;"}

For each goddess/वीरभद्र he first begins with the following invocation:\
[ब्राह्मीम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[माहेश्वरीम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[कौमारीम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[वैष्णवीम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[वराहीम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[इन्द्राणीम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[छामुण्डाम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[वीरभद्रम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}

After the invocation formula specific to each goddess in the above list he follows it with the performance of पञ्चोपचार पूजा for her thus:\
Scent offering (He smears sandal paste on the योन्याकार दीप): [लं पृथिव्यात्मिकायै गन्धं समर्पयामि ।]{style="color:#99cc00;"}\
Flower offering (He places flowers around the योन्याकार दीप):[हं आकाशात्मिकायै पुष्पैः पूजयामि ।]{style="color:#99cc00;"}\
Incense offering (He waves the incense stick before the योन्याकार दीप): [यं वाय्वात्मिकायै धूपमाघ्रायामि ।]{style="color:#99cc00;"}\
The lamp offering (He waves burning camphor before the योन्याकार दीप): [रं अग्न्यात्मिकायै दीपं दर्शयामि ।]{style="color:#99cc00;"}\
The food offering (He offers naivedya of sugar cane juice, वटक-s etc or whatever food offering he has): [वम् अमृतात्मिकायै अम्रित-महानैवेद्यम् निवेदयामि ।]{style="color:#99cc00;"}\
The total offering (He bows to the योन्याकार दीप): [सं सर्वात्मिकायै सर्वोपछार-पूजां समर्पयामि ।]{style="color:#99cc00;"}

He then offers तर्पण-s uttering the formula specific to the given देवी:\
OM + ब्राह्मीं; माहेश्वरीं; कौमारीं; वैष्णवीं; वाराहीं; इन्द्राणीं; चामुण्डां + तर्पयामि | (3 or 10 X)

When he comes to वीरभद्र he performs the पञ्चोपचार पूजा for him thus:\
[लं पृथिव्यात्मने गन्धं समर्पयामि ।]{style="color:#99cc00;"}\
[हं आकाशात्मने पुष्पैः पूजयामि ।]{style="color:#99cc00;"}\
[यं वाय्वात्मने धूपम् आघ्रापयामि ।]{style="color:#99cc00;"}\
[रं अग्न्यात्मने दीपं दर्शयामि ।]{style="color:#99cc00;"}\
[वम् अमृतात्मने अमृतं महानैवेद्यं निवेदयामि ।]{style="color:#99cc00;"}\
[सं सर्वात्मने सर्वोपछारपूजां समर्पयामि ।]{style="color:#99cc00;"}

He follows this with the तर्पण of वीरभद्र:\
[ऒं वीरभद्रं तर्पयामि ।]{style="color:#99cc00;"} (3/10 X)

If accompanied by his कुलाङ्गना, he performs the secret ritual of pleasure accompanying each of the उपचार, which the kaula ritualist never reveals to the public. For this rite he invokes the goddesses in the दूती and वीरभद्र in himself. For success in this practice, it is important that he never lose focus visualization of the goddesses and वीरभद्र in the organs. Moreover, his दूती should necessarily have the instruction and दीक्ष in the yoga of the offering to the deities in their respective organs and should do so both during the पञ्चोपचार पूजा at the utterance of each तर्पण formula. Tradition holds that a veda-knowing ब्राह्मण offers regular naivedya, while a तान्त्रिक ब्राह्मण or member of the remaining castes might offer kuladravya-s as specified in the the त्रिपुरार्णव tantra of the श्रीकुल tradition. The meditation of the goddesses/वीरभद्र in the organs is done as per the following scheme:\
brAhmI -- East -- brain\
माहेश्वरी -- Southeast -- heart\
कौमारी -- South -- mind\
वैष्णवी -- Southwest -- ears\
वाराही -- West -- skin\
इन्द्राणी -- Northwest -- eyes\
चामुण्डा -- North -- tongue\
वीरभद्र -- Northeast -- nose

After the तर्पण of each deity he then recites the below mantra each time followed by the specific मातृका mantra:\
[नम आव्यधिनीभ्यो नमः । विविध्यन्तीभ्यश्च नमः । वो नमः । नम उगणभ्यो नमः । तृंहतीभ्यश्च नमः । वो नमः ॥]{style="color:#99cc00;"} (c.f. शतरूद्रीय, TS).

If he is an अथर्ववेदीन् he instead utters the mantra:\
[नमस् ते घोषिणीभ्यो नमस् ते केशिनीभ्यः ।]{style="color:#99cc00;"}\
[नमो नमस्कृताभ्यो नमः संभुञ्जतीभ्यः ।]{style="color:#99cc00;"}\
[ namas te जायमानायै जातायै uta te नमः ||]{style="color:#99cc00;"} (c.f. AV-P 16.107.1b)

The specific मातृका mantra-s are:\
brAhmI: [ऒं भूर् भुवस् स्वः । तत् सवितुर् वरेण्यं भर्गो देवस्य धीमहि । धियो यो नः प्रछोदयात् । ब्राह्म्यै हुं फट् नमः ॥]{style="color:#99cc00;"}

माहेश्वरी: [ॐ भुवः । ॐ भूर् भुवस् स्वः । तत् सवितुर् वरेण्यं भर्गो देवस्य धीमहि । धियो यो नः प्रचोदयात् । माहेश्वर्यै हुं फट् नमः ॥]{style="color:#99cc00;"}

कौमारी: [क्वाम् ॐ नमो भगवत्यै कौमर्यै शिखिवहन्यै मां पालय पालय सकलासुरमर्दिनि एह्य् एहि रिपून् मर्दय मर्दय हुं हुं फट् स्वाहा ॥]{style="color:#99cc00;"}

वैष्णवी: [ॐ वं सं तत् सवितुर् वरेणियं भर्गो देवस्य धीमहि । धियो यो नः प्रचोदयात् । वैष्णव्यै हुं फट् नमः ॥]{style="color:#99cc00;"}

वाराही: [वं वाराह्यै नमः ॥]{style="color:#99cc00;"}

इन्द्राणी: [ॐ भूर् भुवस् स्वः । इन्द्रानी मासु नारिसु सुपत्नी महमश्रवम् । न ह्यस्या ऽपरम् च न जरसामरते पतिः । स्वः भुवः भूर्^ॐ नमः ॥]{style="color:#99cc00;"}

चमुण्डा: [ह्रीं नमो भगवत्यै चामुण्डीश्वर्यै । सर्व-शत्रु-विनाशिनी सर्व-लोक-भयंकरी एह्येहि मां रक्ष रक्ष मम शत्रून् भक्षय भक्षय हुं फट् नमः ॥]{style="color:#99cc00;"}

For the deployment of each of the above mantra-s the deshika performs the visualization of the respective goddess by reciting the below dhyAna verses. If he is accompanied by his दूती he invokes the goddess into her and also visualizes the goddess in her.

brAhmI:\
[रक्तां रक्त-सुमाल्य-लेप-वसनां भूषादिभिर्-भूषितां ।]{style="color:#99cc00;"}\
[शुद्धां स्मेर-चतुर्-मुखीं शुभतरग्रीवां द्विनेत्रांचलां ॥]{style="color:#99cc00;"}\
[देवीं दण्ड-कमण्डलुं-स्रुव-स्रुचाक्ष-स्रग्-करांभोरुहां ।]{style="color:#99cc00;"}\
[ब्राह्मीं भक्त-जनेष्टदान-निरतां वन्दे सुहंसासनां ॥]{style="color:#99cc00;"}

माहेश्वरी:\
[श्वेताभां श्वेत-वस्त्रां कुचभरनमितां श्वेत-माल्याभिरामाम् ।]{style="color:#99cc00;"}\
[त्रेधा शूलं कपालं कनक-मणि-जपास्रग्-वहन्तीं कराब्जैः ॥]{style="color:#99cc00;"}\
[वन्दे ऽनड्वाह-रूढां स्मित-विशद-चतुर्-वक्त्र-पद्मां त्रिनेत्रां ।]{style="color:#99cc00;"}\
[माहेशीं स्निघ्ध-केशीं मणिमय-वलयां मन्यु-युक्तां प्रसन्नां ॥]{style="color:#99cc00;"}

कौमारी:\
[पीताभां शक्ति-खड्गाभय-वर-सुभुजां पीतमाल्यां शुकाढ्यां ।]{style="color:#99cc00;"}\
[देवीं बर्हाधिरूढां हिमकर-शकला बद्ध-नीलालकान्तां ॥]{style="color:#99cc00;"}\
[नाना-रत्नैर् विरचितैर् अनुखचित-महाभूषणैर् भूषिताङ्गां ।]{style="color:#99cc00;"}\
[ध्यायेत् सर्वार्थ-सिद्ध्यै प्रणत-भयहरां दिव्यहारां कुमारीं ॥]{style="color:#99cc00;"}

वैष्णवी:\
[मरकत-मणि-नीला नीलवासोवसाना ।]{style="color:#99cc00;"}\
[विधृत मणि-जपास्रग्-कंबु-चक्राम्बुजाढ्या ॥]{style="color:#99cc00;"}\
[छपल-गरुडारुढा सर्वतो वक्त्र-पद्मा ।]{style="color:#99cc00;"}\
[दिशतु सुमघदेयी जिवनी वैष्णवी नः ॥]{style="color:#99cc00;"}

वाराही:\
[वज्रोपलसमां नीलां प्रसन्नां महिषासनां ।]{style="color:#99cc00;"}\
[सर्व-दुखहरां घोरां स्तब्धलोमाननां नुमः ॥]{style="color:#99cc00;"}

इन्द्राणी:\
[हेमाभां वरमालिकाभिरछितां हेमां प्रसन्नाननां ।]{style="color:#99cc00;"}\
[नाना-रत्न-विभूषितां नव-नवां नाथां हरीशादिनां ॥]{style="color:#99cc00;"}\
[मायातीत-पराक्रमां मद-गज-स्कन्धे-निबद्धासनां ।]{style="color:#99cc00;"}\
[वन्दे नाग-विनाशिनीं वनकरं वज्रायुधोद्यत्-करां ॥]{style="color:#99cc00;"}

चामुण्डा:\
[ध्यायेत् प्रेतासनस्थां मुसल-शर-गदा-खड्ग-कुन्तास्य्-अभीतीः ।]{style="color:#99cc00;"}\
[गोत्रादानं शरासं-परिघ-जलधिकं-पाश-खेटं-वरं च ॥]{style="color:#99cc00;"}\
[हस्तांभोजैर् वहन्तीं शशि-रवी-नयनां अक्ष-वर्णात्म-रूपां ।]{style="color:#99cc00;"}\
[चामुण्डां मुण्डमालां सकल-रिपुहरां श्यामलां कोमलाङ्गीं ॥]{style="color:#99cc00;"}

For वीरभद्र he uses the following mantra (he invokes वीरभद्र in his own forehead):\
[नमो गणेभ्यो नमः । गणपतिभ्यश् च नमः । वो नमः ॥]{style="color:#99cc00;"}\
An अथर्ववेदीन् instead utters the incantation:\
[रुद्रस्यैलबकारेभ्यो ।असंसूक्तगिलेभ्यो ।अकरं नमः ॥]{style="color:#99cc00;"}

Any of the above are followed by:\
[ॐ वीरभद्राय वैरिवंश-विनाशाय सर्वलोकभयंकराय भीमावेषय हुं फट् श्म्रौं श्म्रौं ह्रीं खं फट् रिपून् फट् नमः ॥]{style="color:#99cc00;"}

He visualizes वीरभद्र thus:\
[मरकत-मणि-नील किङ्किणी-जाल-मालम् ।]{style="color:#99cc00;"}\
[प्रकटित-मुखम्-ईशं भानु-सोमाग्नि-नेत्रम् ।]{style="color:#99cc00;"}\
[अरि-दरम्-असि-खेटत्य्-अग्र-मुण्डाग्र-हस्तम् ।]{style="color:#99cc00;"}\
[विधुधरम्-अहि-भूषं वीरभद्रं नमामि ॥]{style="color:#99cc00;"}

Then he invokes rudra in the flame of the योन्याकार दीप:\
[भवम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[शर्वम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[इशानम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[पशुपतिम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[रुद्रम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[उग्रम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[भीमम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}\
[महादेवम् आवाहयामि ध्यायामि नमामि ।]{style="color:#99cc00;"}

He then performs the पञ्चोपचार:\
[लं पृथिव्यात्मने गन्धं समर्पयामि ।]{style="color:#99cc00;"}\
[हं आकाशात्मने पुष्पैः पूजयामि ।]{style="color:#99cc00;"}\
[यं वाय्वात्मने धूपम् आघ्रापयामि ।]{style="color:#99cc00;"}\
[रं अग्न्यात्मने दीपं दर्शयामि ।]{style="color:#99cc00;"}\
[वम् अमृतात्मने अमृतं महानैवेद्यं निवेदयामि ।]{style="color:#99cc00;"}\
[सं सर्वात्मने सर्वोपछारपूजां समर्पयामि ।]{style="color:#99cc00;"}

Then he performs the तर्पण:\
[भवं तर्पयामि ।]{style="color:#99cc00;"}\
[शर्वं तर्पयामि ।]{style="color:#99cc00;"}\
[इशानं तर्पयामि ।]{style="color:#99cc00;"}\
[पशुपतिं तर्पयामि ।]{style="color:#99cc00;"}\
[रुद्रं तर्पयामि ।]{style="color:#99cc00;"}\
[उग्रं तर्पयामि ।]{style="color:#99cc00;"}\
[भीमं तर्पयामि ।]{style="color:#99cc00;"}\
[महादेवं तर्पयामि ।]{style="color:#99cc00;"}

Then he identifies with rudra and deploys the पञ्चब्रह्म mantra-s followed by kad रुद्राय:\
[सद्योजातं प्रपद्यामि सद्योजाताय वै नमो नमः ।]{style="color:#99cc00;"}\
[भवे भवे नातिभवे भवस्व माम् ।]{style="color:#99cc00;"}\
[भवोद्भवाय नमः ॥]{style="color:#99cc00;"}

[वामदेवाय नमो ज्येश्ठाय नमश् श्रेश्ठाय नमो रुद्राय नमः कालाय नमः कलविकरणाय नमो बलविकरणाय नमो बलाय नमो बलप्रमथनाय नमस् सर्व-भूतदमनाय नमो मनोन्मनाय नमः ॥]{style="color:#99cc00;"}

[अघोरेभ्यो ।अथ घोरेभ्यो घोर-घोरतरेभ्यः ।]{style="color:#99cc00;"}\
[सर्वेभ्यस् सर्व-शर्वेभ्यो नमस्ते अस्तु रुद्र रूपेभ्यः ॥]{style="color:#99cc00;"}

[तत् पुरुषाय विद्महे महादेवाय धीमहि ।]{style="color:#99cc00;"}\
[तन्नो रुद्रः प्रछोदयात् ॥]{style="color:#99cc00;"}

[ईशानस् सर्व-विद्यानाम् ईश्वरस् सर्व-भूतानाम् ब्रह्माऽधिपतिर् ब्रह्मणोऽधिपतिर् ब्रह्मा ।]{style="color:#99cc00;"}\
[शिवो मे अस्तु सदाशिव्^ॐ ॥]{style="color:#99cc00;"}

[कद् रुद्राय प्रचेतसे मीढुश्टमाय तव्यसे ।]{style="color:#99cc00;"}\
[वो चेम शन्तमं हृदे ।]{style="color:#99cc00;"}\
[सर्वो ह्य् एष रुद्रस् तस्मै रुद्राय नमो अस्तु ॥]{style="color:#99cc00;"}

The atharvavedin replaces the kad रुद्राय mantra with the following triad of mantra-s:

[yas समानो yo .असमानो .amitro no जिघांसति |]{style="color:#99cc00;"}\
[रुद्रः शरव्यया तान् अमित्रान् नो वि विध्यतु ॥]{style="color:#99cc00;"} AV-P 1.20.3

[रुद्र यत् ते गुह्यं नाम यत् ते अद्धातयो विदुः ।]{style="color:#99cc00;"}\
[शिवा शरव्या या तव तया नो मृड जीवसे ॥]{style="color:#99cc00;"} AV-P 1.95.2

[स्तुहि श्रुतं गर्तसदं जनानां राजानं भीमम् उपहत्नुम् उग्रं ।]{style="color:#99cc00;"}\
[ मृड jaritre rudra ha स्तवानो anyam asut te ni vapantu सेन्यं ||]{style="color:#99cc00;"} AV-P 18.60.10

His दूती, if present, performs the yoga of the pleasing of rudra mentally meditating on rudra as the great fire.

The visualization for rudra is done with the following dhyAna shloka:\
[ध्यायामि वै रुद्र-रूपं सर्व-लोक-महेश्वरम् ।]{style="color:#99cc00;"}\
[शुद्ध-स्फटिक-संकाषं त्रिणेत्रं पञ्च-वक्त्रकम् ॥]{style="color:#99cc00;"}\
[गङ्गाधरं दष-भुजं सर्वाभरण-भूशितम् ।]{style="color:#99cc00;"}\
[नीलग्रीवं शशाङ्काङ्कं नाग-यज्ञोपवीतिनम् ॥]{style="color:#99cc00;"}\
[व्याघ्र-छर्मोत्तरियं छ वरेण्यम् अभयप्रदम् ।]{style="color:#99cc00;"}\
[कमण्ड्वल्व्-अक्श-सूत्राणि धारिणं शूलपाणिनं ॥]{style="color:#99cc00;"}\
[ज्वलन्तं पिङ्गल-जटं शिखामध्योद-धारिणं ।]{style="color:#99cc00;"}\
[वृष-स्कन्ध समारूढं शाश्वतं विश्वतोमुखं॥]{style="color:#99cc00;"}\
[अमृतेनाप्लुतं शान्तं दिव्य-भोग-समन्वितं ।]{style="color:#99cc00;"}\
[दिग्-देवता-समायुक्तं सुरासुर-नमस्कृतं ॥]{style="color:#99cc00;"}\
[नित्यं च परमाणुं वै ध्रुवम्-अक्शरम्-अव्ययं ।]{style="color:#99cc00;"}\
[सर्व-व्यापिनम्-ईशानं रुद्रं वै विश्व-रूपिणं ॥]{style="color:#99cc00;"}

This is followed by vaishvadeva-तर्पणं:\
[अग्निं तर्पयामि । वायुं तर्पयामि । सोमं तर्पयामि । इन्द्रं तर्पयामि । मरुतः तर्पयामि । ईशानं तर्पयामि । वरुणं तर्पयामि । मित्रं तर्पयामि । अर्यमणं तर्पयामि । विष्णुं तर्पयामि । पूषणं तर्पयामि । भगं तर्पयामि । देवं सवितारं तर्पयामि । त्वष्तारं तर्पयामि । धातारं तर्पयामि । अश्विनौ तर्पयामि । बृहस्पतिं तर्पयामि । प्रजापतिं तर्पयामि । ऋभून् तर्पयामि । स्कन्दं तर्पयामि । यमं तर्पयामि । वैश्रवणं तर्पयामि । धन्वन्तरिं तर्पयामि । तार्क्ष्यं अरिष्तनेमीं तर्पयमि । सर्वान् देवान् तर्पयामि ऽदितिं तर्पयामि । सरस्वतीं तर्पयामि । निरृतिं तर्पयामि । सर्व-देव-पत्नीः तर्पयामि । सर्व-देव-गणान् तर्पयामि । सर्व-देव-गण-पत्नीः तर्पयामि ॥]{style="color:#99cc00;"}

He can perform this krama as a stand alone or initiate it with a preceding homa. For this he kindles the fire with the mantra: "tvam agne rudro asuro महोदिवः..." and makes it blaze with "A vo राजानं adhvarasya रुद्रं ..." He make offering with the स्वाहान्त mantra-s instead of the namo.anta mantra-s specified above. He concludes the offerings with "agne naya supatha..."

Notes

  -  The योन्याकार-दीप: Below is an example of the योन्याकार दीप we use in this ritual.\
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-5H_8JnOQUG0/UNdLFVlwQKI/AAAAAAAACfw/ZN0ElDrt1qU/s800/योन्याकार_दीप_small.JPG){width="75%"}
```{=latex}
\end{center}
```


  -  The krama is predominantly observed among those closely linked to the स्मार्त tradition, with links to the now largely lost AV system. The connections between the AV-tradition and the later mantra-मार्ग are found in texts such as the अङ्गिरस kalpa, which links the AV-P tradition to the कालीकुल and पाङ्चरात्र. The त्रिपुरार्णव tantra of the श्रीकुल also have a chapter on the deployment of the atharvaveda (will be discussed eventually on these pages). The कुब्जिका- kula or पस्चिमाम्नाय has the atharvan connection in the form of the कुब्जिका उपनिषद्. The Asuri-kalpa links the AV tradition to the दौर्गा mata. Such texts were studied in Kashmir, Nepal, Orissa, Gujarat and the Vijayanagaran empire.

  -  Most mantra-s follow the forms specified in the आकाशभैरव tantra.

  -  Atharvavedin-s performed such a krama in the remote and once glorious Sathalapur सप्तमातृका temple in कलिङ्ग, which is now in ruins.

  -  A parallel to the yoga performed in this ritual is found in the famous dehasta-देवता-chakra स्तोत्रं of the illustrious abhinavagupta. There the deities invoked within the body are placed in the मण्डल of the kaula deities Ananda-bhairava and Ananda-भैरवी. The order of deities in that मण्डल is thus:\
gaNapati -- वटुक\
Ananda-bhairava -- guru -- Ananda-भैरवी\
brAhmI-माहेश्वरी-कौमारी-वैष्णवी-वाराही-चामुण्डा-महालक्ष्मी\
क्षेत्रपाल


