
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The skanda-vrata](https://manasataramgini.wordpress.com/2012/10/09/the-skanda-vrata/){rel="bookmark"} {#the-skanda-vrata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 9, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/10/09/the-skanda-vrata/ "7:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier presented a few details pertaining to the great कुमार chakra, which lay at the root of the कुमारशासन the pinnacle of its glory -- a time when महासेन was invoked from बाह्लिक in the north to द्रमिड coast in the south, and even beyond on the great massif in the island of लन्का. The current ritual under discussion is conceptually related to the rituals of that practice. In the great cultic क्षेत्र-s from the most ancient rohitakula in the पञ्चनद, skandapura in कश्मीर, the holy city of मथुरा on the banks of the famed यमुना, the most secluded old nagara क्षेत्र on the western coast of the लाट land, the cave of udayagiri, the riverine shrine of padmapura in the वङ्ग country, the secret site of the attracted ladies at सिंहभूमि in the magadha country, the mound of मयूरपुर (modern Mitwali), in the great hills of श्रीपर्वत and कुमारपर्वत in the andhra and कर्णाट countries respectively, at श्रीस्कन्दपुर and दक्षिण-मथुरा in the द्रमिड land and on the ratnagiri (i.e. Kataragama) in island of the cinnamon trees the practitioners of the secret rites of the old कुमारशासन were active at that point. As we pointed out earlier on these pages the mantra-practice of the स्कन्दशासन evolved from the late Vedic-proto-tantric mantra system, through the middle तान्त्रिक system and finally the shaiva-type late tantrika system. As a result few practitioners know the earliest rites of the कुमारशासन, which preceded the period of the "Ur-skanda पुराण", i.e. the भट्टाराइ edition, by a long time.
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-vq50W8iAzo8/UHPMqaExoXI/AAAAAAAACdw/rAxmraXJbIE/s640/कुमार11.jpg){width="75%"}
```{=latex}
\end{center}
```

*[कुमार फ़्रोम् मथुरा]{style="color:#33cccc;"}*

The ritual might be performed during the 6 tithi-s of the shukla-पक्ष. Those bright parvan-s when the moon attains the षष्ठि tithi in कृत्तिका or विशाखा or ज्येष्ठ are particularly suitable for the rite. On the प्रथमा morning after a bath the ritualist goes to a large arka plant (Calotropis gigantea) and worships the deities मिञ्जिका and मिञ्जिक. He conceives them as red in complexion, as youth male and female twins, and as holding bows, spears and tridents. He makes पिण्ड-s of haridra under the milkweed representing the two deities and invokes them with the mantra:\
[अवाहयामि मिञ्जिका-मिञ्जिकौ रुद्रसंभवाभ्यां नमो नमः ॥]{style="color:#99cc00;"}\
Then he offers arka flowers with the incantation:\
[प्रपद्यामि मिञ्जिका-मिञ्जिकौ ताभ्यां वै नमो नमः ॥]{style="color:#99cc00;"}\
He makes a water or a ghee offering with the mantra:\
[मिथुनं शर्व-संभवं तर्पयामि ॥]{style="color:#99cc00;"}
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-15vSzYdm9x8/UHPLlWcs5aI/AAAAAAAACdo/1EjgrEH9Hm8/s640/vRikShikA.jpg){width="75%"}
```{=latex}
\end{center}
```



*[वृक्षिका from the ancient sthala of मथुरा (now at the museum in उक्षतीर्थ)]{style="color:#33cccc;"}*

Then he collects the milkweed flowers and proceeds in the direction of an ashvattha, nyagrodha or प्लक्ष tree and under it places stones, mud images or turmeric mounds to denote the carnivorous goddesses वृद्धिका and वृक्षिका. He invokes them and offers milk with the incantation:\
[इहागच्च आगच्च ! नृमांसादे तर्पयामि । वृक्षिका सुप्रीता भवतु ! वृद्धिका सुप्रीता भवतु !]{style="color:#99cc00;"}

Then the ritualist looks at the sun by intertwining his fingers and looking through the aperture between them and offers milk to the goddess बहुरश्मिका uttering:\
[ॐ भवस्य रेतसस्-संभूत्यै बहुरश्मिकायै स्वाहा ॥]{style="color:#99cc00;"}

वृद्धिका is conceived as brown in color, वृक्षिका is green in color, and बहुरश्मिका is visualized as reddish yellow in color. All are with two arms, have their hair in plaits and have shapely waists and round breasts.

Then facing east he draws the figure of the Pleiades cluster and offers milk uttering:\
[ॐ अम्बा! दुला! नितत्नी! अभ्रयन्ती! मेघयन्ती! वर्षयन्ती! छुपुणिका! कुमार-मातृभ्यः स्वाहा ॥]{style="color:#99cc00;"}\
The 7 कृत्तिका-s are visualized with two hands and silvery blue in color, with plaited hair, youthful and with crowns.

Under a sprawling chaitya tree, which might be the same where he worshiped the goddesses he lays out a 6 petaled padma diagram and in the petals in visualizes the goddesses: गौरी, विद्या, गान्धारी केशिनी, mitrA, sAvitrI and the great goddess पार्वती, consort of rudra in center. The then recites:

[एह्येहि गौरी गान्धारी विद्या केशिनी मित्रा सावित्री ! प्रिया देव्यस् ताभ्यो वै नमो नमः ॥]{style="color:#99cc00;"}\
[महादेव्यै उमायै नमो नमः ॥]{style="color:#99cc00;"}

With स्वाहा attached to each of the seven names of the above goddesses in the dative case he then offers milk.

Then the ritualist returns to his fire place and on a sthandila he lights a fire from his औपासन or आहवनीय fire uttering the mantra:

[sa yojate aruShA विश्वभोजसा sa dudravat स्वाहुतः |]{style="color:#99cc00;"}\
[सुब्रह्मा यज्ञः सुशमी वसूनां देवं राधो जनानाम् ॥]{style="color:#99cc00;"} \[RV 7.16.2]\
He makes an oblation with:\
[ऒं अग्नये स्वाहा ॥]{style="color:#99cc00;"}

Then he then utters the following mantra and makes an oblation to indra:\
[यत इन्द्र भयामहे ततो नो अभयं कृधि ।]{style="color:#99cc00;"}\
[मघवञ् छग्धि तव तन् न ऊतिभिर् वि द्विषो वि मृधो जहि ॥]{style="color:#99cc00;"} \[RV 8.61.13]\
[ऒं इन्द्राय स्वाहा ॥]{style="color:#99cc00;"}

Then he makes an offering to mitra and वरुण with the mantra:\
[यो अद्य सेन्यो वधो ।अघायूनाम् उदीरते ।]{style="color:#99cc00;"}\
[युवं तं मित्रावरुणाव् अस्मद् यावयतं परि ॥]{style="color:#99cc00;"} \[AV-S 1.20.2]\
[ऒं मित्रावरुणाभ्यां स्वाहा ॥]{style="color:#99cc00;"}

The sacrifices to yama with the mantra:\
[यमाय घृतवद् धविर् जुहोत प्र च तिष्ठत ।]{style="color:#99cc00;"}\
[स नो देवेष्व् आ यमद् दीर्घम् आयुः प्र जीवसे ॥]{style="color:#99cc00;"} \[RV 10.14.14]\
[ऒं यमाय स्वाहा ॥]{style="color:#99cc00;"} (he touches water and then his chest)

He sacrifices to rudra thus:\
[रुद्र जलाष-भेषज नीलशिखण्ड कर्मकृत् ।]{style="color:#99cc00;"}\
[प्राशं प्रतिप्राशो जह्य् अरसान् कृण्व् ओषधे ॥]{style="color:#99cc00;"} \[AV-S 2.27.6]\
[ऒं रुद्राय स्वाहा ॥]{style="color:#99cc00;"} (he touches water and then his chest)

He makes an oblation to the marut-s:\
[ते स्यन्द्रासो नोक्षणो ऽति ष्कन्दन्ति शर्वरीः ।]{style="color:#99cc00;"}\
[मरुताम् अधा महो दिवि क्षमा च मन्महे ॥]{style="color:#99cc00;"} \[RV 5.52.3]\
[ऒं मरुद्भ्यः स्वाहा ॥]{style="color:#99cc00;"}

[इन्द्र आसां नेता बृहस्पतिर् दक्षिणा यज्ञः पुर एतु सोमः ।]{style="color:#99cc00;"}\
[देवसेनानाम् अभि-भञ्जतीनां जयन्तीनाम् मरुतो यन्त्व् अग्रम् ॥]{style="color:#99cc00;"}\[RV 10.103.8]\
[ऒं देवसेनाभ्यः स्वाहा ॥]{style="color:#99cc00;"}

Then he makes six oblations to कुमारा thus:\
[सद्योजातं प्रपद्यामि सद्योजाताय वै नमः ।]{style="color:#99cc00;"}\
[भवे-भवे नादिभवे भजस्व मां भवोद्भवेति भवाय नमः ॥]{style="color:#99cc00;"} \[AV-pari 20.6.1]\
[ॐ अघोराय महाघोराय नेजमेषय नमः स्वाहा ॥]{style="color:#99cc00;"}

Then he makes the three oblations to the circle of twelve कुमार-kula-मातृ-s:\
[आवेशिनी ह्य् अश्रुमुखी कुतुहली हस्तिनी जृंभिणी स्तंभिनी मोहिनी च ।]{style="color:#99cc00;"}\
[कृष्णा विशाखा विमला ब्रह्मरात्री भ्रातृव्य-संघेषु पतन्त्य् अमोघास् ताभ्यो वै मातृभ्यो नमः स्वाहा ॥]{style="color:#99cc00;"}

Then he makes a round of oblations to the देवसेना thus:\
[ॐ स्कन्दाय ब्रह्मण्याय देवसेनापतये मेरुर्-इवाचलाय शक्ति-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ शक्राय त्रिदशनां महेस्वराय पाण्डूर-गज-वाहनाय वज्र-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ विष्णवे वासुदेवाय दानव-सुदनाय चक्र-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ अग्नये हुताशनाय शक्ति-पाणये स्वाहा ॥]{style="color:#99cc00;"}\
[ऒं यमाय राजाय काल-दण्ड-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ वरुणाय धर्मराजाय पाश-विचक्र-धराय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ वैश्रवणाय धनेश्वराय शिबिका-धरय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ निरृताय यातुराजाय खद्ग-शक्तिधराय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ वायवे समीरणाय अङ्कुश-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ अश्विभ्यां दीप्यामाना ओषधयो गृहितृभ्यां स्वाहा ॥]{style="color:#99cc00;"}\
[ऒं धात्रे धनुर्-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ऒं जयाय मुसल-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ त्वष्टाय महाबलाय पर्वत-धराय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ अंशाय शक्ति-धराय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ मृत्यु-देवाय परशु-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ रुद्राय महादेवाय पिनाक-पाशुपत-धराय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ अर्यंणे गृहीत-घोर-परिघाय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ मित्राय क्षुरपर्यन्त-चक्र-धराय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ पुष्णे गृहीत-चक्राय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ भगाय शर-कार्मुक-धराय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ सवित्रे निस्त्रिंश-हस्ताय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ मरुद्भ्यश् चित्रायुधेभ्यस् स्वाहा ॥]{style="color:#99cc00;"}

Then he makes a final round of manifold कौमार offerings with the mantra:

[ऒं भुवे नमस् स्वाहा ॥]{style="color:#99cc00;"}

He conclude the fire ritual by uttering:\
[देवं प्रपद्ये वरदं प्रपद्ये स्कन्दं प्रपद्ये छ कुमारम् उग्रम् ।]{style="color:#99cc00;"}\
[षण्णां सुतं कृत्तिकानां षडास्यम् अग्नेः पुत्रं महासेनं प्रपद्ये॥]{style="color:#99cc00;"}

Then he offer a bali (usually rice mixed with red unguent) in the eastern direction:\
[आयातु देवो मम कार्त्तिकेयो ब्रह्मण्यपित्रैः सह मातृभिश् च ।]{style="color:#99cc00;"}\
[भ्रात्रा विशाखेन च विश्वरूप इमं बलिं सानुचर जुषस्व ॥]{style="color:#99cc00;"}

With that concludes the day ritual for the tithi-s other than the षष्टी day.

After the performance of the सायं संध्या the ritualist heads out to perform the rituals of the night. The teachers say that these are not for the novice and are only for a bold and seasoned मन्त्रवादिन्. He heads in the north or east direction towards a place were four roads cross or else he might directly go to a cemetery(known as पितृवन) with चैत्यवृक्ष-s. He may be accompanied by his female uttara-साधका for the ensuing rites and they arm themselves with knifes, swords and bows.

At the crossroad or at the cemetery he begins the ritual of the great goddess रेवती on a स्थण्डिल. He imagines her as being dark in color with thick plaits on either side, with sparkling ear rings, colorful garlands and clothes. He makes oblations of black sesame oil with the following mantras with स्वाहा at the end:

[षष्टिश् cha ShaT cha revati पञ्चाशत् पञ्च sumnayi |]{style="color:#99cc00;"}\
[चत्वारश् चत्वारिंशच् च त्रयस् त्रिंशच् च वाजिनि ॥]{style="color:#99cc00;"}

[dvau cha te विंशतिश् cha te रात्र्य् एकादशावमाः |]{style="color:#99cc00;"}\
[तेभिर् नो अद्य पायुभिर् नु पाहि दुहितर् दिवः ॥]{style="color:#99cc00;"}

[रक्षा माकिर् नो अघशंस ईशत मा नो दुःशंस ईशत ।]{style="color:#99cc00;"}\
[मा नो अद्य गवां स्तेनो मावीनां वृक ईशत ॥]{style="color:#99cc00;"} \[AV-S 19.47.4-6]

[नाना वस्त्रधर देवी छित्रमाल्यानुलेपना ।]{style="color:#99cc00;"}\
[चलत्-कुण्डलिनी श्यामा रेवती ते प्रसीदतु ॥]{style="color:#99cc00;"}

[उपासते यां सततं देव्यो विविधभूषणाः ।]{style="color:#99cc00;"}\
[लंबा कराला विनता तथैव बहुपुत्रिका ।]{style="color:#99cc00;"}\
[रेवती शुष्क-नामा या सा ते देवी प्रसीदतु ॥]{style="color:#99cc00;"} \[sushruta-uttara tantra 31.10]

When the last two mantras are being deployed the उत्तरसाधका might grind the ashvagandha, अजशृङ्गी, सारिवा, पुनर्नवा, सहा and विदारी herbs to prepare a drug for pediatric illness of the रेवती type. The ghee remaining from the oblations might be used to prepared the medicated ghee as prescribed in the medical saMhitA-s.

He then makes a final set of oblations for succor from pediatric illnesses:\
[ॐ अग्नये स्वाहा । कृत्तिकाभ्यस् स्वाहा । नमस् स्कन्दाय देवाय ग्रहाधिपतये नमस् स्वाहा ॥]{style="color:#99cc00;"} \[cf. sushruta-uttara tantra 27.20-21 ]

After that he and the उत्तरसाधका flick the thumb and the middle finger of their right hand around their heads three times making a cracking noise and utter: [भूर्-भुवस्-स्वरॊं]{style="color:#99cc00;"} for each circuit. Then the utter the incantation to the dreadful rAkShasa-graha and makes multiple offerings of meat, beer or cakes with black-eyed beans. He is visualized as black in color with a frightful mien and hypertrophied canines.\
[आयातु स्कन्द-भृत्यस् ससैनयस् सानुचरः प्रतीतः । प्रतिगृह्णातु इमं हुतं नैरृतेयो ग्रहो महान् ॥]{style="color:#99cc00;"}

If the ritualist wishes to dispatch the rAkShasa graha to seize his enemy he utters the following mantra, pounds an image of his enemy with a parigha and then makes an oblation \[Footnote 1]:\
[यातुधाना निरृतिर् आद् उ रक्षस् ते अस्य घ्नन्त्व् अनृतेन सत्यम् ।]{style="color:#99cc00;"}\
[ इन्द्रेषिता devA Ajam asya mathnantu mA tat saM पादि yad asau juhoti ||]{style="color:#99cc00;"} \[AV-S 7.70.2/AV-P 19.27.2]

If he desires to relieve a victim from the seizure by the rAkShasa-graha, he deploys the following mantra and makes an oblation of a cake. His female partner prepares the drug for countering the effects of the graha as described in the medical saMhitA-s \[Footnote 2]:\
[देवैनसाद् उन्मदितम् उन्मत्तम् रक्षसस् परि ।]{style="color:#99cc00;"}\
[कृणोमि विद्वान् भेषजं यदानुन्मदितो ऽसति ॥]{style="color:#99cc00;"} \[AV-S 6.111.3]

Then he makes an offering the पिषच to afflict his enemies:

[ॐ नमः पिशाचाय द्विषन्तं भ्रातृव्यं कंपय कंपय उन्मादय उन्मादय फट् एह्य् एहि स्कन्द आज्ञापयति स्वाहा ॥]{style="color:#99cc00;"}

He then makes at 6 oblations or multiples there of with glorious मयूरशिखा mantra even as his female assistant makes the secret मयूरशिखा drug:

[ॐ नमः षण्मुखाय शक्ति-हस्ताय मयूर-वाहनाय औषधीकेन देहि मे भव स्वाहा ॥]{style="color:#99cc00;"}

He concludes with an offering with the formula:\
[गणैर् वृतो मातृभिर् अन्वयातो मयूर-संस्थो वर-शक्ति-पाणिः ।]{style="color:#99cc00;"}\
[सेनाधिपतये स कृतो भवेन राजति सूर्येव महा-वपुष्मान् । स्वाहा ॥]{style="color:#99cc00;"}

Footnote 1: Tradition holds that विश्वामित्र deployed a rAkShasa graha to strike his rival वसिष्ठ when he was crossing a bridge.

Footnote 2: The sushruta saMhitA describes the a psychiatric condition attributed to the rAkShasa graha in uttara tantra 60. It states the that rAkShasa-graha seizure results in shameless behavior, cruelty, short temper, lack of cleanliness and bursts of enormous activity. Interestingly, sushruta clarifies that if the graha effects result from a trauma caused by the fall from a hill, an elephant, a tree or senility it is unlikely to be curable at all.

continued ...


