
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कौटिल्य on knowledge](https://manasataramgini.wordpress.com/2012/06/23/kautilya-on-knowledge/){rel="bookmark"} {#कटलय-on-knowledge .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 23, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/06/23/kautilya-on-knowledge/ "7:09 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The wise विष्णुगुप्त, who propelled the mauryan to meteoric heights, while uprooting the tree of the wicked nanda-s and cleansing the taint of the Macedonian barbarians on the land of the bhArata-s, uttered words that remain vivid even after 2300 years. The आचार्य's enumeration of the Hindu system of knowledge goes thus:

[आन्वीक्षिकी त्रयी वार्ता दण्ड-नीतिश् च^इति विद्याः ॥]{style="color:#0000ff;"}

The branches of knowledge are: logical analysis, the three-fold ritual chants \[the veda], business and political science.

[त्रयी वार्ता दण्ड-नीतिश् च^इति मानवाः ॥ त्रयी विशेषो ह्य् आन्वीक्षिकी^इति ॥]{style="color:#0000ff;"}

The school of manu holds that \[there are only three branches]: The three fold ritual chants, business and political science. Because logical analysis is only a special branch of the veda.

[वार्ता दण्ड-नीतिश् च^इति बार्हस्पत्याः ॥ संवरण मात्रं हि त्रयी लोक यात्रा विद इति ॥]{style="color:#0000ff;"}

The school of बृहस्पति holds that \[there are only two branches]: Business and political science. Because the veda is only a cloak for those who are well-versed in worldly customs.

[दण्ड-नीतिर् एका विद्या^इत्य् औशनसाः ॥ तस्यां हि सर्व-विद्या।आरम्भाः प्रतिबद्धा इति ॥]{style="color:#0000ff;"}

The school of ushanas holds that there is only one branch: political science. Because the origins of all types of knowledge are \[ultimately] united in it.

[छतस्र एव विद्या इति कौटिल्यः ॥]{style="color:#0000ff;"}

However, कौटिल्य holds that the branches of knowledge are verily only four \[i.e. logical analysis, the veda, business and political science].

[ताभिर् धर्म।अर्थौ यद् विद्यात् तद् विद्यानां विद्यात्वम् ॥]{style="color:#0000ff;"}

The defining feature of all branches of knowledge is that by knowing them one can learn of dharma and artha.

[सांख्यं योगो लोकायतं च^इत्य् आन्वीक्षिकी ॥]{style="color:#0000ff;"}

Logical analysis is comprised of the systems of सांख्य, yoga and लोकायत.

[धर्माधर्मौ त्रयाम् अर्थानर्थौ वार्तायां नयानयौ दण्ड-नीत्यां बलाबले च एतासां हेतुभिर् अन्वीक्षमाणा लोकस्य उपकरोति व्यसने ऽभ्युदये च बुद्धिम् अवस्थापयति प्रज्ञा-वाक्य-क्रिया-वैशारद्यं च करोति ॥]{style="color:#0000ff;"}

Logical analysis by means of proper arguments of dharma and adharma, which belong to the realm of the veda, profit and loss which belong to the realm of business, good and bad policies which belong to the realm of political science, \[as also] the strengths and weaknesses of each branch of knowledge, benefits the people, keeps the intellect steady both in dire situations and good times, and generates expertise in the spheres of mental, verbal and physical activity.

[प्रदीपः सर्व-विद्यानाम् उपायः सर्व-कर्मणाम् । आश्रयः सर्व-धर्माणां शश्वद् आन्वीक्षिकी मता ॥]{style="color:#0000ff;"} AS 1.2.01-12

The systems of logical analysis \[i.e. सांख्य, yoga and लोकायत] are always the lamp illuminating all knowledge and the means of all actions. It is the foundation of all dharma-s.

Notes\
\~*\~*\~

  -  The loss and recovery of the अर्थशास्त्र: Based on the textual evidence, in the form of the mantra practice, deities, and urban layout we hold that the अर्थशास्त्र, as we have it, comes from a time close to that of विष्णुगुप्त. It is either his own text or that of his near successors. One of his near successors was विष्णुशर्मन्, [who composed the पञ्चतन्त्र inspired by कौटिल्य](https://manasataramgini.wordpress.com/2004/06/05/panchatantra-indias-great-product-origin-and-evolution/). There were many later successors, like कामन्दकि, [who eloquently eulogizes चाणक्य in the beginning of his own tome on political science](https://manasataramgini.wordpress.com/2007/04/06/kamandaki-on-vishnugupta-and-chandragupta/). Thus, the tale of चाणक्य and his knowledge was very widely known and practiced in the early medieval period. However, the text was lost in the later medieval period, and was only rediscovered in 1904 CE by Mysore Shama Shastry. He published the text along with a translation that marking one of the most momentous events in Indian philology.

The front-line thinker of the modern Hindu revival, श्री Sitaram Goel, however, took a dim view of the political tradition of the अर्थशास्त्र. He attributed the inability of the Hindu rulers to ultimately stanch the Mohammedan catastrophe to them adhering too closely to विष्णुगुप्त's मण्डल theory of allies and foes. Thus, Goel reasoned that, acting as per the [मण्डल theory](https://manasataramgini.wordpress.com/2006/02/02/the-mandala-theory/), they were unable to see the beards knocking at the gates of India as an existential threat; instead they simply saw them as residents of the outer मण्डल, who could be used against the neighboring मण्डल by the central mahA-मण्डलेश्वर and his mahA-सामन्त henchmen. We are not sure that Goel's thesis is justified. In fact, we simply hold that the genuine wisdom of विष्णुगुप्त was lost at some point in the medieval period and replaced by a lesser version of the मण्डल theory (incidentally this is the very version parodied so humorously by the great दण्डिन् in his अवन्तिसुन्दरी kathA). We see the decline in the "higher" knowledge of चाणक्य as being the cause of the demise of the Hindu power under the Mohammedan ravages, rather than what was proposed by श्री Goel (for example, the above discussion of knowledge would have helped any one). This, along with the inability to accommodate the reality of the Aryan invasion (unlike Veer Savarkar), happens to be one his rare lapses. Ironically, even the Mohammedans appreciated the wisdom of चाणक्य in the form conveyed by विष्णुशर्मन्. We are informed that his was part of the curriculum of the education of Osman sultans like Kanuni Sultan Suleyman.

  -  trayi: The term trayi stands for the three types of vedic mantra-s not the the three veda-s -- the ऋक्-s or the metrical chants; the यजुष्-s or the prose chants and the sAman-s or the musical chants with 6-7 tones.

  -  The view of the बृहस्पति school that the veda is merely a cloak of the world-savy seems to be reminiscent of the view espoused by the चार्वाक school whose founder is also known to be बृहस्पति. We suspect that this is not entirely coincidental.

  -  The ushanas school, which emphasizes the importance of दण्डनीति, appears to have been [the basis of much of the exposition on this topic (also rAja-dharma) in the महाभारत](https://manasataramgini.wordpress.com/2011/11/13/some-considerations-on-indian-polity/). The text clearly echoes the view of the primacy of दण्डनीति. Indeed, the opinion expounded there is that all human endeavor and knowledge resulting therefrom is merely an offshoot of political activity. While this statement might sound either odd or cynical, it is not entirely without merit. While human knowledge for knowledge's sake and various other aspects of life might be praised, they can all be seen as being only social constructs, which can exist only as a consequence of political organization.

Later in the अर्थशास्त्र, चाणक्य explains his position on दण्डनीति, while not according it the status of being the sole knowledge system, he accepts the basic tenet of the importance of दण्डनीति as per the aushanasa school in the maintenance of knowledge, dharma and business:

[आन्वीक्षिकी त्रयी वार्तानां योग-क्षेम-साधनो दण्डः । तस्य नीतिर् दण्ड नीतिः । अलब्ध-लाभार्था लब्ध-परिरक्षणी रक्षित विवर्धनी वृद्धस्य तीर्थे प्रतिपादनी च ॥]{style="color:#0000ff;"} 1.4.03

The daNDa \[is the power] by which the application and the growth of the systems of logical analysis, the veda and business are upheld. Its management is known as political science. It is the means by which new acquisitions are made, profits are gained \[in business], the acquisitions are kept secure, security and growth \[are achieved] and the fruits of growth are re-distributed among the deserving.

Nevertheless, with respect to the actual application of daNDa, विष्णुगुप्त advocates what might be termed the path of moderation:\
["तस्माल् लोक यात्रा ऽर्थी नित्यम् उद्यत-दण्डः स्यात् ॥ न ह्य् एवं विधं वश^उपनयनम् अस्ति भूतानां यथा दण्डः।" इत्य् आचार्याः ॥]{style="color:#0000ff;"}

"Whoever is desirous of progress of the world shall always do so with the upraised rod of chastisement. Never can there be a better instrument to bring beings \[implying people and other animals] under control than the application of chastisement." So the earlier teachers expounded \[i.e. they advocated a police state].

[न^इति कौटिल्यः ॥ तीक्ष्ण।दण्डो हि भूतानाम् उद्वेजनीयो भवति ॥ मृदु दण्डः परिभूयते ॥ यथा ऽर्ह दण्डः पूज्यते ॥]{style="color:#0000ff;"} 1.4.05-10

"Not so," says कौटिल्य; for whoever imposes severe chastisement becomes tyrannical to the beings \[a police state]; while he who is mild in chastisement is despised/disrespected \[a soft state]. But he who imposes punishment as deserved is respected \[the ideal Hindu state].

  -  आन्वीक्षिकी: What is चाणक्य's आन्वीक्षिकी? Tradition holds that generally by the term आन्वीक्षिकी the systems of nyAya and वैशेषिक are implied. This is consistent with the fact that these are the systems that provide the foundation of Hindu logical analysis. However, विष्णुगुप्त clearly defines it for us as being सांख्य, yoga and लोकायत. Now, tradition has a clear understanding of what is meant by these three darshana-s. But it is interesting to note that the traditional systems that pertain to logical analysis have not been mentioned here. The great scholar of Hindu logical analysis, Kuppuswamy Shastri, has argued that actually चाणक्य meant the nyAya- or वैशेषिक-like systems by the term yoga. What is classically considered yoga is merely a version of सांख्य. This, is certainly a possibility. The point of greatest interest is that चाणक्य does not disparage लोकायत. He sees it as a solid tradition of logical analysis that forms one of the foundations of knowledge. This, attitude is rather distinct from that held by the theistic schools, though it might be remarked that some pashupata shaiva-s and the kaula तान्त्रिक kavI dharma from the लाट country appear to have preserved the लोकायत system and used it in demolishing arguments of their nirgrantha rivals.

Unlike delusive modern liberal expositors, चाणक्य is clear that learning analytical devices is not for all and sundry, and outlines who can do so:

[शुश्रूषा श्रवण ग्रहण धारण विज्ञान^ऊहापोह तत्त्वाभिनिविष्ट बुद्धिं विद्या विनयति न^इतरम् ॥]{style="color:#0000ff;"} 1.5.05

The acquisition of knowledge can only culture those whose intellect is endowed with the abilities of discipline, learning from verbal instruction, grasping concepts, memory, discrimination, inference and applying falsification, but not others \[those who lack these].

In conclusion, we find विष्णुगुप्त's analysis of the knowledge to be one of the most appealing among those we encounter in early history. We have held the opinion that the आन्वीक्षिकी approach has been the most successful of the Hindu approaches to knowledge, though it eventually declined compared to some of the other schools. It is not something widely inculcated in modern education of Hindu-s. Verily, this is manifest in a generally poor grasp of evolutionary thinking and philosophy in the realm of the general physical universe, biological systems, societies and languages. This is also compounded by the general disinterest in acquisition of vast amounts of readily retrievable, well-classified, and usable information in the mental disks through some discipline. This relates to topic we broached in the last epistle, where we emphasized the primacy of information. Indeed, it is not uncommon to hear in the modern education systems that information is merely clutter, and that rather they strive to impart logical thinking. Of course today one can extend mental hard disks with silicon, but even to use the latter effectively, one needs to have the discipline of information acquisition and storage. Only then is logic of any value. It is clear that विष्णुगुप्त realized and emphasized the importance of being widely informed. He stresses the point of how the kShatriya needs to constantly acquire new knowledge and revise what he learns.

He states :\
[श्रुताद् हि प्रज्ञा^उपजायते प्रज्ञाया योगो योगाद् आत्मवत्ता^इति विद्यानां सामर्थ्यम् ॥]{style="color:#0000ff;"} 1.5.16

Verily from hearing (i.e. gathering information) emerges knowledge; that knowledge allows its application; from its application one becomes prudent. This is the power of knowledge.

We shall end by remembering how विष्णुगुप्त emphasizes the role of the educated king and protection of knowledge by the king:\
[विद्या विनीतो राजा हि प्रजानां विनये रतः । अनन्यां पृथिवीं भुङ्क्ते सर्व-भूत-हिते रतः ॥]{style="color:#0000ff;"} 1.5.16

The king who is educated in the fields of knowledge, verily interested in administration of his people, and inclined to benefit all beings will enjoy the earth unopposed.

[गान्धर्वं नृत्तम् आलेख्यं वाद्यं च गणितं कलाः ।]{style="color:#0000ff;"}\
[अर्थशास्त्रं धनुर्वेदं यत्नाद् रक्षेन् महीपतिः ॥]{style="color:#0000ff;"} चाणक्य-राजनीति 75

Music, dance, painting, discourse, mathematics, technologies, economics and military science, a king should painstakingly protect.

These concluding points are largely lost in the contemporary rulers of जम्बुद्वीप.


