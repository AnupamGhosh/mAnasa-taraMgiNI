
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some maxims of वेङ्कटाध्वरिन्](https://manasataramgini.wordpress.com/2012/04/06/some-maxims-of-venkatadhvarin/){rel="bookmark"} {#some-maxims-of-वङकटधवरन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 6, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/04/06/some-maxims-of-venkatadhvarin/ "7:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our medieval coethnic, वेङ्कटाध्वरिन् (1600s of the common era), was a shrauta ritualist, keen observer of humanity, master poet, proponent of विशिष्टाद्वैत and श्रीवैष्णव bigot, all simultaneously rolled in one. In his writings we encounter a brilliant account of the sacred geography of India, which shows that the concept of अखण्ड bhArata was not lost despite the Mohammedan irruptions, and a prescient fear of the रावण-like Europeans who were to bring misery upon the peoples of our land. He was the son of रघुनाथ सूरी of the Atreya gotra and lived in काञ्चीपुरं. Some of his सुभाषित-s have the beauty that might even earned them a place in the great collection of the nAstika विद्याकर, had he lived in an earlier era. Let us look at some of these:

[मन्द-प्रज्ञाध्यापनं क्षुद्र-मैत्री नीरूप-स्त्री-संगतिर् नीच-सेवा ।]{style="color:#0000ff;"}\
[सारासार-ज्ञान-हीनानुसारः केषां न स्यात् खेद-संपादनाय ॥]{style="color:#0000ff;"}

Teaching those of dim intellect, friendship with losers, companionship with an ugly woman, serving the lowly, following one lacking the \[discriminative] knowledge of the essential from the trivial -- who indeed is not brought to regret by these?

One who has lived life long enough indeed encounters such situations and cannot disagree with वेङ्कट.

[पर-संयोग-महिमा लघोर् अपि यतः स्वतः ।]{style="color:#0000ff;"}\
[गुरुत्वं आवहत्य् एव प्रमाणं तस्य पाणिनिः ॥]{style="color:#0000ff;"}

The greatness of association with an eminent one is such, that even if one is a light-weight by himself \[associated with the eminent one], he becomes the bearer of weighty attraction; the testimony for this is पाणिनि \[himself]!

This is a beautiful grammatical श्लेष: para-संयोग might be taken to be association with an eminent one and also simultaneously taken to mean association with a consonant. In the latter sense it refers to the पाणिनिअन् rule, wherein an intrinsically laghu syllable become guru if associated with a consonant.

We know well how in places like the म्लेच्छ-desha a lowly Indian graduate student associates with a mighty म्लेच्छ professor so that he might attain gurutva by "para-संयोग".

[मानार्हं कुसुम-कुलेषु कैतकं तत् प्रायेण प्रथित-विवेकं एकं एव ।]{style="color:#0000ff;"}\
[धुत्तुरैस् समम् अमरद्रु गुच्छम् अच्छं बिभ्राणां न भजति धूर्जटेर् जटां यत् ॥]{style="color:#0000ff;"}

Perhaps in the floral clan the ketaki flower alone displays discrimination; hence it is verily worthy of respect, for it does not share a place in the locks of the thick-locked one \[rudra] which bears the dhuttara flowers and divine tree's unsullied inflorescence together.

This is a classic example of the श्रीवैष्णव's bigotry towards rudra in particular. It is a play on the tale that the ketaki flower was deprecated by rudra for betraying his लिङ्गोद्भव form to brahmA, thereby being excluded from rudra worship. So the श्रीवैष्णव sees it as a paragon of discrimination, because it does not associate with rudra's locks, where both the lowly dhuttura flower and the unsullied heavenly flowers occur indiscriminately together.

[कर्णाटी-चिकुरा इवाति कुटिला गौडी-कटाक्षा इव स्वाभाव्यात्तरलाश् च घूर्जर-वधु-पादाब्जवद्-रागिणः ।]{style="color:#0000ff;"}\
[मन्दा मागध-सुन्दरी-हसित-वत् स्वल्पाश्-च तन् मध्यवत् पाण्ड्य-स्त्री-स्तन-वन्-नटन्ति मलिनाः पश्यन्ति चोर्ध्वं खलाः ।|]{style="color:#99cc00;"}

\[The crooks are] like the curly locks of the Kannada women, they are fickle like the fluttering eyes of the Bengali women, they are prone to passion like the pink lotus-like feet of the Gujarati women \[play on rAga; makes sense only Sanskrit], unaffected like the gentle smile of the Bihari women \[play on manda; makes sense only Sanskrit], and mean even as their slim waists \[play on svalpa; makes full sense only Sanskrit], are sinful, act deceitfully and snootily like the dark, dancing, upturned breasts of the Tamil (i.e. from the पाण्ड्य country) women \[play on मलिनाः, नटन्ति and ऊर्ध्वं खलाः].

This is one of the classical themes of describing women from different parts of India. The absence of certain parts like sindhu, gandhAra, and काश्मीर seen often in earlier works might reflect the overwhelming of those regions by invaders when वेङ्कट was writing.


