
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पाञ्चरात्रिक vaiShNava elements in the astronomy textbook सूrya-siddhanta and other notes](https://manasataramgini.wordpress.com/2012/05/02/pancharatrika-vaishnava-elements-in-the-astronomy-textbook-surya-siddhanta-and-other-notes/){rel="bookmark"} {#पञचरतरक-vaishnava-elements-in-the-astronomy-textbook-सrya-siddhanta-and-other-notes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 2, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/05/02/pancharatrika-vaishnava-elements-in-the-astronomy-textbook-surya-siddhanta-and-other-notes/ "7:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

When it comes to astronomy, more than any other field, the white indologists and their Japanese apers have sought to show Hindus as frauds or idiots (of course, nowadays they cushion it in all manner "sophisticated" jargon, such as "Indo-Moslem scientific collaboration in pre-modern South Asia", etc) \[Footnote 1]. Everything worthwhile in our old astronomy is attributed to Mesopotamians, Greeks and more recently the Chinese (incidentally, we are supposed to have learned about 0, yes about nothing, from them). The 4th book of the dEnkart, informs us that Sassanian emperor क्षयथिय-puthra-I (shapur-I) in 250 CE collected books on astronomy, grammar and time-keeping from the Hindus and the megale-syntaxis (Almagest) of Claudius Ptolemy from the Greeks for his library. This illustrates that, at least in the eyes of the Iranian court, it was not just the yavana-s but who also the Hindus who had something useful to contribute regarding astronomy. One of the early textbooks of Hindu mathematics is the सूrya-siddhAnta, which has been at the center of much controversial attention of the म्लेच्छ-s. We are told that it contains incontrovertible evidence for the enlightenment of the Hindus by the yavana-s, alternatively we are informed that it is a rather inferior piece of science. But the few people have investigated the history of this text as it survives from the viewpoint of a Hindu insider, for whom it marks the early tradition of trigonometry and geocentric astronomy. In Hindu tradition it appears after the siddhAnta of brahmA and before the Greek work of Paulos (paulisha siddhAnta); thus, technically it could contain a core of pre-yavana material. We outline below a few interesting features that have a bearing on the history of the text.

The text itself is presented as an interesting interlocution, as is typical of several शास्त्र-s, between सूrya and the asura maya. Pleased with the penance of maya, सूrya projects a being from his person (as he is very busy with his duties in the sky and too blazing to behold) who taught the asura astronomical lore. The text itself later informs us that the knowledge of astronomy and astronomical instruments is lost at the end of each kalpa and has to be regained from सूrya. Now the text begins thus:\
[अछिन्त्यअव्यक्त-रूपाय निर्गुणाय गुणात्मने ।]{style="color:#99cc00;"}\
[समस्त-जगदाधार-मूर्तये ब्रह्मणे नमः ॥]{style="color:#99cc00;"}

Salutations to the god brahmA, who has an inconceivable and unmanifest form, without guNa-s and also of nature of guNa-s, the form \[i.e. हिराण्यगर्भ] which is the foundation of the whole universe.

Thus, on the whole while this siddhAnta is a saura text, the opening मङ्गलाचरण is to brahmA. This is an important marker: brahmA, who emerged as a neo-ectype of vaidika prajApati towards the concluding phases of the Vedic period, became the focus of the ब्राह्मण concept of a central deity. In this capacity of the central deity he inherits all the features of his vaidika manifestation, first outlined in the हिरण्यगर्भ सूक्त (RV 10.121) and also serves as some kind of personification of the upaniShadic brahman. His centrality in the junction between the vedic and the epic period is seen in the form of of the brahma-याग, a new ritual initiated in this time. This ritual is outlined in some depth in the AV परिशिष्ट 19, and includes the iconic, yantra and fire worship of brahmA as ordained by the भार्गव-s. This central position of brahmA is affirmed in the रामायण and some some degree in the महाभारत. The early पुराण-s such as the core of the मार्कण्डेय पुराण also retain this preeminent position of brahmA. It is this brahmA, who is also seen as the personification of the brahman who is repeatedly attacked by the तथागत and his immediate successors. Thus, there was a period of supremacy of brahmA, which certainly overlapped in time with the beginnings of the bauddha mata. After this period brahmA declined even within the Astika sphere and became secondary to the other two gods of the crystallizing trinity. Thus, we see the मङ्गलाचरण to brahmA as being a marker for a relatively early layer in the extant सूर्यसिद्धान्त.

Examination of the सूर्यसिद्धान्त reveals the imprint of yet another tradition, which was on the ascent in the post-Vedic period. In the 12th chapter maya asks सूrya to explain to him the geocentric model of the solar system and its orbital parameters. In answering him सूrya takes off on an unexpected detour that goes thus:

[वासुदेवः परं ब्रह्म तन्मूर्तिः पुरुषः परः ।]{style="color:#99cc00;"}\
[अव्यक्तो निर्गुणः शान्तः पञ्चविंशात् परो ऽव्ययः ॥]{style="color:#99cc00;"}

vAsudeva is the primal brahman whose primal form is the puruSha. It is imperceptible, unmanifest, without guNa-s, unperturbed and beyond the twenty five tattva-s.

[प्रकृत्य् अन्तर्गतो देवो बहिर् अन्तश् च सर्वगः ।]{style="color:#99cc00;"}\
[सङ्कर्षणो ऽपः सृष्ट्वादौ तासु वीर्यम् अवासृजत् ॥]{style="color:#99cc00;"}

This god \[vAsudeva] pervaded the प्रकृति from within, without and entirely, \[and emanated] सङ्कर्षण, who gave rise to the liquid element and emitted his seed into that.

[तद् अण्डम् अभवद् धैमं सर्वत्र तमसावृतम् ।]{style="color:#99cc00;"}\
[तत्रअनिरुद्धः प्रथमं व्यक्तीभूतः सनातनः ॥]{style="color:#99cc00;"}

That gave rise to a golden egg entirely covered in darkness. In that egg the eternal aniruddha first became manifest.

[हिरण्यगर्भो भगवान् एष छन्दसि पठ्यते ।]{style="color:#99cc00;"}\
[आदित्यो ह्य् आदिभूतत्वात् प्रसूत्या सूर्य उछ्यते ॥]{style="color:#99cc00;"}

He \[aniruddha] is called हिरण्यगर्भ in the Vedic recitations. Due to him being the first manifestation \[of the universe] he is called Aditya, and as the universe emanates from him he is called सूrya (word play on Adi-> Aditya and pra-सू-> सूrya).

[परं ज्योतिस् तमः पारे सूर्यो ।अयं सवितेति छ ।]{style="color:#99cc00;"}\
[पर्येति भुवनान् एष भावयन् भूतभावनः ॥]{style="color:#99cc00;"}

This great light which dispels the darkness is सूrya also known as the god savitar. As सूrya he is generator of all the worlds and the cause for sustenance of life.

[प्रकाशात्मा तमोहन्ता महान् इत्य् एष विश्रुतः ।]{style="color:#99cc00;"}\
[ऋचो ऽस्य मण्डलं सामान्य् उस्रा मूर्तिर् यजूंषि च ॥]{style="color:#99cc00;"}

This embodiment of light , the destroyer of darkness, he is well-known as the primal matter. His sphere \[is identified] with the ऋक्-s, his rays with the sAman-s and his mass with the यजुष्-es.

[त्रयीमयो ऽयं भगवाण् कालात्मा कालकृद् विभुः ।]{style="color:#99cc00;"}\
[सर्वात्मा सर्वगः सूक्ष्मः सर्वम् अस्मिन् प्रतिष्ठितम् ॥]{style="color:#99cc00;"}

This great, auspicious deity \[assumes the] form of time and is the maker of time. He is all-pervasive, moving in all ways, atomic and everything is established in him.

[रथे विश्वमये चक्रं कृत्वा संवत्सरात्मकम् ।]{style="color:#99cc00;"}\
[छन्दांस्य् अश्वाः सप्त युक्ताः पर्यटत्य् एष सर्वदा ।]{style="color:#99cc00;"}|

He rotates the chariot wheel of the universe which is of the form of the year. The seven Vedic meters are yoked as his horses and thus he eternally revolves

[त्रिपादम् अमृतं गुह्यं पादो ऽयं प्रकटो ऽभवत् ।]{style="color:#99cc00;"}\
[सो ऽहङ्कारं जगत् सृष्ट्यै ब्रह्माणम् असृजत् प्रभुः ॥]{style="color:#99cc00;"}

The indestructible 3/4th of him remains hidden, with the remaining ¼ generating the visible universe. He the lord having generated the consciousness of I and emitted brahmA.

[तस्मै वेदान् वरान् दत्त्वा सर्व-लोक-पितामहम् ।]{style="color:#99cc00;"}\
[प्रतिष्ठाप्य् आण्ड-मध्ये ऽथ स्वयं पर्येति भावयन् ॥]{style="color:#99cc00;"}

Having conferred on the grandfather of all worlds \[brahmA] the boon of the veda-s, he installed \[brahma] in the cosmic egg and himself illuminated it.

[अथ सृष्ट्यां मनश् चक्रे ब्रह्माहङ्कार-मूर्ति-भृत् ।]{style="color:#99cc00;"}\
[मनसश् चन्द्रमा जज्ञे सूर्यो ऽक्ष्णोस् तेजसां निधिः ॥]{style="color:#99cc00;"}

brahmA bearing the consciousness of I, \[had the first person experience] of the thought of origination. From his mind was born the moon, and the sun the mass of light from his eyes.

[मनसः खं ततो वायुर् अग्निर् आपो धरा क्रमात् ।]{style="color:#99cc00;"}\
[गुणैकवृद्ध्या पञ्चैव महाभूतानि जज्ञिरे ॥]{style="color:#99cc00;"}

From his mind sprung space; from space gas; from gas arose heat; from heat arose the liquid element and from it the solid element in order.

[अग्नीषोमौ भानुचन्द्रौ ततस् त्व् अङ्गारकादयः ।]{style="color:#99cc00;"}\
[तेजो-भू-खाम्बु-वातेभ्यः क्रमशः पञ्च जज्ञिरे ॥]{style="color:#99cc00;"}

The sun and the moon arose from agni and soma and thereafter the five planets arose from the five primal elements in order: heat/light-\>Mars; solid-\>Mercury; space-\>Jupiter; liquid-\>Venus; gas-\>Saturn.

[पुनर् द्वादशधात्मानं व्यभजद् राशि सञ्ज्ञकम् ।]{style="color:#99cc00;"}\
[नक्षत्र-रूपिणं भूयः सप्तविंशात्मकं वशी ॥]{style="color:#99cc00;"}

The self-possessed \[brahma] divided himself 12 fold to give rise to the राशि-s and 27-fold to give rise to the नक्षत्र-s.

[ततश् चराचरं विश्वं निर्ममे देवपूर्वकम् ।]{style="color:#99cc00;"}\
[ऊर्ध्व-मध्याधरेभ्यो ऽथ स्रोतोभ्यः प्रकृतीः सृजन् ॥]{style="color:#99cc00;"}

Thus, through the combinations of the three streams of प्रकृति (3 guNa-s) he gave rise to the universe made of all the mobile and immobile entities filled with the gods.

[गुण-कर्म-विभागेन सृष्ट्वा प्राग्वद् अनुक्रमात् ।]{style="color:#99cc00;"}\
[विभागं कल्पयामास यथास्वं वेद-दर्शनात् ॥]{style="color:#99cc00;"}

Thus having successively given rise to the various entities classified as per their properties and activities, he ordained the various divisions of the universe as in the Vedic tradition.

[ग्रह-नक्षत्र-तारानां भूमेर् विश्वस्य वा विभुः ।]{style="color:#99cc00;"}\
[देवासुर-मनुष्याणां सिद्धानां च यथाक्रमम् ॥]{style="color:#99cc00;"}

The great \[brahmA] then ordered the planets, lunar constellations, other stars, the earth and the realms of the deva-s, asura-s, men and siddha-s in order.

[ब्रह्माण्डम् एतत् सुषिरं तत्रेदं भूर्-भुवादिकम्।]{style="color:#99cc00;"}\
[कटाह-द्वितयस्येव सम्पुटं गोलकाकृति ॥]{style="color:#99cc00;"} SS 12.12-29

The universe is a void within which the earth, the atmosphere etc are situated is shaped like a sphere comprised of the fusion of two कटाह-s (=Indian cooking vessels; colloquially called called कडाहि).

What is of note here is the introduction of the theistic vaiShNava form of सांख्य that is typical of the पाञ्चरात्रिक tradition. It is comparable to that observed in the proto-पाञ्चरात्र narrated in the महाभारत. It describes the basic emanation from the primal vAsudeva of the व्यूह-s culminating in aniruddha who manifests as brahmA in the primordial cosmic egg. This aniruddha is identified with savitAr who is primal manifestation of light. On the whole this emanational sequence described here is entirely consistent with that described in the महाभरत even though pradyumna is not explicitly mentioned. Thus, it is rather clear that the सूrya-siddhAnta was redacted by the पाञ्चरात्रिक vaiShNava-s who were responsible for this section. When this section is compared with the initial मङ्गलाचरण to brahma it becomes clear that the पाञ्चरात्रिक-s did not seek to completely rework the शास्त्र -- they merely sought to interpolate their material at points which they felt allowed an organic placeholder for their material. This interpolation should be compared with other comparable interpolations by vaiShNava-s in certain other non-sectarian works. As we have shown earlier, they appear to have introduced a mantra associated with the preparation of an [agada termed the महागन्धहस्ति in the चिकित्सास्थान of the charaka saMhitA](https://manasataramgini.wordpress.com/2009/10/16/a-mantra-fossil-from-the-vedo-tantric-transition-in-charaka/) (23.90). There is also an interpolation into the ऋग्विधान of shaunaka, which is clear of vaiShNava origin and attributed to a certain विष्णुकुमार. In that the mantra verses containing the famous vaiShNava recitation beginning with the following are present:

[जितम् ते पुण्डरीकाक्ष नमस् ते विश्व-भावन ।]{style="color:#99cc00;"}\
[नमस् ते अस्तु हृषीकेश महा-पुरुष पूर्वज ॥]{style="color:#99cc00;"} ऱ्‌Vध् 3.33.174

This suggests that the vaiShNava interpolation might have even been पाञ्चरात्रिक in origin as this stotra is central in later पाञ्चरात्रिक tradition. Thus, there appears to have been a phase of vaiShNava/पाञ्चरात्रिक interest in the transition of a variety of non-sectarian texts into which were interpolated elements of पाञ्चरात्रिक worship. When exactly this happened is harder to establish in the case of the सूर्यसिद्धान्त from the interpolation noted above. It does not show the more derived features clear in the later tantra-s and पुराण-s, so it cannot be ruled out that it was from a relatively early layer of पाञ्चरात्रिक tradition.

But how does this go with the incontrovertible evidence for Greek influence? When we look at the text as we have it we cannot deny the presence of Greek words. For example:

  - The word kendra (Gk:kentron) is used 5 times, but nowhere other than in chapter 2.

  - The word koNa (Gk: gon = corner).\
Now even the occidental indologists agree that सूर्यसिद्धान्त does not contain material of Ptolemy's megale-syntaxis so, at least its core, cannot be younger than around 150 CE when he lived. On the other hand the geographical section alludes to the ascendency of Rome (रोमाक) not the yavana-s in the west. So we get a window between the Roman conquest of Greece around 130 BCE to 150 CE. This window is fairly consistent with the सूर्यसिद्धान्त's पाञ्चरात्रिक elements being from the layer of the पाञ्चरात्रिक tradition that was already known to and adopted by some of the yavana successors of Alexander of Macedon prior to the common era (E.g. the Heliodorus garuDa pillar from 140 BCE).

**On certain Hindu astronomical ideas in the सूrya-सिद्दान्त**\
The presence of multiple layers in the extant सूrya-siddhAnta suggests that the presence of Greek words cannot automatically mean that all ideas therein are of Greco-Mesopotamian origin. The multiple redactions of the text imply that they could have been introduced later when the siddhAnta-s were consolidated by later naturalists like वराहमिहिर. The पाञ्चरात्रिक origin mythology positions itself as being founded upon vaidika origin mythology. This position is indeed valid because the ideas presented there are part of various traditions found in the ऋग्वेद itself, such as in the हिरण्यगर्भ सूक्त, puruSha सूक्त, विश्वकर्मा सूक्त (10.82) and नासदीय सूक्त. It should be noted that it clearly indicates a spherical universe with spherical earth. Now white indologists and historians of science have argued that the idea of a spherical earth emerged first in Greece perhaps due to their early sages such as Empedocles or Parmenides between 500-450 BCE and was solidified by Plato and Aristotle. From the Greek world it is supposed to have reached the Hindus after the Alexandrian conquest, who till then were flat-earthers. Thus, the spherical earth in the सूrya-siddhAnta is supposed to be due to Greek influence. This idea is plainly wrong. Long ago the great patriot B.G. Tilak had shown that the vaidika cosmography conceived the earth as spherical surrounded by the multiple rajas and not as a flat plane above which the sky was anchored. Further, some have argued that the vaidika cosmography saw the earth as a concave counterpart of the sky, based on the common refrain in the veda द्याव-पृथिवी (the deities of the sky and earth). This has also been shown to be erroneous by Tilak who pointed out the veda clearly knows of space beneath the earth. The sphericity of the earth is alluded to in the secret language in the famous cosmographic सूक्त of the RV (1.164) in ऋक् 1.164.34-35. It is further clarified in the shatapatha ब्राह्मण as:\
[अयं वै लोको गार्हपत्यः । परिमण्डल उ वा अयम् लोकः ।]{style="color:#99cc00;"} (ष्B 7.1.1.37)\
This world, i.e. the earth is the गार्हपत्य; this world is verily spherical. Any educated shrauta ritualist knows that he meditates on this statement while he installs the गार्हपत्य altar which is explicitly equated with the earth.\
Similarly in ष्B 7.4.1.10 and 9.1.2.40 the sun is similarly described as being spherical:\
[एवैतद् आदित्यम् उपदधाति स हिरण्मयो भवति परिमण्डल ।]{style="color:#99cc00;"}\
[असौ वा आदित्यो हृदयं श्लक्ष्ण एष श्लक्ष्णं हृदयम् परिमण्डल एष परिमण्डलं ।]{style="color:#99cc00;"}

Thus, it is clear that the spherical earth and sun was a concept among Hindus long before any later Greek influence on them. Hence, it is mention in the पाञ्चरात्रिक origin mythology in the सूrya-siddhAnta is not Greek in origin by simply a development of the vaidika thought. Further, in the veda it is clear that the earth is considered to be of finite size and much smaller than the universal sphere (expressed in ऋक् RV 1.52.11). With this idea in place and combined with the notion of sphericity there is no reason that the Hindus did not independently seek to find the diameters of the Earth, sun and moon. Indeed, evidence for their independent attempts in this regard comes from the lecture of संजय to धृतराष्ट्र in the महाभारत 6.13.39-45 ("critical"). This so called भूमिपर्वन् which contains an early version of the much maligned पौरानिc cosmography gives the results one of these early attempts of the Hindus. It presents a primitive planetary theory of eclipses with both the स्वर्भानु (already found in RV 5.40.5 and ताण्ड्य ब्राह्मण 6.6.8; also points to a tradition of astronomical observations of eclipses by the atri-s) and राहु (already present in the AV-vulgate 19.9.9). Given that this theory falls out of vogue in the later more advanced siddhAnta-s, where the correct explanation had been discovered, and the fact that it uses the archaic vaidika term स्वर्भानु, it is clear that it represents a pre-सिद्धान्तिc attempt to measure the diameter of the sun, moon and स्वर्भानु. The स्वर्भानु-राहु theory with these diameters or the bizarre value of pi (\~3.4) are not known from the Greeks who first attempted such estimates like Hipparchus or Mesopotamian sources. A continuation of the the same tradition of measurement is preserved in the ब्रह्माण्ड पुराण (Bह्P). Here the value of circumferences of the sun by its diameter acquires a more normal value of \~3. Thus, it establishes beyond doubt that there was a indeed an endogenous tradition of attempting to estimate planetary dimensions among the Hindus, which had a deeper history than what we know in this regard of the Greeks (they could very well have their own deep history that is now lost).

From the results of R. Balasubramanian we may take the सूrya-siddhAnta yojana to be 7.89 Km from which we get the सूर्यसिद्धान्त planetary dimension parameters thus:\
Moon diameter: 480 yojana-s=3791.1552 Km; Modern value: 3474.2 Km\
Earth diameter 5000 yojana-s = 12488.21 Km Modern value: 12742 Km

  - ------------------------- ----------
  सूrya siddhAnta M/E ratio   0.303
  Aristarchus M/E ratio      .317-.39
  Ptolemy M/E ratio          0.292
  Modern M/E                 0.272

  - ------------------------- ----------

The white indologists inform us that the सूrya siddhAnta has definitely borrowed its Moon/Earth values from the yavana-s because they are close. We are told that till that point the Hindus were either flat-earthers, who stuck to an enormous number as the value of the earth's length or that they had borrowed a huge radius from the Babylonians. It was Aristarchus who first derived the ratios using eclipse observations which he correctly interpreted as shadows. But Hindus had no clue that they eclipses were due to shadows but ascribed them to राहु and ketu. While indeed the स्वर्भानु/राहु theory was the old one with which the Hindus began, it is clear that even within the realm of the स्वर्भानु theory the Hindus were coming to realize that the postulated entities were actually made of shadows. This is apparent in the statement:\
[तुल्यस् तयो।अस्तु स्वर्भानुर् भूत्वाधस् तात् प्रसर्पति ।]{style="color:#99cc00;"}\
[उद्धृत्य पृथिवीछायां निर्मितो मण्डलाकृतिः ॥]{style="color:#99cc00;"} Bह्P 1.24.101\
स्वर्भानु is equal in extant to both \[the sun and moon]. It moves beneath them. It made out of the shadow of the Earth and has a spherical shape.

Thus, the Hindus had arrived at an understanding of shadows as the basis of eclipses within their own system, and could have corrected their theory regarding the dimensions of the earth without outside help. Looking at the above ratios we find that interestingly they do not match the range of Aristarchus and they are greater than that of Ptolemy, even if close. We do not have values of this type in any Babylonian source. So, rather than claim borrowing from an unknown Greek source, it is more likely that Hindu arrived at it by themselves.

Finally, we may consider the interesting issues of the planetary angular diameters provided by सूrya-siddhAnta and compare them to other estimates:

  - -------- ------------ --------- ------- --------
  Planet    सूर्यसिद्धान्त   Ptolemy   Tycho   Modern
  Mars      2.0          1.57      1.67    0.4
  Saturn    2.5          1.74      1.83    0.34
  Mercury   3.0          2.09      2.17    0.16
  Jupiter   3.5          2.61      2.75    0.8
  Venus     4.0          3.13      3.25    1.05

  - -------- ------------ --------- ------- --------

What is clear is that all pre-modern observers way over-estimated their planetary diameters, illustrating the short-comings of the unaided eye. Ptolemy himself attributed these values to Hipparchus who is reputed to have said: "The sun is 30 times the apparent diameter of the smallest star and the diameter of Venus which appears to be the largest star is 1/10th the apparent diameter of the sun." In his scheme Jupiter is 1/12, Mercury is 1/15, Saturn 1/18 and Mars 1/20 that of the sun. Now the white indologists hold that the Hindus again copied their angular diameters from the Greeks to present them in the सूर्यसिद्धान्त. However, as we can see above the values of Ptolemy/Hipparachus, while following the same order as the Hindus, are very different. In fact the Hindu series is a far simpler one, with merely an increment of .5 per planet. We find no evidence for such planetary diameter estimates among the Babylonians or for any occurrence in the Greek world prior to Hipparchus. So just going by the सूर्यसिद्धान्त and Ptolemy's words either could claim priority. However, when we examine the more primitive Hindu model for planetary diameters provided in the Bह्P 1.24 we already see them reporting a system of planetary diameters in yojana-s:

  - -------- --------
  Venus     1125
  Jupiter   843.75
  Mars      632.8
  Saturn    632.8
  Mercury   474.6

  - -------- --------

From this it is clear that the Hindus already had a tradition of planetary diameters and the new order reported in the सूर्यसिद्धान्त could merely be an emendation of that older tradition.

While there is no doubt that the yavana-s and Arya-s participated in an interactive knowledge revolution over a prolonged period (which led to exchanges in medicine, various religious/philosophical traditions, astronomy and astrology) the presence of a clear non-Babylonian older tradition of the Hindus suggests that they were not simply plagiarists in the interaction. They are likely to have arrived at the idea of spherical planets with particular diameter entirely on their own.

Footnote 1: Below is a quote from an early American missionary indologist who collected Hindu astronomical texts: "*It is now well known that Hindu culture cannot pretend to a remoter origin than 2000 B.C., and that, though marked by striking and eminent traits of intellect and character, the Hindus have ever been weak in positive science; metaphysics and grammar---with, perhaps, algebra and arithmetic, to them the mechanical part of mathematical science--- being the only branches of knowledge in which they have independently won honorable distinction. That astronomy would come to constitute an exception to the general rule in this respect, there is no antecedent ground for supposing. The infrequency of references to the stars in the early Sanskrit literature, the late date of the earliest mention of the planets, prove that there was no special impulse leading the nation to devote itself to studying the movements of the heavenly bodies. All evidence goes to show that the Hindus, even after they had derived from abroad a systematic division of the ecliptic, limited their attention to the two chief luminaries, the sun and moon, and contented themselves with establishing a method of maintaining the concordance of the solar year with the order of the lunar months. If, then, at a later period, we find them in possession of a full astronomy of the solar system, our first impulse is to inquire, whence did they obtain it? A closer inspection does not tend to inspire us with confidence in it as of Hindu origin. We find it, to be sure, thoroughly Hindu in its external form, wearing many strange and fantastic features which are to be at once recognized as of native Indian growth; but we find it also to contain much true science, which could only be derived from a profound and long-continued study of nature. The whole system, in short, may be divided into two portions, whereof the one contains truth so successfully deduced that only the Greeks, among all other ancient nations, can show anything worthy to be compared with it; the other, the framework in which that truth is set, composed of arbitrary assumptions and absurd imaginings, which betray a close connection with the fictitious cosmogonies and geographies of the philosophical and Puranic literature of India.*"

[In this context one may also see another American indologist Whitney's comment in our earlier partial note on the वेदाङ्ग ज्योतिष.](https://manasataramgini.wordpress.com/2006/12/02/vedanga-jyotisha-and-other-ramblings-on-early-hindu-calenders/)

While one one might object that citing an old American missionary is anachronistic in this day and age of studies on "Pre-modern knowledge systems of India", it is remarkable to note that Euro-American indologists in the tradition of Neugebauer, Witzel, Pingree and his successors, Minkowski, and the like essentially say the same thing in a sugar-coated fashion.


