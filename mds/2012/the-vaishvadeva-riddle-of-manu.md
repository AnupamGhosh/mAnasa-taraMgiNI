
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The vaishvadeva riddle of manu](https://manasataramgini.wordpress.com/2012/09/23/the-vaishvadeva-riddle-of-manu/){rel="bookmark"} {#the-vaishvadeva-riddle-of-manu .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 23, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/09/23/the-vaishvadeva-riddle-of-manu/ "7:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In 8th मण्डल of the ऋग्वेद there is famous vaishvadeva riddle of father manu vaivasvata (RV 8.29) in the peculiar partial विराट् (dvipada विराट्) meter. In our tradition the one who identifies all the deva-s in it becomes the knower of the deva-s.

[बभ्रुर् एको विषुणः सूनरो युवाञ्ज्य् अङ्क्ते हिरण्ययम् ॥]{style="color:#0000ff;"}

One is a brown, many-formed merry youth, anointing \[himself] with a golden hue.

[योनिम् एक आ ससाद द्योतनो ऽन्तर् देवेषु मेधिरः ॥]{style="color:#0000ff;"}

One is seated at the source of light, the wise one in midst of the deva-s.

[वाशीम् एको बिभर्ति हस्त आयसीम् अन्तर् देवेषु निध्रुविः ॥]{style="color:#0000ff;"}

One bears in his hand a metal ax, the resolute one among the deva-s.

[वज्रम् एको बिभर्ति हस्त आहितं तेन वृत्राणि जिघ्नते ॥]{style="color:#0000ff;"}

One bears a vajra, firmly in his hand, with which he slays the वृत्र-s

[तिग्मम् एको बिभर्ति हस्त आयुधं शुचिर् उग्रो जलाष भेषजः ॥]{style="color:#0000ff;"}

One bears a sharp weapon in his hand, and is shining and fierce, with soothing medicines.

[पथ एकः पीपाय तस्करो यथां एष वेद निधीनाम् ॥]{style="color:#0000ff;"}

One makes the paths prosperous, like a thief this one knows the sites of treasures.

[त्रीण्य् एक उरुगायो वि छक्रमे यत्र देवासो मदन्ति ॥]{style="color:#0000ff;"}

One taking three steps with his wide stride has reached where the deva-s rejoice.

[विभिर् द्वा छरत एकया सह प्र प्रवासेव वसतः ॥]{style="color:#0000ff;"}

On bird-like vehicles, two fly in along with one \[goddess], like travelers going to a dwelling.

[सदो द्वा छक्राते उपमा दिवि सम्राजा सर्पिरासुती ॥]{style="color:#0000ff;"}

Two, have set up their seat in the highest heaven, supreme rulers, drinking melted butter.

[अर्छन्त एके महि साम मन्वत तेन सूर्यम् अरोछयन् ॥]{style="color:#0000ff;"}

Some sing the great sAman they have conceived, with which they have made the sun shine.

The complete and correct interpretation of all the deva-s in the riddle has not been achieved by सायण. Though, to one familiar with vaidika tradition and ritual the answers should be clear. He, who fails to grasp these deva-s, will see his vaidika krama-s to be futile as Ahuti-s made on ash.


