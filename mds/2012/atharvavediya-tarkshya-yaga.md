
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [आथर्ववेदीय तार्क्ष्य याग](https://manasataramgini.wordpress.com/2012/08/25/atharvavediya-tarkshya-yaga/){rel="bookmark"} {#आथरववदय-तरकषय-यग .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 25, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/08/25/atharvavediya-tarkshya-yaga/ "7:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier seen the mantra deployments relating to the [vedokta Ashu-तार्क्ष्य vidhi](https://manasataramgini.wordpress.com/2007/08/19/veda-mantrokta-ashu-garuda-vidya/). We shall now consider another तार्क्ष्य vidhi practiced by the knowers of the अथर्वण shruti. The day before the rite a pandal is put up and and decorated with flowers and flags with pictures of eagles. The ground is flattened and scents are dispersed into the air. In the eastern side of the pandal a circle with a lotus figure inscribed within it is drawn with white powder. It is then decorated with colored powders. In the night before the ritual (around the midnight hour), the ritualist offers bali in baskets at the four cardinal points around the pandal. The bali might comprise of meat, fish, beer or semicircular भक्षण-s (colloquially termed करङ्जिक) and is offered to the four dreadful विनायक-s mentioned in the AV tradition and vainateya. He then lights four ghee lamps, one beside each bali.

The rite begins at the abhijit hour on the bright अष्टक day. An image of the fierce garuDa is installed in the center of the white circle with the mantra:

[त्यम् ऊ षु वाजिनं देवजूतं सहोवानं तरुतारं रथानाम् ।]{style="color:#0000ff;"}\
[अरिष्टनेमिं पृतनाजिम् आशुं स्वस्तये तार्क्ष्यम् इहा हुवेम ॥]{style="color:#0000ff;"} (AVS 7.85.1c)

An अभिषेक of the image of garuDa is performed with the mantra:

[इमा आपः प्र भराम्य् अयक्ष्मा यक्ष्म नाशनीः ।]{style="color:#0000ff;"}\
[गृहान् उप प्र सीदाम्य् अमृतेन सहाग्निना ॥]{style="color:#0000ff;"} (AVS 3.12.9)

Then the ritualist offers sandal paste, flowers, incense, camphor, lamps of ghee, वटक-s flavored with pepper with a hole in the middle and burns guggulu. The ritualist and his people then do a प्रदक्षिण of the image followed by complete नमस्कार-s. Then either in front of the image of garuDa or to its south he makes a square स्थण्डिल and starts a fire and offers ghee while reciting :

[क्रव्यादम् अग्निं प्र हिणोमि दूरम् यमराज्ञो गछतु रिप्रवाहः ।]{style="color:#0000ff;"}\
[इहायम् इतरो जातवेदा देवो देवेभ्यो हव्यं वहतु प्रजानन् ॥]{style="color:#0000ff;"}(AVS 12.2.8)

[यो अग्नौ रुद्रो यो अप्स्व् अन्तर् य ओषधीर् वीरुध आविवेश ।]{style="color:#0000ff;"}\
[य इमाविश्वा भुवनानि चाकॢपे तस्मै रुद्राय नमो अस्त्व् अग्नये ॥]{style="color:#0000ff;"}(AVS 7.87.1)

[स रुद्रो वसुवनिर् वसुदेये नमोवाके वषट्कारोऽनु संहितः ॥]{style="color:#0000ff;"} (AVS_13,4.26a)

Then he joins his palms and does a नमस्कार to the fire.

Then he begins making oblations of ghee with the following mantra-s by uttering [ऒं वैनतेयाय स्वाहा ॥ इदम् न मम ॥]{style="color:#0000ff;"} (at the end of each ऋक् and before the स्वाहाकार mantra he utters the formula [ॐ स्वस्ति नस् तार्क्ष्यों ॥]{style="color:#0000ff;"}) :

[सुपर्णस् त्वा गरुत्मान् विष प्रथमम् आवयत् ।]{style="color:#0000ff;"}\
[नामीमदो नारूरुप उतास्मा अभवः पितुः ॥]{style="color:#0000ff;"}(AVS 4.6.3)

The good-winged गरुत्मान् first of all, O Poison, swallowed you;\
You could not question him or intoxicate him; you became food \[for him].

[दिव्यं सुपर्णं पयसं बृहन्तम् अपां गर्भं वृषभम् ओषधीनाम् ।]{style="color:#0000ff;"}\
[अभीपतो वृष्ट्या तर्पयन्तम् आ नो गोष्ठे रयिष्ठां स्थापयाति ॥]{style="color:#0000ff;"}(AVS 7.39.1)

The great strong celestial eagle, the source of the waters, the lord of the herbs,\
may he install in our cattle-shed, with wealth and please him with the shower of rains.

[अति धन्वान्य् अत्य् अपस् ततर्द श्येनो नृचक्षा अवसान दर्शः ।]{style="color:#0000ff;"}\
[तरन् विश्वान्य् अवरा रजंसीन्द्रेण सख्या शिव आ जगम्यात् ॥]{style="color:#0000ff;"}(AVS 7.41.1)

Observing men, and looking at the dwelling, the raptor has traversed over the fields and waters;\
May he, with indra for a friend, auspicious, traversing all the atmospheric realms, come here \[to the ritual].

[श्येनो नृचक्षा दिव्यः सुपर्णः सहस्रपाच् छतयोनिर् वयोधाः ।]{style="color:#0000ff;"}\
[स नो नि यछाद् वसु यत् पराभृतम् अस्माकम् अस्तु पितृषु स्वधावत् ॥]{style="color:#0000ff;"} (AVS 7.41.2)

The raptor, observing men, the celestial eagle of great strength, with a thousand talons and hundred abodes;\
He shall give us the wealth that was taken away from us; may he partake of the स्वधा offerings with the ancestors.

[उत् केतुना बृहता देव आगन्न् अपावृक् तमोऽभि ज्योतिर् अश्रैत् ।]{style="color:#0000ff;"}\
[दिव्यः सुपर्णः स वीरो व्य् अख्यद् अदितेः पुत्रो भुवनानि विश्वा ॥]{style="color:#0000ff;"}(AVS 13.2.9)

With the great banner has the god ascended, dispelling darkness and spreading light.\
The celestial eagle, the hero, the son of aditi, has looked upon on all the worlds.

He might offer as many rounds of oblations as required with the above mantra-s.

Thereafter he makes a final triad of ghee oblations with the below ऋक्-s using the formula [ॐ अरिष्टनेम्यै स्वाहा ॥ + इदम् न मम]{style="color:#0000ff;"}:

[स्वस्त्ययनं तार्क्ष्यम् अरिष्टनेमिं महद् भूतं वायसम् देवतानाम् ।]{style="color:#0000ff;"}\
[असुरघ्नम् इन्द्र सखं समत्सु बृहद् यशो नावम् इव आरुहेम ॥]{style="color:#0000ff;"} (ऱ्‌व्ख़्ह् 2.4.1)

[अंहो मुचम् आङ्गिरसं गयं च स्वस्त्य् आत्रेयं मनसा च तार्क्ष्यम् ।]{style="color:#0000ff;"}\
[प्रयत पाणिश् शरणं प्रपद्ये स्वस्ति संबाधेष्व् अभयन् नो अस्तु ॥]{style="color:#0000ff;"} (ऱ्‌व्ख़्ह् 2.4.2)

[स्वस्ति न इन्द्रो वृद्धश्रवाः स्वस्ति नः पूषा विश्ववेदाः ।]{style="color:#0000ff;"}\
[स्वस्ति नस् तार्क्ष्यो अरिष्टनेमिः स्वस्ति नो बृहस्पतिर् दधातु ॥]{style="color:#0000ff;"} (RV 1.089.06)

He then makes a single combined ghee oblation to the deva सवितृ and garuDa with below mantra (using [ऒं सवित्रे स्वाहा गरुत्मते स्वाहा ॥+ इदं न मम]{style="color:#0000ff;"}):

[पश्चेदम् अन्यद् अभवद् यजत्रम् अमर्त्यस्य भुवनस्य भूना ।]{style="color:#0000ff;"}\
[सुपर्णो अङ्ग सवितुर् गरुत्मान् पूर्वो जातः स उ अस्यानु धर्म ॥]{style="color:#0000ff;"} (RV 10.149.03)

\[Thereafter, that other world of the worship-worthy realm of the immortal gods came into being; सविता's eagle-bodied गरुत्मान् was indeed the first born one who constantly upheld his dharma.]

He then brings the fire ritual to a conclusion with the recitation of agastya:

[अग्ने नय सुपथा राये अस्मान् विश्वानि देव वयुनानि विद्वान् ।]{style="color:#0000ff;"}\
[युयोध्य् अस्मज् जुहुराणम् एनो भूयिष्ठां ते नमौक्तिं विधेम ॥]{style="color:#0000ff;"} (RV 1.189.01)

Then water is consecrated in a large pitcher with the AV सूक्त-s beginning with[ OM Apo hi ष्ठा... ]{style="color:#0000ff;"}and ending with [शं न आपो धन्वन्याः ...]{style="color:#0000ff;"}Thereafter the ritualist bathes idol of garuDa, with that water even as women sing songs alluding to the heroic deeds of garuDa and play on their musical instruments. This lustration is concluded with a beating of drums and blowing of trumpets and conchs.

Thereafter the ritualist offers तर्पण with water, milk, curds, ghee and fruit juices using the below ऋक्-s followed by: [ऒं गरुत्मन्तम् तर्पयामि ॥]{style="color:#0000ff;"}

[मनोजवा अयमान आयसीम् अतरत् पुरम् ।]{style="color:#0000ff;"}\
[दिवं सुपर्णो गत्वाय सोमं वज्रिण आभरत् ॥]{style="color:#0000ff;"} (RV 8.100.08)

With the speed of thought, swooping into the metal fort he flew on;\
The eagle flew to heaven and brought soma to the wielder of the vajra.

[वैनतेय सोमं पिब सोमं पिबतु वृत्रहा ।]{style="color:#0000ff;"}\
[सोमं धनस्य सोमिनो मह्यं ददातु सोमिनः ॥]{style="color:#0000ff;"} (ऱ्‌व्ख़्ह् 2.6.21)

O vainateya drink soma ! O slayer of वृत्र drink soma!\
The soma drinkers drinking soma grant the wealth; the soma drinkers grant greatness.

[नाके सुपर्णम् उपपप्तिवांसं गिरो वेनानाम् अकृपन्त पूर्वीः ।]{style="color:#0000ff;"}\
[शिशुं रिहन्ति मतयः पनिप्नतं हिरण्ययं शकुनं क्षामणि स्थाम् ॥]{style="color:#0000ff;"} (RV 9.085.11)

The eager ones with their chants seek the eagle who had soared to the heavens.\
The mantra-s gently caress the chick worthy of praise, the golden bird has stationed himself on the earth.

[शाक्मना शाको अरुणः सुपर्ण आ यो महः शूरः सनाद् अनीळः ।]{style="color:#0000ff;"}\
[ yach chiketa satyam it tan na मोघं vasu स्पार्हम् uta jetota dAtA ||]{style="color:#0000ff;"} (RV 10.055.06)

Powerful in his strength is the glowing eagle, great hero, the ancient one who never roosts;\
That which he knows is the truth and never unsuccessful; he indeed and wins and gives the much desired wealth.

[यं सुपर्णः परावतः श्येनस्य पुत्र आभरत् ।]{style="color:#0000ff;"}\
[शतचक्रं यो ऽह्यो वर्तनिः ॥]{style="color:#0000ff;"} (RV 10.144.04)

What from afar, the eagle, child of the raptor (here implying सविता), has brought\
\[is] that around which a hundred chakra-s revolve along with the snake.

Thereafter he takes some of the remaining water from the pitcher and sprinkles it upon the votaries seeking relief from illness caused by अभिचार, toxins and troubles while reciting the following mantra:

[तं वां रथं वयम् अद्या हुवेम स्तोमैर् अश्विना सुविताय नव्यम् ।]{style="color:#0000ff;"}\
[अरिष्टनेमिम् परि द्याम् इयानं विद्याम् एषं वृजनं जीरदानुम् ॥]{style="color:#0000ff;"} (RV 1.180.10)

That your new chariot, with mantra recitations we invoke today, O ashvin-s, for our wellbeing,\
He who circles around heaven with unassailable wings, may we discover strengthening medicines in great abundance.

Then he sprinkles his own and the gathered participants' weapons with the incantation for the destruction of his enemies:

[ye rathino ye अरथा असादा ye cha सादिनः |]{style="color:#0000ff;"}\
[सर्वान् अदन्तु तान् हतान् गृध्राः श्येनाः पतत्रिणः ॥]{style="color:#0000ff;"}(AVS 11.10.24)

Chariot-riders, warriors without chariots, infantry men and cavalrymen,\
All these, slain, let the vultures, eagles, and raptors devour them.

He ties an amulet with the following incantation:

[गरुड पक्ष निपातेन भूमिम् गच्छ महा यशाः ।]{style="color:#0000ff;"}\
[गरुडस्य जात मात्रेण त्रयो लोकाः प्रकम्पिताः ।|]{style="color:#99cc00;"} (ऱ्‌व्ख़्ह् 2.1.3)

The snakes of great fame, struck by the wings of garuDa flee beneath the ground,\
Even as garuDa was born the three worlds quaked.

The ritualist then looks at the sky. If he see a raptor circling then his rite has had complete success. At the conclusion of the ritual the poor and disabled are fed.


