
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सरस्वती the देवी and the सरस्वती the नदी](https://manasataramgini.wordpress.com/2012/06/03/sarasvati-the-devi-and-the-sarasvati-the-nadi/){rel="bookmark"} {#सरसवत-the-दव-and-the-सरसवत-the-नद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 3, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/06/03/sarasvati-the-devi-and-the-sarasvati-the-nadi/ "7:13 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[मसित्ãम् द्ûर्âत् फ़्रस्र्ûत्ãम् ýâ अस्ति अववैति]{style="color:#0000ff;"}\
[ masô ýatha vîspå imå âpô ýå zemâ paiti frataciñti]{style="color:#0000ff;"}\
[ ýâ amavaiti fratacaiti hukairyât haca barezanghat aoi zrayô vouru-kashem ||]{style="color:#0000ff;"}

The large river, known afar, that is as large as the whole of the waters that run along the earth; that runs powerfully from the height hukairya down to the sea vouru-kasha.

[ýअओज़ेञ्ति vîspe karanô zrayâi vouru-kashaya â-vîspô maidhyô ýaozaiti]{style="color:#0000ff;"}\
[ ýat hîsh aoi fratacaiti ýat hîsh aoi frazhgaraiti aredvî sûra anâhitâ,]{style="color:#0000ff;"}\
[ ýenghe hazangrem vairyanãm hazangrem apakhzhâranãm,]{style="color:#0000ff;"}\
[ kascitca aêshãm vairyanãm kascitca aêshãm apakhzhâranãm]{style="color:#0000ff;"}\
[caतहवरॆ-सतॆम aयरॆ-बरनãम हवसपअि निरॆ बरॆमनअि ||]{style="color:#0000ff;"}

All the shores of the sea vouru-kasha are boiling over, all the middle of it is boiling over, when she runs down there, when she streams down there, she, aredvî sûra anâhitâ, who has a thousand cells and a thousand channels: the extent of each of those cells, of each of those channels is as much as a man can ride in forty days, riding on a good horse.

[ainghåsca mê aêvanghå âpô apakhzhârô vî-jasâiti vîspâish aoi karshvãn ýâish hapta,]{style="color:#0000ff;"}\
[ ainghåsca mê aêvanghå âpô hamatha ava-baraiti hãminemca zayanemca,]{style="color:#0000ff;"}\
[ hâ-mê âpô ýaozhdadhâiti hâ arshnãm xshudrå hâ xshathrinãm garewãn hâ xshathrinãm paêma || ]{style="color:#0000ff;"}yaShT 5.3-5

From this river of mine alone flow all the waters that spread all over the seven Karshvares; this river of mine alone goes on bringing waters, both in summer and in winter. This river of mine purifies the seed in males, the womb in females the milk in females' breasts.

[अम्बितमे नदीतमे देवितमे सरस्वति ।]{style="color:#0000ff;"}\
[अप्रशस्ता इव स्मसि प्रशस्तिम् अम्ब नस् कृधि ॥]{style="color:#0000ff;"}

Best mother, best of rivers, best of goddesses, sarasvati; We are,as though without praise, O mother, make us praised.

[त्वे विश्वा सरस्वति श्रितायूंषि देव्याम् ।]{style="color:#0000ff;"}\
[शुनहोत्रेषु मत्स्व प्रजां देवि दिदिड्ढि नः ॥]{style="color:#0000ff;"}

In you, O goddess sarasvati, all lives are situated; be, pleased with shunahotra-s; O goddess grant us progeny.

[इमा ब्रह्म सरस्वति जुषस्व वाजिनीवति ।]{style="color:#0000ff;"}\
[या ते मन्म गृत्समदा ऋतावरि प्रिया देवेषु जुह्वति ॥]{style="color:#0000ff;"} RV 2.41.16-18

Rich in vigor, O sarasvati, be pleased with these incantations of ours; the mental creations which the गृत्समद-s dear to the gods bring to you the upholder of the ऋत.

The देवी सरस्वती and her Iranian counterpart अरेद्वी sUrA anAhitA are descendents of the ancestral proto-Indo-European trans-functional goddess. They retain practically all the features of this ancestral goddess, some of which might be muted or redistributed among the European branches of the Indo-European tree:

 1.  She is a regal or warrior goddess: This trait is very clear in the Vedic सरस्वती and the Iranian anAhitA, a trait, which in India, continues strongly in the later hypertrophy of the trans-functional goddess in the traditions of the पुराण-s and the tantra-s. In the Greek world this trait is strongly preserved in their cognate Athena. In the Roman world she appears as seispes mAter रेगीन, and the last word in her triple title (like the Iranian triple title) corresponds to her role as the deity of the royal warrior, although far more muted than in the case of the former three. This regal link connection persists in the Celtic Eriu in the form of the tradition of the consecration of royal power in Ireland through Eriu and also is redistributed to the Celtic goddess Brigantia.


 2.  Most importantly she is a goddess of water and moisture: In the world of the Arya-s this is emphasized by her name सरस्वती -- full of ponds (PIE seles-\>saras) and among the Iranians by the term अरेद्वी meaning moisture. This is also preserved in Greece in the form of the epithets of Athena like Herse which means dew-giver. Likewise, her epithet Glaucopis is explained as being the one who pours water from the skies and then clears to be blue in color like her eyes. In the Slavic world her triple title is माति स्य्रा ज़ेम्ल्जा, in which the स्य्रा indicates her moisture. Similarly, her other name mokoshi means moisture. In the roman world this function appears to have been redistributed to Ceres. In the Indic and Iranian texts in particular she may be described as having the form of a physical river flowing down from a great height, but at the same time she is also the heavenly water or river (including the Milky Way).


 3.  She is seen as bestowing various human wants like plants, ghee, oil, honey, progeny and being the cause of fertility of the land. She also importantly bestows good speech and intelligence -- but this trait is prominently expressed only in the Indo-Aryan and Greek spheres in सरस्वती and Athena (though there are some allusions to this in the Iranian praise of anAhitA).

Now, the 10th मण्डल of the ऋग्वेद contains a famous सूक्त that contains a list of rivers that every practicing Arya utters during his bathing rituals to this date:

[इमं मे गङ्गे यमुने सरस्वति शुतुद्रि स्तोमं सचता परुष्ण्य् आ ।]{style="color:#0000ff;"}\
[असिक्न्या मरुद्वृधे वितस्तयार्जीकीये शृणुह्य् आ सुषोमया ॥]{style="color:#0000ff;"}

Be pleased with my laud, O गङ्गा, यमुना, sarasvati, shutudri, and परुष्णि;\
with asikni, मरुद्वृध, वितस्ता, आर्जीकी and सुषोमा hear \[me].

[तृष्टामया प्रथमं यातवे सजूः सुसर्त्वा रसया श्वेत्या त्या ।]{style="color:#0000ff;"}\
[त्वं सिन्धो कुभया गोमतीं क्रुमुम् मेहत्न्वा सरथं याभिर् ईयसे ॥ ऋV १०।७५।५-६]{style="color:#0000ff;"}

First with तृष्टाम you rapidly flow forth, with susartu and rasA with श्वेती here,\
You sindhu, with kubha, gomati, krumu and mehatnu, seek you course together.
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-PsXw2gXx1XY/T8u4HddUNwI/AAAAAAAACYs/AIHt56FQnXs/s400/Indus_river.tributaries.jpg){width="75%"}
```{=latex}
\end{center}
```



The Indus system

The geographical concordance of these rivers to the hydrography of the north of the Indian subcontinent is strong. Most rivers can be identified and show some geographical ordering. In the first ऋक् we have rivers largely going from east to northwest:

गङ्गा -- यमुना -- सरस्वती- shutudri (Sutlej)-परुष्णि (Ravi) -- asikni (Chenab) -- मरुद्वृध (maruvardhan) -- वितस्ता (Jhelum) -- आर्जीकी (Haro) -- सुषोमा (Sohan).

The first two are the primary rivers of the गङ्गा system. Then shutudri, परुष्णि, asikni, वितस्ता, आर्जीकी and सुषोमा represent 5 successive rivers forming the east bank tributary system of the Indus. Of these the first four form the पञ्चनदी system before joining the Indus. Only मरुद्वृध which is apparently the minor stream called maruvardhan is obscure today. Geographically the description is largely regular in that the rivers are given in the order in which they would be encountered moving east to northwest till the Indus. Some have suggested that आर्जीकी is actually Beas, but this would break the order. Further, Vedic tradition outside the nadi-सूक्त does remember the विपाशा as a separate river; hence, आर्जीकि is more likely to be Haro.

In the second ऋक् we have:\
तृष्टाम (Tochi?) -- susartu (Kunar) -- rasa (Panjkora?) -- श्वेती (Swat)-- sindhu (Indus) -- kubha (Kabul) -- gomati (Gomal) -- krumu (Kurrum) -- mehatnu (Bara?).

Here Indus is named along with its west bank tributary system. In this case, the identity of some of the RV rivers with the modern streams is less certain to me, especially given that a clear direction pattern is not followed unlike the first ऋक्. The general logic here appears to be to name the outer streams first followed by the main streams joining the Indus from the west. Nevertheless, given the abundance of extant tributary rivers in this region it is clear that all these streams can be "accounted for". The main problem with the Indo-Iranian hydrography is that many of the hydronyms have been repeatedly used as the Indo-Iranians radiated out from their steppe homeland to occupy new territories. Thus, we have cognate names of sarasvati, sarayu, rasA, कुभा, sindhu both in the Indo-Aryan and Iranian spheres of activity, and appear in India proper, Afghanistan, and even the vicinity of the North Caucasus, where populations share a genetic signal with the Indo-Aryans that strongly stands out. Likewise, some cognate toponymns also appear in both in the Indic and Iranian worlds. However, in the case of the नदीसूक्त the presence of multiple unique river names of the पञ्चनद as well as the unique गङ्गा and यमुना suggest that at least this सूक्त was composed in northern India. This leaves us with only one major anomaly with respect to the rivers we see today -- the river सरस्वती.

Indo-Aryan tradition has an internal answer for this anomaly, i.e. the vanishing of the सरस्वती. A persistent tradition first encountered in the ब्राह्मण-s and reiterated by the महाभारत states that the सरस्वती river dried up or vanished. This is described best in the context of two major rituals, the सारस्वत sattra and the अपोनप्त्रीय. While these rituals are described in several ब्राह्मण-s, the best account vis-a-vis the disappearance of सरस्वती comes form the पञ्चविंश ब्राह्मण (PVB) account of the first सारस्वत sattra and the aitareya ब्राह्मण account of the अपोनप्त्रीय ritual. The PVB 25.10 (and also the जैमिनीय ब्राह्मण 2.297) states that the sattra is performed to the south of the place where the सरस्वती river vanishes starting on a new moon day. Then they move up the course of the सरस्वती, along the east bank performing devayajana as per the new moon and fullmoon models, and depending on the parvan culminating in a गोष्टोम or आयुष्टोम. The important points that emerge from this account are: 1) The explicit mention of a place where the सरस्वती vanishes. 2) The cryptic allusion to the end of the सरस्वती: the deva-s are supposed to have supported the sun in the sky with the सरस्वती but it collapsed and hence it is supposed to be full of curves ([तस्मात् सा कुब्जिमतीव]{style="color:#0000ff;"}). This suggests that the river was taking a winding course by the time of the ब्राह्मण probably suggestive of loss of velocity and sediment bulk. 3) At some point along the journey of the ritualists, the दृषद्वती is supposed to join the सरस्वती on the east bank. Here they offer a plate of boiled rice to अपाम् नपात् with सूक्त of गृत्समद in RV मण्डल 2. This indicates that the सरस्वती vanished to the south of the junction with the दृशद्वती, strongly supporting the identification of दृहद्वती with the chautang. 5) At a certain point from the point of the disappearance of सरस्वती they reach a place called प्रक्ष प्राश्रवण (प्लक्ष प्रास्रवन) where they terminate the rite with an इष्टि to अग्नी काम. 6) Here mention is made of a pool located to the north of a large ruined town where the ritualists like the videha king namin साप्य are supposed to have had their अवभृत bath. The mention of a large ruin on the course of the सरस्वती/दृशद्वती is of great interest because there are two prominent abandoned Harappan sites on their courses namely Kalibangan close to the सरस्वती-दृशद्वती संगंअ and Rakhigarhi further upstream on the दृशद्वति.

The अपोनप्त्रीय rite described in 2.19 of the aitareya ब्राह्मण mentions that the ऋषि कवष ऐलूष was driven away from a sattra on the banks of the सरस्वती by the other ritualists who declared that he was a son of दास woman, not a ब्राह्मण, and a fake. They drove him into the desert wishing that he would not drink the water of the सरस्वती and die there from thirst. As कवष was afflicted by thirst in the desert he invoked अपाम् नपात् with a सूक्त (RV 10.20) and the waters of the सरस्वती are said to welled out and run after him into the desert. The other ritualists realized that he was favored by the deva-s and called him back. From thence these mantra-s were used in the ritual for rains. What this account illustrates is that in the period of this ब्राह्मण text Indo-Aryans were inhabiting the banks of the सरस्वती, which was proximal to a desert. Further the account cryptically describes the waters of the सरस्वती being drawn into the desert where they are lost as per the सामवेदिc ब्राह्मण-s. Thus, it is clear that the Indo-Aryans preserved a memory of the actual termination of the सरस्वती in the desert.

In the late 1800s and 1900s philology was joined by geomorphology and archaeology in the quest for the सरस्वती. It was realized that there was a potential river course that connected the seasonal Hakra-Ghaggar and Chautang channels all the way to the Indian ocean in Gujarat. This channel was taken to be the lost सरस्वती. A Hungarian acquaintance brought to our attention how the aged Aurel Stein had realized in the 1940s from investigation of this dried up river channel that it was probably the सरस्वती and had a bearing on the age of the ancient texts of the Indo-Aryans like the veda-s and इतिहास-s. Subsequently, archaeology of the Harappan sites brought up a curious observation. Rather than the Indus valley, the bulk of the sites from the Early and Mature Harappan period lay in an extremely arid area within in what is now the western Indian desert. Closer examination showed that these sites lay clustered along what was supposed to be the dried up course of the सरस्वती going all the way from the region of the Panjab to the sea in Gujarat. This suggested that during the period from the early to the mature Harappan the सरस्वती was probably flowing perennially through what is now desert terrain (\~5200-3900 y BP).

Below are maps showing the geomorphology of the sindhu-सरस्वती-upper गङ्गा systems and the sites dating to the Early and Mature Harappan periods. The tracking of the सरस्वती channel by settlements is clear.
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-iuSdUipBUjo/T8xbxcICkdI/AAAAAAAACZw/08A6uezN34o/s400/IVC_geo_maps.jpg){width="75%"}
```{=latex}
\end{center}
```



[The big cities are R: Rakhigarhi; K: Kalibangan; H: Harappa; M: Mohenjo-daro; D: Dholavira (Giosan et al)]{style="color:#3366ff;"}

Temporal analysis of archaeological sites along the सरस्वती channels reveals that the peak of settlement was during the height of the Harappan civilization around 4500 y BP.\
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-NFfPlspVqIw/T8xbuzPgORI/AAAAAAAACZw/ZQIjzBkFfMA/s400/IVC4500.jpg){width="75%"}
```{=latex}
\end{center}
```



The decline of the Harappan urbanization and the rise of the regional settlements between 3900-3500 y BP show a dramatic decline in the settlements in the lower reaches of the सरस्वती channel along with a progressive withdrawal of settlements closer to the सरस्वती-दृशद्वती संगम. Thus, in the period between 3900-3500 BP we see that सरस्वती was unlikely to perennially carry water into the Indian Ocean and the settlement were withdrawing north-eastwards consistent with its vanishing into the desert downstream of the दृशद्वती confluence. This, suggests that this period is likely to correspond to the accounts of अपोनप्त्रीय or सारस्वत sattra ritual where the ritualist began from the point where it disappeared in the desert up to beyond the दृषद्वती confluence. Further, the occurrence of a large ruin (स्थूलार्म) beyond the दृषद्वती confluence noted in the PVB 25.10 is probably the ruin of the Harappan site of Kalibangan.
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-D8y0Rg6zxtM/T8xbtrH1ZNI/AAAAAAAACZw/ZH4m5r-5qVY/s400/IVC3900.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-A1JbdV0ruMk/T8xbtn3kM8I/AAAAAAAACZw/xTClvx3kU4k/s400/IVC3500.jpg){width="75%"}
```{=latex}
\end{center}
```



By around 3000 y BP, which marked the Painted-Greyware period, the settlements had nearly completely vanished downstream of the सरस्वती-दृषद्वती confluence and there remained only a few small sites along the upper reaches of the सरस्वती-दृषद्वती channels. This suggests that the ब्राह्मण texts clearly predated the PGW period.
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-ebyg6yd0MTU/T8xbti1__NI/AAAAAAAACZw/YLIF1HQDLt0/s400/IVC3000.jpg){width="75%"}
```{=latex}
\end{center}
```



Until recently the causes for the drying up of the सरस्वती remained highly unclear. Geomorphology suggested that at some point the यमुना used to flow west and meet the सरस्वती system rather than the गङ्गा system. Similarly, it has been noted that there is an ancient channel of the शुतुद्री that meets the सरस्वती channel and causes a widening of its course, suggesting that it too might have flowed into the सरस्वती. However, recent studies have shown that the सरस्वती was an active glacier fed river only the Pleistocene and not the Holocene. The capture of the यमुना by the गङ्गा system might have happened as early as 49,000 y BP, which is well before human civilization in the region. Likewise the शुतुद्री's capture by the Indus system was complete by at least 10,000 y BP (Clift et al). Thus, the capture of glacial sources probably had nothing direct to do with the final end of the सरस्वती in the late Harappan period though it indirectly contributed to it. Here, is where a recent combination of sedimentological and climatological studies comes in throw some light on what actually happened (Giosan et al). The results of this study are summarized in the below graphs: 1) Based on O18 isotopes they suggest a decline in the monsoons around 4000-5000 y BP. This decline contributed the relatively lower danger of massive flooding allowing the Harappan civilization to emerge as the flooding hit just the "right level" for agriculture by inundation. However, between 3000-4000 y BP the aridification went beyond the sustainability of agriculture by inundation. 2) Based on sediment dating it is clear that despite loss of the glacial sources the सरस्वती channel remained active with high sedimentation rate from 8000-4000 y BP both on the lower and upper courses. This suggests that it was sustained as a perennially flowing river by the monsoonal activity. However, as the monsoon decline reached a certain point around 3900 y BP the high sedimentation rate deposit vanish on the सरस्वती and low-sedimentation rate deposits are seen in the upper courses. This suggests that the immediate cause for end of the सरस्वती was the decline in monsoon activity rather than the loss of glacial sources, which had happened much earlier in the closing stages of the Pleistocene.
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-lL1CWHgqe08/T8xbuwOSulI/AAAAAAAACZw/6yvs0HSm6a8/s400/IVC_sediment.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[*Here G-H is for Ghaggar-Hakra, i.e. सरस्वती; the black symbols are high sedimentation rates, the white symbols are low sedimentation rates and the grey symbols are sand dunes. In panel B, the grey line is westerly precipitation while the black line is monsoon precipitation (from legend to Fig. 4 of Giosan et al).*]{style="color:#3366ff;"}

A study of the crop pattern shows that during the Early and Mature Harappan phases the most cultivated crop was barley (yava), whereas in the post-urban phase the most cultivated crop was rice (वृहि), with a clear decline of barley. Similarly, dAl (lentil) cultivation greatly increased in the post-urban period, suggesting that common Indian staple of dAl and rice became the mainstay around this period.This shift is marked by the increasing movement of settlements towards the गञ्गा system, where they were not affected by the aridification of the west. This shift is interesting because in the earlier Vedic texts like the ऋग्वेद rice is very infrequently mentioned, but barley is frequently mentioned. In the later ritual formalization of the ब्राह्मण period rice offerings become very important (odana). This again suggests that at least part of the ब्राह्मण material corresponds to the time of the post-urban phase of the Harappan civilization.
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-yKEn31EJcDs/T8xbvnkSTqI/AAAAAAAACZw/OW_kJtaGKlk/s400/crops.jpg){width="75%"}
```{=latex}
\end{center}
```



This then leads to the question as to whether the early saMhitA texts correspond to the mature Harappan period and is there a knowledge of a full-flowing सरस्वती in them?

This question and its answer are by no means trivial as some might think. Given that the सारस्वत sattra was being performed even when the सरस्वती was drying and the center of the civilization was moving towards the गङ्गा system, it is likely that it had some historic sanctity about it. This is corroborated by its extensive memory preserved in the महाभरत in terms of holy spots even when it was drying. The same is true of the manu स्मृति, which preserves a memory of the holiness of the region between the सरस्वती and the दृषद्वती (brahmavarta), even after the river had terminated in the desert. This sanctity of सरस्वती and memory of it being the core civilizational center (brahmavarta or the land of नहुष) is unlikely to have been associated with it if the invading Indo-Aryans had merely encountered it after its diminution and desiccation. This is especially so because at this point it was the गङ्गा system, not the सरस्वती, which was the main civilizational center (this is supported by the fact that in the सारस्वत sattra they finally terminate their journey with an अवभृत bath at कारपचव in the यमुना river). One could of course suggest the alternative that the knowledge of सरस्वती's civilizational significance came from the pre-existing Harappans, who at that point had been Aryanized by the invading Indo-Aryans and were adopting the Arya system. Or one might suggest that the Aryanized Harappans managed to convince the Arya-s of the sanctity of a drying river, to the point that the Arya-s now referred to it after their own great transfunctional goddess of proto-Indo-European vintage. These alternatives seem less likely to us because they do not go well with the processes surrounding the extensive Indo-Aryanization of Northern India, where the memetic contribution of the substratum was relatively limited in the early days of the Indo-Aryan entry. This raises a possibility, [which we have mentioned even in the past](https://manasataramgini.wordpress.com/2011/08/03/homers-illiteracy/), that the entry of Indo-Aryan happened during the peak of the Harappan period or before that. Of course this proposal has its own formidable, though not insurmountable, problems.

The RV saMhitA material is generally considered to be older than most (though not all) parts of the ब्राह्मण material. This appears plausible based on the observation that some of these rituals are likely to have required a complete ऋक् saMhitA. Thus, it is conceivable that the few explicit references to सरस्वती as the physical river in north India in the RV (e.g. the नदी सूक्त and RV 3.23, i.e. the भारताग्नि सूक्त) might belong to a period anterior to that alluded to in the ब्राह्मण-s. This supports at least those parts being composed at a time when the सरस्वती was more active and overlapping with the peak of the Harappan civilization. Here one may ask: What is the big deal ? Does the RV not abound in references to सरस्वती ? This, in our considered opinion, is the most common confusion with respect to the RV. Yes, the RV abounds in references to सरस्वती, but this is the देवी सरवती, and not a particular physical river. As noted above being a goddess fundamentally associated with water, she is often described in riverine terms -- this does not at all mean that they are talking about any particular physical river. Here is where the comparative method illuminates the issue. Just as अरेद्वी surA anAhitA is described as a massive physical river, the देवी सरस्वती is also similarly described by गृत्समद, भरद्वाज and वसिष्ठ. This does not mean that all their सूक्त-s are referring to the physical river. As there are so many parallels with the yaShT of anAhitA and the सूक्त-s of सरस्वती it is clear that much of these compositions are based on models that were already present in the Indo-Iranian period and actually referred to the divine or celestial river that fed all the rivers as per the old Indo-European cosmography. This said, it is nevertheless notable that the Indo-Aryans chose to call the river flowing in the Ghaggar-Hakra course as सरस्वती after their great trans-functional goddess. This suggests that after their entry into the subcontinent, their center of activity was likely to be associated with the Ghaggar-Hakra river that was still displaying notable through flow.

This leads us to a model, which we have articulated in the past, that the Indo-Aryan invasion probably led to a stimulation of civilizational development in the Harappan zone, rather than destroying it as proposed by some. In particular, we suspect that the Indo-Aryans of pastoralist origin in the Inner Eurasian steppes could actually connect the disparate civilizational centers in the Harappan zone by virtue of their inherent mobility. This allowed the development of the material uniformity which characterized the Harappan civilization at its peak. This uniformity was probably further stabilized by the introduction of the वर्ण system along with organization of the pre-Aryan Harappan people into formal guilds of jAti-s . That this happened is strongly suggested by the remarkable recitation that accompanies the पुरुषमेध (the Indo-Aryan human sacrifice: both real and symbolic), which is found in all the complete yajurveda saMhitA-s. As an example let us consider that from the माध्यन्दिन saMhitA, अध्याय 30. This recitation lists a large number of specialized occupations that are typical of more settled rather than pastoralist societies, some of which clearly match the Harappan society as suggested by archaeology:

*Manufacturers*\
मणिकार: A beadmaker (beads are a major production item of Harappan sites); A पेशस्कारी: a garment-maker woman; रजयित्री: A dyer woman (specialized dying workshops are found in Harappan sites); पल्पूली: A tanner woman; ajinasandha: a hide-preparer; हिरण्यकार: a goldsmith; वानिज with a balance: a baniya (balance weights are found in Indus sites); An अयस्ताप: a metal smelter; अञ्जनीकारी: a woman who makes cosmetics; कुलाल: A potter; इषुकार/ धनुष्कार/ ज्याकार: arrow smith; bow makers; rajjusarja: a rope-maker; कण्टकीकारी: a woman who makes needles; सुराकार: a beer brewer; रथकार: a car-maker; A कोशकारी: box-maker woman or a silk-maker woman.

*Diverse professionals*\
भिषक्: physician; नक्षत्रदर्श: astronomer; गणक: an accountant; prashna-विवाक: an consultant for questions; तक्ष: carpenter; मानस्कृत: an designer; adhikalpin: a casino manager; अभिषेक्तृ: a road sprinkler; दाश: a ferryman; भागदुघ: A pay distributer; kAri: mechanic; कर्मार: an engineer or smith.

*Policing activity*\
गृहप: a house guard; क्षत्तृ: a door-keeper; abhikrosha: a policeman; vanapa: A forest guard; दावप: fireman.

*Agriculture and animal husbandry*\
कीनाश: plowman; vapa: a seed sower; hastipa: elephant herder; ashvapa: horse herder; गोपाल: cowherd; अविपाल: shepherd; अजपाल: goatherd; shvanin: dog-breeder.
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-nufBpfrk6Gk/T9OXdvUvqEI/AAAAAAAACaE/DqSHrpXLUUQ/s400/stuff1-001.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[The rare instances of the horse have been gathered but it should be mentioned that their provenance is uncertain]{style="color:#ccffcc;"}

It is interesting to note that while only two entries are provided for farmers, a highly diversified animal husbandry is presented. The word for the farmer seems to be of non-Aryan origin, being one of the early loan-words of the ["ka/ki/ku"](https://manasataramgini.wordpress.com/2006/07/16/the-substrate-in-old-indo-aryan/) variety into Sanskrit. It could very well be a native Harappan word. Of these the horse, cow, sheep, goat and dog breeders are referred to by vintage Indo-European names that are shared across the Indo-European groups. The elephant- breeder, while having a name of purely Indo-European origin, was a purely Indic profession. Images or representations on seals of all these animals are known from the Harappan site, although representations of the horse are very rare. They are not seen on seals and to date no depiction of the horse-borne chariot is known from Harappan sites. This has been the strongest argument against any Indo-European presence in Harappa and remains the one important bit of evidence that stands against the other concordances.

Forest and animal products\
दार्वहार: woodcutter; dhaivara: fisherman; शौष्कल: a fish-drier; bainda: a stone-age tribesman; Anda: a fowler; पर्णक: A betel leaf vendor; मृगयु: hunter;

Entertainment and music\
पुंश्चली: A public woman; स्त्रीषक: a female companion; वीणवाद: vINa player; पानिघ्न: जाल्र player; तूणवध्म: flute player; शङ्खध्म: a conch blower; सूत: story teller; शैलूष: an actor; मागध: a bard.

Although some of the above occupations are clearly those of pastoralists, this diversification of occupations more consonant with settled societies. In general we observe that the Indo-Aryan texts, both the shruti and the early parts of the इतिहास, tend to emphasize only a few specific aspects and professions of this diversified society -- they rarely or nowhere else mention the rest. This is in contrast with the West Asian and north African urban texts. Further, many of the professions have continued unbroken till recent times in bhArata, and interestingly most of the specialized jAti-s corresponding to these are categorized among the अवर्ण-s or mishra-वर्ण-s, outside of the four वर्ण-s. Thus, we posit that the Arya-s gained control of the Harappan civilization and fostered its integration by virtue of their mobility. They also helped organize the labor of the sedentary native Harappans into jati guilds, which were in large part outside the four वर्ण-s, and controlled the flow of goods and specialized labor between the the major urban and rural centers. In a sense a parallel might be seen with the activities of the तान्त्रिक states in later day India. However, the Indo-Aryan elite remained mobile and did not participate directly in most professions of the Harappans -- they just controlled them and consumed their products -- this is reflected in their texts not frequently mentioning the above described professional diversity. Their role as a mobile force in linking up the urban centers made the Old-Indo-Aryan language the dominant link language, while the native Harappan languages remained restricted to the urban centers. Thus, when urbanization collapsed after 3900 y BP, the native languages of the urban centers faded out while Indo-Aryan languages proliferated and expanded as they were less affected by the loss of urban centers. It has often been remarked that the Harappan uniformity does not appear to be comparable to the uniformity imposed by a powerful monarch in a top-down fashion. Indeed, for a urban society there are hardly any depictions of the symbols of monarchy. We see this as also being consistent with a part of the controlling military and administrative elite being of a different type in this civilization -- they were the mobile kShatriya-s and ब्राह्मण-s. In reality several kShatriya clans and alliances could have operated in different zones, all unified by a common culture with divergence inhibited by marriage ties. A third kind of entity occasionally alluded to are the rich paNi or the vittadha who might be some kind of urban elite.

Now certain white indologists and their imitators might chime in that these urban features alluded to above are related to the second Iron age urbanization closer to the time of the nirgrantha and the तथागत which came after the "Vedic night" imposed by the Indo-Aryans. This is unlikely to be a correct view: First, we see that the lists of profession are seen in all the complete yajus saMhitA-s. That means it was likely to have been in the ancestral yajus saMhitA. While there are several temporal layers in the ancestral yajus saMhitA itself, its core can be dated based on the unambiguous astronomical marker of the कृत्तिका period \~4100-4400 y BP. So, there is no strong objection to placing the पुरुषमेध recitation to that period, which overlaps with the core Harappan period. The AV saMhitA-s also mention making shell bead or ornaments that vanish in late Harappan. On the other hand the RV and AV saMhitA-s do not mention glass, whereas the later ब्राह्मण texts of the yajurveda mention glass -- this in a sense parallels the rise of prominence of rice and tila and the सरस्वती ending in the desert. Glass, rice and tila all rise in importance in the late Harappan. Thus, we capture certain transitions in the Vedic tradition that are compatible with the first Bronze age urbanization and not the second Iron Age urbanization by which time the move to the गङ्गा system, which is captured in the late vedic texts and इतिहास-s was complete.

In conclusion, we now see the idea of the invasion of the Indo-Aryans as a key force in the rise of the mature Harappan as being plausible. However, it is confronted with the formidable "horse problem", so we can hardly be certain about it. Unfortunately, central Asian archaeology is guided by the late dates for the Indo-Aryans, thus seeing the Sintashta-Petrovka and Andronovo cultures as the beginning of the movement of Indo-Iranians southwards -- the flawed Kuzmina hypothesis. These dates are simply untenable because the yajurveda contains unambiguous astronomical date markers that predate them and overlap precisely with the Mature Harappan. We opine that the focus should rather be on the Sredny Stog and Yamna cultures, which should closer to the old layers of the RV, for the beginning of the Indo-Aryan movement into the subcontinent. Another important exercise should be the plotting of the frequency of horse remains in the subcontinent over time to see if there are any preservational anomalies.

......................\
References

 1.  Clift PD, et al. (2012) U-Pb zircon dating evidence for a Pleistocene Sarasvati River and\
Capture of the Yamuna River. Geology 40:211--214.

 2.  Giosan L, et al. Fluvial landscapes of the Harappan civilization; [http://www.pnas.org/cgi/doi/10.1073/pnas.1112743109](http://www.pnas.org/cgi/doi/10.1073/pnas.1112743109){rel="nofollow"}


