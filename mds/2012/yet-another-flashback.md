
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Yet another flashback](https://manasataramgini.wordpress.com/2012/09/29/yet-another-flashback/){rel="bookmark"} {#yet-another-flashback .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 29, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/09/29/yet-another-flashback/ "7:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Links to this](https://manasataramgini.wordpress.com/2009/11/07/the-prelude-to-the-philosophical-penetration/)

After we had recovered from the wounds sustained in the battle of द्वादशान्त, we were on a train was chugging along somewhere close to the modern boundary between the महाराट्ट and कर्णाट country. On both sides stretched, as far as our eye could see, the great expanse of the holy land of भारतवर्ष. The landscape was devoid of humans but for those in the train and the flat expanse was dotted by wild lemons glowing golden from the rays emanating from the unrelenting eye of the great kShatriya who watches over then men from the yonder सुवर्गोलोकः. On the stretching field was a tall tree that had been split in half by the vajra, much as an avrata, a dasyu, a despiser of the यज्ञ, had been smashed by the thunderer on behalf of the bringers of idhma. In our hand was a text on the interactions of aromatic pi-electron clouds. Interesting as it was, our eyes were being lulled into sleep by the steady rocking of the train. We lapsed in and out of a reverie wherein we saw histidines, purines and pyrimidines meet with each and with metals and their pi electron clouds merge with the empty shells of the large metal atom or with each other. But we were just unable to focus on the wave functions describing those delocalized electrons. For some reason, our thought also drifted to metals in colored proteins and then to the great Linus Pauling's obsession with ascorbate at the same time -- these apparently unconnected issues were to connect way in the future leading to one of the great discoveries of our life. But our reverie was tinged by a mixture of rasa-s of another kind: the adbhuta from the depths of the chemistry one side and that emerging from the likelihood that we might never see लूतिका again. At this point a stark hypnagogic vision crowded our eyes. We saw a strange biomorphic shape flash across our field of view. It divide into two and then into four in geometric growth. Finally, some of them were attracted towards a bright moving spots and when they touched them died. Only those that did not touch them did not die. But the cycles of replication went on with several dying in each round.After many rounds of birth and death the whole field of view exploded and vanished. Then many more bright flashes of explosions lit the field ending into total blackness. But just before that two phrases intensely flashed in the minds eye: "The ruby in the crown" and the "hare's ear." We smiled to ourselves.
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-NEenbMS1T-k/UGebptvxZmI/AAAAAAAACdQ/zvsObsMAdE8/s400/mathuraman.jpg){width="75%"}
```{=latex}
\end{center}
```

The mauryan मथुरा puruSha with his striyau

Our journey ended as we saw Mn arrived to meet us. After exchanges on the behavior of lanthanides that were to matter more in his life than mine, he introduced us to his charming striyau of oppressive beauty, MC of noble ब्राह्मण birth and KM a शौण्डिका: flanked by them he looked much like the mystery man from Mathura of yore. Sometime later he queried us: "Why do you perform these संध्या rites?" We retorted: "It was you who taught us the basics of the यजुष्-es and now you ask us?" Mn: "Ah! Sad are those who are bound by beliefs of those who came before them. They say you perform these याग-s to attain सुवर्गोलोकः. But you can get there in a rocket, before the shyena chiti ever gets flying. And even if you can do that, do you want to get there after all ! The man who reaches chandraloka certainly cannot live long there and even before he reaches सूर्यलोक he would be verily reduced to plasma. Let me tell you, suvarga is not up there, it is here on this पृथिवी -- the sukha of suvarga is obtained right here in the embrace of the splendid ladies like MC and KM. So stop wasting time on pleasing mitra or वरुण; shape up and pursue such pleasures as I have achieved." We smiled to ourselves: "चूडस्य kuruvinda" and "शशकर्ण".

Realizing, the divergence between us was underway, we moved our own ways. So we decided not to go the long way to the apara-लक्ष्मीनगर. Few days later we were returning from the apara-जीवशाल late in the night. Suddenly, the thunderer massed great heaps of atmospheric water in the welkin even as the marut-s began the chanting their arka-s (as the shruti says: archanty अर्कं अर्किणः) to accompany the flashes of the vajra. We fled for cover towards a secluded looking house. To our great surprise we also saw ST scampering towards the same house to take cover. We knew she was in the general region but were struck by the coincidence in timing. A guy in uniform invited was guarding the house. Most remarkably he invited us into the porch and asked us to take a seat. It was only when we did so did we realize what that house was. The guard asked who we were? Before he did so, ST and me had quickly exchanged a plan in the द्रमिड भाष, which we inferred from the guard's accent that he did not know. We declared our links to the powerful and irascible तैत्तिरीयक and the महाध्यक्ष respectively. The guard took us in immediately to what may be called a lobby and invited us to take seats on plush cushions. Then an assistant arrived and asked if we might want something to eat. ST she would like to take a look directly at the kitchen to make a decision and soon came back after having given him the preferred menu. After a sumptuous dinner we were planning to return to our abodes when the assistant arrived and asked if we might want rooms for the night for free. Surprised at the power of our influence, and as our nobody could have guessed our location we accepted the offer. Shortly, past midnight there was the proverbial calm after the storm. The sky had been washed clean of the pollution from the महानगर and the glories of pitA dyaus shone forth. We then enticed ST to an astronomy lesson on the terrace.

After the usual circuit of the deep sky objects and interesting asterisms, which our occular could afford, I moved to take a look at the jewel in the northern crown. Just in the last year even in the midst of the great yuddha we had witnessed its "mini-minimum" with the reddening, which left us with an image for life. We felt we had show ST the same and struggled to make her aim the occular at the star. What is the so significant about a star such as that one she asked?

We had sometime back read a remarkable paper from 1979 by Carl Sagan and Bishun Khare which defined the tholins. Where did the carbon come for these tholins we wondered. This led us in the direction of these sooty stars. Perhaps, the precursors of the tholins were already being formed in the outer zones of the planetary nebulae ejected by these stars. On reading the famous S & K paper it immediately dawned on us that the origin of life lay in the tholins and thus went back to carbon stars (one of the most dramatic of which we had see was R Leporis, the hare's ear). But unlike them we felt it was not earth that the tholins facilitated the emergence of life but in more distant locales, perhaps on comets, or on plutino-like objects or perhaps even protoplanetary nebulae seeded by the detritus of the carbon stars. We then moved to the longitude of श्रवण and found शणैश्चर mounting the eastern sky. We made ST aim at that to see Titan to get a feel for that great vessel of tholins in the solar system. At that time we remarked that autotrophy was a derived feature of life and these tholins suggest that heterotrophy, or more precisely tholinovory was the most likely primitive state for life. It is interesting that a month after we made this statement Sagan and colleagues published a paper showing utilization of tholins by numerous bacteria. They said: "Tholins may have formed the base of the food chain for an early heterotrophic biosphere before the evolution of autotrophy on the early Earth." It is just that even then we felt that this tholinovorous biosphere was delivered on earth along with the tholins and it was the seeding on earth that offered new niches for autotrophy, chemoautotrophy first and photoautotrophy thereafter leading to the great oxygenation. We also remarked that the lipid envelopes of our cells is a "memory" of the tholin world and there was never anything like an iron-sulfur world.

From the stars we were made and in a star we will end! Hence, to us the smoke that arose from the ritual fires of ours and those of our ancestors represents that soot which shot forth from stars which made us.


