
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The base of Artiodactyla](https://manasataramgini.wordpress.com/2012/07/30/the-base-of-artiodactyla/){rel="bookmark"} {#the-base-of-artiodactyla .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 30, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/07/30/the-base-of-artiodactyla/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Notable aspect of our divergence from our cousins, the chimpanzees, has been our "mutualistic" or "parasitic" association with artiodactyla. Several human populations have repeatedly acquired the ability to domesticate artiodactyls -- a process which began with pigs (suids), but came to include: camelids in Eurasia (at least 2 times independently, Camelus bacterianus and Camelus dromedarius) and the Americas (Lama), to a degree certain dolphins, caprinae, like sheep and goats, giraffids like Sivatherium, cervids (e.g. Rangifer tarandus) and various bovids of the Bos and Bubalus clades. In the making of whole human cultures were a consequence of intimate associations with various artiodactyls. Our own culture and life is one which has not yet lost its links to the bovids. Today, artiodactyls present much varied morphologies keeping with their colonization of various niches -- the camels, pigs, the semi-aquatic hippos, wholly aquatic whales, the distinctive giraffe, deer, goats, cattle and other antelopes. But when exactly did these morphologies come into being and of these morphologies are any close to the primitive artiodactyls is not very clear. Anatomically one of the most distinctive features of the artiodactyls is their "double pulley" astralagus, which they all seem to share and was clearly present in the common ancestor.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2012/07/double_pulley_astralagus.jpg "double_pulley_astralagus"){width="75%"}
```{=latex}
\end{center}
```

\
[*The double pulley astralagus*]{style="color:#99cc00;"}

However, on the whole morphology by itself has proven to be rather unsuccessful in uncovering the evolutionary history of the artiodactyls. Molecular phylogenies have revealed a rather interesting picture of artiodactyl relationships. They unequivocally support the following clades: 1) camelids; 2) suiformes uniting the Old world pigs and the New world peccaries; 3) Whippomorpha uniting the hippos and whales; 4) ruminantia uniting the ruminant clades of the tragulidae (mouse deer), antilocapridae (gazelles, oryxes, muskoxen, goats, sheep), giraffidae (giraffes and okapi), cervidae (deer), moschidae (musk deer) and bovidae (chausingha, nilgai, kudus, buffaloes and cattle). The molecular studies also strongly support the monophyly of whippomorpha and ruminantia (the cetruminantia clade). The camels and pigs consistently emerge as the basal-most clades of the extant artiodactyls. Most studies place the camels as the basal-most lineage, though it has been proposed that a basal pig-camel clade cannot be statistically ruled out. The placing of the fossil artiodactyls in this framework has proved to be even harder and raises some interesting issues. The most extensive studies, to our knowledge, in this direction are those of J. Theodor, O'Leary and coworkers, and we shall use those as a starting point for the ensuing discussion.
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-EE1B8l5hv64/UBjQY_pMbFI/AAAAAAAACaw/KIsABXD1i2s/s400/artiodactyla.jpg){width="75%"}
```{=latex}
\end{center}
```



*[The diversity of artiodactyls]{style="color:#99cc00;"}*

Continued ...


