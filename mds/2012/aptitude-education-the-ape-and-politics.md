
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Aptitude, education, the ape and politics](https://manasataramgini.wordpress.com/2012/02/17/aptitude-education-the-ape-and-politics/){rel="bookmark"} {#aptitude-education-the-ape-and-politics .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 17, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/02/17/aptitude-education-the-ape-and-politics/ "8:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The yogin experiences, but at the height of his practice there is neither object nor subject in his experience. The kavi experiences, and in the pinnacle of his practice his world is unified under increasingly robust ontologies. When he attains mastery he becomes an आचार्य who expounds शास्त्र-s. Indeed the great indra bestows on few the profound शास्त्र-s as that is indra himself. Hence, the kavi states:

[यश् छन्दसाम् ऋषभो विश्वरूपः ।]{style="color:#99cc00;"}\
[छन्दोभ्यो ऽध्यमृतात् संबभूव ॥]{style="color:#99cc00;"}\
[स मेन्द्रो मेधया स्पृणोतु ।]{style="color:#99cc00;"}\
[अमृतस्य देव धारणो भूयासम् ॥]{style="color:#99cc00;"}

The old thinkers of our tradition had opined that विद्या lies at the junction between the आचार्य and the अन्तेवासिन्. But the अध्यायिन् has to be suitable else the विद्या is not transmitted and even its creation might be aborted. An अध्यायिन् who is an embodiment of tamas and lacks aptitude for the ग्रहण of the शास्त्र should be cast aside like a stone. Such can indeed be ruinous for the आचार्य and the very transmission of the शास्त्र-s. Unlike the opinion of the म्लेच्छ-s, our tradition holds that the अधिकार for शास्त्राध्याय is not for all and should not be disbursed to all and sundry without appropriate testing of the अध्यायिन्'s aptitude and attitude. The more involved a शास्त्र, fewer can approach it. Hence, at rarefied heights there are only a few विद्वान्-s who engage in discourse of the शास्त्र. At this point, these, in a sense, become like yogin-s, because the lay society cannot scale the कैलास-like peaks atop which they are stationed. Thus, like a yogin meditating in the solitude of a cave in कैलास these शास्त्राधिकारिन्-s dwell in the pinnacles of knowledge. The greatest among them are the sarva-शास्त्र-vid, who span the शास्त्र-s like an oceanic व्यास or a चाणक्य or भीष्म पितामह. In the society of Arya-s such विद्वान्-s are allowed to remain atop their pinnacles. They are important to the plebeian because their insights can guide and inform mundane matters which are central for the former. However, under the influence of the म्लेच्छादि the society can turn inimical to the विद्वान्-s: It might choose to place obstacles in their practice, or it might set up fake individuals who sound like विद्वान्-s but peddle hollow and faith-dependent doctrines. Even more dangerously, because the plebeians in such a perverted society cannot scale the heights attained by the विद्वान्-s, it might start believing that the little mud mounds, which its plebeians scale are truly pinnacles of शास्त्र-s. Finally, there are those who are not kavi-s, but dharma-virodhaka-s, who because of the systems of the म्लेच्छादि might have acquired शास्त्र-ज्ङान and might place themselves as आचार्य-s. Such might infuse perverted teachings along side the शास्त्र-s and penetrate the plebeian masses with confusion.

.ओ^ऊ.ओ^ऊ.ओ^ऊ.ओ^ऊ.ओ^ऊ.

The old Arya society had both the yogin and kavi. The yogin attaining his state of subject-object-less consciousness occasionally returns to society and expounds on the profound bliss of that condition. The plebeian without having the proclivities and aptitude of the yogin wants to experience the same. Not having the means he turns to a dangerous proxy known as bhakti and sets off on the unguided path. On the way he seriously conflates the mantra-शास्त्र of the yogin with the songs of bhakti. He is unable to distinguish the चर्यागीति-s of a yogin from a bhakti outpouring. His indulgence in such takes him farther away from the realism that emerges at the interface of the kavi and his students, which produces hard works like the dharmaशास्त्र or the अर्थशास्त्र. Thus, we are left with a mass driven by false yearning for the unbroken consciousness of the inner world but a lack of understanding of the world. This might be exacerbated by the delusions of secularism and modernism wafted in by the म्लेच्छादि. The directives for the path of a kavi as laid out by क्षेमेन्द्र are ignored and there is neglect of the study of काणादं and पाणिनीयं from which understanding of the world stems.

.ओ^ऊ.ओ^ऊ.ओ^ऊ.ओ^ऊ.ओ^ऊ.

In the world confused by the म्लेच्छादि, the puruSha-dharma as laid out by the old dharma-शास्त्र-s is seen as a horrible perversion. In contrast, the ideals of secularism, modernism, egalitarianism and blindness to sex, inter-individual and inter-ethnicity differences are seen as superb guiding principles. The great कालिदास had said:\
[पुराणम् इत्य् एव न साधु सर्वं । न छापि काव्यं नवम् इत्य् अविद्यं ॥]{style="color:#99cc00;"}\
Yet, before adopting the navam शास्त्रं and discarding the पुराणं we must apply the tests to check out their relative robustness. For this just as the yogin strives for to excavate the basis of the experiential I the kavi needs to unearth the foundations of the objective human existence. This can only be done when he seeks to answer: Why is this human ape so constructed?

Continued ...


