
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Account of a mantra practice and discussion thereon](https://manasataramgini.wordpress.com/2012/12/27/account-of-a-mantra-practice-and-discussion-thereon/){rel="bookmark"} {#account-of-a-mantra-practice-and-discussion-thereon .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 27, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/12/27/account-of-a-mantra-practice-and-discussion-thereon/ "7:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The below account is mainly for mantra-theorists and practitioners in the circle but certain points might interest others who are in the know. Certain things will left unstated, which can be understood "between the lines" by such साधक-s. While the practice under discussion is a nAstika one its deeper links will not be lost to Astika मन्त्रवादिन्-s.

The worship of आकाशगर्भ is specified in the [चर्या layer](https://manasataramgini.wordpress.com/2009/02/16/nastika-notes-2/) of the ताथागत tantra-s. Its practice belongs to the texts of the महावैरोचनाभिसंबोधि तन्त्रं family. [This class of texts was transmitted to the chIna-s by the तान्त्रिक-s (vajrabodhi of काङ्चीपुरं and amoghavajra) who journeyed to the Tang court to perform rituals for the emperors](https://manasataramgini.wordpress.com/2012/01/15/a-note-on-the-tantric-state-among-the-china-s-and-recovery-of-a-lost-vainayaka-ritual/). From there it was transmitted to Japan. In 717 CE, the illustrious kShatriya मन्त्रवादिन् shubhakara-सिंह revealed to the chIna-s a new mantra-vidhi for आकाशगर्भ that was supposed to confer the siddhi of extraordinary hearing, memory and vision on to the साधक. In 718 CE a Japanese mantra-साधक, Doji, obtained दीक्ष in this vidhi directly from shubhakara-सिंह (whose portrait is preserved in Japan) and transmitted it to the island. In around 796 CE the Japanese genius कूकै learned the vidhi from a मन्त्रवादिन् and became an ubasoku (Skt: उपासक) of this mantra practice. He assiduously practiced this in isolation on the Pacific coast by scaling the cliff of Mount तैर्यू in awa and the cape of muroto in tosa. As result he is supposed to have attained the remarkable vision of आकाशगर्भ that eventually led him to found the shingon tradition in Japan and eventually compose an enormous तान्त्रिक work titled "himitisu mandara जूजूशिन्रोन्" (Ten abiding stages of the mind according to the secret मण्डल-s). Those familiar with Japanese mantra tradition inform us that this a very well-developed paddhati for mantra practice. Not surprisingly, this vidhi of आकाशगर्भ, known to the Japanese as the "kokUzO bosatu nOman shogan saishOshin dhAraNI gumonji hO", which is a close translation of the Chinese translation produced by shubharakara-सिंह in 717, has an important place in their तान्त्रिक practice.

It is an arduous practice like several other related nAstika and Astika mantra practices aimed at obtaining siddhi-s and cannot be easily performed by aspirants. An account of its performance in 1955 CE is given by the great Japanese मन्त्रवादिन्, professor taikO yamasaki. Before going into his account we shall consider the mantra of आकाशगर्भ used in this practice:
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-QGC_1oNZK9g/UNvyT07SyaI/AAAAAAAAChQ/u68-EQ6xBKY/s400/आकाशगर्भ.jpg){width="75%"}
```{=latex}
\end{center}
```



The Japanese record it as:\
[नॊबॊ aकयसहकयरबय ॊम aरय कयमरि बॊरि सॊवक ||]{style="color:#ccffff;"}

Which is a distortion of the original संस्कृत form:\
[नमो आकाशगर्भाय ॐ आर्य कामारि मौली स्वाहा ॥]{style="color:#99cc00;"}

This mantra is from the paddhati of shubhakara-सिंह \[Footnote 1] and is not found in the complex yantra recorded in the महावैरोचन sUtra as preserved by chIna-s (lost in bhArata). There it is:\
[[नमः समन्तबुद्धानां] आकाश-समतानुगत विcइत्राम्बरधर स्वाहा ॥]{style="color:#99cc00;"}

An interesting point to note is that the Japanese have changed some of the original instructions to shubhakara-सिंह's vidhi in order suit their practice. This illustrates how rituals change as they are adapted by new practitioners in different settings. In shubhakara-सिंह's instruction the साधक used a sealed vial of milk symbolizing the "अमृत" of mantra-siddhi. After performance of the अकाशगर्भ-vidhi the milk vial was opened and the way in which the milk had fermented was used to indicate the degree of success of the साधन. In Japan as milk was not commonly known due the lactose-intolerance of the Easterners this process was omitted. The original vidhi also specified the use of a स्फटिक माला, but the Japanese found that to heavy to use and replaced it with a माला made from the wood of the beautiful Torreya nucifera pine. The original Indian version of the rite was performed at a more relaxed pace with 10800 repetitions of the mantra every day for 100 days, with daily japa sessions of about 5 hours, reaching a count of 1080000 at the end. The Japanese practice is half that with 21600 repetitions per day in two sessions totaling to 10 hours. This practice is typically initiated by the साधक at the brahma मुहूर्त (\~3.00 AM) while he has withdrawn into isolation.The practitioner also limits his salt intake before the performance and during the performance does not eat after noon. Thus, this kind of साधन cannot be undertaken by any one except those in best physical condition. The ritual hall itself is open so that the साधक can glimpse the stars. On the eastern side there is a wall with an opening in the middle to see the the stars and the eastern horizon (this is the reason we hear of कूकै performing it high in the mountains). On the wall a picture of आकाशगर्भ is hung which is covered by a white cloth except at the time of the साधन. Immediately below the painting, a मण्डल of आकाशगर्भ, along with the rest of the pantheon of the महावैरोचनाभिसंबोधि texts, carved in wood, specified in the paddhati of shubhakara सिंह is kept on an wooden four-legged stool. An oil lamp is placed in front of it which is keep burning through the rite. In Japan such meditation halls exist in a few places, including at a temple at the original place where कूकै had performed his साधन.

The core of the साधन involves the meditation of आकाशगर्भ in the disc of the planet Venus even as it seen above the eastern horizon. The right is done such that the last day corresponds to a solar or lunar eclipse with the number of days of साधन counted backwards. The practice itself begins with a पूज of आकाशगर्भ in Venus just before it rises in the East. Then he draws two pails of water for ritual use. With water from one of the buckets he performs Achamana with the purificatory visualization. Then he prepares the offerings and enters the ritual arena wearing a white mask over his nose and mouth. In the hall he bows to the image of आकाशगर्भ and lifts the white screen with a special stick while seated in the स्वस्तिकासन. Thereafter, he performs पञ्चाङ्ग न्यास while visualize being absorbed by आकाशगर्भ followed by the other deities of the मण्डल. Then he utters a mantra on the water and sprinkles it over the मण्डल and the floor. Then he displays धूप and invokes a kavacha of the मण्डल deities. Thereafter, he meditates on his unity with आकाशगर्भ as depicted in the image and invokes the deity into the मण्डल. Then he uses the ritual water specified for the deity to wash his feet, rings the bell and makes the five-fold offerings. He worships the तथागत-s thereafter and performs a सर्वाङ्ग न्यास and digbandha. This done he gets into the core ritual visualizing आकाशगर्भ in the rising Venus, while doing japa with his अक्षमाल. He conceives a white disc on the chest of the deity in which the arNa-s of the mantra appear. He meditates on the mantra thus flowing from the deity emitting a golden hue and entering himself through the crown of his head, exiting via his mouth and reentering the deity via his feet. This cycle is kept throughout the ritual. However, in the last round the he performs the धर्मधातु pravesha meditation in which he conceives the white disk with the मन्त्रार्ण-s expanding to occupy the entire universe and then contracting back to the original size. After this he concludes the ritual with the with the initial मण्डल vidhi and the japa of the mantra of महावैरोचन and the other primary deities of the मण्डल. Then after stuti-s and stotra-s he performs the digvimochana and उपस्थान of the deity.

taikO yamasaki's first person account of the ritual gives some important points: He mentions that early in his साधन he suffered from physical pain in the legs and back that eventually gave way to a tranquil clarity. He also experienced non-dual consciousness, and described this experience in no different terms than the various साधक-s who have been there before. He also mentions intense hallucinatory experiences that he states could have shaken anyone with a weaker physique or mental constitution. He mentions a vivid vision in which a मन्त्रवादिन् appeared and offered him a secret mudra, but he did not get distracted as that could have completely ruined his साधन. Interestingly, he notes that शुभकरसिंह had a similar experience while performing this साधन in which he was tempted with a mantra for invisibility. This is what is implied in certain तान्त्रिक texts which talk of siddhi-s which can distract as also in the classical yoga tradition. At the end yamasaki attained siddhi from his साधन as mentioned in the texts of his tradition.

Footnote 1: This mantra while for bauddha देवता has elements of a shaiva mantra -- कामारी and maulin being epithets of shiva. A bauddha informed me that the correct bauddha संस्कृत form was actually:\
[namo आकाशगर्भाय OM Arya kamala-मौली स्वाहा ||]{style="color:#99cc00;"}\
or\
[नमो आकाशगर्भाय ॐ आर्य कमली मौली स्वाहा ॥]{style="color:#99cc00;"}

Where he is perceived as having a lotus garden or lotus diadem. This is consistent with taikO yamasaki's interpretation of the mantra. It is possible that the conversion of Astika-s to the nAstika fold in bhArata led to reinterpretation of it using more familiar shaiva epithets or that originally shaiva epithets were reinterpreted in a nAstika form. A mantra-theorist might ponder upon how a grossly mispronounced mantra might still confer siddhi upon a प्राच्य साधक.


