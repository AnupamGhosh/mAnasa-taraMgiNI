
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A laud of indra](https://manasataramgini.wordpress.com/2012/05/24/a-laud-of-indra/){rel="bookmark"} {#a-laud-of-indra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 24, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/05/24/a-laud-of-indra/ "7:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For the Arya of yore there was nothing more inspiring than indra in his dasyu-smashing manifestation. Indeed, only one who is touched deeply, inspired to heavenly heights and invigorated in ardor upon hearing these ऋक्स् can lay any claim to understanding the spirit of the warlike Arya-s. Fleeting glimpses of this spirit of indra can be seen in the other branches of the Indo-European tree in the glorification of Thor among the Germanic peoples or the poetic flourishes among the yavana-s on the might of Zeus. But the ऋग्वेद is unrivaled in its pure exhibition of the lauds of the great indra.

[त्वम् मायाभिर् अप मायिनो ऽधमः स्वधाभिर् ये अधि शुप्ताव् अजुह्वत ।]{style="color:#99cc00;"}\
[त्वम् पिप्रोर् नृमणः प्रारुजः पुरः प्र ऋजिश्वानं दस्यु-हत्येष्व् आविथ ॥]{style="color:#99cc00;"} (RV 1.51.5 )

You with your mAyA powers blew away those with mAyA, with self-manifest powers those who made offerings into their own mouth. You, full of manliness, have demolished pipru's forts, and impelled ऋजिश्वान् in dasyu-killing \[battle].

[त्वं कुत्सं शुष्णहत्येष्व् आविथारन्धयो ऽतिथिग्वाय शम्बरम् ।]{style="color:#99cc00;"}\
[महान्तं चिद् अर्बुदं नि क्रमीः पदा सनाद् एव दस्युहत्याय जज्ञिषे ॥]{style="color:#99cc00;"} (RV1.51.6)

You boosted kutsa during the killing of शुश्ण, and annihilated shambhara for atithigva; you trampled the mighty arbuda under foot, and verily arose in the ancient past for the killing of dasyu-s

[त्वे विश्वा तविषी सध्र्यग् घिता तव राधः सोमपीथाय हर्षते ।]{style="color:#99cc00;"}\
[तव वज्रश् चिकिते बाह्वोर् हितो वृश्चा शत्रोर् अव विश्वानि वृष्ण्या ॥]{style="color:#99cc00;"} (RV 1.51.7)

In you all powers are closely gathered up; your generosity is pleased by drinking soma; The vajra that is held in your arms is known : shear off all manliness of our enemy !

[वि जानीह्य् आर्यान् ये च दस्यवो बर्हिष्मते रन्धया शासद् अव्रतान् ।]{style="color:#99cc00;"}\
[शाकी भव यजमानस्य चोदिता विश्वेत् ता ते सधमादेषु चाकन ॥]{style="color:#99cc00;"} (RV 1.51.08)

You discriminate the Arya-s and the dasyu-s, punishing non-performers of rites, subjugating them to the ritualist (literally one who spreads the बर्हिष् for the सोमयाग); be the ritualist's powerful impeller; all these acts of yours bring happiness during the soma ritual.

[अनुव्रताय रन्धयन्न् अपव्रतान् आभूभिर् इन्द्रः श्नथयन्न् अनाभुवः ।]{style="color:#99cc00;"}\
[वृद्धस्य चिद् वर्धतो द्याम् इनक्षत स्तवानो वम्रो वि जघान संदिहः ॥]{style="color:#99cc00;"} (RV 1.51.9)

indra subjugates the non-ritualist to the ritualist, enabling the offerers \[of sacraments] to strike down those do not do so. When vamra when praised \[you], he destroyed the fortifications of the builder who was augmenting them to reach the skies.

Thus, savya अङ्गिरस praises indra to bear him aid in the battle against the dasyu-s by performing a soma sacrifice.

He concludes this praise with a mantra seeking the protection of indra thus:\
[इदं नमो वृषभाय स्वराजे सत्यशुष्माय तवसे ऽवाचि ।]{style="color:#99cc00;"}\
[अस्मिन्न् इन्द्र वृजने सर्व-वीराः स्मत् सूरिभिस् तव शर्मन् स्याम ॥]{style="color:#99cc00;"} (RV 1.51.15)

Obeisance to him the self-ruling bull, this mantra is uttered to the truly valiant and mighty one; may we with all the heroes and ritualists together in this ritual enclosure, O Indra, be in your protection.

Some notes:

  -  The second quarter of RV 1.51.5 is difficult to translate. The main problem being the meaning of the word shupti, which occurs as a singular locative noun. It is a hapax legomenon in the ऋग्वेद. Here we follow the suggestion of the learned shAstrI from the वङ्ग country मन्मथनाथ dutta for the translation, based on सायण. The great सायण states that shupti means mukha (face). He then points to two vaidika traditions to defend the interpretation of shupti as mukha: In the shukla yajurvedic tradition of the वाजसनेयिन्-s it is stated that during the conflict between the deva-s and asura-s, the latter said that they would not offer sacrifice to anyone else. In the कौशीतकि tradition, the asuras are described as scoffing agni and making the offerings to themselves. Thus, the "adhi शुप्ताव् ajuhvata" is taken as a reference to this incident, implying that the asura-s made the oblations in their own mouth. The word shupti being a body part is consistent with the following constructs for body parts in Indo-Iranian, all of which have the '-ti' suffix (probably inherited from proto-Indo-European): vistati= palm; पृष्टि=rib; मुष्टि= fist; shruti=ear (also hearing); दृष्टि= eye (also sight); निष्टि= collarbone. However, inference based on homology suggests that it means shoulder rather than mouth: In middle low German schuft= shoulder blade of ox or horse; Dutch schoft; Avestan supt; Middle Persian suft; Albanian sup all meaning shoulder. The interpretation of सायन could still be right, even if shupti actually meant shoulder in the RV, with making offerings on shoulder being an obscure way of stating that they sacrificed to themselves. But translating this quarter with shupti=shoulder leaves room for alternative interpretations. Could it be that it refers to a specifically deprecated ritual associated with the enemies of the Arya-s of making oblations on a shoulder blade? This is a possibility to be explored further.

  -  The term सूरि in RV 1.51.15 specifically means the sponsor of the soma yaga. From the context it is likely to be a kShatriya यजमान, whose ऋत्विक्-s were the अङ्गिरस ritualists like savya. Who could have been his main patron? An interesting hint is provided by his allusion to the young warrior sushravas तूर्वायण in addition to the usual Arya heroes like kutsa and दिवोदास atithigva. In savya-s सूक्त-s, sushravas is supposed to have even exceeded the heroes like kutsa, दिवोदास and the early pUru king Ayu and subjugated 20 kings of men (jana-राज्ञो dvir dasha). But this illustrious sushravas is not mentioned by most other ऋग्वेदिc ऋषि-s who do praise kutsa and दिवोदास commonly. Interestingly, we find the Iranian tradition mentioning a warrior hero kavI husrava who was aided by the god hauma (=soma). Could it have been that he was the cognate of the ऋग्वेदिc sushravas तूर्वायण and savya sought his patronage after moving away from the core territory of the rulers like kutsa or Ayu?

  -  संदिः is translated as fortifications in this context. Its use to describe a wall appears to be related to its original root which means to smear or to pile. This captures the actions of piling stones and smearing with mud to build the early fortifications that were typical of the Indo-Europeans. Indeed, fort-based warfare remained an important aspect of Indo-European military activity wherever they settled after radiating from their homeland in the steppes. Even closer to the ancestral homeland the Indo-Iranian remnants of fortifications are seen in sites like Arkaim. Not surprisingly there are several references to fort warfare in the ऋग्वेद. Certain terms like पुरंधर and poliorcetes point to parallels between संस्कृत and Greek in this regard.


