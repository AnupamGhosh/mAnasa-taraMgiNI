
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [संमेलन song of the vIra](https://manasataramgini.wordpress.com/2012/03/27/sammelana-song-of-the-vira/){rel="bookmark"} {#समलन-song-of-the-vira .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 27, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/03/27/sammelana-song-of-the-vira/ "5:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It was the संमेलन of the holders of the कपाल\
in the great श्मशान of kollagiri, in quest of महानय.\
We were reclined in the haze of the pyres;\
the embrace of our दूती fanning the inner fires;\
the fire of the कुलयाग leaping to accept the महाहुति.\
The songs of the मेलापक and the signs of the योगिनी;\
aroused the visions of the ten: first the panther;\
lion, jackal, monkey and bear, one after the other;\
woman, eagle, crocodile, elephant, and horse thereafter.\
In the श्मशान we call कंकाली, O रणचण्डी;\
O mistress of the eighteen yantra-s, O त्रिशूलिनी.\
We partake of this great गणचक्र celebration,\
conjoined with the दूती attaining heightened cognition.\
Pride incarnate of the great bhairava, buster of the triple-planets\
The wielder of the great scimitar, in the crematorium manifests.\
Beating our resounding Damaru we experience her great passage;\
through the neural kulapatha, splitting triple-points of bondage.\
In the great nocturnal conjunction with the कुलाङ्गना\
we visualize in her pleasure-giving yoni, the r-ब्लूं bIja.\
Verily he who succeeds in joining क्फ्रें with र्ब्लूं;\
meditating on her slaying रक्तबीज dons the armor of huM.\
Thus, he may step for battle, fearless, to fight भ्रातृव्य-s,\
intending to offer them in the great संग्राम as nara-balI-s.


