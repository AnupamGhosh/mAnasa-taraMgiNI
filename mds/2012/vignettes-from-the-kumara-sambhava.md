
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Vignettes from the कुमार संभव](https://manasataramgini.wordpress.com/2012/12/24/vignettes-from-the-kumara-sambhava/){rel="bookmark"} {#vignettes-from-the-कमर-सभव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 24, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/12/24/vignettes-from-the-kumara-sambhava/ "7:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

*The मातृ-s and गण-s of rudra:*\
The image of rudra being accompanied by female deities is an ancient one which goes back to the shruti. In the शतरुद्रीय of the yajurveda the following mantra-s are seen:\
[नम आव्यधिनीभ्यो विविध्यन्तीभ्यश् च वो नमः । नम उगणाभ्यस् तृग्ंहतीभ्यश् च वो नमः ।]{style="color:#0000ff;"}

Similarly in the atharvaveda the following mantra-s are seen:\
[नमस् ते घोषिणीभ्यो । नमस् ते केशिनीभ्यः ।नमस् संभुञ्जतीभ्यो । नमो नमस्कृताभ्यः । नमस् ते जायमानायै । जाताया उत ते नमः ।]{style="color:#0000ff;"} AV-P 16.106.11

These mantra-s describe rudra's agents, female deities who assume various forms, who are organized as female hordes, who strike with tridents (तृंहती) and devour their targets (संभुञ्जती). In the KS, कालीदास furnishes a great poetic description of the march of the मातॄ-s and काली in rudra's retinue. Though, here rudra is portrayed as being as yet unmarried, he is never separated from his kula of shakti-s, who accompany his marriage procession along with his गण-s. This association is the root of the योगिनी tradition, which in turn lies at the root of the तान्त्रिक kaula tradition. The intermediates between the उगणा-s and विविध्यन्ती-s of the shruti and the मातृका-s of the later tradition are seen in the form of the मातृका-s associated with कुमार in the महाभारत and early कौमार texts. The ritual system of मन्थान rudra preserves early elements of the योगिनी-rudra association similar to what is alluded to in the shruti. Now on to कालीदास's verses:

[तं मातरो देवम् अनुव्रजन्त्यः स्व-वाहन-क्षोभ-चला-वतंसाः ।]{style="color:#0000ff;"}\
[मुखैः प्रभा-मण्डल-रेणु-गौरैः पद्माकरं चक्रुर् इवान्तरीक्षम् ॥]{style="color:#0000ff;"} 7.38 ||

The मातृ-s who followed the god, with their earrings swaying to movements of their respective mounts, seemed to make the welkin like a field of lotuses, with their faces hallowed by the radiant orbs of fair pollen.

[तासां च पश्चात् कनक-प्रभाणां काली कपालाभरणा चकासे ।]{style="color:#0000ff;"}\
[बलाकिनी नीलपयोदराजी दूरं पुरः क्षिप्त शतह्रदेव ।|]{style="color:#99cc00;"} 7.39 ||

Behind those [मातृ-s] shining like gold, was काली dressed in gleaming white \[implied by the metaphor of the egrets] skulls, like a flock of egrets against dark blue clouds \[also a metaphor for the breasts of काली] that cast hundred flashes of lighting far ahead of them.

[ततो गणैः शूलभृतः पुरोगैर् उदीरितो मङ्गल-तूर्य-घोषः ।]{style="color:#0000ff;"}\
[विमान-शृङ्गाण्य् अवगाहमानः शशंस सेवावसरं सुरेभ्यः ॥]{style="color:#0000ff;"} 7.40 ||

Those गण-s \[of rudra], holding tridents, flew in the sky in front \[of him], with the auspicious sounds of pipes resonating with the crests of their airplanes declared that the time for the service \[of rudra] by the deva-s had come.

*The kavI as a naturalist:*\
The great Kashmirian savant क्षेमेन्द्र had pointed out that a [kavI should be a naturalist among other things](https://manasataramgini.wordpress.com/2008/02/13/thinking-of-kshemendra/). Indeed, this tradition is well-borne at least among the kavI-s before the assault on Hindu traditions by the मरून्मत्त-s. On these pages we have illustrated this in the form of the sketches of [भवभूति](https://manasataramgini.wordpress.com/2011/11/02/bhavabhutis-avifauna-and-flora/) and [वाक्पतिराज](https://manasataramgini.wordpress.com/2010/07/30/the-kingfisher/) from the neo-mauryan court of yashovarman and alluded to it in the works of the great [bhoja-deva](https://manasataramgini.wordpress.com/2011/04/21/the-mechanical-the-magical-and-the-fantastic-2/). In कालिदास we see some relative early examples of the same tradition:

[वनेचराणां वनितासखानां दरी-गृहोत्सङ्ग-निषक्त-भासः ।]{style="color:#0000ff;"}\
[भवन्ति यत्रौषधयो रजन्याम् अतैलपूराः सुरत-प्रदीपाः ॥]{style="color:#0000ff;"}1.10 ||

Junglemen with girlfriends residing in caves with glowing walls can pursue their sexual pleasures without needing to fill oil lamps as the glowing "herbs" light up darkness.

This in our opinion is one of the earliest records of a bioluminescent fungi. In Hindu tradition we encounter the mention of glowing "herbs" as special toxins or remedies held by the ashvin-s:\
[ओषधीर् दीप्यमानाश् च जगृहाते ऽश्विनाव् अपि ।]{style="color:#0000ff;"} Mbh 1.218.032a

This suggests that कालिदास was not the first in Hindu tradition to record these bioluminescent fungi. Interestingly, bioluminescent fungi can be encountered in the wet hilly regions in several parts of India. For example we have seen them in the remarkable forest surrounding the भीमशंकर ज्योतिर्लिङ्ग. However, despite the Hindu knowing of them for ages they remain poorly characterized to date. They probably are from the mycenoid lineage
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-miO4mctw2tM/UNgHtVk7krI/AAAAAAAACgc/xZyZGR4anqE/s800/bioluminiscent_fungi.jpg){width="75%"}
```{=latex}
\end{center}
```



[कपोल-कण्डूः करिभिर् विनेतुं विघट्टितानां सरल-द्रुमाणाम् ।]{style="color:#0000ff;"}\
[यत्र स्रुतक्षीरतया प्रसूतः सानूनि गन्धः सुरभी-करोति ॥]{style="color:#0000ff;"} 1.9 ||

Where elephants trying to rub off the itch \[due to the ichor oozing from their] temporal glands on pine trees give rise to flows of resin and make the mountain ridges sweet-scented.

Here कालिदस makes the observation regarding tree-rubbing by elephants in musth. With a long history of elephant domestication going back to the Indus civilization, the Hindus were well familiar with the behavioral patterns of the elephant. कलिदास's observation regarding tree-rubbing by the Indian elephant in musth may be compared with the description of of the same by modern naturalists studying its African counterpart:

*"This Musth-Temporal gland secretion may be distinguished from Temporin by its congealed appearance and strong odor... Musth males rub their temporal glands against trees...Marking may be so vigorous that the male departs with bark and debris on the side of his face. Dissected temporal glands have been found to have pieces of bark embedded deep inside them. Non-musth elephants also mark, but the behavior is more ritualized among musth males."* -- Poole and Granli in The Amboseli Elephants: A Long-Term Perspective on a Long-Lived Mammal

One may take this verse to imply that the mountain flanks (of the himAlAya which he is describing here) are made fragrant by virtue of both the resin oozing from the conifers abraded by the elephant and also by the musk-like odor of the temporal gland secretion with odoriferous compounds. As with the luminescent fungi, whose biology is unclear, the biology of musth is also mysterious. It clearly corresponds to a huge (up to 60 fold) spike in the testosterone levels in the male elephant with corresponding behavioral and physiological changes. The temporal gland itself is a modified sweat gland, and its secretions are believed to elicit an avoidance response in calves and females. The odoriferous compounds in it include dimethyl disulfide (foul odor), cyclohexanone (has a peculiar sweet odor), 2-Nonanol (a cucumber-like odor), 2-Nonanone (a cheesy odor), and (E)-farnesol (a compound found in many scents). Thus, marking its presence based on odoriferous compounds appears to be an important aspect of musth.
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-_PcLqjP620E/UNn1rjEyP6I/AAAAAAAACg4/yWVHvNuE2_c/s800/elephant_tree_rubbing.jpg){width="75%"}
```{=latex}
\end{center}
```



Continuing with elephants, we may also consider:\
[न्यस्ताक्षरा धातु-रसेन यत्र भूर्ज-त्वचः कुञ्जर-बिन्दु-शोणाः ।\
व्रजन्ति विद्याधर-सुन्दरीणाम् अनङ्ग-लेख-क्रिययोपयोगम् ॥ ]{style="color:#99cc00;"} 1.7 ||

Where \[i.e. the himAlAya-s] the विद्याधर beauties go for birch bark to use in writing their love letters with mineral extracts, which are like the pink spots on the elephant skin.

Here कालिदास compares the writing of the विद्याधार females on birch bark to the depigmented spots appearing on elephants skin with age. Such depigmentation is largely seen only in Indian elephants and develops mainly around the head, trunk and ears. This depigmentation on the frontal surface of the trunk in particular is indeed reminiscent of the lenticular bark of the Himalayan white-barked birch with the writing on it. This birch can grow high in the himAlAya along with the conifers that are also mention in 1.9.

He has more allusions on conifers, for example:\
[भागीरथी-निर्झर शीकराणां वोढा मुहुः कम्पित देवदारुः ।\
यद् वायुर् अन्विष्ट मृगैः किरातैर् आसेव्यते भिन्न-शिखण्डि-बर्हः ॥]{style="color:#0000ff;"} 1.15 ||

Whose \[i.e. the himAlaya's] breeze carrying the mists from cascade of भागीरथी (i.e. गङ्गा), which repeatedly causes the Himalayan cedars to tremble and the spreads apart the crests and plumage of the peacocks is enjoyed by the tribesmen in the quest for animals.

*शृङ्गार:*

The whole of the कुमार-संभव is a शृङ्गार काव्य -- so what more needs to be said one might ask? In the earliest days of काव्य, as seen in its beginnings in the veda, and then in the इतिहास-s, the kavI-s were most eloquent in the वीर्य rasa -- be it in the सूक्त-s of the ऋग्वेद praising the heroic acts of the great indra or in the इतिहास-s praising the many heroes at founding of the Hindu nation. Indeed, this might even be considered the heritage of the warlike Indo-Europeans, from whom the Hindus trace a part of their genetic and most of their cultural ancestry. This rasa was also not lost on the drAviDa-s, as can be seen in their earliest expressions upon contact with the Indo-Aryans -- the पुराननुरु as a whole is a paean to the heroic society of the Tamil regions. But as Hindu civilization matured, the one rasa Hindu poets consistently excelled at was शृङ्गार. The roots of this in the संस्कृत world, while seen an nascent form in the रामायण and the पुराण-s, really came of its own only in the classical काव्य realm. This was mirrored in the earliest Tamil expressions in the form of the अकनानुरु. Hence, we shall give a few sketches, for after all in कालिदास we see not just relatively early examples of this rAsa that was to dominate later काव्य, but some of its most splendid examples.

[यत्रांशुकाक्षेप-विलज्जितानां यदृच्छया किंपुरुषाङ्गनानाम् ।\
दरी-गृह-द्वार-विलम्बि-बिम्बास् तिरस्करिण्यो जलदा भवन्ति ॥ ]{style="color:#99cc00;"}1.14 ||

Where \[i.e. himAlaya], fortuitously, if the kinnara ladies were to turn shy when their clothes are taken off, the hanging masses of clouds become curtains for the doors of their cave dwellings.

This is a classic example of how a kavI works into his description of natural beauty, i.e. the cloud-girt slopes of the himAlaya, the trope of the शृङ्गार rasa.

Of course 8th sarga, describing the dalliance of rudra and उमा that resulted in कुमार's birth, is one of the best pieces of this rasa in संस्कृत literature and has to be enjoyed as a whole for its unmatched beauty, which is not easily rendered in any other language. Here we just reproduce a pentad:

[लोहितार्क-मणिभाजनार्पितं कल्प-वृक्ष मधु बिभ्रती स्वयम् ।\
त्वाम् इयं स्थिति-मतीम् उपस्थिता गन्धमादन-वनाधिदेवता ॥8.75 ||]{style="color:#99cc00;"}

\[rudra tells उमा]: "The prime goddess of the गन्धमादन forest is arriving, she herself bringing for you in a chalice made of sun-red ruby wine made from the kalpa trees."

[आर्द्र-केसर-सुगन्धि ते मुखं मत्त-रक्त-नयनं स्वभावतः ।\
अत्र लब्ध-वसतिर् गुणान्तरं किं विलासिनि मदः करिष्यति ॥8.76 ||]{style="color:#99cc00;"}

"By your own nature your mouth is naturally fragrant as the fresh saffron flower, and your eye lids have a red shade, sensuous one, what further sensual merits can wine give you?"

[मान्य भक्तिर् अथवा सखीजनः सेव्यताम् इदम् अनङ्ग दीपनम् ।\
इत्य् उदारम् अभिधाय शङ्करस् ताम् अपाययत पानम् अम्बिकाम् ॥8.77 ||]{style="color:#99cc00;"}

"However, honor the devotion of your friend by accepting this \[beverage] which will light up love", saying so, with his generosity शंकर gave अंबिका the liquor.

[पार्वती तद् उपयोग सम्भवां विक्रियाम् अपि सतां मनोहराम् ।\
अप्रतर्क्य-विधि-योग-निर्मिताम् आम्रतेव सहकारतां ययौ ॥8.78 ||]{style="color:#99cc00;"}

When पार्वती had drunk that wine, she was transformed, yet she captivated the mind as though by some inconceivable procedure an ordinary mango were to become a सहकार mango (the most flavored of the Indian mangoes).

[तत्-क्षणं विपरिवर्तित-ह्रियोर् नेष्यतोः शयनम् इद्ध-रागयोः ।\
सा बभूव वशवर्तिनी द्वयोः शूलिनः सुवदना मदस्य च ॥8.79 ||]{style="color:#99cc00;"}

At that moment, her pretty face was in the control of both trident-bearer and the liquor, both took away her coyness, and both led her to the bed and both became kindled passion.

*brahmA:*\
The late Vedic period saw the spectacular rise of the deity prajApati. Diversifying as a god of many functions, including the deity of the year, the progenitor of the deva-s and asura-s, the source of the universe in the form of the हिरण्यगर्भ, the supreme deity and the embodiment of the Vedic ritual, he began encroaching in a big way on the primacy of the old great gods of proto-Indo-European vintage in the Vedic pantheon. It was his role as the embodiment of the Vedic ritual that appears to have greatly fueled his evolution and dominance in the circles of the ritualists who were reorganizing the older rites into the classical shrauta form represented by the later layers of the ब्राह्मण literature. As consequence he appears to have been a major deity in system of the early post-Vedic मीमाम्सक-s. This emerging preeminence of prajApati in the Vedic circles also started coloring the developing mythosphere of the Hindus. In the रामायण, while, indra the foremost deity of the old IE system is still the dominant figure of poetic metaphor and the model for the epic, we find that his position is seriously challenged by prajApati under his preferred post-Vedic apellation, brahmA, who is presented as the preeminent member of the pantheon. This trend is retained in the first concrete layer of the पुराण-s as they survive. It was this preeminent brahmA who came in competition with the expanding sectarian cults of the powerful deities of the old IE system, rudra and विष्णु, as also the other new ascendent deity कुमार. It was this brahmA, as the foremost symbol of the post-Vedic मीमाम्सक-s, whose paramountcy was targeted by the anti-Vedic attacks of the तथागत. Eventually, the dominance of the cults of rudra and विष्णु within the Astika realm, along with the attacks from without, resulted in the decline of brahmA. Nevertheless, as late as abhinavagupta's works we still encounter holdouts of the system of brahmA worship, which appears to have survived among the remaining प्राजापत्य मीमांसक-s. In sarga-2 of the KS we encounter a great stuti of brahmA, suggesting that he had not yet lost his place to ascendent gods in the Astika realm. Indeed, many descriptions of him in this sarga still evocative of the grandeur from his heydays.

His role is primarily presented as different flavors of the संख्यन् universal progenitor:

[नमस् त्रिमूर्तये तुभ्यं प्राक् सृष्टेः केवलात्मने ।\
गुणत्रयविभागाय पश्चाद् भेदम् उपेयुषे ॥]{style="color:#0000ff;"}2.4 ||

\[The deva-s said:] "Obeisance to you for three fold form, who prior to the emission \[of the universe] you existed as the sole consciousness, you who divided yourself into the [three primary facets of matter](manasataramgini.wordpress.com/2008/01/25/the-guna-s-of-samkhya-as-physical-concepts/) and thereafter became the diversity of matter."

[यद् अमोघम् अपाम् अन्तर् उप्तं बीजम् अज त्वया ।\
अतश् चराचरं विश्वं प्रभवस् तस्य गीयसे ॥]{style="color:#0000ff;"}2.5 ||

"Because all mobile and sessile organisms are from the infallible germ you, the unborn one, sowed in water, you are sung as being their origin."

[स्व-काल-परिमाणेन व्यस्त-रात्रिं दिवस्य ते ।\
यौ तु स्वप्नावबोधौ तौ भूतानां प्रलयोदयौ ॥]{style="color:#0000ff;"}2.8 ||

You divide night and day by your measure of time and your sleep and awakening are the end and the begin of beings.

[द्रवः संघात कठिनः स्थूलः सूक्ष्मो लघुर् गुरुः ।\
व्यक्तो व्यक्तेतरश् चासि प्राकाम्यं ते विभूतिषु ॥]{style="color:#0000ff;"}2.11 ||

"You are fluid, and also solid when compacted, you are both bulk matter and the minute particles, light and heavy, manifest and unmanifest, you appear as you wish in all your manifestations."

[त्वाम् आमनन्ति प्रकृतिं पुरुषार्थ-प्रवर्तिनीम् ।\
तद् दर्शिनम् उदासीनं त्वाम् एव पुरुषं विदुः ॥]{style="color:#0000ff;"} 2.13 ||

They opine that you are the the प्रकृति \[matter] that is evolving, for the sake of the puruSha \[consciousness], are also known as the puruSha that is the unaffected witness of the evolution of प्रकृति.


