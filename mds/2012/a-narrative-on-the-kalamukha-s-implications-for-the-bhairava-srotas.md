
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A narrative on the कालामुख-s: implications for the bhairava-srotas](https://manasataramgini.wordpress.com/2012/09/27/a-narrative-on-the-kalamukha-s-implications-for-the-bhairava-srotas/){rel="bookmark"} {#a-narrative-on-the-कलमख-s-implications-for-the-bhairava-srotas .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 27, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/09/27/a-narrative-on-the-kalamukha-s-implications-for-the-bhairava-srotas/ "7:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier briefly considered the कालामुख-s on these pages. While once prominent in both the north and south of the country, their last occurrences are recorded only in the south (though they clearly acknowledge their connections to the northern branches). While the कालामुख institutions received a devastating blow from the army of Islam's rampage through south India under genocidal Ghazis, such as Khalji and Tughlaq, we know from inscriptional evidence that few Acharya-s of the system survived into the early years of the vijayanagara empire. Unfortunately, some time after this period they became extinct and with them vanished their major ritual manual the कालामुख-महातन्त्र. A few fragments of this tradition have nevertheless survived in the Alur town of the कर्णाट country, where the कालामुख-s were once at pinnacle of their glory. These fragments include a text from the vijayanagaran capital that narrates an unusual version of the old tale of the competition between कुमार and विनायक in the context of the origin of the कालामुख tradition. Typically, this tale is used as device to downgrade the more ancient deity कुमार and prop up the emerging deity गणेश within the shaiva hierarchy. But here it is resolved amicably. This, to us, suggests a link to the dominance of the old कौमार tapovana-s of दक्षिणौघ which appear to have been a major force in the Bellary region before the Mohammedan terror intruded upon these regions. The tale goes thus:

"In the long past rudra had narrated to his son कुमार the कालामुख lore. When rudra was holding court on his mountain abode, a देवी known as सेविता, who was the mistress of the forest of nandana trees, appeared and offered a fruit of the कल्पवृक्ष tree to him, after having saluted him and his wife. rudra gave it to अंबिका and in turn कुमार and विनायक wished to have the same. To have some fun, उमा smilingly asked कुमार and गणpati to go round the world and said that she would give it to the one who returned first. कुमार mounted his peacock and went racing around the world. But विनायक was off to a slow start and instead went around उमा and rudra. They were surprised at his action and he explained that they were the origin of the world as the puruSha and प्रकृति and thereby comprise the whole world; by going around them he said he has completed his task. Just them कुमार completed his circuit of the world and arrived there. Pleased with the energy of कुमार and the cunning of गणेश, उमा cut the fruit and gave each of them a half. Two मुहूर्त-s later, महाकाल, a गण of कुमार appeared there. He too had gone around the universe as he was keen to provide service for his lord, कुमार. But since कुमार was flying at a blistering pace and he was going unaided it took him a longer to complete the journey. rudra noticed this and was struck by his loyalty to his leader. So rudra asked कुमार to give him महाकाल and appointed him as one of his भूतगणेश्वर-s beside nandin and वीरभद्र. Then one day कुमार revealed to him the कालामुख-माहातन्त्रं. Thereafter, महाकाल taught it to the deva-s and they imparted it to the muni-s. The students of these muni-s transmitted it to the earthlings as the अघोरसार, and thus कालामुख tradition was promulgated."

In भरद्वाज's narration of it it began with the mantra:\
[नमः शिवाय देवाय हराय परमात्मने रुद्रय कालकालाय कालकण्ठाय शम्भवे ॥]{style="color:#99cc00;"}

Continued ...


