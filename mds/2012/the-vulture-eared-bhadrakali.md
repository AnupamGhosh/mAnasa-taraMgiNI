
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The vulture-eared भद्रकाली](https://manasataramgini.wordpress.com/2012/02/19/the-vulture-eared-bhadrakali/){rel="bookmark"} {#the-vulture-eared-भदरकल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 19, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/02/19/the-vulture-eared-bhadrakali/ "8:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There are several traditions within the काली-kula that associate काली with various animals. There is the polytherocephalous गुह्यकाली whose उपासन is laid out in the गुह्यकाली section of the महाकाल saMhitA. There is secret विद्या of चामुण्डा as वृस्चिकोदरी where the fierce and skeletal goddess is worshiped with a scorpion on her belly. But one of the least known is the विद्या of the vulture-eared भद्रकाली or गृध्रकर्णा that is expounded in the 8th पटल of the फेट्कारिनी tantra.

[श्रीदेव्युवाच:]{style="color:#99cc00;"}\
[अधुना श्रोतुम् इच्छामि गृध्रकर्णा मनुं शिव ।]{style="color:#99cc00;"}\
[श्रीशिवोवाच:]{style="color:#99cc00;"}\
[गृध्रकर्णा मनुं देवि कथयामि शृणु प्रिये ॥ ८।१ ॥]{style="color:#99cc00;"}\
The goddess said: Now I wish to hear about the गृध्रकर्णा incantation. shiva said: Hear O beloved, I will narrate to you the गृध्रकर्णा incantation.

[गृध्रकर्णा पदं चोक्त्वा विरूपिणि पदं ततः ।]{style="color:#99cc00;"}\
[लम्बस्तनि पदं पश्चाद् महोदरि पदं वदेत् ॥ ८।२ ॥]{style="color:#99cc00;"}

Having said the phrase गृध्रकर्णा one says विरूपीणि thereafter. Then lambastani and thereafter one says the phrase mahodari.

[उत्पादय पदं भूयस् तथैवोपरि देवि छ ।]{style="color:#99cc00;"}\
[पदान्य् एतानि वीप्सायै वदेत् तन् मन्त्रम् एव छ ॥ ८।३ ॥]{style="color:#99cc00;"}

After that O goddess is the phrase उत्पादय. This mantra is verily comprised of these phrases.

[वह्नि-जाया पदं छोक्त्वा ततो मन्त्रं जपेद् बुधः ।]{style="color:#99cc00;"}\
[अयुतं मन्त्र-सिद्ध्यर्थं मातृकान्या सतत्परः ॥ ८।४ ॥]{style="color:#99cc00;"}

The intelligent [मन्त्रवादिन्] says the "wife of fire" phrase thereafter while doing japa of the mantra. For the purpose of the siddhi he must repeat the mantra 10,000 times, one after another; literally as a daughter follows her mother. [[ॐ गृध्रकर्णा विरूपिणि लम्बस्तनि महोदरि उत्पादय स्वाहा ॥]{style="color:#99cc00;"}]

[पिप्पलादो मुनि-श्रेष्ठो निवृच् छन्द उदाहृतम् ।]{style="color:#99cc00;"}\
[स्वयम् एव तु पञ्चाङ्ग भद्रकाली तु देवता ॥ ८।५ ॥]{style="color:#99cc00;"}

The famed पिप्प्लाद it the seer of the mantra; निवृत् is said to be its meter; you verily are the five limbs of the mantra deployment and you as भद्रकाली is its deity.

[अतिरौद्री महादंष्ट्रा कृशा दीर्घा कृशोदरी ।]{style="color:#99cc00;"}\
[प्रमत्तनयना शूरा दीर्घघोणा मदान्विता ॥ ८।६ ॥]{style="color:#99cc00;"}\
[स्निग्ध-गम्भीर-निर्घोषा नील-जीमूत-सन्निभा ।]{style="color:#99cc00;"}\
[भ्रुकुट्याग्नीव संदीप्ता महावदन-भीषणा ॥ ८।७ ॥]{style="color:#99cc00;"}\
[दष्टौष्ठा कोप-ताम्रोष्ठी रक्त-दीर्घ-तनूरुहा ।]{style="color:#99cc00;"}\
[त्रिशूलैर्-उग्र-दोर्-दण्डा नर-कीटपलाशिनी ॥ ८।८ ॥]{style="color:#99cc00;"}\
[अतिरक्ताम्बरा देवी रक्त-मांसासवप्रिया ।]{style="color:#99cc00;"}\
[शिरोमाला विचित्राङ्गी पिबन्ती शोणितासवम् ॥ ८।९ ॥]{style="color:#99cc00;"}\
[नृत्यन्ती च हसन्ती च पिशाच-गणसेविता ।]{style="color:#99cc00;"}\
[पिशाच-स्कन्धमारुह्य भ्रमन्ती वसुधातलम् ॥ ८।१० ॥]{style="color:#99cc00;"}\
[शङ्करस्य मुखोत्पन्ना योगिनी योगवल्लभा ।]{style="color:#99cc00;"}\
[इत्थंभूता भद्रकाली मातृभिः परिवारिता ॥ ८।११ ॥]{style="color:#99cc00;"}

Extremely fierce, with great fangs, thin, tall, slim-bellied, with wanton eyes, valiant, with a long nose, flushed with liquor, a smooth, deep voice, of the complexion of a deep blue cloud, with fiery frowning brows, blazing, with a great terrifying mouth, biting her lips, with lips red with wrath, with aroused body smeared with blood, hold a trident in her fierce staff-like arms, eating human flesh, the goddess covered with blood, delighting in blood, flesh and rum, with a garland of heads, with awesome limbs, drinking a libation of blood, served by पिषाच-s who are laughing and dancing, mounted on the shoulders of a पिशाच and wandering on the earth's surface, one who has emerged from shiva's face, the योगिनी and the mistress of yoga, so is भद्रकाली surrounded by hosts of मातृ-s.

[ध्यात्वा सम्यक् समाराध्य मन्त्र-कार्यं समारभेत् ।]{style="color:#99cc00;"}\
[धूमावत्याश् च यत् प्रोक्तमनया च तदाचरेत् ॥ ८।१२ ॥]{style="color:#99cc00;"}

Having thus completely visualized and worshiped \[the देवी] one begins the mantra practice. O dear one performs the rite as it was laid out in the धूमावति section (पटल 7 of the फेट्कारिणी tantra).

Continued...


