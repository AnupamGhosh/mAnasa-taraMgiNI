
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Hindu "butterfly effect" tales](https://manasataramgini.wordpress.com/2012/03/20/hindu-butterfly-effect-tales/){rel="bookmark"} {#hindu-butterfly-effect-tales .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 20, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/03/20/hindu-butterfly-effect-tales/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Lorenz is supposed to have said that a flap of a butterfly might end up in a hurricane weeks later. In his retelling of the पञ्चतन्त्र, वसुभाग has several such childish tales (2.13, 2.14 and 2.15).

2.13: Once a group of day geckos lived in a प्लक्ष tree. They had a great territorial conflict in which a weak one fleeing for its life ran into the trunk of an elephant. The elephant tormented by this ran in to a nearby lake and crushed many animals there. Then it ran out and crushed an Ashrama. Finally, it ran back to the प्लक्ष tree, broke it, and crushed all the lizards living in it. Finally it succeed in blowing the lizard out its trunk. Thus, the weak lizard brought the death of all of them.

2.14: The king kept his pet ram near the doorway to his अन्तःपुर. He used to butt the servants going in and out of the harem. Angered by this one servant wanted to kill him. So one night while she was exiting she struck him her torch and set his fleece on fire. In fright he strained his tether and broke free and tried to extinguish the fire by some means. In course of this he reach the royal stables where some hay caught fire. Several horses suffered burns are result and other died. The veterinarians of the king decided to treat the horses' burns with monkey fat. So there was a great destruction of monkeys in the king's groves. Thus, due the butt of a ram the monkeys got slaugthered.

2.15: In प्रयाग, a rustic man arrived with a pot of honey dangling from his sling. With many horses and chariots on the road the man was distracted and stumbled. His honey pot broke and oozed honey. A fly alighted on it to take a lick. A spider lowered itself on a thread to take the fly. A gecko saw the spider and ran down a wall to take it. A cat saw the lizard and pounced on it with its body arched and tail lashing. Seeing the cat a pet dog of the kShatriya chief attacked the cat. Seeing his cat being killed, its vaishya owner clubbed the dog to death with his rod. Seeing the dog being killed, the kShatriya's dog-man drew his sword and killed the vaishya. Seeing the vaishya killed the citizens fell upon the dog-man and killed him. The kShatriya sent his troops to capture the killers of his dog-man. A great riot broke out and the city was destroyed. Thus a drop of honey enough for a fly brought down an entire city.


