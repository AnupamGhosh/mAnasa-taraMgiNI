
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some inquiries into the श्रीलन्कन् past](https://manasataramgini.wordpress.com/2012/05/30/some-inquiries-into-the-shrilankan-past/){rel="bookmark"} {#some-inquiries-into-the-शरलनकन-past .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 30, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/05/30/some-inquiries-into-the-shrilankan-past/ "6:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Whereas Hinduson the mainland[ in white indological parlance ]{style="display:inline !important;float:none;background-color:#ffffff;color:#2c3338;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:1.2em;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"} are reputed to have no historical sense and consquently no history, their cousins on the island are supposed to have a rich historical account in the form of the pALi महावंश and the चूळवंश. These sources have been the bed rock of the current lankan एकीय and reinterpretted by the modern स्थविरवादिन् संघ with a distinct anti-Tamil and anti-Astika emphasis. A study of the वंश-s themselves suggests that this distinctly anti-Astika streak, while having older antecedents, has been expanded more recently culminating in the द्रमिड-सिंहल महायुद्ध that recently concluded with a massacre of the former. We have been investigating this as part of an attempt to understand the older systems of dharma in the island. This has several historical dimensions that include: 1) The greater Astika influence and presence on the island in the past; 2) The worship of Astika देवता-s within the bauddha framework originally practiced on the island. 3) The role of the द्रमिड-s, chera-s and कलिङ्ग-s at different points in the development of practices in the island. A complete consideration of all this will require a monograph, but that is not our intention here. Instead, we simply collect a few vignettes that illustrate certain aspects of the above points.

One point that caught our attention is centered on the life and time of the श्रीलन्कन् hero पराक्रम-बाहु-I (1153-86 CE) who is described as the great upholder of the स्थविरवाद tradition and it is unifier. The lankans record with much pride his invasions of Myanmar and southern India and on the whole in more recent times he has been presented as the bauddha hero who brought down the Astika Tamils. The चूळवंश records interesting aspects of his life: 1) It mentions that he underwent the upanayana ceremony given the mantra दीक्ष by his uncle कीर्ति-श्री-megha assisted by ब्राह्मण-s well-versed in the veda (CHV 64.13-17). The upanayana by ब्राह्मण-s is not something which a pure follower of the nAstika-mata would normally give in to. What it shows is that at least कीर्ति-श्री-megha was conscious of his kShatriya वर्ण and observed its practices in relation to the shruti, even though he was a bauddha. 2) In this context it should be noted that his predecessor vikrama-बाहु is mentioned as having performed vaidika shrauta rituals aided by his purohita and ऋत्विक्-s well-versed in the veda-s and वेदाङ्ग-s. He too is otherwise described as being a bauddha upholding the traditions of the संघ. Thus, it is clear that the medieval kShatriya-s of lanka even though bauddha-s are likely to have not given up their vaidika rites which are obligatory for a proper member of that वर्ण. 3) Further, पराक्रम-बाहु-I is described as having extensively built several new deva temples and repaired those built earlier by choLa-s (CHV 79.19, 22, 81), in addition to the bauddha relic shrines, ताथागत images and stupas and vihAra-s he was constructing. So the indications are that he did support public worship of Astika deities within श्रीलन्का and the available archaeological evidence appears to support these records of the वंश.

Similarly, पराक्रमबाहु-II, in addition to performing daily vaidika rites with the help of ब्राह्मण-s, is described as having sponsored ब्राह्मण-s who maintained and taught "secular" संस्कृत texts to the lankans. Importantly, after his generals defeated the Indonesian naval invasion of श्रीलन्क (sometime between 1245-1265) he went to Devinuvara, which is southern-most port in the Indian subcontinent, to renovate the temple of विष्णु as an act of thanks-giving for his victory. In this regard, it is of interest to note a ritual chant deployed to विष्णु preserved in lankan text called the sarva-deva-kannalava used by bauddha-s of the island to worship Astika and other local deities. Unfortunately due my difficulties with working through the Lankan script all I have with me now is a translation of the विष्णु chant by a certain सीतारामन्:

Bearing a chakra in the right hand,\
with your body shiny blue,\
mounted on your vehicle garuDa,\
O विष्णु, provide us with good luck.

O lord of the वैकुण्ठ mountain,\
guarding the four great देवालय-s \[of श्रीलन्का]\
accept our acts of पुण्य,\
bring wealth and good luck in our grasp.

श्रीलन्का and the बुद्धशासन\
are under your protection, lord of वैकुण्ठ;\
O divine श्री विष्णु, may you provide\
all that it good in every way.

In the sea, on land, in the sky and beneath or anywhere,\
cries from the high, low, poor and homeless or anyone,\
you have never let down anyone;\
O might deva, give me good protection.

Offering chandana, कर्पूर, lamps and flowers,\
mighty deva, we always seek your help;\
We offer you our acts of पुण्य -- open your eyes\
and remove my troubles.

Of the deva महाविष्णु,\
there is great power and authority.\
If I have committed mistakes, forgive me;\
accept our acts of पुण्य, and save us.

This chant or its variants are widely used by the कपुराल-s or lay bauddha-s in Devinuvara and other parts of the island. They are in hybrid सिंहल-pAlI-संस्कृत language and are distinct from Astika ritual texts, which are in संस्कृत and drawn from various vaidika, पौराणिक and तान्त्रिक texts of the Astika canon. The printed version of the sarva-deva-kannalava had pictures of विष्णु seated on garuDa but his depiction was different from the Astika form in that he lacked the typical U-shaped ऊर्ध्वपुण्ड्र on his forehead. This suggests that a distinct lankan liturgy and iconography was developed for the same Astika deities. Nevertheless, it is clear that the lankans repeatedly received Astika inputs, in large part due to the proclivities of their rulers. Indeed, in support of this we have some evidence based on inscriptions that there were पाञ्चरात्रिक ब्राह्मण-s performing rituals at Devinuvara at the time of परक्रमबाहु-II's reign. Much earlier, in the 800s of CE the श्रीलन्कन् ruler dapulusena is mentioned as having brought in पाञ्चरात्रिक ब्राह्मण-s from Rameshvaram to install the rakta-chandana idol of विष्णु at Devinuvara and consecrate it. Hence, there is evidence for a long पाञ्चरात्रिक presence in the hallowed shrine of Devinuvara. Thus, there appear to have been two parallel but interacting tracks of ritual and iconography -- one of folk worship, such as that represented by the above text, which was followed by the mass of bauddha-s and their कपुराल priests in श्रीलन्का, and second of the ब्राह्मण-s who were sponsored by key the kShatriya clans. During the reign of राजसिंह-I who was a largely Astika ruler among the श्रीलन्कन्-s there was a major battle with the Portuguese terrorists. The king had besieged them in the fort of Colombo in 1587 CE and had reduced them to precarious state. In order relieve the pressure a Portuguese armada attacked and destroyed the Devinuvara temple and carried the idol studded with gems on their ship -- interestingly, the ship carrying the idol was lost at sea. After, this the ब्राह्मण rituals in Devinuvara appear to have had a hiatus and the precise knowledge of the earlier traditions is largely lost. However, राजसिंह-II (\~1635-87 CE) with auxiliary naval support from the marATha naval captain maynak bhaNDArI recaptured Devinuvara and rebuilt the temple. However, there is no evidence for any पाञ्चरात्रिक revival at this point and it appears that the folk lankan form of ritual and liturgy came to be norm.

The evidence for a hybridization of पाञ्चरात्रिक and bauddha elements in the liturgy of the कपुराल-s is clearly indicated by the rite of ताम्बुल offering to विष्णु. The chant which accompanies this ritual goes thus as per the सीतारामन् translation:

After 5000 years of prosperity I will become a buddha,\
like the moon over the Milky Way, लन्का will shine in its greatness;\
The golden screen of the palace of वैकुण्ठ opens;\
May your compassion last a 5000 years, the deva arrives!

Seven chourie bearers holding golden bows and arrows on either side of\
the blue screens and shining white umbrellas in the middle of the वैकुण्ठ palace,\
deva upulavanna \[blue lotus colored] deva नारायण will become the buddha in the future.\
May you O नारायण kindly accept our offerings of flowers and ताम्बुल.

[वसुदेवः परब्रह्मा तन्मूर्तिः पुरुषोत्तम ।]{style="color:#0000ff;"}\
[ avyaktash cha निर्गुणश् cha प्राशान्तात्मा tathaiva cha |]{style="color:#0000ff;"}\
[त्वाम् एव श्री महाविष्णुस् सुनाम्ना पुजयाम्य् अहं ॥]{style="color:#0000ff;"}

[ॐ ह्रं ह्रीं ह्रूं श्री महाविष्णु देवानाम् अधिदेव अस्मिन् शुभ क्षणे भारत वर्षे पूर्व-भागात् आ गमत्; अभिघृण, अभिघृण, निवासय, आधारय, आधारय, रक्ष, रक्ष, जयतु, जयतु ॥]{style="color:#0000ff;"}

The first two shloka-s are in pAlI-सींहल hybrid and represent the nAstika element. The remaining two are essentially संस्कृत and the last one in particular is a तान्त्रिक mantra. These second two betray a पाञ्चरात्रिक origin. Interestingly the mantra explicitly mentions पूर्व-bhArata (eastern India) which might indicate a provenance in the कलिङ्ग country.

We suspect that the end of the kShatriya power in श्रीलन्का resulted in an imbalance between the Astika elements supported by them and the exclusivist nAstika interpretation of the bikkhu-s of the संघ. In 1840s both the Astika-s, spearheaded by ब्राह्मण polemicists like कुमार-कार्त्तिकेय ayyar supported by various drAviDa leaders, and the nAstika revivalists were focusing their attack on the pretamata and the English. 1848 the nAstika-s organized the last great attempt to overthrow the English power under गोन्गलेगोड banDa, who denounced the pretamata and vowed to fight the म्लेच्छ-s with the aid विष्णु. After the English eventual defeated and humiliated him they suppressed any royal worship of the deity. They also incited the संघकार-s to insinuate this as a failure of विष्णु and कुमार to help the lankans in their attempt for freedom. With the loss of the royal patronage, and English encouragement for nAstika separatism, the स्थविरवादिन् elements similar to the old maitreya and धर्मकीर्ति gained an upper hand in their attempt to marginalize the worship of Astika deities.


