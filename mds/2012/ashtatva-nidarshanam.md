
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [अष्टत्व निदर्शनं](https://manasataramgini.wordpress.com/2012/08/11/ashtatva-nidarshanam/){rel="bookmark"} {#अषटतव-नदरशन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 11, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/08/11/ashtatva-nidarshanam/ "6:23 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-BSOW_pjzbx4/UCahhq1uqTI/AAAAAAAACbM/zsm5tALkZs0/s400/fractal_mini.jpg){width="75%"}
```{=latex}
\end{center}
```



