
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Macranthropy and the संबन्ध-s between microcosm and macrocosm](https://manasataramgini.wordpress.com/2012/09/21/macranthropy-and-the-sambandha-s-between-microcosm-and-macrocosm/){rel="bookmark"} {#macranthropy-and-the-सबनध-s-between-microcosm-and-macrocosm .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 21, 2012]{.entry-date}](https://manasataramgini.wordpress.com/2012/09/21/macranthropy-and-the-sambandha-s-between-microcosm-and-macrocosm/ "9:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

May we first invoke indra that he might drive away the obstructions to the study:

[वज्रं यश् चक्रे सुहनाय दस्यवे हिरीमशो हिरीमान् ।]{style="color:#99cc00;"}\
[अरुतहनुर् अद्भुतं न रजः ॥]{style="color:#99cc00;"}

The thunderbolt, which he of golden mane and golden frame bore down for the sound smiting of the dasyu-s; \[he] with a bold jaw is wondrous as the \[entire] heavens.

[यश् छन्दसाम् ऋषभो विश्वरूपश् छन्दोभ्यश् छन्दा।ग्स्य् आविवेश ।]{style="color:#99cc00;"}\
[सचा।ग्ं शिक्यः पुरो वाचोपनिषद् इन्द्रो ज्येष्ठ इन्द्रियाय ऋषिभ्यो]{style="color:#99cc00;"}\
[नमो देवेभ्यस् स्वधा पितृभ्यो भूर्-भुवस्-सुवश् छन्द ॐ ॥]{style="color:#99cc00;"}

That universe-encompassing bull among the veda-chants, who described by the chants and pervades the chants, present before all, the first in causal link, \[which is] the teaching of the उपनिषद्, the great one, indra \[taught the उपनिषद्] for the perception of the ऋषि-s; salutations to the gods, स्वधा offerings to the ancestors, the three primal व्याहृति-s and the metrical chant, OM.

A student of the shruti might ask what is meant by the उपनिषत्? Many teachers are unable to give the meaning for this word even though they might practice an aspect of its teachings, which they consider as being the true vedAnta. If they happen to be adherents of the neo-advaita school they might even ignore the relevance of the meaning of the उपनिषत् and hold the view that they already have a solution to the deepest problem explored by the उपनिषत्. The term उपनिषत् is correctly rendered as "seating things beside each other", i.e. it is part of the मीमाम्स process laid out in the ब्राह्मण of discovering and setting out संबन्ध-s. [We stated this before](https://manasataramgini.wordpress.com/2012/06/16/the-broken-chain-and-the-chain-of-knowledge/) and repeat this point again here because we shall seek to touch upon the great problem explored by the उपनिषत्, which was a central mystery in the ancient inquiries among the Indo-European heathens (primarily Hindus, Iranians and the Hellenes). The संबन्ध-s that concern the preceding ब्राह्मण portions primarily deal with the representational connections between the shrauta ritual and the universe. For example, in the standard सामिधेनि recitation (i.e. 15 ऋक्-s chanted in ekashruti + the hiM भूर्-bhuvasvarO3M ) the संबन्ध is established to the year since the 15 gAyatrI-s (x 24) give the 360 with the hiM and the व्याहृति-s representing the 5 additional days and the additional fraction. Thus, the ब्राह्मण abounds in संबन्ध-s to all phenomena. Then comes the उपनिषद्. Here the primary problem is the nature of the संबन्ध between the microcosm and the macrocosm.

One of the simplest expression of the संबन्ध-s between the microcosm and macrocosm is the principle of macranthropy. Let us turn elsewhere in the IE world for an example. We might look at the macranthropic Zeus in Orphic hymn, fragment 168, quoted by the great yavana sage Proclus; Epiphanius suggests that it was hymn composed by Pythagoras:

*Zeus is the first. Zeus the thunderer, is the last.*\
*Zeus is the head. Zeus is the middle, and by Zeus all things were fabricated.*\
*Zeus is male, Immortal Zeus is female.*\
*Zeus is the foundation of the earth and of the starry heaven.*\
*Zeus is the breath of all things. Zeus is the rushing of indefatigable fire.*\
*Zeus is the root of the sea: He is the Sun and Moon.*\
*Zeus is the king; He is the author of universal life;*\
*One Power, one daemon, the mighty prince of all things:*\
*One kingly frame, in which this universe revolves,*\
*Fire and water, earth and ether, night and day,*\
*And Metis the primeval father, and all-delightful Eros.*\
*All these things are united in the vast body of Zeus.*\
*Would you behold his head and his fair face,*\
*It is the resplendent heaven, round which his golden locks*\
*Of glittering stars are beautifully exalted in the space.*\
*On each side are the two golden taurine horns,*\
*The risings and settings, the tracks of the celestial gods;*\
*His eyes the sun and the opposing moon;*\
*His unfailing mind the royal incorruptible ether.*

The veda can sometimes be pithy and presents a comparable concept regarding indra thus (a mantra composed by गृत्समद shunahotra):\
[यस्मान् न ऋते विजयन्ते जनासो यं युध्यमाना अवसे हवन्ते ।]{style="color:#99cc00;"}\
[यो विश्वस्य प्रतिमानम् बभूव यो अच्युतच्युत् स जनास इन्द्रः ॥]{style="color:#99cc00;"} (RV 2.012.09)

The key phrase being: "yo vishvasya प्रतिमानम् बभूव"; The universe came to being as his form. In the vaidika incantation there are several expressions for the universal or all-encompassing form of indra. For example we encounter one such in the mantra:\
[आ रोदसी अपृणाद् ओत मध्यम् पञ्च देवां ऋतुशः सप्त-सप्त ।]{style="color:#99cc00;"}\
[चतुस्-त्रिंशता पुरुधा वि चष्टे सरूपेण ज्योतिषा विव्रतेन ॥]{style="color:#99cc00;"} (RV 10.055.03)

In this highly cryptic hymn with astronomical allusions indra is described as encompassing the entire universe with the gods, the celestial motions and luminaries within. More indirect expressions might be found in the incantations like the chamaka prashna where indra is implied as coming to the ritualist bearing all the deva-s starting from agni all the way to prajApati:

[agnish cha ma indrash cha me somash cha ma indrash cha me सविता cha ma indrash cha me सरस्वती cha ma indrash cha me पूषा cha ma indrash cha me बृहस्पतिश् cha ma indrash cha me mitrash cha ma indrash cha me वरुणश् cha ma indrash cha me त्वष्टा cha ma indrash cha me धाता cha ma indrash cha me विष्णुश् cha ma indrash cha me .ashvinau cha ma indrash cha me marutash cha ma indrash cha me vishve cha me deवा indrashcha me पृथिवी cha ma indrash cha me .अन्तरीक्षं cha ma indrash cha me dyaush cha ma indrash cha me dishash cha ma indrashcha me मूर्धा cha ma indrash cha me प्रजापतिश् cha ma indrash cha me ||]{style="color:#99cc00;"}

Expression of macranthropy explicitly similar to those seen above in the Orphic hymn however start emerging in the somewhat later layers of vaidika texts and can be encountered over a prolonged period in the iterations such as the puruSha सूक्त, the ashvamedha ब्राह्मण with which the बृहदारण्यक opens, and the आत्मसूक्त of the वैखानस-s. Of these, an early macranthropic representation, seen right in the ऋग्वेद itself, is that of the personified mantra power: RV 10.125. Here, interestingly, the macranthropic manifestation is represented as female -- वाक्, and provides the prior for the depictions of the universal forms of the trans-functional goddess in later Hindu tradition. The puruSha सूक्त is reiterated in some form in all the four veda-s and is re-worked in a definitive account of the macranthropic motif in a late atharvan recitation, that is also preserved within the body of the composite and paradoxical text, the मुण्डकोपनिषद् \[Footnote 1]:

[यथा सुदीप्तात् पावकाद् विस्फुलिङ्गाः]{style="color:#99cc00;"}\
[सहस्रशः प्रभवन्ते सरूपाः ।]{style="color:#99cc00;"}\
[तथा ऽक्षराद् विविधाः सोम्य भावाः]{style="color:#99cc00;"}\
[प्रजायन्ते तत्र छैवापि यन्ति ॥ १॥]{style="color:#99cc00;"}

As from a good blazing fire, thousands of sparks identical to it nature are emitted, so also, O soma-ritualist, do diverse beings emerge from the indestructible entity and again return to it.

[दिव्यो ह्य् अमूर्तः पुरुषः स बाह्याभ्यन्तरो ह्य् अजः ।]{style="color:#99cc00;"}\
[अप्राणो ह्य् अमनाः शुभ्रो ह्य् अक्षरात् परतः परः ॥ २॥]{style="color:#99cc00;"}

The shining but verily formless purusha, is unoriginated and is both within and without; he is not the metabolism of life, not the mind, unsullied and beyond even the supreme indestructible entity \[mentioned in the former verse].

[एतस्माज् जायते प्रणो मनः सर्वेन्द्रियाणि छ ।]{style="color:#99cc00;"}\
[खं वायुर् ज्योतिर् आपः पृथिवी विश्वस्य धारिणी ॥ ३॥]{style="color:#99cc00;"}

From this are born metabolism, signaling, all sensory and motor organs, space, gas, light, liquid and solid, which supports all.

[अग्नीर् मूर्धा चक्षुषी चन्द्र-सूर्यौ]{style="color:#99cc00;"}\
[दिशः श्रोत्रे वाग् विवृताश्च वेदाः ।]{style="color:#99cc00;"}\
[वायुः प्रणो हृदयं विश्वमस्य पद्भ्यां]{style="color:#99cc00;"}\
[पृथिवी ह्य् एष सर्व-भूतान्तर्-आत्मा ॥ ४॥]{style="color:#99cc00;"}

The fire is his head, the sun and moon his eyes; the dimensions of space his ears; his speech emitted the veda-s, the air is his breath; all existence his heart. From his feet emerged the earth; he is verily the inner consciousness of all beings.

[तस्माद् अग्निः समिधो यस्य सूर्यः]{style="color:#99cc00;"}\
[सोमात् पर्जन्य ओषधयः पृथिव्याम् ।]{style="color:#99cc00;"}\
[पुमान् रेतः सिञ्चति योषितायां]{style="color:#99cc00;"}\
[बह्वीः प्रजाः पुरुषात् संप्रसूताः ॥ ५॥]{style="color:#99cc00;"}

\[non-literal translation as per commentarial tradition based on the principle of the संबन्ध of 5 fires as expounded in the narrative in the छन्दोग्योपनिषत् 5.3.1 ] From him emerged in the celestial world the fire with the sun as the fuel \[fire 1 seen as the sun]; in the celestial world the waters emerged from soma; the waters poured down on earth as rain storms \[Fire 2 seen as the lightning ]; from the earth emerged plants \[fire 3 seen as energy in plants]; from the plants arose the male seed \[Fire 4] and the female \[fire 5] into which it is cast; thus, numerous organism emerged from the puruSha.

[तस्माद् ऋचः साम यजूंषि दीक्षा]{style="color:#99cc00;"}\
[ यज्ञाश् cha sarve kratavo दक्षिणाश् cha |]{style="color:#99cc00;"}\
[संवत्सरश् च यजमानश् च लोकाः]{style="color:#99cc00;"}\
[सोमो यत्र पवते यत्र सूर्यः ॥ ६॥]{style="color:#99cc00;"}

From him emerge the ritual verses, songs and prose incantations, the ritual initiations, the fire rituals, the all rites and the ritual fees. The year, the ritualist, and the worlds where the moon and the sun shine, all emerge from him.

[तस्माछ् छ देवा बहुधा संप्रसूताः]{style="color:#99cc00;"}\
[साध्या मनुष्याः पशवो वयांसि ।]{style="color:#99cc00;"}\
[प्राणापानौ व्रीहि-यवौ तपश् च]{style="color:#99cc00;"}\
[श्रद्ध सत्यं ब्रह्मचर्यं विधिश् च ॥ ७॥]{style="color:#99cc00;"}

From him emerge the deva-s and the manifold साध्य-s, men, beasts and birds, with the metabolism of uptake and excretion and the plants \[literally rice and barley]; and also tapas, conviction, truth, observance of celibate student life and ritual rule.

[सप्त प्राणाः प्रभवन्ति तस्मात्]{style="color:#99cc00;"}\
[सप्तार्चिषः समिधः सप्त होमाः ।]{style="color:#99cc00;"}\
[सप्त इमे लोका येषु चरन्ति प्राणा]{style="color:#99cc00;"}\
[गुहाशया निहिताः सप्त सप्त ॥ ८॥]{style="color:#99cc00;"}

The seven metabolic processes, the seven flames of agni, the seven fire-sticks, the seven homa-s, from him come into being; and also the seven "worlds" in which the prANa moves \[i.e. the 7 व्याहृति-s of the savitra प्राणायम] as they lay stationed within the cave seven by seven emerged from him.

[अतः समुद्रा गिरयश् च सर्वे ऽस्मात्]{style="color:#99cc00;"}\
[स्यन्दन्ते सिन्धवः सर्व-रूपाः ।]{style="color:#99cc00;"}\
[अतश् च सर्वा ओषधयो रसश् च]{style="color:#99cc00;"}\
[येनैष भूतैस् तिष्ठते ह्य् अन्तर्-आत्मा ॥ ९॥]{style="color:#99cc00;"}

From him spring all the oceans, mountains and flow forth the rivers of all forms; from him emerge all plants and the life-fluids by which the bodies exist with the inner consciousness embedded in the elements \[i.e. of matter].

As an aside, it might be noted that the above late AV macranthropic expression is central to the later expositions of the vedAnta darshana (e.g. in शंकराचार्य's commentary). However, it is possible to interpret this text as also being consistent with सांख्य. This emerges from the apparent paradox in the first two ऋक्-s. The first one talks of all matter being evolutes of the indestructible "अक्षर" and the second talks of the puruSha, who is not life, or mind, being formless and transcending the अक्षर. Further, he is twice identified with the consciousness. Thus, one can take the material अक्षर referred to at first as being the प्रकृति and the consciousness as the puruSha. But what about the paradox of all existing being evolutes of him in a macranthropic sense (which is explicitly expounded)? The above ऋक्-s can be interpreted to mean that they are evolutes of him associated with the अक्षर, which actually undergoes the transformations, while the puruSha is the अन्तरात्मन्.

The आत्मसूक्त tradition of the वैखानस marked the trend of transferring macranthropy to their primary deva विष्णु. This trend was followed shortly thereafter by the other vaiShNava tradition, that of the पाञ्चरात्रिक-s. It was from one such early पाञ्चरात्रिक expression that the विश्वरूप concept was probably incorporated into the bhagavad गीता (e.g. Bह्ग़् 11.22).Indeed, this expression of macranthropy or macrotheria remains closely associated with विष्णु and reappears in the context of the ऐतिहासिक and पौराणिक accounts of his incarnations, such as हयग्रीव ([which we had described before](https://manasataramgini.wordpress.com/2011/06/26/notes-on-hayagriva-siddhi-among-the-nastika-s/)), वराह, नृसिंह and trivikrama. In iconographic terms, the macrotherian वराह appears to have been prevalent from the gupta period onwards (e.g. the great Eran वराह set up by the मैत्रायणीय ritualist धन्यविष्णु, the वराह of Khajuraho or the प्रतिहार वराह now at the Nelson-Atkins Museum, Kansas City). Similarly, the iconography of विष्णु विश्वरूप is also widely attested throughout Indosphere.
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-9kfvwoaHZ-M/UEWnuWsJGLI/AAAAAAAACcI/Z_ydgdIF9-o/s400/विष्णु_विश्वरूप2.jpg){width="75%"}
```{=latex}
\end{center}
```

\
विष्णु विश्वरूप: the macranthropic विष्णु from Nepal, now in the Rubin Museum
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-J6z9vB8Zldw/UEWnn0zaxtI/AAAAAAAACb0/6auYED8ZPkY/s400/वराह_macranthropic.jpg){width="75%"}
```{=latex}
\end{center}
```

\
The macrotherian वराह from Eran, installed by धन्यविष्णु

The macrotherian वराह is encountered early on in a dharma text of the kaTha tradition, the विष्णु स्मृति, and is further embellished and expanded in the पौराणिक retellings of the यज्ञवराह episode \[Footnote 2]. We provide this is full to appreciate the parallels with other macranthropic accounts:

[ब्रह्म-रात्र्यां व्यतीतायां प्रबुद्धे पद्म-संभवे ।]{style="color:#99cc00;"}\
[विष्णुः सिसृक्षुर् भूतानि ज्ञात्वा भूमिं जलानुगाम् ॥]{style="color:#99cc00;"}

With the night of brahmA coming to an end, and the lotus-born one having awoken, विष्णु seeking to emit the beings, perceived the earth covered with water.

[जल-क्रीडा-रुचि शुभं कल्पाधिषु यथा पुरा ।]{style="color:#99cc00;"}\
[वाराहम् आस्थितो रूपम् उज्जहार वसुंधराम् ॥]{style="color:#99cc00;"}

He delighting in water sports, as at the beginning of each former kalpa, assumed the shape of a boar, and raised up the earth.

[वेद-पादो यूप-दंष्ट्रः क्रतु-दन्तश् चिती-मुखः ।]{style="color:#99cc00;"}\
[अग्नि-जिह्वो दर्भ-रोमा ब्रह्म-शीर्षो महातपाः ॥]{style="color:#99cc00;"}

His feet were the vedas, his tusks the sacrificial posts; his teeth the ritual; his mouth the ritual altar; his tongue the fire, his hair the strewn grass, the ritual incantations-s were his head, and he was of great tapas.

[अहोरात्रेक्षणो दिव्यो वेदाङ्ग-श्रुति-भूषणः ।]{style="color:#99cc00;"}\
[आज्य-नासः स्रुव-तुण्डः साम-घोष-स्वनो महान् ॥]{style="color:#99cc00;"}

He, the divine one had the day and night for his eyes were, his ears were decorated by the वेदाङ्ग-s, his nose the goat butter; his snout the sruva ladle, his grunts the sAman songs and he was of a great \[form].

[धर्म-सत्य-मयः श्रीमान् क्रम-विक्रम-सत्कृतः ।]{style="color:#99cc00;"}\
[प्रायश्चित्त-महाघोणः पशु-जानुर् महाकृतिः ॥]{style="color:#99cc00;"}

He was imbued with dharma and satya, auspicious, his movements and strides perfect, his great nostrils were the ritual expiations, his knees the sacrificial animal and his form was gigantic.

[उद्गात्रान्त्रो होम-लिङ्गो बीजौषधि-महाफलः ।]{style="color:#99cc00;"}\
[वेद्य्-अन्तरात्मा मन्त्र-स्फिग्- विकृतः सोम-शोणितः ॥]{style="color:#99cc00;"}

In this form his entrails were the sAman singers, his phallus the homa ritual; his great testicles were the seeds and plants, his consciousness was the vedi altar, his butt the mantra-s and his blood the soma \[juice].

[वेदि-स्कन्धो हविर् गन्धो हव्य-कव्यादि-वेगवान् ।]{style="color:#99cc00;"}\
[प्राग्वंष-कायो द्युतिमान् नाना-दीक्षाभिर् अन्वितः ॥]{style="color:#99cc00;"}

His shoulders were the second वेदी (i.e. the soma ritual configuration), his smell the oblations; he was of great speed like the fires that bear the oblations to the gods, ancestors and the rest, his body eastern ritual hall; he was resplendent and imbued with the ritual initiations.

[दक्षिणा-हृदयो योग महामन्त्र-मयो महान् ।]{style="color:#99cc00;"}\
[उपाकर्मोष्ठ-रुचिरः प्रवर्ग्या-वर्त-भूषणः ॥]{style="color:#99cc00;"}

His heart was the fees paid to the ritualists, he was of great form imbued with yoga and the great mantra-s; his well-formed lips were the ritual of thread-wearing and his embellishments were the pravargya pourings.

[नानाच्-छन्दो-गति-पथो गुह्योपनिषद् आसनः ।]{style="color:#99cc00;"}\
[छाया-पत्नी-सहायो वै मणि-शृङ्ग इवोदितः ॥]{style="color:#99cc00;"}

The metrical Vedic chants were his foraging path, the secret teaching of the उपनिषत् his resting place; he was accompanied by his wife chAyA and was huge as the jeweled mountain (the Himalayas).

[महीं सागर-पर्यन्तां स-शैल-वन-काननां ।]{style="color:#99cc00;"}\
[एकार्णव-जल-भ्रष्टाम् एकार्णव-गतः प्रभुः ॥]{style="color:#99cc00;"}

The land surrounded by the oceans, with mountains, forests and groves was submerged in the waters of the One ocean; into that One one ocean dived the lord.

[दंष्ट्राग्रेण समुद्धृत्य लोकानां हितकाम्यया ।]{style="color:#99cc00;"}\
[आदिदेवो महायोगी छकार जगतीं पुनः ॥]{style="color:#99cc00;"}

He raised up, with on his tusks, the earth seeking the good of the worlds; the first of the deva-s, the great yogin remade the world thus.

[एवं यज्ञ वराहेण भूत्वा भूत-हितार्थिना ।]{style="color:#99cc00;"}\
[उद्धृता पृथिवी देवी रसातल-गता पुरा ॥]{style="color:#99cc00;"}

Thus, the यज्ञ वराह for the sake of the good of the being beings raised up the earth goddess who had formerly sunk to the nether region of रसताल.

[उद्धृत्य निश्चले स्थाने स्थापयित्वा तथा स्वके ।]{style="color:#99cc00;"}\
[यथा स्थानं विभज्य् आपस् तद्गता मधुसूदनः ॥]{style="color:#99cc00;"}

Thereafter, having raised the land, the slayer of madhu, stationed it upon its own seat and distributed the waters upon it according to their appropriate places.

[सामुद्र्यश् च समुद्रेषु नादेयीश् च नदीषु च ।]{style="color:#99cc00;"}\
[पल्वलेषु च पाल्वल्यः सरःसु च सरोभवाः ॥]{style="color:#99cc00;"}

He distributed water of the oceans into the oceans, the water of the rivers into the rivers, the water of the pools into the pools, and the water of the lakes into the lakes.

[पाताल-सप्तकं छक्रे लोकानां सप्तकं तथा ।]{style="color:#99cc00;"}\
[द्वीपानाम् उदधीनां छ स्थानानि विविधानि छ ॥]{style="color:#99cc00;"}

He made the seven पाताल-s and the seven loka-s, the seven continents, and the seven oceans, and fixed their various boundaries.

[स्थानपालान् लोकपालान् नदीः शैल-वनस्पतीन् ।]{style="color:#99cc00;"}\
[ऋषींश् च सप्त धर्मज्ञान् वेदान् साङ्गान् सुरासुरान् ॥]{style="color:#99cc00;"}

\[And also] The protectors of places, the protectors of the worlds, the rivers, mountains, trees, the seven ऋशिस्, who know the dharma, the veda-s together with their अङ्ग-s, the sura-s and the asura-s.

[पिशाचोरग-गन्धर्व यक्ष-राक्षस-मानुषान् ।]{style="color:#99cc00;"}\
[पशु-पक्षि-मृगाद्यांश् च भूत-ग्रामं चतुर्विधम् ।]{style="color:#99cc00;"}\
[मेघेन्द्रचाप-शम्पाद्यान् यज्ञांश् च विविधांस् तथा ॥ VइS १।१-१७ ॥]{style="color:#99cc00;"}

\[And also] the the पिषच-s, snakes, gandharva-s, यक्ष-s, rAkShasa-s, men, cattle, birds, mammals and others, the four-fold beings of civilization, clouds, rainbows, lightnings and the like and various rituals.

Now, these multiple expressions of macranthropy might be seen as peculiar conceptions of the bronze age mind, which in heathen traditions persisted through the iron age, and some Western scholars and their imitators might even see it as being pathological. However, we hold that these are the early expressions of a class of models of the world that continue to be with us. In essence, the macranthropic models try to illustrate two distinct, but sometime related ideas: 1) What is true at small scales can be extrapolated to large scales 2) The same structures seen at small scales re-emerging even at large scales. At the limit this might be taken as the idea of the recursive structure of the world that approximates a scale-free structure (expressed by the analogy of the विस्फुलिङ्ग in the AV chant; see above). This might also be interpreted as a "holistic" view in which every fragment of the whole recapitulates the whole in pieces. Comparable expressions in more recent models of the world include, for example: Just as the electromagnetic force holds electrons around the nucleus in the microscopic realm, the gravitational force holds the planets around sun. Likewise, just as the quarks are held together in the subatomic scales by the strong force, at the massive scale two stars are held together in a system like beta Lyrae. In descriptions of the structure of life too we see such constructs. For example Jacques Monod quipped: "Anything found to be true of *E. coli* must also be true of elephants." It also finds expression in the cellular structure compared to organismal structure. In a ciliate like *Euplotes*, a single cell has a "mouth", "legs" i.e. cilia, and excretory organs, which might also be found in an multicellular organism, like an animal, made of numerous cells. In the same way, the chlorophyte *Acetabularia* has in single cell "roots", a "stem", "leaves" and "seeds" which are all also seen in a land plant made up of numerous cells. The recursiveness manifests itself in the form of simple prokaryotic cells giving rise to the eukaryotic cell that contains multiple prokaryotic cells and the multicellular eukaryote that contains several such cells. Further, this might extend one level further up where organisms might constitute a super-organism: In the case of the cnidarian superorganism *Physalia* (the Portuguese Man o' War) the super-organismal structure might resemble the structure of a single organism literally -- it mimics the general structure of a jellyfish. Alternatively, the super-organism might merely figuratively resemble a single organism as seen in the social organisms like hymenopterans or isopterans. Indeed, the vaidika conception of human society was similarly super-organismic -- the macranthropic puruSha (RV 10.90).

Now the ancients saw the equivalences implied by their macranthropic world view as being a pointer to a more fundamental underlying unity. For example, this is expressed in the famous early पाञ्चरात्रिक praise of विष्णु as the puruSha नारायण attributed to भीष्म पितामह:

[yasmin विश्वानि bhUtAni तिष्ठन्ति cha vishanti cha |]{style="color:#99cc00;"}\
[गुण-भूतानि भूतेशे सूत्रे मणिगणा इव ॥]{style="color:#99cc00;"}

All the worlds and all elements exist in you and merge into you \[upon destruction]. As the lord of the beings, you are the common thread passing through the guNa-s (of सांख्य) and beings\
like pearls strung together on a thread.

[यस्मिन् नित्ये तते तन्तौ दृढे स्रग् इव तिष्ठति ।]{style="color:#99cc00;"}\
[सदसद् ग्रथितं विश्वं विश्वाङ्गे विश्वकर्मणि ॥]{style="color:#99cc00;"}

In your eternal all-pervading form, like lotuses held together by a strong string, the manifest and unmanifest universe is bound together, you whose body and whose actions comprise the universe.

[हरिं सहस्र-शिरसं सहस्र-चरणेक्षणम् ।]{style="color:#99cc00;"}\
[सहस्र-बाहु-मुकुटं सहस्र-वदनोज्-ज्वलम् ।]{style="color:#99cc00;"}\
[प्राहुर् नारायणं देवं यं विश्वस्य परायणम् ॥]{style="color:#99cc00;"}

You are known as hari of a thousand heads, a thousand feet and eyes, a thousand arms, a thousand crowns, and a thousand blazing faces. You are called the deva नारायण the station of the universe \[see above for iconographic representation].

[अणीयसाम् अणीयांसं स्थविष्ठं च स्थवीयसाम् ।]{style="color:#99cc00;"}\
[गरीयसां गरिष्ठं च श्रेष्ठं च श्रेयसाम् अपि ॥]{style="color:#99cc00;"} Mbh 12.47.12-15

You are the most atomic of all atoms, the most expansive of the expansive, the heaviest of the heavy and the highest of the high.

While this expression of macranthropy is typical of the identification of विष्णु with the puruSha of the veda, it represents a use of the macranthropic motif as unifying principle. The recursiveness of the puruSha is made explicit by stating that the universe is set in him and that he is simultaneously in the form of the smallest particle and also the largest entity, i.e. the universe itself. Hence, he taken to be the unifying principle by using the analogy of the string binding the flowers or passing through the pearls -- thus, he is the common thread of all existence that has manifested and also that which remains as yet unmanifest, with the guNa-s and the भूत-s emerging and dissolving into him.

In an analogous way, our modern visions of the universe have been inspired by the intuitions regarding similarities in patterns observed at the microscopic and macroscopic realms. Thus, for example in physics, electrical force/magnetic force (dominant at microscopic realms) and gravitational force(dominant at macroscopic realms; though both have theoretically infinite ranges) have inverse square laws. This lead to the intuition that they are just distinct manifestations of a unified force. This intuition has sparked the most profound investigation in physics, namely the grand unified theory. Similarly, in biology the above statement of Monod represents an intuition of unity underlying all of biology. The quest for the biological unification emerged from the commonalities observed across life from the microscopic single-celled form to the largest multicellular forms. This led to a partial unification provided by the cell theory, which unified all cellular life forms. This preliminary unification led to the observation that essentially all of life shares a common information storage mechanism in the form of nucleic acids and across life that information specifies action mediated by both nucleic acids and proteins. Thus, a deep unification emerged regarding life in the form of the Central Dogma of molecular biology.

***The fissure in the unity of the micro- and macro-cosm***

While the intuitions regarding the micro- and macro-cosmic parallels promised a vision of deep underlying unity, formally working out such unifications has been all but easy. Instead, our excavations or flights in the quest for the unity have revealed deep complexities that we do not fully understand as yet -- much as the shaiva-s would say regarding the quest for the beginning and the end of their vision of unity -- the लिङ्गोद्भव-मूर्ति. Indeed, in physics despite the gravitational and electromagnetic forces having similar equations, electromagnetic could be unified with the weak, and the unification of electroweak with the strong forces seems within striking range. But, unification of the gravitational force with the rest still remains a much more difficult prospect for modern physics. This is a schism between the macrocosm, which has a good theory in the form of relativity to explain it, and the microcosm, which has a good theoretical framework in quantum mechanics, despite the apparently sound intuition of a unity between them. Biology too has its share of schisms between its microcosms and macrocosm. One of these pertains to the conflicts between the interests of a single cell and a community of cells, either in the context of a colony or a multicellular organism. This also extends to the problem of the interests of the individual organisms within a super-organismic set up. After all selection is acting on the genes, which are so closely tied to the unifying Central Dogma. So the interests of individual genes, the interests of the individual cells or organisms do not necessarily align with that of their respective conglomerates, but why are they then altruistic in sacrificing their fitness for that of the conglomerate? Thus, the transition from the microcosms to the macrocosms of biology is not at all a smooth one. To explain such behaviors Haldane first postulated the concept of included fitness and kin selection, which was then extended by Hamilton. Wilson's studies showed that indeed these might explain the hymenopteran and isopteran eusociality. However, in more recent times he has made an about-turn favoring a role for group selection in addition to kin selection. There are other alternative explanations emerging from game theoretical simulations by Nowak that point to altruistic strategies such as the forgiving tit-for-tat emerging as winners in iterated versions of games such as the prisoners' dilemma. Thus, while there are multiple explanations for the why cooperative organismal conglomerates might arise, it is not all clear at this point if a unified explanation holds for all such cases.

Other schisms too emerge in biology -- everything that is true for *Escherichia coli* is not really true for an elephant -- Their primary DNA polymerases have had independent origins, most of their transcription factors are hardly related even though both make mRNA and their chromatin is packaged in unrelated ways and is profoundly different in functional capabilities. Many of the similarities across the levels of life are a result of interactions between the deep homologies and the strong constrains of physics and chemistry acting as the great enforcers of natural selection. Thus, we can have a wheel and axle at the level of the molecular components of life resembling a wheel made by an engineer. However, wheels with axles are absent at all the other levels of organization life and even wheels without axles are at best rare (e.g. a lizard or wheel spider -- *Carparachne aureoflava* \[Footnote 3]). The reason for this is rather obvious because a living wheel has to be kept alive and if made of dead tissue has to be repaired via a connection to the rest of the living tissue and this by definition precludes a biological wheel. Of course the axle-free wheels are equal to the whole organism -- like the wheel spider cartwheeling. On the other hand biological spears exist at every level. We recently discovered a class of bacterial toxins that are shaped and deployed as poisoned spears; spears made of a single protein. Then we have the trichocysts of ciliates like *Paramecium*, which are still microscopic spears but now made up of numerous protein molecules. Then we have a detachable biomineral spear in the form of the gastropod love darts with which land snails stab each other during courtship to improve chances of fertilization. Other spears are made of many cells but are part of the body like the marlin's snout. There are also spears made of many cells in the form of the seeds of grasses, like *Stipa*. Finally, we have the spears made and used by the great apes like *Pongo*, *Pan* and *Homo* (hence the spear was probably an ancestral hunting weapon among the great apes). Thus, the physical constraints prevent wheels in the biological macrocosms but allow them at the lowest level of organization in the biological microcosms. On the other hand, there are spears all the way through. Thus, the deep unity in biology, in the form of the homologies of nucleic acids and proteins, is not the primary basis for the recursive similarities in forms across biological organization, rather it is physico-chemical constraints that allow that similarities. Thus, in a sense these constraints have already determined a relatively small set of ideal categories into which natural selection can channel the forms of biology. This is reminiscent of the eternal jAti-s of the नैयायिक-s, which remain conserved, even though the individual vyakti-s within it come and go. However, the physico-chemical constraints allow vyakti-s of certain jati-s (e.g. the spear) to exist at all levels of organization, whereas in other jati-s (e.g. the wheel) they allow them to exist only at certain levels. Likewise, we might compare these forms predetermined by the constraints to the the ratu-s of our Iranian cousins or the Platonic ideals in in the yavana world (we may interpret Plato's vision of the circular motions being the property of the nous; "peri noun kai phronesin"; as an early acknowledgment of the constraints in the form of physical laws).

*[The constraints of category: Biological systems can only evolve microscopic wheels with axles but they need to be engineered at the macroscopic level. The spear was invented and engineered many times in biology]{style="color:#00ccff;"}*\
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-fW7cuBoJsZc/UFV_वाOWsaI/AAAAAAAACdA/4ZmsZOC3FXM/s800/stuff2.jpg){width="75%"}
```{=latex}
\end{center}
```



*So did the ancients run into the issue of a potential schism between the microcosm and the macrocosm when they formulated a macranthropic universe?*

We are of the opinion that the answer is yes, and that the realization of this schism gave rise to much discussion and theory among both the Arya-s and yavana-s. We would present it thus:\
The individual animal, like a man, has a consciousness (Atman, i.e. defined as the experiencer of the first person experience or qualia; this is laid out in the ऐतरेयोपनिषद् 3.2). When we construct a macranthropic universe, what is the consciousness of that macranthropic universe (brahman) and what its relationship to the consciousnesses of the individual beings (their Atman) within that macranthropic frame?

It appears that in the Arya world some felt that the consciousness of the macranthropic universe should be identified with various "cosmic" entities. For example, there is the famous vaidika statement: "asau वा Adityo brahmA" or "asav Adityo brahmA", which suggests that some took the yonder sun to be the consciousness of the universe. In the छान्दोग्योपनिषत् we are told of teaching of the kShatriya teacher ashvapati the king of the kekaya-s (in what is today Afghanistan) to several ब्राह्मण-s. Indeed, there we encounter सत्ययज्ञ पौलुषि mention that the sun is the universal consciousness . Similarly, other ब्राह्मण-s like aupamanyava, indradyumna भाल्लवेय, jana शार्करक्ष्य, बुडिल आश्वतराश्वि and उद्दालक आरुणि held views that the universal consciousness might be the sky, the gaseous element, vacuum, the liquid element and the solid element respectively. A parallel might be seen in the yavana world might be found in the discourse of Hermogenes and Socrates (Cratylus; I would like to acknowledge the work of the American philospher and mathematician Stephen Menn in leading me to discovering this parallel). There, Hermogenes mentions different views regarding what might be the entity that is the yavana equivalent of the brahman concept. He says that some hold that it is the sun, others that it is the fire and yet others that it is the heat emnated by the fire. But he then says that Anaxagoras dismisses all those and states that it is none of those but the nous -- this is similar to ashvapati kaikeya dismissing the opinion of the ब्राह्मण-s regarding the universal consciousness and revealing his teaching of the true nature of brahman.

It appears to us that the preferred solution of many of the औपनिषद-s is that contrary to the recursiveness or the scaling seen in other "organs" of the "macranthrope" the consciousness does not participate in the recursion or scaling. It remains undivided, constant and the same between the universe and the individual conscious beings. Thus, it is only one and undivided at all scales of organization. This, of course is ashvapati's preferred inference, which is also repeated by याज्ञवल्क्य and several other ancient thinkers. It has been repeatedly rationalized and rexplained since then by many a Hindu thinker. But we may still ask the question if this conclusion is justified and and is there any evidence that it might be the true one.

An alternative hypothesis was that of भूत-chaitanya-वाद favored primarily by the चार्वाक-s. In its nihilistic form it involves the denial of the consciousness as being anything but illusory. Its more affirmative form posits consciousness as an emergent property that is probably linked with biological or artificially intelligent systems. So put bluntly, the macranthropic thinking has only lead to false ideas, because its premises are flawed in the first place. There is no need to think that a macro-cosmic assemblage of conscious being should have a consciousness because it is a unique emergent property requiring a particular form of sensory architecture and signaling connections. After all, they might reason, that the cells have no consciousness, but the "macranthropic" body, wholly comprised of cells does. When you have emergent properties you are not limited by something being represented at every level of the organization. Modern exponents like Crick went as far as equating this emergent property with a particular form of neural activity -- e.g. coordinated signaling by neurons at frequencies of \~40 Hertz. The way they put it, it was certainly naïve, but one could argue that it is an emergent property due to development of a certain type of neural structures and activity. Now, can we formulate a program to distinguish between these alternative hypothesis? We posit that if consciousness \[Footnote 4] can somehow be shown to have emerged due to natural selection or as a Gouldian spandrel due to selection on some other entity then bhuta-chaitanya-वाद is established. In our opinion Crick and Koch have not demonstrated this. Now if it can be instead shown that it has not emerged due to the action of natural selection, but provides a selective advantage to the possessor then bhuta-chaitanya-वाद has to be forsaken for alternative hypotheses. If it can be shown that it provides no selective advantage and can be separated from coherent evolutionary explanations for the sensory and neural architecture then again भूत-chaitanya-वाद is falsified. Our own investigations into biology have shown that we can use evolutionary principles to coherently explain the origin and elaboration of the sensory and signaling architecture and there is no place for consciousness in this robust theory. Thus, consciousness can be separated from a coherent evolutionary theory for sensory-signaling systems. What we cannot yet firmly demonstrate is that consciousness really provides no selective advantage. So we are not in a position to falsify भूत-chaitanya-वाद yet.

Certain other lines of reasoning suggest that the idea of a unitary consciousness or a conscious macranthropic entity mighty be approached via biological phenomena. Intake of DMT, a compound naturally produced by the pineal, results in experiences similar to ब्रह्मानन्द whereas some have reported macranthropic visions with iboga and related extracts. These observations suggest that such insights are actively kept out of the regular experience by the neural machinery. Yet, it appears that humans, like other animals, attempt to seek such states. For example, iboga is used in the wild in Africa both by our close cousin the gorilla and the more distant one the mandrill baboon, in addition to its use by pigs, crested porcupines and elephants. The Amanita mushroom is used by reindeer for experience of hallucinations in Northern Eurasia, while in the North American Rockies and the deserts of Libya sheep seek green-yellow lichens, probably of the genus Lecanora for hallucinogens. Dogs have been reported to use toad hallucinogens. Indeed, the use of such ओषधि-s by humans is likely to have been learned from other animals. These sought after states being kept out of the general consciousness stream raises the possibility that their unregulated experience could be selectively disadvantageous organism. This is supported by the fact that the sheep in the Rockies and the Libyan desert are known to lose their teeth in scraping the lichen of rocks, resulting the nullification of their fitness due to seeking the special states of conscious experience. While certain forms of human addiction can be posited as a similar situation, we may also point out that certain seekers of the experience of unitary consciousness in Hindu traditions nullify their fitness through the practice of premature संन्यास. Indeed, in the more recent traditions individuals with fitness-erasing proclivities in such direction have been heralded as "brahman-realized" saints in the larger Hindu system. So this line of reasoning suggests that the unregulated experience of unitary consciousness is something that is selectively disadvantageous and actually filtered out by the action of the neural apparatus.

Now someone might turn to us and ask: Are you not positing insights described in this discussion as being pathological yourself, while accusing the white indologists and their fellow travellers of the same? I will conclude by pointing out that my own position is distinct in relation to two points: 1) Just because the experience of unitary consciousness might be kept out by the neural and sensory apparatus does not mean that it is a pathological state. Indeed, it might even be a very profound and perhaps correct insight. As an analogy we might point out to the American biochemist Kary Mullis discovering PCR and some cosmological analogy under the influence of lysergic acid diethylamide. At least the former of these insights, even if developed in a privileged state, which is normally disallowed by the neural and sensory apparatus, results in a correct apprehension of reality. Similarly, the insights regarding a pervasive consciousness or non-भूतचैतन्य explanation could be potentially correct, even if only accessible in a privileged state. 2) While potentially fitness-reducing in unregulated use, controlled utilization of such privileged states and insight ensuing therefrom could have the opposite effect. We suspect that the early Hindus realized this and developed regulated mechanisms for accessing these states and parsing the insights accrued therein. However, I suspect that this rigor was lost in many traditions and replaced by practices that did reduce fitness.


Footnote 1: The मुण्डकोपनिषत् is a composite text which has combined older material of the AV tradition with new material coming from a veda-virodhaka stream. It gives us a glimpse of what might have been the case if the तथागत had taken up the offer of the ब्राह्मण-s to render his veda-virodhaka teachings in Arya-वाक् rather than sticking to the vulgar Pali.

Footnote 2: The वराह myth of course predates these texts and goes back to the veda. The तैत्तिरीय AraNyaka (10.1.8) speaks of a great black boar lifting up the earth ("[उद्धृतासि वराहेण कृष्णेन शतबाहुना]{style="color:#99cc00;"}"). This boar is also mentioned in the AV-vulgate 12.1.48 ("[वराहेण पृथिवी संविदाना सूकराय वि जिहीते मृगाय]{style="color:#99cc00;"}"). In several cases prajApati is described as the one incarnating as this वराह: for example in the तैत्तिरीय saMhitA 7.1.5.1 prajApati is mentioned as lifting out the earth from the water. But in this ब्राह्मण section already विष्णु's power is hinted, so he is close at hand to take over this role. Similarly, in the shatapatha ब्राह्मण (14.1.2.11), prajApati is described as being the boar एमूष that lifted up the earth. But here again विष्णु is at hand because the earth is connected to the head of विष्णु or makha as the narrative occurs as part of the instruction on the pravargya ritual. In both the तैत्तिरीय saMhitA and the RV interestingly indra and विष्णु are mentioned as killing एमूष a boar on the side of the दानव-s. In the same तैत्तिरीय saMhitA 7.1.5.1, the power of indra is also emphasized -- interestingly, in the avestan yaSht to verethraghna it is he who is described as incarnating as the वराह. (Av: वराज़). He is invoked as a mighty वराह in the avestan manthra:\
[verethraghnO ahuradhAtO hu kehrpa वराज़हे]{style="color:#33cccc;"}\
[ paiti-erenO tizhi-dãstrahe arshnO tizhi-असूरहे]{style="color:#33cccc;"}\
[ hakeret-janO वराज़हे anu-pOithwahe ग्रञ्तहे ||]{style="color:#33cccc;"}\
The idea of the ritual-associated वराह goes back to at least the Indo-Iranian period as in the avestan ritual the Iranians invoke a mighty वराह known as दामोइश् upamana (e.g. yasna 6.14: "[ukhrem taxmem dAmOish upamanem yazatem यज़ामैदे |]{style="color:#33cccc;"}" ) who embodies the ritual power and is mentioned as accompanying mithra to the battle.

Footnote 3: Many years back तैत्तिरीय-guru asked और्वशेयी and me whether we thought all biological form was simply a result of natural selection. Our bodies are as complex as a car or a scooter could not have emerged by chance. When did you last see a scooter grow up by chance he thundered and continued: "that is why the world needs an Ishvara". कौण्डिण्या retorted: "It is that very natural selection that precludes cars and scooters from being born of life." The तैत्तिरीय-guru roared: "Why not? I have proved you wrong by inconsistency." She stumbled in answering him, so we stepped in explained why wheels with axles are unlikely to emerge but many other things can. He did not seem to understand, as his biology was insufficient, and attributed our "ignorance" to our lack of Ishvara-bhakti. Some years later कौण्डिण्या brought it to my attention that Richard Dawkins had made similar arguments to what I had done regarding the non-existence of the biological wheel with an axle.

Footnote 4: Western or Western-influenced biologists in general and neurobiologists in particular can exhibit a gross misunderstanding of what consciousness means. They often conflate consciousness with neural signaling and sensory reception. These two are purely biological issues that only impinge on but not comprise consciousness.


