
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The object and mirror image](https://manasataramgini.wordpress.com/2005/11/29/the-object-and-mirror-image/){rel="bookmark"} {#the-object-and-mirror-image .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 29, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/29/the-object-and-mirror-image/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had assembled for the gathering- me, durmukha, maireyapayin, मूलापस्मर, वात व्यादि, म्लेच्छ sundarI, कवर्गा, harir-बारव all सहकुटुम्ब. Glad to have escaped the तीक्ष्ण hima we settled down to reel out the yarns. The conversation drifted to the journey that we had made with कलशजा to the hills in central India, where apparently bhasmasura had placed his hand on his head. कलशजा's mother had provided us with the contact of a pious ब्राह्मण woman who was civil servant in those regions held by the Dravidian tribes. कलशजा, me, the muni and my bhima-like clansman bhojanapriya set out on the expedition. We were picked up at the forbidding station of Pipariya in the late evening by her attendents and taken to her home. We enjoyed the best food that night as though we were enjoying the bhogas of indra's svarga. The state servants of India truely enjoyed immense opulence at public expense.

The next day we were taken by her to the immense massif of Chauragarh. Chauragarh was the capital of the great warrior queen of the Hindus-- रणी दुर्गावति, who valiantly fought the terrible Moslem tyrant Akbar. Every step appeared to be endowed with the great history of the last Hindu struggle in central India. It was a enormous pathway up hundreds of steps leading to the crown of the great massif, atop which rested the shrine of The god. It is known as the त्रिशूल sthala were thousands of tridents of various sizes were planted. People perform the त्रिशूल vrata of carrying a heavy त्रिशूल up the massif and planting it next to the shrine of rudra. Some people had heroically lifted tridents weighing over 300 kgs in weight cooperatively in their pious observance of the vrata. It is the most awe-inspiring shrine of all the shrines of महाकाल and the mantra automatically came to our mouth:\
[áवोcआम न्áमो अस्मा अवस्य्áवः शृण्óतु नो ह्áवं रुद्र्ó मर्úत्वान् ।\
]{style="font-weight:bold;font-style:italic;color:rgb(255, 102, 102);"}It looked as though the most terrible rudra himself stood there surrounded by the maruts bearing tridents. From the heights of Chauragarh one could enjoy the finest sights in India. We felt one with the terrible pashupati, who was roaming around in the air, the waters in the plants and the animals of which he is the lord.[ ]{style="font-style:italic;"}The elemental nature of the deity known as bhava and sharva was felt in the air and to our mind came the formula: [नैनं शर्वो न भवो नेशानः । नास्य पशुन् न समानान् हिनस्ति य एवं वेद ॥\
]{style="font-style:italic;color:rgb(255, 204, 102);"}\
After the heavy excercise of the climb we came back down to enjoy a late evening tiffin and a chat in the verandah. Everything was a pleasure to the indriyas- the food and drink (the very thought of which makes our mouth still water- the parvata of bhojana), the images of pashupati that fill the air, the cool zephyrs blowing in from the window, the rare pleasant smell of the night queen flower, the song of the parasitic कोकिला bird and soft bed to sleep upon. Truely it felt like being in suvargam लोकं. We were unconscious of the time we spent in endless conversation which coursed from topics like the origin and evolution of crocodiles, the mantras of the मन्थान bhairava tantra, the rahasyas of the ऋग्वेद, the battles of दुर्गावती and vIra नारायण सिंहदेव, the antiquity of the vedas, the origin of turtles, the relationship between lizards, synapsids, the tree of life, sexuality of slugs and snails and the tales of the पुराणस्. There was a magical quality to the whole time we were spending.

At night I went with the muni to our shared room. I was then asked to look into a mirror and saw everything except my own image. I wondered why. I was told that the राक्शसि had sucked the image and I will see it in the राक्शसि's mouth 16 years later.\
.....\
The news of the three Kartiks reached us: #1 had died mysteriously; #2 had received the required punishment. #3 was enjoying the pinnacle of his success. (अमात्य now codenamed yoni) . Yoni in the midst of the समूह of दध्नकाया, वितरास्या and ati-दध्ना was the only one who had lost the game of dice. The rest had pocketed their share of wealth and were wending their way confidently. Yoni asked where is my share. I too thought I had won, just like the above said. I cast the die and I thought I saw "Six". The answer came in the mouth of the राक्षसि, also known as the burning lap of निरृति.\
[]{style="font-weight:bold;font-style:italic;color:rgb(255, 102, 102);"}


