
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The kingdom in danger](https://manasataramgini.wordpress.com/2005/06/14/the-kingdom-in-danger/){rel="bookmark"} {#the-kingdom-in-danger .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 14, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/14/the-kingdom-in-danger/ "6:45 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For the first time in our reign did we find the kingdom in such an existential crisis emerging from our earlier conquest of खाण्डवप्रस्त. We checked all past events- it looked as though most of our mantras had worked. But the enemies were very numerous and all it took was a small chink in the armour to break through. We have exactly 8 days to grind out one of our main adversaries and the कृत्या is raining hell all the while.


