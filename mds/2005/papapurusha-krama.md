
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पापपुरुष krama](https://manasataramgini.wordpress.com/2005/10/04/papapurusha-krama/){rel="bookmark"} {#पपपरष-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 4, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/04/papapurusha-krama/ "4:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

An important rite for cleaning oneself of the ritual ills is the performance of the पापपुरुष ध्यानं, which is closely linked to the सांख्य thought that pervades the tantra. The mental vidhi is spelt out in the bhutashuddhi tantra. Parallel purificatory details are also provided by इशानशिव's manual on the siddhAnta tantra stream. First one performs the bhuta सृश्टि krama thus:\
पृथिवी bIja: ऋषि=brahmA, Chandas=gAyatrI, देवता= पृथिवी. One meditates on the feet up to the knees. yantra: square, yellow color, marked with a vajra, with a central bindu with the mantra laM. One should note the पृथिवी तन्मात्र emerging from the laM bIja. Thus, this bhuta is purified.

वरुण bIja: ऋषि=हिरण्यगर्भ, Chandas=अनुष्तुभ्, देवता= वरुण. One meditates on the zone from knees up to the navel. yantra: bow shaped, white color, marked with a bow, with a central bindu with the mantra वं. One should note the jala तन्मात्र emerging from the वं bIja. Thus, this bhuta is purified.

agni bIja: ऋषि=kashyapa, Chandas=jagati, देवता= agni जातवेदस्. One meditates on the zone from navel up to the heart. yantra: triangular, red color, marked with a lotus, with a central bindu with the mantra raM. One should note the rupa-teja तन्मात्र emerging from the raM bIja. Thus, this bhuta is purified.

वायु bIja: ऋषि=किष्किन्ध, Chandas=बृहती, देवता= वायु मातरिष्वान्. One meditates on the zone from heart up to the eyebrows. yantra: circle, dark-blue color, marked with a svastika, with a central bindu with the mantra yaM. One should note the वायु तन्मात्र emerging from the yaM bIja. Thus, this bhuta is purified.

आकाश bIja: ऋषि=rudra, Chandas=त्रिष्तुभ्, देवता= परमात्मन्. One meditates on the zone from eyebrows up to the middle of the head. yantra: formless, colorless, with a central bindu with the mantra haM. One should note the आकाश तन्मात्र emerging from the haM bIja. Thus, this bhuta is purified.

Now the भूत सम्हार krama follows:

पृथिवी तन्मात्र merges into jala तन्मात्र; it merges into the rupa-teja तन्मात्र; then it merges into the वायु तन्मात्र; the it merges into the आकाश तन्मात्र; then that in अहंकार; the that merges into mahat; then that merges into the puruSha.

The the साधक utter अहं ब्रह्मास्मि / ब्रह्मैवाहमस्मि //

Then one should conceive the पापपुरुष in the left side of the abdomen. The पापपुरुष is the size of the thumb and dark blue in color. The killing of a ब्राह्मण is his head. The stealing of हिरण्य form his hands, The drinking of beers and wines is heart. Sex with the teacher' wife his hips. Association with doers of such crimes his legs. The पातकपुरुष's limbs are embodied by the पातकस्. The upa-पातकस् and consumption of abhojya and अभक्ष्य food are his corporal hair. He is red-eyed and red haired and charges with a sword and shield, like a lump of collyrium.

Having meditated upon the pAtaka puruSha, one sips water and mutters:\
yaM it वायु बीजस्य किष्किन्धो ऋषिः, jagati छन्दः, वायुर् देवता. raM it agni बीजस्य kashyapo ऋषिः jagati छन्दः, agnir देवता. वं iti वरुण बीजस्य हिरण्यगर्भ ऋषिः, त्रिष्तुभ् छन्दः, वरुणो देवता. पापपुरुष शोषण दाहन प्लावन iti विनियोगः.

Then with in-breaths he repeats yaM 16 times. He conceives the वायु emanating from the yaM bija drying up the पापपुरुष ruthlessly. Then he utters raM 64 times, each time holding his breath in the lungs. The fire emerging from the raM bija burn the sinful being completely. Then with out-breaths he utters वं 32 times and washes away the ashes with the water emerging from the वं bija.


