
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [indra stuti](https://manasataramgini.wordpress.com/2005/05/30/indra-stuti/){rel="bookmark"} {#indra-stuti .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/05/30/indra-stuti/ "4:42 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The indra stuti is another hymn in the पौष्य section of the adiparavan of the महाभारत (1.3.150-1.3.153 in the critical Poona edition of the महाभारत) which appears to be a composition of the "intermediate period". However it bears certain distinctive features that differentiate if from the ashvina stuti and suggest that it is even closer to the transitional period than the former. It appears in the same frame story as the ashvina stuti of upamanyu: veda of the भार्गव clan was a student of Ayodo dhaumya. After he had gone through his own trial he was granted mastery over the veda and the dharmasutras by Ayodo dhaumya. He then founded his own school and had his own student also of the भार्गव clan, उत्तङ्क. Once the क्षत्रियस् पौष्य and janamejaya invited veda to serve as the brahmA priest in their sacrifice. veda left, instructing उत्तङ्क to take full care of his household. When he was away veda's wife moved by amorous arousal asked उत्तङ्क to have sex with her, but he firmly refused. veda on his return was pleased with the conduct of उत्तङ्क and graduated him, and asked to proceed on his own. He insisted that he would need to pay veda his parting fee though veda asked him to take it easy. Pressed thus veda asked उत्तङ्क to ask his wife for whatever she wanted and provide that as the fee. She demanded the earrings of the queen of पौष्य. So उत्तङ्क went on his quest. On the way he saw a gigantic bull and mighty man tending it. The man ordered उत्तङ्क to eat the dung of the bull and drink its urine. उत्तङ्क refused but the mighty man commanded उत्तङ्क to do stating that his own teacher had done the same (1). After partaking of the unclean material, उत्तङ्क washed his mouth, purified himself and went to पौष्य and asked him for his queen's earrings. The kShatriya asked उत्तङ्क to go and ask her directly in her apartment. He went there but the queen was not in sight. He came back and asked पौष्य why that was so. The king told him that he was impure due to improper purification after his meal and the queen did not give audience to unclean souls. So उत्तङ्क again rigorously purified himself and went to the queen and made the request for her earrings that were desired by veda's wife. She gladly gave that to him but stated that he would have to safe guard them from तक्षक the nAga who also desired them. The king then asked him to have shrAddha lunch before he left as he was a learned brahmin. It turned out that the lunch was cold and defiled by the hair of a woman. Getting furious उत्तङ्क cursed the king with blindness. The पौष्य was taken aback by the curse and in retaliated by cursing the brahmin with sterility. उत्तङ्क objected by stating that it was king who had offended him first and showed him the hair and cold food. The king accepted that there had been a problem with the food, so the brahmin stated that पौष्य's blindness would eventually be cured. The king was unable to withdraw his curse but उत्तङ्क stated that he would eventually smother the curse with his own brahman power as he was not on the wrong.

उत्तङ्क then started off with the earrings, on the way while he was having his bath when a vagabond came and stole the earrings. उत्तङ्क chased him with utmost speed after having worshiped the devas and just as he caught he changed his form to that of the snake तक्षक and started burrowing through a hole. He began pursuing the snake by digging into the hole with a hoe, but failed to make much progress. So he invoked indra for aid, who sent his vajra into उत्तङ्क's hoe and it started drilling a hole behind the serpent. Finally he reached the sarpa loka where he saw the wonderful underground world of the sarpas with enormous palaces and terraces. Here he worshiped दृतराष्ट्र ऐरावत the lord of all नागस् with the sarpa आनुष्टुभ् invocations and asked for the earrings but they did not give it to him. Dejected he looked around and saw a peculiar sight. Two dames were weaving a cloth in fine loom with black and white threads. It was run by a wheel, with twelve spokes, turned by six boys. And he also saw a being with a great horse. He immediately invoked indra with the indra stuti. The being with the horse then said I am pleased with you praise, ask me what you want. उत्तङ्क sought victory over the serpents. The being asked उत्तङ्क to blow into the horse. On doing so flames issued forth from all the orifices of the horse and threatened to burn the loka of the sarpas. Fearing destruction तक्षक handed over the earrings to उत्तङ्क. Just then उत्तङ्क realized that the auspicious moment for his teacher's wife to wear the earrings had come and it would take him a long time to get back to her. He asked the being how he could return. The being gave him his great horse and asked him to ride it back. In a moment the horse bore him back to veda's abode. veda's wife was about to lay a spell on him for not getting the earrings, when he arrived with them just in time and gave her the gift. Pleased with him she instead gave him a boon of complete success.

On being asked by veda about the cause for his delay, he narrated the entire tale. veda explained to him that the mighty man with the enormous bull was indra. The two dames weaving the cloth were धाता and विधाता and the threads were the days and nights. The wheel was the year with the 12 months and the six boys the seasons. The being was indra who had also borne veda aid in the past and the horse was agni. The dung and urine was actually ambrosia which had allowed उत्तङ्क to live while in the loka of the sarpas. veda then bade him to make his own way in the world.

The indra stuti of उत्तङ्क (UIS) has the following interesting features:


 1.  It uses the old vedic style in some respects such as the phrase "vajrasya भर्ता" which is encountered right from the archaic nivids recited in the shrauta ritual.


 2.  In terms of the allegories for the wheel of time the UIS resembles the (UAS) and the सूक्तं RV1.146, suggesting that like the UAS it is clearly based on an astronomical allegory. The main allegory here is that of the year and the hymn provides the vedic formulation of how linear direction time (conceived as a cloth woven with days and nights) emerges from a harmonic process of the celestial cycles (compared to the harmonic movement of the loom).


 3.  The meter is clearly centered on अणुष्तुभ् and is close the classical shloka of epic literature. In this sense it is atypical with respect to a vedic सूक्तं.


 4.  The invocation in the form of a salutation is seen in the last line of the hymn. While the salutation formulae are relatively common in the yajur texts like "namo astu sarpebhyo" or the namaka आनुवाकस् of the shatarudriya chant, the phrases seen in the last line of the UIS is rare or even absent in the vedic texts. However it is very common in the इतिहास-पुराण where the terms like जगदीश्वर or trilokeshvara are very common in the praise of most deities.

Thus the UIS appears to be a sample of a "transitional" text where the old vedic metaphors are used in the context of an old narrative similar to the ब्राह्मण narratives, but new elements typical of the Pauranic corpus are already emerging.

The mythographic frame of the UIS is far more complex than the UAS. At the face of it, like the latter hymn, the UIS also has a clear astronomical underpinning as suggested by the year allegory. While the theft of the earrings and their recovery many be interpreted as a form of a solar regeneration myth there is hardly any emphasis on it here unlike the repeated emphasis in the frame myth of the UAS. This, taken together with the more complex frame, suggests that there is a more complex astronomical imagery embedded in the language of this myth with the stolen earrings. A few elements which emerge from this language are described below.

Firstly, it is most likely that the earrings, as bright objects, do have a solar connection to them. Most importantly the name of the king in the tale is पौष्य (वृद्धि form of पुष्य) and the thief of the earrings is the sarpa (तक्षक) can be linked to the नक्षत्रस् पुष्य (associated with the M44 cluster) and sarpa (Aslesha) are next to each other. The summer solstice, and accordingly the archaic new year full moon lay between these constellations in the end of the vedic period and the immediate post-महाभारत era (1300-1200 BC). The loss, theft or disappearance of bright objects, like fire, and gold are typically associated with myths signifying the [precession of the axis](http://manollasa.blogspot.com/2004/04/great-circle.html) with the sun (the bright object emerging often elsewhere) . Thus, it appears like the earrings of पौष्य's wife appear to be sun slipping to the new point between the snake's head (the नक्षत्र pushya is the head of the serpentine constellation Hydra) पुष्य (M44 and surrounding stars in the constellation of Cancer) during the summer solstice. Here it was recovered by उत्तङ्क with indra and agni's aid. Further evidence for this is offered by the lines from the sarpa chant of उत्तङ्क as he entered nAga-loka.

[सुरूपाश्च विरूपाश्च तथा कल्माष-कुण्डलाः ।]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}\
[आदित्यवन्-नाक-पृष्ठे रेजुर्-ऐरावतोद्भवाः ॥१४०॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

Of beautiful form, of various forms with speckled ear-rings are the serpents. They shine in the roof of the sky like the sun, snakes born of ऐरावत.

[बहूनि नाग-वर्त्मानि गङ्गायास्तीर उत्तरे ।]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}\
[इच्छेत्कोऽर्कांशु सेनायां चर्तुमैरावतं विना ॥ १४१॥\]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

On the northern banks of the गङ्गा are many stations of serpents. Who except ऐरावत would desire to move in the burning rays of the Sun?

Here the serpents are mentioned as shining with the sun in roof of the sky and धृतराष्ट्र ऐरावत, the lord of the serpents is mentioned as moving in the burning rays of the sun. These clearly indicate that serpent lord ऐरावत is celestial and represents the constellation of Hydra whose head is Aslesha with the summer solstice sun (presided by the vedic sarpa). This also explains what is meant by the north bank of the गङ्गा: the celestial गङ्गा is the Milky Way, just to the north of which lies the constellation of Hydra.

Now there are other more obscure motifs in the myth that are also suggestive of astronomical connections or links to other myths:

At the very beginning of the tale of उत्तङ्क is the mention of the mention of the illicit sexual advance made by veda's wife to उत्तङ्क. Such illicit or incestuous sexual relationships are mentioned in other precessional myths of vedic and itihasa-पुराण origin. 1) The precessional myth associated with कृत्तिक and the birth of कुमार has the wives of 6 of the 7 ऋषिस् engaging in an illicit dalliance with agni. 2) The myth associated with रोहिणि has prajApati making an incestuous advance on his own daughter. 3) In the classical precessional myth of the churning of the ocean there is the illicit advance of rudra towards मोहिनी. 4) In the वृषाकपि hymn, correctly interpreted by Tilak as a precessional myth, वृशाकपि (the virile ape) makes and illicit advance towards इन्द्राणि. For which इन्द्राणि punishes him by making a dog (Canis Major) bite him. These analogies suggest that the illicit sexual advance is a motif in the precessional which probably stands as a metaphor for the break in the old order or the longing for a new relationship, previously forbidden.

Another strange motif occurs in two independent of उत्तङ्क tales (1) is the consumption of filth that normally causes the brahmin to fall to ritual impurity. In both cases filth happens to be a concealed form of ambrosia. The exact implications of this concealed truth are not certain, but it could imply that the truth is always concealed. The temporary blindness of the king and the sterility of उत्तङ्क could be a metaphor of the solar decline at the winter solstice embedded within the larger frame. However, it could also signify the mythological "chaos" often associated with the precessional change of the coordinates of the year. Finally, there is the motif of the fire of equine form within [water](http://manollasa.blogspot.com/2004/04/indo-european-divine-tripartation.html) briefly alluded to as "garbham-अपां" -- the horse of indra, which is agni. The connection of this equine fire within water to the astronomical myth of precession needs further exploration.

Another peculiar feature of the UIS is the black garment of indra. Some commentators have taken this to mean the color of the night sky \[with the thousand eyes of indra being the stars] or the thunder clouds. It is interesting to note the connection with the samavedic उद्गाता priest learning the rahasya gAnaM, the great शक्वरी sAman, during which he wears black clothes. The शक्वरी is said to be an embodiment of the vajra and the उद्गाता wearing black clothes comes from the forest to the sacrificial arena, chanting it. This is the rite of bringing of the vajra and is supposed to be followed by a thunderstorm.

A translation of the indra stuti is provided below:

1.3\
[त्रीण्य्-अर्पितान्-यत्र शतानि मध्ये षष्टिश्च नित्यं चरति ध्रुवे ऽस्मिन् ।]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}\
[चक्रे चतुर्-विंशति पर्व योगे षड् यत्-कुमाराः परिवर्तयन्ति ॥१५०॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

Around the axis set with the 300 and 60 \[spokes] the \[wheel] perpetually turns. The six young boys turn the wheel marked with 24 divisions (2) occurring as pairs.

[तन्त्रं चेदं विश्वरूपं युवत्यौ वयतस्-तन्तून्-सततं वर्तयन्त्यौ ।]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}\
[कृष्णान्सितांश् चैव विवर्तयन्त्यौ भूतान्यजस्रं भुवनानि चैव ॥१५१॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

These two dames of universal form are weaving without intermission a cloth with threads. With black and white \[threads] they weave ushering into existence the manifold worlds and the entities within them.

[वज्रस्य भर्ता भुवनस्य गोप्ता वृत्रस्य हन्ता नमुचेर्निहन्ता ।]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}\
[कृष्णे वसानो वसने महात्मा सत्यानृते यो विविनक्ति लोके ॥१५२॥]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

Oh bearer of the vajra, the protector of the universe, you are the slayer of वृत्र and the slayer Namuchi. Oh illustrious one, who wears the black cloth and displays inviolable and violable laws of the universe.

yo वाजिन.n garbham-अपां पुराणं वैश्वानर.n वाहनम्-अभ्युपेतः .\
नमः सदास्मै जगदीश्वराय लोकत्रयेशाय पुरन्दराय ..153..

That horse which was born from the depths of water in the ancient past, \[which is] the fiery वैश्वानर, is furnished as your horse. I salute you, the supreme lord of the universe, the lord of the three worlds, and the destroyer of forts.

.......................\
Notes\
(1) In another tale उत्तङ्क is offered ambrosia by indra in the form of the urine of a niShAda in a desert.

\(2\) The 24 divisions on the wheel of time are the 24 fortnights that occur as pairs corresponding to each month.


