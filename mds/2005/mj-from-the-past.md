
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [MJ from the past](https://manasataramgini.wordpress.com/2005/06/15/mj-from-the-past/){rel="bookmark"} {#mj-from-the-past .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 15, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/15/mj-from-the-past/ "3:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

शचीव and अमात्य met old and witch-like MJ. MJ was a great adversary of ours, but respected our battle skills greatly. She remembered how we destroyed the stuff she hurled at us, despite certain nighmarish weapons in her possession that shook us. MJ now heard of our situation and suddenly reacted very unusually by deciding to work on our side. She deployed the vaiShNava weapons. However, the कृत्या was not countered by that and brushing it aside charged on. We temporarily neutralized the कृत्या's effects due to indra's aid. Now we shall wait and see what happens next.


