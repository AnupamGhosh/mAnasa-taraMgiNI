
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Fourier's spells blocked](https://manasataramgini.wordpress.com/2005/02/20/fouriers-spells-blocked/){rel="bookmark"} {#fouriers-spells-blocked .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 20, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/20/fouriers-spells-blocked/ "5:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Mr. Engineer of Mangalagrama (We shall call him Fourier after his fascination for illustrating Fourier transforms) had become a vairi of our clansmen on account of his daughter. His friend had controlled the bhuta that the board had and gave it to Fourier. He laid many an incantation on some our clansmen. J, was seized and broken up by his charms. J' mother was harrassed immensely by one of those spells. J's father, who rode roughshod on all and sundry with an air of superiority was shattered by Fourier's mantras. Our own PM was struck down to a pitiful condition by another of those spells. We too were scorched but by them, but indra and वरुण, सरस्वती and the maruts and the ashvinau, bore us aid and we overcame those charms finally. However, they had drained us so much that we were left vulnerable on the other fronts. We needed to invoke medholka so that we removed the vighnas that were preventing us from performing the रहोयागस् to attend to those fronts.

We had alluded to our lesser aunt H of the clan of the कौण्डिन्यस्. When me and my cousin had plied her planchette we heard some strange things from the visitor in the coin:\
[For A the elder: ]{style="font-style:italic;"}You will see gold at some distance but you cannot afford it. You might be able to buy copper though. In a pellucid glass case you will see a gold goblet. You will constantly pursue that but it will break and the goblet will vanish.\
[For A the younger:]{style="font-style:italic;"} You will also see gold at a distance. You can easily afford a lot of gold. But each time you touch it you will get a shock.

Fourier's spell became his shock. A the younger was the last of the victims.\
The glass case broke and A the elder is fast watching the goblet vanish as a physical entity.

We realized we can make things with copper that we cannot make with gold. Like a yogi we were standing above pain and pleasure, as चिन्नमस्ता we stood above कामा and rati, and continued to drink blood.


