
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Back in the same place](https://manasataramgini.wordpress.com/2005/08/17/back-in-the-same-place/){rel="bookmark"} {#back-in-the-same-place .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 17, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/17/back-in-the-same-place/ "6:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were set to return to the same battlefield where we had last year fought one of the most disastrous battles of our existence. This battlefield was worse than the पाषण्ड hill: there we had lost out right, but here we had won the battle and the booty we took home was poisoned. It gave us no pleasure to go there but we had to do our part in the larger struggle. When we went to tthat field last time our mind was filled with a desperate sense of anxiety over the war that was to be fought. We had carefully planned every move, and everything went as per plan except a strange event which was a actually the shakuna we had missed at that time. Our mantras fired in unison and it looked as though we were the winners. We did not know the monstrous consequences that were waiting for us like a रक्ष. The shachiva had seen something amiss but failed to alert us sufficiently.

This time we go with no such anxiety of the outcome of the war or that tense expectation of the unknown. We know everything now, and its all about the despair arising from everything that we now know. We only go there with the question how bad can it get? This time we are not even taking steps to proctect the baggage train as we did last time. It is just a charge with whatever few troops we can muster.


