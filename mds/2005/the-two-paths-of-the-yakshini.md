
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The two paths of the यक्षिणि](https://manasataramgini.wordpress.com/2005/07/23/the-two-paths-of-the-yakshini/){rel="bookmark"} {#the-two-paths-of-the-यकषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/23/the-two-paths-of-the-yakshini/ "5:27 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The paths of the यक्षिणि [![Posted by Picasa](https://i0.wp.com/photos1.blogger.com/pbp.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://picasa.google.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/mutraka.jpg){width="75%"}
```{=latex}
\end{center}
```



After having been pursued relentlessly by the कृत्या sent against us we reached the place known as the Dismal Hollows. There we entered The House and called up the यक्षिणि again. She came forth even as agni had shot up when praised by the hymns of the long gone fathers, भृगु, ushana काव्य, अपनवान and aurva. She created that shield that kept of the कृत्या from coming through. The she said that we shall take the flight along two paths. I asked if we may travel along the path where no mortal has coursed before. She said: Sure you will but that path is taken only at the biding of vaivasvata. Then I asked that we course along the backward path. She acquiesed and arose. But the कृत्या seized us and spread fear in our limbs. It looked as though we were simultaneously hit by the भ्रमण, भयञ्कर and stambhana prayogas borne by the कृत्या. We dived within the kavacha of the यक्षिणि. Suddenly we found ourselves above the क्षेत्र shown in the picture. This was a क्षेत्र where we have spent a relatively enormous amount of time from our first memories.

We flew with our hair blowing in the wind and coursed through the welkin like अरिष्टनेमी in the quest for soma. The यक्ष्णि pointed to us the क्षेत्रस् within the क्षेत्रस् and thus we toured them (as per the numbers in the picture):


 1.  Here was where we lived to win and lose many a battle aided by our शचीव and अमात्य and the great muni. While our formative years were spent here discovering for ourselves the tricks that would be needed when the gods were to take us on the path of glory in war.


 2.  We then coursed over the hillock of the वानार chief: there stood the son of keshari upon the hillock, the source of उल्लास for the kavis and place where we fought the दासं वर्णं- the brahma द्विषस्.


 3.  Then went to that spot where we acquired the manuscripts of the veda and the tantra which were in the grip of the mlecchas. There a dasyu had insulted the muni and he pronounced शाप on them and duely the dasyus perished in intercine conflict.


 4.  Then we went to the crowded क्षेत्र where we ranged in Ananda of the शृङ्गार rasa with that one whom the यक्षिणि brought back. Then she laughed horribly. We asked why but she moved on taking us along.


 5.  Then we hovered over the spot where we encountered the terrible ari who tried to place vighnas on our path using the hanumat prayoga. But they were back-hurled and he came begging to us for succour.


 6.  Then we flew by the small temple of the The God built by the Maharattas, which was lying in ruins in vicinity of city of sin.


 7.  Then we coursed to the great temple of विनायक where we entered the giant gold fish and coursed through the waters for a while. Then emerging out we made a great arc in the sky and flew north.


 8.  There we passed the temple of चण्डिका and saluted the great goddess who had borne us aid in that fierce conflict with the mass of भ्रातृव्यस्.


 9.  Then we coursed on to the hill of the वेतालस् and saw the मटृ of कुमार, वेतालजननी, and her frightening hordes. As we passed, the वेतालस् remained utter silent and we bowed in silence to the goddess.


 10.  Then we coursed to the shores of the great lake where we had observed birds in our youth with the fallen brahmin boy. It reminded us of the great 9 kilometer walk with our bounty of shells. On the way we were accosted by the kidnappers, whom we out-ran in the days of our greater swiftness. We became snails and swam in the murky waters and found that the lake had shrunk from its hey days.


 11.  Then we flew that क्षेत्र where we had seized the greatest of our vidyas from the दास दुष्टतम who hid it from us. By the महामन्त्र of skanda we had rebuffed the दास in the black days.

We then coursed to the house of the pretty lady who had brought us joy for 15 years. We do not mark her क्षेत्र at her request. Her mother staring at the मण्डल in front of her summoned the यक्षिणि. They exchanged mudras. Then the यक्षिणि spoke: look at this puruSha who flies beside me, the fates of him and your दुहिता have now inverted. He dwells in naraka and I have briefly lifted him out of it. Your दुहिता dwells in her svarga. The arrow of karma cannot be diverted. My सखी's mother spoke: "It is indeed tragic. We were planning that magic that might have been like a deva कार्य." The यक्षिणि spoke: "When hanumant was born he sought the sun but hammered by the vajra of indra he fell lifeless. Though revived and endowed with tremendous prowess, he threw a भार्गव down from a tree and tormented him. Struck by the atharvan spell he lost knowledge of his powers". Learn ye the lesson from this if contemplating to soar as hanumant did.

We vanished.


