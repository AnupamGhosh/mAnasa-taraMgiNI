
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [काम कला](https://manasataramgini.wordpress.com/2005/09/08/kama-kala/){rel="bookmark"} {#कम-कल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 8, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/08/kama-kala/ "5:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[मुखं बिन्दुं कृत्वा कुच युगम्-अधस्-तस्य तदधो हकारार्धं धायेद् यो हर-महिषि ते मन्मथ-कलां ।]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[\
स सद्यः संक्षोभं नयति वनिता इत्य्-अति-लघु त्रिलोकीम् अप्य्-आशु भ्रमयति रवीन्दु-स्तन-युगां ॥]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}

Making the bindu to be the face, what is beneath to be the breasts, and what is still underneath one half of the syllable ha, one meditates on your manmatha कला, oh she queen of hara.\
One who does so very easily excites girls; he very soon bewitches the Triple-world with the sun and moon as her breasts.

The realization of कामकला follows from the bijas of the yantra associated with the mantra: 4 ह्रींस् enclosed in a circle.

The mAya bIja forms thus:

 1.  Im- the gupta महासारस्वत bIja which has the bindu forming the face and the two semi-circular curves the breasts.

 2.  r- the consonant of fire from the 3rd eye on the forehead.

 3.  h- the हकारर्धं, the lower half of ha which is equivalent of the yoni vowel e. Thus it is the yoni, the root of the manmatha कला.


