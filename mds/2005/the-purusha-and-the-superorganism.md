
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The puruSha and the superorganism](https://manasataramgini.wordpress.com/2005/09/16/the-purusha-and-the-superorganism/){rel="bookmark"} {#the-purusha-and-the-superorganism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 16, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/16/the-purusha-and-the-superorganism/ "5:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[यत् पुरुषं व्यदधुः कतिधा व्यकल्पयन् ।]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[मुखं किमस्य कौ बाहू का ऊरू पादा उच्येते ॥]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[ब्राह्मणो।अस्य मुखमासीद् बाहू राजन्यः कृतः ।]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[ऊरूतदस्य यद् वैश्यः पद्भ्यां शूद्रो अजायत ॥\
[[\
]{style="font-style:italic;"}]{style="font-weight:bold;"}]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}Recently the ecologists Wilson and Hoeldobler claimed that group selection and not kin selection was the major force in the origin of eusociality. Eusociality fascinates us because in many ways it parallels our own societies though we are not eusocial ourselves. So when W and H claimed that eusociality is more likely an outcome by group selection rather than kin selection (as the standard model posits), I was puzzled and wondered what its full implications were. In fact W and H go as far as to suggest that the genetic relatedness seen in ant and bee colonies is a consequence rather than cause of eusociality. They further mused that kin selection may actually be disruptive rather than favorable to the emergence of eusociality because it would result in the formation of kin cliques that would war against other such cliques and reduce cohesion. So eusociality is seen in terms of group selection with all organisms of the eusocial group working as a superorganism and it is the superorganism that propagates itself.

This group selection and the superorganism concepts got me wondering about the theoretical social system that describes such scenarios. Theoretically, genetic caste formation is central to origin of eusocial behavior. Such a theoretical scheme that was conceived for human society was the puruSha concept of the Hindus. The first great Indo-Aryan state in India was that of the पूरुस्, led by their dominant tribe the भारतस्. Upon founding their state they provide it with a constituition and a theoretical frame work which was first expressed in the great puruSha सूक्तं (RV 10.90). This was expanded further by the authors of the dharma शास्त्र who built upon the concept. While the macranthropic puruSha motif is commonly used by Indo-Europeans and other bronze age civilizations, in India we encounter it as an unique social theory:


 1.  The great hymn describes the pUru state as a superorganism with the head made of the ब्राह्मण, the hands with the kShatriya, the middle body by the vaishya and the feet by the shudra.


 2.  The four वर्णस् (castes) which comprise the superorganism the puruSha are produced from a single "genetic blueprint" through differential "expression" or particular गुणस् (as elucidated in the भृगु स्मृति).

Thus one may posit that in deriving a social theory, the Indo-Aryans had actually converged on a formulation that in its ideal formed resembled a eusocial society derived through strong group cooperation. Kinship actually is hardly mentioned in the descriptions of the puruSha. This I find pretty remarkable- almost as though given a chance we would lapse into a eusocial condition. I find the implications of this pretty interesting in terms of the origin of eusociality.\
[[[]{style="font-style:italic;"}]{style="font-weight:bold;"}]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}


