
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कृत्यास्त्र strike](https://manasataramgini.wordpress.com/2005/06/08/the-krityastra-strike/){rel="bookmark"} {#the-कतयसतर-strike .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 8, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/08/the-krityastra-strike/ "5:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We saw that कृत्यास्त्र course towards us and hit us even before we could do much. The muni's efforts were blown to bits, the अमात्य was tossed aside like a twig in a storm, the shachiva simply ran away from the scene. The effects are rather sapping- it just wears you out, and tries to eat out and completely destroy your mantra bala. For sometime we did not know what hit us, as though the deadly धूमावती had been sent to devour us. Our kavacha seemed to be fragmented to bits. Our energy seemed to flow out of us like three rivers. It was indeed the महायुद्ध as we had feared in the past.


