
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The आम्नायस् of the चिञ्चिनी-mata-sAra-sammuchaya](https://manasataramgini.wordpress.com/2005/04/24/the-amnayas-of-the-chinchini-mata-sara-sammuchaya/){rel="bookmark"} {#the-आमनयस-of-the-चञचन-mata-sara-sammuchaya .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 24, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/24/the-amnayas-of-the-chinchini-mata-sara-sammuchaya/ "6:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We worship चिञ्चिनी under the large tamrind tree from whom the sea of spanda has emerged.

There are 4 आम्नायस्:\
[पूर्वाम्नाय:]{style="font-weight:bold;font-style:italic;"} The mukhya shaktI, the great goddess of this आम्नाय is कुलेश्वरी who manifests as त्रिभुवनेश्वरी along with परमान्दभैरव. From this face emerged the trika tantras with खगेन्द्रनाथ as the first of the kaula adept in this lineage. It was temporarily destroyed by कार्त्तिकेय.

[दक्षिनाम्नाय: ]{style="font-weight:bold;font-style:italic;"}The great goddess of this stream is कामेश्वरी- she is pleasing to the eye as the young sun, yet brilliant as hundred million flashes of lightning that strike the consciousness of the adept as the rahasyas of her mantras are realized. She is full of काम and manifests as कुलयोगिनी with a slim waist and is conjoined with kaulesha. The 16 नित्यास् surround her. The great god कुमार born of her coitus with kaulesha taught this divine विद्या to krodhamuni of the atri clan. The श्री kula tantras emerged from this आम्नाय.

[उत्तराम्नाय:]{style="font-weight:bold;font-style:italic;"} The great goddess of this आम्नाया is कालेश्वरी. She emerges as the sun- the भानवीकौलिनी, which shines in the center of the sacrificial hearth of the great suvar loka of the महासमुद्र of shiva. The great mantra consisting of 64 bhairava yonis arises and dissolves in the spanda of this ocean. This विद्या as it unfolds takes the siddha through the incomparable bliss of experiencing the 12-fold dawning of the कालिस् on the "Sky of Consciousness"- this is the परोल्लास. This was first seen by क्रोधराज, the irrascible atri and through the lineage of female teachers went down to the hoary tantric निष्क्रियानन्द. The tantras of कालि kula emerge from this face.

Regarding this a tale is narrated: There was a muni named शीलाचिति in the शिवपीठ of श्रीशैल. His son was a siddha named विद्यानन्द who looked like a शाबर tribesman. He lived in श्मशान-s and performed nightly वीरसाधन by meditating on his chakras. He went to the mountain range to the north of श्रीशैल and there in a gold mine [*] dear to the gods he worshiped, desirous freedom. A tantric निष्क्रियानन्द was pleased with him and initiated him into the कालि krama (CMSS 7). He then transmitted the tradition of कालि krama in भारतवर्ष.

  -  "This cave of gold" might be identified as either one of 3 caverns recognized today: दत्तात्रेय cave, "Akkamahadevi" cave or उमामहेश्वर cave.

[पश्चिमाम्नाय: ]{style="font-weight:bold;font-style:italic;"}The great goddess of this stream is वक्रेश्वरी. She resides contracted and coiled as कुण्डलिनी. She is seen with her mate महाभैरव or kubjeshvara. From her seat in the महायोनी she procedes to through the three vertices of the triangle that contain पूर्णगिरि, जालन्धर and कामrUpa and finally arrives at the ओडियानपीठ at the center. Here she resides in union with the लिङ्ग whose nature is bliss and whose bindu is the "Sky of consciousness". From this face emerges the चिञ्चिनीशास्त्र or the कुब्जिका tantras that was first received by the ancient tantric वृक्षनाथ under the tamarind tree from the founding teacher श्रिनाथ. In the form of the transmitter of the चिञ्चिनीशास्त्र she is worshiped as देवी चिङ्चिनी


