
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Planetary nebula hunt](https://manasataramgini.wordpress.com/2005/09/06/the-planetary-nebula-hunt/){rel="bookmark"} {#the-planetary-nebula-hunt .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 6, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/06/the-planetary-nebula-hunt/ "6:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A great hunt for the planetary nebulae was successfully conducted this night. After an initial survey of bright globulars we pursued the planetary nebulae with a vengence.


 1.  NGC 6210 in Hercules: Small faint disk. About 6500 light years away. Discovered by Wilhelm [[[]{style="font-size:85%;"}]{style="font-family:Arial,Helvetica;"}]{style="font-family:Arial,Helvetica;"}Struve, successor of Herschel in 1827. Hubble has revealed it to contain two shells of gas.\
[http://antwrp.gsfc.nasa.gov/apod/ap981028.html](http://antwrp.gsfc.nasa.gov/apod/ap981028.html){rel="nofollow"}


 2.  NGC 6543 in Draco: Medium bright disk. Discovered by Wilhelm Herschel in 1786 is very close to the ecliptic north pole. Also known as the Cat's eye nebula.\
[http://hubblesite.org/newscenter/newsdesk/archive/releases/1995/01/](http://hubblesite.org/newscenter/newsdesk/archive/releases/1995/01/){rel="nofollow"}


 3.  NGC 6826: The Blinking Nebula in Cygnus. About 2200 light years away. Discovered by Wilhelm Herschel in 1793. It is a bright bluish object, larger than NGC 6543\
[http://hubblesite.org/newscenter/newsdesk/archive/releases/1997/38/image/d](http://hubblesite.org/newscenter/newsdesk/archive/releases/1997/38/image/d){rel="nofollow"}


 4.  NGC 7293: The Helix Nebula in Aquarius. Discovered by Karl Harding, student of Karl Gauss while working with him on the production of [Atlas novus coelestis]{style="font-weight:bold;font-style:italic;"} (a map of 120,000 stars) in 1824. The nearest planetary nebula to earth. Very large and faint.\
[http://hubblesite.org/newscenter/newsdesk/archive/releases/2003/11/](http://hubblesite.org/newscenter/newsdesk/archive/releases/2003/11/){rel="nofollow"}


 5.  NGC 7009: The Saturn Nebula in Aquarius. Discovered by Wilhelm Herschel in 1782 shortly after he began his great hunt for new deep sky objects. Roughly abour 2400-2900 light years away.\
[http://antwrp.gsfc.nasa.gov/apod/ap971230.html](http://antwrp.gsfc.nasa.gov/apod/ap971230.html){rel="nofollow"}


 6.  M27: Dumbell Nebula in Vupecula. Discovered by Messier in 1764. 100o light years away. Bright glorious object. Best planetary in the sky\
[http://antwrp.gsfc.nasa.gov/apod/ap030113.html](http://antwrp.gsfc.nasa.gov/apod/ap030113.html){rel="nofollow"}


 7.  M57: RIng Nebula of Lyra. 2000 light years away. Discovered by French astronomer Antoine Darquier de Pellepoix in 1779, while working on the orbit of Bode's comet of 1779. Merely added by Messier to his catalog. Bright glorious object. Second best planetary in the sky.\
[http://antwrp.gsfc.nasa.gov/apod/ap010729.html](http://antwrp.gsfc.nasa.gov/apod/ap010729.html){rel="nofollow"}

[](http://www.google.com/url?sa=t&ct=res&cd=1&url=http%3A//www.seds.org/messier/xtra/ngc/n7293.html&ei=1zcdQ_a1Dsr0aPeH_cUM)


