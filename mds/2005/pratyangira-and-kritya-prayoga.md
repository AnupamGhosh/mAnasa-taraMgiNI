
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [प्रत्यङ्गिरा and कृत्या prayoga](https://manasataramgini.wordpress.com/2005/02/20/pratyangira-and-kritya-prayoga/){rel="bookmark"} {#परतयङगर-and-कतय-prayoga .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 20, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/20/pratyangira-and-kritya-prayoga/ "6:11 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There was more than one discussion recently as to whether the tantric goddess प्रत्यङ्गिरा was associated with a कृत्या prayoga. The atharvan text clearly deploys the सूक्त of प्रतिचीन आङ्गिरस as a counter charm against कृत्यास्. An extension of the deployment, similar in spirit is seen in the ऋग् विधान of shaunaka. MS Bhat titled his study on the ऋग्विधान as "Vedic Tantrism". This title is somewhat appropriate because the विधान appears to represent intermediate condition through which the evolution of vedic prayogas into tantric prayogas occured. However, Bhat does not pursue the whole evolutionary course. While I have traced much of this evolutionary pathway, the lack of time prevents me from expounding this in a book-length scholarly form. The प्रत्यङ्गिर सूक्तं and subsequently its first ऋक् provide an illustration of this evolutionary process.

There are two extant living paddhatis that I know, and others whose practioners may exist but are unknown to me. The first paddhati is an evolute of the old Atharvavedic ritual of countering abhichara. Here, much of the atharvanic rite, in a relatively unaltered form, using both the प्रतिचीन and वामदेव रक्षोहा agni सूक्तस्, has been completely subsumed within a श्रीविद्या rite directed towards the invocation of the goddess प्रत्यङ्गिर. This procedure is the प्रत्यङ्गिरा tantra paddhati of the श्रीकुल tantras and in a more degenerate form their bauddha evolutes.

The second prayoga provides a more tangible hint between the प्रत्यङ्गिर rite and the कृत्य prayoga. This is rite is given by the प्रपञ्चसार tantra (a tantra narrated by deva विष्णु to brahmA, edited by शंकर भगवत्पाद). This rite recommends fire oblations of the अपामार्ग plant mixed with goat ghee. This offers an important direct link to the atharvan tradition, because the अपामार्ग plant is associated with कृत्य destruction as alluded to in the hymn to the अपामार्ग plant (shaunaka saMhita of AV, सूक्त 7.65). However, the प्रपञ्चसार rite appears bear faint hints of a कृत्या prayoga itself:

yAM कल्पयन्तीत्यस्य ब्रह्मानुष्तुप् प्रत्यङ्गिरस ऋष्याद्याः /\
oM बीजं / हॄं शक्तिः /\
अष्ट chatush-chatush-chatush-ShaD-भिरक्षरैरङ्गानि /\
bhru madhya nayanadvaya gala बाहुद्वय हृन्नाभि पार्श्वद्वय कटि-द्वयान्घुपाद-द्वयेषु न्यासः /

खड्ग-चर्मधरा कृष्णा मुक्तकेशी दिगम्बरा सदंष्ट्रा sa सर्पाभरणा भक्षितारि ध्येया //

That is: of the formula yAM kalpayanti the deity brahmA, the meter is अनुष्टुभ्, the composer is प्रतिचीन the अञ्गिरस्. oM is the seed, हृं is the shakti. 8, 4, 4, 4, 6 syllables in in order are taken from the formula and used in अङ्गन्यास touching between the brows, the two eyes, throat, hands, heart, navel, ribs on two sides, two hips and the feet.

The deity is meditated as: She bears a sword and shield, is of black color, her locks are let loose, she is space-clad and with sabre-teeth and her ornaments are snakes. She eats away the enemies. \[This description of the deity tallies very well with the classical description of कृत्यास् in texts like the shiva or bhAgavata पुराण]

Then prescribed oblations are made reciting just 108 times:\
yAM kalpayanti no .अरयः क्रुरां कृत्यां vadhumiva /\
tAM ब्रह्मणा .अपनिर्नुद्मः pratyak कर्तारमृच्छतु //

This is the पैप्पलाद version of the formula

Then curd oblations are made to indra with the formula:\
yo me पूर्व गतः पाप्मा पावकेनेह कर्मणा इन्द्रस्त्वं devo rAjA जंभयतु स्तंभयतु mohayatu vashayatu मारयतु नाशयतु बलिं tasmai प्रयच्छतु कृतं mama शुभं mama शिवं mama शान्तिः स्वस्त्ययनं mama //

That is: In the eastern direction indra is invoked with a bali offering to devour, paralyze, delude, capture, slay, ruin the evil-doer and implored to bring good, auspicious and peaceful things to the performer and provide him safety.

Likewise the deities agni, yama, निरृति, वरुण, वायु, soma, rudra, and surya ( in lower, upper, nether and heavenly quarters) are invoked with balis and similar implored to do away with the foes.


