
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [यक्षिणि and ऋक्षकर्णि ... the journey continues](https://manasataramgini.wordpress.com/2005/10/15/yakshini-and-rikshakarni-the-journey-continues/){rel="bookmark"} {#यकषण-and-ऋकषकरण-the-journey-continues .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 15, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/15/yakshini-and-rikshakarni-the-journey-continues/ "4:46 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having been trapped with the घोरप्रकृति we sought to escape on that rainy day. After the passage through the कालछिद्रस् and the many paths had been sealed there was a single path winding through naraka. The old bystanders such as Dom, Vidrum, Damar and Blackie were all faintly heard from behind an oppressive screen of darkness. Some were crying as though being tortured by a turushka, others were laughing uncontrollably, as though they had imbibed bhang, others were yelling as though they were jostling in काम. On the sides of the path were rotting corpses and dermestid beetles making short work of them. There were skulls and femurs scattered around. There were jackals and hyaenas yelling out their moans. We kept walking like a पाशुपत had shut off his senses to the surroundings. There we saw a corpse of the most beautiful लोलनयना and then we passed the carcass of श्वेतमोहिनी which was being consumed by a वराह. Then we saw a crooked stump on which sat a वेताल which laughed horribly. Shortly after that the great gandharva विश्वावसु passed our path. He pointed to the crooked stump and laughed and flew away.

The we saw a slight clearing. There we stopped and saw the awful ऋक्षकर्णि. We invoked her with the mighty mantra and said that blood was her offering. She was of nightly hue and had four arms. Her hair was tied up into an erect knot and she had 3 blazing eyes with the ears of a bear. Her breasts were rising and she caused all kinds of rasas to tingle the body. Then she extended her palm and from that arose a यक्षिणि. In the embrace of the यक्षिणि we flew to the higher realms and briefly enjoyed the bliss. Then we were dropped back to the paths of naraka.

We continued to walk on the path with droplets of the blood rain showering. We saw the snaking path to the palace off परीक्षित्. We veered of the original path we were taking and started walking towards the bhArata's realms. We soon found ourselves climbing up the palace of परीक्षित् and reached that room in the highest level where the rAja had been killed by the snake. We occupied that realm and looked out of the window and saw a serene scenery of a sundara nagara there. But from somewhere we heard the low moans of a वेताल, though we were unable to see it. We then sighted the संजय netra and the sword of सात्यकी lying in one of the rooms.


