
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [South Indian Hindu legal authorities of the middle period](https://manasataramgini.wordpress.com/2005/01/05/south-indian-hindu-legal-authorities-of-the-middle-period/){rel="bookmark"} {#south-indian-hindu-legal-authorities-of-the-middle-period .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 5, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/01/05/south-indian-hindu-legal-authorities-of-the-middle-period/ "5:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}


  - 700 CE बालक्रीडा a commentary on the याग़्ण्य़वाल्क्य स्मृति was composed by विश्वरूप in Shringagiri in Karnataka.

  - ?700 CE भारुचि wrote commentaries on the विष्णु dharmashastra and the मनुस्मृति.

  - 1120 CE mitAksharA was written by विग़्ण्य़ानेश्वर who was the prime minister of the चालुक्य king विक्रमादित्य-04. It was a definitive work on Hindu law that gained currency throughout much of India and continues to be authoritative to this date. It was based on the याग़्ण्य़वल्क्य dharmaशास्त्र. He also wrote the work Asaucha dashaka or a brief set of sholkas on ritual pollution.

  - 1120 CE The shilahara king अपरादित्य, who ruled over Goa and the Konkans wrote a great compendium on Hindu law entitled the याग़्ण्य़वल्क्य स्मृति भाष्य. Though it traces it antecedents to याग़्ण्य़वल्क्य's law book, it is a rather independent volume of law in that era. अपरादित्य also composed a work on nyAya atomism and logic: the न्यायसार टीक.

  - 1150 CE नारायण shAstri, a disciple of विग़्ण्य़ानेश्वर composed a compendium on civil law entitled the व्य्वहार शिरोमणि.

  - 1200 devana भट्ट in Karnataka wrote the extensive digest of various स्मृतिस्: स्मृति chandrika

  - 1260s paNDita haradatta in Tamil Nad wrote his famous commentaries on dharma सूत्रस् of Apastamba and gautama. Some authorities believe he lived in the 800s of CE and also authored a work on the grammar of पाणिनि. They may have been distinct

  - 1260 हेमाद्रि, the prime minister of the यादव king of Maharashtra, महादेव यादव, wrote a great Hindu encyclopedia: the chaturvarga चिन्तामणि. It is one of the most voluminous Sanskrit works ever composed and covers at length topics like vratas, dAna, तीर्थ, moksha, परिशेष, प्रायश्चित्त and व्यवहार. All aspects of Hindu law were encompassed in the encyclopedia along with other topics like geography and ritual. On legal aspects he often mentions devana भट्ट.

  - late 1200s वरदराज in Tamil Nad wrote the व्य्वहार निर्णय which tries to interpret the legal principles based on मीमास.

  - 1350s माधव विद्यारण्य, minister of the Vijayanagara empire wrote the पराशर माधवीय, which followed at its core the law book of पराशर. However, it independently developed several aspects that were not covered by the hoary स्मृति.

  - 1350s His brother, सायण, wrote the puruShArtha सुधानिधि which covers several aspects relating to personal law.

  - 1375 vishveshvara wrote a text called mitAkShara subodhini that develops on the earlier work of विग़्ण्य़ानेश्वर.

  - 1500 harita वेन्कटाचार्य, a श्रिवैष्णव scholar composed the स्मृति रत्नाकर which survives to this date amongst श्रिवैष्णव brahmins.

  - 1510 paNDita dalapati wrote the voluminous text नृसिम्ह प्रसाद, a work in 12 chapters that covers all aspects of Hindu civil and ritual law. dalapati was an effective politician who had infiltrated the court of the Nizam Shah and managed to induce the Sultan to allow Hindus to be ruled independent of the Shariat under their own legal code.

  - 1510 लक्ष्मीधर, the famed Tantrik from Orissa wrote the sarasvati विलास, which systematically deals with the contradictions in अपरादित्य, विग़्ण्य़ानेश्वर and भारुचि's work and attempts a conciliatory synthesis.

  - 1600 वैद्यनाथ दीक्षित in Tamil Nad wrote the स्मृति मुक्ताफल which is considered as an authoritative text on legal issues by the स्मार्त brahmins of Tamil Nad.

This brief survey shows that Hindu law was not static as is commonly painted in Leftist and secularist history-writing on India. While the legal issues were based on the earlier legal tradition comprising of dharma सूत्रस् followed by the स्मृतिस् or the dharma शास्त्र, they typically diverged from their precedents. The divergence allowed discussion of new legal issues uncovered by the earlier legal sources and importantly account for the changing times. Attempts were made to compile legal digests by drawing out material from diverse earlier authorities, as well as develop new systems by recombination and reconciliation of earlier sources. In several cases, new concepts may be slipped in with different degrees of conservative or reformist opinion.

However, it should be kept in mind that many of the issues are mainly relevant to the Arya वर्णस् and their pre-occupations. It is clear that the fourth वर्ण was sort outside of the consideration of many of the points discussed in the स्मृति and hence their complaints about enforcement of oppressive laws by the Arya वर्णस् are rather unjustified on occasions.


