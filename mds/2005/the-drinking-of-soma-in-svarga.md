
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The drinking of soma in svarga](https://manasataramgini.wordpress.com/2005/11/04/the-drinking-of-soma-in-svarga/){rel="bookmark"} {#the-drinking-of-soma-in-svarga .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 4, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/04/the-drinking-of-soma-in-svarga/ "7:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Briefly we were awarded the freedom by the grace of the great indra, who is greater than all (विश्वस्माद् indra उत्तरः). We scored two great victories against the dasyus and brought back booty even as our empire was under fierce assaults. Verily indra bore us aid as we locked in combat with two dasyus and we overthrew them after the vajra entered our weapons and shattered theirs. We also made a strong thrust against the third front to bring the fort within striking distance. The spear of skanda relieved us from the पूतना ग्राहि and we were placed in the midst of our rejoicing bands. We quaffed soma effused with thickened sweet milk, ate choice spreads and delighted in svarga as द्युतान in the days of yore. But we were told, like for ययाति, the elevation to svargo लोकः was not a permanent feature and we could be sent back to the midst of the राक्शसीस् anytime. But then all have stated that one should enjoy svarga when you can. We looked forward to the performance of the great धूर्त kalpa with the 6th day of the moon to please the सेनानि.

We were asked to consider two things: 1) For the past 5 days before today swarms of crows filled the trees in our vicinity at might and swirled around making an enormous racket. 2) We were asked to meditate upon the killing of the demon, the giant भीमरथ by विष्णु.


