
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The adaptive radiation of avian and para-avian clades](https://manasataramgini.wordpress.com/2005/02/26/the-adaptive-radiation-of-avian-and-para-avian-clades/){rel="bookmark"} {#the-adaptive-radiation-of-avian-and-para-avian-clades .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/26/the-adaptive-radiation-of-avian-and-para-avian-clades/ "6:14 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Adaptive diversification of avian and para-avian clades. [![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/early_dinobirds.jpg){width="75%"}
```{=latex}
\end{center}
```



Left: [[Epidendrosaurus ]{style="font-style:italic;"}and [Microraptor]{style="font-style:italic;"}]{style="font-weight:bold;"}[; ]{style="font-style:italic;"}Right: [A sinornithosaur (wonderful artwork of L.Rey).]{style="font-weight:bold;"}

The recent dinosaurian finds suggests how adaptive radiation in birds and their closest sister clades of maniraptoran dinosaurs proceeded along very distinctive lines. Of the maniraptoran dinosaurs we see considerable ecological diversity in the most primitive members of the avian clade. We have [Archaeopteryx]{style="font-style:italic;"}, which appears to be a generalized primitive bird, [Rahonavis]{style="font-style:italic;"}, which was clearly a predator probably retaining the ancestral life-style of the entire avian-paravian clade, and finally [Shenzhouraptor]{style="font-style:italic;"}, which appears to have evolved a seed-eating life-style. Another taxon which is a close sister-group of the basal avians is the enigmatic [Epidendrosaurus]{style="font-style:italic;"}, which has a highly elongated manual digit II in contrast to the elongation of manual digit III in other related dinosaurs like [Archaeopteryx ]{style="font-style:italic;"}and the deinonychosaurs. [Epidendrosaurus ]{style="font-style:italic;"}also displays many other adaptations for an arboreal life style like elongate pedal digits for clutching branches. In contrast to these groups their sister group the deinonychosaurs despite their radiation in the Cretaceous, maintained relatively conservative lifestyles as predators in the small to middle range. Some may have been more arboreal predators with greater or lesser flight abilities such as [Microraptor ]{style="font-style:italic;"}and [Sinornithosaurus]{style="font-style:italic;"}, while others where small terrestrial predators like Velociraptor, and yet others like Unenlagia, Achillobator and Utahraptor were fully terrestrial middle range predators. It appears that the pre-occupation of the large-bodied predator niches by the larger tetanurans, ceratosaurs and abelisaurs may have kept the avian and para-avian clades largely out of this niche.

From the distribution of features in the early avians, Epidendrosaurus, and deinonychosaurs, one may conclude that the common ancestor of these groups was definitely an arboreal maniraptoran with limited ability to fly or glide using feathers. From such a base one lineage (the Epidendrosaurus) probably specialized as brachiating tree-dwelling micropredators and a consequence elongation of digit II. The flapping hand movement used by the predatory ancestor was probably used in the initial stages for wing-assisted climbing of inclines or trees. This was then used for the beginings of flight. The avian lineage specialized in flight and probably this favored smaller size and exploitation of much wider ecological niches. The deinonychosaurs remained to a large-extant in the ancestral state of this assemblage, with probable multiple returns to terrestrial predatory niches.[ ]{style="font-size:-1px;"}


