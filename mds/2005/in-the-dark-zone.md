
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the dark zone](https://manasataramgini.wordpress.com/2005/08/14/in-the-dark-zone/){rel="bookmark"} {#in-the-dark-zone .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 14, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/14/in-the-dark-zone/ "9:11 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We sat on the edge of the crater of sorrow, defeat and destruction, which was much like the crater of a great volcano, with the inferal interiors obscured by pumice and granite. We saw many यक्षीस्, apsaras, पिशाचिस् and राक्षसिस् pass by in silence. We were haunted by the memories of the fierce battle at the पाषण्ड hill in our native land in 2003. It was a demon-haunted world into which we had slipped after that battle. The chiefs assembled for the kuriltai, but we could not go there because we wanted to avoid any further pain of the same prying questions. We summoned the राक्षसस् plaguing the internal world and asked them what was the cause of this dismal darkness. One of them replied: "it was daNDa yuddha which was your undoing." The other replied: "It does not matter whether you failed to use दण्डयुद्ध in the battle or not. It was all already destined that you would slip that way." Yet another replied that the rAkShasa मायावि was the cause. We were drowning in this cacophonies of राक्षसस् shouting in the demon haunted world.


