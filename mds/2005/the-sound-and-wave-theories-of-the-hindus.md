
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The sound and wave theories of the Hindus](https://manasataramgini.wordpress.com/2005/11/11/the-sound-and-wave-theories-of-the-hindus/){rel="bookmark"} {#the-sound-and-wave-theories-of-the-hindus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 11, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/11/the-sound-and-wave-theories-of-the-hindus/ "6:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The achievements early Hindu science are hardly known in an objective form. They are either being neglected due to the efforts of the leftist Anti-Hindu academics from the West and India or exaggerated by uneducated Hindus. One of the great Hindu theories of which we get some limited glimpses are the ideas on sound propagation which are discussed at length in the मीमास text of shabara स्वामिन्. In short they include the following idea for the transmission of sound:

 1.  नाद or sound is a property of वायु. 2) The sound movements are constituted by a series of air movements of the nature of a current termed वायुसन्तान. 3) The sound causes the वायु परमाणुस् to successively undergo संयोग विभाग that s conjunctions and disjunctions resulting in spreading of the original impact as sound.

The वैशेषिक scholars like प्रशस्त्पाद and the later श्रीधर introduced the concept of the sound wave. He explicity compares sound to the waves in water. They posited that at anytime sound forms a circle and spreads as circles or spherical shells from the source: यथा जलवीच्या tad avyavahite deshe वीच्यन्तरम् उपजायते tato.apanyat tato.apayaditpanena क्रमेण वीचि सन्तानो bhavati tatha क्रमेण shabda सन्तानो bhavati /


