
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Hayastanika, सांख्य, Plato etc.](https://manasataramgini.wordpress.com/2005/09/26/hayastanika-samkhya-plato-etc/){rel="bookmark"} {#hayastanika-सखय-plato-etc. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/26/hayastanika-samkhya-plato-etc/ "3:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Hayastanika is known as सर्वप्रिया and महासुरापिपाना. Thus, we mused over the turn of the chakra known as the संवत्सर. The wheel turned and शरदः had come resulting in the dismal air that hangs over us. We decided to stay away from ललामी and enjoy the pleasant company of Kappa, which was like a madhu-चषक in itself. We saw the movie सुखावति of Campbell and then began discussing a hero with a 1000 faces. Sparked by the images of Campbell's movie we had an interesting discourse on the further similarities between the fundamentals of Greek thought and their Hindu counterparts. An important parallel is that doxa and mAyA. This doxa is represented as a female deity, Ananke, just as the Hindus represent mAyA by the great trans-functional goddess. Her trans-functionality across the categories of the gods in the Hindu world is a clear inheritence from the proto-Indo-European world. Nextly, in the Greek world she creates Eros first amongst the gods and also causes the "maithuna" of males and females. In the Hindu world this deity, कामेश्वरी, regenerates काम who can conquer rudra and drives the maithuna of organisms. Thus, these deeper connections, which are at the heart of the tantra, was existent in both the worlds. Plato and Socrates drew their inspiration from Parmenides and Heraclitus. The later was an equivalent of the Vedantic thinkers of the Hindu world. His book was dedicated as an offering in the temple of the great goddess Artemis and late in his life, Heraclitus renounced the world and lived as a mountain hermit on wild grasses and plants. By synthesizing the ideas of Heraclitus and Parmenides, the Socratic school under Plato proposed an evolutionary model of the world where the primal "One" and the "Other" spin out the world through a series of emanations. These primary emanations are in the Greek fashion described as natural numbers- from 1 to 10 or their inner core 1-4 (1+2+3+4=10). This four called the tetractys is believed to represent the following: 1= the point transcending sense perception. 2= line, the emergence of subject and object. 3= triangle, the first two dimensional entity. 3= tetrahedron the first 3D entity representing space. Thus, the tetractys symbolizes the emergence of the spatial physical world from an imperceivable unity of the point. Plato declared that the pure unitive knowledge associated with the point was the highest wisdom, whereas the higher emanations associated with sense perception are lower knowledge or ignorance. Theophrastus, clarifies that the one acting upon the "Other (also termed the indefinite dyad by Pythagoras)" produce the conceptual "Ideas". The "Ideas" then acting upon the Other or the indefinite dyad produce the "Particulars". The parallels with सांख्य and vedANta can hardly be escaped. The puruSha and the प्रक्रृति standing in for the One and the Other (or Doxa of Parmenides).

The important point is that in the Indian सांख्यन् reconstruction there are two distinct advances over the primitive Greek models, even if they are conceptually similar. The emergence of तन्मात्रस् and the atomic particles (परमाणुस् of the भूतस्) from प्रकृति replace the Idea and Particulars. Instead of using an explicit geometric system like the Greek Tetractys we note that the Hindus instead generated an ontological series of the Sanskrit Syllables (the ंअतृकस्). Thus, in the tantras (e.g. the tantric work of abhinavagupta- the तन्त्रालोक) we see the emergence of the universe being described as the periodic syllables of the Sanskrit alphabet, arranged as though in a periodic table of elements. The important point to note is that the Hindus did have a geometric version too, which however expresses itself mainly in the tantras alone in the form of the yantras such as the shri-chakra. Another key to understanding these parallels is the observation that पाणिनि and Euclid (in reality Eudoxus ) were essentially Hindu and Greek counterparts of each other, using very similar devices in language and geometry respectively. Not surprising all major encyclopedic Hindu tantras (e.g प्रपञ्चसार, शारदतिलकं, rudra यामल, लक्ष्मि tantra, वामकेश्वर tantra) begin by expounding a version of the सांख्य thought, especially in connection to the तन्मात्र and syllable emanations.

Hence it is clear that the Greek and Hindu were connected with a "tantric element" right from the beginning. What was this precursor, and where did it come from? I believe this is one of the most important questions in comparative philosophy.


