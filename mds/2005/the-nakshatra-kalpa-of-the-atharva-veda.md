
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The nakshatra kalpa of the Atharva veda](https://manasataramgini.wordpress.com/2005/11/27/the-nakshatra-kalpa-of-the-atharva-veda/){rel="bookmark"} {#the-nakshatra-kalpa-of-the-atharva-veda .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/27/the-nakshatra-kalpa-of-the-atharva-veda/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The nakshatra kalpa of the Atharva veda that is considered the first परिशिष्ट of the AV adduces a ऋशि in addition to the tutelary deity to each nakshatra. These connections appears to have emerged much later than the devata-nakshtra connection, but may be of interest to investigate what these ऋशिस् meant. The list is:\
asterism |seer |god\
kR\^ittika |agnivesha |agni\
rohini |अनुरोही |prajapati\
mR\^igshiras |श्वेतायी |soma and maruts\
Ardra |भार्गव |rudra\
punarvasu |वात्स्यायन |aditI\
pushya |भरद्वाज |bR\^ihaspati\
Alshlesha |जतुकर्ण |ahi\
magha |व्याघ्रपद |pitarah\
purva-phalguni |पराश्र |bhaga\
uttara-phalguni |upashiva | aryama\
hasta |मान्ड्वय | सविता\
chitra |gotama | tvashTa\
svati |कौण्दिण्य | vAyu\
vishAkhA |kapi | indrAgni\
anurAdhA |maitreya | mitra\
jyeshTha |kaushika | indra\
mUla |kutsa | NirR\^iti\
purva-अशाढा |harita | ApaH\
uttara-अशाढा |kashyapa | vishvedevAH\
abhijit |shaunaka | brahmA\
shroNa |atri | vishhNu\
shravishTha |garga | vasavaH\
shatabhishak |दक्शायण | indrAvarunA\
pUrva-प्रोश्ठपदा |vatsa | aja-ekapAda\
uttara-प्रोश्ठपदा |agastya | ahirbudhnya\
revatI |शाञ्क्यायन | pUshA\
ashvayuja |कात्यायन | ashvinau\
bharanI |ऋशि-patnya| yama

It must be noted that hasta (Corvus is called the hand of सविता) and the phrase devo vah सविता हिरण्यपाणि प्रतिगृह्नातु is used often in the vedic literature. I wonder if this has a link with the position of Corvus in the East point position during the time these sacrificial rituals were initiated. As Narahari Achar points out the association of the constellation with the East point may have been of significance for sacrificial initiation.


