
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Musings on the अपोनप्त्रीय rite](https://manasataramgini.wordpress.com/2005/11/23/musings-on-the-aponaptriya-rite/){rel="bookmark"} {#musings-on-the-अपनपतरय-rite .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/23/musings-on-the-aponaptriya-rite/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The अपोनप्त्रीय is an important rite associated with the सोमयाग and is described in detail in the aitareya, the कौशीतकि and चागलेय ब्राह्मणस्. The rite seems to be associated with the deities सरस्वती and अपां नपात् and was probably instituted by कवष ऐलूश, the priest of the pUru king कुरुश्रवण. Simply told, it is said that the other ब्राह्मणस्, the madhymas questioned the legitimacy of कवष as a brahmin, calling him a cheat and one born through a दास \[proto-Iranian?] mother. They drove कवष out in the desert and as he departed he composed the hymn "pra devatra ब्रह्मणे..\[RV 10.30]" and it is said that सरस्वती followed him surrounding him with her waters. The madhyamas realizing that he was a great ऋशि favored by sarasvati brought him back and had him institute the अपोनप्त्रीय rite. With these hymns the होता priest collects water in on the day before the sacrifice and this is called the वसतीवरी water. The water collected in the morning of the sacrifice is called the एकधनआ water. The होता then pours these into a goblet with RV 1.83.2 and the adhvaryu mixes honey and soma into these waters for the devas. This pouring is supposed to aid crushing of the rivals through indra. Then with Rv 1.23.16-18, the final pouring of the waters for the water goddess is performed.

One of the most important points garnered in support of the out of India hypothesis of Aryan origins is the river Sarasvati. The river has been physically linked with the Hakra- paleochannel on whose banks the Harappan civilization flourished. The river is textually identified with the aquatic deity of the Indo-Aryans, सरस्वती who is widely worshiped in the ऋग्वेद and persists to this date in the modern Hindu religions as a consort of brahmA. An analysis of the goddess सरस्वती and her Indo-European cognates could help to strengthen or weaken the OIT and other Indo-centric Aryan hypotheses.

One of the important themes that emerges from the अपोनप्त्रीय ritual is the intimate connection between sarasvati and Agni as worshipped as the son of the waters or अपां नपात. This fiery entity hidden with the water is of ancient IE provenance as is evidenced by the word naptha meaning a fiery substance and the same time bearing a relationship to the word naptar or relative, suggesting that it was essentially derived from the ancestral state आपां नपात्. The connection between the water deity and अपां नपात् (who is described as being surrounded) by water nymphs (another ancient IE concept) can be demonstrated in a number of IE myths. A remarkable parallel of the link between सरस्वती and अपां नपात् and sarasvati as illustrated in the अपोनप्त्रीय rite can be seen in the ABAN YASHT of the Iranians. The point to note here is that ardvi sura anAhitA is a perfect cognate of sarasvati and not a physical river as has been often claimed for the सरस्वती. She is generically as well as specifically associated with several rivers: Vitanghuhaiti on whose bank the Naotaras worshipped her, river Rangha homologous to ऋग्वेदिc Rasa (also mentioned in the Jaiminiya ब्राह्मण) or the great river flowing down from Hukriya down to the Vouru kasha. Thus Anahita and सरस्वती are archetypal water goddesses, who were worshiped with much intensity by the IEans and who may get associated with physical rivers as well as अपां napat. In this context it is important to note that the concept of the fire within water is also connected with the Iranian concept of Xwarena- a fiery luster that existed on the floor of the Vouru kasha that Frangrasyan tried to obtain but failed but it followed the Iranian kings emerging from takshma urupi and केरेसास्प. Apam napat appears in a truncated form as Neptune in the religion of the Romans and is a fiery deity associated with the waters. He is said to lead the waves (so also his Keltic counter part Nachtan) as he moves along. Similarly in the Keltic mythosphere we have the goddess Epona who is said to have erupted from a spring of water and led a stream of it riding a horse. Hence we see that Apam Napat as the fire concealed within water and leading the erupting waters either himself or with a Sarasvati like deity to be an old Indo-European theme. So it is clear that in the Indo-Aryan composers of the brahminical explanation for the अपोनप्त्रीय founded by kavasha ailusha, drew straight from their ancient Indo-European mythosphere, They super-imposed the Apam णपात् myth right on to kavasha with all the 'scorching' connotation being suggested by the desert and his leading सरस्वती away very much like Neptune leading the waves or Epona leading the stream away. Hence one may reason that after all the सरस्वती talked about in the myth had nothing much to do with the physical river. In general the high degree of conservation of सरस्वती related mythology amidst the ancient IEans suggests that it was not developed in connection to a particular river and makes the identification of the ऋग्वेदिc Sarasvati with the Hakra channel equivocal.


