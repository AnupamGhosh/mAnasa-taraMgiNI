
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [What?](https://manasataramgini.wordpress.com/2005/04/01/what/){rel="bookmark"} {#what .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 1, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/01/what/ "6:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We have seen many things in life. But never have we seen something hit us as overwhelming as this. There was revolt in खाण्डवप्रस्थ. It looked as though कुम्भकर्ण was standing there on the field and marching steadily towards us. What ever astra we hurled at कुम्भकर्ण simply broke to pieces and fell down. But कुम्भकर्ण, unaffected moved on. It was terrifying to stand there and watch all our weapons crumble like that as behemoth advanced towards us. Like arjuna saw the kaurava warriors rushing head long into mouth of the all consuming विष्णु we saw everything we sent move into the mouth of काल. We felt in real life those fears we only had experienced in our dreams- those dreams in which you go to the exam hall and miss the whole damn thing there- or the nightmare from which you get up in a shock... Never did we realize the stark fate of the muni's words- our divya दृष्टि now acted like that of दृतराष्ट्र's in that moment when he saw the awful विश्वरूप. We asked had we navigated the ocean this far with all the power of our mantras only to be sent against this whirlpool. Will we be alive at the other end of it? It was in such a situation with our bow broken and for the first time all our astras blow away we humbly sought the refuge of the hallowed feet of our great god शण्मुख.

Oh कुमार, the son the blazing agni, your mantra had saved us in the past.\
Oh son of the fierce rudra, when the deva सेना was as rudderless us as us,\
you alone led it victory, Oh lover of devasena.\
When death was besieging the याग्नीकि brahmin, as the troubles are surrounding me,\
you alone O अजारूढ brought the impossible act to fruition.\
I need the same O lord of the 6 stars, for after all you are सुब्रह्मण्य.\
From the in deepest trouble you uplifted even the ones who were supposed to be beyond trouble- O lover of the daughter of मृत्यु, why is it that this fear does not go away.\
I humbly seek you aid, with indra and वरुण along with you. The गणस् of rudra and the विनायकस् may they all be favorably disposed. My your many मातृकस् favor me. may the triple-strider favor me.

I know not to compose in Chandas, I know not compose in high-flown poetry but I have always felt रोमहर्शण at the very mention of your kathas. O कुमार bear me aid as I worship that siddhamantra of yours- I am tied in this विषंअपरिस्तिथि like शुनःशेप of my clan at the stake . Like वरुण save me.\
oM ************ |


