
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Night of The god](https://manasataramgini.wordpress.com/2005/03/09/night-of-the-god/){rel="bookmark"} {#night-of-the-god .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 9, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/09/night-of-the-god/ "7:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[oM अतितीक्ष्ण महाकाय कल्पान्तदहनोपम ]{style="font-size:11pt;"}झङ्कार-भैरवाय[ नमस्तुभ्यं //]{style="font-size:11pt;"}

We invoke the fierce झङ्कार-bhairava. We had worshipped the terrible god with the great litany, the महान्यास on the great night that he may be pacified.

Having worshipped the great god we were reminded of the intriguing words of the illustrious Kashmirian Tantric abhinavagupta of shrinagara in his great work the [तन्त्रालोक]{style="font-style:italic;font-weight:bold;color:rgb(255, 102, 102);"} (TA 15.264b-15.266):

[न खल्वेष शिवः शान्तो नाम कश्cइद्विभेदवान् // २६४]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[सर्वेतराध्वव्यावृत्तो घटतुल्योऽस्ति कुत्रcइत् /]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[महाप्रकाशरूपा हि येयं संविद्विजृम्भते // २६५]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[स शिवः शिवतैवास्य वैश्वरूप्यावभासिता /]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[तथाभासनयोगो ऽतः स्वरसेनास्य जृम्भते// २६६]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}

[The shiva which they imagine, completely pacified, differentiated from all other things, transcendent to all other paths, similar therefore to something inert, inert like glass does not exist anywhere at all. shiva in effect is nothing more than this "state of being", which unfolds its in the form a great light. Its very condition as shiva indeed consists in the fact that the all the varied forms of the universe appear. This process of manifestation into all the form of the universe produces itself completely freely within him.]{style="color:rgb(255, 0, 0);"}[[\
]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}]{style="font-size:11pt;"}


