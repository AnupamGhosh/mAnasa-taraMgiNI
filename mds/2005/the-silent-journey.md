
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The silent journey](https://manasataramgini.wordpress.com/2005/07/29/the-silent-journey/){rel="bookmark"} {#the-silent-journey .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 29, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/29/the-silent-journey/ "5:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After the glorious morning on April 11 th 1986 we were basking in the exhilaration of having seen the Halley's comet in its closest apparition. The vaishya from लाट was excited at the successful sighting under clear skies. That afternoon we decided to catch some sleep to make up for the marathon astronomy session. It was the second great revival of astronomy for us and we were looking forward for another great session with vaishya श्रेष्ठ spanning the whole night. Vidrum called upon us in the evening along with दुष्टनेत्र to go on a cycle venture. We asked him to avoid दुष्टनेत्र and went beside the vaishnava's temple where we chatted for a while with Mis-creant and K in the quadrangle of the latter's housing colony. All of them wanted to join us to sight the comet the next morning. I told vidrum and the yoshas that it was their responsibility find their way to our observing spot on hanumatadri early in the morning, because me and vaishya श्रेष्ठ were going to observe all night round. Vidrum, a bold rock-climber, agreed to join us even earlier. Vidrum after arriving looked at the planets with us for some time, but soon lost interest and said he wanted to visit a "bhUt-bungalow" that Mis-creant had claimed to have discovered. So he clambered down a rock face and proceeded to the BB where he vanished. It was an abandoned house originally built by a clever but evil and fallen brahmin. After a while Vidrum returned stating that he had seen strange happening out there.

We decide to check it out too and followed him. The air was still and occasionally stirred by the noise of crickets. Vidrum felt a cold spot in one corner but we did not. Suddenly we saw a cat sneak in and proceed till the middle of the room and run away. Vidrum took that as a positive sign and moved forward. Vidrum inexplicably slipped and fell down. He was lying on the floor and uttering rubbish. He produced two words Plinkplonkplinkton and Stick-tail over and over again and asked us to watch out. After some time he regained normalcy and we silently returned back to our observing spot without uttering a word.\
\~\~\
On the Hockey patch next to पुरीषाद्रि we encountered Plinkplonkplinkton and Stick-tail, the first bad and the second good.


