
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The symmetric devolution of Jx](https://manasataramgini.wordpress.com/2005/09/23/the-symmetric-devolution-of-jx/){rel="bookmark"} {#the-symmetric-devolution-of-jx .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/23/the-symmetric-devolution-of-jx/ "5:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The chance for the kuriltai was unusually solidified by the bold Argentinian adventurer. At the said hour we met with Mn and the shachiva's prime agent. Mn, was a pioneer and even bolder adventurer than the Argentinian and was as ever silent, but he was shaken the strikes close to home. The shachiva's agent who had maintained silence all this while, suddenly gave in to revealing the secrets of Jx. We were surprised the precision with which the shachiva's agent described the various issues surrounding the राक्षसि. I asked how could she know that much, when the shachiva himself was generally poorly informed and living in figments of his making. The agent then revealed the startling point. She saw Jx looking to the mirror one day and crept up behind him. To her horror she saw that the reflection in the mirror was not that of Jx but that of the राक्शसि. Then everything became clear to the agent by merely gazing at the mirror. We discussed the matter more deeply. It became clear to us that we had not bought a cell phone at all but a dangerous device that looked like one. Even the muni until that point would not have expected that the dangerous contraption was so cleverly concealed. It suddenly struck us that we had picked up the sarpa of the खाण्डवन् blunder: Even janamejaya could not revive the kurus until परीक्षित् had died.

Once the realization dawned that Jx and the राक्शसि were sex-inverted mirror images of each other we realized how bad the situation was. We wondering of the ideal world: The tale of Herr Boras would have worked we thought. Just then were rudely jolted from our reverie by the deadly astra. We received the message: "deploy the दूरदृष्टि prayoga".


