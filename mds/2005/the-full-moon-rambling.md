
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The full moon rambling](https://manasataramgini.wordpress.com/2005/10/20/the-full-moon-rambling/){rel="bookmark"} {#the-full-moon-rambling .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 20, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/20/the-full-moon-rambling/ "4:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

On the past fullmoon several years ago when we were still young we were seated in the pleasant company of the apsaras and drinking the wonderful पायस. We were yarning about the events in the mundane plane of existence, when the foremost of the सुतनूस् asked what was the simplest chaturti vrata to the terrible ulka and how may it be performed. We found the appropriate answer for the same after some thought. It is the ulka vrata of oM गः स्वाहा. It may be done by all to attain release from vighnas.


