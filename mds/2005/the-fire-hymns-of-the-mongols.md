
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The fire hymns of the Mongols](https://manasataramgini.wordpress.com/2005/10/23/the-fire-hymns-of-the-mongols/){rel="bookmark"} {#the-fire-hymns-of-the-mongols .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/23/the-fire-hymns-of-the-mongols/ "4:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The fire worship of the Hindus is an ancient Indo-European trait that was clearly amplified in the Indo-Iranian branch. The primary fire priests of the Indo-Aryans are the भृगुस् and and अङ्गिरसस् who are remembered in the old vedic texts. The भृगुस् also known as atharvans are also remembered as the founders of the fire worship amongst the Iranians. The Iranian word for fire being Atar. One of the interesting trends noticed in the Hindu fire worship is personal association of each major Aryan clan with the invocation of fire, recaptitulating the way their founders did it via the आप्री hymns. Beyond the आप्रीस् there are the original kindlers of the fire who are remembered as part of the begining of the fire rites. This illustrated in the famous formula remembering the first of the भार्गवस् who kindled the fire. In any regular fire sacrifice termed the इष्टि, after the rectitation of the 15-fold सामिधेनी hymns (pra vo वाज ...) the pravara of the यजमान and his founding fathers are recited. Then even as the adhvaryu pours a libation of ghee in the fire even as the hotar chants:

[अग्ने महन् असि ब्राह्मण भारत । भार्गव छ्यावनापनवानौर्व जामदग्न्य ॥]{style="color: #0000ff"}\
agni, you are great and possessing the brahman power, you are of the भारतस्; you are of भृगु, च्यवान, अपनवान, aurva and jamadagni.

The preliminary recitation in the new and full moon इष्टिस् ends with the refrain भृगुणां "अङ्गिरसां tapasa तपयध्वं". This again illustrates the connection of the sacrificial fire with the first kindlers. The formula used to make agni invite the devas to the यज्ञ (gopatha ब्राह्मण of the AV-S 5.89 and RV khila 4.9.2) also alludes to the भार्गवस् as the first kindlers. This hymn is also known as the सुभेषज hymn or the hymn refering to the first knowers of medicine (बेषजानि) or the भैषज्य veda (the Atharvan) . Only a fragment of this hymn is remembered by modern oral tradition:\
dhruvam agnir no दूतो रोदसी हव्यवाड्\
[देवाण् आ वक्षद् अध्वरे ।]{style="color: #0000ff"}\
[विप्रो दूतः परिष्कृतो]{style="color: #0000ff"}\
[यक्षश् च यज्ञियः कविः ।]{style="color: #0000ff"}\
[अप्नवानवद् और्ववद्]{style="color: #0000ff"}\
[भृगुवज् जमदग्निवद् च्यवानवत् ॥]{style="color: #0000ff"}

Truely agni is our messenger, the heavenward oblation-bearer,\
may he bring here the devas to the sacrifice,\
he the wise decorated messenger,\
the mighty and inspired poet of the rite,\
As अपनवान did, as aurva did,\
as भृगु did, as jamadagni did, as च्यवान did.\
\[It is with a similar formula, the adhvaryu asks the hotar to take his place in the manner of the old ritualists in all shrauta इष्टिस् . In that formula the other old sacrificers mentioned are manu and bhArata of the क्षत्रियस्]

In light of this ritual pattern it is fairly clear that the shrauta ritual clearly remembers the first kindlers. In light of this, I was struck by the remarkable parallel seen in a manuscript of old hymns of the Chingizid Mongols recorded by Kublai Khan, which was collected by Walther Heissig. They are addressed to the Mongol fire deity Odqan Talaqan. The point to note is that the old Turko-Mongol fire deity may be alternative addressed as female or sometimes a male:

"You who were kindled by the great holy Chingiz Khan,\
you who were blown into a flame by the mighty Torghon Shira;\
she spreads the smell of burning, which rises to the ninety nine Tengri.\
Radiant like the rising sun of the color of dark red bronze,\
you were kindled by holy Chingiz Khan,\
brought into a flame by the wise queen Borte Ujin [1].\
You were first kindled by Yesugei Bagaatur [2],\
the king appointed by the 99 sublime Tengri,\
you were blown ablaze by Hoe'lun Khatun [3].\
You have the flint for mother, iron for father,\
and the elm as the offering wood,\
You with the forked tongue animated by wind,\
come with a camel skull.

The fire kindling ceremony of the mongols also goes on to mention other ancestral kindlers of the fire like Chagadai, Ogodai and Tolui. The fire rite took place in the spring for the blessings on camels, the summer solistice libation, the great rite at the last day of the year with recitation of several fire hymns, and a section of the marriage ceremony of the mideaval mongols. This ritual pattern and the thematic similarities suggests that the Turko-Mongols might have acquired aspects of their fire rite from the ancient Indo-Iranian substratum of Central Asia. This probably included elements related to the ancestors kindling the fire. This contention is also supported by the Tengris being a multiple of 33 the Indo-Iranian basic count of the number of gods.

[1] Principal wife of Chingiz Khan; [2] father of Chingiz Khan; [3] mother of Chingiz.


