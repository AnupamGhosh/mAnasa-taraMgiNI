
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Wanderings in the sun](https://manasataramgini.wordpress.com/2005/09/05/wanderings-in-the-sun/){rel="bookmark"} {#wanderings-in-the-sun .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 5, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/05/wanderings-in-the-sun/ "12:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

They say it is galanin. It had triggered the waves in the रसार्णव. The warm sun was exceedingly pleasant as it dipped towards the western peg. The relieved ourselves of the ill memories of the tragic bandha temporarily and wandered on the sunny plains. But the samudra within was hardly placid. The huge waves lashed as result of the highest rasa. Then it came under the grasp of धुमावति's sleep and was suppressed for a long time.

As we wandered in the sun we wondered why the waves were lashing about. We connected with the muni and "spoke"-

I: The fatal cell phone torments us.

M: Now is not time to think of the cell phone when you wander in the sun.

I:Indeed, but even the pleasures of hayasthanika and Arginine no longer mean anything.

M: The शचीव and the अमात्य were wandering on the वानर mountain in search of gold. Gold alone of all those metal does not decay due to the virulence of oxygen. They chipped away at the rocks and found iron in abundance. But of what use is iron for this purpose? The atmosphere at your place is so filled with oxygen. They found a few copper nuggets and were transporting it via the पाषण्ड hill, when the traitor "Shoi" stole it from them as they trusted him. You caught up in the battle of पाषण्ड hill were unable to send reinforcements. They of course found a lot of Fool's gold but the phenol test you performed showed their true colors. Then they found a large lump of aluminium and brought it to us. It was correctly realized that the aluminium was no use for the said purpose. You wisely dropped.

I: Indeed all that was true until we made the fatal mistake with the globule of mercury.

M: Thus, Oh मन्त्रवाह, are the vagaries of रसायन ! As we mentioned before the अमात्य sensed some trouble. Still, we walked into the trap! Sometimes you never know until the experiment is done. And in रसायन some experiments are dangerous.


