
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [शूलिनी](https://manasataramgini.wordpress.com/2005/04/19/shulini/){rel="bookmark"} {#शलन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/19/shulini/ "6:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The wings of sharabha are प्रत्यङ्गिरा and शूलिनी. शूलिनी may be invoked to complement the invocation of प्रत्यङ्गिरा. Her iconography is thus:\
[अध्यारूढां मृगेन्द्रं सजल-जलधर-श्यामलां हस्तपद्मैः शूलं बाणं कृपाणं-अरि-जलज-गदा-चाप-पाशान्-वहन्तीं /]{style="color:rgb(255,102,102);font-weight:bold;"}\
[चन्द्रोत्तम्सां त्रिनेत्रां चतसृभिर्-अभितः खेटकान् बिभ्रतीभिः कन्याभिः सेव्यमानां प्रतिभट-भयदां शूलिनीं भावयामि।]{style="color:rgb(255,102,102);font-weight:bold;"}

Her mantra may be combined with those of प्रत्यङ्गिरा to rout the enemy. The mantra is provided as being:\
[ज्वल-ज्वल-पदस्यान्ते शूलिनीति पदं वदेत् /]{style="font-weight:bold;color:rgb(255,204,102);"}\
[दुष्टग्रहं हुमस्त्रान्ते वह्निजायावधिर्मनुः //]{style="font-weight:bold;color:rgb(255,204,102);"}

The ऋषि is दीर्घतमा औचाथ्य the अङ्गिरस्, kakubh is the meter and samasta-sura-वन्दिता शूलिनी देवता.

durge हृदयाय namaH\
varde shirase svAhA\
vidhyAvAsini शिखायै vaShaT\
asuramardini युद्धपूर्वप्रिये त्रासय त्रासय कवचाय huM\
deva-सिद्धसुपूजिते nandini रक्ष रक्ष अस्त्राय फट्


