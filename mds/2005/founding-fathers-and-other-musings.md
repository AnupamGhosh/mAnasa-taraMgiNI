
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Founding fathers and other musings](https://manasataramgini.wordpress.com/2005/12/23/founding-fathers-and-other-musings/){rel="bookmark"} {#founding-fathers-and-other-musings .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/23/founding-fathers-and-other-musings/ "6:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/niall.jpg){width="75%"}
```{=latex}
\end{center}
```



```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/chingiz.2.jpg){width="75%"}
```{=latex}
\end{center}
```



The results of recent chromosomal studies point to an interesting feature regarding the founding fathers of several nations. The biggest impact that has been uncovered to date appears to be that of Chingiz Kha'Khan, the founder of the great Mongol nation, who appears to have left behind 16 million male descendents in the modern world. More modestly the founder of the Manchu empire, the descendents of Chingiz Khan's arch rivals the Jurchen, appears to have left behind 1.6 million descendents today. More recently it was noted that the legendary early Irish king, Uí Néill appears to have fathered a whole bunch of Irishmen such the McDonalds, McAlisters and McDougalls. The statistics of these founding fathers suggests that the male contribution to the extant gene pool may even follow the power law, with few hubs or power-houses who have fathered large swathes of the world population and numerous males who hardly have few living descendents. Of course this conjecture needs much more testing. But even if it were approximately something like that then it clearly shows that Kingship was a real evolutionary game. In primates being the dominant male actually helps you further you genes-- being a King does not merely mean being a superdominant male in terms of power of prestige but also contribution to the gene pool on a large-scale. Of course there may be gay kings like Alexander of Macedon or Alla-ad-din Khalji of accursed memory who do not get anything moving beyond the futile conquest of territory. These, despite their possible effects on the meme pool (that too usually negative), do not leave their imprint on the gene pool.

Further the recent genetic studies also reveal that in the terrorist State of Pakistan the overall genetic diversity is far reduced in comparison to India and the affinities of males show a certain shift in the direction of the Arab world. Thus, the verdict of genetics is that the Islamic invaders did kill of males in parts where they became dominant. We do not have much evidence if that happened with the earlier invaders.


