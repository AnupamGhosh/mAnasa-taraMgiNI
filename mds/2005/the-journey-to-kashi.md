
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The journey to Kashi](https://manasataramgini.wordpress.com/2005/03/10/the-journey-to-kashi/){rel="bookmark"} {#the-journey-to-kashi .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 10, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/10/the-journey-to-kashi/ "7:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Since the Atman, which merely watches, is poised to cast away its sharIra and the old corpses shall lie on the path of no return, as though marked by the dance of sharva, we shall narrate those old events that we shall probably never get to talk of, once we walk ahead on the path. The tale of the yogi prince of Shri Lanka who was offered putrid food by the Dakini in the house of pleasure in bhArata came to our mind. Also the dyadic fate of ययाति presented itself. After the enjoyment of pleasures on earth and svarga one may fall head long from the lokas, as the पुराणस् talk of the meteoric crash of an individual who has exhausted his पुण्य.

The year before the Great Transition our ruffianly band decided to journey via a torturous path to the city of कालभैरव, where the fierce emanation of rudra finally stopped after having slain vishvaksena and drunk the blood of the great Triple-strider. On that journey were the following individuals many of whom are receding from the recesses of our ken : 1) The boisterous mithilan (M). He was one who knew the intricacies of NIA languages that could save us in the hostile belt of the degenerate Aryavarta. Though an year younger was the leader of the grAma. 2) There was the mean kabandhamukha (K). An intelligent and learned soul but given to malicious comments. 3) संजय, a cultured soul, especially given his roots in the acrid bogs of the land. 4) There was also vaishya jyotisha, whose company we enjoyed under clear skies. 5) There was ST who was then pleasing to the eyes, ears and mouth. 6) There was R, the नित्याश्री of the kalashajas. 7) There Igul, the cow amongst the descendents of भालन्दन.

When we arrived in the city of the awful bhairava, K was struck by the very darts of महाकाल via the food. We thought he might attain mokSha rather soon on the banks of the holy जाह्नावी. But he somehow survived only to form an alliance against me with my द्विषन्तं भ्रातृव्य. Due to the enterprise of M we translocated to a remarkably desolate spot in the Doab some distance away from Varanasipura. We were the only humans there for few kilometeres it appeared- a surprising thing for the most densely populated part of the world.

There under the sky dome we saw the stars and I picked the thousands of stars of omega Centauri with my occular. We feasted on ST's cullinary endeavors. Many years later, lacking food, we transcended hunger to see the same sights on the great lake of म्लेच्छपुर.


