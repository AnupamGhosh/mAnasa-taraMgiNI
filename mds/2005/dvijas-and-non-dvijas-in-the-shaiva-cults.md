
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [dvijas and non-dvijas in the shaiva cults](https://manasataramgini.wordpress.com/2005/03/26/dvijas-and-non-dvijas-in-the-shaiva-cults/){rel="bookmark"} {#dvijas-and-non-dvijas-in-the-shaiva-cults .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/26/dvijas-and-non-dvijas-in-the-shaiva-cults/ "7:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As we argued earlier the कालामुखस् were rather orthodox, even if electic, followers of the Astika path. Their intellectual prelidictions were rather clear, and they philosophical abilities are also strongly suggested from what material that survives of the shakti परिषद्, mainly in the Kannada country, and the सिंह parishad in the Andhra and Tamil country. Their Kashmirian precursors were also strongly intellectual brahmins (the काश्मिर पण्डितस् of the inscriptions) who originally coexisted with their cousins who were proponents of other tantric schools like shrikula and trika. This is in contrast with the stark unorthodoxy of the Lingayatas or वीरशैवस् who followed them.

The vaiShNava अचार्य रामानुज in his श्री भाश्य provides a particular disparaging criticism of shaivas (shri भाष्य ii.2.35-37):\
["Likewise the shaivas state that even men belonging to the non-dvija ranks can attain ब्रह्मण-hood and yati-hood by means of certain rites. It is said : one instantly becomes a ब्राह्मण merely by the process of initiation. A man becomes a yati by undertaking the कपाल vrata."]{style="font-weight:bold;color:#0000ff;"}

Thus, the vaiShNava saint tries to generally dismiss all the shaivas as being violators of orthodoxy and hence heretics. This is in strong contrast to the actual behavior of the most orthodox shaiva group the कालामुखस्. रामानुज accurately records 4 schools of shiva worshippers: कापालिकस्, कालामुखस्, पाशुपतस् and shaivas. So, evidently the vaiShNava polemicist is using the कापालिकस् as primary models and guilt by association to dismiss the shaivas as a group. Similar lists of 4 shaiva sects are found in the पुराणस् and the tantric paddhati of इशानशिव.

The वामान पुराण (6.86-92) in particular states that the:

 1.  shaiva sect as founded by a वसिष्ठ of the shakti lineage and his brahmin disciple गोपायन.

 2.  the पाशुपतस् were founded by a भरद्वाज and his pupil the kShatriya somakeshvara.

 3.  the कालामुखस् were founded by an Apastamba and his brahmin pupil क्राथेश्वर.

 4.  the कापालिकस् (महाव्रतिन्स्) were founded by the vaishya dhanada and his shudra student अर्णोदर.

Thus, 3 well known shaiva lineages are clearly of Brahminical provenance, as evidenced by the early texts, the श्वेताशवतर उपनिषद्, atharvashiras and atharva महापाशुपत vrata grantha. Thus, the general claim of all shaivas being anti-vedic or anti-brahminical is patently false. Even more importantly, there is evidence that even the कापालिकस् had a strong brahminical component. This, in any case lays to rest certain modern and medieaval claims (like that of the learned tantric polemicist shri लक्ष्मिधर) that brahmins were at loggerheads with कापालिक or कालामुख ways. For example, the brahmin वैशेषिक scholar, named दशपुरीयन्, from vEDal in Tamil Nad was a कालामुख of हारित gotra and a practioner the तैत्तिरीय school (Rangacharya's collection of stone inscriptions from Tamil Nad).

In this context we should point out an important inscription brought to my notice by R.\
The inscription from Nirmand village in the Kangra district in Himachal Pradesh states that the king sharvavarman installed a लिञ्ग of कपालेशवर in around 576-580 AD. His successor samudrasena in the 600s provided the निर्मण्ड अग्रहार to a school of atharvavedic brahmins for preserving the अथर्वण shruti and maintaining the कापाल rites. Thus, pointing out to relatively early associations of brahmins with the कापालिक stream.

Finally, we look at the root sources of the tantric worship of shiva. This escathology proposes that 5 streams of tantras emerged from the 5 faces of shiva:\
bhuta tantras: सद्योजात\
वाम tantras: वामदेव\
bhairava tantras: aghora\
gAruDa tantras: tatpuruSha\
siddhAnta tantras: ईशान\
The 28 tantras = 10 शिवागमस् and 18 रुद्रागमस् form the siddhAnta tantras which are followed by the sect called shaiva by रामानुज and the other middle period sources.\
The 32 bhairava tantras are the tantras of the कापालिकस् (but not the कालामुखस्). The bhairava tantras include the most terrifying Agamas, like the घोरं, भीमं, वेतालमर्दनं and कपालं, from the अभिचारिक rites of which there is little chance of escape.

The कूर्म पुराण clarifies that the पाशुपतस् were स्मार्त brahmins or क्षत्रियस्. They actually followed the institutes of the veda and the dharma शास्त्रस्. Thus, we may conclude that within the shaivas the dominant force were Brahmins, though there was considerable diversity within them.


