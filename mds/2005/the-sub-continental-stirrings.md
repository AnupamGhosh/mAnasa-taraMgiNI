
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The sub-continental stirrings](https://manasataramgini.wordpress.com/2005/12/18/the-sub-continental-stirrings/){rel="bookmark"} {#the-sub-continental-stirrings .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 18, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/18/the-sub-continental-stirrings/ "6:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier commented how the Trojan horse, whom we used to call Chappal (slipper) in our days of youth was working his wonders with the Indian army. We had speculated that he was specifically an agent of the roguish pirate chief Ponting to do away with his main dangers before the invasion of the subcontinent. We have now got some intelligence as to how the Indian ants invited the parasitoid wasp Greg "Chappal" into their nest. The lazy king of Vanga, seeing his Kiwi mercenary returning to his own country, decided to get Chappal thinking he was an ally of his. A little did he know of the mission Ponting had primed Chappal for. In the mean time with the actual battle performance of the Vanga champion being less than desirable, and the veteran Prince of Bombay being side-lined due to the blows he had received from cannon shards in the thick of battle, the prince of the Karnata country decided to stage a[ coup de tat]{style="font-style:italic;"} with his supporters. Pulling around him a bunch of adventurers such as the Bundela, the Mohammedan Kaif, the विषम Gambhira and others he got over the Trojan plant Chappal to his side. Then he used Chappal to seize the throne by ousting the Vanga warrior and also trying to threaten the Vanga's supporters like the Sikh turbanator and the Andhra chieftain from Vangipura. As result the whole Indian army has been stirred and the atmosphere of fear has been reinstated to possibly trigger a decline in performance.


