
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Nebra Star Disc and Indo-European Archaeoastronomy](https://manasataramgini.wordpress.com/2005/09/15/nebra-star-disc-and-indo-european-archaeoastronomy/){rel="bookmark"} {#nebra-star-disc-and-indo-european-archaeoastronomy .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 15, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/15/nebra-star-disc-and-indo-european-archaeoastronomy/ "5:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Nebra Star Disc [![Posted by Picasa](https://i0.wp.com/photos1.blogger.com/pbp.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://picasa.google.com/blogger/){target="ext"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/stardisk_disc.jpg){width="75%"}
```{=latex}
\end{center}
```



The astronomy of the Indo-Europeans has always been frowned up by the "Western experts". They like to tell us that the Indo-Europeans were a primitive lot with not much astronomy to talk about. Everything the Indo-Europeans got is from the Middle Eastern civilizations of the Biblical milieu- Assyria, Egypt, Sumeria, Babylonia, Mesopotamia is the root of all astronomical and mathematical knowledge- they like the blather on. Hindu borrowed from the Middle East to produce the primitive vedAnga ज्योतिष they tell us. However, a study of the Hindu and the Greek astronomy while revealing several regional developments still have a basic underlying core that was most probably derived from the Indo-European past. The example include the name of the constellation Orion and the name of the Vedic sacrifice आघ्रयण, whose performance was marked originally by the equinoctical position of Orion. The Hindus developed the 27/28 नक्षत्र system. The origins of this are murky but clearly the "28th" नक्षत्र abhijit was used to mark the विशुवान् day at a certain point and dropped later after the axial precession had pushed it away from that point. This and other points suggest that the Hindus have been using the नक्षत्र sytem from at least before 2700 BC, suggesting a long history of their independent developments after branching from the Greeks.

Recently in Nebra, Germany a star disc with an image of the sky was discovered. It contains the following salient points


 1.  A green bronze disc with stars, sun and the moon marked on it.

 2.  A crude representation of the Pleiades (कृत्तिका) is marked out distinctly.

 3.  The rim of the disc is marked by about 36 points indicating a division of the circle in the traditional Indo-European way.

 4.  There are two arcs of 82.7 degrees each on either side marking exactly the arcs in the sky of the between the solisticial rising and setting points respectively on each horizon in Nebra.

 5.  There is a golden boat of the sky between the two arcs.

This reminds us of the below ऋग्वेदिc hymn to deva पुषन् that talk of the gold boat- "[नावो हिरण्ययीर्]{style="font-style:italic;color:#ff9966;"}" in which पुषन् travels acting as an emissary or carrier of the sun- "[दूत्यं सूर्यस्य]{style="font-style:italic;color:#ff0000;"}"

[यास्ते पूषन् नावो अन्तः समुद्रे हिरण्ययीर्-अन्तरिक्षे चरन्ति ।]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
[ताभिर्यासि दूत्यां सूर्यस्य कामेन कृत श्रव इछमानः ॥ (६।५८।३ ओफ़् भरद्वज बर्हस्पत्य)\
]{style="font-weight:bold;font-style:italic;color:#ff0000;"}\
The archaeologists were, as usual, quick to call it a fraud, claim it to be an Egyptian influence or even a Middle Eastern object. But the metal analysis and longitude proved beyond doubt that the Nebra star disc was made right there in Germany in Bronze age before any stellar representations appear in Egypt. This provides strong evidence that Indo-Europeans were making their own astronomical observations all over the zone they had spread into. It is quite possible that they influenced Mesopotamia in turn rather than the other way around. The constellation pits in Muggenberg and Uitgeest in Holland again establishes that the constellations of Canis Major, Pegasus, Taurus and Herakles are ancient Indo-European constellations, rather than having a Mesopotamian origin. This provide indirect support for the ancient observations of the Hindus made regarding these constellations.


