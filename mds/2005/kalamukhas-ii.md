
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कालामुखस्-II](https://manasataramgini.wordpress.com/2005/03/23/kalamukhas-ii/){rel="bookmark"} {#कलमखस-ii .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/23/kalamukhas-ii/ "7:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Modern historians have seen the वीरशैवस् as an organic evolute of the कालामुखस्. While their attachment to लिङ्ग worship, shiva devotion and monastic organization are indeed comparable, there are many differences. The वीरशैव cult, while found by a brahmin basava, had a rather anti-brahminical streak. Firstly, it was vociferously critical of the varNAshrama dharma and secondly sought to replace brahminical rites with imitations. For example, they used the rudra gAyatrI in place of the सविता gAyatrI for the संध्य rite. The virashaiva cult, as characterized by the vachanas of basava, was low on philosophy and high on rhetoric, and associated, as per its own admission, with murder of the ruling king, religious persecution and raising of private militias.

The inscriptions of the कालामुखस् collected by Lorenzen clearly show that the majority of their priests were highly erudite brahmins with orthodox leanings. For example लाकुलीश्वर, a कालामुख priest in the first half of the 1000s has made an inscription that states:\
["The god महादेव's feet are worthy of worship by all the world. The rules laid down in the veda for the वर्णस् and Ashramas is dharma. Who derides these two statements, on his head will I place my foot in the king's court."]{style="color:rgb(51,255,51);font-weight:bold;"}

The philosophical leanings of the कालामुखस् are again made very clear in some of the inscriptions:\
["लाकुलीश्वर is the able supporter of nyAyA and वैशेषिक, he is a वाडव fire to bauddha ocean, a thunderbolt of indra to the मीमाम्सक mountain, a saw for cutting down the लोकायत banyan tree, a great eagle to the साम्ख्य serpent, an axe for propounders of the advaita doctrine, a noose of yama to hostile proud पण्डितस्, and a meteor to the naked jains. "]{style="color:rgb(51,255,51);font-weight:bold;"}

Another inscription regarding a shiva temple of the कालामुखस् made by the chAlukyan chieftain राचमल्ल states:\
["For the deva स्व्यम्भू's personal gratification, theatrical entertainment of the population, offerings of food, restoration of worn out idols and temples, the chaitra and pavitra rites, vedic recitation, lectures on वैशेषिक philosophy, class readings from the shiva-dharma पुराण and feeding of the needy [ ]{style="color:rgb(51,255,51);"}]{style="color:rgb(51,255,51);font-weight:bold;"}[".]{style="color:rgb(51,255,51);"}

This suggests that the कालामुखस् were primarily nyAya-वैशेषिक proponents, and this formed the scaffold of their philosophical constructs. The sudden decline of nyAya-वैशेषिक both in the south and Kashmir appears to have coincided with the destruction of the कालामुखस् who appear to have been its last great expositors.

But at the same time the कालामुख पण्डितस् may be described as being electic and using the various doctrines in their argumentation as seen the inscription describing कालभैरव tatpuruSha munipati, a कालामुख priest who migrated to Karnataka from Kashmir:

["Through his intelligence that tat-puruSha munipati assumes the status of a bhairava to his opponents in debate. His terrifying trident is the मीमाम्स. He agitates the hearts of his proud opponents with the sound of his drum which the bauddha doctrine. He has the terrifying three eyes of the atomic doctrine (वैशेषिक), and the upraise skull brand of the साम्ख्य thought. He causes the interiors of the atmosphere and the sky to be defeaned by the sound of this huge bell which is the nyAya doctrine. "]{style="font-weight:bold;color:rgb(51,255,51);"}

But even here the main thrust is towards their favored doctrines with no mention of advaita or others.

वीरशैवस् appear to have merely adapted some of the prevalent superficial aspects of the कालामुखस् while actually revolting against them in their declining years. The वीरशैव attraction to the superficial and rhetorical aspects of the older layer appears to have acted as an impediment to any philosophical transmission from that layer. However, they preserved the high level of organization of the कालामुखस् and as result were able to gain considerable political mileage for their movement.

The lesson from the कालामुखस् leads me to a conjuncture: Strongly organized systems are best in propagating socially and politically oriented doctrines. They generally fail to preserve intellectual traditions over long periods, under volatile conditions, though they may be excellent in over the sh0rt to medium range.


