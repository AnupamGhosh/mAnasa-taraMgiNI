
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Preparing for the worst](https://manasataramgini.wordpress.com/2005/06/07/preparing-for-the-worst/){rel="bookmark"} {#preparing-for-the-worst .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 7, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/07/preparing-for-the-worst/ "5:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As we remarked earlier we were taking the last step before the कालछिद्र. We have taken it and now hang like त्रिशङ्कु in the median of descent.

We decided that shall boldly face the attack and die like Dattaji Shinde or emerge at the other end with what ever scars the battle gives us. Like ययाति we had fallen head long from svarga before. But now we did not know where our fall was taking us. We were leading a relatively easy life before, but now we were unexpectedly struck by the new missile. We were forced into a crisis before we realized what was happening. We had been specifically targeted at the weakest link in our armor. Our shachiva, अमात्य and the learned muni were all overthrown. The muni was learned and knew everything through the जानश्रुतेय birds but just as we were unable to help him in his crisis he was also unable to do so due his own engagments with the three. Our अमात्य and shachiva were too naieve for their posts and made many blunders and were pounded. We feared our shachiva may even neglect his current duties and compromise us futher- this was his nature. They have their own mantraic accomplishments which we have learned to respect despite its insufficiencies. We asked to take recourse to them in the last stand. But we were cornered precisely in weakest spots. Our friends who were always beside us in happy times had deserted us. We felt like Das and Dasgupta made to open on a Perth green top early on a dewy morning. We told ourself not to fear death and be prepared to face up for the worst of battles.


