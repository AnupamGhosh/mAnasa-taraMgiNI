
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The praise of वराह मूर्ती](https://manasataramgini.wordpress.com/2005/03/12/the-praise-of-varaha-murti/){rel="bookmark"} {#the-praise-of-वरह-मरत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 12, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/12/the-praise-of-varaha-murti/ "7:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[दंष्ट्रायां वसुधा सशैल-नगरारण्यापगा हुंकृतौ वागीशी शसितेऽनिलो रवि विधू बाह्वोश्च दक्षान्ययोः /]{style="color:rgb(255,0,0);font-weight:bold;"}\
The earth with its mountains, cities, forests and waterbodies are in his tusks, his grunts are सरस्वती (वागीशी). The air is his breath and the sun and moon are his right and left hands.

[कुक्षौ स्युर्वसवो दिशः श्रुतिपथे दस्रौ दृशोः पादयोः पद्मोत्थो हृदये हरिः पृथगमी पुज्या मुखे शङ्करः //]{style="font-weight:bold;color:rgb(255,0,0);"}\
His belly is \[made of] the vasus, in his ear are the directions, the ashvins are his eyes, in his feet is the lotus born \[brahma], in his heart is विष्णु and in his mouth is shiva.

This image is encountered on occasions in Hindu iconography with the gods constituting the body of the boar. A famous example is the Khajuraho वराह.


