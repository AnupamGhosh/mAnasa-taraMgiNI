
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The tale of the लिङ्ग](https://manasataramgini.wordpress.com/2005/11/22/the-tale-of-the-linga/){rel="bookmark"} {#the-tale-of-the-लङग .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 22, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/22/the-tale-of-the-linga/ "5:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The tale of the लिङ्ग as the phallus of shiva is found in several पुराणस् and is hence not a late concotion. It can be traced back to the भार्गव redaction of the sauta literature. The following पुराणस् state the story:\
ब्रह्माण्ड 1.2.27\
कूर्म 2.37\
skanda 1.1.6, 7.1.187 and other references\
वामन 22.6.65\
देवीभागवत 4.20\
shiva 4.12

The critical version of the core tale as can be reconstructed from the above sources goes thus:

The great god rudra, deep in sorrow over he loss of his consort sati at the conflagration in daksha's याग wandered into the देवदारु वृक्श vana. The भार्गवस् and angiras, with their women and children had made this forest their abode and performed their यागस् and tapasya. The mighty शङ्कर, wandered naked in this forest with a skull bowl. He performed अट्टहास by prancing about, yelling and laughing aloud like a madman. Seeing his naked form with erect phallus, the womenfolk of the atharvans were filled with amorous passion and rushed towards him. Seeing their wives thus excited the sages rushed out to discern the cause and saw the form of shiva before them. Infuriated the भार्गवस् and the angiras cursed, the deva: "May your लिङ्ग dettach itself and fall". Concomitant with the falling of महादेव's लिङ्ग there were several catastrophic upheavals in the 3 worlds. brahmA alarmed at this went to the lotus-eyed wielder of the drinker of jalandhara's life. मधुसूदन told him the cause for these events and they set out with the other devas to locate the phallus of नीलकण्ठ. Unable to locate it, they worshipped rudra, who told them that they would find it and the cataclysms would cease if the worshipped the linga. They fould the लिङ्ग and brahmA and विष्णु picked up the reddish-brown phallus but were unable to install. So they invoked उमा to bear it in the form of a vagina. Thus the लिङ्ग came into being as a symbol of rudra and the gods worhipped it. विष्णु initiated the four castes of men in the worship of the linga मूर्ति and promulgated the शास्त्रस् required for it.\
[दिव्य-लिन्गं नमामि। श्रिमन् महादेवाय नमः।]{style="font-weight:bold;font-style:italic;color:#0000ff;"}

It is of interest to note in this context that the veda of the भृगुस् and the angiras has a couple of सूक्तस् devoted to the un-maning of people by surgical methods.


