
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Another मारण strike](https://manasataramgini.wordpress.com/2005/08/27/another-marana-strike/){rel="bookmark"} {#another-मरण-strike .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/27/another-marana-strike/ "7:53 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The अमात्य had warned us that we would face a मारण strike anytime. Sensing trouble we deployed the arrows of sharva around us. The archers surrounded us completely to shield us from the कृत्या which was to strike. We were coursing in the grip of the कृत्या, without much apparent effect. But suddenly the ratha in which we travelled was smashed completely. We were glad that the archers had kept us alive and we escaped with life, though our ratha was completely smashed.


