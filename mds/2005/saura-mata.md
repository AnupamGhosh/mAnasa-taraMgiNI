
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [saura mata](https://manasataramgini.wordpress.com/2005/11/21/saura-mata/){rel="bookmark"} {#saura-mata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 21, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/21/saura-mata/ "4:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In बृहत्सम्हित 60.19 वराहमिहिर states that sUrya should be worshipped by installing idols of his and they should be worshipped by specialized priests called मगाचार्यस्. This is corroborated by the bhavishya पुराण chapter 139 that narrates the following tale (a critical reconstruction of it):

[कृश्ण, the hero of the yadus married जांबवति, the daughter of the bear-king जाम्बवान्. Their son was the valiant sAmba. He went to the banks of the river चन्द्रभाग and constructed temple in the honor of sUrya. No local ब्राह्मण knew of the mysteries of his worship and hence could not take up priesthood at the temple. So sAMba sought help of gauramukha, the adviser of the yadu chief, ugrasena. gauramukha asked him to go to shakadvipa and obtain a special class of priests called मगाचार्यस् to worship sUrya. saMba said:" pray, tell me Oh ब्राह्मण what are the antecedents of these worshippers of the sun. gauramukha narrated: "The first of the brahmins amidst the shakhas was called sujihva. He founded a gotra termed the mihira gotra. He had a daughter of the name निक्षुभा. sUrya was enamoured by her and impregnated her. Thus she gave birth to jarashabda who was the founding father of all the मगचार्यस्. They are distinguished by the sacred girdle called the avyanga that they wear around their waist". saMba there upon called on कृश्ण to send him garuDa and flying on his back he landed in shaka द्वीप. He collected the मगाचार्यस्, brought them back to bhArata and installed them as priests of his sUrya temple.]{style="color:#0000ff;font-style:italic;font-weight:bold;"}

The Idol of sUrya should be constructed thusly: He should have a human form with a solar corona placed behind him. He should be on a chariot with the horses standing for the seven solar rays. He should hold a discus and trident in two arms, and lotuses in the other two. His feet should be covered by boots upto the knees. His waist should bear the avyanga.

This temple on the chandrabhaga was situated in what is now the terrorist state of Pakistan and was demolished by Awrangzeb (may piss be upon him) in the 1600s. Earlier it was descrated by Mohd. bin Qasim and the al-Qaramitah. The descriptions of the original idol mention that it had knee-height boots made from Spanish leather and two rubies for the eyes.

There is considerable epigraphic evidence for the prevalance of the saura sect in India and definitely the cult was very popular at the time shankara भगवत्पाद formalized the six sects of sectarian Hinduism \[shaiva, shakta, vaiShNava, गाणपत्य, कौमार, saura] in the 700s of CE. The earliest pieces of evidence clearly support the Iranian connection. The coins of the कुशणस् have an image of sUrya with the inscription miiro. The ग़ोविन्द्पूर् inscription from the 1130s speaks of the magas as being brought to the land by sAmba and 6 great poets who were मगाचर्यस् are mentioned. Mihira Kula, the Hephalite Hun ruler sponsored the construction of another sUrya temple in Gwalior. This suggests that the Iranian influences on the saura sect were continous and over a long period of time. In Rajasthan and Northern Gujarat there were a number of sUrya temples including the well known one at मोढेरा. These contain idols of suryA with the boots up to the knees clearly implying the Iranian connection. Most of these temples were destroyed in the fine vandalistic traditions of the zealots of Allah. Priests with the sirname maga are seen around the sun-temple in Osian in Rajasthan suggesting that it was probably a famous center of the saura-s of Iranian origin. So it is clear that original home of the magas was indeed in the west and following the devastations of the al-Qaramitah and the Ghaznavids their remnants fled to the east and are now found there. Today the remnants of the मगचार्यस् are the शकद्विपी ब्राह्मणस् who are still present in Uttar Pradesh. While they are not accorded the same status as the Arya ब्राह्मणस्, they still observe basic brahminical rites, such उपनयनं and shaucha rules.

Analysis of the names of the queens of the Karkota dynasty of Kashmir shows that there were Iranians amidst them. They seem to have reinforced the saura cult in काश्मीर् as evidenced by the मार्ताण्ड temple in kashmir and a kashmirian idol of ललितादित्य's time with the classic Iranian dress.

However, it should be noted that the Sun cult also existed in Southern India. In Thanjavur there is the Suryanar temple with absolutely no Iranian influence in the iconography. This suggests that while the saura-mata itself spread widely across India, the maga priests and their distinctive iconography did not physically move into the more interior regions. I have heard that the sUrya temple in a hill temple complex built in Pune by the Peshwas was also consecrated by maga priests as late as 1750AD however, it is iconographically intermediate between the northern and southern forms. Finally, the sauras also tried to provide a "vedic tinge" for the sect via the composition of the सूर्योपनिषत् and the appropriation of the सविता gAyatrI.

The visible imprints of the saura mata that persist today are: 1) The highly popular Aditya हृदयं, which is attributed to agastya and has been inserted into the yuddha काण्ड of the रामायण. This hymn appears to be an early composition of the saura school. 2) The second well-known saura contribution is the sUrya नमस्कार vidhi which is a yogic/tantric practice prescribe by saura tantras.


