
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [You cannot get away](https://manasataramgini.wordpress.com/2005/08/20/you-cannot-get-away/){rel="bookmark"} {#you-cannot-get-away .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 20, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/20/you-cannot-get-away/ "8:40 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

GK had been a rival of R. R had genuine knowledge, but GK merely absorbed everything she read without much analysis or understanding. Otherwise they were quite equally matched. GK oozed arrogance, but R emanated genuine mental superiority over most. GK always had the ethnic advantage in both the lokas, but R was in the "foreigners" category. Still R was victorious over GK till the fourth year, after which she departed for the quest of "higher glory". GK had unbridled success after the departure of R and soon became a young physician. GK felt elated being at the peak of glory, when it is speculated that in the thrill of the arrogance of being the most glorious, GK killed a patient accidentally. GK was given a severe reprimand, but continued after retreating to a little village keeping a low profile. The खण्डवन्स् whom we had conquered after a battle last year had made acquaintence with GK. GK had apparently forgotten us just as we had forgotten GK.

One day our अमात्य was examining the fortification on the vayuputra hill when the minister came across agent 1 and 2 of K. Having accousted them, the अमात्य asked them casually some details about their current state, knowing well that K was a practioner of chakra-shambhara. They said that they had received the आनन्दोच्छुष्म from K. They then performed it and attained the state of पर्वतानन्द. They recommended GK to learn it from K. GK brought along the खाण्डवन् to see K. K was dangerous and not easy to approach without much effort. We had had an initial brush with K in days long past. MJ, अमात्य and shachiva had all warned us about K at that point. We saw the destructive power of K in the encounter code named the battle of Vectors. So, we sought the aid of the terrible rudra and repulsed K. \[We did not know much about दण्डयुद्ध or mallauddha then but ओSःअधि-vanaspati was at its height, so we feared little]. So when GK took the खाण्डवन्स् to the lair of K the trouble began. K's lair had a large grassy lawn with Pandanus fronds decorating the pathway. There were two large trees under which there were altars for the भूत prayogas. GK and the खाण्डवन्स् went there on a Wednesday evening. K asked them wait till K could complete preparing dinner. Then K came there after finishing dinner and asked them the purposes of their visit. When the खाण्डवन्स् narrated their defeat at our hands and the need for revenge, K's langorous eyes sparkled: " Now is the chance for our revenge. I shall personally intercede on your behalf. "

Sometime later the खाण्डवन्स् returned to K. "Everything is alright with the invading party" they said. You spells seem to have failed. K laughed and said: " Just wait and watch. This spell shall demolish that long standing foe of ours. Here shall issue the charm named 'the undefinable'. धुमावती has favored us since childhood as our close friend and no one escapes this duck messenger of the deity".


