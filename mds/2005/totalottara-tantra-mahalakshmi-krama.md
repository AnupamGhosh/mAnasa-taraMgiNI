
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [totalottara tantra माहालक्ष्मी krama](https://manasataramgini.wordpress.com/2005/10/10/totalottara-tantra-mahalakshmi-krama/){rel="bookmark"} {#totalottara-tantra-महलकषम-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 10, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/10/totalottara-tantra-mahalakshmi-krama/ "5:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The totalottara tantra is one of the important garuDa tantras of which fragments relating to special worship of महालक्ष्मी are preserved by tradition. It may be done on the night of the concluding महालक्ष्मी day during the sharada नवरात्री or दीपावली.

Of the great mantra of श्री नारायण is the ऋषि; महालक्ष्मी is the देवता

oM shrAM अङ्गुष्ठाभ्यां नमः /\
oM श्रीं तर्जनीभ्यां नमः /\
oM श्रूं मध्यमाभ्यां नमः /\
oM श्रैं अनामिकाभ्यां नमः /\
oM श्रौं कनिष्ठिकाभ्यां नमः /\
oM श्रः करतलकरपृष्ठाभ्यां नमः /

oM shrAM हृदयाय नमः /\
oM श्रीं shirase स्वाहा /\
oM श्रूं शिखायै वषट् /\
oM श्रैं कवचाय huM /\
oM श्रौं नेत्रत्रयाय वौषट् /\
oM श्रः अस्त्राय फट् /

oM सुवर्णपीठाय नमः /\
श्रीं ह्रीं महालक्ष्म्यै नमः //

The Asana and the idol are worshipped.

Then the great of महालक्ष्मी yantra is drawn with particles of sand that represent the atoms that constitute the universe.\
The yantra shall have the longest diagonals of an octagon and the end of each such sector shall have 8 spikes, thus with 64 radiating extremities and 8 sectors converging in the centre.\
A square sacrificial altar shall be set up and the oblations of ghee offered:\
In the four corners of the altar the oblations shall be made thus in clockwise order:

oM घं TaM DaM haM महालक्ष्म्यै नमः / स्वाहा // X n times

oM shrAM हृदयाय नमः / स्वाहा //\
oM श्रीं shirase स्वाहा / स्वाहा //\
oM श्रूं शिखायै वषट् / स्वाहा //\
oM श्रैं कवचाय huM / स्वाहा //\
oM श्रौं नेत्रत्रयाय वौषट् / स्वाहा //\
oM श्रः अस्त्राय फट् /स्वाहा //

oM श्रीं ज्ञानाय उदराय नमः / स्वाहा //\
oM श्रीं शकटये पृष्टाय नमः / स्वाहा //\
oM श्रीं बलाय बाहुभ्यां नमः / स्वाहा //\
oM श्रीं ऐश्वर्याय ऊरुभ्यां नमः / स्वाहा //\
oM श्रीं वीराय जानुभ्यां नमः / स्वाहा //\
oM श्रीं tejase चरणाभ्यां नमः / स्वाहा //

Thus the अङ्गस् of महालक्ष्मी are worshipped.

Then the following deities are offered oblations:

oM muM TaM दुर्गायै स्वाहा //\
oM gUM गणपतये स्वाहा //\
क्ष्म्रां क्षेत्रपालाय स्वाहा //


