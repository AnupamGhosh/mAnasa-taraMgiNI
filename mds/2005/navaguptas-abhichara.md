
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [navagupta's अभिचार](https://manasataramgini.wordpress.com/2005/03/17/navaguptas-abhichara/){rel="bookmark"} {#navaguptas-अभचर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 17, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/17/navaguptas-abhichara/ "7:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

शङ्कर भगवत्पाद had defeated the famous tantric navagupta of the पीठ of kAmarUpa in a debate. navagupta unable to come to terms with his defeat invoked चामुण्डा with oblations of twigs dipped in menstrual blood and the सूतक fluid. And thus he laid a spell on शङ्कर. The unerring अभिचार of the great goddess had its effect on the Acharya and he was afflicted by a terrible sore in his ass. As a result of this he was wasting away much to the dismay of his students. When pressed by them he revealed that it was a result of navagupta's incantations. His student पद्मपाद quickly got into action and invoked the most deadly spell of नृसिंह in the form known as the अष्टमुख गण्डभेरुण्ड उग्रनृसिंह. As result the spell of navagupta was back-hurled and it possessed him and killed him. The accounts state that the आचार्य had to worship कुमार at कुमार parvata to rid himself of the disease. It here that he composed the famous poem to the god in the भुजङ्ग meter.

[//ॐ क्षौं//]{style="color:rgb(255,0,0);font-weight:bold;"}


