
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [रामानुजाचार्य's encounter with the chera magicians](https://manasataramgini.wordpress.com/2005/04/26/ramanujacharyas-encounter-with-the-chera-magicians/){rel="bookmark"} {#रमनजचरयs-encounter-with-the-chera-magicians .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/26/ramanujacharyas-encounter-with-the-chera-magicians/ "5:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A chera magician, who was well-versed in the भूत and bhairava tantras had mentioned that his spells were often unable to penetrate anywhere near the vicinity of the अनन्तपद्मनाभ temple in the chera country.

Regarding this temple, one of my reliable sources offered a curious tale, though I have been unable to recover it from any other source. रामानुज, the arch-advocate of the qualified-advaita had gone to the regions of अनन्तपद्मनाभा temple. After observing the rituals he was unhappy with the use of rites of obscure tantric provenance, which did not adhere to the mainstream पाञ्चरात्र texts. रामानुज and his band sought to bring the temple ritual to conform to the पाञ्चरात्र norms. So रामानुज assembled learned scholars from Kashmir and the Dravida country to counter the chera magicians at the temple, and he was sure of victory. The cheras were not happy with the proceedings are said to have invoked a band of mighty चेटकस् that is said to have lifted the entire party during the night even as they were sleeping and cast them away somewhere. When they woke up they were shocked to see that they were lying in the outskirts of Kanchi. Never again did रामानुज visit the shrine after that.


