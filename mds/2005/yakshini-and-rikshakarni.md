
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [यक्षिणि and ऋक्षकर्णि](https://manasataramgini.wordpress.com/2005/10/14/yakshini-and-rikshakarni/){rel="bookmark"} {#यकषण-and-ऋकषकरण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 14, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/14/yakshini-and-rikshakarni/ "4:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having been trapped with the घोरप्रकृति we sought to escape on that rainy day. After the passage through the कालछिद्रस् the many paths and had been sealed and there was a single path winding through naraka. The old bystanders such as Dom, Vidrum, Damar and Blackie were all faintly heard from behind an oppressive screen of darkness. Some were crying as though being tortured by a turushka, others were laughing uncontrollably, as though they had imbibed bhang, others were yelling as though they were jostling in काम. On the sides of the path were rotting corpses and dermestid beetles making short work of them. There were skulls and femurs scattered around. There were jackals and hyaenas yelling out their moans. We kept walking like a पाशुपत had shut off his senses to the surroundings. There we saw a corpse of the most beautiful लोलनयना and then we passed the carcass of श्वेतमोहिनी which as being consumed by a वराह. Then we saw a crooked stump on which sat a वेताल which laughed horribly. Shortly after that the great gandharva विश्वावसु passed our path. He pointed to the crooked stump and laughed and flew away.

The we saw a slight clearing. There we stopped and saw the aweful ऋक्षकर्णि. We invoked her with the mighty mantra and said that blood was her offering. She was of nightly hue and had four arms. Her hair was tied up into an erect knot and she had 3 blazing eyes with the ears of a bear. Her breasts were rising and she caused all kinds of rasas to tingle the body. Then she extended her palm and from that arose a यक्षिणि. In the embrace of the यक्षिणि we flew to the higher realms.


