
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [gupta विद्या](https://manasataramgini.wordpress.com/2005/11/29/gupta-vidya/){rel="bookmark"} {#gupta-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 29, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/29/gupta-vidya/ "6:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[श्री प्रत्यङ्गिरा अथर्वण भद्रकाली परमभत्तरिका देवी]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}[![Posted by Picasa](https://i0.wp.com/photos1.blogger.com/pbp.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://picasa.google.com/blogger/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/hello/133/1300/400/pratyangira.jpg){width="75%"}
```{=latex}
\end{center}
```



We stood before that great idol of प्रत्यङ्गिरा before the most holy shrine of Tiruchengode. That is the idol filled with the power of अथर्वण हृद्या. When stands one before the deity one attains the most high mantra of the देवी.\
खडूरे .अधिचङ्कमां खर्विकां खर्ववासिनीं |\
ya उदारा अन्तर्हिता गन्धर्वापसरसश्च ye |\
सर्पा इतरजना रक्षांसि ||

The most fierce one manifests with the appropriate बीजस् added.

Indeed only when stand before her in that idol in Tiruchengode does the mantra come to you. dhyAna must completely internalize her and then the mantra may be realized at any time.


