
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [काल Chidra](https://manasataramgini.wordpress.com/2005/05/03/kala-chidra/){rel="bookmark"} {#कल-chidra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 3, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/05/03/kala-chidra/ "5:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The last step had been reached. The powers of the मानव had reached their end. Only the powers of the devas were next recourse. It is also called the setu- the bridge to the unknown loka. We had reached the end of the bridge- we stood at the door stead of the loka hidden beyond the impenetrable veil. Many fragments of thoughts crowded the brain- each having its own significance to us- we record them here for we are passing out that path, which the मानव takes only once in his existence.


 1.  Dattaji Shinde, the Maharatta hero was seriously wounded in the fated encounter again Qutb Khan and he fell into the hands of the Mohammedan. The Turushka asked him would he ever fight the Green flag again. Dattaji replied: "If I survive I shall fight again". Qutb cut off his head and kept it as trophy with himself. Dattaji's life has impressed me in many ways.


 2.  We had fought many battles in our existence till now. The devas had borne us aid in the most remarkable of these strifes- we had won many of these amazing battles, and we lost some. The great trophies from these campaigns of ours surrounded us- were we destined to take them with us when we took last the step on the setu ?


 3.  There was that peculiar dome with a shrine to ananta. There the श्री vaiShNava yogI had attained his siddhis in हठ yoga. Beside it resided the shudra who never worked but earned a lot of money. We being the dvija was ever puzzled by the shudra's way of living. But we understood the 4th वर्ण. We passed by and saw a two more shudras we observed them and learnt of their paths. The first of them accompanied us through thick and thin in the second Great War. At the end of it he was ruined, but we survived. Though R asked us not to help him we invoked the devas that the good shudra may rise back from his ruins. We left behind Igul, K, and many others like discarded vestures and performed the great tapasya that was to give us the glimpse of the mantra हृदय.


 4.  We gazed beyond and saw through our divya दृष्टि the days of the First Great War. Around us only stood traitors and mohanas, but we held our own and coursed through.


 5.  The greatest of our सखीस् was R: at some point we had transcended the need to talk with words. But on that bright day during the revelry after the Great War II (almost 15 years before today!!!!!) we met the wandering niShAda with his भूतचक्र. He declared our paths will fork soon and then converge but we will fork again two years after I cross the veil. When we thought of this time hung for a moment as though indra had stopped the rotating chakra to deliver his death blow on और्णवाभ. In our mind's eye we saw the comets that had passed- the great apparition of Halley- that marked our rise. The apparition of Wilson, the apparition of Hayakutake, that of Hale-Bopp, and that Macholz and many others. The supernova in Centaurus A and the apparition in Magellanic cloud. After the revelry we went to the southern city of the Pandian, even though were briefly held up by the Pandya Sena, and acquired a glimpse of the great धूर्त, उर्ध्वताण्डव bhairava, उग्रविष्णु, कुलयोगिनी and दण्डनायिका, which led us to the secrets of the tantrika perfection.


 6.  We felt like Ramaraya on the eve of Talikota.


 7.  We felt like arjuna the third पाण्डव on the night before the 14th day.


 8.  We had lived the life of the kShatriya, or more precisely like that of रामो भार्गव, always fighting at the front, with our bow knowing no rest. We knew not the life of the the settled city dweller.


 9.  Our enemies whose heads we forced to bow in submission with fiery darts, and false friends were now laughing at us.

We saw काल, as भृगु said: kAle .अयमङ्गिरा devo .अथर्वा चाधि तिष्ठतः |


 10.  We saw those corpses that we had alluded to in the past. They lay beyond on the other side of the setu, from which we had walked away . Some of those who lay dead there were our favored companions, others were our enemies. By the arrow of काल we had left them behind...


 11.  Thus we were still enjoying the पान from the चषक, when we passed through the काल Chidra. It was like shattered mirror: in the pieces we saw clothing, vessels and other things ...

Then I realized the transition of realms had happened- we were once enjoyers and now we were the substratum.

Oh अश्विना, when our ancestor च्यवान sought your aid you brought it to him. May now bear us aid. We have reached that last step of the setu that a मानव can- only daivya step can take one beyond...

  - iti परिसमाप्तं //


