
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The inversion](https://manasataramgini.wordpress.com/2005/02/25/the-inversion/){rel="bookmark"} {#the-inversion .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 25, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/25/the-inversion/ "5:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Jx had procured himself a cell phone. After some time सोमाख्य had also purchased a cell phone. Jx had all kinds of problems with his phone. Everyone who would look at Jx's phone would say: "Wow that is a great phone." All features we want is there in it they would say. But Jx would shake his head and say it just does not work. They would take his phone and place a call and ask their friends to call back and they all felt it worked fine. Yet, Jx would insist that it never worked. सोमाख्य found that his cell phone generally had many features, but he rarely had any use of them. Jx saw सोमाख्य's phone and he said that the ergonomic design of it perfectly fitted his hand shape. He said he was always in search of such feature and wondered why his phone did not have them. But सोमाख्य never understood what was so fancy in his phone.

There were two parrots on the tree named indradatta and विष्णुदत्त. One recited a chant: " The great indra always likes to write plays with ironic inversion " The other recited: "indra's sports are not understood by the mortals until they have enacted the whole play".


