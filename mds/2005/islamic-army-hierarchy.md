
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Islamic army hierarchy](https://manasataramgini.wordpress.com/2005/11/02/islamic-army-hierarchy/){rel="bookmark"} {#islamic-army-hierarchy .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 2, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/02/islamic-army-hierarchy/ "6:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Khan-1o Maliqs\
Maliq-10 Amirs\
Amir-10 sipehsalars\
Sipehsalara-10 Sarkhails\
Sarkhail-10 Gudsavars

