
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The व्रात्य stoma](https://manasataramgini.wordpress.com/2005/09/27/the-vratya-stoma/){rel="bookmark"} {#the-वरतय-stoma .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/27/the-vratya-stoma/ "4:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vratya stoma is described in the first 4 sections of the 17th chapter of the पञ्चविंश ब्राह्मण. The stoma has been described as a means of admitting non-Aryans to the Aryan fold but this is not clear from the vedic text describing the rite. A summary of the ब्राह्मण discourse is given below:\
The व्रात्य stoma's frame tale goes thus. The devas went to svargo लोकः. The गणस् of the dreaded The god were left behind leading the life of व्रात्यस्. They reached the spot where the devas had ascended to svarga but they neither found the stoma nor the Chandas by means of which they may join them. Then the devas told the maruts deliver the attendents of rudra the needful so that they may reach us. To them the maruts delivered that stoma with the 16 mantra chant which is the cryptic अनुष्टुभ्.

Those who lead the life of a व्रात्य are verily destitute or left behind because they neither practise the vedic study nor farming nor trade. It is by the stoma of the maruts that they prosper. For the rite he deploys the chant " adha hi indra गिर्वणः: For o indra who delights in the chants`(RV8.7-9)" which are in the kakubh Chandas. But the kakubh is an unequal Chandas for the सामन्स्. So he shall perform the shifting transformation on unequal padas of the kakubh to get the equalized verses for the song to the maruts known as the [द्यौतान सामन्]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}. When the गृहस्त chants the nidhana of this song it must end in the word "indra" musically uttered. The व्रात्यस् would be excluded from the prosperity if they were to use this nidhana. Hence they must replace indra in the nidhana of the song with the anirukta nidhana which goes thus with the following mystic stobha: au^2 3 ho^1 2345 ; i.e. falling from the highest pitch to the second last svara of the regular saman scale.

The sacrificial oblations to the maruts may be made with the series of chants with 4 stomas with 16 mantras. The structure of the chants in the rite with respect to the the number of mantras they contain is thus: [9, 15, 15, 15, 16 : 16, 17, 17, 17, 16 : 16, 21\
]{style="font-weight:bold;font-style:italic;color:rgb(102, 255, 153);"}\
This rite is supposed to expitiate those who have eaten abhojya food as though the ब्राह्मण's food, those who speak in corrupted sanskrit, who have perform unjust acts, who despite being व्रात्यस् speak in Chandas.

Thus, while there is an element of प्राय्श्चित्त in the prathama व्रात्य stoma does not seem to be for non-Aryans.\
[]{style="font-weight:bold;font-style:italic;color:rgb(102, 255, 153);"}


