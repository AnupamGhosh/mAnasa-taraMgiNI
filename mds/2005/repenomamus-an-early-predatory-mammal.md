
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Repenomamus an early predatory mammal](https://manasataramgini.wordpress.com/2005/01/13/repenomamus-an-early-predatory-mammal/){rel="bookmark"} {#repenomamus-an-early-predatory-mammal .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 13, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/01/13/repenomamus-an-early-predatory-mammal/ "2:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RnCeuU5tW7I/AAAAAAAAAKk/_sNLRSphtDg/s320/repenomamus_skull.jpg){width="75%"}
```{=latex}
\end{center}
```



```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RnCeuU5tW6I/AAAAAAAAAKc/2HZiSvXFh5c/s320/repenomamus_holotype.jpg){width="75%"}
```{=latex}
\end{center}
```



```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RnCeuE5tW5I/AAAAAAAAAKU/G6p9--fJvpI/s320/repenomamus2.jpg){width="75%"}
```{=latex}
\end{center}
```



A fossil of the early branching mammal [Repenomamus robustus]{style="font-style:italic;"} and related species [Repenomamus giganticus ]{style="font-style:italic;"}(may be even a larger version of the previous species) were recently described from fossil found in the Liaoning Province, China. [R.robustus ]{style="font-style:italic;"}had a juvenile [Psittacosaurus ]{style="font-style:italic;"}in its gut, suggesting it was one the earliest classical mammals of sufficiently large size to prey on other vertebrates. However, the mammals do not appear to have been major predators in the Mesozoic and [Repenomamus]{style="font-style:italic;"} may be more of an exception (?). However, the immediate ancestors of the mammals, Triassic pro-mammals were definitely larger active predators.


