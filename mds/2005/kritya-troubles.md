
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कृत्या troubles](https://manasataramgini.wordpress.com/2005/06/23/kritya-troubles/){rel="bookmark"} {#कतय-troubles .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/23/kritya-troubles/ "3:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The कृत्या made a couple of efforts at driving home the मारण and continues to hover around to strike again. Day after day the attacks continue without any respite and most critically they are aiming at cutting off our means of performing the appropriate mighty rites. Today like मारीच and सुबाहु obstructing the rites of विश्वामित्र the कृत्या blocked some critical rites of ours. The कृत्या faced the rebound of our rites and was grounded, but the senders had packed the घटोत्कच module into the कृत्या. How this works is that if the कृत्या is destroyed it would take the destroyer down along with it as it is burned out.

Along with us many of our dependents might be destroyed --- so we decided to warn them that they should look out for their lives in case we are destroyed. We did not warn them earlier out of the selfish reason that they may leave us and run away too soon, in case we did come out alive.


