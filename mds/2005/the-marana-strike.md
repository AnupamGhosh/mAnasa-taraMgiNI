
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The मारण strike](https://manasataramgini.wordpress.com/2005/06/16/the-marana-strike/){rel="bookmark"} {#the-मरण-strike .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 16, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/16/the-marana-strike/ "3:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The दुमावती agent has a very special means of attack. It tries to destroy all the supporting systems and armors, and then zeroes in on the victim. It pressed ahead with the attempt at मारण. We parleyed with the agent: it revealed a peculiar prayoga of the type only known to me previously in the bauddha context. The मारण was to indirectly achieved via a linkage to second person. This was similar to the fate of नगार्जुन when he met his end. The agent it trying to achieve this linkage and finish us off. The कृत्या hovers endless waiting to the first opportunity to make the lunge. We just fled away the first time the dart was hurled at us. The second dart now hit us but did not kill us due to the past mantra bala. Who knows when the next dart may come.


