
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [ashvina stuti](https://manasataramgini.wordpress.com/2005/05/30/ashvina-stuti/){rel="bookmark"} {#ashvina-stuti .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/05/30/ashvina-stuti/ "3:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The adi parvan of the महाभारत provides a remarkable "fossilized" सूक्तं to the ashvins. It comes in the 3rd chapter of the adi parvan known as the पौष्य section (1.3.60 in the critical Poona edition of the महाभारत). This section is a particularly interesting in terms of retaining some of the archaic structures of the epic, probably coming unadulterated from the original form of the jaya epic. Its frame narrates the tale of how janamejaya, the kuru emperor, and conqueror of तक्षशील came to perform his infamous sarpa यज्ञ to avenge the killing of his father परीक्षित् by the nAga chief. From this frame bud off many archaic tales, one of which is the tale of trial of the sage upamanyu of the clan of the वसिष्ठस्. In this tale, upamanyu as a young student in the ashrama of his teacher Ayodo dhaumya was particularly well-fed in his appearance. dhaumya cuts off his means of food, namely from the alms received in the city or from the cows he took to graze, by instructing him not to eat or drink any of those things. In this state dhaumya asked upamanyu to go and graze his cows. upamanyu without any food eats the leaves of the arka plant and as a consequence goes blind and falls into a pit while crawling about. Upon seeing his absence at sunset that day, dhaumya and his students set out in search of upamanyu and call out to him in the pastures. They hear him crying out from a pit and mention his blindness. dhaumya asks him to invoke the ashvins. He immediately composes a series of ऋचस् with which he praises the ashvins. As a result they are pleased and give him an अपूप cake. upamanyu states that he will eat the cake only after offering it to his teacher. They urge upamanyu to do so without offering him, but he refuses. Pleased with his devotion they relieve him of his blindness and the twin gods also give him a denture of gold, unlike the steel denture they had given his teacher dhaumya. As a result dhaumya states that upamanyu has passed his trial and blesses him to be a great scholar of the vedas and the धर्मशास्त्रस्.

This ashvina सूक्तं of upamanyu (UAS) is a rather remarkable hymn in many respects:


 1.  It is unlike most other material in the महाभारत actually very close to the vedic metaphors and style, using a त्रिष्टुभ् based meter that is typical of vedic. For example the use of the term दासपत्नी i.e one powered by the demonic दास or vala is clearly a vedic usage not encountered elsewhere in later Sanskrit.


 2.  However, in its language it shows late features closer to the epic Sanskrit, particularly in the dvandva forms ending in --au as against the vedic forms with --A (e.g. नासत्या or अश्विना). Use of अरुण instead of उषा is also atypical with respect to the vedic formulations.


 3.  It alludes to several mythological motifs of astronomical provenance that are common in the veda but rarely found in those particular forms in the later Hindu mythologies. These include the important motif of the regeneration of the sun and dawns, which is strikingly expressed in multiple forms in this hymn as in the vedic hymns. These expressions include the ashvins leading forth the dawns, setting free the quail grabbed by the eagle, bursting of the mountain stronghold of vala to set free the days and waters and the swallowing of the embryo and its re-birth. Also present are the motifs of the wheel of time with 720 spokes (720 days and nights), with 12 spokes (12 months) and 6 seasons on the rim of the wheel. These are very similar to the metaphors used by दीर्घतमा औचाथ्य of the clan of the अङ्गिरस् in hymn RV 1.165. Another metaphor used in the UAS is that of the 360 cows bearing one calf (the sun), which in turn equated with the gharma offering to the ashvins with the uktha chant in the pravargya rite.

The whole setting of the upamanyu tale involves a mythography that recapitulates the sun regeneration myth often seen in the ऋग्वेद. upamanyu, like other ऋग्वेदिc characters saved by the ashvins, namely च्यवान (buried in a termite hill), परावृज and दीर्घतमा, had gone blind, and like antaka and atri, other protégés of the ashvins, had fallen in a pit. This is an allegory to sun being lost in winter and is made more direct by the mention that quest for upamanyu, who had fallen in the pit begins after the sun had set. Further his denture of gold given by the ashvins is also a cryptic allegory for the returning sunshine.

The UAS in not found in any surviving saMhitA of the veda and upamanyu is far removed in age from the hoary hymn composers of the ऋग् and atharvaN. Instead, it appears that it represents an intermediate hymn, a late vedic composition in the epic period. This suggests that hymn composition in the ऋग्वेदिc style continued well into the epic period. It is thus a rare sample of the class of "Vedic" compositions that happened in the period of composition of the original jaya and is one of the survivals of the actual religious structures of the jaya period, which are hidden under the heavily pauranicized extant महाभारत. We will also reinforce this point with another short hymn, the indra stuti of utanka the भार्गव in the same section of the महाभारत.

The frame tale of UAS also has a structure resembling the mythological fragments of the बृहद्-devata of shaunaka and ब्राह्मण texts, especially like those in the जैमिनीय ब्राह्मण. This suggests that a similar core set of ancestral mythological frame tales were reused both in the generation of ब्राह्मण texts and the इतिहास. These are likely to represent the original इतिहास-पुराण. The frame of the UAS suggests that it is a case where an actual history has been adapted to fit an influential pre-existing mythological motif. The weakening or disappearance of the sun in winter and its regeneration, along with the dawns and the release of the frozen waters, was a striking motif acquired in the northern homeland of the Indo-Europeans. Given the strong impression it left on the Aryans, it was reused in various contexts of act regeneration by the ashvins and other gods. This ancient memory was well-preserved in the language of myth and found its way in the narrative of upamanyu's trial and revival.

We provide below a translation and commentary on the UAS.

1.3\
***[प्रपूर्वगौ पूर्वजौ चित्रभानू गिरा वा शंसामि तपनावनन्तौ ।]{style="color:red;"}***\
***[दिव्यौ सुपर्णौ विराजौ विमानावधिक्षियन्तौ भुवनानि विश्वा ॥ ६०॥]{style="color:red;"}***

You have existed before the emergence of the sun, the first-born beings, of beautiful luster. I desire your help with this chant, oh blazing ones who are infinite. You are the course of existence what pervades existence, the heavenly birds of beauteous feather and are in everything and pervade the universe.

***[हिरण्मयौ शकुनी साम्परायौ नासत्यौ दस्रौ सुनसौ वैजयन्तौ ।]{style="color:red;"}***\
***[शुक्रं वयन्तौ तरसा सुवेमावभि व्ययन्तावसितं विवस्वत् ॥ ६१॥]{style="color:red;"}[]{style="color:rgb(255,102,102);"}***

You are golden birds, you are the future of existence, the truth, with good noses and victorious in battle. Having generated the sun, you weave the wondrous cloth of the year by means of the white thread \[of the day] and the black thread \[of the night].

***[ग्रस्तां सुपर्णस्य बलेन वर्तिकाम् अमुञ्चताम् अश्विनौ सौभगाय ।]{style="color:red;"}***\
***[तावत्सुवृत्तावनमन्त मायया सत्तमा गा अरुणा उदावहन् ॥ ६२॥]{style="color:red;"}***

[]{style="font-size:12pt;font-family:\""}Oh ashvin twins, endowed with auspiciousness, you set free the quail seized by the the strong grip of the eagle. Oh leaders indeed you are of good \[chariot] wheels and indeed un-deceivable, with your powers you go leading the dawns.

***[षष्टिश्च गावस्त्रिशताश् cha dhenava eka.n vatsam.h suvate ta.n duhanti .]{style="color:red;"}***\
***[नाना गोष्ठा विहिता एकदोहनास् तावश्विनौ दुहतो घर्ममुक्थ्यम् ॥ ६३॥]{style="color:red;"}***

Sixty cows and three hundred cows suckle one calf between them (1). The various cowpens together yield one milking, indeed this is the hot milk offering \[gharma in the pravargya pot] with the uktha chant milked by the ashvins.

***[एकाम् नाभिं सप्तशता अराः श्रिताः प्रधिष्वन्या विंशतिरर्पिता अराः ।]{style="color:red;"}***\
***[अनेमि चक्रम् परिवर्तते ऽजरम् मायाश्विनौ समनक्ति चर्षणी ॥ ६४॥]{style="color:red;"}***

There is one nave with 700 spokes fixed, also fixed to it are 20 more spokes (2). This spoked wheel, turns endlessly, you ashvins have set them together \[spokes] in motion.

***[eka.n chakra.n vartate द्वादशारम्.h pradhi षण्णाभिमेकाक्षममृतस्य धारणम्.h.]{style="color:red;"}***\
***[यस्मिन्देवा अधि विश्वे विषक्तास् तावश्विनौ मुञ्चतो मा विषीदतम्॥ ६५॥]{style="color:red;"}***

The one wheel with twelve spokes, and the circumference with the six \[season] rotates around the one axle without an end (3). Upon this the gods are all stationed, Oh ashvins free me from the influence of the toxin.

***[अश्विनाव्-इन्द्रम्-अमृतं वृत्तभूयौ तिरोधत्ताम्-अश्विनौ दासपत्नी ।]{style="color:red;"}***\
***[भित्त्वा गिरिमश्विनौ गामुदाचरन्तौ तद्वृष्टमह्ना प्रथिता वलस्य ॥ ६६॥]{style="color:red;"}***

Oh immortal ashvins and indra, you twins who generate the rotations, oh ashvins remove those powered by the demonic dasas. Oh ashvins, burst the mountain stronghold of the demon vala, and go fort to bring out the waters and spread out the day light.

***[युवां दिशो जनयथो दशाग्रे समानम् मूर्ध्नि रथया वियन्ति ।]{style="color:red;"}***\
***[तासां यातमृषयोऽनुप्रयान्ति देवा मनुष्याः क्षितिमाचरन्ति ॥ ६७॥]{style="color:red;"}***

In the beginning, you twin youths generated \[space] with ten directions; then you set the chariot moving in the \[sky] above (4). The ऋषिस्, according to the course of the same \[sun], perform their sacrifices, and the gods and men, accordingly occupy their respective stations.

***[युवां वर्णान्विकुरुथो विश्वरूपांस् तेऽधिक्षियन्ति भुवनानि विश्वा ।]{style="color:red;"}***\
***[ते भानवो। अप्यनुसृताश् चरन्ति देवा मनुष्याः क्षितिमाचरन्ति ॥ ६८॥]{style="color:red;"}***

Youths! From the \[basic] colors, you have produced all the various forms; it is from these objects that the entire universe is filled. Ordained by these rays of \[color] the gods and men occupy their respective stations.

***[तौ नासत्याव्-अश्विनाव्-आमहे वाम् स्रजं च याम् बिभृथः पुष्करस्य ।]{style="color:red;"}***\
***[तौ नासत्याव्-अमृताव्-ऋतावृधाव् ऋते देवास्-तत्-प्रपदेन सूते ॥ ६९॥]{style="color:red;"}***

You नासत्यस्, ashvins I worship you two, who wear garlands and bear lotus flowers. You नसात्यस्, the immortal ones, the upholders of the ऋत, you impel the ऋत through which the devas attain their \[stations].

***[मुखेन गर्भ।न् लभताम्।ह् युवानौ गतासुरेतत्प्रपदेन सूते ।]{style="color:red;"}***\
***[सद्यो जातो मातरमत्ति गर्भस्तावश्विनौ मुञ्चथो जीवसे गाः ॥ ७०॥]{style="color:red;"}***

The embryo has been taken through the mouth, oh youths impel this one who has expired to obtain \[his station]. The mother, eater of the embryo, has brought it out (5); Oh ashvins release me and bring to life.

**Notes**

\(1\) 360 cows are the days of the year giving birth to the calf, the sun. The allegory here is thus: At the end of 360 days, the year, the sun has declined or disappeared at the winter solstice. The sun is then born in the new year as the calf. In the same ऋक् the pravargya gharma, which represent the sun is also mentioned.

\(2\) 720 days and nights of the year are allude to here.

\(3\) The twelve fold division of the year- the 12 vedic months, and the 6 seasons are allude to here.

\(4\) An allusion to the sun's chariot.

\(5\) The metaphor suggests the embryo, implying the sun, is being eaten and born again at the year beginning.


