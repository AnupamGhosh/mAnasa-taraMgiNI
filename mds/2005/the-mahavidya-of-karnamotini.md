
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The महाविद्या of कर्णमोटिनि](https://manasataramgini.wordpress.com/2005/12/07/the-mahavidya-of-karnamotini/){rel="bookmark"} {#the-महवदय-of-करणमटन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 7, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/07/the-mahavidya-of-karnamotini/ "7:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[[श्री कर्णमोटिनी देवी]{style="font-weight:bold;font-style:italic;color:#ff0000;"}[![Posted by Picasa](https://i0.wp.com/photos1.blogger.com/pbp.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://picasa.google.com/blogger/){target="ext"}]{style="font-size:130%;"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/hello/133/1300/400/कर्णमोटिनि2.jpg){width="75%"}
```{=latex}
\end{center}
```



Not far from the from the town of Tenali from where hailed the great jester of Vijayanagaran court, in direction of the other little town of Narasaraopetai, is the hamlet of Nandindla. It is here that my ancestors through my maternal grandmother roamed, a clan graced with number of notable shrauta sacrificers. One of that clan was a well-learned tantric had attained siddhi of numerous mantras and was deeply proficient in the mantra शास्त्र. In course of his secular duties he was wending his way towards another town, when he came across the spot, where the choLa governor of the Telugu country, known by the title, giripaschima-rAja, had held his court. During the high reign of the reign of king कुल्लोतुङ्ग, this governor had built a temple of the great deity कर्णमोटिनि and planted an Acacia tree in the compound. He attained the siddhi of the mantras of the deity. The place is now in ruins and in the grip of the practioners of adharma. Yet those of the clan that think of her see her fiery manifestation completely subduing all before them. The learned vaiShNava with whom we conversed in our native city told us of his remarkable journey to the most glorious Oddiyana Pitha, where he realized that great Mongol adept of the चीनाचार tantras, shri jahana vajra khaan had attained a siddhi of her mantra. Moving on to Mongolia he saw the idol of श्री महादेवी कर्णमोटिनि and having placed her image in his heart he he retraced his path to Oddiyana. In the high sthala of Oddiyana meditating on her in the midst of the mountain he attained the siddhi of her mantra.

He told us that the mantra ज्ञान of her mantra is hardly easy. But then he asked us to travel like the cloud of मेघदूत beyond the Jalandhara and shAradA पीठ and coursing through the welkin to reach the lands of Oddiyana in our minds eye. There the kiri-मण्डल of दण्डनायिका shone and having circumambulated it we came to the मण्डल of कौमरी. Having uttered her mantra we moved beyond and reached the आवर्ण of कर्णमोटिनि. We worshipped the देवीस्:\
चण्डा, घण्डा, कराली, सुमुखी, दुर्मुखी, रेवती, प्रथमा and घोरा in the वायु chakra Then we worshipped: saumyA, भिषणी, जया, विजया, अजिता, अपराजिता, महाकोटी and रौद्री\
Then we worshipped: विरूपाक्षी, parA, दिव्या, the आकाशमाता-s, सम्हरी, दंष्ट्राला and शुष्करेवती.\
Then we worshipped पिपीलिका, पुष्टिहरा, महापुष्टिप्रवर्धना, भद्रकाली, सुभद्रा, भद्रभिमा, सुभद्रिका, स्थिरा, निष्ठुरा, दिव्या, niSkaMpA and गदिनी.\
Then we received the nectar of the sUrya and soma through the iDa and पिङ्गल. Out of the cup of our skull the goddess drank the nectar. With that we entered the realm of कर्णमोटिनि.


