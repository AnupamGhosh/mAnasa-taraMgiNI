
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The location of Vijayanagara](https://manasataramgini.wordpress.com/2005/04/19/the-location-of-vijayanagara/){rel="bookmark"} {#the-location-of-vijayanagara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/19/the-location-of-vijayanagara/ "7:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The location of Vijayanagara [![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/hampi.jpg){width="75%"}
```{=latex}
\end{center}
```



That great capital of the Hindu revival- remarkably placed on the mid-stream of the life-giving Tungabhadra.

