
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The great chera magicians](https://manasataramgini.wordpress.com/2005/05/29/the-great-chera-magicians/){rel="bookmark"} {#the-great-chera-magicians .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 29, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/05/29/the-great-chera-magicians/ "1:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The नम्बूथिरि brahmins of the chera country are representatives of the earliest great wave of brahminical settlers of the deep Dravida lands. In the modern Tamil country their cousins, the choliars and the दीक्षितस् of Chidambaram represent small pockets of this wave of the early Aryans of South India. They are all members of the पूर्वशिख class of brahmins, who wear their shikha in the front, and separated from the mainstream Aryan core anywhere around 500-300 BC and proceeded southwards. In the Tamil country these brahmins have been largely overrun by the two notable later brahminical migrations: the बृहत्चरण or the great sect that established itself under the pallava patronage and subsequently the various lines of वडमस् or Northerners who came in starting in the choLa period and through the Reddi and Vijayanagaran regimes. Both of whom are also seen in the Andhra and कर्णाट countries and are aparashikha brahmins wearing their shikha at the back of the head. Amongst the नम्बूथिरिस् there were great exponents of three vedic संहितस्, shrauta sacrifices and the tantra.

While the tantric traditions of the chera country show remarkable isolation and archaic preservation in certain directions, in the medieval period the chera tantrics acquired a pan-Indian renown and were part of the country-wide movement to systematize the shrikula tantric material. An example of this was the great kaula tantric [पुण्यानन्दनाथ,]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"} a nambuthiri of the लोपामुद्रा (or हादिमत) lineage of श्रिविद्या who migrated to Kashmir and founded a major school there. He authored the famous text[ कामकला विलास ]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"}which propounds one of the most powerful forms of श्रिविद्या worship based on the योनीरूप. However, our main objective here is to provide a brief account of the greatest medieval tantrics of the chera country, on account of whose systematization of the tantra the weakly educated स्मार्तस् of the Tamil country have gained a great fear of the chera tantrics, whom they refer to as the chera magicians.[\
]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"}

The greatest of the chera magicians,[ ]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"}[छेन्नस् नरायणन् नम्बूथिरिपाड्]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);font-family:Arial,Verdana,Helvetica;"},[ ]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"}a remarkable individual, was born in 1428 in the chennas household in the modern Malappuram county. He was an extraordinarily versatile brahmin who acquired knowledge on a variety of topics at a young age. Firstly, he mastered the vedic and tantric lore, especially digesting an enormous amount of tantric literature and prayogas of various streams, including shAkta, पाञ्चरात्र, सप्तरात्र, भूततन्त्रस्, कुमारागम and शैवागमस्. नारयणन् also acquired mastery of sanskrit poetry and composition and is noted as one the greatest 18 sanskrit poets of the chera country. It is said that he was once visited by उद्दण्द shAstrI, the famed स्मार्त scholar from Kanchipuram, who asked him give him any topic and he would compose two sholkas on the same. The tale goes that नारयणन् gave उद्दण्ड an obscure astronomical topic on which उद्दण्ड could compose only 1.5 sholkas and was thus discomfited. It is also said that उद्दण्ड had defeated all the nambuthiris in सन्स्कृत् debates and thoroughly shamed them by stating the he the tiger from Kanchi had put the nambuthiri elephants of bad poetry to flight. So the nambuthiris took refuge with नारयणन्, who laid an अभिचार on उद्दण्ड and उद्दण्ड was consequently defeated by his own young nambuthiri student.

In the 1440-50s नारयणन् carried out numerous detailed observations on Mars, Saturn and Jupiter which was critical for other great nambuthiri astronomer नीलकण्ठ सोमयाजी to develop his planetary model. नारयणन् next contributed to the development of Hindu calculus providing the geometric form of the limit sin(x)/x=1: x->0. He used to show how the formula of the area of the circle may be formal derived as the area of a polygon n-\>infinity. He also extensively developed the use of a planar coordinate system equivalent to the Cartesian system in solving geometric problems. Here he was following the steps of वाचस्पति mishra, first seen in the latter's great work on वैशेषिक explaining atomic coordinates. He also provided solutions for several trigometric problems concerning tantric yantra constructions and Agamic 3D constructions. These problems include finding the length of the longest diagonals of an octagon given its perimeter or constructing a regular hexagon having the same area as a given square. He summed up his work on pratical 3D geometry in a treatise the [मानव वास्तुलक्षणं[ ]{style="color:rgb(255,0,0);"}]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"}on construction of temples.

He then wrote his elaborate compendium of the tantra, [[the tantra sammuchaya]{style="font-weight:bold;color:rgb(255,0,0);font-style:italic;"},]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"}providing all details on tantric temple worship as well as rites. He was succeeded by his illustrious son[ [शङ्करन् ]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}]{style="color:rgb(0,51,0);font-family:Arial,Verdana,Helvetica;"}[नम्बूथिरिपाड्]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);font-family:Arial,Verdana,Helvetica;"} who wrote another tantric treatise sheSha sammuchaya that covered all deities not covered by his father and the specialized rurujit shAkta temple protocols. These together became the basic manual for all chera magicians. In particular these renowned tantrics established highly secret धूमावती, भद्रकाली and बगलामुखी prayogas that are transmitted only within their fold. Most poorly educated स्मार्तस् of the Tamil and the Telugu countries have hardly any defense against these prayogas and can be easily blown away. A chera tantric of the line of नारायणन् and शङ्करन् performed one of his mighty कौमार prayogas for me, which can be considerably efficacious.


