
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Cell Phone sales](https://manasataramgini.wordpress.com/2005/02/09/cell-phone-sales/){rel="bookmark"} {#cell-phone-sales .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 9, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/09/cell-phone-sales/ "2:34 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We shall dialate upon the cell phone+digital camera sale saga when we get the chance later. But we must remark that we reached the last shop keeping cell phones at the end of a long and arduous trek. We did not find a phone cum camera contraption. But we bought the last cell phone that lay on the last shelf. We await our fate there after.


