
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Royal कुमार worshippers](https://manasataramgini.wordpress.com/2005/10/28/royal-kumara-worshippers/){rel="bookmark"} {#royal-कमर-worshippers .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 28, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/28/royal-kumara-worshippers/ "5:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[The Shunga coins:]{style="font-weight:bold;"} The late Maurya and Shunga period has the square coins of the Ujjain series bearing the image of कुमार. They installed some idols of कुमार in Mathura (175-50 BC).

**The Shakas:** The Shaka king Ayalisha (Azilises-I) has कुमार on his coin. Another Indianized Shaka mentions construction of a well in the name of skanda. The exact date of this inscription is unclear: mentioned as year 201 in some era.

[The Kushanas: ]{style="font-weight:bold;"}The कुमार coins, especially Huvishka's with the legends: skanda कुमार विशाख or skanda कुमार महासेन विशाख. Constructed षष्ठि and कुमार temples in Mathura (\~100 AD).\
[\
The yaudheya republic]{style="font-weight:bold;"} of Punjab/Haryana was administered by its leaders in the name of ब्राह्मण्य deva, कार्त्तिकेय. There are many yaudheya coins with 6 headed कुमार, कुमार and षष्ठि and one headed कुमार (100-330).

[इक्ष्वाकुस् of Andhra:]{style="font-weight:bold;"} several kings named after कार्त्तिकेय namely स्कन्दश्री, स्कन्दसागर and skanda-विशाख. One queen is named षष्ठिश्री. Associated with the इक्ष्वाकुस् were governors who were also कुमार worshippers. These included: skanda-शीतकिरण of the हिरण्यक clan, skandagopa of the पुष्यस्कन्दीय family, senapati कुमार the commander of the army stationed at मण्गलारणय. Their inscriptions states that they were devotees of महासेन and were able to build a temple to sharva due to the grace of कार्त्तिकेय (270-330 AD).

[The nalas of Orissa: ]{style="font-weight:bold;"}The nalas are a little known dynasty with considerable local power in Orissa ruling from their capital of पुष्करि (Near modern day Nawarangpur). In two of their surviving copper plates from Kesaribeda (rAjA arthapati) and Rithapur (rAjA bhavadatta-varman) we find the the header describing their line as "maheshvara-महासेनातिसृष्ट राज्य विभवः". Thus they believed their dynasty had come to power due to the grace of the great god कार्त्तिकेय. One of the kings in the dynasty whose name appears on some inscriptions is named skandavarman supporting their कौमार devotion (300-400 AD).

[gupta empire:]{style="font-weight:bold;"} कुमार and षष्ठि are shown in several gupta coins. Multiple gupta kings are named कुमारगुप्त (after कुमार) and one skandagupta (skanda), both the first कुमारगुप्त and skandagupta were great heroes of the Hindus, who protected India from the Huns. There is a at least one cave temple of the guptas which contains a giant image of कार्त्तिकेय (320-550 AD).

[Pallavas:]{style="font-weight:bold;"} The names of several pallava kings bear the epithets of कुमार. Their early major king was skandavarman the performer of many vedic sacrifices, including the horse sacrifice. His grandson was named skandashishya who expanded the Pallava domain. Following him there are many rulers with names like skandavarman and कुमार-विष्णु. Another king नृसिम्ह- varman राजसिम्ह of the pallavas compares himself in valor to कुमार the wielder of the shakti in an inscription in the कैलाशनथ temple in Kanchipuram. A pallava king nandivarman mentions a grant to a कौमार Acharya at a temple at Nellore (275-901 AD).

**Kadambas:** This brahminical dynasty arose after an arrogant Pallava horseman insulted the brahmin scholar [मयूरशर्मन् ]{style="font-weight:bold;font-style:italic;"}studying at the university in Kanchi. He took to war against the pallavas and founded his own kingdom in Maharashtra, Andhra and TN. One of his surviving inscription states that he was anointed राजन् by षडानन. His or his successor's Iranian general शाफर built a कुमार shrine (now lost) in Gandhara (now in the terrorist state). They took their name after the holy tree of कुमार and his female gana लोहितायनि-the kadamba plant. A king of this line had the name कुमारवर्मन् (345-565 AD).

[Chalukyas: ]{style="font-weight:bold;"}The plates of the चालुक्यस् chartered by कीर्तिवर्मन् indicate that कुमार was their patron deity and is said to have provided them with the boon of founding a long-lived dynasty (Mid-500 AD). विष्णुवर्धन I also mentions in an inscription that he was rendered prosperous by महासेन who had crushed the दानवस्. There are some Chalukya gold coins with कुमार icons known from 600s minted by the king विक्रमादित्य son of pulakeshin II and the famous कुमार temples of Bezwada, Humkarashankari and Chebrole built by yuddhamalla and other चालुक्यस्.

After this period कुमार suddenly goes out of vogue, especially in north India, and also over the large part of south India, except for the folk cult in the Dravida country. The early kaumara tantra's development corresponds to the period when there were several royal patrons of कुमार worship. These tantras were largely forgotten and lost there after. The revival of the technical kaumara srotas in South India occurred circa 900-1000 ACE when the कौमार tantrics from Bengal arrived in Bellary and established the कुमार tapovanas for the worship of the 6-headed deva.


