
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Things](https://manasataramgini.wordpress.com/2005/09/28/things/){rel="bookmark"} {#things .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 28, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/28/things/ "5:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Over and over the downward turn of the yuga was burning us. We were reviewing the reports from the unfair battle with Shoi on the पाषण्ड hill. We noticed that there were two major points that came up: 1) there was a signal known as "daiva" that inexplicably came up at a time when the troops thought all was hunky-dory. The shachiva was completely blind to this signal but the अमात्य saw it and moved troops in panic. 2) There was the signal known as "ओषधि-vanaspati" which we received at around the midday hour. We did not know whether to charge with out troops or to negotiate. We decided to negotiate to buy time hoping the अमात्य would strike a blow with his division and the muni may arrive with his reinforcements in the mean time. Our negotiations were very successful on the मूलक bridge and the bhutapati temple road. This gave us time to move our troops closer with the muni's reinforcements. But the अमात्य had panicked and we failed to complete the encircling move. We were just preparing for a thrust when Shoi saw the breach in the अमात्य's section and made a deadly thrust with the entire enemy decision. The अमात्य was wounded in the encounter. The शचीव and अमात्य sustained injuries when the turushkapura allies of Shoi made a surprise sally on their camps. Me and the muni escaped in time though. But the losses to the troops had greatly set us back.

But going back to the reports we are disappointed to see that we repeated the mistake when confronted by a similar trap. It is almost a Hindu aspect to commit the same military mistakes over and over again. Now with our troops reduced we were not clear as to what strategy to adopt.


