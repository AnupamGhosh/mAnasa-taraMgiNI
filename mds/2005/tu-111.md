
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [TU 1.11](https://manasataramgini.wordpress.com/2005/07/04/tu-111/){rel="bookmark"} {#tu-1.11 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 4, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/04/tu-111/ "4:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[वेदमनुउछ्यअछअर्योन्तेवअसिन्]{style="color:rgb(255,0,0);"}

<div>

[अमनुशास्ति ।]{style="color:rgb(255,0,0);"}\
After completion of vedic studies, the teacher admonishes his resident student:

[सत्यं वद । धर्मं छर । स्वअध्यअयअन्मअ प्रमदः ।]{style="color:rgb(255,0,0);"}\
Speak the truth; Follow your dharma; do not neglect your private\
recitation of the veda.

[आचार्याय प्रियं धनमाहृत्य प्रजातन्तुं मा व्यवच्छेत्सीः ।]{style="color:rgb(255,0,0);"}\
After you have you have given your teacher a decent fee, do not cut\
off your family line.

[सत्यअन्न प्रमदितव्यम् । धर्मअन्न प्रमदितव्यम्]{style="color:rgb(255,0,0);"}.\
Do not neglect the truth; do not neglect your dharma;

[कुशलान्न प्रमदितव्यम् । भूत्यै न प्रमदितव्यम् ।]{style="color:rgb(255,0,0);"}\
do not neglect your health; do not neglect your property/wealth

[स्वअध्यअयप्रवछनअभ्यअं न प्रमदितव्यम् ॥ १॥]{style="color:rgb(255,0,0);"}\
do not neglect your private and public recitation of the veda

[देवपितृकार्याभ्यां न प्रमदितव्यम् ।]{style="color:rgb(255,0,0);"}\
do not neglect your rites to the gods and past ancestors

[मातृदेवो भव ।पितृदेवो भव । आचार्यदेवो भव । अतिथिदेवो भव ।]{style="color:rgb(255,0,0);"}\
Treat your mother as a god; treat your father as a god; treat your\
teacher as a god; treat your guests as gods

[यअन्यनवद्यअनि कर्मअणि । तअनि सेवितव्यअनि । नो इतरअणि ।]{style="color:rgb(255,0,0);"}\
[यअन्यस्मअकꣳ सुछरितअनि । तअनि त्वयोपअस्यअनि ॥ २॥ नो इतरअणि ।]{style="color:rgb(255,0,0);"}\
You should only perform those rites that are irreproachable, and never\
other types of rites. You should hold in high esteem only the good\
practices you have observed in me (the teacher) never the other \[bad]\
practices

[ye ke chaarumachchhreyaa{\m+}so ब्राह्मणाः .]{style="color:rgb(255,0,0);"}\
[तेषां त्वयाऽऽसनेन प्रश्वसितव्यम् ।]{style="color:rgb(255,0,0);"}\
You should greet with honor any great/learned Brahmin who is superior\
to us \[in knowledge] by offering him a seat

[श्रद्धया देयम् । अश्रद्धयाऽदेयम् । श्रिया देयम् । ह्रिया देयम्]{style="color:rgb(255,0,0);"}[। भियअ देयम् । स।न्विदअ देयम् ।]{style="color:rgb(255,0,0);"}\
You should give \[donations] with good faith and never without faith.\
You should give with dignity, you should give with modesty, you should\
give with trepidation, you should give with comprehension.

[अथ यदि ते कर्मविचिकित्सा वा वृत्तविचिकित्सा वा स्यात् ॥ ३॥]{style="color:rgb(255,0,0);"}\
Now if you have a doubt regarding \[performance of a] rite or a legal matter;

[ये तत्र ब्राह्मणाः संमर्शिनः । युक्ता आयुक्ताः ।]{style="color:rgb(255,0,0);"}\
[अलुउxअअ धर्मकअमअः स्युः ।]{style="color:rgb(255,0,0);"}\
should there be experienced, qualified and considerate Brahmins\
devoted to dharma and who are learned to make a judgment in that\
matter;

[यथअ ते तत्र वर्तेरन् ।तथअ तत्र वर्तेथअः ।]{style="color:rgb(255,0,0);"}\
You should observe how they act in that regard and act likewise.

[अथाभ्याख्यातेषु ।]{style="color:rgb(255,0,0);"}\
Similar with regard to practices which may be subject to criticism,

[ये तत्र ब्राह्मणाः संमर्शिनः । युक्ता आयुक्ताः ।]{style="color:rgb(255,0,0);"}\
[अलुउxअअ धर्मकअमअः स्युः ।]{style="color:rgb(255,0,0);"}

should there be experienced, qualified and considerate Brahmins\
devoted to dharma and who are learned to make a judgment in that\
matter;

[यथा ते तेषु वर्तेरन् । तथा तेषु वर्तेथाः ।]{style="color:rgb(255,0,0);"}\
You should observe how they act in that regard and act likewise.

[एष आदेशः । एष उपदेशः ।]{style="color:rgb(255,0,0);"}\
This is the rule; this is the teaching;\
[\
एषा वेदोपनिषत् । एतदनुशासनम् ।]{style="color:rgb(255,0,0);"}\
This is the teaching of the उपनिषद् section of the veda; This is the\
admonition.

[एवमुपअसितव्यम् ।एवमु छैतदुपअस्यम् ॥ ४॥]{style="color:rgb(255,102,102);"}

You should perform rites thus, thus indeed should you worship.

Notes:

 1.  After finishing his yajur vedic education (of the तैत्तिरीय\
school) the teacher imparts the तैत्तिरीय उपनिषद् at the end.\
Before the student leaves teacher reminds him of some basic issues.\
These are the dvija must pursue: the quest for truth; the observance\
of dharma and the maintenance of vedic recitation.


 2.  Then the teacher reminds the student of his need to pay the teacher\
the due fees, and take care of his own health, property, dharma,\
public and private vedic recitations and appropriate treatment of the\
parents, teacher and guests. He also instructs the student stick to\
correct rites of the veda and not false ones, to emulate only good\
practices of the teacher, proper treatment of learned Brahmins and\
right attitude while giving donations to others.


 3.  Then the teacher provides the important rule regarding the\
situation of doubt about correct ritual and legal issues. If such a\
doubt were to arise the student is asked to emulate a considerate\
Brahmin who has full knowledge and understanding of the texts on the\
subject. Likewise if it were a matter that could arouse criticism the\
student is advised to follow such a Brahmin epitome.

</div>


