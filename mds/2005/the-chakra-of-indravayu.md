
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The chakra of इन्द्रवयू](https://manasataramgini.wordpress.com/2005/08/30/the-chakra-of-indravayu/){rel="bookmark"} {#the-chakra-of-इनदरवय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/30/the-chakra-of-indravayu/ "5:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/hurricane2.jpg){width="75%"}
```{=latex}
\end{center}
```



