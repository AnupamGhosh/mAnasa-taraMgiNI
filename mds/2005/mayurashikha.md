
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [मयुरशिखा](https://manasataramgini.wordpress.com/2005/03/03/mayurashikha/){rel="bookmark"} {#मयरशख .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 3, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/03/mayurashikha/ "7:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[ओम् सेनापतये नमः]{style="font-weight:bold;color:rgb(255, 0, 0);"}[![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/kumara.jpg){width="75%"}
```{=latex}
\end{center}
```



[ऐं हुं क्षुं क्लीं कुमाराय नमः //\
ॐ श्रीं ए इ उ न श्रीं रीं कुमाराय नमः //\
]{style="font-weight:bold;color:rgb(255, 0, 0);"}We worship the awful skanda praised by in the most glorious of secret tantras, embraced by the beautiful षष्ठि and holding a cock-banner, a shakti, a vajra and a trishula.

The most secret विद्यास् of the great god कुमार are revealed in the संहितास् of the skanda यामल also known as the सेनापति यामल. Having worshipped the goddess chatushpatha निकेता, the dreadful लोहितायनि, and महामयूरी, and then having propitiated स्कन्दापस्मर, shishu and the goat-head भद्रशाख I shall narrate the lineage of transmission of this विद्या:


 1.  तेजोधामन् saw the सेनापति's संहितास्, 2) from him पृथुशिरस् 3) from him to समदृश 4) from him to vishvabhuk 5) from him to विषवलयगोल 6) from him to the great bhairava 7) from him to his wife भैरवी 8) from her to विशाख 9) from him to कुञ्जरमुख the elephant-faced one 10) from him to sambhadra 11) from him to वीरभद्र 12) from him to उग्रचण्ड 13) from him to mahendra 14) from him to चन्द्रार्कवायुमुनि 15) from him to मनुगण 16) and from him to the world.

The great संहितास् of the सेनापति are: 1) गौहास्फन्दा 2) the अनलकान्थ 3) विषच्छदा 4) shaishava 5) the glorious मयूरशिखा 6) ramanodbhava 7) varada 8) लीलोत्पतमालिका.

The deity shall be mediated upon in the 6-fold yantra with the door-ways guarded by the goddess chatushpatha निकेता, लोहिताक्षि, लोहितायनि and वेतालोजननि.\
[ॐ चतुश्पथ-निकेतायै नमः //]{style="font-weight:bold;color:rgb(51, 255, 51);"}\
[]{style="font-weight:bold;color:rgb(255, 0, 0);"}


