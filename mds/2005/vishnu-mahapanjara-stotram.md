
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [विष्णु महापन्जर स्तोत्रं](https://manasataramgini.wordpress.com/2005/11/25/vishnu-mahapanjara-stotram/){rel="bookmark"} {#वषण-महपनजर-सततर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 25, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/25/vishnu-mahapanjara-stotram/ "6:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

namo-namaste गोविन्दं chakraM गृह्य सुदर्शनं |\
प्राच्यां रक्षस्व mAM विष्णो त्वाम्-अहं शरणं गतः ||

Salutations to govinda, who holds the सुदर्शण discus;\
I seek refuge in you, O विष्णु protect me from the east.

गदां कौमोदकीं गृह्ण पद्मनाभ namo.astu te |\
याम्यां रक्षस्व mAM विष्णो त्वाम्-अहं शरणं गतः ||

Salutations to पद्मनाभ, holding the kaumodaki mace;\
I seek refuge in you, O विष्णु protect me from the direction of yama (south).

हलमादाय saunande namaste पुरुषोत्तम |\
प्रतीच्यां रक्ष mAM विष्णो त्वाम्-अहं शरणं गतः ||

Salutations to the foremost being, with the ploughshare weapon and the sunanda club;\
I seek refuge in you, O विष्णु protect me from the West.

मुसलं शातनं गृह्य पुण्डरीकाक्ष रक्ष mAm |\
उत्तरस्यां jagannAtha भवन्तं शरणं गतः ||

Protect me holding the spiked pestle O lotus-eyed one;\
I seek refuge to the lord of the world in the north.

खड्गमादाय चर्ंअमाथ astra-शस्त्रादिकं hare |\
namaste रक्ष रक्षोघ्न ऐशान्यां शरणं गतः ||

Hari with the sword, shield, missiles and hand-combat weapons;\
Salutations to the to rakshas-killer protect I seek you refuge in the north-west direction.

पाञ्चजन्यं महाशङ्खं अनुद्बोधं cha पङ्कजं |\
प्रगृह्य रक्ष mAM विष्णो आग्न्येयां रक्ष सूकर ||

Grasping the great पाञ्चजन्य conch and anudbodha lotus;\
O विष्णु protect me, in the quarter of agni protect me O boar-formed one.

chandra-सूर्यं समागृह्य खड्गं चान्द्रमसं तथा |\
नैरृत्यां mAM cha रक्षस्व दिव्यमूर्ते नृकेसरिन् ||

Holding the moon and sun together, with the cresent moon scimitar;\
in the quarter of निरृति protect me, O one of blazing leonine form

वैजयन्तीं सम्प्रगृह्य श्रीवत्सं कण्ठभूषणम् |\
वायव्यां रक्ष mAM deva हयग्रीव namo.astu te ||

Wearing the vaijayanti necklace and the shrivatsa ornament;\
in the quarter of वायु protect me O god, salutations to हयग्रीव.

वैनतेयं समारुह्य त्वन्तरिक्षे जनार्दन |\
mAM रक्षस्वाजित सदा namaste .अस्त्वपराजित ||

Riding on vainateya coursing through the air O जनार्दन;\
protect me O unconquerable one, eternal salutations to the unconquered one.

विशालाक्षं समारुह्य रक्ष mAM त्वं रसातले |\
अकूपार नमस्तुभ्यं महामीन namo.astu te ||

Riding on विशालाक्ष in the रसातल nether world protect me;\
salutations to the great turtle; salutations to the great fish.

kara-शीर्षाद्यङ्गुलीषु satya त्वं बाहुपञ्जरम् |\
कृत्वा रक्षस्व mAM विष्णो namaste पुरुषोत्तम ||

You creating verily a protective cage with your hands for the hands, head onwards to fingers;\
protect me O विष्णु salutations to पुरुषोत्तम.


