
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [खाण्डवप्रस्थ निरीक्षणं](https://manasataramgini.wordpress.com/2005/03/30/khandavaprastha-nirikshanam/){rel="bookmark"} {#खणडवपरसथ-नरकषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/30/khandavaprastha-nirikshanam/ "7:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The critical time for खाण्डवप्रस्थ had come. We were seeing signs of revolt. We deployed the dual संग़्ण्य़ानं and कामं. We heavily rely on that. It had temporarily veiled the other party. But the true effects we will have to wait and see. We also need to spend a few days doing तर्पणं to स्त्री on लौकीक matters (I wonder why women are so exaggerated in their mundane लौकीक interests). Like the wandering yogI needing to please the गृहस्त्री or more precisely agastya मैत्रावरुणि needing to pacify लोपामुद्र vaidharbhi. The grand dance and drama had to be arranged to be played out by the नाटी who never learned नाट्य. To add to this enchanting Hayasthanika came with her "grandmother" asking for help on "हहहडक्षड". On other occasions I would easily been smothered by her great charm and done anything for her but with a more important स्त्री on the line Hayasthanika was asked to attend to her own business.


