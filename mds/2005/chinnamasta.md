
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [छिन्नमस्ता](https://manasataramgini.wordpress.com/2005/04/17/chinnamasta/){rel="bookmark"} {#छननमसत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 17, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/17/chinnamasta/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The below has some errors and is superseded by this [PDF](https://manasataramgini.files.wordpress.com/2005/04/chinnamasta-1.pdf)

OM छिन्नमस्ता महाविद्या महाभीमा महोदरी /\
चण्डेश्वरी चण्ड-माता चण्ड-muNDa-प्रभञ्जिनी // 4 //

OM one with a severed head, the great विद्या, the terrible one, one with a great belly;\
the terrible goddess, the frightful mother, the smasher of the demons चण्ड and muNDa.

महाचण्डा चण्ड-रूपा चण्डिका चण्ड-खण्डिनी /\
क्रोधिनी krodha-जननी krodha-रूपा कुहू कला // 5 //

Most terrifying, of terrible form, the terrifying one, the slaughterer of चण्ड;\
the wrathful one, the generator of wrath, of form of wrath, of form of the new-moon phase

कोपातुरा कोपयुता kopa-संहार-कारिणी /\
vajra-वैरोचनी vajrA vajra-कल्पा cha डाकिनी // 6 //

inciter of fury, conjoined with fury, the destroyer of fury;\
vajra-वैरोचनी, of the form of the thunderbolt, a manifestation of the thunderbolt and a डाकिनि (a companion of the goddess)

डाकिनी karma-निरता डाकिनी karma-पूजिता /\
डाकिनी सङ्ग-निरता डाकिनी prema-पूरिता // 7 //

One who delights in the acts of the डाकिनिस्, one who is worshipped by the डाकिनि rites;\
one who delights in the company of the डाकिनिस्, one who satisfies the love of the डाकिनीस्.

खट्वाङ्ग-धारिणी खर्वा खड्ग-kharpara-धारिणी /\
प्रेतासना preta-युता preta-सङ्ग-विहारिणी // 8 //

One who holds the skull-topped rod, with a severed head, bearing a sword and a cleaver;\
seated on a corpse, decorated by corpses, dwelling in the midst of corpses.

Chinna-muNDa-धरा Chinna-चण्ड-विद्या cha चित्रिणी /\
ghora-रूपा ghora-दृष्टा ghora-रावा घनोदरी // 9 //

Holding her own servered head, the terrible विद्या who destroyed चण्ड and the चित्रिणि nerve;\
of awful form, of awful appearance, uttering an awful cry, of a dense form.

योगिनी yoga-निरता japa-यज्ञ-परायणा /\
yoni-chakra-मयी yoniryoni-chakra-प्रवर्तिनी // 10 //

A योगिनी, delighting in yoga, one worshipped by japa and sacrifice;\
Of the form of the yoni yantra, the female organ, the one who stimulates the cycles of generation.

yoni-mudrA-yoni-गम्या yoni-yantra-निवासिनी /\
yantra-रूपा yantra-मयी यन्त्रेशी yantra-पूजिता // 11 //

Assuming the form of the finger pose of the yoni, emerging from the yoni, dwelling in the midst of the yoni yantra;\
of the form of the yantra, manifesting as yantras, the mistress of yantras, one worshipped by yantras.

कीर्त्या कपर्दिनी काली कङ्काली कलविकरिणी /\
आरक्ता rakta-नयना rakta-पान-परायणा // 12 //

Of great fame, with braided locks, the manifestation of time, decorated with bones, creater of melodies;\
the bloody one, with red eyes, the one pleased by the droughts of blood.

bhavAnI भूतिदा भूतिर्भूति-दात्री cha भैरवी /\
भैरवाचार-निरता भूत-bhairava-सेविता // 13 //

The wife of bhava, the giver of well-being, wellbeing personified, the giver of magical powers and the भैरवी;\
pleased by the bhairava tantra worship, served by ghosts and bhairavas.

bhImA भीमेश्वरी देवी bhIma-नाद-परायणा /\
भवाराध्या bhava-नुता bhava-सागर-तारिणी // 14 //

Terrible, the terrible goddess, the goddess, one pleased by terrifying sounds;\
worshipped by her husband bhava, praised by bhava, the one who makes us cross the ocean of saMsAra.

bhadra-काली bhadra-tanurbhadra-रूपा cha भद्रिका /\
bhadra-रूपा mahA-भद्रा सुभद्रा भद्रपालिनी // 15 //

The terrible भद्रकाली, of well-protected body, of the form of the bhadras [गणस् of वीरभद्र] and a female bhadra;\
Of the form of the bhadra, the great protectress, the well-protected one, protector of the bhadras.

सुभव्या bhavya-वदना सुमुखी siddha-सेविता /\
सिद्धिदा siddhi-निवहा सिद्धासिद्ध-निषेविता // 16 //

With great graciousness, with a gracious face, with a beautiful face, served by adept tantrics;\
the givers of siddhis, leading to the path of siddhis, adept in tantric powers, served by adept tantrics.

शुभदा शुभगा शुद्धा shuddha-सत्वा-शुभावहा /\
श्रेष्ठा दृष्ठि-मयी देवी दृष्ठि-संहार-कारिणी // 17 //

The giver of good, auspicious one, pure one, the pure sattva character and flow of auspiciousness;\
the famed one, the embodiment of vision, the destroyer of the evil vision.

शर्वाणी सर्वगा सर्वा sarva-मङ्गल-कारिणी /\
शिवा शान्ता शान्ति-रूपा मृडानी मदानतुरा // 18 //

The wife of sharva, all-pervading, everything, the giver of all felicity;\
The auspicious wife of shiva, the peaceful one, of the form of tranquil, the merciful one, the erotic one.

iti te कथितं devi स्तोत्रं parama-दुर्लभमं /\
गुह्याद्-guhya-तरं गोप्यं गोपनियं प्रयत्नतः // 19 //

Thus is the narrative of the praise of the goddess, which is extremely difficult to attain.\
Its the secret of the secret, hidden and zealously hidden

kimatra bahunoktena त्वदग्रं prANa-vallabhe /\
मारणं मोहनं devi ह्युच्चाटनमतः परमं // 20 //

What has in the beginning been told to you O companion of life;\
It is the primary mantra O goddess to achive killing, delusion or overthrowing of enemies.

स्तम्भनादिक-कर्माणि ऋद्धयः siddhayo.api cha /\
त्रिकाल-पठनादस्य sarve सिद्ध्यन्त्यसंशयः // 21 //

For paralyzing and related rites, for development and siddhis; recited it three times a day and without doubt it leads to the siddhis.

महोत्तमं स्तोत्रमिदं वरानने मयेरितं nitya mananya-बुद्धयः /\
पठन्ति ye bhakti-युता नरोत्तमा bhavenna तेषां रिपुभिः पराजयः // 22 //

Of greatest standing is this strotra O pretty-faced beloved, ever medidated by the learned, those who with devotion read this text become the foremost of men and with that conquer their enemies.


