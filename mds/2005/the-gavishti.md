
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The गविष्टि](https://manasataramgini.wordpress.com/2005/01/04/the-gavishti/){rel="bookmark"} {#the-गवषट .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 4, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/01/04/the-gavishti/ "7:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

O indra and विष्णु come bear us aid as you had done to the इक्ष्वाकु in the great गविष्टि for wealth and glory.\
None among men knows his own life's duration: bear us in safety over these through these battles.

Our foemen त्रिमित्वान्त्, बिडाल-smashru and vyuptakesha assembing their troops rushed towards us with their sword and upraised and spears pointed forward. We who had seen many grim battles, with arrows having lodged themselves in our armor, but having failed to slay us, marched forward for the final the showdown. त्रिमित्वान्त् approached us from the side with his javelin aloft. We saw him through the corner of our eye and moved ahead as though we did not see him. He was certain that we were in his hand and hurled his javelin at us. We were just waiting for this. We ducked and it flew over us and even missed out flankers and hissed into the ground. We suddenly wheeled around and lassoed त्रिमित्वान्त् with our pAsha. He was unhorsed and reeled to the ground. Our men struck him with their musalas and he was history. An arrow fired by बिडाल-smashru hit our helmet and glanced away. We shot back hitting him on his chest and reeled under the impact though his armor saved him. vyuptakesha shot our horse but its armor absorbed the shot. By then बिडाल-smashru had reached us with an upraised club. We dodged the heave and leveled a massive sword-blow at him. He narrowly escaped it and fled away with his co-conspirator, disappearing into the eastern forest.


