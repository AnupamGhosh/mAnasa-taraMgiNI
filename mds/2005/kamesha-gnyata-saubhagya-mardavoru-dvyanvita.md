
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कामेश-ग्ण्य़ात-सौभाग्य-mardavoru-द्व्यान्विता](https://manasataramgini.wordpress.com/2005/09/24/kamesha-gnyata-saubhagya-mardavoru-dvyanvita/){rel="bookmark"} {#कमश-गणय़त-सभगय-mardavoru-दवयनवत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 24, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/24/kamesha-gnyata-saubhagya-mardavoru-dvyanvita/ "5:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

advaita and सांख्य- are they really understood by modern Hindus. Perhaps not. The best means of really understanding the evolution of these old doctrines is via comparative analysis with the equivalent Greek works. When I suddenly reflected upon the old tAntric lines:\
कामेश-ग्ण्य़ात-सौभाग्य-मार्दवोरु-द्व्यान्विता I saw the power of महामाया, that entity called प्रकृति. I wondered where this stream of thought had emerged from. This took me back to the famed Greek sage Parmenides of Elea. Parmenides was a Pythagorean and a student of Xenophanes. Parmenides was a magician, medicine man and poet who was the founder of a remarkable school of Greek philosophical thought. His thoughts are summed up thus:\
The world is unreal but there is a reason why it seems to exist.\
He uses the term hapate to describe the world of sense experience. Hapate means illusion. Xenophanes used the term dokein-" to seem" from which Parmenides derived the word [doxa ]{style="font-style:italic;"}to describe the illusion. The world seems to exist due to this primordial identification with the illusion- [doxa]{style="font-style:italic;color:rgb(255, 0, 0);"}. This doxa is the goddess of illusion Ananke (like महामाया), who sits in the wheels and gears of illusion and runs them. She is described thus:\
[She it is who has charge of all the business of hateful birth and of sexual union, sending female to mix with male and again conversely male to female ]{style="font-style:italic;color:rgb(51, 255, 51);"}(Fr12.4 ff).\
[\
First of all gods she made Eros (Fr. 13)]{style="color:rgb(51, 255, 51);font-style:italic;"}

The comparison with the Hindu world makes the parallels apparent. At this point the question arises as to how these two related systems come into being.


