
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Neuquenraptor argentinus and the deinonychosaurs](https://manasataramgini.wordpress.com/2005/02/26/neuquenraptor-argentinus-and-the-deinonychosaurs/){rel="bookmark"} {#neuquenraptor-argentinus-and-the-deinonychosaurs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/26/neuquenraptor-argentinus-and-the-deinonychosaurs/ "7:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Neuquenraptor argentinus ]{style="font-style:italic;"}[![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/NEUQUEN.jpg){width="75%"}
```{=latex}
\end{center}
```



For a while the deinonychosaurs comprising two great clades: the dromeosaurs and the troodontids were known only from the Late Cretaceous of Northern hemisphere (Central Asia and North America]. They were the closest sister group of the birds, which were represented at that point by solely by [Archaeopteryx]{style="font-style:italic;"} from the Jurassic \[This was the scenario when I was young]. This temporal gap between the birds and their closest sister clade, the deinonychosaurs was considered mysterious by some workers and used by idiots who denied that birds were dinosaurs. However, the closeness of [Archaeopteryx ]{style="font-style:italic;"}to the deinonychosaurs was rather undeniable and this suggested a much earlier origin for the deinonychosaurs. Beyond this basic temporal issue there were several workers who even doubted the monophyly of the two major deinonychosaur clades. The past decade had brought a remarkable closure on several points concerning deinonychsaurus. Firstly, there was the discovery of [Rahonavis]{style="font-style:italic;"}, a primitive bird from Late Cretaceous[[ ]{style="font-family:Arial;font-size:100%;"}]{style="font-style:italic;font-family:Arial, Helvetica, sans-serif;"}Madagascar that showed a hyper-extendable pedal sickle-claw strengthening the theory that amongst the maniraptoran dinosaurs the deinonychosaurs were the closest to the birds. It also suggested that common ancestor of the birds and the deinonychosaurs possessed a sickle-claw characteristic of the later clade.

Then came the Chinese revolution in the form of the extraordinary fossils from various Early Cretaceous beds of the Liaoning province. The first two dromeosaurs from these beds [Sinornithosaurus ]{style="font-style:italic;"}and [Microraptor ]{style="font-style:italic;"}provided the first dramatic glimpses of feathers in dromeosaurs. Microraptor apparently had feathered fore- and hind-limb wings suggesting that at least some deinonychosaurs were flight capable, suggesting that at least limited flight adaptations may have even emerged in the ancestor of the avian and deinonychosaur clades. Subsequently the discovery of another dromeosaur, [Graciliraptor]{style="font-style:italic;"} provided the earliest definitive example of a dromeosaur dating back to the Early Cretaceous, thus minimizing the temporal gap between the earliest known dromeosaurs and avians and suggesting that the diversification of dromeosaurs was probably underway before the Cretaceous itself.

Meanwhile new data also emerged on the troodonitids. [Byronosaurus ]{style="font-style:italic;"}from Late Cretaceous of Mongolia provided a remarably well preserved representative of this group adding to the previously known [Saurornithoides]{style="font-style:italic;"}, [Sinornithoides ]{style="font-style:italic;"}and [Troodon]{style="font-style:italic;"}. The Chinese revolution also provided remarkable examples of troodontids, namely [Sinovenator ]{style="font-style:italic;"}and [Mei]{style="font-style:italic;"}. [Sinovenator ]{style="font-style:italic;"}was a primitive troodontid that once and for all settled the monophyly deinonychosaurs that was being doubted by some workers. [Mei ]{style="font-style:italic;"}was other primitive member of this clade that was found sleeping in a pose very similar to modern birds, with a head tucked under the fore-limb/ wing. This suggested that this sleeping behavior emerged before the divergence of the avian and deinonychosaur clade, in the very least.

The first hints that deinonychosaurs may have been found outside of the Northern continents was hinted by the discovery of a pedal claw resembling theirs from Sudan. However, this was hardly conclusive. Subsequently the discovery of [Unenlagia]{style="font-style:italic;"}, a fragmentary large dromeosaur from Argentina suggested a possible dromeosaur presence from the South. Finally, this week []{style="font-style:italic;"}[Neuquenraptor argentinus]{style="font-style:italic;"} was reported from Argentina. While quite fragmentary it covers\
fragments of cervical vertebra, dorsal ribs, haemal arches, left proximal radius, right femur and distal tibia, proximal tarsals, and most of the left foot. This represents an unambiguous dromeosaur from a Gondwanan continent for the first time. Phylogenetic analysis suggests that it forms a basal clade amongst the dromeosaurs, just like [Unenlagia]{style="font-style:italic;"}. Phylogenetic studies suggest that another clade in dromeosaurs is formed by the Chinese forms Graciliraptor, [Sinornithosaurus ]{style="font-style:italic;"}and [Microraptor]{style="font-style:italic;"}. Then the crown is formed by the dromaeosaurine clade ([Dromaeosaurus, Utahraptor, Achillobator, and Adasaurus]{style="font-style:italic;"}) and velociraptorine ([Velociraptor]{style="font-style:italic;"}, [Deinonychus]{style="font-style:italic;"}, and an unnamed Mongolian velociraptorine). This suggests that South America might have inherited the dromeosaurs an ancestral dromeosaur lineage in the Gondwanaland, prior to its break up. In the early Cretaceous of China their sister lineage appear to have spawned a radiation of Liaoning dromeosaurs and troodontids.

In conclusion, the deinonychosaurs were a remarkably long-lived group of dinosaurs, and their closest sister group the birds entirely comprise the only group of dinosaurs that survived the K-T extinction. Unlike the birds, which occupied a wide range of niches the deinonychosaurs appear to have mainly specialized as small gracile terrestrial or arboreal/flying predators. A few forms like [Achillobator]{style="font-style:italic;"}, [Utahraptor ]{style="font-style:italic;"}and [Unenlagia ]{style="font-style:italic;"}appear to have grown much large in size and assumed the role of mid-sized predators.

[[]{style="font-family:Arial;font-size:100%;"}]{style="font-style:italic;font-family:Arial, Helvetica, sans-serif;"}


