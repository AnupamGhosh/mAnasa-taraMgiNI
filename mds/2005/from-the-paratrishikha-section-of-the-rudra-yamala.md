
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [From the परात्रीशिखा section of the rudra यामल](https://manasataramgini.wordpress.com/2005/03/21/from-the-paratrishikha-section-of-the-rudra-yamala/){rel="bookmark"} {#from-the-परतरशख-section-of-the-rudra-यमल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 21, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/21/from-the-paratrishikha-section-of-the-rudra-yamala/ "6:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the Hindu world view ritual and philosophy are intimately linked. From the substratum of the shrauta ritual the ancient उपनिषद्स् arise. From the matrix of the tantras the highest kaulika thought emerges. Only one who can fully internalize this organic connection between the ritual perform and philosophy, can truly grasp the language and thought of the ancient Hindu. While the veda and the mantra-tantra शास्त्र may appear superficially very distinct they are unified by the same underlying entwinement of ritual practice and philosophical contemplation. They unified by the same discoveries of the ancient Hindu linguists and the geometric thought of the archaic Indo-Europeans. In one of the surviving fragments of the rudra यामल is seen the basis of kaulika thought.

The परात्रीशिखा section of the great rudra यामल (1-9)

श्रीदेवी उवाच:

The auspicious goddess said:

अनुत्तरं कथं deva/ सद्यःकौलिकसिद्धिदम् \
yena विज्ञातमात्रेण/ खेचरीसमतां vrajet/

O deva, how can the anuttara of its own grant kaulika attainment? By means of which, as soon as it is known, he would obtain the condition of sameness with the khechari?

etad गुह्यं महागुह्यं/ kathayasva mama prabho

O lord, my very self, tell me that mysterious secret great unconcealed.

हृदयस्था tu yA शक्तिः/ कौलिनी कुलनायिका/\
tAM me kathaya devesha/ yena तृप्तिं लभाम्य् अहं

Make known that shakti, which abides in the हृदय, known as kaulini, the mistress of the kula. O lord of the gods, reveal to me that by which I may obtain satisfaction, the "अहं"

श्रीभैरव उवाच

The lord bhairava said:

शृणु देवी महाभगे/ उत्तरस्याप्य् anuttaram/\
yad अयं kauliko vidhir/ mama हृद्व्योम्न्य् अवस्थितः

कथयामि na संदेहः/ सद्यः kaulikasiddhidam/

Hear, O देवी, the fortunate one, about the higher than the high, the highest. The kaulika function resides in the "vyoman" of my हृदय. I will describe to you the anuttara, so that no doubt remains, the anuttara, which grants the kaulika perfection.

अथाद्यास् तिथयः sarve/ स्वरा बिन्द्ववसानकाः \
तदन्तः कालयोगेन/ सोमसूर्यौ प्रकीर्तितौ/

All vowels from A to the bindu are the lunar digits, joined with time. The moon and the sun are said to shine within these.

पृथिव्यादीनि तत्त्वानि/ पुरुषान्तानि पञ्चसु \
क्रमात् कादिषु वर्गेषु/ मकारान्तेषु suvrate/

O chaste one, within the five classes of phonemes, from 'k' in the beginning to 'm' at the end, stand in order the 25 principles from earth to the puruSha.

वाय्वग्निसलिलेन्द्राणां/ धारणानां चतुष्टयम् \
तदूर्ध्वे शादिविख्यातं/ पुरस्ताद् ब्रह्मपञ्चकम्/

Beyond that group of 25 is the tetrad of supports, that is वायु, agni, अपः and indra. Beyond them are the phonemes beginning with 'sh', know usually as the pentad of brahman.

amUlA तत्क्रमा ज्ञेया/ क्षान्ता सृष्टिर् उदाहृता \
सर्वेषां chaiva मन्त्राणां/ विद्यानां cha yashasvini/\
इयं योनिः समाख्याता/ सर्वतन्त्रेषु सर्वदा

The process of manifestation, whose root is 'a'and whose end is 'क्ष', has thus been declared. Its course is to be known. O glorious one, this is proclaimed in all tantras as the source of all mantras and all विद्यास्, the giver of all.

चतुर्दशयुतं bhadre/ तिथीशान्तसमन्वितम्/\
तृतीयम् brahma सुश्रोणि/ हृदयम् भैरवात्मनः

O pretty-hipped one, the हृदय of the atman of bhairava is the 3^rd^ brahman united with the 14^th^ phoneme, O dear, and it is followed by the last of the master of the lunar digits.

Code:\
3rd brahman= sa\
14^th^ phoneme=au\
Last of the lunar digits=H\
The mantra= सौः


