
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The secret of सुमुखी](https://manasataramgini.wordpress.com/2005/04/07/the-secret-of-sumukhi/){rel="bookmark"} {#the-secret-of-समख .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 7, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/07/the-secret-of-sumukhi/ "5:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great deva सनत्कुमार, the son of the fiery agni, is always in the company of his two wives: the glorious [बिडालाक्षि]{style="font-weight:bold;font-style:italic;color:rgb(0,153,0);"}and the wonderous [सुमुखी]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}. The one who knows this rahasya and performs the invocation of सुमुखी along with the great धूर्तस्वमी will be favored by them. That glorious goddess सुमुखी also called the great महामयूरी, engaged in amorous dalliance with the धूर्त, is described as सुलभागति. In the midst of her yantra, the mate of कार्त्तिकेय is meditated as residing and invoked with the mighty mantra (in mantra शास्त्र code):\
[कर्ण ; सनयन द्युति; जरासन स्वेतेश ; लक्ष्मी ; दिर्घ नन्दि ; सदृक् क्रिय ; समाधव मेष ; कर्णो भृगु ; सेन्धिका तन्द्री ; खिदेवि म]{style="color:rgb(102,51,255);"}<[स्वयमरुप]{style="color:rgb(255,0,0);font-style:italic;font-weight:bold;"}>[; दीर्घ वियत् ; पिशाचिनी]{style="color:rgb(102,102,204);"}<[स्वयमरुप]{style="color:rgb(255,0,0);font-style:italic;font-weight:bold;"}> [; हिमाद्रिजा ; सर्गिनन्दजत्रितयं।]{style="color:rgb(51,102,255);"}

Thus is the 22 syllabled mantra given in code that can be deconvoluted by those who know.

Her yantra is a pentagon inscribed in circle with 8 petals on its circumference. Within another circle with 16 petals on its circumference enclosed by another circle. All enclosed within a भूपुर square.

Her five beautiful inner shaktis associated with the corners of the pentagon:\
[छन्द्र, छन्द्रानन, छारुमुखि, छमीकर-प्रभा अन्द् छतुरा।]{style="font-weight:bold;color:rgb(255,153,0);"}\
The 8 मातृकस् are associated with the 8 petals.\
Her 16 नित्यास् associated with the 16 petals. They are:\
[कला, कलानिधि, काली, कमला, क्रिया, कृपा, कुला, कुलीना, कल्यानी, कुमारी, कलाभाषिं, कराला, किशोरी, कोमला, कुलभूषणा अन्द् कल्पदा।]{style="font-weight:bold;color:rgb(255,255,102);"}

Her solo iconography is thus provided:\
The beautiful [सुमुखी]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}has necklace of गुञ्ज beads resting on her full breasts. She is in the prime of her youth with a joyous and mirthful face. She is holding a clean human skull in her left hand and a fine bladed sword in her right hand. She is bedecked with red gems, red garments and is with smeared red unguents. All these increase the lustre of her body which emanates a red radiance. She is comfortably seated on a corpse with a severed head.


