
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the sea of thought](https://manasataramgini.wordpress.com/2005/04/12/in-the-sea-of-thought/){rel="bookmark"} {#in-the-sea-of-thought .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 12, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/12/in-the-sea-of-thought/ "5:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We spent a lot of time in mirth with the whole assembly. The nice weather before the return of वृत्र gave a brief respite. We enjoyed it all- a good night of astronomy, a whole day of lolling around in the spring sunshine, talking about all and sundry. We lived in the present the worries of the past and the future, the दीर्घ-जिह्वीस् and their ilk were all forgotten as though they never were- as though erased by the intoxication of the माद्वीपानस्. We saw the लौकीक revellers and realized what had been termed by मानसिका as the "inversion". We then returned to the station by the श्मशान and wound our way back to our cave meditating on the slayer of हिरण्यकशिपु.

  - ------------\
The revolt of खाण्डव continued unabated. But we realized that the destroyer of प्रह्लाद, the demolisher of forts, the one with variegated bow had remained on our side during the difficult conquest of खाण्डव. As we studied the post-mortem intelligence reports on the assault we saw how close we had been to defeat and we were happy to see that the intricate defense plans had after all been successful in pulling off the victory. However, now we face and even more complicated situation that is probably going to be the real bang with no holds bared.

The new vyuha rachana: Front शरीरस्य ओषधिः: All covers blown. It may be a direct fight in the open, but this front has at this point diminished in importance because the enemy was partly delude by the mAya of विष्णु.\
Front kosha: It is no more about kosha and hima in the near future. It is all about दण्डयुद्ध with the दण्डायुध.\
Front dagda ratha: The unknown future- victory on this front is hampered by the problem called andhaka which is only worsening.

But the new encounter will have many new bigger parameters


