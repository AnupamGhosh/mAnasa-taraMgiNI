
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The wicks of भैरवानन्द](https://manasataramgini.wordpress.com/2005/03/08/the-wicks-of-bhairavananda/){rel="bookmark"} {#the-wicks-of-भरवननद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 8, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/08/the-wicks-of-bhairavananda/ "6:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having worshipped krodhabhairava and offered his arghya to शिखाशेखर, भैरवानन्द obtained the four wicks of अत्येन्द्रजाल. These wicks he handed over to the 4 seekers of wealth. This tale is all only too well known to all our compatriots.

Now there is another one. सोमाख्य had visited the 3: इशानशिव, vimalashambhu and उग्रताण्डवदेव. They gave सोमाख्य the wick of झङ्कारभैरव. सोमाख्य carrying that wick wandered widely for many years on the hills such as hanumat parvata, पाषण्ड parvata, दुर्गा parvata, उमा parvata and वेताल parvata. During these wanderings the only voice he heard was that of a talking goose. Finally after descending from the parvatas and suffering intensely in his travels he was walking along the plains of विमानस्थल when his wick suddenly dropped. सोमाख्य vigorously dug at the spot and obtained a load of copper. Carrying this copper with difficutly he placed at a shed in his dwelling. Then he visited vimalashambhu and asked what he may do with the copper. vimalashambhu answered: Perform yoga, you may be able make use of the copper.


