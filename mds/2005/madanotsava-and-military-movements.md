
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [madanotsava and military movements](https://manasataramgini.wordpress.com/2005/04/22/madanotsava-and-military-movements/){rel="bookmark"} {#madanotsava-and-military-movements .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 22, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/22/madanotsava-and-military-movements/ "5:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The madanotsava was observed today. कामदेव was duely worshipped with the appropriate rites. May manmatha be favorable to us.\
.....\
We faced an unexpected attack from the dasyus. We were initially put off but we realized that we would need a combination of रक्षोहा agni and the धूर्त patha again. May the धूर्त come to our aid and disperse the dasyus.

[कृणुष्व पाजः प्रसितिं न पृथ्वीं याहि राजेवामवां इभेन ।]{style="font-weight:bold;color:rgb(255,153,0);"}\
[तृष्वीम् अनु प्रसितिं द्रूणानो ऽस्तासि विध्य रक्षसस् तपिष्ठैः ॥]{style="font-weight:bold;color:rgb(255,153,0);"}

Forcefully casting your widespreading net, like a king impetously setting forth on his elephant,\
swiftly hurling your net, shooting your missiles, pierce the रक्षस् with your intensely hot flames.

[तव भ्रमास आशुया पतन्त्य् अनु स्पृश धृषता शोशुचानः ।]{style="font-weight:bold;color:rgb(255,153,0);"}\
[तपूंष्य् अग्ने जुह्वा पतंगान् असंदितो वि सृज विष्वग् उल्काः ॥]{style="font-weight:bold;color:rgb(255,153,0);"}\
\
Proceed rapidly flying, whirling your \[weapons], follow them closely, and attack blazing in fury.\
Spread with thy tongue the flying flames, O agni; unstoppable, hurl forth firebrands \[like meteors] all around thee.

[प्रति स्पशो वि सृज तूर्णितमो भवा पायुर् विशो अस्या अदब्धः ।]{style="font-weight:bold;color:rgb(255,153,0);"}\
[yo no dUre अघशंसो yo anty agne माकिष् Te vyathir A दधर्षीत् ||]{style="font-weight:bold;color:rgb(255,153,0);"}

Dispath your spies forward, the fast moving one, being undeceived, the guardian of this people \[the Aryas]. From him who, near or far, is bent on evil, O agni let no trouble sent over overcome us.

[उद् अग्ने तिष्ठ प्रत्य् आ तनुष्व न्य् अमित्रां ओषतात् तिग्महेते ।]{style="font-weight:bold;color:rgb(255,153,0);"}\
[यो नो अरातिं समिधान चक्रे नीचा तं धक्ष्य् अतसं न शुष्कम् ॥]{style="font-weight:bold;color:rgb(255,153,0);"}

Rise, O agni, blaze before us, one with sharp missiles, burn down our foes. Oh agitator of fuel, the one who has seeked harm us, smash down that one with your arrows, utterly \[burning that evil-doer] like dry hay.

[ऊर्ध्वो भव प्रति विध्याध्य् अस्मद् आविष् कृणुष्व दैव्यान्य् अग्ने ।]{style="color:rgb(255,153,0);font-weight:bold;"}\
[अव स्थिरा तनुहि यातुजूनां जामिम् अजामिम् प्र मृणीहि शत्रून् ॥]{style="color:rgb(255,153,0);font-weight:bold;"}

Rise up O agni, hit back against those who fight against us, putting forth your celestial might.\
Slacken the firm bows of the inciters of यातुस्, destroy our foemen whether from our people or foreigners.\
......\
Oh agni, as our ancestors, भृगु did, च्यवान did, kavi did, उशणा काव्य did, अप्नवान did, aurva did, स्यूमरश्मी did, nema did, ऋचीक did, jamadagni did, सोमाहूती did, rAma did, इटन्त् did, हिरण्यदान् vaida did, as वाजरत्न did, as somashushma did, अरिष्टशेन did, प्राचीनयोग did, we invoke you. With indra and विष्णु utterly destroy the dasyus stalking us, who Aryas and your sacrificers.


