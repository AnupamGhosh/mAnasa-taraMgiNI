
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Johannes Kepler](https://manasataramgini.wordpress.com/2005/08/14/johannes-kepler/){rel="bookmark"} {#johannes-kepler .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 14, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/14/johannes-kepler/ "5:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The German astrologer Johannes Kepler has always been a major source of inspiration. He lived a difficult life, full of personal troubles and disease, yet he was remarkably productive and innovative. This was a clear sign of his pure genius.

  -  In 1596 he announced his support for the Copernican model in the book [Mysterium Cosmographicum ]{style="font-style:italic;"}and showed its explanative power. It also contained his false polyhedral model of the solar system. He sent copies of it to Galileo, who acknowledge their worth but also plagiarized some of the material for this talks.

  -  In 1604 Kepler and his student Jan Brunowski discovered a supernova in Ophiuchus, which went to attain the magnitude of -2.5. Kepler provided the first description of the light curve of a supernova in the pamphlet [De Stella Nova]{style="font-style:italic;"}, observing it constantly for over and year till it fell below Mag 6.6

  -  He had been dabbling with ovals as possible orbital shapes and in the same year rejected that hypothesis. Towards the end of 1604 he felt was going to die and considered depositing the manuscript of the great work he had been preparing at the University of Tübingen. But he recovered the next year and in the spring of 1605 discovered the first law of planetary motion describing their orbits correctly as ellipses.

  -  In 1609 he published one of the most historic books ever, [Astronomia Nova]{style="font-style:italic;"}, in which he proposed the first two laws of planetary motion :

  - 1) Planets revolve around the sun in ellipses, with it at one focus.

  - 2) The radius vector of the planet sweeps out equal areas in equal time intervals.

  - In [Astronomia Nova]{style="font-style:italic;"} he also showed that the sun rotates about its axis inspired by suggestion of Giordano Bruno who had been executed by the Christians for ideas incompatible with their religion like belief in reincarnation. Galileo demonstrated this through direct telescopic observation of sunspots

  -  In 1610 in a pamphlet he coined the term satellite for the moons of Jupiter and explained their orbital motion. It was here that he presented the correct interpretation of what Galileo had seen --- the 4 "planets" associated with Jupiter. Indeed, when his friend brought him that news from Italy, he deduced that those must be moons of Jupiter as the Moon of the Earth.

* In his next great book *Astronomia Pars Optica* he presented the following discoveries:

  - He explained human vision by refraction within the eye and the use of both eyes for depth perception.

  - He explained how the pinhole camera produces images.

  - He devised lenses for vision defects explaining how they correct near and far sightedness.

* In his next book *Dioptrice* he presented the following discoveries:

  - The concept of real, virtual, inverted and upright images and the concept of magnification.

  - The model for a refracting telescope

  - the discovery of total internal reflection.

  -  In a letter he described the mathematical problem of packing of spheres and hexagonal symmetry.

* In his book *Stereometrica Doliorum* he presented the following:

  - The first foundations of Integral Calculus in the Western world in the context of deriving the formulae for volumes of solids of rotation.

  - He presented the first [de novo ]{style="font-style:italic;"}logarithmic tables in Germania, independent of the Briton Napier (who is usually credited with it, though he himself refers to the "Hindu logarithms" that he was sent from the Indies), and provided a mathematical proof for how logarithms work.

  - He explained tides using the pull of the Moon.

  - He presented the concept of parallax and suggested that the parallax caused by Earth's orbit could be used to calculate the distance of stars. He failed to actually do so due to observational problems. This had to wait till the young Friedrich Bessel calculated the distance of 61 Cygni using his keen observations.

* 1619 in his book *Harmonices Mundi* he presented the following:

  - The 3rd Law: The squares of orbital periods are directly proprotional to the cubes of the average orbital radius. This led to the inverse square law of gravitation, thus founding modern physics.

  - He discovered two new regular polyhedra- the small stellated dodecahedron and the great stellated dodecahedron.

  -  1621 He summed his astronomical and mathematical work in the seven volume[ Epitome Astronomiae. ]{style="font-style:italic;"}Here he also introduces the class of solids known as the antiprisms.

  -  We can see that he was very close to arriving at the inverse square law as we know it. In a discussion with the astronomer David Fabricius he pointed out that the "magnetic force" of the sun spreads itself out like light. Subsequently, he established that this "light's" intensity diminishes as the square of the distance. Finally he stated a version of the principle of universal gravitation: "If one would place a stone behind the earth and would assume that both are free from any other motion, then not only would the stone hurry to the earth, but also the earth would hurry to the stone; they would divide the space lying between in inverse proportion to their weights." (translation by Doris Hellman). Thus, he had basically provided all the ingredients for Newton to present it as the standard inverse square law (That needed a proper statement of the 1st law of Newton).

  -  In the Rudolfine tables he presented the means to calculate planetary positions for any past or future point in time.


