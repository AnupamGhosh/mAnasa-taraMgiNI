
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The path of the यक्षिनि](https://manasataramgini.wordpress.com/2005/07/25/the-path-of-the-yakshini/){rel="bookmark"} {#the-path-of-the-यकषन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 25, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/25/the-path-of-the-yakshini/ "6:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We flew up again with the यक्षिनि. There we stood in the mid-welkin and bewitching यक्षिनि whispered in my ear:\
"We need to talk about about a profound question".\
Me: What is it?\
Y: Of all those sexually reproducing beings out there, the majority of them are heterogamous There are some like Chlamydomonas that produce similar-looking gametes- the male and female gametes are the same. But the majority of cases have converged on clear anisogamy or heterogamy.\
Me: That is indeed so. Why so ?\
Y: Some have claimed that it is due to disruptive selection: The selection favors bigger gametes on one end of the spectrum because they can deliver more resource to the zygote and there by improve its survival (and there by their own fitness). Thus you get the egg. On the other end the more gametes you produce the more chance of propagating your genes. So your produce many little gametes- the sperm. Naturally you have a disruptive selection that favors both extremes and nothing in between because they will be less efficient in their chosen tasks. Thus, we get anisogamy.\
Y: But now tell me the problem with this reasoning.\
Me: Since most organisms start of isogamous the first guy who reduces his gamete size in the quest for making more gametes will deliver less resource to the zygote than an isogamous member. So reduction will be disfavored. This will kill the very emergence of anisogamy. So anisogamy will take place only if some way simultaneous you have guys going small and other correspondingly going big.\
Y: See these centric diatoms- they are oogamous- anisogamous to the extreme. How do you think this evolved.

Y: Do you think we know the answer to this issue? Think on oh मानव.


