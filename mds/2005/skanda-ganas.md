
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [skanda गणस्](https://manasataramgini.wordpress.com/2005/07/23/skanda-ganas/){rel="bookmark"} {#skanda-गणस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/23/skanda-ganas/ "5:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Weighed down by the arrows showered by our foes, we call upon son of the ranger of कैलाश. He who bears the bow in battle and showers arrows on the दानव horde. He who is called ugratamaa and भीमतम, who rushes to battle with the the axe and spear, we worship him in the midst of the six-fold मण्डल. One who is dallying with sumukhi and बिडालाक्षी, we bow to him.

The brahmins asked the सूत: Now, pray tell us the awesome deeds of that son of agni who is praised by the अथर्वण kalpa. The सूत continued: having bowed to the destroyer of the andhaka and his pretty-hipped wife I shall now narrate the deeds of the six-headed one. brahmA, the lokapita, said:

Oh महासेन, rudra combining with agni (as on the fifth day of the piling of the altar in the agnichayana) and उमा with स्वाहा has given rise to you who was suckled by the कृत्तिकस्. A part of the blazing semen of rudra bounced off the yoni of उमा and fell on the महापर्वत from where sprung the two terrible twin female and male गणस् मिङ्जिका and मूजिक. As one enters the आवर्ण of कुमार one must duely offer oblations to these गणस् as one does to विष्वक्सेन while entering the vaiShNava मण्डल. Another part of the semen glancing off the yoni of सुश्रोणी fell in to the रक्तार्णव. From it arose five terrifying भूत गणस् headed by वीरबाहू, who are the lords of the पिशाचस्. These became the attendents of कुमार. From the heart kartikeya emerged the ram-headed नेजमेष, also known as भद्रशाक, who flies around with great fury. He is worshipped with the secret ऋक्स् of the ऋग्विधान. From the left side of कुमार emerged the awful shAka who became the lord of several fierce ganas. From the right side of कुमार emerged the aweful विशाख who terrified the universe by his piercing yell. From agni emerged seven beautiful goddess named kakI, हलिमा, मालिनी with an elephant face, ब्रिन्हिला, आर्यदेवी, पलला and वैमित्रा. From them कुमार generated a terrible, fiery eyed, ferocious son named shishu. He joined the hordes of षण्मुख as a general. These aweful emanations of the six faced one are pacified with the तर्पण indicated by sage बोधायन in his kalpa.

Then the eagle-headed goddess of immense violence, vinAtA also joined the कुमार पार्शद with her horde of violent goddess. She was accompanied by the elegant smiling faced goddess chatushpatha-निकेता who arrived to aid कुमार with a band of horrible ghosts. From the body of कुमार emerged a mighty blazing genius who dropped listlessly to the ground. Agneya raised him up and made him band leader of some of his भूतगणस्, naming him स्कन्दापस्मर. He is terrible and worshipped with तर्पणस् and invoked to cause the enemy to hallucinate. Then, the snake-faced venomous goddess kadru whose hordes are invoked using the secret ऋक्स् known as the दृतराष्ट्रस् came to कुमार. She was followed by pretty goddess holding a skull in her hand, known as लोहितायनी, who had suckled the young skanda. She is offered तर्पणस् under the कदंब tree. With her comes two other pretty goddesses on either side known as रेवती and आर्यमाता. रेवती is accompanied by a horde of nasty genii known as raivatas. They may be invoked to make an enemy's clan extinct. आर्यामाता wreaks havoc with her berserk ghost troops, who joined कार्तिकेय's ranks.

Then कुमार installed the goddesses सरमा with the head of a bitch and सुरभी with the head of a cow. They were accompanied by their own भूत hordes. Then came आरण्यानि with a her hordes and she is offered तर्पणस् under the करञ्ज tree. sIta पूतना, in the form of a hideous lady, the mistress of the पिषचिस् also arrived to join guha's party. All these beings are invoked in the homas, तर्पणस् and abhishekas in connection with the worship of कार्तिकेय.

.........\
We salute आरण्यानि, who had saved our life when we were confronted with death about this time last year. We shall go to her grove again and call up the यक्षिनि now.


