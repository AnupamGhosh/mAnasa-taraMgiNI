
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Formal definition of the shrichakra](https://manasataramgini.wordpress.com/2005/04/09/formal-definition-of-the-shrichakra/){rel="bookmark"} {#formal-definition-of-the-shrichakra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 9, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/09/formal-definition-of-the-shrichakra/ "6:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[श्रिचक्र]{style="font-weight:bold;color:rgb(204,0,0);"}[![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/shrichakra1.jpg){width="75%"}
```{=latex}
\end{center}
```



The glorious yantra of shri ललिता-त्रिपुरसुन्दरी can be defined formally by the following parameters (given for a unit shrichakra):\
[YF=.668; XF=.126; YP=.463; XA= 0.187; YJ=.398; YL=.165; YA=.265; YG=.769; YV=.887; YM=.603; YD=.551 (where X, Y are the coordinates with the point A, F, J G etc with the origin at the intersection of the largest yoni triangle and the bounding circle)\
]{style="font-weight:bold;"}These define the exact shrichakra as laid down by कैवल्याश्रम.

The layers of the shrichakra are from outside in are:

 1.  trailokya-mohana chakra

 2.  सर्वाशापरिपुरक chakra

 3.  sarva शङ्क्षोभन chakra

 4.  sarva-सौभाग्यदायक chakra

 5.  sarvartha-साधक chakra

 6.  सर्वरकशाकार chakra

 7.  sarva rogahara chakra

 8.  sarva सिद्धीप्रद chakra

 9.  sarva Anandamaya chakra

The chakra is नवात्मा in two senses: 1) it is a sum of 9 layers. 2) it is a combination of 9= 4 लिङ्ग and 5 yoni triangles.\
[\
]{style="font-weight:bold;"}


