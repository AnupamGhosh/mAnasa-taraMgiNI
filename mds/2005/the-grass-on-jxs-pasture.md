
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The grass on Jx's pasture](https://manasataramgini.wordpress.com/2005/05/26/the-grass-on-jxs-pasture/){rel="bookmark"} {#the-grass-on-jxs-pasture .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/05/26/the-grass-on-jxs-pasture/ "4:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

"स्वर्गेण स्वर्गं"

There were two cell phones. One of mine and one of Jx. They were both mundane objects. There were numerous cell phones like that in the market. The one Jx possessed was very sleekly designed and was easy to use. The one I possessed was one which Jx coveted but I found very difficult to use. The Mumbai hecklers asked us: After all your mantra and tantra did your arcane gods only give you this cell phone? Before I could say anything Jx answered: What nonsense are you all talking. He has got such a wonderful cell phone- exactly like the one I wanted. The hecklers, who always believed Jx, were confused and did not know what to do or say. They kept mum.

The महातनु asked whether mine and Jx's cups had been interchanged like that of सत्यवती and queen of गाथी. I replied: Perhaps, but the main problem was that Jx mind had been fried by the भ्रातृव्य's prayoga, whereas in my case I was still hanging before the great unknown step like trishanku in his sphere.

.....\
I just realized a great mistake I was making. I was talking too much with Jx's neighbor when I should have instead spent that time more productively or instead spoken more with Jx himself. It was disappointing to realize this so late.\
.....\
A tantric used to tell me: You are a part of flow of the महाताण्डव साक्षिणी. You are part of the waves of her flow called the spanda of the emanations of mahat.


