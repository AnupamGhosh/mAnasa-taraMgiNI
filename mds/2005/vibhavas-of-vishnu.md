
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [vibhavas of विष्णु](https://manasataramgini.wordpress.com/2005/03/10/vibhavas-of-vishnu/){rel="bookmark"} {#vibhavas-of-वषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 10, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/10/vibhavas-of-vishnu/ "7:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vibhavas:\
padmanAbha\
dhruva\
ananta\
shaktIsha\
madhusudhana\
vidyAdhideva\
kapila\
vishvarUpa\
haMsa\
krodAtman\
vaDavaktra\
dharma\
vAgIshvara\
ekArNavashaya\
kUrma\
varAha\
nR\^isimha\
amR\^itAharaNA\
shripati\
kAntAtman\
rAhujit\
kAlanemighna\
pArijAtahara\
lokanAtha\
dattAtreya\
nygrodashAyin\
ekashR\^i\~Ngatanu\
vAmana\
trivikrama\
nara\
nArAyaNa\
hari\
kR\^iShNa\
rAmo bhArgava\
rAmachandra\
vyAsa\
kalkI\
pAtAlashAyin


