
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The lineage of the विष्णु tantras](https://manasataramgini.wordpress.com/2005/08/06/the-lineage-of-the-vishnu-tantras/){rel="bookmark"} {#the-lineage-of-the-वषण-tantras .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 6, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/06/the-lineage-of-the-vishnu-tantras/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[ॐ उग्रं वीरं महाविष्णुं ज्वलन्तं सर्वतोमुखं ।]{style="font-style:italic;color:#33cc00;font-weight:bold;"}\
[नृसिंहं भीषणं भद्रं मृत्यु-मृत्यं नमाम्यहं ॥]{style="font-style:italic;color:#33cc00;font-weight:bold;"}

How did these mightiest mantras of विष्णु come into being? Beyond पञ्चरात्र promulgated by the चित्रशिकण्डिन्स् are the tantras of विष्णु made apparent by the शाण्डिल्यस्. The root tantra of these is the glorious विष्णु यामल. From here stem the ugra विष्णु, हलायुध, ugrachakra and other formulations. After bhairava had been quenched by the draught of विष्णु's blood he asked सदाशिव to narrate the glorious विद्यास् of विष्णु. The lineage goes thus:\
सदाशिव-> कपालभैरव-> स्वच्छन्द bhairava-\>krodhabhairava-> उच्छुष्म-> ruru-> चण्डभैरव-> unmatta-> वीरभैरव-> अनन्तनाग-> भास्कर -> मन्थान -> नीलकण्ठ -> bhIma -> वामक -> महाकाल. Asked by भृगु it was revealed to him by विष्णु. And he revealed it to शाण्डिल्य.\
The core of the विष्णु यामल is made up of the following संहितास्:

 1.  त्रिमुण्डिणी

 2.  बहुरूप

 3.  उच्छुष्म

 4.  चण्ड

 5.  pronmatta

 6.  वीरभैरव

 7.  anantamata

 8.  अनन्तभास्कर

 9.  तृणक

 10.  anantavijaya

4 संहितास् are auxilliaries of anantavijaya know as : 1) bhImA with मारण and उच्छाटन rites; 2) वामा- the left-handed path of विष्णु; 3) महाकाली- नारायणि प्रत्यङ्गिरा and associated rites; 4) गृध्रपाती- the rites of garuDa.

The remaining विष्णु tantras that were taught by the शाण्डिल्यस् along with the विष्णु यामल are:

 1.  कालवदन-saMhitA which was enormous 21,000 shlokas

 2.  विष्नुपसंहित, with 6000 verses

 3.  sudarshana tantra, with 3500 verses


