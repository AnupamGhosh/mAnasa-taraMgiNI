
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Post-खाण्डवन् chaos](https://manasataramgini.wordpress.com/2005/08/14/post-khandavan-chaos/){rel="bookmark"} {#post-खणडवन-chaos .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 14, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/14/post-khandavan-chaos/ "4:28 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We paused for a moment looking out from our high perch into the foggy haze of the air around us. The joys of the world it appeared had deserted us. The devas had sent the brief respite, but it was all over. Why are we here we asked ourselves ?

Our mind raced back to the days of war between October-December of the year 2004. It was a fairly decisive period, but little did we know what the seizure of खाण्डाव had in store for us. Looking back at the correspondence of our अमात्य and shachiva, we realized our mistakes with 20/20 hindsight. It is now clear that we were slipping inexorably towards a deadly trap. In this situation is not surprising that the mind all recedes to the same period in theyear 2003. The smashing defeat we faced on the slopes of the पाषण्ड's hill had sowed the seeds for the future events. Our journey to the ruins of the ancient Jorwe cultures in Nevasa had made us wonder who were those people. Then we observed Mercury, low beside the horizon. At that point our shachiva was blinded and made some mistakes, which embroiled us in the खाण्डव campaign. But now we realized that the conquest of खाण्डव had brought us to a nadir in our history. Everywhere the army was shaking from the multi-pronged attacks, the state itself was in utter disarray. We felt that even though we were a much better warrior, we were not a better king than our sire.

We turn to indra for aid.


