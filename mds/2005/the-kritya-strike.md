
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कृत्या strike](https://manasataramgini.wordpress.com/2005/06/14/the-kritya-strike/){rel="bookmark"} {#the-कतय-strike .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 14, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/14/the-kritya-strike/ "3:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}


  - Our shachiva for the first time was shaken by the hit but could not do much useful at all. We still hold on to him as he has always been trustworthy at the last ditch attempt, which cannot be achieved without his abilities in that direction.

  - Our अमात्य made a valiant attempt with the mantras. We saw it take on the कृत्या, but it eventually lost and our अमात्य was beaten thoroughly.

  - The chera magician's cryptic spell ran thus: The brain of the tree's root was hacked off; the 2 older male fruits of the plant shall be consumed by the two कृत्यास् we have despatched.

  - R warned me that I was descending deep in to a pile of shit. It looks as though she was right.


