
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पणिनि's spirals](https://manasataramgini.wordpress.com/2005/07/30/paninis-spirals/){rel="bookmark"} {#पणनs-spirals .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/30/paninis-spirals/ "4:43 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/pbp.gif){width="75%"}
```{=latex}
\end{center}
```

{target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/spirals.jpg){width="75%"}
```{=latex}
\end{center}
```



