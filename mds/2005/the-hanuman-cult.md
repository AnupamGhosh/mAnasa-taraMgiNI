
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The hanumAn cult](https://manasataramgini.wordpress.com/2005/03/01/the-hanuman-cult/){rel="bookmark"} {#the-hanuman-cult .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 1, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/01/the-hanuman-cult/ "6:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The chief of the simians [![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/hanumat.jpg){width="75%"}
```{=latex}
\end{center}
```



The nilamata पुराण indicates that the cult of hanumAn emerged relatively early along with that of his master सुग्रीव. Soon, in the core Tantro-Pauranic effloresence he developed a major cult of his own and that सुग्रीव cult eventually became extinct. The tantric cult of hanumAn developed as a para-Vaishnava cult and eventually metamorphosed into a pan-Indian cult with considerably complexity. This full-blown complexity was in place by the CE 1500, though hanumAn was a late addition to the Hindu pantheon. He was actually subsuming within him the aspect of the vedic वायु and the maruts (which is linked to his description as a son or manifestation of rudra). We discuss briefly certain key aspects of ritualism pertaining to the hanumAn cult.

The first महामन्त्र of आञ्जनेय is:

[हौं ह्स्फ्रें ख्फ्रें ह्स्रौं ह्स्ख्फ्रें ह्सौं हनुमते नमः //]{style="font-weight:bold;color:rgb(51, 255, 51);"}

hanumAn is worshipped in the middle of the eight-petalled lotus. In the middle of each petal the वानर is worshipped with the following names:

 1.  रामभक्त 2) महातेजाः 3) कपिराज 4) महाबल 5) द्रोणद्रि हारक 6) मेरुपीठार्चन kAraka 7) दक्षिणाशाभास्कर 8) sarvavighna निवारक.

The on the tips of the lotus petals the following वानरस् are worshipped:

 1.  सुग्रीव 2) अङ्गद 3) नील 4) जाम्बवान् 5) nala 6) सुषेण 7) dvivida 8) mainda.

  - If one makes 1000 oblations of the mantra with bananas, mangoes and pomogranates, then feeds 22 ब्रह्मचारीस् then one can overcome भूतस्, taskaras and poisons.

  - If one recites the mantra 108 time holding a copper/silver pot with water and drinks it one can over come the ill-effects of venomous bites/stings.

  - If one recites the mantra 900 times at night for 10 nights then one can ward of fear of enemies.

  - If अभिचार has caused fever to a victim then sprinkling water and ashes (consecrated as above) vigorously on the victim provides relief in 3 days.

  - wounds from weapons, cuts, bruises and bites if smeared with clean ash consecrated 3 times by the hanumat mantra will favor quick healing.

  - From dusk to sunrise a tantric will mutter the mantra with ashes and a nail in his hand for a week. Then he will go an bury the ashes and nail in his enemies area. They will flee the place in terror.

  - one shall make a proper idol of the money chief with the south-east root of the भूताञ्कुष tree and perform a प्राणाप्रतिष्ठ to it. Then it shall be smeared with saffron and buried in front of the house chanting the hanumat mantra. This shall guard him from अभिचार, भूतस्, thieves, fire, poison, kings and disease.

  - One shall make an image of one's enemy in a crematorium at night from clay and write the foe's name on the heart. He shall do a प्राणाप्रतिष्ठ to it. Then chanting :\
[हौं ह्स्फ्रें ख्फ्रें ह्स्रौं ह्स्ख्फ्रें ह्सौं हनुमते नमः / द्विषन्तं भ्रातृव्यं*]{style="font-weight:bold;color:rgb(51, 255, 51);"}[छिन्धि भिन्धि मारय]{style="font-weight:bold;color:rgb(51, 255, 51);"}[[* or enemy's name] //\
]{style="font-weight:bold;color:rgb(51, 255, 51);"}and pierce the image with a spike. Then one shall bite his upper lip and and with ones palms press down the image and go home. If he does this for 7 days, मारुती will kill his enemy.

  - One shall repeat the mantra for 3 nights in a crematorium continuously and then a वेताल will come up to him. He can then capture the वेताल and make it carry out his behests.

Other महामन्त्रस् of hanumAn are:\
[ॐ ऐं श्रीं ह्रां ह्रीं ह्रूं ह्स्फ्रें ख्फ्रें ह्स्रौं ह्स्ख्फ्रें ह्सौं //]{style="font-weight:bold;color:rgb(51, 255, 51);"}

[नमो भगवते आञ्जनेयाय महाबलाय स्वाहा //]{style="font-weight:bold;color:rgb(51, 255, 51);"}

[ॐ यो यो हनुमन्त फलफलित धग धगित आयुराषपरुदाह //]{style="font-weight:bold;color:rgb(51, 255, 51);"}

[ॐ ह्रां ह्रीं ह्रूं ह्रईं ह्रौं ह्रः ॐ //]{style="font-weight:bold;color:rgb(51, 255, 51);"}

[ओम् वज्रकाय वज्रतुण्ड कपिल पिङ्गल ऊर्ध्वकेश महाबल रक्तमुख तडिज्जिह्व महारौद्र दंष्ट्रोत्कटक ह ह करालिने महादृढ प्रहारिन् लन्केश्वर वधाय महासेतुबन्ध महाशैल प्रवाह गगनेचर एह्येहि भगवन् महाबल पराक्रम भैरव आग़्ण्य़ापय एह्येहिमहारौद्र दीर्घ पुच्छेन वेष्टय वैरिणां भञ्जय भञ्जय हुम् फट् //]{style="font-weight:bold;color:rgb(51, 255, 51);"}


