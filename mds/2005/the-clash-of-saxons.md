
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The clash of Saxons](https://manasataramgini.wordpress.com/2005/08/28/the-clash-of-saxons/){rel="bookmark"} {#the-clash-of-saxons .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 28, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/28/the-clash-of-saxons/ "4:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great leader of the Southern Saxon, Pirate king Slit-Eyes had died in the battle with the Hindus during their invasion of the southern continent. Slit-Eyes was succeeded by pirate Ponting, who was ably supported by the old guard of the Slit-Eyes empire, such as Gen. Hayden, bombardier McGrath, raider-in-chief Warnie, captains Langer and Gilchrist, Bishop Martyn and other saboteurs, Clarke, Gillespie and Lee. The Southern Saxons in the heydays of Slit-Eyes and Pirate Emperor Border, had engaged in many raiding operations on the Saxon mainland, and had fought many epic battle even as the pirates of the Caribbeans. Warnie in particular had enriched himself on most of these raids with rich harvests of women and jewels and in the process led the pirates to many victories against the Mainland Saxons. The great pirates now embarked on a new expedition to mainland Saxony with a large force of fusiliers and bombardiers on their well made vessels. The Mainland Saxons recalling their own pirate Francis Drake decided to launch themselves into a spirited defence of their land. In the recent past, in a move to restore the pristine Leukospheric composition of their army, they did away with their earstwhile leader, the wily but unsuccessful Sultan Nasser Husseyn. He was replaced by the phelgmatic King Vaughan, master of all Saxony. He was ably aided by pirate Flintoff, who was believed to be a successor of bootlegger Botham in the heydays of Mainland Saxony.

In the first encounter the Southern Saxons under Ponting played with the Mainlanders as a cat with a mouse. In the next encounter that followed bootlegger Flinthoff slew Gillespie and hurled the Southerns in disarray. Raider Warne was still creating havoc, but he too was controlled by the Mainlanders this time. Thus for the first time in many eras it looked as though the Mainlanders might wrest control. But tommorow is the mother of all battles for the Saxon powers. Let us see what happens


