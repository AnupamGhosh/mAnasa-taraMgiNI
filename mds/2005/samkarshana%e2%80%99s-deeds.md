
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सम्कर्षण's deeds](https://manasataramgini.wordpress.com/2005/08/27/samkarshana%e2%80%99s-deeds/){rel="bookmark"} {#समकरषणs-deeds .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/27/samkarshana%e2%80%99s-deeds/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From the 67th chapter of the हरिवंश

When balarAma was cavorting with his wives and girlfriends and enjoying a pitcher of वारुणि beer, dvivida the mighty ape made obscene gestures. balarAma's eyes rolled in languor, he sang loudly and he moved like a male elephant in musth. The great ape was a student of सुग्रीव in the days of yore and had learnt great deeds in battle. The वानर was spreading much havoc and raping women of the yadu's feudatories in revenge for the killing of his friends from प्रग्ज्योतिष, naraka and mura. He would capture citizens of the yadu's feudatories and seal them in caverns by blocking the entrances with stones, like a wasp imprisons spiders. He was also smashing cow-pens, setting fire to villages and throwing firebrands down mine shafts in the Anarta province. In the west coast he churned up the water and created floods in the coastal villages. He also destroyed trees in the forests of the यादवस् and hammered the वनाध्यक्षस्. He then advanced to the shrauta yards of the garga भारद्वाजस् and other Brahmins and desecrated the sacrificial altars by urinating or defecating into them.

Thus, he arrived at the raivataka mountain where balarAma was stationed with his girls, and repeatedly vocalized the loud noise किलकिला. balabhadra's girls were humored by the ape, but he soon grew obscene. The ape then showed his ass in front of the girls' face. Infuriated balabhadra hurled a rock at the ape. The ape escaped the rock and plucked balabhadra's beer pitcher and smashed it to pieces and made fun of him. Then he tried to pull down the girls' garments and laughed loudly. balabhadra furious with this behavior, shook himself of his languor and picked up his हलायुध and his favorite musala, known as sunanda. dvivida armed with साल trunk, which he uprooted with one hand attack the yadu chief, and struck balarAma on his head. balarAma fell unconscious briefly, but quickly rallying back tore down the log with his हलायुध. Then swinging his musala he hit the ape on his head. Gashed on his skull, the ape was a like split mountain oozing red oxide of mercury. But the ape fought on pulling up another log and attacking the yadu hero. But संकर्षण destroyed that with his hala and then broke up the other trees the ape used on him. Run out of logs the ape attacked the माधव with boulders, but he powdered those rocks with his great musala. The ape then rushed at balabhadra with his fist clenched. rAma kept his weapons aside and fiercely shattered the collar bone of the ape. With that dvivida vomited blood and breathed his last.[]{style="font-size:10px;"}


