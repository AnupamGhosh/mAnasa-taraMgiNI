
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The odd Cambodian inscription of विनायक](https://manasataramgini.wordpress.com/2005/09/07/the-odd-cambodian-inscription-of-vinayaka/){rel="bookmark"} {#the-odd-cambodian-inscription-of-वनयक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 7, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/07/the-odd-cambodian-inscription-of-vinayaka/ "5:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Among the old inscriptions of the विनायक temples in the far-East are the following

 1.  The Angkor Borei inscription of 611 AD, which mentions the construction of a shrine to महागणपति.

 2.  660 AD the jayavarman II inscription describing a temple built to श्रीगणपति.

 3.  817 AD inscription at Po Nagar in Vietnam of harivarman, the Champa King mentioning the temple built to श्री गणपti (विनायक).

 4.  890 AD inscription of yashovarman I mentioning the building of two tantric Ashramas for the worship of विनायक known as चन्दनाद्रि गणेश (sandal mountain गणेश).

The most mysterious inscription is from Prasat Prei Kuk shrine in Cambodia to विनायक built by king इशानवर्मन् I.

[य कस्चिद् दानवेन्द्र परविषय-हरो निर्जितो न्येन शक्त्या]{style="font-weight:bold;color:#99cc00;"}\
The chief of the दानवस् capturing others territory was conquered by the might of another

[बद्धो वै सृन्कलाभिस् चिरम् इह पतितो यं स्तुवङ् चैल-रुद्धः]{style="color:#99cc00;font-weight:bold;"}\
bound with chains after having fallen here for a long time shut up in the mountain praises him

[तन् दृष्ट्वा किन्नराभिश् शतगणसहितस् स्वप्नशोषे हिमाद्रेर्]{style="font-weight:bold;color:#99cc00;"}\
Having seen that, with a 100 गणस् and kinnaras, having woken up, from the Himalayan peak,

[आयातो मोक्षनार्थाङ् जयति गणपतिस् त्वद्-धितायेव सो यं]{style="color:#99cc00;font-weight:bold;"}\
comes this गणपti who for your welfare conquers for the purpose of liberating.

I wonder what mythological tale this alludes to ? May the terrible गणप be pacified


