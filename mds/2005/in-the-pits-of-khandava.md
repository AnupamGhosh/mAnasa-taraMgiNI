
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the pits of खाण्डव](https://manasataramgini.wordpress.com/2005/10/20/in-the-pits-of-khandava/){rel="bookmark"} {#in-the-pits-of-खणडव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 20, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/20/in-the-pits-of-khandava/ "2:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The अमात्य's spells had provided a temporary respite by completely smothering the sapatna's fury. But finally the अमात्य's charms were broken by the counter-attack and the sapatna advanced right to our most critical fort. We felt that the capital could last only a matter of days in the worst case and we were also besieged by an army of म्लेच्छस्. We had gathered our last reserves in our defensive line of forts for the long struggle and then decided to make a surprise sally on the म्लेच्छ as soon as the first chance availed itself and tried to break past their cordon to reach the defensive forts.


