
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Descent to naraka](https://manasataramgini.wordpress.com/2005/03/25/descent-to-naraka/){rel="bookmark"} {#descent-to-naraka .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 25, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/25/descent-to-naraka/ "7:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier narrated how we had spent time in svarga with the apsaras. From there we descended to the himaparvatas- i.e for warm weather souls like us the slopes of the पाशण्ड's hill. We continued sporting there for sometime enjoying the brief bliss of our successes. Then our ill fate brought us crashing to a low hollow in plane of पृथिवि. We thought we had hit the nadir of existence when we were shaking from our thunderous crash to पृथिवि. We did not know that the narakas lay below. Then the noose of karma having caught us dragged us to the lower realms and to our utter shock we lay in naraka. We felt like king युद्धिषिठिर in his final journey on the path to naraka. How is it that we are in naraka? we asked, just as the great king had done. Torments were seizing us from all sides. And there we saw our shachiva and अमात्य also being dragged away to a dark recess of naraka, they were yelling and cursing but each one faces his torment in naraka on his own. At this point all we could say is that why has our fate taken us to naraka? But we found no answers.


