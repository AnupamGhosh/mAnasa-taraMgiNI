
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Trojan horse?](https://manasataramgini.wordpress.com/2005/11/12/the-trojan-horse/){rel="bookmark"} {#the-trojan-horse .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 12, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/12/the-trojan-horse/ "6:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

With the fall of general Wright, the Hindu armies employed a new trainer in the form of the roguish chief from Southern Saxony, Chappell. we have long known Chappell to be an abrasive character, who has always had his own quirks. He quickly got into a conflict with the prince of Vanga and the brave Andhra grenadier from Vangipura. We believe that the Anglo-Saxon is a Trojan horse sent by the king of the Southern Whites to ruin the Hindu armies. We suspect that he might be a plant to spoil the Hindu performance by his policies of targeted disruption and deletion.


