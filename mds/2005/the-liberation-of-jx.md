
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Liberation of Jx](https://manasataramgini.wordpress.com/2005/12/17/the-liberation-of-jx/){rel="bookmark"} {#the-liberation-of-jx .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 17, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/17/the-liberation-of-jx/ "6:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The clansmen got to gether to take stock of the proceedings. The case of Jx was remarkable. He had been pinned down by the attack of the vairi-s who were deluding him. He finally woke up on that fateful Monday morning. His purohita pressed home with the fierce attack that hurled arrows like a hail on the attack. His vairi-s were completely gone. They were gone like the sky blown clean of all the clouds to reveal the azure heights, with nothing but the midday sun blazing on the cobalt vault. His धनक्षय had been ended. He had suffered one major loss, but otherwise had at least emerged on the other shore with his life. He felt it was great victory and was ecstatic. But the other clansmen were vary. While he had finally repulsed the attackers, there could be more. They assembled and asked: O भार्गव what do you think? Is this victory complete?

We answered: It is no doubt a victory and Jx will be completely free from his primary उच्छाटन and stambhana. But we never know if they have caused the rekha भञ्जन.\
...\
We then retired and waited on the terrace for the passing जानश्रुतेय birds. They passed and said the following partly in संध्या भाष:

  - There were 10 in the numerator and one in the denominator.

  - Beware of धूमावती

  - युधिष्ठिर told the यक्ष that all think they may evade their karma

  - They all thought that the end of the road had been reached, but only the muni, other than you, saw a glimpse of the trouble ahead.\
...\
Cryptic as these may sound the possibility of the शिरश्छेद attack became clear. We hurried consulted with the muni and gave him injunctions of what to do in the even of us wending our way to vaivasvata. The muni felt considerable pressure on him due to this, especially if rekha-भञ्जन happens on the Jx front. He asked us: You who have sucked up the oceans of वृद्धि vidya have reached this state? We replied: We once mentioned that like Dattaji Shinde heading forth to face Qutb Khan, we hand no fear and will fight again if we survived the battle.\
...\
We called प्रियसखी. She said: Controlling the mind is most important but be a स्वच्छन्द. The रक्षस् will come you path but remember that even Jx was liberated.


