
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The dispatch from the slopes of Mount Kearsarge](https://manasataramgini.wordpress.com/2005/08/05/the-dispatch-from-the-slopes-of-mount-kearsarge/){rel="bookmark"} {#the-dispatch-from-the-slopes-of-mount-kearsarge .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 5, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/05/the-dispatch-from-the-slopes-of-mount-kearsarge/ "5:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We circumambulated the large lake at the base of Mount Kearsarge with म्लेच्छ adventurer M. We were separated from the bandhas of samsAra at that point. We saw the old वञ्ग chieftain at the end of it. The वञ्ग had battled our old foe for a long time and outwitted him much as we had. He enquired about R1, R2, R3, K1, K2 and S. These brought very painful memories to our mind. It was as though we had fallen like ययाति. We had mentioned earlier how we fell like ययाति after sporting in the groves of the पाषण्ड's hill in the दण्डक region. Then we bored even deeper into पाताल and then escaped the terrible guillotine which we alluded to earlier. But after escaping the guillotine, we had fallen into this new trap from which we were not sure there was a way out. The वञ्ग reminded us of it. He asked me the pathway that led to R. We illustrated it thus:

K1--\>K2--\>S--\>R3--\>R1 (now known as R). R3 was from a clan of great ब्राह्मणस् from गवलकुण्ड, who had originally hailed from Nepal. In the lore of गुह्यकालि and the many authentic sections of the महाकाल saMhita they were masters. They were masters of the वेदाङ्ग jyotisha, and नक्षत्र विद्या. When we were famished in गवलकुण्ड R3 had helped us with food much as उषस्ति चाक्रायन was helped, knowing that my ancestors were brahmas at their shrauta rites. R3 had two suitors: shveta वङ्ग, the other bold वङ्ग chieftain who had been demolished by the म्लेच्छस्, and धर्मदृढ वातव्यादि, who was a friend of the mean बिडाल-smashru. Shveta वङ्ग of the सावर्ण branch of भार्गवस् won the battle for R3. But R3 after that was struck by a terrible affliction from, which it was almost certain that there was no return. But R3's clansmen using their mighty विद्या's just redeemed the अमृत of R3 to reincarnate her and save themselves. R3 had a friend A, who also came from a clan of great ब्राह्मणस् who had distinguished themselves in their ऋग्वेदिc learning and in the heroic defense of the कर्नाट country battling against the turushka Randulla Khan. A had also fed me in the times of dire straits in गवलकुण्ड and even directed the aforesaid वङ्ग chieftain to provide me food. Both in R3 and A the glory of their brahminical ancestors shone like the cashed check we alluded to. A was one day narrating the exploits of the pro-Dravidian George Hart, when suddenly she paused and asked me if I was doing the right things. A said how she upheld the पौराणिc ideals to the highest level and even the dharma शास्त्रस् would have offered her a less stringent path. She told me that in the days of kali such adherence was not common and having told me of the dangers in the path prophesied that trouble may be coming my way. The वञ्ग confirmed it had come too soon and that the main warriors in my camp might even sense the empire's cracks.


