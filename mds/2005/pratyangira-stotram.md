
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [प्रत्यङ्गिरा स्तोत्राम्](https://manasataramgini.wordpress.com/2005/08/06/pratyangira-stotram/){rel="bookmark"} {#परतयङगर-सततरम .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 6, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/06/pratyangira-stotram/ "5:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[ब्रह्म-स्वरूपे ब्रह्मेशी ब्रह्मास्त्र खड्गधारिणी ।]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[ब्राह्मी प्रत्यङ्गिरे देवि अव ब्रह्म-द्विषो जहि ॥]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[विष्णुरूपे वैष्णवी घोरास्त्र खड्गधारिणी ।]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[नारायणि प्रत्यङ्गिरे मम शत्रून् विद्वेषय ॥]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[रुद्र स्वरूपे रुद्रेशी रुद्रास्त्र खड्गधारिणी ।]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[रौद्री प्रत्यन्गिरे देवी ममशत्रून्-उच्छटय ॥]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[उग्रस्वरुपे उग्रेशी उग्रास्त्र खड्गधारिणी ।]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[उग्रकृत्या प्रत्यन्गिरे उग्राभिचार्यान्-नाशय ॥]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[भद्रकालि उग्र-अथर्व सुक्त खड्गधारिणी ।]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[भार्गव-मन्त्र हृदया रक्ष मां शरणागतं ॥]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[महाकृत्ये उग्रविद्या महास्त्र खड्गधारिणी ।]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}\
[महाकृत्या प्रत्यन्गिरे महाक्षुद्रान् विनाशय ॥]{style="font-style:italic;font-weight:bold;color:rgb(51, 255, 51);"}

The stotra श्री प्रत्यङ्गिरा देवी passed through the lineage of hoary tantrics thus:

The trident-bearing श्रीनाथ and कौलिनी roaming in कैलाश founded the विद्या.\
This they taught to the ancient tantric खगेन्द्रनाथ and his wife विजयाम्ब.\
They taught it to the students of brahminical splendour first, विक्तष्टि and his dame illAIambA from the drAviDa country and second, vimala and his wife अनन्तमेखला. These were the pUrvanAthas\
Then it passed to the great tantric master of mantra विद्यास् and prayogas, कूर्मनाथ, and his wife मङ्गलाज्योती. They taught it to jaitra and his wife also named illAIambA and avijita and his wife आनन्दमेखला. These were the दक्षिणनाथस्.

Then it passed to the great brahmin मेषनाथ, knower of veda and tantra, and his beautiful wife काममङ्गला, the knower of the erotic lores. They gave to it their student the mighty vindhya and his wife kullAIAmbA and the yoga master ajita and his wife अजरमेखला. These were the पस्चिमनाथस्.

From them it passed for a while to the mysterious tantric of secret rites, वृक्षनथ who roamed the Himalayan fastness. From him it went to the great ritualist मित्रनाथ. After him the tantric masters committed a fatal error in the worship of कार्त्तिकेय. Enraged, the six-headed god drowned the विद्यास् in the ocean. There it was eaten by a fish. The fisherman matsyendra caught the fish and having read the book eaten by the fish acquired the great विद्यास् as well as the great lore of हठ yoga. Having acquired these lores he and his wife कुङ्कुमाम्बा became the revivers of the tantra in this yuga. In addition to their six sons, they initiated ब्राह्मणस् and क्षत्रियस् who furthered the tantra. They were:\
अमरनाथ and his wife सिल्ला.\
varadeva and his wife एरुण.\
चित्रनाथ and his wife kumArI\
OlinAtha and his wife bodhA\
vindhya and his wife mahAlakShaNA\
guDikanAtha and his wife अपरमेखला

After them came ओडीशनाथ who went from the Himalayan fastness to the west coast of bhArata, where he was succeeded by श्रीषष्ठनाथ, the great adept of the कौमार lore in addition to श्री-kula and कालि-kula tantras. He was succeeded by श्रीचर्येन्द्रनाथ.

Thus, is the line of tantric teachers.


