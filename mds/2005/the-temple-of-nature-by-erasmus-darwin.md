
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The temple of nature by Erasmus Darwin](https://manasataramgini.wordpress.com/2005/08/08/the-temple-of-nature-by-erasmus-darwin/){rel="bookmark"} {#the-temple-of-nature-by-erasmus-darwin .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 8, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/08/the-temple-of-nature-by-erasmus-darwin/ "6:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The grandfather of Charles wrote the following poem, which might be considered one of the foundational verses of evolutionary biology:

[Organic life beneath the shoreless waves]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[Was born and nurs'd in ocean's pearly caves;]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[First forms minute, unseen by spheric glass,]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[Move on the mud, or pierce the watery mass;]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[These, as successive generations bloom,]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[New powers acquire and larger limbs assume;]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[Whence countless groups of vegetation spring,]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[And breathing realms of fin and feet and wing.]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}

One may compare this with the hymns of गृत्समद shaunahotra shaunaka भार्गव:

[स ईं वृषाजनयत् तासु गर्भं स ईं सिशुर्धयति तं रिहन्ति ।]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}\
[सो अपां नपाद्-अनभिम्लातवर्णो ।अन्यस्येवेह तन्वा विवेष ॥]{style="font-weight:bold;font-style:italic;color:rgb(51, 255, 51);"}

Of great fertility, he has generated himself as a germ in those \[waters]; he is their infant; he sucks the \[waters]; the \[waters] moisten him; अपाम् नपात् of the waters of unfading color has entered here in a new body.\
\
[यो अप्स्वा सुचिना दैव्येन ऋतावाजस्र उर्विया विभाति ।]{style="font-weight:bold;font-style:italic;color:rgb(51, 204, 0);"}\
[वया इदन्या भुवनान्यस्य प्र जायन्ते वीरुधश्च प्रजाभिः ॥]{style="font-weight:bold;font-style:italic;color:rgb(51, 204, 0);"}

All other beings are, as it were, branches of him, who, truthful, eternal, and vast, shines amid the waters with pure and divine (radiance); and the shrubs, with their products, are born (of him).

He who is in waters with his own pure splendour spreads forth widely as per the eternal law of nature; The various beings of this world have emerged as descendents of his, the plants with all their descendents have also descended from him.

[]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}


