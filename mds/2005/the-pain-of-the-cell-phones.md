
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The pain of the cell phones](https://manasataramgini.wordpress.com/2005/03/25/the-pain-of-the-cell-phones/){rel="bookmark"} {#the-pain-of-the-cell-phones .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 25, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/25/the-pain-of-the-cell-phones/ "7:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We saw the wonderous charms of Hayastanika fade behind the impenetrable veils. We conjured memories of eating the wonderous superhot mango pickle on a pleasant spring afternoon and talking of the "pancha-shara मुलपधार्त" with the wonderful Kappa on the wonderful summer day in the high mountains -- they were both similar. We were reminded of the distant afternoon of the "curtain" where we experienced the pleasures of R's company. Our reverie, as the pleasures of existence, was broken by the call from Jx. Everyone was talking about Jx's wonderful cell phone, but Jx insisted it never worked. They then looked at my one cell phone which was purchased on last shelf of the last shop in Sadashiv Peth. Unlike Jx's they were not sure if mine would even work. I kept it to my ear and went through the routines. It made some wierd noises. At night it would ring without reason. We remembered our muni's words: "It is a two fold problem of both purchasing a cell phone for you and selling the peculiar camera-cum-phone you have devised". In the Sadashiv Peth operation we had received our current cell phone in barter for the peculiar camera-cum-phone we had made. The learned muni, wise beyond his years, had warned us that in the desh nobody really wanted peculiar devices of that type. प्रेतबुद्धी द्राविडाचार्यपुत्रि had bought that c-c-p we had made. On account of her perverse bent she was unable use the phone. She wondered why such devices existed in the first place.


