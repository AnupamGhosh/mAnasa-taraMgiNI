
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The discourse on cat's tastes](https://manasataramgini.wordpress.com/2005/07/27/the-discourse-on-cats-tastes/){rel="bookmark"} {#the-discourse-on-cats-tastes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/27/the-discourse-on-cats-tastes/ "6:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As I was in the embrace of the यक्षिनि she asked what is so funny about the cat's taste.\
I said: Most mammals sense 5 tastes with 7-transmembrane receptors with extracellular PBP domains. This kind of receptor seems to be an ancient acquisition of the animal lineage from bacteria. Thus our sense of taste seems to have emerged directly from a bacterial sense of taste directly fed into our neurons. The cats interestingly have lost their sweet receptor and cannot sense the sweet taste. The emergence of hyper-carnivory in the cats appears to have made their need to appreciate sweet superflous for all that mattered was taste for raw meat, and not sweet. Thus, the cats lost their ability to sense sweet.

To sense sweet in the mouth it also helps if amylase is secreted in the salivary glands. The promoters of amylase expression in the salivary glands appears to have been provided in humans by the insertion of a retro-element. So we unlike the cats are doomed to love sweet.


