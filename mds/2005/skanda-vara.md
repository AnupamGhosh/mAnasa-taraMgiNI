
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [skanda vara](https://manasataramgini.wordpress.com/2005/03/18/skanda-vara/){rel="bookmark"} {#skanda-vara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 18, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/18/skanda-vara/ "7:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The boons offered by the deity कुमार are of great certainty, but they be extremely perplexing and their good result may only be seen after some ironic hardship undergone by the recepient. Hence is he known as the roguish धूर्त, who composed the taskara विद्या by manifesting as kanakahastin and भास्करनन्दिन्. But even the paths to the attainment of chaurya perfection were not straight. amba wanted to slay भीष्म for which she received a garland from कुमार but none ever wore it. So she had to wait till as शिखण्डिन् she could wear it herself and slay the kuru warrior. Likewise, कात्यायन could not receive the कौमार grammar till he found the boy who learned things on hearing only once.

Thus, when we stood over the wastes of खाण्डवप्रस्त which was offered to us by the 6-headed one we were engulfed by perplexity. We were catching our head in dismay, when our interlocutor was laughing at our jokes thinking that life itself was a big joke. We wondered what had befallen us. Was this खाण्डव supposed to be a boon or a curse? When we had survived the fall of the blade that could have lopped our head off we realized that we had been favored by the peacock-rider. When we moved towards victory in that great strife of हिमप्रारम्भ we though we had been favored. But what does this खाण्डव have for us we wonder?


