
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Sultans felled](https://manasataramgini.wordpress.com/2005/10/18/sultans-felled/){rel="bookmark"} {#sultans-felled .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 18, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/18/sultans-felled/ "4:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A poet रामराज-भुषण in the Vijayanagara court composed this generous account for the Moslem tyrants who met their end at कृश्णदेव rAya's hand:

"O कृष्ण rAya, the foremost of heroes! Some proud and mighty Moslem kings killed by you in battle have arrived in svarga [[1]]{style="color:#00cccc;"}. Here they caused the devas to smile in amusement by their salaaming and addressing बृहस्पति as pIr, indra as सुरत्राण, and इन्द्राणि as bIbI."

The events which led to this composition were as below:\
In 1501, the Bahmanid Sultan Mahmud Ghazi Mujahid passed a resolution in his darbar to conduct an annual Jihad on Vijayanagara till its extermination. He was able to gather a large army in Bidar which was led by Fathullah (Nizam Shah), Qasim Barid (the first Sultan of Bidar), and Yusuf Adil Shah, the Ottoman Turk and founder of the Adil Shahi kingdom by 1509. It marched into Vijayanagaran territory with the hope of Jihad and plunder, thinking that it was ripe for conquest after the death of vIra नरसिंह. But the young कृष्णदेव was ready for the assault, he assembled the Hindu armies and readied them for attack. The Hindu army encountered the Army of Islam near the town of Diwani in Karnataka. कृष्णदेव moved very rapidly and twice evaded the Moslem attempt to encircle his army. He then outflanked them and suddenly took them on the rear. The Moslem army panicked, and कृष्णदेव made a bold attack at the center where Sultan Mahmud was exposed as his wings were stretched in an attempt to turn back and encircle the Hindus. कृष्णदेव personally led the charge, and cutting their way through the elite Turkish guard surrounding the Sultan the Hindu mounted archers and musketmen attacked the Sultan directly. He tried to flee rapidly in the opposite direction, but realized that he was actually moving deeper into Vijayanagaran territory, where the able Vijayanagaran general नन्द्याल नारसिंह had moved in to block any exit route. The Sultan then tried to make a sideways exit, but came into the range of the Hindu gunners who fired at him. Mahmud's horse was killed by a shot, he was hit on his hand and finally he was thrown off and fractured his skull. His guard at the risk to their life lifted him and tried to save him. The brahmin convert Fathullah, Qasim Barid and other Turks rushed reinforcements to allow the Sultan to be saved. कृष्णदेव then directed an attack on the disorganized Mohammedan wings and decisively crushed the Bahmanid army and forced them to retreat.

The Mohammedan armies beaten started retreating in separate divisions. कृष्णदेव was the first Hindu king in many centuries, who decided to pursue the armies mercilessly hunting them down. Yusuf Adil Shah receiving reinforcements decided fight कृष्णदेव from the fort of Kovilkonda. The Vijayanagaran army however marched rapidly and intercepted the Adil Khan before he could take the protection of the fort. An intense battle followed and the Adil Shah's army was gripped between two wings of the Hindu army and कृष्णदेव again made a vigorous direct attack on the Sultan's personal division. The Sultan's guard was thoroughly beaten and he came into the range of the Hindu shooters. Adil Shah was instantaneously struck by several arrows and killed right away. With the Sultan dead the rest of the Moslem army panicked and was completely destroyed. Thus, the fort of Kovilkonda was finally wrested by Vijayanagara \[The great battle of Kovilkonda 1509-10]. In the mean time कृष्णदेव had dispatched his general नन्द्याल नारसिंह against the Turk Quli Qutb Shah who was making a deep thrust towards Kondavidu (The old रेड्डि capital). नारसिंह intercepted him and in fiercely fought battle just outside the Kondavidu fort routed the Mohammedan raiders and chased them back to Golkonda \[Battle of Kondavidu].

Thus, the Hindu struggle took the fight right back to the Army of Islam and put it on the retreat.

[[1]]{style="font-weight:bold;color:#3366ff;"}They attained svarga not naraka because they had met the death of the brave by dying in battle. As per hindu lore even a sinful wretch like a Mohammedan Sultan attains svarga if he dies bravely fighting in battle. However, as per all Islamic accounts a Hindu dispatched by the proselytizing sword of a Ghazi goes to a Hell where his burnt in a fire. That in itself displays the utter gulf between monotheistic violent Islamic thought and the generous pagan thought of the Hindus.


