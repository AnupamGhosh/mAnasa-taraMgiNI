
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The primal shrine of vighnadeva](https://manasataramgini.wordpress.com/2005/02/19/the-primal-shrine-of-vighnadeva/){rel="bookmark"} {#the-primal-shrine-of-vighnadeva .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 19, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/19/the-primal-shrine-of-vighnadeva/ "6:59 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We pay obscience to the terrible medholka, who is surrounded by the eight extraordinarily beautiful goddesses clad in shimmering blue silks: तीव्रा, चालिनि, nandA, भोगदा, कामरूपिणि, उग्रा, तेजोवती, सत्या and is embraced by विघ्ननाशिनि who is seated beside him.

[ॐ रायस्पोषस्य दधिता निधिदो रत्नधातुमान् रक्षोहणो वलगहनो वक्रतुण्डाय हुं ॥]{style="color:#99cc00;"}\
[मेधोल्काय स्वाहा ॥]{style="color:#99cc00;"}\
[ॐ ह्रीं ग्रीं ह्रीं ॥]{style="color:#99cc00;"}

Having worshipped the awful one-tusked son of rudra by the Punya river, the Kashi of the गाणपत्यस्, we saw him in the embrace of the delightful मदनावती. Then we proceeded to the foremost of his shrines in the grove of Mayurapuri which has grown decadent as a result of the downward turn of the age of kali, eating tasty भक्षणस् that delight the fiery one along the way. Having reached the shrine we entered through the gate guarded by लक्ष्मी and नारायण. Then we proceeded to see medholka accompanied by his two wives siddhi and संऋद्धि, who are also known as मदद्रवा and मदनावती. Atop his head rests वासुकी, his eyes and navel are studded with gems and his trunk is turned left. Having worshiped ulka and his wives we proceeded to see the gates guarded respectively by उमा and rudra, the delightful काम and रती, and वराह with mahI seated on his lap. Then we saw the 23 idols of vighna and pacified him eight times as :\
ekadanta, mahodara, गजानन, lambodara, विकट, विघ्नराज, धुम्रवर्ण and वक्रतुण्ड, along with his two wives. Then we worshiped the great मन्थान bhairava who stood at the wall and भवशर्वा. Then we moved to see the rat and the peacock of ulka and the वृषभ of महादेव.

On the place of Mayurapuri, the सूत had thus narrated a tale in the days of yore:\
चक्रपाणि and उग्रा had a son named sindhu. He was favored as result of his devotions by सविता who infused his navel with the soma of immortality. He was given the boon that as long as the soma remained in his navel he could not be harmed by the devas and was immune to their missiles. In due course armed with this boon he rose to be a lord amongst the asuras, restoring their lost glory which had been shorn by the devas. He overcame the devas in a deadly battle and imprisoned them in Gandaki. The devas knew that one deva who had escaped and could relieve them was the terrible heramba, who appeared to them riding a lion, bearing ten arms and promised to relieve them after taking birth through उमा's womb.

Duely, in the sylvan groves of Lenya Parvata, ulka was born to उमा and sharva and named गणेश. vishvakarma provided the young गणनाथ with an axe, अञ्कुश, lasso and a trident when he was six and at seven, gotama performed his उपनयनं. The news of the deva's birth was relayed to sindhu by his spies who sent his agent क्रुरासुर to slay the उमापुत्र. However, after a short struggle गजानन snared him with his lasso and dragging him down cut his head off. In subsequent skirmishes गणpati slew other agents of sindhu such as बलासुर, व्योमासुर, क्शेमासुर and कुशलासुर repulsed their troops with his arrows. One day as गणनाथ was sporting in a sylvan grove he saw a large egg of vinAtA. With his usual curiousity he handled it and in the process broke. From it emerged a giant peacock that he brought under his control with his lasso and made it his वाहन.

Alarmed at his activities sindhu despatched his great commander कमलासुर with a huge army to quell गणेश्वर and bring his head. गणेश्वर mounted the mighty मयूर and from his two sides sprang forth his two wives, siddhi and संऋद्धि fully armed with swords, cleavers bows and tridents. vighna himself, with ten arms was armed with a lasso, mace, trident, bow, अञ्कुश, axe, cleaver, an explosive shatagni weapon, the भिन्दिपाल, the भुशुण्डी, the mudgara and a chakra. Thus, he advanced to attack the asura army. The asuras were assailed by a hail of weapons hurled by the terrible god and his shaktis. Having consumed the army he advanced at कमलासुर. After a prolonged fight he destroyed the bow of the demon and struck him all over with his missiles. The blood flowing out the demon spawned many new demons. But मदद्रवा and मदनावती, beloveds of the great deity, swooped on these demon and swallowed them. Then the wives of vighna drunk all the blood of कमलासुर and stopped the emergence of new demons. Seeing this ganesh hurled his trident at the neck of कमलासुर and the trident having severed his head carried it to the क्षेत्र of Mayurapuri, where it fell.

Then गणpati assembled the army of the गणस् of rudra. He himself formed the center and created a three assault divisions lead by [वीरभद्र, nandin and his brother कुमार](https://manasataramgini.wordpress.com/2004/05/19/rudra-his-agents/ "rudra- his agents"). उमा and rudra also accompanied him as he proceeded to besiege Gandaki to release the devas. nandin was sent as an emissary to sindhu ask him to surrender humbly or face an attack. sindhu choose to fight and arrayed his army under divisions headed by काल and विकाल, his wife's brothers and dharma and adharma, his valiant sons. On the first day वीरभद्र tore off the head of काल with his cleaver after a prolonged fight with the asura commander. nandikeshvara and विकाल fought with much fury and was struck down by his mace. But recovering rapidly he slew him by shattering his skull with his trident. The next day dharma and adharma put up a fierce fight and forced the pramathas and the भूतस् into a rapid retreat. nandin and वीरभद्र were beaten back and the गण army was in disarray, when कुमार rallied it to face the asuras again. कुमार consumed then armies with his fiery dart and finally reached dharma. The two fought each other with charmed shaktis for long and finally the six-headed deity slew him by striking him on his chest with his shakti. Next with a blow from his thunderbolt skanda killed adharma and destroyed sindhu's army. The older demons suggested sindhu to surrender, but he refused and himself charged the next day on गणpati on his flying car. गणpati mounted on the मयूर countered his astras for a while. Finally finding his weakeness, he cut off of his bow and shattered his chariot. However, his arrows and those of कुमार failed against the invincible frame of sindhu. sindhu rushed at गणpati with his cleaver upraised. But गणpati suddenly dismounted his peacock and contracting in size, got under sindhu and shot an arrow at his navel. This broke the receptacle of the soma of immortality which preserved him and it flowed out. With that gone ulka chopped of the head of sindhu and ended his reign.

He then handed the peacock to his brother कुमार as his वाहन.

[मेधोल्काय स्वाह ॥]{style="color:#99cc00;"}


