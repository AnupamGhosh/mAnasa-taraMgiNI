
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [eko मानुश आनन्दः](https://manasataramgini.wordpress.com/2005/10/16/eko-manusha-anandah/){rel="bookmark"} {#eko-मनश-आननद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 16, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/16/eko-manusha-anandah/ "11:04 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

With almost an year passing since the Great Fall we reflect upon him who acquired "eko मानुष आनन्दः" that is alluded to in the texts. He lived once upon a time beside the swamp with snakes and crocodilians in it. He was of immense intelligence and physical abilities, and he was capable of performing complex rites. He was set on the path of knowing the truth. He was recognized for his speed and agility and was recruited amongst the able ones. But in his quest for the truth he decided to understand the universe. He then was given a wife by the gods who was a female mirror image of his in intellect and ability. Together they understood many higher truths over a range of most fundamental matters. He was given a good mansion by the राजन्, and placed in a wonderful village with his family, along with several sacks of gold. He lived surrounded by the greatest intellectuals of his time constantly discussing the most profound topics and enjoying pleasant conversation. He was exceedingly virile and sired several kids, who were also high in intellectual accomplisment. The resources showered on him by the राजन्, and the intellectual abilities of himself, his wife and children allowed him to uncover the greatest mysteries of existence. He knew all of stars, galaxies, quasars, planets, organisms, molecules and particles. The राजन् used to personally invite him to his palace along with his clan and bestow favors upon them.

He was thus enjoying his life in the most congenial surroundings for a long time, when one day he looked up at the sky to observe the constellation of [Canis Major]{style="font-style:italic;color:rgb(255, 0, 0);"}. There he beheld the dog of the son of विवस्वान्. He rushed home and was compassed by his mighty and opulent clansmen. They said they could arrange the best of physicians to comfort him. But who escapes the noose of vaivasvata when one's काल has arrived ? Amongst his wailing people he was dragged away, noose around his neck, in the path of the ancestors by the गणस् of the buffalo-rider.

Those who beheld his funerary procession said: "This no death. He has had eko मानुष आनन्दः. Most others are destined for only asamasta आनन्दः. Will there ever be others who enjoy the Ananda as he did. Or will every one remember him green with envy about that complete Ananda that he had reached."

It was just under an year before the Great Fall. Will it be the Permian extinction of existence?


