
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कौमारी](https://manasataramgini.wordpress.com/2005/04/30/kaumari/){rel="bookmark"} {#कमर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/30/kaumari/ "6:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

कौमारी [![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/kaumari2.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[ShaM नमः षषट्यै ca maM maM maM keki keki महामयूर्यई स्वाहा]{style="font-weight:bold;font-style:italic;"}

