
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [ugra](https://manasataramgini.wordpress.com/2005/12/29/ugra/){rel="bookmark"} {#ugra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 29, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/29/ugra/ "6:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

When the altar is piled, it is termed ugra. It is filled with the fury of rudra महादेव. The agni in the midst becomes rudra and the sacrificer readies to pacify him. He recites thus:

[रुद्र्ó वा एष्á य्áद् अग्न्íस् त्áस्य तिस्र्áः शरव्याः प्रतीcई तिर्áश्cय् अनूcई]{style="font-weight:bold;color:rgb(255, 0, 0);"}

This fire is rudra, three are his missiles, one that strikes straight, one transversely, and one that strikes from below (These are the 3 stars of Orion, the invakas, which are the 3 arrows fired by rudra slaying prajapati).

[ताभ्यो वा एष्á आ वृश्cयते य्ò अग्न्íं cइनुत्é]{style="font-weight:bold;color:rgb(255, 0, 0);"}

By them he who piles the fire \[altar] is cut up.

[अग्न्íं cइत्वा तिसृधन्व्áम् áयाcइतम् ब्राह्मणाय दद्यात् ताभ्य एव्á न्áमस् करोति áथो ताभ्य एवात्मानं न्íष् क्रीणीते]{style="font-weight:bold;color:rgb(255, 0, 0);"}

Having piled the fire altar \[the यजमान] should give unasked a bow and three arrows to a priest; Indeed to them \[the bow and arrows of rudra] he pays homage. Indeed he buys protection for himself from them. \[The priest stands symbolizing rudra for the यजमान as he is given the bow and the 3 arrows and with that gift he is buying protection from rudra]

[य्áत् ते रुद्र पुर्áः ध्áनुस् त्áद् वातो áनु वातु ते त्áस्मै ते रुद्र संवत्सर्éण न्áमस् करोमि]{style="font-weight:bold;color:rgb(255, 0, 0);"}

O rudra , in the east \[is] your bow, may the wind blow after it for you, to you, O rudra , with the first year \[of the five year calendrical cycle] I pay homage.

[य्áत् ते रुद्र दक्षिणा ध्áनुस् त्áद् वातो áनु वातु ते त्áस्मै ते रुद्र परिवत्सर्éण न्áमस् करोमि]{style="font-weight:bold;color:rgb(255, 0, 0);"}

O rudra , in the South \[is] your bow, may the wind blow after it for you, to you, O rudra , with the second year \[of the five year calendrical cycle] I pay homage.

[य्áत् ते रुद्र पश्cआद् ध्áनुस् त्áद् वातो áनु वातु ते त्áस्मै ते रुद्रेदावत्सर्éण न्áमस् करोमि]{style="font-weight:bold;color:rgb(255, 0, 0);"}

O rudra , in the West \[is] your bow, may the wind blow after it for you, to you, O rudra , with the third year \[of the five year calendrical cycle] I pay homage.

[य्áत् ते रुद्रोत्तराद् ध्áनुस् त्áद् वातो áनु वातु ते त्áस्मै ते रुद्रेदुवत्सर्éण न्áमस् करोमि]{style="color:rgb(255, 0, 0);font-weight:bold;"}

O rudra , in the North \[is] your bow, may the wind blow after it for you, to you, O rudra , with the fourth year \[of the five year calendrical cycle] I pay homage.

[य्áत् ते रुद्रोप्áरि ध्áनुस् त्áद् वातो áनु वातु ते त्áस्मै ते रुद्र वत्सर्éण न्áमस् करोमि]{style="font-weight:bold;color:rgb(255, 0, 0);"}

O rudra , in the Zenith \[is] your bow , may the wind blow after it for you, to you, O rudra , with the fifth year \[of the five year calendrical cycle] I pay homage.

[रुद्र्ó वा एष्á य्áद् अग्न्íः स्á य्áथा व्याघ्र्áः क्रुद्ध्áस् त्íष्ठत्य् एव्áं वा एष्á एत्áर्हि स्áंcइतम् एत्áइर् úप तिष्ठते नमस्कार्áइर् एव्áइनग़्ं शमयति]{style="font-weight:bold;color:rgb(255, 0, 0);"}

rudra is verily \[manifest] in this fire; just as tiger stands in fury, so also he stands. When the \[fire-altar] is piled with these \[bricks], he worships \[rudra], and indeed with homage he soothes him.

[यो रुद्रो अग्नौ यो अप्सो य ओषधीषु यो रुद्रो विश्व भुवना ऽविवेश तस्मै रुद्राय नमो अस्तु ॥]{style="font-weight:bold;color:rgb(255, 0, 0);"}

rudra in the fire, in the waters, in the plants, rudra who has entered all entities of the universe, to that rudra homage.

He makes pourings of soma and fire offerings of the गावीधुक charu to the terrible rudra with the shatarudriya incantations. Finally with the above formula he makes an oblation of गावीधुक charu on the last brick of the altar to soothe and pacify rudra.

For the कापालिक observing the महाव्रत, these five fold mantras are the ones with, which he worships the आम्नायस् of rudra. The body is his altar. His own skull is his soma cup. His food is the गावीधुक charu. Having fed the heads of rudra with the internal soma, streaming from the chandra nerve he realizes the अम्नाय know as anuttara. He then recites: "[रुद्र्ó वा एष्á य्áद् अग्न्íः स्á य्áथा व्याघ्र्áः क्रुद्ध्áस् त्íष्ठत्य् एव्áं वा एष्á एत्áर्हि स्áंcइतम् एत्áइर् úप तिष्ठते नमस्कार्áइर् एव्áइनग़्ं शमयति]{style="font-weight:bold;font-style:italic;color:rgb(255, 255, 0);"}["]{style="color:rgb(255, 255, 0);"} The face of the anuttara manifests, like the crocodile face of tumburu.


