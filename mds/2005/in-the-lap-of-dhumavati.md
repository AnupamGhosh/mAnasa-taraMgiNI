
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the lap of धूमावती](https://manasataramgini.wordpress.com/2005/12/01/in-the-lap-of-dhumavati/){rel="bookmark"} {#in-the-lap-of-धमवत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 1, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/01/in-the-lap-of-dhumavati/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The harsh cawing of the solitary crow at night announces her presence. Of large size with dishevelled hair she roams. The severed heads of विष्णु and rudra are seen beside her. The mystic bIja ग्राअं is reverberating all around her. There are the सेनास् of the लुलापमुखस् around her commanded by the dreaded कालवाहनवदन. Beside her sits her fair daughter rakta-ज्येष्ठा or upa-धूम्रा. Her पार्शधस् are yelling hideous statements like without शिवा, shiva is but a shava. Others yell it is not in this jagat. In her hand she holds a large terrible शूल.

How shall the journey to the abode of धुम्रप्रकृति be they asked?\
They are conducted there by the awful rakta-ज्येष्था in the flying car with no yokes. When they arrive their power deserts the मूलाधार.


