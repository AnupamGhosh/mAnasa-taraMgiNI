
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The last stretch](https://manasataramgini.wordpress.com/2005/04/20/the-last-stretch/){rel="bookmark"} {#the-last-stretch .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 20, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/20/the-last-stretch/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We stood at the last stretch of the passage. Hence, we wanted to comment on somethings as they represent our very last comments in this phase of existence. As we remarked before we do not know if we shall speak of the face of the पिशाची. As our previous comments here, they shall be in the संध्याभाष with the same codes in place.

We marched to the edge of the great श्मशान. In the midst was seated The god. He was खड्गरावण, with 5 faces and a sword dripping in blood. He was laughing awfully. वेतालस् howled endlessly. I looked around and I saw the corpses of Blackie, Horse-face, White beauty, Sambhi, Palm-tree, Pygmy, Grey shudra, White shudrika, Yellow वैश्यका, Black किकट, grAma मण्डुक and बृहत्स्फिग्. Some were without heads, some were without arms and of others only heads remained. There were also those राक्शसस्, namely dursachi, hrasvaroman, lambashiras, कुटिलरोमन्, ugrashmashru, विट्शूल, पुंश्चलीप्रिय and sphigja who were horribly howling and roaming around. There were the three fields beside the श्मशान which were utterly empty- were one went in looking for something and came back, usually losing what one had.

We had long sought "that thing" and in course of our wandering reached this part. There we realized we had been seized by the पिशाची. She fed on the blood of the above-mentioned fallen ones and rejoiced with the awful राक्षसस्. For 15 years we wandered after the dream in which the वज्रयोगिनी appeared when we slept in the shrine of the terrible नित्या and विलासिनी in search of "that thing". But now it had culminated with the seizure by the पिशाची. Will the धूर्त, the ब्राह्मण्य deva come to our aid?

    षह् एइन् ःनब्ऽ एइन् ऋöस्लेइन् स्तेह्न्,
    ऋöस्लेइन् औफ़् देर् ःएइदेन्,
    Wअर् सो जुन्ग् उन्द् मोर्गेन्स्छ्öन्,
    Lief er schnell, es nah zu sehn,
    षह्ऽस् मित् विएलेन् Fरेउदेन्,
    ऋöस्लेइन्, ऋöस्लेइन्, ऋöस्लेइन् रोत्,
    ऋöस्लेइन् औफ़् देर् ःएइदेन्।
    ...
    ुनद दॆर विलदॆ कनबॆ बरcह
    ऽस् ऋöस्लेइन् औफ़् देर् ःएइदेन्;
    रöसलॆिन वॆहरतॆ सिcह ुनद सतcह,
    ःअल्फ़् इह्म् दोछ् केइन् Wएह् उन्द् आछ्,
    Mußt' es eben leiden.
    ऋöस्लेइन्, ऋöस्लेइन्, ऋöस्लेइन् रोत्,
    ऋöस्लेइन् औफ़् देर् ःएइदेन्।

[Johann Wolfgang von G]{style="font-style:italic;"}[öthe]{style="color:rgb(102,0,204);font-style:italic;"}


