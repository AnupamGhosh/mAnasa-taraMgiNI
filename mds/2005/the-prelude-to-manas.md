
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The prelude to Manas](https://manasataramgini.wordpress.com/2005/09/02/the-prelude-to-manas/){rel="bookmark"} {#the-prelude-to-manas .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 2, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/02/the-prelude-to-manas/ "5:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From the time of the defeat of the Uighurs to the rise to the powerful Mongol tribes of the Khitan, who founded the Qara Khitai empire in Central Asia, the supreme rulers of "Imperial Mongolia" were the Kirghiz taken to their zenith by Manas Kha'Khan and his valiant successors. Imperial Mongolia was always the center of power of the Altaic peoples- the Turks and the Mongols. The rulers of imperial Mongolia were the supreme rulers of the steppes, and it became the power center of the Altaic empires since the rise of the first Hun empire under the Khan Motun-Tegin who destroyed the Shakas and the Kushanas and established himself as the supreme lord of Central Asia. He was the first Central Asian power to defeat the Han Chinese and illustrate the power of mounted archery, which he had acquired from the Kushans. Then Anegue Khan founded the second great Hun empire of the Juan-Juan, whose armies widely invaded the states of outer Asia, including Iran and India. These Hun empires were the first Mongol empires and even Chingiz Kha'Khan much later acknowledged them as the first military and political successes of the Mongols. Then the power passed to the earstwhile allies and vassals of the Huns, the Blue Turks under the Khan Bumin. After a glorious reign the Blue Turks were displaced by the Uighurs, another Turkic tribe as the lords of central Asia. The next Turko-mongol tribe to gain control were the Kirghiz, followed by the Mongol tribe of the Khitan and finally the Chingizid Mongols.


