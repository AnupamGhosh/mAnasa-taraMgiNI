
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पाणिनि's trees](https://manasataramgini.wordpress.com/2005/07/30/paninis-trees/){rel="bookmark"} {#पणनs-trees .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/30/paninis-trees/ "7:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/pbp.gif){width="75%"}
```{=latex}
\end{center}
```

{target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/mycelial_trees.jpg){width="75%"}
```{=latex}
\end{center}
```



The one of the greatest of the भार्गवस्, पाणिनि of the अष्टाध्यायि fame devised the first representation of what may be called a context free grammar. We take his माहेश्वर सूत्रस् as elementary rules for branching. Applying recursive rules on them, similar to the fashion vedic ritual is specified in the shrauta sutras, we get these dendritic structures, which we call पाणिनि's trees. A rich as well as pleasing structure may emerge from some rules, even as the भाष known as संकृत. Above are some of those emerging from my recent experiments


