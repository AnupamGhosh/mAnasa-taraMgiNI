
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [tumburu, the manifestation of rudra](https://manasataramgini.wordpress.com/2005/02/13/tumburu-the-manifestation-of-rudra/){rel="bookmark"} {#tumburu-the-manifestation-of-rudra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 13, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/13/tumburu-the-manifestation-of-rudra/ "3:51 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From the northern head of the terrible rudra, known as the वामदेव head, manifests the emanation of वामदेव known as the four-faced tumburu surrounded by his 4 sisters. From the four faces of tumburu narrate the four great वाम tantras: 1) The frightening [शिरश्छेद]{style="color: #ff6666"} (also known as the [जयद्रथ यामल]{style="color: #0000ff"}. 2) the terrific [वीणाशिका]{style="color: #0000ff"}. 3) the [महासंमोहन]{style="color: #0000ff"}and 4) [नयोत्तर]{style="color: #0000ff"}.

The[ शिरश्छेद tantra ]{style="color: #ff6666"}gives the following description of the tumburu emanation of वामदेव:

[tumburu is the lord rudra himself and is called the called the great fierce reptile, the crest jewel of all the devas adorning their shikha (a cryptic allusion to the सहस्रार). He two aspects asakala and sakala, the latter being described here. He has four energies which reside in the cave of the heart of his universal consciousness. They are शान्तिकला, विद्याकला, निवृत्तिकला and प्रतिष्ठकला. These corresspond to his four companion goddesses जया, विजया, अजिता and अपराजिता who generate the phenomenal universe, while he himself is the fifth and highest energy शान्त्यतीतकला, which transcends all manifestation. He resides in the abode of soma (सौम्यधाम), which is full of the savour of countless mantras. His entire body is illuminated by the circle of his light and rays of the jewels of his many विद्यास्. He smells sweetly with the scent on many garlands and his body is smeared with a blood-red anguent. He shows the mudra of the शक्तिपीठ and his मण्डल is that of the चन्द्रदुर्दण्ड. He has 4 hands in which he holds a lasso, अञ्कुश, trident and varada mudra. His four faces are white, red, yellow and black. His body is like the blazing light of mount Meru. His front face is like that of a man whose lotus eyes are blooming and is most beautiful. He wears an earring and its rays cover his face with a network of light. His neck is like a precious gem and his hand is adorned with a ruby and emerald ring. He wears rubies and emeralds and his strong hips are raimented in a rich Kashmiri cloth. From his mouth comes a deep sound like the rumble of a thundercloud. Of his remaining faces one is that of a crocodile and the two side ones are of birds singing sweetly. The atoms emerging from his lotus feet is the bliss realized by the वीरस्. The अञ्कुश is the goad of the universe, the lasso is the abode of all fetters that binds beings as they rise to higher levels or descend. The trident he sports has for its three prongs persistance, origin and destruction. He bestows all that people desires with the varada mudra.]{style="color: #ff6666"}

[tumburu resides in the circle of the 14 powers and great fetters and is surrounded by a host of female गणस्. His female attendents laugh and yell horribly and their forms are hideous in appearance. He is the great hero who can consume the entire universe with a mere twand of his great bow. At the same time he is the source of all existence and its protector. He is the cosmic breath, the great महाकाल and कालग्निरुद्र.]{style="color: #ff6666"}

[He sits on a lotus with 4 petals. On the eastern petal is seated जया, विजया in the south, अजिता in the north and अपराजिता in the western petal. These lovely goddess are his sisters and they melodiously play on their वीणास्.]{style="color: #ff6666"}

When the Javan naval expedition had ravaged Cambodia and conquered it, the prince jayavarman II was taken captive to Java. There he met a learned ब्राह्मण from India named हिरण्यदामन् who was a great exponent of the tantras of the वामस्रोत. He taught the 4 tumburu tantras mentioned above to shivakaivalya, the purohita of jayavarman and performed a rite according to the वीणाशिखा tantra to confer certain siddhis on the prince jayavarman II. It is said that through the yoga of the tantra rudra's emanation tumburu courses through the path of पिञ्गला nerve and bestows powers to perform terrific deeds (raudrakarman) and then tumburu courses through the iDA nerve track and gives the ability to perform "bright deeds" shuchikarman. With these siddhis the prince jayavarman is said to have waged war for 12 years and finally routed the Javan garrison in 802 AC. Having liberated the kingdom of Cambodia and raised it to great glory and founded his impregnable capital of Hariharalaya (the city of shiva and विष्णु). It was from this impregnable base that the Angkor empire founded by jayavarman II held sway for almost 600 years with only a single military defeat in this entire period. At the time of jayavarman's coronation at Hariharalaya, हिरण्यदामन् recited the 4 tantras and shivakaivalya wrote all of them down. The tantras of the वामस्रोत learned by shivakaivalya were also used in the rites involved in the consecration of the great shiva temple with a 1000 लिञ्गस् at Phnom Kulen built by jayavarman II.


