
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [agni, the divine hotars and the अध्रिगुप्रैषसंप्रैष](https://manasataramgini.wordpress.com/2005/02/26/agni-the-divine-hotars-and-the-adhrigupraishasampraisha/){rel="bookmark"} {#agni-the-divine-hotars-and-the-अधरगपरषसपरष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/26/agni-the-divine-hotars-and-the-adhrigupraishasampraisha/ "6:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Before the animal sacrifice of the pashubandha the मैत्रवरुण priest utters the chant (RV 4.15.1-3):\
**[]{style="font-size:85%;"}**[अग्निर् होता नो अध्वरे वाजी सन् परि नीयते/ देवो देवेषु यग़्ण्य़ियः //]{style="font-weight:bold;color:rgb(255,0,0);"}\
[]{style="font-size:78%;"}\
agni, the hotar, being the booty bearing horse, is led around in our sacrifice; a deva to be worshipped amongst the devas.

[अ परि त्रिविष्ट्य् अध्वरं यात्य् अग्नी रथीर् इव / आ देवेषु प्रयो दधत् //\
]{style="font-weight:bold;color:rgb(255,0,0);"}\
agni, like a car-warrior, goes around 3 times in our sacrifice, distributing amongst the devas the sacrificial offerings.

[परि वाजपतिः कवीर् अग्निर् हव्यान्य् अक्रमीत् / दधत् रत्नानि दाशुषे]{style="font-weight:bold;color:rgb(255,0,0);"}[//]{style="font-weight:bold;color:rgb(255,0,0);"}[](http://flaez.ch/rv/rv?wort=daashu/Se){target="al"}

agni, the lord of the booty, the seer, has walked around the oblations, conferrings gems to the worshipper.

Then he chants the formula of adhrigu, the ancient dashagva.\
The formula of the great dashagva goes thus:\
[अजैद् अग्निर् असनद् वाजं नि देवो देवेभ्यो हव्या ऽवाट् प्रञ्चोभिर् हिन्वानो धेनाभिः कल्पमानो यग़्ण्य़स्यायुः प्रतिरन्न् उपप्रेष्य होतर् हव्या देवेभ्यः //]{style="color:rgb(255,0,0);font-weight:bold;"}

agni has conquered; he has captured the booty; the deva has conducted the oblations to the devas, being impelled by those facing forward, being made fit with chants, extending the life of the ritual. Impel, O hotar the oblations to the gods.

This is also paralleled by a त्रिष्तुभ् chant similar to the gAyatri chant of the मैत्रवरुण which was noted above. It is from the same वामदेव collection (RV 4.6).From these it is clear that one of the divine hotars was agni. This was never in much doubt because the very first hymn of the RV mentions agni explicitly as the divine ऋत्विक् and hotar, who gives gems:

[अग्नीम् ईळे पुरोहितं यग़्ण्य़स्य देवम् ऋत्विजं / होतारं रत्नधातमं //]{style="font-weight:bold;color:rgb(255,0,0);"}

The same point is suggested by a parallel त्रिष्तुभ् chant to the gAyatri chant of the मैत्रवरुण which was noted above. It is from the same वामदेव collection (RV 4.6.2):

But then the आप्रीइस् mention two divine always hotars (even the soma only आप्री of the काश्यपस्). So who were the 2 hotars? The बृहद्देवता explains that the two hotars of the आप्रीस् are the terrestrial and the middle forms of agni (BD3.11). This explanation was evidently prevalent in the Vedic world as is suggested by यास्क's statement in the nirukta: "daivyau होतारव् अयं चाग्निर् asau cha मध्यमः (Nk8.2)". They are said to be daivya because both the middle and the earthly forms of agni are born from the celestial (divya) agni. Elsewhere, the BD clarifies these earthly and middle forms of agni.\
BD 1.91: Because this \[terrestrial agni] is lead by men and that \[other agni] leads him from this \[other world], therefore these two \[agnis], while having the same name, have performed their work separately.\
1.92: Because he is know when born (जातः) or because his known here by all beings (जातैः), therefore these two, while having an identical name pervade both worlds separately.\
1.93: He as the middlemost if these 3 agnis, shines in the air discharging rain: accordingly some name of agni are only incidently.\
1.94: This (terrestrial) agni is hairy with flames, and the middle one with lightning branches, while that (celestial one) is hairy with rays: So the (ऋशि) calls them the hairy ones (this is an allusion to the 3 hairy ones in dirghatamas's hymn: 1.164.44). The 3 forms of agni are mentioned as agni, जातवेदस् and वैश्वानर. The first two being the divine hotars.\
[]{style="font-size:78%;"}


