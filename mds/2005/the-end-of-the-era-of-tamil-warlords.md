
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The end of the era of Tamil warlords](https://manasataramgini.wordpress.com/2005/08/26/the-end-of-the-era-of-tamil-warlords/){rel="bookmark"} {#the-end-of-the-era-of-tamil-warlords .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/26/the-end-of-the-era-of-tamil-warlords/ "5:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The पुरनानुरु and related पुरं texts of early Tamil literature mark the end era of the Tamil warlords. The violent, grandiose and impractically generous Tamil warlords were passing out of vogue with the solidification of the 3 great Tamil kingdoms- the चोळन्, the cheran and the पाण्डियन्. The great reign of the 3 Tamil polities lasted till the external kalabhra rule ended the sangam phase of tamil literature. They were followed by the more Sanskritized Pallavas and finally by the revival of the big 3 till the days of pan-India tripartite struggle between, north, middle and south India, all alternately contending for supremacy over the land. But the image of the tamil warlords still inspires the modern tamil world. So what can we glean about them- Ay आण्डिरन्, vEl पारि and अतियमान् नेदुमान् anchi. These tamil warlords literally lived with the vAL (hatchet), vEl (spear) and vil (bow), and generously rewarded the expert poets, ब्राह्मणस् and shudras alike, who sung their praises. There were many different clans of these warlords of diverse origins, but we know very little of them other than from the early tamil texts and a few coins minted by some of the clans. Studies by Raghava Iyengar have strongly suggested that the kochar and the वेळीर् clans were naturalized North Indians settled in the Tamil country. The वेळीर् clan appears to have descended from the northern yadus of द्वारक about 49 generations before the days of kapilar, the ब्राह्मण poet in tamil. The most celebrated of the वेळीर्स् was Ay अण्डिरन् who ruled on the podimalai mountain, probably a rocky spur near Karur. The वेळीर्स् appear to have emerged from the Dvaraka via the Konkans where they fought many a fierce battle and then settled in the Tamil country. The kochars were another northern clan who appear to have come from Kashmir and settled in the Dravida country beside the वेळीर्स् with whom they engaged in a long destructive combat, by alternatively siding with chera and चोळस्. vel पारि and नेडुमान् anchi seem to be home-grown Dravidian warriors, who also challenged to choLa, chera and पण्डियन्स्.

Ay अण्डिरन्: "If for Ay अण्डिरन् who leads in murderous war, the unclouded sky were to blossom Into stars without end, turning all white, the black holes vanishing, it would be like the elephants he has granted those who came to him in need or would at least bear some resemblance." \[a hero of the वेळीर् clan]

नेडुमान् anchi: You warriors, Take care of yourselves! Let us tell you My lord \[anchi] is like a crocodile who in shallow water muddied by the playing of children from the city, can drag in, bring down, and slaughter an elephant, with water only knee deep!

vEl पारि: "The kings with great armies \[the big 3] came against the hill in war, ignorant of how difficult it would be to prevail against पारि, he who wielded a sharp spear, he who was the master of abundant toddy."

Thus the tamil poets describe their warlords.

Some of these warlords were critical for the aryanization of the tamil zone and can be clearly identified with the archaeological remnants of the megalithic cultures which are scattered all around South India. Their time line can be estimated from archaeology as spanning the period from around 800 BC to 150 BC. The heydays of the warlords was coming to an end by around 200 BC when the mauryans failed to recognize them and big 3 were dominant. Ultimately they were all swept away by the combine of the chera, the choLa and the पाण्डियन्, valiantly defending their little hills and fortifications.


