
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The wheel on Jx's head](https://manasataramgini.wordpress.com/2005/04/02/the-wheel-on-jxs-head/){rel="bookmark"} {#the-wheel-on-jxs-head .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 2, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/02/the-wheel-on-jxs-head/ "5:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The last wick of भैरवानन्द does not fall before one goes under the wheel. We saw Jx with the wheel spinning on his head. We asked him- hey Jx how did you get that wheel on your head. And lo behold, the wheel flew off his head and crashed on to mine.\
...\
It was like Holmes and Moriarty locked in that final encounter on the cliff in Switzerland. One of them had to die-- which one of them will it be or may be both ?\
...\
The hirsute M won an amazing battle. It exactly like that of Jx. Still his fate was sealed. There was no escape at the other end of the tunnel. We only won खाण्डवप्रस्थ, yet our fate was looking pretty damn grim.\
...\
It looked as though the कृत्या had hit her target --- She was uprooting everything in its path. One after the other our kavachas were failing. We cannot even inform the muni of our fate !! We have been thus blocked by the कृत्या.\
...\
Definitely we had hit the nadir of our existence-- or was there worse --- we were just amazed at the noose of fate-- it drags you without you being able to do anything right into naraka.


