
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The other side of the guillotine and other musings](https://manasataramgini.wordpress.com/2005/10/26/the-other-side-of-the-guillotine-and-other-musings/){rel="bookmark"} {#the-other-side-of-the-guillotine-and-other-musings .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/26/the-other-side-of-the-guillotine-and-other-musings/ "4:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

What ever happened after we passed on to the other side of the guillotine we were not sure. Evidentally at that point we did not know if the guillotine had fallen or not. In reality the English encounter was the begining of the troubles. We got past the deployer of the ghora दूरदृष्टि विद्या and were resting silently. We then wandered on the Deccan plains in the year 2002, when are our armies were still unstoppable, and had a brief wandering with the muni who was declining towards the abyss. The muni then saved himself and proceeded to the central plains. After that the अमात्य spotted what may be termed the English agent S in the horizon. We encountered S in the context of a SNF2/SWI2 proteins with a PHD finger. Agent S's progress was remarkably lackluster and took things very lightly. But soon S captured us with the इन्द्रजाल and on Nov 6th 2001 around 6.30 PM we had a dramatic showdown. We were using a ricketty old SUN workstation and the machine crashed mysteriously 3 times when we were composing something important, yet we failed to get the signal. In the bind of इन्द्रजाल we were neither communicating with the muni nor R, so the message of the जानश्रुतेय birds did not reach us. So we were taken by surprise and hit by the तामस-धूम at that time. For 6 days we did not known what had seized us. Then gradually with अथर्वण हृदय we emerged out of the पाशस् of agent S and completely overcame S. We then wandered on the Deccan plains in the year 2002, when are our armies were still unstoppable, and had a brief wandering with the muni who was declining towards the abyss. The muni then saved himself and proceeded to the central plains. We then faced the दुSट राक्षसि in the battle of chaitya and were again pinned down for 6 days in August of 2002, but wriggled our way out of the trouble. We were advancing towards the great swell of the AAA+ आट्Pअसेस् (codenamed project POSTBOX) when agent Auriga was sighted by the आमात्य and शचीव. We wriggled out of the Auriga trap but were ambushed at the most fierce battle of the पाषण्ड hill of which we have spoken many times. At that point we mused about the fall of the guillotine. Subsequent to that there was a period of silence as the शचीव and अमात्य were fooled by the dreadful ग्राहि. Then due to the fateful slip we faced the धुमावती prayoga. In the mean time Jx had already been shattered by the धूमवती and became unmatta. Mn of greatest power and intelligence was subdued by the उच्छुष्म. The पिषाचि was sucking us dry, and it looked as though we were hanging as though from a spider's web.


