
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Pyrotechnics in the gloom](https://manasataramgini.wordpress.com/2005/01/06/pyrotechnics-in-the-gloom/){rel="bookmark"} {#pyrotechnics-in-the-gloom .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 6, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/01/06/pyrotechnics-in-the-gloom/ "6:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The day was enveloped in a forbidding gloom, and the air reeked with monsoonal decay, even as the Hindu civilization in its downward slide. The studious youth busied themselves with the most mundane of chemical experiments. Some were vigorously blowing into a charcoal lump with the hope of seeing a bead of lead. Yet others were soulfully bubbling a foul smelling miasma of sulfuretted hydrogen through a tube and waiting for a precipitate to form with bated breath. Still others were furiously inscribing in the most pleasant scripts the notes scrawled on the chalk-board by the sanguine lecturer. Lambashiras, Blackie and others were humbly bowing to a lecturer to get his stamp approval on their diligently copied notes. Our outer mind mindlessly imitated the deeds done by the rest of this flotsam mass. In performing our karma we reached the triple waisted Kipps flask to bubble the foul gas through our tube. Even as we reached there comely Mis-creant (we called her that, so we shall refer to her thus) from her sub-section of the closed system also arrived at the fetid source of the gas. She joined the line along with me as there was an orderly queue for the gas. We looked at each other with a tired expression and seemed to almost hold a conversation within our minds without any words. We almost simultaneously seemed to turn to the large block of sodium that was lying in the kerosene. Mis-creant cut out a decent cube of the block when nobody watched. We smiled at each other; then I picked up the cube of Na and threw it into a puddle out of the window. There was a tremendous explosive flash, very like the last flash of Essen Taiji lighting up the decadent Mongol horizon on the steppes. Our plebeian bystanders were immensely shaken even as the ignorant Hindu is rudely awakened from his joyous complacence by a violent Abrahmist. We felt a great sense of satisfaction.

On the way out even as our nerdy fellow beings discussed the arcana of the next exam, we picked up some tubes of silver chloride and proceeded to the welding shop which lay on our usual scooter circuit. We collected a bottle of acetylene. Blackie, Nerdie, K and B driven by a certain perverse curiosity arrived to see what we were upto. We then set up an concentrated ammoniacal solution of silver nitrate and bubbled the acetylene through it till we got a buff colored precipitate. We then washed it alcohol and dried it even as we watched some dreary cricket match on TV. Then we placed it under a metal cylindrical box and lit it with a magnesium wire. The ensuing action sent the metal box about 20 feet into the air and the patio was stained silver. Long after Blackie, Nerdie, K and B had passed out of our lives we used to still see that silver stain and remember gloomy day with a profound upwelling of मान्सीक rasa- the रसार्णव. Mis-creant told me that Blackie, Nerdie etc are like the old sharira-s that are cast aside by the Atman as it passes along to new shariras. The explosion of silver acetylide was like the insight of brahman that allowed one to see the atman for the first.

We stood at the fork again. We looked behind, the dead sharIra-s shed of the past littered the path. Which path shall we take O पूषन्?


