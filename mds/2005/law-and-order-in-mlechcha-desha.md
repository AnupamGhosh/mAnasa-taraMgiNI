
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Law and order in म्लेच्छ desha](https://manasataramgini.wordpress.com/2005/08/31/law-and-order-in-mlechcha-desha/){rel="bookmark"} {#law-and-order-in-मलचछ-desha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 31, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/31/law-and-order-in-mlechcha-desha/ "5:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The land of the म्लेच्छस् is in क्रौञ्च द्वीप is renowned for its orderliness arising from the maintenance of law and order. However, when the chakra of indra and वायु struck the land it was striking to note that the law and order collapsed rapidly. The state largely lost control and the dire circumstances resulted in an intense human competition and lawlessness. This did not seem to be case in bhArata despite the similar strike by the manyu of indra. The lawlessness appears to be of a different kind in bhArata from म्लेच्छ desha. One wonders if this is because of स्वभाव of the concerned people or the social inequalities between the कृष्णवर्णस् and shvitinchyas. One can imagine in terms of evolution, how people placed in these circumstance will be so easily subject to natural selection and adaptation to such niches. The vagaries of life in the dark continent, as well as the fate of degraded human slaves of the New World, have surely produced such adaptations. But it is clear that most people in म्लेच्छदेश may not acknowledge the point.


