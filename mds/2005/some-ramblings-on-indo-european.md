
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some ramblings on Indo-European](https://manasataramgini.wordpress.com/2005/08/10/some-ramblings-on-indo-european/){rel="bookmark"} {#some-ramblings-on-indo-european .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 10, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/10/some-ramblings-on-indo-european/ "6:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Due to a late night encounter back from work we got rambling about one of our favorite topics the evolution of IE languages from PIE. Despite the difficulties, that we are familiar with from other areas of science, we have some idea of the phylogeny of IE. Below is the picture as we see it today:

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/pbp.gif){width="75%"}
```{=latex}
\end{center}
```

{target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/indoeur.jpg){width="75%"}
```{=latex}
\end{center}
```



This made me drift back in time toward my ancestors who composed the glorious mantras of the veda in the nascent days of Indo-Aryans. The earliest of their compositions in the ऋग्वेद appear to have happened in the distant Uralic steppes and the allusions to the high latitudes and dark winters still linger on though largely unrecognized by the modern Hindu mind. Beyond lies the RUKI-SATEM clade which unifies I-Ir with Balto-Slavic. This made me think more about the polycephalous deities like Svantovit, Porevit and Rugevit and their iconography. What does this mean for Indic iconography?


