
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Under the कर्णमोटा tree](https://manasataramgini.wordpress.com/2005/12/06/under-the-karnamota-tree/){rel="bookmark"} {#under-the-करणमट-tree .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 6, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/06/under-the-karnamota-tree/ "6:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We walked up the path of the hill of the वानर, when the dasyus were not its rulers yet. There was a glorious cleft on the side of the hillock, which led to a deep valley with wonderfully eroded paths. There a cool breeze often blew and we wandered around with the muni, कुम्भोद्भवा, and occasionally कृष्णवर्ण and the रामानुजवादिन्. The muni and myself had installed a installed a large लिङ्ग there for the worship of The god in the cleft and worshipped it with the brAhmi plant and the शङ्क पुष्प. We learned from the कुम्भोद्भवा that a few days later the place had been defiled by dasyus. We first reconnoitered and found that there was an enormous band of dasyus whom we could not fight on our own. We also noticed that the लिङ्ग had been stolen by the dasas and the place dug up. We were furious but could not do much. We worshipped महादेव with the hima offering and saw him conjoint with the shakti who gave us the signal to attack. In the deathly spot near where we fought our enemies grew the huge कर्णमोटा tree, which is related to khadira tree. We walked in a straight line from the small khadira tree in the spot of दत्तात्रेय to the giant the कर्णमोटा tree and tied the mystic ओषधि known as वाराही to the trunk of the कर्णमोट tree.

There using the oozing gum of the कर्णमोटा tree, also known as रेतसधृत, oblations were offered to the terrible goddess कर्णमोटिनि intertwined with that of कौमारी.

Then the muni sped with the fire of saumyA. It spread terribly over the hill of the कपिश्रेष्ठ. We were reminded of the statement: "O agni utterly destroy these दासस् and drive them away" and the कृणुष्व पाज iti पञ्च. The विद्या of कर्णमोटिनि seized them and hurled stones on their houses. They were driven away and we miraculously found the लिङ्ग again and brought it back with us despite its weight.

One of the important acts for the proper performance of the षट्कर्म of tantric parlance is the acquisition and study of the महाविद्या of देवी कर्णमोटिनि.


