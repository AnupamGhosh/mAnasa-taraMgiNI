
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कर्णमोटिनि विद्या](https://manasataramgini.wordpress.com/2005/12/08/karnamotini-vidya/){rel="bookmark"} {#करणमटन-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 8, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/08/karnamotini-vidya/ "5:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/hello/133/1300/400/कर्णमोटिनि.0.jpg){width="75%"}
```{=latex}
\end{center}
```



[ॐ छण्डायै नमः। ॐ घण्डायै नमः। ॐ कराल्यै नमः। ॐ सुमुख्यै नमः। ॐ दुर्मुख्यै नमः। ॐ रेवत्यै नमः। ॐ प्रथमायै नमः। ॐ घोरायै नमः।]{style="font-style:italic;color:rgb(51, 204, 0);"}\
[वयु-तत्त्वात्मिकायै नमः॥ इति वयु मण्डलं]{style="font-style:italic;color:rgb(51, 204, 0);"}

[मम शत्रून् शोषय शोषय ।]{style="font-style:italic;color:rgb(51, 204, 0);"}

[ॐ सौम्यायै नमः। ॐ भिषण्यै नमः। ॐ जयायै नमः। ॐ विजयायै नमः। ॐ अजितायै नमः। ॐ अपराजितायै नमः। ॐ महाकोट्यै नमः। ॐ रौद्र्यै नमः।]{style="font-style:italic;color:rgb(51, 204, 0);"}\
[रुप-तेज-तत्त्वात्मिकायै नमः॥ इति रुप-तेज मण्डलं]{style="font-style:italic;color:rgb(51, 204, 0);"}

[मम शत्रून् विद्रावय विद्रावय ।]{style="font-style:italic;color:rgb(51, 204, 0);"}

[ॐ विरूपाक्ष्यै नमः। ॐ परायै नमः। ॐ दिव्यायै नमः। ॐ आकाशमात्रे नमः। ॐ सम्हर्यै नमः। ॐ दंष्ट्रालायै नमः। ॐ शुष्करेवत्यै नमः।]{style="font-style:italic;color:rgb(51, 204, 0);"}\
[जल-तत्त्वात्मिकायै नमः॥ इति जल मण्डलं]{style="font-style:italic;color:rgb(51, 204, 0);"}

[मम शत्रून् प्लावय प्लावय ।]{style="font-style:italic;color:rgb(51, 204, 0);"}

[ॐ पिपीलिकायै नमः। ॐ पुष्टिहरायै नमः। ॐ महापुष्टिप्रवर्धनायै नमः। ॐ भद्रकाल्यै नमः। ॐ सुभद्रायै नमः। ॐ भद्रभिमायै नमः। ॐ सुभद्रिकायै नमः। ॐ स्थिरायै नमः।]{style="font-style:italic;color:rgb(51, 204, 0);"}\
[पृथिवि-तत्त्वात्मिकायै नमः॥ इति पृतिवि मण्डलं]{style="font-style:italic;color:rgb(51, 204, 0);"}

[मम शत्रून् ताडय ताडय]{style="font-style:italic;color:rgb(51, 204, 0);"}

[ॐ निष्ठुरायै नमः। ॐ दिव्यायै नमः। ॐ निष्कंपायै नमः। ॐ गदिन्यै नमः।]{style="font-style:italic;color:rgb(51, 204, 0);"}

[आकाश चतुर्कोनात्मिकायै नमः ॥ इति आकाश चतुर्कोणं]{style="font-style:italic;color:rgb(51, 204, 0);"}

[ॐ सूर्यछन्द्राभ्यां नमः ॥]{style="font-style:italic;color:rgb(51, 204, 0);"}

[ॐ ह्रीं कर्णमोटिनि बहु-रूपे बहु-दंष्ट्रे ह्रूं फट् ।]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[ॐ हः ॐ ग्रस ग्रस कृन्त कृन्त छक छक छक ह्रूं फट् नमः ॥]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}\
[\
]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}

He shall meditate upon कर्णमोटिनि who assumes many forms, with a core emanation with two arms bearing a noose and an अङ्कुश, seated in the ललितासन. He shall than offer oblation to the fire with the मूल mantra and then gaze upon the yantra generating her image through the process of spanda.


 1.  By concentrating on the passage of prANa between the हृदय to the मूलाधार, and from there to the vishuddhi while meditating on the incantation he shall invoke कर्णमोटिनी to strike the शत्रू with roga and मारण.


 2.  By concentrating on the prANa passing through the lalana chakra he invokes कर्णमोटिनी brings prosperity.

 3.  By passing concentrating on the prANa flowing from the आज्ञ्न chakra to the minute बलवान् chakra he shall invoke कर्णमोटिनी to perform stambhana and उच्छाटन.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/blogger/2010/410/320/karna_yantra.jpg){width="75%"}
```{=latex}
\end{center}
```




