
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Game Levels](https://manasataramgini.wordpress.com/2005/02/23/game-levels/){rel="bookmark"} {#game-levels .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/23/game-levels/ "5:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There were the two players of Tetris who were known as Wump and Tump. They Wump and Tump were good each playing at their respective level. But obviously one was better than the other. My स्त्री finally gave me a hint of which level I was at. I feared I was the worst of the pack but in the least there were others worse than me. So assuming that my कामिका was right, may be I do play at a reasonable level. The two नित्यश्री-s who always advised me had proposed the two laws: One was: Play the game at the right level. After 4 trials you will know your level at a sub-conscious level but will need somebody to actually tell you that before you actually know it. The other was: Things could have been worse.

Then my कामिका of unsteady eyes asked me to pass through the 3 levels. I am enjoying the transient bliss of level 1 but never know if could come out of it and graduate. I tell her that it is all depends on her, whether I graduate or not. Remember Wump was happy with scoring hundreds at his favorite level. What will happen to us next? I am thinking thus when the two नित्यश्रीस् tell me: You will only grasp everything when you narrate the story of the purchase and sales of the two cell phones.


