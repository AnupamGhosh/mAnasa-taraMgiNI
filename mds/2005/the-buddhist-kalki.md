
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Buddhist kalki](https://manasataramgini.wordpress.com/2005/03/01/the-buddhist-kalki/){rel="bookmark"} {#the-buddhist-kalki .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 1, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/01/the-buddhist-kalki/ "3:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The bauddhas have internalized the last incarnation of विष्णु, the all-destroying kalki or pramati who is said to annihilate the mlecchas and dasyus and restore the world to the Aryas. The कालचक्र tantra of the unclean उपचीनाचार stream gives a highly remarkable account of the end of the yuga. It's parallels to the mainstream Hindu version are unmistakable and appears to have been clearly derived from it \[It is even possible that there was a now lost Hindu version which might have been a closer precursor of the version in the कालचक्र tantra]. But the inspiration for the account from the dreaded Islamic violence is very clearly seen and perceived by its author, and incorporated in typical prophetic style seen in the पुराणस् (verse 1.150 onwards of the कलाचक्र tantra). The nature of Islam was clearly understood by these bauddha authors.

The 25th king of shambhala, will be raudra कल्की or raudra chakrin, wielder of the terrible disk. This raudra कल्की is कृष्ण himself.

Aadam, Enakh, Ibrahim and five others endowed with dark tamas in the family of asura-नागस्, namely Musa, Isa, the White--Clad One, Muhammad, and Mathani, who is the eighth belonging to utter darkness will arise. The seventh will clearly be born in the city of bhagadatta (Baghdad) in the land of Mecca, where the mighty, merciless structure of the mlecchas, that the demonic manifestation, lives in the world. They will kill camels, horses, and cattle, and cook the flesh together with blood. They cook calves with butter and spice, and rice mixed with vegetables, all at once on the fire. Where the men eat that together with dates, and where they drink birds' eggs, that is the place of the Meccan demons.

At the end of the yuga, among those rulers of shambala, in the orderly reckoning of twenty-five reigns, raudra कल्की, the lord of the gods, saluted by the best of gods, shall appear in the lineage of कल्की. For साधुस् his peaceful form shall be the giver of ultimate bliss, on the other hand, he shall be the nemesis for the entire assembly the of mlecchas. The great chakrin will be mounted on a mountain horse, with an astra in his hand, and with the radiance of the sun, shall smite all the mleccha enemies.

At the time when hand (2) with age (4)* number (=24) of descendents have passed within the lineage of Kalki, the mleccha dharma will definitely be introduced from the land of Mecca. At that time when the vicious lord of the mlecchas, कृण्मति, is holding sway, at the time of the lord raudra कल्की, saluted by the best of gods, a fierce battle will occur on the abode of the surface of the earth.

The great कल्की shall come out at the end of the yuga, from the city the devas fashioned on कैलाश parvata. He shall smite the mlecchas in battle with his own four-division army, in a furious war raging on the entire surface of the earth. The fierce rudra, the valiant skanda and his brother गणेन्द्र, and hari as well, shall enpower and accompany कल्की. The great gods indra, agni, वायु, वरुण, yama and the awful goddess निरृति will also join कल्की in the war. And so shall the mountain horses, the lords of elephants, the descendents of manu in gold chariots, and their warriors with weapons in hand. The mighty lord of the monkeys hanumAn shall also join the fight with his hosts. The arrows of the great warriors on कल्की's side will unobstructedly pierce what ever they aim at and thus rain havoc on the mlecchas.

Various asura मारस् will come to the aid of the Meccan demons, but the arrows of agni and शण्मुख, the nooses of वरुण and gaNapati, the immense vajra of indra, and other weapons of the gods will destroy them and the mlecchas completely. hanumAn shall smash the great general of the mlecchas who will be riding a horse. Some the weapons used are described as: A waving wind-cloth and a post together with banners are fastened in one ocean--cornered platform. Men pull the machine from the back side, and it goes up from the earth by the rope. Rising up by the wind, it definitely goes in the sky above the fort on a crag. The fire-oil cast from that machine completely incinerates the mleccha army and the entire fort.

Finally, the bearer of the wheel, raudra kalki smote the lord of the Meccan demons, कृण्मति, with his astra and destroyed him for good. After that raudra-kalki will restore the dharma on earth and will return to the abode of bliss from which he came.

\*Note the use of the Hindu styled numeral code (like the कतपयादि).


