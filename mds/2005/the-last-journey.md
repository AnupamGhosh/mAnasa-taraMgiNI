
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The last journey](https://manasataramgini.wordpress.com/2005/10/24/the-last-journey/){rel="bookmark"} {#the-last-journey .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 24, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/24/the-last-journey/ "12:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[](http://www.intratext.com/IXT/SAN0010/LM.HTM){#2FU9}Having completed his existence as he has passed into the lists of पितृस्, joining the line of आङ्गिरसस् and इक्ष्वाकुस् of yore (Ardra कृष्ण षष्टि; Ashvina mAsa; kali yuga 5106).

[इमं यम प्रस्तरमा हि सीदाण्^गिरोभिः पितृभिःसंविदानः /]{style="font-style:italic;color:rgb(255,102,102);"}\
[आ त्वा मन्त्राः कविशस्ता वहन्त्वेना राजन्हविषा मादयस्व //]{style="font-style:italic;color:rgb(255,102,102);"}

Come, sit on this bed of grass, O yama, along with the आङ्गिरसस्, the fathers.\
let the mantras recited by the kavis bring you O king yama, let this oblation make you happy.

[अन्^गिरोभिर गहि यज्ञियेभिर् यम वैरुपैर् इह मदयस्व /]{style="font-style:italic;color:rgb(255,102,102);"}\
[विवस्वन्तम् हुवे यह् पित ते।अस्मिन् यज्ञे बर्हिस्ह्यनिसद्य //]{style="font-style:italic;color:rgb(255,102,102);"}

Come, yama, with the आङ्गिरसस्, the ritualists, rejoice here with many fold आङ्गिरस sacrificers.\
To sit on grass at our rite, I call विवस्वान्, too, thy father here.

[अग्निष्वात्ताः पितर एह गच्छत सदः-सदः सदत सुप्रणीतयः /]{style="font-style:italic;color:rgb(255,102,102);"}\
[अत्ता हवींषि प्रयतानि बर्हिष्-यथा-रयिं सर्व वीरं दधातन //]{style="font-style:italic;color:rgb(255,102,102);"}

Fathers whom agni's flames have eaten, come here: you well disposed guests, take ye each your place in order.\
Eat sacrificial offerings presented on the grass: grant riches with a many hero sons.

[](http://www.intratext.com/IXT/SAN0010/B8.HTM){#2G4W}[सूर्यं चक्षुर् गच्छतु वातम् आत्मा द्यां च गच्छ प्ऱ्‌थिवीं च धर्मणा /]{style="font-style:italic;color:rgb(255,102,102);"}\
[अपो वा गच्छ यदि तत्र ते हितम् ओषधीषु प्रति तिष्ठा शरीरैः //]{style="font-style:italic;color:rgb(255,102,102);"}

The sun receives your eye, वात your breath; go, as per your dharma proceed to earth or the skies.\
Go, if its thus destined, to the waters; go, be stationed in the plants with the elements of your body.

Proceed on the path as manu did, as इक्ष्वाकु did, purukutsa did and the great bowman and hero trasadasyu did. May the fierce yama, who is stalking with fierce nooses, ensnaring all, let that line of trasadasyu to continue.\
[](http://www.intratext.com/IXT/SAN0010/SWU.HTM){#2G5D}[](http://www.intratext.com/IXT/SAN0010/GOG.HTM){#2G2T}[]{#2FUY}


