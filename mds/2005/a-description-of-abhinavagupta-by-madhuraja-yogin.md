
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A description of abhinavagupta by मधुराज yogin](https://manasataramgini.wordpress.com/2005/11/15/a-description-of-abhinavagupta-by-madhuraja-yogin/){rel="bookmark"} {#a-description-of-abhinavagupta-by-मधरज-yogin .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 15, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/15/a-description-of-abhinavagupta-by-madhuraja-yogin/ "6:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the greatest tantrics of all times was the prolific trika savant abhinavagupta, one of the greatest Indian philosophers of all times. He wrote at least 44 books on tantra, aesthetics (including नाट्यशास्त्र) and philosophy. His student मधुराज yogin leaves the following account:\
He sits like दक्षिणामूर्ती himself as though he has taken a new incarnation in Kashmir. He sits in the middles of a garden of fruits, inside a splendid pavilion filled with elegant paintings. The room smells wonderfully because of he flower garlands, incense sticks and lamps. The walls are smeared with sandal paste. There is music from instruments and singing and dancing in his pavilion. He sits on a golden seat and has pearls decorating his canopy. abhinavagupta is attended by his many students with क्षेमराज at their head who write down the various topics he discourses on. To his side are his two female consorts, who are his कुलाङ्गनास् in the tantric rites. They hold a pot of wine, a box of betel-leaves, lotus and मतुलुङ्ग fruits. abhinavagupta's eyes are filled with the esctasy of tantric bliss. In the middle of his forehead is the conspicous triad mark of विभूती. He has one ear-ring with a रुद्राक्ष bead. His long locks are held by a garland of flowers, he has a long beard and his neck is smeared with the tantric यक्षपङ्क powder. His यज्ञोपवीत hangs from his neck and he wears a silken dhoti white as the moon beams. He sits in the yogic वीरासन position holding a रुद्राक्ष माला and shows a mudra signifying the knowledge of महादेव.


