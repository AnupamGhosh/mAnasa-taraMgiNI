
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mahA-कुमार-rahasya](https://manasataramgini.wordpress.com/2005/11/18/the-maha-kumara-rahasya/){rel="bookmark"} {#the-maha-कमर-rahasya .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 18, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/18/the-maha-kumara-rahasya/ "6:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great rahasya of the 6-headed one goes thus:

 1.  First there is the प्रणव, which encompasses the अक्षरस् from श्रीकण्ठ the father of the young god to the last of the pentads of brahman.

 2.  कामरूपं वह्न्यारूढं बिन्दुमालिनीभुषितं कालबिन्दुमिलितं (the first core bIja)

 3.  त्र्यश्रं योनिः (the 2nd core bIja)

 4.  त्रितीय \[tithi] iti इच्छा ( the 3rd core bIja)

 5.  उन्मेष (the 4th core bIja)

 6.  आकाश तत्त्वं cha अनुत्तरं (5th core bIja)

 7.  तस्यान्ते विष्णुप्रिया ( for the second time 6th core bIja)

 8.  वह्नी वामलोचना cha binduruttara रूपं (7th core bIja)

 9.  तस्यान्ते द्वितीय षडाक्षरी iti //

After having strung the बीजस् and combined them with the second षडक्षरि of कुमार you get the महामन्त्र. Of all the विद्यास् of कुमार it is the most difficult to grasp.


