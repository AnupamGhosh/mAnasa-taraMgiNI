
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rise of the Kirghiz Khaganate](https://manasataramgini.wordpress.com/2005/09/01/the-rise-of-the-kirghiz-khaganate/){rel="bookmark"} {#the-rise-of-the-kirghiz-khaganate .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/01/the-rise-of-the-kirghiz-khaganate/ "5:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Kirghiz Khaganate came to the fore after Uighurs were destroyed by the Kirghiz in the 840 AD. The rise of the Kirghiz was shrouded in mystery. All we knew was that a Uighur general defected to the Kirghiz with critical information. The Kirghiz were in rebellion to avenage the Uighur campaign of 740 AD, where the latter had demolished the Kirghiz completely and conquered their lands on the Ob and Yennesei. Before that the Blue Turks had also suppressed the Kirghiz and subjugated them, in course of the breath-taking campaigns of the great Blue Turk warriors Kueltegin and Bilge Kha'Khan. Kirghiz suddenly rebounded in 840 AD and having routed the Uighur Khaganate, seized the empire of Mongolia for about a 100-120 year period. How did this happen?

This information was always around in the form the national epic of the Kirghiz- their hero after whom they named places after the Soviet retreat from their lands. We glean information of the great rise of the Kirghiz from their national epic [Manas ]{style="font-weight:bold;color:rgb(255, 0, 0);"}by sifting through the mythic and mideaval layers. The history begins with the first struggle of the Kirghiz under their Khan Nogai followed by the disastrous defeat by the Uighurs. This was followed by the rise of Khan Qoguedei Mergen, who organized the Kirghiz tribes. He was followed by his sister's son Khan Manas who is the hero of the epic. This khan led the Kirghiz to the great victory of 840 against the Uighurs and then he opened hostilities with the Chinese. Manas was followed by his son Khan Semetei who continued the expansion of the Kirghiz empire against the Chinas. The final stabilization of the kingdom with the surrender of China was achieved by his son Khan Seytek. For the Kirghiz Manas is their Chingiz Khan. In many ways Manas is like a prelude to Chingiz Khan, a hero of the steppes, have all the pre-adaptation that was one day going to spawn the phenomenon called Chingiz, but not yet reaching his oceanic proportions.


