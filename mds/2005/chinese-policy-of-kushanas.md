
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Chinese policy of कुशाणस्](https://manasataramgini.wordpress.com/2005/11/26/chinese-policy-of-kushanas/){rel="bookmark"} {#chinese-policy-of-कशणस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/26/chinese-policy-of-kushanas/ "6:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Under the Kushanas, unlike in the days of later Indian rulers, the Indians followed an active policy of supporting the central Asian oasis civilization in the Tarim region. This forward policy was adopted by Vima Taktu, furthered by Vima Khadphises and Kanishka. The most important victory was that of Kanishka in central Asia was against the Chinese, when he came to the aid of the Tarim chiefs, who had been severely oppressed by the Chinese warriors like Pan Cha'o. Having routed them, he brought four Chinese princes as hostages to India and detained them in fort near what is now Amritsar. The village interesting bears the name Chiniyari, as a distant echo of the long past days. These Chinas are supposed to have introduced the pear to India. This is one of the few defeats of the China army at the hands of the Indians that even the chronicle of Huan Tsang records. Kanishka to convey his international status to all concerned adopted 4 titles simultaneously: महारजाधिरज (Emperor of India), Kaisara (for the romans), Kshatriya-tama (for the Persians), and deva-putra (for the Chinese). So firmly was Sogdiana, brought under the Indian sphere of influence that the Chinas referred to it as utto-lo-pa-do: उत्तरापथ or the northern frontier of India. Of course Kanishka's entry in to Central Asia, other that its economic benefits, resulted in a tremendous injection of sanskrit literature into China many of whose original were lost in India. This process to a certain extant actually improved Sino-Indian relations and facilitated technological exchange between the then superpowers: the Parthian empire, India and China. Kanishka's domain stretched from the Tarim regions to Central India. His southern capital was Mathura where some statues of his vandalized by the Moslems can still be found. From a Hindu standpoint the Kushana regime was critical for the rise of Kaumarism in India.

It is perhaps very likely that, Western powers of today wish to curtail India by propping up the encircling Moslem states. Because if these Moslem potentates were smashed, India's cultural and military influence would flow to regain its natural extant in the direction of the Roof of World.


