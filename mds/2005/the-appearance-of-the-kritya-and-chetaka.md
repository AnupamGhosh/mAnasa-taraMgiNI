
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The appearance of the कृत्या and chetaka...](https://manasataramgini.wordpress.com/2005/11/07/the-appearance-of-the-kritya-and-chetaka/){rel="bookmark"} {#the-appearance-of-the-कतय-and-chetaka .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 7, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/07/the-appearance-of-the-kritya-and-chetaka/ "6:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

["Everyone knows well how the भरद्वाज yavakri met his end due the kAshyapa's spells."\
]{style="font-weight:bold;"}We had fallen back from सुवर्गं लोकं. We then started walking along the great path, when we saw a ratha similar to that of our ally of उत्कील's clan. We turned around to see the ratha, when we sensed someone was coming in front of us. We turned and were surprised to see a beautiful woman, dressed very formally. On such a desolate day, at such an hour and time it was very unusual to see such a woman coming out from such familiar territory down such a familiar path. The woman passed us and we glanced her smile a sinister smile. Having passed behind us she vanished. Through our third eye we realized that it was not a woman but the कृत्या. We then saw the chetaka attempting to bite at the kavacha but failed to do so. Then the chetaka lobbed a तीक्ष्ण क्षुरिक at us through the kavacha and then vanished.[\
]{style="font-weight:bold;"}


