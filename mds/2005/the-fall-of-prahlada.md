
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The fall of asura प्रह्लाद](https://manasataramgini.wordpress.com/2005/08/07/the-fall-of-prahlada/){rel="bookmark"} {#the-fall-of-asura-परहलद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 7, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/07/the-fall-of-prahlada/ "5:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

प्रह्लाद, the demoniac son of हिरण्यकशिपु, ascended the throne of the asuras after his father had been slain by नृसिम्ह. Spreading fear and havoc in the in the worlds due to his pride and immense strength he caused much anxiety to the gods. It was then that the young कुमार, the commander of the deva army, filled with youthful pride decided to display his sports. He threw his shakti and pierced the earth with it. He then posed a challenge to the universe by stating:

"If there be any being that is superior to me in might or protects the brahmins better than me, or protects the veda and the brahman better than me or is possessed of energy like me, let him draw up this spear or at least shake !"

विष्णु noticed that the devas, यक्षस्, asuras and राक्षसस् were filled with consternation at this challenge. विष्णु, himself unable to bear the challenge with respect to moving the shakti looked at the son of agni and stepped forward. The mighty विष्णु seizing that blazing shakti with his left hand began to shake it. When the missile was thus shaken by trivikrama of great energy the earth shook violently. There were upheavals in the sea, the mountains quaked, and the huge forests trembled and fell. विष्णु then turning to the asurapati प्रह्लाद said: "*I have shown my might by shaking the spear and stopped for the good of the earth. Behold the might of कुमार. None else in the universe can raise this shakti.*" Unable to bear the challenge the fierce asura seized with all his hands and tried to shaken it. But he was unable to move it and swooning from his exertions fell head long on the हिमालायस्. Thus was the fall and the defeat of the daitya प्रह्लाद.\
shalya-parvan 328


