
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [खान्डवप्रस्थ](https://manasataramgini.wordpress.com/2005/02/19/khandavaprastha/){rel="bookmark"} {#खनडवपरसथ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 19, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/19/khandavaprastha/ "7:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Those battles were over. Our gaping breaches in the fronts of sharirasya ओषधि, dagdaratha and kosha remained, but the realization dawned on us that we had won the encounter. Our अमात्य and सचीव had been unfortunately cursed with naraka which had visited then in the form of the dreadful possession of the old one. indra had however given them a boon that they would pass through naraka in pleasant surroundings. We had invested the fort of खाण्डवप्रस्थ after much fighting. Our internal affairs were relatively precarious but we now ruled over खाण्डव. अजदाढिक, विट्मुख and मुण्डकच lay dead or wounded on the field. We roamed around in खाण्डवग्राम not knowing where to start and what to do. We received a congratulatory message from the [Princess of Hayastan]{style="color:rgb(51,255,51);"}, who then asked to try to cultivate masura lentils in खाण्डव.

Hayastanika then asked me narrate the story of the sale and the purchase of the two cell phones.


