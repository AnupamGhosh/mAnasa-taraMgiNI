
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [disposing उच्छिष्ट](https://manasataramgini.wordpress.com/2005/11/11/disposing-uchchishta/){rel="bookmark"} {#disposing-उचछषट .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 11, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/11/disposing-uchchishta/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Lie down comfortably on the bed, (eating a sweet or ताम्बूलं if you want) and then repeat the formula 44 times:\
gaM haM क्लौं glauM उच्छिष्टगणेशाय महायक्षायायं बलिः //

Then without अचमनं he shall go to the outside arena (ideally a श्मशान or 4-cross roads) and mark a square area out with his fingers and then utter om + नमः for the following names in the ten directions in प्रदक्षिण order facing north, then up+down.\
वक्रतुण्ड, एकदंष्ट्र, lambodara, विकट, धूम्रवर्ण, vighna, गजानन, विनायक, gaNapati, hastidanta.

The he shall perform homa by lighting a fire in the square and offering the उच्छिष्ट to ulka with the मूलमन्त्र cited above: gaM haM क्लौं glauM उच्छिष्टगणेशाय महायक्षायायं बलिः //\
The oblations shall be done with a betel leaf till the उच्छिष्ट is over. He may make a final oblation of a garlic pod to make his enemy lose wits and fail to do rites.


