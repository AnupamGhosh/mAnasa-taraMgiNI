
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some considerations on the tantric path](https://manasataramgini.wordpress.com/2005/11/15/some-considerations-on-the-tantric-path/){rel="bookmark"} {#some-considerations-on-the-tantric-path .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 15, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/15/some-considerations-on-the-tantric-path/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I decided to put down some matters arising in the discourse with R during the winding train journey.

The following are listed as authoritative tantras by the great trika tantrics:

 1.  कामिक 2) किरण 3) निःश्वास 4) पौष्कर 5) raurava 6) स्वायम्भुव 7) पारमेश्वर.\
These may be termed the proper शैवागमस्\
(some of these contain important details concerning the पञ्चाक्षरि of महादेव, of which another aspirant had made some recent enquiries to me).

The trika tantrics also mention the following tantras, many of which are far more obscure. 1) The rudra यामल of which only fragments survive and the परात्रिशिका is of particular interest to the trika savant abhinava. 2) निर्मर्याद tantra- an obscure "left-handed" tantra of which only small fragments are known to practioners 3) कालिकुल tantra- the basic doctrine of भाणवि kaulini is expounded here. 4) उर्मिमहा tantra 5) ratna माला 6) trishiro-bhairava 7) स्वच्छन्द: This enormous saMhitA lays out the path of the highest yoga of प्राणायाम and meditation on the प्रणव, amongst other mantras. 8) siddha-योगेश्वरी-mata 9) देवी यामल 10) मृत्युङ्जय vidya 11) मालिनी vijaya: This tantra appears to have been originally a section of the great siddha-योगेश्वरी-mata. abhinavagupta composed a 1500 verse commentary on this text. 12) विज्ञान bhairava: The विज्ञान bhairava text is considered an उपनिषद् and the kAshmirian tantric क्शेमराज has expounded a commentary on it.

Some of these tantras clearly go under different names. The मृत्युङ्जय vidya is also known as the netra tantra and expounds the vedic and tantric rituals related to the मृत्युजित् विद्या at length. It is a very important tantra that provides the critical mantras that provide defense against मारण attacks and from an academic viewpoint the various early lineages of the tantric schools. A major commentary on it is by the great क्शेमराज.


