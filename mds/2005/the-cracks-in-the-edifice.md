
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The cracks in the edifice.](https://manasataramgini.wordpress.com/2005/11/13/the-cracks-in-the-edifice/){rel="bookmark"} {#the-cracks-in-the-edifice. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 13, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/13/the-cracks-in-the-edifice/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In 1326 the Army of Islam had completed the conquest of Hind and almost all of the old Hindu dynasties from the royal shishodias and छाहमानस् in the north, the यादवस् and hoyasalas of Middle India and the Cholas and Pandyas of South India lay prostrate before the cresent banner. The great edifice of India, starting from the kuru and the पाञ्चाल, through the mauryans, shungas, guptas and their successor states was destroyed. The last in this long line of India states in central India was the यादव state that gained control of central India from the चालुक्यस् of मान्यखेत and कल्यानि. They established their capital in devagiri and were blessed with 4 notable rulers singhana, कृश्णदेव, महादेव and रामदेव. Their long-lived brahmin minister was the illustrious हेमाद्रि from the कर्णाट country whose enormous breadth of scholarship was illustrated in the encyclopaedic work chaturvarga चिन्तामणि and its colophons. He developed a special type of temple architecture known as the hemadripantha style. He was a physician and agriculturist who developed the cultivation of bajra as a nutrious food for arid zones in Maharashtra. Above all he provided a legal and administrative scaffold for the यादव kingdom much as the brahmin विज्ञानेश्वर had done for the चालुक्यस् before them. Other great scholars of the यादव kingdom were keshava the physician, vopadeva the grammarian and physician and भास्कर II the mathematician. There were revolutions in Hindu medicine in the यादव period, where the great scholars developed diagnosis of disease considerably over the earlier Hindu treatises. It also saw the rise of differential calculus in the work of भास्कर II.

It was this well-administered Hindu state that suddenly collapsed under the assault of the most terrible Islamic invader, Ala-ad-din Khalji. In 1296 ACE Ala-ad-din suddenly invaded Devagiri when the main army of the यादवस् was away in the south near the border with the Hoysalas. First the कृष्ण a यादव general attacked the invading Moslem force. The कृष्ण was ambushed by the fast-moving Moslem horsemen and killed. The royal kShatriya women taking up arms fell upon the Moslem army. So fierce was the charge the of the Hindu women that the Moslems were beaten. But Nusrat Khan arrived with more reinforcements and the Moslems finally destroyed the fighting women. The King रामदेव was not aware of the attack and was surprised by the Moslem force. He was forced to hole himself in the impregnable fort of Devagiri, which was one of the greatest military strongholds of India. But Ramadeva ironically could not make use of this wonderful stronghold, because he was low on supplies. But रामदेव' son singhana II returned with his army and vigorously attacked Ala. Ala was in a perilous situation as his ranks wavered under the Hindu counter-attack. But the wily Turk had cleverly placed an army under Nusrat Khan to engage रामदेव and to prevent him from sandwiching the Moslem army between Devagiri fort and singhana's forces. Seeing Ala in trouble Nusrat Khan left his position and came to reinforce the attack on singhana. Ala spread the rumor that Nusrat was arriving with 20,000 horse even though he had only 2000 horsemen. singhana's men bought the rumor and disobeying their commander's words and scattered. The Moslem invader killed numerous ब्राह्मणस् and rich vaishyas spreading terror in the countryside. रामदेव with his army and state in utter disarray surrendered meekly, delivering his enormous treasury to Ala-ad-din which comprised of 22 tons of gold, 260 kgs of pearls, 37 tons of silver, 74 kgs of precious gems, 4000 rolls of silk and numerous elephants. He also raped a daughter of रामदेव and carried her away.

Why did the famed Hindu kingdom of the यादवस् collapse so dismally before the invading Moslems. It is the answer to this that reveals how important वर्ण and the sanskrit language were for the stability of the Hindu state and how disruption of it exposed the Hindus to external assault.

The secret for this lay in the activities of the मानभाव् (महानुभाव) cult. A brahmin name haripaldeva came under the influence of a heterodox saint govinda prabhu and was given दीक्ष under the name chakradhara. chakradhara toured througout Maharashtra and got many brahmins under his control founding the मानभाव् cult. His emotional oratory drew huge crowds of disciples who soon started considering him a deity. He promulgated many rules regulating the lives of his followers and began by a series of attacks on advaita. But it soon broadened as he propounded several heterodox ideas: 1) He rejected the authority of the veda and vedic rituals, while accepting the उपनिषद्स् as having good thoughts. 2) he vigorously attacked the वर्ण system and called for non-recognition of जातिस्. 3) He denounced the worship of many gods, and believed that कृष्ण was sufficient as a single supreme god and other being irrelevant and not worthy of any worship. 4) He rejected the concept of ritual purity and also attacked the tantrics vigorously as practioners of "evil". 5) They rejected the use Sanskrit and prohibited its use in ritual and composition, instead favoring archaic Maharatti and local dialects.

Seeing anti-वर्ण and ब्राह्मण messages being propagated by the मानभाव् cult and winning over of several converts, the great minister हेमाद्रि intervened to block his influence during the early phase reign of रामदेव यादव. But chakradhara's many converts acquired considerable influence on the royal elite and started replacing the brAhmin ministers with his own agents. This widespread interference resulted in decay of the administrative system so carefully developed by हेमाद्रि and his administrative council. Seeing the trouble hemadri sent his troops and killed chakradhara. But रामदेव was goaded by the many followers of chakradhara, who raised the issue that hemadri was acting without the king's approval and was a threat. This resulted in रामदेव charging hemadri of murder and executing him by crushing him with an elephant and dismissing hemadri's appointees. The result of this was a break down of the civil service and many regional intrigues and disrespect for brahminical administration under महानभाव् instigation. More dangerously the महानभाव्स् leaked intelligence to various Moslem Sufis and spies hoping that they could secure Moslem help against chiefs like कृष्ण who still stood by हेमाद्रि's strong orthodoxy.

We observe that the Hindu edifice has ever since been similarly attacked by pernicious movements.


