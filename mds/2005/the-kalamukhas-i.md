
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कालामुखस्-I](https://manasataramgini.wordpress.com/2005/02/23/the-kalamukhas-i/){rel="bookmark"} {#the-कलमखस-i .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/23/the-kalamukhas-i/ "6:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the most enigmatic of the shaiva groups of India were the कालामुखस्. Their origins and the rise to power are shrouded in mystery. An added mystery is their suddence disappearance despite their prominent presence in the mideaval period. I stood before a great shrine of The god once built by the कालामुखस् in the city of my birth and wondered about their origins. They were described as कालामुखस् after the practice of marking their foreheads with a black streak. There were two great schools or councils (परिषद्स्) कालामुखस्. The सिंह परिषद् was common in Andhra and the shakti परिषद् was common in Karnataka. The latter परिषद् is most actively represented in epigraphic records studied by Lorenzen. Though there is not much evidence for the कालामुखस् having any presence elsewhere in India, the South Indian epigraphic records suggest that they were also present in Kashmir. We do not find much connection between the कालामुखस् and the much maligned कापालिकस्, and the siddhAnta shaiva streams. The main center of the shakti parishad was the दक्षिण केदारेश्वर temple in North Karnataka from where the कालामुखस् fanned out into the rest of the state. From the inscriptions we get the following lineage of the कालामुखस्.

The founding अचार्य was the learned केदारशक्ति. His first student was रुद्राभरण. His student was वाल्मीकि. केदारशक्ति's second student was श्रिकण्ठ. His student was someshvara. His student was विद्याभरण. He had 3 students वामशक्ति, gautama and ज्ञानशक्ति-I. gautama's student was वामशक्ति-II. His student was ज्ञानशक्ति-II. His student was श्रिकण्ठ-deva II. His student was वामशक्ति-deva III. Another line from ज्ञानशक्ति-I was started by his student चन्द्रभूषण. His student was क्रीयाशक्ति. Thus from late 900s to 1225 the कालामुख scholars dominated the scene in Karnataka.

The inscriptions suggest that the कालामुखस् were the direct representatives of the पाशुपत stream. This is the tradition founded by the ancient लाकुलीश, who described in the shiva पुराण as the promulgator of the पाशुपत cult. The antecedents of the पाशुपत stream can be traced back the परिशिष्ठस् of the atharva veda which describe the ancient version of the पाशुपत vrata. For example the आचार्य रुद्राभरण is mentioned as being "an ornament of the लाकुल-doctrine". The modern orthodoxy, has tended to malign the कालामुखस् as a heterodox school that is to be shunned by the mainstream brahmins. However, the inscriptions clearly show that the कालामुखस् were not heterodox followers of non-Aryan ways but a branch of brahmins. For example the Kedareshvara inscription states that "the कालामुखस् study in the secluded and quiet मठस्, the ऋक्, yajur, sAman and atharvan संहितस् with their auxiliary texts. A scholarly study is also made of the कौमार, पाणिनीय, शाकटायन शब्दानुशासन grammars. They also are mentioned as studying: nyAya, वैशेषिक, मीमाम्स, सांख्या and bauddha philosophy as well as पौरणिc and secular संस्कृत् literature, in addition to the लाकुलीश tantras". In particular many of the कालामुखस् were specialists in Hindu atomism of nyAya and वैशेषिक. One of them someshvara was a particular expert in these schools as per one of the inscriptions. The कालामुख मठ is also mentioned as feeding jaina and bauddha mendicants in addition to Hindu brahmins and संन्यासिस्.

Some कालामुख आचार्यस् of both परिषद्स् in the Mysore region bear the title: काश्मीर paNDita. More explicitly the Muttagi inscription from Bijapur states that a certain कालामुख paNDita had migrate to Karnataka from Kashmir. Thus, at least the कालामुख cult had a certain adherants in Kashmir. The Ghaznavid invasion ravaged the two great पाशुपत centres: Mathura of the kaushika lineage and Somanatha of the गार्ग्य lineage. So it is quite likely that some of the North Indian scholars migrated to Karnataka and Andhra which were relatively safe from Islamic violence at that point. Thus, an understanding of the place and distribution of the कालामुखस् in the larger matrix of पाशुपत cults has been largely erased as their sister groups were destroyed in Somanatha and Mathura by the Islamic ravages. Like Buddhism, the eventual fall of the कालामुखस् can be attributed to their highly centralized monastic structure. When Islam struck, these centralized विहारस् or मठस् that were entirely dependent on massive patronage and solely devoted to scholarly pursuits were blown away resulting their rapid extinction. In contrast the more amorphous Hindu groups dispersed over the countryside survived and revitalized the Hindu world in face of the Islamic assault.


