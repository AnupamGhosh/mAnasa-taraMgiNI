
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The heart of the tantra](https://manasataramgini.wordpress.com/2005/03/11/the-heart-of-the-tantra/){rel="bookmark"} {#the-heart-of-the-tantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 11, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/11/the-heart-of-the-tantra/ "6:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The very word tantra conveys that deep meaning to the "knowers". That secret power that resides in the '[हृदय]{style="color:rgb(51, 204, 0);font-weight:bold;"}' of bhairava and is known as [कौलिनी]{style="color:rgb(51, 204, 0);font-weight:bold;"}[. ]{style="font-weight:bold;"}The [अनुत्तर]{style="color:rgb(255, 102, 102);"}of its own confers [कौलिक]{style="color:rgb(0, 153, 0);font-weight:bold;"}perfection. The tantra is like a science because it is reproducible in its real form. The साधक on the true path first gets a glimpse of "[kaulika existence]{style="color:rgb(255, 255, 102);"}" when he can grasp the the सम्वाद of मन्थान bhairava and tripura भैरवी. If he can go beyond, the bIja mantra activates the apparatus of the chakras and the ever-sleeping कुब्जिका awakens and courses on the "path". The साधक then experiences the awe as he realizes the enfolded state of the [विश्व]{style="font-style:italic;color:rgb(255, 0, 0);"}in the '[हृदय]{style="color:rgb(51, 204, 0);font-weight:bold;"}'. If the साधाक can go beyond, then he is stationed at the the "mid-point". There he sees the oscillation of [visarga ]{style="color:rgb(255, 0, 0);"}moving in the two directions at once. When he reaches there he has reproduced in totality the words of मन्थान bhairava. He reproduces the experience of the guru, he reproduces the experience of the true tantrics of yore. This act of reproducing the experience gives the सधाक the experience of ecstasy. This is like the feeling of doing real science the reproducibility of the observation/ the experiment.

[कौलिनी कुलयोगिनी]{style="font-weight:bold;color:rgb(153, 153, 255);font-style:italic;"}


