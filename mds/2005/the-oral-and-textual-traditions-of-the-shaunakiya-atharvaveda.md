
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The oral and textual traditions of the शौनकीय atharvaveda](https://manasataramgini.wordpress.com/2005/10/01/the-oral-and-textual-traditions-of-the-shaunakiya-atharvaveda/){rel="bookmark"} {#the-oral-and-textual-traditions-of-the-शनकय-atharvaveda .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 1, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/01/the-oral-and-textual-traditions-of-the-shaunakiya-atharvaveda/ "5:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The only complete surviving oral tradition is that of the 4th veda is the शौनकीय(AV-S) recitation. Even this is dwindling now. The पैप्पलाद recitation (AV-P) is not preserved with svaras. An ekashruti recitation is preserved amongst the Orissan atharvavedis, and a few ritually critical mantras AV-P mantras with svaras, especially in the context of the tantric rituals of कुब्जिका उपनिषद् (relating to अथर्वण भद्रकालि and बगलमुखी) are known to some of the surviving atharvavedis (but largely kept secret). S.S. Pandit and Satavalekar have produced reasonable printed texts of the AV-S. Satavalekar's edition depended heavily on Pandit Ramachandra Shastri Ratate of Varanasi, who knew the whole saMhita पाठ orally. An examination of his son's Atharvan recitation illustrates that his had a high fidelity of svaras. Pandit used multiple reciters of AV-S. Among them were Bapuji Jivanram from Gujarata, who a good recitational musicality and knew much of the saMhita orally and also pada पाठ and krama पाठ of the first 4 काण्डस्. The Maharastri AV scholar Keshava Bhatta from Mahuli near Thane knew whole saMhitA and pada recitation except the funerary hymns and also krama of काण्डस् 1-4. Venkan Bhatta another of Pandit's sources was the greatest Maharashtrian reciter of the AV-S who knew whole saMhitA, pada and a considerable part of the krama and जटा पाठस्. The विकृति पाठस् of the AV-S saMhita are now largely extinct with the exception of few ritually critical recitations that are known by a small number of reciters.

In the Vaidik Samshodhana Mandal (Pune) we have seen a manuscript of an archaic मन्त्रलक्षणं on how the atharva vedic जट and krama recitations were constructed. The basic rules are discussed: padas:\
[a, b, c -(krama)-> ab/bc/b iti b\
a,b,c -(जटा)-\>abbaab / bccbbc / b iti b]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}

Further examples are discussed of the actual transformations in action.

जट and krama manuscripts belonging to the Gujarati Pancholi Brahmanas survive, but none of them are complete and nor is an extensive विकृति tradition known from extant Pancholis. The known जटा manuscripts do not correspond to the expected rules of vedic accentuation. However, they are systematic throughout in their differences from the expected rules and from each other. This suggests that there might be many unknown issues with the recitational aspects of the AV-S विकृति which have been lost.

However, the krama पाठ of the 2oth काण्ड of S-AV closely resembles the ऋग्वेद krama पाठ. The RV krama पाठ has not been printed, but I have seen a complete recitation of it by the Vedavikritilekhana Mandal (with fragment Audio recordings) in Pune by a Maharastri ऋग्वेदिc savant, Bhalachandrashastri.\
[]{style="font-weight:bold;font-style:italic;color:rgb(255, 0, 0);"}


