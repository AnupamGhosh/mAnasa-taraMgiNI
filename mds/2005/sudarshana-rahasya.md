
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सुदर्शण rahasya](https://manasataramgini.wordpress.com/2005/07/18/sudarshana-rahasya/){rel="bookmark"} {#सदरशण-rahasya .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 18, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/18/sudarshana-rahasya/ "5:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having bowed to the one sleeping atop the coils of the snake of time we salute the greatest of his weapons.\
oM उरुगायाय hum फट् //

That supreme emanation of the shakti of नारायण manifested with a predominance of her tejas, is as bright as many millions of suns, moons and fires in profusion. It pervades the tryambaka vidya of the 7th मण्डल of the ऋग्वेद. It is described as the supreme udyama, foremost prANa and all weapons and shaktis of the hordes of विष्णु and vishvaksena arise from it. Of नारायण modes of activity based on his five functions this is the most effective. No single act conducted by the supreme विष्णु can performed with its aid. This is the सुदर्शण disc. The emanation of सुदर्शण starts functioning at the moment of the origin of the universe and लक्ष्मि adheres to her chakra-रूप even as नारायण sustains and absorbs creation.

It is the क्रियाशक्ती of विष्णु and is identical with agni who pervades all existence. The secret of the mantra शास्त्र pertaining to the mantras of श्री and नारायण arise arise from the सुदर्शण manifesting as the matrika chakra. The first mantras to emerge are तारिका and taraka, the pair belonging to विष्णु in the axle of the wheel. The 16 svara syllables, possessing the form of the light of soma and sUrya occupy the hub of the wheel. The 24 letters from ka to bha and those from ma to ha occupy the spokes reaching the inner circumference. The वर्गान्त which is identified with agni appears as a पिण्ड and expands to the periphery of the chakra. This is the essence of the secret matrika न्यास of विष्णु.

Taking the first letter of soma, one should join on the letter coded as prANa at its end. Then the letter called अमृत is added after it and it is conjoined to the कालपावक letter there after. After it comes the blazing anala. It is then followed by the conjoining of the mAyA of विष्णु and the व्यापिन्, which is the third stride of trivikrama. This is the highest विद्या of सुदर्शण and is the क्रियाशक्ती of श्री. The कलपावक syllable at the end is the destroyer of foes. For सुदर्शण is awful he must be pacified by the syllables of तारिका and अनुतारिका that emerge from लक्ष्मी after invoking him. The core of this without the mAyA and the व्यापिन् is the पिण्ड form of सुदर्शण.

From it emanates संञ mantra of the great chakra by the interspersion of vyapin-s at the end of each element of the पिण्ड rupa. The syllable of varman is inserted there after and after that comes the syllable of the astra. This syllable is the संकर्षण who destroys the universe at the end of the kalpa with the explosive noise.

Then when the पिण्ड of सुदर्शण is combined with the kavacha syllable and the missile hurled by the wrathful संकर्षण and the mantra is set 3 groups of letters we get that great rahasya mantra of सुदर्शण. This cycle has neither name nor form. It shines with the splendour of the six attributes. This great mantra is the embodiment of the क्रियाशक्ती of वैष्णवी consisting of the six syllables supported by the chakra held by the supreme essence of the atharva veda known as नरायणी प्रत्यङ्गिरा. The लाञ्गलास्त्र of वाराही with which she hammers the daityas emanates from it.


