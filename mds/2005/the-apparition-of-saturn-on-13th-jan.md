
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The apparition of Saturn on 13th Jan.](https://manasataramgini.wordpress.com/2005/02/28/the-apparition-of-saturn-on-13th-jan/){rel="bookmark"} {#the-apparition-of-saturn-on-13th-jan. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 28, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/02/28/the-apparition-of-saturn-on-13th-jan/ "7:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Saturn was at its closest on 13th. Marc spent a while observing it with his 8 inch sweetie. [![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/saturn_13.jpg){width="75%"}
```{=latex}
\end{center}
```



I saw it with my eyes from the plane window just before landing in भारतवर्श.

I always felt Marc was favored by the devas because his लोलाक्षी who can make any male's heart miss a beat or more faithfully accompanies him to every expedition to see the stars and can knowledgeably converse about all these matters because of her "purvajanma" training with me. I wondered what it might take for me to make my कामिका give me company in gazing at the celestial dome. But Marc made it clear that the grass only appears greener on the otherside because R true to her origins does not accompany him on any expeditions in the cold months, especially for something as mundane as an apparition of Saturn. Nor does she seem to encourage his own expeditions. I can appreciate that because I doubt I would want to be out in the snow, even if my कामिका thought that is a really great idea.

Astronomy was an obsession in भारतवर्श, in mlecchadesha it is a rare luxury, like the soma from मूजवन्त्.


