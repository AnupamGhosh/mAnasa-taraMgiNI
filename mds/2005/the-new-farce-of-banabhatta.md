
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The new farce of बणभत्त](https://manasataramgini.wordpress.com/2005/04/25/the-new-farce-of-banabhatta/){rel="bookmark"} {#the-new-farce-of-बणभतत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 25, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/25/the-new-farce-of-banabhatta/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

bANa had just written a new play and was showing it to rAja हर्ष. हर्ष said he was impressed and decided that it must be put upon stage without any further delay. हर्ष called in his entertainers and asked them to make the arrangements. After a while, the entertainers reported to the महाराजादिरज परमभट्टारक of स्थानेश्वर stating that they were hard-pressed to find a suitable विदूषक for the play. The rAja thought for a while and quipped: "I am pretty certain that our paNDita, shri bANa could as well be the विदूषक. " He summoned bANa and asked him to be the विदूषक in his own play. bANa complained: "How can I be the विदूषक? I have never acted before." "All the better"; said the king. "I have decided that you shall be the विदूषक yourself". All bANa could do was to enjoy himself at his own expense.

  -  Any similitude to historical characters is purely coincidental.


