
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The tantric manual of the atharvan-अङ्गिरस्](https://manasataramgini.wordpress.com/2005/04/23/the-tantric-manual-of-the-atharvan-angiras/){rel="bookmark"} {#the-tantric-manual-of-the-atharvan-अङगरस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 23, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/23/the-tantric-manual-of-the-atharvan-angiras/ "6:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A famed archaic tantric manual of the atharvans is glorious कुब्जिका उपनिषद्. Its material is remarkable and worthy of detailed discussion at some point. It was founded by the भार्गवस्, who are supposed to have obtained it directly from धूर्त कुमार in the manner in which पतञ्जल काप्य's daughter became the medium of the ancient भार्गव. That tale goes thus:\
When the students of पतञ्जल काप्य, who had his school in the kingdom of the mAdras, asked him for an ancient विद्या, he said that it was only known to the long gone भार्गव kabandha आथर्वण. The daughter of पतञ्जल काप्य was a shamaness, who in a shamanic fit got into herself the "disembodied spirit" of kabandha आथर्वण in the form of a gandharva. Then the students asked the gandharva to answer their questions and through the daughter of their teacher the ancient भार्गव spoke forth the high विद्या.

So also when पैप्पलाद आङ्गिरस् questioned the mahA-atharvan aurva Agneya, धूर्त skanda entering him revealed to the former foundation of the first manifestation (shiva and उमा; विष्णु and shri) tantra-mantra शास्त्र. From the पैप्पलाद it passed to the वसिष्ठस् of the पराशर branch.

The text reveals the high atharvanic mode installation of life (जीवन्यासं कुर्यात्) into the flour pratimas of shaktis. This mode of installation provides the साधक with a tremendous advantage over any other mode of worship of flour pratimas. The root source of the भावना, and there by the emanation of राजराजेश्वरी उपासन and श्रिविद्या is from the khANDa 14 of this text. The most guarded and efficacious उपासन of धूमावती that can hardly be overcome is mentioned in khANDa 18. In the 22nd is that exalted rite of श्री प्रत्यङ्गिरा-अथर्वण भद्रकाली-रुद्रकुब्जिका देवी that is the highest of all prayogas of the great leonine goddess.


