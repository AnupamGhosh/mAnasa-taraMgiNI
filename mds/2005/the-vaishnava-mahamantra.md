
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The vaiShNava महामन्त्र](https://manasataramgini.wordpress.com/2005/10/16/the-vaishnava-mahamantra/){rel="bookmark"} {#the-vaishnava-महमनतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 16, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/16/the-vaishnava-mahamantra/ "3:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[ॐ ॐ ॐ ह्रीं ह्रीं ह्रीं श्रीं श्रीं श्रीं ०ं अं अं अं हौं हौं हौं हंसः हंसः हंसः ॐ ह्रीं श्रीं नमो विष्णवे //]{style="font-style:italic;color:rgb(255, 153, 0);font-weight:bold;"}

The mantra rahasyas:\
That great mantra encompasses a series of 3 प्रणवस्. The 3 प्रणवस् are the AdhAra that is known as the brahman. This is followed by the 3 तारिकास् and the 3 अनुतारिकास् which are two wives of विष्णु. They are the supremre states with all things existing in these two shaktis of विष्णु. This sequence represents the primary reality which is the absolute puruSha, the शान्त and the नाद that manisfest there after. श्री is an intrinsic part of each stage in the form of time manifesting as निमेष and उन्मेस्ष.

The प्रणवस् are the primary state of existence the absolute puruSha subtle and with the shakti who is as yet motionless. This is base into which the shakti tArA expands giving rise to the universe. The first manifestation from the puruSha due to the shakti contains both the existing substratum (bhavat) and the manifest existence (bhAva) and tArA resides therein. That state which immediately precedes the expansion of the universe is the inert condition characterized by अनुतारा; this is state is called शान्ता.

The प्रणव, the brahman, manifests the aprameya (a) the unfolding or the evolution of the universe and after fully expanding the universe becomes extinct into the vyomesha (M). Thus the triad of aM bijas.

In this universe the origin, stable existence and absorbtion is seen to be sUrya, which continuously provides the activity in the universe, which is the हकार. This combines with the unfolding of the worlds the aprameya (a). The knowers of this see the Ananda (A). This results Ananda results in the udaya of विष्णु (u) in the प्रज्ञ्न (U). All merges again in the vyomesha on becoming extinct (M). Thus the bIja हौं emanates.

The manifest universe is pervaded by the prANa हम्सः. Thus is the great विष्णु flanked by his two wives tArA and अनुतारा, paid obscience. Hence it is said in the ancient words:\
"ह्रीश्-cha te लक्ष्मीश्-cha patnyau /"

His head is dasha hotar (TA 3.7- prajapatir dasha होता) ; his limbs are chatur hotar (TA 3.7); his hair, flesh, bones, marrow and blood are panchahotar (TA 3.7); his chest, genitalia and posterior are the ShaD hotar (TA 3.7). His seven प्राणस् regulated by his brain are the sapta hotar (TA 3.7). His handsome glory are दक्षिणा (TA 3.10) and the शम्भारस् are his skeletal articulations (TA 5.2.11). The devapatnyas (TA 3.9) are his blood vessels, the होतृणं हृदयं (TA 3.11) is his mind; the puruSha is his consciousness (RV 10.90) and the श्री सूक्तं is his shakti (RV Kh 5.87). प्रणव and tArA बीजस् are his secret names, while rudra (TA 4.5.7) and shukra (1.10.1) are is unconcealed names.


