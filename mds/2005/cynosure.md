
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Cynosure](https://manasataramgini.wordpress.com/2005/12/29/cynosure/){rel="bookmark"} {#cynosure .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 29, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/29/cynosure/ "6:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For some reason I used the word cynosure. The muni and I had a brief discussion on this word some time ago. It appears the the word cynosure is from the name tail of Canis Major (cynos-ura). This construct seems to be an archaic Indo-European word because we encounter a similar set of words as the names of the sons of the fallen भार्गव ajigarta. The most famous of them was the भार्गव ऋषि शुनःशेप Ajigarti who later became देवरात वैश्वामित्र. He amongst other mantras composed the celebrated वारवन्तीय. The power of the वारन्वन्तीय is indescribable. The fury of वैश्वानर can only be channelized by the वारवन्तीय.

But we have a problem: cynosure means the focus or center of all attraction. In the heavens this is not the tail of Canis Major but the tail of Ursa Minor. This makes one wonder if Ursa Minor was once called a dog and the name of the Ajigarti was after all dhruva, the nail on the world axis.


