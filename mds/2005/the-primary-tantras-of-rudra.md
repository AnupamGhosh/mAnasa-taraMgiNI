
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The primary tantras of rudra](https://manasataramgini.wordpress.com/2005/03/26/the-primary-tantras-of-rudra/){rel="bookmark"} {#the-primary-tantras-of-rudra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/26/the-primary-tantras-of-rudra/ "6:41 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[उर्ध्वस्रोतस्- सिद्धान्त तन्त्रस्]{style="font-weight:bold;color:rgb(255,0,0);font-style:italic;"}\
[शिवागमस्:]{style="font-weight:bold;color:rgb(153,255,153);"}

 1.  कामिक 2) yogaja 3) chintya 4) कारण 5) ajita 6) दीप्त 7) सूक्ष्म 8) sahasraka 9) अम्शुमान् and 10) suprabheda.\
[रुद्राग्मस्:]{style="font-weight:bold;color:rgb(153,255,153);"}

 11.  vijaya 12) निह्श्वास 13) स्वायम्भुव 14) anala 15) वीरभद्र 16) raurava 17) मकुट 18) vimala 19) चन्द्रग़्ण्य़ान 20) mukhabimba 21) udgita 22) lalita 23) siddha 24) सन्तान 25) नारसिंह 26) पारमेश्वर 27) किरण 28) vAtUla\
[षोमे औxइल्लिअर्य् तेxत्स्:]{style="color:rgb(153,255,153);font-weight:bold;"} 1) मतङ्ग पारमेश्वर 2) मृगेन्द्र 3) पराख्य 4) उपस्वायम्भुव 5) aindram 6) महाघोरं

[पुर्वस्रोतस्- गारुड तन्त्रस्]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

 1.  haram 2) हुङ्कारं 3) बिन्दुसारं 4) कलामृतं 5) देवत्रासं 6) सुत्रासं 7) शबरं 8) कालशबरं 9) पक्षिऱजं 10) शिखायोगं 11) शिखासारं 12) शिखामृतं 13) पञ्चभूतं 14) विभागं 15) शुल्यभेदविनिर्णयं 16) कालष्ठं 17) कालाङ्गं 18) कालकूटं 19) पटद्रुमं 20) कम्भोजं 21) कम्बलं 22) कुंकुमं 23) काळकुण्दं 24) कटाहकं 25) सुवर्णनेखं 26) सुग्रीवं 27) तोतलं 28) तोतलोत्तरं

[पस्छिमस्रोतस्- भूततन्त्रस्]{style="font-weight:bold;color:rgb(255,0,0);font-style:italic;"}

 1.  हालाहलम् 2) हयग्रीवं 3) करकोटं 4) कटङ्ककं 5) करोटं 6) मण्डमानं 7) कङ्कोटं 8) खद्गरावणं 9) चाण्डासिधारं 10) हुङ्कारं 11) हाहाकारं 12) शिवारवं 13) घोराट्टहासं 14) उच्छिष्टं 15) घुर्घुरं 16) दुष्टत्रासकं 17) विमलं 18) विकटं 19) महोत्कटं 20) यमघण्टं

[उत्तरस्रोतस्- वाम तन्त्रस्]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

 1.  nayam 2) नयोत्तरं 3) मूकं 4) मोहनं 5) मोहनांऋतं 6) करपूजाविधानं 7) वीणाशिखा 8) जयं 9) विजयं 10) अजितं 11) अपराजितं 12) सिद्धनित्योदयं 13) ज्येष्ठं 14) चिन्तामणिमहोदयं 15) कुहकं 16) कामदेनुकदम्बकं 17) आनन्दं 18) रुद्रं 19) भद्रं 20) किंकरं 21) अनन्तविजयं 22) भोक्तं 23) दौर्वासं 24) बीजभेद

[दक्षिणस्रोतस्- भैरव तन्त्रस्]{style="font-weight:bold;font-style:italic;color:rgb(255,0,0);"}

 1.  स्वच्छन्दभैरव 2) चण्डभैरव 3) krodhabhairava 4) unmattabhairava 5) असिताङ्गभैरव 6) rurubhairava 7) कपालीशं 8) समुच्चयं 9) घोरं 10) घोषणं 11) ghoram-2 12) निशासञ्चरं 13) दुर्मुखं 14) भीमाङ्गं 15) डामररावं 16) भीमं 17) वेतालमर्दनं 18) उच्छुष्मं 19) वामं 20) कपालं 21) भैरवं 22) पुष्पं 23) अद्वयं 24) त्रिशिरोभैरवं 25) एकपादं 26) सिद्धयोगेश्वरं 27) पञ्चामृतं 28) प्रपञ्चं 29) योगिनीजालशम्बरं 30) विश्वविकण्ठं 31) झङ्कारं 32) तिलकोद्यानभैरव


