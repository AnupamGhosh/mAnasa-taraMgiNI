
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [aghora bhairava prayoga](https://manasataramgini.wordpress.com/2005/12/16/aghora-bhairava-prayoga/){rel="bookmark"} {#aghora-bhairava-prayoga .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 16, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/16/aghora-bhairava-prayoga/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

When the bhairava mantras are appropriately discharged they are like arrows whizzing from the पिनाक. The shatru सेना is as though rushing forward when it is suddenly attacked from the flanks by flaming missile shot by the agents of rudra. Each bhairava mantra is like a shower of fatal arrows raining on them with unstoppable fury.

Meditate upon the Adi-guru, the kShatriya woman, लक्ष्मीञ्करा, who is the founder of the lineage of teachers of the lore of the bhairava in the kali yuga, the great princess, the sister of the noble राजन् इन्द्रभूती. She is of lotus complexion, of splendid beauty, seated in in padmAsana, graced by a crimson tilaka, with her long eyes painted with black collyrium, showing the ग्यान mudra surrounded by students receiving the [bhairava]{.st0} विद्या from her. She has condensed the secret विद्यास् of bhairava of गालव, विश्वामित्र, भार्गव, यमोघण्ट, शूक, shikhin and others.

[मेधोल्काय स्वाहा ॥]{style="font-weight:bold;color:#ff0000;"} (utter) X 4 and worship the awful विनायक tightly embracing his blue-hued shakti मदद्रवा.

Then the 6-fold तर्पणं is offered with the following formulae:

[ष्फां फट् फां फीं ह्रीं श्रीं अघोरं भैरवं तर्पयामि ।]{style="font-weight:bold;color:#33cc00;"}\
[फ्रें फट् फां फीं ह्रीं श्रीं खचक्र-भैरवं तर्पयामि ।]{style="font-weight:bold;color:#33cc00;"}\
[फ्रें फट् फां फीं ह्रीं श्रीं रविभक्षण-भैरवं तर्पयामि ।]{style="font-weight:bold;color:#33cc00;"}\
[फ्रें फट् फां फीं ह्रीं श्रीं चण्ड-भैरवं तर्पयामि ।]{style="font-weight:bold;color:#33cc00;"}\
[फ्रें फट् फां फीं ह्रीं श्रीं नभो-निर्मल भैरवं तर्पयामि ।]{style="font-weight:bold;color:#33cc00;"}\
[फ्रें फट् फां फीं ह्रीं श्रीं डमर-भास्कर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#33cc00;"}

Then water oblations may be made (before the idol or a bimba of aghora bhairava)\
[ॐ नमो भगवते उग्र-भैरवाय सर्व विघ्नान् नाशय नाशय हुं फट् स्वाहा ॥]{style="color:#0000ff;font-weight:bold;"}

[ॐ ह्रीं आं अङ्ग-भैरव [देवदत्तम्] कोपशमनं कुरु कुरु स्वाहा ॥]{style="color:#0000ff;font-weight:bold;"}

[ॐ नमो भगवते महा भीम भैरवाय लोकभयङ्कराय सर्व-शत्रु संहारकाय हुं [देवदत्तम्] ध्वंसय ध्वंसय स्वाहा ॥]{style="color:#0000ff;font-weight:bold;"}

[ॐ वं रं ह्रूं ॐ नमो भगवते विजय-भैरवाय सर्व शत्रु विनाशनाय विबुध- वाहनाय नर-रुधिर माम्स भक्षणाय [देवदत्तम्] उच्छाटय उच्छाटय हुं ताडय ताडय भस्मी कुरु भस्मी कुरु स्वाहा ॥]{style="color:#0000ff;font-weight:bold;"}

[ॐ श्रीं ह्रीं क्लीं ॐ नमो भगवते स्वर्णाकर्शण भैरवाय प्रणताभि परिपूरणाय एह्येहि करुणानिधे मह्यं हिरण्यं दापय दापय श्रीं ह्रीं क्लीं स्वाहा ॥]{style="font-weight:bold;color:#ffcc66;"}

Then the 10-fold तर्पणं is done with the following formulae:

[ॐ ह्रीं श्रीं फट् फां फ्रें सृज-वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें स्थिति-वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें संहार-वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें रक्त-वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें यम वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें मृत्यु वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें भद्र वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें परमार्क वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें मर्ताण्डा वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}\
[ॐ ह्रीं श्रीं फट् फां फ्रें कलाग्निरुद्र वीर भैरवं तर्पयामि ।]{style="font-weight:bold;color:#009900;"}

The activation of the bhairava power with the brahman of sAvitrI:

[ॐ ह्रां ह्रीं हंसः मार्ताण्ड भैरवाय प्रकाश शक्ति सहिताय स्वाहा ।]{style="color:#0000ff;font-weight:bold;"}\
[ॐ तत्-सवितुर् वरेण्यं । भर्गो देवस्य धिमही । धियो यो नः प्रछोदयात् ॥ स्वाहा]{style="color:#0000ff;font-weight:bold;"}\
X 3 अर्घ्यं is offered

[ॐ ह्रां ह्रीं हंसः मार्ताण्ड भैरवं सवितारं तर्पयामि । ]{style="font-weight:bold;color:#ffff00;"}

[ॐ ऐं ह्रीं श्रीं ॐ ह्रीं नमो भगवत्यै महात्रिपुर भैरव्यै मम त्रैपुर रक्षां कुरु कुरु ।]{style="font-weight:bold;color:#3333ff;"}\
(water oblation)

Sprinkle water on head with the following formula:\
[तीक्ष्ण दंष्ट्र महाकाय कल्पान्त दहनोपम मन्थान भैरवाय नमस्तुभ्यं अनुज्ञां दातुमर्हसि]{style="font-weight:bold;color:#ff0000;"}

  - Sit in padmAsana and concentrate for some time on सहस्रार

  - conceive aghora bhairava thus: He is dressed in the skin of tiger and elephant and holds in his hands trishula, पिनाक, chakra, पाशुपत, खट्वाङ्ग, khadga, tomara, भिन्दिपाल. He is surrounded by 7 धूमावतीस्, each with 4 canine teeth, holding skull bowls with red mada and cleavers, crows are perched on their shoulders, they wear clothing of pretas, and are howling hideously.

One may use the following gAyatrI for meditation:\
[ॐ श्वनध्वजाय विध्महे। शूलहस्ताय धीमहि । तन्नो भैरवः प्रचोदयात् ॥]{style="color:#33ff33;font-weight:bold;"}

  - Meditate on the महामन्त्र:\
[ॐ नमो महाभैरवाय विकृत दंष्ट्रोग्र-रूपाय पिन्गलाक्षाय त्रिशूल-खड्ग-धराय वौषट् ।]{style="color:#ff0000;font-weight:bold;"}

  - followed by:\
[ॐ नमो अघोर भैरवाय खड्ग परशु हस्ताय ॐ ह्रूं विघ्न-विनाशाय ॐ ह्रूं फट् ।]{style="color:#ff0000;font-weight:bold;"}

  - followed by:\
[ॐ स्खां स्खीं स्खौं महाभैरवाय नमः ।]{style="font-weight:bold;color:#ff0000;"}

Then utter the following incantations (+water oblation) and conceive the पार्षदस् of दाकिनिस् and चण्डेश्वर rushing forth to devour the शत्रूस्:

[ॐ छः छः छः डाकिनीमत बन्धु नमः ।]{style="color:#0000ff;font-weight:bold;"}\
[ॐ नमो भगवते वज्राय चन्डेश्वराय ईं ईं फट् स्वाहा ।]{style="color:#0000ff;font-weight:bold;"}\
[ॐ स्त्रीं स्त्रीं वलीं वलीं ईं अः फट् स्वाहा ।]{style="color:#0000ff;font-weight:bold;"}\
(water oblation -- prevents मृत्यु sent by the धूमावती deploying attacker and hurls it back)\
[ॐ नमो जले मोहे कुले फलानि संकुले स्वाहा ।]{style="color:#0000ff;font-weight:bold;"}\
[ॐ नमो जले मोहे द्रां अब्जिनि फट् स्वाहा ।]{style="color:#0000ff;font-weight:bold;"}\
[ॐ भूर्-भुवस्-स्वः स्वाहा ।]{style="color:#0000ff;font-weight:bold;"}\
[[ॐ नमो जले मोहे हन हन दह दह पच पच मथ मथ मे वशंआनय स्वाहा ।]{style="color:#0000ff;"}\
]{style="font-weight:bold;"}\
Then kindle fire with:

[रं सं सीदस्व महान् असि शोचस्व देव वीतमः ।]{style="font-weight:bold;color:#33ff33;"}\
[वि धूमं अग्ने अरुषं मियेध्य सृज प्रशस्त दर्शतं वषट् ॥]{style="font-weight:bold;color:#33ff33;"}

Make fire oblations 10 or 108 times with ghee, white tila or gavedhuka:

[ह्रूं ह्रीं क्लीं अघोर भैरवाय [देवदत्तम्] मोहय स्वाहा ॥]{style="font-weight:bold;color:#ff0000;"}

[स्फें स्फें प्र्á वः पáन्तं रघुमन्यव्ó ऽन्धो यज्ńáं रुद्रáय मीळ्ह्úषे भरध्वं। स्फ्रीं स्फ्रीं दिव्ó अस्तोष्य् áसुरस्य वीरíर् इषुध्य्éव मर्úतो र्óदस्योः । प्रीं हुं फट् स्वाहा ॥]{style="font-weight:bold;color:#ff0000;"}

[ॐ वज्रकाय वज्रतुण्ड कपिल पिङ्गल ऊर्ध्व-केश महा-बल रक्त-मुख तडिज्-जिह्व महारौद्र दंष्ट्रोत्कटक ह ह करालिने महादृढ प्रहारिन् लन्केश्वर-वधाय महासेतुबन्ध महाशैल प्रवाह गगनेचर एह्येहि भगवन्-महाबल पराक्रम]{style="font-weight:bold;color:#ff0000;"}[भैरव]{.st0 style="font-weight:bold;color:#ff0000;"}[आज्ञापय एह्येहि महारौद्र दीर्घ पुच्छेन वेष्टय [वैरिणाम्] भञ्जय भञ्जय हुम् फट् ॥]{style="font-weight:bold;color:#ff0000;"}

Do आचमनं and arise.

Of the second mantra the ऋश्यादि is thus stated: parama-gotama-kula-योनी ऋषिः; त्रिष्टुप् छन्दः; rudra-गणाः देवताः |\
The 6-fold न्यास may be done with:prá वः paántaM; raghumanyavó .andho yajńáM; rudraáya मीळ्ह्úSए भरध्वं; divó अस्तोSय् ásurasya; वीरíर् इSउध्य्éव; marúto ródasyoH


