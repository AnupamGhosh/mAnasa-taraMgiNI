
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Forms of the प्रत्यङ्गिरा ऋक्](https://manasataramgini.wordpress.com/2005/03/05/forms-of-the-pratyangira-rik/){rel="bookmark"} {#forms-of-the-परतयङगर-ऋक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 5, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/05/forms-of-the-pratyangira-rik/ "6:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[]{style="font-size:12pt;font-family:\""}[याम् कल्पयन्ति नो अरयः क्रूराम् कृत्याम् वधूम् इव /]{style="color:rgb(255, 102, 102);"}\
[ताम् ब्रह्मणा परि निज्मः प्रत्यक् कर्तारम् ऋcचतु //]{style="color:rgb(255, 102, 102);"}\
[\
This version is seen in the ऋग्वेद khila 4.5.1. It may be used for countering कृत्यास् via the procedure recommended by the विधान of shaunaka.]{style="font-size:12pt;font-family:\""}

[यां कल्पयन्ति नो ऽरयः क्रुरां कृत्यां वधुमिव /]{style="color:rgb(255, 204, 204);"}\
[तां ब्रह्मणा ऽपनिर्नुद्मः प्रत्यक् कर्तारमृच्छतु //]{style="color:rgb(255, 204, 204);"}

This version is seen in the पैप्पलाद saMhitA of the atharvaveda and is used in the कृत्या destroying atharvanic rites and the tantric प्रत्यङ्गिरा paddhatis.

[यां कल्पयन्ति नो ऽरयः क्रुरां कृत्यां वधुमिव /]{style="color:rgb(255, 102, 102);"}\
[ब्रह्मणा ऽपणिर्णुद्मः प्रत्यक् कर्तारमृच्छतु //]{style="color:rgb(255, 102, 102);"}

This version is from the largely lost jalada saMhitA of the atharvaveda and is deployed only in certain tantric paddhatis of प्रत्य्ङ्गिरा.

It is used in practice with the insertion of बीजस् thus:\
[ॐ हॄं यां कल्पयन्ति नो ऽरयः क्रुरां कृत्यां वधुमिव /]{style="color:rgb(255, 102, 102);"}\
[ह्रां ब्रह्मणा ऽपणिर्णुद्मः प्रत्यक् कर्तारमृच्छतु हॄं ॐ //]{style="color:rgb(255, 102, 102);"}

[यां कल्पयन्ति वहतौ वधुमिव विश्वरूपां हस्तकृतां चिकित्स्वः /]{style="color:rgb(255, 102, 102);"}\
[सारादेत्वप नुदां एनां //]{style="color:rgb(255, 102, 102);"}

This is the version in the shaunaka saMhitA of the atharvaveda and is in the rites of the vaitana sutra.

The atharvans may also deploy another mighty ऋक् from the same सूक्तं to slay the sender of spell with two oblations with its recitation.

[भवाशर्वा वस्यतां पापकृते कृत्याक्ऱि^ते / दुष्कृते विद्युतं देवहेतिं //]{style="color:rgb(255, 102, 102);"}\
(shaunaka form)\
Typically the paippalada form is used:\
[भवाशर्वा वस्यतां पापकृत्नवे कृत्याक्ऱि^ते / दुष्कृते विद्युतं देवहेतिं //]{style="color:rgb(255, 102, 102);"}

The oblations: भवाशर्वाभ्यां स्वाहा /namo रुद्राय pashupatye स्वाहा


