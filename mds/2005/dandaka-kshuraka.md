
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [दण्डक क्षूरक](https://manasataramgini.wordpress.com/2005/11/09/dandaka-kshuraka/){rel="bookmark"} {#दणडक-कषरक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 9, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/09/dandaka-kshuraka/ "5:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The दण्डक क्षुरक attack was made. That led to further trouble in the troubled area. The ओषधि-vanaspati was ravaged- that brought the bank balance down to zero. The dagda ratha caused havoc. An year after the event there were only ruins. As we were passing by them we noticed the passage of fate. The यक्षिणि passed by but did not descend. Instead she merely hovered around and said the following: 1) The death of the foolish donkey with the leopard's skin. 2) The bashing of the blue jackal that howled on a new moon night.

Then she narrated this story: When indra slew vala with a blow from his vajra he fell from the skies. The great serpent वासुकि pulled out his gall bladder and was flying through the sky as though splitting the welkin in two. He appeared like a bridge of silver across the sky illumined by the gem in his head. Then garuDa attacked the sarpa as if he may destroy heaven and earth. The mighty serpent to escape dropped the gall bladder of the demon on the ridge of the माणिक्य mountain on which तुरुष्क trees which secrete copious resin grow. garuDa caught some of the daitya's bile from the but became unconscious due to it and emitted it through his nostrils. वायु pulled out the nails of vala and threw them on clusters of lotuses. agni threw the blood of vala in the low lying marshes of the narmada. The sarpa recovering grabbed the intestines of the demon vala and deposited them in the chera country.


