
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The quest for faint objects](https://manasataramgini.wordpress.com/2005/10/02/the-quest-for-faint-objects/){rel="bookmark"} {#the-quest-for-faint-objects .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 2, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/02/the-quest-for-faint-objects/ "3:51 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the great expedition of last night the following unusually faint objects were captured: A galaxy and a planetary nebula.

NGC242 in Cetus. The Nebula occurs just below a group of stars termed the Phi1 and Phi2 Ceti forming a near equilateral triangle with them. The object is a very faint shroud that appears to envelop a group of stars around int in nebulosity. Discovered by Wilhelm Herschel.\
[http://www.ne.jp/asahi/stellar/scenes/object_e/ic342.htm](http://www.ne.jp/asahi/stellar/scenes/object_e/ic342.htm){target="_blank"}

IC342 in Cameleopardus is a low brightness galaxy of the Mafei group. Though it is very close to the Milky Way (about 8.2*10^6 light years), it is not at all easy to sight. A group of faint stars from the Milky way that surround it help one to distinguish the nebulosity.\
[http://www.ne.jp/asahi/stellar/scenes/object_e/ngc246.htm](http://www.ne.jp/asahi/stellar/scenes/object_e/ngc246.htm){target="_blank"}


