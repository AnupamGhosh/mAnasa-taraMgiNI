
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Human skull diversity](https://manasataramgini.wordpress.com/2005/10/27/human-skull-diversity/){rel="bookmark"} {#human-skull-diversity .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/27/human-skull-diversity/ "5:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/pbp.gif){width="75%"}
```{=latex}
\end{center}
```

{target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/skull_composite.jpg){width="75%"}
```{=latex}
\end{center}
```



European Sudanese

Geneticist Armand Leroi recently wrote an interesting article pointing how genetic determinants of cranial diversity in humans may be a very interesting topic for study.

