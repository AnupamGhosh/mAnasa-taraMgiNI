
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The heart of वाराही](https://manasataramgini.wordpress.com/2005/05/02/the-heart-of-varahi/){rel="bookmark"} {#the-heart-of-वरह .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 2, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/05/02/the-heart-of-varahi/ "6:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

सामन्तदेव had entered the shrine of the 64 योगिनीस्, the योगिनी at vajrapura in कलिञ्ग. He stood in the center worshiping नित्यक्लिन्ना. He saw an apparition of the great goddess who instructed him to seek दण्डनाथा. Go north she said. सामन्तदेव crossed the पीठ of shAradA and moved beyond to the heart of ओड्डियान. There in the middle of the most enchanting ओड्डियान, where the siddhi of yoga manisfests itself, the very thought of which causes one to proceed of the flight of the योगिनी, सामन्तदेव went on to create the kirichakra मण्डाल with colored sand. At heart of the chakra, in the center of the triangle, was the bindu, which lay at the mid-point of the spanda and emanated the glauM bIja. This bindu is where the great सूकरानना, the beloved enchantress of उरुगाय, with curly locks and of deep blue complexion resides. Here सामन्तदेव worshiped the great दण्डनायिका with the rahas-तर्पणस् of wine. At the vertices of the triangle stand जृंभिनी, मोहिनी and स्तम्भिनी who are of the complexion of the fresh pomogranate flower and have little sharp tusks. They hold the musala, hala and नागपाश weapons and a pitcher of beer, and their elegant arms are decorated by ruby bangles. The ठं bija emanates from them.

Beyond triangle in the pentad ring are invoked andhini, rundhini, जृंभिनी, मोहिनी and स्तम्भिनी. They are of flaming orange-yellow color and with boar-heads and tusks. They hold their bows and swords ready to strike. To them सामन्तदेव offered the तर्पणं of blood. Beyond the pentad ring lies the hexad ring were माहेश्वरी, वैष्णवी, कौमारी, brAhmI, इन्द्राणी and the awful चामुण्डा are invoked. Beyond this lies the heptAd ring were the धतुनाथा goddess serve वाराही. They are याकिनी, राकिनि, लाकिनी, वाकिनी, शकिनी, डाकिनी and हाकिनी. सामन्तदेव invoked them in each of the chakras and the dhatus of the body and then merging together to form हाकिनी filling his body with light. In half of the heptad ring the deities स्तंभिनी and कोर्धिनी, who hold terrible weapons in their hands. They were offered blood तर्पणस्. Then the weapons of वाराही, the hala and the musala are offered their oblations. Then the awful गण of वाराही, चण्डोचण्ड, who has four arms and three eyes, holding a त्रिशूल, a प्रेतपाश (the ghost noose), a sword is worshiped in front of the the मण्डल. Then the octad ring is set up and the deities, वार्ताली, वाराही, वाराहमुखी, andhini, rundhini, जृंभिनी, स्तम्भिनी and मोहिनी are invoked. सामन्तदेव again propitiated them with the रहस्तर्पण of wine.

Then the buffalo of पोत्रिणि is worshipped with a milk libation. Then the देवता chakra is worshipped starting with indra, then वरुण, mitra, विष्णु and the other Adityas, rudra and the rudras, ashvins, वायू, prajApatI, and बृहस्पती. Then the पिशाचस्, यक्षस् with वैक्श्रवण at their head and the रक्षस् with नरवाहन at their head are invoked. Then जृंभिनी, मोहिनी and स्तम्भिनी are again invoked in the outer triangle and then क्षेत्रापाल is invoked with a vajra, नागपाश and Damaru in his hands and with 3 heads emitting meteoric flashes.

Then the lion of वाराही is worshiped. The the 1000 शक्तीस् of the 1000 petalled lotus of the kirichakra are worshipped. They are all dark-blue in color with, boar-heads and sharp tusks and holding diverse weapons in their arms. They also hold the skull-cups filled with blood of their foes. They are invoked using the rahasya सहस्रनामं. Thereafter the antelope of दण्डनायिका is worshiped. Then the beer pitcher of पोत्रिणि is worshiped with libations of beer.

In the outer most circle of the kirichakra मण्डल stand the eight bhairavas with tridents : hetuka,त्रिपुरारि, agni, yamajihva, एकपाद, काल, कराल, भीमरूप, हाटकेश and\
achala are invoked and made offerings.

Then सामन्तदेव saw an apparition of वार्तालि flying on the aerial tamolipta car and shooting the andhaka missile at the demons.


