
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A Japanese गाणापत्य text](https://manasataramgini.wordpress.com/2005/04/30/a-japanese-ganapatya-text/){rel="bookmark"} {#a-japanese-गणपतय-text .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 30, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/30/a-japanese-ganapatya-text/ "5:03 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the unusual tantra inspired texts from Japan that shows the origin of the Japanese गाणपत्य cult from the उच्छिष्ट gaNapati stream is the kangiten kOshiki, which was composed by the Japanese sage Kakuban, who lived between 1095-1143. gaNapati is called kangiten in the Japanese pantheon. The great Japanese hero Sugawara Michizane was an ardent worshiper of gaNapati and is said to have attained his "siddhis" through the invocation of the Japanese equivalent of उच्छिष्ट gaNapati. An annual grand abhishekam to kangi with thousand oil pourings was performed by the imperial palace of Japan.

From Kakuban's description of gaNapati (kangiten kOshiki=the ritual of deva gaNapati):

  - The merits of the Lord kangiten reach higher than the heavens, the profit he gives is broader than the earth is wide. He defends dharma in every direction.

  - His root nature is that of the lord of universal and wondrous enlightenment, rank that is concealed as if hidden in a spotless moon.

  - The manifest form of kangi is that of a male and female deva.

  - By performing the vow (from vrata in संस्कृत ) of kangi, which benefits all creatures, wealth, wisdom, courage and love can be obtained. With it one can subjugate demons, dispel disease and extend life.

  - Lord kangiten is the root of yin and yang. From him the 10,000 objects of the universe emerge. He is the Lord who has founded the teaching lineage of the garbha and vajra मण्डल from which all buddhas are born.

  - In the Mitsugon heaven he appears as vairochana

  - The male deva is an emanation of the lord maheshvara. He drives away celestial and earthly demons and distributes profit in this world and next.

  - The female देवी is an emanation of the 11-headed form of avalokiteshvara, and it is the most potent of her 33 forms.

  - The two are standing in conjugal embrace representing the union of yin and yang.

  - That they have elephant heads and human bodies is to show the co-penetration of all ten realms.

The accessible material of this text suggests a rather simple process by which the उच्छिष्ट gaNapati from the महाचिनाचार tantras was first "colored" by Taoist material in the land of the चीनस् where it first took root. From China it was then transmitted to Japan and presented in canonical form by Kakuban.

The clear evidence for this incorporation into Taoist-Hindu fusion texts that were inspired by the Sanskrit tantras in the chIna country is provided by an obscure text of the चीनाचार्य Po-jo-je Chieh-lo. This text dates to AD 861 and is found in both China as: Sheng Huan-hsi T'ien shih-fa and in Japan as: shO kangiten shikihO (meaning the rituals of गणेश). This text describes the construction of the गणेश मण्डल with गणेश and his shakti in the center and idols of indra, agni, yama and kubera placed around the dual गणेश idols and then other devatas in the आवर्णस् around this center square. Many special dhyAna-s are given for attainment of wealth, वशिकरण of women, and मारण of enemies. The text also depicts two amulets that contain twelve squares are are common in Taoism. This text provides evidence for how the Hindu material penetrate the ritual setting in the far East by way of central Asia.


