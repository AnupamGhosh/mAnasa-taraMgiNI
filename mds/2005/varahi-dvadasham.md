
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [वाराही द्वादशं](https://manasataramgini.wordpress.com/2005/04/27/varahi-dvadasham/){rel="bookmark"} {#वरह-दवदश .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/27/varahi-dvadasham/ "5:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[वारही द्वादशं इति मन्त्रस्य श्री अश्वाननः ऋषिः । अनुष्टुप् छन्दः । वाराही देवता । श्री वारहि देवी प्रसाद सिद्धि द्वार सर्व सङ्कट हरणं जपे विनियोगः ॥]{style="font-style:italic;color:rgb(255,0,0);font-weight:bold;"}

[पञ्चमी दन्डनाथा cha सङ्केत समयेश्वरी /]{style="font-weight:bold;color:rgb(51,204,0);"}\
[तथा समय-सङ्केता वाराही पोत्रिणी शिवा //]{style="font-weight:bold;color:rgb(51,204,0);"}\
[वार्ताली च महासेना ऽप्याज्ञाचक्रेश्वरी तथ /]{style="font-weight:bold;color:rgb(51,204,0);"}\
[अरिघ्नी चेति सम्प्रोक्तं नाम द्वादशकं मुने //]{style="font-weight:bold;color:rgb(51,204,0);"}\
[नाम-द्वादशधाभिख्य वज्र-पञ्जर-मध्यगः /]{style="font-weight:bold;color:rgb(51,204,0);"}\
[सङ्कटे दुःखम्-आप्नोति न कदाचन मानवः //]{style="font-weight:bold;color:rgb(51,204,0);"}


