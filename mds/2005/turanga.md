
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [तुरङ्गा](https://manasataramgini.wordpress.com/2005/04/25/turanga/){rel="bookmark"} {#तरङग .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 25, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/25/turanga/ "6:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

स्वस्त्री had gone to check the original hall for its temperature and reported that as she came out she saw तुरङ्गा. My उग्रकामी tried to ask तुरङ्गा what she was doing there but she ran away. स्वस्त्री tried to chase her but even the पुमान् have never beaten तुरङ्गा. The muni knew this as me and the muni had one watched a long gone दुष्ट chase तुरङ्गा and failed. I was intensely agitated by the display of तुरङ्गा, but the young muni who had transcended the रसार्णव was unmoved. But स्वस्त्री was unusually alarmed of तुरङ्गा's presence and changed the venue. I started thinking of तुरङ्गा and saw the two shakunas. I was puzzled as when I noticed them first but I still do not understand them. But we still march ahead- for they all these shakunas are only for traders and not true warriors.


