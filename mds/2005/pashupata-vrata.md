
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पाशुपत vrata](https://manasataramgini.wordpress.com/2005/06/10/pashupata-vrata/){rel="bookmark"} {#पशपत-vrata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 10, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/10/pashupata-vrata/ "11:59 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having had a major struggle with the forces of fate, and having been reduced to the condition of Jx in a certain sense, and having dropped the proverbial last wick of भैरवानन्द he was wandering around with the wheel spinning on his head. [They wanted that most tasty fruit that grew atop the नृ trees. But unfortunately for them they did not know how to nurture such trees. Hence they spoiled their sapling quite badly and there was no great success in getting the fruit. There was that talk between the urvan of the tree and वरुण much like talk between the urvan of the cow and Ahura Mazda. Hopefully वरुण answers the tree's call.]{style="font-style:italic;color:rgb(255,0,0);"} In the mean time as he wandered at the brink of the desert and the खाण्डव jungle. At this point he wondered what if anything may save him. He was asked to perform the पाशुपत vrata. If pashupati wishes he may be saved the itinerant पाशुपत told. Thus, he lapsed into the पाशुपत yoga state.


