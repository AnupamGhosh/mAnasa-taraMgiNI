
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Our nAga शाप](https://manasataramgini.wordpress.com/2005/06/29/our-naga-shapa/){rel="bookmark"} {#our-naga-शप .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 29, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/06/29/our-naga-shapa/ "4:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is said that when पाण्डुस् conquered खाण्डव from the नागस् they got the curse of the नागस्. For the sake of खाण्डव they lost most of their sons. In the next generation परीक्षित् lost his life due to the nAga and so on.

We had earlier alluded to the seizure of our खाण्डव in what seemed a successful campaign. A little did we know that we were headed to worst of the existential crises we had ever faced as result of our possession of खाण्डव. It looked as though our favors from the gods had run out now. The attack now extended on all fronts. Jx had said that cell phones were harmless devices, but we learned to our horror there were some which even came with bombs, leave alone cameras. Jx received a strike. The big eater received another but survived after some bad effects. The learned muni himself was struck but some how survived. Some of our dependents were struck badly and one of our great treasures were robbed. pretas issued forth from the floor. When Marc and R had visited Hayastanika they obtained the latter's message for me. The [Abrus ]{style="font-style:italic;"}seeds had fallen out of the window and now a deadly grove of plants grew there. We were never so bereft of energy and were at our wits end. We had then spoken of कुम्भकर्ण and the missiles failing at him. It was now as though we were in a कारागृह of रावण.


