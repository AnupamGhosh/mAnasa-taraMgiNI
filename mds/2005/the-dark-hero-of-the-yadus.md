
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The dark hero of the yadus](https://manasataramgini.wordpress.com/2005/08/27/the-dark-hero-of-the-yadus/){rel="bookmark"} {#the-dark-hero-of-the-yadus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 27, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/27/the-dark-hero-of-the-yadus/ "6:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The birth anniversary of the dark hero of the yadus dawned. We decided to\
recall his awful deeds in course of his momentous life that overlapped with the tumult taking place in the bhArata realms.

  - The yadu was born to vasudeva and देवकी, who were imprisoned by कंस, देवकी's brother.The tyrant कंस had been told that देवकी's 8th son will kill him one day.

  - देवकी's eighth son was born on a रोहिणी अष्त्टमि, and was spirited away by vasudeva to his cowherd friend nanda's abode. Instead he brought back nanda's daughter to replace his son.

  - कृष्ण देवकीपुत्र's possible escape still worried कंस so he sent a demoness पुतना to slay देवकीपुत्र. But कृष्ण slew पूतन instead.

  - कृष्ण then still a child slew effortlessly demons such as त्रिणवर्त, अघासुर and dhenuka. -कृष्ण then subdued the nAga chief kaliya on the banks of the yamuna.

  - कृष्ण's brother balabhadra, son of रोहिणि and vasudeva slew the demon pralamba.

  - देवकीपुत्र then killed the agents of कंस like shankachuda, ariSTa, keshi and vyoma.

  - then कृष्ण and balarAma proceeded to the court of कंस, at अक्रूर's invitation.

  - then the brother advanced to the capital of कंस, mathura, and were pitted against the wrestlers चाणूर and मुष्टिक, whom they promptly slew.

  - then कृष्न killed कंस by suddenly attacking him and restored ugrasena on the throne In course of his varied career the mighty bowman vAsudeva killed a number of powerful adversaries of the yadus. He successively destroyed, after killing कंस, कालयवन, naraka the king of प्रग्ज्योतिष and his general mura, paundraka the false vAsudeva, जरासन्ध via पाण्डव bhima, शिशुपाल during the राजसूय of युद्धिष्ठिर, shalva the great invader, dantavakra and vidhuratha.


