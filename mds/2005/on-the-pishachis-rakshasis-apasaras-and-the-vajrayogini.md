
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On the पिशाचिस्, राक्षसिस्, अपसरास् and the वज्रयोगिनी](https://manasataramgini.wordpress.com/2005/04/19/on-the-pishachis-rakshasis-apasaras-and-the-vajrayogini/){rel="bookmark"} {#on-the-पशचस-रकषसस-अपसरस-and-the-वजरयगन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/04/19/on-the-pishachis-rakshasis-apasaras-and-the-vajrayogini/ "6:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had in our long wanderings encountered many a पिशाचि, राक्षसि and अप्सरा. What we were fated to have we knew not. We were locked in the embrace of the the वज्रयोगिनी. We sunk into the ultimate intoxication of her आपीनवक्शाब्यः and gazed at her उरूभ्यः with our third eye which was open in midst of the कृष्ण रात्री, even as महाकाल danced the ताण्डव on our शिखा. She split herself into three forms and provided us pleasure in the midst of the कालरात्री and the yuddhachakra. We now hold on to her before the uncovering of the पिशाचि's face hoping that she might possess the nagara-पिशाचि herself.

oM vetalajananyai नमः


