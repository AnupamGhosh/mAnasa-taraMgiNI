
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The श्रीधर krama](https://manasataramgini.wordpress.com/2005/10/15/the-shridhara_krama/){rel="bookmark"} {#the-शरधर-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 15, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/15/the-shridhara_krama/ "4:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The fragment of the totala tantra preserved by tradition has an important rite the श्रीधर karma to विष्णु that is thus performed (we provide a edited version from a reasonably well made transcription):

  - The उपासक shall have a bath, then perform संध्या and then enter the sacrificial room.

  - He shall then wash his hands and feet and perform Achamana. He shall place his right palm on the left palm in the संकल्प posture and utter the मूल mantra:

[[oM श्रीं ह्रीं श्रीधराय विष्णवे नमः]{style="font-weight:bold;"}]{style="font-size:130%;"}

This mantra dispels ill-luck and ill-health and sins and brings enjoyment and salvation.

Then the न्यास should be done thus:

[ॐ हां अङ्गुष्ठाभ्यां नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}\
[ॐ हीं तर्जनीभ्यां नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}\
[ॐ हूं मध्यमाभ्यां नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}\
[ॐ हैं अनामिकाभ्यां नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}\
[ॐ हौं कनिष्ठिकाभ्यां नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}\
[ॐ हः करतलकरपृष्ठाभ्यां नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}

[ॐ हां हृदयाय नमः / ॐ हीं शिरसे स्वाहा/ ॐ हूं शिखायै वषट् / ॐ हैं कवचाय हुं / ॐ हौं नेत्रत्रयाय वौषट् / ॐ हः अस्त्राय फट् /]{style="font-weight:bold;font-style:italic;color:#33cc00;"}

  - Then the Atman mudra will be shown by the उपासक.

  - Then having performed a purificatory प्राणायांअ he shall meditate on विष्णु within himself in the cavity of the heart with the following ध्यानं:

[ध्यायेत् परं विष्णु हृत्-कोटर-समाश्रितम् /]{style="font-weight:bold;font-style:italic;color:#ffff33;"}\
[शङ्ख-चक्र-समायुक्तं कुन्देन्दु-धवलं हरिं //]{style="font-weight:bold;font-style:italic;color:#ffff33;"}\
[श्रीवत्स-कौस्तुभ-युतं वनमाला-समन्वितं /]{style="font-weight:bold;font-style:italic;color:#ffff33;"}\
[रत्न-हार-किरीटेन संयुक्तं परमेश्वरं //]{style="font-weight:bold;font-style:italic;color:#ffff33;"}

  - Then he shall meditate upon an egg surrounding him and harden its shell with the formulae:

yaM विष्णवे नमः / क्षं विष्णवे नमः / raM विष्णवे नमः //

  - Then he shall pervade the egg from within with the mantra:

oM

  - Then he shall offer flowers to विष्णु within himself

  - Then he shall invoke the deities of the Asana of विष्णु and worship them with flowers thus:

वैष्णवासन-देवता आगच्छत

[ॐ समस्तपरिवारायाच्युताय नमः / ॐ धात्रे नमः /ॐ विधात्रे नमः / ॐ गङ्गायै नमः / ॐ यमुनायै नमः / ॐ शङ्ख-निधये नमः / ॐ पद्मनिधये नमः / ॐ चण्डाय नमः / ॐ प्रचण्डाय नमः / ॐ द्वारश्रियै नमः / ॐ आधारशक्त्यै नमः / ॐ कूर्ंआय नमः / ॐ अनन्ताय नमः / ॐ श्रियै नमः / ॐ धर्माय नमः / ॐ ज्ञानाय नमः / ॐ वैराग्याय नमः / ॐ ऐश्वर्याय नमः / ॐ अधर्माय नमः / ॐ अज्ञानाय नमः / ॐ अवैराग्याय नमः / ॐ अनैश्वर्याय नमः / ॐ सं सत्त्वाय नमः / ॐ रं रजसे नमः / ॐ तं तमसे नमः / ॐ कं स्कन्दाय नमः / ॐ नं नीलाय नमः / ॐ लं पद्माय नमः / ॐ अं अर्कमण्डलाय नमः / ॐ सों सोममण्डलाय नमः / ॐ वं वह्निमण्डलाय नमः / ॐ विमलायै नमः / ॐ उत्कर्षिण्यै नमः / ॐ ज्ञानायै नमः / ॐ क्रियायै नमः / ॐ योगायै नमः / ॐ प्रह्व्यै नमः / ॐ सत्यायै नमः / ॐ ईशानायै नमः / ॐ अनुग्रहायै नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}

  - Then he shall repeat the न्यास and the Atman mudra and invoke विष्णु into the svastika मण्डल

  - Then he shall offer arghya, स्नान, vastra, आचमनं, गन्धपुष्पं, धूपं, दीपं and नैवेद्यं

  - Then he will do japa with the मूल mantra about 10 or 100 times.

  - Then he shall perform अङ्ग, Ayudha and परिवार उपासन with the following mantras:

अङ्ग पूज:\
[ॐ हां हृदयाय नमः / ॐ हीं शिरसे नमः /ॐ हूं शिखायै नमः /]{style="font-weight:bold;font-style:italic;color:#ff0000;"}[[ॐ हैं कवछाय नमः /ॐ हौं नेत्रत्रयाय नमः/ ॐ हः अस्त्राय नमः]{style="color:#ff0000;"} ]{style="font-weight:bold;font-style:italic;"}

लक्ष्मी पूज:\
[ॐ श्रियै नमः /]{style="font-style:italic;font-weight:bold;color:#ffff33;"}

Ayudha पूज:\
[ॐ शङ्काय नमः / ॐ पद्माय नमः / ॐ चक्राय नमः / ॐ गदायै नमः / ॐ श्रीवत्साय नमः / ॐ कौस्तुभाय नमः / ॐ वनमालायै नमः / ॐ पीताम्बराय नमः / ॐ खड्गाय नमः / ॐ मुसलाय नमः / ॐ पाशाय नमः / ॐ अङ्कुशाय नमः / शार्ङ्गाय नमः / ॐ शराय नमः /]{style="font-weight:bold;font-style:italic;color:#ff0000;"}

परिवार पूज :\
[ॐ ब्रह्मणे नमः / ॐ नारादाय नमः / ॐ पूर्वसिद्धेभ्यो नमः / ॐ भागवतेभ्यो नमः / ॐ गुरुभ्यो नमः / ॐ परमगुरुभ्यो नमः /]{style="font-weight:bold;font-style:italic;color:#ff0000;"}

दिग्देवता पूज:

[ॐ इन्द्राय सुराधिपतये सवाहन-परिवाराय नमः / ॐ अग्नये तेजो ऽधिपतये सवाहन-परिवाराय नमः / ॐ यमाय प्रेताधिपतये सवाहन-परिवाराय नमः / ॐ निरृतये रक्षो ऽधिपतये सवाहन-परिवाराय नमः / ॐ वरुणाय जलाधिपतये सवाहन-परिवाराय नमः / ॐ वायवे प्राणाधिपतये सवाहन-परिवाराय नमः / ॐ सोमाय नक्षत्राधिपतये सवाहन-परिवाराय नमः / ॐ ईशानाय विद्याधिपतये सवाहन-परिवाराय नमः / ॐ अनन्ताय नागाधिपतये सवाहन-परिवाराय नमः / ॐ ब्रह्मणे लोकाधिपतये सवाहन-परिवाराय नमः /]{style="font-weight:bold;font-style:italic;color:#ff0000;"}

दिग्देवता Ayudha पूज:\
[ॐ वज्राय हुं फट् नमः / ॐ शक्त्यै हुं फट् नमः / ॐ दण्डाय हुं फट् नमः / ॐ खड्गाय हुं फट् नमः / ॐ पाशाय हुं फट् नमः / ॐ ध्वजाय हुं फट् नमः / ॐ गदायै हुं फट् नमः / ॐ त्रिशूलाय हुं फट् नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}[ॐ छक्राय हुं फट् नमः / ॐ पद्माय हुं फट् नमः /]{style="font-weight:bold;font-style:italic;color:#33ff33;"}

विषवक्सेन पूज:\
oM वौं विष्वक्सेनाय नमः/

  - Then the साधक shall worship विष्णु with the following stotra:

विष्णवे देवदेवाय namo vai प्रभविष्णवे/\
विष्णवे वासुदेवाय नमः स्थितिकराय cha /\
ग्रसिष्णवे namashchaiva नमः प्रलयशायिने /\
देवानां prabhave chaiva यज्ञानां prabhave नमः/\
मुनीनां prabhave नित्यं यक्षाणां प्रभविष्णवे/\
जिष्णवे सर्वदेवानां सर्वगाय महात्मने/\
brahmendra-rudra-वन्द्याय सर्वेशाय नमोनमः/\
sarva-loka-हितार्थाय लोकाध्यक्षाय vai नमः /\
sarva-goptre sarva-kartre sarva-दुष्ट-विनाशिने /\
वरप्रदाय शान्ताय वरेण्याय नमोनमः /\
शरण्याय सुरूपाय dharma-कामार्थदायिने//

  - Then he shall silent engage in japa of the मूल mantra


