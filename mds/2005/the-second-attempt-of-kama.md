
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The second attempt of काम](https://manasataramgini.wordpress.com/2005/09/11/the-second-attempt-of-kama/){rel="bookmark"} {#the-second-attempt-of-कम .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 11, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/09/11/the-second-attempt-of-kama/ "4:31 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The contest between shiva and काम is much celebrated in Hindu literature. The effects of काम on shiva have been enshrined in the highest poetry of संस्कृत by कालिदास in poem on the birth of the war-god. But the ब्रह्माण्ड पुराण is unique in providing a version of the tale, where काम has a second attempt to go at shiva and in this attempt conquers him. It is one of those eternal pieces of decorative prose in Hindu literature. The tale goes thus:

After इशानी had through her side glance revived manmatha. She then said by my boon your arrows will torment the living world. ईशान succumbing to your darts will be unable to burn you now and will suffer the pangs of काम. bhava will even give half of his body to his wife to that he can remain perpetually conjoined to her...

Having received the boon काम set forth to complete his task. From his hair pores emerged thousands of madanas who were of splendid appearance and could enchant the universe. After deluding the world with his arrows काम again reached the Ashrama of the aweful rudra to conquer the moon-crested deity. He was accompanied by vasanta, chandra, sweet breezes of spring. The sounds of kokilas followed him, as he embraced by rati, lifted his victorious bow and showered volleys of arrows on rudra. Struck by these arrows, powered by कामेश्वरी's assurance shiva as overcome.

rudra abandoned all regular observances and became sick with longing, frequently meditating on गौरी. His cheeks lost color and he heaved deep sighs of frustration. He was shedding tears constantly and recollected again and again उमा haimavati. The fever raging in his frame was not cooled by the गङ्ग flowing from his head nor the cool rays of the चन्द्रकला. The trident-bearer was thus tormented by the arrows of manmatha. He rolled frequently in the flowery bed brought by his circle of गणस् headed by nandin, भृन्गिन् and कालभैरव. Supported by nandin he went restlessly from one flowery bed to another, rolling restlessly on them. The soma exuding from the कला did not cool his frame. Desirous of suppressing he flames in him he applied snow on himself and sat on slabs of ice. On the slabs of ice he drew the pictures of उमा with his nails. Looking at the pictures further enhanced his pangs. He drew her picture standing coquettishly and casting side glances on a slab of ice. He placed the slab all over his body and experienced horripilations and distressing sexual emotions. He imagined her to be around everywhere and conversed with them as though she was in front of him in a mad delirium. Considering her his ultimate goal he spents days and nights narrating her nectar-like anecdotes. He started making garlands of flowers and wreathing her as though she was around. Thus did the great pashupati suffer from the grip of काम.\
.......\
The piece is not merely a narrative but the representation of the shAkta view of the longing and ultimate merger with the shaktI.


