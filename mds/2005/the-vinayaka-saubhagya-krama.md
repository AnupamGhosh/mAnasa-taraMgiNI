
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The विनायक सौभाग्य krama](https://manasataramgini.wordpress.com/2005/10/21/the-vinayaka-saubhagya-krama/){rel="bookmark"} {#the-वनयक-सभगय-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 21, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/10/21/the-vinayaka-saubhagya-krama/ "4:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

vighnas may be crushed with the vidhi of the terrible विनायक. It is thus performed.

  - The votary makes a haridra बिंब of and invokes विनायक into it with the formula:\
oM आगच्छोल्काय अवाहनं करिष्यामि/

  - Then sandalwood paste or scent is offered with:\
oM गन्धोल्काय गन्धं समर्पयामि /

  - Then flowers are offered with:\
oM पुष्पोल्काय पुष्पं समर्पयामि /

  - Then incense sticks are waved with:\
oM धूपोल्काय धूपं आग्रापयामि /

  - The lamp is shown with:\
oM दीपोल्काय दीपं दर्शयामि /

  - - bali of a gourd or a pumpkin is offered with:\
oM महोल्काय बलिं समर्पयामि /\
The pumpkin is cleaved and the inner surfaces smeared with कुंकुम.

Then करन्यास is done:\
oM महाकर्णाय अङ्गुष्ठाभ्यां नमः / oM vidmahe तर्जनीभ्यां नमः / oM वक्रतुण्डाय मध्यमाभ्यां नमः /oM धीमहि अनामिकाभ्यां नमः / oM tanno दन्तिः कनिष्ठिकाभ्यां नमः / oM प्रचोदयात् करतलकरपृष्ठाभ्यां नमः /

Then अङ्ग न्यास is done:\
oM glauM glAM हृदयाय नमः / oM gAM gIM gUM shirase स्वाहा/ oM ह्रूं ह्रीं ह्रीं शिखायै वषट् / oM gUM कवचाय huM / oM गों गैं नेत्रत्रयाय वौषट् / oM गों अस्त्राय फट् /

Then the gAyatrI is recited 4 times:\
oM महाकर्णाय vidmahe : वक्रतुण्डाय धीमहि : tanno दन्तिः प्रचोदयात्

Then the fire is lit and at least 8 fire oblations of white or red sesame seeds are made with the formula

oM गः स्वाहा //

Then the गण oblation is made with the formula:\
गणाय गणपतये स्वाहा कूष्माण्डकाय cha अमोघोल्कायैकदन्ताय त्रिपुरान्तकरूपिणे नमः /\
oM श्यामश्-cha danta-विकरालास्याजिपतये vai नमः padma-दंष्ट्राय स्वाहा //

4 such offerings may be done.

Then the गणेश mudra will be shown.

Then the devotees performs अट्टहास by dancing, laughing and clapping his hands.

immerse the haridra bimba with:\
oM सिद्धोल्काय विसर्जनं करिष्यामि /


