
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On the vedic anusvAra.](https://manasataramgini.wordpress.com/2005/12/26/on-the-vedic-anusvara/){rel="bookmark"} {#on-the-vedic-anusvara. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 26, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/12/26/on-the-vedic-anusvara/ "3:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The shaunaka, मान्डुक्य, पाणिनि and other authorities have declared that the anusvAra 'M' in its original state is a voiced sound involving only the नासिक and no other oral articulation (e.g. [अनुस्वार यमानां छ नासिका स्थानं उछ्यते ।]{style="font-style:italic;color:#0000ff;"}). This differentiates it from the other अनुनासिकस् such as 'ma', 'na' Na, 'ञ', 'ङ' which are sounds with specific oral articulations that are tinged with passage of air through the nasal cavity. In the pure anusvAra the mouth is kept naturally closed without forming any particular articulation and the air is allowed to pass, superficially contacting the oral cavity into the nasal cavity. However, it does not pass into the oral cavity to resonate within it. In English this approximated by the sound value of 'n' in [saint]{style="font-weight:bold;"}. In practical vedic recitation there are certain complexities in the actual sound values taken.

Typically the terminal अनुनासिकस् such 'm' in rudra(m) 'n' in ud va(n)danam are consonants and pronounced as such. In the below discussion the अनुस्वारस् are bracketed by {}. The ऋग्वेद (शाकल्य) and atharvaveda (shaunaka and पैप्पलाद) the pure अनुस्वारस् are encountered in two contexts:

 1.  The first is denoted by the chandra-bindu notation in Devanagari scripts. E.g. "mahA{n} indro ya ओजसा". The {n} is uttered as a pure anusvAra described above and cause the vowel preceding it to extend an extra matra.


 2.  The M coming before the consonants 'ghn' or 'GY'. e.g.1 "नकिष्ट{M} ghnanty antito na दुराद्". e.g. 2 sa{M}ज्ञानेन

These अनुस्वारस् are technically termed the शुद्धानुस्वारस्.

The gAnaM-s of the सामवेद may render these as musical reduplicated nasal sounds.

In the yajurveda the system is more complex with the presence of multiple अनुस्वारस् including the GM sound:

 1.  The simple शुद्धानुस्वार occurs in the yajurveda only when a 'ghn' or 'GY' occurs after it. E.g: "ima{M} ghnanti" or "sa{M}ज्ञानं". It is pronounced as a purely nasal sound as described above.

 2.  If a terminal अनुनासिक occurs before a 'ra', 'sha', 'Sha', 'sa' and 'ha' OR if it is an anusvAra indicated in the ऋक्-s by a chandrabindu occuring before a naked vowel, then it transforms into a GM.\
E.g. (Clause 1) vivaya{M} ruhema=\>vivaya{GM} ruhema; दिवं दृ{M}ha=> दिवं दृ{GM}ha; प्रजया sa{M}रराणो=> प्रजया sa{GM}रराणो\
E.g. (Clause 2) mahA{n} indro वज्रबाहुः=> mahA{GM} indro वज्रबहुः\
These are technically known as आगमानुस्वारस्.


 3.  If the terminal अनुनासिक is preceded by a long vowel and it occurs before a consonant sh, Sh, s, h, then instead of a GM it becomes a mere G with out the 'M' sound.\
e.g. tapU{M} Shy agne=> tapU{G} Shy agne; ज्योती{M}Shi अर्चः =(sandhi rule)=> ज्योती{M}Shy अर्चः =(anusvAra rule)=> ज्योती{G}Shy अर्चः\
This is known as the lupta आगमानुस्वार.


 4.  If the terminal अनुनासिक is preceded by a short vowel (a, i, u) and it occurs before a consonant sh, Sh, s, h, then instead of a GM it becomes a re-duplicated GG with out the 'M' sound.\
e.g. pu{M}schali=> pu{GG}shchali; tva{M} hi agne =(sandhi rule)=> tva{M} hyagne =( anusvAra rule)=> tva{GG} hyagne; इन्द्रं svasti => इन्द्रग़्ग़् svasti.\
This is known as the द्विर्भूत lupta आगमानुस्वार. The above three GM, G and GG अनुस्वारस् occur in the specified fashion in the तैत्तिरीय, मैत्रायण, kaTha, charaka and माध्यंदिन saMhitA-s of the yajurveda. The काण्व saMhitA converts all आगंआनुस्वारस् to G or GG.


 5.  There are special अनुस्वारस् indicated by the number '3'. In this case the long vowel preceeding them are drawn out to musically to 3 मात्रस्. These are on rare occasions also encountered in the ऋग्वेद (e.g. i n the आरण्यानि सूक्तं and नासदीय सूक्तं) and the atharva veda. In the yajur veda one of the famous occurrences of this anusvAra in the brahmAn formula to mitra-वरुण and indra (1.8.16).\
e.g. (from the brahmAn chant): सुश्लोका३{n} सुमङ्गला३{n} सत्यराजा३{n}\
Note the special '3' anusvAra symbol superceding all other rules. This is the समायत anusvAra.


