
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The sleep of धूमावती](https://manasataramgini.wordpress.com/2005/08/20/the-sleep-of-dhumavati/){rel="bookmark"} {#the-sleep-of-धमवत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 20, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/08/20/the-sleep-of-dhumavati/ "10:11 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We have heard of the नैरृत spell of धूमा which overpowers you with tremendous force. Even as we were scanning the horizon for the निरृति attack, the spell found its mark on us with the उcछुष्म-rudra wings. The torpor it induced was unbearable, we twice fell asleep in the very place in which we were. The spell also has a peculiar effect on the mind- it causes a mental vibration with lingering unpleasant and delusive images from which one cannot break free. It also induces a grave fear of the foe and all manner of bodily aches as it seizes you. Shortly after we awoke from the spell the खाण्ड्वन्स् attacked directly. We simply decided to brush them aside knowing that their power came from the spell.


