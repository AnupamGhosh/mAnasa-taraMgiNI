
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some great men from other cultures](https://manasataramgini.wordpress.com/2005/03/24/some-great-men-from-other-cultures/){rel="bookmark"} {#some-great-men-from-other-cultures .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 24, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/24/some-great-men-from-other-cultures/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some great men transcend cultural boundaries and simply amaze you with the sheer enormity of their acts. In my opinion these first among these is the great Mongol leader Chingiz Kha'Khan. If anyone comes close to complete success then it is none other than the great Kha'Khan. Literally from rags after his father was killed by the Tatars he created a super-power nation, defeating in a two front war, Islam and China. Even the modern superpower with its entire panoply would not survive a two front war on both these fronts. His surge through Central Asia was studded with an unbroken string of victories in all kinds of circumstances, that no military leader after him has ever been able repeat. Not only that he set up an entire nation with administration, writing, currency and a postal system, where none existed before. Most importantly, he became the most prolific progenitor of Central Asia. And all this was achieved after being tied down in Mongolia in tribal conflicts till the age of 40.

Chingiz Khan- was a miracle to his people, and world still looks at him in wonder if there could ever be a man like that. It is a small wonder that he was deified as seen in these mideaval Mongol chants recovered by Walther Heissig:\
Heaven-born Chingiz Khan,\
Born from the decision of the sublime Tengri lord of heaven,\
Your body provided with heavenly rank and name,\
you who took over lordship over the world's people.\
Fortunately born sovereign,\
whose origin is from the fortunate gods,\
Great in good fortunte and majesty,\
You who have wisdom without instruction,\
you who rule without error.

Another some what younger chant shows the clear Indian influence (the Chingiz Khan dhAraNi and आवाहनं):\
Rest here on the place, to come to which fulfils wishes,\
On the throne made of incomparable precious things,\
On the carpet with eight glorious petals of the lotus,\
Most harmonious protective यक्ष, white उपासक, Chingiz Khan.

You with great power as the Kha'Khan of the dharma chakra,\
may conquer the disbelieving wild enemy\
strengthen long life, joy, peace and strength,\
secure for me that my actions fulfil my wishes,


