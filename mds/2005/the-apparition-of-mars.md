
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The apparition of Mars](https://manasataramgini.wordpress.com/2005/11/02/the-apparition-of-mars/){rel="bookmark"} {#the-apparition-of-mars .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 2, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/02/the-apparition-of-mars/ "5:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We have been observing Mars since October 30th. We have had the most spectacular views of the planet ever in our whole observing career. At magnitude below -2.1 it stole the show in the whole observing session. We also got a good view of sanaishchara with its elegant rings.

