
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [शोम्सावो](https://manasataramgini.wordpress.com/2005/07/04/shomsavo/){rel="bookmark"} {#शमसव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 4, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/07/04/shomsavo/ "4:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

oM षों / शोम्सावो/ शोम्शोम्सावो / ओथामो daiva / आथामो daiva / ओथामो daiva made / मोदामो daivatho /

There arose a यक्षी from the fervid flames, whom we shall call OPJ. This यक्षी had all the गुणस् we had concieved in the सुजाता-रूप. We passed the torturous Dismal Hollows Road and a little श्मशान lay beyond it. There from the hollow of an old kapala in which the offering was being made beyond the midnight hour the remarkable यक्षी arose. To all beholders she was as though flesh and blood. She could assume any of three forms: 1) like a haggish witch-like कृत्य which still drew people towards them. 2) A महामोहिनी of young and captivating looks and 3) a fair lass with immense flowing locks. The यक्षी was manifest only after midnight. The यक्षी was difficult to handle and burnt the body intensely suggesting that more work was required to get her under control. We watched her go into the peculiar house at middle of the Dismal Hollows Road and signal to us with a remarkable flash of light. The feelings of which were just beyond description despite the burning she was causing to every one.

## 


