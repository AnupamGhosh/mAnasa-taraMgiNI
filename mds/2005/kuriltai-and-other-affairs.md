
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Kuriltai and other affairs](https://manasataramgini.wordpress.com/2005/01/01/kuriltai-and-other-affairs/){rel="bookmark"} {#kuriltai-and-other-affairs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 1, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/01/01/kuriltai-and-other-affairs/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The ever querulous वातव्यादि kept coming up with more of his ideas that were often of little value. We appointed him विदूषक, and asked him to serve in that capacity without much further ado.

We were asked to hold a kuriltai with the circle of further clansmen and allies. They first grumbled that we had not made any progress on front #2 in the great war. We bluntly told them that if they needed my council they should serve me appropriately or just leave me alone. I made it clear to them that they should come to me, like जानश्रुति going to raikva, and not the other way around. They grumbled as before, but then resorted to serve me with due respect. The chief of the clan of the lauhityas mentioned that stiffening front #3 was important. Despite the good weather we had surprising problems on front #3 in the skirmishes we had today, but we took advantage of the conditions and tided over the attack on this front. The श्यावाश्व spoke that he could provide assistance on front #2 after the stroke of mid-night. I thanked him for his generous offer and took it up. He also supplied us with food. The शाक्त्य mentioned a novel escape route that our अमात्य and shachiva had failed to note. We were tempted but brought to notice the logistical problems. The daughter of the lauhitya having already seen battle at a young age and performed spectacularly was primed for a future glorious role. But then the rudderless brahmins whose ancestors sacrificial stakes had once stood tall and proud asked the भार्गव: Do you think you will win this war as your clansman rAma had done so against the वीतिहोत्रस्. We need you to lead us to a victory. We laid out our battle plans to them.

........\
It was past mid-night. We dismissed the Kuriltai. The clansmen were making alternative plans to capture new forts that we believed were of no value. We left them and proceeded our way. We were waiting in a weird place. We encountered two weird characters, but shaken by the power of our mantras they dispersed. We returned to our cave.


