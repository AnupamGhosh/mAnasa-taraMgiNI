
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [राक्षसि](https://manasataramgini.wordpress.com/2005/11/19/rakshasi/){rel="bookmark"} {#रकषस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 19, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/11/19/rakshasi/ "6:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Long after the cell phone had exploded violently we lay unable to make sense of what happened in the daze of the blast. We then saw that figure moving like धुमावति. It was laughing loudly and had all manifestations of अलक्ष्मि. We longed for the lengthy conversations with the muni. We marched through beaten path of the evil तुरुष्क sultan with the low sun beating down upon. Having reached an appropriate bend we made our offerings of savitar and वरुण.

o3 मदा mo3m पिबा mo3m devo वरुणः prajapati सविता 3 annaM iha 2 harad annapate 2 हरा हरो३ं //

We were walking down the स्वापन path in late 1998 when the राक्षसि seized us. Now we were taking the second path and saw another राक्षसि waiting there.


