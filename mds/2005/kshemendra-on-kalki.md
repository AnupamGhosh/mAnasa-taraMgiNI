
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [क्षेमेन्द्र on kalki](https://manasataramgini.wordpress.com/2005/03/03/kshemendra-on-kalki/){rel="bookmark"} {#कषमनदर-on-kalki .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 3, 2005]{.entry-date}](https://manasataramgini.wordpress.com/2005/03/03/kshemendra-on-kalki/ "6:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great Kashmirian author in his text the दशावतारचरित्र which was written in 1066 AD describes the conditions that would prevail on the earth just before the coming of kalki.\
"The Turushkas, Pathans, Daradas and dwellers of Shakastan will cause the earth to decay as do the leper his open oozing sores. Every direction overrun by the dreadful म्लेच्छस्, the earth will resound with the clash of swords drawn in combat and her soil will be soaked with blood."

The मन्थानभैरव tantra's colophon has a statement that in the रक्ष रावण incarnated to the west of the sindhu with the ripening of the kali yuga . His presence shall initiate the barbarous tyranny of the म्लेच्छस् who will spread havoc and bloodshed throughout the world.

Thus, the कालचक्र tantra and the Hindu depictions are quite parallel and definitely inspired by the dreaded advent of Islam \[a problem that continues to seize the world].


