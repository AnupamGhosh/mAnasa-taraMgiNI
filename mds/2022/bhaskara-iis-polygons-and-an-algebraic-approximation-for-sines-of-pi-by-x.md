
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [भास्कर-II's polygons and an algebraic approximation for sines of pi by x](https://manasataramgini.wordpress.com/2022/12/28/bhaskara-iis-polygons-and-an-algebraic-approximation-for-sines-of-pi-by-x/){rel="bookmark"} {#भसकर-iis-polygons-and-an-algebraic-approximation-for-sines-of-pi-by-x .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 28, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/12/28/bhaskara-iis-polygons-and-an-algebraic-approximation-for-sines-of-pi-by-x/ "7:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Unlike the Greeks, the Hindus were not particularly obsessed with constructions involving just a compass and a straightedge. Nevertheless, their pre-modern architecture and yantra-s from the तान्त्रिक tradition indicate that they routinely constructed various regular polygons inscribed in circles. Of course, the common ones, namely the equilateral triangle, square, hexagon, and octagon are trivial, and the earliest preserved geometry of the Hindus is sufficient to construct these. The pentagon and its double the decagon are a bit more involved but are still constructible by the Greek compass and straightedge method; however, few have looked into how the Hindus might have constructed it. These aside, we do have multiple examples of yantra-s with heptagons and nonagons. A particularly striking example is the wide use of the nonagon in yantra-s (likely related to the early श्रीकुल tradition of 9 योगिनी-s and the division of the व्योमव्यापिन् mantra of the सैद्धान्तिक-s) found at the मरुन्दीश्वर temple near Chennai. The kaula tradition of the कुब्जिका-mata-tantra has a सूत्र that mentions a yantra with multiple regular polygons relating to pacifying the seizure by षष्ठी and others ([त्रिकोणं नवकोणं च षट्कोणं मण्डलाकृतिः ।]{style="color: #0000ff"}). The heptagon and the nonagon cannot be constructed using just a compass and a straightedge. To construct them precisely, one would require a means of accurately drawing conics (other than circles and straight lines) of particular specifications. While Archimedes invented a machine to draw ellipses, and examples of ellipses are occasionally encountered in early Hindu architecture, the technology for the easy generation of desired conics was unlikely to have been widely available to premodern architects. Hence, the Hindus should have constructed their regular polygons, including heptagons and nonagons through other means.

![Polygon_Sine](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/polygon_sine.png){width="75%"}Figure 1.

It is easy to see (Figure 1) that for a circle of diameter  $d$ 

In लीलावती 206-209, भास्कर gives a table for the sides of the inscribed polygons in three अनुष्तुभ्-s (see below) followed by a numerical example (L 209). We have resolved the संधि with a + for ease of reading the numbers:

[त्रि-द्व्यङ्काग्नि-नभश्-छन्द्रैस् त्रिबाणाष्टयुगाष्टभिः ।]{style="color: #0000ff"}\
[वेदाग्नि-बाण-खाश्वैश् च ख-खाभ्राभ्र-रसैः क्रमात् ॥]{style="color: #0000ff"} L 206

tri+dvi +अङ्क +agni +नभश् +candrais (103923)\
tri +बाण +अष्ट +yuga +अष्टभिः (84853) |\
veda +agni +बाण +kha +अश्वैश् (70534)\
ca kha +kha +abhra +abhra +रसैः (60000) क्रमात् ||

[बाणेषु-नख-बाणैश् च द्वि-द्वि-नन्देषु सागरैः ।]{style="color: #0000ff"}\
[कु-राम-दश-वेदैश् च वृत्त-व्यासे समाहते ॥]{style="color: #0000ff"} L 207

बाण +इषु +nakha +बाणैश् (52055) ca\
dvi +dvi +nanda +इषु +सागरैः (45922)\
ku +राम +दश +वेदैश् ca (41031)

[ख-ख-खाभ्रार्क संभक्ते लभ्यन्ते क्रमशो भुजा ।]{style="color: #0000ff"}\
[वृत्तान्तर् त्र्यस्र-पूर्वाणां नवास्रान्तम् पृथक् पृथक् ॥]{style="color: #0000ff"} L 208

वृत्त-व्यासे समाहते kha +kha +kha +abhra +arka (120000) संभक्ते labhyante क्रमशो भुजा\
वृत्त-antar tri +asra-पूर्वाणाम् nava +asra-antam पृथक् पृथक्

Essentially, the above means that one should multiply the diameter of the circle with the numbers specified in the above table in verse form and divide them by 120000. This gives, in order, the sides of the inscribed regular polygons from a triangle to a nonagon. Thus, the ratios of these numbers provide rational approximations for  $\sin\left(\tfrac{\pi}{n}\right)$  for  $n=3..9$ . We compare these to the actual values in the below table:

![sine_Table1](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/sine_table1.png){width="75%"}

These rational approximations provided by भास्कर are best for a triangle, square, pentagon, hexagon and octagon. These are the angles for which he derives closed forms in his Jyotpatti (On the generation of sines) and correspond to the constructible polygons of the Greek tradition. The sines of the heptagonal and nonagonal angles which have no closed forms were obtained using serial interpolations or a sine-approximating function.

In terms of the latter, भास्कर specifies a formula for the length of a cord corresponding to an arc in a वसन्तलिका verse that can be used to obtain an algebraic function approximating  $\sin\left(\tfrac{\pi}{x}\right)$ 

चाप +ऊन-nighna-परिधिः prathama +आह्वयः\
स्यात् पञ्च +आहतस् paridhi-varga-caturtha-भागः |\
आद्य +ऊनितेन khalu tena bhajet +catur +ghna-\
व्यास +आहतम् prathamam आप्तम् iha ज्यका स्यात् ||

The circumference is reduced by the arc and multiplied by the arc: this is called the prathama.\
One-fourth of the circumference squared is multiplied by 5\
This is then reduced by the प्रथमा. The प्रथमा multiplied by 4\
and the diameter should be divided by the above result. The fraction thus obtained is the chord.

Let the diameter of the circle be  $d$ , its circumference  $c$ , the length of the given arc  $a$  and  $y$  its chord. Then the above can be written in modern notation as:

 $$y= \dfrac{4da(c-a)}{\frac{5c^2}{4}-a(c-a)} = \dfrac{16da(c-a)}{5c^2-4a(c-a)}$$ 

Now, the arc can be written as the  $x^{\mathrm{th}}$  fraction of the circumference,  $\therefore a=\tfrac{c}{x}$ . By plugging this into the above equation, we get:

 $$y= \dfrac{16d\tfrac{c}{x}(c-\tfrac{c}{x})}{5c^2-4\tfrac{c}{x}(c-\tfrac{c}{x})}$$ 

This allows us to eliminate  $c$  and write

 $$y=\dfrac{16d\left(x-1\right)}{5x^{2}-4\left(x-1\right)}$$ 

Thus, we get an algebraic function approximating  $y=\sin\left(\tfrac{\pi}{x}\right)$ :

 $$y=\dfrac{16\left(x-1\right)}{5x^{2}-4\left(x-1\right)}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/bhaskara_sinpibyx.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 2.

In the below table we show the values of the polygon sines for  $n=3..9$ 

![sine_Table2](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/sine_table2.png){width="75%"}

We can see that the values from this function are more approximate than those provided by the table. Thus, it is clear that भास्कर did not use this algebraic function to generate his table. However, the fact that he provides this formula after the table indicates that he meant this as an alternative method to get rational approximations for the polygonal sines. Such a method too could have been readily used by artists/artisans in their polygonal constructions in architecture and yantra preparation.

