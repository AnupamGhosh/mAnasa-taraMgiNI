
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A catalog of attractors, repellors, cycles, and other oscillations of some common functional iterates](https://manasataramgini.wordpress.com/2022/10/03/a-catalog-of-attractors-repellors-cycles-and-other-oscillations-of-some-common-functional-iterates/){rel="bookmark"} {#a-catalog-of-attractors-repellors-cycles-and-other-oscillations-of-some-common-functional-iterates .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 3, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/10/03/a-catalog-of-attractors-repellors-cycles-and-other-oscillations-of-some-common-functional-iterates/ "4:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the reasons we became interested in functional iterates was from seeking an analogy for the effect of selective pressure on the mean values of a measurable biological trait in a population. Let us consider a biological trait under selection to have a mean value of  $x_n$  at a given point in time in a population. Under the selective pressure acting on it, in the next generation, it will become  $x_{n+1}$ . Thus, the selective pressure can be conceived as a function that brings about the transformation  $x_{n+1} = f(x_n)$ . Thus, iterating this function with its prior value with give us the trajectory of the measure of the said trait in the population. While it might be difficult to establish the exact function  $f$  for a real-life biological trait under selection, we can imagine it as being any common function for a simplistic analogical model. This led us to the geometric representation of the process --- the cobweb diagram.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp_fig1_cobweb1.png){width="75%"}
```{=latex}
\end{center}
```

=\tfrac{1+x}{2+x^2}$ *

For example, let us take the function acting on  $x_n$  to be  $f(x)=\tfrac{1+x}{2+x^2}$  (Figure 1). We can see that the iterative application of this function on any starting  $x_0$  (point B in Figure 1) eventually leads to convergence to a fixed point that can be determined by obtaining the intersection between  $f(x)$  and the line  $y=x$ . In this case, one can prove that it will be 0.6823278..., the only real root of the equation  $x^3+x-1=0$ . Thus, 0.6823278... can be described as the attracting fixed point or attractor of this functional iteration.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp_fig2_cobweb2.png){width="75%"}
```{=latex}
\end{center}
```



*Figure 2. Cobweb diagram for the functional iterates of  $f(x)=\tfrac{1}{x}-x$ *

Instead, consider the same process under the function  $f(x)=\tfrac{1}{x}-x$  (Figure 2). Here, we can show that there would be two points that emerge as a result of the intersection between  $f(x)$  and  $y=x$ : the two roots of the equation  $2x^2-1=0$ ,  $\pm \tfrac{1}{\sqrt{2}}$ . These two points draw the iterates towards themselves but the competition between them results in the outcome being chaos unless  $x_0$  is exactly at one of them. Thus, these two fixed points can be described as repelling fixed points or repellors. Thus, exploring different simple functions, we realized that there can be three possible broad outcomes for functional iterates: (1) Convergence to an attractor; (2) Convergence to a cyclic attractor, where the endpoint is to cycle between 2 or more fixed points; (3) Chaotic oscillations driven by repellors. Hence, we conjectured that even in the evolutionary process under selection we will see these three outcomes. Convergence to an attractor is commonly observed when populations starting with different mean values of the trait are driven by selection to a similar endpoint. The cycle is less common but might be seen in situations like the coexistence of different morphs of males and females, each with a distinct mating strategy, e.g., in beetles, damselflies and lizards. Finally, the absence of convergence but chaotic wandering of the trait is less-appreciated but we believe is also manifested in nature. We shall see below that there are different forms of chaos and each of them might have rather different consequences.

One can find some of the fixed points or other consequences of functional iteration in certain mathematical volumes or online resources. However, we did not find any of those to be comprehensive enough for easy reference. Hence, we thought it would be useful to provide such a catalog covering a subset of the common functions we have explored. We provide these by stating the function and the consequence of the iteration (attractors, cycles or chaos with associated repellors), followed by comments in some cases. We omit trivial cases like  $\sin(x)$ , which shows a gradual convergence to 0. The gradual convergence in cases like this is related to their limit as  $x\to 0$ ; e.g.,  $\lim_{x \to 0} \tfrac{\sin(x)}{x} =1$ . In the below catalog,  $\phi$  denotes the Golden Ratio and  $\phi'$  its reciprocal.

\(1\) Simple algebraic functions. Here the attractors or repellors can be easily determined by solving the polynomial equations defined by the difference equation specifying the map.

 $$\sqrt{1+|x|}$ :  $\phi$$ 

 $1+\dfrac{1}{x}$ :  $\phi$ ; This attractor also extends to the complex plane. For more discussion of this system see [our earlier note](https://manasataramgini.wordpress.com/2017/06/17/cobwebs-on-the-golden-hyperbola-and-parabola/).

 $2+\dfrac{1}{x}$ :  $1+\sqrt{2}$ ; This attractor also extends to the complex plane.

 $$1+\dfrac{1}{2x}$ :  $\dfrac{1+\sqrt{3}}{2}$$ 

 $$2+\dfrac{1}{2x}$ :  $1+\sqrt{\frac{3}{2}}$$ 

 $$\dfrac{1+x}{2+x}$ :  $\phi'$$ 

*[![FP3_algebraic_map](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png){.size-full .wp-image-13942 .aligncenter attachment-id="13942" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="FP3_algebraic_map" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png" orig-size="3300,2400" permalink="https://manasataramgini.wordpress.com/2022/10/03/a-catalog-of-attractors-repellors-cycles-and-other-oscillations-of-some-common-functional-iterates/fp3_algebraic_map/" sizes="(max-width: 3300px) 100vw, 3300px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png 3300w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp3_algebraic_map.png)Figure 3. Chaotic functional iterates of some simple algebraic functions*

 $$\dfrac{1}{x}-x$$ 

 $x-\dfrac{1}{x}$ : sawtooth chaos:  $\phi, \phi'$  are repellors.

The two above systems (Figure 3, first two panels) show chaotic behavior with a peculiar pattern. In the first one, there are rapid oscillations giving an overall symmetric appearance. In the second one, there is a sharp rise to the local peak or valley followed by a slower, convex return towards 0. The profiles of these maps have a tooth-like appearance, though the first is constituted by oscillations fitting into a similar profile as the second.

 $2x^2-1$  (Chebyshev 2): chaotic (-1,1)

 $4x^3 -3x$  (Chebyshev 3): chaotic (-1,1)

These next two functions are the Chebyshev polynomials 2 and 3, which show chaotic behavior if  $x_0$  lies in the interval (-1,1). At -1,1 they remain stationary and beyond those they diverge. Despite the chaos, the values of the iterates show a characteristic U-shaped distribution, with the highest density close to the boundaries, -1, 1, and low densities throughout the middle of the interval (Figure 4). This type of distribution is typical of many chaotic iterates of polynomial functions, e.g., the famous logistic map.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp4_cheb3_hist.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 4. Distribution of the functional iterates of  $4x^3 -3x$ *

\(2\) Circular trigonometric functions
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp5_cos_complex.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 5. Number of iterations to convergence or divergence to  $\infty$ *

 $\cos(x)$ : 0.73908513321516 (the solution of the equation  $x=\cos(x)$ ) is the attractor for all real values. On the complex plane, other than those values in the white region (Figure 5), all values within a fractal boundary converge at different rates (indicated by coloring) to the same attractor.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp6_tan_map.png){width="75%"}
```{=latex}
\end{center}
```



*Figure 6. Iterates of  $\tan(x)$ *

 $\tan(x)$ : chaotic (Figure 6). The oscillations are generally of low amplitude but are punctuated by rare "explosions" of huge amplitude (hence, shown in  $\mathrm{arcsinh}$  scale in the figure). See our [earlier note](https://manasataramgini.wordpress.com/2019/08/27/chaos-eruptions-and-root-convergence-in-one-dimensional-maps-based-on-metallic-sequence-generating-functions/) on functions with comparable behavior. Such behavior is analogous to what have been termed Levy flights.

 $\sin(2x)$ : 0.94774713351699

![FP7_cos2x_hist](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp7_cos2x_hist.png){width="75%"}*Figure 7. Distribution of the functional iterates of  $\cos(2x)$ *

 $\cos(2x)$ : chaotic. The iterates are contained in the interval  $(-1,1)$  with certain exclusion zones. The most prominent exclusion zone contains the primary repellor 0.514933264661... (solution of the equation  $x=\cos(2x)$ ; red point in Figure 7).In the negative part of real line, the exclusion begins at  $\cos(2)$  (purple point in Figure 7). The points of the other exclusions zones (black points) are more mysterious.

 $\tan(2x)$ : chaotic

 $\sin(x)-\cos(x)$ : -1.25872817749268

 $\sin(x)+\cos(x)$ : 1.2587281774927

 $\cos(x)-\sin(x)$ : bicycle: -0.83019851706782, 1.41279458572762; These attractors are also valid in the complex plane.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp8_secx.png){width="75%"}
```{=latex}
\end{center}
```

\
*Figure 8. Functional iterates of 160801 starting points of  $\sec(x)$ *

 $\sec(x)$ : chaotic for both real and complex values. Interestingly, in the complex plane, the iterates show certain preferred regions of density that are symmetric about the real axis (Figure 8). The centers of these regions of density appear to be close to the multiple of  $\pi$  Figure 8; red points).

 $\cot(x)$ : While it is chaotic on the real line, on the complex plane it converges to either  $\pm 1.1996786402577i$  depending on the initial point.

 $\csc(x)$ : 1.1141571408719
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp9_cossquared.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 9. Regions of convergence or divergence to  $\infty$ *

 $\cos^2(x)$ : 0.6417143708 is the attractor for real starting points. In the complex plane all initial points withing the fractal boundary converge to the same attractor while the rest diverge (Figure 9).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp10_cscsquaredx.png){width="75%"}
```{=latex}
\end{center}
```$ *

 $\csc^2(x)$ : 1.17479617129 is the attractor for real starting points. In the complex plane all initial points withing the fractal boundary converge to the same attractor while the rest diverge (Figure 10).

 $\sec^2(x)$ : chaotic

*[![FP11_xbytanx](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png){.size-full .wp-image-13950 .aligncenter attachment-id="13950" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="FP11_xbytanx" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png" orig-size="2400,2400" permalink="https://manasataramgini.wordpress.com/2022/10/03/a-catalog-of-attractors-repellors-cycles-and-other-oscillations-of-some-common-functional-iterates/fp11_xbytanx/" loading="lazy" sizes="(max-width: 2400px) 100vw, 2400px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png 2400w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp11_xbytanx.png)Figure 11. Number of iterations of function  $\tfrac{x}{\tan(x)}$ *

 $\dfrac{x}{\tan(x)}$ : The attractor on the real line is  $\dfrac{\pi}{4}$ . On the complex plane, the points within a fractal boundary (Figure 11) converge to the same point at different rates (the contours in Figure 11).

 $\sin(\cos(x))$ : 0.69481969073079

 $\tan(\sin(x))$ : 1.5570858155247

 $\sin(\tan(x))$ : -0.99990601241267

 $\sin(\sec(x))$ : 0.97678326638014

 $\cos(\sec(x))$ : 0.44604767999913 (root of the equation  $\cos(x)= \tfrac{1}{\arccos(x)}$ ) it the attractor both on the real line and the complex plane.

 $$\sin(\csc(x))$ :  $\pm 0.94403906661161$  is the attractor both of the real line and the complex plane depending on the starting point defined by (root of the equation  $\sin(x)= \tfrac{1}{\arcsin(x)}$$ 

 $\tan(\cos(x))$ : bicycle: 0.013710961966803, 1.55708579436399; These values are remarkably close to but not identical to the solution of the equation  $\arccos(x)= \tan(\cos(x))$ , i.e.,  $r=0.01371006057$  and  $\arccos(r)=\tan(\cos(r))=1.55708583668$ . Thus, the sum of these two values is close to  $\tfrac{pi}{2}$ .

 $\cos(\tan(x))$ : bicycle: 0.013710102886935, 0.999906006233481; These values are remarkably close to but not identical to the solution of the equation  $\arccos(x)= \cos(\tan(x))$ , i.e.,  $r=0.999906018592$  and  $\arccos(r)=\cos(\tan(r))=0.01371006057$ .

 $$\cos(\csc(x))$ : octocycle: 0.366798375086067, -0.938273127439933, 0.324922488718667, -0.999958528842272, 0.373119965761099, -0.921730305866654, 0.310327826505175, -0.991153343837468; this cycle appears to be associated with oscillations close to  $r=\arcsin(\tfrac{1}{\pi})=\mathrm{arccsc}(\pi)=0.323946106932$  and  $\cos(\csc(r))=-1$$ 

\(3\) Hyperbolic trigonometric functions\
 $\coth(x)$ : converges to either  $\pm 1.19967864026$  (solutions of the equation  $x= coth(x)$ ) depending on the starting point.

 $\mathrm{sech}(x)$ : 0.7650100

 $\dfrac{1}{\mathrm{arcsinh}(x)}$ :  $\pm 1.07293831517215$  depending on the starting point.

\(4\) Exponential functions\
 $e^{-x}$ : 0.5671433; remarkably this is  $\mathrm{W}(1)$ , where  $\mathrm{W}(x)$  is the function discovered by the polymath Johann Heinrich Lambert, in the 1700s. This value can be computed using the below definite integral:

 $$k= \displaystyle\int_{-\pi}^{\pi}\log\left(1+\dfrac{\sin(x)}{x}e^{\tfrac{x}{\tan(x)}}\right) dx$$ 

Then the fixed point of the exponential function,  $\textrm{F}(e^{-x})=\dfrac{k}{2\pi} \approx 0.5671433 \cdots$ latex
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp12_expdecay_complex.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 12. Number of iterations for convergence of functional iterates of  $e^{-x}$ *

In the complex plane, all points within the fractal boundary (Figure 12) converge to the same attractor at different rates or diverge to  $\infty$  (white regions).

 $2^{-x}$ : 0.64118574450499; comparable behavior as above in the complex plane. The closed form for this fixed point can be derived from the Lambert function:  $\textrm{W}(x)$ :

 $$\displaystyle \textrm{W}(x)=\dfrac{1}{2\pi}\int_{-\pi}^{\pi}\log\left(1+\frac{x\sin\left(t\right)}{t}e^{\frac{t}{\tan\left(t\right)}}\right)dt$$ 

Then the  $\textrm{FP}(2^{-x})= e^{-\textrm{W}(\log(2))}$ 

 $e^{-\tan(x)}$ : 0.54522571736464
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp13_gaussian.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 13. Number of iterations for convergence of functional iterates of  $e^{-x^2}$ *

 $e^{-x^2}$ : the attractor 0.652918640419 is the solution to the equation  $x^2+\log(x)=0$ . We can again find a closed form for this fixed point using  $\textrm{W}(x)$ :

 $$\textrm{FP}(e^{-x^2})= e^{-\frac{\textrm{W}(2)}{2}}$$ 

In the complex plane, all points within the fractal boundary (Figure 13) converge to the same attractor at different rates or diverge to  $\infty$  (white regions). It is interesting to see that one of the convergence contours recapitulates the curve  $y=e^{-x^2}$  reflected about the real axis (Figure 13).

 $\dfrac{1}{e\^x-x}$ : 0.7384324007018

 $\dfrac{1}{(e\^x-x)^2}$ : 0.63654121332649
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/fp14_1bylogofxsquared_complex.png){width="75%"}
```{=latex}
\end{center}
```

}$ *

 $\dfrac{1}{\log(x^2)}$ : This function is interesting in that it is chaotic on the real line with a repellor at 1.4215299358831... As the iterates approach 1 from below they are prone to negative explosions; if they do so from above, they undergo a positive explosion. The distribution of the iterates shows a preponderance of small values but when extreme values occur they are very large (explosions). Interestingly, in the complex plane, it converges to -0.32447650840966+0.31470495550992i (Figure 14). The number of iterations to convergence reveals a fractal pattern of interlocking circles.

While the fixed points can be determined by numerical solving the equations specifying them, the closed forms, if any, remain unknown for many of them. Finding if they exist would be a good exercise for the mathematically minded.

