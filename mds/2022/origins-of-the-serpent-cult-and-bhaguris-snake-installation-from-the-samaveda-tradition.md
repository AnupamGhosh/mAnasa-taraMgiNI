
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Origins of the serpent cult and भागुरि's snake installation from the सामवेद tradition](https://manasataramgini.wordpress.com/2022/12/27/origins-of-the-serpent-cult-and-bhaguris-snake-installation-from-the-samaveda-tradition/){rel="bookmark"} {#origins-of-the-serpent-cult-and-भगरs-snake-installation-from-the-समवद-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 27, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/12/27/origins-of-the-serpent-cult-and-bhaguris-snake-installation-from-the-samaveda-tradition/ "6:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/naga_mathura2.png){width="75%"}
```{=latex}
\end{center}
```



*Mathuran नाग installations*

From the few centuries preceding it down to the first few centuries of the Common Era we see numerous installations of snake deities, i.e., नाग-s, at various archaeological sites throughout northern India (most famously at the holy city of मथुरा). Comparable, but usually smaller नाग installations continue to this date in South India, usually in association with शैव and कौमार shrines. A related icon is that of the great सात्वत्त वैष्णव deity Balabhadra, who is depicted with a hooded snake canopy. Tradition holds that he was the incarnation or homomorph of the snake of विष्णु, often named Ananta. Given that विष्णु was the "time-god" par excellence, we hold that the snake imagery (the coils) associated with his bed is a metaphor for periodicities in time --- diurnal, lunar, solar and precessional cycles. In this note, we explore the connections of these later manifestations of the serpentine cult with the Vedic roots of snake worship (Ahi Budhnya of the earliest Vedic tradition), with probable deeper Indo-European antecedents and broad Eurasiatic ramifications.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/samkarshana_mathura.2.jpg){width="75%"}
```{=latex}
\end{center}
```



*संकर्षण installations at Tumbavana (L) and मथुरा (R)*

On one hand, we have the श्रौत sarpa-sattra outlined in the [सामवेद ब्राह्मण](https://manasataramgini.wordpress.com/2006/07/16/the-sarpasattra-vedic-and-aitihasic/)-s, which is modeled after a ritual supposed to have been performed by the नाग-s to gain their venom. The sarpa-sattra in an inverted form, viz., the ritual of Janamejaya to destroy the नाग-s who were responsible for his father's death, is the frame story of the national epic the महाभारत. The core story of the महाभारत itself is permeated with simultaneous inter-generational conflicts and marriages between the नाग-s and the पाण्डु-s. On the other hand, we have the गृह्य sarpabali that is enjoined in various गृह्यसूत्र-s and certain विधान-s. The sarpabali or the offering to the snake deities is performed when the full moon occurs in श्रवणा (the ecliptic division associated with the longitude of Altair). This bali usually coincides with the Indian Southwest Monsoon. Along with this bali the ritualist and his family sleep on a raised bed until the आग्रयण ritual. This, along with the contents of the ritual, indicate that its primary function was protection from snakes that might enter houses during the monsoons due to the flooding of their lairs. While the rite is found in most गृह्यसूत्र-s, that of the हिरण्यकेशिन् school associated with the तैत्तिरीयक tradition gives a rather detailed account of the rite. At first, the ritualist makes oblations of unbroken grains, unbroken fried grains, coarsely ground grains, leaves and flowers of the Kimsuka tree to Agni पार्थिव, वायु Vibhumant, सूर्य Rohita and विष्णु Gaura:

[नमो ।अग्नये पार्थिवाय पार्थिवानाम् अधिपतये स्वाहा । नमो वायवे विभुमत आन्तरिक्षाणाम् अधिपतये स्वाहा । नमः सूर्याय रोहिताय दिव्यानाम् अधिपतये स्वाहा । नमो विष्णवे गौराय दिश्यानाम् अधिपतये स्वाहा ॥]{style="color: #0000ff"}

After these oblations, the snakes of the earth (the real ones), the snakes of the atmosphere (lightning), the snakes of the heavens (आश्लेषा  $\approx$  the constellation of Hydra), and those of the directions (the serpent ogdoad) are worshiped with the famous यजुष्-es beginning with [नमो अस्तु सर्पेभ्यः]{style="color: #0000ff"} ... (TS 4.2.8.3) followed by the bali incantations [ये पार्थिवाः सर्पास्तेभ्य इमं बलिं हरामि । य आन्तरिक्षाः । ये दिव्यः । ये दिश्याः ॥]{style="color: #0000ff"}

Following the bali, the ritualist goes thrice around his dwelling in a circle corresponding to the radius that he wishes to keep the snakes away from sprinkling water from a pot while uttering the below incantation (the पारस्कर-गृह्य-सूत्र instead prescribes drawing a line with a white pigment):\
[अप श्वेत पदा जहि पूर्वेण चापरेण च । सप्त च मानुषीर् इमास् तिस्रश् च राजबन्धवैः । न वै श्वेतस्याभ्याचरेणाहिर् जघान कं चन । श्वेताय वैदर्वाय नमो नमः श्वेताय वैदर्वाय ॥]{style="color: #0000ff"}\
Smite away, O white one, with your foot, fore and hind, these seven women with the three of the king's clan. No one indeed, in the roaming ground of the white one the snakes have ever killed.

In the above incantation, the king and his clan evidently refer to the नाग king and his folks and the women to the नागकन्य-s. The white one is described as having fore and hind feet. This implies that he is none other than the white snake-killing horse (Paidva) given to Pedu by the अश्विन्-s:\
[युवं श्वेतम् पेदव इन्द्रजूतम् अहिहनम् अश्विनादत्तम् अश्वम् ।]{style="color: #0000ff"} RV 1.118.9a\
[पैद्वो न हि त्वम् अहिनाम्नां हन्ता विश्वस्यासि सोम दस्योः ॥]{style="color: #0000ff"} RV 9.88.4c

However, it is notable that the विष्णु deity specific to this ritual is विष्णु Gaura, or the white one, paralleling the color of the horse. In this regard, we may also point to the role of विष्णु in the अश्वमेध rite. After the horse has successfully wandered for an year, the emperor undergoes consecration. In preparation for the sacrifice, the oblations known as वैश्वदेव culminating in the पूर्णाहुति are offered over a period of seven days. On days one and two he offers to Ka प्रजापति; on day three to Aditi; on day four to सरस्वती; on day five to पुषन्; day six to त्वष्टृ विश्वकर्मन्; on day 7 to विष्णु the with the पुर्णाहुति. As per the तैत्तिरीय-श्रुति, two विष्णु deities are invoked in the rite in addition to the standalone विष्णु:\
[विष्णवे स्वाहा । विष्णवे निखुर्यपाय स्वाहा । विष्णवे निभूयपाय स्वाहा ॥]{style="color: #0000ff"}

These peculiar names of the two विष्णु deities, Nikhuryapa and निभूयप are rather enigmatic. Since they are unique to the अश्वमेध, we posit that Nikhuryapa could be विष्णु as the protector of the hoofs (khura: hoof), whereas निभूयप could be विष्णु as the protector of the stallion which makes the herds increase. These equine associations of विष्णु in the अश्वमेध raise the possibility that the white snake-smiting horse was also associated with the White विष्णु of the ritual. Interestingly, the color the संकर्षण is also said to be white. Moreover, the later tradition starting from the महाभरत preserves strong equine connections for विष्णु as हयशिरस्. Thus, in the least, one could say that the sarpabali ritual established an early connection between विष्णु and offerings to the snakes, which could have presaged its augmentation in the later tradition.

Other traditions associated with the Vedic sarpabali were also expanded in the later serpent cult. Evidence for this comes from an adaptation of the ritual found in the यजुर्विधान-सूत्र-s of the वाजसनेयिन्-s (YVS 15.8-11):\
[नमो ।अस्तु सर्पेभ्य इति घृत-पायसं नागस्थाने जुहुयात् । सुवर्णम् उद्पद्यते ॥ वृष्ट्यर्थे शिखण्ड्यादीञ् जुहुयात् वृष्टिर् भवति । अतसी-पुष्पैर् महावृष्टिर् भवति ॥]{style="color: #0000ff"}

The sarpa-यजुष् is deployed with oblations of ghee and milk pudding in the locus of the नाग-s in order to obtain gold. For rains he offers oblations of peacock feathers; for torrential rains, he offers flax flowers. Thus, in this विधान deployment of the sarpabali mantra, we see a reworking for obtaining gold (a connection already mentioned in the महाभारत 5.114.4 "vulgate": he guards the wealth/gold generated by Agni for Kubera) and rain (a connection possibly going back to Ahi Budhnya in the ऋग्वेद: RV 4.55.6; RV 7.34.16; तैत्तिरीय-संहिता in 1.8.14). The यजुर्विधान-सूत्र-s also describe a rite with a trident and a लिङ्ग made of cow dung in the fire-shed using this mantra for the rain-making and fearlessness ([नमो ।अस्तु सर्पेभ्य इति तिसृभिर् अर्घ्यं दद्याद् अग्न्यागारे गोमय-लिङ्गं प्रतिष्ठाप्य पञ्चगव्येन संस्नाप्य दक्षिणतः शूलं निखनेत् । पुनः सहस्रं जपेत् । सुवर्ण-शतंल् लभेत् सिद्धं । कर्मेत्य् आचक्षते वृष्टौ शिखण्डान् अतसीपुष्पाणि वा युञ्जन्तीति । महाभये जपेद् अभयं भवति ॥]{style="color: #0000ff"}). Similarly, a rite using an iron trident is offered for the subjugation of नाग-s with a mantra to Agni ([अजीजन इति रहस्यो मन्त्र (ऋग्वेद ३।२९।१३) एतेन नागा वशम् उपयान्ति । लौहं त्रिशूलग्ं सहस्राभिमन्त्रितं कृत्वा दक्षिण-पादेनाक्रम्य पयो-दधि-मधु-घृतैर् अयुतं हुत्वा विकृत-रूपा स्त्रिय उत्तिष्ठन्ति । किम् अस्माभिः कर्तव्यम् इति ब्रुवन्त्यो अभिरुचिकामेन ताम् आज्ञापयेत् ॥]{style="color: #0000ff"}). This later rite is developed further within the शैव context in the जयद्रथयामल-tantra. These objectives outlined in the विधान were greatly expanded in early शैव and bauddha traditions (also seen in the Indic-influenced चीन dragon traditions). These themes are brought together rather dramatically in the story of the द्राविड मन्त्रवादिन्, the नाग महापद्म residing in a Kashmirian lake, and the king जयापीड narrated by कल्हण in the राजतरंगिणी (4.593 onward).

However, the question remains as to whether the sarpabali of the old गृह्य tradition had any connection with the installation of the images of नाग and संकर्षन seen at the archaeological sites. A potential transitional rite describing a Vaidika snake installation comes from a now apparently extinct सामवेद tradition, namely the Gautama school, which seems to have been practiced in some form in the कर्णाट country till around 1600-1700 CE. The Gautama-गृह्य-परिशिष्ट furnishes a detailed नाग-प्रतिष्ठ ritual attributed to भागुरि (GGP 2.12):

  - The ritual is to be performed on the 12th tithi of a शुक्लपक्ष when the moon is in a देवनक्षत्र (i.e., Northern half of the ecliptic) or during the northern course of the sun or on an auspicious नक्षत्र.

  - On the day before the installation rite, the ritualist brushes his teeth, takes a bath with water from a तीर्थ (holy ford) and having performed the संकल्प for the installation, immerses the image in water.

  - He chooses an आचार्य who delights in right conduct and of peaceful temperament and performs the rite via his instruction.

  - Having cleansed the spot for installation, the आचार्य washes his feet, performs आचमन, and having seated himself, performs प्राणायाम and संकल्प.

  - He recites the पुण्याह incantations ([हिरण्यवर्णाः...]{style="color: #0000ff"}) and sprinkles the image with water. He recites the triple व्याहृति-s and lustrates the image with the five bovine products.

  - He washes the images with clean water utter [आपो हि ष्ठ...]{style="color: #0000ff"} (SV-Kauthuma 1837) and [तरत् स मन्दी धावति...]{style="color: #0000ff"} (SK-K 500, 1057)

  - He utters ॐ and lustrates the image with water in which gold flakes, the shoots of दूर्व grass and पलश leaves have been placed. He offers flowers and दूर्व grass at the feet of the image.

  - He utters the सावित्री or ॐ and cloaks the image with newly woven unwashed clothing.

  - He offers special naivedya and recites [स्वस्ति न इन्द्रो...]{style="color: #0000ff"} incantation. Thereafter, he immerses the image in a river while singing the वरुण-सामन्.

  - He rises the next day and performs his नित्यकर्माणि, he proceeds with the आचार्य and assistant ritualists (like in the श्रौत ritual) to the place where he has immersed the image. There, they bring out the image while reciting [प्रैतु ब्रह्मणस् पतिः प्र देव्य् एतु सूनृता ।]{style="color: #0000ff"}...(SV-K 56). Then they install it at the designated spot and perform प्राणायाम and संकल्प.

  - They again lustrate the image with the five bovine products while reciting [ॐ नागाय नमः]{style="color: #0000ff"}. Then they wash it with clean water and cloak it with a new dress. They decorate it with scented unguents and flowers.

  - Then they perform न्यास both of the self and the image thus: [ॐ नागाय नमः । हृदयाय नमः । ॐ नागाय नमः । शिरसे नमः । ॐ नागाय नमः । शिखायै नमः । ॐ नागाय नमः । कवचाय नमः । ॐ नागाय नमः । नेत्रत्रयाय नमः । ॐ नागाय नमः । अस्त्राय नमः ।]{style="color: #0000ff"}\
Then he does a ध्यान of the serpentine deity:\
[सर्पो रक्तस् त्रिनेत्रश् च द्विभुजः पीतवस्त्रगः ।]{style="color: #0000ff"}\
[फण्कोटिधरः सूक्ष्मः सर्वाभरण-भूषितः ॥]{style="color: #0000ff"}

  - He then measures out a द्रोण of paddy, clean rice and sesame seeds and spreads them out one over the other. On them, he draws out an eight-petaled lotus and installs a pitcher on top of it.

  - Inside the pitcher, he places five each of barks, shoots, soils, gemstones, bovine products, ambrosial sweets, scents, kinds of rice, medicinal herbs, and unguent powders.

  - He drapes the pitcher with a new piece of cloth and invokes नागेश in it:\
[ॐ भूः । पुरुषम् आवाहयामि । ॐ भुवः । शेषम् आवाहयामि । ओग्ं सुवः । अनन्तम् आवाहयामि ॥]{style="color: #0000ff"}

  - He then provides the deity with the 16-fold sacraments uttering ॐ अनन्ताय नमः for each.

  - He then worships the deity with the following mantra:\
[आयातु भगवान् शेषः सर्व-कर्म-सनातनः ।]{style="color: #0000ff"}\
[अनन्तो मत् प्रियार्थाय मद् अनुग्रह-काम्यया ॥]{style="color: #0000ff"}

  - The four ब्राह्मण ritual assistants and the आचार्य touch the pitcher and recite [आपो हि ष्ठ...]{style="color: #0000ff"}

  - Then they recite the पुरुष hymn.

  - Then they sing the following सामन्-s: Sarpa, वामदेव्य, Rathantara, बृहत्, ज्येष्ठ and भारुण्ड.

  - Then they recite [ॐ नमो ब्रह्मणे...बृहते करोमि]{style="color: #0000ff"} (तैत्तिरीय आरण्यक 2.13.1).

  - To the west of the pitcher, the आचार्य sets up a स्थण्डिल (fire altar). To the north of the altar, he collects twelve materials for the प्रधानाहुति-s (main oblations) and offers them with the following incantations into the fire followed by a [स्वाहा]{style="color: #0000ff"} and the त्याग formula: [इदं अनन्ताय न मम ।]{style="color: #0000ff"}\
[सद्योजातम् प्रपद्यामि...]{style="color: #0000ff"}: samidh-s\
[वामदेवाय नमो...]{style="color: #0000ff"}: ghee\
[aghorebhyo 'tha...]{style="color: #0000ff"}: cooked rice\
[तत् पुरुषाय विद्महे...]{style="color: #0000ff"}: fried rice\
[ईशानः सर्वविद्यानाम्...]{style="color: #0000ff"}: saktu flour\
[ॐ नागाय नमः]{style="color: #0000ff"}: milk\
[हृदयाय स्वाहा]{style="color: #0000ff"}: barley\
[शिरसे स्वाहा]{style="color: #0000ff"}: sesame\
[शिखायै स्वाहा]{style="color: #0000ff"}: sugarcane\
[कवचाय स्वाहा]{style="color: #0000ff"}: banana\
[नेत्र-त्रयाय स्वाहा]{style="color: #0000ff"}: jackfruit\
[अस्त्राय स्वाहा]{style="color: #0000ff"}: mustard

  - 25 oblations are made of each item. Thereafter, he offers sesame  $8 \times, 28 \times, 108 \times$  with [ॐ भूर्-भुवः स्वः स्वाहा ।]{style="color: #0000ff"}.

  - He then worships the serpentine deity with the below incantation calling on him to accept all the oblations:\
[त्वाम् एव चाद्यम् पुरुषम् पुराणम् ।]{style="color: #0000ff"}\
[सनातनम् विश्वधरं यजामहे ।]{style="color: #0000ff"}\
[मद् अर्पितं सर्वम् अशेष-हव्यम्]{style="color: #0000ff"}\
[गृह्णीष्व मां रक्ष जगन्निवास ॥]{style="color: #0000ff"}

  - Then he gives the ब्राह्मण-s their fees and sings the वामदेव्य सामन्.

  - Then to the singing of the सर्पसामन् he lustrates the image of नागेश with the contents of the pitcher, followed by the five bovine products, the five ambrosial sweets, curds, milk, coconut juice, whey, sugarcane juice and finally scented water.

  - Then he recites the Mantra ब्राह्मण 2.8.6, utters [ॐ नागाय नमः]{style="color: #0000ff"} thrice, and offers पाद्य to the image.

  - He recites [अन्नस्य राष्ट्रिर् असि...]{style="color: #0000ff"} (MB 2.8.9) and offers arghya.

  - With [यशो ।असि...]{style="color: #0000ff"}(MB 2.29.16) he offers आचमन.

  - With [यशसो यशो ।असि...]{style="color: #0000ff"} (MB 2.8.11) he offers madhuparkam.

  - With [ॐ नागाय नमः]{style="color: #0000ff"} he successively offers, lower garments, an उपवीत, upper garments, and ornaments.

  - With [गन्धद्वारां दुरादर्षाम्...]{style="color: #0000ff"} he offers scents.

  - With [ईडिष्वा...]{style="color: #0000ff"} (SV-K 103) he offers incense.

  - With [पवमानः...]{style="color: #0000ff"} dyad (SV-K 484) he offers a lamp.

  - The आचार्य drapes the image with a new robe and also himself.

  - With [शुक्रम् असि ज्योतिर् असि तेजो ।असि]{style="color: #0000ff"} (in TS 1.1.10) he takes up a golden needle. With [विश्वतश्चक्षुर् उत विश्वतोमुखो विश्वतोबाहुर् उत विश्वतस्पात् ।]{style="color: #0000ff"} (RV 10.81.3a) and uttering ॐ he activates the eyes of the image with the golden needle.

  - He touches the heart of the image and recites the प्राणप्रतिष्ठ incantation invoking the goddess Anumati  $28 \times$  to infuse the image with consciousness:\
[असुनीते पुनर् अस्मासु चक्षुः]{style="color: #0000ff"}\
[पुनः प्राणम् इह नो धेहि भोगम् ।]{style="color: #0000ff"}\
[ज्योक् पश्येम सूर्यम् उच्चरन्तम्]{style="color: #0000ff"}\
[अनुमते मृऴया नः स्वस्ति ॥]{style="color: #0000ff"} (RV 10.59.6)

  - Maidens of good disposition display lamps to the image and a cow is led before it.

  - The image is placed over a deposit of a gemstone, pearl, coral, gold and silver atop which a white cloth has been spread.

  - Having decorated the image, the यजमान worships the deity with the incantations: [ॐ]{style="color: #0000ff"} [शेषाय नमः । ॐ भूधराय नमः । ओम् अनन्ताय नमः ।]{style="color: #0000ff"}

  - He then offers naivedya of milk pudding, cooked rice, sesame rice, turmeric rice, अपूप cake, पूरिका bread, and the शर्कराढ्य sugar pastry. Thereafter, he offers betel leaves.

  - Having given gifts to the आचार्य and his assistant ब्राह्मण-s, he takes the image and has it permanently installed at a temple of Rudra or विष्णु, or under a pipal tree while reciting the mantras:\
[उद्गातेव शकुने साम गायसि]{style="color: #0000ff"}\
[ब्रह्मपुत्र इव सवनेषु शंससि ।]{style="color: #0000ff"}\
[वृषेव वाजी शिशुमतीर् अपीत्या]{style="color: #0000ff"}\
[सर्वतो नः शकुने भद्रम् आ वद]{style="color: #0000ff"}\
[विश्वतो नः शकुने पुण्यम् आ वद ॥]{style="color: #0000ff"} (RV 2.43.2)

  - He then worships the serpentine deity performing 12 नमस्कार-s with the following incantations:\
[अनन्ताय नमः । नागाय नमः । पुरुषाय नमः । सर्पेभ्यो नमः । विश्वधराय नमः । शेषाय नमः । विश्वम्भराय नमः । संकर्षणाय नमः । बलभद्राय नमः । तक्षकाय नमः । वासुकये नमः । शिवप्रियाय नमः ।]{style="color: #0000ff"}

  - He concludes by feeding 12 ब्राह्मण-s of good learning and character and educating children.

  - He who does such a snake installation obtains 8 children, whatever he prays for, and the higher realms.

There are several notable points regarding this ritual:

  - Its essential details, including the new mantra-s specifically spelt out in the text, closely relate to other iconic स्थापन rites specified in the late Vedic texts. These include: 1. The installation and worship of Skanda (AV Skandaयाग and धूर्तकल्प of the बोधायन-परिशिष्ट); 2. The black goddess of the Night, रात्री-देवी (AV-परिशिष्ट 6); 3. The भार्गव Brahma-याग (AV-par 19b), where an image of the god Brahman is installed; 4. गोशान्ति (AV-par 66), where an image of Rudra fashioned out of cow dung is installed in the midst of a मण्डल for the protection of cattle. Similarly, a metal/stone image of Rudra is installed in the बोधायन-परिशिष्ट and also deployed by the वाधूल-s in their वाधूलगृह्यागम, a versified version of their गृह्यसूत्र-s  and परिशिष्ट-s. 5. Installations of the images of विष्णु and दुर्गा according to the बोधायन-परिशिष्ट-s. The former is also specified in the वाधूल collection. Thus, it may be inferred that the नाग-प्रतिष्ठ of the Gautama-गृह्य-परिशिष्ट is of the same genre and likely the same temporal period marking the tail end of the Vedic age and the transition to the Tantro-पौराणिच् age.

  - Here the character of the नाग has evolved from that seen in the earlier गृह्य sarpabali. While the sarpa-s are venerated in the sarpa-यजुष् they are also expelled by means of the white horse of Pedu and the perimeter of safety is established. However, in the नाग-प्रतिष्ठ the snake deity is not just clearly positive but is also identified with the पुरुष himself.

  - The text presents an early example of the षोडशोपचार-पुजा that was to become dominant in the Tantro-पौराणिच् iconic worship. It may also mark the earliest account of the eye-opening rite that became prominent in the later आगमिक strand of the religion.

  - Several mantra-s which are provided only by प्रतीक-s are missing in the Kauthuma-राणायनीय and जैमिनीय texts and their auxiliary mantra collections. This suggests that the Gautama सामवेदिन्-s had their own auxiliary mantra collection that was distinct from the extant texts. It is conceivable that they had the पञ्चब्रह्म-mantra-s, which today are only found as a complete group in the तैत्तिरीय and AV महानारायण texts.

  - The text rather remarkably combines both शैव and वैष्णव elements. The former is seen in the form of the पञ्चब्रह्म-mantra-s and the latter is seen in the form of the explicit identification of the serpentine deity with the पुरुष and also संकर्षण/Balabhadra. Both these aspects persisted in the subsequent layers of the religion. The serpentine form remained a key aspect of the iconography of the संकर्षण and Ananta figure as the bed of विष्णु. The नागप्रतिष्ठ continued as a ritual with new शैव accretions in the सैद्धान्तिक stream in रुद्रालय-s (e.g., the Raurava tantra). Notably, it was also continued with modifications in the Bauddha practice of the मूलमन्त्र-सूत्र (where it is combined with the old rain-making ritual) that was preserved in a rather pristine form among the Chinese ritualists.

We see a convergence of philology and archaeology with respect to नाग-प्रतिष्ठ-s, offset by 2-3 centuries, perhaps due to preservation bias. In the bauddha lore, we hear of the famous conflict between the तथागत and the Vaidika ब्राह्मण Urubilva जटिल काश्यप (Vinaya 1.25). The latter had evidently installed a नाग in his fire-shed which the तथागथ is claimed to have subjugated. This would be consistent with some version of the rites as recorded in the नाग-प्रतिष्ठ from the सामवेद tradition being in place by around the time of the स्हाक्य. Alternatively, it could be an allusion to the snake deity Ahi Budhnya being stationed at the गार्हपत्य fire altar upon the conclusion of rituals in it (उपस्थान).  Subsequently, as noted above, by the Mauryan-शुङ्ग age we see evidence for such installations and also images of Balabhadra in archaeology continuing down to the age of the कुषाण-s. Notably, both the early bauddha and jaina texts mention the worship of Balabhadra providing approximately coeval philological evidence for the same. Further, some of the early पाशुपत शैव shrines like that of Bhogyavardhana (modern Bhokardhan in Maharashtra state) and विष्णुकुण्डिन् temple (at Devunigutta, Kothur, modern Andhra Pradesh) depict the संकर्षण suggesting further development of the potential links indicated by the use of the पञ्चब्रह्म-mantra-s in the installation of the snake.

