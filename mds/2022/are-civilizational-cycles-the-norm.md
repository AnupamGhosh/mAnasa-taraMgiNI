
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Are civilizational cycles the norm?](https://manasataramgini.wordpress.com/2022/02/20/are-civilizational-cycles-the-norm/){rel="bookmark"} {#are-civilizational-cycles-the-norm .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 20, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/02/20/are-civilizational-cycles-the-norm/ "7:12 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Nearly two and half decades back, we used to have several conversations with a late शूलपुरुषीय professor, mostly on topics with a biological angle. While not a mathematician, he had a passing interest in dynamical systems, for he felt that they held the clues to great secrets of biology. We, too, were interested in dynamical systems for the lessons they offered at a certain abstract level that went beyond biology --- as far as biology was concerned, we are of the firm opinion that there is no substitute to understanding its actual molecular details and no amount of mathematics can fill in for insight from that domain. Once, while chatting more generally, the शूलपुरुषीय professor was piqued by our interest in the ideas of his compatriot Oswald Spengler. He confessed that though they were no longer popular in academic circles, he too had a deep interest in Spengler's ideas. Our शूलपुरुषीय professor had a friend who had (then) recently died, a महाम्लेच्छ professor of upari-शूलपुरुषीय extraction, who in life was a deep student of macroevolution although little known outside his circles --- precisely the kind of personality who would be buried in the current mleccha Zeitgeist. That dead professor, in turn, was close to a remarkable Russian of several talents --- an evolutionist, an adventurer, an author of fiction and a prognosticator. Our late शूलपुरुषीय professor was in possession of a discussion on Oswald Spengler and related matters between the upari-शूलपुरुष and the Rus, with the former taking the view that ultimately Spengler was right in his broad account on the life-history of civilizations. The Rus, in contrast, had a peculiar view that our शूलपुरुष was puzzled by but knew little about. He correctly realized we might be the best to take a look at that.

In the exchange with the upari-शूलपुरुष the Rus had said that inspired by his readings of Hindu lore, he believed that all civilizations would go through cycles, large and small, rising and falling with some quasi-periodicity. We were amused to see that he had explicitly stated we were verily in the Kaliyuga whose terminal days would see a decline and collapse of civilizations across the globe. Our शूलपुरुषीय professor remarked that people could have felt like that in every age; thus, it becomes a self-fulfilling prophecy with little reality to it. Nevertheless, he stated that the Spenglerian termination would by no means be pleasant either, and the question remains of what comes after that? He wished us to present our views on this topic. He had a vague sense that perhaps unbroken civilizations, such as those we came from, might have different memories than those of the Occident. We mentioned to him that indeed many civilizations of the old world had the concept of the end of times, which had been borrowed by the Abrahamists and incorporated into their own framework. However, our tradition had a concept of many cycles of existence, yuga-s and kalpa-s, with things going up and down, which is unique in terms of its time scale and detail. In our teens, we wondered about this peculiarity of our tradition and felt it might represent an inductive projection from a long-lasting tradition that had memories of cycles of booms and busts.

The average human life is long enough to see change in course of it but is too short relative to the existence of a species to place this change in the context of historical time. Thus, it is not surprising if the lifetime experience tilts one towards a unidirectional view of change. This not only affects an individual but a whole civilization if it has a short tradition. Indeed, the ascendant Occident might be viewed as a civilization with such an abbreviated tradition. Hence, there is a stronger tendency to view civilizational dynamics as a simplex and an insistence that growth is forever in some quarters. Even if someone were to see through this, it sometimes benefits them to maintain the lie because, in the least, it deflects the frustration of the masses away from them if the yuga was indeed on an अवसर्पिणि course. In contrast, a civilization with a long tradition might see the risings and fallings of civilization to be a natural process --- something which is unavoidable even if one were to swim against the tide desperately. One could say that Hindus have existed in such a state for a while. The notable feature of the Hindu model is its scale --- going hand in hand with the much-maligned Hindu love for big numbers, the cycles are said to span huge temporal registers. This is closely intertwined with the Hindu mathematical approach to astronomy (going back to the श्रुति), wherein cycle synchronizations using solutions to indeterminate equations play an important role. Thus, it admits the space for the perception gained over a human lifespan being a misreading of the real trend of the slowly plodding yuga.

However, we kept wondering if such cyclicity should really be the norm of civilizational existence, especially when so many so-called intellectuals from the modern Occident kept assuring us that a great future of eternal progress was what we should look forward to. We were well conversant with the logistic growth curve; hence, we knew that such a dream was ultimately delusional. However, the scale was not clear --- were we in the early stage where growth can be close to exponential, or were we nearing the inflection towards the eternal plateau. We did realize that certain resources, like energy, which were fueling our growing frenzy, were finite; hence, once they were exhausted, we would have to simplify. Finally, works like that of Tainter, [which we have discussed before](https://manasataramgini.wordpress.com/2018/01/19/civilizational-collapse-complexity-innovation-and-neomania/), indicated that civilizational collapse was the norm. Hence, the same could befall us. The most profound impact in this regard came in the 16th year of our life from our learning of the 2D cellular automaton of Ulam, the original investigator of cellular automata (CA). We had explored several CA before and after this period, which yielded deep insights germane to the topic under discussion. However, here we shall limit ourselves the classical Ulamian automaton, which offers useful insights, despite its extreme simplicity. After that study, we have been rather convinced that chaotic civilizational cycling, albeit with some predictable features, is unavoidable. We gave a demonstration and an account of the same to our शूलपुरुषीय professor, and it seemed to excite him greatly. We must reiterate that this is only a demonstration of an analogy, not a proof of anything. However, such dynamics with the simplest of rules lead us to conjecture that is also true of the more complex systems of everyday life, although with different parameters.

The Ulamian CA is played on a 2D grid with each cell in it capable of assuming 2 states, either 0 or 1 (shown below as red and blue). The system can be represented by a square matrix  $M$  whose elements are either 0 or 1. The state of each cell in the subsequent step is dependent on the states in a neighborhood of 5, i.e., the cell itself  $M\lbrack j,k\rbrack$  and its 4 neighbors as shown below  $(M\lbrack j-1,k\rbrack , M\lbrack j+1,k\rbrack , M\lbrack j,k-1\rbrack , M\lbrack j, k+1\rbrack )$ . The rule for assigning the state of a given cell in step  $n+1$  is: if the sum of a cell's neighborhood is even (0, 2, 4), the cell is 0; if the sum of the neighborhood of a cell is odd (1, 3, 5) is it 1.

![Ulam_neighborhood](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/ulam_neighborhood.png){width="75%"}

*The neighborhood for the Ulamian CA; the value of the neighborhood is 2 in this case*

Thus, the simple system is balanced --- there is an equal number of neighborhood states that can lead to it being 0 or 1. It allows for "autocatalytic" conversion, i.e., single 1 cell to convert its neighbors in an outward growth at the same time it has inbuilt inhibition of crowding by setting the even neighborhoods to 0. Hence, it may be seen as capturing some of the basics of processes seen in life or structure like a civilization that grows by the expansion of its units.

![2Dauto_129_animation](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_animation.gif){width="75%"}

*Figure 1. The Ulamian CA on an infinite planar grid*
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_1.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 2. The first 65 steps of the Ulamian CA on an infinite grid*

The CA can be played on an infinite planar grid or a finite torus. We first consider the former case by initializing it with a single 1 cell at  $n=1$ 

**1, 5, 5, 17, 5, 25, 17, 61, 5, 25, 25, 85, 17, 85, 61, 217, 5, 25, 25, 85, 25, 125, 85, 305, 17, 85, 85, 289, 61, 305, 217, 773, 5, 25, 25, 85, 25, 125, 85, 305, 25, 125, 125, 425, 85, 425, 305, 1085, 17, 85, 85, 289, 85, 425, 289, 1037, 61, 305, 305, 1037, 217, 1085, 773, 2753, 5 ...**

*[![2Dauto_129_entropy_evolution](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png){.wp-image-13545 .aligncenter attachment-id="13545" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="2Dauto_129_entropy_evolution" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png" orig-size="6600,4200" permalink="https://manasataramgini.wordpress.com/2022/02/20/are-civilizational-cycles-the-norm/2dauto_129_entropy_evolution/" height="359" loading="lazy" sizes="(max-width: 565px) 100vw, 565px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png 565w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png 1128w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png 1024w" width="565"}](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_129_entropy_evolution.png)Figure 3. Number of 1 cells and entropy in the evolution of the Ulamian CA on an infinite grid*

One observes that new "big" maxima (Figure 3) are attained at steps equal to powers of 2: **1, 5, 17, 61, 217, 773, 2753, 9805, 34921, 124373, 442961, 1577629...** that are followed by minima of value 5. The value at the step just before the  $2\^k$ 

Further, from the above relationships we obtained a closed form for the above sequence of the total number of cells with value 1 in the Ulamian CA:\
\$f[1]=1; f[2k-1]=f\[k]; f[4k-2]=5f\[k]; f[4k]=3f[4k-1]+2f[4k-3]\$
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_513_evolution.png){width="75%"}
```{=latex}
\end{center}
```

*Figure 4. The number of 1 cells in the evolution of the Ulamian CA to 513 step*s

Thus, we can compute the number of 1 cells of the CA on an infinite grid at any step without actually having to run it (Figure 4). This shows us that this sequence has a fractal structure with cycles of length  $2^{k+1}-2\^k$ 

![2Dauto_37_animation](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_37_animation.gif){width="75%"}*
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_37_3.png){width="75%"}
```{=latex}
\end{center}
```

*[Figure 6. First 300 steps in the evolution of the Ulamian CA on a torus of circumferences of 37]{style="color:initial;"}*

A more interesting situation arises when we play the automaton on a finite torus (Figure 5 shows an animation on a torus flattened out as a  $37 \times 37$ 

![2Dauto_37_entropy_evolution](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/2dauto_37_entropy_evolution.png){width="75%"}*Figure 7. Number of 1 cells and entropy in the evolution of the Ulamian CA on a torus for 1100 steps*

Thus, the Ulamian CA played on a torus captures key features of real-world populations in a very simple way: The rules of the CA establish the need for preexisting founders for growth as well as a local effect of death from over-crowding. The torus mimics the finiteness of resources or space for growth. Thus, after reaching the maximal circumference, the populations have to fold onto themselves, which on one hand can allow more complexity by fostering a greater diversity of local configurations and, on the other, cause overcrowding leading to a bust. Thus, when we first watched the Ulamian CA play out on our computer screen, we realized that civilizational cycles are likely to be inevitable --- both in terms of population density and complexity of organization. It also told us that, in qualitative terms, the cycles would be chaotic; however, there would be certain statistically predictable elements. This brings to mind a popular adage: "History may not repeat itself. But it rhymes (pseudo-Mark Twain)." It also offered us three key insights regarding the complexity of civilizations. First, high densities can only be sustained via structural complexification --- an approximation of fractal organization. Second, there is usually close to maximal complexity before a great bust --- the end is usually sudden. Third, the maintenance of a protracted golden age goes with constant reconfigurations of high complexity states, i.e., there is no stable high complexity convergence. So the golden ages are not marked by a single stable state but a constant churn that results in a certain quasi-fractal complexity being maintained without tipping over to collapse from overcrowding and loss of that fractality. This again reminds one of the ideas of authors like [Tainter](https://manasataramgini.wordpress.com/2018/01/19/civilizational-collapse-complexity-innovation-and-neomania/), who have closely studied civilizational collapse --- there is increasing complexification followed by a collapse.

Real population and civilizations are way more complex in their specifics than a 2D automaton operating under simple rules. Yet, our intuition is that if a system with the simplest of assumptions to capture growth and local resource competition can produce great complexity of behavior, then the same should be expected of a more parameterized system. Among other things, the various specifics can be seen as vectors acting in different directions and canceling out each other. Thus, it leaves us with the relatively simple inescapables --- just as population dynamics and life-history can be modeled by simple equations like the logistic curve even though the actual elements contributing to the  $r, K$ 

