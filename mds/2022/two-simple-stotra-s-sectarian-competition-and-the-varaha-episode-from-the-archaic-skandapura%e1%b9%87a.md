
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Two simple stotra-s, sectarian competition, and the वराह episode from the archaic Skandaपुराण](https://manasataramgini.wordpress.com/2022/12/21/two-simple-stotra-s-sectarian-competition-and-the-varaha-episode-from-the-archaic-skandapura%e1%b9%87a/){rel="bookmark"} {#two-simple-stotra-s-sectarian-competition-and-the-वरह-episode-from-the-archaic-skandaपरण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 21, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/12/21/two-simple-stotra-s-sectarian-competition-and-the-varaha-episode-from-the-archaic-skandapura%e1%b9%87a/ "7:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Ur-Skandaपुराण (SkP) or the "archaic" Skandaपुराण (  $\approx$  the भट्टाराइ edition known as the अम्बिका-खण्ड) is a शैव text with affinities to the पाशुपत branch of that tradition. Though it is aware of the mantra-मार्ग traditions like the मातृ-tantra-s and the यामल-tantra-s as being part of the शैव scriptural corpus, its emphasis on the पाशुपत-vrata makes it clear that the core affiliation of the text was with the पाशुपत-mata that later शैव tradition identified as अतिमार्ग. Nevertheless, it shows imprints of a three-way struggle for dominance between the major Hindu sects --- शैव, कौमार and वैष्णव. As the Skandaपुराण, the existence of a कौमार layer is unsurprising. However, in the text, as it has come down to us, the कौमार elements are largely subordinated to the "वीर" (as in strongly sectarian)-शैव elements. The subordination of the वैष्णव-mata is primarily directed against the great deeds of विष्णु in his [नृसिंह](https://manasataramgini.wordpress.com/2012/02/02/notes-on-vaishnava-shaiva-and-kaumara-sectarian-competition-in-the-purana-s/) and वराह forms. In this regard, the Ur-SkP has a rather unprecedented ordering of the Daitya dynasty and the corresponding incarnations of विष्णु:

हिरण्यकशिपु  $\to$  हिरण्याक्ष  $\to$  Andhaka  $\to$  प्रह्लाद  $\to$  Virocana  $\to$  Bali.

While Vipracitti is mentioned as assisting हिरण्याक्ष in his battle with विष्णु, and being overthrown by the latter, it is not clear if he ever occupies the Daitya throne. The thus ordered Daitya-s are respectively slain by विष्णु as नृसिंह; विष्णु as वराह; Rudra; विष्णु (and Indra); Indra; विष्णु as वामन-Trivikrama. The main battle with प्रह्लाद as the Daitya emperor is situated in the episode of the churning of the World-ocean, during which विष्णु manifests as the gigantic turtle कूर्मा and also the bewitching female form मोहिनी. As per the Ur-SkP, while विष्णु suppresses प्रह्लाद in an epic battle during this episode, he continues leading the Daitya-s in several further fights till विष्णु assisted by Indra destroys him. However, the पद्मपुराण (13.186) places his slaying by Indra ([an incident alluded to in the श्रुति itself](https://manasataramgini.wordpress.com/2013/07/25/a-brief-note-on-prahlada-hiranyakashipu-and-an-early-narasimhakhyana/)) right in the episode of the churning of the World-ocean followed by the slaying of his son Virochana by Indra during the तारकामय देवासुर-yuddha. Correspondingly, in contrast to most other पुराण-s, in the Ur-SkP, the vibhava-s of विष्णु come in the order: नृसिंह, वराह, कूर्म/मोहिनी, वामन/Trivikrama.

As we have seen [before](https://manasataramgini.wordpress.com/2012/02/02/notes-on-vaishnava-shaiva-and-kaumara-sectarian-competition-in-the-purana-s/), नृसिंह is shown as being subdued by Rudra in his dinosaurian शरभ form after he has slain हिरण्यकशिपु. The Ur-SkP has several parallels to the वामन-पुराण, but in the latter, the शरभ-नृसिंह is given a स्मार्त resolution rather than a demonstration of Rudra-paratva. Upon being subdued by शरभ, in the Ur-SkP, नृसिंह is said to have recited the below stotra to Rudra. A votary who recites the stotra is said to attain the state of a गण of Rudra.

The stotra to शरभ by नृसिंह:

[नमः शर्वाय रुद्राय सेनान्ये सर्वदाय च ।]{style="color: #0000ff"}\
[नमः परम-देवाय ब्रह्मणे परमाय च ॥५४॥]{style="color: #0000ff"}\
[कालाय यमरूपाय कालदण्डाय वै नमः ।]{style="color: #0000ff"}\
[नमः कालान्त-कर्त्रे च कालाकाल-हराय च ॥५५॥]{style="color: #0000ff"}\
[नमः पिनाकहस्ताय रौद्र-बाण-धराय च ।]{style="color: #0000ff"}\
[चण्डाय वामदेवाय सर्वयोगेश्वराय च ॥५६॥]{style="color: #0000ff"}\
[नमो विद्याधिपतये ब्रह्मणः पतये नमः ।]{style="color: #0000ff"}\
[नमो 'सुरवरघ्नाय कालचक्राय वै नमः ॥५७॥]{style="color: #0000ff"}\
[संवर्तकाग्नि-चक्राय प्रलयान्तकराय च ।]{style="color: #0000ff"}\
[नरनारायणेशाय नरनारायणात्मने ॥५८॥]{style="color: #0000ff"}\
[ममैव वरदात्रे च सर्वकामप्रदाय च ।]{style="color: #0000ff"}\
[शरभाय सुरूपाय व्याघ्र-चर्म-सुवाससे ॥५९॥]{style="color: #0000ff"}\
[नन्दीश्वर-गणेशाय गणानां पतये नमः ।]{style="color: #0000ff"}\
[इन्द्रियाणामथेशाय मनसां पतये नमः ॥६०॥]{style="color: #0000ff"}\
[नमः प्रधानपतये सुराणां पतये नमः ।]{style="color: #0000ff"}\
[नमो 'स्तु भावपतये तत्त्वानां पतये नमः ॥६१॥]{style="color: #0000ff"}\
[चराचरस्य पतये भूतानां पतये नमः ।]{style="color: #0000ff"}\
[त्रैलोक्यपतये चैव लोकानां पतये नमः ॥६२॥]{style="color: #0000ff"}\
[योगदाय नमो मह्यं तथैवैश्वर्यदाय च ।]{style="color: #0000ff"}\
[अवध्यत्व-प्रदात्रे च तथैवाजय्यदाय च ॥६३॥]{style="color: #0000ff"}\
[भगवंस् त्वत्प्रतिष्ठो ।अस्मि त्वन् निष्ठस् त्वत् परायणः ।]{style="color: #0000ff"}\
[शरणं त्वां प्रपन्नो ।अहं प्रसीद मम सर्वदा ॥६४॥]{style="color: #0000ff"}

We shall discuss below some notable epithets used in this stotra:\
1. The first three epithets: शर्व, Rudra and सेनानि, betray the influence of the शतरुद्रीय; this influence is seen in several later शैव stotra-s.\
2. Parama-deva and brahman indicate the identification of Rudra with the supreme deity, keeping with the पाशुपत affiliation of the text.\
3. काल, यमरूप, कालान्त-कर्तृ: These epithets associated with Yama and the end of time bring to mind the epithets in the opening mantra-s for लिङ्गस्थापना: nidhanapati and निधनपतान्तिक.\
4. raudra-बाण-dhara: evidently a reference to the पाशुपतास्त्र.\
5. कालचक्र: While विष्णु was the original time deity, within the शैव tradition, Rudra gradually began expanding into that domain. This is one of the early references to Rudra as the कालचक्र -- a term that was to be used by the वज्रयान bauddha-s for their eponymous Bhairava-like deity. On the Hindu side, the original कालचक्र-tantra was a saura text. We have philological and iconographic evidence for a prolonged interaction between the Saura-s and शैव-s. Interestingly, the पशुपत shrines at काम्यकेश्वर and हर्षनाथ combine शैव and Saura elements. Most striking are two shrines near काम्यकेश्वर: लकुलीश is shown on the lintel of the Saura temple, and सूर्य is shown on the lintel of the रुद्रालय. Thus, we posit that the syncretic or interacting शैव-Saura tradition influenced the emergence of the Bauddha deity कालचक्र.\
6. संवर्तकाग्नि-cakra: The fire of the dissolution of the universe --- this is the epithet used for नवात्मन्-bhairava in the Kaula पश्चिमांनाय tradition emerging from the Bhairavasrotas in the मन्त्रमार्ग. Indeed, the foundational सूत्र-s of the पश्चिमांनाय are known as the संवर्तामण्डल-सूत्र-s.\
7. Nara-नारायणेश, Nara-नारायणात्मन्: The Nara-नारायण tradition is very prominent in the महाभारत and appears to be a quasi-humanized ectype of the Indra-विष्णू dyad of the Veda. This dyad, while important in the early नारायणीय पाञ्चरात्र of the महाभारत, faded away in the later वैष्णव tradition. However, its presence here shows that this dyad was still important in the contemporaneous stream of the वैष्णव tradition with which the Ur-SkP interacted (A tradition with connections to the हरिवंश; see below).\
8. व्याघ्र-carma-सुवासस्: The wearer of the tiger-skin robe --- an epithet related to कृत्तिवासस् found in the शतरुद्रीय.\
9. शरभ: While the whole stotra is to शरभ there is little description of him in it beyond a single mention of his name.\
10. नन्दीश्वर-गणेश: The lord of the गण नन्दीश्वर. This गण's association with Rudra goes back to the single mention in the प्रत्यङ्गिरा-सूक्त of the RV Khila (also seen in the AV संहिता-s). He subsequently rises to great prominence in the सैद्धान्तिक tradition. His presence here indicates that this was already presaged in the पाशुपत tradition.

After the नृसिंह cycle, the Ur-SkP moves to the वराह cycle. At the beginning of that cycle, the gods praise विष्णु with the below stotra to urge him to take on the वराह Nandivardhana form which they constitute with their own bodies. The votary who recites it is said to become free of sins and sorrow.

![viShNu_janArdana_rAghapura](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/vishnu_janardana_raghapura.png){width="75%"}

*The विष्णु जनार्दन installed at the शैव temple of विश्वेश्वर at Raghapura, Odisha.*

[नमः सर्व-रिपुघ्नाय दानवान्तकराय च ।]{style="color: #0000ff"}\
[नमो 'जिताय देवाय वैकुण्ठाय महात्मने ॥१५॥]{style="color: #0000ff"}\
[नमो निर्धूत-रजसे नमः सत्याय चैव ह ।]{style="color: #0000ff"}\
[नमः साध्याय देवाय नमो धाम्ने सुवेधसे ॥१६॥]{style="color: #0000ff"}\
[नमो यमाय देवाय जयाय च नमो नमः ।]{style="color: #0000ff"}\
[नमश् चादिति-पुत्राय नर-नारायणाय च ॥१७॥]{style="color: #0000ff"}\
[नमः सुमतये चैव नमश् चैवास्तु विष्णवे ।]{style="color: #0000ff"}\
[नमो वामनरूपाय कृष्ण-द्वैपायनाय च ॥१८॥]{style="color: #0000ff"}\
[नमो रामाय रामाय दत्तात्रेयाय वै नमः ।]{style="color: #0000ff"}\
[नमस्ते नरसिंहाय धात्रे चैव नमो नमः ॥१९॥]{style="color: #0000ff"}\
[नमः शकुनि-हन्त्रे च नमो दामोदराय च ।]{style="color: #0000ff"}\
[सलिले तप्यमानाय नागशय्या-प्रियाय च ॥२०॥]{style="color: #0000ff"}\
[नमः कपिलरूपाय महते पुरुषाय च ।]{style="color: #0000ff"}\
[नमो जीमूतरूपाय महादेव-प्रियाय च ॥२१॥]{style="color: #0000ff"}\
[नमो रुद्रार्धरूपाय तथोमारुपिणे नमः ।]{style="color: #0000ff"}\
[चक्र-मुद्गर-हस्ताय महेश्वर-गणाय च ॥२२॥]{style="color: #0000ff"}\
[शिपिविष्टाय च सदा नमः श्रीवत्सधारिणे ।]{style="color: #0000ff"}\
[धुन्धुमाराय शूराय मधुकैटभघातिने ॥२३॥]{style="color: #0000ff"}\
[चतुर्भुजाय कृष्णाय रत्न-कौस्तुभ-धारिणे ।]{style="color: #0000ff"}\
[त्रिविक्रम-वियत्-स्थाय पीत-वस्त्र-सुवाससे ॥२४॥]{style="color: #0000ff"}\
[नमः पुर-विघाताय गदा-खड्गोग्रधारिणे ।]{style="color: #0000ff"}\
[योगिने यजमानाय भृगुपत्नी-प्रमाथिने ॥२५॥]{style="color: #0000ff"}\
[वृषरूपाय सततं आदित्यानां-वराय च ।]{style="color: #0000ff"}\
[चेकितानाय दान्ताय शौरिणे वृष्णिबन्धवे ॥२६॥]{style="color: #0000ff"}\
[पुराश्वग्रीव-नाशाय तथैवासुर-सूदिने ।]{style="color: #0000ff"}\
[नमस्ते शार्ङ्गधनुषे सौभ-साल्व-विघातिने ॥२७॥]{style="color: #0000ff"}\
[नमस्ते पद्मनाभाय ब्रह्मसत्पथ-दर्शिने ।]{style="color: #0000ff"}\
[नमो जयाय शर्वाय रुद्र-दत्त-वराय च ॥२८॥]{style="color: #0000ff"}\
[नमः सर्वेश्वरायैव नष्ट-धर्म-प्रवर्तिने ।]{style="color: #0000ff"}\
[पुरुषाय वरेण्याय नमस्ते शतबाहवे ।]{style="color: #0000ff"}\
[तव प्रसादात् कृच्छ्रान् वै तरामः पुरुषोत्तम ॥२९॥]{style="color: #0000ff"}

We discuss below some of the notable epithets found in this stotra:\
1. वैकुण्ठ: This distinctive epithet first appears in the महाभारत and is repeatedly used in the early पाञ्चरात्रिक section of that text (parvan 12). There it appears as a name of the god both in विष्णुसहस्रनाम and the 171-epithet early पाञ्चरात्रिक mantra of विष्णु composed by नारद. It also appears in a similar mantra in a stava composed by कश्यप in the हरिवंश. In later iconography, the epithet is usually taken to mean विष्णु चतुरात्मन् with anthropomorphic, leonine, porcine and Kapilan heads. विष्णु is specifically addressed by this name in the Ur-SkP as he prepares to slay हिरण्याक्ष with the cakra (see below).\
2. निर्धूत-rajas: One who has freed himself from the dust. The dust here might be seen as the particulate bonds --- or the आत्मन् bound to the evolutes of प्रकृति.\
3. साध्य deva: In the पुरुष-सूक्त we are enigmatically informed of an ancient class of deities known of the साध्य-s alongside the deva-s --- nothing more is said of the former. They appear episodically in various ब्राह्मण texts and are generally seen as a class of celestial deities. By making विष्णु a साध्य, the stotra expands his domain to include these obscure deities.\
4. Yama deva: Interestingly, like Rudra, विष्णु too is identified with Yama.\
5. Aditi-putra, आदित्यानां-vara: विष्णु membership in the आदित्य class of deities is not just cemented, but he has risen to be the chief of them.\
6. वामनरूप, Trivikrama-viyat-stha, Nara-नारायण, कृष्ण-द्वैपायन, the two राम-s (रामचन्द्र ऐक्ष्वाकव and Rama भार्गव or the संकर्षण is unclear), दत्तात्रेय, नरसिंह, Kapila, शौरिन्, वृष्णिबन्धु, कृष्ण, Saubha-साल्व-विघातिन्: The late दशावतर has not yet crystallized, but the tendency in that direction is clear in the list. We have नरसिंह, वामन, two राम-s, and कृष्ण who figure in the classic lists. वराह is specifically avoided because that incarnation is about to occur in the current narrative. Yet, anachronistically, there is a clear acknowledgment of the सात्त्वत religion with the identification of विष्णु with कृष्ण and various कार्ष्णि/सात्त्वत epithets in the above list. These include the famous act of कृष्ण देवकीपुत्र, i.e., the killing of साल्व and the destruction of his airplane the Saubha. Some other incarnations that are widely accepted, but not in the classic list of 10, are also mentioned such as कृष्ण-द्वैपायन and दत्तात्रेय. This shows that the incarnational model pioneered by the सात्त्वत religion had already been expanded to include a wider range of figures.\
7. शकुनि-हन्तृ: This epithet is peculiar because, at first sight, people take शकुनि to mean the eponymous prince of गन्धार. However, this is not the case because that शकुनि was not killed by विष्णु or कृष्ण. The हरिवंश tells us that:\
[पूतना शकुनी बाल्ये शिशुना स्तनपायिना ।]{style="color: #0000ff"}\
[स्तनपानेप्सुना पीता प्राणैः सह दुरासदा ॥]{style="color: #0000ff"} HV 65.26

also:\
[राक्षसी निहता रौद्रा शकुनी-वेषधारिणी ।]{style="color: #0000ff"}\
[पूतना नाम घोरा सा महाकाया महाबला ।]{style="color: #0000ff"}\
[विष-दिग्धं स्तनं क्षुद्रा प्रयच्छन्ती महात्मने ॥]{style="color: #0000ff"} HV 96.31

Here, the mighty and terrible पूतना, whom कृष्ण slew when he drank her milk as she tried to breast-feed him in his infancy with her poisoned breast, is described as शकुनी and a राक्षसी. Hence, the epithet शकुनि-हन्तृ records this episode. We know from the कौमार tradition that शकुनी, पूतना and रेवती are the names of pediatric avicephalous कौमार goddesses who are invoked for freeing a child from various diseases. Indeed, this identification was known even in the HV in the ancient hymn to the transfunctional goddess, the आर्या-stuti:\
[शकुनी पूतना च त्वं रेवती च सुदारुणा ।]{style="color: #0000ff"} HV ("appendix") 1.8.39

Thus, the पूतना-शकुनी episode represents an example of ancient sectarian competition between the कौमार-s and the सात्त्वत stream of the वैष्णव-s who portray their hero as slaying the demonized कौमार avicephalous goddess and thus expanding into the domain of pediatric apotropaism that belongs to the god Skanda.

![avicephalous_kaumAra](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/avicephalous_kaumara.png){width="75%"}*Avicephalous and therocephalous कौमार goddesses from कुशन age मथुरा*

8. Cakra-mudgara-hasta, शार्ङ्गधनुष्, गदा-खड्गोग्रधारिन्: The principal traditional weapons of विष्णु are all mentioned, but the mudgara (war-hammer) is unusual.\
9. Madhu-कैटभघातिन्, धुन्धुमार, अश्वग्रीव-नाश, भृगुपत्नी-प्रमाथिन्: These epithets concern the ancient Asura/असुरी-s slain by विष्णु. Of these Dhundhu, the son of Madhu, is said to have caused landslides or earthquakes and was killed by the इक्श्वाकु hero कुवलाश्व, the son of बृहदश्व, into whom the tejas of विष्णु had entered (HV, chapter 9). It is possible that this epithet implies that the said king was seen as an incarnation of विष्णु (a parallel to the later इक्ष्वाकु incarnation as राम). In contrast to this more widespread legend, a parallel myth alluded to in the लिङ्गपुरान suggests that विष्णु himself slew the Asura by acquiring the cakra from Rudra. The killing of अश्वग्रीव is alluded to in both the इतिहास-s and the later पुराण-s either connect it with the Pravargya-like tale of the beheading of विष्णु by the rebound of his bow or the Matsya incarnation. The ancestress of our clan, पौलोमी, the wife of भृगु, is said to have been an असुरी or a partisan of the दानव-s. She was killed by विष्णु for aiding them --- this is already known in the रामायण.\
10. रुद्रार्धरूप: An acknowledgment of the Harihara form. The first surviving icons of this form are known from the कुषाण age.\
11. जीमूतरूप: Of the form of a cloud --- this is an unusual name. It likely indicates the expansion of विष्णु into the domain of Parjanya via a specific myth found earlier in the Ur-SkP (chapter 31). There the personified Vedic ritual, यज्ञ, was designated by Brahman to do good to the world. He soon found himself possessing insufficient power to do that. Hence, he performed tapas and pleased Rudra. Rudra granted him the boon of becoming a cloud (जीमूत) and delivering life-giving waters to the world. Before Rudra acquired his bull, यज्ञ as the cloud also became his vehicle -- it is stated by becoming the abode of lightning (which as per the Veda is a manifestation of Rudra -- 11 lightnings of the Yajurveda; also the name अशनि) he carried Rudra on his back. Given the Vedic incantation:  [यज्ञो वै विष्णुः ।]{style="color: #0000ff"}, the cloud is identified with विष्णु.\
12. वृषरूप --- In the हरिवंश, वासुदेव slays a son of Bali named Kakudmin वृषरूप. However, here given that it is the name of विष्णु, it might imply an identification with Rudra's bull, who was his next vehicle after यज्ञ as the cloud.\
13. उमारूपिन्: This is an unusual identification that was to have a long life in the later tradition all the way to the late श्रिकुल system of गोपालसुन्दरी and parallels the coupling of मोहिनी and Rudra or the Harihara iconography.\
14. महादेव-priya, महेश्वर-गण, Rudra-datta-vara: In the Ur-SkP, विष्णु is not outright antagonistically demoted vis-a-vis Rudra. He is instead cast as a mighty god who is, however, second to Rudra. This is made clear by calling him dear to महादेव (or even equating him with उमा: the above epithet), while at the same time subordinating him as a गण of the god and one receiving boons from Rudra.\
15. Salile तप्यमान: This is again a rather peculiar epithet because it applied to Rudra in the Ur-SkP and goes back to the महाभारत where it occurs in the stotra uttered by कृष्ण and Arjuna to Rudra in order to obtain the पासुपत missile. Thus, its application to विष्णु here might indicate his identification with Rudra. This idea of a deity heating the waters as part of the evolutionary process is an idea going back to the Veda. In the context of Rudra, it related to his लिङ्ग form -- i.e., स्थाणु. That said, there are other clear links between विष्णु and the primordial waters -- he is termed नारायण -- typically interpreted as the abode of the waters. Moreover, the same stotra also refers to him as नागशय्या-priya -- i.e., fond of this serpentine bed. Tradition unequivocally places this bed in the midst of the ocean. His हयग्रीव form is also mentioned in the महाभारत as dwelling in the waters consuming oblations (related to the ancient motif of the submarine equine fire). Thus, this epithet could specifically apply to that form.\
In the वराह cycle of this पुराण, this sectarian tension plays out in the battle between the Daitya and विष्णु and the events that follow it. A brief synopsis of it is provided below:

  - The gods formed the boar body for विष्णु with their own bodies. Thus, he advanced against the Daitya-s by diving into the ocean --- an account is given of his encounter with various marine life, like different kinds of whales, sharks, fishes and molluscs. Visiting the various nether realms, he advanced to रसातल where the Asura-s lived.

  - There, a submarine Daitya guard Nala sighted वराह and in fear rushed to inform his lord. But वराह followed him and thus discovered the Asura stronghold.

  - प्रह्लाद informed हिरण्याक्ष that he had a bad dream that someone in a man-boar form might kill him.

  - हिरण्याक्ष says that he too had a dream in which Rudra asked him to surrender to Indra, give up his dominion, and come to dwell near him. The Daitya-s suggested that हिरण्याक्ष not go to battle. Instead, they suggested that they would head out to the battle with Andhaka as their head. Vipracitti suggested that he would go himself to destroy the gods.

  - Nala came in and informed the Asura-s that he had sighted a terrifying boar coming to attack them.

  - प्रह्लाद urged हिरण्याक्ष to take some action as he realized that the boar was none other than विष्णु who has come to destroy them with the aid of his माया.

  - हिरण्याक्ष responded that he wished to avenge his brother's death by killing विष्णु and offering his boar-head as a bali for Rudra.

  - He sent forth his Asura troops to attack वराह. At first, वराह ignored them saying that he was just searching for his wife and the one who had kidnapped her. The Asura-s launched a massive attack on him, and he retaliated by demolishing them.

  - Hearing of their defeat हिरण्याक्ष asked the great Asura generals प्रह्लाद, Andhaka, Vipracitti, Dhundhu, व्यंस and others to get ready to confront वराह.

  - वराह made an anti-clockwise circuit of the Asura stronghold and stormed it via the southern gate. He destroyed the शतघ्नि missiles fired from the gate and also the कालचक्र missiles that were hurled at him. The Asura-s made a great sally at him. He feigned a retreat and drew them out of their fortifications. However, the Asura-s realized his plan and attacked him from the rear. The deva-s in his body were able to detect this attack and oriented him towards the Asura-s attacking him from behind.

  - वराह challenged them to one-on-one duels. Andhaka agreed that it was the right thing to do. However, प्रह्लाद informed them that the vile वराह was none other than the wicked विष्णु using his माया because he was afraid to fight them with his own form. Then प्रह्लाद showered astra-s on him and asked the other Asura-s to join him in a combined attack.

  - वराह then smashed प्रह्लाद's chariot and hammered him with his own standard on his head. The daitya retaliated with his mace, but it had no effect on वराह.

  - He then attacked fought Andhaka and Vipracitti in a great battle. In the end, he carried both like Garutmat carrying the elephant and the tortoise and hurled them down like bolts of lightning.

  - He then destroyed and slew the divisions of the remaining Asura-s.

  - Vipracitti returned to the battle having rearmed himself, but after a strong fight वराह whirled him around and sent him crashing into the fortress of हिरण्याक्ष.

  - हिरण्याक्ष alarmed by the noise went to check things out and found his general unconscious. After reviving him, the Daitya emperor asked him who could possibly defeat him. Vipracitti then told him that it was the invincible वराह and perhaps it was similar to नृसिंह who had earlier crushed them. He suggested that the Asura-s should abandon their stronghold and flee.

  - Disregarding Vipracitti, हिरण्याक्ष set out for battle himself. He is said to have been of the complexion of a heap of collyrium but with a blond beard and four fangs.

  - His advance is described using two astronomical allegories: He is said to be like a great comet and Vipracitti who accompanies him is like a reflection of that comet. He is also described as being like the Sun, with प्रह्लाद, Andhaka and others surrounding him like the planets --- an interesting heliocentric simile.

  - वराह scattered the other Daitya-s and दानव-s and rushed at हिरण्याक्ष, who, however, paralyzed him by piercing his joints with his arrows. The deva-s removed those arrows with magical incantations, and वराह resumed the attack. This time he came close to striking the Asura's car, but the Daitya's charioteer steered it away, and हिरण्याक्ष bound वराह with the नागास्त्र.

  - Then the Asura-s massed around him and tried to chop him up with their weapons. However, गरुड came to his aid and released him from the नागास्त्र. Thus revived, he smashed the Daitya-s and resumed his attack on हिरण्याक्ष.

  - The Daitya then pierced him in the heart with an astra causing him to faint. On regaining consciousness, he called on the deva-s to reinforce him, and they filled him with their tapas. Thus, he shone like seven suns, resembling Rudra preparing to destroy the worlds.

  - वराह then displayed several मायाविन् tactics and overcoming the नागास्त्र-s of the Asura king destroyed his chariot. He continued fighting on foot and struck वराह with the मोहनास्त्र, which stunned the boar. The deva-s in his body countered it, and वराह returned to the battle.

  - वराह uprooted a tree (axial mytheme) and struck the Asura lord on his head. The latter fell unconscious, and his bow with five arrows slipped from his hand. The other daitya-s and दानव-s wailed thinking he was dead and fell upon the boar with their weapons. वराह simply swallowed all those weapons.

  - As वराह was engaged with the daitya-s, हिरण्याक्ष recovered, and uttering the mantra [रुद्राय वै नमः ।]{style="color: #0000ff"}, he hurled a mighty spear at his enemy. वराह was struck in the heart by that and fell down as if dead.

  - The sun then lost its luster, and the planets were on collision course. Brahman at that point invoked Rudra. वराह rose up again, and the tejas of Rudra entered him. Pulling out the spear stuck in him, blazing like a thousand fires, he pierced हिरण्याक्ष like Skanda striking क्रौञ्च. However, the Asura was unfazed by that blow.

  - The Daitya returned the blow with his sword, but वराह felt no pain and struck the sword away with the back of his palm.

  - Then the two engaged in a prolonged wrestling bout at the end of which an incorporeal voice told वराह that he can kill the Daitya only with Rudra's cakra.

  - Invoking Rudra and calling the cakra that was born of the "waters" (an oblique reference to the जलंधर episode, where Rudra killed the Asura using a cakra that he drew from water: the whirlpool mytheme), वराह assumed a gigantic form pervading the triple-world.

  - हिरण्याक्ष fought him with various astra-s and displays of माया, but वराह destroyed all of them with the cakra and finally cut off the Daitya's giant head.

  - वराह then searched for पृथिवी by destroying the parks and tanks of the Asura-s and uprooting mountains. Going south he uprooted शङ्ख mountain and found her bound there, and guarded by दानव-s. He hurled the शङ्ख mountain and slew the दानव-s and drove away the नाग-s. He then seized the jewels of the Asura-s.

![varAha_pRithivI](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/varaha_prithivi.jpeg){width="75%"}

*पृथिवी clinging to वराह's tusk from Gupta age Udayagiri.*

  - He carried पृथिवी clinging to his tusk even as Brahman had carried the former earth वसुधा when he had assumed a boar form (It is notable that the शैव-s revived the memory of this old Vedic narrative of प्रजापति's boar form probably to obliquely indicate that विष्णु's वराह form was only second to that of Brahman).

  - He then handed over the triple-world to Indra and reaffirmed their eternal friendship.

  - वराह indicated that he wished to enjoy the pleasures of his boar form in fullness. Thousands of Apsaras-es become sows to consort with him even as the ब्राह्मण-s lauded him with their hymns.

  - Mating with his wife in the form of the sow चित्रलेखा he birthed a lupine son known as वृक (Temples of वराह as the father of the lupine वृक -- कोकमुखस्वामिन् -- seem to have been there in Nepal and from there transmitted to Bengal).

  - वृक roved around the world with his pack eating various animals. Finally, he arrived at the forest of Skanda at गौरीकूट with medicinal plants, minerals and gems. At that time Skanda was away visiting the Mandara mountain and had deputed his avicephalous or therocephalous गण Kokavaktra (himself with a lupine head or with a cuckoo or waterfowl head) to guard his forest.

  - वृक ravaged कुमार's forest. At first Kokavaktra tried to be good to him and told him that he was happy with his power. Kokavaktra asked वृक to stop and told him that he would repair the damage and put in a word with Skanda to make him a गण.

  - वृक refused and attacked the Skandapārṣada. After a fight, Kokavaktra knocked down वृक and bound him with पाश-s (A rare reflex of the Germanic Fenris wolf motif in the Hindu world).

  - When Skanda returned he sentenced वृक to be subject to नरकत्रास-s by his गण-s.

  - नारद informed वराह about what had happened and told him that due to his childish arrogance, Skanda does not bow before the great god and has bound his lupine son.

  - Infuriated, वराह proceeded to fight Skanda. Skanda and his गण-s neutralize the cakra and other weapons of वराह. Finally, Guha pierced वराह's heart with his संवर्तिका spear. विष्णु at that point abandoned his वराह body and resumed his usual form.

![skanda_gupta](https://manasataramgini.wordpress.com/wp-content/uploads/2022/12/skanda_gupta.jpg){width="75%"}

Skanda wearing the tusks of वराह on his necklace, Gupta age.

  - विष्णु then praised Rudra who conferred a boon to him. विष्णु asked him to teach him the पाशुपत-vrata. Rudra mounted his bull and went to Sumeru to teach विष्णु the said vrata.

Here we see a three-way competition between शैव-s, वैष्णव-s and कौमार-s. The normally accommodating relationships between the कौमार-s and वैष्णव-s (barring some conflicts as the पूतना case alluded to above), seem to have broken down probably under शैव influence. The incident of the defeat of वराह by कुमार is seen in both the South Indian Skandaपुराण and the Ur-SkP, suggesting it was there in an ancestral SkP. It has some Gupta-पुष्यभूति age iconographic representation in the form of Skanda wearing the tusks of the boar in his necklace. However, in this text, the शैव-s trump both of them with the final flourish of विष्णु ultimately asking Rudra to teach him the पाशुपत-vrata. We believe that the वराह episode in the Ur-SkP is a genuine early version of this famous mytheme, but it was strategically tweaked at certain points by the शैव-s to downgrade विष्णु and exalt Rudra.

