
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कौमार cycle in the Skandaपुराण's शंकर-संहिता](https://manasataramgini.wordpress.com/2022/07/24/the-kaumara-cycle-in-the-skandapura%e1%b9%87as-sa%e1%b9%83kara-sa%e1%b9%83hita/){rel="bookmark"} {#the-कमर-cycle-in-the-skandaपरणs-शकर-सहत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 24, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/07/24/the-kaumara-cycle-in-the-skandapura%e1%b9%87as-sa%e1%b9%83kara-sa%e1%b9%83hita/ "5:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many खण्ड-s, माहात्म्य-s and संहिता-s attach themselves to the sprawling "Mega-Skandaपुराण". We use this term to distinguish it from the "Ur-Skandaपुराण", which was first published by भट्टाराइ in the late 1980s and is now known to survive as three related recensions, one of which is represented by rather early manuscripts from Nepal. Of the texts associated with the "Mega-Skandaपुराण", the शंकर-संहिता, remains relatively poorly known. It is unclear if there was a pan-Indian understanding of its constituent texts and if a complete version was ever extant in any part of the Indosphere. As far as we can tell, one of its खण्द-s known as the शिवरहस्य is preserved only in South India and is likely of South Indian origin. It was most likely composed in the द्राविड country; though one cannot entirely rule out the Southern Andhra country or parts of Southern कर्णाट as its original source. It was edited by a maternal श्रौत-ritualist- and पौराणिक-clansman of ours in the 1950s-1960s. Upon completing its editing, he offered it to the shrine of Skanda housing the कुलदेवता of our clan. The text as available still has some corruptions, several of which might have been introduced while typesetting. The शिवरहस्य presents its relationship to the Mega-Skandaपुराण thus:

[तेष्व् अपि+इदम् मुनि-श्रेष्ठाः स्कान्दं सुखदम् उत्तमम् ।]{style="color:#0000ff;"}\
[सर्व-वेदान्त-सारस्वं पञ्चाशत् खण्डमण्डितम् ॥]{style="color:#0000ff;"}\
[आद्या सनत्कुमारीया द्वितीया सूत-संहिता ।]{style="color:#0000ff;"}\
[ब्राह्मी तु संहिता पश्चात् तुरीया वैष्णवी मता ॥]{style="color:#0000ff;"}\
[पञ्चमी शांकरी-ज्ञेया सौरी षष्ठी तु संहिता ।]{style="color:#0000ff;"}\
[आद्या तु पञ्च-पञ्चाशत् सहस्रैः श्लोककैर् युता ॥]{style="color:#0000ff;"}\
[द्वितीया संहिता विप्राः षट्सहस्रैर् अलंकृता ।]{style="color:#0000ff;"}\
[त्रिसाहस्रैर् युता ब्राह्मी पञ्चभिर् वैष्णवी-युता ॥]{style="color:#0000ff;"}\
[त्रिंशत्भिः शांकरीयुक्ता खण्डैर् द्वादशभिस् तथा ।]{style="color:#0000ff;"}\
[षष्ठी तु सौरी संयुक्ता सहस्रेणैक केनसा ॥]{style="color:#0000ff;"}\
[ग्रन्थ-लक्षैर् युतं स्कान्दं पञ्चाशत् खण्ड-मण्डितम् ।]{style="color:#0000ff;"}\
[तद् अद्य सम्प्रवक्ष्यामि युष्मभ्यं विप्र-पुंगवाः ॥]{style="color:#0000ff;"}\
[तत् त्रया संहिता प्रोक्ता शांकरी वेद-सम्मता ।]{style="color:#0000ff;"}\
[त्रिंशत् सहस्रैर् ग्रन्थानां विस्तरेण सुविस्तृता ॥]{style="color:#0000ff;"}\
[आदौ शिव-रहस्याख्यं खण्डम् अद्य वदामि वः ।]{style="color:#0000ff;"}\
[तत् त्रयोदश-साहस्रैः सप्तकाण्डैर् अलंकृतम् ॥]{style="color:#0000ff;"}

The Mega-Skandaपुराण is divided into 6 संहिता-s that have a total of 50 खण्ड-s among them. These are listed as follows with their corresponding verse counts: 1. सनत्कुमार: 55,000; 2. सूत: 6000; 3. ब्राह्मी: 3000; 4. वैष्णवी: 5000; 5. शांकरी: 30,000; सौरी: 1000. Thus, the entire text is said to be of 100,000 verses. Within it, the शंकर-संहिता (शांकरी) is said to have 12 खण्द-s of which the शिवरहस्य of 13,000 verses is one. The शिवरहस्य itself is divided into 7 काण्ड-s, which are: 1. Sambhava; 2. आसुर; 3. माहेन्द्र; 4. Yuddha; 5. Deva; 6. दक्ष; 7. उपदेश.

The published Mega-Skandaपुराण does not align precisely with this tradition and has 7 खण्ड-s: 1. माहेश्वर; 2. वैष्णव; 3. Brahma; 4. काशी; 5. Avanti; 6. नागर; 7. प्रभास. The माहेश्वर-खण्ड in this compendium is not the same as the शांकरी सम्हिता under consideration in this discussion. However, they share many common themes that include the central thread gathered around the destruction of दक्ष's sacrifice, the marriage of पार्वती and Rudra, the birth of कुमार and the killing of तारक by him, the birth of गणेश, the शिवरात्रि ritual and the worship of Rudra at अरुणाचल. The tale of Skanda and the तारक war is repeated twice in the माहेश्वर-खण्ड of the Mega-Skandaपुराण.

The first 5 काण्ड-s and parts of 6 and 7 of the शिवरहस्य in the शांकरी सम्हिता comprise a narration of the कौमार cycle partly modeled after the रामायण of वाल्मीकि. Much of the काण्ड-s 6 and 7 are primarily शैव material relating to the observation of vrata-s and शिव-dharma --- these thematically overlap with the material in the माहेश्वर-खण्ड of the Mega-Skandaपुराण. The कौमार portions of the शिवरहस्य were rendered in Tamil by the सैद्धान्तिक guru काश्यपशिव in the medieval period as the Tamil Skandaपुराण. His version has some differences from the extant Sanskrit text of the शिवरहस्य --- it is unclear if these differences are due to his reformulation of the narrative or because he was using a distinct recension of the text. A Telugu rendering of the text also exists but we do not have much familiarity with it. While the ancient versions of the कौमार cycle have the killing of the दानव/daitya महिष or तारक by the god Skanda as their centerpiece (रामायण and महाभारत), this text presents an unusual version of it: after the initial section culminating in तारक's killing, there are two extended sections dealing with the elder brothers of तारक. These culminate in the great battle in which Skanda slays these demons, Siṃhamukha and शूरपद्म, along with their vast horde of Asura-s. So far, we have not seen any record of these demons outside of South India. Long before काश्यपशिव's Tamil rendering, शूरपद्म appears in the South Indian tradition as represented by the earliest surviving Tamil texts, such as the पुऱनानूऱु (पुऱनानूऱु 23, a poem probably roughly contemporaneous with the कुषाण age in the North given that it describes the early पाण्ड्य king नेटुञ्चेऴियऩ्), and a subsequent Tamil poetic anthology, the परिपाटल्. This suggests that the South Indian tradition had a deep history of certain unique elements of कौमार mythology.

As far as archaeology goes, we know that there was an active कौमार tradition in the Andhra country starting from the Andhra empire down to their smaller successor states, such as the इक्ष्वाकु-s and विष्णुकुण्डिन्-s among others, which had नागार्जुनकोण्ड, as one of its foci. In the Tamil country, clear-cut archaeological evidence for strong कौमार traditions can be seen from the Pallava period onward. We believe this temporal period stretching from the Andhra empire down to the rise of the Pallava-s overlaps with the period during which the पुऱनानूऱु and the later परिपाटल् were composed in the Tamil country. The परिपाटल् displays a distinctive combination of the worship of विष्णु with his पाञ्चरात्रिक व्यूह-s and कुमार --- this pattern is seen in the Northwest, i.e., Panjab/Gandhara, and in Mathura during the शक-कुषाण age. This was mirrored in the South Indian Maturai (approximately the same longitude as its Northern namesake Mathura), the cultic locus of the परिपाटल्. Thus, one could argue that the core कौमार tradition in the Tamil country was a transmission of this Mathuran tradition.

Apart from the references to शूरपद्म, the themes in the परिपाटल्, while clearly linked to the ancient कौमार narratives, such as those seen in the महाभारत, show certain unique archaisms which have not survived in the Sanskrit tradition. For example, in परिपाटल्-5 by कडुवन् इऌअवेयिननार् we encounter an incorporation of the पौराणिक Marut mytheme into the tale of the birth of कुमार. Here, after a prolonged dalliance with Rudra, mirroring the Sanskrit sources, पार्वती becomes pregnant with कुमार. Then Indra, who had acquired a boon from Rudra, cut the developing embryo into pieces with his Vajra (the number seven is implied by the repeated mention of seven in this verse) --- the पौराणिक Marut-motif. Then the pieces were placed in the three ritual fires by the seven ऋषि-s (allegorically identified in the text with the seven brightest stars of Ursa Major), who realized that they would form the future commander of the deva-s. The pieces were purified by Agni and placed in the wombs of six of the wives (कृत्तिका-s=Pleiades), barring अरुन्धती, of the seven ऋषि-s (c.f. archaic महाभारत version). Thus, this South Indian tradition preserves a memory of the connection between the Vedic Marut-s, who are the sons of Rudra, and Skanda that was largely forgotten elsewhere (except for the reference to कुमार as leader of the seven Marut troops in the oldest version of the cycle in the महाभारत).

When we take the whole कौमार corpus, we have reason to suspect that the ancient version of the tradition was much richer and more polymorphic than what is seen in the later Sanskrit tradition. As a parallel, we could point to the Aindra mythology. The Veda alludes to many mythemes that were clearly common knowledge when the ऋग्वेद was originally composed. Further, the epics point to a degree of para-Vedic polymorphism in the Aindra tradition. However, what survived of that tradition in the extant पौराणिक corpus is relatively limited. Likewise, with the कौमार tradition, we see that the महाभारत preserves a rich mythology, which included the triumphs of the god over महिष, तारक, and hints at an even richer body of myth by mentioning in passing the overthrow of several other demons (e.g., त्रिपाद and Hradodhara) by Skanda. By the time of the composition of the extant पौराणिक corpus, the कौमार myth of महिष was mostly forgotten, surviving only in the वामन-पुराण. The महिष myth was instead transferred to कुमारी (विन्ध्यवासिनी section of Ur-Skandaपुराण). She originally started off as the virgin goddess, a female counterpart of कुमार, and was subsequently subsumed under the great transfunctional goddess, the शक्ति of Rudra. Thereafter, कुमार was only left with the तारक myth across much of the Sanskrit tradition. Hence, posit that at the zenith of the कौमार tradition there was a considerably larger and more polymorphic body of कौमार material. The vitality of this old कौमार tradition is seen in Mathura --- based on the remains of images, we infer the existence of at least 33 कौमार shrines in Mathura during the कुषाण age. Thus, we propose that some of this original polymorphism in the tradition was preserved in the transmission to South India, even as the tradition in the Tamil country remained relatively isolated from the later transmissions from the North (e.g., the transmission of the Eastern कौमार Lodge from वङ्ग to Bellary in कर्णाट). Hence, we posit that the special emphasis on शूरपद्म was a remnant of this old transmission that did not make it into other पौरणिक transmissions.

Some of those mythic elements strongly persisted in the Tamil country and found their way into the शिवरहस्य narrative, which the evidence presented below indicates is a later text:

 1.  In the शिवरहस्य, the गणेश्वर Nandin is prominent. Our textual analysis (to be presented later) has revealed that this is a strong marker of a text influenced by the सैद्धान्तिक शैव tradition. There are several other allusions throughout the text that point to its affiliation with the सैद्धान्तिक rather than any other शैव school of the मन्त्रमार्ग or the अतिमार्ग. This would also explain why the सैद्धान्तिक काश्यपशिव chose to render it Tamil. Whereas in North India (outside of Nepal) and वञ्ग, the rise of the Siddhānta resulted in considerable erosion of the कौमार tradition from the 700s of CE, in the द्राविड country, the strong कौमार tradition was co-opted and incorporated within a सैद्धान्तिक framework. For example, this is seen in the works of the great polymath अघोरशिव-देशिक, who in addition to his numerous सैद्धान्तिक treatises also composed a work on the स्थापना of कौमार shrines. This places the शिवरहस्य in a distinct stratum from the परिपाटल् era (and even perhaps the तिरुमुरुकार्रुप्पटै period) when Siddhānta was dominant in the Tamil country.

 2.  Its narration of the birth of कुमार omits the coitus of Rudra and पार्वती, which indicates a "sanitization" of the sexual elements of that narrative, which, for example, are an important aspect of its presentation in the रामायण, महाभारत, शिवपुराण and कालिदास's कुमारसंभव. This change in attitude again points to a relatively late date for शिवरहस्य.

 3.  None of the early narrations of the कौमार cycle in the इइतिहास-s or the पुराण-s attempt to model themselves after the रामायण. In fact, the कुमाराख्यान was seen as one of those old, independent mythic motifs of Hindu tradition that formed the basis of numerous retellings by different narrators, even as it was with the रामायण. Thus, the modeling of parts of the शिवरहस्य, namely those concerning the war with शुरपद्म and his clan (and possibly the arrangement in seven काण्ड-s), after the रामायण betrays a late "reconstruction" following the loss of continuity with the old कौमार पौराणिक tradition.

 4.  The text acknowledges an already large Skandaपुराण of the size of 100,000 verses. This implies that it comes from a period when the accretion of texts to form a mega-Skandaपुराण was common knowledge.

While these elements point to a relatively late date for the शिवरहस्य, we should point out that like all पौराणिक corpora it does preserve several notable elements that have ancient roots going back to the Indo-European past. While the काण्ड-s 6 and 7 are dominated by the शैव material, its core is primarily a कौमार text intent on the aggrandizement of Skanda. Beyond the distinctive form of the कौमार cycle, there are multiple elements that indicate a southern locus for its immediate origin:

 1.  It presents a prominent role for the god शास्तृ or आर्य as Hariharaputra. This transmogrified southern ectype of Revanta (commonly seen as Hariharaputra) was prominently worshiped at least since the time of the composition of the famous Tamil epic शिल्पाधिकार.

 2.  It presents विनायक as elder to Skanda. While this is the position adopted by the text, its core कौमार narrative of the conquest of the demons still clearly indicates a tradition where गणेश was not yet born/in place.

 3.  The text describes two marriages of Skanda --- one to देवसेना, seen across the Indosphere, and the other to Valli (related to the Dravidian term for tubers such as the tapioca and the sweet potato), that emerged in the Southern folk traditions and spread through the Southern zone of influence in the Indosphere.

 4.  The presence of the कावेरी-Agastya myth, which specifically points to the द्राविड country.

 5.  The staging ground of कुमार in course of his campaign is called शेन्तीपुर, which in the Tamil version of काश्यपशिव is identified as तिरुचेञ्दूरु, a major कौमार center, in the द्राविड country. It is already mentioned as a shrine of Skanda by the sea with a beautiful beach in पुऱनानूऱु 55.

 6.  The shrine of अरुणाचल in the द्राविड country is praised as an important शैव-क्षेत्र. Several other shrines in the द्राविड country as mentioned throughout the text, e.g., the त्यागराज and the मध्यार्जुन shrines.

With this background, we shall briefly examine the contents of the शिवरहस्य and a few of its notable points:

 1.  The Sambhava काण्ड\
This section opens with a मङ्गलाचरण seeking succor from Rudra, उमा, and their sons:\
[मङ्गलं दिशतु मे विनायको मङ्गलं दिशतु मे षडाननः ।]{style="color:#0000ff;"}\
[मङ्गलं दिशतु मे महेश्वरी मङ्गलं दिशतु मे महेश्वरः ॥]{style="color:#0000ff;"}

This is followed by short stotra-s with invocations of गणेश and Skanda by a set of 16 names each.\
गणेश:\
[ओम्कार-निलयं देवं गजवक्त्रं चतुर्भुजम् ।]{style="color:#0000ff;"}\
[पिचण्डिलम् अहं वन्दे सर्वविघ्नोपशान्तये ॥]{style="color:#0000ff;"}\
[सुमुखश् चैकदन्तश् च कपिलो गजकरणकः ।]{style="color:#0000ff;"}\
[लम्बोदरश् च विकटो विघ्नराजो विनायकः ॥]{style="color:#0000ff;"}\
[धूमकेतुर् गणाध्यक्षः फालचन्द्रो गजाननः ।]{style="color:#0000ff;"}\
[वक्रतुण्डः शूर्पकर्णो हेरम्बः स्कन्दपूर्वजः ॥]{style="color:#0000ff;"}

Skanda:\
[सुब्रह्मण्यम् प्रणम्याहं सर्वज्ञं सर्वगं सदा ॥]{style="color:#0000ff;"}\
[अभीप्सितार्थ सिद्ध्य् अर्थं प्रवक्ष्ये नाम षोडश ।]{style="color:#0000ff;"}\
[prathamo ज्ञानशक्त्यात्मा द्वितीयः skanda eva ca ॥]{style="color:#0000ff;"}\
[अग्निभूश् च तृतीयः स्यात् बाहुलेयश् चतुर्थकः ।]{style="color:#0000ff;"}\
[गाङ्गेयः पञ्चमो विद्यात् षष्ठः शरवनोद्भवः ॥]{style="color:#0000ff;"}\
[सप्तमः कार्त्तिकेयः स्यात् कुमारः स्याद् अथाष्टकः ।]{style="color:#0000ff;"}\
[नवमः षण्मुखश् चैव दशमः कुक्कुट-ध्वजः ॥]{style="color:#0000ff;"}\
[एकादशः शक्तिधरो गुहो द्वादश एव च ।]{style="color:#0000ff;"}\
[त्रयोदशो ब्रह्मचारी षाण्मातुर्श् चतुर्दशः ॥]{style="color:#0000ff;"}\
[क्रौञ्चभित् पञ्चदशकः षोडशः शिखिवाहनः ।]{style="color:#0000ff;"}\
[एतत् षोडश नामानि जपेत् संयक् सदादरम् ॥]{style="color:#0000ff;"}

These stotra-s are popular in South India in गणेश- and Skanda-पूजा-s. However, it is notable that the names of Skanda do not mention शूरपद्म or Siṃhamukha; instead, they only utilize the pan-Indospheric कौमार material.

This is followed by the following topics:

  - An account of the origin of the पुराण as narrated by the सूत, the student of व्यास, to the ब्राह्मण-s at नैमिशारण्य and the nature of the Skandaपुराण.

  - An account of कैलास the abode of Rudra. This is followed the by usual शैव cycle of पार्वती and her marriage that includes the below events.

  - काम approaches Rudra who is in meditation.

  - The incineration of काम by the fire from Rudra's third eye.

  - The lament of Rati.

  - Rudra tests पार्वती by appearing to her as an old man.

  - Rudra reveals his true form to पार्वती.

  - Rudra sends the seven ऋषि-s/stars of Ursa Major as his emissaries to seek the hand of पार्वती in marriage.

  - The construction of the marriage hall.

  - The makeup and jewelry of पार्वती.

  - The गणेश्वर Nandin leads the gods to the marriage of Rudra and पार्वती.

  - The names of the Rudra-s and an account of their vast hordes in the marriage procession. This is followed by an account of the retinue of Rudra. Below is a notable section of this text:\
सहस्राणां सहस्राणि ye रुद्राः पृथिवीषदः ।\
[सहस्र-योजने लक्ष्य-भेदिनः सशरासनाः ॥]{style="color:#0000ff;"}\
[ते रुद्रास् त्रिदश-श्रेष्ठास् त्रिनेत्रं संसिषेविरे ।]{style="color:#0000ff;"}\
[अस्मिन् महति सिन्धौ ये ये 'न्तरिक्षे दिवि-स्थिताः ॥]{style="color:#0000ff;"}\
[नीलग्रीवास् त्रिनेत्रास् ते 'संख्याताश् चापुरीश्वरम् ।]{style="color:#0000ff;"}\
[अघः क्षमाचराश् चान्ये सर्वे ते नीलकन्धराः ॥]{style="color:#0000ff;"}\
[गिरीशयो 'स्तु कल्याणं सिषेविषव आपिरे ।]{style="color:#0000ff;"}\
[वृक्षेषु पिञ्जरा रुद्राः नीलकण्ठा विलोहिताः ॥]{style="color:#0000ff;"}\
[भूतानां चाधिपतयो विकेशाश् च जटधराः ।]{style="color:#0000ff;"}\
[सहस्रैर् अप्य् असंख्याताः सायुधाः प्रापुरीश्वरम् ॥]{style="color:#0000ff;"}\
[अन्नेषु ये विविध्यन्ति जनान् पात्रेषु भुञ्जतः ।]{style="color:#0000ff;"}\
[ये पथां पथि रक्षन्ति तीर्थानि प्रचरन्ति च ॥]{style="color:#0000ff;"}\
[ये रुद्रा दिक्षु भूयांसस् तिष्ठन्ति सततं च ते ।]{style="color:#0000ff;"}\
[गौरी-कल्यान-सेवायै गिरिशं समुपाश्रयन् ॥]{style="color:#0000ff;"}

Here the account of the hordes of Rudra is adapted from that of the great multitude of Rudra-s provided in the final अनुवाक (11) of the Yajurvedic शतरुद्रीय. Apart from these, a great retinue of goddesses and natural phenomena is said to accompany Rudra on his marriage procession. The bluish violet विष्णु is said to have joined them with his four forms, i.e., पाञ्चरात्रिक व्यूह-s, and was introduced by Nandin.

  - Rudra enters the marriage hall and the marriage is concluded.

  - Brahman and the other gods send वायु as their messenger to urge Rudra to produce a son with पार्वती. However, Nandin turned him back asking him not the break the marital privacy of the deities.

  - All the gods went to कैलास themselves and beseeched Rudra, whose half was occupied by अम्बिका, to produce the promised son who would relieve them from the Asura-s.

  - Rudra assumed a six-headed form blazing like a crore suns and enveloped the realms of the universe terrifying all beings. Then, from the third eye of each of his six heads, the upward seminal flow (ऊर्ध्वरेतस्) exploded as six flashes of intense light that vaporized the directions (दुद्रवः sarvato दिशः). Terrified by this manifestation, all sought refuge in Rudra, praising him with hymns.

  - He gathered back those six blazes and they came together as six pacified minute sparks. He then instructed वायु and Agni to take them to the arrow-reed forest on the banks of the गङ्गा and vanished along with अम्बिका. Thereafter, वायु and Agni, each getting tired after a while, with much effort bore the sparks to the गङ्गा and deposited them there. The other gods eager to see what would happen also arrived there.

  - The Marut-s with their joyful selves filled the quarters with a pleasant breeze ([दिशः प्रसेदुर् मरुतो ववुश् च सुखमात्मनां]{style="color:#0000ff;"}). Then, in the midst of a lotus in the arrow-reed forest, a six-headed, twelve-armed, two-footed divine boy took shape (note recurrence of the ancient motif of the birth of Agni in the lotus: ऋग्वेद 6.16.13).

  - विष्णु called upon the six कृत्तिका-s to nurse him. Instantaneously, becoming six separate kids he drank from their breasts.

  - Even as the six flashes from Rudra were vaporizing the directions, पार्वती too was startled and jumped away. As a consequence, the anklet fell from her feet and broke spilling the gems within it. अम्बिका was reflected on those nine gems and appeared tenfold --- herself and the 9 reflections. These became the कालिका goddesses, who were fertilized by the rays emanating from Rudra and became pregnant.

  - The droplets of the sweat of the startled goddess were also fertilized by Rudra. From them were born a 100,000 fierce गण-s (who became the retinue of Skanda).

  - अम्बिका was displeased by seeing these goddesses pregnant and cursed them that they would have an unending and painful pregnancy. They went to Rudra seeking his aid and upon his counseling अम्बिका released them from her curse and each gave birth to a mighty son of the complexion of their respective mothers.

  - Goddesses and the corresponding sons were: रक्ता (ruby) --- वीरबाहु; तरला (pearl) --- वीरकेसरिन्; पौषी (topaz) --- वीरमहेन्द्र; गोमेदा (garnet) --- वीरमहेश्वर; वैडूर्या (beryl) --- वीरपुरंदर; वज्रमणि (diamond) --- वीरराक्षस; मरकता (emerald) --- वीरमार्ताण्ड; प्रवाला (coral) --- वीरान्तक; इन्द्रनीला (sapphire) --- वीरधीर. These nine वीर-s became the companions of Skanda and were known as his brothers.

  - Then Rudra told अम्बिका that they have actually generated a mighty son and asked her to come along on his bull vehicle to see him.

  - They set out with thousands upon thousands of रुद्रकन्या-s, मातृ-s, गण-s and the Marut-s.

  - Then उमा hugged the six separate kids who became a single षण्मुख and fed him with her milk.

The narrative of the birth of कुमार up to this point presents several interesting points:\
1. There is a prominent role for वायु along with the usual Agni in the birth of Skanda. We believe that this is the survival of an ancient motif that is already seen in the Veda, where on rare occasions, apart from the usual Rudra, वायु is presented as the father of the Marut-s. This is not a mere slip, because in the Indo-Iranian world we see an overlap in the Rudra- and वायु class deities. On the Indian side that goes back to the worship of Rudra in the context of the rites of the Proto-शैव-s, the व्रात्य-s, and in the Eastern Iranian world in the character of the deity वायु Uparikairya, who is iconographically identical to the Hindu Rudra.\
2. We see the subliminal presence of the Marut-s, even in this late reflex of the कौमार origin myth suggesting a long survival of this memory in the circles conversant with the Veda.\
3. A variant of the "fertilizing sweat motif" attested in this myth presents the origin of the 100,000 Skanda-गण-s from the sweat of गौरी.\
4. The Nava-वीर-s are a unique feature of the South India कौमार cycle. However, the number nine is also mentioned as the total of the कौमार-वीर-s even in one of the most ancient surviving variants of the कौमार cycle, which is seen in the महाभारत:\
[काकी च हलिमा चैव रुद्राथ बृहली तथा ।]{style="color:#0000ff;"}\
[आर्या पलाला वै मित्रा सप्तैताः शिशुमातरः ॥]{style="color:#0000ff;"}\
[एतासां वीर्य-संपन्नः शिशुर् नामातिदारुणः ।]{style="color:#0000ff;"}\
[स्कन्दप्रसादजः पुत्रो लोहिताक्षो भयंकरः ॥]{style="color:#0000ff;"}\
[एष वीराष्टकः प्रोक्तः स्कन्दमातृगणोद्भवः ।]{style="color:#0000ff;"}\
[छाग-वक्त्रेण सहितो नवकः परिकीर्त्यते ॥]{style="color:#0000ff;"}\
This account in the Mbh states that by the grace of Skanda, the 7 goddesses (Skandammātṛ-s), i.e., काकी, etc., gave birth to the terrifying red-eyed deity शिशु, who was called the eighth वीर. However, when नेजमेष = भद्रशाख with the head of a ram, generated by Agni is taken into account, शिशु is said to be the ninth वीर. Then the question arises as to who were the remaining seven? From the preceding account in the Mbh we can infer that these were विशाख and other कुमारक-s who were emanated by Skanda when struck by Indra's vajra. We believe that these वीर-s were ectypes of the Marut-s filtering down through later mythic overlays. It also appears likely that in the शिवरहस्य, the most prominent of the nine वीर-s, वीरबाहु, is essentially an ectype of विशाख as the younger brother of Skanda. This connection to one of the oldest surviving versions of the कौमार cycle suggests that this aspect of the Southern tradition was a memory coming down from its ancient layer originally brought from the North.

  - Thereafter Skanda displayed his childhood लीला-s, some of which bring out his roguish (धूर्त) aspects that are known from the oldest layers of the कौमार tradition. One notable set of such cosmic sports is expressed in beautiful मन्दाक्रान्ता verses:\
[ज्योतिश् चक्रं ध्रुव-कर-गतं वात-रश्मि-प्रणद्धं]{style="color:#0000ff;"}\
[छित्वा बालः प्रथित-महिमा स्वानुगानां कराग्रैः ।]{style="color:#0000ff;"}\
[दिक्ष्व् अष्टासु स्वयम् अपि दधन् धारयन् व्योम-गङ्गा]{style="color:#0000ff;"}\
[नक्रान् बध्वा व्यसृजद् अभिसंभाव्य-पाथः पुनस्तान् ॥]{style="color:#0000ff;"}\
The boy putting forth his greatness, having taken in his hands the self-moving ones (planets) split the reins of propellant force (wind ropes) which bind them to the polar rays around which the celestial wheel revolves. Giving to himself the eight quarters, he then took on the Celestial गङ्गा (Milky Way); binding the crocodiles (the constellation of Scorpius) he released them again into the Sun.\
[पश्चाद् ऊर्ध्वम् महर् अपि जनस् तत् तपः सत्य-लोकं]{style="color:#0000ff;"}\
[गत्वा गत्वा तद् अधि वसतीन् विश्व-साध्यामरेन्द्रान् ।]{style="color:#0000ff;"}\
[लीला-लोलो नव च कलयन् वेधसो भीमभूतान्]{style="color:#0000ff;"}\
[लोकालोकं गिरिम् अपि मुदा प्राप चिक्रीड बालः ॥]{style="color:#0000ff;"}\
Thereafter, he ascended upwards to the Mahar, Jana, Tapa and Satya realms \[of the universe], and kept going on to the excellent dwellings of the विश्वेदेव-s, साध्य-s, the immortals and Indra. Making anew celestial fierce beings, joyfully attaining the boundaries of the universe (लोकालोक mountain), the playfully sporting boy sported.

This displacement of the celestial bodies by Skanda already has a germ in the Mbh account: [प्रच्युताः सहसा भान्ति चित्रास् तारागणा इव ।]{style="color:#0000ff;"}. The celestial army of Indra attacked by Skanda is said to have been like the clusters of stars thrown off their orbit. Another notable point is a subtle astronomical allegory in the first verse. The constellation of Scorpius is called the nakra in Hindu tradition. Hence, we take the account of Skanda seizing the nakra-s and releasing them into the solar blaze as an allusion to the कार्त्तिक month (when Skanda was born) when the Sun is opposite to the कृत्तिका-s in Scorpius.

  - Alarmed by Skanda's sports the gods fought him.

  - Skanda defeated the gods and shows them his विश्वरूप (macranthropic) form.\
The विश्वरूप of Skanda, while comparable to other विश्वरूप-s in the इतिहास-पुराण tradition, has some interesting cosmic verses:\
[तस्मिन् तेजसि ते देवा वैश्वरूपे जगत्-त्रयम् ।]{style="color:#0000ff;"}\
[कोटि-ब्रह्माण्ड-पिण्डानाम् महा-वपुषि रोमसु ॥]{style="color:#0000ff;"}\
[यूकाण्डानीव कोटीनि चैकैकस्मिन् सहस्रशः ।]{style="color:#0000ff;"}\
[तत्-तद् आवरणैः सार्धं तत्रत्यैर् भुवनैर् जनैः ॥]{style="color:#0000ff;"}\
[भूतैर् भव्यैर् भविष्यद्भिर् ब्रह्म-विष्णवादिभिः सुरैः ।]{style="color:#0000ff;"}\
[जानु-प्रदेश-मात्रे 'स्य दृष्ट्वा विस्मयम् आगताः ॥]{style="color:#0000ff;"}\
In his radiance, were the gods of all forms and the triple world. In the hairs of his great body were the crores of spheres of galactic realms (ब्रह्माण्ड-पिण्डा-s). They were like the eggs of lice \[of the hairs], in each one of the crores there were thousands of world-systems, each with its own set of orbits and local inhabitants of those worlds. Seeing the past, the present and the future, the Brahman, विष्णु and like of gods all coming only to the height of his knees, they reached bewilderment.

  - Realizing who he was, the gods crowned Skanda as the commander of their army.

  - The incident of the runaway sacrificial ram of नारद. Skanda dispatched वीरबाहु to capture it and bring it back. Skanda then took it as one of his vehicles. A homologous episode is found in the अजोपाख्यान of the शिवपुराण; there, instead of a ram, it is a goat. It may be noted that the Tamil allusion to this myth in the poem by the सङ्गम् poet Maturai नक्कीरनार् (तिरुमुरुकार्रुप्पटै 200-210) also records a caprine animal that might be interpreted as either a goat or a ram.

  - Skanda chastised and imprisoned Brahman for his lack of gnosis of the प्रणव.

  - He then stationed himself at कुमारपर्वत. At Rudra's behest, Nandin tried to get him released, but Skanda warned Nandin that he might have him join Brahman and asked him to leave right away. Then Rudra and उमा finally made their son release Brahman and he taught Brahman the secrets of the प्रणव, associating it with the Yajurvedic/सामवेदिच् incantation  [सुब्रह्मण्योम् ।]{style="color:#0000ff;"} (the mantra used in the Soma ritual to invite Indra for the libation).

  - कुमार initiated the campaign against the Asura-s by marching against the fortress of तारक.

  - Skanda sent वीरबाहु to launch an attack on तारक and क्रौञ्च (an asura who had assumed a mountainous form due to a curse of Agastya).

  - Being informed by his spies of the assault, तारक sallied forth to meet the गण-s led by the nine वीर-s. Fierce encounters took place between वीरकेसरिन्, वीरबाहु and तारक. वीरबाहु repelled तारक's माया with the वीरभद्रास्त्र. तारक drew वीरबाहु into a feigned retreat and traps him in the mountainous cavern of क्रौञ्च, putting him to sleep. He then routed the गण-s showering missiles on them. तारक is described as having an elephantine head.

  - Skanda entered the field to rally his गण-s with वायु as his charioteer. Skanda routed the Asura-s. Taraka said that while he is a foe of Indra and विष्णु, he had no enmity with Rudra. But Skanda pointed to his sins and crimes against the deva-s and attacked him. After a fierce fight, Skanda cut off his trunk and tusks and pierced his head. He fell unconscious but on getting up he hurled the पाशुपत missile at Skanda. Skanda caught it with his hand and took it for himself. तारक then asked क्रौञ्च to aid him with his माया. After repulsing their magic, Skanda finally killed both तारक and क्रौञ्च with his शक्ति.

  - Skanda took his station at Devagiri and gifted the पाशुपत missile he had caught to वीरबाहु.

  - राजनीति section where the court suggested to शूरपद्म, who was enraged by the death of तारक, that they should avoid a confrontation with Rudra's party.

  - Skanda goes on a mostly शैव (apart from कञ्चि and वेङ्कटाचल, where Skanda is said to have run away when उमा did not give him the mango) pilgrimage.

  - Skanda releases the पाराशर-s from a curse they had gotten from their father due to their cruelty towards fishes in their youth.

  - Skanda goes to शेन्तीपुर.

The Sambhava-काण्ड ends here. We believe that the core of this काण्ड derives from an older कौमार tradition that was of pan-Indospheric distribution. The structure of the narrative is such that शूरपद्म and Siṃhamukha, who are unique to the Southern tradition, only have a minor role in it. The break in the narrative between the killing of तारक and क्रौञ्च on one side of the remaining Asura-s on the other side supports that part as being an accretion to this archaic core.


 2.  The Asura-काण्द. From here on the narrative starts paralleling the रामायण in several ways.

  - Asurendra, the lord of the Asura-s and his wife मङ्गलकेशी birthed a daughter named सुरसा. She became a student of उशनस् काव्य and acquired the moniker माया due to her proficiency in माया. When she reached adulthood, काव्य lamented the condition of the Asura-s due to their crushing defeats at the hands of Indra and विष्णु. He asked her to have sons of great might through कश्यप and have them learn the praxis of ritual from him.

  - Seduced by the beauty of सुरसा, कश्यप abandoned his austerities and cohabitated with her. From their coitus when they assumed celestial forms शूरपद्म was born. When they engaged in coitus as lions, Siṃhamukha with a leonine head was born. From their coitus as elephants तारक was born with an elephantine head. When they mated in the form of goats they birthed the demoness अजामुखी. Taking many other animal forms they birthed several other fierce Asura-s. From their sweat, during each intercourse, numerous other demons arose.

  - कश्यप then taught his sons the शैव lore.

  - Then abandoning कश्यप, सुरसा took her sons away and instructed them to perform a great sacrifice to Rudra to gain boons from him.

  - Having pleased Rudra with his mighty ritual, where शूरपद्म offered himself as the oblation, he obtained the boons of the overlordship of a 1008 galactic realms ([अष्टोत्तर-सहस्राणाम् अण्डानां सर्वभौमताम् ।]{style="color:#0000ff;"}), overlordship over the gods with enormous equipment and wealth, an adamantine body, invulnerability and the पाशुपत missile. Rudra granted him and his brothers such boons with the condition that no force except that originating from Rudra himself could destroy them.

  - Armed with these boons and blessed by काव्य, the Asura-s attacked Kubera and conquered his realm, taking him prisoner.

  - They then conquered the realms of the gods and subjugated them.

  - विष्णु fought तारक for long but realizing his invulnerability from Rudra's boons retreated after congratulating him.

  - शूरपद्म then had त्वष्टृ build great forts for himself and his brothers.

At this point the narrative takes detour into Agastya cycle.

  - अजामुखी forced दुर्वासस् to engage in congress with her. As a result, she birthed two sons Ilvala and वातापि. They went to दुर्वासस् and asked him to transfer his tapas power to them. He refused but offered them an alternative boon. They remained adamant and got into an altercation with him. He escaped from the place with his magic after cursing them that someday Agastya will slay them.

  - They obtained their peculiar boon of resurrection from Brahman.

  - They slew many ब्राह्मण-s through their well-known goat trick.

  - They were finally slain by Agastya, who digested वातापि and hurled the पाशुपत that he had obtained from Rudra at Ilvala slaying him.

  - शूरपद्म tried to abduct इन्द्राणी. The Asura-s caused a drought; as a consequence they dried up Indra's gardens.

  - With the aid of विनायक, Indra caused the कावेरी river to flow out of Agastya's pot and water the gardens.

  - After the churning of the ocean, Rudra wanted to engage in coitus with मोहिनी, the alluring female form of विष्णु. विष्णु indicated that it was impossible. Rudra pointed out that विष्णु was actually one of his शक्ति-s and thus a valid female. They copulated beneath a साल tree in Northern जम्बुद्वीप. The sweat from their passionate intercourse gave rise to the गण्डकी river. In it, the mollusks known as vajradanta-s gave rise to the सालग्रामन्-s used in the worship of विष्णु.

  - From their conjugation, the god शास्तृ was born.

  - Rudra placed शास्तृ in charge of protecting इन्द्राणी from abduction.

  - शास्तृ in turn brought in महाकाल as the bodyguard for इन्द्राणी.

  - अजामुखी and her friend दुर्मुखी tried to abduct इन्द्राणी for शूरपद्म. However, महाकाल swung into action and chopped off their hands.

  - अजामुखी complained to शूरपद्म of her dismemberment.

  - शूरपद्म forced Brahman to heal her and दुर्मुखी.

  - शूरपद्म's son भानुकोप seeking revenge attacked the realm of Indra.

  - He fought a fierce battle with Jayanta, the son of Indra, and eventually took him prisoner.

  - Unable to find Indra or इन्द्राणी, भानुकोप destroyed the realm of Indra.


 3.  The वीरमाहेन्द्र-काण्ड.

  - Before attacking the fortress of वीरमाहेन्द्र, Skanda sent वीरबाहु as a messenger to शूरपद्म to ask him to peacefully surrender, release the deva-s he had incarcerated and return their realms that he had occupied.

  - वीरबाहु went to the गन्धमादन mountain and prepared to fly to वीरमाहेन्द्र. He mounted the massif assuming a gigantic and fierce form and laughed terrifyingly (अट्टहास). He felt like extending his arms so that he could crush the Asura-s fortifications and cities with his hands like an oil-press crushing sesame seeds.

  - As he pressed on the mountain the surviving warriors of तारक who were hiding in the caves came forth and were crushed by वीरबाहु.

  - Thinking of his guru, Skanda, वीरबाहु flew into the sky causing the world to quake as he sped through the welkin.

  - As he arrived at लन्का, which was the capital of शूरपद्म's general व्यालिमुख (who was visiting his lord), he was challenged by his deputy वीरसिंह and his troops. After a quick fight, वीरबाहु chopped off वीरसिंह's arms and head with his sword.

  - वीरबाहु leapt on लन्का and pushed it under the ocean.

  - वीरबाहु was then attacked by व्यालिमुख's son अतिवीर and his troops who emerged out of the water. वीरबाहु cut down the daitya troops and took on अतिवीर who fought with a cleaver obtained from Brahman. However, Skanda's वीर cut his head off.

  - Flying a thousand yojana-s he reached वीरमाहेन्द्र. As he was wondering which might be the best gate to make an entrance he arrived at the southern gate. There, he was challenged by गजास्य, a monstrous demon with a thousand trunks and two thousand arms. After a closen fight, वीरबाहु chopped off his trunks and hands and slew him with a kick.

  - Realizing that this fight would bring more Asuras into the fray, he used his magic to become minute and entered via the eastern gate.

  - There he saw the enslaved gods and the dwellings of mighty demons.

  - कुमार appeared in the dreams of Jayanta and the gods who were being subject to indignities by the Asura-s. Skanda told them that he had already killed तारक and क्रौञ्च and that he had sent वीरबाहु as his emissary who would wreak havoc among the Asuras. He assured them that thereafter he himself would attack the दानव stronghold and slaughter them.

  - वीरबाहु met Jayanta and the other imprisoned gods and told them that their sufferings were due to their siding with दक्ष during his ritual. He assuaged them by stating that the spear-wielding god would destroy the Asura-s and relieve them shortly.

  - वीरबाहु audaciously appeared before शूरपद्म and intimated him of the conditions for his surrender placed to him by Skanda.

  - शूरपद्म rejected the terms and sent his general शतमुख to capture वीरबाहु. A fierce battle broke out between them during which वीरबाहु demolished 20,000 Asura fortifications. In the end, he slew शतमुख by thrashing him.

  - Taking a giant form he crushed many other Asura warriors.

  - Uprooting a mountain he smashed the Asura city.

  - After another fierce fight, वीरबाहु slew शूरपद्म's ten-headed son वज्रबाहु by chopping off his heads with his sword.

  - As वीरबाहु was flying away, the polycephalous व्यालिमुख challenged him. Another fierce fight ensued and वीरबाहु finally killed him by chopping off his heads.

  - Returning to शेन्तीपुर, वीरबाहु bowed thrice to Skanda.

  - The Asura-s rebuilt their capital demolished by वीरबाहु and prepared to fight Skanda.

  - Skanda built the fort of हेमकूट as the base for this attack on वीरमाहेन्द्र.

  - शूरपद्म's spies informed him that कुमार was gearing for an attack on वीरमाहेन्द्रपुर.

  - He called his son भानुकोप and asked him to attack Skanda and his troops right away as they had approached the Asura city and fortified themselves at हेमकूट.


 4.  The Yuddha-काण्ड.

[षण्मुखं द्वादश-भुजं त्रिषण्णयन-पङ्कजम् । कुमारं सुकुमाराङ्गं केकी-वाहनम् आश्रये ॥]{style="color:#0000ff;"}

  - भानुकोप donned his armor and mounting his car sallied forth with numerous Asura heroes.

  - Skanda sent heavily armed, ruby-colored वीरबाहु at the head of the भूतगण-s to intercept him. Wielding a bow like the पिनाक of Rudra he sallied forth. The two armies met amidst a shower of arrows from warriors on both sides.

  - The fight was evenly poised but eventually, the गण-s of Skanda started gaining an upper hand as the भूत commanders slew their Asura counterparts.

  - Seeing his forces retreating from the assault of the भूतगण-s of Skanda, भानुकोप rallied them back. Bending his bow to a circle he released an unending stream of arrows on them striking down many and causing the गण-s to fall back. Seeing this, the गण Ugra got close to भानुकोप and attacked him with a rod. भानुकोप destroyed that rod with his arrows and struck down the गण with the Brahma-spear. Then the गण दण्डिन् attacked भानुकोप with a mountain and struck his chariot and driver. भानुकोप furious felled दण्डिन् with a shower of arrows. Next, he fought the गण पिनाकिन् who had rushed at him and struck him down with a shower of thousand arrows. Thereafter, several other गण-s mounted a massed attack on भानुकोप who routed them with his unending shower of arrows.

  - He then fought the नववीर-s of Skanda. वीरसिंह was struck down by the नारायण weapon of भानुकोप.

  - Then वीरमार्ताण्ड attacked the Asura. He got close to भानुकोप and broke his bow; however, भानुकोप struck him down with his cleaver.

  - वीरराक्षस next attacked him from close quarters and both fell to the ground. भानुकोप recovered consciousness and mounting a fresh car resumed the battle. Five of the remaining वीर-s surrounded him and fought a great bow-battle for a while. Striking some of them down and brushing aside the rest he rallied the Asura troops and marched straight at वीरबाहु.

  - They fought a terrific battle in which both lost their bows but taking up new bows resumed the assault. वीरबाहु smashed भानुकोप's helmet but he donned a new one and continued. Finally, tiring from the fight, भानुकोप swooned.

  - Immediately, the Asura forces surrounded their hero to protect him even as the गण-s recovering from his assault surged forward. By then भानुकोप stood up and continued the battle with वीरबाहु.

  - Despite all his attempts he could not dispel वीरबाहु's showers of arrows. Hence, he deployed the मोहास्त्र, a missile that caused the गण-s to be paralyzed and fall to the ground. Even वीरबाहु was paralyzed in his car. Taking advantage of this भानुकोप shot numerous arrows at Skanda's forces drenching them in blood.

  - Skanda seeing his गण-s in dire danger fired the Amoha missle from हेमकूट. This destroyed the मोहास्त्र and revived all the गण-s.

  - Seeing the nullification of all his tactics, भानुकोप retreated for the day deciding to resume the battle later.

  - Realizing the seriousness of the situation, शूरपद्म himself decided to join the battle. He sallied forth along with अतिशूर, the son of Siṃhamukha and Asurendra, the son of तारक.

  - Indra saw them and informed Skanda of the impending attack. Having saluted Indra, Skanda decided to lead his forces personally in the battle. Indra asked वायु to be Skanda's charioteer.

  - A great battle broke out between the गण Ugra and अतिशूर. अतिशूर discharged numerous divine missiles at his adversary but they failed because Skanda provided immunity against those missiles to his follower. Finally, अतिशूर discharged the पाशुपत, but it did not harm its own party and returned to महादेव. Frustrated thus, he leapt out of his car and struck Ugra with a rod. However, Ugra survived that blow and snatched the weapon from the demon and pummeled him to death with it.

  - तारक's son Asurendra rushed to shore up the ranks after his cousin's death. He faced the गण-s Kanaka, Unmatta, Siṃha, दण्डक, and Vijaya in fierce encounters, defeating each of them.

  - Then वीरबाहु flew in on his car and started showering arrows on the demon. After a prolonged encounter, वीरबाहु smashed his enemy's chariot. The two engaged in a great battle flying in the skies, with Asurendra wielding a rod and वीरबाहु, a sword. The latter finally beheaded Asurendra.

  - Seeing his nephews slain, शूरपद्म launched a fierce attack on the भूतगण-s. He was attached by the nine Skanda-वीर-s. Fierce fights took place between him and वीरराक्षस, वीरमहेन्द्र, वीरधीर, and वीरमाहेश्वर in that order and he overcame all of them. वीरबाहु then entered the fray to shore up his half-brothers. He deployed the Aindra and वारुण, Brahma, वैष्णव missiles on the demon and शूरपद्म countered those with his मायासुरास्त्र. Then both deployed the पाशुपत missile but it returned back to the respective users. Then they both deployed शक्ति-s that neutralized each other. Finally, शूरपद्म struck वीरबाहु on his chest with a दण्ड.

  - Seeing his champion's discomfiture, कार्त्तिकेय attacked शूरपद्म and a great fight ensued. Bending his bow Skanda fired a profusion of arrows at the Asura lord. They cut each other's shafts in mid-flight. शूरपद्म hurled his शक्ति at Skanda, who cut it off with 14 arrows. Then शूरपद्म uttering a loud roar hurled a trident at Skanda who cut it off with four and the seven arrows thereafter. Then with another missile, Skanda smashed the helmet of शूरपद्म and destroyed his armor with further shafts.

  - Thereafter, the six-headed son of अम्बिका hurled a cakra and cut down the Asura hordes that accompanied शूरपद्म and the पिशाच-s feasted on their corpses.

  - शूरपद्म replenished his gear and returned to the fight. He tried deploying many divine missiles but they were all nullified by कुमार with his शक्ति missile. Thus, on the brink of defeat, शूरपद्म vanished and flew back to his fortified city thinking he would return later to fight कुमार.

  - Skanda ordered his भूतगण-s to storm the fortifications of the Asura stronghold.

  - The गण-s launched a massive assault on the fortifications of वीरमाहेन्द्रपुर. In course of this assault, they killed the demon Atighora.

  - भानुकोप sallied forth again to fight वीरबाहु, this time armed with his grandmother माया's Sammohana missile.

  - After prolonged use of various missiles both tried to get better of the other with the पाशुपत. Both पाशुपत missiles neutralized each other.

  - At this point, भानुकोप deployed his grandmother's Sammohana missile, which not only made वीरबाहु but also the rest of the Skandapārṣada-s unconscious and hurled them into the ocean.

  - भानुकोप returned to this father to tell him of his great victory and promised him that he would head out the next day to slay Skanda.

  - On receiving intelligence that his army had been drowned by भानुकोप, the six-headed son of Rudra launched his शक्ति, which sped to the ocean, and, destroying the Sammohana weapon, led his forces out back to the Asura capital.

  - The Skandapārṣada-s now launched a ferocious assault on the defenses of the city. The Asura व्याघ्रास्य advanced to defend the fortifications. He was slain after a trident fight by the Skandaगण named Siṃha.

  - As the Skandapārṣada-s demolished the defenses of वीरमाहेन्द्रपुर, वीरबाहु hurled the आग्नेय and वायव missiles and set the city on fire.

  - शूरपद्म's men informed him of the reversal and the impending destruction of his city by the fires. He brought in the महाप्रलय clouds to douse the fires. As they were putting off the fires, वीरबाहु struck back with the वाडव missile to vaporize the clouds. Seeing the havoc in his city, शूरपद्म wanted to head out himself to fight Skanda and his forces.

  - Just then शूरपद्म's son हिरण्य came to him and told him that it might be prudent to surrender to the six-headed son of Rudra, reminding him that there was no one who could counter him and his dreadful गण-s.

  - शूरपद्म warned his son that he would kill him if continued speaking thus. हिरण्य calmed him down and decided to enter the battlefield himself.

  - हिराण्य baffled the Skandapārṣada-s for a while with his display of माया. Finally, his माया was overcome by the Cetana missile shot by वीरबाहु. हिराण्य fought वीरबाहु for a while but the latter cut his bow and smashed his car with his astra-s. हिराण्य knew that he would be killed shortly. He also realized that nobody would be left to perform the funeral rites for his family once Skanda's troops stormed the city. Hence, he took the form of a fish and vanished into the ocean.

  - शूरपद्म's son Agnimukha next took the command of the Asura forces and led them against the Skandaगण-s with a vast force of Asura-s. In the battle that followed, वीरपुरंदर slew the Asura-s सोमकण्टक and Megha with his arrows. After a pitched battle, Agnimukha slew seven of the वीर-s, barring वीरपुरंदर and वीरबाहु with the पाशुपत. Agnimukha the advanced on वीरबाहु. After an even astra-fight, Agnimukha got भद्रकाली to fight on his behalf. वीरबाहु defeated her and she left saying no one could stop the Skandapārṣada-s. Then Agnimukha returned to the fight showering thousands of arrows. वीरबाहु hurled the वीरभद्रास्त्र which burst Agnimukha's head.

  - As वीरपुरंदर and वीरबाहु were lamenting the fall of their brothers, the latter flared up and shot an arrow into Yamaloka engraved with the message that, he the younger brother of कार्त्तिकेय, wanted Yama to release his seven half-brothers. Duly Yama released their आत्मन्-s and they reanimated their bodies.

  - A great battle next took place between three thousand Asura-s born of शूरपद्म and a thousand of the chief Skandapārṣada-s. The battle was evenly poised for a while, but the Asura-s started gaining the upper hand as their severed limbs were restored by a special gift they had obtained from Brahma. Hence, the गण-s turned to Skanda, who appeared in their midst, and gave one of their leaders, Vijaya, the Bhairava missile. As he deployed it, one of three thousand Asura-s named Matta deployed the मायास्त्र. However, the भैरवास्त्र destroyed it and cut off the heads of all three thousand Asura-s.

  - Next, Dharmakopa, another son of शूरपद्म advanced with his troops against the वीर-s. His troops were destroyed by the वीरस्-s even as Dharmakopa closed in on वीरबाहु. After an exchange of missiles, Dharmakopa struck down वीरबाहु's charioteer. Then they engaged in a closen fight where Dharmakopa tried to strike वीरबाहु with a rod and then a thunderbolt. Evading both, वीरबाहु killed him with a kick.

  - भानुकोप received the news that while he thought he had won, the forces of Skanda had returned and wreaked enormous destruction on the Asura-s. He told his father perhaps it was a futile attempt, and they should surrender to Skanda. His father refused; hence, भानुकोप set forth again to fight the Skandapārṣada-s. After a great fight भानुकोप overthrew the भूतगण-s attacking him and rushed forward with a shower of arrows. वीरबाहु bent his bow and destroyed the missiles hurled by भानुकोप. Thereupon, वीरबाहु hurled a शक्ति and struck भानुकोप. However, recovering from the blow he resumed the fight. For a while, neither could get better of the other in their exchange of astra-s. भानुकोप then baffled वीरबाहु with his माया powers. वीरबाहु destroyed the माया display with his own magic. As their fight raged on, भानुकोप smashed वीरबाहु's chariot. But वीरबाहु retaliated by breaking the Asura's chariot and cutting his bow with his shafts. Finally, they closed in for a sword fight. वीरबाहु chopped off the right hand of भानुकोप but he took the sword in his left and continued. वीरबाहु cutoff that hand too. Handless, he tried to deploy the माया missile but वीरबाहु swept his head away with a blow from his sword.

  - शूरपद्म on hearing of the death of his son fainted. He lamented much when he received his son's mutilated body parts. He called his brother Siṃhamukha to come from his city of आसुरपुर to join the fight. Shocked by the news of the defeat of the Asura-s, and the death of his sons and nephews, Siṃhamukha flew over from his city to aid his brother.

  - Donning his armor, he set forth for battle with his great troop of Asura-s. Skanda called वायु to get his chariot ready. वीरबाहु with his half-brothers wanted to lead the 100,000 Skandapārṣada-s to the encounter first. Skanda let him do so and a fierce fight ensued. Showering balls, arrows, axes, and plows on the गण, the great lord of the Asura-s advanced. His demons fought several गण-s in melee as they exploded each other's weapon discharges to smithereens. वीरमार्ताण्ड used the ज्ञानास्त्र to counter the माया being deployed by the demons. Siṃhamukha cut through the गण ranks like a great mountain on the move. He put to flight the 100,000 Skandapārṣada-s with showers of missiles. He then proceeded to crush them as though one would crush mosquitoes. Seeing this, वीरबाहु counter-attached showering thousands of arrows with his bow. The hundred sons of Siṃhamukha surrounded him and returned the showers of arrows. With his missiles, वीरबाहु smashed their chariots and broke their bows. Then they rushed at him with their swords but वीरबाहु slew all hundred with an arrow and his sword.

  - Infuriated and saddened by this, Siṃhamukha rushed at वीरबाहु. They ground each other's missiles to dust and had a prolonged astra fight. वीरबाहु slew his charioteer. Siṃhamukha hurled a गदा at him, but it burst on striking his adamantine form born of Rudra. वीरबाहु then demolished the Asura's car. He resumed the battle taking new cars over and over again and deploying thousands of bows. वीरबाहु kept breaking them repeatedly. Seeing himself unable to get better of Skanda's warrior, Siṃhamukha deployed the मायापाश. The great lasso weapon immediately bound वीरबाहु and the गण-s who were on the field and hurled them atop the Udaya mountain in a state of paralysis. Sensing victory, Siṃhamukha roared loudly and thought that Skanda too had fled. However, his spies informed him that Skanda was still very much there with the remaining गण-s at हेमकूट. मातरिश्वन् informed Skanda of the events and Siṃhamukha's advance towards their fort.

  - Mounting his car driven by वायु, Skanda led his forces into battle. In the battle that ensued the Asura-s began to retreat. At that point, Siṃhamukha assumed a gigantic form with two thousand arms. Grabbing all the गण-s of Skanda he swallowed them. शूरपद्म's spies informed him of his brother's deeds and he ascended an observation turret to see the great form of his brother.

  - Skanda then strung his bow and twanged the string causing the whole universe to resound and the Asura vehicles fell to the ground from the sound emanating from the bow twangs of the son of उमा. Siṃhamukha rushed at him to do battle. कुमार fired a mighty missile that split open the firm belly of the demon and from the fissures through which blood was pouring out, some of the गण-s who had been swallowed emerged forth. Stanching the slits in his belly with his many arms, the demon hurled a dreadful rod at Skanda. He split it up with four missiles, which then proceed the strike the demon on his forehead. Losing his strength, he lifted his hands off his belly and the remaining गण-s too escaped.

  - Skanda then fired a missile that proceeded to the Udaya mountain and destroyed the मायापाश. Then turning into an airplane the missile brought वीर-s and गण-s back to the field and returned to the six-headed god's quiver.

  - To shore up their leader, the Asura-s who had retreated returned to attack Skanda upon hearing his terrifying roar. As they surrounded the god, he hurled thousands of projectiles and also attacked the demons with rods, swords, spears and axes. वायु maneuvered the car with great speed even as Skanda dispatched his missiles that lit up the entire universe like the वडव fire. The great god pierced the many galactic realms with his weapons and rent asunder the limbs of the demons and shattered their vehicles. The cluster missiles shot by Skanda branched repeatedly giving rise to crores upon crores of arrows and penetrated all the galactic realms slaying numerous दानव-s wherever they were. Seeing these weapons being fired by their commander, all the deva-s sang the praises of the son of Rudra.

  - The whole field was filled with corpses of the demons. Wielding a thousand bows Siṃhamukha again attacked the commander of the deva-s.

  - He then struck वायु on the chest with numerous arrows and the god fainted. But Skanda controlling his own car destroyed the Asura's chariot with a hundred arrows. Then with a thousand arrows, the son of महादेव, cut down all the bows of the demon at once. Siṃhamukha hurled a trident at the god, who cut it down with fourteen arrows. Then he rushed at the god with a rod who powdered it with seven shafts. The demon then sent the death-dealing पाश but Skanda cut it up with a thousand projectiles. Then he cut up the two thousand arms of the Asura. The great demon (termed माहामद here) uttered a "महाकिलिकिलाराव" and rushed at Skanda. The god sliced off his thousand heads with an equal number of arrows.

  - Siṃhamukha regenerated his severed limbs and reentered the fray. Skanda let this happen eight times as part of his battle sport. Then he cut all his hands except for a pair and heads except for one. The demon roared that he would slay the god with just those and uprooted a mountain and rushed at his adversary. कार्त्तिकेय rent asunder that mountain with a single arrow. The Asura now attacked him with a terrifying दण्ड. Thereupon, Guha hurled his vajra which blazed up like several crores of suns. It destroyed that दण्ड and striking the Asura on his chest annihilated him.

  - Having bathed in the Celestial गङ्गा कार्त्तिकेय returned to the हेमकूट fort with his वीर-s and गण-s.

  - Hearing of the death of his brother from his agents, शूरपद्म fell down from the observation turret he had mounted to witness the battle. Regaining his composure, he decided to himself lead the Asura-s in the war against Guha. He ordered all the surviving Asura-s from the numerous galactic realms that he controlled to come over and join him for the battle.

  - Mounting his special battle car armed with all the divine missiles, with a great force of दानव-s he headed out of his fortified city with their roars filling the whole universe.

  - The gods called on their commander Skanda to enter the field against the evil demon. Worshiped by all the gods and his 100,009 वीर-s, Skanda took up all his weapons and set forth for battle on the car driven by the god वायु:\
[आदाय परशुं वज्रं शूलं शक्तिं विभीषणां ।]{style="color:#0000ff;"}\
[खड्गं खेटं बृहच् चक्रं दण्डं मुसलम् एव च ॥]{style="color:#0000ff;"}\
[धनुश्-शरान् महाघोरांस् तोमराणि वरण्य् अपि ।]{style="color:#0000ff;"}\
[विनिर्गत्य रथं रम्यं वायुना नीतम् अग्निभूः ॥]{style="color:#0000ff;"}\
[आरुरोह सुरैस् सर्वैः पूजितः पुष्प-वृष्टिभिः ।]{style="color:#0000ff;"}\
[नव-संख्याधिकैर् लक्षैर् वीरैर् अपि महाबलैः ॥]{style="color:#0000ff;"}

  - The other gods asked विष्णु if कार्त्तिकेय might meet with success in the impending encounter. विष्णु assuaged their doubts saying that their troubles would end soon as Skanda would definitely triumph.

  - As the battle was joined, शूरपद्म's demons charged with a great shower of weapons. Skanda twanging his dreadful bow, which resounded like the flood at the end of the Kalpa, entered the field. He launched into an orgy of slaughter with his missiles reaching the limits of the universe. Wherever the demons went, his bolts cruised after them. Breaking through the walls of the galactic realms they entered whichever region the Asura-s flew to and slew them. Floods of blood and mountains of corpses of the Asura warriors started to pile all around.

  - Furious, शूरपद्म joined the fray and laid low most of the Skandapārṣada-s with his terrifying missiles. वीरबाहु rushed forth to engage him and cut his bow with his cleaver. But शूरपद्म punched with his fist and he fainted. Deciding not to kill him for he was just a messenger of Skanda, शूरपद्म seized by his feet and hurled him into the sky.

  - He then charged at Skanda and engaged him in a fierce bow battle with the exchange of innumerable arrows. Finally, शूरपद्म struck down the flag of Skanda with a shower of arrows and blew his conch as a mark of victory. Skanda however quickly retaliated cutting and hurling शूरपद्म's banner into the sea with seven shafts. Then, the thousand-headed गण भानुकम्प blew on a thousand conchs and विष्णु too blew on his. Agni turned into the cock banner and went to adorn the car of Guha. The cock crowed loudly. All this created a terrifying din.

  - Angered, शूरपद्म now turned to the gods were and attacked them with his weapons. Skanda followed him with वायु driving his car at top speed. Skanda protected the gods and attacked the demon with a shower of weapons. As the chariots of the two of them wheeled around in battle the whole universe to vibrated violently and whole mountains were reduced to atoms.

  - शूरपद्म attacked his enemy with हलायुध-s, भिन्दिपाल-s, कुलिश-s, tomara-s and परशु-s. Then Skanda destroyed his vehicle completely with fourteen missiles.

  - The demon then mounted the Indralokaratha (the space-station he had captured from the gods) and started tunneling into the various galactic realms he had conquered. However, he found the tunnels into them blocked by the arrows of महासेन and found that many of his demons were trapped in each one of them. He broke down the obstructions with his weapons and let out his demons. Those Asura-s came out and mounted a furious attack on Skanda. With his cakra, परशु, दण्द and musala the son of उमा slaughtered them, and chasing them to each of the realms, he burnt them down with his weapons to a fine ash.

  - The Asura then deployed the deadly सर्वसंहारक-cakra, but महासेन sportingly captured it for himself. Next, he tried माया tactics but Skanda easily overcame those with the ज्ञानास्त्र.

  - Thus, defeated he finally went to his mother and asked if she might have a means of victory. She told him that she did not see a way out against the son of Rudra, but the only thing he could do is to get the सुधामन्दर mountain to revive the dead demons.

  - He mounted a lion-vehicle and sent the Indralokaratha to bring the said mountain. The craft brought the सुधामन्दर to the field and the wind blowing from it started reanimating the dead demons. Seeing this, Skanda deployed the पाशुपत missile that started branching and emitting numerous Rudra-s, the Marut-s, Agni-s and vajra-s. These destroyed all the reanimated demons and also the vivifying mountain. Thereupon, the Asura sent the Indralokaratha to scoop up वीरबहु and the remaining 100,008 troops of Skanda and stupefy them. Guha retaliated with a series of missiles that grounded the craft and brought it back to him. The गण-s got out of it safely and the space-craft became the property of Skanda.

  - Then the Asura injured वायु with his shafts and briefly Skanda's chariot was out of control. But regaining control he cut the bow of the demon. शूरपद्म thereafter attacked him with the terrifying missile known as the सर्वसंहारक-शूल. Skanda shot numerous arrows to destroy his lion-vehicle and then hurled the Ghora-कुलिश that neutralized the said शूल and brought it back to Skanda.

  - The Asura assumed the form of a gigantic fierce bird and started pecking at Skanda's car and blowing his गण-s away with the blasts from his wings.

  - Skanda then looked at Indra and the latter took the form of a peacock. Mounting the peacock Skanda and Indra fought the demon. Indra pecked him and clawed him in the form of the peacock, even as Skanda pierced him with many arrows. शूरपद्म dived in his bird form and broke Skanda's bow, but Guha drew out his sword and hacked the bird-formed demon to pieces.

  - The demon then took the form of the earth. Skanda taking a new bow drowned that earth-formed demon with the oceanic missile. The demon then took the form of the sea. With a hundred fiery missiles Skanda dried up that form. He successively took various forms, including the gods, but Skanda destroyed all of those with his missiles.

  - Finally, to reveal that those forms of the demon were merely fictitious forms, Skanda assumed his macranthropic form with all existence and all the gods comprising his body --- the planets and stars his feet; वरुण and निरृति his ankle joints; Indra and Jayanta his thighs; Yama and time his hips; the नाग-s and the ambrosia his genitals; the gods his ribs; विष्णु and Brahman his arms; the goddesses his fingers; वायु his nose; Rudra his head; the numerous galactic realms his hair follicles; the ओम्कार his forehead; the Veda his mouth; the शैवागम-s his tongue; the seven crores of mantra-s his lips; all knowledge his यज्ञोपवीत.

  - Seeing this macranthropic form of कुमार, शूरपद्म had the doubt for the first time in his existence if after all this god might be undefeatable --- many great Asura-s had fought him and many great missiles, which were previously infallible had been, used yet he triumphed over all of those. However, the demon brushed aside these feelings and assumed a gigantic form with numerous arms, heads and feet, and casting great darkness that enveloped the whole universe he rushed forward to eat Skanda and the other deva-s.

  - Skanda immediately hurled his mighty शक्ति. Blazing like crores of suns it destroyed the overpowering darkness of the Asura and cruised after him. He dived into the sea of the fundament even as the शक्ति chased him there. The दानव immediately became the "world-tree" --- a giant mango tree stretching across the limits of the universe. Blazing like the trident of Rudra, the शक्ति split that tree in half. शूरपद्म assumed his own form and drawing his sword rushed at Skanda. The शक्ति struck him immediately and slew him.

  - His remains were transformed into a peacock and a cock that respectively replaced Indra and Agni as the mount and the banner of Skanda, even as the two gods returned to their prior state. Thereafter, the gods praised महासेन for his glorious acts.

  - शूरपद्म's primary wife expired on hearing the news of his death and his son हिरण्य who had hidden as a fish in the ocean performed the last rites of the dead demons with help of उशनस् काव्य.

The final battle between Skanda and शूरपद्म has some mythic motifs of interest. First, the many transmogrifications of the demon in course of his battle with the god are reminiscent of the shamanic transformations. This motif is encountered in several distant traditions --- most vividly in the folk Mongolian account of Chingiz Khan's final fight with the Tangut emperor composed by his descendant Sagang Sechen (Also note the motif of the nine heroes of the Khan in that account). There, Khasar, the brother of the Khan (one of the nine heroes) plays a role similar to वीरबाहु in destroying a witch who was guarding the Tangut capital and preventing the entry of Subetei. Khasar killed her with his arrows allowing the Mongols to storm it. Then the Khan and the Tangut lord fought a magical battle with both of them taking on many forms like a snake, गरुड, tiger, lion, and the like. Finally, the Khan took the form of Khormusta Khan Tengri (the great Mongol god) himself and put an end to the shape-shifting of the Tangut. Though he struck the Tangut with many arrows and swords he still could not kill him. ठे Tangut let slip the secret of his death in the form of a magical wootz steel sword hidden in his boot that the Khan seized and slew him. Thus, we suspect that the shape-shifting of शूरपद्म in the final battle is from an ancient shamanic layer of the कौमार tradition that is attested in the सङ्गम् Tamil tradition (the muruka-veri, e.g., तिरुमुरुकाऱ्‌ऱुप्पटै 200-210).

Second, the final dive of शूरपद्म into the ocean and/or his transformation into a mango tree is a motif that has deep roots in Tamil कौमार tradition. It is mentioned in multiple सङ्गम् texts such as: 1) Pattiṟṟupattu: here the Cēra king नेटुञ्च्ēरलातऩ्, who built a fleet to fight a naval battle with the Romans, is said to have destroyed his enemies even as Skanda cut down the tree of the Asura-s. 2) The परिपाटल् 18.1-4 mentions how Skanda pursued the demon into the deep ocean. परिपाटल् 5.1-4 mentions both his pursuit into the ocean and his destruction by the Skandaशक्ति in the form of a mango tree. 3) तिरुमुरुकाऱ्‌ऱुपटै 45-46 mentions his pursuit into the ocean. तिरुमुरुकाऱ्‌ऱुपटै 58-61 mentions his destruction in the form of the mango tree and also his centaur transmogrification, which is absent in the पुराण. We take these mythemes to be reflexes of the famous precessional myth --- i.e., the shattering of the old world axis that is widespread across Hindu tradition and beyond. A variant of this myth is associated with the submerging of the old equinoctial constellation beneath the equator (the world ocean, e.g., the वराह myth): thus, both of these are combined here in the final fight of शूरपद्म. Notably, both in the Pattiṟṟupattu and the परिपाटल्, Skanda is described as riding an elephant rather than a peacock in the final battle against शूरपद्म. This is probably an archaism as this elephant vehicle is mentioned in the ancient कौमार material found in the medical काश्यप-संहिता (129) as having been generated from ऐरावत by Indra for Skanda.

5. The Deva काण्ड.

  - The rule of the gods is restored.

  - Skanda is engaged to Indra's daughter देवसेना.

  - The marriage of Skanda and देवसेना.

  - Skanda seeks the daughter of विष्णु born as वल्ली in a sweet potato excavation among the pulinda hunter-gatherers.

  - He appears as an old man to her. गणेश frightens her as an elephant, and she comes into the hands of Skanda in the form of the old man seeking help from the elephant.

  - वल्ली recognizes Skanda and starts a clandestine affair with him.

  - When she elopes with Guha, the hunters, including वल्ली's brothers and father chase and attack Skanda, who strikes them down with his arrows.

  - कुमार revives the dead huntsmen and marries वल्ली and returns to his abode with his two wives.

  - The praise of emperor Mucukunda.

  - Mucukunda installs the image of Rudra known as त्यागराज.

6. The दक्ष-काण्ड\
This section is mostly शैव material relating to the cycle of दक्ष. One notable point is that here Rudra generates वीरभद्र and उमा generates भद्रकाली to destroy दक्ष's ritual. Notably, this parallels the Ur-Skandaपुराण wherein उमा generates भद्रकाली by rubbing her nose. There Rudra generates Haribhadra. This might indicate a connection to that ancient Skandaपुराण version of the दक्ष cycle. However, we may note that a similar situation is also seen in the ब्रह्मपुरण ((39.51), where Rudra created the lion-formed वीरभद्र, whereas उमा creates भद्रकाली to accompany him. Further, interestingly, here as वीरभद्र destroyed the male partisans of दक्ष, भद्रकाली destroyed the females of दक्ष's clan. This symmetry appears to be an ancient motif --- in the Greek world we have Apollo kill the male Niobids while Artemis killed the female ones. Apart from the दक्ष cycle, this काण्ड contains:

  - The ऋषि-s' wives at दारुकवन run after Rudra. They attack him with various beings and he destroys them.

  - The killing of गजासुर by Rudra.

  - Churning of the ocean and Rudra consumes the हालाहल.

  - The appearance of eleven crore Rudra-s at the मध्यार्जुन shrine (Tiruvidaimarudur in the द्राविड country).

  - The beheading of Brahman by Bhairava.

  - Dharma becomes Rudra's bull.

  - Rudra destroys the universe and smears the residue as his ash.

  - Rudra slays Jalandhara.

  - The birth of गणेश.

  - ज्योतिर्लिङ्ग-s and अरुणाचल.

  - पौराणिक geography.

7. उपदेश-काण्ड\
The only कौमार-related material in this काण्ड are: 1. The praise of Skanda's peacock and chicken. 2. The "backstories" of the birth of शुरपद्म, his mother and his clan. 3. The Skandaṣaṣṭhī festival.

The rest of this काण्ड is again largely शैव material pertaining to शिवधर्म for lay devotees. Indeed, chapters in this section seem to self-identify as a शिव-पुराण. It begins with an account of the Rudra-गण in कैलास. It contains several accounts of humble animals (including men like cora-s) attaining higher births from acts of शैव piety. Similarly, sinners who defile/steal from शैव shrines or take even things like lemons or bananas from them attain hell. The observance of key festivals of Rudra and the Bhairava-वीरभद्र festival are laid out. Further, it gives the 1008 names of Rudra, the practice of अष्टाङ्ग-yoga, the theological principles of Siddhānta and the iconography of the twenty five images of Rudra that are displayed in शैव shrines.

It also contains accounts of: 1. Rudra destroying the Tripura-s. 2. Rudra slaying Andhaka. 3. The rise of the most terrible demon भण्डासुर. Rudra performs a fierce ritual, offering Brahman, विष्णु, Indra and other gods as samidh-s in a fire altar where he himself was the fire. From it arose the youthful goddess त्रिपुरा who slew भण्डासुर. This minimal account lacks the details seen in the ललितोपाख्यान, where this myth takes the center-stage. 4. The killing of महिष by दुर्गा through the grace of Rudra as केदारेश्वर. 5. The killing of रक्तबीज by काली and her dance with Rudra.

