
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on श्री, विष्णु and शृङ्गार](https://manasataramgini.wordpress.com/2022/02/27/a-note-on-sri-vi%e1%b9%a3%e1%b9%87u-and-s%e1%b9%9b%e1%b9%85gara/){rel="bookmark"} {#a-note-on-शर-वषण-and-शङगर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 27, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/02/27/a-note-on-sri-vi%e1%b9%a3%e1%b9%87u-and-s%e1%b9%9b%e1%b9%85gara/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[यः पूर्व्याय वेधसे नवीयसे]{style="color: #0000ff"}\
[सुमज्-जानये विष्णवे ददाशति ।]{style="color: #0000ff"}\
[यो जातम् अस्य महतो महि ब्रवत्]{style="color: #0000ff"}\
[सेद् उ श्रवोभिर् युज्यं चिद् अभ्य् असत् ॥]{style="color: #0000ff"} RV 1.156.2\
Whoever repeatedly performs rituals to the wise one\
to the ancient and the new one, विष्णु, along with his consort,\
whoever speaks of the great birth of the great \[god],\
he alone will surpass his peer in glory.

While the aspect of the विष्णु as a manly warrior of great might (like as the suppressor of the malignant wolf) is well-expressed in his cognates from more than one branch of the Indo-European tradition (going by extant material), his fertility and erotic aspects are less-known. By the latter, we are not referring to the prominent reflexes of these seen in the कार्ष्णि cult of the सात्त्वत branch of the religion but to those pertaining to the form of the deity in the mainstream of the Indo-Aryan religion. These aspects are already alluded to in the ऋग्वेद itself. For example, with regard to his role in fertility, we have:

[सप्तार्धगर्भा भुवनस्य रेतो विष्णोस् तिष्ठन्ति प्रदिशा विधर्मणि ।]{style="color: #0000ff"} RV1.164.36ab\
The seven embryos of the world-hemispheres, the semen of the universe,\
stand in the expanding space by the institutes of विष्णु.

[विष्णुर् योनिं कल्पयतु ।]{style="color: #0000ff"} RV 10.184.1a\
विष्णु prepares the womb. (An incantation that is used in the main Hindu fertility ritual of गर्भाधान)

The above mantra-s present the function of विष्णु in preparing the womb for the embryos both in the cosmic (in relation to Dyaus and पृथिवी or the world hemispheres) and the human realms. The god's famous name शिपिविष्ट that is used by वसिष्ठ in RV 7.99 and 7.100 ties together his erotic and fertility aspects. The fertility aspect of विष्णु under this name is remembered down to भागवत from the late पौराणिक stratum, wherein we hear of the ब्राह्मण-s performing a rite for the king of अङ्ग to bear a son:

[इति व्यवसिता विप्रास् तस्य राज्ञः प्रजातये ।]{style="color: #0000ff"}\
[पुरोडाशं निरवपन् शिपिविष्टाय विष्णवे ॥]{style="color: #0000ff"} Bh 4.13.35\
Having decided thus, the vipra-s offered a cake to विष्णु शिपिविष्ट for the sake of progeny for that king.

Finally, we may also note विष्णु's association with a retinue of consort goddesses in the RV:

[विष्णुं स्तोमासः पुरुदस्मम् अर्का]{style="color: #0000ff"}\
[भगस्येव कारिणो यामनि ग्मन् ।]{style="color: #0000ff"}\
[उरुक्रमः ककुहो यस्य पूर्वीर्]{style="color: #0000ff"}\
[न मर्धन्ति युवतयो जनित्रीः ॥ऋग्वेद 3.54.14 ]{style="color: #0000ff"}\
To विष्णु of many marvels the songs and chants\
have gone, like singers on the road of Bhaga.\
The wide-striding bull whose followers are many \[goddesses];\
The youthful mother goddesses never forsake him.

In the तान्त्रिक tradition straddling the border of पाञ्चरात्र and शैव sects, the शिपिविष्ट aspect of विष्णु is made explicit in the iconography of his erotic form [मायावामन](https://manasataramgini.wordpress.com/2006/04/16/the-peculiar-mythology-behind-the-mayavamana-mantras/). The erotic aspect of विष्णु and श्री is also inherited by the स्मार्त श्रिकुल tradition (recorded in the ब्रह्माण्ड-पुराण), where a lay devotee couple might recite an [incantation to विष्णु and his consort श्री so that their coitus might become an act of kaula offering](https://manasataramgini.wordpress.com/2009/07/13/smarta-kaula-intersections/). The existence of a now extinct वैष्णव तान्त्रिक tradition with erotic rituals paralleling those of the शैव यामल tantra-s and bauddha-s of the वज्रयान stream is indicated by the bauddha commentator आनन्दगर्भ, who in his commentary on the गुह्यसमाज-tantra tries to explain why the Buddha expounded this tantra while in coitus with the [Buddha-योषित्-s](https://manasataramgini.wordpress.com/2009/02/16/nastika-notes-2/). He remarkably states that this was done to draw away the worshipers of विष्णु to the bauddha-mata. He goes on to state those वैष्णव-s worshiped विष्णु (the etymologies he provides makes it clear that he is not referring to a कार्ष्णि cult) via erotic pleasure-giving rituals. Thus, it appears that the bauddha-s were themselves mimicking a तान्त्रिक वैष्णव tradition and trying to justify their own rituals within a "conversion" framework. We believe that the tradition on consideration was focused on the form of विष्णु known as [विष्णु Trailokyamohana](https://manasataramgini.wordpress.com/2006/08/05/mahavaishnava-krama/), whose mantra-s are found in several extant texts, but a more detailed consideration of those is beyond the scope of this note.

Reflexes of the ancient erotic aspect of विष्णु also find considerable expression in classical काव्य. In the rest of this note, we provide some examples culled from various anthologies where श्री or विष्णु are invoked in the context of their erotic sports.

[किञ्जल्क-राजिर् इव नील-सरोज-लग्ना]{style="color: #0000ff"}\
[लेखेव काञ्चन-मयी निकषोपल-स्था]{style="color: #0000ff"}\
[सौदामिनी जलद-मण्डल-गामिनीव ।]{style="color: #0000ff"}\
[पायाद् उरः स्थल-गता कमला मुरारेः ॥]{style="color: #0000ff"} (वसन्ततिलका)\
Like an array of pistils clinging to a blue waterlily,\
like the streaks of gold marking a touchstone,\
like lightning flashing against a mass of rain clouds,\
May कमला lying on the chest of मुरारि protect us.

[किं युक्तं बत माम् अनन्य-मनसं वक्षः-स्थल-स्थायिनीं]{style="color: #0000ff"}\
[भक्ताम् अप्य् अवधूय कर्तुम् अधुना कान्ता-सहस्रं तव ।]{style="color: #0000ff"}\
[इत्य् उक्त्वा फण-भृत्-फणा-मणि-गतां स्वाम् एव मत्वा तनुं]{style="color: #0000ff"}\
[निद्राच् छेदकरं हरेर् अवतु वो लक्ष्म्या विलक्ष-स्मितम् ॥]{style="color: #0000ff"} (शार्दूलविक्रीडित; attributed to भास)\
"Why, alas, is it appropriate that, abandoning me who single-mindedly lies on your chest\
full of attachment, you now take a thousand others as your wives?"\
She said so, taking the \[reflections] of her own body in the gems borne on the serpent's hoods.\
May that embarrassed smile of लक्ष्मी that broke the sleep of Hari protect you!

This verse potentially plays on the multiplicity of consorts, already alluded to in the RV, being reflections of the singular श्री.

[केलि-चलाङ्गुलि-लम्भित-लक्ष्मी-नाभिर्-मुर-द्विषश् चरणः ।]{style="color: #0000ff"}\
[स जयति येन कृता श्रीर् अनुरूपा पद्मनाभस्य ॥]{style="color: #0000ff"} (आर्या)\
The toe of the foot of Mura's foe playfully tickled the navel of लक्ष्मी;\
victorious is that which makes श्री a suitable wife of lotus-naveled one.

Here a pun is played on विष्णु being पद्मनाभ --- lotus-naveled --- and also having lotus feet. By tickling श्री on her navel with his toe, he has also made her "पद्मनाभा" and thereby a suitable female counterpart of his.

[कच-कुच-चुबुकाग्रे पाणिषु व्यापृतेषु]{style="color: #0000ff"}\
[प्रथम-जलधि-पुत्री-संगमे ।अनङ्ग-धाम्नि ।]{style="color: #0000ff"}\
[ग्रथित-निबिड-नीवी-बन्ध-निर्मोचनार्थ]{style="color: #0000ff"}\
[चतुराधिक-कराशः पातु वश् चक्र-पाणिः ॥]{style="color: #0000ff"} (मालिनी)\
With her hair, breasts and chin-tip engaged in his hands,\
in the first erotic congress with the ocean's daughter,\
may the wheel-bearer wishing for more arms than four,\
for untying the tightly fastened knot of her skirt, protect you!

[उत्तिष्ठन्त्या रतान्ते भरम् उरग-पतौ पाणिनैकेन कृत्वा]{style="color: #0000ff"}\
[धृत्वा चान्येन वासो विगलित-कबरी-भारम् अंशं वहन्त्याः ।]{style="color: #0000ff"}\
[भूयस् तत्-काल-कान्ति-द्विगुणित-सुरत-प्रीतिना शौरिणा वः]{style="color: #0000ff"}\
[शय्याम् आलम्ब्य नीतं वपुर् अलस-लसद्-बाहु लक्ष्म्याः पुनातु ॥]{style="color: #0000ff"}\
(स्रग्धरा; attributed to Vararuci)\
Raising herself at the end of coition by holding on to the serpent-lord with one hand,\
bearing her garment in the other, with her mass of heavy disheveled tresses on her shoulder\
But again, with the beauty of her form doubling his desire for love, शौरिन् pulls her back\
to the couch. May the body of लक्ष्मी with her indolently embracing arms purify you.

[लक्ष्म्याः केश-प्रसव-रजसां बिन्दुभिः सान्द्र-पातैर्]{style="color: #0000ff"}\
[उद्वर्ण-श्रीर् घन-निधुवन-क्लान्ति-निद्रान्तरेषु ।]{style="color: #0000ff"}\
[दोर्-दण्डो 'सौ जयति जयिनः शार्ङ्गिणो मन्दराद्रि-]{style="color: #0000ff"}\
[ग्राव-श्रेणि-निकषम् असृण-क्षुण्ण-केयूर-पत्रः ॥]{style="color: #0000ff"}(मन्दाक्रान्ता, attributed to certain श्री भगीरथ)\
With the droplets of pollen falling thickly from the flowers in लक्ष्मी's hair,\
brightly decorating it as he wearily sleeps in between intense erotic sports \[with her],\
may that cudgel-like arm of the conquering wielder of the शार्ङ्ग bow, be victorious,\
for whose irresistible polished armband the rocky array of mount Mandara was the touchstone.

The above is an allusion to the incarnation of विष्णु as the gigantic turtle bearing the axial Mandara mountain during the churning of the ocean.

