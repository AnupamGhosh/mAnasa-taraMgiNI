
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Alkaios' hymn to the Dioskouroi: Hindu parallels](https://manasataramgini.wordpress.com/2022/05/25/alkaios-hymn-to-the-dioskouroi-hindu-parallels/){rel="bookmark"} {#alkaios-hymn-to-the-dioskouroi-hindu-parallels .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 25, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/05/25/alkaios-hymn-to-the-dioskouroi-hindu-parallels/ "4:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In this note we shall see how even a short "सूक्त" of the yavana Alkaios to the Dioskouroi (individually named Kastor and Polydeukes), the Greek cognates of the अश्विन्-s, offers several parallels to the Hindu tradition in the Veda. In the Veda, the अश्विन्-s are [the sons of Rudra](https://manasataramgini.wordpress.com/2020/01/12/the-asvin-s-and-rudra/) hinting at his overlap with Dyaus ([त्वम् अग्ने रुद्र असुरो महोदिवः ।]{style="color:#0000ff;"} or [भुवनस्य पितृ]{style="color:#0000ff;"}). In the Greek and Roman traditions, they are the son of Zeus or Jupiter maintaining that old connection going back to the Proto-Indo-European tradition and probably beyond to prehistoric times. In Greece, the memory of their Rudrian character is recorded in a 600-500 BCE stele from Sellasia in the Spartan realm where Plestiadas, a pious votary of the deities, inscribed a verse stating that he erected it "out of fear of the fury of the Tyndarid twins (the Dioskouroi)".

![Dioskouroi_Pius(2)](https://manasataramgini.wordpress.com/wp-content/uploads/2022/05/dioskouroi_pius2.jpg){width="75%"}\
Figure 1. A denarius of the Roman emperor Antoninus Pius showing the twin gods Castor and Pollux with the eagle of Jupiter between them. This iconography closely parallels that of their para-Vedic relatives Skanda and विशाख. The stars above them signify their association with the constellation of Gemini --- an ancient association also reflected in the तैत्तिरीय ब्राह्मण.

deũté moi nãson Pélopos lípontes,\
paĩdes íphthimoi [ड्íओस्]{style="color:#ff0000;"} ēdè [ळ्ḗदस्]{style="color:#ff0000;"},\
eùnóōi thúmōi prophánēte, [ःáस्तोर्]{style="color:#ff0000;"}\
kaì [Pओल्úदेउकेस्]{style="color:#ff0000;"},\
oì kát eùrēan khthóna kaì thálassan\
paĩsan èrkhesth' ṑkupódōn ep' ìppōn,\
rē̃a d' anthrṓpois thanátō rúesthe\
zakruóentos,\
eúsdúgōn thrṓiskontes ep' ákra náōn\
pḗlothen lámproi próton' ontrékhontes\
argaléai d' en núkti pháos phérontes\
nãï melaínai;

Come to me all the way from Pelops' island,\
powerful sons of Zeus and Leda,\
make your appearance with a kindly soul, Kastor\
and Polydeukes!\
You ride over the wide earth and the entire sea\
on your quick-footed horses;\
you rescue men with ease from death\
due to freezing,\
leaping from afar to the tops of their well-benched ships,\
shining brightly as you run up the forestays;\
to that in trouble in the night you bring light,\
to the ship in darkness.

We shall now consider both linguistic and philological equivalences with Sanskrit usages:\
• paĩdes = putra  $\to$  son; This occurs in the phrase "paĩdes íphthimoi Díos"  $\to$  the powerful sons of Zeus (and Leda). That parallels the phrase: [दिवो नपाता वृषणा]{style="color:#0000ff;"}: the manly offspring of Dyaus.\
• We render thúmōi as soul. The thumos is a cognate of धूम is Skt (= smoke/steam going back to PIE with same meaning). In Greek, one of its meanings, breath, is related to the original meaning, from which we get soul. The equation of soul and breath is also seen in H tradition: For instance, प्राण is called the "soul". The other Skt word आत्मन् is related to an old IE word for breath (e.g., German Atem= breath).\
• eùrēan khthóna  $\approx$  [उरुव्यछसम् पृथिवीम्]{style="color:#0000ff;"}. The first word is an exact cognate of uru = wide. The khthóna= क्ष्मा (क्ष)  $\to$  earth;\
• ṑkupódōn ep' ìppōn parallels the phrase used for the अश्विन्-s in RV 1.117.9 and RV 7.71.5: [आशुम् अश्वम्]{style="color:#0000ff;"}: swift horse; podon = padam = foot; आशु = ōku  $\to$  swift; ippos = अश्वः  $\to$  horse. A comparable phrase is used by गृत्समद शौनहोत्र in his spell for the chariot: [आशवः पद्याभिः]{style="color:#0000ff;"} in RV 2.31.2: with swift steps/feet.\
• rúesthe  $\approx$  रक्षथः; c.f. [रक्षेथे द्युभिर् अक्तुभिर् हितम्]{style="color:#0000ff;"} in RV 1.34.8: you protect through day and night. The protection at night is also mentioned in the Greek hymn(below).\
• núkti = nakta (0-graded to अक्ता) = night; pháos = भास्  $\to$  shine/light. phero > phérontes = bhara = to bear; náōn= नावम्  $\to$  boat. This protection offered to sailors by the Dioskouroi is mirrored in the marine rescue of Bhujyu stranded at sea that is mentioned in the श्रुति: [यद् अश्विना ऊहथुर् भुज्युम् अस्तं शतारित्रां नावम् आतस्थिवांसम् ।]{style="color:#0000ff;"} RV1.116.5: when, अश्विन्-s, you ferried Bhujyu to the shore after he mounted your ship of a hundred oars.

![Dioskouroi(3)](https://manasataramgini.wordpress.com/wp-content/uploads/2022/05/dioskouroi3.jpg){width="75%"}\
Figure 2. Castor and Pollux on a coin of the Roman republic with the ship on reverse.

