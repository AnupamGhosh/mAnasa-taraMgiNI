
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [RV 10.78](https://manasataramgini.wordpress.com/2022/11/01/rv-10-78/){rel="bookmark"} {#rv-10.78 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 1, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/11/01/rv-10-78/ "7:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

RV 10.77 and 10.78 are similarly themed सूक्त-s to the Marut-s by our ancient clansman स्यूमरश्मि भार्गव. He is mentioned twice by authors within the RV -- in RV 1.112.16 by Kutsa आङ्गिरस and in RV 8.52.2 by आयु काण्व. In the first instance, he is mentioned as being aided by the अश्विन्-s, and in the second he is mentioned as performing a soma sacrifice where he made offerings to Indra. Of his two सूक्त, we shall only consider 10.78 below. While the अनुक्रंअनि lists it as being composed of त्रिष्टुभ्-s and jagati-s, several ऋक्-s do not conform to those meters (the syllable count is given in brackets). Instead, in several of them, one hemistich is त्रिष्टुभ्-like and the other is jagati-like. Some, like the first ऋक्, conform to neither. It was perhaps an unusual meter that was lost in later Indo-Aryan tradition. It has been suggested that it might have been a मात्र meter like those from the later register of the language.

[विप्रासो न मन्मभिः स्वाध्यो]{style="color: #0000ff"}\
[देवाव्यो न यज्ञैः स्वप्नसः । (१८)]{style="color: #0000ff"}\
[राजानो न चित्राः सुसंदृशः]{style="color: #0000ff"}\
[क्षितीनां न मर्या अरेपसः ॥ १ (२१)]{style="color: #0000ff"}

Well-minded like vipra-s with mantra-thoughts,\
wealthy like those seeking the gods with rituals,\
beautiful in appearance like splendid kings,\
spotless like the young warriors of the nations...

[अग्निर् न ये भ्राजसा रुक्म-वक्षसो]{style="color: #0000ff"}\
[वातासो न स्वयुजः सद्य-ऊतयः । (२४)]{style="color: #0000ff"}\
[प्रज्ञातारो न ज्येष्ठाः सुनीतयः]{style="color: #0000ff"}\
[सुशर्माणो न सोमा ऋतं यते ॥ २ (२३)]{style="color: #0000ff"} (hypometrical jagati)

Who with golden ornaments on their chests blaze like Agni,\
like winds with their own yokemates bring instant aid,\
guides who like elders provide good council,\
who provide good protection like soma offerings to seekers of the law...

[वातासो न ये धुनयो जिगत्नवो]{style="color: #0000ff"}\
[।अग्नीनां न जिह्वा विरोकिणः । (२१)]{style="color: #0000ff"}\
[वर्मण्वन्तो न योधाः शिमीवन्तः]{style="color: #0000ff"}\
[पितॄणां न शंसाः सुरातयः ॥ ३ (२१)]{style="color: #0000ff"} (doubly hypometrical त्रिष्टुभ्)

Who like roaring winds move quickly,\
like the tongues of fires shine forth brightly,\
striving like armored warriors,\
liberal like the ancestors at the ritual lauds...

[रथानां न ये ।अराः सनाभयो]{style="color: #0000ff"}\
[जिगीवांसो न शूरा अभिद्यवः । (२१)]{style="color: #0000ff"}\
[वरेयवो न मर्या घृतप्रुषो]{style="color: #0000ff"}\
[।अभिस्वर्तारो अर्कं न सुष्टुभः ॥ ४ (२१)]{style="color: #0000ff"} (doubly hypometrical त्रिष्टुभ्)

Who, like the spokes of wheels, have the same nave (navel=source),\
like conquering brave warriors facing heaven,\
showering ghee like the young warriors wooing \[their bride= रोदसी],\
like chanters reciting the arka incantation...

[अश्वासो न ये ज्येष्ठास आशवो]{style="color: #0000ff"}\
[दिधिषवो न रथ्यः सुदानवः । (२२)]{style="color: #0000ff"} (त्रिष्टुभ्-like)\
[आपो न निम्नैर् उदभिर् जिगत्नवो]{style="color: #0000ff"}\
[विश्वरूपा अङ्गिरसो न सामभिः ॥ ५ (२४)]{style="color: #0000ff"} (jagati-like)

Who are swift like the best horses,\
good givers like the charioteers seeking a common bride [=रोदसी]\
like waters constantly moving with dense moisture,\
multiform like the अङ्गिरस्-es with their Saman-s...

[ग्रावाणो न सूरयः सिन्धुमातर]{style="color: #0000ff"}\
[आदर्दिरासो अद्रयो न विश्वहा । (२४]{style="color: #0000ff"}) (jagati-like)\
[शिशूला न क्रीऴयः सुमातरो]{style="color: #0000ff"}\
[महाग्रामो न यामन्न् उत त्विषा ॥ ६ (२२)]{style="color: #0000ff"} (त्रिष्टुभ्-like)

Liberal ones like soma-pressing stones, with the river as their mother,\
repeatedly smashing everything like rocks,\
playful like little children, they with a good mother,\
move like a great troop imbued with impetuosity...

[उषसां न केतवो अध्वरश्रियः]{style="color: #0000ff"}\
[शुभंयवो नाञ्जिभिर् व्य् अश्वितन् । (२२)]{style="color: #0000ff"} (त्रिष्टुभ्-like)\
[सिन्धवो न ययियो भ्राजदृष्टयः]{style="color: #0000ff"}\
[परावतो न योजनानि ममिरे ॥ ७ (२४)]{style="color: #0000ff"} (jagati-like)

Imparting auspiciousness to the ritual like the rays of the dawns,\
Shining forth with brilliance as if seeking auspiciousness,\
rushing like rivers, with blazing spears,\
as if they have measured out the yojana-s of the yonder realm...

[सुभागान् नो देवाः कृणुता सुरत्नान्]{style="color: #0000ff"}\
[अस्मान् स्तोतॄन् मरुतो वावृधानाः । (२३)]{style="color: #0000ff"} (hypometrical jagati-like)\
[अधि स्तोत्रस्य सख्यस्य गात]{style="color: #0000ff"}\
[सनाद् धि वो रत्नधेयानि सन्ति ॥ ८ (२१)]{style="color: #0000ff"} (hypometrical त्रिष्टुभ्-like)

O gods, make us the possessors of good shares and good gems,\
us reciters of chants to you O Marut-s, who have been eulogized,\
May you attend to our chant and friendship,\
for indeed since ancient times the gifting of gems has been yours.

The सूक्त has the structure of a riddle hymn, or a brahmodya, where the first 7 ऋक्-s are a series of similes. There are a total of 28 similes using na as the comparator, one per foot, each presenting an attribute of the deities of the सूक्त. This 4 x 7 pattern is perhaps an implicit acknowledgement of the 7-fold troops of the Marut-s. The सूक्त finally culminates in the answer to the riddle in ऋक्-8, where the name of the deities is revealed as the Marut-s. To cap it off, the pronoun नः (us) is used in the last ऋक्. to pair with the comparator na found in the rest. Another striking feature of the सूक्त is the repeated (12 times) use of words with the prefix su-, i.e., good or auspicious. Its count in each of the ऋक्-s is provided below:\
1 3\
2 2\
3 1\
4 2\
5 1\
6 1\
7 0\
8 2\
While the 7th does not feature such a word, it has two successive words, अध्वरश्रियः and शुभंयवः, which respectively feature श्री and शुभम्, both of which imply auspiciousness. We suspect this is intentional, with the build-up of 6 ऋक्-s with the su- prefix leading to ऋक्-7, where the author reveals his purpose by stating that they confer auspiciousness to the ritual. He then concludes by returning to the su- prefix in ऋक्-8 now that he has made apparent his intention in the previous one.

There are a few other notable features in this सूक्त:\
1. In ऋक्-5 the Marut-s are compared to the अङ्गिरस्-es singing सामन्-s. This brings to mind the [riddle सूक्त of father Manu](https://manasataramgini.wordpress.com/2012/09/23/the-vaishvadeva-riddle-of-manu/), where the same metaphor is used for the Marut-s: [अर्चन्त एके महि साम मन्वत तेन सूर्यम् अरोचयन् ।]{style="color: #0000ff"}

2. There are several direct and suggestive "linkages" between the ऋक्-s: 1 and 4 are linked by the word marya describing the Marut-s are young warriors. ऋक्-s 2 and 3 are linked by double similes comparing them to both Agni and the वात-s. The coupling of the Marut-s with Agni is an important feature of their membership in the Raudra-class, reflecting the duality of Agni and their father Rudra. This is presented in ritual in the form of the offerings accompanying the अग्निमारुत-शस्त्र (see RV 1.19). Their connection to the वात-s, is emphasized in the post-Vedic traditions starting with the रामायण -- मारुति as the son of वायु-वात and the पौराणिक identification of the Marut-s with the winds. This potentially reflects a parallel early IE tradition (c.f., the Greek reflex of the sparkling or swift-moving wind-deity Aeolus/Aiolos with this 12 stormy children). On the other hand, the connection to Agni (and also वायु in the [[Southern कौमार tradition]{style="color: #0000ff"}](https://manasataramgini.wordpress.com/2022/07/24/the-kaumara-cycle-in-the-skandapura%e1%b9%87as-sa%e1%b9%83kara-sa%e1%b9%83hita/)) is retained in the कौमार tradition of Skanda, the [para-Marut](https://manasataramgini.wordpress.com/2007/07/25/maruts-as-para-skanda-and-other-elements-of-their-mythology/). Further, the accouterments of the warrior (marya) are seen in ऋक्-s 2 and 3 -- the first has शर्मन् -- implying a helmet and the second has varman -- armor. ऋक्-4 refers to the arka and ऋक्-5 to the सामन् -- this probably reflects the combination of the शस्त्र and stotra recitation occurring in the soma offering to the Marut-s.

3. ऋक्-s 6 and 7 are linked by riverine similes. ऋक्-6 speaks of the matriline of the Marut-s -- they are said to have good maternity, implying पृष्णी. However, remarkably, they are also said to have the river as their mother. This is a rare phrase and in a non-metaphorical sense is only applied elsewhere in the RV to the other sons of Rudra, i.e., the अश्विन्-s (RV 1.46.2[: या दस्रा सिन्धु-मातरा मनोतरा रयीणाम् ।]{style="color: #0000ff"}). This strikingly parallels the birth of Skanda, often in a hexadic form, from the river in the later tradition. Notably, this motif also occurs in one of the narrations of the birth of गणेश, where he is born from the bathwater of पार्वती cast into the गङ्गा and drunk by the riverine elephant-headed goddess मालिनी (e.g., the Kashmirian मन्त्रवादिन् Jayaratha's हरचरितचिन्तामणि). Further, like the hexad of कुमार-s and the other कुमारक-s born of Rudra, in this ऋक्, the Marut-s are referred to as शिशूल-s (c.f., शिशु, the red-eyed, fierce companion of Skanda in the Skandopākhyāna of the महाभारत). Hence, we postulate that even in the core Vedic tradition there was an association between Rudra's progeny and the river mother. This could merely be a metaphor for पृष्णी, given her atmospheric nebular connections or represent her terrestrial ectype in the form of a river. This riverine connection also extends to the aquatic goddess सरवती, who is explicitly called the friend of the Marut-s (मरुत्सखा in RV 7.96.2) and epithet otherwise only applied to one other goddess, i.e., इन्द्राणी (RV 10.86.9).

4. ऋक्-s 4 and 5 are linked by the similes of the Marut-s wooing a bride -- वरेयवः and दिधिषवः. This is an allusion to their wooing of their common bride, रोदसी, who elsewhere in the RV is mentioned as riding in the chariot along with the Marut-s, gleaming like a beautiful lightning (RV 1.64.9) or the spears they bear (RV 1.167.3). This common wife of the Marut-s is reflected in the para-Marut कौमार tradition by the name of Skanda, भ्रातृस्त्रीकाम (AV परिशिष्ठ Skandayāga), i.e., an allusion to षष्ठी as the shared wife of Skanda and विशाख.

