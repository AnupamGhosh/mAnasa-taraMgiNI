
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A sampler of Ramanujan's elementary results and their manifold ramifications](https://manasataramgini.wordpress.com/2022/10/23/a-sampler-of-ramanujans-elementary-results-and-their-manifold-ramifications/){rel="bookmark"} {#a-sampler-of-ramanujans-elementary-results-and-their-manifold-ramifications .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 23, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/10/23/a-sampler-of-ramanujans-elementary-results-and-their-manifold-ramifications/ "6:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As we have remarked before, Ramanujan seemed as if channeling the world-conquering strides of विष्णु, when he single-handedly bridged the lacuna in Hindu mathematics from the days of the ब्राह्मण-s of the Cerapada to the modern era. Starting around the age of 16, he started recording his results in his now famous notebooks. Till that point, Ramanujan had access to only two primary educational sources: Plane trigonometry by Sydney Luxton Loney of Surrey and Synopsis of Pure Mathematics by George Shoobridge Carr of Middlesex which were used in the English educational system. The influence of Loney's opening pages filled with essential formulae and Carr's laconic presentations are writ large over the notebooks. It can be inferred that he had obtained many of the results while he was still at school, before the time he started writing them down in his notebooks. For instance, we hear that he had derived for himself the series expansions for trigonometric functions, which he only later learned to be common knowledge -- the is said to have then thrown away that early discovery in embarrassment. His first three notebooks were largely completed over a period of six years during his youth in India, with only some entries recorded in the time he was in England. When in England, he mentions that he was only intending to publish his current research, rather than those in the notebooks, until the World War I came to an end. Much of the so called "Lost Notebook" appears to have been written down in the final year of his short but momentous life. Unfortunately, it is believed that several other unpublished works of Ramanujan were entirely lost. Given the time range covered in the notebooks, we do find several elementary results that gives non-mathematicians like us a glimpse into the great man's mind. We provide below some discussion on a sampler of elementary results from his notebooks. The original entries of Ramanujan discussed in this note can be found in "Ramanujan's Notebooks, Part I-IV" by Bruce Berndt; here we express them or their corollaries in our own way.

** $\pi$  and squaring of a circle**\
Ramanujan is well-known for his numerous approximations of  $\pi$  both in a paper he published on the subject and the various entries in his notebooks. One of those leads to an approximate squaring of the circle that is eminently suitable for a modern श्रौत ritualist to construct an आहवनीय that is equal in area to the गार्हपत्य (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/ramanujan_squaring_circle_simple.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 1. Approximate quadrature of the circle by Ramanujan's formula

The said construction goes thus:\
1. Divide the radius of the circle into 5 equal parts.\
2. Extend the radius by 4 of these parts. This gives a segment of length  $\tfrac{9}{5}$ . Use that segment to construct a circle with diameter  $1+\tfrac{9}{5}$ .\
3. Apply the geometric mean theorem on that circle (Figure 1) to obtain a segment of length  $\sqrt{\tfrac{9}{5}}$ . Use that to construct a segment of length  $\tfrac{9}{5}+\sqrt{\tfrac{9}{5}}$ . With that segment construct a circle of diameter  $1+ \tfrac{9}{5}+\sqrt{\tfrac{9}{5}}$  (Figure 1).\
4. Apply the geometric mean theorem on that segment to obtain the side of the desired square.

One can see that this construction corresponds to Ramanujan's approximation:  $\pi \approx \tfrac{9}{5}+\sqrt{\tfrac{9}{5}}$ . This is very close to आर्यभट's approximation:  $\pi \approx \tfrac{62832}{20000} = \tfrac{3927}{1250}$ . People have claimed that आर्यभट arrived at his value by using polygons to approximate a circle. There is absolutely no evidence for this claim making one wonder if he had somehow arrived at a formula like that of Ramanujan. It remains unknown to me if Ramanujan had found some special connections relating to this value. The same value is also recommended as a correction to the very approximate Bronze Age values by द्वारकानाथ Yajvan, a medieval श्रौत ritualist and commentator on the शुल्बसूत्र of बौधायन. This value is somewhat less accurate than another ancient value  $3\tfrac{16}{113}$  recorded by वीरसेन and in some भास्कर-II manuscripts ([Ramanujan also provides a construction for the quadrature using that approximation):](https://manasataramgini.wordpress.com/2011/12/21/squaring-of-the-circle-by-srinivasa-ramanujan/)

[व्यासं षोडश-गुणितं त्रि-रूप-रूपैर्-भक्तम् ।]{style="color: #0000ff"}\
[व्यासं त्रिगुणितं सूक्ष्माद् अपि तद् भवेत् सूक्ष्मम् ॥]{style="color: #0000ff"}

**The relationship between the reciprocals of odd numbers and  $\pi$ **

 $4n-3$  defines the alternate odd numbers: 1, 5, 9, 13, 17, 21, 25, 29, 33, 37...\
 $4n-1$  defines the remaining odd numbers absent in the above sequence: 3, 7, 11, 15, 19, 23, 27, 31, 35, 39...\
There is an interesting relationship between the reciprocals of these two sets of odd numbers and  $\pi$ :

 $$\displaystyle \pi = 4\sum_{n=1}^{\infty} \left( \dfrac{1}{4n-3} - \dfrac{1}{4n-1}\right)$$ 

It is an interesting though not very efficient formula for  $\pi$  reaching 3.1 after 13 terms. This relationship can be obtained from the general cotangent relationship, which is valid for any number  $z$  (including the complex plane) that Ramanujan discovered for himself:

 $$\displaystyle \pi\cot(\pi z) = \sum_{n=1}^{\infty} \left(\dfrac{1}{n-1+z} - \dfrac{1}{n-z}\right)$$ 

Remarkably, Ramanujan records this after a result related to the zeta function, which in turn implies the famous series for the digamma function  $\psi(x)$ , i.e., the ratio of the derivative of the gamma function to the gamma function:

 $$\displaystyle \psi(x+1)=\dfrac{\Gamma'(x+1)}{\Gamma(x+1)}=\sum_{n=1}^{\infty} \left(\dfrac{1}{n}-\dfrac{1}{n+x}\right) -\gamma$$ 

Here  $\gamma$  is Euler's constant.

** $\sqrt{10}$ , cubes of triangular numbers and  $\pi$ **
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/demos_ramanujan_pi_10.png){width="75%"}
```{=latex}
\end{center}
```



**Figure 2.  $10-\pi^2$  **

In old India (e.g., Brahmagupta and the Jaina प्रज्ञाप्ति texts) we find  $\sqrt{10}$  as an approximation for  $\pi$ . This is interestingly close to the Egyptian approximation  $\left(\tfrac{16}{9}\right)^2$ . We can ask the converse question of how close is  $\pi^2$  to 10 (Figure 2). Ramanujan discovered an interesting answer for this.

The triangular numbers,  $T_n$ , are the sums of successive integers up to  $n$ , i.e,  $T_n = \tfrac{n^2+n}{2}$ : 1, 3, 6, 10... Then,

 $$\displaystyle 10-\pi^2 = \dfrac{1}{8}\sum_{n=1}^{\infty} \dfrac{1}{T_n^3}$$ 

**Reciprocals of the cubes of odd numbers and  $\zeta(3)$ **\
The zeta function elicits an almost mystical experience in us --- when you realize how it connects what were seen as disparate branches of mathematics you get a sense of the deep order in the Platonic realm. The function was first discovered by Euler in course of solving what was called the [Basel problem](https://manasataramgini.wordpress.com/2018/02/03/our-auto-discovery-of-the-mobius-and-mertens-sequences/). It was known (probably since antiquity) that the sum of the reciprocal of integers slowly diverges to  $\infty$ :

 $$\displaystyle \sum_{n=1}^{\infty} \dfrac{1}{n} =\infty$$ 

This can be proved easily with a basic school level mathematics using the comparison test to the reciprocals of the powers of 2 bounding each interval of integer reciprocals (i.e.,  $\tfrac{1}{3}> \tfrac{1}{4}; \tfrac{1}{5}, \tfrac{1}{6}, \tfrac{1}{7} > \tfrac{1}{1/8}$  so on). However, the question of the sum of the reciprocals of the squares of integers defied attempts of brilliant mathematicians, such as the Bernoulli clan (hence, the Basel problem), until it fell to Leonhard Euler in 1734 CE. Thus, the zeta function can be defined as a generalization of such sums for any number  $z$  on the complex plane:

 $$\displaystyle \zeta(z) = \sum_{n=1}^{\infty} \dfrac{1}{n\^z}$$ 

While Euler originally defined it for positive integers, it was generalized as above by Chebyshev on the real line and then by Bernhard Riemann on the complex plane. Thus, the Basel problem is essentially the value of the zeta function at 2, which Euler proved to be  $\zeta(2) = \tfrac{\pi^2}{6}$ . Euler subsequently established that the reciprocal of  $\zeta(2)$  gives the probability of two integers drawn at random from the interval between  $n_1$  and  $n_2$  being mutually prime (i.e., having GCD=1). This suggested the link between the zeta function and primality, and finally, three years after solving the Basel problem Euler showed the explicit link between prime numbers and the zeta function by his product formula:

 $\displaystyle \zeta(z) = \sum_{n=1}^{\infty} \dfrac{1}{n\^z} = \prod_{p} \dfrac{1}{1-\dfrac{1}{p\^z}}$ ,

where the product is over all primes.

From the subsequent work culminating in Riemann's famous hypothesis, the relationship between the zeros of the zeta function and the prime number distribution became clear. Remarkably, Ramanujan discovered many aspects of the zeta function all by himself unaware of the developments in the west, such as those of Chebyshev and Riemann. Among other things, was his much-ridiculed result, where he provided the sum of integers (1+2+3+4...) as a finite negative number  $-\tfrac{1}{12}$  (found in his first notebook and communication with Godfrey H. Hardy) --- this was essentially his auto-discovery of the value of  $\zeta(-1)$ . He also discovered for himself the connection between the zeta function and prime numbers. He discovered that the zeta function displays an oscillatory behavior on the negative real line, taking the value 0 at all even negative numbers (-2, -4, -6...). He used these zeros to derive the distribution of primes, paralleling the work of Riemann. However, he overestimated the accuracy of his results for he was unaware of further zeros discovered by Riemann on the complex plane that he only learnt of from Hardy when he went to England. From his notebooks, we learn that during the Indian phase of his career, like Chebyshev, he also explored the values of the zeta function on the real line beyond 2. Thus, Ramanujan discovered a general formula, one of whose special cases is a series specifying  $\zeta(3)$  that is today sometimes called Apéry's constant. After the days of Ramanujan, this constant has appeared in several areas of physics.

 $$\displaystyle \zeta(3)=\dfrac{8}{7} \sum_{n=0}^{\infty} \dfrac{1}{(2k+1)^3}$$ 

Now,  $\zeta(3)$  is also the area under the below curve for positive  $x$  (Figure 3), thereby giving an alternative integral formula for the sum based on Ramanujan's formula.

 $$y= \dfrac{x^{2}}{2\left(e^{x}-1\right)}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/demos_ramanujan_zeta.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 3.  $\zeta(3)$ 

However, Ramanujan's formula more generally goes on to provide several other series that link the cubes of the reciprocals of numbers separated by 3 (1, 4, 7, 10...), 4 (1, 5, 9, 13...) so on which have the general form  $k_1 \pi^3+k_2 \zeta(3)$ , where  $k_1, k_2$  are constants specific to each sum:

 $$\displaystyle \sum_{n=0}^{\infty} \dfrac{1}{(3k+1)^3} = \dfrac{1}{27}\left(\dfrac{2}{3\sqrt{3}}\pi^3+13\zeta(3)\right)$$ 

 $$\displaystyle \sum_{n=0}^{\infty} \dfrac{1}{(4k+1)^3} = \dfrac{1}{16}\left(\dfrac{1}{4}\pi^3+7\zeta(3)\right)$$ 

We were puzzled by what might be the sum of the reciprocals of cubes of numbers separated by 5: 1, 6, 11, 16... Taking the limit as  $n \to \infty$  of the digamma derivative-based series formula we established that this can be expressed in a rather compact form with the tetragamma function, i.e., the second derivative of  $\psi(x) \Rightarrow \psi^{(2)}(x)$ :

 $$\displaystyle \sum_{n=0}^{\infty} \dfrac{1}{(5k+1)^3} = -\dfrac{1}{250} \psi^{(2)}\left(\dfrac{1}{5}\right) \approx 1.0059121444577$$ 

**The Ramanujan primorial plus one fourth sequence**\
The prime numbers 2, 3, 5, 7, 11... are denoted by  $p_1, p_2, p_3, p_4, p_5 \dots$ . By analogy to the factorial product, one can define the primorial as the product of successive primes:

 $$\displaystyle p_n# = \prod_{k=1}^{n} p_k$$ 

Ramanujan defines a sequence such that  $2 \sqrt{p_n# + \tfrac{1}{4}}$  is an odd integer. This holds for the below values of  $n$ :

![Ramanujan_primorial](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/ramanujan_primorial.png){width="75%"}

It remains unknown to us if there is any further term in this sequence and if there is one, how many more exist?

**Elementary results relating to powers of numbers**\
Ramanujan provides numerous elementary results relating to the sums of the powers of numbers that he likely derived when he was still in school. One of the simplest is the following, which, however, was apparently unknown until his discovery:\
if  $ad=bc$  then for  $n=2, 4$ :

 $$(a+b+c)\^n+(b+c+d)\^n+(a-d)\^n = (c+d+a)\^n +(d+a+b)\^n+(b-c)\^n$$ 

Thus, it is a parametrization that allows one to find six numbers such that the sum of the squares and the fourth powers respectively of the first 3 is equal to those of the last 3.

  - --- ---- ---- ---- ---- ----
     1    9   10    5    6   11
     2   11   13    7    7   14
     3   13   16    9    8   17
     4   15   19   11    9   20
     5   17   22   13   10   23
     6   19   25   15   11   26
     7   21   28   17   12   29
     8   23   31   19   13   32
     9   25   34   21   14   35
    10   27   37   23   15   38
    11   29   40   25   16   41
    12   31   43   27   17   44

  - --- ---- ---- ---- ---- ----

With this parametrization, we can obtain the above hexad where the first term is every positive integer; the second term is every odd integer starting with 9; the fourth term is every odd integer starting with 5; the fifth term is every positive integer starting with 6. The third and sixth terms are the sums of the previous two terms. The sum of the squares of the two triads constituting these hexads will be defined by:  $14n^2 + 70n + 98; \; n=1, 2, 3 \dots$  The sum of the 4th powers is given by  $98 (n^4+ 10 n^3 + 39 n^2+ 70 n +49 )$ .

The next problem in this genre is to find rational solutions to the indeterminate equations:

 $$2w^2=x^4+y^4+z^4 \; ; \; 2w^4=x^4+y^4+z^4 \; ; \; 2w^6=x^4+y^4+z^4$$ 

Ramanujan gives parametrizations to solve such equations: if  $a+b+c=0$  then,

 $$2(ab+bc+ac)^2 = a^4 +b^4+c^4 \dots \S 1$$ 

 $$2(ab+bc+ac)^4 = (a(b-c))^4+(b(c-a))^4+(c(a-b))^4 \dots \S 2$$ 

 $$2(ab+bc+ac)^6 = (a^2b+b^2c+c^2a)^4+(ab^2+bc^2+ca^2)^4+(3abc)^4 \dots \S 3$$ 

The equation  $\S 1$  is quite trivial. For the equation  $\S 2$ , using Ramanujan's parametrization one can obtain several sets of tetrads. Below is an example where we take  $a$  to be successive integers starting from 0 and  $b$  to be 1 more than  $a$ :

 $$a=0,1,2,3\dots; b=a+1$$ 

      w     x    y     z

  - ---- ----- ---- -----
      1     0    1     1
      7     5    3     8
     19    16    5    21
     37    33    7    40
     61    56    9    65
     91    85   11    96
    127   120   13   133
    169   161   15   176
    217   208   17   225
    271   261   19   280

For this tetrad, one sees that  $y$  is the sequence of odd numbers.  $w$  (first column) are the [hex numbers](https://manasataramgini.wordpress.com/2017/10/23/triangles-hexes-and-cubes/), i.e., the centered hexagonal numbers given by the quadratic expression  $3n^2+3n+1$ . The sequence defined by  $w$  also defines the maximum number of bounded areas you can obtain by drawing triangles on a plane: With 1 triangle you can obtain at most 1 bounded area; with 2 you can obtain at most 7 (hexastar); with 3 you obtain 19 and so on.  $x$  (second column) is the square star numbers (Figure 4), i.e., square numbers with triangular numbers on each side, given by the quadratic expression  $3n^2+2n$ , while  $z$  (fourth column) are the square grid numbers given by the expression  $3n^2-2n$  (Figure 5).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/square_star_numbers.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 4. Square star numbers.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/square_grid_numbers.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 5. Square grid numbers.

Notably, these three columns are also linearities on the hexadic spiral (Figure 6).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/hexadic_spiral.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 6. The hexadic spiral. The 3 linearities in blue boxes correspond to 3 of the columns in the above parametrization

We can again use Ramanujan's parametrization for  $\S 2$  with  $a=1$  and  $b$  as successive integers starting with 0. This yields the below sequence of tetrads:

     w    x    y    z

  - --- ---- ---- ----
     1    1    1    0
     3    3    0    3
     7    5    3    8
    13    7    8   15
    21    9   15   24
    31   11   24   35
    43   13   35   48
    57   15   48   63
    73   17   63   80
    91   19   80   99

The sequence corresponding to  $w$  in this tetrad is specified by  $n^2 - n + 1$ . Remarkably, this sequence appears in multiple geometric contexts. One is the Euler (Venn) diagram problem. It is an analog of the problem of the maximum number of bounded areas obtained with triangles. What is the maximum number of bounded compartments you can represent using circles (Figure 7)? The answer is this sequence.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/ramanujan_venn_diag_param.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 7. The Euler diagram problem.

A further geometric context relates to the sequence of triangles where one side is  $1$ , the second side is  $1, 2, 3 \dots$  and the angle between these two sides is  $\tfrac{\pi}{3}$ . Then the squares on the third sides of this sequence of triangles will have an area equal to the sequence  $w$  (Figure 8). In this tetrad  $x$  is the sequence of odd numbers,  $y$  takes the form  $n^2 - 2n$  and  $z$  is the same sequence as  $y$  offset by 1 in the backward direction. This sequence appears in the so-called Monty Hall problem discussed by Martin Gardner many years ago illustrating the difficulty of understanding probability even in simple problems.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/10/ramanujan_sequence_triangle.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 8\quad The length of the third side of the 60-degree triangle problem.
```{=latex}
\end{center}
```


