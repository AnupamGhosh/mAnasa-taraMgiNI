
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On the rise of the मातृका-s and the goddess चामुण्डा](https://manasataramgini.wordpress.com/2022/02/08/on-the-rise-of-the-mat%e1%b9%9bka-s-and-the-goddess-camu%e1%b9%87%e1%b8%8da/){rel="bookmark"} {#on-the-rise-of-the-मतक-s-and-the-goddess-चमणड .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 8, 2022]{.entry-date}](https://manasataramgini.wordpress.com/2022/02/08/on-the-rise-of-the-mat%e1%b9%9bka-s-and-the-goddess-camu%e1%b9%87%e1%b8%8da/ "7:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**The roots of the मातृका-s in the श्रुति and the कौमार tradition**\
The standard list of 7/8 goddesses known as the मातृका-s is a hallmark feature of the classical religion: ब्राह्मी, माहेश्वरी, कौमारी, वैष्णवी, वाराही, इन्द्राणी, चामुण्डा, sometimes with the central चण्डिका (महालक्ष्मी), to make a list of 8. Most of them are the female counterparts of the prominent gods of the classical pantheon. These goddesses have a relatively constant iconography throughout the Indosphere, starting from the Gupta age. Despite this relatively late arrival on the iconographic landscape, they have deep roots going back to the ancestral Indo-European religion and its earliest Indo-Aryan manifestation. Right in the ऋग्वेद, we have the incantations for the पत्नी-संयाज or the worship of the goddesses at the गार्हपत्य altar (e.g., RV 2.32.4-8; RV 5.46.7-8), along with Agni गृहपति. Similar ऋक्-s are also seen in the Atharvavedic tradition. Three types of goddesses can be discerned in these incantations: 1) The great trans-functional goddess of proto-Indo-European provenance, सरस्वती; 2) The deva-पत्नी-s or the wives/female counterparts of the gods; 3) The "[lunar goddesses](https://manasataramgini.wordpress.com/2006/11/01/ekanamsha-in-nastika-myth-making-and-implications-for-astika-paurano-tantric-history/)", कुहू (गुङ्गू), Sinīvālī, राका, अनुमती. Below we provide an Atharvavedic version of these incantations (the subset which is found in RV is effectively the equivalently).

[या राका या सिनीवाली या गुङ्गूर् या सरस्वती ।]{style="color:#0000ff;"}\
[इन्द्राणीम् अह्व ऊतये वरुणानीं स्वस्तये ॥]{style="color:#0000ff;"}\
I call she who is, Sinīvālī, who is गुङ्गू, who is राका, who is सरस्वती, for my aid I call इन्द्राणी, and वरुणाणी for my well-being.

[सिनीवालीम् अनुमतीं राकां गुङ्गूं सरस्वतीम् ।]{style="color:#0000ff;"}\
[देवानां पत्नीर् या देवी इन्द्राणीम् अवसे हुवे ॥]{style="color:#0000ff;"}\
I invoke for protection Sinīvālī, अनुमती, राका, गुङ्गू, and सरस्वती, the wives of the gods, and she who is the goddess इन्द्राणी.

[सेनासि पृथिवी धनंजया अदितिर् विश्वरूपा सूर्यत्वक् ।]{style="color:#0000ff;"}\
[इन्द्राणी प्राषाट् संजयन्ती तस्यै त एना हविषा विधेम ॥]{style="color:#0000ff;"}\
You are सेना (the goddess of the divine army), the Earth, the conqueress of wealth, Aditi, multiformed, and sun-skinned. O इन्द्राणी, \[for you] the प्राषाट् call, O all-conquering one; we pay homage to her with this offering.

[उत ग्ना व्यन्तु देवपत्नीर् इन्द्राण्य१ ग्नाय्य् अश्विनी राट् ।]{style="color:#0000ff;"}\
[आ रोदसी वरुणानी शृणोतु व्यन्तु देवीर् य ऋतुर् जनीनाम् ॥]{style="color:#0000ff;"}\
May the goddesses, the wives of the gods, come, इन्द्राणी, अश्विनी, अग्नायि, and the Queen. May रोदसी \[wife of the Marut-s] and वरुणाणी hear us, and the goddesses come to the ritual of the mothers.

[या विश्पत्नीन्द्रम् असि प्रतीची सहस्र-स्तुकाभियन्ती देवी ।]{style="color:#0000ff;"}\
[विष्णोः पत्नि तुभ्यं राता हवींषि पतिं देवि राधसे चोदयस्व ॥]{style="color:#0000ff;"}\
The Queen of the folks, you are Indra's equal, the goddess with a thousand tresses, coming to us. O wife of विष्णु, to you, these offerings \[are] made. O goddess, urge your husband to be liberal \[towards us].
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/indrani_nepalian.jpg){width="75%"}
```{=latex}
\end{center}
```



*A five-headed इन्द्राणी from a Nepalian मातृका-पूजा manuscript*

Of these goddesses, इन्द्राणी and विष्णुपत्नी (वैष्णवी) are featured in most classic मातृका lists of the later religion. Another goddess from the classical मातृका list, रुद्राणी, is seen in यजुष् incantations of the तैत्तिरीय and कठ schools of the कृष्ण-yajurveda. The goddess सेना is associated with the divine army. In the कृष्ण-Yajurvedic कठ and मैत्रायणि schools, सेना identified with इन्द्राणी: [इन्द्राण्यै चरुं निर्वपेत् सेनायाम् उत्तिष्ठन्त्याम् । स्éना वा इन्द्राणी ।]{style="color:#0000ff;"} This association continues in the late Yajurvedic incantation known as the आयुष्य-सूक्त (बोधायन-mantra-प्रश्न), where the same goddess is explicitly termed इन्द्रसेना. In later tradition, she is seen as the wife of Skanda (देवसेना), who is also mentioned in the आयुष्य-सूक्त by her other name षष्ठी.

A persistent pattern in these Vedic rituals is the presence of a single male god accompanying a cluster of goddesses. As noted above, Agni गृहपति is the single male deity who accompanies the goddesses receiving offerings in the पत्नी-संयाज. The RV also repeatedly states that the wives of the gods arrive at the ritual accompanied by the god त्वष्टृ (RV 1.22.9; 1.161.4; 2.31.4; 2.36.3; 6.50.13; 7.34.20; 7.34.22; 7.35.6; 10.18.6; 10.64.10; 10.66.3) Additionally, in the तैत्तिरीय school we have two versions of the देविका oblations. In the first of them, the god त्वष्टृ accompanies the goddesses सरस्वती and Sinīvālī. In the second, the god धातृ (a later ectype of त्वष्टृ \[Footnote 1]) is invoked together with the goddesses Aditi, अनुमती, राका, Sinīvālī and गुङ्गू. This pattern may be seen as analogous to the usual situation, wherein several male gods associated with different functions are spanned by a single transfunctional goddess --- सरस्वती \[Footnote 2]. Here, a single male generative deity is associated with a multi-functional cluster of goddesses who actualize the dormant generative capacity of the former. While the पत्नी-संयाज associated with the शुलगव ritual of the शुक्ल-yajurveda invokes the goddesses with Agni गृहपति as usual, it presents a unique set of them (पारस्कर-गृह्य-सूत्र 3.8):

[शूलगवः ।]{style="color:#0000ff;"}\
The impaled bull \[sacrifice]

[स्वर्ग्यः पशव्यः पुत्र्यो धन्यो यशस्य आयुष्यः ।]{style="color:#0000ff;"}\
It procures heaven, cattle, sons, riches, renown, long life.

औपासनम् अरण्यं हृत्वा वितानं साधयित्वा रौद्रं पशुम् आलभेत ।\
Having taken the औपासन fire to the forest, and having performed the spreading \[of grass], he should obtain the animal for Rudra.

[साण्डम् ।]{style="color:#0000ff;"}\
With testicles (i.e., not castrated)

[गौर् वा शब्दात् ।]{style="color:#0000ff;"}\
Or a cow as the name \[of the ritual specifies]

[वपां श्रपयित्वा स्थालीपाकम् अवदानानि च रुद्राय वपाम् अन्तरिक्षाय वसां स्थालीपाक-मिश्रान्यवदानानि जुहोत्य् अग्नये रुद्राय शर्वाय पशुपतये उग्रायाशनये भवाय महादेवायेशानायेति च ।]{style="color:#0000ff;"}\
Having cooked the omentum, a plate of rice, and the cuts from \[the victim], he offers the omentum to Rudra, the fat to the atmosphere, and the cuts of meat with the rice to Agni, Rudra, शर्व, पशुपति, Ugra, अशनि, Bhava, महादेव and ईशान.

[वनस्पतिः ।]{style="color:#0000ff;"}\
The Vanaspati (offering to the sacrificial post is made).

[स्विष्टकृद् ante ।]{style="color:#0000ff;"}\
At the end \[offerings are made to Agni] स्विष्टकृत्.

[dig व्याघारणम् ।]{style="color:#0000ff;"}\
Then the sprinkling to the directions \[is performed].

[व्याघारणान्ते पत्नीः संयाजयन्तीन्द्राण्यै रुद्राण्यै शर्वाण्यै भवान्या अग्निं गृहपतिम् इति ।]{style="color:#0000ff;"}\
At the end of the sprinkling the offer the पत्नी-संयाज oblations to इन्द्राणी, रुद्राणी, शर्वाणि, भवानी, and Agni गृहपति.

[लोहितं पालाशेषु कूर्चेषु रुद्राय सेनाभ्यो बलिं हरति यास् ते रुद्र पुरस्तात् सेनास् ताभ्य एष बलिस् ताभ्यस् ते नमो यास् ते रुद्र दक्षिणतः सेनास् ताभ्य एष बलिस् ताभ्यस् ते नमो यास्ते रुद्र पश्चात् सेनास् ताभ्य एष बलिस् ताभ्यस् ते नमो यास् ते रुद्रोत्तरतः सेनास् ताभ्य एष बलिस् ताभ्यस् ते नमो यास् ते रुद्रोपरिष्टात् सेनास् ताभ्य एष बलिस् ताभ्यस् ते नमो यास् ते रुद्राधस्तात् सेनास् ताभ्य एष बलिस् ताभ्यस् ते नम इति ।]{style="color:#0000ff;"}\
He \[then] offers the blood \[of the sacrificed bull] as bali with a bunch of *Butea frondosa* leaves to Rudra and his troops with \[the incantations]: "O Rudra, those armies, which you have to the East (to the South; to the West; to the North; upwards; downwards), to them is this bali. Obeisance be to them and to you."

[ऊवध्यं लोहित-लिप्तम् अग्नौ प्रास्यत्य् अधो वा निखनति ।]{style="color:#0000ff;"}\
He casts the gut and the blood-smeared remains into the fire or buries them beneath.

[अनुवातं पशुम् अवस्थाप्य रुद्रैर् उपतिष्ठते प्रथमोत्तमाभ्यां वा ।अनुवाकाभ्याम् ।]{style="color:#0000ff;"}\
Having \[placed the remains of] the animal such that the wind blows from himself to it, he goes towards it by \[reciting] the Rudra incantations, or the first and last अनुवाक \[i.e., शतरुद्रीय].

[नैतस्य पशोर् ग्रामं हरन्ति ।]{style="color:#0000ff;"}\
They do not take anything of the animal to the village.

[एतेनैव गोयज्ञो व्याख्यातः ।]{style="color:#0000ff;"}\
By this the cow-sacrifice is also expounded.

[पायसेनानर्थ-लुप्तः ।]{style="color:#0000ff;"}\
It is done with an offering of milk, and the \[rituals] not meant for it are omitted.

[तस्य तुल्यवया गौर् दक्षिणा ।]{style="color:#0000ff;"}\
A cow of the same age (as the sacrificed animal) is the ritual fee.}

Thus, the पत्नी-संयाज of the शुलगव departs from the classical one in combining इन्द्राणी with the three raudra goddesses, रुद्राणी, शर्वाणि and भवानी. Hence, we are already seeing a hint of the tendencies in the मातृका system of the classical religion, wherein the माटृका-s typically have an explicitly raudra connection. Indeed, this triad of raudra goddess might indicate a connection to the old name of Rudra, Tryambaka, which implies his association with three mothers. Other than the above-mentioned male deities coming with a cluster of goddesses, in the late तैत्तिरीय tradition of the बोधायन-मन्त्रप्रश्न, we see Skanda appearing with a cluster of 12 माटृका-s:

[अघोराय महाघोराय नेजमेषाय नमो नमः ॥]{style="color:#0000ff;"}\
[आवेशिनी ह्य् अश्रुमुखी कुतुहली हस्तिनी जृंभिणी स्तम्भिनी मोहिनी च ।]{style="color:#0000ff;"}\
[कृष्णा विशाखा विमला ब्रह्मरात्री भ्रातृव्यसंघेषु-पतन्त्य् अमोघास् ताभ्यो वै मातृभ्यो नमो नमः ॥]{style="color:#0000ff;"}

Similarly, another late तैत्तिरीय tradition, the वैखानस-मन्त्रप्रश्न, provides an incantation for a set of goddesses who are said to be born of Guha (i.e., Skanda) and said to bear the गण of Rudra:

[ज्वाला माला गुम्भिनी गुह-जाता रौद्रं गणं या बिभृयात् सुरूपी स्वाहा ॥]{style="color:#0000ff;"} VP 6.36.5

A second incantation describes a set of goddesses, including the one who bears Skanda:

[मोही विमोही विमुखी गुह-धारिणी च निद्रा च देवी विरजास् तु भूत्यै स्वाहा ॥]{style="color:#0000ff;"} VP 6.37.9

Starting from its late Vedic roots, the association of Skanda with the मातृ-s was remembered over a prolonged period in Indian historical tradition. For instance, it figures in the famous मङ्गलाचरण of the चालुक्य monarchs where they describe themselves thus: [...मातृ-गण-परिपालितानां स्वामि-महासेन-पादानु-ध्यातानाम्...]{style="color:#0000ff;"} Protected by the troop of मातृ-s and meditating on the feet of lord महासेन. This association of a cluster of goddesses with Skanda is further developed in the महाभारत:

[ततः संकल्प्य पुत्रत्वे स्कंदं मातृगणो ।अगमत् ।]{style="color:#0000ff;"}\
[काकी च हलिमा चैव रुद्राथ बृहली तथा ।]{style="color:#0000ff;"}\
[आर्या पलाला वै मित्रा सप्तैताः शिशुमातरः ॥]{style="color:#0000ff;"} Mbh 3.217.9 ("critical")\
Having established the sonship of Skanda, the band of मातृ-s went away: काकी, हलिमा, रुद्रा, बृहली, आर्या, पलाला and मित्रा, these are the seven mothers of शिशु.

A similar list is given in the "mega"-Skandaपुराण:

[काकी च हिलिमा चैव रुद्रा च वृषभा तथा ।]{style="color:#0000ff;"}\
[आर्या पलाला मित्रा च सप्तैताः शिशुमातरः ॥]{style="color:#0000ff;"} SP-M 1.2.29.175-76

Notably, in this cluster of goddesses, we see, for the first time, an explicit list of 7 मातृ-s --- a number that became characteristic of the classical group of मातृका-s. In subsequent narrations of the कौमार cycle in the [medical](https://manasataramgini.wordpress.com/2008/09/08/kaumara-material-in-the-ashtangasamgraha-a-rambling-discussion/) and पौराणिक traditions, the number of मातृ-s who accompany Skanda is numerous and varied. Nevertheless, one of the archaic versions from the Mbh we encounter for the first time the description of these मातृ-s as having the form of female versions of the gods:

[याम्यो रौद्र्यस् तथा सौम्याः कौबेर्यो ।अथ महाबलाः ।]{style="color:#0000ff;"}\
[वारुण्यो ।अथ च माहेन्द्र्यस् तथाग्नेय्यः परंतप ॥]{style="color:#0000ff;"}\
[वायव्यश् चाथ कौमार्यो ब्राह्म्यश् च भरतर्षभ ।]{style="color:#0000ff;"}\
[वैष्णव्यश् च तथा सौर्यो वाराह्यश् च महाबलाः ।]{style="color:#0000ff;"} \[missing in some recensions]\
[वैष्णव्यो ।अतिभयाश् चान्याः क्रूररूपा भयंकराः ।]{style="color:#0000ff;"} \[Alternative for above]\
[रूपेणाप्सरसां तुल्या जवे वायुसमास् तथा ॥]{style="color:#0000ff;"} Mbh 9.45.35-36\
O scorcher of foes, possessed of great might \[these goddesses took the forms] of Yama, Rudra, Soma, Kubera, वरुण, the great Indra and Agni. O bull among the Bharata-s, yet others of great might took the form of वायु, कुमार, Brahman, /विष्णु, सूर्य, वाराही/विष्णु and other terrifying and fierce forms evoking great terror./ Endowed with the beauty of Apsaras-es, they were possessed of the speed of wind.

Thus, within this कौमार context, we see the first expression of a मतृ list converging on the classical system. कौमार-मातृ-s appear first in [iconography](https://manasataramgini.wordpress.com/2009/09/21/chaturdasha-skanda-matri-kula-chakra/) in the company of Skanda in कुषण and pre-कुषण sites like the holy city of Mathura. Even the earliest iconographic exemplars of the 7 classical मातृ-s are connected to Skanda (Gupta age; see also the Patna inscription which speaks of कुमारगुप्त's brother-in-law installing a shrine for Skanda at the head of the मतृ-s). However, in the post-Gupta age, the connection of कुमार and the classical मातृ-s gradually faded away. However, the old Vedic template of a multiplicity of goddesses in the company of a single god continued to be expressed. Among the gods of taking the place of Skanda in the मातृका panels were Kubera, Rudra, both in his classic form and as Tumburu, वीरभद्र and गणेश. Of these, as we have noted before, the association with Rudra is archaic, having Vedic roots. It is reiterated in the Mbh:

[वनस्पतीनां पतये नराणां पतये नमः ।]{style="color:#0000ff;"}\
[मातॄणां पतये चैव गणानां पतये नमः ।]{style="color:#0000ff;"}\
[अपां च पतये नित्यं यज्ञानां पतये नमः ॥]{style="color:#0000ff;"} Mbh 7.173.38\
Salutations to the lord of the trees, men, the मातृ-s, the गण-s, the waters, and ever the lord the rituals.

Indeed, a large host of 190 ferocious therocephalic and avicephalic मातृका-s, comparable to those accompanying Skanda, is described as being generated by Rudra to drink up the blood of Andhaka in the मत्स्यपुराण. In the same narrative, नृसिंह also generates 32 मातृका-s to pacify the former. These latter goddesses are the अर्ण-देवी-s of the famed 32-syllabled mantra of नृसिंह. A band of पिशाची-s, who eat up the corpse of the daitya हालाहल killed by the गण-s of नीललोहित-rudra, when he comes to take the skull of Brahman, are also known as कपालमातृ-s and associated with Rudra as per the proto-Skandaपुराण (7.15-23). The association between Rudra and the मातृका-s is also emphasized in the historical record by the famous Gupta age inscription from Bagh, Madhya Pradesh, which mentions the पासुपत teacher, Lokodadhi, installing a temple that is the station of the मातृ-s in the village of पिञ्च्छिकानक: [... भगवल् लोकोदधि पासुपताचार्य-प्रतिष्ठापितक-पिञ्च्छिकानक-ग्राम-मातृ-स्थान-देवकुलस्य ...]{style="color:#0000ff;"} In conclusion, the conceptualization of the मातृका-s in the classical religion can be traced back to the Vedic layer of the religion, with an organic evolution in the महाभारत and the early medical literature, and close associations to the कौमार and शैव sects.

**चामुण्डा, her distinctness, and preeminence**
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/chamunda_mantra_handbook_nepal.jpg){width="75%"}
```{=latex}
\end{center}
```



*चामुण्डा from a prayoga manual from Nepal*

However, there is one mysterious मातृका in the classical list who does not fit the general deva-पत्नी prototype inherited from the श्रुति. She goes by the common name चामुण्डा or is alternatively known as बहुमांसा in some early पौराणिक texts. She is often distinguished by a corpse-, an owl- (e.g., परशुरामेश्वर temple in कलिङ्ग) or a vulture (the Mayamata प्रतिष्ठातन्त्र of सैद्धान्तिक tradition) ensign. While she is explicitly linked to Rudra, she is often distinguished from his typical female counterpart रुद्राणी. चामुण्डा does not occur in any Vedic text (except in masculine form in a late addendum to the वैखानस-mantra-प्रश्न); nor does she occur in the epics.

In contrast, she appears profusely in पौराणिक texts. However, even here, in one of her early occurrences, directly pertaining to one of her holiest shrines, कोटिवर्ष, she is presented under a different name, बहुमाम्सा, which appears to be a euphemistic double entendre. An apparently archaic version of the कोटिवर्ष-माहात्म्य, the famous पीठ in वङ्ग, also known as देवीकोट (see below), was incorporated into the "proto"-Skandapuraṇa. Briefly, the frame story goes thus: Brahman once performed संध्या at the Bay of Bengal for a crore years. Then he founded a beautiful city that eventually came to be known as कोटिवर्ष, where people lived a youthful existence. Once the god left the city, it was invaded by daitya-s, who committed many atrocities and slew thousands of ब्राह्मण-s and desecrated rituals. To remedy this, the gods went to Brahman and with him they went to Rudra. There गौरी was performing tapasya in a grove and turned all the gods into females. Rudra then told them that they could slay the daitya-s only in their female forms. Thus:

[ततो देवो ।असृजद् देवीं रुद्राणीं मातरं सुभम् ।]{style="color:#0000ff;"}\
[विकृतं रूपम् आस्थाय द्वितीयाम् अपि मातरम् ।]{style="color:#0000ff;"}\
[नाम्ना तु बहुमांसां तां जगत्-संहार-रूपिणीम् ॥]{style="color:#0000ff;"}\
[नियोगाद् देवदेवस्य ततो विष्णुर् अपि प्रभुः ।]{style="color:#0000ff;"}\
[मातराव् असृजद् द्वे तु वाराहीं वैष्णवीम् अपि ॥]{style="color:#0000ff;"}\
[अभूत् पितामहाद् ब्राह्मी शर्वाणी संकराद् अपि ।]{style="color:#0000ff;"}\
[कौमारी षड्मुखाच् चापि विष्णोर् अपि च वैष्णवी ।]{style="color:#0000ff;"}\
[वाराही माधवाद् देवी माहेन्द्री च पुरंदरात् ॥]{style="color:#0000ff;"}\
[सर्वतेजोमयी देवी मातॄणां प्रवरा सुभा ।]{style="color:#0000ff;"}\
[बहुमांसा महाविद्या बभूव वृषभध्वजात् ॥]{style="color:#0000ff;"}\
[सर्वेषां देवतानां च देहेभ्यो मातरः सुभाः ।]{style="color:#0000ff;"}\
[स्वरूपबलधारिण्यो निर्जग्मुर् दैत्य-नासनाः ॥]{style="color:#0000ff;"}\
[वायव्या वारुणी याम्या कौबेरी च महाबला ।]{style="color:#0000ff;"}\
[महाकाली तथाग्नेयी अन्यास् चैव सहस्रसः ॥]{style="color:#0000ff;"}\
[ता गत्वा तत् पुरं रम्यं दैत्यान् भीमपराक्रमान् ।]{style="color:#0000ff;"}\
[जघ्नुर् बहुविधं देव्यो घोरनादैर् विभीषणैः ।]{style="color:#0000ff;"}\
[दैत्यहीनं च तच् चक्रुः पुराग्र्यं हेमभूषितम् ॥]{style="color:#0000ff;"} "proto"-KM17-23\
Then the god (Rudra) generated the goddess, mother रुद्राणी. Taking an auspicious yet grotesque form, he also generated a second mother going by the name बहुमांसा, she of world-destroying form. At the mandate of the god of the gods, the mighty विष्णु also generated two mothers, वाराही and वैष्णवी. From the Grandfather came ब्राह्मी and from शंकर \[came] शर्वाणी. कौमारी from Skanda (note irregular संधि: षड्मुखात् similar to that found in the कौलज्ञाननिर्णय of Matsyendra. This supports the archaic nature of this narrative). वाराही from माधव and goddess माहेन्द्री from the smasher of forts (Indra). All these foremost of mothers were full of luster and auspicious. बहुमांसा, the great wisdom goddess, came into being from the bull-bannered one (Rudra). From the bodies of all these deities emerged auspicious mothers, each bearing their respective form and might, and set forth for the destruction of the demons: वायव्या, वारुणी, याम्या, कौबेरी, महाकाली, then आग्नेयी of great strength, and thousands of others. The manifold goddesses slew \[the demons], uttering terrifyingly ferocious yells. They made that foremost city decorated with gold free of the demons.

On one hand, the मातृका-s of narrative are reminiscent of the older कौमार cycle, and the Vedic पत्नीसंयाज in naming a more inclusive set of goddesses including those generated by वायु, वरुण, Agni, etc. On the other hand, the core set of goddesses, who are named first, has crystallized to the classical 7-मातृका list. The only difference is that बहुमांसा replaces चामुण्डा. However, their equivalence is clear from the account as Rudra specifically assumes a grotesque to generate her. As the narrative continues, बहुमांसा's preeminent place in कोटिवर्ष is made amply clear:

[हेतुकेश्वर-नामाहं स्थास्याम्य् अत्र वरप्रदः ।]{style="color:#0000ff;"}\
[युष्माभिः सह वास्यामि नायकत्वे व्यवस्थितः ॥]{style="color:#0000ff;"}\
[यस् तु युष्मान् मया सार्धं विधिवत् पूजयिष्यति ।]{style="color:#0000ff;"}\
[सर्व-पापविमुक्तात्मा स परां गतिम् आप्स्यति ॥]{style="color:#0000ff;"}\
[दानवा निहता यस्माच् छूलेन बहुमांसया ।]{style="color:#0000ff;"}\
[शूलकुण्डम् इदं नाम्ना ख्यातं तीर्थं भविष्यति ॥]{style="color:#0000ff;"}\
[इह सूलोदकं पीत्वा बहुमांसाम् प्रणम्य च ।]{style="color:#0000ff;"}\
[अवध्यः सर्व-हिंस्राणाम् भविष्यति नरोत्तमः ॥]{style="color:#0000ff;"} "proto"-KM29-32\
Under the name of हेतुकेश्वर, I (Rudra) will station myself here as the boon-giver. I will stay with you all, taking the position of leadership. Whoever worships you all together with me as per the ritual injunctions shall become free of sins and attain the highest state. From the trident with which बहुमांसा slew the demons, this holy pond will be known by the name of शूलकुण्ड. He who drinks the शूल-water here and worships बहुमांसा will become unassailable to all harm-doers and will become the foremost of men.

Keeping with the preeminence of बहुमांसा in the early कोटिवर्ष cycle, in a subset of her early पौराणिक occurrences, चामुण्डा appears independently of the classical मातृका lists. For example, चामुण्डा is described in the वराहपुराण as the slayer of the danava Ruru independently of the classical मातृका-s. This episode with the specific etymology furnished therein for the name चामुण्डा (see below) is also told in the देवीपुराण in an expanded form (DP 83-88). However, in the DP version, चामुण्डा is presented as one of the 7 classical मातृका-s. The slaying of Ruru is also retold in the मातृसद्भाव, an auxiliary text of the ब्रह्मयामल tradition. While other मातृ-s are mentioned therein as being in the company of Rudra as Hetuka Bhairava (हेतुकेश्वर in the KM narrative), the main protagonist is चामुण्डा in the form of कर्णमोटी (कर्णमोटिनी), who pursues Ruru into पाताल and slays him after a battle lasting a crore years. This is used as an alternative etymology for कोटिवर्ष. The same text also mentions that Rudra emanated the goddess एकवीरी from his forehead (third eye; note the Athena motif shared with the Greeks) to slay the daitya दारुक. In the Cera country, the cult of Rurujit is widespread and closely associated with that of भद्रकाली, who is described as the slayer of दारुक. Indeed, the ब्रह्मयामल tradition preserved in south India tends to equate भद्रकाली, the killer of दारुक, with चामुण्डा:  [भद्रकालि तु चामुण्डा सदा विजय-वर्धिनी ॥]{style="color:#0000ff;"} Interestingly, while the मातृसद्भाव's focus is कोटिवर्ष, its extant manuscripts are found in the Cera country, suggesting that the cult was carried south from वङ्ग after the destruction of देवीकोट by the Meccan demons. Below we provide the account of the slaying of Ruru from the वराहपुराण 96:

[तस्या हसन्त्या वक्त्रात् उ बह्वयो देव्यो विनिर्ययुः ।]{style="color:#0000ff;"}\
[याभिर् विश्वम् इदं व्याप्तं विकृताभिर् अनेकशः ॥]{style="color:#0000ff;"}\
[पाशाङ्कुशधराः सर्वाः सर्वाः पीनपयोधराः ।]{style="color:#0000ff;"}\
[सर्वाः शूलधरा भीमाः सर्वाश् चापधराः शुभाः ॥]{style="color:#0000ff;"}\
[ताः सर्वाः कोटिशो देव्यस् तां देवीं वेष्ट्य संस्थिताः ।]{style="color:#0000ff;"}\
[युयुधुर् दानवैः सार्धं बद्धतूणा महाबलाः ॥]{style="color:#0000ff;"}\
[क्षणेन दानव-बलं तत् सर्वं निहतन्तु तैः ।]{style="color:#0000ff;"}\
[देवाश् च सर्वे सम्पन्ना युयुधुर् दानवं बलम् ॥]{style="color:#0000ff;"}\
[कालरात्र्या बलञ् चैव यच् च देवबलं महत् ।]{style="color:#0000ff;"}\
[तत् सर्वं दानव-बलम् अनयद् यम-सादनम् ॥]{style="color:#0000ff;"}\
[एक एव महादैत्यो रुरुस् तस्थौ महामृधे ।]{style="color:#0000ff;"}\
[स्वाञ् च मायां महारौद्रीं रौरवीं विससर्ज ह ॥]{style="color:#0000ff;"}\
[सा माया ववृधे भीमा सर्व-देव-प्रमोहिनी ।]{style="color:#0000ff;"}\
[तया विमोहिता देवाः सद्यो निद्रान्तु भेजिरे ॥]{style="color:#0000ff;"}\
[देवी च त्रिशिखेनाजौ तं दैत्यं समताडयत् ॥]{style="color:#0000ff;"}\
[तया तु ताडितस्यास्य दैत्यस्य शुभलोचने ।]{style="color:#0000ff;"}\
[चर्म-मुण्डे उभे सम्यक् पृथग्भूते बभूवतुः ॥]{style="color:#0000ff;"}\
[रुरोस्तु दानवेन्द्रस्य चर्म-मुण्डे क्षणाद्यतः ।]{style="color:#0000ff;"}\
[अपहृत्याहरद् देवी चामुण्डा तेन सा 'भवत् ॥]{style="color:#0000ff;"}\
[सर्वभूत-महारौद्रो या देवी परमेश्वरी ।]{style="color:#0000ff;"}\
[संहारिणी तु या चैव कालरात्रिः प्रकीर्तिता ॥]{style="color:#0000ff;"}

As she [रुद्राणी] laughed, from her mouth arose numerous goddesses with many strange forms, by whom this universe was enveloped. These terrifying and auspicious goddesses all held lassos, goads, tridents, and bows, and all had full breasts. All those crores of mighty goddesses stood surrounding the \[primary] goddess, bearing quivers, together fought with the दानव-s. The whole दानव force was rapidly assaulted by those \[goddesses], and together with them, the gods fought the दानव force. The great deva-force, together with the army of कालरात्रि, sent the entire दानव force to Yama's abode. The great daitya Ruru stood alone in the great battle. He then released his terrible रौरवी magic. That terrible all-god-deceiving magic grew, and, overcome by it, the gods immediately fell asleep. Then the goddess struck the daitya on the battlefield with her trident. Thus, struck by the beautiful-eyed goddess, the skin and the head of the daitya were cleanly separated. As the goddess instantly seized and took away the skin and the head of the lord of the दानव-s, she came to be known as चामुण्डा. From the terrifying form of the supreme goddess that destroys all beings, she came to be known as कालरात्री.

The text then provides the below praise of रौद्री, where she is clearly identified as चामुण्डा:

[जयस्व देवि चामुण्डे जय भूतापहारिणि ।]{style="color:#0000ff;"}\
[जय सर्वगते देवि कालरात्रे नमो'स्तु ते ॥]{style="color:#0000ff;"}\
[विश्वमूर्ते शुभे शुद्धे विरूपाक्षि त्रिलोचने ।]{style="color:#0000ff;"}\
[भीमरूपे शिवे वेद्ये महामाये महोदये ॥]{style="color:#0000ff;"}\
[मनोजवे जये जम्भे भीमाक्षि क्षुभितक्षये ।]{style="color:#0000ff;"}\
[महामारि विचित्राङ्गे जय नृत्यप्रिये शुभे ॥]{style="color:#0000ff;"}\
[विकरालि महाकालि कालिके पापहारिणी ।]{style="color:#0000ff;"}\
[पाशहस्ते दण्डहस्ते भीमरूपे भयानके ॥]{style="color:#0000ff;"}\
[चामुण्डे ज्वालमानास्ये तीक्ष्णदंष्ट्रे महाबले ।]{style="color:#0000ff;"}\
[शत-यान-स्थिते देवि प्रेतासनगते शिवे ॥]{style="color:#0000ff;"}\
[भीमाक्षी भीषणे देवि सर्वभूतभयङ्करि ।]{style="color:#0000ff;"}\
[कराले विकराले च महाकाले करालिनि ।]{style="color:#0000ff;"}\
[काली कराली विक्रान्ता कालरात्रि नमोस्'तु ते॥]{style="color:#0000ff;"}

In the above cycles, चामुण्डा is identified with रुद्राणी and also as the primary दानव-slaying goddess, a role otherwise performed by दुर्गा. A folk memory of this was probably prevalent throughout the Indosphere and is today mainly seen in South India (e.g., the Cera country or Mysuru in the कर्णाट country). Additionally, चामुण्डा is also presented as a distinct goddess, who is part of Rudra's retinue, independently of the other मातृक-s. This being a popular position is established by multiple such references in post-Gupta काव्य. In one such, she is paired in her skeletal form with the equally skeletal गण भृङ्गिरिटि in a beautiful शार्दूलविक्रीडित verse of योगेश्वर regarding the celebration in Rudra's retinue of the birth of Skanda:

[देवी सूनुम् असूत नृत्यत गणाः किं तिष्ठतेत्य् उद्भुजे]{style="color:#0000ff;"}\
[हर्षाद् भृङ्गरिटावयाचित-गिरा चामुण्डयालिङ्गिते ।]{style="color:#0000ff;"}\
[अव्याद् वो हत-दुन्दुभि-स्वन-घन-ध्वानातिरिक्तस् तयोर्]{style="color:#0000ff;"}\
[अन्योन्य-प्रचलास्थि-पञ्जर-रणत्-कङ्काल-जन्मा रवः ॥]{style="color:#0000ff;"} सुभाषित-ratna-कोश 5.1\
The goddess [रुद्राणी] has birthed a son. O गण-s rise up and dance!\
Why? From joy, भृङ्गिरिटि raising his arms sings unasked embraced by चामुण्डा.\
On top of the loud din from the beating of the resounding drums\
is the rattling born of the bones from the skeletal cages of those two --- may it protect you!

She is also praised as the goddess slaying निशुम्भ by भवभूति in another masterly शार्दूलविक्रीडित with an allusion to the famous axial motif:

[सावष्टम्भ-निशुम्भ-संभ्रमनमद्-भूगोल-निष्पीडन-]{style="color:#0000ff;"}\
[न्यञ्चत्-कर्पर-कूर्म-कम्प-विचटद्-ब्रह्माण्ड-खण्ड-स्थिति ।]{style="color:#0000ff;"}\
[पाताल-प्रतिमल्ल-गल्ल-विवर-प्रक्षिप्त-सप्तार्णवं]{style="color:#0000ff;"}\
[वन्दे नन्दित-नीलकण्ठ-परिषद्-व्यक्त-र्द्धि वः क्रीडितम् ॥]{style="color:#0000ff;"} सुभाषित-ratna-कोश 5.3\
From your whirling of निशुम्भ, the earth-globe, with the axis, is pressed,\
down on the turtle's shell shaking it, shattering the support of the universe's hemisphere.\
The seven oceans trying to flood the netherworld instead fall into your cavernous cheeks.\
I praise your perfectly performed dance that delights the retinue of the blue-necked one.\
\*Also could be a double entendre for the pressed down foot.

While she is not explicitly named here, the cavernous cheeks indicate that the goddess being referred to is none other than चामुण्डा (see below). The two notable points in this verse are: first, Rudra's retinue is the audience for her dance, again suggesting that she is an integral part of it. Second, as we noted above, she is shown as assuming the role normally assigned to the goddess दुर्गा/चण्डिका, i.e., killing निशुम्भ, again reinforcing the idea of a folk memory of चामुण्डा as the primary demon-slayer. This overlap with चण्डिका will be further explored below.

चामुण्डा also occurs independently of the classical मातृका list but as part of other clusters of goddesses in certain पौराणिक and तान्त्रिक traditions, e.g., the दक्ष-यज्ञ episode at the beginning of the Mega-Skandaपुराण (1.1.3.49-53; माहेश्वर-खण्ड, केदार-खण्ड):

[वीरभद्रो महाबाहू रुद्रेणैव प्रचोदितः ।]{style="color:#0000ff;"}\
[काली कात्यायनीशाना चामुण्डा मुण्डमर्दिनी ॥]{style="color:#0000ff;"}\
[भद्रकाली तथा भद्रा त्वरिता वैष्णवी तथा ।]{style="color:#0000ff;"}\
[नव-दुर्गादि-सहितो भूतानां च गणो महान् ॥]{style="color:#0000ff;"}\
[शकिनी डाकिनी चैव भूत-प्रमथ-गुह्यकाः ।]{style="color:#0000ff;"}\
[तथैव योगिनी-चक्रं चतुःषष्ट्या समन्वितम् ॥]{style="color:#0000ff;"}\
[निर्जग्मुः सहसा तत्र यज्ञवाटं महाप्रभम् ।]{style="color:#0000ff;"}\
[वीरभद्र-समेता ये गणाः शतसहस्रशः ॥]{style="color:#0000ff;"}\
[पार्षदाः शंकरस्यैते सर्वे रुद्र-स्वरूपिणः ।]{style="color:#0000ff;"}\
[पञ्चवक्त्रा नीलकण्ठाः सर्वे ते शस्त्रपाणयः ॥]{style="color:#0000ff;"}\
Impelled by Rudra himself, the mighty-armed वीरभद्र marched forth right away to the grove of the यज्ञ with काली, कात्यायनी, ईशाना, चामुण्डा, मुण्डमर्दिनी, भद्रकाली, भद्रा, त्वरिता and वैष्णवी, the nine दुर्गा-s and the rest: the great भूत-गण-s, शकिनी, डाकिनी, the ghosts, pramatha-s and Kubera-s agents (guhyaka-s). The circle of 64 योगिनी-s also accompanied him. Hundreds of thousands of गण-s accompanied वीरभद्र. These troops of शंकर all had the form of Rudra, with five heads, blue-throats, and weapons in their hands.

The mention of the nine दुर्गा-s, after the list of nine goddesses, implies that these nine, including चामुण्डा, are those दुर्गा-s. The remaining मातृका-s (barring वैष्णवी, who is also counted among the nine दुर्गा-s) are not featured in this list. Similarly, in the iconographic section of the अग्निपुराण (a similar account is also found in the iconographic manual, the प्रतिष्ठा-लक्षण-सार-samuccaya), we encounter a tradition, where multiple चामुण्डा-s are presented as part of a group of 8 cremation-ground mothers, the अम्बाष्टक, again almost entirely distinct from the classical मातृका-s (AP 50.30-37):

[कपाल-कर्तरी-शूल-पाश-भृद् याम्य-सौम्ययोः ॥]{style="color:#0000ff;"}\
[गज-चर्म-भृद् ऊर्ध्वास्य पादा स्यात् रुद्रचर्चिका ।]{style="color:#0000ff;"}\
रुद्रचर्चिका \[is depicted] holding a skull, battle scissors, trident, lasso to the right and left. She holds an elephant hide, and her leg is raised up.

[सैव चाष्टभुजा देवी शिरो-डमरुकान्विता ।]{style="color:#0000ff;"}\
[तेन सा रुद्रचामुण्डा नाटेश्वर्य् अथ नृत्यती ॥]{style="color:#0000ff;"}\
रुद्रचामुण्डा is verily the eight-handed goddess holding a severed head and a डमरु. She is shown dancing as the goddess of the dance (c.f. the above verse of भवभूति).

[इयम् एव महालक्ष्मी-रुपविष्टा चतुर्मुखी ।]{style="color:#0000ff;"}\
[नृ-वाजि-महिषेभांश् च खादन्ती च करे स्थितान् ॥]{style="color:#0000ff;"}\
The goddess महालक्ष्मी is indeed shown in a four-faced form. She \[is depicted] eating a man, horse, buffalo, and elephant held in her hands.

[दश-बाहुस् त्रिनेत्रा च शस्त्रासि-डमरु-त्रिकं ।]{style="color:#0000ff;"}\
[बिभ्रती दक्षिणे हस्ते वामे घण्टां च खेटकं ॥]{style="color:#0000ff;"}\
[खट्वाङ्गं च त्रिशूलञ् च सिद्ध-चामुण्डकाह्वया ।]{style="color:#0000ff;"}\
Siddha-चामुण्डका is depicted, with ten arms and three eyes, bearing a weapon, a sword, a डमरु, a trident, in her right arms; a bell, a shield, a skull-topped brand and a trident in her left arms.

[सिद्धयोगेश्वरी देवी सर्व-सिद्धप्रदायिका ॥]{style="color:#0000ff;"}\
[एतद् रूपा भवेद् अन्या पाशाङ्कुशयुतारुणा ।]{style="color:#0000ff;"}\
The goddess Siddhayogeśvarī (the goddess of the kaula पूर्वाम्नाय = Trika), who bestows all accomplishments, is shown with another form, crimson in color, holding a lasso and a hook.

[भैरवी रूप-विद्या तु भुजैर् द्वादशभिर्-युता ॥]{style="color:#0000ff;"}\
भैरवी, the beautiful wisdom goddess, is shown with 12 arms.

[एताः श्मशानजा रौद्रा अम्बाष्टकम् इदं स्मृतं ।]{style="color:#0000ff;"}\
These raudra \[goddesses] of the cremation ground are known as the cluster of eight-mothers.

[क्षमा शिवावृता वृद्धा द्विभुजा विवृतानना ॥]{style="color:#0000ff;"}\
क्षमा is shown surrounded by jackals as an old female with two arms and a gaping mouth.

[दन्तुरा क्षेमकरी स्याद् भूमौ जानुकरा स्थिता ।]{style="color:#0000ff;"}\
The fanged क्षेमकरी is shown \[seated] on the ground with her hands on her knees.

These अम्बाष्टक goddesses, रुद्रचर्चिका, रुद्रचामुण्डा, महालक्ष्मी, Siddha-चामुण्डा, Siddhayogeśvarī, भैरवी, क्षमा and क्षेमकरी are likely associated with the 8 महास्मशान-s of the तान्त्रिक tradition. This is supported by the presence of महालक्ष्मी in the list, who is associated with the महास्मशान of Kollagiri or लक्ष्मीवन (modern Kolhapur). In the list, we find two explicitly named चामुण्डा-s, which hearkens back to the mega-Skandaपुराण नवदुर्गा-s, where चामुण्डा is followed by मुण्डमर्धिनी, who on etymological grounds could be seen as the second चामुण्डा. A third goddess of the अम्बाष्टक, क्षमा, is depicted as an old female with jackals --- again, iconographically similar to चामुण्डा. The ogdoad also features रुद्रचर्चिका, another ectype of चामुण्डा (see below). Thus, we have at least four goddesses in the अम्बाष्टक group, who can be described as conforming to the चामुण्डा type. This multiplicity hints at चामुण्डा being worshiped as the primary goddess at several of the महास्मशान-s.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/chamunda_bhishana_bhairava.jpg){width="75%"}
```{=latex}
\end{center}
```



*चामुण्डा with her husband भिषण-bhairava: the deities of एकाम्र (a Nepalian depiction)*

At least one of these महास्मशान-s featuring चामुण्डा was perhaps located at एकाम्र (modern Bhubaneswar) in the कलिङ्ग country. The association of चामुण्डा with this site, along with her Bhairava consort and Kubera or his female counterpart कौबेरी, is abundantly attested in the kaula tradition: tantra-s (e.g., कुब्जिकामत) and prayoga manuals of the पश्चिमाम्नाय (e.g., Siddhi-लक्ष्मी-क्रमार्चना-vidhi-s), the उत्तराम्नाय traditions like निशि-संचर, and डामर texts like the त्रिदश-डामार-प्रत्यङ्गिरा. For example, we have the below mantra-s from the पश्चिमाम्नाय (or its combination with the उत्तराम्नाय in the case of the last mantra) tradition:

[ऐं ॐ एकाम्रक-महाक्षेत्र-भीषण-महा-भैरवाय यं चामुण्डा-शक्ति-सहिताय एकपाद-क्षेत्रपालाय धनाधिपतये नमः ॥]{style="color:#0000ff;"}\
[ॐ ऐं यं रं लं वं शं एकाम्रके ओहायी कालरात्री छिप्पिनी चामुण्डा कौबेरी । औ-क्षः (ओ-क्षः) भीषण-भैरव श्रीपादुकभ्यां नमः ॥]{style="color:#0000ff;"}\
[ॐ ऐं यं भीषण-भैरवाय चामुण्डा-सहिताय एकाम्रक-क्षेत्राधिपतये नमः ॥]{style="color:#0000ff;"}\
[ॐ एकाम्रके क्षेत्रे यं भीषणभैरव यां चामुण्डा अम्बापाद ख्फ्रें ॥]{style="color:#0000ff;"}

Coming to चर्चिका, her equivalence with चामुण्डा is established by multiple sources. For example, अमरसिंह in his lexicon says:[कर्ममोटी तु चामुण्डा चर्ममुण्डा तु चर्चिका ।]{style="color:#0000ff;"} (AK 1.1.92). The great भास्करराय मखीन्द्र reiterates this in his gloss on the ललिता-सहस्रनाम. Consistent with this, we also have the शार्दूलविक्रीडित verse of तुङ्ग, which mentions चर्चिका in Rudra's retinue, separately from रुद्राणी, in a manner similar to चामुण्डा, as noted above.

[चर्चायाः कथम् एष रक्षति सदा सद्यो नृ-मुण्ड-स्रजं]{style="color:#0000ff;"}\
[चण्डी-केशरिणो वृषं च भुजगान् सूनोर् मयूराद् अपि ।]{style="color:#0000ff;"}\
[इत्य् अन्तः परिभावयन् भगवतो दीर्घं धियः कौशलं]{style="color:#0000ff;"}\
[कूष्माण्डो धृति-संभृताम् अनुदिनं पुष्णाति तुन्द-श्रियम् ॥]{style="color:#0000ff;"}\
How does he ever protect his garland of fresh human heads from चर्चा?\
Also his bull from चण्डी's lion and his snakes from his son's peacock?\
Thus, wondering to himself about the lord's deep mental skill\
कूष्माण्ड daily nourishes the growing satisfaction of his belly's corpulence.

The presence of चर्चिका in Rudra's retinue, independently of the classical मातृका-s, is also seen in some पौराणिक traditions, such as the [वामन-पुराण](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/shiva_gana-s-1.pdf) (70 in vulgate; 45 in short edition). Here, in the final battle with Andhaka, Rudra then took all the gods and his गण-s into his body (c.f. Greek Kronos motif). When Andhaka struck him with his mace and caused him to bleed, from his own blood, Rudra generated the 8 Bhairava-s. Then from his fertilizing sweat, Rudra generated the virgin goddess चर्चिका from his forehead (c.f. Greek Zeus-Athena motif) and then Kuja (the planetary archon of Mars) from his sweat that dropped on the ground. Together, चर्चिका and Kuja drank up the blood of Andhaka. This myth, with चर्चिका drinking up the blood of Andhaka, is widely depicted in images throughout India.

Finally, चर्चिका replaces चामुण्डा in some classical 7/8 मात्रिका lists in तान्त्रिक mantra-prayoga-s, like those in the हाहारव or the Picumata of the ब्रह्मयामल tradition. Below are the famed अंबापाद mantra-s of the हाहारव belonging to the अथर्वण-गुह्यकाली tradition (उत्तराम्नाय), where the 8 classical मातृका-s are associated with the eight Bhairava-s and manifest as काली-s, each surrounded by a retinue of 64 योगिनी-s:

[ॐ ह्रीं हुं छ्रीं फ्रें [भैरव] +आसनाय अष्टाष्टक-योगिनी-सहिताय [देवी] कालि २ अंबापाद हुं फ्रें नमः ।]{style="color:#0000ff;"}\
असिताङ्ग-bhairava : ब्रह्मवती\
Ruru-bhairava : रुद्रवती\
चण्ड-bhairava : कुमारवती\
Krodha-bhairava : विष्णुमती\
Unmattabhairava : घोरणवती/वरहावती\
कपाल-bhairava : महेन्द्रवती\
भीषण-bhairava : चर्चिकावती\
संहार-bhairava : महालक्ष्मीवती\
[ॐ ह्रीं हूं छ्रीं फ्रें मुण्डिन्यै [देवी] २ अंबापद छ्रें हूं नमः ॥]{style="color:#0000ff;"}\
देवी= काली, मुद्गला, दंष्ट्रिनी, शृङ्गारा, शूलिनी, वज्रिणी पाशिनी, अंकुशिनी

चर्चिका is associated with two great shrines at the Western and Eastern extremities of India, respectively हिङ्गुला (वमनपुराण vulgate 70.47) and कोटिवर्ष. Of these, कोटिवर्ष in the वङ्ग country is explicitly known as a महास्मशान. Hence, we may identify रुद्रचर्चिका, who heads the अम्बाष्टक list, as the goddess associated with this site. In support of this proposal, we have the famous वङ्गीय inscriptions found in the vicinity that specifically mention the shrines of चर्चिका (e.g., the Siyān and Bangarh inscriptions). The Parbatiya inscription of the वनमालवर्मन्, the king of Assam in the 800s of CE, also mentions the renovation of the temple of हेतुकेश्वर, the Rudra associated with कोटिवर्ष. King नयपाल (r. 1043--1058 CE) says that he built a temple for the image of चर्चिका his ancestor, emperor महेन्द्रपाल (r. 845--860 CE), had installed:

[... महे[न्द्र]पाल-चर्चाया महेन्द्र-सदृशोदयः । यः शैलीम् वडभीम् शैले सोपानेन सहाकरोत् ...]{style="color:#0000ff;"}\
He who presented like Mahendra built for महेन्द्रपाल's चर्चा a stone वडभी temple on the hill with steps \[leading to it] (Siyan inscription, unfortunately, damaged by the Meccan demons).

The सैद्धान्तिक-शैव-देशिक, मूर्तिशिव, a preceptor of the पाल monarchs, mentions that he installed a similar temple for चर्चिका and worships her in two verses, in अनुष्टुभ् and शार्दूलविक्रीडित, thus:\
[ॐ नमश् चर्चिकायै ।]{style="color:#0000ff;"}\
[सुरासुरशिरः-श्रेणि-पट-वास-समा जगत् ।]{style="color:#0000ff;"}\
[पान्तु विश्वकृताभ्यर्चाश् चर्चा-चरण-रेणवः ॥]{style="color:#0000ff;"}\
[दंष्ट्रा-संधि-निलीनम् एक-कवलम् विश्वं तद् अश्नामि किं?]{style="color:#0000ff;"}\
[सप्ताम्भोधि-जलानि हस्त-सुषिरे गुप्तानि किम् पीयते ?]{style="color:#0000ff;"}\
[इत्य् आहार-दरिद्रताकुलतया शुष्यत् तनुम् बिभ्रती]{style="color:#0000ff;"}\
[कल्पान्ते नृ-कपाल-मण्डन-विधिः पायाज् जगच् चर्चिका ॥]{style="color:#0000ff;"} (Bangarh inscription)\
Obeisance to चर्चिका.\
Like perfumed powder for the turbans of the array of gods and demons,\
worshiped by the world-maker (विश्वकर्मन्), may the dust from चर्चा's feet protect the world.

"The universe is a single morsel that will lodge in space between my teeth. Then what shall I eat?\
The waters of the seven oceans will be hidden in the hollow of my palm. Then what may be drunk?"\
Thus, anxious from the poverty of her meal, with her body becoming desiccated, observing,\
at the end of the age, the rite wearing a garland of human heads, may चर्चिका protect the world.

In the above verse, one may note the parallel to the drinking of the oceans seen in भवभूति's verse cited above. In addition, कोटिवर्ष, the same region of वङ्ग (वरेन्द्री) also had another famous चामुण्डा shrine, पुण्ड्रवर्धन, which is mentioned along with एकाम्र by the great Kashmirian मन्त्रवादिन् Abhinavagupta in his तन्त्रालोक. The उत्तराम्नाय has the below incantation remembering this now lost site:\
[ॐ ह्रीं श्रीम् श्री-पुण्ड्रवर्धन-महोपक्षेत्रे चामुण्डा अम्बापाद ख्प्रें नमः ।]{style="color:#0000ff;"}
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/charchika_pala.jpg){width="75%"}
```{=latex}
\end{center}
```



*पाल चर्चिका *

The recovery of at least 24 images of the एकवीरा type, i.e., चर्चिका or चामुण्डा independently of other मातृका-s, from the पाल age sites ruined by the Mohammedans, in the vicinity of कोटिवर्ष and पुण्ड्रवर्धन, attests to the importance of her cult in the वरेन्द्री region. One of these images is remarkable in depicting चर्चिका, flanked by गणेश and the Bhairava or महाकाल, surrounded by 20 चर्चिका-s (totally with the central figure 21 goddesses; c.f. the 21 तारा-s of the bauddha tradition). This multiplicity of चर्चिका-s brings to mind the array of 20 चर्चिका-s mentioned the Siddhāṅga-पञ्चक-mantra-s of the शक्तिसूत्र:

[घस्मरा चर्चिका विच्चेश्वरी श्रीविच्चाव्वा- पादुके पूजयामि ऐं ॥]{style="color:#0000ff;"}\
[स्वरूपवाहिनी नव-स्थानगा श्रीनन्दिनी- विच्चाव्वा पादुके पूजयामि ऐं ॥]{style="color:#0000ff;"}\
[पद्मा शोभा विकाशिनी विच्चाव्वा- पादुके पूजयामि ऐं ॥]{style="color:#0000ff;"}\
[महाशान्ता विकस्वरा विकाशिनी विच्चाव्वा- पादुके पूजयामि ऐं ॥]{style="color:#0000ff;"}\
[नाद-चर्चिका शब्दाद्या श्रीषट्का विच्चाव्वा- पादुके पूजयामि ऐं ॥]{style="color:#0000ff;"}

If we count the last goddess, विच्चाव्वा, who is the same in each of the five, then we get a list of 16 comparable to the 16 चामूण्डा-s mentioned in the southern ब्रह्मयामल with roots in the वङ्ग country.

**The scorpion-goddess**
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2022/02/vrishchikodari_prayaga.jpg){width="75%"}
```{=latex}
\end{center}
```



वृश्चिका from प्रयाग

We next consider a goddess who is iconographically similar to चामूण्डा but distinguished by a scorpion ornament on her belly. Images of this goddess are widely distributed across India, but a scorpion is never mentioned as an ornament in any of the numerous iconographic accounts of चामूण्डा. Moreover, this scorpion goddess is only found in एकवीरा form, never with the other मातृका-s. To decipher who she is, we have to turn to the mega-Skandaपुराण, which mentions scorpions as the medallion of चण्डी, a member of the entourage of रुद्रसदाशिव:

[तथोद्यतो योगिनी-चक्र-युक्तो गणो गणानां पतिर् एक-वर्चसाम् ।]{style="color:#0000ff;"}\
[शिवं पुरस्कृत्य तदानुभावास् तथैव सर्वे गणनायकाश् च ॥]{style="color:#0000ff;"}\
[तद् योगिनी-चक्रम् अति-प्रचण्डं टंकार-भेरी-रव-स्वनेन ।]{style="color:#0000ff;"}\
[चण्डी पुरस्कृत्य भयानकां तदा महाविभूत्या सम्-अलंकृतां तदा ॥]{style="color:#0000ff;"}\
[कण्ठे कर्कोटकं नागं हार-भूतं चकार सा ।]{style="color:#0000ff;"}\
[पदकं वृश्चिकानां च दन्दशूकांश् च बिभ्रती ॥]{style="color:#0000ff;"}\
[कर्णा-वतंसान् सा दध्रे पाणि-पाद-मयांस् तथा ।]{style="color:#0000ff;"}\
[रणे हतानां वीराणां शिरांस्य् उरसि चापरान् ॥]{style="color:#0000ff;"}\
[द्विपि-चर्म-परीधाना योगिनी-चक्र-संयुता ।]{style="color:#0000ff;"}\
[क्षेत्रपालावृता तद्वद् भैरवैः परिवारिता ॥]{style="color:#0000ff;"}\
[तथा प्रेतैश् च भूतैश् च कपटैः परिवारिता ।]{style="color:#0000ff;"}\
[वीरभद्रादयश् चैव गणाः परम-दारुणाः ।]{style="color:#0000ff;"}\
[ये दक्ष-यज्ञ-नाशार्थे शिवेनाज्ञापितास् तदा ॥]{style="color:#0000ff;"}\
[तथा काली भैरवी च माया चैव भयावहा ।]{style="color:#0000ff;"}\
[त्रिपुरा च जया चैव तथा क्षेमकरी शुभा ॥]{style="color:#0000ff;"}\
[अन्याश् चैव तथा सर्वाः पुरस्कृत्य सदाशिवम् ।]{style="color:#0000ff;"}\
[गन्तु-कामाश् चोग्रतरा भूतैः प्रेतैः समावृताः ॥]{style="color:#0000ff;"}\
Then the accompanied by the circle of योगिनी-s, the गण, the lord of the गण-s (Nandin) of singular splendor and all the leaders of the गण-s followed, keeping शिव at their head. The circle of योगिनी-s was most-terrifying and resounding with their yells, like the beating of kettledrums. They kept at their forefront the terrible चण्डी adorned with great magnificence. She had made the कर्कोटक snake in the form of a necklace on her neck. She had a medallion of scorpions and bore snakes. Her earrings were made of severed hands and legs. She wore the severed heads of warriors slain in battle on her chest. She wore a skirt of elephant-hide and was accompanied by the circle of योगिनी-s. She was surrounded by क्षेत्रपाल-s and likewise by Bhairava-s. Similarly, she was accompanied by zombies, ghosts and deceiver ghosts. With वीरभद्रा at their head, were the most-terrifying गण-s, who had been ordered by शिव to destroy दक्ष's sacrifice (the Raumya-s). Likewise, there were काली, भैरवी, the frightful माया, त्रिपुरा, जया and the auspicious क्षेमकरी. These and all others, of great ferocity, keeping सदाशिव at their head, surrounded by ghosts and zombies, desired to march forth.

This is an account of the bridal procession of Rudra's retinue on the occasion of his marriage to पार्वती. Though he was displaying this sport of marrying his wife again following Sati's reincarnation as पार्वती, his शक्ति-s never really left him. The chief among them is चण्डी, wearing a badge or medallion of scorpions. Thus, it appears that this ever-present शक्ति of Rudra is depicted as the एकवीरा वृश्चिकोदरी goddess. It is possible that the scorpion was identified with the eponymous constellation. Thus, its placement on the belly of the destroying goddess might have had an astronomical symbolism related to the constellation's position at the southern point of the ecliptic (associated with her seasons: autumn/winter: right in the Yajurveda) and its connection to the Vedic goddess of the netherworld and doom, निरृति. In another account of the destruction of दक्ष's sacrifice from the कायावरोहण-माहात्म्य (5.82), रुद्राणी herself generates a goddess by rubbing her nose, who is described by the epithet शहस्रचरणोदरी. This might be interpreted either as she with a 1000 feet and bellies or with a millipede on her belly, giving a possible parallel for this iconography.

Despite her widespread presence in शैव temples, the presence of this goddess in the mantra-शास्त्र is limited. The goddess कुब्जिका is described as having scorpion ornaments like her husband Aghora in the prayoga-texts of the Kaula tradition (e.g., षडाम्नायपूजाविधि):

[अग्नि-ज्वाला-प्रभाभैर् ज्वलन-शिखि-पिच्छ-वृश्चिकैर् हारमाला ।]{style="color:#0000ff;"}\
[मुण्डस्रङ्-मुण्ड-भागाभय-दुरित-हरा कुब्जिकेशी नमस् ते ॥]{style="color:#0000ff;"}\
Obeisance to you. Goddess कुब्जिका, who takes away misfortune has the glow of blazing flames, is garlanded by a wreath of shining peacock feathers and scorpions, and a garland of severed heads, holds a severed head, and shows the gestures of protection and boon-giving.

The scorpion-goddess वृश्चिका also figures in the श्रिकुल practice of the meditation on the six cakra-s along the path of the सुषुंण. In the अनाहत-cakra, she is worshiped in the circle of the goddess राकिनी presiding over blood along with her Rudra. Here, she is worshiped along with several other goddesses, including चामुण्डा, on the 12 spokes of the cakra. The वङ्गीय मन्त्रवादिन् पूर्णानन्द explains it thus in his तत्त्वचिन्तामणि:

[अनाहते न्यसेत् पश्चात् परितः क-ठ-वर्णकैः ।]{style="color:#0000ff;"}\
[कालरात्रिः खातिता च गायत्री घण्टिका ततः ॥]{style="color:#0000ff;"}\
[ङा वृश्चिका च चामुण्डा छाया जया तथैव च ।]{style="color:#0000ff;"}\
[झङ्कारिणी तथा ज्ञाना टङ्कहस्ता च विन्यसेत् ॥]{style="color:#0000ff;"}\
[ठङ्कारी च क्रमादेता ध्यात्वा वीरश्च पूर्ववत् ।]{style="color:#0000ff;"}\
The equivalence between the goddesses and the बीज-s formed from the अर्ण-s is thus: कं: कालरात्रिः; खं: खातिता; गं: गायत्री; घं: घण्टिका; ङं: वृश्चिका; चं: चामुण्डा; छं: छाया; जं: जया; झं: झङ्कारिणी; ञं: ज्ञाना; टं: टङ्कहस्ता;\
ठं: ठङ्कारी.

This meandering discussion establishes that, while चामुण्डा was seen as part of the 7/8 मातृका-s, she also had a separate existence either as a preeminent figure among the मातृका-s or as an independent goddess. In the latter capacity, she both iconographically and mythologically, overlapped with the domains of two independent goddesses भद्रकाली and चण्डी/चण्डिका. An example of this overlap in folk tradition is the [poem of भुषण त्रिपाठी on शिवाजी](https://manasataramgini.wordpress.com/2010/04/25/the-scope-of-shivajis-plan-of-svarajya/), where he says that चण्डी is growing fat from eating the Mohammedans offered to her by the मराठा advance --- thus, he equates चण्डी to the emaciated चामुण्डा.

**चामुण्डा's place in the mantra-शास्त्र**\
We now turn to the मन्त्रशास्त्र to examine some aspects of her worship. The KM already signals that the worship of the goddesses at कोटिवर्ष was according to the tantra-s known as the यामल-tantra-s:

[अहं ब्रह्मा च विष्णुस् च ऋषयस् च तपोधनाः ।]{style="color:#0000ff;"}\
[मातृतन्त्राणि दिव्यानि मातृ-यज्ञविधिं परम् ।]{style="color:#0000ff;"}\
[पुण्यानि प्रकरिष्याम यजनं यैर् अवाप्स्यथ ॥]{style="color:#0000ff;"}\
[ब्राह्मं स्वायम्भुवं चैव कौमारं यामलं तथा ।]{style="color:#0000ff;"}\
[सारस्वतं सगान्धारम् ऐशानं नन्दियामलम् ॥]{style="color:#0000ff;"}\
[तन्त्राण्य् एतानि युष्माकं तथान्यानि सहस्रसः ।]{style="color:#0000ff;"}\
[भविष्यन्ति नरा यैस् तु युष्मान् यक्ष्यन्ति भक्तितः ॥]{style="color:#0000ff;"}\
[नराणां यजमानानां वरान् यूयं प्रदास्यथ ।]{style="color:#0000ff;"}\
[दिव्यसिद्धिप्रदा देव्यो दिव्ययोगा भविष्यथ ॥]{style="color:#0000ff;"}\
[यास् च नार्यः सदा युष्मान् यक्ष्यन्ते सरहस्यतः ।]{style="color:#0000ff;"}\
[योगेस्वर्यो भविष्यन्ति रामा दिव्यपराक्रमाः ॥]{style="color:#0000ff;"} KM 34-38\
I \[Rudra], Brahman, विष्णु and the sages with a wealth of tapas will compose the pure and divine मातृ-tantra-s \[expounding] the foremost procedures for the rituals to the मातृ-s, by which you all would be worshiped. ब्राह्म, स्वायम्भुव, कौमार-यामल, सारस्वत with गान्धार, ऐशान, Nandi-यामल --- these tantra-s of yours and thousands of others will come into being, and men will piously worship you all with them. You, O goddesses, endowed with divine yoga, will grant boons to the men who worship you and confer magical powers on them. Those women who will continuously worship you with the secret rituals will become the mistresses of yoga, beautiful and possessed of magical prowess.

Consistent with the above, the root tantra of the ब्रह्मयामल tradition, the Picumata, has a major section devoted to the योगिनी-kula-s associated with each of the मातृका-s. Given the preeminence of चामुण्डा at कोटिवर्ष, one would expect a special place for her among the मातृका-s in the mantra-शास्त्र. We believe that imprints of this are seen throughout the शैव- and शाक्त- मन्त्रशास्त्र and their arborizations. For example, the प्रतिष्ठा-tantra, Mayamata, after giving the iconographic specifications for the 7 मातृका-s flanked by वीरभद्र and विनायक, provides a second section only for चामुण्डा. There it describes the installation of her images with 4, 6, 8, 10, 12, or 16 arms made from wood, clay, or stucco. It states they should show her dancing (see above) the twilight dance either by herself or place her beside Rudra, shown performing the same dance. It further mentions that the images with 4 or 6 arms are helpful for pacificatory purposes. It is again specified that the worship/festivals should be officiated as per the यामल-tantra-s (e.g., Southern ब्रह्मयामल).

The dominance of चामुण्डा in the शैव systems is also apparent in the archaic mantra-शास्त्र recorded in the डामर-tantras. There, चामुण्डा is invoked in a "fever-missile" incantation deployed to strike adversaries with a wasting fever thus:\
[ॐ बकामुखा चामुण्डा क्षीर-मांस-शोणित-भोजिनी [अमुकं] खः खः ज्वरेण गृह्ण गृह्ण गृह्णापय गृह्णापय हुं फट् स्वाहा ॥]{style="color:#0000ff;"}\
Remarkably, the goddess is described as being heron-faced. While this is not encountered in any of her later extant iconographies, on one end, it connects her to the archaic avicephalous goddesses associated with Skanda in the early कौमार cycles and the avicephalous goddesses of the कुमारी (कौशिकी विन्ध्यवासिनी) cycle of the proto-Skandaपुराण. On the other end, it connects her to the terminal शैव-शाक्त महाविद्या tradition, which features the attacking goddess बगलामुखी, whose name likewise means heron- or stork- headed. बगलामुखी too is not commonly shown with an avian head, but we have multiple prominent exemplars of such iconography in her case. First, we have such a painting from Kangra, Himachal. There is also the avicephalous बगलामुखी with 16 hands at the सण्कट घाट temple in वाराणसि. In a painting from the बगलामुखी temple at Bankhandi, Himachal, she is shown riding a crane, which also attacks the daitya whom she slays. Notably, at the बगलामुखी temple at नीलाचल, Assam, she is depicted with an owl ensign, another feature shared with चामुण्डा. This suggests that grotesqueness of चामुण्डा's form included within it a long, iconographically unexpressed memory of the ancient avicephalous goddesses, which was subsequently passed on बगलामुखी. She is also identified with another of the महाविद्या-s, छिन्नमस्ता, in the opening verse of her [famous stotra](https://manasataramgini.wordpress.com/2005/04/17/chinnamasta/), though their iconography is rather distinct:

[ॐ छिन्नमस्ता महाविद्या महाभीमा महोदरी ।]{style="color:#0000ff;"}\
[चण्डेश्वरी चण्ड-माता चण्ड-मुण्ड-प्रभञ्जिनी ॥]{style="color:#0000ff;"}

Her presence is also felt across the शक्ति-para शैव systems. कुब्जिका, the supreme goddess of the पश्चिमाम्नाय, is seen as having several deities (Rudra and दूती-s) associated with her respective न्यास अङ्ग-s:\
हृदय: काली\
शिरस्: Siddhayogeśvarī or जुष्टाचाण्डाली\
शिखा: Svacchanda-bhairava (the deity of the बहुरूपी ऋक्)\
Kavaca: शिवा\
Netra-traya: रक्तचामुण्डा (परा in some traditions)\
Astra: प्रत्यङ्गिरा or गुह्यकाली\
As one can see, the deity of the eyes of कुब्जिका is none other रक्तचामुण्डा. Her mantra is given as:

[ॐ रक्ते महारक्ते छामुण्डेश्वरी स्वाहा ॥]{style="color:#0000ff;"}\
[ऐं रक्ते महारक्ते छामुण्डेश्वरी ख्फ्रें स्वाहा ॥]{style="color:#0000ff;"}\
The first is the root form and the second version is the kaula deployment.

रक्तचामुण्डा's deep presence is indicated by her presence in other शैव mantra traditions. For example, the डामर tradition deploys her mantra for the successful procurement of medicinal herbs:\
[ॐ ह्रीं रक्तचामुण्डे हूं फट् स्वाहा ॥]{style="color:#0000ff;"}

The डामर tradition also worships her in the company of Rudra as नृसिंह, a feature shared with the गुह्यकाली tradition:\
[ॐ ह्रीं श्रीं क्लीं द्रं चण्डोग्रे त्रिनेत्रे चामुण्डे अरिष्टे हूं फट् स्वाहा । ह्रीं नमाम्य् अहं महादेवं नृसिंहं भीमरूपिणं ॐ नमस् तस्मै ॥]{style="color:#0000ff;"}

At the root of the शाक्त tradition are the famed mantra-s of चामुण्डा, the most fundamental of which is the नवार्ण-mantra. Tradition hails it as the best of the best of the शाक्ति-mantra-s: [विच्चे नवार्ण-मन्त्रो ।अयं शक्ति-मन्त्रोत्तमोत्तमः ।]{style="color:#0000ff;"}. It goes thus:\
[(ॐ) ऐं ह्रीं क्लीं चामुण्डायै विच्चे ॥]{style="color:#0000ff;"}\
It might be expanded to include चामुण्डा at the end of the मातृका-list as the Sarva-मातॄ-maya-mantra:\
[(ॐ) ह्रीं ब्रह्माणी-माहेश्वरी-कौमारी-वैष्णवी-वाराही-ऐन्द्री-चामुण्डायै विच्चे स्वाहा ।]{style="color:#0000ff;"}\
We then have the [पदमाला-mantra](https://manasataramgini.wordpress.com/2007/07/01/the-padamala-mantra-of-chamunda/) found in several texts like the देवीपुराण and the युद्धजयार्णव-tantra (which in turn is included in अग्निपुराण; AP 135). This long mantra, while including all the मातृका-s as the above, has as its primary deity the 28-handed चामुण्डा. It ends with the mantra-pada:\
[ॐ चामुण्डे किलि किलि ॐ विच्चे हुं फट् स्वाहा ॥]{style="color:#0000ff;"}\
These mantra-s indicate the intimate connection between चामुण्डा and the mysterious mantra utterance "vicce". Indeed, Abhinavagupta even calls her विच्चिका in his तन्त्रालोक, making one wonder if there is some connection to वृश्चिका via a प्राकृत form. In any case, the ending of the पदमाला-mantra is remarkably similar of the root mantra of the पश्चिमांनाय, the समयाविद्या, which is the female counterpart of the बहुरूपी ऋक् (the Aghora-brahma-mantra \[Footnote 3]). Every syllable of the समयाविद्या corresponds to one of the 32 syllables of the metrical बहुरूपी. The standard पश्चिमांनाय समयाविद्या:\
[ॐ भगवति घोरे ह्स्ख्फ्रें श्रीकुब्जिके ह्रां ह्रीं ह्रौं ङ-ञ-ण-न-मे अघोरमुखि छ्रां छ्रीं किणि किणि विच्चे ॥]{style="color:#0000ff;"}\
The पश्चिमांनाय समयाविद्या as per the वङ्गीय tradition:\
[ॐ नमो भगवति ह्स्ख्फ्रें हौं कुब्जिके ऐं ह्रीं स्रीं अघोरे घोरे अघोरमुखि क्लीं क्लीं किलि किलि विच्चे ॥]{style="color:#0000ff;"}\
This suggests that the चामुण्डा mantra influenced the construction of the समयाविद्या.

Moreover, the अष्ट-मातृका-mantra-s of the पश्चिमाम्नाय also parallel the Sarva-मातॄ-maya-mantra:

[ऐं अघोरे अमोघे वरदे विमले [बीज] [देवी] विच्चे ॥]{style="color:#0000ff;"}\
[श्रीं: ब्रह्माणी; चं: कौमारी; टं: वैष्णवी; थं: वाराही; पं: इन्द्राणी; यं: चामुण्डादेवी; शं: महालक्ष्मी]{style="color:#0000ff;"}

As noted above, we have the शक्ति-सूत्र of the पश्चिमाम्नाय, which as has the Siddhāṅga-पञ्चक-mantra-s to the array of चर्चिका-s. Finally, a similar kind of influence of the चामुण्डा mantra, likely via the पश्चिमाम्नाय, is also seen on the construction of the long mantra of the erotic goddess भगमालिनी of the दक्षिणाम्नाय (note pada [bhaga-vicche]{style="color:#0000ff;"}):

[ॐ आं ऐं भगभगे भगिनि भगोदरि भगाङ्गे भगमाले भगावहे भगगुह्ये भगयोनि भगनिपातिनि सर्वभगे भगवशङ्करि भगरूपे नित्यक्लिन्ने भगस्वरूपे सर्वभगानि मे ह्यानय वरदे रेते सुरेते भगक्लिन्ने क्लिन्नद्रवे क्लेदय द्रावय अमोघे भग-विच्छे क्षुभ क्षोभय सर्वसत्त्वान् भगेश्वरि ऐं ब्लूं जं ब्लूं भें मों ब्लूं हें ब्लूं ऐ/ ब्लूं क्लिन्ने सर्वाणि भगानि मे वशमानय स्त्रीं ब्लूं ह्रीं भगमालिनी-नित्या-कलायै नमः ॥]{style="color:#0000ff;"} (As per the ज्ञानार्णव--tantra 15)

**The etymology of चामुण्डा**\
Finally, we come to the peculiar issue of the etymology of चामुण्डा. The पुराण-s offer multiple alternative "folk etymologies," e.g., from carma+मुण्ड (देवी- and वराह- पुराण-s) or from चण्ड and मुण्ड; however, none of these are by any means grammatical Sanskrit derivations. Moreover, the multiplicity also suggests that its actual roots were forgotten by the time the name was in common use. Yet, it seemed deserving of an explanation to the Sanskrit speaker as it did not seem "natural", unlike the names of the other मातृका-s with deep Indo-Aryan roots. This goes with the fact that she is not a straightforward female counterpart of the gods, unlike most other मातृका-s.

Interestingly, some of her earliest mentions in the कोटिवर्ष-माहात्म्य and the विन्ध्यवासिनी section of the proto-Skanda-पुराण, furnish her with a bivalent, but proper Sanskrit name, बहुमांसा. However, this name did not persist. Indeed, the other names of चामुण्डा, चर्चिका and कर्णमोटी, or the peculiar mantra-pada vicce are also not attested in the earliest Sanskrit texts and lack a transparent Sanskrit etymology. This suggests that the names of this goddess adopted from a non-Aryan language eventually acquired wide currency within Sanskrit. This was hinted at even by भस्करराय. Moreover, as we noted above, चामुण्डा overlaps with the domain of the related goddess known as चण्डी/चण्डिका, who is traditionally an ectype of दुर्गा. It does seem like चण्डी and चर्चिका too might have originated from different attempts at Sanskritization of the same non-Aryan word that also gave rise to चामुण्डा.

Its absence in the Vedic and epic layers of the language suggests that the word was unlikely to have been borrowed from a language the आर्य-s encountered en route to India (Uralic or Bactria-Margiana) or upon first entering India (the Harappan language). We cannot etymologize it as Dravidian or Austro-Asiatic either. Hence, we have to tentatively admit that it might be from a now-extinct tribal language. Adopting such a name when an early Sanskrit name बहुमांसा was already in use suggests that simple syncretism with tribal goddesses was not at play. Instead, foreign words might be adopted to indicate special powers in incantations (e.g., the jharbhari-turphari सूक्त to the अश्विन्स् right in the RV itself). Thus, in the तान्त्रिक tradition, the adoption of words from or which sound like from other languages is part of the same process. We posit that this was the mechanism by which the नवार्ण-mantra was constructed and, in turn, this made चामुण्डा a more popular name displacing बहुमांसा. This also goes hand in hand with a widespread tendency to show the universality of the deity. In the case of the great Rudrian goddesses, this was also associated with a tendency to emphasize her links to the Rudrian domain of the exterior, which includes the tribal groups (already seen in the Yajurveda). This is also signaled by indicating the worship of the goddess in distant or non-Aryan lands. For instance, in the proto-Skandaपुराण, the virgin goddess कौशिकी born of the black slough of पार्वती emanated numerous avicephalous and therocephalous goddesses to aid her in the killing of शुम्भ and निशुम्भ. Thereafter, these goddesses were installed for worship in various countries, like the owl-headed उपका in पारसीक (Iran); the raven-headed वायसी in Yavana (Greek lands); the lion-headed प्रचण्डा in Tukhara (Tocharia); वानरी among the शबर tribesmen; several others in the provinces of लन्का. This is reflected in the mantra-शास्त्र by the names of the goddesses like गान्धारी or द्रमिडि.

  - -----------------------------------------------------------------------

Footnote 1: First appears as theonym of त्वष्टृ in his aspect as विश्वकर्मन् in RV 10.82.02. There विश्वकर्मन् is explicitly described as धातृ. The equivalence of धातृ, विश्वकर्मन् and त्वष्टृ is established in the late Yajurvedic version of the पुरुष-सूक्त with the appendix termed the उत्तरनारायण.

Footnote 2: This pattern is reproduced in the national epic on the earthly plane in the unusual polyandry of the five पाण्डव-s to द्रौपदी.

Footnote 3: The Kashmirian कट्ःअ tradition preserves a form of the बहुरूपी that combines both the Rudra-s and the रुद्राणी-s; recorded by the मन्त्रवादिन् स्वामिन् लक्ष्मण जू

