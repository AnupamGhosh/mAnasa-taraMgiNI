
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A simple second order differential equation, ovals and chaos](https://manasataramgini.wordpress.com/2020/06/07/a-simple-second-order-differential-equation-ovals-and-chaos/){rel="bookmark"} {#a-simple-second-order-differential-equation-ovals-and-chaos .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 7, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/06/07/a-simple-second-order-differential-equation-ovals-and-chaos/ "12:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In our youth as a consequence of our undying fascination with ovals we explored many means of generating them. In course of those explorations we experimentally arrived at a simple second order differential equation that generated oval patterns. It also taught us lessons on chaotic systems emerging from differential equations even before we actually explored the famous Lorenz and Rossler attractors for ourselves. The inspiration came from the very well-known harmonic oscillator which is one of the first differential equations you might study as a layperson:  $\tfrac{d^2x}{dt^2}=-ax$ , where  $a$  is a positive constant that has some direct meaning in physics as the ratio of the restoration constant to the mass of the oscillator. With this foundation, we wondered what might happen if we used a cubic term instead which was in turn coupled to a periodic forcing from a regular harmonic oscillator. Thus, we arrived at the below equation with 3 parameters  $a, b, c$ :

 $$\dfrac{d^2x}{dt^2}=-abx^3+a\cos\left(\dfrac{2\pi t}{c}\right) \; \; \; \S 1$$ 

It became obvious to us this equation has no solution in terms of elementary functions but we could semi-intuitively figure out that solutions are likely to generate oval-like figures. To test this out we had to actually solve it numerically. Just before this foray, a visiting relative, who was a college teacher, mentioned to us that the mark of man was to solve ordinary differential equations using the method of रन्गा and कुट्टि on hand-held calculator. We had learned Euler's method as a part of our education in basic numerical calculus but the this new method, apparently discovered by these south Indian savants, was something we had never heard of. Accordingly, we looked up a book featuring numerical analysis, which our father often recommended to us, and realized that the relative was actually referring to a generalization of the Eulerian method by the two शुलपुरुष-s Runge and Kutta. This differential equation offered us an opportunity to apply it to something interesting and we spent sometime writing a program to do the same. With that in hand, we could solve the above 2nd order DE by writing it as two linked 1st order ordinary DEs.

 $$\dfrac{dx}{dt}=ay$$ 

 $$\dfrac{dy}{dt}=-bx^3+\cos\left(\dfrac{2\pi t}{c}\right)$$ 

![Figure 1. Some solutions of  $\S 1$  for different values of the parameters  $a, b, c$  with  $x_0=y_0=0$  (click to magnify).](https://manasataramgini.wordpress.com/wp-content/uploads/2020/06/cubtrigx2.png){width="75%"}

Consequently, we had an unfolding of the solutions for it which presented an interesting picture (Figure 1): We saw before our eyes the entire range of oscillations with simple periodicity, quasi-periodicity, superimposed beats of different frequencies and various types of chaotic oscillations. Thus, we saw this simple DE recapitulate all manner of oscillatory phenomena we had encountered in nature: the changes is numbers of molecules in during the cell cycle, populations of organisms in the ecosystem, climatic patterns and the light-curves of variable stars.

![Figure 2.  $x, \dot{width="75%"}

If we plotted  $x, \dot{x}$  we got our desired oval-like shapes. Notably, the cases where the solution is clearly chaotic we get a space-filling entangling of near oval paths in this plot.

![Figure 3. Plots of solutions with the  $b$  parameter changing while keeping  $a, c$  constant in each row (click to magnify).](https://manasataramgini.wordpress.com/wp-content/uploads/2020/06/cubtrigx5.png){width="75%"}

![Figure 4.  $x, \dot{width="75%"}

From Figure 4 we can see that the amplitude parameter  $a$  and the wavelength parameter  $c$  of the sinusoidal term of  $\S 1$  are the primary determinants of "shape" of the solution, while the parameter  $b$  associated with the cubic terms interacts with them to determine where chaos occurs.

