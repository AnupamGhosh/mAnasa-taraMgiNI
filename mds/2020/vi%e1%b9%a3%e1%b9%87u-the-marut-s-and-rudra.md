
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [विष्णु, the Marut-s and Rudra](https://manasataramgini.wordpress.com/2020/05/25/vi%e1%b9%a3%e1%b9%87u-the-marut-s-and-rudra/){rel="bookmark"} {#वषण-the-marut-s-and-rudra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 25, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/05/25/vi%e1%b9%a3%e1%b9%87u-the-marut-s-and-rudra/ "12:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note might be read as a continuation of the these two earlier ones:\
1) [The अश्विन्-s and Rudra](https://manasataramgini.wordpress.com/2020/01/12/the-asvin-s-and-rudra/)\
2) [The roots of वैष्णवम्: a view from the numerology of Vedic texts](https://manasataramgini.wordpress.com/2020/01/02/the-roots-of-vai%e1%b9%a3%e1%b9%87avam-a-view-from-the-numerology-of-vedic-texts/)

In the ऋग्वेद (RV), the Marut-s are seen associating with विष्णु on several occasions. This often occurs in the context of the epithets evaya or एष (meaning swift or ardent) being applied to them. This association is not per say out of place or surprising because it might be seen in the context of विष्णु accompanying Indra in the battle against वृत्र, where the Marut-s too accompany Indra as his troops (गण-s). Thus, they might be seen as surrounding विष्णु, one of the leaders of the deva-s in this battle. However, this association is not purely a mythic one --- in the context of the RV it extends to the Marut-s and विष्णु being invoked together for receiving offerings in a specific ritual. For example, this is clearly stated by गृत्समद शौनहोत्र:

[तान् वो महो मरुत एवयाव्नो]{style="color:#0000ff;"}\
[विष्णोर् एषस्य प्रभृथे हवामहे ।]{style="color:#0000ff;"}\
[हिरण्यवर्णान् ककुहान् यतस्रुचो]{style="color:#0000ff;"}\
[ब्रह्मण्यन्तः शंस्यं राध ईमहे ॥]{style="color:#0000ff;"} RV2.34.11

Verily those great Maruts, speeding along\
we invoke in the ritual offering of the swift विष्णु\
with extended sruc-s, the golden eminent ones,\
composing incantations, we implore them of praiseworthy liberality.

This is again presented in the context of the soma offering by Gotama राहूगण:

[ते 'वर्धन्त स्वतवसो महित्वना]{style="color:#0000ff;"}\
[नाकं तस्थुर् उरु चक्रिरे सदः ।]{style="color:#0000ff;"}\
[विष्णुर् यद् धावद् वृषणम् मदच्युतं]{style="color:#0000ff;"}\
[वयो न सीदन्न् अधि बर्हिषि प्रिये ॥]{style="color:#0000ff;"} RV 1.85.7

Those growing in their greatness, the self-powerful ones\
They stood in the high heaven and made \[themselves] a broad seat.\
When विष्णु washed the bull dripping with exhilaration (soma)\
like birds they sat down on the dear ritual grass.

Unlike the Marut-s, in the RV, their father Rudra is not commonly paired with विष्णु except in the general context of the incantations invoking multiple deities. This is keeping with the absence of any evidence for his participation in the Vedic narration of the वृत्र-हत्या. However, there is one exception where Rudra is mentioned along with विष्णु in a ritual context similar to the Marut-s by वसिष्ठ मैत्रवरुणि:

[अस्य देवस्य मीऴ्हुषो (')वया]{style="color:#0000ff;"}\
[विष्णोर् एषस्य प्रभृथे हविर्भिः ।]{style="color:#0000ff;"}\
[विदे हि रुद्रो रुद्रियम् महित्वं]{style="color:#0000ff;"}\
[यासिष्टं वर्तिर् अश्विनाव् इरावत् ॥]{style="color:#0000ff;"} RV 7.40.5

The appeasement of the god who is bountiful (Rudra)\
\[is done] in the ritual of the swift विष्णु with oblations;\
for Rudra knows his Rudrian might.\
May you अश्विन्-s drive on your food-bearing orbit.

This is notable in two ways: 1) there is a specific mention of Rudra being appeased in the ritual of विष्णु. This exactly parallels the offering to the Marut-s in the ritual of विष्णु. 2) There is also a reference to अश्विन्-s being called for the ritual. They are said to come on their food-bearing orbit (vartis), which reminds one of their epithet रुद्रवर्तनी, i.e. they who follow on the track of Rudra.

What is the ritual being referred to here? The answer to this comes from the deployment in the सोमयाग of another सूक्त emphasizing the link of the Marut-s and विष्णु --- the famous Evayamarut-सूक्त of the Atri-s. This सूक्त is deployed in a key recitation of the होतृ-s in the सोमयाग known as the शिल्प-शस्त्र-s. While it is described in all the RV ब्राह्मण-s, the most detailed account is given the Aitareya-ब्राह्मण:

[शिल्पानि शंसन्ति । देवशिल्पान्य् एतेषाम् वै शिल्पानाम् अनुकृतीह शिल्पम् अधिगम्यते । हस्ती कंसो वासो हिरण्यम् अश्वतरी-रथः शिल्पं; शिल्पं हास्मिन्न् अधिगम्यते य एवं वेद; यद् एव शिल्पानी३न् आत्म-संस्कृतिर् वाव शिल्पानि; छन्दोमयं वा एतैर् यजमान आत्मानं संस्कुरुते ।]{style="color:#0000ff;"}

They recite the शिल्प-s. These are divine art-works; by imitating these \[divine] art-works a \[human] work of art is achieved here. An elephant (evidently image of one), metal-work, weaving, gold-work, mule-cart-making are \[such human] craft-works. A work of art is accomplished by him who knows thus. Regarding those known as the शिल्प-s, the शिल्प-s are a perfection of the self; indeed by them the ritualist perfects himself imbued with the meters.

In this introduction to the शिल्प-शस्त्र-s, the ब्राह्मण teaches the Hindu "Platonic" principle that all human craft-works are imitations of the works of the deva-s (also c.f. [the Ratu-s of the Iranians](https://manasataramgini.wordpress.com/2012/06/16/the-broken-chain-and-the-chain-of-knowledge/)). It is in this spirit the ritualist engages in this शस्त्र recitation so that he might become संस्कृत or perfect even as the metrical chants --- their composers saw them as similar to crafts, sometimes using the phrase that they composed the ऋक्-s much like a craftsman making a chariot. Now, the शिल्प-शस्त्र-s consist of a long series of chants: 1) the two सूक्त-s of the ancient नाभानेदिष्ठ, the descendant of Manu सावर्णि (his name is also recorded in Iranian tradition). The first of these prominently features the gods Rudra and the अश्विन्-s (explicitly termed sons of Rudra-s in this सूक्त) and is recited by the होतृ. 2) The long aindra-सूक्त-s of the काण्व-s known as the वालखिल्या-s in the बृहती and सतोबृहती meters. These are recited by मैत्रवरुण by intricate separations of the pada-s and half verses. 3) The सूक्त of the great descendant of दीर्घतमस्, सुकीर्ति काक्षीवत (RV 10.131), which is central to the offering of beer in सौत्रामणि ritual, featuring the अश्विन्-s as physicians primarily along with Indra and सरस्वती again in her medicinal form. Then the enigmatic वृषाकपि-सूक्त is deployed which presents the banter between Indra and इन्द्राणि. As this सूक्त is recited the ritualist identifies its verses with the constitution of his body from hair, skin, fleshy organs, bones and marrow. This is recited by the ब्राह्मणच्छङ्सिन्. 4) The recitation of the शस्त्र that includes the Rudra-धाय्या and the एवयामरुत्-सूक्त to विष्णु and the Marut-s (see below). In extant tradition the Rudra-धाय्या is a single ऋक् RV 1.43.6 of कण्व Ghaura. Between the two is inserted the सूक्त of भरद्वाज starting with "dyaur na ya ..." (RV 6.20). This शस्त्र is explicitly recited with the insertion of the "o" vowels, i.e. न्यूङ्ख style of recitation by the अचावाक, the fourth of the hotraka-s. The Aitareya-ब्राह्मण records an interesting old tale regarding this शस्त्र, which suggests that it was redacted to attain its current structure:

[स ह बुलिल आश्वतर आश्विर् वैश्वजितो होतासन्न् ईक्षां चक्र: एषां वा एषां शिल्पानां विश्वजिति सांवत्सरिके द्वे मध्यंदिनम् अभि प्रत्येतोर् हन्ताहम् इत्थम् एवयामरुतं शंसयानीति । तद् ध तथा शंसयां चकार; तद् ध तथा शस्यामने गौश्ल आजगाम; स होवाच होतः कथा ते शस्त्रं विचक्रम् प्लवत इति; किं ह्य् अभूद् इत्य् ? एवयामरुद् अयम् उत्तरतः शस्यत इति; स होवाचैन्द्रो वै मध्यंदिनः,]{style="color:#0000ff;"}\
[कथेन्द्रम् मध्यंदिनान् निनीषसीति; नेन्द्रम् मध्यंदिनान् निनीषामीति होवाच; छन्दस् त्व् इदम् अमध्यंदिनसाच्य् अयं जागतो वातिजागतो वा। सर्वं वा इदं जागतं वातिजागतं वा । स उ मारुतो मैव शंसिष्टेति; स होवाचारमाछावकेत्य्; अथ हास्मिन्न् अनुशासनम् ईषे; स होवाचैन्द्रम् एष विष्णुन्यङ्गं शंसत्व्; अथ त्वम् एतं होतर् उपरिष्टाद् रौद्र्यै धाय्यायै पुरस्तान् मारुतस्याप्यस्याथा इति; तद् ध तथा शंसयां चकार । तद् इदम् अप्य् एतर्हि तथैव शस्यते ॥]{style="color:#0000ff;"}

That Bulila आश्वतर आश्वि was the होतृ in the विश्वजित् ritual; he observed: of these शिल्प-s in the year-long विश्वजित् ritual these two (the recitations of the मैत्रवरुण and ब्राह्मणच्छङ्सिन्) are added to the midday recitation. Well, let me have the एवयामरुत् recited \[by the अचावाक]. He then made that to be recited. When it was being thus recited गौश्ल came up; he said: "O होतृ why is your शस्त्र sinking without a wheel?" B.A.A: "What happened? The एवयामरुत् is being recited to the north of the altar." He (G) said: "Indra is verily the midday. Why do you seek lead Indra away from from the midday?" "I do not seek to lead Indra away from the midday", he replied. G: "The meter is also not that for the midday, jagati or atijagati. All these \[incantations] (एवयामरुत्) are either jagati or atijagati. It is also of the Marut-s; do not recite it." He (B.A.A) said: "Stop O अचावाक". He (B.A.A) then sought an instruction on this. He (G) said: "He may recite the Indra hymn with the mark of विष्णु. Now, O होतृ this is inserted between the preceding Rudra धाय्या and the following मारुत (i.e. एवयामरुत्). Then he (B.A.A) made it be be thus recited. Even now that is how it is recited."

The tale hints that originally or in certain traditions this शिल्प-शस्त्र consisted of just the invocation of Rudra, विष्णु and the Marut-s in an offering centered on विष्णु and the Marut-s. However, as stated, it was emended to include Indra to maintain the connection of the midday rite with Indra. Nevertheless, even in this, the सूक्त indicated to Indra was chosen such that the original connection of the offering with विष्णु was retained. The second ऋक् of the inserted सूक्त goes thus:

[दिवो न तुभ्यम् अन्व् इन्द्र सत्र]{style="color:#0000ff;"}\
[असुर्यं देवेभिर् धायि विश्वम् ।]{style="color:#0000ff;"}\
[अहिं यद् वृत्रम् अपो वव्रिवांसं]{style="color:#0000ff;"}\
[हन्न् ऋजीषिन् विष्णुना सचानः ॥]{style="color:#0000ff;"} RV 6.20.2

Just as that of Dyaus, to you, O Indra, the power\
of the Asura-s was entirely ceded by the deva-s,\
when you, O drinker of silvery juice, accompanied by विष्णु\
smote the snake वृत्र blocking the waters.

This is the mark of विष्णु mentioned in the ब्राह्मण. Given that the immediate juxtaposition of Rudra, विष्णु and the Marut-s in a ritual all together is a distinctive one, it is apparent that one of the form encapsulated in the final शिल्प-शस्त्र was something widely known to the early RV composers and specifically alluded to. This is further supported by the observation that the above वासिष्ठ ऋक् also mentions the अश्विन्-s. As can be seen in the शिल्प-शस्त्र-s, another prominent deity of the recitations are the अश्विन्-s who are explicitly coupled with Rudra in the first नाभानेदिष्थ सूक्त.

It may also be noted that the juxtaposition of Rudra, the Marut-s and विष्णु also has a further echo in the श्रौत ritual. In the piling of the Agnicayana altar, after the fifth and final layer of bricks has been laid it is said to ghora or terrible and if the adhvaryu steps on it he is said to die. This is because the newly laid altar is said to be possessed by the fierce manifestation of Agni as Rudra. Hence, Rudra has to be pacified by offerings of goat milk with a milkweed leaf on a specific brick of the altar (brick 189 in the standard Agnicayana eagle altar) with the recitation of the शतरुद्रीय by the adhvaryu. Once that is over, the ritualist goes clockwise around the altar impersonating Rudra by holding a bow and 3 arrows. Stopping at the vertices of a pentagon in course of that circuit, at each stop he recites the incantation to Rudra of the 5 years of the संवत्सर cycle as the lord of the wind. The adhvaryu gives a pitcher of water to the प्रतिप्रस्थातृ and asks him to make 3 circuits pouring it out in a continuous stream. As he does so, the adhvaryu and the ritualist recite the incantation to the Marut-s (e.g. तैत्तिरीय-संहिता 4.6.1.1) who are called upon to provide the energy residing in stones, mountains, wind, rain, the fury of वरुण, water bodies, herbs and trees as the strength of food. Once the pacification of Rudra is complete, and the fire is installed on it, the altar is said to be शान्त or peaceful and to give the yajamana a great bounty that is asked for in the camaka incantations. These accompany the vasor धारा offerings wherein a continuous stream of ghee is poured into the fire with a special furrowed log-guide known as the praseka that is as tall as the ritualist. Finally, after the offering is done, the ghee-soaked praseka itself is offered in the fire. This ritual begins with the following गायत्री incantation:

[अग्नावीष्णू सजोषसेमा वर्धन्तु वां गिरः । द्युम्नैर् वजेभिर् आगतम् ॥]{style="color:#0000ff;"}

Agni and विष्णु, may these chants glorify you together. Come with radiance and vigor!

Regarding these oblations the श्रुति of the तैत्तिरीय-s offers the following ब्राह्मण:

[ब्रह्मवादिनो वदन्ति: यन् न देवतायै जुह्वत्य् अथ किंदेवत्या वसोर् धारेति ? अग्निर् वसुस् तस्यैषा धारा; विष्णुर् वसुस् तस्यैषा धारा आग्नावैष्णव्य् अर्चा वसोर् धारां जुहोति; भागधेयेनैवैनौ सम् अर्धयति; अथो एताम् एवाहुतिम् आयतनवतीं करोति; यत्काम एनां जुहोति तद् एवाव रुन्द्धे; रुद्रो वा एष यद् अग्निस्; तस्यैते तनुवौ घोरान्या शिवानया; यच् छतरुद्रीयं जुहोति यैवास्य घोरा तनूस् तां तेन शमयति; यद् वसोर् धारां जुहोति यैवास्य शिवा तनूस् तां तेन प्रीणाति; यो वै वसोर् धारयै प्रतिष्ठां वेद प्रत्य् एव तिष्ठति ॥]{style="color:#0000ff;"} in TS 5.7.3

The ब्रह्मवादिन्-s say: "Given that they do not offer to any deity (i.e स्वाहा-s are uttered without naming the deity), which deity has the vasor धारा oblation? Wealth is Agni (or Agni is a Vasu); this stream is his. Wealth is विष्णु; this stream is his. With the verse addressed to Agni and विष्णु (the above गायत्री) he offers the stream of wealth; verily he unites them with their proper portions. He also makes this offering in order to have an abode. He wins that desire for which he makes this offering. The fire is Rudra; now two are his bodies, one is dreadful, the other is auspicious. That in which he offers the शतरुद्रीय is its dreadful one. He pacifies it with that [शतरुद्रीय offering]. That in which he offers the vasor धारा is the auspicious one. He delights it with that \[vasor धारा offering]. He, who knows the foundation of the vasor धारा indeed stands well-founded."

Thus, Rudra and the Marut-s on one hand and विष्णु on the other are identified with the opposing but juxtaposed characteristics of Agni and invoked as deities in the two key rituals associated with completed Agnicayana altar. That this juxtaposition is not just incidental but a deeper feature of the traditions of the Indo-Aryan world and in a more general form the greater Indo-European world is hinted by the tendencies expressed in the इतिहास-s: In the रामायण, Rudra offered great favors the malefic रावण who is opposed to the Indra-विष्णु duo humanized as राम and लक्ष्मण. More explicitly, in the महाभारत, the humanized manifestation of Rudra, अश्वत्थामन् is the malevolent force opposed to the humanized Indra-विष्णु-Agni manifestations in the form of Arjuna, कृष्ण and धृष्टद्युम्न and the other daiva forces. Similarly, Rudra backs Jayadratha allowing him to overcome the पाण्डु-s on the fateful day of the slaying of Abhimanyu. Yet again, in the same epic a malefic Rudra-backed figure शिशुपाल (born with 3 eyes) is also presented in opposition to the humanized manifestation of विष्णु. A comparable form of opposition also extends the Greek world, where, in their national epic, the Rudrian deities Apollo and Ares prominently back the Trojans against the Greeks who are backed by Athena.

Based on the inferred prominence of Rudra and विष्णु in the para-Vedic and "greater" Vedic horizons (i.e. the root of the आध्वर्यव tradition), we can say that this juxtaposition of them was a reflection in the "standard aindra religion" of tendencies which were more pronounced outside it: i.e. the cults centered on Rudra and विष्णु. This is hinted by the fact that right in the कौषीतकि-ब्राह्मण, in the corresponding account of the शिल्प-शस्त्र-s with the Rudra-धाय्या and the एवयामरुत् with the न्यूञ्खा "o" insertions, we encounter the below statement:

[अथो रुद्रो वै ज्येष्ठश् च श्रेष्ठश् च देवानाम् ।]{style="color:#0000ff;"}

Now Rudra is indeed the eldest and the best of the gods.

This indicates that the early "शैव" view was already impinging on the "standard aindra religion". As we have noted [before](https://manasataramgini.wordpress.com/2020/01/02/the-roots-of-vai%e1%b9%a3%e1%b9%87avam-a-view-from-the-numerology-of-vedic-texts/), the Aitareya-ब्राह्मण correspondingly, provides an early "वैष्णव" viewpoint hinting the primacy of विष्णु. That such tendencies were ancient is indicated by the fact that they are not restricted to branches of the आर्य-s who eventually conquered India --- indeed they are hinted by parallels seen among the Iranians and the Germanic peoples. Interestingly, it was that stream of the religion that was to eventually dominate the आर्य religious traditions in India in the form of the शैव and वैष्नव cults.

