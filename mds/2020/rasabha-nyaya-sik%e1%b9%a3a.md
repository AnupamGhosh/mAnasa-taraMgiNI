
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [रासभ-न्याय-शिक्षा](https://manasataramgini.wordpress.com/2020/05/12/rasabha-nyaya-sik%e1%b9%a3a/){rel="bookmark"} {#रसभ-नयय-शकष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 12, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/05/12/rasabha-nyaya-sik%e1%b9%a3a/ "3:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Vrishchika had been seeing several kids of patients affected by the chemical leak that had happened sometime ago. While she saw some purely for routine clinical practice, she was also particularly interested in the several cases exhibiting heterotaxy and had amassed a collection of such. With Indrasena she had identified a collection of mutations in these patients and those from elsewhere. Eventually, with Lootika and Somakhya they had translated it into some interesting and ancient biochemistry concerning membrane protein stability that was widespread in eukaryotes. Now they were sitting down at Somakhya and Lootika's house to work on a manuscript on this matter. As they did so, they asked their sons to get some lessons from Jhilleeka.

To get her nephews started Jhilleeka asked them: "So what did you two learn from Tigmanika's grandfather little ones?"\
Guhasena: "He taught us something very important which the very basis of the apprehension of the space of our everyday life: the gardabha-न्याय."\
J: "Could you tell me what that is?"\
Tigmanika: "He said that even though in the early days when our ancestors roamed on the steppes they saw the braying ass as something alien and evil, they came around to see it as an animal that should be respected. He felt that was because it taught us the gardabha-न्याय: [गर्दभो 'पि त्र्यश्रे ह्रसिष्टम् पथम् आधत्ते ।]{style="color:#0000ff;"}"\
Guhasena: "It is the Donkey's theorem: No side a triangle is greater than the sum of the other two."\
J: "What more did his tell you about this?"\
G: "He also taught us the area of a triangle and the भुजा-कोटि-कर्ण-न्याय."

J: "So, nephews, what is the सूत्र for the area of a triangle given its three sides?"\
T: "[भुजयोगार्धस्य भूजयोगार्ध-प्रत्येक-भुजोन-घातात् पदम् फलम् इति ।]{style="color:#0000ff;"}"\
J: "That formula is known from at least the work of the old scientist Brahmagupta who had a mix of good and bad ideas. Guha write it down on the board."

G:  $A = \dfrac{1}{4}\sqrt{(a+b+c)(a+b-c)(a+c-b)(b+c-a)}$ 

J: "Boys do you realize from that is self-evident from this that if a triangle exists the Donkey's theorem must be true?"\
T and G: "Yes! If any side of a triangle were greater than the sum of the other two then by this formula the triangle cannot have an area because what is under the square root will be negative. So the existence of a triangle predicates the gardadbha-न्याय."

J: "That is good. Knowing your grandfather, I am sure he gave you some problems --- he used to entertain me with such whenever we met. Were you able to solve them?"\
T: "Yes, he gave us three. We solved one of them easily and we believe we solved the second one but mom said our solution was not good enough and that our grandfather might call us idiots if we show it to him."\
J: "OK. What were they?"
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/donkey.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


G: "The first goes thus: Let  $a, b, c$  be the 3 sides of a triangle from shortest to longest side. In what interval will the ratio of the sum of the short and long side to the middle side fall? We solved it thus: At one extreme will be a degenerate triangle whose sides will be  $a=0, b=1, c=1$ . At the other extreme we have another degenerate triangle with sides  $a=1, b=1, c=2$ . If we reduce the triangles, all others will have sides in between these. Hence,  $1 \le \tfrac{a+c}{b} \le 3$ ."

J: "Not bad. That's indeed the case. It is not for nothing our intellectual predecessors would say: '[त्रै-राशिकेनैव यद् एतद् उक्तं व्याप्तं स्व-भेदैर् हरिणेव विश्वम् ।]{style="color:#0000ff;"}' All that has been stated is permeated by the principle of proportions even as विष्णु pervades all with his own \[manifold] forms. Those forms of विष्णु are the 1000s which exist in the different adhvan-s as laid out in the teachings of the वैष्णव mode of तान्त्रिकोपासन. Alright, so what was the next problem whose solution was found wanting by Lootika?"

T: "Let  $a, b, c$  be the 3 sides of a triangle from shortest to longest side. What will be the interval in which the maximal proportion of the short to the middle side or middle to the long side fall?"\
J: "So, what did you do?"\
T: "One can see that when we have and equilateral triangle or degenerate triangles such that  $a=0, b=1, c=1$ , or  $a=1, b=1, c=2$  then  $\tfrac{a}{b}=1; \tfrac{b}{c}=1$  or  $\tfrac{a}{b}=0; \tfrac{b}{c}=1$  or  $\tfrac{a}{b}=1; \tfrac{b}{c}=\tfrac{1}{2}$ ; So,  $\max\left(\tfrac{a}{b}, \tfrac{b}{c}\right) \le 1$ . Thus, we have one end of the interval but the other end of the interval needed some experiments to determine."\
G: "We can see that it should be a degenerate triangle such that from  $\tfrac{a}{b}, \tfrac{1-\tfrac{a}{b}}{1}$  we should get the maximum. By writing out different proportions for degenerate triangles we realized it is some number close to 0.618."\
J: "That's not bad nephews. You are close to the answer. What you have to do is to get the actual number with some formal algebra. You were right in reasoning that it should be a degenerate triangle where  $a+b=c=1$ . If  $\tfrac{a}{b}$  were to decrease from 1 such that it remains greater than  $b$  then you come closer and closer to the lower bound of your desired interval. But if you cross that bound then  $b$  becomes greater than  $\tfrac{a}{b}$ . So your bound will be where the two are equal. Thus, you can write:  $\tfrac{a}{b}= b; a=b^2$ . But since it is a degenerate triangle  $a=1-b$ ; by plugging that we get the quadratic equation  $b^2+b-1=0$ . Now, I have taught you how to solve those; so write the answer on the board"\
G and T:  $b=\tfrac{\sqrt{5}-1}{2}$ \
J: "As you can see that number is close to what your experiment yielded. It is known as  $\phi'$  or the conjugate Golden Ratio. Thus, as a consequence of the gardabha-न्याय the maximal proportion of the successive sides of a triangle always lies in the interval  $\phi' \le \max\left(\tfrac{a}{b}, \tfrac{b}{c}\right) \le 1$ . The more advance in age the more you will realized of the ubiquity of the former bound like Indra pervading existence. Let us move on; how did you fare with the 3rd problem?"

G and T: "Given an angle  $\theta$ , construct a triangle such that its sides  $a, b, c$  will be in an arithmetic progression. We have been able to obtain a few cases like  $1, 1, 1$  or  $3, 4, 5$  but we have been breaking our head over how to solve it generally?"\
J: "Fine. First I'll give you the construction and then explain the rationale. Do this construction on your tablets as I tell you:
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/angle_arithmetic_prog_tri.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```



 1.  Draw  $\angle{\theta}$  with point  $A$  as its apex and one ray in the horizontal direction. On the other ray mark out a point  $Q$ . From  $Q$  drop a perpendicular to meet horizontal line at point  $P$ .

 2.  Along the horizontal ray of  $\angle{\theta}$  mark point  $R$  such that  $\overline{AR}=2\overline{AQ}$ .

 3.  Trisect  $\overline{PR}$  to get  $\overline{PC'}=\tfrac{\overline{PR}}{3}$ .

 4.  Mark point  $C$  on the horizontal ray of  $\angle{\theta}$  such that  $\overline{AC}=\overline{PC'}$ 

 5.  Mark point  $B$  on ray  $\overrightarrow{AQ}$  of  $\angle{\theta}$  such that  $\overline{AB}=\tfrac{\overline{AQ}}{2}$ .\
6)  $\triangle ABC$  is the desired triangle with sides in an arithmetic progression.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/arith_trigs1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Now check out a few examples of such triangles with this construction and verify if we are getting what we want."

G and T: "Aunt, that is almost like magic: how did you know this would work?"\
J: "It is simple. You have to first bring to mind an important generalization of the भुजा-कोटि-कर्ण-न्याय known as the कोटिज्या-न्याय which applies to any triangle:

 $$c^2=a^2+b^2-2ab\cos(C)$$ 

Kids, you can see that if the sides are in arithmetic progression we can write them as  $a=x-k, b=x; c=x+k$ . Moreover, constant of the progression  $k=b-a=c-b$ . Now we apply the कोटिज्या-न्याय for such a triangle:

 $$c^2+b^2-2bc\cos(A)=a^2$$ 

 $$(x+k)^2+x^2-2x(x+k)\cos(A)=(x-k)^2$$ 

 $$x^2+2kx+k^2+x^2-2x(x+k)\cos(A)=x^2-2kx+k^2$$ 

 $$2k+x-2(x+k)\cos(A)=-2k$$ 

 $$4k+x=2(x+k)\cos(A)$$ 

 $$\cos(A)=2-\tfrac{3b}{2c}$$ 

That is the formula which we have expressed in the construction I just gave you. When we are done you can figure that out and how the कोटिज्या-न्याय implies the gardabha-न्याय."

Jhilleeka next decided to use the gardabha-न्याय to introduce her nephews to a fundamental understanding of the conics. J: "I'm sure Tigma's grandfather has introduced you to the conics, the ellipse, the parabola and the hyperbola those most important curves a person ought have a proper understanding of. Such was a signal sent to the यवनाचार्य-s by their god Apollo, who is a cognate of our Rudra."\
T and G: "Yes we have learned to draw them and have even taught our sister Prithika who seems to have taken a fancy for drawing ellipses."\
J: "That is indeed an auspicious sign. Now, if I tell you that the existence of conics are a natural consequence of the truth of the gardabha-न्याय (triangle inequality), then would it surprise you two?"\
T and G: "That sounds very interesting. Could you please tell us more."

J: "OK. Now consider the following problem: Given 2 points  $F_1$  and  $F_2$  on the same side of a line  $l$ , when will the sum of the distances from a point  $P$  on  $l$  to  $F_1$  and  $F_2$  reach a minimum. I advise you to follow along reproducing my construction and to think about this deeply by yourself for sometime after I'm done."
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/ellipse_donkeyt.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


J: "As you can see, we have point  $P$  on  $l$  and  $F_1$  and  $F_2$ .

 1.  Reflect  $F_1$  on  $l$  to get  $F'_1$ . Because it is a mirror image  $\overline{F_1P}=\overline{F'_1P}$ .

 2.  We thus get  $\triangle{F'_1PF_2}$  for which the Donkey's theorem applies thus:  $\overline{F'_1P}+\overline{F_2P} \ge \overline{F'_1F_2}$ 

 3.  Hence, as  $P$  moves along  $l$  we will get  $\min(\overline{F'_1P}+\overline{F_2P})$  only then the triangle become degenerate, i.e. point  $P$  lies on the segment  $F'_1F_2$ . We can call that version of  $P$  point  $P_0$ . Thus, we get  $\min(\overline{F'_1P}+\overline{F_2P})=\overline{F'_1P_0}+\overline{F_2P_0}=\overline{F_1P_0}+\overline{F_2P_0}$ 

You can see that for any given line  $l$  there will be only one such point  $P_0$  with respect to the points  $F_1, F_2$  and the sum  $\overline{F_1P_0}+\overline{F_2P_0}$  will be some constant  $k$ , which will have to be greater than the distance between  $F_1, F_2$ . Now, there will be other lines like  $l$  on which a  $P_0$  will yield the same constant with respect its distances from  $F_1, F_2$ . So what will the total set of such points amount to?"

T: "Since we define an ellipse as the curve on which every point has the same sum of distances from its 2 foci, these  $P_0$  should define an ellipse. Now I see how the mere existence of an ocean of lines and points in the fabric of space automatically specify ellipses!"\
J: "Good. So what will that collection of lines be?"\
G: "These should all be tangents to the ellipse since only one point on the line  $P_0$  also lies on the ellipse."\
J: "Good. Also observe in the figure I've drawn that the angle made by  $\overline{F_1P_0}$  and  $l$  on one side is congruent to the angle made by  $\overline{F_2P_0}$  and  $l$  on the other side. As you would have learned the angles are congruent in this way when you reflect a light ray of a mirror. Thus, you can see that if you make a mirror with the curvature of an ellipse and place a light source at one focus then the rays will be reflected and converge at the other focus.

Now the two of you work out a similar case when the points  $F_1$  and  $F_2$  are on opposite side of the line  $l$ . However, in this case we instead seek when the difference of the distances of a point  $P$  on line  $l$  from  $F_1$  and  $F_2$  reaches a maximum rather than a minimum"
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/hyperbola_donkeyt.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


T: "We learned of another way writing of the Donkey's theorem: as  $a+b \ge c$  we have  $a \ge c-b$ . If  $b \ge c$  we write the absolute value; thus,  $a \ge|c-b|$ . If the absolute value of the difference were to be bigger than  $a$  it would violate the Donkey's theorem. Therefore,  $a \ge |c-b|$  and this is the form of the theorem we have to use to find the solution."\
G: "I can intuitively feel that we are headed to a hyperbola."\
J: "That's right but now work it out fully."

The two worked it out with their aunt helping and after sometime obtained the following solution on their tablets (Figure 5):

 1.  Reflect Point  $F_1$  on line  $l$  to get  $F'_1$ 

 2.  Due to the reflection,  $\overline{F_1P}=\overline{F'_1P}$ . Further, since  $F_1, F_2$  are fixed  $F'_1F_2$  is of fixed length.

 3.  Apply the Donkey's theorem to  $\triangle{F'_1PF_2}$ :  $\overline{F'_1F_2} \ge |F'_1P-F_2P|$ .

 4.  The difference  $|F'_1P-F_2P|$  being less than the distance between  $F'_1,F_2$  will attain a maximum only when  $\triangle{F'_1PF_2}$  becomes degenerate and  $P, F'_1, F_2$  lie on the same line --- let us call this unique  $P$  as  $P_0$  and here  $\overline{F'_1F_2} = |F'_1P_0-F_2P_0|$ 

 5.  By definition a hyperbola is the curve such that points on it have a constant difference of their distances from the 2 focii. Thus, all  $P_0$  that will yield a certain constant difference  $k$  of their distances from  $F_1, F_2$  will define a hyperbola and the corresponding lines will be tangents of that hyperbola.

Jhilleeka remarked: "Here too you will notice that, as in the ellipse, the line  $l$  bisects the  $\angle{F_1P_0F_2}$ ."\
G: "But aunt Jhilli what about the parabola?"\
J: "That is a good question. You may know this thing about the conics: There are many flavors of ellipses and hyperbolas but ultimately all parabolas and circles are the same. Imagine an ellipse and a hyperbola with a common fixed focus and passing through a fixed point. Let the second focus of each of them move in opposite directions towards infinity. When the two foci of the ellipse coincide we get a circle. As the second focus moves towards infinity the ellipse converges to a parabola from within. As the second focus of the hyperbola moves to infinity it converges to the parabola from without. Thus, the parabola is a limit for both the ellipse and the hyperbola and is unique. Because of it emerging from one of the foci going to infinity it is endlessly open on one side and has only one axis of symmetry unlike the other two. Hence, we need to take a slightly more complicated approach to understand its emergence from the Donkey's theorem. Let me show you that."
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/parabola_donkey_bkk.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


J: "For this we have to consider the following setup: Let there be two lines  $l$  and  $l'$  and a fixed point  $F$ . For a point  $P$  on  $l$ , when will the signed difference of the distances of  $P$  to  $l'$  and to  $F$  reach a maximum? To work this out do this construction:


 1.  Reflect point  $F$  in  $l$  to get  $F'$ . Hence,  $\overline{FP}=\overline{F'P}$ .

 2.  From  $F, F'$  choose that point which is closer to  $l'$ . In our case (Figure 6) it is  $F'$ . Since  $F$  is fixed so is  $F'$  and the distance of  $F'$  to  $l'$  is a positive constant  $k$ .

 3.  From the construction we can see that the distance of  $P$  from  $l'$  will be  $k+y$  when  $P$  is above point  $C$ , i.e. the point of intersection of  $l$  and  $d$ , a line parallel to  $l'$  through  $F'$ . When  $P$  is below  $C$  then its distance from  $l'$  will be  $k-y$ .

 4.  Thus, the signed difference between the distances of  $P$  to  $l'$  and  $F'$  (or  $F$ ) will be  $k \pm y- \overline{F'P}$ .

 5.  We see that  $\triangle{F'BP}$  is a right triangle. A specific consequence of the Donkey's theorem for such a triangle is that the कर्ण (hypotenuse) will always be longer than the two legs. Hence,  $k \pm y -\overline{F'P} < k$  unless  $+y=\overline{F'P}$ . In this instance,  $k + y -\overline{F'P} = k$  and it will be the maximum signed value that it can ever attain. This will happen only when  $F'$  lies on the perpendicular dropped from  $P$  to  $l'$ . We can call this special  $P$  as  $P_0$ .

You can see that the collection of all such  $P_0$  on lines  $l$  defined by the maximal signed difference of the distances of  $P$  from  $l'$  and  $F$  being  $k$  will amount to a parabola. The parabola as you all know is defined as curve such that every point on it shows the same distance from the focus and a fixed line. You can see that the fixed line is  $d$  the directrix of the parabola."

T and G: "It is amazing that the conics are a natural consequence of boundary conditions of the gardabha-न्याय."\
J: "Study and think about this simple demonstration more deeply. The old thinkers thought that certain very simple principles lay at the heart of diverse manifestations in mathematics: For example, the soma-drinking निलकण्ठ of Cerapada thought that त्रैराशिक-न्याय and the भुजा-कोटि-कर्ण-न्याय pervades all astronomical calculations ([भुजा-कोटि-कर्ण-न्याययेन त्रैराशिक-न्यायेन चोभाभ्यां सकलं ग्रह-गणितं व्याप्तम् ।]{style="color:#0000ff;"}). भास्कर-II expanding on what he mentioned regarding the deva विष्णु states further that:

[यथा भगवता श्री-नारायणेन जनन-मरण-क्लेश-अपहारिणा निखिल-जगत्-जनन+एक-बीजेन सकल-भुवन-भावनेन गिरि-सरित्-सुर-नर-असुर+आदिभिः स्व-भेदैर् इदं जगत् व्याप्तं तथा +इदम् अखिलम् गणित-जातम् त्रैराशिकेन व्याप्तम् ॥]{style="color:#0000ff;"}

Just as the lord श्री-नारायण, who takes away the afflictions of birth and death, who is the one seed that gave rise to the entire universe, causing the manifestation of all the worlds by pervading this universe within his own forms as mountains, rivers, gods, men, demons and the like so also all these branches of mathematics are pervaded by the त्रैराशिक (i.e. the principle of proportions).

You may wonder why so much ado over something as simple as the principle of proportions and its exaltation by comparison to विष्णु's universal manifestation taught by the पाञ्चरात्रिक-s. Nephews, I'd say that it is not mere hyperbole nor a peculiar analogy but an intertwined metaphor of how the origin of the universe from the root cause termed श्री-नारायण according to the पाञ्चरात्रिक-s, i.e. the conjugate of लक्ष्मी and विष्णु is an expression of elementary mathematical principles. The principle of proportions governs the measurement of space and existence of scale that is the basic foundation of this universe. Likewise, the simple gardabha-न्याय we have dwelt upon is a pervasive principle linked to the generalization of the भुजा-कोटि-कर्ण-न्याय that "selects" for particular genera of curves in space on which the foundations of physics rest. [इत्य् अलं विस्तरेण ।]{style="color:#0000ff;"}"

