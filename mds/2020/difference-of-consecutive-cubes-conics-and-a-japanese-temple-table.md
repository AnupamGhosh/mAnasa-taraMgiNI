
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Difference of consecutive cubes, conics and a Japanese temple tablet](https://manasataramgini.wordpress.com/2020/02/08/difference-of-consecutive-cubes-conics-and-a-japanese-temple-table/){rel="bookmark"} {#difference-of-consecutive-cubes-conics-and-a-japanese-temple-tablet .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 8, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/02/08/difference-of-consecutive-cubes-conics-and-a-japanese-temple-table/ "5:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**Introduction**\
In our part of the world, someone with even a nominal knowledge of mathematics might be aware of the taxicab number made famous by the conversation of Ramanujan and Hardy: the smallest number that can be expressed as the sum of two distinct pairs positive cubes:  $1729=1^3 + 12^3 = 9^3 + 10^3$ . This number is just one of a family of such taxicab numbers with deep connections to other objects in the mathematical world that were discovered by Ramanujan long before anyone knew of their significance. There are several other interesting questions, which, in a similar vein, relate to the sum and difference of cubes. From Fermat's last theorem we know that the indeterminate equation  $x^3+y^3=z^3$  cannot have any non-trivial integer solutions (non-trivial being where none of the solutions are 0). However, this still leaves open other possibilities with sums of 3 cubes and differences of the cubes and the like. For example, one popular and widely investigated one asks which integers can be expressed as a sum of any three cubes. When we learned that Fermat's last theorem precludes integer solutions for  $x^3+y^3=z^3$  in our early youth, we wondered if there are non-trivial positive integer solutions indeterminate equation:  $x^3+y^3+z^3=w^3$ . Soon we became interested and investigated one specific version of it of the form:

 $$x^3+y^3=w^3-z^3$ , where  $w=z+1$ ; thus, we have  $x^3+y^3=3z^2+3z+1 \;\;\; (\S 1)$$ 

This led us to discovering and proving for ourselves a simple arithmetic theorem concerning such cubes. Later, we read to our amazement that a related problem had been considered on a Japanese temple tablet in a beautiful "mystical"-sounding verse and solved in a commentary on it. This inspired us to look at the problem again and we discovered further interesting links between conics and these cubes. We detail these explorations and the Japanese temple tablet below.

**The difference of consecutive cubes and an arithmetic theorem**\
We first asked when does the difference of consecutive cubes result in a sum of two cubes. This idea came to us as a parallel to the भुजा-कोटि-कर्ण triples. While we have 4 cubes in  $(\S 1)$ , given that two are consecutive, we only have a triple of distinct positive integers  $x, y, z$  bearing the relationship:

 $$z = \dfrac{\sqrt{12x^3 + 12y^3 - 3} -3}{6} \; \; \; (\S 2)$$ 

Thus, we have to search for the all the cases where  $(\S 2)$  evaluates to an integer given dyads of positive integers  $x, y$ . If we order the resultant triples such that  $x < y < z$  we get a unique set of triples, e.g. 1, 6, 8:  $1^3+6^3=9^3-8^3$ . Figure 1 shows a plot of  $x, y, z$  for all  $x, y< 20000$ , a total of 1173 triples.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/cube.diff3d_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


The plot initially reminds one of a first time observer of the sky with a great mass of stellar points with some vague patterns among them. Given that  $z$  is dependent on  $x, y$  and the points lie on a single curved surface in 3D, we can reduce dimensions for simplicity and plot just  $x, y$  (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/cube.diffsolns_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


As we have ordered our triples  $x, y$  all points obviously lie above the  $y=x$  line. We then notice the first clear pattern. There are set of regularly arranged points (in red in Figure 2) that mark the left margin of the plot. These points lead us to the following theorem:

**The cube of every positive integer is equal to the difference of the cubes of two consecutive positive integers minus the cube a third positive integer.**

To prove this let us consider those regularly arranged points. The first few of them are tabulated below:

    x     y

  - -- -----
    1     6
    2    17
    3    34
    4    57
    5    86
    6   121

We observe that the  $x$  values of these points include each positive integer in order. The corresponding  $y$  grows rapidly and follows a peculiar pattern. This pattern is represented by the numbers lying on the 5th spoke hexagonal spiral (Figure 3) where 6 spokes separated by the rotation angle of  $\tfrac{\pi}{3}$  radians pass through the origin.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/cube.diffspiral_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


The numbers lying on the 5th spoke of this spiral can be represented by the formula:  $3n^2+2n+1$ . Thus, we have  $x=1, 2, 3...n$  and the corresponding  $y=3n^2+2n+1$ . Hence,

 $$x^3+y^3=n^3+(3n^2+2n+1)^3= 3(3n^3 + 3n^2 + 2n)^2 + 3 (3n^3 + 3n^2 + 2n) + 1 \;\;\; (\S 3)$$ 

If we write  $z=3n^3 + 3n^2 + 2n$ , the right hand side of  $(\S 3)$  becomes  $(z+1)^3-z^3$ 

 $$\therefore x^3=(z+1)^3-z^3-y^3$ , where  $x=n;\; y=3n^2+2n+1;\; z=3n^3 + 3n^2 + 2n \;\;\; _{...\blacksquare}$$ 

Thus, this theorem illustrates one deep connection between the cubes of numbers the hexagonal number spiral.

**The Japanese temple tablet**\
The Samurai intellectual Shiraishi Chochu recorded a problem inscribed in tablet hung at a temple in the 1800s by the poorly known mathematician Gokai Ampon:

*"There are three integral numbers, heaven, earth, and man, which being cubed and added together give a result of which the cube root has no decimal part. Required to find the numbers."* -- translation from Smith and Mikami

In essence, Gokai Ampon wants us to find integer solutions to indeterminate equation  $x^3+y^3+z^3=w^3$ , which is the same question that had originally prompted our quest. His solutions recorded by Shiraishi Chochu are a particular class of solutions to  $x^3+y^3=3z^2+3z+1$ , i.e. the sum of the cubes of two positive integer being the difference of the cubes of consecutive positive integers.

If we look at Figure 2, we find that among the mass of points with apparently no discernible order there are a group of regularly arranged points coming in pairs with the same  $x$  value and lying on a slim parabola (colored purple). It was this group of points that caught Gokai Ampon's attention. The first few of the pairs are listed below:

      x   [y~1~]{.math .inline}   [y~2~]{.math .inline}

  - ---- ----------------------- -----------------------
      3                       4                      10
     12                      19                      31
     27                      46                      64
     48                      85                     109
     75                     136                     166
    108                     199                     235

We observe that for these points  $x$  takes the form  $3n^2; \; n=1, 2, 3...$ . For  $y_1$  we get a fit with  $6n^2 -3n +1$  and for  $y_2$  with  $6n^2 +3n +1$ . Thus, with  $y_1$  we have:

 $$x^3+y_1^3=(3n^2)^3+(6n^2 -3n +1)^3 = 3 \cdot 3^2(2 n^2-3 n^3 - n)^2 + 3 \cdot 3 (2 n^2-3 n^3 - n) + 1 \;\;\; (\S 4)$$ 

By writing  $z_1=3(2 n^2-3 n^3 - n)$  and plugging it in  $(\S 4)$  we get  $x^3+y_1^3+z_1^3= (z_1+1)^3$ , which is a family of valid solutions to the Japanese problem. Given that we are only considering positive integers, the final parameterization will be  $z_1=3(3 n^3- 2 n^2+ n)-1$ . Similarly, with  $y_2$  we get:

 $$x^3+y_2^3=(3n^2)^3+(6n^2 +3n +1)^3 = 3 \cdot 3^2 (3 n^3 + 2 n^2 + n)^2 + 3 \cdot 3 (3 n^3 + 2 n^2 + n) + 1 \;\;\; (\S 5)$$ 

By plugging  $z_2= 3 (3 n^3 + 2 n^2 + n)$  in  $(\S 5)$  we get  $x^3+y_1^3+z_2^3= (z_2+1)^3$ , the second valid family of valid solutions to the Japanese problem.

One can see that these two solutions are lattice points on a parabola whose parametric equation is  $(x=3t^2, y=6t^2 \pm 3t +1)$ . Thus, both our solution which covers the cubes of every positive integer and the Japanese solutions are 2 distinct parameterized families corresponding to parabolas in the  $(x,y)$  plane.

**The elliptical families**\
When we learned of the Japanese solutions, we wondered if there might be any other families of solutions hidden within the apparent disorder of the total set of all solutions. Returning to the  $x-y$  plot, we noted that several points lie on arcs of increasing size (shown as orange and pink points in Figure 2). Examining these, we discovered that they are integer points lying on pairs of related ellipses of eccentricity  $\sqrt{\tfrac{2}{3}}$  that have equations of the form:

 $$\begin{cases} x^2-xy+y^2-ax-by+c=0 \lbrack 6pt\rbrack x^2-xy+y^2-bx-ay+c=0 \end{cases} \; \; \; (\S 6)$$ 

Where the 3 parameters  $a, b, c$  are defined by:

 $$c=27u^4+9u^2+1\lbrack 6pt\rbrack a=c+(9u^2+1) = 27u^4 + 18u^2 + 2\lbrack 6pt\rbrack b=c-(9u^2+2)= 27u^4 - 1$$ 

The lattice points of  $(\S 6)$  are solutions to  $(\S 1)$  and emerge at special values of  $u$ . First few values of  $u$  and the number of solutions they yield on the 2 corresponding ellipses are shown in Figure 4 and Table 1.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/cube.diffelliptical_fig4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


Table 1

![cubediff_table1](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/cubediff_table1.png){width="75%"}

The ellipses first corresponding to the few values of  $u$  that yield solutions numbering  $\ge 2$  are show in Figure 5. Not all values of  $u$  are equally rich in terms of solutions for  $(\S 1)$ . The integers (1, 2, 3...), thirds i.e.  $n \pm \tfrac{1}{3}$  consistently yield solutions but the integers tend to be clearly richer than the thirds (Figure 4). Other than integers, certain irrational values of the form  $u=\sqrt{\tfrac{6n+1}{6}}$  are particularly rich in solutions. Other than those,  $u=\sqrt{12}$  and certain values of the form  $u=\sqrt{\tfrac{6n+1}{18}}, u= \sqrt{\tfrac{6n+2}{18}}$  are also rich in solutions. Thus,  $12a, 12b, 12c$  are integers for all  $u$ , yielding solutions lying on ellipses. However, it remains unclear if there is a general rule to determine which of the quadratic surds that take the above forms will be  $u$  that yield solutions for  $(\S 1)$ . In any case the given that integer  $u$  yield solutions for  $(\S 1)$  that lie on defined ellipses the family of such elliptical solutions is infinite.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/cube_ellipse_complete_workout.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


Finally, we may note one special feature of the family of elliptical solutions that are specifically associated with the integer values of  $u$ . Given that the solutions  $x, y, z$  define a simple curved surface (Figure 1), i.e. the surface does not show any folding, there can utmost be 2 pairs of  $x, y$  that yield the same  $z$ . There are 29  $z$  which can be derived from 2 distinct pairs of  $x, y< 20000$ . For example:  $9^3+ 58^3= 22^3 + 57^3=256^3-255^3$ . Thus, (9, 58) and (22, 57) form a pair that yield the same  $z=255$ . The majority of such pairs of solutions with the same  $z$  lie on the ellipses arising from integer  $u$ . Further, each such ellipse contains a pair whose whose  $x$  values are respectively defined by  $x_1 =9u^3; x_2= 9u^3+9u^2+3u+1$ . The corresponding  $y$  values can be obtained by plugging  $x_1$  into the second ellipse and  $x_2$  into the first ellipse in  $(\S 6$ ). Thus we get  $y_1= 27n^4 + 18n^3 + 9n^2 + 3n + 1; y_2=27n^4 + 18n^3 + 9n^2 + 3n$ ; thus, for these cases  $y_1=y_2+1$ . These pairs are shown below for  $u=1, 2, 3, 4$ .

Thus, these paired values define an infinite family by themselves lying on in the curves defined by the below parametric equations (Figure 5, blue curve):

 $$\begin{cases} x= 9t^3\lbrack 6pt\rbrack y= 27t^4 + 18t^3 + 9t^2 + 3t + 1 \end{cases}\lbrack 10pt\rbrack \begin{cases} x=9t^3+9t^2+3t+1\lbrack 6pt\rbrack y=27t^4 + 18t^3 + 9t^2 + 3t \end{cases}$$ 

There are a minority of pairs which lie outside of the ellipses. We do not know as yet if they define any other families of solutions. More generally, it is also not clear if there are any other families of solutions beyond the above parabolic and elliptical families.

**Sum of cubes of 2 positive integers that equal the difference of cubes successive same-parity positive integers**\
In this final section we shall briefly consider a related indeterminate equation:

 $$x^3+y^3=(z+2)^3-z^3=6z^2+12z+8 \; \; \; (\S 7)$$ 

One can see right away that some of the regular families of solutions of  $(\S 7)$  are related to those of  $(\S 1)$ . The first relates to the above theorem regarding the cubes of every positive integer. In this case the equivalent is:

**The cube of every positive even number is equal to the difference of the cubes of two consecutive even numbers minus the cube of another even number.**

These correspond to the solutions to  $(\S 7)$  of the form  $(x=2n, y= 6n^2 + 4n + 2$  (Figure 6, violet curve). Further, the equivalents of the solutions to Gokai Ampon's points in this case lie on the parabola defined by the parametric equation:  $(x=6t^2, 12t^2 - 6t + 2)$  (Figure 6, purple curve). However, the solutions to  $(\S 7)$  feature a unique parabolic family of solutions with no equivalent among the solutions of  $(\S 1)$ . These lie on the parabola defined by the parametric equation:  $(3t^2 - t + 1, 3t^2 + t + 1)$  (Figure 6, orange curve). These correspond to  $x, y$  such as:\
(3, 5); (11, 15) (25, 31) (45, 53)... Thus,  $x=3n^2 - n + 1$  and  $y=3n^2 + n + 1$  provide another link to the hexagonal number spiral as they correspond to numbers that respectively lie on its 2nd and 4th spoke (Figure 3). With this in hand, we can show that for these  $x, y$  give rise to  $z=3 n^3 + 2 n - 1$ , which defines the sequence: **4, 27, 86, 199, 384, 659, 1042...**

As with the solutions to  $(\S 1)$ , here too we have the equivalent elliptical families corresponding to the integer lattice points on ellipses of eccentricity  $\sqrt{\tfrac{2}{3}}$  that have equations of the form:

 $$x^2-xy+y^2-ax-by+c=0 \lbrack 6pt\rbrack x^2-xy+y^2-bx-ay+c=0$$ 

Here the 3 parameters  $a, b, c$  are defined by:

 $$a=54u^4 + 36u^2 + 4\lbrack 6pt\rbrack b=54u^4 - 2\lbrack 6pt\rbrack c=108u^4 + 36u^2 + 4$$ 

However, the  $u$  which yield elliptical solutions for  $(\S 7)$  are the same as those that yield solutions for  $(\S 1)$  and there is an equivalence in the corresponding solutions. Figure 6 shows a few elliptical solutions  $\left(u=2, \sqrt{\tfrac{37}{3}}, \tfrac{\sqrt{73}}{3}, 3\right)$ .

In conclusion, this exploration reveals connections between a certain class of cubic indeterminate equations and families of solutions defined by particular parabolas and ellipses. It is not known to us if any one previously studied these elliptical families or reported any other families beyond those considered here.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/cube2_ellipse_workout.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


 

