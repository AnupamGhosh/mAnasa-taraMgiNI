
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Conic conquests: biographical and historical](https://manasataramgini.wordpress.com/2020/07/15/conic-conquests-biographical-and-historical/){rel="bookmark"} {#conic-conquests-biographical-and-historical .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 15, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/07/15/conic-conquests-biographical-and-historical/ "5:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[PDF file of same article](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/ellipse.pdf)

Studying mathematics with our father was not exactly an easy-going experience; nevertheless, it was the source of many a spark that inspired fruitful explorations and life-lessons. We recount one such thread here, and reflect on how our personal education matched up to history. When we were a kid, and learning about the area and perimeter of plane figures --- we used to do it with a graph paper --- our father told us that  $\pi$  was not  $\tfrac{22}{7}$  as our school lessons claimed but a "never-ending" number. He pointed to us that  $\tfrac{22}{7}$  was just like  $\frac{1}{3}$  which repeated after a certain run of numbers. This inculcated in us a life-long fascination, to the degree our meager mathematical capacity allowed, for both these types of numbers and their deeper significance. Some time thereafter our parents took us to watch a series of documentaries that were screened at a nearby auditorium on the evolution of man and various intellectual developments in science and mathematics in the Occident. In one of them, the presenter mentioned that the यवनाचार्य of yore, Archimedes, had arrived at the approximation  $\tfrac{22}{7}$  for  $\pi$  by inscribing polygons in a circle. This sparked a great a excitement in us for we were then fascinated by construction of regular polygons  $(\mathcal{P}\_n)$ . On returning home from the screening, we quickly got back to that geometric activity realizing that the very first construction we learned in life, that of a regular hexagon, yielded  $\pi \approx 3$  (Figure 1).

![Figure 1. Construction of a regular hexagon (polygon  $\mathcal{width="75%"}

That was hardly anything to write home about, but even as people used to think that "developmental ontology recapitulates phylogeny", this realization recapitulated the very beginnings of the human knowledge of  $\pi$ . This value was used for the crudest of constructions in the Vedic ritual going back to its ancient roots. It has been suggested that this knowledge is encapsulated in a cryptic manner with a peculiar play on the name of the god Trita (meaning 3rd):

[indro yad वज्री धृषमाणो अन्धसा bhinad valasya परिधींर् iva त्रितः |]{.mark .has-inline-color style="background-color:rgba(0,0,0,0);color:#0000ff"}

When the vajra-wielding Indra invigorated by the soma draught, split the \[fortification] perimeter of Vala (the दानव's name is used in a possible play on term for a circle), even as Trita \[had done].

The allusion to the god Trita in the simile here is unusual and is evidently an allusion to his breaking out a well (an enclosure with a circular section); this strengthens the idea that a word play on the circular perimeter being split up in 3 by the diameter was exploited by Savya आङ्गिरस. This crude approximation continued to be used in स्थूल-व्यवहार by the Hindus and the Nirgrantha-s till the medieval period (e.g., in the nagna text तिलोयसार: [वासो तिगुणो parihi |]{.mark .has-inline-color style="background-color:rgba(0,0,0,0);color:#0000ff"}; the Prakrit corresponds to the Sanskrit [व्यासो त्रिगुणः परिधिः |]{.mark .has-inline-color style="background-color:rgba(0,0,0,0);color:#0000ff"}:  $P(\bigcirc)= 3d$  ). We also hear that the Jews used the same value in building a religious structure in their early history. While that is a lot of words to expend on this crudest of approximations, one could say that it at least gets you to  $95.5\%$  of the real thing. Around that time our father had introduced us to the radian measure of an angle and informed us that it was the natural one, for after all the number 360 for the degree measure was an arbitrary one coming from an approximation of the year. This most elementary of constructions, the hexagon, gave us an indelible visual feel for the radian for after all if 3 got us to  $95.5\%$  of  $\pi$  then the radian should be roughly  $57.3^\circ$ . More importantly, it informed us that this unit is best understood in multiples of  $\pi$  and that the interior angle of the hexagon should be  $\tfrac{\pi}{3}$ . In terms of history we had caught up with the emergence of the germ of this concept in आर्यभट.

![Figure 2. A regular dodecagon from a square and equilateral triangles](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/dodecagon_area.png){width="75%"}

The aftermath of the above apprehension led us to doing a few more constructions and origami folding that led us to a somewhat more interesting realization from an aesthetically pleasing construction of a regular dodecagon which goes thus (Figure 2):


 1.  Draw a starting square and an equilateral triangle facing inwards on each of its sides (relates to the basic origami construction of an equilateral triangle from a square paper as indicated by Sundara Rao more than 100 years ago).


 2.  The inward-facing vertices of these equilateral triangles will define a new square orthogonal to the original square.


 3.  The 4 midpoints of the sides of this new square and the intersections of the equilateral triangles help define the sides of a regular dodecagon --- in effect arising from a geometric multiplication of  $4 \times 3$ . Thus, this dodecagon is inscribed in the inner square.


 4.  Notably, this construction by itself helps define two tiles, a  $150^\circ-15^\circ-15^\circ$  isosceles triangle  $T_1$  (violet) and an equilateral triangle  $T_2$  (green). Using these tiles both the inner square  $(\mathcal{P}_4)$  and the dodecagon  $(\mathcal{P}_{12})$  can be completely tiled thus (Figure 2):  $\mathcal{P}_4=32T_1+16T_2$  and  $\mathcal{P}_{12}=24T_1+12T_2$ . This means that the area of the inscribed dodecagon is  $\tfrac{3}{4}$  the area of the inner square.


 5.  A corollary to the above is that if a unit circle were inscribed in the inner square then square will have area 4 and the dodecagon will have area 3.

Thus, it indicated that we would need a polygon of twice the number of sides to get the same approximation of  $\pi$  via its area as that of an inscribed polygon which gives the same from its perimeter. Hence, perimeter of the inscribed polygon is better than the area to obtain an approximation of  $\pi$ . Further, these exercises taught us something notable: If the Yajurvedic tradition had used polygon inscription (likely it did not) then it would have required a decagon to get something close to its values  $\approx 3.09; 98.4\%$  the real value. While बौधायन or कात्यायन are not explicit about it, आपस्तम्ब is clear that these  $\bigcirc \leftrightarrow \square$  conversions are approximate. Squeezing out additional digits beyond that point is a process of diminishing returns; however, in the ancient world the मैत्रायणीय school of the आध्वर्यव tradition and the Egyptians achieved similar success reaching close to  $99.4..99.5\%$  of the real value. If one wanted to achieve such a level of approximation with the polygon method you would need to inscribe  $\mathcal{P}_{18}$  to get almost exactly the value of the मैत्रायणीय tradition. While old Archimedes is said to have labored with a 96-side polygon to reach his  $\tfrac{22}{7}$ , आर्यभट would have needed something like 360-polygon to get his value. This made us suspect that it was unlikely he used polygon inscription and instead had a trick inherited from the non-polygonal methods typical of the old Hindu quadrature of the circle \[Footnote 1]. On the other hand Archimedes' early triumph undoubtedly rode on the quadrature of the circle achieved by the Platonic school.

From our father's instruction we were reasonably conversant with basic trigonometry before it had been taught in school and he used that as base to introduce us to the basics of calculus in the form of limits. He told us that we could use our polygon inscription to informally understand the limit  $\lim_{x \to 0} \tfrac{\sin(x)}{x}=1$ . With this in hand, he told us that we could, if we really understood it, prove the formulae of the perimeter and area of a circle as the limiting case of the  $\infty$ -sided polygon. From our earliest education in mensuration we had been puzzled by how that mysterious number  $\pi$  appeared in these formulae --- we understood quite easily how the formulae of rectilinear figures like rectangles and triangles had been derived but this "correction factor" for the circle had been an open question for us. Hence, we were keen to figure this out using our newly acquired knowledge of limits. Being of only modest mathematical ability it took us a few days until we arrived at the proof with limits but we were then satisfied beyond words by the experience of putting down the below:

![Figure 3. The circle as the limit of an inscribed polygon](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/limit_pi2.png){width="75%"}

From Figure 3 we can write the perimeter of an inscribed polygon in a unit circle as:

 $$P(\mathcal{P}\_n)=2n\sin\left(\frac{\pi}{n}\right) = 2 \pi \dfrac{\sin\left(\frac{\pi}{n}\right) }{\frac{\pi}{n}}$$ 

Similarly we can write its area as:

 $$A(\mathcal{P}\_n)=n\sin\left(\frac{\pi}{n}\right)\cos\left(\frac{\pi}{n}\right) = \pi \dfrac{\sin\left(\frac{\pi}{n}\right) }{\frac{\pi}{n}} \cos\left(\frac{\pi}{n}\right)$$ 

 $$n \to \infty \; \tfrac{\pi}{n} \to 0 \therefore \displaystyle \lim_{\tfrac{\pi}{n} \to 0} \dfrac{\sin\left(\tfrac{\pi}{n}\right)}{\tfrac{\pi}{n}} \to 1$$ 

By taking the above limit we get  $P(\mathcal{P}_\infty)=P(\bigcirc)=2\pi; \;A(\mathcal{P}_\infty)=A(\bigcirc)=\pi$ 

While this gave us the formulae for the perimeter and the area of a circle, the actual value of  $\pi$  was still a challenge and progress on that front had to wait for other developments. Around the same time, our fascination with the other conics was growing, mainly as an offshoot of our concomitant interest in astronomy. Armed with the high-precision German-made templates we had received from our father we began studying these conics closely. We soon realized that the circle was at one end of the continuum of ellipses and the parabola the end. The hyperbolae lay beyond that end almost as if the ellipse had wrapped around infinity and its two apices had folded back towards each other. It also struck us right away that the method of limits we had used to derive the area and perimeter of a circle could not applied to these other conics. Informally (i.e., by squeezing a circle perpendicular to one of the diameters while preserving area), we could figure out that the area of an ellipse should be  $\pi a b$  where  $a, b$  are its semimajor and semiminor axes. We also got the idea of "area under a curve" intuitively; however, it was unclear how the formulae for perimeters of these other conics could be derived. We had seen formulae for them in tables of functions we had at home \[Footnote 2]. However, the tables stated that the multiple formulae it offered perimeter of the ellipse  $P(E)$  were approximate:

 $$P(E) \approx 2 \pi \sqrt{ab}$$ 

 $$P(E) \approx \pi (a+b)$$ 

 $$P(E) \approx \pi\sqrt{2(a^2+b^2)}$$ 

The first two formulae were attributed in the tables to Johannes Kepler, who had reason to calculate this as he studied ellipses in course of his monumental work on planetary orbits. While I have confirmed him as the source of the first formula, it is not clear if he was the first to propose the second one. The third formula was proposed by Leonhard Euler. In Hindu tradition, to our knowledge, the perimeter of an ellipse (आयत-वृत्त) was treated for the first time by महावीर in this गणित-सार-संग्रह. He gives a formula that goes thus:


[व्यस-क्र्तिः षद्गुणिता द्वि-सङ्-गुणायम-क्र्तियुता पदम् परिधिः ।]{style="color:#0000ff"}

[ ]{style="color:#0000ff"}

[व्यास-चतुर्-भाग-गुणश् चायत-वृत्तस्य सुक्ष्म-फलम् ॥]{style="color:#0000ff"}

[Six times the square of the minor axis plus the square of twice its major axis; the root of this gives the perimeter. That multiplied by one fourth of its minor axis is the high precision area of the ellipse.]{style="color:var(--color-text);font-size:1rem"}

[In modern usage the perimeter will be: \$स्तर्त्\$P(ऎ) \अप्प्रोx २\स्क़्र्त्{४अ^२+६ब्^२}\$एन्द्\$]{style="color:var(--color-text);font-size:1rem"}

![Figure 4. Ellipse perimeter approximations](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/ellipses_formulae.png){width="75%"}

Figure 4 shows the approximate perimeters obtained from the various formulae for selected ellipses with semimajor axis  $a=1$  and the variable semiminor axis  $b = .2, .4, .6, .9$ . The average method is the second of the above formulae. We realized that each approximation is optimized for a different types of ellipses even before we had achieved the exact value for ourselves. One can see that if the ellipse becomes a circle then महावीर's formula would become  $P(\bigcirc)=2\sqrt{10}r$ ; this is keeping with his approximation of  $\pi$  as  $\sqrt{10}$ , which was a misapprehension also held by Brahmagupta contra आर्यभट. Thus, it appears that he tried to "break up" that  $\pi$  between the two axes --- this approximation does reasonably well at the extremes and has a maximum error of around  $3.25\%$  for  $b \approx .438$ . In the rare instances when ellipses where used in Hindu architecture (e.g. in the मण्डप of the temple at Kusuma) they are usually of the proportions  $a=1, b=\tfrac{1}{\sqrt{2}}$ ; for such ellipses the Hindu formula would have given an error of about  $2\%$ .

Our explorations in this direction had set in our mind a strong desire to obtain the exact formula for the perimeters of the ellipses and parabolic arcs. Hence, like our ancestor भृगु going to the great asura वरुण we went to our father seeking the way forward. He informed us that for achieving those objectives we needed to apprehend the further branches of calculus and that those would anyhow come as part of our curriculum in college. But we were not going to wait till then; so, he suggested that we go to the shop and get those simple textbooks a bit in advance and I could attempt to study them over the vacations. Over the next two years we made modest progress and by then we were already in junior college where elementary calculus was to start from the second semester. A prolonged shutdown from a strike gave us exactly what we wanted --- the time to explore these matters by ourselves. By then, armed with the basics of the different branches of calculus, we made exciting progress for our low standards: 1) We rediscovered for ourselves the hyperbolic equivalents of the circular trigonometric functions, their derivatives and integrals. 2) We studied and (re)discovered some additional methods for constructing conics. 3) Most importantly, we non-rigorously derived for ourselves the general method of determining arc length of a curve between  $x=a, b$  using differential and integral calculus:

 $$\displaystyle L = \int_a\^b \sqrt{1+\left( \dfrac{dy}{dx} \right)^2}dx$$ 

We got our first opportunity to put it to practice when we explored the following question: Given a semicircle, how would you inscribe a circle in it? What would be the locus of the centers of such inscribed circles? The construction shown in Figure 5 provides a completely self-evident proof that the locus should a parabola with the tangent to the semicircle at its apex as its directrix and the diameter of the semicircle as its latus rectum. Nevertheless we expand it for a naive reader (Figure 5).


 1.  First draw the line tangent to the apex  $D$  of the semicircle.


 2.  To inscribe a circle that touches a semicircle at point  $E$ , join its center  $C$  to  $E$ .


 3.  Then draw a line perpendicular to radius  $\overline{CE}$  at  $E$ ; this will be the tangent to the semicircle at  $E$ .


 4.  This line intersects the tangent at  $D$  at point  $F$ .


 5.  Drop a perpendicular from  $F$  to the diameter of the semicircle. It will cut radius  $\overline{CE}$  at  $G$  which will be the center of an inscribed circle touching the semicircle at  $E$  and its diameter at  $H$ .

6\)  $\angle CHG = \angle GEF =\tfrac{\pi}{2}$  and  $\angle CGH = \angle EGF$ . Moreover,  $\overline{GE}=\overline{GH}$ . Thus,  $\triangle CGH \cong EGF$  by the side-angle-angle test. Hence,  $\overline{GC}=\overline{GF}$ .


 7.  Thus, for every inscribed circle its center  $G$  will be equidistant from the tangent to the semicircle at its apex and from its center  $C$ . Hence, this locus is a parabola with its focus at  $C$  and the above line as its directrix. Accordingly, the diameter of the semicircle would be its latus rectum.

![Figure 5. Semicircle-parabola construction](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/parabola_semicircle.png){width="75%"}

Accordingly, we applied the above integration to this parabola (Figure 5) whose equation would be  $y=-\tfrac{x^2}{2a}+\tfrac{a}{2}$ , where  $a$  is the radius of the generating semicircle to obtain the arc length of the parabola bounded by its latus rectum:

 $$\dfrac{dy}{dx}=-\dfrac{x}{a}$$ 

 $$\therefore \displaystyle \int_{-a}\^a \sqrt{1+\dfrac{x^2}{a^2}}dx = \left. \dfrac{x}{2}\sqrt{1+\dfrac{x^2}{a^2}}+\dfrac{a}{2}\textrm{arcsinh}(x/a)\right|_{-a}\^a = a\left( \sqrt{2} + \textrm{arcsinh}(1) \right)$$ 

With this we realized that the parabola as a unique conic (i.e. fixed eccentricity), just as the circle, has an associated constant comparable to  $\pi$  that provides its arc length bounded by the latus rectum in terms of the semi-latus rectum  $a$ ; hence all parabolas are like just as all circles and differ only in scale. Thus, we had rediscovered the remarkable parabolic constant, the ratio of the arc length of the parabola bounded by its latus rectum to its semi-latus rectum:  $P= \sqrt{2} + \textrm{arcsinh}(1) \approx 2.295587$ .

This also brought home to us that, unlike the circle and the parabola, the ellipse (and the hyperbola) will not have a single constant that relates their arc length to a linear dimension. Instead there will be a family of those which would be bounded by  $\pi$  and  $\sqrt{2} + \textrm{arcsinh}(1)$ . Our meager mind was immensely buoyed by the successful conquest of the parabola and believed that the comparable conquest of the ellipse was at hand. But hard as we tried we simply could not solve the comparable integral for the ellipse in terms of all the integration we knew. Later that summer we got to meet our cousin who was reputed to have enormous mathematical capacity but had little interest in conics. With a swagger, he said it should be easy but failed to solve it just as we had. However, he had a computer, and for the first time we could attack it with numerical integration. This gave us some intuition of how the integral specifying the arc length of an ellipse behaves and that there is a likely generalization of the circular trigonometric functions to which they might map. At that point we asked an aunt of ours, who used to teach mathematics, if she had any leads to solving that integral. She flippantly asked if we did not know of elliptic integrals? That word struck cord --- not wanting to expose our ignorance further we set out to investigate it further. We went back to our father, who handed us a more "advanced" volume and told us that we were now grown up and could pursue our mathematical fancies on our own. That was indeed the case --- like our ancestor भृगु before he realized the वारुणी-विद्या. Therein we finally learned that the elliptic integrals, like the one we had battled with, were functions in themselves which could not be expressed in terms of elementary functions --- there were special tables that gave their values for ellipses of different eccentricities even as we had circular and rectangular hyperbolic trigonometric functions. But those books had a terrible way of teaching elliptical integrals; hence, we had to chart our own method of presenting them for a person of modest intelligence. Once we did so we felt that these could be easily studied in their basic form along with the regular trigonometric functions.

Thus, we learned that our quest for the perimeter of the ellipse following the course from महावीर through Kepler had reached the dawn of modern mathematics by converging on the famous elliptic integral which has attracted the attention of many of a great mind. The early modern attack on the perimeter of an ellipse began with Newton's attempt with numerical integration, which we had recapitulated using a computer. In the next phase, the 26 year old Leonhard Euler, who declared it to be one great problems that had mystified geometers of the age, used some basic geometry and remarkable sleights of the hand (or should we say the mind) with the binomial theorem to prove the below series for the perimeter of an ellipse. One could say that the paper in which it appeared ("Specimen de constructione aequationum differentialium sine indeterminatarum separatione") had a foundational role in modern mathematics:

Let  $d=\dfrac{a^2}{b^2}-1$  then,

 $$P(E) = 2 \pi b \left( 1+ \dfrac{1 \cdot d}{2 \cdot 2} - \dfrac{1 \cdot 1 \cdot 3 \cdot d^2}{2 \cdot 2 \cdot 4 \cdot 4} + \dfrac{1 \cdot 1 \cdot 3 \cdot 3 \cdot 5 d^3}{2 \cdot 2 \cdot 4 \cdot 4 \cdot 6 \cdot 6} -\dfrac{1 \cdot 1 \cdot 3 \cdot 3 \cdot 5 \cdot 5 \cdot 7 d^4}{2 \cdot 2 \cdot 4 \cdot 4 \cdot 6 \cdot 6 \cdot 8 \cdot 8} \dots \right)$$ 

Our numerical experiments showed that the above series yielded values close to the actual perimeter of an ellipse with 10...20 terms when its eccentricity is between  $0 \cdots \tfrac{1}{\sqrt{2}}$ . However, below that it starts faring poorly and has increasingly poor convergence. From Euler's original series a second series which uses the eccentricity  $e_E$  can be derived with somewhat better convergence:

Let  $m=e_E^2$  then,

 $$\displaystyle P(E) = 2 \pi a \left(1 - \sum_{j=1}^\infty \dfrac{1}{2j-1} \left( \dfrac{(2j)!}{(2\^j j!)^2} \right)^2 m\^j \right)$$ 

20 terms of this series gives values close to the real perimeter for ellipses with eccentricities in the range  $(0,0.95)$  and poorer approximations at higher eccentricities like  $e_E=.995$ . This showed to us that the problem we had struggled with was a truly worthy one and even with Euler's attack getting precise values throughout the eccentricity range was not an easy one. This line of investigation was brought to a closure by the great Carl Gauss who in his twenties had worked out a series with superior convergence for the perimeter of the ellipse that was related to his discovery of the hypergeometric function with a profound impact on modern mathematics. This series is defined thus:

Let  $\displaystyle C(n) = \dfrac{\displaystyle \prod_{j=1}\^n \left(\dfrac{1}{2}-j+1\right)}{n!}$ 

Let  $h=\left(\dfrac{a-b}{a+b}\right)^2$  then,

 $$\displaystyle P(E) = \pi (a+b) \left(1+ \sum_{j=1}^{\infty} (C(j))^2 \cdot h\^j \right)$$ 

This series gives accurate perimeters within 20 terms for  $e_E \in (0,0.99995)$ . Another man who also took a similar path, although almost entirely in the isolation, was Ramanujan of कुम्भघोण but that story is beyond the scope of this note. The above series was hardly the only achievement of Gauss in this direction. He had figured out an algorithm that put the final nail into this problem. Before we get to that, we shall take a detour to define the different original elliptic integrals and take a brief look at the other places we encountered them.

The ellipse  $\tfrac{x^2}{a^2}+\tfrac{y^2}{b^2}=1$  can be divided into 4 quadrants due its symmetry and in the first quadrant its equation can we written as

 $$y=b\sqrt{1-\dfrac{x^2}{a^2}}$  and by the chain rule  $\dfrac{dy}{dx}=-\dfrac{bx}{a^2\sqrt{1-\dfrac{x^2}{a^2}}}$$ 

We make the substitution  $t=\tfrac{x}{a}$ ; hence,

 $$\dfrac{dy}{dx}=-\dfrac{bt}{a\sqrt{1-t^2}}$$ 

 $$\therefore \sqrt{1+\left(\dfrac{dy}{dx}\right)^2}=\sqrt{1+\dfrac{b^2t^2}{a^2-a^2t^2}} =\sqrt{\dfrac{a^2 \left(1-\left(1-\frac{b^2}{a^2}\right)t^2\right)}{a^2(1-t^2)}}$$ 

We observe that  $e_E^2=1-\tfrac{b^2}{a^2}$  is the square of the eccentricity of the ellipse. Keeping with the commonly used elliptic integral convention we use  $e_E = k$ . Thus, the above expression becomes:

 $$\sqrt{\dfrac{1-k^2t^2}{1-t^2}}$$ 

From the above substitution  $dx=a \cdot dt$ ; when  $x=0, t=0$  and when  $x=a, t=1$ . Thus, taking into account all 4 quadrants our arc length integral for the perimeter of the ellipse becomes:

 $$\displaystyle P(E) = 4a \int_0^1 \sqrt{\dfrac{1-k^2t^2}{1-t^2}}dt$$ 

More generally, it may be expressed using the substitution  $t=\sin(\theta) \; \therefore dt=\cos(\theta) d\theta$  with which a version of the integral for an elliptic arc defined by angle  $\phi$  becomes:

 $$\displaystyle E(k,\phi) = \int_0^\phi \sqrt{1-k^2\sin^2(\theta)} d\theta$$ 

This was the very form of the integral we had battled with in our youth before realizing that it was the definition of the elliptic integral of the second kind. One can see that for a quadrant of the ellipse with  $e_E=k$  the above integral is from 0 to  $\phi=\tfrac{\pi}{2}$ . This is then called the complete elliptic integral of the second kind and it may be simply written as  $E(k)$ . Limits between other angles will give the corresponding lengths of elliptical arcs and the general integral  $E(k,\phi)$  is thus the incomplete elliptic integral of the second kind.

If this is the elliptic integral of the second kind then what is the first kind? Incomplete elliptic integral of the first kind is defined as:

 $$\displaystyle F(k,\phi) = \int_0^\phi \dfrac{d\theta}{\sqrt{1-k^2\sin^2(\theta)}}$$ 

If the limits of this integral are taken from 0 to  $\phi=\tfrac{\pi}{2}$  then we get the complete elliptic integral of the first kind which is confusingly denoted by  $K(k)$ .

The relationship between the complete integrals of the 2 kinds and  $\pi$  was discovered by the noted French mathematician Adrien-Marie Legendre, who greatly expanded their study from what Euler had done. As we shall see below, this key relationship led to the Gauss algorithm for calculating  $\pi$  most efficiently. If the eccentricity of an ellipse is  $k$  then the ellipse of complementary eccentricity  $k'$  is defined thus:  $k^2+k'^2=1$ . Then we get the Legendre relationship between the 2 kinds of complete elliptic integrals:

 $$K(k') \cdot E(k)+E(k') \cdot K(k) - K(k') \cdot K(k) = \dfrac{\pi}{2}$$ 

We had already had our brush with  $K(k)$  earlier in our youth in course of the auto-discovery of conic-associated pedal and envelop curves recapitulating some deep history in this direction. For an ellipse or a hyperbola we can define the pedal curve as the locus of the feet of the perpendiculars dropped from the center of the conic to its tangents. We can define a second curve as the envelope of the circles whose centers lie on an ellipse or a hyperbola and which pass through the center of the said conic. We found that these two curves differ only in scale with the later being double the former in a given dimension. If  $a, b$  are the semi- major and minor axes of the parent conic, these curves have the polar equations:

 $\rho^2 = a^2\cos^2(\theta)+b^2\sin^2(\theta)$  the pedal curve for an ellipse

 $\rho^2 = 4\left(a^2\cos^2(\theta)+b^2\sin^2(\theta)\right)$  the envelope curve for an ellipse

 $\rho^2 = a^2\cos^2(\theta)-b^2\sin^2(\theta)$  the pedal curve for a hyperbola

 $\rho^2 = 4\left(a^2\cos^2(\theta)-b^2\sin^2(\theta)\right)$  the envelope curve for a hyperbola

![Figure 6A. The hippopedes of an ellipse](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/ellipse_construction2_hippopede.png){width="75%"}

We subsequently learned that these elliptical version of the curves was termed the hippopede by the great Proclus who investigated them (Figure 6A). He was consciously one of the last in the line of great yavana investigators of curves starting from the discovery of the conics at the Platonic academy and this died with the destruction of the Greek tradition by the "Typhonic winds" of the second Abrahamism. One observes that if  $a=b$  then the hippopedes become circles. If  $b=0$  then again we get a pair of circles. The hyperbolic versions in contrast specify  $\infty$ -shaped curves that cross at origin. If the hyperbola is rectangular (i.e.  $a=b$ ) then we get the pedal version to be:  $\rho^2 = a^2\left(\cos^2(\theta)-\sin^2(\theta)\right)= a^2\cos(2\theta)$ . This curve was first studied by Jakob Bernoulli, the eldest brother of the first famous generation of the Bernoulli clan. It has an interesting property: if the ellipse is the locus of points the sum of whose distances from the two foci is a constant, the lemniscate is the locus of points the product of whose distances from the two foci is constant.

![Figure 6B. The lemniscates of a hyperbola](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/hyperbola_lemniscate.png){width="75%"}

We realized that unlike the other above curves whose arc lengths pose some terrible integrals that for the lemniscate can be reduced to a form comparable to what we got for the ellipse. Given a lemniscate with the polar equation:  $\rho^2 = a^2\cos(2\theta)$ ,

 $$\dfrac{d\rho}{d\theta}=-\dfrac{a\sin(2\theta)}{\sqrt{\cos(2 \theta)}}$$ 

The arc length formula in polar coordinates is:

 $$\displaystyle \int_a\^b \sqrt{\rho^2 + \left(\dfrac{d\rho}{d\theta}\right)^2}d \theta$$ 

Thus, the arc length of the first quadrant of the lemniscate is:

 $$\displaystyle \int_0^{\pi/4} \sqrt{a^2 \cos(2\theta)+a^2 \dfrac{\sin^2(2\theta)}{\cos(2\theta)}}d\theta= a\int_0^{\pi/4}\sqrt{\dfrac{\sin^2(2\theta)+\cos^2(2\theta)}{\cos(2\theta)}}d\theta$$ 

 $$\displaystyle = a \int_0^{\pi/4} \dfrac{d\theta}{\sqrt{\cos(2\theta)}}$$ 

In the above we make the substitution  $\cos(2\theta)=\cos^2(\phi) \; \therefore -\sin(2\theta)d\theta=-2\sin(\phi)\cos(\phi)d\phi$ 

 $$d\theta = \dfrac{\sin(\phi)\cos(\phi)d\phi}{\sin(2\theta)}= \dfrac{\sin(\phi)\cos(\phi)d\phi}{\sqrt{1-\cos^4(\phi)}} =\dfrac{\sin(\phi)\cos(\phi)d\phi}{\sqrt{(1-\cos^2(\phi))(1+\cos^2(\phi))}}=\dfrac{\cos(\phi)d\phi}{\sqrt{1+\cos^2(\phi)}}$$ 

 $$=\dfrac{\cos(\phi)d\phi}{\sqrt{2(1-\frac{1}{2}\sin^2(\phi))}}$$ 

This substitution results in the limits of the arc length integral changing to  $0..\tfrac{\pi}{2}$ . Thus, it becomes:

 $$\displaystyle = \dfrac{a}{\sqrt{2}} \int_0^{\pi/2} \dfrac{\cos(\phi)d\phi}{\cos(\phi)\sqrt{(1-\frac{1}{2}\sin^2(\phi))}}=\dfrac{a}{\sqrt{2}} \int_0^{\pi/2} \dfrac{d\phi}{\sqrt{(1-\frac{1}{2}\sin^2(\phi))}}$$ 

Thus, the perimeter of this lemniscate is:

 $$\displaystyle P(L)=\dfrac{4a}{\sqrt{2}} \int_0^{\pi/2} \dfrac{d\phi}{\sqrt{(1-\frac{1}{2}\sin^2(\phi))}}$$ 

We see that the integral specifying the perimeter of a lemniscate is a complete elliptic integral of the first kind with as  $\phi=\tfrac{\pi}{2}$  and  $k=\tfrac{1}{\sqrt{2}}$ , i.e.  $K\left(\tfrac{1}{\sqrt{2}}\right)$ . This was one of the integrals studied by Count Fagnano, a self-taught early pioneer in the calculus of elliptical and lemniscate arcs --- we were amused and somewhat consoled to learn that, like us, he has initially tried to solve this integral in terms of elementary functions and failed. Moreover, like the circle and the parabola, the lemniscate is a unique curve, such that the ratio of its perimeter to its horizontal semi-axis  $a$  is a constant (the lemniscate constant) mirroring  $\pi$  and the parabolic constant  $P$ :

 $$L= 2\sqrt{2}K\left(\tfrac{1}{\sqrt{2}}\right) \approx 5.244116$$ 

This is also a special value where we have the below relationship which can be used to compute  $\pi$  efficiently (corollary to Legendre's identity):

 $$2K\left(\tfrac{1}{\sqrt{2}}\right)\left(2E\left(\tfrac{1}{\sqrt{2}}\right)-K\left(\tfrac{1}{\sqrt{2}}\right)\right)=\pi$$ 

Around the time we acquired a grasp of these elliptic integrals we also learned of another practical appearance of  $K(k)$ . In elementary physics one learns of simple oscillators like the pendulum and derives its period  $T$  using the basic circular trigonometric differential equation:

 $$T \approx 2\pi\sqrt{\dfrac{l}{g}}$$ 

Here,  $l$  is the length of the pendulum and  $g \approx 9.8 m/s^2$  is the gravitational acceleration. This emerges from an approximation for small angle oscillations where  $\sin(\theta) \approx \theta$  and corresponds to the period relationship discovered by Galileo and the apparent failed attempt of गणेश दैवज्ञ. We had already realized that integrating the differential equation for a larger amplitude presented an integral that we had failed to solve using elementary functions. However, it can be solved with the elliptical integral of the first kind to give the accurate value for period as:

 $$T=4\sqrt{\dfrac{l}{g}}K\left(\frac{\theta_0}{2}\right)$$ 

Here  $\theta_0$  represents the initial angle at which the pendulum is released. One can see that if  $\theta_0 \to 0$  then  $K(k) \to \tfrac{\pi}{2}$  giving us the low amplitude formula. Taking the standard value of  $g=9.80665 m/s^2$  given in physics textbooks we get the period of a meter pendulum with a low amplitude displacement as  $T= 2.006409 s$ . If we instead give it a  $60^\circ$  release then we get  $T=2.153242 s$  with the elliptic integral  $K\left(\sin\left(\tfrac{\pi}{6}\right)\right)$ . Hence, one can see that the Galilean linear approximation is not a bad one for typical low angle releases.

This finally leads to what was a burning question for us in our youth: How do we effectively compute these elliptic integrals? In our opinion, this should be taught first to students and that would go some way in making the elliptics trivial as trigonometric functions. We saw the various series methods of Euler and Gauss. While the latter does quite well it is still a multi-term affair, that takes longer to converge higher the eccentricity. But the 22 year old Gauss solved this problem with a remarkable algorithm that rapidly gives you the values of these integrals --- something, which in our early days, we had even done with a hand calculator while teaching it to a physics student. Right then, Gauss realized that it "opens an entirely new field of analysis" as he wrote in his notes accompanying the discovery. This is the famous arithmetic-geometric mean  $M$  algorithm which goes thus:

Given 2 starting numbers  $x_0, y_0$ , apply the map:  $x_{n+1}=\dfrac{x_n+y_n}{2}, \; y_{n+1}=\sqrt{x_n \cdot y_n}$ .

The map converges usually within 5 iterations for typical double precision values to the arithmetic-geometric mean  $M(x_0,y_0)$ . Let  $k$  be the eccentricity value for which we wish to compute the complete elliptic integral of the first kind.  $k'=\sqrt{1-k^2}$  then we have,

 $$K(k)=\dfrac{\pi}{2M(1,k')}$$ 

For  $E(k)$  we used to originally use a Gaussian algorithm (see below) have now rewritten the function using Semjon Adlaj's more compact presentation of the same:

Given 2 starting numbers  $x_0, y_0$ , define  $z_0=0$ . Then apply the map:  $x_{n+1}=\dfrac{x_n+y_n}{2}, \; y_{n+1}=z_n+\sqrt{(x_n-z_n)(y_n-z_n)}, \; z_{n+1}=z_n-\sqrt{(x_n-z_n)(y_n-z_n)}$ 

When  $x_n=y_n$  within the limits of your precision stop the process (within 5..6 iterations for double precision). The number they have converged to is the variant arithmetic-geometric mean  $N(x_0, y_0)$ . If  $k$  is the eccentricity and  $k'=\sqrt{1-k^2}$  then we have,

 $$E(k) = \dfrac{\pi N(1,k'^2)}{2M(1,k')}$$ 

Thus, with the Gaussian algorithm the complete elliptic integrals or perimeter of the ellipse to any desired accuracy is as easy as that. Further, by way of the Legendre identity this also yields the extremely efficient Gaussian algorithm for calculating the value of  $\pi$ :

 $$\pi = \dfrac{2M(k)M(k')}{N(k^2)+N(k'^2)-1}$$ 

By putting any eccentricity and its complement one can now compute  $\pi$  from it --- every reader should try it out to see its sheer efficiency.

With the complete integrals in place, we were next keen apprehend the Gaussian algorithm for the incomplete integrals. After some effort with the geometric interpretation of the arithmetic-geometric mean, we realized that it was not ideal for the hand calculator and we had to use to a computer, which was not yet available at home. Nevertheless, we wrote down the algorithm and rushed to the "public computer" input it as soon as we could. It goes thus; We have as our input  $k$  the eccentricity parameter and  $\phi$  the angle defining the partial elliptical arc. We then initialize with:

 $$x_n=1; \; y_n=\sqrt{1-k^2}$$ 

 $$\phi_n=\phi; \; c_n=k$$ 

 $$s_n=1-\dfrac{c_n^2}{2}; \; s'\_n=0; \; t_n=1$$ 

We then iterate the below process for a desired  $n$  number of steps. For most values double precision values can be achieved within 5..6 iterations:

 $$d_n=\arctan\left(\dfrac{(x_n-y_n)\tan(\phi_n)}{x_n+y_n\tan^2(\phi_n)}\right)$$ 

 $$\phi_{n+1}=2\phi_n-d_n$$ 

 $$x_{n+1}=\dfrac{x_n+y_n}{2}$$ 

 $$c_{n+1}=\dfrac{x_n-y_n}{2}$$ 

 $$y_{n+1}=\sqrt{x_n y_n}$$ 

 $$s_{n+1}=s_n-t_nc_{n+1}^2$$ 

 $$s'_{n+1}=s'\_n+c_{n+1}\sin(\phi_{n+1})$$ 

 $$t_{n+1}=2t_n$$ 

Finally, upon completing iteration  $n$  we compose the solutions for the incomplete integrals as below:

 $$F(k, \phi)= \dfrac{\phi_{n+1}}{2^{n+1} x_{n+1}}$$ 

 $$E(k, \phi)= s_{n+1} F(k, \phi)+s'_{n+1}$$ 

Of course one can see that this algorithm also yields the corresponding complete integrals:

 $$K(k)=\dfrac{\pi}{2x_{n+1}}=\dfrac{\pi}{2y_{n+1}}$$ 

 $$E(k)=s_{n+1}K(k)$$ 

It was this method by which we originally computed  $E(k)$  in our youth as Adlaj's algorithm was published in English only later. In any case the Gauss algorithm made a profound impression on us for more than one reason. First, the connection between the convergent  $M(x,y)$  and the elliptic integrals was remarkable in itself. Second, Gauss devised this algorithm in 1799 CE when no computers were around. Being a great mental computer (a trait Gauss passed on to one of his sons) it was no issue for him; however, this method was eminently suited for computer age that was lay far in the future. Indeed, in a general sense, it reminded one iterative algorithms of the Hindus like the square root method of Chajaka-putra, the famed चक्रवाल or the sine algorithm of नित्यानन्द. Third, as we learnt for the first time of the Gauss algorithms for the elliptic integrals, we were also exploring and discovering various iterative maps with different types of convergences: fixed points of note, fixed oscillations and strange attractors. This hinted to us the iterative algorithms were an innate feature of computational process that emerge in systems independently of the hardware (though some hardware might be better suited than others to execute them). A corollary was that various numbers underlying attractors could play a direct role in the patterns observed in structures generated by natural computational processes.

That brings us to the final part of this story, namely the relationship between the elliptic integrals and the circular trigonometric functions. As mentioned above, even in course of our futile struggle to solve the elliptic integrals in terms of elementary functions, it hit us that underlying them were elliptical equivalents of trigonometric functions. Hence, when we finally learned of these functions in our father's book we realized that our geometric intuition about their form was informal but correct. That is shown using the Eulerian form of the ellipse in Figure 7.

![Figure 7. The elliptic generalization of circular trigonometric functions](https://manasataramgini.wordpress.com/wp-content/uploads/2020/07/elliptic_integral.png){width="75%"}

Thus, given an ellipse with semi-minor axis  $b=1$ , semi-major axis  $a> 1$  its eccentricity is  $k=\sqrt{1-\tfrac{1}{a^2}}$ . For a point  $A$  on this ellipse determined by the radial vector  $r$  (vector connecting it to origin  $O$ ) and position angle  $\phi$ , we can define the following elliptic analogs of the circular trigonometric functions:

 $$\textrm{cn}(u,k)=\dfrac{x}{a}$$ 

 $$\textrm{sn}(u,k)=y$$ 

 $$\textrm{dn}(u,k)=\dfrac{r}{a}$$ 

Here, the variable  $u$  is not the position angle  $\phi$  itself but is related to  $\phi$  via the integral:

 $$u=\displaystyle \int_B\^A r \cdot d\phi$$ 

When the ellipse becomes a circle,  $r=a=b$  and the above integral resolves to  $\phi$  with  $\textrm{cn}(\phi, 0)=\cos(\phi)$ ,  $\textrm{sn}(\phi,0)=\sin(\phi)$  and  $\textrm{dn}(\phi,0)=1$ . Further, one can see that these functions have an inverse relationship with the lemniscate arc elliptic integral  $F(k, \phi)$ . We have already seen that by definition:

 $\displaystyle F(k,\phi) = \int_0^\phi \dfrac{d\theta}{\sqrt{1-k^2\sin^2(\theta)}}$ , then:

 $$\textrm{cn}(u,k) =\cos(\phi); \; \textrm{sn}(u,k)= \sin(\phi)$$ 

The complete elliptical integral  $K(k)$  determines the period of these elliptic functions and provides the equivalent of  $\tfrac{\pi}{2}$  in circular trigonometric functions for these elliptic functions. Thus, the values of  $\textrm{sn}(u,k); \textrm{cn}(u,k); \textrm{dn}(u,k)$  will repeat at  $u+४न्ख़्(k)$ , where  $n=1, 2, 3 \dots$ . Moreover,

 $$\textrm{sn}(0,k) =0; \textrm{cn}(0,k) =1; \textrm{dn}(0,k) =1$$ 

 $$\textrm{sn}(K(k),k) =1; \textrm{cn}(K(k),k) =0; \textrm{dn}(K(k),k) =1$$ 

Further, the geometric interpretation (Figure 7) also allows one to understand the elliptical equivalents of the fundamental trigonometric relationships:

 $\textrm{sn}^2(u,k)+\textrm{cn}^2(u,k)=1 \rightarrow$  a consequence of the definition of an ellipse.

 $$\textrm{dn}^2(u,k) +k^2\textrm{sn}^2(u,k)=1$$ 

 $$\textrm{dn}^2(u,k)+k^2=1+k^2\textrm{cn}^2(u,k)\; \therefore \textrm{dn}^2(u,k)= k'^2+k^2\textrm{cn}^2(u,k)$$ 

Again parallel to the circular and hyperbolic trigonometric functions, the derivatives of the elliptic functions also have parallel expressions:

 $$\dfrac{\partial \textrm{sn}(u,k)}{\partial u}=\textrm{cn}(u,k) \textrm{dn}(u,k)$$ 

 $$\dfrac{\partial \textrm{cn}(u,k)}{\partial u}=-\textrm{sn}(u,k) \textrm{dn}(u,k)$$ 

 $$\dfrac{\partial \textrm{dn}(u,k)}{\partial u}=-k^2\textrm{sn}(u,k) \textrm{cn}(u,k)$$ 

At this point we will pause to make a few remarks on early history of these elliptic functions that has a romantic touch to it. While still in his early 20s, Carl Gauss studied the elliptic integrals of the first type in the context of the lemniscate arc length problem leading to the celebrated arithmetic-geometric mean algorithm that we saw above. In course of this study, he discovered that the inverse of this integral led to general versions of the circular trigonometric functions like sine and cosine. He had already discovered their basic properties, such as those stated above, and made several higher discoveries based on them. He had already realized that these were doubly periodic when considered in the complex plane. However, as was typical of him (and the luxuries of science publication in the 1700-1800s) he did not publish them formally. Almost 25 years later, the brilliant young Norwegian Niels Abel, rising like a comet in the firmament, rediscovered these results of Gauss and took them forward establishing the foundations of their modern study. One striking point was how Abel's notation closely paralleled that of Gauss despite their independent discovery. When we learnt of this and reflected at our own limited attempt in this direction, it reinforced to us the idea that such mathematics is not created but merely discovered by tapping into a deep "Platonic" realm. Abel submitted an initial version of his work on elliptic integrals at the French National Academy; however, it seems to have been lost due to Augustin-Louis Cauchy discarding it unread among his papers. The subsequent year Abel published a more elaborate work which rediscovered Gauss's findings.

Around the same time, the brilliant mathematician Carl Jacobi also rediscovered the same results and extended them further. This sparked a rivalry between him and Abel with a flurry of publications each bettering the other. Consequently, Legendre, the earlier pioneer of the elliptic integrals, remarked that as a result they were producing results at such a pace that it was hard for his old head to keep up with them. But this competition was to soon end with Abel slipping into deep debt from his European travels and dying shortly thereafter from tuberculosis. The Frenchman Évariste Galois, who paralleled the research of his contemporary Niels Abel in so many ways, wrote down numerous mathematical discoveries in his last letter just before his death in a duel at the age of 20. In those were found studies on the elliptic functions including rediscoveries of Abel's work and generalizations that Jacobi was to arrive at only a little later. Ironically, in that letter he stated to his friend: "Ask Jacobi or Gauss publicly to give their opinion, not as to the truth, but as to the importance of these theorems. Later there will be, I hope, some people who will find it to their advantage to decipher all this mess." With Abel and Galois dead, the field was open to Jacobi. While he did not live much longer either, he had enough time to take their investigation to the next stage and these generalizations of circular trigonometric continue to be known as Jacobian elliptic functions.

Now again in our youth we were keen write computer functions to that could accurately output the values of these elliptic functions so that we could play with them more easily. In the process, we learned of Ramanujan blazing his own trail through the elliptic functions that led to series for evaluating them. However, the computationally most effective approach to calculate them was the Gaussian arithmetic-geometric mean algorithm which we present below. This algorithm has two parts: first, in the "ascending part" wherein we compute the iterates of the means as in the elliptic integral algorithm. Second, having stored the above iterates we "descend" with them to compute the values of corresponding  $\phi$  from which we can extract the Jacobian elliptic through the circular trigonometric functions. As input we have the variable  $u$  and the eccentricity  $k$ :

 $$x_1=1; \; y_1=\sqrt{1-k^2}; \; c_1=k$$ 

Then we carry out a desired  $n$  iterations thus:

 $$x_{n+1}=\dfrac{x_n+y_n}{2}$$ 

 $$y_{n+1}= \sqrt{x_n y_n}$$ 

 $$c_{n+1} = \dfrac{x_n - y_n}{2}$$ 

Once this is complete we compute:

 $$\phi_{n+1}=2^{n+1} a_{n+1} u$$ 

Then we carry out the "descent" in  $n$  till  $n=1$ :

 $$d = \arcsin \left( \dfrac{c_{n+1} \sin\left(\phi_{n+1}\right)}{a_{n+1}}\right)$$ 

 $$\phi_n = \dfrac{\phi_{n+1}+d}{2}$$ 

Once the descent is complete we extract the Jacobian elliptic functions thus:

 $$\textrm{sn}(u,k)=\sin\left(\phi_1\right)$$ 

 $$\textrm{cn}(u,k)=\cos\left(\phi_1\right)$$ 

 $$\textrm{cd}(u,k)=\cos\left(\phi_2-\phi_1\right)$$ 

 $$\textrm{dn}(u,k) =\dfrac{\textrm{cn}(u,k)}{\textrm{cd}(u,k)}$$ 

![Figure 8.  $\textrm{width="75%"}

With this we could finally visualize the form of these elliptic functions (Figure 8): with increasing  $k$ ,  $\textrm{sn}(u,k)$  develops from a sine curve to one with increasing flat crests and troughs.

We end this narration of our journey through the most basic facts pertaining to the elliptic functions with how it joined our other long-standing interest, the oval curves, and helped us derive ovals parametrized using Jacobian elliptics. From the above account of the fundamental identities of the elliptic functions and considering the derivative only with respect to  $u$  for a constant  $k$  we get:

 $$\dfrac{d\textrm{cn}(u) }{du}= -\sqrt{(1-\textrm{cn}^2(u))(1-k^2+k^2\textrm{cn}^2(u))}$$ 

 $$\therefore (x')^2=(1-x^2)(1-k^2+k^2x^2)$$ 

By differentiating the above again and resolving it we get the differential equation:

 $$x''=-2k^2x^3 + (2k^2-1)x$$ 

This differential equation whose solutions take the form of the  $\textrm{cn}(u,k)$  function is a generalization of the harmonic differential equation. Having obtained we discovered much to our satisfaction that the curves parametrized by  $\textrm{cn}(u,k)$  and its derivative (i.e. position-momentum plots of dynamics defined by this DE) can take the form of ovals (Figure 9), a class of curves we were coevally investigating. These "elliptic ovals" are part of continuum ranging from elliptic hippopede-like curves to elliptic lemniscates, paralleling the continuum of classic Cassinian ovals (Figure 9). It was this intuition that led us to the discovery of the chaotic oval-like curves we had narrated earlier. These curves have an interesting property:  $k=\tfrac{1}{\sqrt{2}}$  marks a special transition value. For all  $k$  less that the solutions define concentric curves (Figure 9). For all  $k$  greater than that we get lemniscates, ovals, and centrally dimpled curves.

![Figure 9. Solutions of the above  $\textrm{width="75%"}

  - -----------------------------------------------------------------------

Footnote 1: Already in Yajurvedic attempt recorded by authors like बौधायन we see an alternating pattern of positive and negative fractions of decreasing magnitude to effect convergence. The Yajurvedic formula can be written as  $\pi \approx 4\left(1-\tfrac{1}{a} + \tfrac{1}{a \cdot 29}- \tfrac{1}{a \cdot 29 \cdot 6}+ \tfrac{1}{a \cdot 29 \cdot 6 \cdot 8}\right)$ ;  $a=8$  is used by बौधायन in his conversion. However, if we use  $\tfrac{853}{100}$  we get  $\pi \approx 3.1415$ . There is a history of such correction within the श्रौत tradition recorded by ritualists like द्वारकानथ यज्वान् who has a correction to बौधायन's root formula giving  $\pi \approx \left (\tfrac{236}{39(2+\sqrt{2})}\right)^2 \approx 3.141329$  indicating that in later practice values much closer to the real value were used.

Footnote 2: Something people used in the era when computers were not household items. The scientific calculator gave you most of the basic ones like the trigonometric triad and logarithms but for the rest you looked up such tables.
