
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Counting pyramids, squares and magic squares](https://manasataramgini.wordpress.com/2020/09/27/counting-pyramids-squares-and-magic-squares/){rel="bookmark"} {#counting-pyramids-squares-and-magic-squares .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 27, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/09/27/counting-pyramids-squares-and-magic-squares/ "11:54 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/09/pyramidal_numbers.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad Pyramidal numbers
```{=latex}
\end{center}
```


The following note provides some exceedingly elementary mathematics, primarily for बालबोधन. Sometime back we heard a talk by a famous contemporary mathematician (M. Bhargava) in which he described how as a kid he discovered for himself the formula for pyramidal numbers (i.e. defined by the number of spheres packed in pyramids with a square base; Figure 1). It reminded us of a parallel experience in our childhood, and also of the difference between an ordinary person and a mathematician. In those long past days, we found ourselves in the company of a clansman who had a much lower sense of purpose than us in our youth (it seems to have inverted in adulthood). Hence, he kept himself busy by leafing through books of "puzzles" or playing video games. He showed us one such "puzzle" which was puzzling him. It showed something like Figure 1 and asked the reader to find the total number of balls in the pile if a base-edge had 15 balls. We asked him why that was a big deal --- after all, it was just a lot of squaring and addition and suggested that we get started with a paper and pencil. He responded that he too had realized the same but had divined that what the questioner wanted was a formula into which we could plug in a base-edge with any number of balls and get the answer. We tried to figure out that formula but failed; thus, we sorted with the mere mortals rather than the great intellectuals.

Nevertheless, our effort was not entirely a waste. In the process of attempting to crack the formula, we discovered for ourselves an isomorphism: The count of the balls in the pyramid is the same as the total number of squares that can be counted in a  $n\times n$  square grid (Figure 2). In this mapping, the single ball on the top is equivalent to the biggest or the bounding square. The base layer of the pyramid corresponds to the individual squares of the grid. All other layers map onto interstitial squares --- in Figure 2 we show how those are defined by pink shading and cross-hatching of one example of them. In this mapping, the entire pyramid is mapped into the interior of the apical ball, which is now represented as a sphere. Thus, the number of balls packed into a pyramid and the number of squares in a  $n\times n$  square grid are merely 3D and 2D representations of the same number, i.e. the sum of squares  $1^2+2^2+3^2...n^2$ 
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/09/pyrfig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad The total number of squares formed by contact in a square grid.
```{=latex}
\end{center}
```


We got our answer to this a couple of years later when we started reading the आर्यभटीयम् of आर्यभट, one of the greatest Hindu scientists of all times. He says:

[सैक-स-गच्छ-पदानाम् क्रमात्-त्रि-संवर्गितस्य षष्ठो ।अंशः ।]{style="color:#0000ff;"}\
[वर्ग-चिति-घनः स भवेत् चिति-वर्गो घन-चिति-घनश् च ॥ अब् २।२२]{style="color:#0000ff;"}

The sixth part of the product of the three quantities, viz. the number of terms, the number of terms plus one, and twice the number of terms plus one is the sum of the squares. The square of the sum of the (original) series is the sum of the cubes. \[vide KS Shukla].

In modern language, we would render the first formula, which concerns us, as:

 $$\displaystyle \sum_{j=1}^{n} j^2 = \dfrac{n(n+1)(2n+1)}{6}$$ 

This is the formula for the figurate numbers known as the pyramidal numbers as they define square pyramids (Figure 1). A प्रत्यक्ष geometric proof for this offered by the great गार्ग्य नीलकण्ठ सोमयाजिन् (Figure 3). While this proof appears in नीलकण्ठ's भाष्य on the आर्यभटीयम्, it is likely that some such proof was already known to आर्यभट.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/09/pyramidal_numbers_nilakantha.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad The formula for pyramidal numbers or the sum of squares of integers.
```{=latex}
\end{center}
```


For बालबोधन:


 1.  He first asks you to lay a rectangular floor of  $(2n+1)(n+1)$  cubic units.


 2.  Then you erect the walls on 3 of sides of the floor of height  $n$  cubic units, namely the 2 sides of length  $n+1$  and 1 side of length  $2n+1$ .


 3.  The shell thus constructed has:

 $(2n+1)(n+1)=2n^2+3n+1 \rightarrow$  floor

 $(2n+1)n-(2n+1)=2n^2-n-1 \rightarrow$  backwall

 $2(n^2-n)=2n^2-2n \rightarrow$  sidewalls

i.e. a total of  $6n^2$  cubic units or bricks.

From the figure, it is apparent that the shell can accommodate another shell based on  $(n-1)$ , which in turn can accommodate one based on  $(n-2)$  units and so on till 1. Thus, we can fill a cuboid of volume,

 $\displaystyle n(n+1)(2n+1)= 6\sum_{j=1}^{n} j^2$ ,

This yields आर्यभट's formula from which we can write the sequence of pyramidal numbers  $Py_n$  as:

**1, 5, 14, 30, 55, 91, 140, 204, 285, 385, 506, 650, 819, 1015, 1240...**

We had earlier seen आर्यभट and नीलकण्ठ's work on triangular numbers (sum of integers) and tetrahedral numbers (the sum of the sum of integers) [[footnote 1](https://manasataramgini.wordpress.com/2017/10/23/triangles-hexes-and-cubes/)]. From that, we know the formula for tetrahedral numbers to be:

 $$Te_n=\dfrac{n(n+1)(n+2)}{6}$$ 

**1, 4, 10, 20, 35, 56, 84, 120, 165, 220, 286, 364, 455, 560, 680...**

We see that  $Py_n=Te_n+Te_{n-1}$ . This can be easily proven by seeing that merging two successive tetrahedral piles of spheres we get a square pyramid pile of balls (see figure in [[footnote 1](https://manasataramgini.wordpress.com/2017/10/23/triangles-hexes-and-cubes/)]). Shortly thereafter, this led us to finding for ourselves the space occupancy or density constant for atomic packing. Consider uniformly sized spherical atoms to be packed in a pyramid, like in Figure 1. Then the question is what fraction of the volume of the pyramid will be occupied by matter. We know that the volume of the pyramid whose side length is  $l$  is  $V = \tfrac{l^2a}{3}$ , where  $a$  is its height. From the भुजा-कोटि-कर्ण-न्याय we have its height as  $\tfrac{l}{\sqrt{2}}$ . Hence,

 $$V = \tfrac{l^3}{3\sqrt{2}}$$ 

Now the volume occupied by the atoms from आर्यभट's series sum is:

 $$V_a=\dfrac{2n^3+3n^2+n}{6}\cdot \dfrac{4\pi r^3}{3}$$ 

The radius of each atom is  $r=\tfrac{l}{2n}$ . Plugging this in the above we get:

 $$V_a=\dfrac{2n^3+3n^2+n}{6}\cdot \dfrac{4\pi l^3}{24n^3}$$ 

Simplifying we get:

 $$V_a = \left( \dfrac{1}{18}+ \dfrac{1}{12n}+\dfrac{1}{36n^2}\right)\pi l^3$$ 

Since the atoms have infinitesimal radius we can take the limit  $n \to \infty$  and we are left with:

 $$\displaystyle \lim_{n \to \infty} V_a =\dfrac{\pi l^3}{18}$$ 

Thus, we get,

 $$\dfrac{V_a}{V}=\dfrac{\pi}{3\sqrt{2}} \approx 0.7404805$$ 

Hence, little under  $\tfrac{3}{4}$  of the space occupied by solid matter is filled by uniform spherical atoms. This meditation on atomic packing led us to another way of counting squares. Imagine circles packed as in Figure 4. The circles can then be used to define squares. The most obvious set of squares is equivalent to the  $n \times n$  grid that we considered above. Here the smallest squares of the grid are equivalent to those circumscribing each circle, or alternative inscribed within it as shown in the example with just 1 circle. We can also join the centers of the circles and get bigger squares. If we instead circumscribe the circles we get an equivalent number corresponding to the bounding and interstitial squares of the  $n \times n$  grid. However, we notice (as shown in the  $3 \times 3$  example) that we can also get additional squares by joining the centers cross-ways. So the question was what is the total number of squares if we count in this manner?
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/09/pyrfig4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad Squares defined by packed circles.
```{=latex}
\end{center}
```


We noticed that the first two cases will have the same number of squares as the pyramidal number case. However, from the  $3 \times 3$  case onward we will get additional squares. We noticed that for  $3 \times 3$  we get one additional square beyond the pyramidal numbers; for the  $4 \times 4$  case we get 4 additional squares. It can be seen that the number of additional squares essentially define tetrahedral numbers; thus, we can write the sequence  $S_n$  of this mode of counting as below:

 $S_1=Py_1, S_2=Py_2$ , when  $n\ge 3, S_n=Py_n+Te_{n-2}$ .

 $$\therefore S_n=\dfrac{n(n+1)(2n+1)}{6} + \dfrac{n(n-1)(n-2)}{6}= \dfrac{n^3+n}{2}$$ 

 $S_n:$  **1, 5, 15, 34, 65, 111, 175, 260, 369, 505, 671, 870, 1105, 1379, 1695...**

We can also derive this sequence in another way. Write the natural numbers thus:

**(1); (2,3); (4,5,6); (7,8,9,10); (11,12,13,14,15);...**

If we then take the sum of each group in brackets, which has  $1, 2, 3 ... n$  elements, we get  $S_n$ .

We observe that from the 3rd term onward this sequence remarkably yields the magic constants  $M$  (row, column and diagonal sums or the largest eigenvalue of the matrix defined by the magic square) for the minimal magic squares (bhadra-s) i.e. magic squares made of numbers from  $1:n^2$  where  $n$  is the order or the side length of it [[footnote 2](https://manasataramgini.wordpress.com/2017/05/14/the-magic-of-the-deva-ogdoad/)]. We also realized then that this was the basis of the "magic choice" property which we used in a schoolyard trick. That is illustrated in Figure 5.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/09/pyrfig5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad Magic choice.
```{=latex}
\end{center}
```


Write a  $n \times n$  square of all integers from  $1...n^2$ . Ask a person to silently randomly choose  $n$  numbers such that each row and column of the square is represented once (orange circles in Figure 5) and sum them up. Then, without him revealing anything you tell him the sum. The sum will be  $S_n$ . This is the weaker condition which can be converted to a magic square for all  $n\ge 3$ .

Finally, we will consider another sequence that can be derived like the above. It is the simple sum of pyramidal and tetrahedral numbers without shifting the latter by 2 terms as we did above to get  $S_n$ . Thus, this new sequence is:

 $$I_n = \dfrac{n(n+1)(2n+1)}{6} +\dfrac{n(n+1)(n+2)}{6} =\dfrac{n(n+1)^2}{2}$$ 

 $I_n:$  **2, 9, 24, 50, 90, 147, 224, 324, 450, 605, 792, 1014, 1274, 1575, 1920, 2312, 2754, 3249, 3800, 4410...**

We observe that this sequence defines the sum of the integers in the interstices between triangular numbers (Figure 6). Further, it also has a geometric interpretation in the form of the area of the triangular number trapezium (Figure 6). Successive, triangular number trapezia are defined by the following 4 points:  $(0,T_n); (T_n, T_{n+1}); (T_{n+1},T_{n+2}); (T_n+1, 0)$ . These trapezia always have an integral area equal to  $I_n$  starting from 9.

![pyrFig6A](https://manasataramgini.wordpress.com/wp-content/uploads/2020/09/pyrfig6a.png){width="75%"}

![pyrFig6B](https://manasataramgini.wordpress.com/wp-content/uploads/2020/09/pyrfig6b.png){width="75%"}Figure 6. Triangular number interstitial sums and integer area trapezia.

  - -----------------------------------------------------------------------

Footnote 1: <https://manasataramgini.wordpress.com/2017/10/23/triangles-hexes-and-cubes/>\
Footnote 2: <https://manasataramgini.wordpress.com/2017/05/14/the-magic-of-the-deva-ogdoad/>

