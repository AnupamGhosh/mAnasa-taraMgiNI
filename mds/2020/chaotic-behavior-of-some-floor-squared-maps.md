
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Chaotic behavior of some floor-squared maps](https://manasataramgini.wordpress.com/2020/05/04/chaotic-behavior-of-some-floor-squared-maps/){rel="bookmark"} {#chaotic-behavior-of-some-floor-squared-maps .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 4, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/05/04/chaotic-behavior-of-some-floor-squared-maps/ "12:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Consider the one dimensional maps of the form:

 $$x_{n+1}=\dfrac{\left(\lfloor x_n \rfloor \right)^2 + \{x_n\}^2 }{ax_n}$ , where  $\{x_n\}=x-\lfloor x \rfloor$  is the fractional part of  $x$$ 

What will be evolution of a  $x_0$  under this map when  $a=2$  or  $a=3$ ? We can see that for  $x_0> 0$  it will tend converge. However, the behavior is far more interesting for  $x_0< 0$ : It turns out that in these cases the trajectory of  $x_n$  exhibits chaotic behavior (Figure 1, 2, 3, 4).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/floor_square01a2_ev.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Here,  $a=2$  and we use  $x_0=-1.464$ ; of course all other  $x_0< 0$  show comparable behavior (but the choice of this for illustration  $x_0$  will become clear below). The evolution is plotted after discarding the first few values of  $x_n$ . While the evolution of  $x_0$  under the map is chaotic it is not entirely random. There are preferred zones and which  $x_n$  inhabits. This can be better visualized by plotting a histogram of all the values of  $x_n$  in the evolution under the map for 20000 iterations.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/floor_square01a2_hist.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


We can see that for any value of  $x_0< 0$  the iterates will be quickly pushed below -1. Further, we can also see that once a value is  $-2.5 \le x \le -1$  it will remain orbiting within these bounds. Thus, it settles into an attractor in this interval. However, we observe that there is are 2 zones of exclusion in this interval. Even though we initiate the mapping in the middle of the larger zone of exclusion (which is why we chose  $x_0=-1.464$ ), we observe that  $x_n$  moves away from that zone and mostly keeps away from it. As it oscillates between -1 and -2.5  $x_n$  repeatedly approaches the second zone of exclusion from either side but gets repelled by it. How can we precisely determine these repellors of the map? Those can be determined by solving the equation:

 $$2x^2=\left(\lfloor x \rfloor \right)^2+\left(x-\lfloor x \rfloor \right)^2$$ 

We have to solve such equations piecemeal due to the discontinuity of the  $\lfloor x \rfloor$  function. Because of the interval within which the attractor lies we have to only consider its solutions where  $\lfloor x \rfloor=-2$  and  $\lfloor x \rfloor=-3$ . By substituting these two values of the floors in the above floor equation we get the two quadratics  $x^2-4x-8=0$  and  $x^2-6x-18=0$ , whose roots will yield the repellors. The solutions of the first are  $2 \pm 2\sqrt{3}$ . Since only the negative root is within interval of for our attractor, we have one repellor as  $r_1=2 - 2\sqrt{3}$ . Similarly, from the second equation we get the second repellor to be  $r_2=3 - 3\sqrt{3}$ . These are shown as green lines in the above figures. One can see we initiated the map close to  $r_1$  and saw how it was repelled. However, if  $x_0=2 - 2\sqrt{3}, x_0=3 - 3\sqrt{3}$  then  $x_n$  remains fixed. Thus, while  $r_1, r_2$  repel  $x_n$  in their vicinity they are fixed points on the map (green points).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/floor_square01a3_ev.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Here,  $a=3$  and we initiate the mapping with  $x_0=-0.618$ . We can see that in this case for all  $x_0 < 0$  the iterates with be pushed below  $-\tfrac{1}{3}$ . Further, once  $-\tfrac{5}{3} \le x_n \le -\tfrac{1}{3}$  we can see that  $x_n$  will be trapped in an orbit within this interval. As in the above case, in the example illustrated in Figure 3, after briefly oscillating close to  $x_0$ ,  $x_n$  gets repelled away from it and that region is an exclusion zone for  $x_0< 0$ . However, unlike in the above case we do not have a clear second exclusion zone here (Figure 4).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/floor_square01a3_hist.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


Instead, we see that there are repeated attempts to come close to a certain line followed by repulsion away from it to flanking bands. As a result we do not get the second exclusion zone but have a saddle-like distribution around the line that is repeatedly approached. As in the above case to identify the primary repellor and the secondary value that behaves like a pseudo-attractor and also a repellor we need to solve the floor equation:

 $$3x^2=\left(\lfloor x \rfloor \right)^2+\left(x-\lfloor x \rfloor \right)^2$$ 

Again we solve it piecemeal. This time given the interval of our attractor  $\lbrack -\tfrac{5}{3}, -\tfrac{1}{3}\rbrack$  we have to consider only  $\lfloor x \rfloor= -1$  and  $\lfloor x \rfloor= -2$ . By substituting the first floor value in the above equation we get the quadratic  $x^2-x-1=0$  whose solutions are  $\phi, -\tfrac{1}{\phi}$ , where  $\phi$  is the Golden Ratio. Taking only the negative value that is in the interval which matters for us, we get the repellor of this map to be  $r_1=\tfrac{-1}{\phi}$ . With the next floor we get the quadratic  $x^2-2x-4=0$  with roots  $2 \phi, -\tfrac{2}{\phi}$ . The second of these give us  $r_2=-\tfrac{2}{\phi}$  the pseudo-attractor-repellor. This pseudo-attractor-repellor is like the superficially alluring woman who draws you but is a repellor you when you get too close. As in the above case  $r_1, r_2$  are also fixed points of the map.

Finally, we can investigate the evolution of closely separated  $x_0$  to see how closely they parallel each other (Figure 5).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/05/floor_square01a2_evcomp.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


Here we compare the evolution of  $x_0=-0.1464$  and  $x_0=-0.1465$  under the first map. We observe that while they are statistically the same in behavior the actual trajectories rapidly diverge. This is a hall mark of chaotic behavior.

