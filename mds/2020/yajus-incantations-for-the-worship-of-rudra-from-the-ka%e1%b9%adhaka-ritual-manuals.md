
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Yajus incantations for the worship of Rudra from the काठक ritual manuals](https://manasataramgini.wordpress.com/2020/12/27/yajus-incantations-for-the-worship-of-rudra-from-the-ka%e1%b9%adhaka-ritual-manuals/){rel="bookmark"} {#yajus-incantations-for-the-worship-of-rudra-from-the-कठक-ritual-manuals .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 27, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/12/27/yajus-incantations-for-the-worship-of-rudra-from-the-ka%e1%b9%adhaka-ritual-manuals/ "6:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This article is available as a [pdf document](https://manasataramgini.wordpress.com/wp-content/uploads/2020/12/katha_rudra-3.pdf). The notes from it are appended below.

The loss of the northern and northwestern कृष्नयजुर्वेद traditions due to the Mohammedan depredations of Northern India (aided an abetted by the predatory Anglospheric regimes) has been one the great tragedies faced by Hindudom. Hence, it is rather important to collate and restore whatever remains of these traditions, namely those belonging to the कठ and कपिष्ठल schools, which were once dominant in the greater Panjab and Kashmir. In the 1940s, विद्याभास्कर, वेदान्तरत्न सूर्यकान्त, संस्कृताचार्य of the पञ्जाब-विश्वविद्यालय, Lavapura (modern Lahore) had collated several कठ fragments that came from lost texts outside of the relatively well-preserved संहिता. These came from the lost ब्राह्मण and the surviving आरण्यक, as well as the lost मन्त्रपाठ of the कठ-s that went with the सूत्र-s of लौगाक्षि. Notable in this regard, were the following manuscripts that सूर्यकान्त found in what is today the terrorist state: 1) A शारदा manuscript which was written in 1033 Vikrama-संवत्, bright आषाढ अष्टमि (approximately June of 1111 CE) in Gilgit. Ironically, this manuscript was found in the possession of a mulla named Hafiz ar Rahman of the Panjab \[footnote 1] and contained 340 folios. This was an extensive paddhati with several कठ mantra-s and ब्राह्मण sections used in their late गृह्य rituals. Another शारदा manuscript, found in the possession of the same mulla, of 180 folios contains overlapping content from ब्राह्मण and mantra material used in कठ rituals. Finally, there was the D.A.V. college manuscript with two parts of 189 and 169 folios respectively that was again an extensive paddhati with overlapping material. The above Rudra-mantra-s come in the sections labeled Rudra-मन्त्राः or शताध्याय(Rudra)मन्त्राः and comprise their second division, coming after the शतरुद्रीय. The fate of these manuscripts after the vivisection of India in 1947 CE remains unclear. In the past year, the एग़न्गोत्रि trust has made freely available two independent texts which span the mantra-s in question from the रघुनाथ Mandira Sanskrit collection, Jammu. One is a शारदा manuscript of the शताध्याय-दीक्ष and another is a print version of the शताध्याय produced in the 1920s by the Kashmirian ब्राह्मण-s, तारछन्द कौला and केशव भट्ट. These have helped correct some problematic parts of the सूर्यकान्त texts.

The first part of this fragment is a rather important because is the only occurrence of a variant version of this famous incantation to Rudra found outside the Atharvaveda संहिता-s. The said incantation occurs as सूक्त 11.2 in the AV-vulgate (often taken to be the शौनक संहित) and as सूक्त 16.104 in the पैप्पलाद संहिता. In totality, the two AV versions resemble each other more closely and have a more extensive set of mantra-s. This clearly establishes that it was not a late acquisition of the कठ-s from the neighboring पैप्पलाद-s, who were also prominent in the same region (e.g. the Kashmirian intellectual भट्ट Jayanta). Two further points are notable. This text is entirely rhotacizing (e.g. अरिक्रवेभ्यः) relative the fully or partial lambdacizing AV संहिता-s (AV-vul: अलिक्लवेभ्यः; AV-P अरिक्लवेभ्यः). On the other hand, it has मृऌअतम्, mimicking the ऋग्वेद dialect, instead of the AV मृडतम्. Similarly, this text shows the archaism of using the RV-type dual form भवा-शर्वा as opposed to the AV भवाशर्वौ. This was likely originally part of the कठ-मन्त्रपाठ which went the सूत्र-s of लौगाक्षि.

It shares with the AV and शाङ्खायन-RV traditions, the conception of Rudra in his twin form --- Bhava and शर्व. In the शाङ्खायन-श्रौतसूत्र (4.20.1-2), Bhava and शर्व are called the sons of Rudra महादेव, thus presenting them as ectypes of the अश्विन्-s, who are the sons of Rudra in the RV \[footnote 2] and mirror the para-Vedic Skanda-विशाख dyad who are coupled with Rudra (e.g. in गृह्य-परिशिष्ट-1 of the Kauthuma Samaveda: [ॐ रुद्रं स्कन्द-विशाखयोस् तर्पयामि ।]{style="color:#0000ff;"}). In contrast, while Bhava and शर्व are used as epithets of Rudra in other Yajurveda traditions (e.g. तैत्तिरीय), they are not presented as twins. This suggests that the the कठ tradition developed in proximity to the locale where AV traditions original diversified in which the cult of the twins Bhava and शर्व, like that of the Greek Dioscouri, was dominant.

The second part is homologous to the equivalent section of the अरुण-प्रश्न of the तैत्तिरीय आरण्यक, which is used in the आरुणकेतुकचयन ritual, where the bricks of the citi are replaced by water-filled pots. It might have been part of an equivalent lost section of the कठ ब्राह्मण. It is largely equivalent to the TA version with a few variants that we have retained due to consistency across कठ manuscripts. Variants of the final mantra are found as AV-vulgate 7.87.1; AV-P 20.33.7 and तैत्तिरीय संहिता 5.5.9.3; कठ संहिता 40.5.33. The कठ version is oddly formed and unmetrical both in the संहिता and across the prayoga manuals. Hence, we retain it as is without emendation or metrical restoration based on the other संहिता-s.

  - -----------------------------------------------------------------------

footnote 1: He could have descended from converted ब्राह्मण-s\
footnote 2: <https://manasataramgini.wordpress.com/2020/01/12/the-asvin-s-and-rudra/>

