
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion-4](https://manasataramgini.wordpress.com/2020/07/19/matters-of-religion-4/){rel="bookmark"} {#matters-of-religion-4 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 19, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/07/19/matters-of-religion-4/ "8:56 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Pinakasena was visiting Somakhya and Lootika. He was seeking instruction on the सद्योजात-mantra from his hosts: "O आथर्वण and शारद्वती, I wish to learn the rahasya-s pertaining to the deployment of the सद्योजात-mantra to the great god कुमार, the patron deity of all our clans. We have borne his worship since the time our ancestors were in the now fallen lands of गन्धार in उत्तरापथ. Then they bore it to the Yaudheya republic and then to कान्यकुब्ज during the reign of emperor of हर्षवर्धन and finally to दक्षिणापथ."

His hosts led him to their fire room and seated him on the western side of their औपासन-vedi. There, they asked him to purify himself for the ritual with the sprinkling using the mantra-s to divine horse दधिक्रावन् and the waters. Lootika then signaled to him: "Offer the pouring with with incantation to the daughters of Rudra". Pinakasena did as instructed:

[namo रुद्रियाभ्यो .अद्भ्यः स्वाहा ||]{.mark .has-inline-color style="background-color:rgba(0, 0, 0, 0);color:#0000ff;"}

Obeisance to the daughters of Rudra. Hail to the waters.

Somakhya: "Now invoke प्रजापति the lord of the भृगु-s and अङ्गिरस्-es and offer fire sticks to him".

He did as instructed:

[नमः परमेष्ठिने प्रजापतये स्वाहा हिरण्यगर्भाय स्वाहा भृगूणां पत्ये स्वाहा ।अङ्गिरसां पतये स्वाहा ॥]{style="color:#0000ff;"}

Then he offered तर्पण-s:

[वसुभ्यो नमो रुद्रेभ्यो नम आदित्येभ्यो मरुद्भ्यो नमो मारुतेभ्यो नमो ।अश्विभ्यां नमो वैश्रवणाय नमो धर्माय नमः कामाय नमो ऋषिभ्यो नम आर्षेयेभ्यो नमो अङ्गिरोभ्यो नम आङ्गिरसेभ्यो नमो अथर्वेभ्यो नम आथर्वणेभ्यो नमः ॥]{style="color:#0000ff;"}

Then he recited after his hosts and made oblations at the appropriate calls:

[यश् छन्दसाम् ऋषभो विश्वरूपश् छन्दोभ्यश् छन्दांस्य् आविवेश । सतां शक्यः प्रोवाचोपनिषद् इन्द्रो ज्येष्ट इन्द्रियाय ऋषिभ्यो नमो देवेभ्यः स्वधा पितृभ्यो भूर्-भुवः-सुवश् छन्द ॐ ॥]{style="color:#0000ff;"}

He \[Indra], who is the bull among the meters, of all forms, emerging from the meters entered into the meters. The great one, Indra, who is possible to be \[reached] by the good, proclaimed the उपनिषत् to the ऋषि-s for attaining powers (full experience). Obeisance to the gods, for the ancestors the good station, the 3 व्याहृतिस्, the Veda, ॐ.

Then stressing to Pinakasena that his instruction was a continuation of the tradition imparted by the god Indra to their ancient ancestors, Somakhya began his instruction after placing a fire stick for the repelling of the non-sacrificing foes:

[॥ अयज्वनः साक्षि विश्वस्मिन् भरो३म् ॥]{style="color:#0000ff;"}

Thereafter he instructed Pinakasena to offer a fire stick after he recited the [incantation](https://manasataramgini.wordpress.com/2019/12/30/matters-of-religion-3/):

[त्वं देवानाम् असि रुद्र श्रेष्ठ]{style="color:#0000ff;"}

[तवस् तवसाम् उग्रबाहो ।]{style="color:#0000ff;"}[]{style="color:#0000ff;"}

[हृणीयसा मनसा मोदमान]{style="color:#0000ff;"}[]{style="color:#0000ff;"}

[आ बभूविथ रुद्रस्य सूनो ॥ + स्वाहा + इदं न मम ।]{style="color:#0000ff;"}

Somakhya: "The ऋषि of the सद्योजात-mantra is Gopatha. Its meter is अनुष्टुभ्, though it has an additional bhakti that is non-metrical and its देवता is कुमार Bhavodbhava. Alternatively depending on the prayoga there is a variant form where the देवता is Skanda-विशाखौ. The mantra itself goes thus:

[सद्योजातं प्रपद्यामि सद्योजाताय वै नमः ।]{style="color:#0000ff;"}

[भवे-भवे नादिभवे भजस्व मां भवोद्भव ॥]{style="color:#0000ff;"}

I take refuge in the one who had instantly arisen (सद्योजात). Verily obeisance to him who has arisen instantly. In existence and after existence do not partition my share; provide me a share of the ultimate existence, O one born of Bhava!

This is the core अनुष्टुभ् to which in regular practice the bhakti: [भवाय नमः ॥]{style="color:#0000ff;"} is appended at the end.

There are some key points that you must understand in relation to this ऋक्: Why is Skanda known as सद्योजात? It relates to him arising instantly from the semen of Rudra in the शरवन or गङ्गा river. The term is also used for Agni, the father of Skanda, in the ancient आप्री of the भार्गव-s:

[सद्यो जातो व्य् अमिमीत यज्ञम् अग्निर् देवानाम् अभवत् पुरोगाः ।]{style="color:#0000ff;"}

At birth he instantly measured out the ritual \[space]; Agni became the leader of the gods.

This is reminiscent Skanda becoming the leader, i.e. सेनानि, of the gods. In the old tradition of the भाल्लवि-s, he is said to have emerged from Rudra as the dual of Agni to burn down the पिशाची when invoked by वृश जान, the badger-like purohita of the इक्ष्वाकु lord त्र्यरुण. That is why this ऋक् is associated with the मूलमन्त्र of कुमार: [ॐ वचद्भुवे नमः ॥]{style="color:#0000ff;"} or that of बोधायन: [ॐ भुवे नमः स्वाह ॥]{style="color:#0000ff;"}. Indeed, one may do japa of the mantra using the following yantra. In the corners of the central त्र्यश्र-yamala (forming the hexagonal star) one must place the syllables of one of the above षडक्षरि-s. The in a circle around the central star one must place the 32 syllables of the core अनुष्टुभ्. Then in the central hexagonal hub of the star one must place the bhakti of 5 syllables. The name of कुमार, वचद्भू, is evidently an old linguistic fossil and indicates the emergence of the god from the पिशची-repelling incantation."

PS:"How is the second part of the mantra to be understood?"

Lootika: "The imperative verb bhajasva is to be applied twice once with the negation particle na and once without it. The na is coupled with the locative आम्रेडित bhave-bhave. When with the negation particle bhava is understood in more than one way depending on the votary and their interests. For people of the world, like us, a bhava can be each unit of conscious existence --- it could be each time we get up from sleep and return to conscious अहं-भाव. Here the negated verb na bhajasva might be understood as do not divide what is ours. In the positive sense it is applied to आदिभव --- the primal state or when the consciousness associated with you is in an identity loop --- a state of bliss. For the yati-s, like the practitioners of the पूर्वांनाय of yore in the वङ्ग or the कर्णाट country, it implies not being partitioned into multiple rebirths and being instead given मोक्ष (आदिभव)."

Somakhya: "While performing japa you may meditate on the deva mounted on a peacock with a fowl-banner surrounded by the troops Marut-s bearing spears, backed by Agni riding a rhinoceros. He is in the midst of a fierce battle leading the deva-s against the daitya-s and showering arrows on his foes. Thus one must meditate on him for the destruction of ब्रह्मद्विष्-es and dasyu-s. Alternatively, you may meditate on him in his dual form as Skanda-विशाख. In this case one must replace the terminal bhakti with: [नमो भवाय च शर्वाय चोभाभ्याम् अकरं नमः ॥]{style="color:#0000ff;"}. One may also perform the prayoga where Skanda is visualized as surrounded by the 12 awful goddesses:


 1.  विमोचिनी; 2) मोहिनी; 3) सुनन्दा; 4) पूतना; 5) आसुरी; 6) रेवती; 7) शकुनी; 8) पिशाचिका; 9) पाशिनी; 10) महामारी; 11) कालिका; 12) भामिनी.

Certain traditions holds them to be पौलस्त्या-s, the 12 sisters of रावण and call it the राक्षसी-prayoga but we do not subscribe to that. This may be done for pediatric purposes.

One may also deploy the mantra visualizing कुमार along with विशाख, शाख, नेजमेष and षष्ठी along with कौशिकी, the daughter Rudra and उमा. They should be accompanied by the following therocephalic and avicephalic goddesses who accompany कौशिकी:


 1.  वायसी, crow-headed; 2) उपका, owl-headed; 3) प्रचण्डा, lion-headed; 4) उग्रा, tiger-headed; 5) जया, elephant-headed; 6) जयन्ती, peahen-headed; 7) जयमाना, horse-headed; 8) प्रभा, goose-headed; 9) शिवा, jackal-headed; 10) सरमा, dog-headed; 11) विजया, falcon-headed; 12) प्रभावती, sheldrake-headed; 13) मृत्यु, crane-headed; 14) Niyati, grebe-headed; 15) अशनि, fowl-headed; 16) वृषदंशा, badger-headed; 17) शकुनी, thrush-headed; 18) पोतकी, munia-headed; 19) वानरी, monkey-headed.

The following देवी-s are visualized with human heads:


 20.  रेवती (some say she is cat-headed); 21) पूतना; 22) कटपूतना; 23) आलम्बा; 24) किंनरी; 25) मुखमण्डिका; 26) अलक्ष्मी or ज्येष्था; 27) अधृतिː 28) लक्ष्मी; 29) स्पृहा; 30) अपराजिता.

Along with षष्ठी and कौशिकी these constitute the 32 अऋण-देवी-s of the सद्योजात-ऋक् and should be worshiped with बीज-s derived from each of the syllables of the ऋक्. The meditation on कुमार and his emanations visualized in the midst of these देवी-s is the highest साधना of his mantra and may be deployed for षट्कर्माणि. This साधना may be accompanied by the use of the  $4 \times 4$  pan-diagonal magic square that adds up to 34 comprised of numbers from 1:16 with each representing a pair of the 32 goddesses. Of the sum, 32 are the goddesses that I've just mentioned. The remaining 2 are are Skanda and षष्ठी.


```
 wp-block-preformatted
\begin{tabular}{|c|c|c|c|} \hline 16 &2 & 3 & 13\ \hline 5 & 11 & 10 & 8\ \hline 9 & 7 & 6 & 12\ \hline 4 & 14 & 15 & 1\ \hline \end{tabular}
```


Now, Pinaki, what श्रुति-वाक्य do these goddesses with an avicephalic emphasis bring to your mind?"

PS:"I'm reminded of the mysterious words of my ancestor Atri Bhauma:

[वयश् चन सुभ्व आव यन्ति]{style="color:#0000ff;"}

[क्षुभा मर्तम् अनुयतं वधस्नैः ।]{style="color:#0000ff;"}

And like mighty birds \[the Marut-s] swoop down here, turbulently, to the mortal pursued by deadly weapons.

They are again compared to birds by Gotama:

[वयो न सीदन् अधि बर्हिस्हि प्रिये ।]{style="color:#0000ff;"}

Like birds \[the Marut-s] sit down on the dear sacred grass."

Given the genetic connection of [Skanda and his retinue to the Marut-s](https://manasataramgini.wordpress.com/2007/11/23/the-seven-fold-agni-and-the-seven-fold-troops-of-the-maruts/), who are said to descend like birds to the sacrificer, I find this avicephalic emphasis resonant.

Lootika "Good. Indeed, these goddesses accompanying षन्मुख are the precedents of the योगिनी-s of kula practice. They reveal themselves to the साधक usually in the form of certain reptiles and mammals. Thus, you may get the confirmation of your mantra-साधन with your दूती Shallaki by the apparition of योगिनी-s in the form of certain birds in quiet sylvan spots."

Somakhyaː "To conclude, Lootika will show you how to prepare the धत्तुरादि-विष for the विष-prayoga."

After Lootika showed him how to prepare the गुह्यविष from ओषधि-s, she said: "The details of these plants are not something you might know offhand. You can get them from your brother or my sister. In addition to Skanda, विशाख, शाख and नेजमेष you have to invoke and worship the 32 अऋण-देवी-s when making the विष. When a devadatta is subject this prayoga he is seized by a dreadful graha and with a crooked look on his face he wanders yelling and singing: [रुद्रः स्कन्दो विशाखो ।अहम् इन्द्रो ।अहम् ।]{style="color:#0000ff;"} even as old वग्भट has described the graha seizure. Such a seizure could also happen due to other reasons by agents of कुमार such as the ovine sprite or the caprine sprite or the आपस्तम्ब sprite. It could also happen among the ritually weak when they visit लन्का, मालाद्वीप or कास्ह्मीर where various पिशाच-graha-s naturally reside. The seizure often manifests differently among males and females. In females it might manifest as the state of pinning for ones lover as the old द्राविड-s would say. In such cases one may deploy this mantra with the visualization of कुमार with the मातृ-s. One also worships them when goes to the special holy spots around Eurasia."

PS: "Indeed, I heard from Shallaki of such a seizure of one of her relatives when he visited कास्ह्मीर. My brother informs me of the Kaubera-vrata-s that need to be performed in कास्ह्मीर to invoke Kaubera-पिशाच-s to counter seizures."

Somakhyaː"Thus, go ahead an practice this mantra. Sleep on the ground, avoid eating sweets and drinking sweet beverages, and sitting on cushions. Now you should conclude by performing the तर्पण as promulgated by बोधायन":

[ॐ स्कन्दं तर्पयामि । ओम् इन्द्रं तर्पयामि । ॐ षष्ठीं तर्पयामि । षण्मुखं तर्पयामि । ॐ जयन्तं तर्पयामि । ॐ विशाखं तर्पयामि । ॐ महासेनं तर्पयामि । ॐ सुब्रह्मण्यं तर्पयामि । ॐ स्कन्द-पार्षदांस् तर्पयामि । ॐ स्कन्द-पार्षदीश् च तर्पयामि ॥]{style="color:#0000ff;"}

