
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Two squares that sum to a cube](https://manasataramgini.wordpress.com/2020/02/19/two-squares-that-sum-to-a-cube/){rel="bookmark"} {#two-squares-that-sum-to-a-cube .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 19, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/02/19/two-squares-that-sum-to-a-cube/ "4:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**Introduction**\
This note records an exploration that began in our youth with the simple arithmetic question: *Sum of the squares of which pair integers yields a perfect cube?* Some obvious cases immediately come to mind:  $2^2+2^2=2^3; 5^2+10^2=5^3$ . In both these cases we can see that the addition of a square the to the square of the first number yields its cube. Hence, we can ask another related question: *Which are the integers whose squares when added to another perfect square yield their cubes?* In general terms the answers to these questions are the non-trivial (i.e. where  $x, y, z \ne 0$ ) solutions of the indeterminate equation:

 $$x^2+y^2=z^3 \;\;\; (\S 1)$$ 

Figure 1 shows the first few solutions lying on the surface defined by  $\S 1$ . Due to the 8-fold symmetry we restrict ourselves to positive solutions such that  $x \le y$  for a given  $z$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/sqrs_cube_xyz_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Since  $z$  is explicitly defined by  $x, y$  we can study the solutions of  $\S 1$  by plotting  $x, y$  on the  $x-y$  plane (Figure 2). At first sight, a mathematically naive person might perceive only a limited order in this plot of the solutions. However, as we shall see below, a closer examination and some algebra reveals a deep structure.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/sqrs_cube_xy_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad Some of the more obvious structure is indicated in different colors and is discussed below in detail.
```{=latex}
\end{center}
```


**Solution circles, 2 square numbers and Brahmagupta's identity**\
Some simple algebra can provide an understanding of the structure of the solutions of  $\S 1$ . From  $\S 1$  it is obvious that all solutions would be lattice points  $(x,y)$  on circles of radius  $z^{3/2}$ . Based on this, we can determine a set of  $x, y, z$  that are to solutions of  $\S 1$  thus: let  $z$  be an integer that is the sum of two perfect squares,  $z=m^2+n^2$ . Then,

 $$\left(m^2+n^2\right)^3= m^6 + 3m^4n^2 + 3m^2n^4 + n^6\lbrack 6pt\rbrack =(m^6+2m^4n^2+m^2n^4)+(n^6+2m^2n^4+m^4n^2)\lbrack 6pt\rbrack =m^2(m^4+2m^2n^2+n^4)+n^2(n^4+2m^2n^2+m^4)\lbrack 6pt\rbrack =(m(m^2+n^2))^2+(n(m^2+n^2))^2 \; \; \; (\S 2)$$ 

Thus, if  $z=m^2+n^2$  (i.e. it is a 2 square number)  $\S 2$  gives us an expression for  $z^3$  as the sum of 2 squares. Hence, the valid solutions to  $\S 1$  will be all

 $$x=m^3+n^2m\lbrack 6pt\rbrack y=n^3+m^2n\lbrack 6pt\rbrack z=m^2+n^2$$ 

For example, if  $z=5 = 1^2+2^2$  we have  $m=1; \; n=2$ . Thus,  $x=1^3+2^2\times 1=5$  and  $y=2^3+1^2\times 2= 10$ . Hence,  $z$  which belong to the 2 square sequence, i.e. numbers that can be non-trivially (i.e. one of the terms is not 0) expressed as the sum of 2 perfect squares, are valid solutions: **2, 5, 8, 10, 13, 17, 18, 20, 25, 26...** The corresponding  $x, y$  can be derived as above. One also notices from the above expressions that  $m^2+n^2=z$  is a common factor for  $x, y, z$  in all such solutions to  $\S 1$ .

While this method yields  $x=5, \; y=10, \; z=5$ , we notice that the same  $z=5$  also yields a second pair of  $x=2, y=11$ . Now how do we account for these additional pairs and for what values of  $z$  they arise?

The most obvious set of these correspond to  $z$  being equal to special numbers such as **5, 13, 17, 29...** One notices that these are both 2 square numbers and primes of the form  $4n+1$ . Fermat first noticed that such primes are always 2 square numbers. This attracted the attention of some of the greatest mathematicians like Euler, Lagrange and Gauss, who proved it to be so by using different methods. What is important for us is that such primes are also the कर्णा-s (hypotenuses) of primitive भुजा-कोटि-कर्णा triples. Thus, we have  $z=m^2+n^2$  from the fact that they are 2 square numbers and  $z^2=a^2+b^2$  from the fact that they are bh-k-k hypotenuses. Hence, we can write,

 $$z^3=z \cdot z^2 =(m^2+n^2)(a^2+b^2)\lbrack 6pt\rbrack =a^2m^2 + a^2n^2 + b^2m^2 + b^2n^2\lbrack 6pt\rbrack =(a^2m^2 + b^2n^2 +2am \cdot bn) + (a^2n^2 + b^2m^2 - 2am \cdot bn))\lbrack 6pt\rbrack =(an-bm)^2+(am+bn)^2 \; \; \; (\S 3)$$ 

Alternatively,

 $$(a^2m^2 + b^2n^2 -2am \cdot bn) + (a^2n^2 + b^2m^2 + 2am \cdot bn)\lbrack 6pt\rbrack (am-bn)^2 + (an+bm)^2 \; \; \; (\S 4)$$ 

 $\S 3$  and  $\S 4$  give us the famous identities of Brahmagupta from which we get two expressions for  $z^3$  as the sum of a pair of perfect squares when  $z$  is a  $4n+1$  prime. From  $\S 3$  we get:

 $$x =|bm-an| \lbrack 6pt\rbrack y = am+bn\lbrack 6pt\rbrack z = m^2+n^2=\sqrt{a^2+b^2}$$ 

From  $\S 4$  we get:

 $$x= |am-bn|\lbrack 6pt\rbrack y= an+bm\lbrack 6pt\rbrack z=m^2+n^2=\sqrt{a^2+b^2}$$ 

For example, if we take  $z=5$ , we have  $m=1, \; n= 2, \; a=3, \; b=4$ . From first set of expressions we get  $x=|4-6|=2, \; y=3+8=11$ . From the second we get  $x=|3-8|=5; \; y= 6+4= 10$ . The second pair  $x, y$  is equivalent to what we can get via  $\S 2$ . Thus, when  $z$  is a 2 square prime its cube can be expressed as the sum of distinct two pairs of squares.

Now, one can get a further set of  $z$  corresponding to solutions with two pairs of  $x, y$  when say  $z=2p$  where  $p$  is a  $4n+1$  prime. Here  $z^3=2p \cdot (2p)^2$ . From above we can write,

 $$p=m^2+n^2\lbrack 6pt\rbrack \therefore 2p= (m+n)^2+(m-n)^2$$ 

Further,

 $$p^2=a^2+b^2\lbrack 6pt\rbrack \therefore (2p)^2= (2a)^2+(2b)^2\lbrack 6pt\rbrack \therefore z^3= ((m+n)^2+(m-n)^2)((2a)^2+(2b)^2)$$ 

Thus, from Brahmagupta's identity we have:

 $$x = 2a(m+n)-|2b(m-n)|\lbrack 6pt\rbrack y = 2b(m+n)+|2a(m-n)|$$ 

Alternatively,

 $$x=2a(m+n)+|2b(m-n)|\lbrack 6pt\rbrack y=2b(m+n)-|2a(m-n)|$$ 

For example, if  $z=26= 2 \times 13$  then  $m=2, \; n=3, \; a=5, \; b=12$ . Thus,  $x=74, \; y=110$  or  $x=26, \; y=130$ . Similarly, one can derive formulae for  $z=4p, 8p...$ 

When  $z=p^2 =a^2+b^2$  where  $p=m^2+n^2$  is a  $4n+1$  (2 square) prime we can show via repeated application of Brahmagupta's identity that there 3 possible  $x,y$  pairs whose squares can compose  $z^3$ . We can derive the below formulae for them:

 $$x=a^3+b^2a\lbrack 6pt\rbrack y=b^3+a^2b$$ 

Or,

 $$x=p(a^2-b^2)\lbrack 6pt\rbrack y=2pab$$ 

Or,

 $$x= |m (b^2 n + 2 a b m - a^2 n ) - n | a^2m + 2 ab n - b^2 mb| | \lbrack 6pt\rbrack y= (a m - a n + b m + b n) (a m + a n - b m + b n)$$ 

As an example, by applying the above formulae we can see that for  $z=25 = 5^2$  its cube,  $25^3$ , can be split up into 3 distinct pairs of squares:  $35^2+120^2 = 44^2 + 117^2 = 75^2 +100^2$ .

The nested combinatorial application of Brahmagupta's formula can thus result in increasingly complex formations with multiple alternative partitions of a perfect cube into perfect squares for different multiples of the 2 square primes. Thus if  $z=50=2 \times p^2$  we get 4 distinct pairs of  $x, y$ ; for  $z=125= 5 \times 5^2$  we get 5 distinct pairs of  $x, y$ .

When a number is product of two distinct  $4n+1$  primes then its cube can be partitioned into 8 distinct pairs perfect squares. For instance,

 $$65^3= 7^2 + 524^2 = 65^2+ 520^2 = 140^2 + 505^2 = 191^2 + 488^2= 208^2 + 481^2 = 260^2+ 455^2= 320^2+ 415^2 = 364^2+ 377^2$$ 

The same applies to the multiples of these numbers by 2, 4... When a number is a product of a  $4n+1$  prime and the square of a distinct  $4n+1$  prime then the cube of that number can be partitioned into 14 different pairs of perfect squares. Thus,  $325=5^2 \times 13$  is the first number whose cube can be thus partitioned. The next is  $425 = 5^2 \times 17$ . Figure 3 shows some circles with multiple pairs of  $x, y$  whose squares sum to the same  $z^3$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/sqrs_cube_xy_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Ramanujan and Landau had independently discovered that the number of positive integers,  $N(x) \le x$ , that can be written as a sum of 2 squares including 0 defines a constant:

 $$\displaystyle K_{RL} = \lim_{x \to \infty} N(x)\dfrac{\sqrt{\log(x)}}{x} = 0.7642236535...$$ 

Based on this, we can also look at how many unique  $z \le x$ , i.e.  $N(z)$  by defining:

 $$K=N(z)\dfrac{\sqrt{\log(x)}}{x}$$ 

We empirically observe that this  $K \approx 0.8071$  (Figure 4). The  $K_{RL}$  has a closed form which has deep connections to the Riemann  $\zeta(x)$  and the Dirichlet  $\beta(x)$ . However, we are not aware of a closed form for the constant  $K$  in our case or even its exact value as  $x \to \infty$ . This  $K$  seems to reach a fairly stable value around that reported above but  $K_{RL}$  converges very slowly.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/sqrs_cube_rl_fig4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


**Solution families on curves**\
In addition to the arrangement of solutions as lattice points on circle of radius  $z^{3/2}$ , there are also other patterns that become apparent from a closer look at the solutions in the  $x-y$  plane (Figure 2). The most obvious is the set of points lying on the  $y=x$  line at the right diagonal boundary of the plot. These are defined by a family of the form:

 $$x= y =2n^3; \; z= 2n^2$$ 

This family defines the sequence of integers twice whose square equals a perfect cube: **2, 16, 54, 128, 250, 432, 686, 1024 ...** (blue line in Figure 3).

Then we see pairs of regularly positioned points that eventually lie closer to the right diagonal boundary (emphasized in red and blue in Figure 3). These are families that lie on one of two parallel curves. The first is defined by the parametric equations:

 $$x = 2t^3 + 6t^2 + 3t - 2 \lbrack 6pt\rbrack y = 2t^3 + 12t^2 + 21t + 11$$ 

This curve has a lobe (Figure 5; gray) and generally resembles the shape of the curve of von Tschirnhaus (see below). The second curve takes the form:

 $$x=2t^3+6t^2+9t\lbrack 6pt\rbrack y=2t^3+12t^2+27t+27$$ 

This curve has no lobe and lies inside the divergent arms of the first one (Figure 5; dark orange).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/02/sqrs_cube_xyz_fig5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


A further family lies on the curve defined by the below parametric equations (emphasized in red in Figure 3):

 $$x=3t^2-1\lbrack 6pt\rbrack y=t^3-3t$$ 

This curve has a single lobe and two divergent arms (Figure 5, light orange) and is a version of the eponymous curve discovered by Ehrenfried Walther von Tschirnhaus, the famous German polymath, who among other things reinvented porcelain in the Occident. It crosses over the above two curves and proceeds closer to the vertical left boundary of the  $x-y$  plot.

Finally, we a family of families lying on the family of curves (shown in blue in Figures 3, 5) defined by the parametric equations:

 $x=kt^2 + k^3\lbrack 6pt\rbrack y= t^3 + k^2t$ ,\
where  $k=1, 2, 3 ...$ 

The first member of each of the new families on this family of curves starts from where the curve intersect the  $y=x$  line. Thus, each starts with the points  $x=y=2k^3$  (Figure 3). The first of this gives yields the answer to the second question posed in the introduction. When the square of  $x=n^2+1$  is added to the square of  $y=n^3+n$  we get the cube of  $z=n^2+1$ , i.e.  $x=z$ . It remains unknown to us if there are any further families beyond these.

