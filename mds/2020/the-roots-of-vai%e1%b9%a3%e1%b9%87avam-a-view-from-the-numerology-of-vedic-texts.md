
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The roots of वैष्णवम्: a view from the numerology of Vedic texts](https://manasataramgini.wordpress.com/2020/01/02/the-roots-of-vai%e1%b9%a3%e1%b9%87avam-a-view-from-the-numerology-of-vedic-texts/){rel="bookmark"} {#the-roots-of-वषणवम-a-view-from-the-numerology-of-vedic-texts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 2, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/01/02/the-roots-of-vai%e1%b9%a3%e1%b9%87avam-a-view-from-the-numerology-of-vedic-texts/ "4:29 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While glorified with a 1000 names in the famous stotra of the early सात्वत tradition of the महाभरत, in the texts of an even earlier period the god primarily went under that name विष्णु. Indeed, even the litany of the 1000 names begins with [विश्वं विश्णुर् वशत्कारः]{style="color: #0000ff"} signaling the connection to that ancient layer of tradition, with विश्वम् i.e, "everything" being an etymological elaboration of his name विष्णु, "the all-pervader". Further, वषट्कार indicates his link to the Vedic ritual (the वषट् offering to विष्णु is already specifically mentioned twice in the ऋग्वेद) wherein we can trace the meteoric rise of this god as the head-deity of one of the great sectarian streams of the आस्तिक-s.

His later rise notwithstanding, it should be borne in mind that विष्णु is a prominent deity of early Indo-European vintage. Mentioned 113 times in ऋग्वेद itself (Table 1), one can already sense his prominence in the pantheon. His cognate in the Germanic world Víðarr provides evidence of his importance in the early undivided IE days. In the Gylfaginning of the Icelandic prose Edda, Snorri Sturluson, who still had links to the old Germanic heathenism, described Víðarr thus:\
*"Víðarr is the name of the silent asa. He has a very thick shoe, and he is the strongest next after Thor. From him the gods have much help in all hard tasks."*\
Here, his being "strongest next to Thor" is mirrored in the आर्य tradition where विष्णु is nearly equal/equal to his elder brother/friend Indra. Víðarr "helped the gods in the hard tasks", a specific feature shared with विष्णु in the आर्य tradition. Finally, his thick shoe is related to the stride Víðarr takes in the final battle of the gods, Ragnarok, where he tramples the nether jaw of the demonic Fenris-wolf and opens its mouth wide apart to slay it. In the आर्य tradition, likewise विष्णु's three demon-conquering wide strides are well-known and this gives him his alternative name in the Veda, उरुगाय, "the wide-goer". This is related to again related to the etymology of Víðarr = "wider" and the very cognate of Víðarr, vitara, appears in the श्रुति in the context of विष्णु: [अथाब्रवीद् वृत्रम् इन्द्रो हनिष्यन् सखे विष्णो वितरं वि क्रमस्व ॥]{style="color: #0000ff"} : Then Indra spoke as he prepared to slay वृत्र: "friend विष्णु stride widely." The strides of विष्णु are also said to make space by widening the universe for Indra to swing his vajra to slay वृत्र. This is parallel to Víðarr holding the jaws of the Fenris-wolf wide apart.

Both विष्णु and Víðarr are mentioned as possessing a special world/realm. In the case of विष्णु, who is called the "cowherd" or the cattle-protector, it is wide pasture in a mountainous realm. Víðarr's is a case it is mentioned as thick with grass. Finally, we may note that Víðarr is one of the deities to survive the Ragnarok and usher in the new Germanic "satyayuga". Among the आर्य-s, as the "time-god", he is seen as again surviving the yuga-s in the later विष्णु-centric traditions.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/01/vishnu_taranis.png){width="75%"}
```{=latex}
\end{center}
```


  - 

In the Celtic world, the chief Gaulish deity of the Indra-class, Taranis, appears to have absorbed elements of his partner, the deity of the विष्णु-class. We go somewhat out on a limb to suggest that the late iconography of Taranis, before the end of the Gaulish religion, was actually influenced by that of विष्णु carried either directly by Indian or Indianized Iranic travelers to the West or from them via intermediaries to the Gauls (Figure 1). Examples of this influence are seen in the wheel-wielding images of Taranis and his depiction on the famous Gundestrup cauldron. In the case of the latter, the Indian influence is clinched by the elephants associated with the goddess (probably Rhiannon) who is depicted similar to लक्ष्मी the wife of विष्णु. We postulate that this influence was because they could see obvious parallels between their deities and the Indian counterparts, which in turn was a consequence of Taranis absorbing elements of the original विष्णु-class deity.

Closer to the Indo-Aryan realm, our Iranic cousins have deity called Rashnu Razishta, the heavenly judge, who superficially plays a Citragupta-like role. His name and sudden appearance without parallels elsewhere in the IE world suggests that he is none other than a cognate of the Indo-Aryan विष्णु. His name probably underwent a folk-linguistic mutation from 'vi' to 'ra' for the Iranians probably (wrongly) interpreted the 'vi' as a prefix with a negative connotation and "corrected" it to 'ra' the proper one (A suggestion also made by Puhvel/Dumezil though, in my opinion wrongly, opposed by some Indo-Europeanists). His popular worship among the Iranians is hinted by the occurrence of his name as part of personal names found in the Persepolis Fortification Tablets from the time of the Achaemenid emperor Darius-I: रस्ह्नुदात and Rashnuka and the Parthian name Rashnumithra. Rashnu's link to विष्णु is indicated by his accompanying Mithra to his right on his chariot when he rides forth for battle. In the Zoroastrian strain of the Iranic religion, Mithra with his vazra has taken the place of Indra and Rashnu retains the role of विष्णु now as Mithra's assistant instead (Note that even RV विष्णु is specifically linked to Mitra, e.g., in RV 1.156.1 and RV 8.52.3). Notably, Yasht 12 to Rashnu (which has been clearly redacted to interpolate Ahura मज़्दा's name for Rashnu in the initial manthra-s) shows that he held an important role in the ritual and he is described as tall, forceful deity praised in superlative terms indicating the importance he once held in Iranian realm:

[rashnvô ashâum rashnvô razishta rashnvô spênishta rashnvô vaêdhishta rashnvô vidhcôishta rashnvô parakavistema rashnvô dûraêdareshtema rashnvô arethamat bairishta rashnvô tâyûm nijakhnishta...]{style="color: #008000"}

O holy Rashnu! O most-true Rashnu! most-beneficent Rashnu! most-knowing Rashnu! most- discerning Rashnu! most-fore-knowing Rashnu! most far-seeing Rashnu! Rashnu, the best doer of justice! Rashnu, the best smiter of thieves (tâyu = Skt स्तायु)...

Keeping with his far-seeing nature, the yasht is unusual in describing the Iranic karshvare-s (world regions) and heavenly constellations as the regions seen by Rashnu as he flies along. This peculiar feature of the yasht, seen with no other Iranic deity, points to two parallels with विष्णु: (1) The listing of the realms is suggestive of विष्णु pervading them or striding over them. (2) Among the places Rashnu is invoked there are 3 stations mentioned in succession the quarters of the earth, boundary of the earth and all over the earth. Apart from reminding one of the three strides, it also reminds one of the ऋक् where विष्णु is described as fixing the boundary of the earth by the pegs at the quarters (RV 7.100.3).

In conclusion, we see that the विष्णु-class of deities, despite their spotty occurrence in what has come down to us of the IE traditions, can be traced to a prominent proto-deity in the early IE religion. Here, by early we mean at least the time when the western thrust of the IE people from their Yamnaya homeland took place --- the group from which the Germanic, Celtic and Indo-Iranian branches ultimately derive. This conclusion is of importance in understanding the rise of विष्णु in the Indian tradition. We shall explore the early stages of that by looking at the counts of the occurrence of the name विष्णु in several Vedic texts (Table 1). These texts span the entire range of Vedic tradition from the earliest RV to the [ऋग्विधान]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"} which clings to the very edge of the late Vedic literary activity. In between lie the संहिता-s of the 3 other Veda-s, the khila of the RV, the ब्राह्मण-s, the श्रौत-सूत्र-s and the गृह्य-सूत्र-s. Across these texts the most common name of the deity under consideration is विष्णु. He is known by other names like उरुगाय, शिपिविष्ट and the epithet evaya in the core Vedic period. In the latest Vedic texts we encounter the name नारायण (e.g. तैत्तिरीय-आरण्यक), which became prominent in the epic period. However, all these names are generally rare making the count for विष्णु a good proxy for estimating the extent of his mentions.

Table 1. The occurrence of विष्णु in Vedic texts

  Text                       Veda      #

  - ------------------------- ------ -----
  ऋग्वेद                       RV       113
  ऋग्वेद-खिलानि                RV        15
  कौषीतकि-ब्राह्मण             RV        14
  Aitareya-Brahmana          RV        24
  आश्वलायन-श्रौतसूत्र            RV        39
  शाङ्खायन-श्रौतसूत्र            RV        48
  आश्वलायन-गृह्यसूत्र             RV         1
  शाङ्खायन-गृह्यसूत्र             RV        10
  ऋग्विधान                    RV        14
  Atharvaveda (vulgate)      AV        69
  Atharvaveda (पैप्पलाद)       AV        71
  Gopatha-ब्राह्मण             AV        22
  वैतान-सूत्र                   AV        14
  कौशिक-सूत्र                  AV        11
  मैत्रायणी-संहिता              KYV      233
  काठ-संहिता                  KYV      197
  तैत्तिरीय-संहिता              KYV      175
  तैत्तिरीय-ब्राह्मण             KYV       79
  बौधायन-श्रौतसूत्र             KYV      165
  आपस्तम्ब-श्रौतसूत्र             KYV      101
  हिरण्यकेशि/सत्याषाढ-श्रौतसूत्र   KYV       88
  मानव-श्रौतसूत्र               YYV       84
  वाराह-गृह्यसूत्र               KYV        5
  मानव-गृह्यसूत्र                KYV        7
  वैखानस-गृह्यसूत्र               KYV       23
  हिरण्यकेशि-गृह्यसूत्र            KYV        7
  लौगाक्षी-गृह्यसूत्र             KYV        2
  वाजसनेयि-संहिता (माध्यन्दिन)   SYV       76
  शतपथ-ब्राह्मण (माध्यन्दिन)     SYV      223
  काट्यायन-श्रौतसूत्र            SYV       40
  पारस्कर-गृह्यसूत्र              SYV       12
  सामवेद Kauthuma-संहिता       SV        22
  पञ्चविंश-ब्राह्मण              SV        28
  Mantra-ब्राह्मण              SV        18
  खादिर-गृह्यसूत्र               SV         4
  Jaimini-गृह्यसूत्र             SV        12
  Kauthuma-गृह्यसूत्र            SV         4

First, let us get some caveats regarding this table out of the way. These texts are of very different sizes; thus, someone could claim that the counts could simply reflect the size differences. Hence, one would wish to normalize it by text size. But what unit do you use for normalization? The word would be the ideal unit but it is difficult to obtain for all these texts because of samasta-pada-s typical of Sanskrit not being separated in each case. An alternative is the size of the file in bits. However, this depends on the encoding/format used and we do not have all the texts in a common encoding/format. Next, we have ascii or utf-8/16 encoding files for many of them but not all. The rest were counted using the html files on the TITUS system. We cannot be sure of the completeness of all the texts in TITUS. This said, we can still use the absolute counts reasonable well by comparing "apples with apples". The संहिता-s may be approximately seen to be of the same order of magnitude. The ब्राह्मण can be again approximately compared, and the equivalent classes of kalpa texts can be similarly compared.

With these caveats in place, one thing that stands out is the extraordinary prominence of विष्णु in the Vedic tradition represented by the Yajurveda. This is clearly in contrast with the other Vedic tradition, namely that of the Samaveda. Tellingly, the one Samavedic text that is enriched in विष्णु is the Mantra-ब्राह्मण, which is primarily a collection of यजुष्-es within the Samavedic tradition. This short collection of यजुष्-es has the same order of magnitude of mentions of विष्णु as the SV Kauthuma-संहिता and पञ्चविंश-ब्राह्मण, both of which are several times the size of the Mantra-ब्राह्मण.

The divisions in the Vedic tradition correspond to the roles of the ritualists: the hautra tradition of the RV practitioners, the आध्वर्यव tradition of the YV practitioners and the छान्दोग tradition of the SV practitioners. The AV practitioners are associated with the role of the Brahman but in reality have their own parallel श्रौत tradition. We see that in the आध्वर्यव tradition the prominence of विष्णु is across the three sections, i.e., the संहिता, the ब्राह्मण and श्रौत-सूत्र-s that roughly corresponding to temporal layers within the tradition. Further, in the hautra tradition, the श्रौत-सूत्र-s have more mentions of विष्णु than their ब्राह्मण-s, which are larger than the सूत्र-s in size. Again, notably, with one exception (we will come to that in the end) the गृह्य-सूत्र-s have relatively low occurrences of विष्णु across the board relative to the corresponding श्रौत-सूत्र-s.

Now, in the श्रौत practice the adhvaryu plays the most important role in terms of the physical actions of the ritual (of course accompanied by यजुष् incantations). He also issues calls to the होतृ and उद्गातृ to play their parts. The होतृ in contrast primarily plays the role of reciting the सामिधेनी-s and the शस्त्र-s and the like, while the उद्गातृ's main role is the singing of the stotra-s during the Soma ritual. While the YV and SV texts as we *have them today* clearly postdate and presuppose the core RV, we know from the internal evidence of the RV that there were already YV and SV practitioners alongside the composers of the RV. E.g.: [उद्गातेव शकुने साम गायसि]{style="color: #0000ff"} : You O bird sing a सामन् like the उद्गातृ. t[वम् अध्वर्युर् उत होतासि पूर्व्यः प्रशास्ता पोता जनुषा पुरोहितः ।]{style="color: #0000ff"} : You (Agni) are the adhvaryu, the primal होतृ, the प्रशस्तृ (an assistant of the होतृ who is also known as the उपवक्तृ or the मैत्रावरुण who plays a special role in recitations to those two gods), the पोतृ (the assistant of the Brahman who specializes in प्रायश्चित्त-s) and from birth the purohita. Further, the parallel in the Zoroastrian tradition suggest that at least the होतृ, the adhvaryu and some version of the brahman go back to the proto-Indo-Iranian period.

While these ऋत्विक्-s function in unison in the श्रौत rituals their mantra collections and activities suggest that they originally represented alternative ritual traditions within the early Indo-European fold, which were brought together under a single framework by at least the proto-Indo-Iranian period. While under a common framework they clearly maintained their own parallel traditions, techniques of composition and ritual principles. When we take this in to account, along with the evidence for the presence of a prominent विष्णु-class deity in the early Indo-European religion, we can account for the development of वैष्णव tendencies in the Veda: we posit that it was not that विष्णु rose to prominence in the middle Vedic period (as opposed to the early Vedic core RV with the ऐन्द्राग्न and old आदित्य dominance) from nothing but was always a special deity in the आध्वर्यव tradition of the Indo-Aryans. What happened was the rise to dominance of the आध्वर्यव tradition in the Indo-Aryan श्रौत ritual. Conversely, it is conceivable that on the Iranian side the hautraka tradition dominated, at least in early Zoroastrianism where Zarathustra calls himself the cognate zaotar.

These observations can be summed up by one statement in the आध्वर्यव tradition: [यज्ञो वै विष्णुः ।]{style="color: #0000ff"}: The ritual is verily विष्णु. Thus, by identifying विष्णु with the ritual he is identified with the core activities of the आध्वर्यव tradition. Hence, this is consistent with this frequent mention within this tradition. This proposal is further strengthened by the two special देवता-dvandva-s that are common though not unique to the YV tradition but not found in the RV संहिता, which is otherwise rich in देवता-dvandva-s. The main dvandva featuring विष्णु in the RV is इन्द्राविष्णू, where he is linked to his usual partner Indra. The देवता-dvandva अग्नाविष्णू, found in the AV, RV-ब्राह्मण and frequently in the YV tradition, marks the special position of विष्णु not only as the last of the deities to receive the sacrifice but also tacitly or not so tacitly indicates his supremacy by placing him at the end of the pantheon (Aitaryeya-ब्राह्मण: [अग्निर् वै देवानाम् अवमो विष्णुः परमस् तदन्तरेण सर्वा अन्या देवता ।]{style="color: #0000ff"} : Agni is the lowest of the gods, विष्णु is the foremost, all the other deities lie in between. While primarily positional it also hints the primacy of विष्णु). The second is the dvandva विष्णू-वरुणा seen, for example, in the तैत्तिरीय and the Aitaryeya-ब्राह्मण traditions. This dvandva has a special role that is typical of the आध्वर्यव tradition, proper completion of यज्ञ, the invocation of विष्णु protects from the badly done यज्ञ while the invocation of वरुण protects the well-done one and between the two all is taken care off [विष्णुर् वै यज्ङस्य दुरिष्टम् पाति वरुणः स्विष्टं । तयोर् उभयोर् एव शान्त्यै ॥]{style="color: #0000ff"}).

Thus, the unusual situation of dvandva-s not found in the RV but shared by the RV ब्राह्मण-s and YV tradition, and the prominence of विष्णु in the "little YV" within the SV, i.e., the Mantra-ब्राह्मण, suggests that these are late entrants into the SV and RV tradition from the YV tradition. Indeed, the even greater prominence of विष्णु in the RV श्रौत-सूत्र-s as opposed to the ब्राह्मण-s again indicates the आध्वर्यव dominance now entering the hautra territory. Thus, it suggests that the rise of विष्णु was essential a feature of adhvaryava dominance in the श्रौत ritual. This in contrast to the protogonic प्रजापति, who while emerging late from the para-Vedic periphery, uniformly affected all the Vedic traditions. The प्राजापत्य-s competed with विष्णु for the two figures of the primordial turtle कश्यप and the primordial boar वराह. While the former was originally associated with Indra (from the RV itself), the latter is hinted to be associated with विष्णु in the early AV tradition recorded in the पैप्पलाद-संहिता. However, the प्राजापत्य-s laid a strong claim to both before विष्णु eventually won and claimed both the figures as his अवतार-s in the Post-Vedic period.

Finally, we saw that the गृह्य-सूत्र-s have a low frequency of mention of विष्णु with the exception being that of the तैत्तिरीय-affiliated वैखानस tradition. The गृह्य-सूत्र-s for most part generally record an archaic core of household rites of passage quite different from the श्रौत rituals. Thus, the absence of a special role of विष्णु beyond specific contexts like the fertility ritual for preparing the womb for the embryo is not unexpected. Now, one of the two earliest sectarian वैष्णव traditions arose among the वैखानस-s. Thus, what we are seeing in the case of the वैखानस-गृह्यसूत्र is the emergence of this tradition which went on to become a still extant system of the combined iconic and fire worship of विष्णु. The second sectarian tradition centered on विष्णु, the पञ्चरात्र, explicitly identifies itself with the शुक्ल-yajurveda. Thus, we may say that rise of वैष्णवम् itself is an internal development within the आध्वर्यव tradition, with the two early schools emerging from each of the main Yajurvedic divisions. Once these had emerged, they influenced the latest of the Vedic texts across the traditions. We see this in the case of the ऋग्विधान, which, for a relatively short text, has several mentions of विष्णु in the context of what appears to be an early पाञ्चरात्रिक section.

To conclude we may ask what about the सात्वत tradition that affiliates itself to the पाञ्चरात्रिक tradition? We have evidence that विष्णु's primary manifestation in that system the वासुदेव along with the 3 other व्यूह deities and the goddess एकानंशा probably have para-Vedic roots in the Indo-Iranian borderlands. Likewise, with the watery नारायण or his humanized dyadic form of Nara-नारायण. But these traditions always saw itself as a part of वैष्णव system. Hence, we posit that they are part of greater "वैष्णव" tradition of old IE provenance that in addition to influencing the आध्वर्यव tradition also had "lower" para-Vedic registers such as these. Nevertheless, this tradition's links to the old विष्णु are hinted by certain specific features, for example: (1) The वासुदेव in his human manifestation (कृष्ण देवकीपुत्र) consorts with a large number of women. विष्णु consorting with a bevy of goddesses is already mentioned in RV 3.54.14. This also relates to his early fertility role as the protector of the sperm of males. (2) Already in the RV विष्णु is repeatedly mentioned as being the cowherd or the cow-protector (RV 1.22.18; RV 3.55.10). Thus, this aspect of the देवकीपुत्र of the सात्वत religion are likely merely a humanization of an old trait of the deity.

