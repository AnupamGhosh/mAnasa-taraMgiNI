
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [बृहस्पति-शनैश्चरयोर् yuddham-2020 इत्यादि](https://manasataramgini.wordpress.com/2020/12/14/b%e1%b9%9bhaspati-sanaiscarayor-yuddham-2020-ityadi/){rel="bookmark"} {#बहसपत-शनशचरयर-yuddham-2020-इतयद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 14, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/12/14/b%e1%b9%9bhaspati-sanaiscarayor-yuddham-2020-ityadi/ "6:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The below is only for information. Parts of it should not be construed as any kind of prognostication on our part.

The great Hindu naturalist वराहमिहिर describes various kinds of planetary conjunctions or grahayuddha-s in his ब्रिहत्संहिता (chapter 17) thus:

[युद्धं यथा यदा वा भविष्यम् आदिश्यते त्रिकालज्ञैः ।]{style="color:#0000ff;"}\
[तद् विज्ञानं करणे मया कृतं सूर्य-सिद्धान्ते ॥]{style="color:#0000ff;"}\
The time and nature of planetary conjunctions (graha-yuddha) can be predicted by astronomers. That science has been \[taught] in astronomical work composed by me \[based on] the सूर्य Siddhānta. \[Here he is referring to his पञ्चसिद्धान्त]

[वियति चरतां ग्रहाणाम् उपर्युपर्य् आत्म-मार्ग-संस्थानाम् ।]{style="color:#0000ff;"}\
[अति-दूराद् दृग्-विषये समताम् इव सम्प्रयातानाम् ॥]{style="color:#0000ff;"}\
The planets revolve in space in their respective orbits that are positioned one above the other. \[However,] due to their great distance, when observed they appear as if revolving on the same surface (i.e. the sky). \[Here, वराहमिहिर clarifies that even though it is called a yuddha how it must be understood in the scientific sense.]

[आसन्न-क्रम-योगाद् भेद+उल्लेख+अंशु+मर्दन+असव्यैः ।]{style="color:#0000ff;"}\
[युद्धं चतुष्प्रकारं पराशर आद्यैर् मुनिभिर् उक्तम् ॥]{style="color:#0000ff;"}\
In the order of the proximity of the conjunct planets: 1. bheda (occultation); 2. ullekha (near tangential contact); 3. अंशुमर्दन (the grazing of rays); 4. asavya (apart) are the four types of conjunctions described by पराशर and other sages.

[भेदे वृष्टि-विनाशो भेदः सुहृदां महाकुलानां च ।]{style="color:#0000ff;"}\
[उल्लेखे शस्त्र-भयं मन्त्रिविरोधः प्रियान्नत्वम् ॥]{style="color:#0000ff;"}\
In the bheda conjunction, there is drought and friends and persons of great families become enemies; in the ullekha conjunction there is fear of weapons, a rebellion of ministers, but there is \[abundance of] good food.

[अम्शु-विरोधे युद्धानि भूभृतां शस्त्र-रुक्-क्षुद्-अवमर्दाः ।]{style="color:#0000ff;"}\
[युद्धे च+अप्य् अपसव्ये भवन्ति युद्धानि भूपानाम् ॥]{style="color:#0000ff;"}\
In the अंशुमर्दन conjunction, kings go to war and people are afflicted by weapons, disease or famine. In apasavya (asavya) conjunction, rulers go to war.

[रविर् आक्रन्दो मध्ये पौरः पूर्वे +अपरे स्थितो यायी ।]{style="color:#0000ff;"}\
[पौरा बुध-गुरु-रविजा नित्यं शीतांशुर् आक्रन्दः ॥]{style="color:#0000ff;"}\
The Sun in mid-heaven is \[called] आक्रन्द; paura in the east and when stationed in the west a यायिन्. Mercury, Jupiter, and Saturn are always paura. The Moon is always आक्रन्द.

[केतु-कुज-राहु-शुक्रा यायिन एते हता घ्नन्ति ।]{style="color:#0000ff;"}\
[आक्रन्द-यायि-पौरान्-जयिनो जयदाः स्व-वर्गस्य ॥]{style="color:#0000ff;"}\
Ketu, Mars, राहु and Venus are always यायिन्-s. These are either struck (defeated) or strike (win). Depending on whether the आक्रन्द, यायिन् or paura losses or wins the objects associated with of their respective categories \[suffer or prosper].

[पौरे पौरेण हते पौराः पौरान् नृपान् विनिघ्नन्ति ।]{style="color:#0000ff;"}\
[एवं याय्य् आक्रन्दा नागर-यायि-ग्रहाश् च+एव ॥]{style="color:#0000ff;"}\
A paura defeated by another paura, results in city-dwellers and kings being smitten. Similarly, if a यायिन् or an आक्रन्द is defeated by another or respectively by a paura or यायिन् \[the objects associated with them are affected accordingly].

[दक्षिण-दिक्स्थः परुषो वेपथुर् अप्राप्य सन्निवृत्तो +अणुः ।]{style="color:#0000ff;"}\
[अधिरूढो विकृतो निष्प्रभो विवर्णश् च यः स जितः ॥]{style="color:#0000ff;"}\
That \[planet] which is positioned to the south, at a cusp, showing rapid variability of brightness, goes retrograde immediately after conjunction, with the smaller disc, occulted, gets dimmer or changes color is said to be defeated.

[उक्त-विपरीत-लक्षण-सम्पन्नो जयगतो विनिर्देश्यः ।]{style="color:#0000ff;"}\
[विपुलः स्निग्धो द्युतिमान् दक्षिणदिक्स्थो +अपि जययुक्तः ॥]{style="color:#0000ff;"}\
If the planet appears endowed with the appearance opposite of the above-described it is deemed the victor. So also that which appears bigger, smooth in the motion or brighter is considered the winner even if stationed to the south.

[द्वाव् अपि मयूख-युक्तौ विपुलौ स्निग्धौ समागमे भवतः ।]{style="color:#0000ff;"}\
[तत्र +अन्योन्यं प्रीतिर् विपरीताव् आत्मपक्षघ्नौ ॥]{style="color:#0000ff;"}\
If both are endowed with bright rays, growing larger and smooth moving it becomes a समागम conjunction. There is \[consequentially] a conciliation between the objects associated with the two planets; if it is the opposite (i.e. both are small, growing dim, etc) both the associated objects will be destroyed.

[युद्धं समागमो वा यद्य् अव्यक्तौ स्वलक्षणैर् भवतः ।]{style="color:#0000ff;"}\
[भुवि भूभृताम् अपि तथा फलम् अव्यक्तं विनिर्देश्यम् ॥]{style="color:#0000ff;"}\
In cases where it the characteristics are not clear as to whether the conjunction of two planets is in a yuddha or a समागम conjunction, it is likewise unclear as to what the fruits will be for the rulers.

Thus, वराहमिहिर describes the general omenology of planetary conjunctions as per hoary Hindu tradition. This year is marked by a remarkable conjunction of Jupiter and Saturn that will peak as per geocentric coordinates on the day of the winter solstice (December 21, 2020; Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/12/cdc_conjunction.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


This has caused tremendous fear and excitement among those with a belief in such omenology. Even as we were examining the conjunction for purely astronomical reasons, we received one such call from a believer. Hence, we looked up वराहमिहिर to see what he has to say. Notably, this conjunction on Dec 21 is the closest since the famous Keplerian conjunction of 1623 CE. Thus, it is definitely an अंशुमर्दन and nearly an ullekha. From the above, we can see that, as per tradition, it is a grahayuddha of the अंशुमर्दन type between two paura planets, which prognosticates death from weapons, disease, or famine. Further, it is evident that Jupiter is to the south and it reduces in magnitude as it emerges from the conjunction. So as per the Hindu typology of conjunctions, it is defeated by Saturn (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/12/cdc_conjunction4.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 2

वराहमिहिर further provides specific omenology for the defeat of Jupiter by Saturn (BS 17.19):

[भौमेन हते जीवे मध्यो देशो नरेश्वरा गावः ।]{style="color:#0000ff;"}\
[सौरेण च+अर्जुनायन-वसाति-यौधेय-शिबि-विप्राः ॥]{style="color:#0000ff;"}\
If Jupiter is beaten by Mars, मध्यदेश region, kings and cows suffer. \[When beaten by] Saturn, the अर्जुनायन, वसाति, Yaudheya, शिबि \[peoples] and the ब्राह्मण-s suffer. \[It is noteworthy that one of the used words for Jupiter in the last 2000 years is Jiva. This word is not encountered in the earlier layers of the Sanskrit tradition. It is a loan from Greek, the vocative declension of Zeus, and is one of the marks of the influence of Greek astrology (e.g. the पौलीश-सिद्धान्त and Yavana-जातकम्) on its Indian counterpart.]

It was that latter prognostication that caused the fear in our interlocutor. These conjunctions of Jupiter and Saturn have an interesting geometric feature: the great trigon. For superior planets one can approximately calculate the frequency of conjunctions thus: Let  $p_1, p_2$  be the periods of revolution of the two planets and  $p_2 > p_1$ . Then  $\tfrac{360^\circ}{p_1}, \tfrac{360^\circ}{p_2}$  will be the mean angular speeds of the two planets respectively. The difference in their speeds would be:

 $$\dfrac{360^\circ(p_2-p_1)}{p_1p_2}$$ 

Hence, the time the faster planet will take relative to the slower planet to complete one revolution (i.e. catch up with it again) will be:

 $$p_c=\dfrac{360^\circ}{\dfrac{360^\circ(p_2-p_1)}{p_1p_2}} = \dfrac{p_1 p_2}{p_2-p_1}$$ 

This  $p_c$  will be the duration between successive conjunctions. The period of Jupiter is 4331 days of Saturn is 10747 days. Hence,  $p_c= 7254.56 \; \mathrm{days} = 19.86245\; \mathrm{years}$ . Thus, the Jupiter-Saturn conjunctions will repeat approximately every 20 years. One can see that the next two conjunctions will occur with respect to the original one at:

 $$\left (p_c \dfrac{360^\circ}{4331} \right ) \mod 360 \equiv 243.0112^\circ; \left (2p_c \dfrac{360^\circ}{4331} \right ) \mod 360 \equiv 126.0224^\circ$$ 

Thus, the three successive conjunctions will trace out an approximate equilateral triangle on the ecliptic circle --- the great trigon. This trigon caught the attention of the great German astrologer [Johannes Kepler](https://manasataramgini.wordpress.com/2005/08/14/johannes-kepler/), the father of the modern planetary theory in the Occident. He described this in his work the "De Stella Nova in Pede Serpentarii" that announced the discovery of his famous supernova. Since the successive trigons are not exactly aligned, they will progress along the ecliptic (Figure 3). We see that after 40 successive conjunctions it occurs very close to the original conjunction  $\approx 0.4488778^\circ$  (Figure 3). This amounts to about 290182.4 days (794.4979 years). This is the roughly 800 cycle that Kepler was excited about and thought that he was in the 8th such cycle since the creation of the world (being conditioned by one of the West Asian diseases of the mind).

![jupiter_saturn](https://manasataramgini.wordpress.com/wp-content/uploads/2020/12/jupiter_saturn.png){width="75%"}\
Figure 3. Progression of the trigons in the cycle of 40

The above formula can also be used to calculate the successive oppositions (when the configuration is Sun--Earth--superior planet) or inferior conjunctions (Earth--inferior planet--Sun) or superior conjunctions (Earth-Sun-inferior planet). We those obtain other interesting patterns. One such is successive oppositions of Jupiter which happen every 398.878 days. Thus, they nearly inscribe a hendecagon on the ecliptic circle (Figure 4).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/12/jupiter_hendecagon.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad Successive oppositions of Jupiter
```{=latex}
\end{center}
```


Now the successive inferior or superior conjunctions of Venus happen every 583.9578 days. Thus, these successive events trace out a near pentagonal star (Figure 5). This comes from the fact the ratio of the orbital periods of Earth to Venus is nearly  $\tfrac{13}{8}$  which is a convergent of the Golden Ratio.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/12/venus_pentagon.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad Successive conjunctions of Venus
```{=latex}
\end{center}
```


Beyond this, we may also note that the successive oppositions of Saturn nearly inscribe a 28-sided polygon (roughly corresponding to one per नक्षत्र) whereas those of Mars nearly inscribe a polygon of half that number (14 sides). The minor planet Ceres nearly inscribes an 18-sided star in the ecliptic circle (Figure 6).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/12/ceres.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad Successive oppositions of Ceres
```{=latex}
\end{center}
```


