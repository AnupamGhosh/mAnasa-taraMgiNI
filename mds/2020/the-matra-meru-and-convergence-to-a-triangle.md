
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The मात्रा-meru and convergence to a triangle](https://manasataramgini.wordpress.com/2020/01/19/the-matra-meru-and-convergence-to-a-triangle/){rel="bookmark"} {#the-मतर-meru-and-convergence-to-a-triangle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 19, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/01/19/the-matra-meru-and-convergence-to-a-triangle/ "6:28 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

What is presented below will be elementary for someone with even just the mastery of secondary school mathematics. Nevertheless, even simple stuff might present points of interest to people who see beauty in such things. Consider the following question:

Given the first 2 terms  $0 \le x_1, x_2 \le 1$ , what will be the behavior of the sequence defined by the recursive relationship:

 $$x_{n+1}=x_n\sqrt{1-x_{n-1}^2}+x_{n-1}\sqrt{1-x_n^2}$$ 

*Answer:* It will converge to a cycle of length 3, where  $x_n, x_{n+1}, x_{n+2}$  will be the sines of the 3 angles of a right or an acute triangle. Further, let  $M$  be the well-known मात्रा-meru sequence: **1, 1, 2, 3, 5, 8, 13, 21...**, then:

 $x_n=\sin\left(M\lbrack n\rbrack \arcsin\left(x_1\right)+M\lbrack n+1\rbrack \arcsin\left(x_2\right) \right)$ ;\
 $x_{n+1}=\sin\left(M\lbrack n+1\rbrack \arcsin\left(x_1\right)+M\lbrack n+2\rbrack \arcsin\left(x_2\right) \right)$ ;

 $$x_{n+2}=\sin\left(\pi-M\lbrack n+2\rbrack \arcsin\left(x_1\right)+M\lbrack n+3\rbrack \arcsin\left(x_2\right) \right)$$ 

Where  $M\lbrack n+1\rbrack \arcsin\left(x_1\right)+M\lbrack n+2\rbrack \arcsin\left(x_2\right)$  is the largest such angle that is  $\le \tfrac{\pi}{2}$ 

This can be easily proved thus:


 1.  Since  $0 \le x_1, x_2 \le 1$ , we can write  $x_1=\sin(A), x_2=\sin(B)$ .


 2.  Thus, given the recursive relationship the next term becomes,

 $$x_3=\sin(A)\cos(B)+\sin(B)\cos(A)=\sin(A+B)$$ 

 3.  Continuing this way, we can write,\
 $x_4=\sin(A+2B); \; x_5=\sin(2A+3B); \; x_6= \sin(3A+5B)$ .

We notice the multiplicands of  $A, B$  are the successive terms of the मात्रा-meru sequence. Thus,  $x_n=\sin\left(M\lbrack k\rbrack A+M\lbrack k+1\rbrack B \right)$ .


 4.  This will continue till  $M\lbrack k\rbrack A+M\lbrack k+1\rbrack B$  comes closest to  $\tfrac{\pi}{2}$ . Then the next term  $M\lbrack k+1\rbrack A+M\lbrack k+2\rbrack B \ge \tfrac{\pi}{2}$ . But due the symmetry of the sine function,

 $\sin(M\lbrack k+1\rbrack A+M\lbrack k+2\rbrack B)=\sin(\pi-M\lbrack k+1\rbrack A+M\lbrack k+2\rbrack B)$ .

Since,  $M\lbrack k+1\rbrack A+M\lbrack k+2\rbrack B=M\lbrack k-1\rbrack A+M\lbrack k\rbrack B+M\lbrack k\rbrack A+M\lbrack k+1\rbrack B$ ,

we get  $M\lbrack k-1\rbrack A+M\lbrack k\rbrack B+M\lbrack k\rbrack A+M\lbrack k+1\rbrack B+\pi-M\lbrack k+1\rbrack A+M\lbrack k+2\rbrack B = \pi$ .

Thus, at this stage the three successive terms  $x_n, x_{n+1}, x_{n+2}$  are sines of the 3 angles of an acute or right triangle and they will settle into a cycle of those 3 values  $_{...\blacksquare}$ 

Hence, the above recursive relationship results in any pair of  $x_1, x_2$  converging to the 3 sines of an acute or right triangle. As a corollary if you start with  $x_1, x_2$  which are already sines of an acute or right triangle then you stay on that triangle. Let us consider some special cases below (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/01/contri_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad The angles are given in degrees for ease of representation
```{=latex}
\end{center}
```


When  $x_1=x_2=\tfrac{\sqrt{3}}{2}$  then the values are on an equilateral triangle and the iterates remain fixed on that triangle (Figure 1, panel 1).

When  $x_1=\tfrac{3}{4}; \; x_2=\tfrac{4}{5}$  then the values are on the 3-4-5 right triangle and the iterates remain fixed on that triangle (Figure 1, panel 2).

When  $x_1=\tfrac{1}{\sqrt{2}}; \; x_2=\tfrac{1}{\sqrt{2}}$  then the values are on the half-square right triangle and the iterates remain fixed on that triangle (Figure 1, panel 3).

When  $x_1=x_2=\tfrac{1}{2}$  then they are not an acute or right triangle. However, within one iteration the iterates converge to the sines of a right triangle, namely the  $30^\circ-60^\circ-90^\circ \; \triangle$  (Figure 1, panel 4).

It is easy to see that if  $\tfrac{1}{\sqrt{2}} \ge x_1=x_2 \le 1$  then they are on an isosceles triangle and remain on that. However, if  $\arcsin(x_1)+\arcsin(x_2) < \tfrac{\pi}{2}$  then can we converge to an isosceles acute triangle? This happens in special cases which can be determined by solving an equation. In order to do so we shall take  $x_2=x; \; x_1=k-x; \; k< \sqrt{2}$ . From the above proof the successive angles corresponding to the iterates of  $x_n$  are:

 $$\arcsin(k-x); \; \arcsin(x); \arcsin(k-x) + \arcsin(x); \; \arcsin(k-x) + 2\arcsin(x); \; 2\arcsin(k-x) + 3\arcsin(x); \; 3\arcsin(k-x) + 5\arcsin(x)...$$ 

Thus, we have to look for real solutions of the equations such as:

 $$\arcsin(k-x)=\pi - (\arcsin(k-x)+\arcsin(x))$$ 

 $$\arcsin(x) = \pi -(\arcsin(k-x) + \arcsin(x)$$ 

 $$\arcsin(x) = \pi -(\arcsin(k-x) + 2\arcsin(x)) ...$$ 

Let consider the example of  $k= 1$ : with either of the first two equations we get degenerate triangles (e.g.  $x_1=0, x_2=1, x_3=1$ ). However, if we instead take  $x_1=\tfrac{1}{m}, x_2=\tfrac{m-1}{m}$  for some large  $m$  we get near-isosceles triangles (Figure 1, panel 5).

The one equation with a real solution for  $k=1$ , which gives a unique isosceles triangle, is seen when:

 $$\arcsin(1-x) +3 \arcsin(x) = \pi, \; x \approx 0.83756543528332$$ 

This  $x$  is the greatest root  $(r_1)$  of the cubic equation  $4x^3-4x+1=0$ . Thus,  $x_1=1-r_1, x_2= r_1$  yields an isosceles triangle with its equal angles  $\arcsin(r_1) \approx 56.88^\circ$  (Figure 1, panel 6).

Next we shall consider the evolution of certain special sequences of triangles. The first is where  $x_1, x_2$  are constituted by successive terms of the मात्रा-meru sequence (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/01/contri_fig2_meru.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Here, the triangles start with the  $30^\circ-60^\circ-90^\circ \; \triangle$  and converge to a unique scalene triangle with angles  $\arcsin\left(\tfrac{1}{\phi}\right) - \arcsin\left(\tfrac{3\phi-1}{2\phi+1}\right) - \arcsin\left(\tfrac{2\sqrt{\phi}}{\phi+1}\right) \approx 38.17^\circ - 65.48^\circ - 76.35^\circ$ , where  $\phi= \tfrac{1+\sqrt{5}}{2}$  is the Golden ratio.

The last panel here shows an interesting numeric coincidence. If you start with  $x_1=r_1, x_2=1-r_1$  (see above for  $r_1$ ) you converge to a triangle close to that emerging from the मात्रा-meru sequence. Is there more to this than the coincidence of values?

Finally, let us consider 2 other special triangles that emerge as convergents for 2 related types of operations based on the मात्रा-meru sequence (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/01/contri_fig3_meru_dens.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


The first 2 rows (in light green) show triangles emerging from  $x_1=\tfrac{1}{M\lbrack k\rbrack }, x_2=\tfrac{1}{M\lbrack k+1\rbrack }$ , where  $k=2, 3, 4...$ . Here again, we start with a  $30^\circ-60^\circ-90^\circ \; \triangle$  and converge to a triangle of the form  $\approx 70.82^\circ - 65.41^\circ - 43.77^\circ$ .

The second 2 rows (in yellow) show triangles emerging from  $x_1=\tfrac{1}{M\lbrack k\rbrack }, x_2=\tfrac{1}{M\lbrack k\rbrack }$ , where  $k=3, 4, 5...$ . These converge to a triangle of the form  $\approx 87.29^\circ - 57.30^\circ - 35.41^\circ$ .

