
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Notes on miscellaneous ब्राह्मण passages from the Yajurveda](https://manasataramgini.wordpress.com/2020/04/26/notes-on-miscellaneous-brahma%e1%b9%87a-passages-from-the-yajurveda/){rel="bookmark"} {#notes-on-miscellaneous-बरहमण-passages-from-the-yajurveda .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 26, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/04/26/notes-on-miscellaneous-brahma%e1%b9%87a-passages-from-the-yajurveda/ "3:47 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The उपस्थान ritual is performed to let the sacrificial fire remain in residence after the primary oblations are complete. In the triple-fire श्रौत rite this is done at the आहवनीय altar with several incantations specified in the संहिता-s of the Yajurveda, e.g., तैत्तिरीय-संहिता 1.5.5. Among those incantations the following is recited:

[इन्धानास् त्वा शतं हिमा द्युमन्तः समिधीमहि ।]{style="color: #0000ff"}\
[वयस्वन्तो वस्यस्कृतं यशस्वन्तो यशस्कृतं ॥]{style="color: #0000ff"}\
[सुवीरासो अदाभ्यम् अग्ने सपत्न-दम्भनं वर्षिष्ठे अधि नाके ॥]{style="color: #0000ff"}

Kindling you, may we kindle \[you] in luster through a hundred snows \[winters].\
The strong one the maker of strength, the famous one the maker of fame.\
With good heroes, the undeceived, O Agni, the deceiver of foes in the highest heaven.

The तैत्तिरीय-संहिता furnishes the following ब्राह्मण passage for the above mantra-s (In TS 1.5.7):

[इन्धानास् त्वा शतं हिमा इत्य् आह । शतायुः पुरुषः शतेन्द्रिय आयुष्येवेन्द्रिये प्रति तिष्ठति । एषा वै सूर्मी कर्णकावती । एतया ह स्म वै देवा असुराणां शततर्हां स्तृंहन्ति । यद् एतया समिधम् आ दधाति वज्रम् एवैतच् छतघीं यजमानो भ्रातृव्याय प्र हरति स्तृत्या अछंबट्कारम् ।]{style="color: #0000ff"}

He recites: "may we kindle \[you] through a hundred snows \[winters]". He lives a hundred years and has a hundred senses. Verily he is stable in life and senses. This is a red-hot tube with cutting edges. By this indeed the deva-s have struck hundreds of piercings on the asura-s. When he takes up the fire-stick with this \[incantation], the ritualist hurls the शतघ्नी like the vajra for his enemy's overthrow without fail.

The text is notable in being, what to our knowledge, the earliest mention and description of the weapon frequently encountered in the इतिहास-s and पुराण-s and also found in the अर्थशास्त्र of विष्णुगुप्त चाणक्य. The weapon is described as being a सूर्मी, which tradition holds to be a red-hot hollow cylinder. It is described as being कर्णकावति, i.e., with prongs or cutting edges. As its name goes, शतघ्नी, and from the description of the attack by the deva-s on the asura-s in this ब्राह्मण, it is clear that it was intended to be a weapon of "mass destruction". Thus, it is reminiscent of what the ब्रःमण-s are described as having deployed on the Egyptian Herakles by Flavius Philostratus in his biography of Apollonius of Tyna. It is in this context that he says that if Alexander had penetrated beyond the विपाशा river he might have not been able to take the fort of Indians even if he had 10000 Achilles-es and 30000 Ajax-es with him. Given that the इतिहास-s mention it as being used as a mechanical device for the defense of forts it is likely that it was indeed some form a siege engine of the early Arya-s.

For the same incantation the मैत्रायणी-संहिता (in MS 1.5.2.8) gives a rather different explanation:

[मनोर् ह वै दश जाया आसन् । दशपुत्रा । नवपुत्रा । अष्टपुत्रा । सप्तपुत्रा । षट्पुत्रा । पञ्चपुत्रा । चतुष्पुत्रा । त्रिपुत्रा । द्विपुत्रा । एकपुत्रा । ये नव आसन् तान् एक उपसमक्रामत् । ये अष्टौ तान् द्वौ । ये सप्त तान् त्रयः । ये षट् तान् चत्वारः । अथ वै पञ्च एव पञ्च आसन् । ता इमाः पञ्च । दशत इमान् पञ्च निरभजन् । यद् एव किं च मनोः स्वम् आसीत् । तस्मात् ते वै मनुम् एव उपाधावन् । मनौ अनाथन्त । तेभ्य एताः समिधः प्रायच्छत् । ताभिर् वै ते तान् निरादहन् । ताभिर् एनान् पराभावयन् । परा पाप्मानं भातृव्यं भावयति य एवं विद्वान् एताः समिध आदधाति ।]{style="color: #0000ff"}

Manu indeed had 10 wives. \[Respectively,] with 10 sons, with 9 sons, with 8 sons, with 7 sons, with 6 sons, with 5 sons, with 4 sons, with 3 sons, with 2 sons, with 1 son. The 1 son \[of the wife with 1] joined with the 9 sons \[of the wife with 9]; Those of the she with 2 sons with those of she with 8 sons; 3 sons with 7 sons; 4 sons with 6 sons. The 5 \[of she with] 5 remained that. They were just 5. The 10s (i.e. 50) dispossessed these 5. Now Manu had some \[possession] of his own. Therefore, those [5] indeed ran to Manu himself. The sought protection in Manu. He gave each these fire-sticks. With those \[fire-sticks] the [5 sons] indeed burnt up those [50]. They defeated those [50]. He who knows this and offers the fire-sticks defeats his evil enemies.

This ब्राह्मण is notable for more than one thing: We know that the Hindus were aware of the formula for the sum of natural numbers  $1..n$  from the famous citation of the sage शाकपूणि in the बृहद्देवता regarding the hidden आग्नेय ऋक्-s associated with the "जातवेदसे सुनवाम..." सूक्त (ऋग्वेद 1.99). There he gives the sum of 1+2+3...+1000=500500. This ब्राह्मण gives the derivation of the formula for the sum of such a series cryptically and is reminiscent of how Carl Gauss said he computed such a sum as kid. Both ब्राह्मण-s from the two YV संहिता-s importantly play on the power of the word शत (100) in the incantation. Once Manu gives his 5 dispossessed sons the samidh-s they are said to have beaten their 50 rival half-brothers. How does this come about? Again, it seems to rely on an arithmetic symbolism. When he gave them the samidh-s, as the TS states that samidh makes you शतेन्द्रिय. Thus, it seems to imply that the 5 become  $5\times 100 > 50$  thereby beating their half-brothers.

That apart it also provides a clue regarding the origin of the story formula of the महाभरत. We have remarked several times before that when old history was composed it was poured in the bottles of preexisting myth and thereby took their shape. Thus, when we encounter formulaic numbers like 100 (Kaurava-s) and 5 (पाण्डव-s) one is immediately alerted to the use of a preexisting mythic frame. Indeed, such a formula related to the count and the differentiation of the पाण्डु-s extends beyond the Indo-Iranian horizon. In the Secret History of the Chingizid Mongols we hear the tale of their legendary ancestress Alan Qo'a bearing 5 sons. Notably, there too 2 of her sons are of different father from the from other 3. A birth through a divine non-human father is implied for 3 of them (all 5 in the case of the पाण्डु-s). Further, Alan Qo'a specifically instructs her 5 sons to be united despite their being half-brothers. So, there is a hint of the need of unity in face of conflict fraternal conflict. These motifs echo what are seen far away in time and space in the महाभारत of the Hindus. Hence, we were curious as to whether we might find earlier echoes of the महाभारत story formula in earlier Indo-आर्यन् tradition. We believe that the above ब्राह्मण is indeed the "essentialized" motif of fraternal conflict featuring the number of 5 against 50 that became 5 versus  $50 \times 2$  in the महाभारत where the party of 5 prevailed in the end despite being initially dispossessed as in this case.

Now we move on to an entirely different theme from a ब्राह्मण passage from the कपिष्ठल-कठ-संहित 35.8.

[सा एषा अनुष्टुप् । तस्याः सप्ताक्षरम् एकम् पादम् अष्टाक्षराणि त्रीणि । तेषां सप्तानां यानि त्रीणि तान्य् अष्टाव् उपयन्ति । तन्य् एकादश । सा त्रिष्टुप् । यानि चत्वारि तान्य् अष्टाव् उपयन्ति । तानि द्वादश । सा जगती । यान्य् अष्टौ सा गायत्री ।परा पाप्मानं भातृव्यं भावयति य एवं विद्वान् एताः समिध आदधाति ।]{style="color: #0000ff"}

There was this अनुष्टुभ्. 1 of its feet had 7 syllables, the \[remaining] 3 had 8. Of the \[foot] which had 7, 3 of them went to the 8. That made 11. This was the त्रिष्टुभ्. The \[remaining] which were 4 \[of that foot] went to the 8. That made 12. This was the जगती. The \[remaining] feet which had 8 \[syllables became] the गायत्री (कपिष्टल-कठ-संहिता 35.8).

The model being proposed here is something comparable to what we see in evolution of nucleic acid and protein sequences. The original अनुष्टुभ् is conceived thus:

 $$\-\-\-\-\-\-\--|\-\-\-\-\-\-\--$$ 

 $$\-\-\-\-\-\-\--|\-\-\-\-\-\--$$ 

There is fission and fusion with deletion shown by []:\
 $\-\-\-\-\-\-\--|\-\--\lbrack \-\-\--\rbrack \; \rightarrow\; \-\-\-\-\-\-\-\-\-\--$  (1 foot of त्रिष्टुभ्) or\
 $\-\-\-\-\-\-\--|\lbrack \-\--\rbrack \-\-\-- \; \rightarrow\; \-\-\-\-\-\-\-\-\-\-\--$  (1 foot of जगती)

What remains is:

 $$\-\-\-\-\-\-\--|\-\-\-\-\-\-\--$$ 

 $\-\-\-\-\-\-\--$  (गायत्री)

Of course, no surviving regular अनुष्टुभ् from the earliest Vedic tradition has a 7-syllabled final foot. So, is this merely a case of ब्राह्मण numerology or does it reflect some early development in prosody? Now the existence of a basic 8-syllabled unit is something mentioned early in Vedic tradition e.g., by Kurusuti काण्व:

[वाचम् अष्टापदीम् अहं नवस्रक्तिम् ऋतस्पृशम् ।]{style="color: #0000ff"}\
[इन्द्रात् परि तन्वम् ममे ॥(ऋग्वेद8.076.12)]{style="color: #000080"}

An eight-\[syllable]-footed, nine-cornered utterance, touching ऋत\
I have measured out its body from Indra.

Being a गायत्री with 3 feet of  $8$  syllables each, the ऋक् is testimony for what it states. Further, what is the "nonagonal" form of the utterance? The सूक्त in question is composed as sets of तृच-s of गायत्री-s. Thus, each तृच has  $3 \times 3$  8-syllabled feet giving us the navasrakti structure the ऋषि mentions. Thus, we can be sure that 8 was definitely a recognized unit of the foot from early one. Further, the very term पाद as a metrical foot has an Iranian cognate pa $\delta$ a used in the same sense. Further, πούς (poús) as used by Aristophanes suggests that this was indeed the originally the Greek cognate of the Vedic पाद. Thus, a comparable metric foot was recognized early in Indo-European. By comparing Sanskrit, Avestan and Greek, Martin West proposed that there are reconstructible proto-feet of Indo-European prosody with 8 syllables and other versions of them with 6 and 7 syllables. If this were true, then the ब्राह्मण might be recalling or reconstructing an origin mechanism wherein the shortened 7-syllabled element fragments and merges with adjacent 8-syllabled feet to give rise to the त्रिष्टुभ् and जगती.

In any case, this irregular hypothetical अनुष्टुभ् is not the standard अनुष्टुभ् of Sanskrit which since the earliest Vedic record came to be of  $4 \times 8$  syllables. Indeed, this form of the अनुष्टुभ् was the basis of a child's mathematical problem presented by भास्कर-II in his लीलावतीː

[समानाम् अर्ध-तुल्यानाम् विषमाणाम् पृथक् पृथक् ।]{style="color: #0000ff"}\
[वृत्तानाम् वद मे संख्याम् अनुष्टुभ्-छन्दसि द्रुतम् ॥]{style="color: #0000ff"}

Tell me quickly what are the counts of the variations of अनुष्टुभ् meter with:\
(i) same \[configuration of feet]\
(ii) half-equivalent \[feet]\
(iii) dissimilar \[feet]

Each syllable can take a short (0) or a long state (1) . If every foot has the same configuration of 0,1 then we only have to look for combinations of one 8-footed element. If it has half-equivalent feet the two  $2 \times 8$  syllable configurations are the same. But we have to remove all instances where all 4 feet are same for a strict definition. Now if all feet are dissimilar then we have 32 syllables available, but we have to remove all those where either all or a pair of feet are the same. Thus, we get the answers as:\
(i)  $2^8=256$ \
(ii)  $2^{16}-2^8= 65536-256=65280$ \
(iii)  $2^{32}-2^{16}=4294967296-65536=4294901760$ 

Interestingly, these numbers make an appearance in modern computing e.g., as in 16-bit and 32 bit computing and the largest 32 bit number.

