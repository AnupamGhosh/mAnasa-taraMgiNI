
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The अश्विन्-s and Rudra](https://manasataramgini.wordpress.com/2020/01/12/the-asvin-s-and-rudra/){rel="bookmark"} {#the-अशवन-s-and-rudra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 12, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/01/12/the-asvin-s-and-rudra/ "6:19 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The twin अश्विन्-s and Rudra are both Indo-Aryan reflexes of two deity-classes which can be reconstructed as likely being present in the Proto-Indo-European religion. Both are likely to have even deeper roots going back to even earlier religious traditions across a wide swath of humanity. Indeed, the divine twins feature even outside the IE religions. At the face of it it is not obvious if these two deity classes show any special links. For example, with Rudra as a focus we can use the ऋग्वेद to sample his association with other deities. One proxy for association can be how often Rudra is mentioned with another deity in the same पाद or hemistich of a ऋक्. Below is a table showing his co-occurrences by this metric.

***Table 1. The association of various देवता-s with Rudra in the RV***

  **देवता**     **#**

  - --------- --------
  Marut-s          21
  Vasu-s           18
  आदित्य-s          13
  Agni             10
  ऋभु-s              7
  Indra             6
  अश्विन्-s           6
  Soma              6
  वरुण               5
  Mitra             5
  Bhaga             5
  पुषण्               4
  विष्णु              3
  बृहस्पति            2
  सरस्वती            2
  त्वष्टृ              2
  Aryaman           1
  सवितृ              1
  Aditi             1
  वायु               1

Not surprisingly, he is most closely associated with his sons the Marut-s who are often referred to as the plurality of Rudra-s. The other close associations come from the fact that the Rudra-s are mentioned as a group of gods along side to two other big groups of gods: the Vasu-s and the आदित्य-s. He further shows notable associations with one of the archetypal deities of the Vasu-group, Agni with who he shares a duality. Beyond that his mentions along with the अश्विन्-s is no more frequent than with Soma, Indra or the ऋभु-s (the divine craftsmen whose masters are said to be the Rudra-s: RV 8.7.12). That said a closer look reveals a deeper link between Rudra and the अश्विन्-s.

[उत त्या मे रौद्राव् अर्चिमन्ता]{style="color: #0000ff"}\
[नासत्याव् इन्द्र गूर्तये यजध्यै ।]{style="color: #0000ff"}\
[मनुष्वद् वृक्तबर्हिषे रराणा]{style="color: #0000ff"}\
[मन्दू हितप्रयसा विक्षु यज्यू ॥]{style="color: #0000ff"} RV 10.61.15

Also these two sons of Rudra, the नासत्य-s \[worshiped] with ऋक्-s, are to be welcomed and made offerings by me, O Indra. The two who are liberal to him, who as Manu \[had done, invites them] to the woven grass (the twisted बर्हिष् in the vedi), the delightful \[twins] for whom pleasing offerings are made, the twins who seek the ritual among the people.

Here the ऋषि कक्षीवान् calls the अश्विन्-s the sons of Rudra in a manner similar to the Marut-s. Further, this सूक्त mentions our ancient ancestor च्यवान as the one who had measured out the vedi for the for the अश्विन्-s. च्यवान is mentioned in tradition as the one who instituted the rite where soma is offered to the अश्विन्-s. Indeed, in the above ऋक् it appears that कक्षीवान् seeks Indra permission for the same as there are indications from later narratives that Indra was not entirely on board with that. In any case, what is important for our current discussion is the relatively unambiguous link of the अश्विन्-s and Rudra in this ऋक्.

This is not the only instance the अश्विन्-s are called Rudra-s or the son-s or Rudra. Indeed, we encounter another such example elsewhere in the RV:

[ताव् इद् दोषा ता उषसि शुभस्]{style="color: #0000ff"}\
[पती ता यामन् रुद्रवर्तनी ।]{style="color: #0000ff"}\
[मा नो मर्ताय रिपवे वाजिनी-]{style="color: #0000ff"}\
[वसू परो रुद्राव् अति ख्यतम् ॥]{style="color: #0000ff"} RV 8.22.14

Just these two in the evening, these two auspicious lords at dawn, the two who follow the tracks of Rudra in the course. Don't look over and beyond us to a roguish mortal, O Rudra-s with booty-bearing mares.

In this ऋक् of Sobhari काण्व, they are not just called Rudra-s but also specific described as रुद्रवर्तनी. This word is of considerable interest in regard to this connection between these deities. It belongs to class of compounds of the form "x+vartani" that are found throughout the RV and used for different deities. The word vartani means wheel (typically of the chariot) or track of the chariot wheel. Thus, x+vartani compounds are usually interpreted as बहुव्रीहि-s. Below we list all the instances of such compounds in the RV along with the gods they denote and the number of occurrences of each case:

-   **Rudravartani**

  -    अश्विन्-s, 4
-   **हिरण्यवर्तनि**

  -    अश्विन्-s, 6

  -    सरस्वती, 1

  -    river goddess, 1
-   **Raghuvartani**

  -    अश्विन्-s, 1

  -    Soma, 1
-   **घृतवर्तनि**

  -    अश्विन्-s, 1
-   **वृजिन्वर्तनि**

  -    Agni, 1
-   **कृष्णवर्तनि**

  -    Agni, 1
-   **गायत्रवर्तनि**

  -    इन्द्राग्नी, 1
-   **Dvivartani**

  -    Agni, 1

It is immediately apparent that this class of compounds are special descriptors of the अश्विन्-s for 12 of the 19 occurrences of them are used for the अश्विन्-s. This is likely a special allusion related to the oft-mentioned speeding tricycle (tri-cakra) chariot of these gods. However, it should be noted that such compounds, while most frequently used for the अश्विन्-s are not limited to them; e.g. हिरण्य-vartani is use for at least 2-3 distinct deities (river goddess Sindhu could be a cryptic allusion to सरस्वती). Some of the usages, such as हिरण्य-vartani or Raghu-vartani can be simply interpreted as the ones with the golden wheels or one with swift wheels and appear to apply to other deities than the अश्विन्-s. Indeed, a related term "हिरण्य-cakra" is used for the actual Rudra-s, i.e. Marut-s (e.g. RV 1.88.5) or for that matter हिरण्य-ratha used for Indra (e.g. RV 1.30.16). Further, some might be even typical of other deities: e.g. कृष्ण-vartani (with black tracks: alluding to the smoke) and वृजिन-vartani (with curving tracks, alluding to the flames) are apt for Agni for whom they are used. The form dvi-vartana used Agni is simply indicative of two tracks left behind by the two chariot wheels.

However, of all these Rudra-vartani is specifically used only for the अश्विन्-s on multiple occasions and by multiple composers. Outside the RV it is found across all the Yajurveda texts (both शुक्ल and कृष्ण) specifically as an epithet of the अश्विन्-s in one of the सौत्रामणि incantations:\
[तद् अश्विना भिषजा रुद्रवर्तनी सरस्वती वयति पेशो अन्तरम् ।]{style="color: #0000ff"}\
[अस्थि मज्जानं मासरैः करोतरेण दधतो गवां त्वचि ॥]{style="color: #0000ff"}\
सरस्वती puts together (literally weaves) the muscles within and अश्विन्-s, the physicians, following on Rudra's track,\
place the marrow \[in] the bones \[as the]  the wort \[from] the fermented grain with a sieve on the ox hide.

This suggests that this term has a special connection with the अश्विन्-s. Drawing the cue from the more frequent हिरण्य-vartani, also used for the अश्विन्-s, Rudra-vartani has been tradition interpreted as either Rudra= ruddy or Rudra = fierce. Thus, the compound is understood as the अश्विन्-s with ruddy tracks, or those with fierce tracks --- perhaps as an allusion to their speeding chariot that is frequently seen in the RV, or as those who go along terrifying paths. Entirely, independently of our investigations, we learned that this line of reasoning was first explored in detail by the great patriot Aurobindo Ghose. But the key is the observation that this term is specific to the अश्विन्-s. Rudra is not used in the sense of "ruddy" elsewhere in the corpus. "Fierce or terrible tracks" would have implied the form raudra-vartani, which we do not ever encounter in this corpus. Further, ruddy horses or chariots are described by terms like [अरुणयुग्भिर् अश्वैः]{style="color: #0000ff"} (RV 6.65.2) and fierce chariots by terms like त्वेष-ratha for the Marut-s (RV 5.61.13; also perhaps a personal name of a Mitanni ruler among the Indo-Aryans of West Asia). Indeed, the ferocity of the Marut-s' chariots with ruddy horses are alluded to elsewhere too (e.g. RV 1.88.2) but the term Rudravartani is never applied to them even if it might be natural in this sense.

Thus, taken together with the instances where the अश्विन्-s are called Rudra-s or the sons of Rudra (see above and also below) we conclude that Rudra-वर्तनी specifically indicates the association of the twins with Rudra and means "the two who follow the track of Rudra". After we reached this conclusion, a search revealed that such a translation had been independently conceived by the German indologist Hermann Oldenberg. Notably, this link to Rudra is further strengthened by another instance where they are called both Rudra-s and हिरण्यवर्तनी:

[आ नो रत्नानि बिभ्रताव्]{style="color: #0000ff"}\
[अश्विना गच्छतं युवम् ।]{style="color: #0000ff"}\
[रुद्रा हिरण्यवर्तनी]{style="color: #0000ff"}\
[जुषाणा वाजिनीवसू]{style="color: #0000ff"}\
[माध्वी मम श्रुतं हवम् ॥]{style="color: #0000ff"}RV 5.75.3\
Bearing treasures to us, अश्विन्स्, come here, you two, O Rudra-s with golden wheels, with booty-bearing mares, being pleased, the holders of the honey-lore, hear my invocation.

We believe there are many dimensions to this connection:

 1.  The ancient name of the अश्विन्-s is Divo नपता. The twin sons of Dyaus. This is an equivalent of the name of their Greek cognates the Dioskouroi (the national deities of the Spartans), meaning the youths of Zeus (the cognate of Dyaus; Skt दिवः कुमारौ) or their Lithuanian cognates Dievo Suneliai (Sons of Dieva = Dyaus). Now Dyaus on occasion is identified with Asura Rudra in the RV:

[त्वम् अग्ने रुद्रो असुरो महो दिवः]{style="color: #0000ff"} (RV 2.1.6)\
You, O Agni, are Rudra the Asura of heaven (Dyaus)

[यथा रुद्रस्य सूनवो दिवो]{style="color: #0000ff"}\
[वशन्त्य् असुरस्य वेधसः ।]{style="color: #0000ff"} (RV 8.20.17)\
It shall be \[just] as they wish, the sons of Rudra, the Asura of heaven (Dyaus) are the wise ones.

And like in this case too:\
[indrota तुभ्यं tad dive tad रुद्राय स्वयशसे |]{style="color: #0000ff"} (RV 1.129.3)\
Indra, this is for you and that heaven (Dyaus), for that self-glorious Rudra.

The above indicate that there was an early Vedic tradition that identified Rudra with Dyaus, in which sense he was also seen as the [father of world](https://manasataramgini.wordpress.com/2008/11/09/the-chants-of-the-rudra-s-a-commentary-based-on-our-discussions/) by the भरद्वाज-s (RV 6.49.10). This, together with the appellation Divo नपाता for the अश्विन्-s, hints a parallel Vedic tradition which saw them as deities in the Rudra-class associated with the leader of that class Rudra, the great Asura of Dyaus. Their "Raudra nature" is clearly brought out in the ऋक् RV 10.93.7: [उत नो रुद्रा चिन् मृऴताम् अश्विना]{style="color: #0000ff"} : Also, though being Rudra-s, may the अश्विन्-s be merciful. This plea for mercy to them is comparable to that typically made to Rudra or the Marut-s. Consistent with this, they share their medical prowess with Rudra (as physicians of the gods; see सौत्रामणि incantation above) and even more tellingly also their knowledge of poisons with Rudra (RV 1.117.16: where they either kill the brood of विष्वाच् with poison or destroy the poison associated with the brood of विष्वाच्). Thus, across the RV, composers from different clans occasionally saw the अश्विन्-s as Rudra-s or Rudra's sons and allude to their Rudrian properties.


 2.  One of the notable aspects differentiating the early अथर्वण् tradition from the RV tradition with regard to Rudra is the use of the twin appellation भवा-Śarv[आ]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"} for the deity in the former. These names of Rudra also persist in the आध्वर्यव tradition preserved in the Yajurveda-s but the twinning is less prominent relative the AV tradition. Notably, in the celebrated मृगारेष्टि ritual the AV tradition features a सूक्त to the twin भवा-Śarv[आ,]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"} whereas in its place the KYV tradition has ऋक्-s to the अश्विन्-s. Further, in the incantations for the [Ś]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"}ūlagava ox sacrifice laid out in the शाङ्खायन-श्रौतसूत्र (4.20.1-2), [भवा-शर्व्]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"}आ are called the sons of Mah[आ]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"}deva.

[तस्य ते धनुर् हृदयम् मन इषवश् चक्षुर् विसर्गस् तम् त्वा तथा वेद नमस् ते अस्तु सोमस् त्वा अवतु मा मा हिंसीः । याव् अरण्ये पतयन्तौ वृकौ जम्बवन्तौ इव । महादेवस्य पुत्राभ्याम् भव-शर्वाभ्याम् नमः ॥]{style="color: #0000ff"}

The heart is your bow, the mind is your arrow, the eye is your shooting. Thus, we know you. Obeisance to you. May Soma protect him and may you never ever harm me. The two who roam around in the forest like wolves with jaws wide open; obeisance to the two sons of महादेव, Bhava and Śarv[a.]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"}

This points to two parallel streams within the early Vedic tradition which featured Rudra in singular form (apart from the plurality of the Rudra-class) as seen in the RV or in twin form of भवा-Śarv[आ]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"} as seen in the AV and the [Ś]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"}आṅkhआyana-श्रौतसूत्र (as the twin sons of Rudra). This suggests that the Rudra-class had an ancient intrinsic twin nature shared with the अश्विन्-s which lingers in the भवा-Śarv[आ]{style="display: inline !important;float: none;background-color: #ffffff;color: #2c3338;cursor: text;font-family: 'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size: 16px;font-style: normal;font-variant: normal;font-weight: 400;letter-spacing: normal;line-height: 1.2em;text-align: left;text-decoration: none;text-indent: 0;text-transform: none"} dyad. Notably, even in Greek tradition the deity of the Rudra-class, Apollo is born with twin (albeit female), Artemis. While the RV अश्विन्-s are identical twins in character, sometimes, in the IE world one sees some differentiation of the the twins with one of them associated with healing and animal-husbandry and the other with warfare. For example, among the Yavana-s, one of the pair, Castor is a horse-trainer while the other one Polydeuces is a boxer. A similar differentiation is perhaps reflected in the twin Rudra-s, with the name Bhava indicating welfare and health, and the and शर्व meaning and archer and indicating the warrior nature of the second twin. Thus, it is likely that Bhava and शर्व were part of the parallel Vedic tradition where they played the role of the अश्विन्-s as twin Rudra-s.


 3.  A later reflex of this twinning in the Rudra-class appears to have emerged via developments in the form of the कौमार tradition emerging in the Indo-Iranian borderlands. There we see the dual form of the god कुमार as Skanda-विशाख. This dyad is earliest attested in the अथर्ववेदीय-Skanda-याग from the AV-परिशिष्ठ-s and has a prolonged presence in the Hindu tradition. We find an allusion to this in a simile used in the महाभारत when कृष्ण describes the twins, Nakula and Sahadeva, to his brother हलायुध:

[यौ तौ कुमाराव् इव कार्त्तिकेयौ]{style="color: #0000ff"}\
[द्वाव् अश्विनेयाव् इति मे प्रतर्कः ।]{style="color: #0000ff"}\
[मुक्ता हि तस्माज् जतुवेश्मदाहान्]{style="color: #0000ff"}\
[मया श्रुताः पाण्डुसुताः पृथा च ॥ (म्भ् "च्रितिचल्" १।१८०।२१)]{style="color: #0000ff"}\
The two who are youths, like twin कार्त्तिकेय-s,\
are the sons of the two अश्विन्-s; so I infer.\
It has been heard by me that the sons of पाण्डु and पृथा\
have indeed escaped from the burning of the wax-house.

Here the sons of the अश्विन्-s (their earthly manifestations) are explicitly connected to the twin कार्त्तिकेय-s --- an allusion to the twin deities, Skanda and विशख. The grammarian पतञ्जलि mentions metal images of Rudra along with the Skanda-विशाख dyad in the Mauryan age (\~322-185 BCE). The persistence of this tradition is illustrated by a much later attestation of this dyad, evidently drawn from a now lost early tradition, seen in the कालिका-पुराण with a predominantly East Indian locus:

[दहनो 'पि तथा काले प्राप्ते गङ्गोदरे स्वयम् ।]{style="color: #0000ff"}\
[रेतः सङ्क्रामयामास शांभवं स्वर्ण-सन्निभम् ॥]{style="color: #0000ff"}\
Agni himself, in due course of time, transmitted the semen of Rudra, which shone like gold, to the womb of गङ्गा.

[सा तेन रेतसा देवी सर्व-लक्षण-संयुतं ।]{style="color: #0000ff"}\
[पूर्ण-काले 'थ सुषुवे पुत्र-युग्मं मनोहरम् ॥]{style="color: #0000ff"}\
Then by that semen the goddess, upon completion of pregnancy, gave birth to charming twin sons endowed with all \[good] features.

[एकः स्कन्दो विशाखाख्यो द्वितीयश् चारु-रूप-धृक् ।]{style="color: #0000ff"}\
[शक्ति-द्वय-धरौ द्वौ तौ तेजः कान्ति-विवर्धितौ॥]{style="color: #0000ff"} KP 46.82-84\
One was Skanda and the second bearing a beautiful form was known as विशाख. The two held a spear each and two shone with their radiance.

Thus, we see Rudra siring the twin कौमार deities Skanda and विशाख who are described as bearing spears much like the depiction of the spear-bearing Dioscuri in the yavana tradition. Interestingly, their beauty is specifically described much like that of the अश्विन्-s in Vedic tradition. Further, the कालिका-पुराण recommends the worship of this dual कौमार form for the षष्ठी night:

[रात्रौ स्कन्द-विशाखस्य कृत्वा पिष्ट-पुत्रिकाम् ।]{style="color: #0000ff"}\
[पुजयेच् छत्रुनाशाय दुर्गायाः प्रीयते तथा ॥]{style="color: #0000ff"} KP 60.50\
In the night having made images of Skanda and विशाख from flour one should worship them for the destruction of enemies and for pleasing दुर्गा.

In archaeological terms we find depictions of the Skanda-विशाख dyad on कुषाण coins and also कुषाण age images from a lost कौमार shrine from the holy city Mathura (now housed in the collections of the Mathura museum). What is notable about their numismatic appearances is their resemblance to the twin gods on Greek Dioscuri coins. Thus, like Bhava-शर्व in a parallel Vedic tradition, Skanda-विशाख are likely developments of the ancient Raudra twins in a Para-Vedic tradition that then entered the Indo-Aryan mainstream.


 4.  Finally, it is plausible that the term रुद्रवर्तनी and the association of the अश्विन्-s with Rudra have an astronomical significance. In the classic नक्षत्र system that developed by the time of the AV and the YV the अश्विन्-s are associated with the constellation of अश्वयुजौ which corresponds to part of Aries. However, the obvious constellation that resembles the divine twins is Gemini, which was recognized as the Dioscuri in the Greek tradition. While it was termed the constellation of Aditi, the आर्य-s too recognized the dual nature of the asterism Punarvasu made up two stars Castor and Pollux --- it is occasionally used in dual like: [पुनर्वसू नक्षत्रम् अदितिर् देवता ॥]{style="color: #0000ff"}. Early on we see the recognition of the twin nature in the statement that Aditi is two-headed in the Yajurveda (तैत्तिरीय संहिता in 1.2.4; शतपथ ब्राह्मण 3.2.4.16). Ironically, पाणिनि reinforces the dual nature with the सूत्र: [छन्दसि पुनर्वस्वोर् एकवचनम् ।]{style="color: #0000ff"}| PAA 1.2.61. In the Veda Punarvasu might be \[optionally take a] singular declension. The grammarians clarify that this is limited to the Veda while in common speech it is always dual indicating its twin nature. The only direct allusion to the asterism in RV (along with Revati) appears to be in RV 10.19.1 (by our ancestor च्यवान), which associates it with Agni and Soma in a cryptic hymn whose actual meaning has been hard to discern. However, a potential connection is seen in RV 10.39.11 where the अश्विन्-s are called रुद्रवर्तनी and explicitly linked Aditi --- pairing that is otherwise rather unusual:

[न तं राजानाव् अदिते कुतश् चन]{style="color: #0000ff"}\
[नांहो अश्नोति दुरितं नकिर् भयम् ।]{style="color: #0000ff"}\
[यम् अश्विना सुहवा रुद्रवर्तनी]{style="color: #0000ff"}\
[पुरोरथं कृणुथः पत्न्या सह ॥]{style="color: #0000ff"} RV 10.39.11\
From nowhere troubles nor evil nor fear reach him, along with his wife, for whom you two kings and Aditi prepare a chariot to be in the forefront. O अश्विन्स्, you who are easy to invoke, follow the track of Rudra.

We take this to mean that the अश्विन्-s here are associated with Aditi and thereby the asterism of Punarvasu. Now, Punarvasu rises after the constellation of Rudra, i.e., आर्द्र (corresponding to Sirius and the proximal bright stars of Canis Major). Thus, we can see the twins literally following the track of Rudra in the sky.

