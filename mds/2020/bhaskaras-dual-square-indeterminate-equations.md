
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [भास्कर's dual square indeterminate equations](https://manasataramgini.wordpress.com/2020/11/10/bhaskaras-dual-square-indeterminate-equations/){rel="bookmark"} {#भसकरs-dual-square-indeterminate-equations .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 10, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/11/10/bhaskaras-dual-square-indeterminate-equations/ "12:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[PDF for convenient reading](https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_indeterminate_square.pdf)

**![squares_bhAskara_Fig1](https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png){.alignnone .size-full .wp-image-12698 .aligncenter attachment-id="12698" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="squares_bhAskara_Fig1" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png" orig-size="1300,531" permalink="https://manasataramgini.wordpress.com/2020/11/10/bhaskaras-dual-square-indeterminate-equations/squares_bhaskara_fig1/" sizes="(max-width: 1300px) 100vw, 1300px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png 1300w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig1.png 1024w"}Figure 1**. Sum and difference of squares amounting to near squares.

In course of our exploration of the भूजा-कोटि-कर्ण-न्याय in our early youth we had observed that there are examples of "near misses":  $8^2+9^2=12^2+1$ . Hence, we were excited to encounter them a little later in an interesting couple of indeterminate simultaneous equations in the लीलावती. Exhibiting his prowess as both a kavi and a mathematician, the great भास्कर-II furnishes the following वसन्ततिलका verse in his लीलावती:

[राश्योर् ययोः कृति-वियोगयुती निरेके]{style="color: #0000ff"}\
[मूलप्रदे प्रवद तौ मम मित्र यत्र ।]{style="color: #0000ff"}\
[क्लिश्यन्ति बीजगणिते पटवो'पि मूढाः]{style="color: #0000ff"}\
[षोढोक्त-गूढगणितं परिभावयन्तः ॥ ल् ६२॥]{style="color: #0000ff"}

Tell me, O friend! those 2 \[numbers], the sum and difference of whose squares\
reduced by one result in square numbers, wherein even experts in algebra who\
keep dwelling upon the mysterious mathematical techniques\
stated in six ways, come up as dim-witted \[in solving this problem].

  - Translation adapted from that conveyed by पण्डित रामसुब्रह्मण्यन्, a learned historian of Hindu mathematics

In terms of his kavitvam, भास्कर-II abundantly illustrates the use of figures of speech, such as the yamaka-s or alliterative duplications. ऋआमसुब्रह्मण्यन् also mentions that he uses the figure of speech termed the "उल्लास" via the opposition of "पटवः" and "मूढाः" in the same verse to bring out the wonder associated with this problem, i.e. it is difficult even for those who are adept at the 6 operations of traditional Hindu mathematics: addition, subtraction, multiplication, division, squaring and square-root-extraction. The problem, put in modern notation goes thus:

Let  $x, y, a, b$  be rational numbers with  $y> x$ . Then,

 $$x^2+y^2-1=a^2$$ 

 $$y^2-x^2-1 =b^2$$ 

When we first encountered it, we wondered if it was really that difficult but soon our investigation showed that it was hardly simple for us. To date, we do not have a general solution in integers, placing us squarely among the dull-witted. However, in the course of our study of the integer solutions, we discovered parametrizations with interesting connections beyond those provided by भास्कर. We suspect he was aware of one or more of these, which is why he termed it a difficult problem for even those well-versed in arithmetic operations. In essence, the problem the generation of 2 new squares plus a unit square for each of them from the sums and differences of the areas of 2 starting squares (Figure 1). Before we consider the integer solutions, let us see the parametrizations offered by भास्कर to obtain rational fractional solutions. By way of providing several numerical examples (a white indologist of the German school but with a style more typical of the American school had once stated with much verbiage what essentially amounts to "Hindoos must be idiots" for presenting such repetitive examples. He evidently forgot the fact that it was also the style of the great Leonhard Euler), he says:

[अत्र प्रथमानयने\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$कल्पितम् इष्टम् । अस्य कृतिः\$स्तर्त्\$\त्फ़्रच्{१}{४}\$एन्द्\$। अष्ट-गुणोजातः २ । अयं व्येकः १ । दलितः\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$। इष्टेन\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$हृतो जातः । अस्य कृतिः १ । दलिता\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$सैका\$स्तर्त्\$\त्फ़्रच्{३}{२}\$एन्द्\$। अयम् अपरोराशिः । एवम् एतौ राशी\$स्तर्त्\$१, \त्फ़्रच्{३}{२}\$एन्द्\$॥ एवम् एकेन+ इष्टेन जातौ राशी\$स्तर्त्\$\त्फ़्रच्{७}{२}, \त्फ़्रच्{५७}{८}\$एन्द्\$द्विकेन\$स्तर्त्\$\त्फ़्रच्{३१}{४}, \त्फ़्रच्{९९३}{३२}\$एन्द्\$||]{style="color: #0000ff"} (Parametrization 1)

[अथ द्वितीय-प्रकारेण+ इष्टं १ अनेन द्वि-गुणेन २ रूपं भक्तम्\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$। इष्टेन सहितम् जातः प्रथमो राशिः\$स्तर्त्\$\त्फ़्रच्{३}{२}\$एन्द्\$द्वितीयो रूपम् १ एवं राशी\$स्तर्त्\$\त्फ़्रच्{३}{२}, १\$एन्द्\$॥ एवं द्विकेन+ इष्टेन\$स्तर्त्\$\त्फ़्रच्{९}{४}, १\$एन्द्\$। त्रिकेण\$स्तर्त्\$\त्फ़्रच्{१९}{६}, १\$एन्द्\$त्र्य्-अंशेन जातौ राशी\$स्तर्त्\$\त्फ़्रच्{११}{६},१\$एन्द्\$||]{style="color: #0000ff"} (Parametrization 2)

The second parametrization he offers is rather simple:

 $$x=1; y=\dfrac{2t^2+1}{2t}$$ 

He illustrates it with the integers  $t=1, 2, 3...$  and  $t= \tfrac{1}{3}$ . With integers, we see that  $y$  is defined by a fractional sequence whose denominators are the successive even numbers and whose numerators are defined by the sequence  $2n^2+1: 3, 9, 19, 33, 51...$ . This sequence has interesting geometric connections. One can see that it defines the maximum number of bounded or unbounded regions that a plane can be divided into by  $t$  pairs of parallel lines. Thus, one can see that the  $y$  of भास्कर's second parametrization provides the ratio the maximum partitions of a plane to the total number of parallel lines drawn in dyads used for the purpose.

**![squares_bhAskara_Fig2](https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png){.alignnone .size-full .wp-image-12699 .aligncenter attachment-id="12699" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="squares_bhAskara_Fig2" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png" orig-size="1015,426" permalink="https://manasataramgini.wordpress.com/2020/11/10/bhaskaras-dual-square-indeterminate-equations/squares_bhaskara_fig2/" sizes="(max-width: 1015px) 100vw, 1015px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png 1015w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig2.png 768w"}Figure 2.** Division of plane into regions by parallel lines: 3, 9, 19... regions by 1, 2, 3 pairs of parallel lines.

भास्कर's first fractional parametrization takes the form:

 $$x=\dfrac{8t^2-1}{2t}$$ 

 $$y= \dfrac{x^2}{2}+1 = \dfrac{64t^4-8t^2+1}{8t^2}$$ 

He illustrates this with  $t= \tfrac{1}{2}, 1, 2$ , which respectively yield the pairs of solutions  $(1, \tfrac{3}{2})$ ,  $(\tfrac{7}{2}, \tfrac{57}{8})$ ,  $(\tfrac{31}{4}, \tfrac{993}{32})$ .

The problem can also be seen as that of finding the intersection between coaxial circles and hyperbolas. Restricting ourselves to intersections in the first quadrant, we can see that the general form of the solutions would be:

 $$x=\sqrt{\dfrac{a^2-b^2}{2}}$ ;  $y=\sqrt{1 + \dfrac{a^2+b^2}{2}}$$ 

From the above, for integer solutions we can say the following: 1) Given that  $a^2+b^2$  must be an even number,  $a, b$  should be even. 2) Hence, from the original pair of equations, we can say that  $x, y$  must be of opposite parity with  $x$  being even and  $y$  odd. 3) Further, for  $x$  to be an even number it has to be divisible by 8. 4) Hence,  $y \mod 8 \equiv 1$ . Therefore, all solutions should be of the form  $x=8m, y=8n+1$ , where  $m, n$  are integers. Beyond this, not being particularly adept at mathematics, to actually solve the equations for integers, we took the numerical approach and computed the first few pairs of solutions. It was quite easy to locate the first solution  $(8, 9)$  (Figure 1) which in a sense is like the most primitive भुजा-कोटि-कर्ण triplet (3, 4, 5). The first few solutions are provided below as a table and illustrated as a  $x-y$  plot in Figure 3.

**[![Bhaskara_squares](https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png){.alignnone .size-full .wp-image-12702 .aligncenter attachment-id="12702" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="Bhaskara_squares" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png" orig-size="3300,2400" permalink="https://manasataramgini.wordpress.com/2020/11/10/bhaskaras-dual-square-indeterminate-equations/bhaskara_squares/" sizes="(max-width: 3300px) 100vw, 3300px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png 3300w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/bhaskara_squares.png)Figure 3.** First few integer solutions.

**Table 1**

![squares_bhAskara_Table1](https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_table1.png){width="75%"}

We quickly noticed that there is one family of solutions that lie on a clearly defined curve (dark red in Figure 3).

**Family 1.** This family has the convergents:  $\tfrac{y}{x} \rightarrow$  1, 2, 3, 4  $\dots$ . We can easily obtain parametrization defining this family to be:

 $$x=8t^3$$ 

 $$y=8t^4+1$$ 

This yields (8,9); (64,129); (216,649); (512,2049); (1000,5001); (1728,10369); (2744,19209); (4096,32769); (5832,52489); (8000,80001). This corresponds to the the third parametrization offered by भास्कर that may be used to obtain rational fractional or integer solutions:

[अथवा सूत्रम् --]{style="color: #0000ff"}\
[इष्टस्य वर्ग-वर्गो घनश् च तव् अष्ट-सङ्गुणौ प्रथमः ।]{style="color: #0000ff"}\
[सैको राशी स्याताम् एवम् व्यक्ते+ अथ वा अव्यक्ते ॥ ल् ६३]{style="color: #0000ff"}\
Or the सूत्र: Square the square of the given number and the cube of that number respectively multiplied by 8, adding 1 to the first product, the solutions are obtained both for arithmetic examples or as algebraic parametrization.

[इष्टम्\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$अस्य वर्ग-वर्गः\$स्तर्त्\$\त्फ़्रच्{१}{१६}\$एन्द्\$अष्टघ्नः\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$सैको जातः प्रथमो राशिः\$स्तर्त्\$\त्फ़्रच्{३}{२}\$एन्द्\$पुनर् इष्टम्\$स्तर्त्\$\त्फ़्रच्{१}{२}\$एन्द्\$अस्य घनः\$स्तर्त्\$\त्फ़्रच्{१}{८}\$एन्द्\$अष्ट-गुणो जातो द्वितीयो राशिः १ एवं जातौ राशी\$स्तर्त्\$\त्फ़्रच्{३}{२}\$एन्द्\$, १ । अथ+ एकेन इष्टेन ९, ८ । द्विकेन १२९, ६४ । त्रिकेण ६४९, २१६ । एवं सर्वेष्व् अपि प्रकारेष्व् इष्ट-वशाद् आनन्त्यम् ॥]{style="color: #0000ff"}

By taking  $\tfrac{1}{2}$  as the given, the square of the square of the given number is  $\tfrac{1}{16}$ , which multiplied by 8 is  $\tfrac{1}{2}$ . This plus 1 yields the first number of the solution  $\tfrac{3}{2}$ . Again given  $\tfrac{1}{2}$ , its cube is  $\tfrac{1}{8}$  which multiplied by 8 yields the second number of the solution, 1. Thus, we have the pair (1, 3/2). Now with 1 as the given we get (8, 9); with 2 we get (64, 129); with 3 we get (216, 649). Thus, with each of these parametrizations (i.e. all the 3 he offers) by substituting any number one gets infinite solutions.

However, this parametrization hardly accounts for all the solutions. Through analysis of the remaining solutions, we could discover several further families with distinct more complex parametrizations. They are:\
**Family 2.** This family has the convergent  $\tfrac{y}{x} \rightarrow 1$ 

 $$x= (T_t(3))^2-1$$ 

 $$y=(T_t(3))^2$$ 

Here,  $T_t(x)$  is the  $t$ -th Chebyshev polynomial of the first kind that is defined based on the multiple angle formula of the cosine function:

 $$\cos(x) =\cos(x)$ ;  $\cos(2x)=2\cos^2(x)-1$ ;  $\cos(3x)= 4\cos^3(x)-3 \cos(x)$$ 

Thus, we get the Chebshev polynomials  $T_n(x)$  as:

 $$T_1(x)=x$ ;  $T_2(x)= 2x^2-1$ ;  $T_3(x) = 4x^3-3x \dots$$ 

Thus, we get the pairs (8,9); (288,289); (9800, 9801); (332928, 332929). All these points lie on the line  $y=x+1$ 

One observes that  $\tfrac{x}{2}=k^2$  where  $k \rightarrow$  2, 12, 70, 408  $\dots$ 

From the above, it is easy to prove that the sequence of fractions  $\tfrac{\sqrt{y}}{k}$  are successive convergents for  $\sqrt{2}$ . For  $t=4$  we get  $\tfrac{577}{408 } = 1.41421 \dots$ , which is बौधायन's convergent approximating  $\sqrt{2}$  to 5 places after the decimal point.

**Family 3.** This family has the convergent  $\tfrac{y}{x} \rightarrow \tfrac{4}{3}$  (green line in Figure 3). It can be parametrized thus:

 $$x=8 \left \lceil \dfrac{(9 - 3 \sqrt{7}) (8 + 3 \sqrt{7})\^t}{28} \right \rceil$$ 

Thus, the ratio of successive  $x$  converges to  $8 + 3 \sqrt{7}$ , a number which is again related to Chebyshev polynomials of the first kind evaluated at 8:

 $$\displaystyle \lim_{n \to \infty}\dfrac{T_n(8)}{T_{n-1}(8)} = 8 + 3 \sqrt{7}$$ 

 $$y=\left \lfloor \dfrac{32}{3} \left \lceil \dfrac{(9 - 3 \sqrt{7}) (8 + 3 \sqrt{7})\^t}{28} \right \rceil \right \rfloor -1$$ 

Thus, we arrive at the pairs constituting this family as: (8,9); (80,105); (1232,1641); (19592,26121)  $\dots$ 

**Family 4.** This family has the convergent  $\tfrac{y}{x} \rightarrow \tfrac{\sqrt{5}}{2} \approx 1.1180339$ . It can be parametrized thus:

 $$x= M(6t)$ , i.e.  $x$  is every 6th term of the मात्र-meru sequence  $M(n): 1, 1, 2, 3, 5, 8, 13...$$ 

Thus, we can also express  $x$  using the Golden ratio  $\phi \approx 1.61803 \dots$ :

 $$x= \dfrac{\phi^{6t}-\phi^{-6t}}{2\phi-1}$$ 

Thus, the ratio of successive  $x$  converges to  $\phi^6=9+\sqrt{80}$ ; we can also write  $x$  using the hyperbolic sine function:

 $$x= \dfrac{2 \sinh(6t\log(\phi))}{2\phi-1}$$ 

Similarly, we get:

 $$y=\dfrac{\phi^{6t}+\phi^{-6t}}{2}$$ 

As with  $x$  we can also get a hyperbolic trignometric expression for  $y$ :

 $$y= \cosh(6t \log(\phi))$$ 

Finally, we can also write  $y$  compactly in terms of Chebyshev polynomials of the first kind:

 $$y= T_t(9)$$ 

Thus, the first few members of this family are: (8,9); (144,161); (2584,2889); (46368,51841)  $\dots$ 

**Family 5.** This family has the convergent  $\tfrac{y}{x} \rightarrow \tfrac{\sqrt{65}}{4} \approx 2.0155644$ . It can be parametrized using the continued fraction expressions of the convergent:

 $$x \rightarrow$  denominators of odd terms of the continued fraction convergents of  $\tfrac{\sqrt{65}}{4}$$ 

 $$y \rightarrow$  numerators of odd terms of the continued fraction convergents of  $\tfrac{\sqrt{65}}{4}$$ 

The relevant partial convergents are  $\tfrac{129}{64}$ ;  $\tfrac{33281}{16512}$ ;  $\tfrac{8586369}{4260032}$ ;  $\tfrac{2215249921}{1099071744} \dots$ 

Thus, we see that the convergents with odd numerators and even denominators yield the  $(x,y)$  corresponding to this family, with the first term matching the second term of family 1; hence, it may be seen as branching from family 1. In practical terms, one can obtain these values using the below 2-seeded recursions:\
 $y \rightarrow f\lbrack n\rbrack = 258f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =0, f\lbrack 2\rbrack = 64$ ; second term onward\
 $y \rightarrow f\lbrack n\rbrack = 258f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =1, f\lbrack 2\rbrack = 129$ ; second term onward

**Family 6.** This family has the convergent  $\tfrac{y}{x} \rightarrow \tfrac{\sqrt{689}}{20} \approx 1.31244047$ . It can be parametrized using the partial convergent fractions approximating the convergent.

 $x \rightarrow$  denominators of partial convergents of  $\tfrac{\sqrt{689}}{20}$  divisible by 8

 $$y \rightarrow$  numerators of partial convergents of  $\tfrac{\sqrt{689}}{20}$   $\mod 8 \equiv 1$$ 

The relevant partial convergents are  $\tfrac{105}{80}$ ;  $\tfrac{22049}{16800}$ ;  $\tfrac{4630185}{3527920}$ ;  $\tfrac{740846400}{972316801} \dots$ 

The first term is the same as the second term of family 4. In practical terms, one can obtain these values using the below 2-seeded recursions:

 $y \rightarrow f\lbrack n\rbrack = 210 f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =0 f\lbrack 2\rbrack = 80$ ; second term onward\
 $y \rightarrow f\lbrack n\rbrack = 210 f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =1 f\lbrack 2\rbrack = 105$ ; second term onward

In the case of families 4, 5 and 6, we observe that the sum and the difference of the squares of the numerators and denominators yield perfect squares. Further, the denominator is always an even number and the numerator a surd of the form  $\sqrt{k^2+1}$  or  $\sqrt{k^2+k/2}$ . This suggests an approach for discovering new families. Our search till 100000 uncovered 2 more families of this form

**Family 7.** This family has the convergent  $\tfrac{y}{x} \rightarrow \dfrac{\sqrt{29585}}{104} \approx 1.6538741$ ;  $29585 = 172^2+1$ 

It can be parametrized using the partial convergent fractions approximating the convergent thus:

 $x \rightarrow$  denominators of partial convergents of  $\tfrac{\sqrt{29585}}{104}$  divisible by 8

 $$y \rightarrow$  numerators of partial convergents of  $\tfrac{\sqrt{29585}}{104}$   $\mod 8 \equiv 1$$ 

The relevant partial convergents are:

 $$\tfrac{59169}{35776}$ ;  $\tfrac{7001941121}{4233660288}$ ;  $\tfrac{828595708317729}{501002891125568} \dots$$ 

In practical terms, one can obtain these values using the below 2-seeded recursions:

 $y \rightarrow f\lbrack n\rbrack = 118338 f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =0 f\lbrack 2\rbrack = 35776$ ; second term onward\
 $y \rightarrow f\lbrack n\rbrack = 118338 f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =1 f\lbrack 2\rbrack = 59169$ ; second term onward

**Family 8.** This family has the convergent  $\tfrac{y}{x} \rightarrow \dfrac{\sqrt{44945}}{208} \approx 1.0192421$ ;  $29585 = 212^2+1$ 

It can be parametrized using the partial convergent fractions approximating the convergent thus:

 $x \rightarrow$  denominators of partial convergents of  $\tfrac{\sqrt{44945}}{208}$  divisible by 8

 $$y \rightarrow$  numerators of partial convergents of  $\tfrac{\sqrt{44945}}{208}$   $\mod 8 \equiv 1$$ 

The relevant partial convergents are:

 $$\tfrac{89889}{88192}$ ;  $\tfrac{16160064641}{15854981376}$ ;  $\tfrac{2905224100939809}{2850376841726336} \dots$$ 

In practical terms, one can obtain these values using the below 2-seeded recursions:

 $y \rightarrow f\lbrack n\rbrack = 179778 f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =0 f\lbrack 2\rbrack = 88192$ ; second term onward\
 $y \rightarrow f\lbrack n\rbrack = 179778 f\lbrack n-1\rbrack - f\lbrack n-2\rbrack ; \; f\lbrack 1\rbrack =1 f\lbrack 2\rbrack = 89889$ ; second term onward

In families 7, 8 there are no small terms that connect them to any of the other families; keeping with Hindu love for big numbers, they start relatively large and grow rapidly. These 8 families cover all the solutions in Table 1 and Figure 3  $(\le 10^5)$ . The relationships between them are shown in Figure 4.

**![squares_bhAskara_Fig4](https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png){.size-full .wp-image-12700 .alignnone attachment-id="12700" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="squares_bhAskara_Fig4" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png" orig-size="1146,817" permalink="https://manasataramgini.wordpress.com/2020/11/10/bhaskaras-dual-square-indeterminate-equations/squares_bhaskara_fig4/" loading="lazy" sizes="(max-width: 1146px) 100vw, 1146px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png 1146w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2020/11/squares_bhaskara_fig4.png 1024w"}Figure 4.** The relationship between families.

Is there a general way to obtain all parametrizations for the integer solutions of this pair of indeterminate equations? Perhaps this has already been answered by mathematicians or perhaps not. In any case, as भास्कर had stated, the solutions to this couple of equations is not an entirely trivial problem and sufficiently absorbing for an enthusiast of arithmetic.

