
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An arithmetic experiment and an unsolved problem](https://manasataramgini.wordpress.com/2020/08/05/an-arithmetic-experiment-and-an-unsolved-problem/){rel="bookmark"} {#an-arithmetic-experiment-and-an-unsolved-problem .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 5, 2020]{.entry-date}](https://manasataramgini.wordpress.com/2020/08/05/an-arithmetic-experiment-and-an-unsolved-problem/ "12:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We realized that a simple arithmetic experiment we had performed in our youth is actually related to an unsolved problem in number theory. It goes thus: consider the sequence of natural numbers  $n=1, 2, 3, 4 \cdots$  Then find the distance of  $n$  to nearest prime  $p$  that is 1) greater than or equal to  $n$  or 2) less than or equal to  $n$ . Thus,  $d_1\lbrack n\rbrack =p-n, d_2\lbrack n\rbrack =n-p$ . We then define the arithmetic function  $f_1\lbrack n\rbrack =d_1\lbrack n\rbrack d_2\lbrack n\rbrack$ . Since, 1 has no prime before it, we can either have  $f_1\lbrack 1\rbrack$  as undefined or assign 0 to it. The corresponding sequence goes thus:

 $$f_1 \rightarrow ?, 0, 0, 1, 0, 1, 0, 3, 4, 3, 0, 1, 0, 3, 4 \cdots$$ 

The function as a nice shape with symmetric maxima that remind one of reptilian teeth (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/08/prime_distance_1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Now, where do the successive local maxima of this function occur? If we leave out the undefined  $f_1\lbrack 1\rbrack$  we see that these occur in a sequence which we call  $f_2$ :

 $$f_2 \rightarrow 3, 5, 8, 11, 14, 17, 20, 25, 29, 33, 38 \cdots$$ 

One can right away intuitively conclude that this sequence captures the occurrences of primes in natural number space by defining some kind of central position between them. Hence, we can more explicitly ask, what is the relation of  $f_2$  to the arithmetic and geometric means of successive primes behave? We find that the above sequence  $f_2= \lfloor \textrm{GM}(p_n, p_{n+1}) \rfloor$  and  $f_2 = \textrm{AM}(p_n, p_{n+1}) - 1$  for primes starting with 3 onward. One can see that the local maxima of  $f_1$ , i.e. the values of  $f_1\lbrack f_2\rbrack$  (if we count leaving out the undefined first term in  $f_1$ ), are all square numbers. These have a specific relationship to the prime difference function  $\textrm{pd}\lbrack n\rbrack = p_{n+1}-p_n$  starting from 3 (Figure 2). Given that from 3 onward every prime is odd, the corresponding  $\textrm{pd}\lbrack n\rbrack$  will be even. Then, we have the following relationship to the local maxima in  $f_1$ :

 $$f_1\lbrack f_2\rbrack = \dfrac{\left(\textrm{pd}\lbrack n\rbrack \right)^2}{4}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/08/prime_distance_2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Thus, the local maxima of  $f_1$  help define a certain parabolically rescaled version of the prime difference function, which, as we will see below, has utility in understanding aspects of the occurrence of successive primes.

We know that  $\textrm{AM} \ge \textrm{GM}$  and  $\textrm{GM}(p_n, p_{n+1})$  will never be a whole number. Thus, we can define the arithmetic function  $\textrm{pmd}\lbrack n\rbrack = \textrm{AM}(p_n, p_{n+1}) - \textrm{GM}(p_n, p_{n+1})$ . One experimentally notes the asymptotic behavior that as  $n \to \infty, \; \textrm{pmd}\lbrack n\rbrack \to 0$ . However, this secular decay is marked local fluctuations. There are two notable features of this (Figure 3): 1) The maximum value of  $\textrm{pmd}\lbrack n\rbrack$  is 0.22503561260788 for  $n=4$  which corresponds to the  $p_n=7, p_{n+1}=11$ . Thus, we can conjecture that the difference between the arithmetic and geometric means of successive primes is always less than one fourth, i.e.  $\textrm{pmd}\lbrack n\rbrack < \tfrac{1}{4}$ . 2) The fluctuations of  $\textrm{pmd}\lbrack n\rbrack$  starting from  $n=2$  exactly mirror the fluctuations defined by the local maxima of  $f_1\lbrack n\rbrack$ , i.e.  $f_1\lbrack f_2\rbrack$ , with the magnitude of the  $f_1\lbrack f_2\rbrack$  peak tracking the magnitude of the peak in  $\textrm{pmd}\lbrack n\rbrack$ . The first time a peak of given magnitude appears in  $f_1\lbrack f_2\rbrack$  it has the largest corresponding effect in  $\textrm{pmd}\lbrack n\rbrack$  with all subsequent appearances of the peak of the same magnitude being increasingly muted.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2020/08/prime_distance_3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Thus, we can empirically determine that (Figure 4):

 $$\dfrac{f_1\lbrack f_2\rbrack }{\textrm{pmd}\lbrack n\rbrack } \approx 2.3 n \log(n)$$ 

![prime_Distance_4](https://manasataramgini.wordpress.com/wp-content/uploads/2020/08/prime_distance_4.png){width="75%"}$  while the dark red curve is  $y=2.3 n \log(x)$ 

Whether there is some closed form for the constant 2.3 remains an open question to us. After we posted this note, an acquaintance from Twitter provided a [proof](https://colab.research.google.com/drive/1dbzmt61xvNwCLEr6dXKcNLl1tjGZWxcI) for why the constant in the above equation should be should be 2 for large  $n$ .

Prime gaps have been intensely studied since at least Legendre who had a conjecture regarding them; several tighter variants of that conjecture have been proposed repeatedly since then. Hence, looked up the literature to see if our conjecture regarding the difference of the arithmetic and geometric means of successive primes might be equivalent to any of those. We learnt recently that it is a version of a conjecture stated by Andrica, in 1986, just about a decade before when we began exploring the function  $f_1\lbrack n\rbrack$ . It goes thus:

 $$\sqrt{p_{n+1}}-\sqrt{p_n} < 1$$ 

The form in which we present the conjecture appears to be a nice statement of a strong version of the Andrica conjecture and  $f_1\lbrack f_2\rbrack$  provides a cleaner comparison for the fluctuations in  $\textrm{pmd}\lbrack n\rbrack$  than the simple prime gap function. Remarkably, simple as these conjectures are, they have not been proven to date. Moreover, it seems that even if the Riemann hypothesis were to be true, that by itself will not imply these conjectures. Thus, yet again we have simple arithmetic statements that can be understood or arrived at even by a school kid but are extraordinarily difficult to prove formally. The philosophical implications of these are interesting to us.

