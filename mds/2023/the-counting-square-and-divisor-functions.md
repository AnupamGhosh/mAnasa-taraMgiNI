
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The counting square and divisor functions](https://manasataramgini.wordpress.com/2023/09/09/the-counting-square-and-divisor-functions/){rel="bookmark"} {#the-counting-square-and-divisor-functions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 9, 2023]{.entry-date}](https://manasataramgini.wordpress.com/2023/09/09/the-counting-square-and-divisor-functions/ "9:50 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Leonhard Euler's discovery of the zeta function in the course of solving the Basel problem was one of those momentous "unifications" in mathematics with deep philosophical implications that eventually led to the Riemann hypothesis. By bringing together trigonometry and arithmetic, it presented the first glimmer in the Occident for an arithmetic foundation of the ideal world. We have found that it always helps to revisit the basics of knowledge and reflect upon it from the vantage point of all you learnt since you first acquired those basics. In this case, the basics that we shall revisit are counting with integers and operations related to them. When you learn those basics (close to the boundary from when life-long memories are retained), things like the values of the zeta function or even  $\pi$  beyond your very limited horizon (at least for most ordinary mortals). Hence, a glimpse of the deeper significance and philosophical implications of the basic operations appears only later in life. In this note, we detail one such exploration from our youth that brought home to us the connection between basic arithmetic and  $\pi$  via  $\zeta(2)$ .

Consider the square matrix, which we call the counting box. It is a representation of the basic arithmetic operation of counting with integers from which we get both multiplication and division. It is constructed by filling successive columns of a  $n \times n$  matrix by the first  $n$  elements of the following repeating sequences: 1,0,1,0,1,0,1,0  $\ldots$ ; 1,2,0,1,2,0,1,2,0  $\ldots$ ; 1,2,3,0,1,2,3,0,1,2,3,0  $\ldots$ . While the matrix can essentially be infinite, we can show  $n \times n$  blocks of it (e.g., below, we have a  $n= 10$  matrix).

![count_square_matrix](https://manasataramgini.wordpress.com/wp-content/uploads/2023/09/count_square_matrix.png){width="75%"}

Now, the upper triangle (diagonal included) of this matrix is rather predictable: row  $k$ , counting from 1, of this triangle will have a run of  $(n-k+1)$  instances of  $k$ . The lower triangle of this matrix (diagonal excluded) is a little more interesting. Each row is the sequence  $k \mod 2, k \mod 3, \ldots, k \mod k$ . Thus, for row 7 of the matrix, the corresponding row in the lower triangle is 1, 1, 3, 2, 1, 0. This is because, by its very definition, this array represents the division process by successive integers, and the remainders of these divisions will be seen in the successive elements of a row of the lower triangle. For a row  $k$  of the lower triangle of the matrix, the first  $\left\lfloor\tfrac{k}{2}\right\rfloor-1$  elements will be the unique reminder signature of the division of  $k$  by all integers  $\le \tfrac{k}{2}$ . The remaining  $\left\lceil\tfrac{k}{2}\right\rceil$  elements of this row of the lower triangle will be successive integers in decreasing order from  $\left\lceil\tfrac{k}{2}\right\rceil-1$  to 0. Thus, for both rows 9 and 10, this run will be 4, 3, 2, 1, 0.

We were curious about the curve defined by the values of the row-wise sums of the above matrix --- the sum is plotted as the  $y$  value and the corresponding row  $k$  as the  $x$  value. This can be done for this matrix of any size  $n$ ; hence, to maintain the same scale, we normalized the curve by dividing the  $k$  values by  $n$  and the sums on the  $y$  axis by the maximum value attained by the row sum. We then asked for what value of  $\tfrac{k}{n}$  will the maximum sum (1 when normalized) be attained. We can easily place some lower bounds on it. As we saw above, each row of the upper triangle of the matrix has  $(n-k+1)$  instances of  $k$ . Hence, the sum of these values would be the quadratic function  $s_1=(n+1)k-k^2$ . Using some elementary calculus to find the maximum of this parabola, we see that it will be attained at  $k=\tfrac{n+1}{2}$ . Thus, when we normalize it for large  $n$ , it will be practically at  $\tfrac{1}{2}$ . Hence, the maximum of the row sum of the matrix will certainly be attained at a value  $> \tfrac{1}{2}$  because the above only accounts for the upper triangle part of a given row. As we saw above, the terminal part of row  $k$  of the lower triangle has an arithmetic progression of integers from  $\left\lceil\tfrac{k}{2}\right\rceil-1 \ldots 1$ . The sum of this part will hence be:

 $$s_2=\dfrac{1}{2}\left(\left\lceil\tfrac{k}{2}\right\rceil^2-\left\lceil\tfrac{k}{2}\right\rceil \right)$$ 

We can approximate this as:

 $$s_2 \approx \dfrac{k^2-2k}{8}$$ 

 $$\therefore s_1+s_2 \approx k\left(n+\dfrac{3}{4}\right)- \dfrac{7k^2}{8}$$ 

Again, finding where the maximum occurs for this parabola, we get it as  $\tfrac{4}{7}$  when  $n$  is large. Again the actual value where the maximum occurs would be greater than  $\tfrac{4}{7}$  (Figure 1) because this sum still leaves out the first  $\left\lfloor\tfrac{k}{2}\right\rfloor-1$  forming the unique modulo signature of  $k$ . Given our limited knowledge when we did this exercise in our youth, this bound-setting, which a mathematician might laugh at, gave us a sense of what to expect --- an approximately parabolic curve with maximum occurring for a  $\tfrac{k}{n} > \tfrac{4}{7}$ . We then empirically computed the actual maximum (Figure 1) and obtained a value of  $0.608$  --- this immediately led us to realize that the maximum is attained at approximately  $\tfrac{1}{\zeta(2)}= \tfrac{6}{\pi^2}$ . Here,  $\zeta(x)$  is the famous Zeta function known after Bernhard Riemann.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/09/count_square_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Figure 2 shows the computation of the value of  $\tfrac{k}{n}$  at which the maximum occurs for increasing values of  $n$ , which when averaged yields  $\approx \tfrac{6}{\pi^2}$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/09/count_square_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


To understand why this was so, we next tackled the issue of the row sums of the lower triangle of this matrix. It is essentially the sum of the modulos:  $(2 \ldots k) \mod k$ . Using the full square matrix we can infer this to be the below formula for row  $k$  (Figure 3):
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/09/count_square_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```

 $$\displaystyle S_{ltr}= k^2 - \sum_{j=1}^{k} \sigma_1(j)$$ 

Here,  $\sigma_1(j)$  is the arithmetic divisor function going back to the work of Dirichlet, which is simply the sum of all the divisors of an integer. This, in turn, yields the exact formula for the sum of the complete row  $k$  in the  $n \times n$  matrix as (plotted in yellow in Figure 1):

 $$\displaystyle S_r= (n+1)k - \sum_{j=1}^{k} \sigma_1(j)$$ 

Thus, the key to understanding how  $\zeta(2)$  emerges in this formula is to understand the sum of  $\sigma_1(k)$ . As above, we could empirically establish that asymptotically its first level approximation is  $\tfrac{\pi^2 k^2}{12}$  (pink in Figure 3). Indeed, as a result of the work starting from Dirichlet in the 1800s and culminating in that of Hardy in the last century we have the asymptotic formula for the summatory function of  $\sigma_1(k)$  as:  $S_{\sigma_1} = \tfrac{\pi^2 k^2}{12} + O(k \log(k))$ , where the Landau-Bachmann big  $O$  notation is used for the second term. This formula is established by Hardy and Wright in their "An Introduction to the Theory of Numbers."

The generalized version of the divisor function  $\sigma_n(k)$  can be defined thus:

 $\displaystyle \sigma_n(k) = \sum_{d|k} d\^n$ ; here  $d|k$  means  $d$  divides  $k$ , i.e., is its divisor.

Thus  $\sigma_0(k)$  is the divisor function which specifies the number of divisors. With this in place we can see, that the lower triangle of the matrix also encodes interesting numbers related to the divisor functions:

First, the number of 1s in each row of the lower triangle gives the sequence of the number of divisors of an integer that are greater than 1, i.e. the sequence of  $\sigma_0(k)-1 \to 0, 1, 1, 2, 1, 3, 1, 3, 2, 3, 1, 5 \ldots$  The number of 0s in each row gives the same sequence without the initial 0. The number of non-0 terms of each row of this triangle gives:  $k-\sigma_0(k) \to 0, 1, 1, 3, 2, 5, 4, 6, 6, 9, 6, 11 \ldots$  starting from  $k=2$ . The number of 2s in each row gives the number of divisors of  $k$  that are greater than 2. Likewise, the number of 3 in a given row specifies the number of divisors of  $k$  that are greater than 3 and so on.

