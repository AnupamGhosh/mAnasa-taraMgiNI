
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rise of यज्ञ and कौशिक exceptionalism](https://manasataramgini.wordpress.com/2023/04/25/the-rise-of-yajna-and-kausika-exceptionalism/){rel="bookmark"} {#the-rise-of-यजञ-and-कशक-exceptionalism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 25, 2023]{.entry-date}](https://manasataramgini.wordpress.com/2023/04/25/the-rise-of-yajna-and-kausika-exceptionalism/ "7:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The extant Vedic ritual is bifurcated into two domains the गृह्य (the household rites and rites of passage) and the श्रौत (large-scale/grand rituals). First, in operational terms, they are distinguished by the use of a single fire (the औपासन) in the former and the three fires with the central vedi in the latter; the grander श्रौत rites involve the construction of a more complex system of altars. Second, the गृह्य rites are either done by the ritual patron by himself or with a single ritual specialist, the purohita. In contrast, the श्रौत rituals involve an increasingly larger number of ritual specialists, the ऋत्विक्-s, culminating in a number of 16 for the great sacrifices. However, as can be seen from the extent गृह्यसूत्र and श्रौतसूत्र of the Atharvaveda, respectively the कौशिकसूत्र and the वैतानसूत्र, these ritual structures are not strictly mutually exclusive. In terms of their structure, frequency and objectives, it can be easily seen that the Full Moon and New Moon rites, the चातुर्मास्य and the annual animal sacrifice overlap with the गृह्य equivalents and/or are close to that domain of ritual activity. On the other end, the rites like राजसूय, वाजपेय and अश्वमेध are clearly part of the exclusive श्रौत domain. In between are the various lower-end सोमसंस्था-s that belong to the श्रौत domain, but might have originally had roots in private one-day soma rites closer to the गृह्य pole.

A key question confronting students of early Indo-European tradition is how far back we can trace the antecedents of these ritual structures. Using the comparative method, it can be tentatively said, that there was already some kind of bifurcation between household rituals and grand rituals performed on behalf of an elite ritual patron/leader by multiple ritual specialists in the proto-Indo-Hittite (Indo-Anatolian) period. However, the extensive West Asian substratal influence on the Hittite and other Anatolian rituals prevents us from a more precise reconstruction of the most ancient strata of IE ritual traditions. At the other end, closer to the terminal breakup of the main IE branches, we can confidently state that the common Indo-Iranian tradition already featured a version of the श्रौत fire ritual with multiple ritual experts more or less mapping on to the होतृ, the adhvaryu and the brahman. Further, comparisons of the Indo-Iranian system with: (i) the central cult at Rome; (ii) the Iguvine Tablets of the Umbrian branch of Italic; (iii) fragmentary information of the Mycenaean and Homeric Greek rites indicate that a version of what might be called proto-श्रौत rituals was already in place in early IE times. By early IE, we mean the ancestral IE tradition after the divergence of Anatolian but before the Indo-European expansion from their steppe heartland (archaeologically identified with the early Yamnaya culture).

However, within each branch, these श्रौत-like rites underwent considerable transformation via the exaggeration or de-emphasis of particular facets, innovation within the IE tradition and to a smaller degree incorporation of some non-IE practices. For example, there is no evidence for an early core IE ritual using soma, though there were definitely rituals featuring libatory offerings. However, in the I-Ir branch, the offering of soma rose to centrality. Hence, it would seem that the soma ritual was a development in this branch of the IE domain that likely took the place of earlier libatory offerings. Moreover, even within a lineage, we can see evidence for evolution, with both cooperation and competition between different ritual sub-traditions. Whether soma (as in Ephedra) as a sacrament was adopted from a non-IE group remains unclear. We can also say that the ancestral Indo-Iranian tradition definitely had certain ritual diversity even in the mainstream --- there were practitioners with a more hautra-oriented style of ritual and those with a more आध्वर्यव-oriented style. In the Zoroastrian lineage of Iranians, the hautra-style seems to have dominated (Zarathustra called himself a zaotar), whereas on the Indic side after an initial hautra dominance (probably also in the ancestors of the Kalasha), it shifted towards an आध्वर्यव dominance that was likely catalyzed by a new group of adhvaryu entrants with a [विष्णु-focal tradition](https://manasataramgini.wordpress.com/2020/01/02/the-roots-of-vai%e1%b9%a3%e1%b9%87avam-a-view-from-the-numerology-of-vedic-texts/) (which also led to the broader Indic phenomenon of the वैष्णव sect). In the early Indo-Aryan श्रौत tradition, the soma ritual greatly expanded and fostered a new specialist class with it --- उद्गातृ-s, who like the adhvaryu-s, ate into the original domain of the होतृ-s. Nevertheless, by the time of the initial Aryan conquest of India, a certain compact was reached between these ritual specialists, such that each got their share in the performance of grand श्रौत rituals.

That said, the question remains whether we can trace some of the subtleties of ritual evolution prior to the rise of the आध्वर्यव dominance and the factor(s) that predisposed the tradition towards it. We posit that, within the I-Ir world, there arose a practice, which was formally termed यज्ञ (Ir: yasna) that became the foundation of a reformulated श्रौत ritual which provided the ground for the rise of the adhvaryu-s. The equivalence of the IA यज्ञ and Ir yasna is apparent from the fact that the Iranian tradition uses the term in a sense identical to the Vedic tradition as seen in the Avestan incantation:

[सुरुनुयå नो +यसनॆम xशनुयå नो यसनहॆ ुप नो यसनॆम ाहिश]{style="color: #339966"}\
May you hear our yasna \[invocation]; be pleased with our yasna; may you sit at our yasna.

The IA yaj- and Ir yaz- root has derivatives of its cognates somewhat widely attested across the Indo-Anatolian world: Anatolian branch (Luwian): izi (=worship, Skt yajati); Greek: hagios (=holy); Italic: ieiunus (=fasting). However, in I-Ir, it displays a rather extensive development with several distinct formations that are only attested within this branch and often shared by its two daughter clades IA and Ir, e.g., यज्ञ : yasna; yešti : इष्टि; yajana : yadhana; yajata : yazata; यज्ञिय : yesnya. This suggests that though the root meant worship/holiness even in the ancestral Indo-Anatolian tradition, it likely became a technical term for the solemn श्रौत form of the ritual, यज्ञ, in the common Aryan period. That it was the श्रौत form of the ritual is clear from the fact that the performance of the yasna on the Iranian side minimally requires ritual specialists known as the zaotar (=होतृ) and the राथ्भीक (= ऋत्विक्) playing the role of the होतृ and adhvaryu respectively. While today the surviving Iranian ritual only employs these two, the original version in the Avestan tradition had a set of 8 (half the IA set of 16). Further, the development of the yasna ritual went along with the development of Yajurveda-like material, in the Iranian Yasna texts in 72 chapters relating to its performance. Thus, the expansion of the formations of the root yaj-/yaz- in the I-Ir tradition can be associated with the development of a formalized यज्ञ ritual.

The यज्ञ as we on the Indo-Aryan side perform it is rather complex and probably reflects a survival from its formalization in the Kuru-पाञ्चाल period. However, on the Iranian side, we have a retention of a more primitive version of the yasna. While some might argue for a simplification from the degeneration of the tradition as the number of ritual specialists listed as 8 was brought down to 2 in the extant Iranian rite. We would counter by saying that as a tradition under an existential threat, they simply came down to the minimal version (as in smaller IA rituals like full/new moon rites) in the extent form, but the Yasna texts describe the situation when their tradition was at its acme. However, even that ancient version is clearly simpler than what we have on the IA side. This simpler version however gives us some clues about the ancestral I-Ir version before the extraordinary complexification on the IA side; hence, we note some key points of it:


 1.  The ritual involves the offering of haoma (=soma), ritual flour cakes known as draonah (= Skt द्रोण: a measure of flour) which are equivalents of the Vedic पुरोडाश and ghee. This suggests that the soma offering was a key part of the यज्ञ in the shared I-Ir tradition.

 2.  The offerings were made in a special ritual enclosure parallel to the Vedic version. In the extant Iranian version, it is performed in a permanent designated ritual enclosure in the temple of the fire altar of Mithra. This ritual enclosure is marked by furrows just as the adhvaryu marks it with furrows made with the sphya in the IA tradition.

 3.  Before the actual ritual, a sub-ritual known as the परग्णा is performed to prepare the haoma and extract the juice from it for use in the ritual.

 4.  The haoma libation involves the recitation of the Haoma Yašt akin to the deployment of मण्डल-9 incantations in the IA soma ritual.

 5.  The zaotar recites long incantations similar to the शस्त्र recitations of the होतृ --- the Zoroastrian reflex of the Iranian tradition being a hautra-dominant one, this takes a prominent place with a smaller role for the adhvaryu cognate, the राथ्भीक.

 6.  The yasna has a ritual patron who is like the यजमान, but he does not have any manthra recitations at all unlike his Hindu counterpart.

 7.  The yasna is performed daily in the morning. This is an important departure from the extant Vedic ritual for there is no indication that even an एकाह ritual is performed daily. There may be year-long rituals, but there is no indication that it was continued for perpetuity. However, in the ऋग्वेद, we have hints of the performance of a daily or more simplified soma rite. Hence, we posit that the Iranian yasna might retain features of this ancestral simpler version.

Given all these indications that the यज्ञ/yasna ritual tradition developed within the shared I-Ir heritage, we depart from the mainstream Indo-Europeanists, Indologists, and Iranicists in proposing that the core of the hautra and आध्वर्यव texts and their Iranian counterparts were not composed after the IA and the Ir had gone their separate ways but during the common Aryan period already starting in the western reaches of the steppes. We do accept the fact that since then the texts might have accreted some clearly newer material and also been reworked in terms of organization and the linguistic register in which they have come down to us. Hence, we propose that we can find signals of the early rise of the यज्ञ ritual within the extant Vedic texts.

A simple investigation in this regard is to see how the term यज्ञ is distributed across three core texts of the Vedic corpus that relate to the performance of the ritual. Given that these texts are of different sizes (a numerological problem [we have alluded to before](https://manasataramgini.wordpress.com/2020/01/02/the-roots-of-vai%e1%b9%a3%e1%b9%87avam-a-view-from-the-numerology-of-vedic-texts/)), we used the most natural unit for normalization of an old Sanskrit text --- the total syllable count:

![yajna_Count_table](https://manasataramgini.wordpress.com/wp-content/uploads/2023/04/yajna_count_table.png){width="75%"}

There is a clear over-representation of the term यज्ञ in the Yajurveda as represented by the तैत्तिरीय-संहिता. In contrast, the RV and the AV संहिता queried here show similar densities of the word. This indicates that the word यज्ञ has a special association with the आध्वर्यव tradition. Thus, given the above discussion on its I-Ir roots, we posit that the formalization of the यज्ञ ritual provided the ground for the rise of the आध्वर्यव tradition. We have to emphasize that we do not mean that the आध्वर्यव tradition invented it in the first place --- it was common to the hautra and आध्वर्यव traditions as indicated by its Iranian reflex. However, its expansion was a pre-disposition that favored the आध्वर्यव dominance. This leads to the question of whether there was some group within the I-Ir tradition that facilitated its expansion. At least on the IA side, we can address this because the RV is divided into compositions that are primarily grouped together by clan. Hence, just as we compared different Vedic texts in the above table, we can see if there is some kind of anisotropy within the RV itself.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/04/yajna_etc_bar.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Figure 1 shows the density per 100 ऋक्-s of 8 terms associated with the यज्ञ for each मण्डल of the RV as bar graphs: 1. यज्ञ and formations incorporating it; 2. यजमान --- the ritual patron; 3. होतृ; 4. adhvaryu/adhvara; 5. जातवेदस्, the Agni देवता specifically associated with the ritual (see below); 6. वैश्वानर: the Agni देवता representing the universal nature of Agni; 7. पुरोऌआश: the ritual flour cake; 8. manth- the root meaning churn/agitate used both in the sense of churning out the fire with the fire-drill and stirring the soma for preparing drinks mixed with milk or barley water.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/04/yajna_strch.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Figure 2 shows the same data such that each term is normalized by the maximum value for that term. Thus, the मण्डल with the maximum density will be 1, and every other मण्डल will be a fraction between [0,1] of that maximal मण्डल.

The exceptionalism of the कौशिक-मण्डल (RV3) with विश्वामित्र the son of गाथिन् as its primary composer is immediately apparent. In 6 out of the 8 terms, it shows the maximum density, and in the remaining two it is the second most dense. With respect to these terms, most other मण्डल-s clearly pull away as a separate band from RV3. Apart from the word यज्ञ itself, we find the कौशिक exceptionalism in certain other words to be illuminating. The god Agni manifests as several देवता-s in the Vedic tradition, the two main ones are Agni जातवेदस् and Agni वैश्वानर. There are other less frequent ones like Agni रक्षोहन्, Agni अनीकवत्, Agni Saptavat and the like. Of these, Agni जातवेदस् is central to the concept of यज्ञ as he is identified with the fire established at the beginning of the यज्ञ through the churning action of the fire-drill and thereafter burning unbroken through the यज्ञ. Hence, it is rather notable that the root related to churning, manth-, is also over-represented in RV3. He is also identified with the Agni maintained through the generations by the ritualists, an ancestral IE tradition. Thus, he is seen as the local divine होतृ who conveys the oblations offered in the ritual fire to the gods --- a concept clearly expressed by Madhuchandas, the son of विश्वामित्र, in the opening ऋक् of the RV. Hence, one may interpret Agni जातवेदस् as both the one with knowledge of the birth of the ritual in the proximal sense and the one with knowledge of it in the sense of continuity from the primordial establishment of the fire. In this regard, the कौशिक-s of मण्डल-3 are known from tradition to have a special connection through marriage and reciprocal ritual collaboration with the भृगु-s, one of the primordial institutor clans of the fire ritual. Further, in the RV, जातवेदस् is offered oblations at the dawn ritual (specifically mentioned by the विश्वामित्र-s of RV3). Thus, जातवेदस् also has knowledge of the beginning (birth) of the day's ritual. This supports the idea that, like the Iranian daily morning yasna, there was probably also an early IA daily यज्ञ ritual.

Agni जातवेदस् is paired with a complementary universal manifestation of the god, Agni वैश्वानर --- one common to all folks, unlike the private जातवेदस् who is associated with a particular ritualist clan from the time they established the fire ritual. वैश्वानर is described as the universal fire from whom all other fires emerge as branches (RV 1.59.1). He is simultaneously in the middle of the earth, at the center of the celestial hemispheres indicating his manifestation as solar radiance and also at the equinoctial colure marking the celestial path of the gods, the देवयान, associated with the yearly ritual cycle. He is the bringer of light for the आर्य and the smiter of their dasyu enemies. However, despite his universality, keeping with his highest density in RV6, Agni वैश्वानर seems to have had a special connection with the भरद्वाज-s, one of the ancient आङ्गिरस clans associated with the foundation of the fire rite (e.g., RV 1.59.7, RV 6.7- entire सूक्त, RV 6.8- entire सूक्त, RV 6.9.7). While RV3 is not the highest ranked in density of वैश्वानर, it comes second and is one of the few मण्डल-s (other than RV6 and RV7) with an over-representation of this Agni deity. Hence, we suggest that the कौशिक-s promulgated and popularized a version of the यज्ञ by presenting an installation of the global Agni वैश्वानर as जातवेदस्, who is local to the ritual being initiated at dawn.

Another notable word where RV3 shows unusual density is पुरोऌआश (RV dialect for पुरोडाश), the equivalent of the Iranian draonah, a flour cake made for the ritual. The baking and the offering of the पुरोऌआश appear to have been a central feature of the basic form of both the IA and Ir यज्ञ. Its prominence in RV3 suggests that the कौशिक-s probably contributed to making it a key feature of the यज्ञ. Interestingly, while several clans (including the कौशिक-s) offer the पुरोऌआश-s to Indra, only the कौशिक-s have an entire सूक्त pertaining to offering them to Agni जातवेदस् at the three soma offerings, morning, midday and evening, with गायत्री, त्रिष्टुभ् and जगती incantations followed by one for the अतित्रात्र. In the extant ritual, at these offerings, जातवेदस् is seen as manifesting as Agni स्विष्टकृत् --- the one who ensures that the ritual is correctly done. Thus, the centrality of जातवेदस् in the developing यज्ञ was accompanied by the incorporation of a special पुरोऌआश offering. Several of these actions in the "adhvara", namely the churning of the fire (manth-), making of the पुरोऌआश, and more generally the preparation of the soma are clearly related to the domain of the adhvaryu. Thus, even though the यज्ञ in the वैश्वामित्र मण्डल is still a होतृ-dominated tradition, it had laid the groundwork for the rise of the adhvaryu, which is seen in the tradition of the YV.

Parallel to the rise of the reformulated यज्ञ, was another distinct ritual development related to root IA yaj-/Ir yaz, viz., the rise of a certain type of formula, with the verb IA यजामहे and Ir yazamaide (we sacrifice to); e.g., त्र्यम्बकं यजामहे RV 7.59.12. This type of formula occurs only 10 times in the RV (RV1:  $6 \times$ , RV7:  $1 \times$ , RV8:  $1 \times$ , RV10:  $2 \times$ ). Given that it is almost entirely absent in the core family books of the RV, despite arising from the same root as यज्ञ, it might not be closely linked to the above-discussed developments. Similarly, it is not frequently found in the Yajurveda collections associated with the आध्वर्यव tradition either; e.g., तैत्तिरीय-संहिता  $7 \times$ ; तैत्तिरीय-ब्राह्मण  $6 \times$ . However, it occurs more commonly in the distinctive smaller Atharvan collections: AV-vulgate  $25 \times$ ; AV-पैप्पलाद  $30 \times$ . Interestingly, these instances in the AV संहिता-s closely parallel the cognate occurrences in the Yašt layer of the Avestan corpus. This formula was finally incorporated into the later synthetic elaboration of the यज्ञ in the form of the famous preamble incantation of the होतृ before he recites the ऋक् for the offering: "ye3 यजामहे". From these observations, we may infer that: (1) After the initial reformulation of the old IE श्रौत-class ritual as the यज्ञ in the ancestral I-Ir tradition, it underwent several temporally and focally distinct developments. (2) However, these developments tended to interact with each other repeatedly resulting in synthetic reformulations. (3) One development was the reformulation of the यज्ञ among the कौशिक-s and probably their Iranian counterparts (the स्पर्श-s mentioned by बौधायन? With regard to the possible Ir-कौशिक interaction one may also point to the special place of Mitra for both) gave rise to one form of the ritual. This provided the ground for the rise of the आध्वर्यव tradition. Another, which probably rose in the Atharvan tradition and its Iranic counterpart was the sacrificial offering with the formula "यजामहे". (4) The above two developments came together in a synthesis even before the Indo-Iranians split up. Hence, we argue that even though further synthesis might have happened both on the IA and Ir sides after they split, much of their branch of the IE religion developed prior to their split. A corollary to this, contrary to the mainstream reconstruction, is that the original core of much of the Vedic (and Avestan) scripture was composed not in their final destinations but much earlier on the steppe.

[यज्ञो हि त इन्द्र वर्धनो भूद्]{style="color: #0000ff"}\
[उत प्रियः सुतसोमो मियेधः ।]{style="color: #0000ff"}\
[यज्ञेन यज्ञम् अव यज्ञियः सन्]{style="color: #0000ff"}\
[यज्ञस् ते वज्रम् अहिहत्य आवत् ॥]{style="color: #0000ff"}\
Verily, o Indra, the यज्ञ became your magnifier,\
as also the dear soma libation, the ritual offering.\
With the यज्ञ aid the यज्ञ being the one who receives the यज्ञ;\
the यज्ञ aided your vajra in the slaying of Ahi.

  - विश्वामित्र, the son of गाथिन्, RV 3.32.12

