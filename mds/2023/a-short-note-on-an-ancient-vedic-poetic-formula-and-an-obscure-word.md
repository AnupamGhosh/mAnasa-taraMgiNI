
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A short note on an ancient Vedic poetic formula and an obscure word](https://manasataramgini.wordpress.com/2023/07/31/a-short-note-on-an-ancient-vedic-poetic-formula-and-an-obscure-word/){rel="bookmark"} {#a-short-note-on-an-ancient-vedic-poetic-formula-and-an-obscure-word .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 31, 2023]{.entry-date}](https://manasataramgini.wordpress.com/2023/07/31/a-short-note-on-an-ancient-vedic-poetic-formula-and-an-obscure-word/ "5:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A couple of ऋक्-s, respectively from मण्डल-s 6 and 7 of the ऋग्वेद, are rather striking for their parallel structure:
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/07/rv_stiya1.png){width="75%"}
```{=latex}
\end{center}
```



You are the bull of Heaven, the bull of Earth,\
the bull of the rivers, the bull of the "immobile/dense";\
For you, the manly, O Bull, the Indu has waxed,\
the sweet juice, the honey-drink, for your choice.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/07/rv_stiya2.png){width="75%"}
```{=latex}
\end{center}
```



Sought in Heaven, established on Earth,\
the leader of rivers, the bull of the "immobile/dense";\
He shines forth to the peoples of Manu,\
वैश्वानर waxing by his own choice.

The first ऋक् is by शंयु बार्हस्पत्य and is addressed to Indra, while the second is by वसिष्ठ मैत्रावरुणि and addressed to Agni वैश्वानर. Nevertheless, as can be seen above, they are structurally equivalent in the following ways: (1) In each case, the god is invoked in four stations, namely heaven, earth, the rivers, and something termed the स्तिया-s, which will be the focus of the discussion in the concluding part of this note. (2) In both cases, the respective gods are referred to as bulls (वृषभ) --- a common Vedic metaphor emphasizing their preeminence and manliness. (3) In the second hemistich, the word vara (choice, wish) is used. While it is declined differently in the two ऋक्-s, in both cases, there is a sense of the autonomous nature of the god in making the choice. (4) In the second hemistich of both ऋक्-s, there are distinct but semantically overlapping words meaning "waxing/growing/swelling". In the ऋक् to Indra, it is पीपाय that is often used in the context of the "waxing" of soma both in the sense of the moon (Indu) and the turgidification of the soma stalks before the extraction of the juice. In the one addressed to Agni, it is the perfect participle formation वावृधानः. (5) While in the ऋक् to Indra, the term वृषभ is repeatedly used, it can be seen as semantically overlapping with नेतृ used in the cognate position of the Agni ऋक्. It is not uncommon to see parallels between the compositions of the different ऋषि-s; hence, why do we make a big deal of this?

First, it is not a case of identical repetition of certain characteristic phrases (e.g., agnim ईऴे; भद्रं no api वातय मनः; मृल्(आ/यासि) नः) or a repeated refrain. Rather, the entire structure mentioned above is recapitulated, albeit in the context of different deities and with differing intents. Second, there is no special connection between the भरद्वाज-s of मण्डल 6 and the वसिष्ठ-s of मण्डल 7 beyond the fact that they were ऋषि-s of a common आर्यन् tradition. This is most starkly illustrated by looking at their associations with specific rulers. The table below shows the number of times the rulers Atithigva, दिवोदास and सुदास् are mentioned in the respective मण्डल-s. Most students of the RV agree that Atithigva is either the father or a dynastic title of दिवोदास.

![RV_stiyA3](https://manasataramgini.wordpress.com/wp-content/uploads/2023/07/rv_stiya3.png){width="75%"}

The link between the भरद्वाज-s and the Atithigva-दिवोदास line is in sharp contrast to that between वसिष्ठ and सुदास्. It is also notable that while there is at least one mention each of Atithigva and दिवोदास by the वसिष्ठ-s, there is no mention of सुदास् by the भरद्वाज-s. Irrespective of both their adherence to contrafactual ideas like autochthonous आर्यन्-s of India, and the mapping of Atithigva and दिवोदास to the homonymous kings mentioned in the dynastic lists of the पुराण-s, most students of the RV agree that they were temporally anterior to सुदास्. Thus, it would be safe to conclude that not only did the early भरद्वाज-s and वसिष्ठ-s not have any special connection, but also belonged to distinct temporal periods of the Early Vedic age. Hence, we believe that the parallels between the ऋक्-s presented above represent an old आर्यन् poetic formula with a special significance that allowed the portrayal of a god in a specific fashion, simultaneously embodying multiple key elements: (1) The universal lordship of the deity in association with both the heavenly realm and earth and the swiftly motile (rivers) and the "immovable/dense". This might have been used in a metaphorical sense similar to स्थावर-जङ्गम (immobile and mobile) used in the later registers of the language; (2) The sense of expansion; (3) the sense of self-choice, evidently with respect to the sacrificers (also apparently indicating the granting of boons --- vara-s to them).

In the above, we translated the word स्तिया noncommittally as immobile/dense, but can we infer what it really meant? Traditionally it has been taken to mean stagnant water bodies in contrast to rivers. To our knowledge, स्तिया is only attested in the early Vedic dialect and is not seen in the classical language. Hence, one cannot infer its actual meaning directly from continuity with the classical dialect. However, the "stagnancy" inherent in it can be literally etymologically inferred as the word is seen as derived from a root that was likely a paralog, already present in early Indo-European, of a more widely attested root (Skt: स्था < PIE \*steh $_2$ ; we have no particular opinion on the reconstructed laryngeal in this form as we have not studied that closely), which is equivalent to English "stand". This paralogous root is reconstructed as steyh $_2$  from which we likely have the Skt roots styai and स्त्या. The form स्त्यान, which is derived as a kta passive past participle of the root styai in traditional व्याकरण, is attested in the classical dialect and means viscous, coagulated or immobile. This is consistent with the inference of stagnancy behind स्तिया in the RV. A closer look at the descendants of steyh $_2$  in other IE languages presents an interesting picture. We have the following cognates:\
(1) Greek: stía= stone, pebble; stîon= stone, pebble; stiáōn= altar made of stones.\
(2) Germanic: Proto-form: \*stainaz > English: stone; stayn (Middle Eng.); German: Stein; Old Norse: Steinn.\
(3) Slavic: Proto-form: stena > Russian: stená= rock, stone wall/cliff; Serbo-Croatian: stijéna= rock;

Thus, from these distinct branches of IE, one would infer an ancestral meaning of the paralogous root steyh $_2$  as meaning stone; however, the size of the stone could range from a pebble to a rock. It is notable that this meaning survives in Slavic --- something relevant to Vedic since Balto-Slavic is a likely sister group of Indo-Iranian. Under this consideration, given the uncertain meaning of Vedic स्तिया, we would parsimoniously infer that it actually meant "rock/stone" as opposed to a stagnant water body. We now return to the RV to see if there is any support for this claim. We find that the term "rivers" as in the above ऋक्-s is also found in a parallel context in the below ऋक् of विश्वामित्र:

[मित्रो अग्निर् भवति यत् समिद्धो]{style="color: #0000ff"}\
[मित्रो होता वरुणो जातवेदाः ।]{style="color: #0000ff"}\
[मित्रो अध्वर्युर् इषिरो दमूना]{style="color: #0000ff"}\
[मित्रः सिन्धूनाम् उत पर्वतानाम् ॥]{style="color: #0000ff"} RV 3.5.4\
The ritual fire becomes Mitra when it is kindled;\
As the होतृ, he is Mitra, as जातवेदस् \[he is] वरुण.\
Mitra is the Adhvaryu, the strongman of the household,\
\[as also] is Mitra of the rivers and mountains.

Here, sacrificial fire is said to become Mitra when kindled, and as the manifestation of Agni that pertains to the ritual of the sacrificer (जातवेदस्), he is seen as वरुण. Thereafter this ritual fire in the form of Mitra is said to become the Adhvaryu and होतृ of the ritual, as also the lord of the household, the rivers and the mountains. This devotion to Mitra likely relates to the special place of the god for विश्वामित्र --- indeed, his own name is likely an early theophoric name signaling the universal nature of the god Mitra. In this context, it is not surprising the metaphors indicative of the lordship of the various stations are deployed in this ऋक्, similarly to those in the opening two ऋक्-s under discussion in this note. Thus, the expression सिन्धूनाम् uta पर्वतानाम् can be seen as an equivalent of सिन्धूनाम् ... स्तियानाम् in those ऋक्-s. Further, a comparable juxtaposition of rivers and mountains can be seen in a ऋक् of the Father of our people, Manu, to विष्णु. Here the word sindhu for river is replaced by नदी:\
[\
शर्म पर्वतानां वृणीमहे नदीनाम् । आ विष्णोः सचाभुवः ॥]{style="color: #0000ff"} RV 8.31.10\
We choose the shelter of the mountains and of the rivers, of विष्णु, who stands \[with us] as a helper.

A comparable coupling of the rivers and mountains is also seen in the वैश्वदेव incantations of the Atri-s and वसिष्ठ-s:\
[उत त्ये नः पर्वतासः सुशस्तयः सुदीतयो नद्यस् त्रामणे भुवन् ।]{style="color: #0000ff"}RV 5.46.6a\
[शं नः पर्वता ध्रुवयो भवन्तु शं नः सिन्धवः शम् उ सन्त्व् आपः ॥]{style="color: #0000ff"} RV 7.35.8c\
In the second of these, the immobility of the mountains is explicitly mentioned, evidently in contrast to the flow of the rivers.

Based on the multiple juxtapositions of rivers and mountains in the early layer of the Vedic tradition, along with the IE tradition indicating that the ancestral meaning of the noun derived from the root steyh $_2$  is likely to have been stone, we cautiously propose that स्त्íया (उदात्त indicated) meant stone and by metonymy mountain. Indeed, in the RV itself, we see a similar metonymic duality between stone and mountain in the word adri. This is made explicit in RV 10.94.1, where the soma-pounding stones (adri-s) are explicitly also called mountains (parvata-s). Thus, rather than being stagnant water, we believe that the Vedic स्त्íया preserved in old IE meaning. Curiously, a juxtaposition of the waters and the stone is seen in a name of Germanic provenance, Epstein, that is widely borne by paleo-Abrahamists in our day (e.g., separately, a virologist and a criminal who was mysteriously eliminated). Apparently, it was originally a place name in Northwestern Germany --- the Stein part is obvious and meant mountain by metonymy --- we believe Ep is from an IE word that might have been Apa (= Skt apas). The root of the latter might have originally been from Celtic given the name Abiona (also Old Irish aub for river) found in continental Celtic inscriptions, which appears to have signified a water/river goddess. Alternatively, it was from a Western Baltic tongue, given the Old Prussian word "ape"=river. Thus, the German place name likely meant "river-mountain".

