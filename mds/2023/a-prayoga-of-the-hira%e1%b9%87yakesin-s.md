
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A prayoga of the हिरण्यकेशिन्-s](https://manasataramgini.wordpress.com/2023/11/19/a-prayoga-of-the-hira%e1%b9%87yakesin-s/){rel="bookmark"} {#a-prayoga-of-the-हरणयकशन-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 19, 2023]{.entry-date}](https://manasataramgini.wordpress.com/2023/11/19/a-prayoga-of-the-hira%e1%b9%87yakesin-s/ "7:32 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

To our knowledge, the हिरण्यकेशिन् branch of the तैत्तिरीयक-s is today only found in pockets in महाराष्ट्र and the द्राविड states. While they follow the तैत्तिरीय-श्रुति like other तैत्तिरीयक-s, they additionally have their own unique मन्त्रपाठ furnished with its own little ऋक्संहिता. They have a unique procedure for the गृह्य rite of पुण्याहवाचन, which combines both Vaidika and laukika, i.e., पौराणिक elements. A हिरण्यकेशिन् from the मरहट्ट country, who was unaware of their मन्त्रपाठ, wanted advice on performance of the पुण्याहवाचन and मातृका- and laghu-deva-पुजा-s for which he had a short prayoga-paddhati handed down in his family, but did not know some of the mantra-s specified only by प्रतीक therein. Hence, we used his paddhati to spell out all the mantra-s (without svara-s for the Vaidika ones). We present it here as it has some notable features. The paddhati itself is similar to a longer version we had seen in the manual of a learned ब्राह्मण from the same region (it borrows from बोधायन in multiple places where evidently the हिरण्यकेशिन् counterpart was either absent or sketchy). The longer version differs from the below in packing several additional Vaidika mantra-s into the पुण्याह recitation. At the end of the rite, both this short paddhati and the long one I had seen earlier provide a मातृका-पुजा without details of essentials such as प्राणप्रतिष्ठा and षोडशोपचार-पूजा (the longer manual has प्राणप्रतिष्ठा independently as a section later in the text). The long manual follows this up with a नान्दी-prayoga and an अङ्कुरारोपण. Our interlocutor's short manual used mantra-s corresponding to अङ्कुरारोपण for a laghu-deva-पूजा to the five gods प्रजापति, Indra, Yama, वरुण and Soma, followed by a तर्पण to longer list of deities. This longer list of deities is offered आहुति-s as part of the Grahamakha (the planetary fire-rite of the हिरण्यकेशिन्-s) in the longer manual. His objective was to perform the पुण्याहवाचन and the subsequent मातृका- and laghu-deva-पुजा-s, which we were able to successfully advise him on. More generally, we believe that this could be a model for a generic deva-पुजा by a non-sectarian lay Hindu.

[PDF of Devanagari script encoding of the text.](https://manasataramgini.wordpress.com/wp-content/uploads/2023/11/hiranyakeshin_punyahetyadi_skt.pdf)

[अथ हिरण्यकेशिनाम् पुण्याहवाचन-विधि :]{style="color: #993300"}\
[हरिद्रा-बिम्बे गणपति-स्थापनम् :]{style="color: #0000ff"}\
[गणानां त्वा गणपति ँ हवामहे कविं कवीनाम् उपमश्रवस्तमम् ।]{style="color: #0000ff"}\
[ज्येष्ठराजम् ब्रह्मणाम् ब्रह्मणस् पत आ नः शृण्वन्न् ऊतिभिः सीद सादनम् ॥]{style="color: #0000ff"}

[पुष्पाञ्जलिं दत्वा सर्वोपचारैः संपूज्य :]{style="color: #0000ff"}\
[मन्त्रहीनं क्रियाहीनं भक्तिहीनं गजानन ।]{style="color: #0000ff"}\
[yat पूजितं मया deva परिपूर्णं tad astu me ॥]{style="color: #0000ff"}

[विघ्नेश्वराय वरदाय सुरप्रियाय]{style="color: #0000ff"}\
[लम्बोदराय विकटाय गजाननाय ।]{style="color: #0000ff"}\
[विनायकाय श्रुति-यज्ञविभूषिताय]{style="color: #0000ff"}\
[गौरी-सुताय गणनाथा नमो नमस् ते ॥]{style="color: #0000ff"}

Notes: This section is the classic invocation of विनायक in a clod of turmeric. It became a staple opening for स्मार्त rites with the rise of the पौराणिक register of the religion. Hence, this part can be assigned to the classic पौराणिक period.

[कलष-स्थापनम् :]{style="color: #993300"}\
[कलशत्रयं स्थापयेत् ।]{style="color: #0000ff"}

[मही द्यौः पृथिवी च न इमं यज्ञम् मिमिक्षताम् ।]{style="color: #0000ff"}\
[पिपृतां नो भरीमभिः ॥]{style="color: #0000ff"}

[तयोः पञ्चरत्नानि प्रक्षिपेत् :]{style="color: #0000ff"}\
[युवा सुवासाः परिवीत आगात् । स उ श्रेयान् भवति जायमानः । तं धीरासः कवय उन्नयन्ति । स्वाधियो मनसा देवयन्तः ।]{style="color: #0000ff"}

[कलशोपरि वस्त्रवेष्टनं कृत्वा :]{style="color: #0000ff"}\
[पूर्णा दर्वि परा पत सुपूर्णा पुनर् आ पत ।]{style="color: #0000ff"}\
[वस्नेव वि क्रीणावहा इषम् ऊर्ज ँ शतक्रतो ॥]{style="color: #0000ff"}

[कलशे वरुणस्यावाहनम् :]{style="color: #0000ff"}\
[तत् त्वा यामि ब्रह्मणा वन्दमानस् तद् आ शास्ते यजमानो हविर्भिः ।]{style="color: #0000ff"}\
[अहेडमानो वरुणेह बोध्य् उरुश ँस मा न आयुः प्र मोषीः ॥]{style="color: #0000ff"}

[कलश-मुखे अप आवाहयेत् :]{style="color: #0000ff"}\
[सर्वे समुद्राः सरितस् तीर्थानि जलदा नदाः ।]{style="color: #0000ff"}\
[आयान्तु मम शान्त्य् अर्थं दुरित-क्षय-कारकाः ॥]{style="color: #0000ff"}

[अभिमन्त्रणम् :]{style="color: #0000ff"}\
[द्रविणोदा द्रविणसस् तुरस्य द्रविणोदाः सनरस्य प्र यंसत् ।]{style="color: #0000ff"}\
[द्रविणोदा वीरवतीम् इषं नो द्रविणोदा रासते दीर्घम् आयुः ॥]{style="color: #0000ff"}

[नवोनवो भवति जायमानो 'ह्नां केतुर् उषसाम् एत्य् अग्रे ।]{style="color: #0000ff"}\
[भागं देवेभ्यो वि दधात्य् आयन् प्र चन्द्रमास् तिरति दीर्घम् आयुः ॥]{style="color: #0000ff"}

[उच्चा दिवि दक्षिणावन्तो अस्थुर् ये अश्वदाः सह ते सूर्येण ।]{style="color: #0000ff"}\
[हिरण्यदा अमृतत्वम् भजन्ते वासोदाः सोम प्र तिरन्त आयुः ॥]{style="color: #0000ff"}

Notes: This part features the installation of the pitchers and the invocation of the god वरुण in them. It follows the general pattern seen across स्मार्त rites of other traditions. Given the importance of the god वरुण in the Indo-Iranian tradition, some version of it could have gone back to an early predecessor of स्मार्त household rites.

[प्रधानाङ्गम् :]{style="color: #993300"}\
[शान्तिर् अस्तु १ । पुष्टिर् अस्तु २ । तुष्टिर् अस्तु ३ । वृद्धिर् अस्तु ४ । अविघ्नम् अस्तु ५ । आयुष्यम् अस्तु ६ । आरोग्यम् अस्तु ७ । शिवं कर्मास्तु ८ ।कर्म-संऋद्धिर् अस्तु ९ । धर्म-संऋद्धिर् अस्तु १० । वेद-संऋद्धिर् अस्तु ११ ।शास्त्र-संऋद्धिर् अस्तु १२ । पुत्र-संऋद्धिर् अस्तु १३ । धन-धान्य-संऋद्धिर् अस्तु १४ । इष्ट-संपद् अस्तु १५ । अरिष्ट-निरसनम् अस्तु १६ । यत् पापं तत् प्रतिहतम् अस्तु १७ । यच् छ्रेयस् तद् अस्तु १८ । उत्तरे कर्मण्य् अविघ्नम् अस्तु १९ । उत्तरोत्तरम् अहर् अहर् अभिवृद्धिर् अस्तु २० । उत्तरोत्तराः क्रियाः शुभाः शोभनाः सम्पद्यन्ताम् २१ । तिथि-करण-मुहूर्त-नक्षत्र-सम्पद् अस्तु २२ । तिथि-करण-मुहूर्त-नक्षत्र-ग्रह-लग्नाधि-देवताः प्रीयन्ताम् २३ । तिथि-करणे मुहूर्त-नक्षत्रे सग्रहे सदैवते प्रीयेताम् २४ । दुर्गा-पाञ्चाल्यौ प्रीयेताम् २५ । अग्नि-पुरोगा विश्वे देवाः प्रीयन्ताम् २६ । इन्द्र-पुरोगा मरुद्गणाः प्रीयन्ताम् २७ । ब्रह्मपुरोगाः सर्वे वेदाः प्रीयन्ताम् २८ । विष्णु-पुरोगाः सर्वे देवः प्रीयन्ताम् २९ । माहेश्वरी-पुरोगा उमा-मातरः प्रीयन्ताम् ३० । वसिष्ठ-पुरोगा ऋषि-गणाः प्रीयन्ताम् ३१ । अरुन्धती-पुरोगा एकपत्न्यः प्रीयन्ताम् ३२ । ऋषयश् छन्दांस्य् आचार्या देवा वेदा यज्ञाश् च प्रीयन्ताम् ३३ । ब्रह्म च ब्राह्मणाश् च प्रीयन्ताम् ३४ । श्री-सरस्वत्यौ प्रीयेताम् ३५ । श्रद्धा-मेधे प्रीयेताम् ३६ । भगवती कात्यायनी प्रीयताम् ३७ । भगवती माहेश्वरी प्रीयताम् ३८ । भगवती पुष्टिकरी प्रीयताम् ३९ । भगवती तुष्टिकरी प्रीयताम् ४० । भगवती ऋद्धिकरी प्रीयताम् ४१ । भगवती वृद्धिकरी प्रीयताम् ४२ । भगवन्तौ विघ्न-विनायकौ प्रियेताम् ४३ । भगवान् स्वामी महासेनः सपत्नीकः ससुतः सपार्षदः सर्व-स्थान-गतः प्रीयताम् ४४ । हरि-हर-हिरण्यगर्भाः प्रीयन्ताम् ४५ । सर्वा ग्रम-देवताः प्रीयन्ताम् ४६ । सर्वा कुल-देवताः प्रीयन्ताम् ४७ । सर्वा इष्ट-देवताः प्रीयन्ताम् ४८ । हता ब्रह्म-द्विषः ४९ । हताः परिपन्थिनः ५० । हता अस्य कर्ंअणो विघ्न-कर्तारः ५१ । शत्रवः पराभवं यान्तु ५२ । शाम्यन्तु घोराणि ५३ । शाम्यन्तु पापानि ५४ । शाम्यन्त्व् ईतयः ५५ । शुभानि वर्धन्ताम् ५६ । शिवा आपः सन्तु ५७ । शिवा ऋतवः सन्तु ५८ । शिवा अग्नयः सन्तु ५९ । शिवा आहुतयः सन्तु ६० । शिवा ओषध्यः सन्तु ६१ । शिवा वनस्पतयः सन्तु ६२ । शिवा अतिथयः सन्तु ६३ । अहोरात्रे शिवे स्याताम् ६४ । निकामे निकामे नः पर्जन्यो वर्षतु ६५ । फलिन्यो न ओषधयः पच्यन्ताम् ६६ । योग-क्षेमो नः कल्पताम् ६७ । आदित्य-पुरोगाः सर्वे ग्रहाः प्रीयन्ताम् ६८ । भगवान् नारायणः प्रीयताम् ६९ । भगवान् पर्जन्यः प्रीयताम् ७० । भगवान् स्वामी महासेनः प्रीयताम् ७१ ॥]{style="color: #0000ff"}

[प्रतिवाक्यम् पात्रे जलम् पातयेत् । तत्र+अरिष्ट-निरसनम् अस्तु, यत् पापं तत् प्रतिहतम् अस्तु इति द्वाभ्यां वाक्याभ्यां हता ब्रह्म-द्विष इत्य् आदिभिः सप्तभिर् वाक्यैश् च पात्राद् बहिर् उत्तरतो जलम् पातनीयम् ।]{style="color: #0000ff"}

Notes: This section relates to the gratification of various deities with water offerings, which is related to what in other traditions is called the पौराणिक-पुण्याहवचन. However, it differs from that in having a longer list of statements, including several unique ones. Despite its apparently पौराणिक origins, we believe it has elements from a "transitional" period of the tradition as it includes certain Vaidika tendencies like the invocation of Agni and Indra with the Marut-s at the opening of the list of gods. It is also notable in multiple ways. It invokes दुर्गा together with पाञ्चाली. This is a unique combination that is not seen elsewhere. It also seems to be the first mention of the द्रौपदी cult that still survives in popular form in pockets of the द्राविड country (e.g., among the Vahniyar-s). Interestingly, this द्राविड cult of पाञ्चाली also intersects with their popular cult of दुर्गा. Also notable are the mentions of Skanda (and his परिवार). While the worship of कुमार faded away in the मरहट्ट country, some remnants of it survived among the ब्राह्मण-s of the कोङ्कण-s, like the पेश्व-s of the मरहट्ट empire. Given that many of these were हिरण्यकेशिन्-s, it is possible that rites such as this kept the memory of his worship alive. The dual विनायक-s are also an interesting peculiarity of this ritual.

[स्वस्ति-वचनम् :]{style="color: #993300"}\
[स्वस्ति न इन्द्रो वृद्धश्रवाः स्वस्ति नः पूषा विश्ववेदाः ।]{style="color: #0000ff"}\
[स्वस्ति नस् तार्क्ष्यो अरिष्टनेमिः स्वस्ति नो बृहस्पतिर् दधातु ॥]{style="color: #0000ff"}

[स्वस्ति नो मिमीताम् अश्विना भगः स्वस्ति देव्य् अदितिर् अनर्वणः ।]{style="color: #0000ff"}\
[स्वस्ति पूषा असुरो दधातु नः स्वस्ति द्यावापृथिवी सुचेतुना ॥]{style="color: #0000ff"}\
[स्वस्तये वायुम् उप ब्रवामहै सोमं स्वस्ति भुवनस्य यस् पतिः ।]{style="color: #0000ff"}\
[बृहस्पतिं सर्वगणं स्वस्तये स्वस्तय आदित्यासो भवन्तु नः ॥]{style="color: #0000ff"}\
[विश्वे देवा नो अद्या स्वस्तये वैश्वानरो वसुर् अग्निः स्वस्तये ।]{style="color: #0000ff"}\
[देवा अवन्त्व् ऋभवः स्वस्तये स्वस्ति नो रुद्रः पात्व् अंहसः ॥]{style="color: #0000ff"}\
[स्वस्ति मित्रावरुणा स्वस्ति पथ्ये रेवति ।]{style="color: #0000ff"}\
[स्वस्ति न इन्द्रश् चाग्निश् च स्वस्ति नो अदिते कृधि ॥]{style="color: #0000ff"}\
[स्वस्ति पन्थाम् अनु चरेम सूर्याचन्द्रमसाव् इव ।]{style="color: #0000ff"}\
[पुनर् ददताघ्नता जानता सं गमेमहि ॥]{style="color: #0000ff"}

[श्रिये जातः श्रिय आ निर् इयाय श्रियं वयो जरितृभ्यो दधाति ।]{style="color: #0000ff"}\
[श्रियं वसाना अमृतत्वम् आयन् भवन्ति सत्या समिथा मितद्रौ ॥]{style="color: #0000ff"}

[श्रीर् अस्तु ( $\times$ ३) । वर्ष-शतम् पूर्णम् अस्तु । मङ्गलानि भवन्तु । शिवं कर्मास्तु ।]{style="color: #0000ff"}

Notes: This section is entirely Vedic illustrating the hybrid nature of the हिरण्यकेशिन् ritual.

[दक्षिण-हस्ते उत्तर-कलशं दक्षिण-कलशं वाम-हस्ते गृहीत्वा ताभ्यां धाराद्वयं सततम् पत्रे निषिञ्चेत् ।]{style="color: #0000ff"}

[वास्तोष् पते प्रति जानीह्य् अस्मान्त् स्वावेशो अनमीवो भवा नः ।]{style="color: #0000ff"}\
[यत् त्वेमहे प्रति तन् नो जुषस्व शं न एधि द्विपदे शं चतुष्पदे ॥]{style="color: #0000ff"}

[वास्तोष् पते शग्मया स ँसदा ते सक्षीमहि रण्वया गातुमत्या ।]{style="color: #0000ff"}\
[आवः क्षेम उत योगे वरं नो यूयम् पात स्वस्तिभिः सदा नः ॥]{style="color: #0000ff"}

[अमीवहा वास्तोष् पते विश्वा रूपाण्य् आविशन् ।]{style="color: #0000ff"}\
[सखा सुशेव एधि नः ॥]{style="color: #0000ff"}

[नमो रुद्राय वास्तोष्पतये ॥]{style="color: #0000ff"}

[शिवं शिवं शिवम् ॥]{style="color: #0000ff"}

Notes: the worship of Rudra वास्तोष्पति in this context is a distinctive aspect of this tradition.

[पात्रे पातितेन जलेन पल्लव-दूर्वाभिर् उदङ् मुखास् तिष्ठन्त उपविष्टा वा परिवारम् अभिषिञ्चेयः ।]{style="color: #0000ff"}

[समुद्रज्येष्ठाः सलिलस्य मध्यात् पुनाना यन्त्य् अनिविशमानाः ।]{style="color: #0000ff"}\
[इन्द्रो या वज्री वृषभो रराद ता आपो देवीर् इह माम् अवन्तु ॥]{style="color: #0000ff"}

[देवस्य त्वा सवितुः प्रसवे 'श्विनोर् बाहुभ्याम् पूष्णो हस्ताभ्या ँ सरस्वत्यै वाचो यन्तुर् यन्त्रेणाग्नेस् त्वा साम्राज्येनाभि षिञ्चामीन्द्रस्य बृहस्पतेस् त्वा साम्राज्येनाभि षिञ्चामि ॥]{style="color: #0000ff"}\
[देवस्य त्वा सवितुः प्रसवे । अश्विनोर् बाहुभ्याम् । पूष्णो हस्ताभ्याम् । अश्विनोर् भैषज्येन । तेजसे ब्रह्मवर्चसाय+अभिषिञ्चामि । देवस्य त्वा सवितुः प्रसवे । अश्विनोर् बाहुभ्याम् । पूष्णो हस्ताभ्याम् । सरस्वत्यै भैषज्येन ॥]{style="color: #0000ff"}\
[देवस्य त्वा सवितुः प्रसवे । अश्विनोर् बहुभ्याम् । पूष्णो हस्ताभ्याम् । इन्द्रस्येन्द्रियेण । श्रियै यशसे बलायाभिषिञ्चामि ॥]{style="color: #0000ff"}

[ॐ वरुणाय नमः । वरुणम् उद्वासयामि । यथा स्थानम् प्रतिष्ठापयामि शोभनार्थाय क्षेमाय पुनर् आगमनाय च ॥]{style="color: #0000ff"}

[भूर् भुवः सुवः ।]{style="color: #0000ff"}\
[तच् छंयोर् आवृणीमहे गातुं यज्ञाय गातुं यज्ञपतये ।]{style="color: #0000ff"}\
[दैवी स्वस्तिर् अस्तु नः स्वस्तिर् मानुषेभ्यः ।]{style="color: #0000ff"}\
[ऊर्ध्वं जिगातु भेषजं शं नो अस्तु द्विपदे शं चतुष्पदे ॥]{style="color: #0000ff"}

Notes: This Vaidika section largely follows the pattern seen in other स्मार्त traditions.

[अथ मातृका-पूजा :]{style="color: #800000"}\
[शुचौ रङ्गवल्ल्याद्य् अलंकृते देशे कृतग्न्य् उत्तारण-प्राण-प्रतिष्ठासु प्रथिमासु अभावे 'क्षत-पुञ्जेषु वा गौर्यादि-देवता आवाहयेत् ।]{style="color: #0000ff"}\
[गौर्यै नमः । गौरीम् आवाहयामि । पद्मावत्यै नमः । पद्मावतीम् आवाहयामि । शच्यै नमः । शचीम् आवाहयामि । मेधायै नमः । मेधाम् आवाहयामि । सावित्र्यै नमः । सावित्रीम् आवाहयामि । विजयायै नमः । विजयाम् आवाहयामि । जयायई नमः । जयाम् आवाहयामि । देवसेनायै नमः । देवसेनाम् आवाहयामि । स्वधायै नमः । स्वधाम् आवाहयामि । स्वाहायै नमः । स्वाहाम् आवाहयामि । मातृभ्यो नमः । मातॄर् आवाहयामि । लोकमातृभ्यो नमः । लोकमातॄर् आवाहयामि । धृत्यै नमः । धृतिम् आवाहयामि । पुष्ट्यै नमः । पुष्टिम् आवाहयामि । तुष्ट्यै नमः । तुष्टिम् आवाहयामि । कुलदेवतायै । कुलदेवताम् आवाहयामि । ब्राह्म्यै नमः । ब्राह्मीम् आवाहयामि । माहेश्वर्यै नमः । माहेश्वरीम् आवाहयामि । कौमार्यै नमः । कौमारीम् आवाहयामि । वैष्णव्यै नमः । वैष्णवीम् आवाहयामि । वाराह्यै नमः । वाराहीम् आवाहयामि । इन्द्राण्यै नमः । इन्द्राणीम् आवाहयामि । चामुण्डायै नमः । चामुण्डाम् आवाहयामि । गणाधिपाय नमः । गणाधिपम् आवाहयामि । दुर्गायै नमः । दुर्गाम् आवाहयामि । क्षेत्रपालाय नमः । क्षेत्रपालम् आवाहयामि । वास्तोष्पतये नमः । वास्तोष्पतिम् आवाहयामि ॥]{style="color: #0000ff"}

[इत्य् आवाहयेत् । ततो गौर्याद्य् आवाहित-देवताभ्यो नमो नम इति षोडशोपचारैः प्रपूजयेत् ॥]{style="color: #0000ff"}

Notes: The मातृ-s invoked here belong to two groups. The first are a set of goddesses similar to a list seen in the शुक्लयजुर्वेदीय गृह्य-परिशिष्ट tradition. They include consorts of Rudra, विष्णु, Indra, प्रजापति, कुमार, Agni and पुषन् among others. The second set is comprised of the classical सप्तमातृ-s accompanied by गणेश, क्षेत्रपाल and Rudra वास्तोष्पति. That pattern is mimicked in iconography where the सप्तमातृ-s are typically accompanied by गणेश and Rudra/वीरभद्र.

[अथ लघु-देव-पूजा :]{style="color: #993300"}\
[ब्रह्मादि-देवताः प्रियन्ताम् इति पात्रात् जलं क्षिप्त्वा शुचौ देशे चतुरश्रं मण्डलं विधाय रङ्गवल्ल्यादिभिर् अलंकृत्य श्वेताक्षतान् संप्रकीर्य+अद्भिर् अभ्युक्ष्य प्राण-प्रतिष्ठासु प्रथिमासु अभावे 'क्षत-पुञ्जेषु वा ब्रह्मादि-देवता आवाहयेत् ।]{style="color: #0000ff"}\
[ॐ भूर् ब्रह्माणम् आवाहयामि । ॐ भुवश् चतुर्मुखं ब्रह्माणम् आवाहयामि । ॐ सुवः प्रजापतिं ब्रह्माणम् आवाहयामि । ॐ भूर् भुवः सुवर् हिरण्यगर्भं ब्रह्माणम् आवाहयामि ।]{style="color: #0000ff"}\
[इति मध्यमे स्थाने ब्रह्माणम् आवाहयामि ॥]{style="color: #0000ff"}

[हिरण्यगर्भः सम् अवर्तताग्रे भूतस्य जातः पतिर् एक आसीत् ।]{style="color: #0000ff"}\
[स दाधार पृथिवीं द्याम् उतेमां कस्मै देवाय हविषा विधेम ॥]{style="color: #0000ff"}\
[प्रजापते आगच्छ ।]{style="color: #0000ff"}

[प्रजापते न त्वद् एतान्य् अन्यो विश्वा जातानि परि ता बभूव ।]{style="color: #0000ff"}\
[यत् कामास् ते जुहुमस् तन् नो अस्तु वय ँ स्याम पतयो रयीणाम् ॥]{style="color: #0000ff"}\
[प्रजापतिं तर्पयामि ॥]{style="color: #0000ff"}

[ॐ भूर् वज्रिणम् आवाहयामि । ॐ भुवः शचीपतिम् आवाहयामि । ॐ सुवर् इन्द्रम् आवाहयामि । ॐ भूर् भुवः सुवः शतक्रतुम् आवाहयामि ।]{style="color: #0000ff"}\
[इति पूर्वस्यां दिशि वज्रिणम् आवाहयामि ॥]{style="color: #0000ff"}

[यत इन्द्र भयामहे ततो नो अभयं कृधि ।]{style="color: #0000ff"}\
[मघवन् छग्धि तव तन् न ऊतये वि द्विषो वि मृधो जहि ॥]{style="color: #0000ff"}\
[indra आगच्छ ।]{style="color: #0000ff"}

[स्वस्तिदा विशस् पतिर् वृत्रहा विमृधो वशी ।]{style="color: #0000ff"}\
[वृषेन्द्रः पुर एतु नः स्वस्तिदा अभयंकरः ॥]{style="color: #0000ff"}\
[इन्द्रं तर्पयामि ॥]{style="color: #0000ff"}

[ॐ भूर् यमम् आवाहयामि । ॐ भुवो वैवस्वतम् आवाहयामि । ॐ सुवः पितृपतिम् आवाहयामि । ॐ भूर् भुवः सुवः प्रेतपतिम् आवाहयामि ।]{style="color: #0000ff"}\
[इति दक्षिणस्यां दिशि यमम् आवाहयामि ॥]{style="color: #0000ff"}

[त्रिकद्रुकेभिः पतति षड् उर्वीर् एकम् इद् बृहत् ।]{style="color: #0000ff"}\
[त्रिष्टुब् गायत्री छन्दा ँसि सर्वा ता यम आहिता ॥]{style="color: #0000ff"}\
[यम आगच्छ ।]{style="color: #0000ff"}

[यद् उलूको वदति मोघम् एतद् यत् कपोतः पदम् अग्नौ कृणोति ।]{style="color: #0000ff"}\
[यस्य दूतः प्रहित एष एतत् तस्मै यमाय नमो अस्तु मृत्यवे ॥]{style="color: #0000ff"}\
[यमं तर्पयामि ॥]{style="color: #0000ff"}

[ॐ भूर् वरुणम् आवाहयामि । ॐ भुवः प्रचेतसम् आवाहयामि । ॐ सुवः सुरूपिणम् आवाहयामि । ॐ भूर् भुवः सुवर् अपाम्पतिम् आवाहयामि ।]{style="color: #0000ff"}\
[इति पश्चिमायाम् दिशि वरुणम् आवाहयामि ॥]{style="color: #0000ff"}

[इमम् मे वरुण श्रुधी हवम् अद्या च मृडय । त्वाम् अवस्युर् आ चके ॥]{style="color: #0000ff"}\
[वरुण आगच्छ ।]{style="color: #0000ff"}

[तत् त्वा यामि ब्रह्मणा वन्दमानस् तद् आ शास्ते यजमानो हविर्भिः ।]{style="color: #0000ff"}\
[अहेडमानो वरुणेह बोध्य् उरुश ँस मा न आयुः प्र मोषीः ॥]{style="color: #0000ff"}\
[वरुणं तर्पयामि ॥]{style="color: #0000ff"}

[ॐ भूः सोमम् आवाहयामि आवाहयामि । ॐ भुव इन्दुम् आवाहयामि । ॐ सुवर् निशाकरम् आवाहयामि । ॐ भूर् भुवः सुवः शशिनम् आवाहयामि ।]{style="color: #0000ff"}\
[इत्य् उत्तरस्यं दिशि सोमम् आवाहयामि ॥]{style="color: #0000ff"}

[सोमो धेनु ँ सोमो अर्वन्तम् आशुं सोमो वीरं कर्मण्यं ददातु ।]{style="color: #0000ff"}\
[सादन्यं विदथ्य ँ सभेयम् पितुःश्रवणं यो ददाशद् अस्मै ॥]{style="color: #0000ff"}\
[soma आगच्छ ।]{style="color: #0000ff"}

[अषाढं युत्सु पृतनासु पप्रि ँ स्वर्षाम् अप्सां वृजनस्य गोपाम् ।]{style="color: #0000ff"}\
[भरेषुजा ँ सुक्षिति ँ सुश्रवसं जयन्तं त्वाम् अनु मदेम सोम ॥]{style="color: #0000ff"}\
[सोमं तर्पयामि ॥]{style="color: #0000ff"}

[ॐ आपो हि ष्टा मयोभुवः ... । हिरण्यवर्णाः शुचयः ... मोर्जयन्त्या पुनातु ।]{style="color: #0000ff"}

[ब्रह्मादीन् संस्नापयेत् । पुष्पाञ्जलि-समर्पणम् ॥]{style="color: #0000ff"}

[दिशाम् पतीन् नमस्यामि सर्वकाम-फल-प्रदान् ।]{style="color: #0000ff"}\
[कुर्वन्तु सफलं कर्ं कुवन्तु सततं शुभम् ॥]{style="color: #0000ff"}

[ईश्वरं तर्पयामि । उमां तर्पयामि । स्कन्दं तर्पयामि । विष्णुं तर्पयामि । ब्रह्माण ँ स्वयभुवं तर्पयामि । इन्द्रं तर्पयामि । यमं तर्पयामि । कालं तर्पयामि । चित्रगुप्तं तर्पयामि । अपस् तर्पयामि । भूमिं तर्पयामि । विष्णुं तर्पयामि । इन्द्रं तर्पयामि । इन्द्राणीं तर्पयामि । प्रजापतिं तर्पयामि । सर्पान् तर्पयामि । ब्रह्माणं तर्पयामि । गणपतिं तर्पयामि । दुर्गां तर्पयामि । वायुं तर्पयामि । आकाशं तर्पयामि । अश्विनौ तर्पयामि । वास्तोष्पतिं तर्पयामि । क्षेत्रपालं तर्पयामि । इन्द्रं तर्पयामि । अग्निं तर्पयामि । यमं तर्पयामि । निरृत्तिं तर्पयामि । वरुणं तर्पयामि । वायुं तर्पयामि । सोमं तर्पयामि । ईशानं तर्पयामि । वसून् तर्पयामि । रुद्रान् तर्पयामि । आदित्यान् तर्पयामि । अश्विनौ तर्पयामि । विश्वान् देवान् तर्पयामि । यक्षान् तर्पयामि । सर्पान् तर्पयामि । गन्धर्वाप्सरसस् तर्पयामि । स्कन्दं तर्पयामि । नन्दीश्वरं तर्पयामि । महाकालं तर्पयामि । दक्षं तर्पयामि । दुर्गां तर्पयामि । विष्णुं तर्पयामि । मृत्यु-रोगान् तर्पयामि । गणपतिं तर्पयामि । अपस् तर्पयामि । मरुतस् तर्पयामि । पृथिवीं तर्पयामि । नदीन् तर्पयामि । सागरान् तर्पयामि । मेरुं तर्पयामि । ऐन्द्रीं तर्पयामि । कौमारीं तर्पयामि । ब्राह्मीं तर्पयामि । वाराहीं तर्पयामि । चामुण्डां तर्पयामि । वैष्णवीं तर्पयामि । महेश्वरीं तर्पयामि । वैनायकीं तर्पयामि ॥]{style="color: #0000ff"}

[पूर्वोक्तैर् ॐ भूर् ब्रह्माणम् इत्य् आद्यैर् मन्त्रैर् उद्वासयामि+इत्य् ऊहितैर् यथाक्रमं देवता उद्वासयेत् ॥]{style="color: #0000ff"}

Notes: The initial invocation of प्रजापति, Indra, Yama, वरुण and Soma in a मण्दल reminds one of the early मण्डल worship seen in the अथर्ववेदीय परिशिष्ट-s. It is notable that, as in certain early texts, Soma takes the northern position, unlike Kubera in the later texts. This and the मातृ-पुजा provide glimpses of the early iconic worship of the Hindus. The longer तर्पण list at the end of this section includes deities from both the old Vedic and पौराणिक pantheons. Some notable features include the worship of Nandin and महाकाल, which probably marks one of the early instances of the worship of these deities unique to the शैव pantheon in a स्मार्त context. The मातृ-s in this list differ from the above ritual in being an अष्टमातृका group via the inclusion of वैनायकी. The inclusion of वैनायकी in the अष्टमातृका is uncommon but is iconographically attested in some मातृ depictions, such as in भूलेश्वर, महाराष्ट्र. One again wonders if the presence of this tradition in the region influenced the local iconographic depictions of this goddess.

