
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Notes on the वैष्णव retinue deities-1](https://manasataramgini.wordpress.com/2023/12/26/notes-on-the-vai%e1%b9%a3%e1%b9%87ava-retinue-deities-1/){rel="bookmark"} {#notes-on-the-वषणव-retinue-deities-1 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 26, 2023]{.entry-date}](https://manasataramgini.wordpress.com/2023/12/26/notes-on-the-vai%e1%b9%a3%e1%b9%87ava-retinue-deities-1/ "7:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The शैव pattern of worship, which is especially emphasized in the इशान-srotas (सिद्धान्त), features the निर्माल्य-देवता, चण्डेश्वर with roots in the older पाशुपत अतिमार्ग. This deity receives offerings, such as leaves and flowers, that have been initially offered to Rudra-सदाशिव. In the शैव-saura system, he appears as तेजश्चण्ड, the निर्माल्य-देवता of सूर्य. The वैखानस and पाञ्चरात्रिक systems of the वैष्णव tradition also have an equivalent deity in the form of विष्वक्सेन. However, we posit in this note that he also encompasses to a degree features of the ape-faced शैव deity Nandin (sometimes bull-faced, especially in the later reflexes of the tradition), who is one of the great गणेश्वर-s of Rudra's hordes, and the sons of Rudra, the gods Skanda and विनायक. His worship is laid out in multiple पाञ्चरात्रिक tantra-s. Below we shall primarily consider aspects of his worship taught in one of the older पाञ्चरात्रिक texts, the पौष्कर-संहिता, and the शक्ति (श्री)-oriented पाञ्चरात्रिक text, the लक्ष्ंई-tantra, along with parallels drawn from the जयाख्य-सम्हिता, another old tantra of the पञ्चरात्र-s. The पौष्कर is presented as the teaching of the Bhagavant (विष्णु) to पौष्कर (the lotus-born one, i.e., Brahman). Its colophon calls it the पौषकर-संहिता from the महोपनिषत् of the पाञ्चरात्र. Despite this appellation, it resembles a rather standard तान्त्रिक text in its structure and style of presentation. The two versions of the पौष्कर that we used for this note have corruptions and lacunae, some of which might have crept into the transliterations presented below despite our attempts at restoration.

The name विष्वक्सेन appears for the first time in the महाभारत and is associated with the सात्त्वत strand of the proto-पाञ्चरात्रिक tradition. There, it is used primarily as a name or an ectype of the वासुदेव. Examples of this include the विष्णु-sahasra-नाम and the विष्वक्सेन-stuti found in the अनुशासन-parvan ("Critical" 13.143). In the latter, it is made clear that the वासुदेव, equated with the सात्त्वत hero, is the source and resort of everything (Vedic rituals etc.) in his विष्वक्सेन manifestation:

[वेदांश् च यो वेदयते ।अधिदेवो ।]{style="color: #0000ff"}\
[विधींश् च यश् चाश्रयते पुराणान् ॥]{style="color: #0000ff"}\
[कामे वेदे लौकिके यत् फल्ं च ।]{style="color: #0000ff"}\
[विष्वक्सेने सर्वम् एतत् प्रतीहि ॥]{style="color: #0000ff"}\
[ज्योतींषि शुक्लानि च सर्वलोके ।]{style="color: #0000ff"}\
[त्रयो लोका लोकपालास् त्रयश् च ॥]{style="color: #0000ff"}\
[त्रयो ।अग्नयो व्याहृतयश् च तिस्रः ।]{style="color: #0000ff"}\
[सर्वे देवा देवकीपुत्र एव ॥]{style="color: #0000ff"}\
[संवत्सरः स ऋतुः सो ।अर्धमासः ।]{style="color: #0000ff"}\
[so .अहोरात्रः sa कला vai sa काष्ठाः ॥]{style="color: #0000ff"}\
[मात्रा मुहूर्ताश् च लवाः क्षणाश् च ।]{style="color: #0000ff"}\
[विष्वक्सेने सर्वम् एतत् प्रतीहि ॥]{style="color: #0000ff"}\
[चन्द्रादित्यौ ग्रह-नक्षत्र-ताराः ।]{style="color: #0000ff"}\
[सर्वाणि दर्शान्य् अथ पौर्णमास्यः ॥]{style="color: #0000ff"}\
[नक्षत्र-योगा ऋतवश् च पार्थ ।]{style="color: #0000ff"}\
[विष्वक्सेनात् सर्वम् एतत् प्रसूतम् ॥]{style="color: #0000ff"}

Again, in the हरिवंश we encounter विष्वक्सेन used in the same sense in the early पाञ्चरात्रिक mantra of 101 words and 367 syllables (symbolizing विष्नु as the year) taught by कश्यप found in its Appendix. The same sense is seen in the विष्णु-गायत्री that was used by the कठ-s of the Panjab and Kashmir in their daily worship following the manual of लौगाक्षी (in place of the नारायण-गायत्री of the तैत्तिरीयक-s):\
[विश्वरूपाय विद्महे विष्वक्सेनाय धीमहि । तन्नो विष्णुः प्रचोदयात् ॥]{style="color: #0000ff"}

This continues into the पुराण-s such as the पद्मपुराण, where the name is used in the above sense in several stuti-s dedicated to विष्णु. Thus, the transformation of विष्वक्सेन from a name or an ectype of the वासुदेव in proto-पाञ्चरात्र to the निर्माल्य-देवता of तान्त्रिक पाञ्चरात्र happened well after the महाभारत, हरिवंश and certain पौराणिक sections. Nevertheless, interestingly, in the पौष्कर-सम्ःइता we have वासुदेव expound a theology of विष्वक्सेन that hearkens back to the above stuti from the अनुशासन-parvan. Thus, it appears that विष्वक्सेन as the निर्माल्य-देवता still bears a connection to this older usage of the name (पौष्कर-सम्ःइता, chapter 20):

पौष्कर asks विष्णु about विष्वक्सेन thusly:

[क एषो ।अतुलवीर्यो हि यस्य दूराद् द्रवन्ति च ।]{style="color: #0000ff"}\
[विघ्ना निमेष-मात्रेण त्रैलोक्योन्मूलन-क्षमाः ॥]{style="color: #0000ff"}

Who is this, indeed of unequaled heroism, by whose mere wink obstacles capable of uprooting the triple-word melt away in the distance?

विष्णु answered:

[क्लवैश्वानराख्या या मूर्तिस् तुर्यात्मनो विभोः ।]{style="color: #0000ff"}\
[स एष द्विज देवः स्याद् विष्वक्सेनः प्रकीर्तितः ॥]{style="color: #0000ff"}\
[स्थित आहवनीयादि-भेदेन मख-याजिनाम् ।]{style="color: #0000ff"}\
[ऋक्-पूतम् हुतम् आदाय तर्पयत्य् अखिलं जगत् ॥]{style="color: #0000ff"}\
[एवं मन्त्र-मयाद् यागात् सात्त्विकात् ब्रह्मभावितात् ।]{style="color: #0000ff"}\
[संप्राप्य गुरुमूर्तेर् वै प्रापणं मन्त्र-सत्-कृतम् ॥]{style="color: #0000ff"}\
[अनाहूतां सुराणां च सर्व-लोक-निवासिनाम् ।]{style="color: #0000ff"}\
[स्वयं संविभजत्य् आशु तद् अनुग्रह-काम्यया ॥]{style="color: #0000ff"}

He is known as कालवैश्वानर (the fire of the end of time), who is an emanation of the four-fold lord (i.e., the वासुदेव with the 3 other व्यूह-s). O twice-born one, he is that god who is known as विष्वक्सेन. He is situated in the types of fire altars viz. आहवनीय and the like of the Vedic ritualists. Having taken the oblations sanctified by ऋक्-s, he satisfies the entire universe. Thus, obtaining \[the essence] from the rituals filled with incantations, from purity, proper ritual intention and the form of the teacher, he verily gains the good deeds of the mantra \[deployment]. And he himself quickly apportions these to the uninvoked gods dwelling in all the worlds with the intention of gaining their favor.

In terms of the temple ritual, in addition to the contruction of a विष्वक्सेन shrine within the third circuit of the विष्णु complex, he may be housed in a separate temple to the south of a village (सनत्कुमार-संहिता). His worship in these shrines is accompanied by a special dance and music. The emergence of विष्वक्सेन as the निर्माल्य-देवता seems to have led to a special group of priestly assistants, the कारिन्-s, who partake of the final offerings handed over to विष्वक्सेन.

The yantra of विष्वक्सेन (पौष्कर, chapter 20):
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/12/vishvaksena.png){width="75%"}
```{=latex}
\end{center}
```



[धर्माद्य् अनन्त-पर्यन्तं पञ्चकं नवकं तु वा ।]{style="color: #0000ff"}\
[सत्त्वेनाच्छादितम् पश्चात् केवलम् अम्बुजं स्मरेत् ॥]{style="color: #0000ff"}\
[ऐशान-सोम-दिङ्-मध्ये चतुर्-अश्र-पुरे ।अथ वा ।]{style="color: #0000ff"}\
[द्वार-शोभाग्र-निर्मुक्ते रेखा-त्रितय-भूषिते ॥]{style="color: #0000ff"}\
[तद् अन्तरे ।अर्ध-चन्द्रस्थे कमले ।अष्ट-दलान्विते ।]{style="color: #0000ff"}\
[साम्राज्ये विनियुक्तं यद् विघ्नानाम् अच्युतेन तत् ॥]{style="color: #0000ff"}

One may meditate on the pentad or nonad of deities, Dharma etc., until Ananta. Thereafter, one may meditate on the One lotus enveloped by sattva. \[Then one worships विष्वक्सेन] between the northeastern and the northern directions or in a square yantra. It lacks doors and flanges (typical of other square yantra-s), and \[its periphery is] marked with triple lines. Inside it, stationed atop a lunar crescent, is a lotus with eight petals. \[In it is stationed] he who is appointed as the overlord of obstacles by Acyuta himself.

The above rendering is faithful to that mentioned in the पौष्कर, including some further details that are implicit in it and followed by मन्त्रवादिन्-s. The central mantra of विष्वक्सेन is that specified in संध्य-भाषा in the लक्ष्मीतन्त्र and made explicit in its gloss (र्हूं वौं). In actual practice, it is inscribed by साधक-s in the central position occupied by विष्वक्सेन (see below). There is also a strong conservation of the location for the worship of विष्वक्सेन in different early पाञ्चरात्रिक texts. For example, this is specified thus in the लक्ष्ंई-tantra, chapter 38:

[सोम-शंकर-दिङ्-मध्ये ख-स्थितं संस्मरेत् प्रभुम् ।]{style="color: #0000ff"}\
[विष्वक्सेनम् उदाराङ्गम् आयान्तं गगनान्तरात् ॥]{style="color: #0000ff"}\
One should meditate on the lord, stationed in the sky, between the northern and northeastern directions. विष्वक्सेन of benefic body \[is visualized] as coming from the sky.

A similar account is given by the जयाख्य संहिता in its 13th chapter describing the मण्डल worship of विष्णु:\
[त्र्यम्बकोत्तर-दिग्भ्यां तु मध्यतः ख-स्थितं स्मरेत् ।]{style="color: #0000ff"}\
[विष्वक्सेनं द्विजश्रेष्ठ आयान्तं गगनान्तरात् ॥]{style="color: #0000ff"}

The visualization of विष्वक्सेन (पौष्कर, chapter 20):

[इष्ट्वा हृत्-पुण्डरीके तु स्वापेक्षा-निष्कलात्मकम् ।]{style="color: #0000ff"}\
[तम् एव सकलत्वेन यातं ध्यात्वा यजेद् बहिः ॥]{style="color: #0000ff"}\
[नव-दूर्वाङ्कुराभं च त्वीषत् पीतल-कान्ति-धृत्]{style="color: #0000ff"}\
[चतुर्-दंष्ट्रं चतुर्-बाहुं चतुर्-मुष्कं चतुर्-गतिम्॥]{style="color: #0000ff"}\
[पूर्णाङ्गं केसरि-स्कन्धं पृथूरस्-स्थल-राजितम् ।]{style="color: #0000ff"}\
[दक्षिणा-वर्त-निम्नेन नाभि-रन्ध्रेण शोभितम् ॥]{style="color: #0000ff"}\
[आजानु-बाहुं श्रीमन्तं पिङ्गलार्चिर्-जटाधरम् ।]{style="color: #0000ff"}\
[द्रवत् कनक-पिङ्गाक्ष-चुबुकं पृथु-नासिकम्॥]{style="color: #0000ff"}\
[सित-दीर्घ-नख-श्रेणि-शोभितं कुटिल-भ्रुवम् ।]{style="color: #0000ff"}\
[मुक्ता-विभूषितं मध्ये महारत्नोप-संस्कृतम्॥]{style="color: #0000ff"}\
[कुर्याच् च दक्षिणे पक्षे कृत-श्रीवत्स-मङ्गलम् ।]{style="color: #0000ff"}\
[अमासितक्योमणि(?) (...) तन्मध्ये कमलालयम् ॥]{style="color: #0000ff"}\
[द्विगुणं ब्रह्म-सूत्रं स्यान् नाभेश् चाभि-प्रदक्षिणम् ।]{style="color: #0000ff"}\
[विस्तीर्ण-गण्ड-वदनं बालेन्दु-कुटिलोपमैः ॥]{style="color: #0000ff"}\
[नव-किंशुकारुणाभैर्-लोमैः संपूर्णा-विग्रहम् ।]{style="color: #0000ff"}\
[शोभनेन प्रलम्बेन पृथुना प्रोन्नतेन च ॥]{style="color: #0000ff"}\
[माणिक्य-कुण्डलाढ्येन युक्तं श्रोत्र-द्वयेन तु ।]{style="color: #0000ff"}\
[मुकुटेनोन्नतेनैव हाराद्यैर् उपशोभितम् ॥]{style="color: #0000ff"}\
[चित्र-कौशेय-वसनं विचित्र-स्रग्विमण्डितम् ।]{style="color: #0000ff"}\
[प्रलय-द्वादशादित्य-सहस्रगुण-दीधितिम् ॥]{style="color: #0000ff"}\
[ईषद् ऊर्ध्वे तथा तिर्यग् विनिपातित-लोचनम् ।]{style="color: #0000ff"}\
[कुन्देन्दु-कान्ति-दशनं किञ्चिद्-विहसिताननम् ॥]{style="color: #0000ff"}\
[स्वभाव-सौम्यम् अमलम् माया-क्रोधोपरञ्जितम् ।]{style="color: #0000ff"}\
[सविलास-चलत्-पादन्यास-स्थानक-संस्थितम्॥]{style="color: #0000ff"}\
[स्वेनान्तः करणेनैव भावयन्तम् परम् पदम् ।]{style="color: #0000ff"}\
[अङ्गुष्ठेन कनिष्ठान्तम् अङ्गुलैस् तु लतात्रयम् ॥]{style="color: #0000ff"}\
[नामयित्वोन्नता चैका घ्राणेन विनियोजिता ।]{style="color: #0000ff"}\
[सद्-विघ्न-भीति-प्रदया त्व् अनया मुद्रया ।अन्वितम् ॥]{style="color: #0000ff"}\
[रथाङ्ग-शङ्ख-हस्तं च लम्बमान-गदाधरम् ।]{style="color: #0000ff"}\
[श्रोणी-तट-निविष्टेन सावहेलेन पाणिना ॥]{style="color: #0000ff"}\
[इत्थं रूपधरं देवम् अनेकाद्भूत-विक्रमम् ।]{style="color: #0000ff"}\
[कर्णिका-मध्यगं तस्य हृद्-आद्यामुख्य-मन्त्रवत् ॥]{style="color: #0000ff"}

Having worshiped him in the heart-lotus, verily in his independent, undifferentiated form, visualizing him as having gone forth in his differentiated form, one may then worship him externally \[visualizing him thus:] Having the complexion of newly-sprouted Durva grass tinted with a slight yellowish luster; having four fangs, four arms, four testes and four gaits; with a full body, lion-shoulders and flaunting a broad torso, endowed with a navel cavity bearing a clockwise spiral whorl; having arms reaching to his knees, opulent and bearing dread-locks with a tawny blaze; having eyes and chin like molten gold and with a broad muzzle; with an array of white, long nails, and with curved brows; with a pearl-decked necklace decorated with a great gem in the middle. He should be made to bear the auspicious श्रीवत्स on his right side. [---corrupt+lacuna---] in the midst of a lotus pond; with a two-fold ritual thread wound clockwise around the navel; with broad cheeks and face; with a form covered completely covered with ruddy hair of the color fresh किंशुक flowers; endowed with a beautiful, broad, well-stretched, and tall \[form]; with two ears joined to ornaments of ruby; ornamented with a tall crown, garlands and the like; with eyes slightly upturned and cast obliquely; with teeth white as jasmine and the moon, and a face bearing a slight smile; though pacific and mild by nature, he displays the tint of mock anger. He stands with one foot stepping forth in a playful manner. He is seen as meditating upon the supreme state (विष्णु) as his inner consciousness. Having bent three of his creeper-like fingers from the thumb to the little finger, holding one erect, he unites it with his nose (This mudra might indicate प्राणायाम). His other \[hand] assumes the mudra of causing fear to the obstacles \[faced] by the good. His hands hold a wheel, a conch and a mace hanging downwards; One of his arms rests on the side of the hip in a warning pose. One should worship the god with such a form with many a miraculous power in the midst of the pericarp \[of the lotus मण्डल].

There are several parallels found in the much shorter description provided by the लक्ष्ंई-tantra's account of the daily पाञ्चरात्रिक-homa-vidhi. Here, the ritualist sets aside some of the cooked rice for the offering and makes fire sacrifices to विष्वक्सेन (लक्ष्ंई-tantra, chapter 40):

[प्राग् एव विभजेद् अन्नं प्रापणात् संप्रदानतः ।]{style="color: #0000ff"}\
[तेनान्नेन यजेत् सम्यग् विष्वक्सेनं चतुर्भुजम् ॥]{style="color: #0000ff"}\
[मण्डलान्तम् उपानीय समाहूयाम्बरान्तरात् ।]{style="color: #0000ff"}\
[नवाम्र-पत्र-सदृशं पिङ्ग-भ्रू-श्मश्रु-लोचनम् ॥]{style="color: #0000ff"}\
[पीत-वस्त्रं चतुर्-दंष्ट्रं स्व-मुद्राद्वितयान्वितम् ।]{style="color: #0000ff"}\
[गदा-खड्ग-धरं देवम् अभ्यर्च्य क्रमशः सुधीः ॥]{style="color: #0000ff"}\
[साङ्ग-मुद्राम् अथा दर्श्य गत्वा कुण्ड-समीपतः ।]{style="color: #0000ff"}\
[विष्वक्सेनस् ततो भक्त्या तर्पणीयस् तिलाक्षतैः ॥]{style="color: #0000ff"}\
[वौषड् अन्तेन मन्त्रेण दद्यात् पूर्णाहुतिं ततः ।]{style="color: #0000ff"}\
[मण्डले पूजयित्वाथ कुर्यात् तस्य विसर्जनम् ॥]{style="color: #0000ff"}\
[स्वमन्त्रेण सुरश्रेष्ठ क्षमस्वेति पदं वदन् ।]{style="color: #0000ff"}\
[मुद्रां च दर्शयेत् तं च नभस्य् उत्पतितं स्मरेत् ॥]{style="color: #0000ff"}\
[विष्वक्सेनार्चनं सर्वम् अगाधे ।अम्बुनि निक्षिपेत् ।]{style="color: #0000ff"}\
[तोयेनास्त्र-प्रजप्तेन प्लावयेन् मण्डलं च तत् ॥]{style="color: #0000ff"}

The ritualist should partition a portion of cooked rice from the storage vessel (we render प्रापण as such based on the parallel version in the जयाख्य; see below) before offering it \[to विष्णु]. With that rice, he should sacrifice to the four-armed विष्वक्सेन. Having brought him close from the sky, the intelligent ritualist should invoke and worship with the due procedure [विष्वक्सेन] in the मण्डल as having the complexion of a young mango leaf with tawny brows, mustache and eyes; wearing a yellow garment, with four fangs, displaying his own mudra; holding a mace and a sword. Having then displayed the mudra-s of the अङ्ग-s and having gone close to the fire altar, he should thereafter, with devotion, make quenching offerings of sesame seeds and parched rice. Then, with the [विष्वक्सेन]-mantra ending with a वौषट्, he should offer the final oblation. Having worshiped him in the मण्डल, he should then perform his sendoff, o lord of the gods (Indra, to whom लक्ष्ंई is revealing the tantra), with his own mantra (विष्वक्सेन) ending in the phrase "pardon me". He should then display the mudra-s and visualize him as flying away into the sky. He should then discard the items used in the worship of विष्वक्सेन in deep water. He then immerses the मण्डल in water by muttering the astra incantation.

A similar ritual is provided in the जयाख्य संहिता, chapter 14:

[तेन भाण्ड-स्थितेनापि समाहूयाम्बरान्तरात् ।]{style="color: #0000ff"}\
[विष्वक्सेनं यजेद् भक्त्या ध्यात्वा वै मण्डलान्तरे ॥]{style="color: #0000ff"}\
[चतुर्-भुजम् उदाराङ्गं गदा-शङ्ख-धरं विभुम् ।]{style="color: #0000ff"}\
[नवाम्र-पत्र-सङ्काशं पिङ्गल-श्मश्रु-लोचनम् ॥]{style="color: #0000ff"}\
[पीत-वस्त्रं चतुर्-दष्ट्रं स्व-मुद्राद्वितयान्वितम् ।]{style="color: #0000ff"}\
[समभ्यर्च्य क्रमात्साङ्गं मुद्राम् अस्याथ दर्शयेत् ॥]{style="color: #0000ff"}\
[गत्वा कुण्ड-समीपं तु मन्त्रं पुष्पैः प्रपूज्य च ।]{style="color: #0000ff"}\
[भस्मना ।अस्त्राभितप्तेन ललाटे तिलकं शुभम् ॥]{style="color: #0000ff"}\
[कृत्वा मण्डलवत् पश्चाद् उपसंहृत्य चात्मनि ।]{style="color: #0000ff"}\
[विष्वक्सेनस् ततो भक्त्या तर्पणीयस् तिलाक्षतैः ॥]{style="color: #0000ff"}\
[वौषड् अन्तेन मन्त्रेण दद्यात् पूर्णाहुतिं द्विज ।]{style="color: #0000ff"}\
[मण्डले पूजयित्वा ।अथ कुर्यात् तस्य विसर्जनम् ॥]{style="color: #0000ff"}\
[स्वमन्त्रेण द्विजश्रेष्ठ क्षमस्वेति पदेन च ।]{style="color: #0000ff"}\
[मुद्रा-समन्वितेनाथ नभस्य् उत्पतितं स्मरेत् ॥]{style="color: #0000ff"}\
[पूर्णेन कलशेनाथ अस्त्र-जप्तेन नारद ।]{style="color: #0000ff"}\
[क्षीराम्बु-मधुराज्येन प्रापयेन् मण्डलं तु तत् ॥]{style="color: #0000ff"}\
[ततः कुण्डात् समुत्थाप्य विष्वक्सेनं यथा पुरा ।]{style="color: #0000ff"}
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/12/vishvaksena2.jpg){width="75%"}
```{=latex}
\end{center}
```



A key point regarding विष्वक्सेन's iconography in the तान्त्रिक texts is his apparently therianthropomorphic form. While some वैष्णव-s closer to our times have conflated his iconography on occasions with that of विनायक (see below), it is clear from the old पाञ्चरात्रिक texts that this was not the case for विष्वक्सेन --- he is never called Gajavaktra in these texts. Nevertheless, the above-cited संहिता-s are united in mentioning him as having four fangs suggestive of therocephaly. This is made more concrete in his most detailed iconography presented in the पौष्कर. There, several explicit descriptors are provided, such as lion-shouldered, having a broad muzzle, ruddy hair, tawny locks, and long nails/claws. Taken together with him being four-fanged, it appears that the early पाञ्चरात्रिक tantra-s conceived this god as having leonine aspects or being lion-faced. This therianthropomorphic form also suggests a thematic overlap with Nandin in the शैव world, who was originally seen as being ape-headed. Like Nandin, विष्वक्सेन is seen as the chief of the गणराजेश्वर-s of विष्णु's hosts (see below). While these texts do not specify the iconography of विष्वक्सेन's wife, पुष्पधरा (the medieval श्रीवैष्णव scholar वेदान्त-देशिक call her सूत्रवती), her iconography is briefly specified by the पाद्म-संहिता as being similar to that of श्री (क्रियापाद, chapter 22):

[देवीं च विष्वक्सेनस्य वामपार्श्वे प्रतिष्ठिताम् ।]{style="color: #0000ff"}\
[नाम्ना पुष्पधरां कुर्यात् कमलाम् इव लक्षिताम् ॥]{style="color: #0000ff"}

The other गणराजेश्वर-s (पौष्कर, chapter 20):

[पद्मच्-छदान्तरस्थां च तदा-करद्युतिं विना ।]{style="color: #0000ff"}\
[किन्त्व् अङ्गानां च सर्वत्र ध्यानम् उक्तं सितादिकम् ॥]{style="color: #0000ff"}\
[गजाननो जयत्सेनो हरिवक्त्रो महाबलः ।]{style="color: #0000ff"}\
[कालप्रकृति संज्ञश्व चतुर्थः कमलोद्भव ॥]{style="color: #0000ff"}\
[गण-राजेश्वरा ह्येते चत्वारश् चण्डविग्रहाः ।]{style="color: #0000ff"}\
[आज्ञा-प्रतीक्षकाश् चास्य सुश्वेत-चमरोद्यताः ॥]{style="color: #0000ff"}\
[विनायकादयश् चैव विघ्नेश-प्रवरास्तु ये ।]{style="color: #0000ff"}\
[अमीषां गणनाथानां नित्यम्-आज्ञानुपालिनाम् ॥]{style="color: #0000ff"}\
[ईशानादिषु कोणेषु पद्म-बाह्यस्थितान् न्यसेत् ।]{style="color: #0000ff"}\
[वीक्षमाणा विभोर् वक्त्रं तत् तुल्य स्थानका-स्थिताः ॥]{style="color: #0000ff"}\
[तद्वत् कराङ्किताः सर्वे किन्तु मुद्रा-विवर्जिताः ।]{style="color: #0000ff"}\
[ध्यानम् एषां पृथग् भूतं शरीरम् अवधारय ॥]{style="color: #0000ff"}\
[भीमं द्विपेन्द्र-वदनं चतुर्-दंष्ट्रं त्रिलोचनम् ।]{style="color: #0000ff"}\
[कम्बु-ग्रिवं चतुर्-बाहुं पूर्ण-चन्द्रायुत-द्युतिम् ॥]{style="color: #0000ff"}\
[हार-नूपुर-केयूर-मेखलादाम-मण्डितम् ।]{style="color: #0000ff"}\
[नाना स्रग्-गन्ध-वस्त्राढ्यम् अनौपम्य-पराक्रमम् ॥]{style="color: #0000ff"}\
[ध्यायेद् गजाननम् अतो जयत्सेनं च संस्मरेत् ।]{style="color: #0000ff"}\
[महत्-तुरङ्ग-वदनं पद्मरागाचल-प्रभम् ॥]{style="color: #0000ff"}\
[द्रवच् चामीकराक्षं च अनेकाद्भुत-विक्रमम् ।]{style="color: #0000ff"}\
[हरिवक्त्रम् अतो ध्यायेत् सटाच् छुरित-मस्तकम् ॥]{style="color: #0000ff"}\
[निष्टप्त-कनक-प्रख्यं घोर-घर्घर-निखनम् ।]{style="color: #0000ff"}\
[मृग-राड्-वदनं विप्र कल्पान्तानिल-वेगिनम् ॥]{style="color: #0000ff"}\
[कालप्रकृति-नामानं भावयेद् अञ्जनाद्रिवत् ।]{style="color: #0000ff"}\
[दंष्ट्रा-कराल-वदनं पिङ्गल-श्मश्रु-लोचनम् ॥]{style="color: #0000ff"}\
[झष-कुण्डलिनं रौद्रं मीनवन् निम्न-नासिकम् ।]{style="color: #0000ff"}\
[गण-राजेश्वरा ह्येते महा-पुरुष-लक्षणैः ॥]{style="color: #0000ff"}\
[संयुक्ताश् चाखिलैर् विप्र त्व् आपादात् कन्धराव् अधि ।]{style="color: #0000ff"}\
[यत् किञ्चिन् मण्डनं वस्तु तदाद्योक्तं स्मरेत् त्रिषु ॥]{style="color: #0000ff"}\
[एतेषाम् अर्चनं कुर्यात् स्वनाम्ना प्रणवादिना ।]{style="color: #0000ff"}\
[नमोन्तेनाब्ज-संभूत नाना-सिद्धि-फलाप्तये ॥]{style="color: #0000ff"}

Within the bounds of the lotus [मण्डल] and without the \[central] radiance of rays (this part is unclear to us, but it apparently refers to the petals of the yantra outside the central region where the radiance of विष्वक्सेन is situated) but with all limbs [अङ्ग mantra-s] the meditation of the retinue gods is specified, colored white etc (मन्त्रवादिन्-s state that these are white, red, yellow and black). गजानन, Jayatsena, Harivaktra and कालप्रकृति of great might, these together are known as the tetrad, o lotus-born one. These four king-lords of the गण-s \[of विष्णु] of fierce forms attend to [विष्वक्सेन's] orders, with good white yak-tail fly-whisks held aloft. Then there are विनायक, etc., those who have विघ्नेश as their chief, ever obedient to the orders of those lords of the गण-s (The aforesaid tetrad of गणराजेश्वर-s). These should be placed in the Northwest etc corners (i.e., at the interstitial directions) outside the lotus. They should look towards their \[respective गण] lord, adopting a stance equivalent to them. Their hands are equipped with the same implements, but they do not display any mudra. Please pay attention \[now] to the visualization of the embodied forms assumed by each [गणराजेश्वर]. With the face of a terrible elephant-lord, with four tusks and three eyes, having a neck \[smooth as a] shell, with four arms and the radiance of a 10,000 full moons; ornamented with garlands, anklets, armlets, a girdle and a necklace; decorated with various garlands, perfumes and vestures, and having unequaled valor; thus, one should meditate on गजानन. Jayatsena should be visualized with a great horse-face, the radiance of a ruby mountain, with eyes like molten gold, endowed with many miraculous powers. Then one meditates on Harivaktra with a head strewn with manes, resembling heated gold and roaring like a terrible musical instrument (gharghara is an old Indo-Aryan musical instrument accompanying Vedic recitations and singing). O vipra, he has the face of the kings of the animals and the velocity of the wind at the end of the kalpa. He, कालप्रकृति by name, should be visualized as \[resembling] a mountain of collyrium; with a terrifying face displaying fangs; with tawny mustache and eyes; wearing shark-earrings, fierce and having a fish-like deep snout. O vipra, all these king-lords of the गण-s possess all the marks of the great persons from the feet to their necks. Whatever item of ornamentation was described for the first (Gajavaktra) should also be visualized for the \[other] three. O lotus-born one, the worship of all these should be performed with their respective names starting with the प्रणव and ending in 'नमः' for the attainment of various siddhi-s.

There are multiple remarkable features regarding these गणराजेश्वर-s:\
1. Their names indicate a clear mapping onto their शैव counterparts. Gajavaktra (Elephant-faced)= गणेश; Jayatsena (Conquering Army)= Skanda; Harivaktra (Lion-faced)= Haribhadra/वीरभद्र; कालप्रकृति= महाकाल. In terms of iconography, that of Gajavaktra is transparently equivalent to that of गणेश. While the name of Jayatsena is an obvious match for Skanda, his hippocephalous morphology is a stark deviation from that of the latter and appears to be an attempt to capture some of the preexisting, ancient वैष्णव iconography of हयग्रीव. While the later iconography of वीरभद्र lost the leonine visage, that idiosyncrasy mentioned in the early texts (Haribhadra; e.g., the Proto-Skandaपुराण) persisted in the classic Rudra-परिवार icons from Nepal. As for कालप्रकृति, he retains, in the least, the complexion of महाकाल. Given that these deities have deep roots in the शैव tradition, unlike the sudden appearance of their counterparts in the वैष्णव tradition, one may posit that they were pantheonic duplications to mirror their counterparts in the former tradition.

2. As noted above, in most old संहिता-s विष्वक्सेन is presented as iconographically distinct from गणेश. विष्वक्सेन shares the function of the remover of obstacles with गणेश. Nevertheless, multiple वैष्णव tantra-s emphasize his distinctness from the latter and provide separate accounts of the worship of each of them (e.g., जयाख्य, पौष्कर, लक्श्मी). In the पाद्म-संहिता, his iconography is provided thus (क्रियापाद, chapter 22):

[विष्वक्सेनं चतुर्बाहुं श्यामवर्णं किरीटिनम् ।]{style="color: #0000ff"}\
[लम्बोदरं च मुख्येन करेणाभयधारिणम् ॥]{style="color: #0000ff"}

Thus, he shares the lambodara feature with विनायक; however, even here, he is not described as Gajavaktra and the iconography of विनायक is expounded separately from him:

[गजाननं चतुर्बाहुं लम्बकुषिं सितप्रभम् ।]{style="color: #0000ff"}\
[करण्डिका-मुकुटिनं लम्ब-यज्ञोपवीतिनम् ॥]{style="color: #0000ff"}

These again point to pantheonic duplication rather than a simple replacement of विनायक by विष्वक्सेन. The presentation of विष्वक्सेन in the midst of 4 विनायक-s in the पौष्कर mirrors the depiction of Skanda in the midst of the same in the old कौमार tantra, the षण्मुख-kalpa. Further, as the commander of विष्णु's hosts, and in the very form of his name (whose army is everywhere), विष्वक्सेन takes on certain features of कुमार.

3. The account of the 4 विनायक-s in the पौष्कर has some unclear points. First, all their names are not specified in this text. Practicing मन्त्रवादिन्-s give them as विनायक, विघ्नेश, Pravara and गणनाथ. These four names are derived from the above text, but they are not used in that sense in the text. Second, they are said to be stationed outside the lotus of the yantra. Given that the lotus has 8 petals, with विष्वक्सेन in the pericarp, and the गणराजेश्वर-s in the 4 petals of the cardinal directions, we are still left with 4 more petals. Some मन्त्रवादिन्-s logically place the 4 विनायक-s in those petals contrary to the account in the text.

Thus, the convergence between the retinues of the Rudra and विष्णु in the Tantric texts presents some of the same features seen more broadly in the course of the evolution of natural religions, especially the Indo-European religion. While we see strong ritual-category convergence (निर्माल्य-देवता-s), their nature in terms of divine functionality shows a more diffuse overlap with the deities from the pantheons of other sects.

