
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Ones, twos, threes ... hundreds and thousands in the ऋग्वेद](https://manasataramgini.wordpress.com/2023/07/17/ones-twos-threes-hundreds-and-thousands-in-the-%e1%b9%9bgveda/){rel="bookmark"} {#ones-twos-threes-hundreds-and-thousands-in-the-ऋगवद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 17, 2023]{.entry-date}](https://manasataramgini.wordpress.com/2023/07/17/ones-twos-threes-hundreds-and-thousands-in-the-%e1%b9%9bgveda/ "7:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[In the previous note](https://manasataramgini.wordpress.com/2023/07/14/the-gods-in-triples/), we looked at some special numbers relating to the count of the gods in the Veda and the influence of the Proto-Indo-European tripartition on them. Here we more generally look at the distribution of numbers in the Veda with a focus on the ऋग्वेद (RV). Among other things, we hope to discern key aspects of early numerical thought among the Hindus. One such is how and when the proclivity for big numbers started. Are there any traces of it in the earliest textual traditions of the Hindus? As we noted [before](https://manasataramgini.wordpress.com/2023/07/14/the-gods-in-triples/), the RV is replete with numbers. Hence, the first question we can ask is how they are distributed. Figure 1 shows a simple plot of the frequencies of the commonly mentioned numbers in the RV.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/07/rv_numbers.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad Common numbers in the RV.
```{=latex}
\end{center}
```


We observe that as we proceed from 1 upwards, the frequency of the integers tends to decline, but there are multiple exceptions to this trend. We can ask if there is indeed a real declining pattern barring the exceptions. The available  $n$  is not large to make an accurate statistical estimate of the deviation of particular numbers from the trend, but we can still construct an objective picture of that trend. Now, there is a law known as the Newcomb-Benford law that defines the probability distribution of the first significant digit of a dataset. It was discovered by the American astronomer Newcomb when he was looking at the first digits in a log table. Benford subsequently showed its validity for several other datasets. The law may be described thus: Given a long list of numbers generated via physical measurements (e.g., astronomical observations) or financial data or even something like log tables, reciprocal tables etc., the fraction of the numbers  $p_d$  beginning with  $d=1, 2, 3, ..., 9$  is given as,

 $$p_d=\log_{10}(1+\frac{1}{d})$$ 

Therefore, a deviation from the Newcombian distribution will imply that some deliberate process privileging or discriminating certain numbers is involved. However, in the current case, we are not looking at the first significant digit of the numbers in question but the actual frequency of integers in the RV. Hence, we cannot apply the Newcombian distribution as is. However, inspired by it, we can see if there is any background trend in the numbers against which exceptions stand out. Thus, we can fit a background trend for the frequency  $f$  of an integer  $n$  of the form:

 $$f=283 \times 10^{-n/7}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/07/rv_numbers_log_dis.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


As one can see in Figure 2, this gives an excellent fit for 1, 2, 4, 5, 8, 11, 12  $(p=10^{-4})$ . This implies that 3, 7, 9 and 10 are overrepresented, with 6 probably a bit underrepresented. By this yardstick, it is also rather obvious that the powers of 10, viz., 100 and 1000, are overrepresented, but 10000 is not particularly common (Figure 1). Of these, the overrepresentation of 3 confirms the Dumézilian view that tripartition was integral to IE ideology. A closer look at the 3s reveals numerous specific associations:\
1. As we noted [before](https://manasataramgini.wordpress.com/2023/04/12/tricakra/), it is intimately linked to the tricyclic chariot (with three wheels and niches) of the twin अश्विन्-s.  $\frac{1}{5}$ th of all the सूक्त-s featuring the number 3 also feature the अश्विन्-s.\
2. The three strides of विष्णु.3. In an early expression of the polycephalous iconography typical of the Hindu world, Agni is described as having 3 heads ([त्रिमूर्धानं सप्तरश्मिं गृणीषे ।अनूनम् अग्निम् पित्रोर् उपस्थे ।]{style="color: #0000ff"} RV1.146.1a). In the counter-pantheon, विश्वरूप त्रिशिरस् त्वाष्टृअ, a figure of PIE vintage, who was slain by Indra, is similarly three-headed.\
4. The triple realms of the universe.\
5. Trikadruka-s: A mysterious appellation for a group of deities accompanying Indra (when he together with विष्णु), and Yama. Given that the primary companions of Indra are the Marut-s, some have proposed that it was a term for them. The meaning of this term and its triadic nature still remain an unresolved issue for me (see below).\
6. त्रिधातु: Literally tripartite. This word, occurring 26 times in the RV, encapsulates the ideology of tripartition. It might be applied to the universe ([य उ त्रिधातु पृथिवीम् उत द्याम् एको दाधार भुवनानि विश्वा ॥]{style="color: #0000ff"} RV 1.154.4); the shelter/protection offered by the gods ([त्रिधातु शरणं; त्रिधातुना शर्मणा पातम्]{style="color: #0000ff"}); the tripartite horn of Agni (त्रिधातुशृङ्गः).\
7. Personal names: त्रिशोक, त्र्यरुण.

The next overrepresented integer is 7, which again shows several specific associations in the RV:\
1. 7 होतृ-s/ऋषि-s: The seven ancient sages or ritualists is a tradition shared with the Mesopotamian tradition (Apkallu) and the pre-Aryan Harappan tradition of India. It seems to have served as a placeholder that was later adopted in Greece, Iran and China. Thus, it could have been an ancient traveling motif. Prior to the आध्वर्यव reformulation of the श्रौत rite, the term 7 होतृ-s appears to have been originally used for the 7 ritualists the होतृ, पोतृ, नेष्टृ, अग्नीध्र, Adhvaryu, प्रशास्तृ and Brahman, who in turn might have been modeled after the 7 ancestral sages. In at least some cases, even in the RV, this term seems to have signified the constellation of Ursa Major, which in later tradition was unanimously associated with the seven ऋषि-s ([दिव इत्था जीजनत् सप्त कारून् अह्ना चिच् चक्रुर् वयुना गृणन्तः ॥]{style="color: #0000ff"} RV 4.16.3  $\to$  Indra generating the seven sages of the Heaven); similarly in RV 10.82.2; RV 10.130.7.\
2. 7 rays/heads/tongues of the Sun, the god Agni and some times Indra or बृहस्पति: [पाति नाभा सप्तशीर्षाणम् अग्निः]{style="color: #0000ff"} RV.3.5.5 (Agni); [दिवश् चिद् अग्ने महिना पृथिव्या वच्यन्तां ते वह्नयः सप्तजिह्वाः ॥]{style="color: #0000ff"} RV 3.6.2 (Agni); [यः सप्तरश्मिर् वृषभस् तुविष्मान्]{style="color: #0000ff"} RV 2.12.2 (Indra); [सप्तास्यस् तुविजातो रवेण वि सप्तरश्मिर् अधमत् तमांसि ॥]{style="color: #0000ff"} RV4.50.4 (बृहस्पति); [सूर्यस्य सप्त रश्मिभिः ॥]{style="color: #0000ff"} RV 8.72.16 (Sun). In the case of the Sun, this motif might also be expressed as the seven horses of the solar chariot. How exactly the arrangement of the 7 heads, horns, or tongues was imagined remains unclear. However, from the later ritual tradition, it would seem that it was imagined as the items placed on the vertices of a hexagon and its center.\
3. The 7 goddesses (mothers or sisters): This theme persisted in later Hindu tradition as the famous sapta-मातृ-s. At least in some contexts, this might have corresponded to the old count of the Pleiades as 7, which later became 6. Just as the 6 कृत्तिका-s associate with Skanda, these 7 goddesses sometimes associate with the newly born Agni (e.g., RV 1.141.2, RV 3.1.4). These 7 goddesses are also described as heavenly ([स्वाध्यो दिव आ सप्त यह्वी रायो दुरो व्य् ऋतज्ञा अजानन् ।]{style="color: #0000ff"} RV 1.72.08), suggesting that they are indeed an earlier reference to the Pleiades.\
4. sapta सिन्धवः or sapta यह्वीः: The 7 rivers or streams. These might sometimes overlap with the above and might be associated with the goddess सरस्वती. At different points, they might have been identified with earthly streams in the lands occupied by the Aryans.\
5. sapta-रत्नाः: the 7 gems.\
6. sapta-धामानि: the 7 realms. विष्णु is said to traverse these in the course of his famous strides. This concept appears to have developed into the karshvare-s (7 world regions) on the Iranian side.\
7. The 7 Marut-s  $\to$  RV 8.28. This is the count given for the Marut-s in the said सूक्त and sometimes in later tradition (or as  $7^2=49$ ).

Beyond the above, it is not surprising that the two key numbers 3 and 7 come together as a product त्रिः सप्तः, i.e., 21. This number is also seen in the opening सूक्त of the Atharvaveda and represents the 21 basic अक्षर-s of the संस्कृत "alphabet." It might signify many other things, including in some reckonings the count of the Marut-s. This count might have a relation to the mysterious Trikadruka-s, who might have been the Marut-s

The overrepresentation of 9 is notable as it is also a special number for the Mongols and possibly the early Turks. It usually occurs in the phrase, a 9 and a 90. One could take this as the integer 99, but we count it among the 9s because of the specific form of the usage. The number 99 typically stands for the fortifications of the antagonists like शम्बर or the number of the arms of the demon उरण whom Indra struck down. As the old Indo-Aryan number system is primarily decimal, it is not entirely surprising that 10 and its powers 100 and 1000 are particularly common. It is in this decimal exponentiation that we encounter the first inklings of the Hindu love of large numbers. We have the following ऋक् of गृत्समद शौनहोत्र:\
[त्वाम् अग्ने दम आ विश्पतिं विशस्\
त्वां राजानं सुविदत्रम् ऋञ्जते ।\
त्वं विश्वानि स्वनीक पत्यसे\
त्वं सहस्राणि शता दश प्रति ॥]{style="color: #0000ff"} RV 2.1.8\
To you, Agni, as the lord of the folks at home, the folks,\
to you, the king, who is benevolent, are attracted.\
You, of good radiance, lord over all;\
you compare to thousands, hundreds and ten.

The term सहस्राणि शता दश, mentioning the 3 successive powers of 10, can be interpreted as the number  $10 \times 100 \times 1000 =10^6$ . i.e., a million, due to the singular 10 and plural 100s and 1000s. This seems to be the largest explicitly mentioned number in the RV and foreshadows the chant of the powers of 10 seen in the Yajurveda texts:\
[एका च दश च शतं च सहस्रं चायुतं च प्रयुतं च नियुतं चार्बुदं च न्यर्बुदं च समुद्रश् च मध्यं चान्तश् च परार्धश् च ता मे अग्न इष्टका धेनवः सन्तु ॥]{style="color: #0000ff"} कठ संहिता 17.10 (in minimal form without the duplications)\
Here, the powers of 10 are listed from one (eka) to a trillion (परार्ध). In some texts, like the वाजसनेय संहिता or तैत्तिरीय संहिता, the positions of prayuta and niyuta are exchanged in this chant. Of these powers, the largest that is mentioned in the RV is ayuta. A similar sequence is incorporated into the रामायन to give an impression of the magnitude of the वानरसेना. However, in later tradition, we find that some further powers were introduced, and परार्ध came to mean an even larger number. The word परार्ध may be understood as para+ardha --- the remote side of the half (the world hemisphere); hence, we take the term as implicitly indicating the radius of the universe. Indeed, already in the RV, we encounter the use of smaller powers of 10 to indicate something of great magnitude --- namely the universe-encompassing nature of Indra in a ऋक् of Puruhanman आङ्गिरस:\
[\
yad द्याव indra te śataṃ\
शतम् भूमीर् उत स्युः ।\
न त्वा वज्रिन् सहस्रं सूर्या अनु\
न जातम् अष्ट रोदसी ॥]{style="color: #0000ff"} RV 8.70.5\
If a 100 Heavens, O Indra,\
as also a 100 Earths could be yours,\
O Vajrin, a 1000 suns did not,\
even as you were born, nor did the world hemispheres, equal you.

Here, the magnitude of Indra is indicated by comparing him to the extent of a 100 heavens and earths and he is said to exceed a 1000 suns. The luster of gods measured in terms of huge numbers of suns is widely seen in later Hindu tradition. To our knowledge, the above ऋक् is the first expression of that. We also believe that there is an interesting wordplay (श्लेष) in it: the word अष्ट is the passive past participle form (kta) of the root अश्-. However, it is also homophonic with the word for 8 (अष्ट); hence, we believe that it was used on purpose to subliminally remind the hearer of what were to become holy numbers in later Hindu tradition --- 108 and 1008. Thus, we believe that in verses like the above, we see the beginnings of the Hindu apprehension of vast magnitudes that were to become important both in their religion and cosmography. In terms of such expressions, a good linker between the older Vedic layer and the newer Hindu tradition can further be seen in the opening of the famous पुरुषसूक्त (RV 10.90), where the all-engendering cosmic being is conceived with a thousand heads, eyes and feet.

To get a better feel for the 100s and 1000s in the RV, we next look at their frequency by मण्डल both as raw counts and as normalized counts per 10000 syllables (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/07/rv_numbers_100s_1000s.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


For the 100s, we see that, while all मण्डल-s have a similar normalized frequency, मण्डल 8 has a significantly higher frequency (conservatively, p= 0.01), 2.5 times the mean frequency of that seen in the remaining मण्डल-s. This overrepresentation comes primarily from an epithet of Indra, शतक्रतु --- he of a hundred acts. The raw and the normalized frequency of शतक्रतु is shown in Figure 4.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/07/rv_numbers_shatakratu.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


शतक्रतु is significantly overrepresented in मण्डल 8 (7.2 times the mean in other मण्डल-s in normalized counts; conservatively,  $p< 10^{-4}$ ) and underrepresented in मण्डल 9 (conservatively,  $p=4 \times 10^{-4}$ ). This suggests that the conception of Indra as शतक्रतु seems to have arisen in the community to which the काण्व-s belonged. This community shows several linguistic and historical features distinguishing them from the authors of most family मण्डल-s and potentially associating them with a distinct proto-Iranian group. Once the term शतक्रतु emerged, it appears to have acquired considerable popularity and remained so in the इतिहास-पुराण as we have it even after the mainstream Vedic religion declined. In contrast, the 1000s are significantly overrepresented in मण्डल 9 (conservatively,  $< 10^{-3}$ )), which is exclusively devoted to the Soma ritual. Here we frequently encounter terms such as: सहस्रधार (a 1000 streams), सहस्रवल्श (a 1000 shoots), sahasravarcas (a 1000 splendors), सहस्रपाजस् (1000-fold strength), sahasroti (a 1000 aids), sahasrajit (the 1000-conqueror), सहस्रचक्षस् (1000-eyed), सहस्रणीथ (praised with a 1000 chants), and sahasraretas (he with semen of 1000-fold magnitude). This indicates a hyperbolic usage that was specifically associated with the Soma ritual and libations. Hence, we posit that the exhilaration of the Soma drink produced a psychosomatic experience that gave birth to this perception of extraordinary magnitude. Thus, the germs of the Hindu love for large numbers might have stemmed from the unique experience of the Soma ritual intersecting with the old decimal system.

