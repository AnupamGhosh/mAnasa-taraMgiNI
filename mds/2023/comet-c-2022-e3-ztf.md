
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Comet C/2022 E3 (ZTF)](https://manasataramgini.wordpress.com/2023/02/12/comet-c-2022-e3-ztf/){rel="bookmark"} {#comet-c2022-e3-ztf .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 12, 2023]{.entry-date}](https://manasataramgini.wordpress.com/2023/02/12/comet-c-2022-e3-ztf/ "7:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The dispiriting cloud cover lifted briefly on two nights (Wed 8/2/2023 and Fri 10/2/2023) finally giving us an opportunity to catch the latest Agni-putra-ketu in the welkin, Comet C/2022 E3 (ZTF). On the first night, there was still some haze, but we managed to barely get it with our  $20 \times 70$ . It was easy to track given its proximity to  $\iota$  Aurigae. Below  $\iota$ , the triad of stars, HD 31233 (7.34), HD 31234 (7.47), HD 30842 (7.59) bounded the comet and their magnitudes, along with that of HD 30453 (5.91), in the vicinity allowed us to estimate its magnitude as  $\approx 6.2-6.4$ . Below is a long exposure image captured by our friend's camera that illustrates the position and the view close to what saw through our  $20 \times 70$ .
```{=latex}
\begin{center}
```

![]\",\"camera\":\"Pixel 7\",\"caption\":\"\",\"created_timestamp\":\"1675902044\",\"copyright\":\"\",\"focal_length\":\"6.81\",\"iso\":\"139\",\"shutter_speed\":\"15.999575\",\"title\":\"\",\"orientation\":\"1\"}" image-title="Comet" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg" orig-size="1430,1147" permalink="https://manasataramgini.wordpress.com/pxl_20230209_040522981-night2/" sizes="(max-width: 1430px) 100vw, 1430px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg 1430w, https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/pxl_20230209_040522981.night2_.jpg 1024w"}
```{=latex}
\end{center}
```



On the second night, we had a clear period between 6:40-6:55 PM when we got a much better view of the शिखिन् close to the zenith than on the first night. We could discern a faint tail. It had come close to Mars and could be seen in the same field. HD 29459 (6.25) offered a comparison and the comet was approximately the same magnitude as it. It was a reasonably good night with the ecliptic studded with Venus (-3.8) close to the western horizon, Jupiter (-2) above it and finally Mars (0) all visible to the naked eye. Between Jupiter and Mars was Uranus (5.8), which from our urban locale, was too faint for the naked eye.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/comet.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2023/02/cometc2022e3ztf.jpg){width="75%"}
```{=latex}
\end{center}
```



This comet's eccentricity has been estimated at 1.0003320 or 1.00002 --- the first comet in our lifetime of comet observations so far that has come this close to a parabola --- it is not going to visit these realms again unless some unpredicted gravitational perturbation occurs in far space. In high-resolution photos, its color is recorded as distinctly green. This color brings to mind the cometary observations of ancient H astronomers. Unlike their post-Siddhāntic counterparts who developed a serious character flaw in the form of a disinterest in the sky outside the ecliptic, the early H astronomers of the Vedic age were intrepid comet-watchers. Some of their comet lore is preserved in AV परिशिष्ट-s and also by the much later, great naturalist वराहमिहिर. These old observers like नारद, Asita, Devala, Garga and पराशर are said to have recorded 1000 or 101 comets and vividly described their properties. नारद had the peculiar theory that all comets are merely reappearances of the same one. We do not know if this surmise came as a result of some knowledge of Halley's comet or was pure speculation. The appearances of these comets were linked to an ancient H omenology resembling the Roman Omina et Portenta --- comets with certain appearances were believed to bring weal while others were said to prognosticate more negative events. Aristotelian physics believed comets to be atmospheric phenomena --- this view remained current in Europe until Geminiano Montanari showed them to be distant celestial objects. In contrast, H tradition distinguished atmospheric and earthly formations resembling comets from the truly celestial one and saw the latter as "sons" of the planets or of gods. The grouping of the comet (the deathly धूमकेतु) with the planets (graha-s), the Moon, the Sun, planetary shadows (rahu-s) is already known in the Atharvaveda (AV-vulgate 19.9.10) and distinguished in the said सूक्त from earthly phenomena. वराहमिहिर, probably following these earlier authors, also noted that while luminous, the comets are not "fiery" but emit reflected or "phosphorescent" light. In any case, a green comet along with those displaying several other colors are classified in the old H tradition among the comets known as the Agni-putra-ketu-s --- the sons of the god Agni.

[शुक-दहन-बन्धुजीवक-लाक्षा-क्षतजोपमा हुताशसुताः ।]{style="color: #0000ff"}\
[आग्नेय्यां दृश्यन्ते तावन्तस् ते +अपि शिखि-भयदाः ॥]{style="color: #0000ff"} (बृहत्संहिता 11.11)\
Of the color of a parrot (green), fire, बन्धुजीविक (flower), lac or blood are the sons of Agni.\
They appear in the south-east and such comets cause (prognosticate) fear.

The H portent tradition also associates a passage of a comet through an ecliptic constellation with certain outcomes, in this case, negative:\
[अश्विन्याम् अश्मकपं भरणीषु किरातपार्थिवं हन्यात् ।]{style="color: #0000ff"}\
[बहुलासु कलिङ्गेशं रोहिण्यां शूरसेनपतिम् ॥]{style="color: #0000ff"}(बृहत्संहिता 11.54)\
If the asterism of अश्विनि \[is "smoked" by the comet] the lord of अश्मक is killed, भरणि the lord of the tribals is killed;\
If it is the Pleiades cluster then the lord of कलिङ्ग, it is its रोहिणि, then it is the शूरसेन lord.

Thus, the old H would have seen this as a negative portent for the शूरसेन-s.

In commemorating our observations of the comet, we composed two awful verses that we anyhow append below:\
Night 1:\
And there stood the Rudra known as पशुपति,\
even as the polycephalous leonine avenger of Sati.\
Then there was the Rudra Bhauma, glowing red,\
even as the red रोहिणी trembled in dread.\
Betwixt them lay sprawled प्रजापति's cervine यज्ञ,\
Its dying flames forth wafting away as a cometary plasma!

Night 2:\
The western welkin: the twin fires of the भृगु-s and the अङ्गिरस्-es shine forth.\
They call them शुक्र and बृहस्पति --- the graha-s lighting up gods' celestial path.\
Yonder is Arka, glaring reddish at the eye of the bull charging on the heavenly vault.\
Lo behold! He is now confronted by a sidereal interloper emitting greenish froth!

\*Arka is the old Vedic name for Mars (e.g., [the ग्रहेष्टि of the कठ-s](https://manasataramgini.wordpress.com/2010/02/18/the-oldest-surviving-planetary-ritual-of-the-hindus/)).

