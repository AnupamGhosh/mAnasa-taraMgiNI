
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Microraptor as a biplane](https://manasataramgini.wordpress.com/2007/01/25/microraptor-as-a-biplane/){rel="bookmark"} {#microraptor-as-a-biplane .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 25, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/25/microraptor-as-a-biplane/ "6:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RbhbKi6_e3I/AAAAAAAAACI/c8UnxQH4gaA/s320/Microraptor3.jpg){width="75%"}
```{=latex}
\end{center}
```

\
When the चीनाचार्य Xu announced his dramatic find [Microraptor gui]{style="font-style:italic;"} in the British tabloid something struck me as odd. One of the best preserved skeletons, the holotype, was in a very typically bird-like pose with the femur, tibia and tarso-metatarsus in Z-shaped disposition much like eagles when they are zoning in on their prey. In spite of this pose of the fossil, the chIna-s reconstructed [Microraptor ]{style="font-style:italic;"}like "tetrapteryx" of Beebe and Gerhard Heilmann who pictured the ancestor of birds to be a 4-winged animal with splayed-out legs bearing wings. To me this was patently un-anatomical. The femoral head of a typical theropod dinosaur simply cannot splay out like that to get into the "tetrapteryx" pose picture by Xu following the old Beebe. The funny thing was that the chIna-s did this despite their fossil showing that it was not the case. Then they piled on declaring [Microraptor ]{style="font-style:italic;"}was strictly arboreal and could not really walk on ground without damaging its feathers. In a sense, Xu and his chIna-s cannot be entirely blamed for their mistake, because reconstructing functional behavior from a skeleton, especially one preserved in 2D, without an equivalent modern analog can be difficult.

Now paleontologist Chatterjee and aeronaut Templin have claimed that dinosaur indeed glided in a pose comparable to how it was fossilized with the hind-limb feathers forming a lower aerofoil thus making it a biplane, as opposed to the monoplane morphology of [Archaeopteryx ]{style="font-style:italic;"}and the modern birds. This is an unusual and interesting reconstruction -- it is definitely far more likely than Xu's reconstruction, but who knows if it was really so. I do think C and T are however correct when they state that the tibial feathers, which are comparable to the "trouser" like leg feathers of attacking eagles, help in streamlining the air flow around the tibial shaft. Given the presence of similar feathers in an earlier enigmatic deinonychosaur from China, [Pedopenna ]{style="font-style:italic;"}one wonders if such a strategy of gliding using hind-limb feathers was prevalent in the earlier phase of dinosaurian flight. Of course this now needs to be reconciled with Dial's theory of wing-assisted incline running (WAIR) as the precursor of dinosaurian flight, for which there is support from living dinosaurs like the chukar partridge. WAIR is attractive because it might explain the strategies of other terrestrial deinonychosaurs and oviraptorsaurs with asymmetric feathers on their forelimbs alone.

Our complex speculation goes thus: In the ancestor of the coelurosaur clade including the deinonychosaurs, scansoriopterygids, oviraptorosaurs and therizinosaurs (in the least) WAIR was adopted as a strategy to help running up inclines both for hunting prey or fleeing from predators up into trees. But this did not lead to flight immediately. It only favored the emergence of some arboreal theropods as a result of the tree niche being opened up due to the ability to run into them as a result of WAIR. Once in the trees the first phase appears to have involved gliding with both hand and leg features (perhaps in the biplane mode). In this phase the long tail with flight feathers was very important as a control of flight or as a pitch damper in gliding. Then came the phase like [Archaeopteryx and Jinfengopteryx]{style="font-style:italic;"}, where more efficient flight emerged with loss of the leg feathers but retention of the long tail as a control device. This was followed by loss of the long tail and the pygostyle-borne tail feathers as seen in some early forms like [Confuciusornis]{style="font-style:italic;"}.


