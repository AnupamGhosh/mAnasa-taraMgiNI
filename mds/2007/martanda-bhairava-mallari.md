
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [मार्ताण्ड bhairava: मल्लारि](https://manasataramgini.wordpress.com/2007/06/14/martanda-bhairava-mallari/){rel="bookmark"} {#मरतणड-bhairava-मललर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 14, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/14/martanda-bhairava-mallari/ "5:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After visiting the shrine of the fiery ulka, accompanied by his two shaktI-s, we were filled with the urge to pay a visit to the shrine atop the massif of Jejuri. It houses खण्डोबा, a pastoral deity, an evolute of the ancient [revanta,](https://manasataramgini.wordpress.com/2006/08/31/the-deva-revanta-mythology-iconography-history-and-ritualism/ "The deva revanta: Mythology, iconography, history and ritualism") in Andhra, Karnataka and Maharashtra. A Sanskrit tale is narrated in association with this shrine:

  - The ब्राह्मण-s inform rudra that the world was being tormented by the demons maNi and malla.

  - They go to rudra to ask him to save them from the demons. From his locks he produces a fierce shakti named घृतमारी

  - He himself manifests as मार्ताण्ड bhairava. He is described as having golden flaming robes, with teeth like flashing rubies and he was surrounded by a troop of fierce dogs.

  - मार्ताण्ड bhairava appointed कुमार as the commander of his forces with a horde of skandagraha-s mounted on peacocks.

  - कुमार further partitioned his division under indra, agni, yama, निरृति, वरुण, kubera, brahmA and विष्णु. There was also a horde of विनायकस् mounted on rodent-faced horses led by the fiery ulka.

  - The forces of मार्ताण्ड bhairava and those of maNi and malla fought a fierce battle.

  - In the final fierce battle the horse-faced maNi fought मार्ताण्ड bhairava riding a white horse. Finally, he was struck by an arrow of the bhairava and thrown into a lake termed मणिचल् along with his horse. मार्ताण्ड bhairava then stamps him underfoot, when he pleased the god with a hymn, and asks for a boon to be stationed at the door-step of his temple. He also gives his white horse as a वाहन to the bhairava.

  - Then malla attacked the deva army with his own personal force by uttering so loud a roar that it caused पार्वती-s crown to fall in कैलाश. मार्ताण्ड bhairava dispatched घृतमारी to attack his hordes.

  - घृतमारी swooping down on them like a कृत्या swallowed all of them and ground them the dust.

  - Then घृतमारी and malla fought a terrible battle. He pierced her head with 100 arrows, her breasts with 27 arrows each and another 100 to immobilize her shoulders and neck.

  - Then मार्ताण्ड bhairava entered the field riding his white horse. With a cresent-headed arrow he cut off the demon's bow, but the demon retaliated by hurling his axe to cut the bow of the bhairava. The then two joined a hand-to-hand fight with swords.

  - Finally the bhairava struck him down and placed his foot on him. Before he was beheaded, he asked the flaming bhairava that his head should be placed outside the temple of the god and that he should be given sacrifices of goats or sheep. He also asked that his name be mentioned before that of his conqueror. मार्ताण्ड bhairava conferred these boons and dispatched him --- he took the name मल्लारि to satisfy the point of the the asura's name coming first.

.....

This is the canonized Sanskrit work. However, the शूद्र-s of महराष्ट्र and Andhra also narrate a colloquial version. Several शूद्र-s I have spoken to were well aware of the connection between मार्ताण्ड bhairava and खण्डोबा, and also generally know the Sanskrit version of the story. The colloquial version is usually narrated by a pious शूद्र bard called a वाघ्या or in the midst of the shepherds by a sooth-sayer termed the "देवर्षि". The Andhra and marAThA version differ in minor points, mainly place names. Some Andhras seem to have located the tale to Mallikarjuna or श्रीशैलम्; in the कण्णड country I have noticed some say that it was मल्लेश्वरं, now within Bangalore city. Clearly these places were there in the marAThA mind too, because शिवाजी bhosle and his clan, great worshipers of खण्डोबा, associated him with श्रीशैलं and मल्लेश्वरं. शिवाजी even wanted to renounce the world and become a mendicant at श्रीशैलं, but his men reminded him of his great role to play for स्वराज्य. This grandson emperor शाहु himself, like a वाघ्या came to his court along with his dog on which he placed the royal crown thinking of खण्डोब. But I supply below the story as narrated in the frame of the marAThA country, for that is where I have explored the many shrines of the deity:

The ब्राह्मण-s asked shiva to help the people from the depredations of the two demons on earth. shiva told उमा that he was leaving kailasa to deal with two asuras.\
उमा:" You are always taking avatara-s leaving me alone here. I want to come along this time."\
shiva: "Alright, after I kill the asuras you can come along. We will settle there and live for a while. You incarnate as म्हाळ्सा the daughter of a tima-शेट्टि, a लिङ्गायत shopkeeper. After killing the asura-s I will come marry you and take you to my palace atop the hill of jejuri."\
गङ्गा who saw all this, held in the coils of rudra's hair said: "This time I want to come too".\
shiva: "OK- you take the incarnation of a shepherd girl बानू among the शूद्र-s known as the dhangar (cattle herders). I will come to your hamlet and bring you over to my palace."\
उमा: "Alright, but why can't we come now? You may need us for battle."\
गङ्गा: "You will need us! see when the time comes, you will need us."\
shiva: "OK ladies go and take your incarnations, I really need to be doing this man's work of fighting asura-s."\
rudra took some haridra and sprinkled it on his snake --- it became a metal chain that he wore around his neck. In due course rudra incarnated as मल्लण्ण or खण्डोबा starts a war on maNi and malla. विष्णु also incarnates as ईठोबा or हेगडि प्रधान् as बानू's brother.

In the fight maNi on being wounded starts spawning new maNi-s from his blood- like रक्तबीज or andhaka. खण्डोबा seeing the trouble sends for the ladies म्हाळ्सा and बानू to come an join him in the battle. म्हाल्स taking up a sword and spear rushed to the field. बानू picked up a magic bow and arrows and ran to the field. Behind बानू came their dog. First the two ladies try to drink up the blood. But seeing it to be too much they used skulls of beheaded asuras to catch the blood. When even that did not suffice they ran around shouting in consternation and were hit by the arrows of the asuras. Then their dog seeing the predicament came and lapped up all the blood. With his blood drying up maNi was losing strength. Then ईठोबा also came to the field and gave खण्डोबा a chakra, with which he killed maNi.

बानू and ईठोभा killed many demons in malla's army. Then खण्डोबा attacked malla, with म्हाल्सा mounting his horse and wielding a spear. After a prolonged fight, malla finally rushed on खण्डोबा and seized his horse's reins. But the horse reared and allowed म्हाळ्सा to spike him on his flank, even as खण्डोबा struck off his head with his sword. The day of victory was a bright षष्ठी day in मार्गशीर्ष. Then खण्डोबा married म्हाळ्सा and also brought बानू along, and settled down with the two ladies in his hill-top palace at jejuri [कण्णड-s explicitly call बानू a concubine].\
....\
In जेजुरी, the shrine of विष्णु as ईठोबा or हेगडी pradhan is half-way up the hill. In front of his shrine goat sacrifices are made- he is suppose to convey the goat sacrifices to the recipients. खण्डोब himself only takes vegetarian offerings in the main shrine on the top of the hill. There, on one side in the courtyard, there is a large image of maNi and one of the steps has the image of the head of malla. म्हाळ्सा has her own shrine, while बानू has a little platform on which her image is placed. In the smaller, but venerated, धमन्केळ् shrine in Junnar near Pune, खण्डोबा is seated on a muNDa mancha with the two demons' heads as the foot-rests. म्हाळ्सा is seated next to him, while बानू is peeping from behind his shoulder. In his hands he has a sword, trident and shield. Besides the 3-peaked त्र्यंबक लिङ्ग, ईठोबा or विष्णु stands with a sword and shield.


