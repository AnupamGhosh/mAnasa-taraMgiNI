
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Early Angiosperm evolution](https://manasataramgini.wordpress.com/2007/03/16/early-angiosperm-evolution/){rel="bookmark"} {#early-angiosperm-evolution .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 16, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/16/early-angiosperm-evolution/ "5:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RfozNscE71I/AAAAAAAAAFY/RHjALntImbQ/s320/Nymphaea.jpg){width="75%"}
```{=latex}
\end{center}
```



```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RfozN8cE72I/AAAAAAAAAFg/ltcPJ_kzGM8/s320/Trithuria.jpg){width="75%"}
```{=latex}
\end{center}
```



1\) [Cabomba]{style="font-style:italic;"}; 2) Waterlily; 3)[Trithuria]{style="font-style:italic;"}

Molecular methods have caused a cataclysmic shake up in angiosperm phylogeny. Since Linnaeus the mainstream belief was that the angiosperms were divided into Monocots and Dicots, with each clade being supported by numerous characteristics, the chief amongst which were: Fibrous roots, single cotyledon, parallel venation, and specific vascular features for monocots, and tap roots, two cotyledons, reticulate venation and their own vascular features for dicots.

But what the molecular phylogenies suggested was that the dicots were not a natural group. Instead, a subset of dicot termed the "Eudicots" were found to be a sister group of the Monocots forming the crown group of the angiosperm tree. A sister group to this crown group was the magnoliid-type clade which included the classic eumagnoliids and the family chloranthacaeae. The fossils from the later Mesozoic generally supported this picture of the magnoliids being a sister group of the above crown angiosperms. In these phylogenies basal angiosperms outside of the above clades were supposed to include: 1) the enigmatic [Amborella, ]{style="font-style:italic;"}a shrub from New Caledonia, described first by BGL Swami from Bangalore, usually considered the basal-most angiosperm clade. 2) The waterlilies and the [Cabomba ]{style="font-style:italic;"}or the nymphaeales 3) The mainly woody austrobaileyales.

Saarela et al have recently reported that the enigmatic family of Hydatellaceae are actually a sister group of the Nymphaeales. They are aquatic plants entirely submerged and growing in ponds and lakes. Recognized only in the 1970s [Trithuria ]{style="font-style:italic;"}and [Hydatella ]{style="font-style:italic;"}were found only in Australia, Tasmania and New Zealand. In 1994 two Indian botanists found this plant in ponds in Maharashtra (Yadav, S.R. & M.K. Janarthanam, Hydatellaceae: a new family to Indian flora with a new species. [Rheedea ]{style="font-style:italic;"}Vol. 4(1) 1994; 17-20). This distribution suggests that this plant might be a remnant of the Mesozoic Gondwanaland. The fossil angiosperm [Archaefructus ]{style="font-style:italic;"}comes to mind-- like the Hydatellaceae it has naked unisexual flowers in inflorescences and both are aquatic. This raises the possibility that the great angiosperms that were to take over as the dominant plants in the Mesozoic made their advent as aquatic plants with inconspicuous flowers. The rise of the waterlilies from such a ancestor might represent one of the most ancient innovations of large flowers for attracting pollinators. This find also suggests that the angiosperms very rapidly exploded to colonize practically every niche available to a seed plant undergoing numerous morphological adaptations --- they probably had something that other dominant plants of the era did not or were given a chance by a mass extinction (?). So it might be useful to study the molecular developmental aspects of these angiosperm lineages at greater depth. In particular I would be interested in seeing the differences in expansions of the AP2, VP1, WRKY MYB, MADS ब्ZईP and ब्ःळ्ः between these plants and the crown group angiosperms.


