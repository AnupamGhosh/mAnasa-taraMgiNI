
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On king युधिष्ठिर's path](https://manasataramgini.wordpress.com/2007/08/27/on-king-yudhishthiras-path/){rel="bookmark"} {#on-king-यधषठरs-path .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 27, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/27/on-king-yudhishthiras-path/ "5:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I had a lengthy conversation with KR on topic typical of our conversation --- the decline of the dharma. Just when I came out of it the horror of negligence struck me... But I walked through the crematorium like vikrama uttering the guhya mantra.

"Every day men watch others like them die, yet they believe they might live one".\
We flew out on the high path borne. No यक्षिणि was with us, yet we soared transcending the सागर-s and द्वीप-s. We moved back and forth in time and space as though none of it limited us. We stood high above the stone building we had frequented in the days of bhoga. That mAyA जाल that had wrecked us on the one day in the एकादशम् came upon us like a spider's web. We were reminded of the sand-cockroach caught in the large nephelioid spider's web, with a kleptoparastic male having a ball of a time. We now evaded it soaring away and then diving back like a sand-piper. We came face to face with गुह्यसोमा; she too had arrived there on her long flight. We were lost in her embrace--every moment of it was like draught of तीव्र soma, the limitations of space and time had ended. I saw the भल्लूक that ambushed me in front of all on that fateful day in the ekadasham, when कुलुञ्च putra hurled a spear at me. But गुह्यसोमा capable of पराकाय-pravesha entered the body of both the भल्लूक and कुलुञ्चपुत्र at the same time and neutralized them.

With our arms wrapped around pretty गुह्यसोमा, like the roots of an ashvatta tree, with our minds merged into one we advanced to the the great circle of the श्मशान. We drank the soma from the graha of the shining moon. Around us in the reflection of the white graha we saw bones and corpses. Some burnt, some waiting to be burnt and other reduced to ashes. Like the king of the kuru-s on his last walk we walked surrounded by the air of मृत्यु -- the smell of oxidizing proteins permeated the air. But we were accompanied by गुह्यसोमा, with the masses of her hair like the monsoonal clouds stretching over the parched भारतवर्श, exciting in us the rasa-s like parjanya the indra-gopaka-s and नीलपर्वत-s. The light reflecting of the उपाम्शु cup falling on her breasts stirred us from within with the force of अनङ्ग, even as a तिमिङ्गल agitating the high seas. The sweet smell emanating from गुह्यसोमा was like the gandha-वाह blowing the unguents of indra's svarga with a bevy of अप्सरा-s. The cup of soma dipped down to pour the oblations to the deva-band led by shatamanyu. For a moment the sky stood in stillness at the point of the संध्या. गुह्यसोमा closed our eyes with her soft palms that felt like the balm of the ashvin-s.

At that point the आकाश above and the chid-gagana became one. We enfolded गुह्यसोमा and a tremendous energy entered us -our age vanished and we became young again -- there was light but no luminary. We attained the state of वरुण propping up the नक्षत्र of the day in our system. I said to गुह्यसोमा: "I am the मयूख of विष्णु, you are the आकाश. I am free from the limitations." In that आकाश only one luminary then rose on the Eastern rim- she was भानवीकौलिनी. गुह्यसोमा said to me: "Behold me O भार्गव, Let the chit receive the reflection of the avyaya-s and let the manas not screen it. Behold me and let the unfolding happen in the chid-gagana" The krama of thirteen काली-s fill the revolution of time-- The essence of rasas that induce उल्लास flashed all at once with the emergence of each काली. For a moment we saw the glory of the the 13 देवी-s, all conjoined with the bIja prefixes and the ambA पाद suffix.


