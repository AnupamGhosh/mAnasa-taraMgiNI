
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Why has he attained eko मानुष Ananda ?](https://manasataramgini.wordpress.com/2007/09/19/why-has-he-attained-eko-manusha-ananda/){rel="bookmark"} {#why-has-he-attained-eko-मनष-ananda .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 19, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/19/why-has-he-attained-eko-manusha-ananda/ "5:41 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We finally had that chance for the wonderful meeting with ekanetra and ST. ST said: "I think I have found one who has एकोमानुष Ananda". "Lead us to that one" we said. ST lead us to her charming friend जायद्रथि, who in someways reminded us of our friend R. ekanetra who knows the ways of the स्त्री-s declared to ST and me in संध्यभाष: "This जायद्रथि has not attained eko मानुष Ananda." But we knew that she could lead us to that one who had attained eko मानुष Ananda. She took us across the nadi of bhaya to the great palace in which lived the one graced by the mother of the राक्ष-s who adorns the southwest. He was one who had attained eko मानुष Ananda. On seeing him we knew this. We were awed by his power and dazzled by the energy emanating from him. He was surrounded by his several अङ्गन-s, putras and भृत्य-s all of coral or shell-like complexion. The जायद्रथि said he is a महापुरुष like whom no one exists. We saw him in wonderment, made strongly aware of our own fragile and powerless selves. His presence towered over ours like a gigantic statue of the tathagata made by the nAstIka-s. He truly looked like one for whom indra had given all. Then did ST ask that profound question: "Why has this निरृत्ति-priya attained eko मानुष Ananda ?"\
ekanetra said: "I believe he has been given all by indra. After all the श्रुती stated: युवास्यात् sAdhu युवाध्यायकः ... सर्वा vittasya पूर्णास्यात्".\
I said: "O वैश्वामित्र I do think that the vajrin girt by the maruts and aided by the triple-striding विष्णु has given us many things that he has not given that महापुरुष, but shatamanyu has yet given him eko मानुष आनन्दः."\
ekanetra said: "That might be so, O भार्गव, but why has he attained it while we have been consigned to our sthala-s"?\
ST: "But we all know that वृत्रहन् places all good things apart."

Then we went to hang out with the जायद्रथी and enjoy the pleasures with which ST could inundate, the tongue, the nose, the eyes and the belly. ST's new inventions in sUpa-kala was like the soma drunk by dashadyau in the realm of shatakratu or the food eaten in suvargo loka. This was pleasing to the external senses. But the presence of जायद्रथी caused within us an emptiness --- a sense utter failure of our astra-s, like the वानर-s crumbling before कुम्भकर्ण's last march. We then moved away and went the regions of Hayastanika. As we have always mentioned in the past Hayastanika emanates a tremendous revitalizing energy. The very sight of Hayastanika, who was a like materialization of a side glance of कामेश्वरी enlivened our spirits, like the trikadruka-s refreshing maghavan. Hayastanika, who exceeds most in the combination of physical might, beauty, and intelligence shone like a Venus in the firmament of the senses. The inner senses were delighted, even as a momentary glimpse of the मयूख-s of कौलिनी arouses the sadhaka to supreme delight. Delighting in the company of she whose tresses are like the prop roots of a nyagrodha tree we let some time pass.

...*...

We had defeated the evil कृश-mukha in battle. It was glorious victory, one of the biggest ever we had scored after the fierce battle of सप्तदशान्त. It was similarly bloody. In other ways it was like the great victory of दक्षिण nadi, but there our forces had total superiority in every way. Our vairi-s, the म्लेच्छ chiefs, trembled before our forces to whom mahendra had given the signal in the form of the eagle after we had performed the autumnal rite. With indra on our side we totally dominated the battle from start to finish. On the way we scattered some chIna-s who dared to challenge us. Here, कृश-mukha अञ्कुश-नास, विट्-mukha आणिशिरस् and their allies formed a vyuha against us. But several म्लेच्छ-s from their side came over to ours as we used diplomatic tactics to go hand-in-hand with the offensives. Thus, we prevailed over them. It was one of our great victories but in a sense empty --- our mitra with whom we had gone to the center of the town in a mirthful advance was not there to celebrate. In a sense, we felt like king युद्धिष्ठिर after that fateful clash of 18 days on the kuru field:\
The four vIra-s were scattered; It was like the forgotten struggle of the bundela-s against the Moslems; We were unable to enjoy the mirth our brave vIra-s who faced the fury of the enemy arrows as the banners were mixed and the dust rising to the heaven the great clash of men.

...*...

On the other front despite this great victory the खाण्डवन्-s remained active. The unseen one who deployed that strike of the "crane" was also active. It was the 3rd time the strike of the crane was being deployed, one for each अर्धसंवत्सर.


