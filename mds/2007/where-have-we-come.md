
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Where have we come?](https://manasataramgini.wordpress.com/2007/09/10/where-have-we-come/){rel="bookmark"} {#where-have-we-come .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 10, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/10/where-have-we-come/ "5:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ekanetra brought to mind an old incident. दुष्ट-putraka, gola-mukha and पीडामुख were all exultant being at the brink of a great victory against us in the patra-spardha. But ekanetra and me rallied our forces and snatched victory from them. I mentioned that mantra of the इक्ष्वाकु emperor trasadasyu invoking indra in that battle when purukutsa had been bound. 17 years ago ekanetra, ST, R and me rode on our ashvas past the palace of the founder of the Maharatta nation turned left and went a long way. Having tied the ashva-s and laid a spell of कञ्काल bhairava to ward off chora-s and taskara-s who might steal our ashva-s. We engaged on an energetic climb. Having, taken the path below the प्रासाद of धूर्त-svamin we reached that clearance on the basaltic spur where seated ourselves on the 4 rocks. Interestingly, we realized that we were all descendants of agnichit-s, but something about us was more reminiscent of the decline of the brahminical glory, rather than agni fed by the ब्राह्मणस् shooting to the skies. In some sense we felt a melancholy identity with [Kareik](http://manollasa.blogspot.com/2007/04/last-arya.html) and Baghashai, something I have often alluded to in these pages- that sense of decline and demise.

It was not entirely a case of the cashing of the [check](http://manollasa.blogspot.com/2004/08/brahmin-brain-drain.html) that we talked about before (though this was true to some degree of ST and R. I must state in advance that R does not agree at all --be pacified). But it was something else. Just then मुण्डपुत्र and नास्तीकपुत्र walked by. They told us what they were planning to do and asked what we were up to. We exchanged notes. Both parties were utterly perplexed as to why the other would want to do what it stated. So we parted ways. ekanetra tore a piece darbha grass and asked me what do they mean? In my idealism of youth I declared that it did not matter because we will emerge winners due to our superior clarity of thought and extensive theoretical framework. ekanetra agreed readily. But subsequently I realized that clarity of thought and the framework were fine but the tide had turned against us. This is the other cause of the decline. I compare it to ecological change--when the climate suddenly changes there is extinction. What were once highly successful species go extinct rapidly.

Coming back to the present: ekanetra, ST and me were on the phone. They asked what remains of that lost world ? I could not express myself though they were all there embedded in memory as vague reflection on the consciousness. For a moment it brought to fore the very question of the nature of conscious. Then I gazed at Google Earth and recalled the क्षेत्र where all the play of youth had transpired, where the gods had placed our foes in the jaws of the yama, where we used the विद्वेषण mantra to defeat the dasyu. To our mind came the ऋग्वेदिc mantra: ["]{style="color:rgb(255, 102, 102);"}[अहम् इन्द्रो रोधो वक्षो अथर्वणः ]{style="font-style:italic;color:rgb(255, 204, 102);"}". Suddenly fragments emerged: The three पीठ-s -- 1) of delusion 2) of sorrow 3) of joy. Our encounter with Mis-creant in all those 3 पीठ-s came to mind. The root lay in the lair of delusion, the door to knowledge in the station of sorrow (the long yarn with her on dipterans, neutrally evolving sequences, ग़्ट्Pअसेस् and GATA-type zinc fingers domains), and the ephemeral Ananda in the causeway of joy. The सामवेदिन्, our teacher's student, tried to grasp it but was felled. Verily he was sunk like the eastern singers being united with the vajra of the wrathful thunderer. "Think of those brahmins who were annihilated by indra's vajra", I cried. ekanetra replied "think of the तैत्तिरीयक who fell prey to the agni shalya when the atreya's father-in-law sacrificed". ST did not understand. But her intuition is great. She said: "from beneath the cover in the realm of joy had slipped away that thing which you continue to seek. Will it be that you chase it even as Orion chases his quarry, with two nets and two dogs in the welkin."


