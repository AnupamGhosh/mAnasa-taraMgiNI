
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A विद्या of अथर्वण-bhadra-काली](https://manasataramgini.wordpress.com/2007/01/29/a-vidya-of-atharvana-bhadra-kali/){rel="bookmark"} {#a-वदय-of-अथरवण-bhadra-कल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 29, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/29/a-vidya-of-atharvana-bhadra-kali/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The atharvaN-s have several distinctive prescriptions of mantra prayoga-s for प्रत्यङ्गिरा or अथर्वण-भद्रकाली. One such is the महाचण्डयोगेश्वरी विद्या. It is recommended only for those who study the AV, though no niyama or पुरश्चरण is required for deployment.\
पिप्पलाद ऋषिः; Chando नास्ति यजुष्ट्वतः; अथर्वण-भद्रकाली जयदुर्गा देवता |\
OM बीजं; ह्रीं शक्तिः; jaye cha विनियोगः|\
AM AM AM हृदयाय नमः |\
IM IM IM shirase स्वाहा |\
UM UM UM शिखायै वषट् |\
EM EM EM कवचाय huM |\
AuM AuM AuM netra-त्रयाय वौषट् |\
प्रेखं अस्त्राय फट् ||

AM AM AM अङ्गुष्ठाभ्यां नमः |\
IM IM IM तर्जनीभ्यां नमः |\
UM UM UM मध्यमाभ्यां नमः |\
EM EM EM अनामिकाभ्यां नमः |\
AuM AuM AuM कनिष्ठिकभ्यां नमः||\
प्रेखं karatalakara-पृष्टाभ्यां नमः ||

dhyAnaM\
shyAmAm-indu-धरां देवीमा.a.a.ताम्र-nayana-त्रयीम् |\
वामे rakta-कपालं cha त्रिशूलं दक्षिणे tatha ||\
कृशोदरीं rakta-वस्त्रां pINa-स्तनीं नितम्बिनीम् |\
पद्मस्थां युवतीं ध्यायेत् स्मेरा.a.अस्याम्-ati सुन्दरीम् ||\
भद्रकालीं महादेवीं जयदात्रीं सुशीतलां |\
pujayed AsurI पीठे स्युस्तदा.a.अवरणानि cha ||

OM ह्रीं चण्ड्योगेश्वरी ! फट् ! स्वाहा ! \[for homa]\
OM ह्रीं चण्ड्योगेश्वरी ! फट् ! नमः ! \[for पूज]


