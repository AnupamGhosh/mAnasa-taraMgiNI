
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The sense of पारोक्ष and अनुमान](https://manasataramgini.wordpress.com/2007/10/16/the-sense-of-paroksha-and-anumana/){rel="bookmark"} {#the-sense-of-परकष-and-अनमन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 16, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/16/the-sense-of-paroksha-and-anumana/ "4:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

What constitutes the meeting of the minds with a vipra (used in the sense of the vedic kavI, not used in the sense of the वर्ण)? We wondered about this many a time. The vipra is one who has the sharp sense of अनुमान and can express satya as a पारोक्ष वाक्य or a काव्य. The true meeting of minds with vipra-s happens only when the other person understands the style of पारोक्ष वाक्य-s and काव्य-s. Thus, when the two such converse one might have what can be called a brahmodaya. Hence, brahmodaya is the form in which vedic knowledge was transmitted.

In my existence there have been few with whom I have had this intersection. With such even if the level of knowledge is different the meeting can occur. First and foremost it was it was the learned muni whose path was set apart by the great wielder of the vajra who is hymned by all the great vipra-s of yore: "yas तिग्मशृङ्गो वृषभो na bhImA एकः कृष्टीश् च्यावयति pra विश्वाः". The muni alone can grasp some of the flights of the mind and can express these as काव्य if required like the kavI-s of yore. We can delight in वाक्य-संदोह and darshana-वाद.

Other having come in our orbit have begun to feel this intersection with us, but other despite coming in our orbit fail to do so. It is rarely among स्त्री-s that such an intersection can happen --- by default they do not see the पारोक्ष but see only the स्थूल all the time. मातश्री could sense the पारोक्ष but was limited in the sense of अनुमान. तैलोदका had अनुमान but lacked पारोक्ष and was hence limited. but कलशजा has them all, like the promulgator of the ardha-tryambaka विद्या.

Those who live in the realm of पारोक्ष cannot mix well with those who do not, for the pursuits of the rest seem too crass. But they can observe the rest and look at these from afar like a naturalist observing snails.


