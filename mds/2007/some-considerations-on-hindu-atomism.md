
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some considerations on Hindu atomism](https://manasataramgini.wordpress.com/2007/04/08/some-considerations-on-hindu-atomism/){rel="bookmark"} {#some-considerations-on-hindu-atomism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 8, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/08/some-considerations-on-hindu-atomism/ "4:59 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[तनीयांसं पांसुं तव चरण पङ्केरुहभवं]{style="font-style: italic;color: #0000ff"}\
[विरिञ्चिः संचिन्वन् विरचयति लोकान् अविकलम्]{style="font-style: italic;color: #0000ff"}\
[वहत्य् एनं शौरिः कथम् अपि सहस्रेण शिरसां]{style="font-style: italic;color: #0000ff"}\
[हरः सं-क्षुभ्यैनं भजति भसितोद्-धूलन-विधिम् ॥]{style="font-style: italic;color: #0000ff"}(saundarya lahari 2)

Minute particles have arisen from the dust of your feet,\
gathering these together brahmA constructs the orderly universe,\
विष्णु with his thousand heads verily holds these,\
hara in destroying the universe splits these apart and smears himself with them as ash.

The learned कैवल्याश्रम, the late medieval tantric guru from an orthoprax मट्ःअ in the southern Konkans, in his commentary on the saundaryalahari clearly explains this shloka as describing the परमाणुस् as emerging from the feet of tripura-sundarI and being used by the trimurti in their loka-कार्यम्. In his commentary mentions how the fundamental particles the परमाणु-s combine to give rise to the basic अणु combinations like द्व्याणु-s and त्र्याणु-s. This is consonant with the mantra prayoga of this shloka --- mastery over the knowledge of all matter or nature (Long back while studying this shloka I was inspired by षोडशिका that great mistress of the kula path, flanked by सञ्केता and सचिवेशी).

In my opinion, one of the pinnacles of Hindu thought, along with पाणिनि's study of language are the वैशेषिक sUtra-s of कणाद. The वैशेषिक thought in its pristine form was largely dead in the later days of Hindu thought. Yet, it was the beginning of physics in India, and the foundation of a very productive line of addressing the universe. To us (this a very personal view) वैशेषिक is the best of the darshana-s for a veda-practicing आस्तीक in the context of modern knowledge. Many Hindus are unable to grasp the significance वैशेषिक. We believe that वैशेषिक is a prelude to scientific thought and not just a metaphysics. Thus, a "modern darshana" that an आस्तीक can follow without a contradiction in Weltanschauung would build from the base of वैशेषिक, just as medieval Hindus built on early वैशेषिक thought (e.g. [वाचस्पति mishra](http://manollasa.blogspot.com/2004/09/vachaspati-on-atomic-coordinates.html)). Sadly many early works of the pristine वैशेषिक period are now lost. It was once a great challenge to the nआस्तीक-s who expended their efforts to undermine or co-opt it.

Some early lost वैशेषिक works are preserved in citations of nआस्तीक-s in their debates against the आस्तीकस्. One example is given by परमार्थ (499-569 CE), an आचार्य who traveled from bhArata to the land of the chIna-s to teach darshanas and translated a number of works from Sanskrit to chIna-भाषा during the short-lived Liang empire. In one such, the लक्षणानुसार शास्त्र, he describes वैशेषिक, albeit mistaking certain points. He gives an account of a "तीर्थक", a successor of प्रशस्तपाद, on the universe, which in broad outline agrees with the idea that saundaryalahari 2 has as its background: 1) The universe has two kinds of संवर्त-s (end or dissolution) --- the antara संवर्त and the तेजः संवर्त. 2) The former संवर्त occurs at the end of each kalpa comprised of approximately 10^10 years, the total life of the universe being roughly 3*10^10 years. 3) The first kalpa is known as the tapah kalpa (the period of heat), the second the the jala-kalpa (the period of liquid) and the third the वायु-kalpa (the period of gas). 4) At the end of each kalpa all matter is dissolved into the constituent परमाणु-s (fundamental particles of वैशेषिक) and exist in state of disjunction. 5) At the beginning of each kalpa the परमाणु-s combine due to the unseen forces (as कणद states) and produce अणु-s that grow larger in size to produce the entire universe. 5) The consciousness and mind are conjoined or separated when atoms do the same. 6) After three rounds of tejah संवर्त one coming at the end of each triadic universal period the universe lapses into a state of pure disjoint परमाणु-s and consciousness.

A certain parallel with jaimini's sUtra-s and the वैशेषिक sUtra-s exists suggesting that their authors belonged to a common intellectual tradition (i.e. successors of the veda):\
[अथातो धर्म जिज्ञासा ।]{style="color: #0000ff"}(MS1)\
[अथातो धर्म व्याख्यास्यामः ।]{style="color: #0000ff"} (VS1)\
both mean the approximately same (जिज्ञासा= inquire into or व्याख्यास्यामः -- explain) and are clearly distinguished from the inquiry into brahman or yoga or causes of sorrow or principles of logic.\
[छोदनालथो अर्थो धर्मः ।]{style="color: #0000ff"} (MS2)\
[यतोऽभ्युदय निःश्रेयस सिद्धिः स धर्मः ।]{style="color: #0000ff"}(VS2)\
The constructs are very similar between मीमाम्स and वैशेषिक, but the former declares that the veda's prescription is dharma. Whereas, वैशेषिक declares that one that produces abhyudaya and निःश्रेयस is dharma (Note that there two distinct terms used and not mokSha. Much after the pristine वैशेषिक faded away, later authors thought that these terms were related to mokSha of vedAnta).

[तस्य निमित्त-परीष्टिः ।]{style="color: #0000ff"} (MS3)\
[तद्वछनाद् आम्नायस्य प्रामाण्यं ।]{style="color: #0000ff"}(VS3)\
Here again the two darshana-s probably imply similar (not same) things: The मीमाम्सक-s seem to mean (going by shabara स्वामिन्): examination (of veda) is proof of that (dharma).\
The अणुवादिन्स् state: authoritativeness rest with that vedas because it is a declaration of that (dharma).

So of all darshana-s these are the two that directly express their intent on dharma and its vedic roots. \[As an aside- Since the तथागत and his successors wanted to subvert precisely this, namely dharma, they used the same term dharma and tried to give it an entirely different meaning. It is not a big surprise that the initial conflicts of the bauddha nआस्तीक-s was precisely with these two darshanas (e.g. buddha attacking उरुवेला जटिल kAshyapa).]

We observe that वैशेषिक thought is present in the bhArata and the पुराण-s. In the former it is seen in the lecture of the great kShatrIya woman सुलभा प्राधानी during the discourse with the king of मिथिला. In the पुराण-s, for example the bhagavata पुराण, it appears in the lecture of [maitreya](http://manollasa.blogspot.com/2004/09/maitreyas-atomism.html). However, unlike सांख्य (yoga) and in the much latter पौराणिc milieu, vedAnta, neither मीमाम्स nor वैशेषिक are discussed in their technical details in these texts. Again, चाणक्य only mentions सांख्य, yoga and लोकायत, suggesting the former two were the "popular" philosophies that were seen as successors of the upaniShadic philosophical speculation. Both मीमाम्स and वैशेषिक instead appear to have arisen in a specialized setting of vedic ritualists (ironically bauddha nआस्तीक-s also later arose in this setting), but assumed very different paths despite certain similar foundational concepts.

Many philosophers have noted the points of divergence of मीमाम्स and वैशेषिक after a similar opening of their darshana sUtra-s. वैशेषिक is not interested in the two prime issues of मीमाम्स: 1) The veda being by its very nature valid. 2) The eternal existence of the veda. While not bothered about these issues, in the very last sUtra-s (अध्याय 10, Ahnika 2), कणाद stresses the authoritativeness of the veda. Then both वैशेषिक and मीमम्स agree upon a critical point: There is no creator for the universe and there is no Ishvara. The universe is entirely a result of combination of the fundamental particles and the sole agent for their combination is karma. Finally, मीमाम्स depends heavily on the position that sound is eternal, and this is connected with its world view of the अपौरुषेयत्व of the veda. कणाद examines the possibility of the eternality of sound and then rejects it. To me in addition to the above positions, it is this position on sound that makes वैशेषिक an over all superior philosophy of all the hindu systems.

If the opening stress on dharma and the closing stress on the veda is an important feature of वैशेषिक, then the question arises as to its [specific ]{style="font-style: italic"}vedic antecedents if any. The use of वैशेषिक terminology in charaka's medical treatise is an important pointer in this regard. In charaka, वैशेषिक is heavily overlayed by the "populist" संख्य thought. It is possible that in the yajur-vedic school of charaka and कठस् arose the early formal वैशेषिक which continued to linger on the later medical treatises produced by the former school. \[Of the कृष्ण yajurvedin-s the कठस् appear to be closest to the charakas. The ब्राह्मण of the kaTha-s preserved now as the terminal ब्रह्मण of the काठक section of the तैत्तिरीय-s has a मीमाम्स on the chyana with different metals, like gold, silver, lead etc. It is in this context that the पंसु-s (atoms) are mentioned.] An important corollary to this use of वैशेषिक thought by the charaka-s is that वैशेषिक actually served as a proto-scientific naturalistic base in hindu analysis of nature's phenomena. The charaka-s saw that वैशेषिक theories provided a reasonable frame to understand chemistry of substances, and there by determine the behavior or particular substances in the animal body or guide the deployment of particular substances in therapeutics.

That वैशेषिक theory was principally meant as a system of proto-scientific explanation is seen in its application within its tradition starting from कणाद. उद्योतकार following the tradition used it to explain refraction of light. udayana used it to explain how heat from the sun was the ultimate source of all earthly heat stores. भट्ट jayanta used it to explain photo-chemical reactions. It also influenced the great nआस्तीक intellectual, the jaina उमास्वति who incorporated the वैशेषिक concept of संयोग, विभाग in the form attractive and repulsive forces between परमाणु-s and अणु-s.

In conclusion it might be said that the ancient Hindus had in वैशेषिक a proto-scientific frame work whose important features included: 1) The absence of a creator for the universe or an Ishvara as the cause of the universe; 2) The construction of the universe through the combination of fundamental particles परमाणु-s- "atoms" to give rise to "अणु-s" molecular particles. 3) The particle nature of light and heat. 4) An explanation of natural phenomena using molecular and atomic particles and a small set of basic physical forces. 5) karma chiefly implies physical forces and force itself has a particle nature. 6) The mind is molecular in nature. 7) consciousness is not per say "non-material" but associated with matter- perhaps as a distinct particle or as a "property" of a particle, perhaps like charge.

But going by later Indian thought and modern Hindus it is clear that this naturalism has been receding to the background. In this context, I have been puzzled by certain observations: I believe that the majority of modern Hindus believe in Ishvara as the cause of the universe or are creationists of different grades. Some may restrict themselves with seeing the universe's cause as the Ishvara, others may see all the diversity and complexity of nature as a sign of Ishvara. Yet others may believe in the special entities like the सूक्ष्म sharIra transmitting their pApa-s and पुण्य-s from one स्थूल sharIra to another and so on. Majority of Hindus using the Abrahamistic terminology say they believe in God and even go through some effort to say that they are not polytheistic but actually believe in one basic God. Many Hindus and particularly Hindu vedAntic अचार्य-s do not accept evolution of life by natural selection. Is all of this because of Abrahamistic subversion of their thought alone? Or is it because they have been pre-conditioned for this mental state due to their separation from old वैशेषिक and सांख्य thought?

I feel that it might be a combination of both factors. If this were so it is disappointing that Hindus have actually lost their intellectual rigor and are no longer philosophically innovative as their predecessors.\
...\
At this point the learned jaina muni-s bhuvana-vijaya and jambu-vijaya must be acknowledged for their tremendous services to the आस्तीक-s. A proper text of the वैशेषिक sUtra-s had been hard to get in modern times, in part due to the above reasons. muni bhuvana-vijaya obtained pristine manuscripts of the वैशेषिक sUtra-s along with a commentary of चन्द्रानन्द (\~700-800 CE) from a jaina manuscript collection in जैसाल्मेर्, राजस्थन. His son jambu-vijaya carefully edited the text to provide a reliable version of the वैशेषिक sUtra along with चन्द्रानन्द-s commentary. चन्द्रानन्द cites the veda-s and पुराण-s showing his wide knowledge of Hindu lore in general. My teacher knowing my proclivities pointed to me to चन्द्रानन्द's commentary and its importance in grasping the vedic milieu in which वैशेषिक arose.


