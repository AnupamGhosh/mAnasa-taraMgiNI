
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The recent great apes of Africa](https://manasataramgini.wordpress.com/2007/09/02/the-recent-great-apes-of-africa/){rel="bookmark"} {#the-recent-great-apes-of-africa .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 2, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/02/the-recent-great-apes-of-africa/ "5:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Today 4 great apes still survive in Africa all of which share a more recent common ancestor to the exclusion of the Asiatic [Pongo ]{style="font-style:italic;"}and appear to have diversified relatively recently from a common ancestor in Africa. The 4 African great apes display the following indisputable phylogenetic relationship:\
((([Homo]{style="font-style:italic;"}([Pan paniscus]{style="font-style:italic;"}, [P.troglodytes]{style="font-style:italic;"}))[Gorilla]{style="font-style:italic;"})[Pongo]{style="font-style:italic;"})\
Molecular studies strongly support a divergence of [Homo ]{style="font-style:italic;"}from the [Pan ]{style="font-style:italic;"}lineage at best around 4.5 Million years ago. In contrast the fossil hunters have been digging up an amazing array of fossil apes from Africa that have only resulted in more questions than answers with respect to affinities between various apes, their divergence times and their biogeography. Let us look at these apes and some issues:

  - There are at least 3 representatives of genus [Homo ]{style="font-style:italic;"}that nobody doubts are closer to extant [Homo sapiens]{style="font-style:italic;"} than any other great ape: 1) [H.habilis]{style="font-style:italic;"}, 2) [H.erectus]{style="font-style:italic;"} and archaic [H.sapiens]{style="font-style:italic;"}. There is the popular belief that [H.habilis]{style="font-style:italic;"} spawned the later [H.erectus]{style="font-style:italic;"}, which in turn spawned [H.sapiens]{style="font-style:italic;"}. Nevertheless, recent finds increasingly support the proposal where the two species overlap : [H.habilis]{style="font-style:italic;"} and [H.erectus]{style="font-style:italic;"} seem to have overlapped by around 500,000 years suggesting that they were sister species. Further, these finds like those based on the skull cap KNM-ER 42700 raise the possibility that[ H.erectus]{style="font-style:italic;"} could have shown major sexual dimorphism, comparable to that seen in [Gorilla]{style="font-style:italic;"}. This is in contrast to [H.sapiens]{style="font-style:italic;"} where the dimorphism is low. Hence, it is possible that [H.sapiens]{style="font-style:italic;"} did not descend directly from [H.erectus]{style="font-style:italic;"} but archaic versions of [H.sapiens]{style="font-style:italic;"} were representatives of a sister lineage to [H.erectus]{style="font-style:italic;"}. Thus, we have the possibility that the divergence of these two [Homo ]{style="font-style:italic;"}species was related to differential niche colonization and life-style divergence, rather than a linear model of one spawning the other. Finally, with respect to archaic [H.sapiens ]{style="font-style:italic;"}there is the question of what is its relationship with modern early-branching African[ H.sapiens]{style="font-style:italic;"}. None the less, in the absence of further fossils that situation is far from resolved.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RuK6vvXcXjI/AAAAAAAAANs/X9T9hNtVfNI/s320/herectus_skulls.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[Dramatic size variation in adult [Homo erectus]{style="font-style:italic;"}, with retention of anatomical similarity]{style="color:rgb(51, 255, 51);"}

  - In addition to these problems, there is the issue of the relationship between African [H.erectus]{style="font-style:italic;"} and [H.habilis ]{style="font-style:italic;"}with the early [Homo ]{style="font-style:italic;"}from Dmanisi, Georgia. Some have taken these Eurasian specimens to imply a possible origin for early [Homo ]{style="font-style:italic;"}in Eurasia followed by back migration to Africa.

  - -[H.rudolfensis]{style="font-style:italic;"}: Based on the absence of the characteristic supraorbital torus seen in [H.habilis ]{style="font-style:italic;"}specimens like KNM ER 1813 or OH24 and a much large cranial capacity it was assumed that KNM ER 1470 is a different species of [Homo ]{style="font-style:italic;"}from [H.habilis]{style="font-style:italic;"}. This species was termed [H.rudolfensis ]{style="font-style:italic;"}and could potentially represent a precursor/sister group of [H.sapiens,]{style="font-style:italic;"} whereas [H.habilis]{style="font-style:italic;"} is exclusively allied with [H.erectus]{style="font-style:italic;"} (for example KNM ER 1813 might show an incipient transverse torus as seen in [H.erectus]{style="font-style:italic;"}). However, in the absence of further remains we cannot be sure of whether [H.rudolfensis]{style="font-style:italic;"} is indeed distinct or merely a part of the morphological variation spectrum shown by [Homo ]{style="font-style:italic;"}of that period.

  - The Australopithecines: The australopithecines clearly appear to be closer to [Homo ]{style="font-style:italic;"}than to [Pan]{style="font-style:italic;"}. 3 major forms of australopithecines are recognized along with many other "controversial" forms. They are [1) A.afarensis 2) A.africanus ]{style="font-style:italic;"}and 3) the robust australopithecines, which are often given a separate genus [Paranthropus]{style="font-style:italic;"} and 3 "species" are recognized within them: [P.aethiopicus, P.boisei]{style="font-style:italic;"}, and [P.robustus]{style="font-style:italic;"}. Their robust flared skulls with enormous muscle attachment sites like the striking sagittal crest suggest a highly specialized ecology in terms of high fiber vegetable diet. More recently several other species have been named:\
[A.anamensis]{style="font-style:italic;"}-- this appears to be an form of the [A.afarensis, ]{style="font-style:italic;"}currently going back to 4.1 MYA. It has been recognized a different species mainly due to its early age, but morphologically is within the observed variability of [A.afarensis]{style="font-style:italic;"} and probably represents the earliest members of that lineage. It is interesting to note that it is close to the molecular dates for the human-chimp split suggesting that chimp-human lineage inter-mating might have continued to about the time "[A.anamensis]{style="font-style:italic;"}" i.e. the earliest representatives of [A.afarensis ]{style="font-style:italic;"}appeared.\
[\
A.bahrelghazali]{style="font-style:italic;"}: this is the western-most specimen of an australopithecine ape, and all that is known of it is a single mandible that appears to be very close to [A.afarensis]{style="font-style:italic;"}. However, on the basis of a 3-rooted premolar it was made a separate species. Most likely it represents a poorly known western population of [A.afarensis ]{style="font-style:italic;"}that might have at best undergone regional diversification.

[A.garhi]{style="font-style:italic;"}: this ape appears to be a larger version of [A.afarensis]{style="font-style:italic;"}. However, it was given a separate species status because of it is unusually large premolars and molars. It is also reported as having a crest, which is reminiscent of the robust australopithecines. Post-crania from the same site suggest a long femur, like [Homo ]{style="font-style:italic;"}for this species. Its describers are fairly certain that it might represent a transitional form between [A.afarensis ]{style="font-style:italic;"}and [Homo]{style="font-style:italic;"}. Given the scrappy nature of the finds it remains unclear if it might represent a distinct species of [Australopithecus]{style="font-style:italic;"} or a variant of[ A.afarensis]{style="font-style:italic;"}.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RuK5tvXcXiI/AAAAAAAAANk/hC6tJaLys5o/s320/primate_ramus_convergence.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[Convergence of ramal anatomy of australopithecines and ]{style="color:rgb(51, 255, 51);"}[Gorilla]{style="font-style:italic;color:rgb(51, 255, 51);"}[ as discovered by Rak [et al]{style="font-style:italic;"}.]{style="color:rgb(51, 255, 51);"}

The comparisons of jaw morphologies suggest that the well-established 3 australopithecine species share a derived jaw morphology, which appears to be convergently very close that of the [Gorilla]{style="font-style:italic;"}. The [Homo ]{style="font-style:italic;"}lineage in contrast retains the primitive state. It has hence been proposed that the well-established australopithecine lineages may hence be precluded from human ancestry. In light of this, we need to see what the anatomy of [A.garhi]{style="font-style:italic;"} is, if it ever becomes better known, and whether it might be closer to [Homo. ]{style="font-style:italic;"}But the take-home message is that convergence between related apes can mess up phylogeny, especially when we are dealing with such fragmentary remains.

  - -[Kenyanthropus platyops]{style="font-style:italic;"}: While described as a new ape contemporaneous with [A.afarensis]{style="font-style:italic;"} there has been much debate about its validity due to the poor preservation of the fossil. However, one should note that it has a small external acoustic porus compared to other australopithecines suggesting that it might indeed be a distinct lineage. Some have noted similarities with [H.rudolfensis]{style="font-style:italic;"}, but these may indeed be superficial and convergent. Yet again this is another case of a tantalizing fossil without a proper phylogenetic home.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RuK7uvXcXkI/AAAAAAAAAN0/0VQDl-CObLA/s320/Ardipithecus_chimp.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[Dental similarity of ]{style="color:rgb(51, 255, 51);"}[आर्दिपिथेचुस् कदब्ब]{style="font-style:italic;color:rgb(51, 255, 51);"}[ and]{style="color:rgb(51, 255, 51);"}[[ P.troglodytes]{style="color:rgb(51, 255, 51);"} ]{style="font-style:italic;"}

  - -[Ardipithecus]{style="font-style:italic;"}, [Orrorin ]{style="font-style:italic;"}and [Sahelanthropus]{style="font-style:italic;"}: These 3 apes appear to be the oldest of the apes belonging to the African great ape clade. All the 3 apes have been claimed to be in the human lineage to the exclusion of [Pan.]{style="font-style:italic;"} All three have been claimed to be erect bipedal walkers. One group has even gone as far as a to claim that all 3 are even the same genus of ape. However, they are all quite enigmatic as yet, and all of the above claims are definitely debatable and probably even incorrect. [Ardipithecus ramidus]{style="font-style:italic;"}, which was identified first was claimed to be on the human lineage, after its divergence from the chimps and the common ancestor of the Australopithecines from which [Homo ]{style="font-style:italic;"}is eventually believed to have been derived. Subsequently a second species, [Ardipithecus kadabba]{style="font-style:italic;"} was described. [Ar.ramidus]{style="font-style:italic;"} was dated to about 4.5 MYA, whereas [Ar.kadabba]{style="font-style:italic;"} was dated as being 5.4-5.8 MYA. By the molecular criteria [Ar.kadabba]{style="font-style:italic;"} squarely falls in the period when the chimp and human lineages were still mating and probably only barely separated. Consistent with this [Ar.kadabba]{style="font-style:italic;"} is very chimp-like especially in its dental features, like the upper-canine lower premolar occlusion. This raises questions regarding whether the available features truly distinguish whether Ar[dipithecus ]{style="font-style:italic;"}belongs to the chimp to human lineage. Between[ Ar.kadabba]{style="font-style:italic;"} and[ Ar.ramidus]{style="font-style:italic;"} are we seeing gradual divergence or is it merely an issue of sample size?

[Orrorin ]{style="font-style:italic;"}is dated to around 6 MYA is again fragmentary and poorly described. [Sahelanthropus ]{style="font-style:italic;"}has recently been re-interpreted and definitely seems to belong outside of the [Homo ]{style="font-style:italic;"}lineage. Based on molecular criteria both these apes seem to be members of common ancestral lineage of [Pan ]{style="font-style:italic;"}and [Homo]{style="font-style:italic;"}. They either represent the common ancestor itself or radiations of that ancestral lineage. Most striking is the biogeography of [Sahelanthropus --- ]{style="font-style:italic;"}like "A.[bahrelghazali]{style="font-style:italic;"}" it is from central Africa suggesting that around 6 MYA the great apes of Africa were already well distributed over the continent. It also shows how little we know of the real spread of the African great ape clade. This has considerable consequences for inferences regarding the ancestral habitat of this clade, as well as the role of habitat, if any, in the emergence of the human lineage.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RuK-IfXcXlI/AAAAAAAAAN8/zv4rv81Tj8A/s320/chororapithecus.jpg){width="75%"}
```{=latex}
\end{center}
```

\
Comparison of [Chororapithecus ]{style="font-style:italic;"}and [Gorilla ]{style="font-style:italic;"}

  - -[Chororapithecus abyssinicus]{style="font-style:italic;"}: The latest of the African great apes to be reported. It is a pathetic assemblage of a few gorilla-like teeth from a 10 MY old Chorora formation near Afar. The teeth surely look like a gorilla in gross as well as a certain subtle features of enamel-dentine ratios, but they are too little to say much, given the rampant convergence of various dental and mandibular features amongst great apes. All we can say is that there was a large gorilla-sized great ape around 10-11 MYA in Africa. This definitely fills in the "ape gap" of Africa and raises questions as to whether the African great apes are a re-invasion from Eurasia (after the [Sivapithecus-Pongo]{style="font-style:italic;"} lineage migrated there). After all this means African great apes could have emerged in Africa itself from a [Dryopithecus]{style="font-style:italic;"}-like form and only the [Pongo ]{style="font-style:italic;"}lineage left to Eurasia.


