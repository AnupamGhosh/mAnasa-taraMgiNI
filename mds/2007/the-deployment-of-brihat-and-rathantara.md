
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The deployment of बृहत् and rathantara](https://manasataramgini.wordpress.com/2007/01/10/the-deployment-of-brihat-and-rathantara/){rel="bookmark"} {#the-deployment-of-बहत-and-rathantara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 10, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/10/the-deployment-of-brihat-and-rathantara/ "7:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The ancient भार्गव उशना काव्य has declared how the बृहत् and rathantara are to be deployed for attacking the भ्रातृव्य-s. When dispatched the two सामन्स् assumes the form of a chakra of brahman and proceed to slay the foe even as the deva-s had imbued a chakra with the force of indra's vajra to overthrow the दानवस्. At the same time it protects the यजमान.\
The ritualist first sings the rathantara and makes a soma offering to indra who is glorified by the sAman. Then he utters the formula " namo मात्रे पृथिव्यै rathantara mA mA हिंसीः ||"\
Then he sings the बृहत् and offers soma and utters the mantra: " दिवं पितरं upashraye बृहन् mA mA हिंसीः ||"\
Then he recites both and utters the deadly formula: "प्राणैर् अमुष्य प्राणान् वृङ्क्ष्व |\
तक्षेन तक्षणीयसायुर् asya प्राणान् वृङ्क्ष्व | kruddha enaM मनुन्या दण्डेन jahi | dhanur enaM आतत्येष्वा vidhya ||"\
With that the brahman of the सामन्स् issues forth as a deadly astra in pursuit of the foe. The जैमिनीय declares "बृहद्-rathantarayor एवैनं mukhe .अपिदधाति | yaM द्वेष्टि taM पुरा संवत्सरात् प्राणो जहाति |"


