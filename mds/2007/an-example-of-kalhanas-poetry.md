
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An example of कल्हण's poetry](https://manasataramgini.wordpress.com/2007/08/30/an-example-of-kalhanas-poetry/){rel="bookmark"} {#an-example-of-कलहणs-poetry .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 30, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/30/an-example-of-kalhanas-poetry/ "5:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

guhon-मुखी nAga-मुखापीत भुरिपया रुचिं | गौरी yatra वितस्तात्वं याताप्ययुज्झति नोचितां | (rAja-तरङ्गिणी 1.29)\
Even though गौरी has assumed the form of वितस्ता, she does not leave her innate inclinations-\
she turns her face towards guha (कुमार) and the elephant-faced one (nAga-mukha- विनायक) drinks her abundant, tasteful milk OR \[she flows towards the ravine (guhon-mukhi) and distributary streams (नागमुखा= snake-mouths) drain her abundant tasteful water.]

वितस्ता is identified with गौरी because the river is born in himavat, just as गौरी.


