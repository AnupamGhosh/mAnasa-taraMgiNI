
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [गायत्रं](https://manasataramgini.wordpress.com/2007/03/13/gayatram/){rel="bookmark"} {#गयतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 13, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/13/gayatram/ "6:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RfZGsMcE7zI/AAAAAAAAAFI/S8Ci3qaS_7E/s320/gAyatram.jpg){width="75%"}
```{=latex}
\end{center}
```



