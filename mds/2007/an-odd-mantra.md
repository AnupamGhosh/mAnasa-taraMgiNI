
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An odd mantra](https://manasataramgini.wordpress.com/2007/03/23/an-odd-mantra/){rel="bookmark"} {#an-odd-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 23, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/23/an-odd-mantra/ "6:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R mentioned to me a while back a mantra of great significance to सरस्वती that her ancestors had acquired from Shymol Ram:\
OM धीः shruti-विज्ञा || (1)\
Neither R nor me have not been able to locate it any tantra available to us. However it appears as a mantra to प्रज्ञापारमिता in #658 of the highly corrupt Bali stuti saMhitA. As the Balinese were isolated from India in the Islamic period their Sanskrit education declined greatly and when they recorded these mantras and stuti-s they were horribly corrupt. In a very general sense the mistakes they make reminds one of the hilariously atrocious mistakes made by the द्राविडस् in transmitting Sanskrit via the degraded medium of Tamil. It would be interesting to see if the source tantra of the mantra can be traced from the Indonesian texts.\
With some searching we found a cunning variant of it bauddha tantric digest, the साधना-माला:\
OM धीः shruti-स्मृति-vijaye स्वाहा || (2)\
The nAstika in his typical way wants to present his deity as being the victor over the shruti and स्मृति of the Arya-- thus the पाषण्ड subtly alters the word विज्ञा to विजया to generate this variant mantra. This shows that the original did actually go from the Hindus to the bauddha-s and at least initially was preserved in the proper form (1).


