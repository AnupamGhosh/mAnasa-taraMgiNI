
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [वैखानस childhood rites pertaining to कुमार](https://manasataramgini.wordpress.com/2007/02/19/vaikhanasa-childhood-rites-pertaining-to-kumara/){rel="bookmark"} {#वखनस-childhood-rites-pertaining-to-कमर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 19, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/02/19/vaikhanasa-childhood-rites-pertaining-to-kumara/ "3:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Childhood rites in connection कुमार, षष्ठि and their गण-s are widespread in Hindu tradition. An early example of this offered by the वैखानस tradition. In the ceremony of नामकरण a series of blessings are bestowed on the child by the parents. In the series for a male child the following formulae a recited:\
[शाङ्करिर् iva सर्वत्राजयो bhava |\
]{style="color:#99cc00;"}Be invincible as the [son of शङ्कर]{style="font-weight:bold;"} (कुमार).\
[नर-नारायणाव् इव तपो-बलोत्कृष्टो भव ।]{style="color:#99cc00;"}\
Be rich in power rising from tapas as nara and नारायण.\
[लोकेशा इव सर्व-भूताधिपतिर् भव ।]{style="color:#99cc00;"}\
Be the lord of the beings like the gods.\
[बृहद्-ब्रह्म-गुणा इव सर्वार्थ-संसिद्धो भव ।]{style="color:#99cc00;"}\
attain all worldly needs like बृहद्-brahma-guNa-s (?). (वैखानस स्मार्त sUtra 3.19.20)\
The blessing for the new born son to be like कुमार are seen in other texts such as those of sushruta and charaka, where there invocations not only for the son to be like कुमार, but also invocations to protect the child from कुमार grahas.

The rite to be performed when the chlid is taken out for the first time. After returning from the outing the faher takes flowers, sweets, fees for the ritualist and the along with the child goes to a [कुमार-गृह](http://photos1.blogger.com/x/blogger2/6438/855/1600/251284/कुमारgRiha.png). On the way he keeps muttering the mantra kanikradat suvar apo जिगाय (TS.2.3.14.6) and the shakuna-सूक्तं. He goes around the कुमार-गृह east to west and then enters it. He worships the image of guha therein by offering flowers and sweets. He then takes some of those flowers and adorns the child saying:\
[[गुहस्य]{style="color:#99cc00;"}]{style="font-weight:bold;"}[सेषं गुरुभिः सुपुजितं पुष्पं ददामि -स्य]{style="color:#99cc00;"}[[षण्मुखात् ।]{style="color:#99cc00;"}]{style="font-weight:bold;"}\
I give to the flowers remaining from the worship of the 6-faced guha distributed by the gurus.\
He then makes the child recite the पवमानि-s and takes him home. The further prescribed rites are performed (VSS3.22).


