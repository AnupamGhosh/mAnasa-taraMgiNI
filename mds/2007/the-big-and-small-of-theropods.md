
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The big and small of theropods](https://manasataramgini.wordpress.com/2007/06/19/the-big-and-small-of-theropods/){rel="bookmark"} {#the-big-and-small-of-theropods .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 19, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/19/the-big-and-small-of-theropods/ "5:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/Rn1Nv05tXAI/AAAAAAAAALM/rxvTZMzZ1Jo/s320/majungasaurus.jpg){width="75%"}
```{=latex}
\end{center}
```



The recent bonanza of theropod finds have brought to fore an old issue that we and others have been thinking about for a while: the evolutionary trends in body size in course of theropod evolution. Prior to the application of evolutionary classification, the dominant form of theropod classification was based on the non-empirical "super-carnivore" assertion of the idiotic Henry Osborn \[As an aside, Henry Osborn's son also of the same name was an environmentalist who made the claim that Indians had destroyed their own land and made it unsustainable for life. However, what he does is to cover up the major role of the white invaders during the British rAj in set this process rolling --- not surprising given the white supremacist world view of Osborb Sr.]. Osborn saw the large theropods as carnosaurs and the small ones as coelurosaurs, with each having an innate evolutionary drive culminating in the greatest of the large and the small forms. The great German dinosaurologist Friedrich von Huene had first suggested that this typological classification of theropods is probably incorrect. However, these advanced views did not take root in the English speaking world due to the dead weight of Osborn projected as a consensus by the famous American schools like Columbia and Princeton Universities. In our teens we had figured the error in this model by using the data from von Huene. Only much later, due to Thomas Holtz and others, did the real picture return to the fore in the scientific world.

It became clear that the theropod phylogeny involved many radiations, most of which we still incompletely understand, in part due to the incompleteness of the Triassic and early to middle Jurassic record. Yet there is increasing evidence that that there was probably no single trend in terms of size in course of theropod evolution. Large and small theropods repeatedly emerged in different lineages. The current phylogenetic consensus suggests that the earliest branch of theropods included the coelophysioids. This branch radiating in the Triassic and persisting till the Lower Jurassic ([Segisaurus]{style="font-style:italic;"}) included several small-bodied animals like [Segisaurus]{style="font-style:italic;"}, medium sized forms like [Coelophysis ]{style="font-style:italic;"}and moderately large forms like [Dilophosaurus]{style="font-style:italic;"}. The next great theropod radiation was the Ceratosauroid clade including [Ceratosaurus ]{style="font-style:italic;"}from the Jurassic and the Cretaceous abelisauroids. These included several large-bodied forms like [Ceratosaurus ]{style="font-style:italic;"}itself and among the abelisauroids the large abelisaurs from the Gondwanan continents like [Carnotaurus]{style="font-style:italic;"}, [Abelisaurus]{style="font-style:italic;"}, [Aucasaurus ]{style="font-style:italic;"}(all South America), [Majungasaurus ]{style="font-style:italic;"}(Madagascar), [Rugops ]{style="font-style:italic;"}(Africa), [Rajasaurus ]{style="font-style:italic;"}and [Indosaurus ]{style="font-style:italic;"}(India). Of these recent [Majungasaurus]{style="font-style:italic;"}, [Rajasaurus ]{style="font-style:italic;"}and [Indosaurus ]{style="font-style:italic;"}form an Indo-Madagascar clade to the exclusion of other forms, while Carnotaurus and Aucasaurus form a specialized south American clade with tiny arms generically and convergently reminiscent of [Tyrannosaurus]{style="font-style:italic;"}. Over the years several finds have suggested that the abelisauroids also included several small-bodied forms with unusual adaptations. Among them are [Noasaurus, Ligabueino, Jubbulpuria]{style="font-style:italic;"} (all very fragmentary) and [Masiakasaurus]{style="font-style:italic;"}, which along with the medium-sized [Elaphrosaurus ]{style="font-style:italic;"}represent small and medium sized ceratosauroids. The peculiar dentition of Masiakasaurus (probably for piscivory) and the long neck of [Elaphrosaurus ]{style="font-style:italic;"}(in a general sense like the ornithomimosaurs) hints the wide ecological range occupied by these small forms.

The remaining theropods comprise the tetanuran radiation. Most phylogenetic analysis have consistently recovered 3 major monophyletic tetanuran clades: two basal clades namely spinosauroids and allosauroids and a derived clade of coelurosaurs (including the only surviving dinosaurs, the birds). In addition there are several other basal forms tetanurans that do not appear to fall inside any of the above clades have been found : [Piatnitzkysaurus]{style="font-style:italic;"}, [Cryolophosaurus ]{style="font-style:italic;"}and [Torvosaurus]{style="font-style:italic;"}. The spinosauroid clade is characterized by their long snouts and superficially crocodile-like rostrum. The allosauroid clade includes within it the sinraptorids and the carcharodontosaurids which are amongst the largest of all theropods in addition to other extremely large predatory dinosaurs like [Allosaurus ]{style="font-style:italic;"}and [Acrocanthosaurus]{style="font-style:italic;"}. Until recently both the basal-most clades as well as the spinosauroids and allosauroids are all known to be large bodied forms leading to the idea that the tetanurans began big and then decreased in size only in the coelurosaur clade. However, the discovery of [Condorraptor ]{style="font-style:italic;"}showed that there were small-bodies tetanurans too.

In the coelurosaurs the trend has been exactly the reverse: With the exception of the relatively basal tyrannosaurs most clades were considered to be small-bodied -- especially the derived clade leading to the birds. In part this perception was due to popular ignorance of the large-bodied forms like [Therizinosaurus ]{style="font-style:italic;"}and [Deinocheirus]{style="font-style:italic;"}, which were poorly preserved. But a re-analysis of numerous recently described [bona fide]{style="font-style:italic;"} coelurosaurs shows that both large and small bodied forms occur in various clades:\
Composognathids: [Compsognathus ]{style="font-style:italic;"}(small); [Sinocalliopteryx ]{style="font-style:italic;"}(medium-large).\
Tyrannosaurs: [Dilong ]{style="font-style:italic;"}(small); [Eotyrannus, Guanlong]{style="font-style:italic;"} (medium) [Tyrannosaurus ]{style="font-style:italic;"}(large)\
Ornithomimosaurs: [Pelicanimimus ]{style="font-style:italic;"}(small); [Gallimimus ]{style="font-style:italic;"}(medium); [Deinocheirus ]{style="font-style:italic;"}(large)\
Alvarezsaurids: [Parvicursor ]{style="font-style:italic;"}(small); [Rapator ]{style="font-style:italic;"}(large)\
Oviraptorosaurs: [Ingenia ]{style="font-style:italic;"}(Small); "Triebold Caenagnathid" (Medium); [Gigantoraptor ]{style="font-style:italic;"}(large)\
Therizinosaurs:[Beipaosaurus ]{style="font-style:italic;"}(Small); [Therizinosaurus ]{style="font-style:italic;"}(Large)\
Deinonychosaurs : [Microraptor ]{style="font-style:italic;"}(small); [Unenlagia ]{style="font-style:italic;"}(medium); [Utahraptor?, Achillobator ]{style="font-style:italic;"}(large)\
Birds: [Passer]{style="font-style:italic;"}(small); [Phorusrhacos]{style="font-style:italic;"}(large)\
The interesting question that emerges from this observation is: Did each clade independently radiate out to occupy different ecological niches associated with different body size guilds rather than the whole clade specializing in particular niches (except birds which are today largely restricted to the air). OR Was it that body size did not play a major role in deciding the ecological niche occupied --- hence the same ecological niche could be occupied at different body sizes?


