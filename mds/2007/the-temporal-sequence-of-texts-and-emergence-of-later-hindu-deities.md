
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The temporal sequence of texts and emergence of later Hindu deities](https://manasataramgini.wordpress.com/2007/02/04/the-temporal-sequence-of-texts-and-emergence-of-later-hindu-deities/){rel="bookmark"} {#the-temporal-sequence-of-texts-and-emergence-of-later-hindu-deities .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 4, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/02/04/the-temporal-sequence-of-texts-and-emergence-of-later-hindu-deities/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The idea here is not trace the history of Hindu deities to their proto-Indo-European past, but only to investigate their relative times of emergence and the chronology of the texts referring to them. Further, all Hindu texts including the vedic saMhitA-s are layered texts- that is they have material from different chronological strata. Yet well defined coherent chronological cores can be identified in them.

  - In the vedic period one major transition, which might be termed the prajApati transition, is observed. The old vedic pantheon shared with the Iranians gained the first major addition in the form of the prajApati. There is no or hardly any mention of prajApati in any of the family मण्डल-s. He appears first in the मण्डल 10 in the हिरण्यगर्भ सूक्तं. In the yajur veda he is again not prominent in the old core of the soma and fire-kindling rites. However, he is prominent in the canonized late shrauta ritual laid out in the YV and the associated gAnaM-s composed in the sAman collection. Thus, prajApati rises tremendously prominence through the canonization of the shrauta ritual and becomes very prominent in the ब्राह्मण-s. In the ब्राह्मण period we get the first hints of him challenging indra's preeminent position.

  - In the very late ब्राह्मण period or late upaniShadic period we see hints of the rise of the two great gods of sectarian Hindu dharma -- rudra and विष्णु. This is observed in texts like the श्वेताश्वतर, मैत्रायणि ब्राह्मण उपनिषत्, atharvashiras and नारायण-valli.

  - In the earliest post-vedic texts we see two new deities rise to prominence: कुमार and brahmA. Of these the four-headed brahmA is clearly a homolog of the vedic prajApati and shares numerous attributes with him. While there are some allusions to brahmA in the vedic hymns themselves, it is not immediately clear if in every context they imply brahmA as in the deity or the power of mantras brahman. In the atharva vedic tradition we find a mention of brahmA as "भूतानाम् प्रथमं", in a late hymn used in शौनकीय काण्ड homa. This supports the beginnings of prajApati as brahmA were in the late vedic world. He appears in the इतिहास-s in his full-blown form. The late vedic prajApati assumes forms like the fish and turtle which were later associated विष्णु, but never associated with brahmA.

  - कुमार appears in the late vedic texts: 1) नेजमेष in the khila of the RV in connection to a rite performed in pumsavana. 2) skanda सनत्कुमार the teacher of नारद in the चान्दोग्य उपनिषत्. In both the earliest post-vedic texts रामायण and महाभारत is he is a prominent deity. The skanda-yaga of the atharva veda परिशिष्ठ appears to be coeval with the rise of skanda in the इतिहासस्.

  - Of the इतिहासस् the core of the रामायण appears clearly older (of course leaving out the uttara-काण्ड). The former is mentioned by the bhArata not vice versa. Further, the prominence of indra is much greater in the रामायण, with him probably standing slightly ahead of brahmA in prominence. In the महाभरत the decline of indra and the meteoric rise of विष्णु and shiva has set in full scale.

  - In both the इतिहास-s the trans-functional shakti as कालि or durga or the 8 mothers have a very limited presence or are non-existent. The first hints of this shakti in the classical form is seen in the हरिवंश as एकानंश who is also विन्ध्यवासिनि. The trans-functional shakti-s precursors may be seen in the AsurI दुर्गा of the atharvanic tradition (AsurI kalpa) and प्रत्यङ्गिरा.

  - The later layers of the महाभारत and the हरिवंश mark the beginnings of the rise of the proto-पाञ्चरात्रिc pantheon of विष्णु as कृष्ण, balabhadra and associated manifestations of विष्णु or अवतार-s. However, the full-blown दशावतार concept is not in place.

  - The core bhAgavata पुराण marks the emergence of रामचन्द्र as an अवतार of विष्णु.

  - The मार्कण्डेय पुराण marks the full-blow emergence of the trans-functional shakti and the 8 mothers (Though this might belong to the latest layer of the पुराण).

  - One of the biggest anchor points is the emergence of one of latest classical Hindu deities- the classical गणेश. The texts containing the classical विनायक and those which do not thus mark a major chronological divide in the Hindu world. The इतिहास-s do not mention the classical गणेश. विनायक-s of the 4-fold form emerge in the गृह्य sUtra-s and the AV-परिशिष्ठ, and allusion to these seizing विनायक-s are seen scattered in the महाभारत but they are distinct from the elephant-headed deity. The same seizing विनायक is mentioned in the मार्कण्डेय पुराण as a raudra-विघ्नराट्-perhaps marking the beginnings of his direct association with deva rudra. In the याज्ञवल्क्य स्मार्त prayoga-s the seizing विनायक-s are merged as one and is mentioned as being installed by rudra as a vighna-rAja. Here अम्बिका, the wife of rudra is also mentioned as his mother. This marks the first emergence of the prototype of the deity, though not mention of his elephant head is still made. But the classical गणेश is clearly missing in the core bhAgavata पुराण, मार्कण्डेय पुराण and हरिवंश. This suggests that the पुराणस् as they survive today belong to two age categories- the older group that does not mention विनायक and the newer group where he is mentioned or is a prominent deity. कालिदास does not mention विनायक and hence appears to belong in age closer to the earlier group. In this period skanda was very prominent in northern India especially in centers like Mathura. Thus, it is quite clear that कालिदास did not live in the gupta (or worse परमार court), but lived much earlier, prior to the common era. Thus, even the extant Hindu पुराण-s represent a much longer tradition than commonly believed. Early iconography of classical विनायक shows him in the company of the 8 mothers, and this is first mentioned in the gobhila स्मृति. This suggests that that स्मृति is in the least coeval with these early images of गणेश. The विनायक शान्ति of the बौधायन गृह्य sheSha sUtra appears to belong to the period after the emergence of the classical gaNapati. The मानव गृह्य sUtra, अर्थशास्त्र, याज्ञवाल्क्य स्मृति in contrast belong to an earlier period with with seizing विनायक graha-s.


