
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The path of two arrows and the path of one arrow](https://manasataramgini.wordpress.com/2007/07/01/the-path-of-two-arrows-and-the-path-of-one-arrow/){rel="bookmark"} {#the-path-of-two-arrows-and-the-path-of-one-arrow .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/01/the-path-of-two-arrows-and-the-path-of-one-arrow/ "11:15 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We felt the arms of the golden-handed asura. We felt the presence of that other wise asura all over. We walked along the path like a खड्ग in the grasslands of pragjyotisha. We joined with गुह्यसोमा, the golden kula girl of pretty locks and protean forms. We sat on that high seat quaffing soma. Sound had ceased to be. She of bewitching thighs, the mistress of yoga, said:\
["There are two paths that you see now. On one two arrows can travel, but on the other only one arrow travels. So much even those who are like अविद्वान्-s in the soma rite can see. But those who know the way of yoga can shoot two arrows along the second path. And verily stand at the midpoint from where the arrows are shot. This they say is the identity with the triple-striding plunderer of forts, and also the point of his 3rd step. This pain from the seizure by asura prachetas, and that pleasure from the coitus with the enticing women filled with aviveka do not grab the independently joyous chit. Now behold through yoga that 4th foot which pervades the parorajas -- the subtlety that underlies the light of सविता.]{style="font-style:italic;"}[]{style="font-style:italic;color:rgb(51, 204, 255);"}["]{style="font-style:italic;"}

OM भूर्-bhuvas-सुवः | tat-सवितुर्वरेण्यं | bhargo devasya धीमहि | dhiyo yo नः प्रचोदयात् | parorajase .असावदों || गृण्हि-sUrya Adityo na प्रभावात्यक्षरं | मधुक्षरन्ति tad रसं | सत्यं vai tad रसं Apo jyoti-raso .अमृतं brahma भूर्-bhuvas-सुवरों || yata indra भयामहि tato no अभ्यं क्ऱ्‌^धि | मघवञ् Chagdhi tava tanna Utaye विद्विषो विमृधो jahi ||

She of sweet smiles said: ["Ere long one sees the ऋत of mitra and वरुण. Those laws which are inviolable comprise the satya of mitra. Those laws which get repeatedly transgressed but are eternally enforced is the dharma of वरुण. These two types of laws satya and dharma govern the universe and are collectively the ऋत. That is the reason we things around us are the way they are."]{style="font-style:italic;"}


