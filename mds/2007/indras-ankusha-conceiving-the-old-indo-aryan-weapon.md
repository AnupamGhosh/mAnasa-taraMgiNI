
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [indra's अङ्कुश: conceiving the old Indo-Aryan weapon](https://manasataramgini.wordpress.com/2007/03/04/indras-ankusha-conceiving-the-old-indo-aryan-weapon/){rel="bookmark"} {#indras-अङकश-conceiving-the-old-indo-aryan-weapon .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 4, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/04/indras-ankusha-conceiving-the-old-indo-aryan-weapon/ "7:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The अङ्कुश is a common weapon of the gods. This in itself appears a bit strange. Among the gods it is most closely associated as the weapon of choice of the dreadful विनायक and कामेश्वरी. Most medieval icons of these deities show the अङ्कुश as the elephant goad and अङ्कुश is even translated as such. In classical Sanskrit and the vulgar prakrits that temporally overlap with the former it is again used in the context of the elephant goad. However, it appeared a bit strange that the अङ्कुश is hardly used as a weapon in the warfare of the मानवस्.

When we look at the ऋग् veda we find that it was a weapon of the gods even there, and was wielded by indra in battle:\
[इमं बिभर्मि सुकृतं ते अङ्कुशं येनारुजासि मघवञ्-छफारुजः ।]{style="color:#33ff33;font-style:italic;"}\
[अस्मिन् सु ते सवने अस्त्वोक्यं सुत इष्टौ मघवन् बोध्याभगः ॥]{style="color:#33ff33;font-style:italic;"} (RV10.44.9)\
I bear this well-made अङ्कुश of yours, with which you, maghavan, tears the \[attackers] dismembers them under the horse's hoofs. At this soma-pressing you be pleased and drink the juice and accept the sacrificial share, maghavan.

The implication is reasonable here- it was used as a weapon in chariot or horse-borne combat against the attacker.

Other vedic references add more to the picture:\
[आ नस्तुजं रयिं भरांशं न प्रतिजानते ।]{style="color:#33ff33;font-style:italic;"}\
[वृक्षं पक्वं फलम् अङ्कीव धूनुहीन्द्र सम्पारणं वसु ॥]{style="color:#33ff33;font-style:italic;"}(RV 3.45.4)\
In this mantra विश्वामित्र asks indra to bear him wealth with his अङ्कुश even as one would pull down ripe fruit from the tree.

[दीर्घस्ते अस्त्व् अङ्कुशो येना वसु प्रयछसि । यजमानाय सुन्वते ॥ ]{style="color:#33ff33;"}(RV 8.17.10)\
A similar metaphor is used by इरिंबिठि काण्व in this mantra where indra is asked to pull in wealth with his long अङ्कुश (दीघस्ते astv अङ्कुशो). Similar to the idea of pulling in wealth is the concept of pulling a woman towards you and indra's अङ्कुश is invoked for that purpose in the स्त्री-वशीकरण mahA-mantra in the atharvanic rite for obtaining a wife (this mantra is also used in the marriage ritual of the atharvavedins):\
[yas te .अङ्कुशो वसुदानो बृहन्न् indra हिरण्ययः |]{style="font-style:italic;color:#33ff33;"}\
[तेना जनीयते जायां मह्यं धेहि शचीपते ॥]{style="font-style:italic;color:#33ff33;"} (AV-S 6.82.3/ AV-P 19.17.6)\
That wealth-giving, great golden अङ्कुश of yours O indra, with that O lord of शची give me who seeks a woman, a wife.

In a comparable context, there are two closely related mantras from the पैप्पलाद atharva veda describing the use of extracts of the pATa herb ([Stephania hernandifolia]{style="font-style:italic;"} or a related plant) by a woman seeking to break up the connection between her lover and a rival female (AV-P 7.12.9 and 20.42.11).\
[pATA bibharty अङ्कुशं हिरण्यवन्तम् अङ्किनम् |]{style="font-style:italic;color:#33ff33;"}\
[तेन सप्त्न्या वर्च आ लुम्पामि ममेद् असत् ॥]{style="font-style:italic;color:#33ff33;"} (AV-P 7.12.9)\
[pATA bibharty अङ्कुशं हिरण्यवन्तम् अङ्किनम् |]{style="font-style:italic;color:#33ff33;"}\
[तेनाहम् अन्येषां स्त्रिय आ लुम्पामि ममेद् असून् ॥]{style="font-style:italic;color:#33ff33;"} (AV-P 20.42.11)\
Both the mantra-s use the metaphor of the अङ्कुश to describe how the pATa herb pulls away the rival woman (or her fertility or sexual fervor). Here, again the अङ्कुष is used in the context of hooking and pulling away a human entity. Interestingly, some modern studies have shown the menispermacean plants like[ S.hernandifolia]{style="font-style:italic;"} to cause depression of testosterone levels in male rodents. It is important to note the use of the verb 'lumpati' along with the noun अङ्कुश, which means to pull or tear away. It is the same verb which is used again in the context of the अङ्कुश in an atharvanic surgical procedure (below).

From these earliest allusions to the अङ्कुश we notice the following: 1)The अङ्कुश is a weapon which was actually used and most probably by horse-/chariot- borne warriors. 2) It is never mentioned in the context of an elephant, unlike in classical sanskrit and the depictions in medieval Hindu iconography. Instead, multiple references to it allude to pulling away/down human entities, even if metaphorically. 3)Unlike the short elephant goad, the weapon is clearly described as being long and used in the context of pulling down fruits from trees. 4) However, at this point it must be mentioned that in the पैप्पलाद atharvaveda the अङ्कुश is also used describe a surgical instrument devised by the ashvins. In AV-P 2.81.2 it is mentioned that a metal अङ्कुश is used to incise and pull out the cataract that causes blindness (the earliest account of the cataract surgery).

Thus, the अङ्कुश, in large part, was originally not an elephant goad but a pole-arm similar to the fouchard or the Mongol hook-spear. Especially the latter was an effective pole-arm --- deployed by the Mongols on horseback to pull down the enemy and then finish them off, fitting well with the description of the अङ्कुश of indra in RV 10.44.9. However, in the medical literature of the atharvans the अङ्कुश was used as a technical term to describe a surgical instrument for cataract removal- a technique which continued into the later day Ayurveda. The अङ्कुश in traditional iconography is often mentioned with another weapon the pAsha. Interestingly, this was also used in a similar context by the Mongol warriors in lassoing enemies and pulling them down from horse back. The description of the construction and use of the pAsha in the surviving fragments of Hindu dhanurveda is very similar its reported use by Mongol warriors. The pAsha, the dhanurveda, states is used in combination with a club that is used to finish of the enemy after pulling him down with the pAsha. Thus, it appears that the अङ्कुश and the pAsha were originally used by the early Aryans in a manner similar to other steppe warriors --- in pulling down riders or chariot warriors by other horse/chariot riding users. This use appears to have eventually gone out of vogue in India and the object with a similar shape which was in common use, the elephant goad, took over its name.

However, it should be noted that the symbolism associated with it-- as an instrument by which the gods draw wealth or a woman for the यजमान --- is an old one, which persisted in context of the new Hindu deities. It is not without reason that the wealth-giving विनायक and the fulfiller of all काम-s, including शृङ्गार bhoga, कामेश्वरी wields the अङ्कुश. This another remarkable case of functional persistence of symbolic motifs in the Hindu world from the earliest times, despite changing meanings and deities.


