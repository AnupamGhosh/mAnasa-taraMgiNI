
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The code of the अष्टाक्षरी विद्या](https://manasataramgini.wordpress.com/2007/05/30/the-code-of-the-ashtakshari-vidya/){rel="bookmark"} {#the-code-of-the-अषटकषर-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 30, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/30/the-code-of-the-ashtakshari-vidya/ "12:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is well-known that the atharva vedic tradition provides the vidhi for one of the great rites of कुमार- the skanda याग promulgated by gopatha. But known only to a select few, who maintain the continuity with the divyaugha via the मानवौघ on the कौमार path the AV provides that most supreme rahasya of skanda -- the mahA-mantra with which the manifestation of deva attains completeness. This great mantra is the अष्टाक्षरी विद्या of the atharvaveda. It is to deployed only by a practitioner of the अथर्वण-shruti, who has been introduced to the rahasya-s of the भृगु veda. One who has not sworn his allegiance to the veda of the भृगु-अङ्गिरस् should undergo a new upanayana and study the AV. He then performs oblations to the atharva veda with the two सूक्तंस् (AV-vulgate edition 19.22 and 19.23) as done by the atharva वेदी-s on the उपाकर्म day, before learning this mantra (AV 20-132.16). This mantra belongs to the khila section found in काण्ड 20 of the vulgate text known as the कुन्ताप hymns that are deployed in the shrauta context during the महाव्रत rituals. The कुन्ताप-s are all enigmatic hymns that at the face of it make no sense. They are attributed to the भार्गव ऐतष (the ऐतष प्रलाप), a descendant of aurva. The mantra's ऋषि is भृगु, the Chandas is eka-pada gayatrI and its देवता is skanda.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RlojOWWV1CI/AAAAAAAAAJU/3OQJcUttNwM/s320/av3.JPG){width="75%"}
```{=latex}
\end{center}
```



He first performs japa of the mantra a 108 times in a silent voice, but noting all the svara-s as ordained in atharva vedic recitation. Then he shall light the fire on a स्थण्डिल as mentioned by gopatha in the धूर्त-kalpa. Then he shall make oblations of ghee or white/wild sesame seeds with the अष्टाक्षरी विद्या. During this he may combine it with the atharvanic brahmashiras with the special atharvanic व्याहृतिस्: bhur-bhuvas-suvo-janad-वृधत्-karat-ruhan-mahat-tac-Cham-OM. In the end he utters the formulae: "mayi भर्गः; mayi महः; mayi यशः mayi सर्वं". Then he shall perform उपस्थानं again as in the धूर्त kalpa after reciting the formula "सद्योजातं प्रपद्यामि..."

The key secret of the अष्टाक्षरी lies in the सम्पुटीकरण with the two षडक्षरी-s that result in the emanation of the two basic मण्डल-s of कार्त्तिकेय: the vajra-मण्डल and the ghana-मण्डल. When the अष्टाक्षरी is combined with the षडक्षरी (नमः कुमाराय) then it results in the expansion of the planar षट्कोण yantra defined by the षडक्षरी' syllables occupying the vertices of the yantra into the 3D octahedral vajra-मण्डल. Now the 6 syllables of the षडक्षरी occupy the 6 vertices of the octahedron, while the 8 syllables of the अष्टाक्षरी occupy the 8 faces of the मण्डल.

[vajra-मण्डल]{style="font-weight:bold;font-style:italic;color:#33ff33;"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/Rlokc2WV1DI/AAAAAAAAAJc/3uv4WY_zPig/s320/av1.JPG){width="75%"}
```{=latex}
\end{center}
```



When the अष्टाक्षरी is combined with a षडक्षरी (vachadbhuve नमः) then it results in the expansion of the planar षट्कोण yantra defined by the षडक्षरी's syllables occupying the 6 equilateral compartments of the yantra into the 3D cubic ghana-मण्डल. Now the 6 syllables of the षडक्षरी occupy the 6 faces of the cube, while the 8 syllables of the अष्टाक्षरी occupy the 8 vertices of the मण्डल.

[घन-मण्डल]{style="color:#33ff33;font-weight:bold;font-style:italic;"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RlokdGWV1EI/AAAAAAAAAJk/JYXlp3uTbWs/s320/av2.JPG){width="75%"}
```{=latex}
\end{center}
```



A प्रणव is located in the center of each of the two polyhedra. The मण्डल-s must be made solid ideally using a noble metal; other substances might also be used, but require repeated reconsecration. They can be made as separate polyhedra but ideally should be combined as an octahedron mounted on a cube.

This symmetry in the संपुटीकरण of the अष्टाक्षरी and the two षडक्षरी-s illustrates a geometric relationship of the two polyhedra known as the [dual]{style="font-style:italic;"}. The dual of a polyhedron A is defined as polyhedron B, which constructed by taking the centers of the faces of the A as the vertices of B. The cube (ghana मण्डल) and the octahedron (vajra-मण्डल) are duals of each other.


