
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [With the other पक्ष](https://manasataramgini.wordpress.com/2007/06/18/with-the-other-paksha/){rel="bookmark"} {#with-the-other-पकष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 18, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/18/with-the-other-paksha/ "4:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The third hero advanced with a large force to defend his turf. His men watched with mixed feelings-- some where excited, others were confused, yet others were sad because their own people were dying. We were roaming like a खड्ग in the grasslands of yore (we were reminded of the statement made by R1's father that in our proverbial last janma we were a khadga), when our clansman called on us to bring the march of the third hero to our attention. We also heard from him that he was marching with his own detachment having scored a direct hit on the भ्रातृव्य and asked us to join his march. We however pointed out the mistakes in his mantra prayoga and warned him of its consequence. We expressed our inability to do so due to lack of troops at this point as we were mired in the खाण्डवन् war that sat on us like the वेताल on vikrama in the महाश्मशान. We then continued our wanderings --- we saw the descendant of Fourier of मण्गल-grAma lying prone on the battle field struck by a shara of the third vIra. That person called to us --- and we stopped to talk in that field. That field reminded me of the days when the third vIra and I were in a श्मशान surrounded by skeletal remains from long gone pretas.

The fallen descendant of Fourier spoke of the battle in which they were arrayed against the 3rd vIra to settle the scores ensuing from the hostilities that occurred after he came face to face with the wall of the crane. The third vIra's horse and ratha had been smashed by the Fouriers and their chera magician allies. By the Fourier's he had been bound in a cave and atop a hill where the muni had saved him. But he was a surly warrior who had fought many more battles than any of us. He received from his large network of spies the intelligence that he will be aided by the सुपर्ण at the opportune moment, just as the शाक्त्य in the quest for his girl. In a diplomatic manipulation he aligned the virile तैत्तिरीयक and his party against the Fouriers. He attacked the Fouriers as they forded a pass in the madhya-yuga. They deployed the action of brahma and क्षत्र and a fierce battle followed. But the 3rd hero over came them because the सुपर्ण came to his aid, like gAyatrI bearing the soma for the gods. The Fouriers were routed and the descendant of Fourier of मण्गल-grama lay pierced. That person said: "With that the third hero has settled the past issues. With our destruction the chapter of the war initiated by Fourier of mangala-grAma has be closed. The third hero and the virile तैत्तिरीयक are the victors and with that the कृत्या powers of Fourier have all dried up like a pond in summer. If we remain in this loka we will take sannyasa from all this, if we join the पितृस् we will be remembered as a brave warrior who died on the field."

We continuing to wander like a व्रात्य; we felt like Zarathushtra reciting the Gayam Urvan, or more aptly like atharvan approaching father वरुण inquiring about the 4th foot that lies in the परोरजसः. We looked to the unblinking asura who gazes with his numerous eyes to be released from the pAsha-s. The mantra "yA वां इन्द्रावरुण ... came to mind"


