
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Hexagonal nesting](https://manasataramgini.wordpress.com/2007/11/22/hexagonal-nesting/){rel="bookmark"} {#hexagonal-nesting .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 22, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/22/hexagonal-nesting/ "2:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/R0TsD1IKzJI/AAAAAAAAAQs/KwUeT4gb7XY/s320/flower_pat.png){width="75%"}
```{=latex}
\end{center}
```

\
click image to enlarge

