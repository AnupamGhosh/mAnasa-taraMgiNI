
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [shri bhadreshvara and the kings of चम्पावती](https://manasataramgini.wordpress.com/2007/05/13/shri-bhadreshvara-and-the-kings-of-champavati/){rel="bookmark"} {#shri-bhadreshvara-and-the-kings-of-चमपवत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 13, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/13/shri-bhadreshvara-and-the-kings-of-champavati/ "5:06 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RkdZhI9MdRI/AAAAAAAAAIs/JPg5O8CNiXE/s320/bhadra1.jpg){width="75%"}
```{=latex}
\end{center}
```



The two लिङ्ग-s in the ruins of the गर्भा-गृह of the bhadreshvara complex

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RkdPEY9MdPI/AAAAAAAAAIc/eTXiJxYsE2Q/s320/bhadra2.jpg){width="75%"}
```{=latex}
\end{center}
```



The nandin and the bali पीथ-s a another site on the bhadreshvara complex

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RkdPEY9MdQI/AAAAAAAAAIk/HZm25h4uFFc/s320/bhadra3.jpg){width="75%"}
```{=latex}
\end{center}
```



The vidyeshvara-s on the massive walls of the bhadreshvara complex

We know little of the early history of the Hindu rulers of चम्पावती (modern Vietnam) of the dynasty of श्री-मारवर्मन्. But it is important to understand this to gain insight into the history of the mantra-मार्ग and पाशुपत shaiva matas. मारवर्मन् appears to have reigned in the 2nd century CE as suggested by his Sanskrit inscription. There is evidence that he led a naval attack on the chIna-s to seize the chIna-held province of Tonkin. His much later successor had a border dispute with the chIna-s over the Hoan Sonh mountains. When the negotiations with the chIna-s broke down the चम्पापति invaded Nhut Nam and conquered it from the chIna-s. He was however wounded in this battle and died shortly there after. During his son's reign the chIna-s of the Han organized a massive invasionary force by land and sea with several new weapons like fire spears and the like and attacked चम्पावती. The Hindu ruler faced heavy losses and was forced to retreat from the fertile Nhut Nam province. His son was the great rAja श्री bhadravarman. He retaliated on large scale and inflicted a crushing defeat on the chIna-s. He forced them give up Nhut Nam and continued the invasion into the chIna-held lands to conquer Than Hoa from them.

bhadravarman reigned from 380CE to 413 CE and Sanskrit inscriptions from his reign show that he had learned the recitation of the vedic saMhitA-s and performed vedic rituals. He calls himself a devotee of rudra, and he celebrated the grace of rudra in securing his victory over the chIna-s by building the glorious temple for shiva termed श्री bhadreshvara. Now we know that somewhat later in the far east the main lineage of the mantra-मार्ग shaiva mata was that of the [वाम-srotas of the Cambodian empire](http://manollasa.blogspot.com/2007/02/greatest-temple-of-shiva.html) with its depiction of tumburu. But the question arises as to what were the affinities of the earlier shaiva-mata at चम्पावती. An analysis of the ruins of the bhadreshvara temple at ंय्Sओन्, Vietnam suggest that siddhanta tantras of the Urdhva-srotas along with elements of the पाशुपत shaiva were in operation. The key elements of the Urdhvasrotas or the pre-pentafurcation mantra-मार्ग include the worship of the vidyeshvaras and the लिङ्ग with a square yoni. Possible performance of the जयाभिशेक as described in the कूर्म पुराण is an indication of the पाशुपत affinities, which was also coevally common in the gupta empire in India. One of the images of an 8-armed shiva in नृत्य are also highly reminiscent of similar depictions in the पाशुपत temples in India. Thus, it is clear that multiple waves of mantra-मार्ग (+पाशुपत) shaiva practice moved from India to the Indic civilizations of the East. In this sense the developments there paralleled those on the mainland where different dynasties were associated specifically with different branches of mantra-मार्ग or पाशुपतस्.

\[It should be kept in mind that the original bhadreshvara complex was renovated repeatedly by the later rulers of चम्पावती like rudravarman and shambhuvarman. But the complex today is in poor condition making it difficult to separate the different layers. Further, the Americans with their unsurprising Abrahamistic mindset destroyed a significant portion of the temples in this complex during their invasion of modern Vietnam]


