
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The silent return](https://manasataramgini.wordpress.com/2007/01/20/the-silent-return/){rel="bookmark"} {#the-silent-return .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 20, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/20/the-silent-return/ "7:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RbHLud_ytqI/AAAAAAAAABM/jCa_Fk2oo5Y/s320/teichmuller_crown.png){width="75%"}
```{=latex}
\end{center}
```



On the path of nothingness we returned;\
before us stood the bright prajApati,\
with rudra's bright dart pinning him to the welkin.

In one hand we held an iron trident,\
in the other we held a skull.\
Beyond the carcass of prajapati\
shone his wain, with the ruddy gem in it struck.

Further ahead shone the six mothers,\
of our patron god with the spear.\
Around us stood the city's denizens.

We were reminded of the bygone days.\
that strangely to us seemed so near.\
The prodigious one did aim,\
a kite in the firmament to place.

The muni batted an eyelid not,\
and like the wise vainaemoinen\
pronounced in the inner mind:\
"this contraption is ain't flying".

This wisdom struck us not then.\
We unheedingly declared then\
that we seek not to get\
worthless basalt home\
in place of metal that we sought.

Chasing phantasms and black shadows,\
we wandered on the plains of खाण्डव,\
thrilled fleetingly by the merriment\
of our blithe fellow-farers,\
verily like the Lankan prince\
in the pleasure house in bhArata.

Chasing that dark shade,\
down the dark alley we coursed.\
At the bend in the path\
concealing the grim unknown,\
the shade was unmasked!

Out came the राक्षसि,\
indeed laughing hideously\
even as jarA in a magadhan midden.

Our reverie broke.\
The wayfarers of uncontrolled gaiety\
receded into the chaos,\
even as the सूतपुत्र and suyodhana\
were seen vanishing\
into the gape of विश्वरूप.

Alone on the path of sorrow,\
we stood staring--\
the slow changing heavens\
in the daily circuits.\
The bleak shriek of वात and\
the राक्षसि beyond.


