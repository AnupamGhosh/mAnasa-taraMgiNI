
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The crashing doors](https://manasataramgini.wordpress.com/2007/01/15/the-crashing-doors/){rel="bookmark"} {#the-crashing-doors .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 15, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/15/the-crashing-doors/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note should be considered along with our earlier notes on the topic:\
[The crossing of अश्मन्वती](https://manasataramgini.wordpress.com/2006/11/26/the-crossing-of-ashmanvati/)\
[The path of fire](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/)\
von Dechend and De Santillana, while flippantly referring to "the Hindu verbiage," repeatedly find themselves needing to take recourse to the same "Hindu verbiage" to adduce evidence for their conjectures. But they must be credited for noting the significance of mythic motifs that were previously overlooked by most. One point they just allude to but fail to expand upon is the issue of crashing doors. What exactly are these crashing doors in the language of myth? Based on some myths, the above authors conclude that it must have some connection to the frame of time but fail to clarify this explicitly, especially in connection to their central theme --- the mythic accounts of the precession of the axis. One striking tale they narrate contains this motif, with some other motifs found in connection with precession myths in the Hindu world. This is apparently a tale from the Catlo'Itq tribes of British Columbia recorded by a Jewish ethnographer Franz Boas towards the end of the 1800s:

[A man had a daughter who possessed a [wonderful bow and arrow]{style="font-weight:bold;"}, with which she was able to bring down everything she wanted. But she was lazy and was constantly sleeping. At this, her father was angry and said: "Do not be always sleeping, but take thy bow and shoot at the navel of the ocean, so that we may get fire." [The navel of the ocean was a vast whirlpool in which sticks for making fire by friction were drifting about]{style="font-weight:bold;"}. At that time, men were still without fire. Now the maiden seized her bow, shot into the navel of the ocean, and the material for fire-rubbing sprang ashore. Then the old man was glad. He kindled a large fire; and as he wanted to keep it to himself, [he built a house with a door that snapped up and down like jaws and killed everybody that wanted to get in.]{style="font-weight:bold;"} But the people knew that he was in possession of the fire, and the stag determined to steal it for them. He took resinous wood, split it, and stuck the splinters in his hair. Then he lashed two boats together, covered them with planks, danced and sang on them, and so he came to the old man's house. He sang: "O, I go and will fetch the fire." The old man's daughter heard him singing and said to her father: "O, let the stranger come into the house; he sings and dances so beautifully." The stag landed and drew near the door, singing and dancing, and at the same time sprang to the door and made as if he wanted to enter the house. Then the door snapped to, without however touching him. But while it was again opening, he sprang quickly into the house. Here he seated himself at the fire, as if he wanted to dry himself, and continued singing. At the same time, he let his head bend forward over the fire, so that he became quite sooty, and at last, the splinters in his hair took fire. Then he sprang out, ran off, and brought the fire to the people.]{style="font-style:italic;color:#33cc00;"}

I was reminded of this most remarkable tale due to the striking statement made by the जैमिनीय ब्राह्मण in connection with the deployment of the [बृहत् and rathantara](https://manasataramgini.wordpress.com/2007/01/10/the-deployment-of-brihat-and-rathantara/): ["बृहद्-रथन्तरयोर् एवैनं मुखे ऽपिदधाति । यं द्वेष्टि तं पुरा संवत्सरात् प्राणो जहाति ।"]{style="color:#0000ff;"}\
It almost looked as though the samans in whose mouth the enemy is placed are like the clashing doors in the above myth and kill him by the completion of the year. The above tale is rich in motifs: the female archer reappears as the archer goddess Satit in Egypt shooting the Sothis cow, as the archer goddess Artemis in Greece shooting Orion at the instigation of Apollo (whose ortholog rudra shoots the same arrow in the Hindu world). There is the whirlpool with the fire sticks. There is the stag, the stealer of fire for men (c.f. the deer that ran away with युधिष्ठिर's fire-kindling sticks in the महाभारत), like भृगु, brought fire for the Aryas. But what about the doors? Divine doors are mentioned in the great rite every true Arya does-- the daivi द्वारा of आप्री rite. But these dangerous crashing doors do appear in a hidden way in the veda, finally providing the clue for what they are:\
The तैत्तिरीय ब्राह्मण 1.2.3.1/2 provide the ब्रह्मवाद for the [दिवाकीर्त्य](https://manasataramgini.wordpress.com/2007/01/13/the-divakirtya-samans/) chant, which is central to the sacrifice performed on the विषुवान् day-

[विषूवान् दिवाकीर्त्यम् । यथा शालायै पक्षसी । एवग़्ं संवत्सरस्य पक्षसी । यद् एते न गृहेरन् । विषूcई संवत्सरस्य पक्षसी व्यवस्रग़्ंसेयाताम् । आर्तिम् आर्छेयुः । यद् एते गृह्यन्ते । यथा शालायै पक्षसी मध्यमं वग़्ंशम् अभि समायच्छति ॥ एवग़्ं संवत्सरस्य पक्षसी दिवाकीर्त्यम् अभि संतन्वन्ति । नार्तिम् आर्छन्ति ।]{style="color:#0000ff;"}

Here the विषुवान् day or the summer solstice day is identified with the दिवाकीर्त्य song. The same identification is reiterated in the aitareya ब्राह्मण 4.18 and पञ्चविंश ब्राह्मण 4.6. In AB 4.18, the विषुवान् day is termed the एकविंश as it comes on the middle day of the middle month flanked by 10 days on each side (10+1+10) in the एकविंश rite of the old Aryan sacrificial calendar. This is also more cryptically described in the तैत्तिरीय saMhitA 7.3.10, where the singing of the दिवाकीर्त्य is central, surrounded by other gAnaM-s which are symmetrically placed about it. But the TB prose given above goes further to clarify --- "Just as the door of the sacrificial hall has two halves even so the year has two halves (about the दिवाकीर्त्य or विषुवान्). If he does not grasp this, the doors of the year crash asunder, and the year falls apart. He is then harmed. If grasps this, even as the doors of the sacrificial hall hold it up in the middle, so the दिवाकीर्त्य holds the doors of the year together. He is then not harmed." The brahma वाद given in aitareya ब्राह्मण 4.18 further clarifies that the विषुवान् was indeed associated with the prop by which the deva-s uphold the sun's frame in the heavens. The दिवाकीर्त्य sung by the उद्गातर् priest symbolizes this. The PB 4.6 again mentions the year to be made of two parts, with agni being born at the विषुवान् day and cryptically states that the दिवाकीर्त्य represents the frame that props the sun (PB4.6.13; a comparable statement comes in काठक saMhita 33.6.31.21). Thus, the crashing doors are the halves of the year held by the frame of solstices and equinoxes.

The veda also furnishes direct accounts of the deadly jaw-like nature of the halves of the years (about which we started introspecting on account of the बृहत् and the rathantara), providing a complete comparison for the deadly jaw-like crashing doors in the Native American tale. In the atharva veda-shaunaka, we have the mantra (11.6.22= AV-P 15.14.10):\
[या देवीः पञ्च प्रदिशो ये देवा द्वादश र्तवह्।]{style="color:#0000ff;"}\
[संवत्सरस्य ये दंष्ट्रास् ते नः सन्तु सदा शिवाः ॥]{style="color:#0000ff;"}\
Here the jaws or the fangs of the year are invoked to be benign in the second hemistich of the mantra. The same concept emerges again in the spell laid to counter the maker of कृत्या-s as per the atharvanic tradition. Here the foe crunched by the jaws or the fangs of the year (AV-P 1.63.2):\
[व्यात्ते परमेष्ठिनो ब्रह्मणापीपदाम तं ।]{style="color:#0000ff;"}\
[संवत्सरस्य दंष्ट्राभ्यां हेतिस् तं सम् अधाद् अभि ॥]{style="color:#0000ff;"}\
The missile crunches him like the jaws/fangs of the year close upon him.

The पञ्चविंश ब्राह्मण also brings up the issue of the danger posed by the jaws of the year closing on the यजमान.\
["संवत्सरस्य वा एतौ दंष्ट्रौ यद् अतिरात्रौ तयोर् न स्वप्तव्यं संवत्सरस्य दंष्टयोर् आत्मानं नेद् अपिदधानीति । तद् आहुः कोऽस्वप्तुम् अर्हति यद् वाव प्राणो जागार तद् एव जागरितम् इति"]{style="color:#0000ff;"}\
If the यजमान sleeps on the days of the अतिरात्रस् (which in the yearly sattra would mark the solstice nights), he gets crunched by the jaws/fangs of the year. Hence he must remain awake on these nights

Thus, if Franz Boas' account of the Native American myth is to be taken as reality, then indeed, the deadly crashing doors are an ancient mythic motif representing the year and might be used in the precession myths. Here it is worth noting that other great precessional myth: Where was हिरण्यकशिपु slain by विष्णु? At the door!

A corollary would be that these mythic motifs had come into being at least by 12000 years BP, when the Native Americans are believed to have separated from the Eurasians. If so, the long records of precession 'literally" yuga after yuga," might not be surprising.


