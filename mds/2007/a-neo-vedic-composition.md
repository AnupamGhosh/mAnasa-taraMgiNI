
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A neo-vedic composition?](https://manasataramgini.wordpress.com/2007/06/02/a-neo-vedic-composition/){rel="bookmark"} {#a-neo-vedic-composition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 2, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/02/a-neo-vedic-composition/ "5:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R was giving me ride when we observed some sight that greatly reminded us of a shared juvenile experience, that filled us with much fuzzy nostalgia. At that moment R asked me if anybody could compose vedic mantra-s again or सूक्त-s. I asked what the criteria where and we seemed to agree- they should resemble the original सूक्त-s or yajushes and should follow vedic grammar and should have correct vedic svaras. They should be in genuine Chandas, adhere to the vedic lexical statistics and deities. Evidently, even though later उपनिषद्-s have been couched in vedic language over the ages they clear differ from the original. Nor do the पुराण-s even superficially attempt revival of the vedic form of expression. Even though the vedic composers themselves saw their works as काव्य, the later day classical काव्य is clearly distinguished from its vedic precursors. So much so that the later day commentators and stylists explicitly exclude the vedic काव्य from the classical काव्य. Yet at some point vedic style composition was prevalent and at least had one representative outside the Indian zone in the form of the avesta of the Iranians. There is पाणिनि, whose grammar describes the vedic language, and by including the unpublished sUtra-s of the आर्ष पाठ one could theoretically arrive at vedic composition. Yet we see hard any such attempts to imitate to shruti.

But the muni informed me that there was one possible example. These are the details I could gather:

  - In 1917 a ब्राह्मण named गजानन sharma aka दैवरात from गोकर्ण, a student of the great संस्कृत savant काव्यकण्ठ gaNapati shAstrI, composed a set of 50 सूक्त-s encompassing 448 ऋक्-s.

  - He said to have spontaneously composed this while performing tapasya.

  - The majority are in the jagati Chandas, followed by त्रिष्तुभ्, then अनुष्टुभ् and finally a small set of gAyatrI-s.

  - The primary देवता-s of his composition are agni, indra, सरस्वती, सविता, बृहस्पति, आपः, वायु, rudra, aditi and soma. He also has some compositions on puruSha and other resembling the philosophical works like the नासदीय सूक्तं.

  - The ऋक्स् are marked with svara-s and the composer also created a pada पाठ as seen in the traditional saMhitA-s

Unfortunately, copy of this was in the Telugu script and the gentleman with a copy of this was my only source of obtaining the information. I could not analyze it myself to determine if it met all other lexical and grammatical criteria of a vedic composition. Nevertheless, from what I could see it did seem a work worth considering as a candidate for the neo-vedic genre. If only somebody printed a copy of it in the nAgarI script...

gaNapati shAstrI was a great paNDita himself with a profound understanding of the veda and he composed more modern veda inspired compositions in संस्कृत like the indra सहस्रनामम् to indra, इन्द्राणी saptashati, गीतामाला to various vedic deities, and indra गीतमु in telugu imitating a vedic praise of indra. It is clear that he was one of those rare individuals who had a genuine grasp of the vedic thought and action. Some people have the action to perform the shrauta ritual and others the inspiration of the vedic mind. A true knower of the veda understands both.


