
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [bIja संपुटिकरणस् of the कुमार विद्या and related mantras](https://manasataramgini.wordpress.com/2007/08/15/bija-samputikaranas-of-the-kumara-vidya-and-related-mantras/){rel="bookmark"} {#bija-सपटकरणस-of-the-कमर-वदय-and-related-mantras .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 15, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/15/bija-samputikaranas-of-the-kumara-vidya-and-related-mantras/ "4:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great कौमार तान्त्रिक, [रामानुज deshika](http://manollasa.blogspot.com/2007/01/shri-ramnuja-deshika-one-of-last-great.html) collected several कौमार विद्या-s into two compendiums the सुब्रह्मण्य kalpa (SK) and the कुमार-tantra sAra (KTS). These contains fragments of the older कौमार मूल tantras that might be partly or entirely lost. These old मूल tantra-s include: 1) गौहास्फन्दा 2) अनलकान्थ 3) विषच्छदा 4) shaishava 5) मयूरशिखा 6) ramanodbhava 7) वरदा 8) लीलोत्पतमालिका. The main information in this regard comes from 2 relatively good paper and pen transcripts prepared from the palm leaf originals of the SK and KTS in my possession.

One of the important elements from the great मयूरशिखा are the bIja सम्पुटिकरण-s for the कुमार विद्या-s. The मन्त्रोद्धार-s are given for the following forms:\
[अष्टवर्ग द्वितीया यत् स्कन्द बीजम् इदं परं ।\
षष्ठानल समोपेतं बिन्दुनाद समायुतं ।\
मूल-मन्त्रम् इदम् ज्ञेयं सर्व कामार्थ सिद्धिदं॥\
[ऒं सूं स्कन्दाय नमः ।-- इति मूल मन्त्रः]]{style="color:#99cc00;"}

[षट् वर्गस्य द्वितीयम् च षष्ठ-स्वर समन्वितं ।\
चतुर्दश स्वरन्तस्य बिन्दु-नाद समायुतं ।\
सुब्रह्मण्य छतुर्थम् नमो ।अन्तं प्रणवादिकं ।\
[ऒं सूं सौं सुब्रह्मण्याय नमः ।]]{style="color:#99cc00;"}

[द्वितीय वर्ग प्रथमम् चैव षष्ठानल समायुतं ।\
कवर्ग प्रथमं चैव पञ्चम-स्वर समन्वितं ।\
कुमार मन्त्रम् इदम् स्यात् बिन्दुनाद समन्वितं ॥\
[ऒं कूं कं कुमाराय नमः ।]]{style="color:#99cc00;"}

[अष्टवर्ग द्वितीयन्तु पञ्चम स्वर संयुतं ।\
स्वामिन् गुहाय मन्त्रान्ते नमो ।अन्तं प्रणवादिकं ।\
बिन्दु-नाद समायुक्तं सर्व कामार्थ सिद्धिदं ॥\
[ऒं सुं स्वामिन् गुहाय नमः ।]]{style="color:#99cc00;"}

[सप्तवर्ग पञ्चान्तं चतुर्थ स्वर-संयुतं ।\
शरवनभवं बिन्दु-नाद समायुतं ।\
स्वनामाद्यक्षरं चैव नमोन्तं प्रणवादिकं ॥\
[ॐ शीं शं शरवन-भवाय नमः ।]]{style="color:#99cc00;"}

[अष्टवर्ग तृतीयन्तु मायाग्नि समन्वितं ।\
तस्य वर्गाद् इमम् छैव बिन्दु-नाद समायुतं ।\
इदं बीजं विजानीयात् षण्मुखाय नमो ऽन्तकं ॥\
[ॐ ह्रीं षं षण्मुखाय नमः]]{style="color:#99cc00;"}

[Then the पञ्च brahmA combinations are provided:\
ऒं सं सद्योजाताय नमः । [सद्योजातं ...]\
ऒं सिं वामदेवाय नमः । [वामदेवाय नमो...]\
ऒं सुं अघोराय नमः । [अघोरेभ्योत् ...]\
ॐ सें तत्पुरुषाय नमः । [तत्पुरुषाय ...]\
ॐ सां ईशान-मूर्ध्ने नमः। [ईशान सर्व ...]]{style="color:#99cc00;"}

Then the ShaN-मूर्ति mantras:\
[ॐ सुं सुब्रह्मण्याय नमः । ॐ कुं कुमाराय नमः । ॐ हं हरसूनवे नमः । ॐ सुं सुराग्रजाय नमः । ॐ सें सेनापतये नमः । ॐ सुं सुरेशाय नमः ॥]{style="color:#99cc00;"}

Then the shaktyuta मूर्ति षडक्षरी विद्या-s:\
[ॐ जगद्भुवे नमः । ॐ वचद्भुवे नमः । ॐ विश्वभुवे नमः । ॐ रुद्रभुवे नमः । ॐ ब्रह्ंअभुवे नमः । ॐ भुवोत्भुवे नमः ॥]{style="color:#99cc00;"}

कौमार वशीकरणादि विद्या:\
[ॐ श्रीं ह्रीं क्लीं सुं वं यं सर्व-लोकं मे वशमानय षण्मुखाय मयूरवाहनाय सर्व-राज भय विनाशाय सर्व देव-सेनाधिपतये सर्व शत्रुनाशाय सूं सुब्रह्मण्याय नमः ।]{style="color:#99cc00;"}

homa mantra:\
[ॐ शरवनभव ह्रीं स्वाहा ॥]{style="color:#99cc00;"}


