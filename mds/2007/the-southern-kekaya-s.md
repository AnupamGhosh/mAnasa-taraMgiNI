
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Southern kekaya-s](https://manasataramgini.wordpress.com/2007/03/14/the-southern-kekaya-s/){rel="bookmark"} {#the-southern-kekaya-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 14, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/14/the-southern-kekaya-s/ "5:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The early kShatriya-s of the अन्ध्रापथ are important to understand the origins and transmission of the early medieval कौमार sect. They are also critical to understand the peregrinations of kShatriya-s throughout India and the role they played in the unification of the country. These movements are also important to understand the early movements of the पाशुपत shaiva lineage into the southern country. We have earlier alluded to the the role played by the इक्ष्वाकु-s of अन्ध्रापथ in the development of the [कौमार sect](http://manollasa.blogspot.com/2005/10/royal-kumara-worshippers.html) in the south. We believe that this movement of the इक्ष्वाकुस् and associated clans into दक्षिणा-patha was a key element of the entry of the कौमार religion into the drAviDa country, where it has lingered in a peculiar form to this day even after it faded away in other parts of India. The इक्ष्वाकु movement to the south is suggested by the establishment of the principalities of ashvakas and मूलक-s by members of this dynasty on the river banks of the गोदावरी, which were around during the शतवाहन period. The fall of the शतवाहनस् allowed many of these gain territory, and in अन्ध्रापथ a line of इक्ष्वाकु-s gained ground under the great king शान्तमूल, the performer of vedic sacrifices and the worshiper of कुमार.

Interestingly, D Sircar's studies on inscriptions brought to light a lineage of kekaya-s too in the southern country who lay to the West of the southern इक्ष्वाकु-s centered around the chitradurga town in Mysore. The kekaya-s are a member of the पञ्च jana as per traditional testimony and are the descendants of anu. As per the testimony of the veda, पुराण-s and इतिहास-s they along with another anu lineage, the माद्र-s, lay to the northwest of the sub-continent. Their great king ashvapati kekaya was rAjarShi who taught many ब्राह्मणस्. The रामायण mentions them as in-laws of dasharatha and allies of the इक्ष्वाकु-s who helped bharata and his sons तक्ष and पुष्कल in the wars against the gandharas. The memory of their capital rAja-गृह in Balkh is recorded by the Chinese agent Yuan Chwang.

The inscriptions pertaining to the southern branch mentions "इक्ष्वाकुभिर् api राजर्षिभिः कृत् आवाह विवाह-- implying that they exchanged both sons and daughters in marriage with the family of इक्ष्वाकु rAjarShi-s. The use of the term rAjarShi strongly suggests that they were performers of shrauta sacrifices. The kadamba king विष्णुवर्मन् is mentioned as: "kaikeya-सुतायाम् utapanna" implying that his mother was a kekaya princess. In an inscription from 470 CE we have the statement that the queen of the kadambas, wife of king मृगेशवर्मन्, is kaikeya महाकुल प्रसूता (born in the noble family of the kekayas). Another inscription in Haldipur, Karnataka mentions the wife of the pallava chief चण्डमहासेन, who was a great कौमार, as being from the kekaya kula. Their son was पल्लवराज-गोपालदेव. Thus the kekayas appear to have had relationship with the other 3 major southern dynasties, the इक्ष्वाकुस्, pallavas and kadambas. Given that powerful dynasties like the शतवाहनस्, pallavas, kadambas, विष्णु-कुण्डिन्स् and शालङ्कायनस् never claimed origin from the historic Aryan dynasties of the पुराणस्, there is no reason to doubt that the इक्ष्वाकुस् and kekayas of south India were migrant versions of the northern kShatriya dynasties. This suggests that after the rise of the mauryan power in North India, many remnants of the old dynasties started migrating outwards from their home zones, mainly towards दक्षिणापथ. In the south they seem to have initially survived in the shadow of the शतवाहनस् before coming of their own.

The end of the kekaya power seems to have come after the pallavas destroyed them along with the kadambas in the battlefield of नणकास. The chitradurga inscription mentions that their king shivanandavarman was wounded mortally in the battle and after the destruction of the kadamba king कृष्णवर्मन्, he lay on a darbha bed and waited for his death. This inscription mentions that he was a parama-माहेस्वर, or a great पाशुपत.


