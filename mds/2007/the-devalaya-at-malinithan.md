
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The देवालय at मालिनीथन्](https://manasataramgini.wordpress.com/2007/05/14/the-devalaya-at-malinithan/){rel="bookmark"} {#the-दवलय-at-मलनथन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 14, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/14/the-devalaya-at-malinithan/ "5:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the remarkable forgotten देवालय-s of the eastern fringes of प्रग्ज्योतिष is that of मालिनीथन् in the modern state of Arunachal Pradesh. ekanetra and me were rummaging through some old grainy photos collected by his sachiva during his expedition to Arunachal Pradesh, when we were reminded of this remarkable shrine. There is no support for the medieval dates floated around by the archaeologists, and not much is known of its builders and patrons. Evidently, at some point it went out of the larger Hindu memory because it lies in utter ruins today.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RkgB149MdVI/AAAAAAAAAJM/5D_L2Ut9YMQ/s320/mahagaNapati.jpg){width="75%"}
```{=latex}
\end{center}
```

\
उच्छिष्ट-gaNapati with मदनावती

The main images of the shrine appear to be carved in granite while the smaller ones are in sandstone. A well-fashioned nandin suggests that the main shrine was perhaps a शिवालय, but then it does not survive. The other granite images are of दुर्गा, उच्छिष्ट-gaNapati with his shakti मदनावती on his lap, कुमार with his peacock, deva सविता and indra mounted on ऐरावत. The विनायक is shown with several weapons (more like महागणपति) but tickling the yoni of his shakti with his trunk like in case of उच्छिष्ट-gaNapati.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RkgB1o9MdTI/AAAAAAAAAI8/kigeqqlYYws/s320/indra_malinithan2.jpg){width="75%"}
```{=latex}
\end{center}
```



[indra ]{style="font-style:italic;"}

The indra is of considerable interest in iconographic terms. His third eye on the fore seems horizontal as recommended in the शिल्पशास्त्र-s, he has a large यज्ञोपवीत, and beside him are two ayudha puruSha-s of his vajra and अङ्कुष. There is additionally a second similar image of indra in the मालिनीथन् site made of sandstone and in a greater state of ruin. An iconographically similar series of indra images have been found in the ruins of temples in Chatrakara (one of the finest indra images ever seen in India), Dibrugarh, Pandughat and Narakasura hill in Assam (the last site producing even a bronze utsava मूर्ती of indra). The presence of several indra icons in the East is rather interesting, given his general rarity as a prominent deity in the temples of other parts of India. This suggests that there was a vigorous worship of indra even in the medieval period in this region of India, even after he had declined elsewhere. One point of great interest to investigate in this regard is whether there was an associated vedic tradition in these regions that is now lost and whether the defining feature of the Hindu nation, the indra-dhvaja festival was observed in these regions.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RkgB1o9MdUI/AAAAAAAAAJE/LdHr1YEafYg/s320/विपरीत_ratI.jpg){width="75%"}
```{=latex}
\end{center}
```



[कालिका in विपरीत-rati with लिङ्ग ]{style="font-style:italic;"}

A sandstone icon of considerable interest in the मालिनीथन् ruins is that of दक्षिण-कालिका in विपरीत-rati with a लिङ्ग. The only parallels of this form of दक्षिण-कालिका are apparently seen in the पिङ्गलेश्वर temple in Assam, Gauhati and the nearby temple on a hill a Sualkuchi. Thus, in conclusion मालिनीथन् is an unusual temple combining a series of poorly studied features of seen in the eastern reaches of the country. The shAkta-tantra-s role, in particular kula tantra-s, influence is seen in the form some of the iconographic details including the कालिका and the विनायक. While at the same time the persistent survival of indra and कुमार worship is a unique feature of this region that needs more detailed study.


