
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कामन्दकि on विष्णुगुप्त and chandragupta](https://manasataramgini.wordpress.com/2007/04/06/kamandaki-on-vishnugupta-and-chandragupta/){rel="bookmark"} {#कमनदक-on-वषणगपत-and-chandragupta .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 6, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/06/kamandaki-on-vishnugupta-and-chandragupta/ "2:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[यस्य+अभिछार-वज्रेण वज्र-ज्वलन-तेजसः ।\
पपात मूलतश्-श्रीमान् सुपर्वा नन्द पर्वतः ॥]{style="font-style:italic;color:#0000ff;"}\
[एकाकी मन्त्र-शक्त्या यश्-शक्त्या शक्ति-धरोपमः ।\
आजहार नृ-चन्द्राय चन्द्रगुप्ताय मेदिनीं ॥]{style="font-style:italic;color:#0000ff;"}\
[नीति-शास्त्रामृतं धीमान् अर्थशास्त्र महोदधेः ।\
समुद्दध्रे नमस्-तस्मै विष्णुगुप्ताय वेधसे ॥]{style="font-style:italic;color:#0000ff;"}\
[दर्शनात्-तस्य सुदृशो विद्यानां पारदृश्वतः ।\
यत्-किञ्चिद्-उपदेक्ष्यामः राज-विद्या-विदां मतं ॥]{style="font-style:italic;color:#0000ff;"}

By the vajra-like अभिचार of the one who shone like the flash of the vajra, the opulent mountain of the nanda-s fell root and branch.\
Who alone with his ministerial power \[pun on mantra power] , which was like the power of the wielder of the shakti \[skanda], conferred the earth on chandragupta, the moon amongst men.\
The one who churned out the nectar of political wisdom, from the vast ocean of state-craft, to that wise विष्णुगुप्त our salutations.\
From the great work of that man, who had explored the limits of knowledge, we are going to extract and teach in brief the lore, as promulgated by the knowers of state-craft.


