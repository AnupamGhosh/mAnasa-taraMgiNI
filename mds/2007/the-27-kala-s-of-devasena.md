
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The 27 कला-s of देवसेना](https://manasataramgini.wordpress.com/2007/08/24/the-27-kala-s-of-devasena/){rel="bookmark"} {#the-27-कल-s-of-दवसन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 24, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/24/the-27-kala-s-of-devasena/ "5:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/Rs5u7fXcXhI/AAAAAAAAANc/f77pxaAh8qU/s320/devasena_skanda.jpg){width="75%"}
```{=latex}
\end{center}
```



The kaushika prashna of the कौमारागम gives the 27 fold कला mantra-s of the goddess देवसेना. skanda-पत्नी should be meditated as being taken as a wife by the fiery son of agni or rudra. Each कला mantra is made from one of the following 27 names of देवसेना:

OM शचीसुता cha देवी cha देवसेना cha sundarI |\
उत्पला पुष्पधारी cha पीताम्बर-परिवेष्टिता ||\
ईश्वरी cha महादेवी लक्ष्मी vijaya-साधकी |\
रमणी दामिनी cha वासवी सौभाग्यदायिनी ||\
airavata-कुलोद्भूता देवेन्द्रतनया शची |\
एकरूपी महाशक्ति sthula-सूक्ष्म-bhavAni cha |\
सारन्ग-लोचनी केकी कृत्तिका-priya-कान्तकी |\
सर्वैश्वर्यप्रदा chaiva skanda-प्रीता cha भामिनी ||

They are combined with 27 sets of bIja-s

 1.  bAlA त्रयं 2) त्रिपुरा त्रयं 3) bhuvana त्रयं 4) महालक्ष्मी 5) mAyA 6) madana 7) वाणी 8) shakti 9) महामाया 10) manu 11) तन्त्रमूलं 12)दशाङ्ग dasha बीजं 13) वासवी 14) दृष्टि 15) shakti 16) mAyA 17) कीलक 18) पञ्च भूत पञ्च बीजं 19 indra 20) agni 21) yama 22) निरृत्ति 23) वरुण 24) वायु 25) kubera 26) ईशान 27) prANa त्रयं

These are then combined in dative with नमः to obtain the final mantras.


