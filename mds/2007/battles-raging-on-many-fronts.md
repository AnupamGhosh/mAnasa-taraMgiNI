
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Battles raging on many fronts](https://manasataramgini.wordpress.com/2007/08/12/battles-raging-on-many-fronts/){rel="bookmark"} {#battles-raging-on-many-fronts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 12, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/12/battles-raging-on-many-fronts/ "4:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

On the front of jIva-sUtra parama पदारथ we had faced a terrible defeat due to the mechanisms of our current भ्रातृव्य and old सुदानु. However, our रोमाक allies gave us a chance to strike through the gap. We ably aided by our best vIra-s on the battlefront broke through the gap and won a victory that for years we had yearned for. We also took the fortress of जीवसूत्र kendra. Aided by our able सेनाध्यक्ष we broke through the defenses of the lohita-dhara and sharkara-dhara and conquered them. But on yet another front we were pursued by the terrible कृत्या-s of धूमावती and rakta-ज्येष्ठा. We tried to flee to gaja-लण्ड-pura and from there to कामसेतु. Yet, the कृत्या-s pursued us like a malarial infestation persisting in a victim. Still gripped by the कृत्या, like the atri दुर्वास being pursued by the chakra, we went before the younger brother of indra, who strides triply to crush the प्रह्लादीयस्. The wielder of the chakra, who prowls like the dreaded lion in the mountains, revealed to us the prayoga of the haridra-lepa. With that we brought into control the attack of the कृत्या powered by rakta-ज्येष्ठा. But the fury of the other one pursued us even into our fortress-- thus our last defense had been penetrated, like the वीतहव्य horsemen and chariots raiding kAshI. For several hours she remained on our path until we fled to the young yoddha-s fortification to evade the कृत्या.

\~*\~

It almost appeared that Fourier functioned beneath his grave. Seeing that the third hero had achieved extraordinary feats in battle and had sacked piles of wealth, the partisans of the dead engineer of मण्गलग्राम decided to launch an attack on the un-guarded sachiva of the 3rd hero. "The hammer of the crane" it is called or trailokya-स्तम्भिनी --- normally fatal. But our अमात्य using a dUra-दृष्टि prayoga saw clearly that the terrifying विद्या was on its way. Our अमात्य selflessly tried to intercept and sent forth a peculiar ulka-prayoga charged by the frightful vighna. As the texts say " the devadatta's hands, legs and then speech depart from him when a स्तम्भिनी-विद्या hits him" so it transpired. It looked as though he was being borne to vaivasvata. But then the counter-prayoga brought him back. Our sachiva and the अमात्य of the third hero then combined to save the victim from destruction. But for the dUra-दृष्टि prayoga he could have been history by now.


