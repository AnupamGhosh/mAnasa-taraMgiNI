
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Strike as expected](https://manasataramgini.wordpress.com/2007/01/12/strike-as-expected/){rel="bookmark"} {#strike-as-expected .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 12, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/12/strike-as-expected/ "6:54 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had the दूरदृष्टि about the movements of the खाण्डवन् vairi-s. We knew precisely how they were going to strike. So we waited patiently and as we knew that they had two prayogas against us and where the अभिचार was going to be placed. We saw the first graha coming and were for a while caught in its stambhana. Then the great rudra came to our aid when we made the 8-fold oblations and neutralized it. Who verily escapes the त्रिशूल of The god? Then we moved forth and dug up the अभिचार and retrieved the needful. However, to our dismay in the process a dreadful कृत्या escaped, much like the one which nearly sent us to the buffalo-rider. The कृत्या could not immediately hit us as we were protected by the vajra of the thunderer, but it is around and could hit anytime.

Further news in streamed in on the mischief. The MMM had heard the howling of the wolf almost like in one of those perfect motif scenes from one of the इतिहास and was terrified by it. MMM was overpowered by their time tested strike of धूमावती. One packet of धूमावती was sent our way with all its typical accouterments . That one who was consumed earlier by the बगलामुखी prayoga, however fought back fiercely, and was saved the other अमात्य's and MMM-intersection's guhya prayoga. But now it was our पक्ष's time under the fire as they had turned our way in the alternating attack. We also realized that the spasha-s had deeply penetrated us. Then the news reached us the that the स्वामात्य was taken by another mystery hit. We received the message that as in the battle where kabandha-mukha had been repulsed by are mantras to बृहस्पति, we needed बृहस्पति's aid again.

We saw the कर्ण-पिशाचिनि might be used and moved with utmost caution with the fiend-driving vajra of indra to cause the पिशाचिनि to lose track of us. As the भृगु-s of yore frustrated the पिशाचिनि-s we too frustrate them. May indra nullify the कृत्या makers, may we not lose an eye like भट्ट कुमारिल.

...*...*...

The कृत्या launched two attacks on the expected day. We panicked briefly but were able to hold fort. This was a classical धुमावती कृत्या in every sense --- the खाण्डवन्स् were completely unaffected by it even when back hurled. Instead it kept hovering around us constantly making attempts to pierce our shield of skanda


