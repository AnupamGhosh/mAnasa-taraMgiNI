
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The दिवाकीर्त्य सामन्स्](https://manasataramgini.wordpress.com/2007/01/13/the-divakirtya-samans/){rel="bookmark"} {#the-दवकरतय-समनस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 13, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/13/the-divakirtya-samans/ "5:41 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The celebrated दिवाकीर्त्य gAnaM deployed during the [eclipse](https://manasataramgini.wordpress.com/2007/01/18/divakirtya-s-eclipses-and-severed-heads/) and the [*[solstice rites ]{style="color:#0000ff;"}*](https://manasataramgini.wordpress.com/2007/01/15/the-crashing-doors/)comprises of 5 सामन्स् that are expounded in the पञ्चविंश brAhman 4.6. Not surprisingly they are termed collectively termed Adityasya vrata शुक्रियानि and are the following सामन्स्:\
भ्राज: अरण्येगेय gAnaM 6.1.5\
It is composed on the lyric: RV 9.66.19\
[अग्न आयूंषि पवस आ सुवोर्जम् इषं च नः । आरे बाधस्व दुछुनां ॥]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
agni you purify our life, send to us swift strength. Prevent the misfortune from over taking us.

The magical stobha (of course there are the regular स्तोभास् of sAman like uvA etc.) of this song is भ्राज/ e भ्राज.

आभ्राज: अरण्येगेय gAnaM 6.1.6\
It is composed on the lyric: RV 8.44.16\
[अग्निर् मूर्धा दिवः ककुत् पतिः पृथिव्या अयं । अपां रेतांसि जिन्वति ॥]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
agni is the head and the axis of heaven, the lord of this earth. He makes the seed of water successful.

The magical stobha of this song is A भ्राज/ e A भ्राज

विकर्ण: अरण्येगेय gAnaM 6.1.7\
It is composed on the lyric: RV 10.170.1. The lyric in the Aranya saMhitA 5.2 slightly differs (see brackets).\
[विभ्राड् बृहत् पिबतु सोम्यं मध्वायुर्दधद् यज्ञपतावविह्रुतं ।]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[वातजूतो यो अभिरक्षति त्मना प्रजाः पुपोष (पिपर्ति) पुरुधा (बहुधा) वि राजति ।]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
May the great bright one drink soma with honey, giving the lord of the sacrifice (यजमान implied)undiminished life. He impeled by the wind personally protects our people, having nourished them with food and lords over many regions.

The magical stobha-s of this song are: iDA; ऋतं me; फट् (uttered with musical elongation); भ्राट्

भास: अरण्येगेय gAnaM 6.1.8\
It is composed on the lyric: RV 6.8.1. However, the lyric given in the आरण्य saMhitA 3.8 of the kauthuma/राणायनीय सामवेद slightly differs (see brackets) suggesting that this might be derived from a different ऋग् tradition than शाकल्य or re-worded for ritual deployment.\
[पृक्षस्य (प्रक्षस्य) वृष्णो अरुषस्य नू सहः (महः) प्र नु (नो) वोचं (वचो) विदथा जातवेदसः ।]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[वैश्वानराय मतिर् नव्यसी शुचिः सोम इव पवते चारुर् अग्नये ॥]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
In this assembly of sages I will proclaim the greatness of the red-spotted (the red destructive) bull जातवेदस्. A pure and new mantra flows to वैश्वानर, like the purified tasty soma for agni.

This song has two types of specialized magical stobhas. Type 1: hiM hiM hiM; haM haM haM; भं (uttered with musical elongation).\
Type 2: idA; ऋतं me; भद्रं; श्रेयः; वामं; वरं; सुवं; asti; अभ्राजीत्; ज्योतिः; दीदिवः

महादिवाकीर्त्य: अरण्येगेय gAnaM 6.1.9-18\
The महादिवाकीर्त्य songs are largely stobha-gAnaM-s each having its magic stobha-s\
The 1st song is called the head of the दिवाकीर्त्य and its magic stobhas are:\
आयुः; ज्योतिः; vAg-ज्योतीः\
The 2nd song is called the neck and it has two type of magic stobhas. Type 1: haM; sthi Type 2: हियेवा; hiyagni; hi-indrA; hi-पूषन्; hi-देवाः.\
The 3rd is called the shoulders has the following magic stobhas: वयोमनः; वयोप्राणाः; वयश्चक्षुः; वयः श्रोत्रां; vayo घोशः; vayo व्रतं; vayo भूतं\
The 4th is called the sternum and has the same magical stobhas as the 3rd song but set to an entirely different tune.\
The 5th is called the right ribs and it has the stobhas: Urk; वाक्; dharma; adharma\
The 6th is called the left ribs and it has the two types of special stobha-s. Type 1: haM वं va. Type 2: dharma; vidharma; सत्यं गाय; ऋतं vada\
The 7th known as the Atman is composed on the lyric RV 10.170.1/Aranya saMhitA 5.2 as the विकर्ण sAman but is set to a totally different tune. Its magical stobhas are the "special" व्याहृतिस् (all musically elongated): भुवः; janat; वृधत्; karat\
Type 2: e अभ्राजीत्; ज्योतिः\
The use of the special व्याहृति-s is rather striking because outside of the sAman tradition as in the दिवाकीर्त्य-s it is only used in the atharvanic form of the shrauta ritual. Many of these calls are inaudibly made by the brahman at the time of different soma pourings. The atharvan-s also deploy them in their formula known as the praise of indra in all great sacrifices:\
OM भूर् भुवः svar janad वृधत् karad ruhan mahat tach Cham OM indravanta stute ||\
The 8th महादिवाकीर्त्य is known as the right thigh and has the special stobha-s: भूमिः; अन्तरिक्षं; द्यौः; e भुताय\
The 9th महादिवाकीर्त्य is known as the left thigh and has the special stobha-s: द्यौः; अन्तरिक्षं; भूमिः; e आयुषि\
The 10th song known as the tail is characterized by the special stobha: ज्योतिः\
It exits in the nidhana: I(3) 12345


