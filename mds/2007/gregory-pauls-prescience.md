
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Gregory Paul's prescience](https://manasataramgini.wordpress.com/2007/09/22/gregory-pauls-prescience/){rel="bookmark"} {#gregory-pauls-prescience .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 22, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/22/gregory-pauls-prescience/ "5:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The previous snippets on related topics:

  -  [The big and small of theropods](http://manollasa.blogspot.com/2007/06/big-and-small-of-theropods.html)

  -  [Organ and Edwards on dinosaur genomes](http://manollasa.blogspot.com/2007/03/organ-and-edwards-on-dinosaur-genomes.html)

  -  [Microraptor as a biplane](http://manollasa.blogspot.com/2007/01/microraptor-as-biplane.html)

  -  [Jurapterxy- wrong inferences?](http://manollasa.blogspot.com/2006/03/jurapterxy-wrong-inferences.html)

  -  [Tyrannosaurs and morphological evolution in coelurosaurs](http://manollasa.blogspot.com/2006/02/tyrannosaurs-and-morphological.html)

  -  [The rise of tyrannosaurs](http://manollasa.blogspot.com/2006/02/rise-of-tyrannosaurs.html)

  -  [Buitreraptor](http://manollasa.blogspot.com/2005/10/buitreraptor.html)

  -  [The adaptive radiation of avian and para-avian clades](http://manollasa.blogspot.com/2005/02/adaptive-radiation-of-avian-and-para.html)


  -  [Neuquenraptor argentinus and the deinonychosaurs](http://manollasa.blogspot.com/2005/02/neuquenraptor-argentinus-and.html)

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RvX8XEaSUdI/AAAAAAAAAOk/dJkgPiar3Gk/s320/thermopolis_archie.jpg){width="75%"}
```{=latex}
\end{center}
```



[ठेर्मोपोलिस्]{style="color:rgb(51,255,51);"}[Archaeopteryx]{style="font-style:italic;color:rgb(51,255,51);"}

Gregory Paul, the artist and maverick paleontologist published a remarkable book in 1988 "The predatory dinosaurs of the world". For its era it was a masterpiece of scientific art. At that point in time, other than [Archaeopteryx, ]{style="font-style:italic;"}feathered dinosaur fossils were hardly known from the Mesozoic. But Paul restored his theropods with feathers. What was striking was that his dromaeosaur and oviraptorosaur representations might be as good as those made after the "Chinese revolution" from Liaoning. He also predicted the hyperextendible claw in [Archaeopteryx, ]{style="font-style:italic;"}which was proved after the publication of the Thermopolis specimen in 2005[. ]{style="font-style:italic;"}However, what was even more striking was the theory presented first in that book that the dromaeosaurs were actually secondarily flightless dinosaurs that had reverted to terrestriality from an ancestor that resembled a flighted form like [Archaeopteryx]{style="font-style:italic;"}. In a subsequent book, "The Dinosaurs of the Air", another artistic masterpiece, Paul went on build the case for the possibility of secondary flightlessness in the dromaeosaur, troodontid, oviraptorosaur and therizinosaurid clades. However, Gregory Paul is not one who uses phylogenetic methods to construct evolutionary trees and lay out his theories in the framework of a phylogenetic tree. Instead he builds his case based on mechanistic and anatomical arguments. As a result dinosaur evolutionists have tended not to cite his work. This sad in a sense because I believe that phylogenetic frameworks are not only vindicating his prescience but also adding details that were not accessible to him.

The latest in this line of finds is that of the dromaeosaur [Mahakala omnogovae]{style="font-style:italic;"} from Mongolia reported by Turner et al. Also authors on the paper are two of the most noted American paleontologists of the modern era, Norell and Clarke (famous for numerous evolutionary studies on avian and non-avian dinosaurs). [Mahakala ]{style="font-style:italic;"}is from the Djadokhta formation in Mongolia from the Campanian age (i.e. the second last epoch of the Cretaceous). The remains are relatively scrappy but covering a good part of the skeleton --- a partial skull with brain case, sacrum, partial pelvis, several vertebra, femur, foot with sickle claw, and ulna. Despite its relatively young age it is clearly a primitive dromaeosaur showing that primitive members of this clade were around even in the penultimate epoch of the Cretaceous. Its size was estimated at around 70 cm.

A phylogenetic analysis of the avian-paravian clade including [Mahakala]{style="font-style:italic;"} produced striking results. The basal most division was into birds and deinonychosaurs. The birds have as their basal most branch [Archaeopteryx ]{style="font-style:italic;"}and [Shenzhouraptor ]{style="font-style:italic;"}uniting as a monophyletic lineage of primitive long-tailed toothed birds. The next most basal lineage of birds that are a sister group to all other birds are the Omnivoropterygids which include the extremely enigmatic oviraptorosaur-like [Sapeornis ]{style="font-style:italic;"}and [Omnivoropteryx]{style="font-style:italic;"}. The deinonychosaurs split up into troodontids and dromaeosaurs. The basal-most lineage of troodontids are the Chinese forms [Mei]{style="font-style:italic;"} and [Sinovenator]{style="font-style:italic;"}, with a crown group formed of [Byronosaurus]{style="font-style:italic;"}, [Saurornithoides ]{style="font-style:italic;"}and [Troodon]{style="font-style:italic;"}. The basal-most dromaeosaur is Mahakala with 3 major radiations including: 1) The unenlagines- [Unenlagia ]{style="font-style:italic;"}(\>Nequenraptor?), [Buitreraptor]{style="font-style:italic;"}, [Rahonavis ]{style="font-style:italic;"}and [Shanag]{style="font-style:italic;"}. Of these, all except Shanag are from the Gondwanan fragments. 2) The Chinese radiation comprised of [Microraptor]{style="font-style:italic;"}, [Graciliraptor ]{style="font-style:italic;"}and [Sinornithosaurus]{style="font-style:italic;"}. [Bambiraptor ]{style="font-style:italic;"}might also belong to this clade, but the prudish paleontologists, due to some weird dispute regarding specimen ownership or something like that tend to ignore this magnificent specimen. 3) The velociraptorine clade comprised of [Velociraptor]{style="font-style:italic;"}, [Deinonychus, Saurornitholestes, Tsaagan, Adasaurus, Dromeosaurus]{style="font-style:italic;"} and the large [Utahraptor]{style="font-style:italic;"} and [Achillobator]{style="font-style:italic;"}.

This tree suggests that flighted forms were potentially present on all the 3 major clades- Aves, dromaeosaurids and troodontid. Amongst the deinonychosaurs, [Rahonavis ]{style="font-style:italic;"}from the unenlagine clade was initially described as a bird and was definitely flighted. [Microraptor ]{style="font-style:italic;"}from the basal "Chinese clade" is also definitely flighted with the potential "biplane morphology". Among the troodontids [Mei ]{style="font-style:italic;"}and [Jingfengopteryx ]{style="font-style:italic;"}might have had limited flight capabilities. Using the tree, Turner [et al]{style="font-style:italic;"} also reconstruct the common ancestor of deinonychosauria as well as the paravian clade (aves+deinonychosauria+Scansoriopterygids) as being around 70 cm and \~700 g which is consistent with them being fliers. Turner et al do not include the enigmatic and poorly preserved scansoriopterygids, which could further destabilize their clades in different ways. There are other fragmentary forms like [Pedopenna]{style="font-style:italic;"}, which might unify with one of the known clades or form yet other poorly known lineages within paraves. It is more likely that actually the flighted ancestor amongst coelurosaurs goes back further in time as Paul suggested, including the oviraptorosaurs and therizinosaurs, and perhaps even the alvarezsaurids (initially believed to be birds).

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RvX790aSUcI/AAAAAAAAAOc/1Isczus6mQk/s320/sapeornis.jpg){width="75%"}
```{=latex}
\end{center}
```

\
[षपेओर्निस्]{style="font-style:italic;color:rgb(51,255,51);"}

This proposal has an important implication (which is in line with Paul's original suggestion): Could our current cladistic analysis be artificial grouping flighted forms together into a monophyletic aves merely because they retain several flight adaptations? Is it possible that just like [Rahonavis ]{style="font-style:italic;"}moved inside dromaeosaurs, other lineages currently grouped inside aves might actually group with other coelurosaur groups. One immediate candidate for this [Archaeopteryx ]{style="font-style:italic;"}itself. Could it be that it is actually close to the ancestor of the entire paravian clade. Being the earliest representative of the clade from the Tithonian epoch of the Jurassic it is well-placed to occupy this position. Another more unclear case is the omnivoropterygid clade within Aves. Both [Sapeornis ]{style="font-style:italic;"}and [Omnivoropteryx]{style="font-style:italic;"} share a general cranial similarity with oviraptorosaurs, in which some semi-pygostylic forms like [Nomingia ]{style="font-style:italic;"}have been described. Is it possible that these represent the early-branching flighted representatives of the oviraptorosaurid clade ? More fossils, if and when they become available, might resolve these issues.

More recently, in another paper Turner[ et al ]{style="font-style:italic;"}present the discovery of quill knobs on the ulna of [Velociraptor]{style="font-style:italic;"}, just as those reported in [Rahonavis]{style="font-style:italic;"}. This establishes beyond doubt that even the flightless larger forms retained their secondary feathers This also shows how flightless derived forms with unmodified clawed hands proceeded in directions other than that seen [Patagopteryx ]{style="font-style:italic;"}or the Ratites. Flightlessness in the deinonychosaurs and possibly oviraptorosaurs was often accompanied by retention of the flight feathers of the arm suggesting that that the feathers had an adaptive value beyond flight. Many possibilities exist but limited volancy in juveniles, and wing-assisted incline running are the ones I favor most.

Whatever the case it is becoming increasingly clear that as Paul had suggested flight emerged much before the avian crown group amongst theropods and might have been repeatedly lost in the early stages of its evolution and coexisted with precursors and derivatives like wing-assisted incline running, gliding from trees, and biplane volancy. It also appears likely that feathers (or the homologous dino-down) evolved before flight, most possibly as the functional equivalent of mammalian hair. When did this dino-down first emerge in archosaurs? We know most likely this emerged after the crocodile line branched off, by the exact point of emergence is the big question still in need of an answer.


