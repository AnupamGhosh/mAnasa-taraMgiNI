
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The asura](https://manasataramgini.wordpress.com/2007/06/17/the-asura/){rel="bookmark"} {#the-asura .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 17, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/17/the-asura/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R was telling me how she was reviving her संस्कृत by reading the हरिवंश that I had given. R being like the proverbial hidden Rustam of the Iranians, can show a sharp eye. She was struck by a name of वरुण, i.e. prachetas, in the हरिवंश and was discussing its provenance with me. अमरसिंह gives a name of वरुण as prachetas. कालिदास following पौराणिc tradition in the शकुन्तला (if I recall right) calls him prachetas and this name is mentioned by all major vedic commentators like ऊवट, सायण and महीधर of mantra-mahodadhi fame. prachetas might have two meanings: wise or ever-alert. Both of which are applicable to वरुण as he is described in the ऋग्वेद. This reminded me of the ऋग्वेदिc mantra of शुनःशेप Ajigarti: "क्ष्यन्-नसंभ्यं [असुर प्रछेताः]{style="font-style:italic;color:rgb(255, 0, 0);"}" (RV1.24.14); here he is called the asura prachetas- which captures the semantics of his Iranian cognate exactly: ahura मज़्धा (the wise ahura). This phrase also occurs again in RV 8.90.6 which seems to refer to वरुण as the asura who is prachetasa. This suggests that just as माज़्धा (cognate of medhira in Vedic, an adjective applied to वरुण) was used in the Iranian world, prachetas was the preferred cognate of this form of the name in the Indo-Aryan world.

We also encounter a similar term in RV 8.42.1 "अस्तभ्नाद् द्याम् asuro विश्ववेदा", where वरुण is called asuro vishvavedas- the all knowing asura.


