
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The figure in the shadows](https://manasataramgini.wordpress.com/2007/03/29/the-figure-in-the-shadows/){rel="bookmark"} {#the-figure-in-the-shadows .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 29, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/29/the-figure-in-the-shadows/ "5:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After the futile battle of खाण्डव, which to us was like the decline into the sufferings of the kali age, we had climbed into the tower of परीक्षित्. Having been driven away from there by fate we had landed in राजग्राम, ever shadowed by that ghoul of मलपट्टन. We wandered long ago like a performer of the कपाळ vrata in the forests around the dasyu शाल. कृष्ण-mukha, khara-mukha and mala-tvacha were seated on the wall at the rim and laughing loudly. We ran into the niShada and asked him what lay beyond the yonder mist. He told 4 stories and the wheel of काल raced ahead. Down a deep tunnel-like alley were projected the laughing three-some. We saw them in deep, and then they vanished into nothingness. Then the niShAda smiled and said these भ्रातृव्य-s will be washed away like the rivals of सुदास being washed away by the परुष्णि. But then there will rise the dreadful one, the ghoulish one; the dream will show it. We went on a long ride on our ashva and crossed the stream of go-मूत्रक.\
We saw सुजाता and asked her the following cryptic question: "What will your number be and when will it come ?" She replied: "it will be two and end at द्वादशान्त. I will inform you in the most subtle of ways." She did so recently.\
We saw the kaushika and asked him: "When ?" He said: "When you are burning in battle I alone shall be beside you, even as the वैश्वामित्र-s around रामो भार्गव."\
We saw kalashodbhava-सुमङ्गला and asked: "what is your number ?" She said: "It will start just before द्वादशान्त. As for its full extant do not ask."

Then we stumbled into the senior भार्गव. He said: "the third hero has broken forth, but why do the two ghouls chase their victims like the beings sent forth by the kAshyapa to slay the भारद्वाज?"\
...\
We were nearing the end of a train journey. It was similar to the end of another such journey at the end of the mirth and joy of दशान्त. It ended and we went up the steep rock face near giri पाद that lay on the opposite side of the city from the कुमार गृह. We saw vidrum then --- in a few days the flow of काल had swept him aside as the flood on the परुष्णि bore the enemies of सुदास. We survived, and emerged victorious in battle. But then after getting down from this journey we did not know who would be swept away.


