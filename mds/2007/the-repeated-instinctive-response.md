
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The repeated instinctive response](https://manasataramgini.wordpress.com/2007/05/18/the-repeated-instinctive-response/){rel="bookmark"} {#the-repeated-instinctive-response .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 18, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/18/the-repeated-instinctive-response/ "5:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many years ago, when we were still in the country founded by our ancestors, it was a very quiet evening, and the yoga kept things in proper condition. We really did not want to be there, instead wanting to indulge in mirth with the muni. Nevertheless, we went to मलस्थान, a little beyond the southern bank of go-मूत्रक. There in the desolate, musty and grimy corridors we stood along with ekanetra, शाण्डिल्य and vaishya-श्रेष्ठ, in a certain state of mind that we are unable to even recover in our dreams. I had tied my ashva to a tree near the sabha owned by the rich मूढक who boasted a lot to the अमात्य. Unexpectedly, R rushed in on her तुराश्व bringing Sujata with her. We had an enjoyable chat for a little with me envying R's position of freedom and dreading Sujata's state of being under the yoke. Soon they all dispersed : R and ekanetra free, but the rest bound, and I was alone to put up with the महाजन for 2 hours with a motley bunch of the samatibhindakas. At the end of it, as we left the mahajana and stumbled out it was a inky dark night with a forbidding quietness. We looked above and saw रोहिणि glittering in the eye of the वृषभ. We saw our ashva remained unharmed at the tree protected from the कुलुञ्च by the spell of bhairava that we had laid there. We thought we should proceed to observe the skies with the vaishya-श्रेष्ठ. We were passing by the wall flanking the middle of the 3 lanes leading out of मलस्थान when were startled by the noise of someone closing in on us from behind on a fast ashva. With utter darkness, and not being able to see more than a couple of steps in front of us we feared it may be our dangerous shatru of the times K. The rider caught up with me and I was surprised to hear the familiar voice of ekanetra.\
Heaving a sigh of relief I asked him: "Why ambush me here, I was almost dead from fear."\
ekanetra: "I knew you would avoid crossing the bridge over the gomutraka at night and would take this route. This was my best chance to intercept you, before you were lost in the crowd of दक्षिण व्यायाम शाला".

ekanetra and me obtained some fried spinach भृज्जिका-s of the poor brahmin cook and went to the spot where the large ashvattha tree stood. The only noise was that of a cricket patiently trying to impress a mate. ekanetra lit the kerosene lamp oil with which he navigated his ashva in the dark and placed it down on the rim of the chaitya वृक्ष. He then pulled out a large sheet of paper printed using 123 or some such predecessor of MSExcel. On it were many boxes and numbers in the 1000's He had placed a mark next to some of these numbers. I eagerly checked certain boxes and found them to be blank. In one box there was strange mark. I asked ekanetra about that one. He said it was just what it normally meant. I was intrigued and even a bit puzzled by the numbers and the marks. He said that was the reason he had wished to bring them to my notice so urgently. I gave up all thought of astronomical observation despite the clarity of the sky and went home immersed in the thought of the spreadsheet of ekanetra.

The next evening ekanetra and others gave me a shout to call me to our usual game of musala-कन्दूक. I was still in the grip of his spreadsheet and excused myself from the game. ekanetra feeling something might be wrong came back to see me and suggested a solution to the apparent problem of the spreadsheet. But after some thought I realized it was too simplistic to be true. I asked R about it and she reminded me of the magnitude of those numbers, which gave me a solution. She also said that like any other pashu we would repeatedly do the same thing due to instinct. Then we got talking excitedly about G-protein signaling and soon the worries of ekanetra's sheet fell away.

*-*-*

During the [kuriltai](http://www.blogger.com/publish-confirmation.g?blogID=7010598&postID=110456498780989171&timestamp=1179553730856&javascriptEnabled=true) with our clansmen, certain issues were discussed and they were urging us for a great campaign on the then unfinished front. We launched a large-scale invasion but were frustrated by the खाण्डवन्स्. Our clansmen scattered completely repulsed by the खाण्डवन्'s. We were standing there, like droNa leaning on his bow brooding over the व्यूह being broken by arjuna, सात्यकी and भीमसेन on the afternoon of 14th day. Now cut off from our clansmen by the राक्षसि, we stood alone. We checked the number and suddenly realized the significance of the mark on the ekanetra's sheet.

*-*-*

I called R and told her that the magnitude of the numbers was great indeed, but given enough time the wind can grind a mountain to dust. R again said instinct will guide your action on this matter and asked me not to have expectations beyond the trends projected on ekanetra's sheet. We were now near a different वृक्ष. We were waiting for a few hours. The म्लेच्छ arrived. We treated the म्लेच्छ well as an Arya does to a guest as enjoined in the shruti. The म्लेच्छ displayed moderate intelligence but little inspiration. 3 rough-looking individuals, that included our friends and compatriots who had ridden out on many fierce campaigns, where arrows fell thick and hard, joined us. Looking at these warriors the म्लेच्छ got greatly frightened. We told the म्लेच्छ that men of the world fight real battles with rough भ्रातृव्य-s. As in कुरुक्षेत्र you kill or get killed.

*-*-*

We were executing the loops of instinct when suddenly matters became serious. The "flight of the crane" was seen again. The prayoga had been deployed on the 14th of this month. 14 days short of an year from the previous one. Against various prayoga-s the gods had led us to triumphs but this one was simply unstoppable. The sachiva, the अमात्य's brother all had been struck by this one.


