
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [veda mantrokta Ashu-garuDa विद्या](https://manasataramgini.wordpress.com/2007/08/19/veda-mantrokta-ashu-garuda-vidya/){rel="bookmark"} {#veda-mantrokta-ashu-garuda-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 19, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/19/veda-mantrokta-ashu-garuda-vidya/ "4:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RsfVxPXcXdI/AAAAAAAAAM8/7RfR51Fgnyw/s320/ashugaruda2.jpg){width="75%"}
```{=latex}
\end{center}
```



The terrifying आशुगरुड is a dinosaurine deity of the आकाश bhairava tradition that includes other dinosaurine deities like [पक्षिराज sharabha](http://manollasa.blogspot.com/2006/05/arimaspas-dinosaurs-griffins-and.html) and [आकाशभैरव](http://manollasa.blogspot.com/2006/12/oldest-archaeological-signs-of-ritual.html). One of the विद्या-s of this देवता involve the deployment of the ऋग्वेदिc mantras (RV 10.178) in a tantric context and this is known as the veda mantrokta Ashu-garuDA विद्या.

The साधक invokes आशुगरुड into the yantra with the mantra:

OM श्रीं svasti nas-त्राक्ष्यो अरिष्टनेम्यों ||

Then he performs homa with the following mantras:\
The ऋषि is शङ्कर, the Chandas is त्रिष्टुभ्, the devata is ashu तार्क्ष्य.\
[त्यमू Shu वाजिनं deva जूतं सहावानं तरुतारं रथानाम् |]{style="font-style:italic;"} yaM hUM\
[ariShTanemiM पृतनाजम् आशुं svastaye तार्क्ष्यम् इहा huvema ||]{style="font-style:italic;"}\
OM श्रीं गरुडाय महागरुडाय समस्ताण्डाण्ड-त्रैलोकनायकाय nAga षोणित दिग्धाङ्गाय स्वाहा ||

[indrasyeva रातिम् आजोहुवानाः svastaye नावम् ivA ruhema | ]{style="font-style:italic;"}yaM hUM\
[urvI na पृथ्वी bahule गभीरे mA वाम् etau mA paretau रिषाम]{style="font-style:italic;"} ||\
OM श्रीं गरुडाय महागरुडाय समस्ताण्डाण्ड-त्रैलोकनायकाय nAga षोणित दिग्धाङ्गाय स्वाहा ||

[sadyashchid yaH शवसा पञ्च कृष्टीः sUrya iva ज्योतिषा आपस्ततान |]{style="font-style:italic;"} yaM hUM\
[sahasrasAH शतसा asya रंहिर् नस्मा varante युवतिं na शर्याम् ||]{style="font-style:italic;"}\
OM श्रीं गरुडाय महागरुडाय समस्ताण्डाण्ड-त्रैलोकनायकाय nAga षोणित दिग्धाङ्गाय स्वाहा ||

Ashu-garuDa is meditated as having a reddish complexion, with a yellowish face, with a crest with black plumes on the head, reddish brown eyes, with two horns above the eyes and having sharp talons and fangs. He is seen as displaying great fury and pervading the universe with a dreadful blaze emanating from his body like that corresponding to the end of the kalpa.

The original deity Ashu-garuDa seems to have had his origins in the ancient garuDa tantra-s of the पूर्व srotas and was absorbed by the siddhAnta tantra-s of the Urdhva srotas in the दधीचि saMhitA along with sharabha and आकाश bhairava.


