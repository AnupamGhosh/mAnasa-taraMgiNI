
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [skanda प्रीणनं of the सामवेदिc tradition](https://manasataramgini.wordpress.com/2007/06/03/skanda-prinanam-of-the-samavedic-tradition/){rel="bookmark"} {#skanda-परणन-of-the-समवदc-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 3, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/03/skanda-prinanam-of-the-samavedic-tradition/ "10:27 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier see how the ऋग्वेदिc hymns have been [adapted for the worship of कुमार](https://manasataramgini.wordpress.com/2006/10/18/vedic-mantras-adapted-for-kaumara-ritual/) as specified in the Kashmirian कौमार ritual of the kaTha yajurvedin-s. The [देवी-भागवतं states that the worship of षष्ठि](https://manasataramgini.wordpress.com/2006/01/28/the-vrata-of-our-dear-goddess/) is connected with the kauthuma school of the सामवेद. When one digs into the roots of this tradition one arrives at the सामविधान ब्राह्मण, a peculiar विधान work of the सामवेदिन्स् which is termed a ब्राह्मण. It provides a series of गान-s termed the skandasya saMhitA, which are to be used in the pacification or worship of कुमार. The association of skanda with the late सामवेदिc tradition is not unprecedented given that he is mentioned in the last section of the चान्दोग्य उपनिषत् as the teacher of नारद. नारद is also a primary teacher of the सामविधान ब्राह्मण lineage.

The skandasya saMhitA consists of 11 गान-s all drawn from the ग्रामगेय गान-s:\
1-3 (GG246) -- the अग्नेर्वंरानि\
These 3 songs are composed on the following ऋक् of विश्वामित्र:\
[आ मन्द्रैर् इन्द्र हरिभिर् याहि मयूर-रोमभिः ।]{style="color:#0000ff;font-style:italic;"}\
[मा त्वा केचिन् नि यमन् विं न पाशिनोऽति धन्वेव तानिहि ॥ (ऱ्‌V ३।४५।१)]{style="color:#0000ff;font-style:italic;"}\
come here indra, with your golden horses, exhilarated, with tails like a peacock's feathers. Let one stop you course like noose-wielders trap the bird, pass over them as though going the coast.\
This mantra appears to have been chosen because of the term "मयूर-रोमभिः" which reminds one of the peacock वाहन of skanda.

4-6 (GG269) -the शाक्राणि\
These 3 songs are composed on the ऋक् of the काण्व-s:\
[आ नो विश्वासु हव्य इन्द्रः समत्सु भूषतु ।]{style="font-style:italic;color:#0000ff;"}\
[उप ब्रह्माणि सवनानि वृत्रहा परमज्या ऋचीषमः ॥ (ऱ्‌V८।९०।१)]{style="font-style:italic;color:#0000ff;"}\
May indra, who in battles must be invoked, be on our side;\
may the killer of वृत्र, the wielder of greatest power, praised by ऋक्स्, come to the soma pressing and chanting of mantras.\
This mantra appears to have been chosen because of reference to battle- कुमार being the war-god.

7-9 (GG533) -- the kutsasya-अधीरथीया\
These songs are composed on the ऋक्:\
[प्र सेनानीः शूरो अग्रे रथानां गव्यन्नेति हर्षते अस्य सेना ।]{style="font-style:italic;color:#0000ff;"}\
[भद्रान् कृण्वन्-निन्द्रहवान् सखिभ्य आ सोमो वस्त्रा रभसानि दत्ते ॥ (ऱ्‌V९।९६।१)]{style="font-style:italic;color:#0000ff;"}\
The hero proceeds in the forefront of the chariots, the commander of the army, he captures wealth as his army rejoices. soma dyes his robes with good colors, and makes his friends' invocation of indra successful.\
This mantra appears to have been chosen because of the term सेनानी. कुमार is called सेनानी and is the leader of the army of the gods. This is the most appropriate of the hymns for this particular context.

10,11 (GG565) -- the arka पुष्प\
These two, also deployed in the pravargya rite are composed on:\
[पवित्रं ते विततं ब्रह्मणस्पते प्रभुर् गात्राणि पर्येषि विश्वतः ।]{style="font-style:italic;color:#0000ff;"}\
[अतप्त तनूर् न तद् आमो अश्नुते शृतास इद् वहन्तस्तत् समाशत ॥ (ऱ्‌V९।८३।१)]{style="font-style:italic;color:#0000ff;"}\
Your soma-filter is spread out, O brahmaNaspati, as the lord you enter its limbs from all over.\
The raw, unheated mass does not attain this \[filter]; only the well-cooked mass, which bears \[the active soma], attains it.

Originally this mantra refers to the process of the boiled soma being filtered for the juice. However, it might have been adapted for kaumara worship because of the word "ब्रह्मणस्पते". After all कुमार is ब्रह्मण्य deva.


