
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The confluence of four paths](https://manasataramgini.wordpress.com/2007/12/06/the-confluence-of-four-paths/){rel="bookmark"} {#the-confluence-of-four-paths .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 6, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/12/06/the-confluence-of-four-paths/ "5:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vaiShNava during his perambulations in Central Asia had noted two texts with collection of Mongol "mantra-s":

 1.  iruegel maghataghal : a collection of ritual formulae and hymns.

 2. Altan erike by Arya paNDita who lived among the Khalkha branch of the Mongols.

In course of their study he uncovered a deity named guejir kuengker tengri, whom he brought to my attention. This deity was described as the deity who provides fodder for herds and also food for men. The Mongolian scholars Heissig, Poppe ("Zum Feuerkultus bei den Mongolen" and "Mongolische Volksdichtung mit deutscher Ubersetzung") and Partanen ("A description of Buriat Shamanism") have all noted this deity appear also under the name maqa galan Darqan Guejir tengri as the special deity of horned cattle. A remarkable hymn composed to the deity has survived in the Mongol ritual collection:

[ङ्रेअत् मक़ा गालन् डर्क़न् ङुएजिर् तेन्ग्रि,]{style="color:#0000ff;"}\
[ *Eldest brother of the ninety-nine tengri,*]{style="color:#0000ff;"}\
[ *Who arose through the blessing of Qormusta Tengri,*]{style="color:#0000ff;"}\
[ *At the command of the holy teacher Buddha,*]{style="color:#0000ff;"}\
[ *You who have a glorious cast-iron shield,*]{style="color:#0000ff;"}\
[ *you who have a beautiful colored throne,*]{style="color:#0000ff;"}\
[ *you who have a great golden hammer,*]{style="color:#0000ff;"}\
[ *you who have a great silver anvil,*]{style="color:#0000ff;"}\
[यॊु मक़ गलन दरक़न गुॆजिर तॆनगरि, ि वॊरसहिप,]{style="color:#0000ff;"}\
[ *Deign to come here on your blue-grey horse,*]{style="color:#0000ff;"}\
[ *you who determines all things,*]{style="color:#0000ff;"}\
[ *Deign to come here on your black horse,*]{style="color:#0000ff;"}\
[ *For the sake of my child's life and spirit,*]{style="color:#0000ff;"}\
[ *For the sake of life and spirit of the four kinds of livestock,*]{style="color:#0000ff;"}\
[ *That live in the steppe, I ask you.*]{style="color:#0000ff;"}

What is remarkable here is that one sees 4 religious layers:

 1.  The Iranian Zarathushtrian cult of Ahura-Mazdha is the basis for the deity named Qormusta Tengri.

 2.  Darqan Guejir tengri himself is an archetypal Altaic deity of the steppes, a smith god, who wielded a hammer and an anvil. He appears to have been the patron deity of Jelme, one of the great generals of Chingiz Kha'khan

 3.  He is identified as मक़ा गालन् which is the Hindu element in the mix: महकाल, who as the archetypal rudra-pashupati is a bucolic lord.

 4.  There is the nAstIka element seen in the form of the holy teacher buddha who is placed in a supreme position under the Lamaistic influence.

As we have discussed [before](https://manasataramgini.wordpress.com/2005/10/23/the-fire-hymns-of-the-mongols/), the early interaction between the Iranian tribes on the steppes with the Mongols was critical for the rise of the latter. It was during this process the first amalgamation of the cult of Ahura Mazda and the Altaic religion took place. It probably began during the first great Hun Khaganate when its founder Motun Tegin was a hostage of the Iranian tribe of the Kushanas. Subsequently the Hepthalite Huns were a part of the confederation of second great Hun Khaganate of the Juan-Juan. One Hepthalite Hun coin from Afghanistan shows a fire altar with Ahura Mazdha. These two Hun Khaghanates were recognized as the precursors of the Chingizid Mongols, suggesting that the Iranian influence of the Ahura Mazda cult was an early element.


