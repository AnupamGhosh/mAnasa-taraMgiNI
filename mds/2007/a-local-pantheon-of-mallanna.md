
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A local pantheon of मल्लण्ण](https://manasataramgini.wordpress.com/2007/06/24/a-local-pantheon-of-mallanna/){rel="bookmark"} {#a-local-pantheon-of-मललणण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 24, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/24/a-local-pantheon-of-mallanna/ "5:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A train of thought culminated in me remembering the peculiar case of the equestrian deity मल्लण्ण found in Ailoni. There is a smaller temple of मल्लण्ण in that village. Here he is depicted with his two wives with four arms hold a sword, trident, skull bowl and Damaru. His first wife is माडलदेवी who is supposed to be उमा incarnated amongst the लिन्गायत-s or gone कापु-s. She is depicted with a crown and two hands with a sword and skull bowl. She is the Andhra ortholog of म्हाळ्सा. His second wife is केतम्मा who is believed to be गङ्गा incarnated as a golla, and is the ortholog of बानू. She is similarly armed as माडलदेवी but has an elaborate hair-do instead of a crown. Also shown are 3 heads of Meccan demons or म्लेच्छ-तुरुष्क-s or yavana-kula daitya-s who where beheaded by मल्लण्ण.

He has a peculiar entourage of deities around him. He has a herd of sheep that he obtained from his wife केतम्मा. He has an army of 7 (or 7 times 100) dogs (vedic: shvabhya shvapatibhyashcha vo नमः; also compare with 7 fold troops of maruts). His brother is बयण्ण (a fierce bhairava; bhairoba in the महाराट्ट country). There is a goddess पोशम्मा who may have many arms and is like शाकम्भरी. There is then agni and वायु. Then there is वासुकी or nAga-सर्पं. मल्लणा is supposed to make 13 cups of sheep milk for the 7 dogs, बयण्ण, पोशम्मा, वासुकी, agni, वायु and himself. In the temple following this a ritual is performed where the cups of leaf are made and milk offered the deities. Here, as in some forms of the खण्डोबा myth from महाराष्ट्र, मल्लण्ण is said to have incarnated to slay Meccan demons or green-robed demons who had seized the land in the age of kali and were tormenting the Hindus. maNi and malla may be called सुरत्राण-s who ruled from Bidar and some other place.


