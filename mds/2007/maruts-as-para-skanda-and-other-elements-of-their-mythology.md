
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [maruts as \"para-skanda\" and other elements of their mythology](https://manasataramgini.wordpress.com/2007/07/25/maruts-as-para-skanda-and-other-elements-of-their-mythology/){rel="bookmark"} {#maruts-as-para-skanda-and-other-elements-of-their-mythology .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 25, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/25/maruts-as-para-skanda-and-other-elements-of-their-mythology/ "5:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many deities undergo drastic changes when they move across the veda-पुराण transition: Some lose status: e.g. indra; others lose status and change character e.g. वरुण; others rise to exalted heights e.g. विष्णु and rudra. But their mythology is still rooted in large part in their vedic antecedents. The marut-s are one of the most important deities of the veda-s, praised by all clans of ऋषि-s in various hymns and rituals. But the maruts not only fade away greatly, they but also undergo a drastic shift in mythology. In the पुराण-s the marut-s are minor deities and are actually called sons of the mother demoness diti, whose fetus indra cut up into seven pieces. Perhaps, this is a misinterpretation of the vedic term सुदानव (good-giver) as su-दानव (the good demon). The maruts are reduced to mere winds in classical Sanskrit, often used in singular as a synonym of वायु, a very different vedic deity. This synonymy is seen right from the रामायण, where hanumAn, while inheriting many features of the vedic maruts, is described as the son of वायु/marut. As result of this, the maruts are thoroughly misunderstood by the common Hindu and their true nature remains shrouded. One point, while apparently rather obvious, is quite completely ignored by most students of the veda and mythology as far as I can see (The learned vaishya Prithvi Kumar Agrawala being a possible exception): this is the relationship of marut-s with skanda. Hence, I have decided to briefly outline the obvious:


 1.  skanda and maruts are sons of rudra\
skanda is also termed the son of agni. rudra and agni are closely associated in the veda-- if two distinct deities are ever strongly identified with each other in the veda, agni and rudra form that pair. In at least 2 mantras agni is said to have fathered the maruts: RV 1.71.8 and 6.3.8. In the shatapata ब्राह्मण one of the manifestation of agni as rudra is termed कुमार (an epithet of skanda).

 2.  skanda's mother is a river (गङ्गा) and the maruts are described as having a river for their mother (e.g. "sindhu-मातरः" RV 10.78.6).

 3.  skanda is described as being born as six distinct but identical children and united to form one body with six heads. The maruts are described as being all of the same age and identical to each other. In different mantras in the RV and AV they are described a many individuals united together (RV 8.20.1/21, AV13.4.8). skanda also preserves a degree of his multiplicity by existing with his identically formed brothers shAkha, विशाख and nejamesha.

 4.  The youthfulness of skanda is emphasized: he described as being a playful child or a youth (bAla or कुमार). The maruts are also described as young playful children (shishu in RV 10.78.6; 7.56.16). Incidentally the term shishu is used in one of the earliest versions of the kumara story to describe an emanation of skanda (MBH 3.227 in Vulgate).

 5.  The primary weapon of skanda is the spear (shakti is the preferred epic term) and he is also depicted with a vajra. The primary weapon of the maruts is the spear (the preferred vedic term is ऋष्टि) and they also use the lighting (vidyut) or vajra. Both skanda and the maruts are also occasionally depicted as archers.

 6.  Both skanda and the maruts are war gods par excellence. skanda is the victorious commander of the deva army, and the lord of shardhas, पार्षदा-s or गणस्. The maruts are the one set of deities most commonly termed as गण-s/shardha, and in the veda are the constituents as well as leaders of the deva सेना, leading it to a demolishing victory (RV 10.103.8 "देवसेनानाम् abhi-भञ्जतीनाम् ज्áयन्तीनाम् marúto yantu ágram").

 7.  skanda shatters the क्रौञ्च mountain with his spear and splinters the peaks of the shveta parvata by striking it with his deadly mace (MBH 3.224 in Vulgate). The maruts are frequently described as shattering mountains in their fury (RV 1.85.10 "chid-bibidhur vi पर्वतं" or RV 5.52.9 "ádrim bhindanti ójasA")

 8.  skanda is described as born atop a mountain and seated atop a hill shortly after his birth (e.g. MBH 3.224 in Vulgate). The marut-s are also described as born on a mountain ("गिरिजाः" RV 5.87.1) and dwelling atop a mountain ("मारुतं गणM गिरिष्टां" RV 8.94.12)

 9.  The maruts as archetypal "storm gods" are associated with roaring noises of thunder --- their roars are said to terrify the denizens of the worlds. They are also described as making loud noises by blowing on organs termed वाण-s and भृमि-s ("dhamanto वाणं" RV 1.85. 10, "भृमिम् dhamanto" RV 2.34.1) . In the earliest version of the कौमार myth a comparable aspect of skanda is described. He is mentioned as making a loud noise like a mass of thunder clouds, and repeatedly roaring and yelling terrifying war cries that frightened everyone. He is also mentioned blowing into a conch and making a frightful noise (MBH 3.224 in Vulgate).

 10. The marut-s brightness is repeatedly mentioned and they are compared with agni in multiple places, and also said to be bright as the sun (स्वभानवः, RV 1.37.2). They are also described as being bright like the lightning (ahi-भानवः: snake-light=lightning or vidyut RV 1.172.1, 5.54.2). vidyut in the RV is mainly used as an epithet of the marut-s. This aspect is particularly highlighted in the early version of the skanda cycle where he is described as being bright as the sun, and agni (MBH 3.226, also 9.46). Specifically, he is also described as being like flashes of lightning from a mass of ruddy clouds (MBH3.224).

 11.  skanda is closely associated with indra, and is even described as his son-in-law marrying his daugther deva-सेना, and skanda calls himself a servant of indra in the earliest version of the kaumara myth (MBH 3.228). In the veda, maruts are the closest companions of indra.

 12.  कुमार can be roguish (hence called धूर्त) and cause harm and disease with his agents. The maruts likewise can cause harm (RV 1.39.8, 1.171.1, 7.58.5) and are invoked to keep harm away.

 13.  The relatively infrequent verbal root skand, from which skanda is derived, the RV occurs in connection with the marut-s as "skandanti" (RV 5.52.3).

 14.  The marut-s are described as having a common wife, the goddess रोदसी of the veda. In the earliest kaumara rituals associated with the atharvanic tradition, skanda is described as having a common wife with his brothers.

The final, very significant element of this connection comes from a conversation between rudra and skanda in the oldest version of the कौमार myth (MBH 3.230 in Vulgate):\
[अथाब्रवीन् महासेनं महादेवो बृहद् वचः ।]{style="color:#99cc00;"} [सप्तमं मारुत स्कन्धं]{style="font-weight:bold;color:#ff0000;"} [रक्षनित्यम्-अतन्द्रितः ॥]{style="color:#99cc00;"}\
And then महादेव said these weighty words to महासेन: "You must watchfully command the seven-fold troops of maruts."

[स्कन्दोवाछ:]{style="color:#99cc00;"} [सप्तमं मारुत स्कन्धं]{style="color:#ff0000;font-weight:bold;"} [पालयिष्याम्य्-अहं प्रभो। यद्-अन्यद्-अपि मे कार्यम् देव तद्-वद माचिरं ॥]{style="color:#99cc00;"}\
skanda replied, "Very well, my lord! I shall command the seven-fold marut troops. Now tell me quickly if there is any other task of the devas to be done."

Hence, at least in the earliest कौमार cycle, कार्त्तिकेय was specifically seen as the leader of the seven-fold marut troops (7 fold as described in the RV: sapta me sapta शाकिन; RV 5.52.17).

Thus, we see that there are many specific similarities between the later deity skanda and the maruts suggesting an evolutionary relationship between the two. However, unlike brahmA and prajapati, the marut-skanda connection is not ancestor-descendent. They appear to exist as paralogs, with the marut-s under going degeneration and a functional shift. How do we explain this?\
To answer this we take a detour:\
If we go farther afield in the Indo-European world we encounter two skanda-like deities: Ares of the Greeks and Mars (\*Martis as in Dio Marti -- god Mars) of the Romans. skanda and these deities share some iconographic similarity: depicted as vigorous youths and holding a spear in hand. One could object that these are generic features of war gods, but they do have deeper connections between them. As per the tradition of the आश्वलायन-s the graha-s are associated with pratyadhi-देवता-s and the pratyadhi-देवता for मण्गल (Mars) is skanda. Further, the shiva पुराण preserves a tale that occurs just before the कुमार संभव section that describes the birth of मण्गल (Mars) as a son of shiva (SP 3.10, rudreshvara saMhitA, पार्वति काण्ड). This myth appears to be a paralogous version of the कुमार cycle --- Mars is born from drops of shiva's tear/sweat falling to the earth and his iconographic characteristics are described as similar to those of skanda. While there is a prevalent view that the word Mars had an Etruscan origin, this is erroneous because of the homology of the deities Ares and Mars (the Greeks had no Etruscan contact). In this regard the early Indologists Kühn and Müller were correct in astutely observing that Martis, Ares (0-grade) and marut are likely to be derived from the same ancestral Indo-European root. Thus, these deities were derived from a common ancestral deity present even in the early Indo-European period.

We note that skanda preserves some primitive features with Martis and Ares the cognates from other other IE cultures, but has specific similarities with the vedic maruts. Hence, we argue that skanda did not directly descend from the maruts in the late vedic period, but was a homolog of the maruts in a para-vedic culture. This para-vedic culture merged with the Hindu mainstream in the late vedic period, and thus brought in skanda as a paralog of the maruts in the Hindu world. We also suspect that in this para-vedic culture the maruts were mainly winds and this induced the trivialization of the vedic maruts in the epic/पौराणिc period. Who were these para-vedic people? Some Indologists of perverse understanding might push them to the historical Kushana-s. This view is incorrect, for skanda's presence definitely preceded the coming of Kushana-s among the Indians as suggested by numerous early texts. Instead, we suspect that this people were associated with the late vedic period, and probably corresponded to the "पाण्डव-s" of the bhArata who inserted themselves into kuru-पञ्चाल realm.


