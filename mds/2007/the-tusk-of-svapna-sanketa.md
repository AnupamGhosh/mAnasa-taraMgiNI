
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The tusk of svapna-सङ्केता](https://manasataramgini.wordpress.com/2007/05/13/the-tusk-of-svapna-sanketa/){rel="bookmark"} {#the-tusk-of-svapna-सङकत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 13, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/13/the-tusk-of-svapna-sanketa/ "6:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After the third vIra had broken forth the vairi-s turned their attention towards the 4 other distant vIra-s. Two of them were protected by the spells of the तैत्तिरीयक, the bull amongst the कुण्डिनस्. The राहु which seized us like a graha of कुमार or a विनायक seizing a victim will be described when we emerge on the other side of the ग्रहण. There was a vague intelligence we received that the अमात्य was the principal target. It was not as clear as the dreaded कृत्या that we saw last year in the middle of our day like a शूर्पनख in the दण्डक-- verily like a pill of venom coated in honey. But it was like the मारण that took us closest to vaivasvata, before we were literally hauled out of the raging waters by the यक्षिणी नीललोहितमेखला of indescribable beauty with the strength of 8000 elephants. We did not know when the अमात्य would be struck or even if the अमात्य was really the target.

The kavi among the भार्गव-s however was aided most remarkably by the highest prayoga of सूकरानना of the dreams. As a result he clearly saw the dreaded chaturashra yantra with साध्य curled under the mAyA bIja in the middle, much like the vighna yantra of भण्डासुर. With this clear intelligence available, he saw that the अमात्य was the target. He then saw exactly what अभिचार had been deployed: the unexpected दिनास्त्र and it was advancing rapidly. Before he could do anything the अमात्य was struck. He almost simultaneous retaliated with the first counter-prayoga. But he saw this being repulsed by the दिनास्त्र, though it gave enough time for the अमात्य to hold on to life. Keeping his presence of mind he retaliated almost immediately with the combination of nabho-nilaya-bhairava and surA-देवी and महिषारुढ-वाराही. This destroyed the मारण prayoga and saved the अमात्य-s life. Then the ओषधि-s of अथर्वण-भद्रकालि were deployed to counter the lasting effects of the अभिचार. The bhargava was able to pull this off beyond all his expectations only because of the anugraha of the tusked one.


