
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The greatest temple of shiva](https://manasataramgini.wordpress.com/2007/02/15/the-greatest-temple-of-shiva/){rel="bookmark"} {#the-greatest-temple-of-shiva .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 15, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/02/15/the-greatest-temple-of-shiva/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RdQNa-8rbvI/AAAAAAAAAEA/c-oohFLsEEw/s320/elephanta_03_0001.jpg){width="75%"}
```{=latex}
\end{center}
```



We converged on this issue via a peculiar way --We were blessed after many years to enjoy the sUpa-कला of ST. For our tongues, which are dead as that of the prince of श्री-lanka in the Indian pleasure house, it was a like water for a man in the desert (of course we were reminded of the same simile in the ऋवेदिc mantra RV10.4.1). In course of enjoying her superlative efforts, I was educating ST on the vagaries of Hindu history, when she brought to my attention a verse by the great Octavio Paz, the ex-ambassador of Mexico:

[Down at the foot of the sublime sculptures,]{style="color:#ff9966;font-style:italic;"}\
[Disfigured by the Mohammedans and the Portuguese,]{style="color:#ff9966;font-style:italic;"}\
[The multitude has left its litter,]{style="color:#ff9966;font-style:italic;"}\
[Tiffin for the crows and the curs.]{style="color:#ff9966;font-style:italic;"}\
[I damn it to a thousand reincarnations,]{style="color:#ff9966;font-style:italic;"}\
[Each on a muck-heap]{style="color:#ff9966;font-style:italic;"}\
[ While those others,]{style="color:#ff9966;font-style:italic;"}\
[Can be carved, in living flesh, for aeons,]{style="color:#ff9966;font-style:italic;"}\
[[In the hell for defacers of statues.]{style="color:#ff9966;"}]{style="color:#ffcc00;font-style:italic;"}

  - Sunday on Elephanta

ST: "One of the greatest works of art of ours but when was it made and who made it ?"

To me it was greatest temple of the terrible rudra. Unfortunately it lies vandalized by Christian terrorists filled with brains rotting from the Abrahamistic mental disease. Truly the words of the venerable Celsus comes to mind: '...Christians are like a synod of worms on a dunghill, croaking and squeaking, "For our sakes was the world created."'

In this regard an example of the stinking Christian intra-cranial decay is vividly illustrated by the comment of the Portuguese vandal Antonio Bocarro: "There is also a large and deep tank of water without which the heathens of the East never build their pagodas; because among their other abominations they believe that water purifies and cleanses them" [!]

To me the greatness of the temple of श्री-पूरी mainly lies in the fact that it is one of the few temples that is literally a पुराण in rock. It helps us reconstruct the history of the पाशुपतस्, and predecessors of the कालामुख-s of later days. To me on seeing the glory of shiva on rock the words came to mind:

[दंष्ट्रा-करालं दिवि नृत्यमानं हुताश-वक्त्रं ज्वलनार्क-रूपं ।]{style="color:#99cc00;"}\
[सहस्र-पादाक्षि-शिरोभि युक्तं भवन्तम्-एकं प्रणमामि रुद्रं ॥]{style="color:#99cc00;"}\
[नमो।अस्तु सोमाय सुमध्यमाय नमो।अस्तु देवाय हिरण्यबाहो ।]{style="color:#99cc00;"}\
[नमोऽग्नि-चन्द्रार्क-विलोचनाय नमोऽम्बिकायाः पतये मृडाय ॥]{style="color:#99cc00;"}

From the evidence of the copper coins of कृष्ण-rAja that appear to have been used as a payment for the workers on the island of श्री-पूरी it seems the cave-temple of shiva was excavated during the reign of the early kalachuri-s a branch of the haihaya clan between 525 to 575 of CE. The chief sponsors seem to have been the kings कृष्ण-rAja and शङ्कर-गण. The main idol other than the गर्भागृह which houses the लिङ्ग is 3-headed maheshvara मूर्ति. This triadic representation is suggestive of the लाकुलीश पाशुपतस् because they clearly emphasize the triadic nature of rudra: the पाशुपत सूत्रस् state that rudra manifests as ऋषि, vipra and mahat. It also provides another triad as which rudra manifests: वामदेव, ज्येष्ठ and rudra. वामदेव generates various कार्य-s; ज्येष्ठ is the supreme being or the sole existence; rudra is one who unites the universe with destruction. It is precisely this triad of the ancient para-vedic पाशुपत sUtra-s that is represented by the splendid mahesha-मूर्ति in the central southern niche of श्री-पूरि caves. The important point to note here is that even though the पाशुपतस् use the pentadic mantras of सद्योजात etc their shiva is essentially triadic. It is in the mantra-मार्ग that the पञ्च-brahma is the central theme, especially as outlined in the siddhAnta thought. This connection is further strengthened by the presence of an image of the holy लकुलीश, the founder of the of the पाशुपत lineage, in the north-eastern-most niche of the temple. This shows that the early kalachuris were patrons of the लाकुलीश lineage of पाशुपत-s rather than classical mantra मार्ग shaivas. It was only their late descendants after the 800s who made the shift to the mantra-मार्ग of the matta-मयूर branch of siddhAnta shaivas.

We have been able to piece together some of the history of the श्री-पूरी \[Elephanta] Island that has some bearing on the spread of the पाशुपत sect of the shaiva-s, their cultural contributions and the parallel rise of the mantra-मार्ग shaivas, especially the siddhAnta-shaivas.

The early kalachuri dynasty came to a dismal end in the first half of the 600s of CE after they faced a dual attack from the rising चालुक्य-s and remnants of the old maurya-s that had established themselves in Konkans (their descendant chandra-rao more of Javali in the Konkans was killed by Shivaji to finally end the dynasty). The mauryan ruler in a naval campaign took over the island of श्री-पूरी from from the kalachuris during the reign of बुद्धराज, even as the chalukya general मङ्गलेश invaded the northern realm of the kalachuri-s and conquered it. However, the maurya-s could not hold on to the island as the next चालुक्य king, the mighty pulakeshi II defeated the maurya-s by griping them from both sides via a naval and land attack. The Aihole inscription poetically describes this \[translation from Epigraphica Indica vol 6 modified by me based on the original]:\
[["In the Konkanas the impetuous waves of the forces lead by \[pulakeshi] swiftly swept away the rising wavelets of pools- the mauryas. When radiant like the destroyer of tripura he besieged पूरी, the श्री of the western sea, with hundreds of ships in appearance like arrays of rutting elephants, the sky, dark-blue as a young blue-water lily and covered with rows of massive clouds, resembled the sea, and the sea was like the sky."]{style="color:#ff9900;"}]{style="color:#33ff33;font-style:italic;"}

Before the mid-750s of CE the mauryas appeared to have reappeared to control श्री-पूरि and the surrounding Konkans perhaps as a nominal feudatory of the चालुक्य-s and then some what independently. Around the mid-700s a large Moslem attacking fleet overwhelmed the maurya defenses of the island of श्री पूरी and occupied it, but the chalukya king of Gujarat, पुलकेशिराज sent a naval expedition to destroy the Moslems. This is alluded to in an allegorical form in the late book of the shiva पुराण termed the कोटि-rudra saMhitA. Here a vaishya going to the island of श्री पूरी was captured by the राक्षसस् and imprisoned with other worshipers of shiva. These राक्षसस् are said to greatly harass the people and the vaishya invokes shiva who comes and gives him the पाशुपतास्त्र. Using this he kills the राक्षसस्. Then a king वीरसेन the son of महासेन is said to liberate the island after receiving weapons from shiva to kill the म्लेच्छस् in naval expedition.

The maurya-s appear to have held their ground in the island despite these historical tremors albeit as subordinates of more powerful rulers of the mainland. We have an inscription from 1069 CE that states that govinda-rAja the maurya ruler was a vassal of the यादव ruler seunachandra II and probably administered the island for the yadava overlord. It may have also been held by them for the Kalyani चालुक्य-s for a while there after. At some point after the conquest of Konkans by Mahmud Gavan and Adil Shah from the Vijayanagara rulers it seems to have been taken over by the Mohammedans who inflicted some damage to it. Subsequently it was taken from them by the Christian vandals who fired cannons on to the cave to demolish most sculptures. The early European visitors record that the icons were vividly painted and that there were several paintings in the caves like those at Ajantha. This suggest that it had survived in reasonable condition till then.

The dispersal of the kalachuri-s seem to have taken the पाशुपत-s in two directions : 1) northwards to West-central India, where the kalchuri-s appear to have survived for while till the great kalachuri king kokalla revived them in the 800s of CE. Another northern branch also established a strong hold in सर्यूपार in Gorakhpur. In course of their dispersal the पाशुपत-s probably moved with them and found new patrons in western India in the form of the guhilot राज्पुत्स्- the prince bappa रावल् (first half of the 700s of CE) was initially disciple of a लाकुलीश guru of the harita lineage. He subsequently became a disciple of the lineage of matsyendra and was initiated into the mantra-मार्ग. This dispersal along with that of the maurya-s to valabhi appears to have resulted in the spread of the पाशुपतस् throughout western India where they established many famous centers of pashupata learning like अणहिलपातक in Gujarat. 2) A second branch of the kalachuri-s dispersed to the south where they were initially vassals of the Kalyani चालुक्यस् in Karnataka. They eventually overthrew the चालुक्यस् and became rulers themselves and allowed the grand revival of the लाकुलीश पाशुपत sect in the form of the great कालामुखस्. This revival was further supported by further influx of fleeing पाशुपत-s from the north especially Kashmir and possibly अणहिलपातक during the Islamic depredations. The kalachuri reign in the south was eventually overthrown due to a degraded shaiva movement in the form the वीरशैव-s, whose rise was in part favored by the general shaiva fervor cultivated by the कालामुखस्.

The मन्त्रमार्ग shaiva-s appear to have initially existed symbiotically with the पाशुपत lineages as suggested by the subscription to a similar mythological base (tirumular's mythology) and मूल mantra collection (पञ्चाक्षरि and पञ्च-brahma) as well as association with shAkta temples (A पाशुपत inscription from the gupta era, 374 CE, shows that the पाशुपताचर्य lokodadhi established a shakti temple in madhya pradesh). However, the मन्त्रमार्ग-s developed their hallmark, the elaborate mantra-prayoga, which they extensively used to aid royal disciples in gaining power. This feature is also shared with the पाशुपताचर्य-s who deployed royal atharva vedic rites and performed a similar role in Mewar. Examples of associations in the rise of major dynasties are:\
पाशुपत-s: guhadatta (guhilot) dynasty which gave rise to the shishodia clan with luminaries like the mahArANa-s Bappa Raval, kumbha, Sanga and Pratap. Even till recently the Chittor ruling family visited the shiva temple with the image of लकुलीश and worships it every Monday.\
[mantra-मार्ग]{style="font-weight:bold;font-style:italic;"}\
दकिष्ण-srotas(bhairava tantras):A भैरवाचार्य performed tantric rites in a श्मशान explicitly for the establishment of the dynasty of पुष्पभूति that spawned the great king harshavardhana.\
[वाम-srotas (tumburu tantras)](http://manollasa.blogspot.com/2005/02/tumburu-manifestation-of-rudra.html): The ब्राह्मण हिरण्यदामन् performed the rites as per the वाम-srotas for the rise of jayavarman-II the founder of the Angkor empire centered in Cambodia.\
Urdhvasrotas (siddhAnta tantras): The various आचार्यस् of the Urdhvasrotas performed major rites for the kings of the shilahara, chola, राष्ट्रकूट, चालुक्य, परमार and भूपाल dynasties. Their rites have been implicated the rise of the परमार-s and the revival of the kalachuri-s and their highly aggressive king गाङ्गेय deva. गाङ्गेयदेव's Acharya of the Urdhvasrotas performed rites for him to liberate the temples of Kangra and retaliate against the Mohammedan attack on Varanasi by Ahmed Niyaltegin, the general of Mahmud Ghaznavi. When other Hindu kings were watching bewildered, गाङ्गेयदेव and his valiant son कर्ण single-handed attacked the Moslems in Kangra and drove them out.\
पूर्व-srotas (garuDa-tantras): prayoga-s performed by this were used by the malla rulers in Nepal in their ascent. These prayogas are comparable to those of पृथिवि-नार्यण of Nepal of a later era.

In cultural terms the spread of the पाशुपत-s appears to have played an important role in the preservation of classical Hindu theater- the नाट्य of bharata and ताण्ड्य \[These two appear to be depicted in the श्री-पूरी naTarAja panel]. Looking at the depictions in the श्री-पूरी caves we find the following : 1) लकुलीश 2) naTarAja, with उमा, विनायक, skanda, 3) अन्धकासुर vadha, 4) रावण shaking कैलास, 5) the marriage of पार्वती and rudra, 6) sporting of sati and rudra on कैलास, 7) अर्धनारीश्वर, 8) the taming of गङ्गा and 9) the mahesha-मूर्ती. There is also a separate set of icons of the 8 मातृका-s with गणpati and वीरभद्र flanking them. Other icons associated with shiva include indra, विष्णु, brahmA, भृङ्गिन् and nandin. These depictions are all indicative of the post-कालिदास period (rise of विनायक as major deity) and is consistent with the descriptions of the core "story sections" of the shiva पुराण (excluding the late book like कोटिरुद्र saMhitA). It is likely that the core shiva पुराण composed during the gupta period was comprised of the story sections corresponding to the rudra-saMhitA and the rudreshvara saMhitA-s of the modern shiva पुराणा, which were probably combined with ritual material on the पञ्चाक्षरी, लिङ्ग-स्थापन, bhasma-vidhi, रुद्राक्ष and deva-yajana. Based on the comparative analysis with other पुराणस्, the महाभारत and and vedic ब्राह्मणस् it is clear that there was an earlier body of shaiva tales that consisted of:

 1.  defeat of the ब्राह्मण-s of दारुकवन 2) origin of लिङ्ग 3) shiva's marriage with sati and destruction of दक्ष's sacrifice 4) the marriage of shiva and पार्वती. 5) birth of कुमार 6) destruction of tripura 7) killing of andhaka 8) killing of गजासुर 9) Drinking of the हालहल. Perhaps some additional material on jalandhara or शङ्खचुद was also present with this set. This set was probably present during the शुङ्ग times which was the template for कालिदस's work. It is not clear if they were a floating mass of stories distributed amongst various then current पुराणस् or already collected into a saMhitA. In any case this body served as the base for the core shiva-पुराण of gupta times during which विनायक was inserted after the ancient skanda tale.


