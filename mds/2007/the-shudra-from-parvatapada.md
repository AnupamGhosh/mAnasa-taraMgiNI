
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The शूद्र from पर्वतपाद](https://manasataramgini.wordpress.com/2007/10/12/the-shudra-from-parvatapada/){rel="bookmark"} {#the-शदर-from-परवतपद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 12, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/12/the-shudra-from-parvatapada/ "4:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the distant past we had little knowledge of the future that awaited us. We were surrounded by fierce warriors contending for fame, like दीर्घदन्त hrasvaroman, ब्रिन्दापुत्र, taskaraputra of evil nature, कृष्ण-शूद्र, shveta-dadhna, भृहत्भ्रू वैष्णवमत, the valiant ekanetra from the regions of पाषण्डाद्रि, jayadratha, and also that शूद्र warrior known as दीर्घशिरस् सूचीलोमन्. There were also lowly fighters like vidruma, क्षुद्र-शूद्र, ashvamukha, सूकरमुख, and नालिकेरानन who contended for the great glory of victory in battle, but were nowhere amongst the fiery yoddha-s. Then there were bewitching स्त्री-शिरोमणि-s distinguished themselves from the collyrium heap of durmukhi-s like the ornaments of the sharan-निशा . Amongst these were: तीक्ष्ण-त्रिपथिका, कौन्डिन्यकुलाङ्गना, सुचित्रा trailokyasaMcharI, vaishya-महिषि, नागकेशी durdanti, kamala-नयना चम्पकनसा, गुप्तसुन्दरी मिनलोचनी, तुरङ्गा श्यामत्वची and कन्दूक-नेत्री ( :) how many got into the composite ? ). Some puruSha-s bewitched by these स्त्री-s gave up their arms, and were lapped up in the great war by them like कृत्या-s sent by shiva. Having pierced that veil of time we found ourselves back in that frame of time. There was the excitement of the great yuddha where few stood alive at the end.

That valiant कृष्ण-शूद्र having taken a battering was fleeing from the field. The अमात्य asked us: why associate with such a poor शूद्र, when you have won riches in this great समराङ्गण? Why not exult with yonder yoddha-s of great wealth and status. But we did not leave our companion like युद्धिष्ठिर remaining with yama embodied as a dog. कलशजा in the great exultation of that victory said who is this black शूद्र, let him be and let us move on to celebrate at the nygrodha-prastha. The शूद्र's uncle told him : "O ANi-kesha I have a way out for you. Sincerely follow my vidhi and you will be happy 8 years from now. At that point many of these who laughed at you will be trudging their way to the great अङ्गण of avimukta. Give up all your attachments to women and pursue the path I have laid for you relentlessly. I will financially support you throughout the yuddha."

Retreating to परवतपाद, where he lived, much derided by his hecklers he applied himself to his uncle's plan.

Years later the wheel of fate had turned for many of the yoddha-s who were victorious in this महायुद्ध of Aditya-s (संध्य). One of them whom indra had assigned a स्थान in नेदीयस् was ST. She remarked seeing the mirthful life of the जायद्रथि: "Can anyone have a worse fate than mine."\
कलशजा said: "Surely it is better than that of the शूद्र whose totem was the khadira tree."\
I said: Oh ST of sweet smiles, we have gone the whole way and can assure you of that greater sorrows are wait if you chose that path you yearn for. But you will be in सुवर्गं लोकं if you were to swap places with the शूद्र from पर्वतपाद.\
The two, who were like celestial jewels lighting the morbid darkness of the shishira-निशा, said: "How could that ever be. Tell us what the real fate of कृष्ण-शूद्र is?"

To tell them of the fate of कृष्ण-शूद्र we went out of our dream. We thought of our ancient clansman, vipula the student of devasharman. Like उशना काव्य entering shiva we entered the म्लेच्छ hero. We became a साक्षि of the pleasures he was enjoying. We then exited him and entered Manganese, the third Hero. We became of साक्षि of his extraordinary insights for a while we experienced what it was to experience things our brain had never fathomed. Then we passed into सुजाता and like a bee to nectar she went straight to That One. Leaving her we entered That One. From us emanated the first one that was the first of the मानसिका-s. Then came the second one of the मानसिका. We let them loose so that they may watch pass the information in case we go trapped. We then entered रवीन्द्र on the banks of the गोमूत्रक-मूलक संगम. Residing in him we went to the temple of The god and saw his लिङ्ग. Then we passed being within रवीन्द्र to the temple of the awful विनायक who was sleeping in a grotto on the flank of the river bed. The terrible विनायक refused to see रवीन्द्र and we could not experience him. We wandered hiding thus for a while until रवीन्द्र was struck by the darts of rudra (hence they say rudrasya hetir परिवोवृणक्तु). Reeling under them filled with disease and distress he stumbled along near the the shrine of the विनायक. We tried to get out him but were trapped and unable to come out. Strangely we saw a second copy of ourselves freely moving along the banks of the मूत्रक and then suddenly flying away into the sky.

रवीन्द्र was then seized by a पिशाची who stuck to him like leech to its prey and refused to let go of him like a tiger its quarry. With this dread पिशाची sucking from him he wandered along a dusty road beyond the मूलक river in a hazy state until he arrived at an old धर्मशाल that had been built by a notable महरट्ट prime minister. There, he lay and dropped into a deep sleep until he awoke from dream where he felt he was eating metal wires. I from within experienced this dream of his and when he awoke from within him also experienced the rays of sunlight he saw streaming into the room. He then awoke and stumbled along tormented by the darts of The deva, like Apollo-shoot-afar showering his arrows on the Niobids. Thus wandering he arrived at the desk of the शूद्र whose totem was the khadira tree and asked him to save.


