
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The last Arya](https://manasataramgini.wordpress.com/2007/04/10/the-last-arya/){rel="bookmark"} {#the-last-arya .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 10, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/10/the-last-arya/ "4:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Through the corrupt gramophone recording we heard the voice of Kareik, the last devlole of the Kalash, reciting the ancient hymns of dishani, भगिष्ट्, sutarem, imra and mahandeu. These were recorded by Morgenstierne in 1929 and five years later the devalole was dead and with that the oral tradition of the Kalash was lost. Several decades earlier the Kalash had already faced the death blow from the Jihad of Abd-ar-Rahman, the murderous Ghazi from Afghanistan. Per say there was nothing new in the event --- Islam and the cult of Jeshua ben Yusuf had annihilated, and were to continue annihilating, numerous religions and cultures of the world. Yet, there was something in this that struck a chord in us. We heard some time back from both Sh and Hayasthanika that the पार्षु-s were reduced to a tribe and amongst them those maintaining the oral Avestan tradition were on their way out. These people were our Indo-Iranian sister groups, their religions and deities homologous of ours. Despite bearing head on blow from Islam they had survived this long due to the innate resilience that characterizes our ancestral culture. But the effects of time could not be overcome and they were on the path of extinction. We, last of this great superior tradition, were the next in line and are seen sliding down that path. The vedic शाखस् being denuded, simultaneously attacked by the two Abrahamisms, we seem like a pretty Hindu girl surrounded by unwashed, bearded Meccan ruffians.

Why have we reached this condition ? Some parasitoid wasps inject polydnaviruses into their host and with that they lose their ability to mount a immune response on the inserted wasp larva. This is what has happened to Hindu society because the Hindu immune surveillance has just failed to recognize the parasites. The prolix Balagandadhara of Ghent's researches provide data to visualize this problem.


