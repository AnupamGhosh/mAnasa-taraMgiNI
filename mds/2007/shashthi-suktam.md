
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [षष्ठी सूक्तं](https://manasataramgini.wordpress.com/2007/12/09/shashthi-suktam/){rel="bookmark"} {#षषठ-सकत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 9, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/12/09/shashthi-suktam/ "4:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

bANa's कादम्बरि mentions षष्ठी and skanda being painted on the walls of the birthing room of the heroine, and he also alludes to her in the हर्षचरित. This shows that the worship of षष्ठी was prevalent in North India at least till the first quarter of the 600s of the CE. A long history of षष्ठी worship can be established through much of northern Indian: For example both bauddha and jaina sources mention shrine of देवी षष्ठी being present at विशाल at the time of two nAstIka founders. The archaeological studies by NP Joshi ( Hindi essay: देवी षण्मुखी yA षष्ठी), PK Agrawala (mathura नागी=षष्ठी), VS Agrawala (षष्ठी-देवसेना on yaudheya coins) have recovered considerable iconographic material of षष्ठि from कुशान and yaudheya sites, especially the early shrines of the deity in mathura.

Today the worship of षष्ठी has faded away in most of India except in the वङ्ग country (and amongst knowers of the archaic कौमार path). Interestingly, even in the late medieval period the worship of षष्ठी remained vigorous in Bengal and surrounding areas (also probably Nepal) as suggested by the Bengali poem षष्ठी मङ्गल् composed in the mid-1600s of CE. Of course the tradition of षष्ठी worship is rather ancient. Attempts to trace it take one back to the late Vedic period, where she appears just like her mate skanda. Interestingly, the उपाख्यान of षष्ठी in the देवी भागवतं mentions that her rite is connected with the [kauthuma school of the सामवेद](http://manollasa.blogspot.com/2006/01/vrata-of-our-dear-goddess.html). The meaning of this allusion becomes clear when one searches for the earliest surviving traces of षष्ठी worship.

We first encounter षष्ठी in rites associated with the कृष्ण yajurvedic tradition of the तैत्तिरीयक-s. The आयुष्य सूक्त, which is deployed among other purposes, for the first year rite of a child has the formula:\
[श्रियं लक्ष्मीं-औपलांबिकां गां षष्ठीं च यां इन्द्रसेनेत्युदाहुः ।]{style="color:#0000ff;"}\
[तां विद्यां ब्रह्मयोनिग़्ं सरूपाम् इहायुषे तर्पयामो घृतेन ॥]{style="color:#0000ff;"}

With this formula श्री-लक्ष्मी, अम्बिका associated with the aniconic stone and the heavenly cow (as in rudra's wife; cf. the शूलगव ritual), षष्टी and इन्द्रसेना are invoked as the manifestations of the knowledge of the brahma, and for the sake of long life are pacified with offerings of ghee.

This old formula appears to have been a popular, as it was reused in at least two later tantric उपनिषद्-s to identify श्री the shakti of नृसिम्ह and विनायकी the shakti of gaNapati with all the primary goddess.

नृसिंह पूर्व तापनीय states:\
[स ईं पाहि य ऋजीषी तरुत्रः स श्रियं लक्ष्मीं-औपलांबिकां गां षष्ठीं च यां इन्द्रसेनेत्युदाहुः । तां विद्यां ब्रह्मयोनिं सरूपाम् इहायुषे शरणमहं प्रपद्ये ॥]{style="color:#0000ff;"}

varada-पूर्व-तापनी 2.2 (a tantric उपनिषद् of the गाणपत्य school describing the valagahan अनुष्टुभ् of गणेश):\
[स ईं पाहि य ऋजीषी तरुत्रः स श्रियं लक्ष्मीं-औपलां अंबिकां गां षष्ठीं च यां इन्द्रसेनेत्युत आहुस्-तां विद्यां ब्रह्मयोनि-स्वरूपां । तांइहायुषे शरणं प्रपद्ये ॥]{style="color:#0000ff;"}

The medieval उपनिषद्ic commentator नारायण explains that this formula equates the shakti of the primary deity of the tantric उपनिषद् with श्री-लक्ष्मी the shakti of नारायण, अंबिका the shakti of rudra, कामधेनु, षष्ठी the shakti of skanda and इन्द्रसेना, who is इन्द्राणी.

This transfunctional equation of the goddesses also plays out in the most definitive early source on the worship of षष्ठी from the कृष्ण yajurvedic tradition of the मैत्रायणीय-s. The मानव गृह्य sUtra 2.13, termed the षष्ठी kalpa records a rite performed over 6 shulka षष्ठी-s in 6 months or in 3 months on both षष्ठी-s. The rite involves a day of sexual abstinence and homa offering of a स्थालीपाक to षष्ठी with a hymn termed the षष्ठी सूक्त, followed by individual Ajya oblations.

The षष्ठी सूक्तं:

[धनदाꣳ वसुम् ईशानां कामदाꣳ सर्व-कामिनाम् ।]{style="color:#0000ff;"}\
[पुण्याꣳ यशस्विनीं देवीꣳ षष्ठीꣳ शक्र जुषस्व मे ॥]{style="color:#0000ff;"}

[नन्दी भूतिश्च लक्ष्मीश्चादित्या च यशस्विनी ।]{style="color:#0000ff;"}\
[सुमना वाक् च सिद्धिश्च षष्ठी मे दिशतां धनं ॥]{style="color:#0000ff;"}

[पुत्रान् पशून् धनं धाण्यं बह्वश्वाज-गवेडकं ।]{style="color:#0000ff;"}\
[मनसा यत्-प्रणीतं च तन्मे दिशतु हव्यभुक् ॥]{style="color:#0000ff;"}

[कामदाꣳ रजनीꣳ विश्वरूपाꣳ षष्ठीम् उपवर्ततु मे धनं ।]{style="color:#0000ff;"}\
[सा मे कामा कामपत्नी षष्ठी मे दिशतां धनं ॥]{style="color:#0000ff;"}

[आकृतिः प्रकृतिर्-वचनी धावनिः पद्म-चारिणी मन्मना भव स्वाहा ॥]{style="color:#0000ff;"}

[गन्ध-द्वारां दुरादर्षां नित्यपुष्टां करीषिणीं ।]{style="color:#0000ff;"}\
[ईश्वरीꣳ सर्व भूतानां तामिहोफवये श्रीयं ॥]{style="color:#0000ff;"}

[नाना-पत्रका सा देवी पुष्टिश्-चाति-सरस्वती ।]{style="color:#0000ff;"}\
[अरिं देवीं प्रपद्येम्-उपवर्तयतु मे धनं ॥]{style="color:#0000ff;"}

[हिरण्यप्राकारा देवि माꣳ वर आगच्छत्वायुर्-यशश्-च स्वाहा ॥]{style="color:#0000ff;"}

[अश्व-पूर्णाꣳ रथमध्याꣳ हस्तिनाद-प्रमोदिनीं ।]{style="color:#0000ff;"}\
[श्रियं देवीम् उपवह्ये श्रीर् मा देवी जुषतां ॥]{style="color:#0000ff;"}

[उपयन्तु मां देव-गणान्-नागाश्-च तपसा सह ।]{style="color:#0000ff;"}\
[प्रादुर्-भूतोऽस्मि राष्ट्रेऽस्मिन् श्रीः श्रद्धां [कीर्तीम् वृद्धिं] दधातु मे ॥]{style="color:#0000ff;"}

[श्रियै स्वाहा। ह्रियै स्वाहा। लक्ष्म्यै स्वाहा। उपलक्ष्म्यै स्वाहा। नन्दायै स्वाहा। हरिद्रायै स्वाहा। षष्ठ्यै स्वाहा। समृद्ध्यै स्वाहा। जयायै स्वाहा। कामायै स्वाहा॥]{style="color:#0000ff;"}

मानव गृह्य sUtra 2.13

The parallels between the षष्ठी सूक्तं and the श्री-सूक्तं from the khila of the RV or the बोधायन tradition of the तैत्तिरीयक-s cannot be missed. This transfunctional association between षष्ठी and श्री-लक्श्मी is explicitly stated in the स्कन्दोपाख्यान of मार्कण्डेय from the महाभारत:\
[एवम् स्कन्दस्य महिषीं देवसेनां विदुर्बुधाः ।]{style="color:#0000ff;"}\
[षष्ठीं यां ब्राह्मणाः प्राहुर् लक्ष्मीम् आशां सुखप्रदाम् ।]{style="color:#0000ff;"}\
[सिनीवालीं कुहूं चैव सद्वृत्तिम् अपराजिताम् ॥]{style="color:#0000ff;"} (Mbh 3.218.47 )

Here the queen of skanda देवसेना or षष्ठी is explicitly identified with लक्ष्मी, as well as as the other lunar goddesses such as [सिनीवाली and कुहू.](http://manollasa.blogspot.com/2006/10/ekanamsha-in-nastika-myth-making-and.html) Thus, the link with लक्ष्मी is not a mere innovation of the मानव GS of the मैत्रायणीय-s but appear to be a well-established facet of the goddess षष्ठी in that period. Another point to note is the presence of the name मनसा in the षष्ठी सूक्तं. This archaic element appears to have remarkably survived in Bengal, where a goddess मनसा is commonly associated with षष्ठी, and corresponds to the cobra-headed goddess associated with कुमार. This suggests that the देवी भागवतं's उपाख्यान of षष्ठी is recording the presence of a rite similar to that of the मैत्रायणीय-s of the कृष्ण yajur veda amongst the kauthuma-s. It would be of interest to see if any of the surviving गृह्य परिशिष्ठ of the kauthuma-s records such a tradition.


