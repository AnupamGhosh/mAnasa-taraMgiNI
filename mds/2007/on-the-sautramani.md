
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On the सौत्रामणी](https://manasataramgini.wordpress.com/2007/05/06/on-the-sautramani/){rel="bookmark"} {#on-the-सतरमण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 6, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/06/on-the-sautramani/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The celebrated सौत्रामणी rite, which is one of the rare shrauta rites in which oblations of [beer](https://manasataramgini.wordpress.com/2004/03/22/vedic-beer/) are offered is a conservative one that goes back to the early vedic period. The key oblations are readied thus are prepared thus (the production of the [vedic beer](https://manasataramgini.wordpress.com/2004/03/22/vedic-beer/) has already been described):\
The cups for the ashvins\
Material: ashvattha wood\
Flavoring: wheat flour and juice/powder of red large jujube fruit (kuvala)\
mantra: tejo asi.. . SYV vajasaneyi saMhitA 19.9\
The cup for the sarasvati\
Material:udumbara wood\
Flavoring: coriander powder and juice/powder of yellow green large jujube fruit (badara)\
mantra: वीर्यम् asi ... VS 19.9\
The cup for indra\
material: nygrodha wood\
mantra: balam asi ... VS 19.9\
Flavoring: Barley powder and juice/powder of small red jujube (karkandhu).\
Powder of wolf, tiger and lion hair are respectively added to the 3 sets of cups with the mantra ojo asi (VS19.9)\
The total set of cups are 2 milk cups for ashvins; 2 beer cups for ashvins; 1 milk cup for sarasvati; 1 beer cup for sarasvati; 1 milk cup for indra; 1 beer cup for indra. The beer cups are filled by placing them on earthen स्थालि saucepans.\
These are for offerings in the आहवनीय. The adhvaryu then takes the 100-holed pitcher with a kusha grass filter and a gold ornament and offers the remaining beer into the दक्षिणाग्नि with the mantras punantu mA (VS 19.37-44). In the charaka-सौत्रामणि it offered in the southern आहवनीय (without the raised altar) typical of this rite. In this case the pitcher may alternatively be 9-holed.

A variant of the सौत्रमणि is performed within the राजसूय, the charaka सौत्रामणि as instituted by the कृष्ण-yajurvedic charakas.

The structure of the सौत्रामणि ritual closely follows its oldest mention in the indra सुत्रामन् सूक्तं (RV 10.131):\
[युवं सुरामम् अश्विना नमुचावासुरे सचा ।]{style="color:#99cc00;"}\
[विपिपानाशुभस् पती इन्द्रं कर्मस्वावतम् ॥]{style="color:#99cc00;"}\
Young ashvins, auspicious lords, drank swigs of good beer, and refreshed indra in his work with the Asuric namuci.

[पुत्रम् इव पितराव्-अश्विनोभेन्द्रावथुः काव्यैर्-दंसनाभिः ।]{style="color:#99cc00;"}\
[यत् सुरामं व्यपिबः शचीभिः सरस्वती त्वा मघवन्-नभिष्णक् ॥]{style="color:#99cc00;"}\
As parents aid a son, the two Asvins, have helped you O indra, with their wondrous powers and mantras. When you, with glory had drunk the swigs of beer, sarasvati refreshed you maghavan.


