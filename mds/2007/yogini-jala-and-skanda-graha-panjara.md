
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [योगिनी-जाल and skanda-graha-पञ्जर](https://manasataramgini.wordpress.com/2007/07/01/yogini-jala-and-skanda-graha-panjara/){rel="bookmark"} {#यगन-जल-and-skanda-graha-पञजर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/01/yogini-jala-and-skanda-graha-panjara/ "11:06 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/Rog0Hu2Ko4I/AAAAAAAAALU/_mIX8Df98pE/s320/64_yoginis_inverse.png){width="75%"}
```{=latex}
\end{center}
```

[yogini-जाल-chakra]{style="font-weight:bold;font-style:italic;"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/Rog0xu2Ko5I/AAAAAAAAALc/JyCdpNJ1_6A/s320/225_skanda_grahas_inverse.png){width="75%"}
```{=latex}
\end{center}
```



[skanda-graha-पञ्जर]{style="font-weight:bold;font-style:italic;"}

