
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Intellectual cretinism of Hindus](https://manasataramgini.wordpress.com/2007/04/01/intellectual-cretinism-of-hindus/){rel="bookmark"} {#intellectual-cretinism-of-hindus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/01/intellectual-cretinism-of-hindus/ "5:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Intellectual cretinism is a prevalent trend amongst the Hindu elite. The sincere Hindus at the संस्कृत sabha were showing me a video made by Western गौडीय vaiShNava-s that they were hoping to use to educate the modern hindu youth about the greatness of their culture and religion. While I am happy that the संस्कृत sabha is making efforts to educate Hindus, I am appalled by the intellectual laziness of much of the Hindu elite. Many of them are ब्राह्मण-s who are not worth the thread slung on their shoulders and hardly suited to take the place in the shiras of the puruSha. The things that are particularly futile in this section of the Hindu elite are the following: 1) The pursuit of flogging [dead Germans and other assorted Europeans](https://manasataramgini.wordpress.com/2006/07/24/flogging-dead-germans/) [ad nauseum]{style="font-style:italic;"}. Over and over again they talk of the evils of Mueller, Oldenburg, Jolly and Gonda, as though they are relevant today, and work themselves up on how they have insulted the vedas, hindus etc. The only catch here is that these dead Europeans are firmly behind their graves and Hindus have overthrown their white conquerors over 50 years ago. But in these 50 odd years the Hindu elite did not pay attention to reacquiring their traditions -vedic, tantric and secular. And in fact in the mean time the successors of the old Indologists have developed into more subtle subversionists (Note Sheldon Pollock' work) unbeknownst to the Hindu elite.

 2.  For all the talk the Hindu elite, especially the first two वर्णस्, has not shown any discipline to study the veda, or the स्मृतिस् or the tantras. In fact most who wax eloquent on such matters have little knowledge of the veda in its original. If the first two वर्णस् are so blatantly derelict in their duty then who will teach and guide the remaining Hindus? After all the one thing ancient Hindus did well was to teach their lore. Have the modern Hindu elite collected these texts and edited them? Have they studied the intricacies of vedic language before saying "everything is in the veda".

 3.  advaita, advaita advaita-- they prattle as though Hindus had no other intellectual tradition --- they even name their sons "advaita". They do not realize that you study the पूर्व-पक्ष-s too like the great माधव विद्यारण्य had done in his famous work. As result they fail to see the totality of Hindu thought and fail to appreciate the intellectual stars like the atharvavedin भट्ट jayanta.

 4.  Most fatally they have an alarming tendency to reject Indo-European linguistics. "There is no PIE", "IE linguistics is an Indological subversionist tactic" they blather. It is this last point that exposes this section of the Hindu elite as utter intellectual cretins. It must be clearly said that a person who denies the monophyly of Indo-European and the reality of PIE is an Idiot. If one fails to understand IE linguistics one is seriously handicapped in understanding the intricacies of the languages of Hindu thought. It is really tragic that the successors of पाणिनि and पतञ्जलि in this era are unable to grasp key linguistic issues. Perhaps, they must stop using computers for these were also invented by our European enemies or whoever.

In the field of vedic studies we have true intellectual cretins amongst Hindus like Dandekar and sadly to a certain extant Madhava Deshpande (though to give credit were due, he did produce a useful work on atharva vedic विकृति पाठस्). Dandekar scrapes the rock bottom in his studies on Vedic देवता-s, and it is such stuff that is parroted back by other Hindus. How many Hindus in the last 3o years (especially those with much greater access to resources than many of us) did comparable work to the greats like PV Kane, VRR Dikshitar, V. Raghavan, VS Agarwala, Sukhtankar and the like? Our friend ekanetra asked me why did Hindu scholars not do the job of Daniel Smith, Teun Goudriaan, Mark Dyczkowski, late Jan Schroterman or Somadeva Vasudeva ? ekanetra himself felt greatly indebted to the works of the above-named white scholars. Well, perhaps those who did a comparable job were not well-known as the above because they printed works in local Indian presses that gave them poor circulation. But I do seem to get a feeling like him that the Hindu elite simply did not do enough to support or widely spread the results of their own scholars. The secular government might not have supported us, but our elite should come forth and support scholarship privately.


