
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The signal for the long drawn charge](https://manasataramgini.wordpress.com/2007/09/01/the-signal-for-the-long-drawn-charge/){rel="bookmark"} {#the-signal-for-the-long-drawn-charge .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/01/the-signal-for-the-long-drawn-charge/ "5:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

How did we get to this state:

  -  [The imprisonment](http://manollasa.blogspot.com/2005/06/imprisonment.html)

  -  [कृत्या troubles](http://manollasa.blogspot.com/2005/06/kritya-troubles.html)

  -  [The कृत्या strike](http://manollasa.blogspot.com/2005/06/kritya-strike.html)

  -  [The कृत्यास्त्र strike](http://manollasa.blogspot.com/2005/06/krityastra-strike.html)

  -  [Towards the battle of काल-निर्णय](http://manollasa.blogspot.com/2004/12/towards-battle-of-kala-nirnaya.html)

  -  [Battle of हिमप्रारम्भ](http://manollasa.blogspot.com/2004/12/battle-of-himaprarambha.html)

  -  [The 3 front war](http://manollasa.blogspot.com/2004/11/3-front-war.html)\
We stood there alone waiting for the signal. Suddenly, in the cooling air we heard a rustle. Our agent arrived from the far end. We were sensing the moment of the great struggle --- would we live or die ? The outcome was unknown to us as it will occur only in the future, and no man knows what the future will be. The agent standing at some distance spoke: "yes the fight is real and you know how you got there. You have recorded all the steps, with the 2 मारण strikes after and the one before the funerary day in the 5th month of the civil year. You have journeyed through the various naraka-s after your fall like that of [ययाती](http://manollasa.blogspot.com/2005/06/preparing-for-worst.html). But a free man cannot remain long in the state of suppression. He has to fight-- if he dies in the process he is relieved of his suffering. If he lives he rules the world like युधिष्ठिर even if he has lost much. But there are some paths that lead to deeper naraka-s with more suffering be sane not take those paths again. You have done that before and do not want to do that again."

We : "What are they after?"\
The agent said: "They are not like Fourier, who battled only for honor. They want the हिरण्य rukma-s. Fourier only relied on mantra prayoga. They will try to use all means. One needs to be utterly ruthless in dealing with them --- but beware they can exploit every weakness and chink. They will be ready to deploy much large forces than during the खाण्डवन् battle."

We :"What about the last vIra?"\
Agent: "He will tied up with the म्लेच्छ सेना from गजलण्डपुर."


