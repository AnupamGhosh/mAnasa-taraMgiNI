
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नक्षत्र homa as per the काठक yajurvedins](https://manasataramgini.wordpress.com/2007/12/07/nakshatra-homa-as-per-the-kathaka-yajurvedins/){rel="bookmark"} {#नकषतर-homa-as-per-the-कठक-yajurvedins .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 7, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/12/07/nakshatra-homa-as-per-the-kathaka-yajurvedins/ "6:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The नक्षत्र homa as performed by the तैत्तिरीयक-s utilizes the famed नक्षत्र-सूक्तं and the elaborate mantra prescriptions from from the तैत्तिरीय ब्राह्मण (3.1). The kaTha-s have a very different tradition of नक्षत्र homa. With the denudation and extinction of the kaTha tradition this appears to have been lost, but we can reconstruct it in large part due to the commentary of the kaTha ritualist देवपाल. देवपाल was in the line of great kaTha yajurvedins from like: 1) gauradhara the ऋग्/yajur द्विवेदी from Kashmir who composed a now lost, but supposedly notable, commentary on the काठक saMhitA known as veda-विलास. 2) लक्ष्मीदेव, a noted performer of shrauta rituals and 3) Adityadarsha. देवपाल appears to have been one of those last great brahmin scholars who was intimately acquainted with the rituals of the kaTha tradition and he describes the denudation of brahminical tradition around him. While by all accounts he was स्मार्त, in terms of tantric and लौकीक disposition he was inclined towards the Kashimirian lineage of पाञ्चरात्र mantra-शास्त्र and even composed a great stava on विष्णु. In terms of philosophical affiliations he belonged to the yoga school and composed a scholarly commentary on the sUtra-s of पतङ्जलि. At the same time his work clearly shows that he was a master of the मीमांस school. He was born in Jalandhar, Panjab, to a vedic scholar हरिपाल son of भट्ट upendra. He wrote his great work on the लौगाक्षि kalpa sUtra-s in jayapura near Shrinagar, Kashmir.

The offerings are made in the shrauta context as पुरोडाशस् on 8 कपाल-s or in the औपासन fire as charu-s of rice or barley. The mantras used for the oblations are completely different from those of तैत्तिरीय tradition (some of them have readings unique to the kaTha tradition different from those found in the तैत्तिरीय ब्राह्मण or saMhitA. While I have gathered all these distinct mantra-s due to the effort of keying in I will for now stick to प्रतीक-s):

 1.  कृत्तिका: agnir मूर्ध दिवः ककुत्पतिः...

 2.  रोहिणि: हिरण्यगर्भ सम्वर्तताग्रे...

 3.  मृगशीर्ष: maruto yaddha vo दिवः...

 4.  Ardra: मृड no rudro ta no mayas कृधि...

 5.  punarvasu: सुत्रामाणं पृथिवीं द्यां अनेहसं सुशर्माणं अदितिं सुप्रणीतिं |...

 6.  तिष्य: tava shriya व्यजिहीत parvato गवां गोत्रं uda सृजो yad अङ्गिरः |... (= RV 2.23.18)

 7.  आल्श्लेष: namo astu sarpebhyo ye ke cha ...

 8.  magha: Aham पितॄन् सुविदत्रान् avitisi नपातं ...

 9.  purva फाल्गुनि: सदा सुगः पितुमां astu pantha madhvA ...

 10.  uttara फाल्गुनि: ye te aryaman bahavo देवयानाः पन्थानो ...

 11.  hasta: tat savitur वरेण्यं bhargo devasya ...

 12.  chitra: ya ime द्यावा पृथिवी जनित्री ... (= त्वष्ट mantra from भार्गव आप्री)

 13.  स्वाति: vayur अग्रेगा यज्ञप्रीः साकं अन्मनसा य\~ज्नं ...

 14.  विशाखा: इन्द्राग्नि रोचना दिवः parij ...

 15.  अनुराधा: mitro janan यात yati ब्रुवाणो ...

 16.  ज्येष्ठ: indra सुत्रामा स्ववान् अवोभिः ...

 17.  मूल: yam te devi निरृतिराबबन्ध पाशं ...

 18.  vishve devA ऋता वृध ऋतुभिर् ...

 19.  उत्तराषाढ: sham no देवीर् अभीष्टय Apo bhavantu ...

 20.  abhijit: brahma जज्ञानं प्रथमं पुरस्तात् ...

 21.  श्रवण: वषट् te विष्णवास A कृणोमि tanme ...

 22.  श्रविष्ठ: A me गृह भवन्त्वा prajA

 23.  शतभिषक्: imam me वरुण ...

 24.  पूर्व प्रोष्टपद: upa प्रागाचसनं वाज्यर्वा ...

 25.  uttara प्रोष्टपद: ahir iva भोगैः paryeti बाहुं ज्याया हेतिं ...

 26.  रेवती: पूषा gA anvetu naH

 27.  अश्विनी: pra वां दंसांस्यश्विनाव् avochamasya पतिः ...

 28.  भरणी: yamo दाधार पृथिवीं

27 or the 28 नक्षत्र-s have the same deities as in the नक्षत्र homa of the तैत्तिरीयक-s. The only exception being मृगशीर्ष which is presided by soma in the तैत्तिरीय tradition whereas the kaTha-s attribute it to the maruts. The kaTha-s rather than compose new mantra-s as in the तैत्तिरीय tradition instead resorted to culling mantras for the presiding deities from various preexisting collections. In most cases their choice is a proper, except in the case of ahirbudhnya, where, probably due to the lack of a proper mantra, the redactors were forced to chose one which just had the word ahi in it.


