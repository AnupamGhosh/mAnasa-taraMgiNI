
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The yaudheya-कुषान conflict](https://manasataramgini.wordpress.com/2007/05/12/the-yaudheya-kushana-conflict/){rel="bookmark"} {#the-yaudheya-कषन-conflict .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 12, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/12/the-yaudheya-kushana-conflict/ "3:34 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RkX2Co9MdNI/AAAAAAAAAIM/Jp-ytnai-78/s320/कुमार_saramA.jpg){width="75%"}
```{=latex}
\end{center}
```



A yaudheya coin with कुमार and a yaudheya style image showing a लिञ्ग, कुमार and [रेवती देवी.](http://manollasa.blogspot.com/2006/10/akhyana-of-skanda-patni.html)

One of the murky aspects of Indian history is the overthrow of the Hinduized Iranian rulers, the Kushanas and the mahA-क्षत्रप-s. We know that the mahA-क्षत्रप-s appear to be descendants of the shAka haumavarga, and held sway mainly in the Western part of India. After a glorious rise under rudra-dAman these Iranians appear to have declined gradually and moved into oblivion, with their military power curbed by the andhras (शतवाहनस्). The end of the कुशानस् appears to have been more dramatic, especially in the mainland of Aryavarta. Given that these Iranians were thoroughly Hinduized it is not clear if a major nationalist response to them was initiated --- i.e. similar to something against the Macedonians of Alexander or much later the movement of the later gupta-s against the हूण राजस्. However, it is clear that the original kShatriya-s who were overrun by the Iranians were not going to take their subordinate status lying down and eventually organized the struggle that destroyed the कुशान rule.

There are claims in the tamil epic the shilap-पधिकारं that the chera king senguttavan defeated a certain कनिष्क in Aryavarta. He is said to have made this kanaka and another ruler vijaya carry the image of pattini (कण्णकि-अम्माल्) to the drAviDa country. There is little clarity on what this meant. Altekar argued using numismatic evidence that it was the yaudheya republic that overthrew the कुशानस्. Altekar correctly inferred that prior to the कुषान period, the yaudheya republic's territory encompassed a northern Rajasthan and Punjab/Haryana of modern times. One of their chief capitals was the great कौमार center of rohitaka (Rohtak), [their patron deity being कुमार](http://manollasa.blogspot.com/2005/10/royal-kumara-worshippers.html). The महाभरत states that the republic was founded by the son of युधिष्ठिर through देविका, a princess of the आणव kingdom of the shaibyas.

The yaudheya republic was overrun by the कुशानस् under kanishka who annexed their territory to his empire. In 145 AD they made their first attempt at breaking the stranglehold by declaring independence in Northern Rajasthan. The कुशान-s took the aid of their co-ethnic rudra-dAman to crush the yaudheyas. He declares with much pride in the Junagad inscription that the yaudheya-s who were much respected for their valor by all the kShatriya-s were destroyed by his sena which moved northwards to smother their attempt at revolt. For about 30 years after this crushing defeat the yaudheya-s seemed finished. But they rose again defeating the क्षत्रप-s in the south and then in a series of battles driving out the कुशान-s from the original yaudheya territory. After this point the कुशान-s lost the entire territory east of the shutudri (Sutlej) river to the yaudheyas, and subsequently crumbled and disintegrated eventually becoming vassals of the Sassanid rulers of Iran in the extreme west. After this victory of the yaudheyas, we notice their cities reviving again in places corresponding to modern Rohtak, Dehra Dun, Saharanpur, Delhi, Ludhiana and Kangra. The latter city was originally founded by the औदुम्बरायनस्, who were destroyed completely by the कुशान-s. This glorious victory against and overthrow of one of the most powerful empires of the world of that era, one which had defeated the chIna-s and taken as hostage a prince and princess of the Han chIna-s, greatly increased the the prestige of the yaudheya republic. Their victorious advance of the yaudheya-s was attributed to the jaya-mantra of the ब्रह्मण्य deva --- a point often mentioned in the legends of their coins below the image of a spear-wielding skanda ("यौधेयाणम् jaya mantra धराणां"). A point of interest that Altekar noted regarding the yaudheya coins was that the script used was brAhmi and not Greek or even kharoShThi, as was typical of the कुशान-s who also had coins with कुमार images. This might indicate that yaudheya-s were explicitly flaunting their national spirit by repudiating the foreign and northwestern scripts.

The study of the coin inscriptions has also been noted that the yaudheya republic grew by amalgamation of other republic they incited into the overthrow of the कुशान-s. These were: 1) the enigmatic अर्जुनायन-s, who claimed descent from the पाण्डु hero and liberated the territory in the Agra-Jaipur zone. 2) कुणिन्द-s who liberated the area in the north-west between the shutudri and विपाशा (Beas). Some historians feel that the numbers '2' or '3' coming after the legend yaudheya-गणस्य जयः indicated these associated republics that coalesced with the core yaudheya-s. Their rule was one of the golden periods of the कौमार sect, with the development of much of the classical कौमार mantra-शास्त्र.


