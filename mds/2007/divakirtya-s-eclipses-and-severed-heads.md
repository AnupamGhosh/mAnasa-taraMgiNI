
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [दीवाकीर्त्य-s, eclipses and severed heads](https://manasataramgini.wordpress.com/2007/01/18/divakirtya-s-eclipses-and-severed-heads/){rel="bookmark"} {#दवकरतय-s-eclipses-and-severed-heads .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 18, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/18/divakirtya-s-eclipses-and-severed-heads/ "6:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We saw how the aitareya ब्राह्मण describes the [दिवाकीर्त्य-s](https://manasataramgini.wordpress.com/2007/01/15/the-crashing-doors/) as the frame of solstice points that "holds up" the sun. The पञ्चविम्श brAhman 4.6 describes the दिवाकीर्त्य-s, just as the तैत्तिरीय and kaTha texts, in the context of the solstice rite but also adds a second context: namely that of the eclipse. It repeats a famous tale of the दानव स्वर्भानु (PB4.6.13-15): " The demonic स्वर्भानु seized the sun with darkness. The gods drove the darkness with the दीवाकीर्त्य. The दिवाकीर्त्य-s are the rays of the sun, by means of the rays they indeed take hold of the sun." The it states that the [भ्राज and आभ्राज](https://manasataramgini.wordpress.com/2007/01/13/the-divakirtya-samans/) सामन्स् remove the darkness from the upper part of the sun, the विकर्ण and महादिवाकीर्त्य from the middle part and the भास from the lower part. This suggests a progressive movement of the eclipse with different parts of the sun liberated successively from it. The term स्वर्भानु is a very overt reference to the solar eclipse because the same term is used from the earliest times (the atri मण्डल of the RV) to describe the eclipse which was observed and predicted by the atri-s using their quadrants (तुरीय in the RV). Following the दीवकीर्त्य-eclipse connection of the सामवेदिc tradition through other vedic texts we move into a series of more cryptic allusions that might provide parallels with later day pauranic mythology.

So far in studying the दीवकीर्त्य-s we had not alluded to the evidence from the shukla yajurvedic tradition. We find a rather notable allusion to it the shatapatha ब्राह्मण in 4.1.5 of the shatapatha ब्राह्मण. This section of the SB first begins by mentioning of how the भृगु-s and angirasa-s saw the svargo loka. It goes on to describe the restorative act of the ashvin-s by providing the earliest version of the famous tale of chyavana, सुकन्या and शर्यात मानव in the context of the offering made to the ashvin-s (SB 4.1.5.1-14). This offering is related to the ashvins as the divine physicians restoring the severed head of the यज्ञ. The blindness of chyavana followed by his restoration is, at its heart, a solar restoration myth superimposed on the the history of the भार्गव-s. The ashvin offerings in this context are connected to the दिवाकीर्त्य-s that restore the head to the यज्ञ as per SB 4.1.5.15:

"[विशीर्ष्णा वै यज्ञेन यजध्व इति कथं विशीर्ष्णेत्युप नु नौ ह्वयध्वम् अथ वो वक्ष्याव इति तथेति ता उपाह्वयन्त ताभ्याम् एतम् आश्विनं ग्रहम् अगृह्णस्तावध्वर्यू यज्ञस्याभवतां तावेतद् यज्ञस्य शिरः प्रत्यधत्तां तद् अदस्तद्दिवाकीर्त्यानाम्ब्राह्मणे व्याख्यायते यथा तद् यज्ञस्य शिरः प्रतिदधतुस्-तस्माद्-एष स्तुते बहिष्पवमाने ग्रहो गृह्यते स्तुते हि बहिष्पवमान आगcअताम्]{style="color:#0000ff;"}"

Then in SB 4.1.5.17 the tale of yet another भार्गव, dadhichi is mentioned in relation to the restoration of his head by the ashvin-s in the context of the मधुविद्या. Thus, the दिवाकीर्त्य-s are here cryptically linked to the restoration of the sun's brightness by the ashvin-s. Thus, the severed head of the यज्ञ in the pravargya might also be linked to solar restoration.

This leads us to the those mysterious mantras of RV 10.170/171 which clears some of this up: The विकर्ण sAman amongst the दिवाकीर्त्य-s is based on RV 10.170.1. This सूक्तं is noteworthy in some other respects: 1) It uses the word भ्राजः which corresponds to the name of one of the दिवाकीर्त्य-s. 2) Prominent words in this सूक्तं like s \[vi]भ्राट्, भ्राजः, ज्योतिः, दिवः, अयुः, satya, dharman occur as \[or as parts of] the magical stobhas of the दिवाकीर्त्य-s. 3) This is one of those relatively few mantra-s to the sun \[sUrya] in the RV. 4) It has the word asura in a negative connotation-- such a negative usage of asura is rare in the RV and occurs elsewhere in the context of sUrya and the eclipse (स्वर्भानु is also called asura).

When we look more carefully at the second mantra some interesting elements come to light:

[विभ्राड् बृहत् सुभृतं वाज-सातमं धर्मन् दिवो धरुणे सत्यम् अर्पितम् ।]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
[अमित्रहा वृत्रहा दस्युहन्तमं ज्योतिर् जज्ञे असुरहा सपत्नहा ॥]{style="color:#0000ff;font-style:italic;font-weight:bold;"}\
We note that first hemistich contains the words सुभृतं, dharman, धरुणे, all implying bearing, propping or supporting of the heavens, which relates with the basic theme of the दिवाकीर्त्य-s as a prop of sUrya as described so vividly in the aitareya ब्राह्मण. In the second hemistich where we encounter the phrase "jyotir जज्ञे असुरहा ..." i.e. he generated a light that smote the asura. This atypical use of the negative asura along with its smiting reminds one of the the सूक्तं of atri (RV 5.40) where the negative asura is repeatedly used to describe the solar eclipse. These observations together suggest that the सूक्तं RV 10.170 comprised a "proto-दिवाकीर्त्या", which already was linked to elements seen in the classical दिवाकीर्त्य-s.

Interestingly, the very next सूक्तं RV 10.171 of इटन्त् भार्गव describes the severed head of makha. In this सूक्तं in the second mantra indra is said to taken away the skin from the head of makha: "[त्वं मखस्य दोधतः शिरो ऽव त्वचो भरः]{style="color:#0000ff;"}" (RV 10.171.2). The 4th mantra is again pretty striking:

[त्वं त्यमिन्द्र सूर्यं पश्चा सन्तं पुरस् कृधि । देवानां चित् तिरो वशम् ॥]{style="color:#0000ff;font-style:italic;font-weight:bold;"}(RV 10.171.4)\
Bring, indra, the sun to the front that is lingering behind, hidden against the wishes of the deva-s.

This finally completes a rather tangled path --- the इतान्त् भार्गव सूक्तं is recording an eclipse with two allegories: 1) the skin being taken of the head of makha. The head of makha is the severed head of the sacrifice that became the sun as per the ब्राह्मणस्. 2) The sun being hidden against the wishes of the god. Thus, juxtaposition of the makha and the विभ्राड् hymn in the RV मण्डल 10 is unlikely a chance event --- it follows the real tradition recorded in the shatapatha ब्राह्मण 4.1.5 where the head of makha/यज्ञ is restored by the दिवाकीर्त्य-s.

So the tangled contextual information suggests that in the vedic mind several different "regenerative" events used a similar set of motifs for description and ritual enactment: 1) The regeneration of the sun after sinking in winter in the northern latitudes of the early Indo-Europeans. 2) The restoration of the sun after an eclipse. 3) The restoration of the world axis, the frame of the sun after a precessional shift. Hence, not surprisingly, the sauchika agni hymn which concerns itself with precession also introduces the important number of the Saros cycle in a cryptic way.

Finally, this motif of the severed head in relation to the eclipse lingers on in the Hindu world, reappearing in the पुराणस् as head of राहु who eats the sun. Here again the head of the asura is severed by विष्णु, just as indra slays स्वर्भानु in the veda. We suspect that there was a para-vedic विष्णु-centric Indo-Aryan stream where the myth of राहु arose from the same old base motifs found in the Indo-European world.


