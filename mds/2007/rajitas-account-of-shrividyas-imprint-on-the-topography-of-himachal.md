
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Rajita's account of श्रीविद्या's imprint on the topography of Himachal](https://manasataramgini.wordpress.com/2007/10/17/rajitas-account-of-shrividyas-imprint-on-the-topography-of-himachal/){rel="bookmark"} {#rajitas-account-of-शरवदयs-imprint-on-the-topography-of-himachal .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 17, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/17/rajitas-account-of-shrividyas-imprint-on-the-topography-of-himachal/ "4:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R was telling us of how the holy configurations of श्रीविद्या are embedded in the geography of Himachal Pradesh. She mentioned that these ancient footprints of श्रीविद्या still are seen in the temples of the land but are largely forgotten by the local people who continue to worship the deities but not aware of the deep significance or the mantra-शास्त्र in most instances. Instead for them the fairs (jatras) and the social events assume a much greater significance. R gathered a lot of interesting information during her last visit to those regions. Of particular note was her account of the stations of the earthly मण्डल-s of some of the primary देवता-s of श्रीविद्या. The earthly श्रीचक्र of कामेश्वरी is located near the village of Mandi about 100 Km from Shimla. There is a wooden temple marking the spot of this shrichakra, the form of the mistress of the kula path. The earthly geyachakra of सचिवेशी मन्त्रिणि is located in Shimla. This temple is apparently near the mall in Shimla. The city itself gets it name from her name श्यामला, the dark mistress of the geyachakra. The earthly kiri-मण्डल is located at Devidhura at the border between Nainital and Almora. Here there is an old temple of वारही marking its spot. Thus, the कुलयोगिनी is worshiped flanked on either side by सञ्केतयोगिनी and सञ्गीतयोगिनी.

As per the श्रीविद्या practioners of the हादि-mata and सादि-mata who were once prominent in the Himalayan regions sarva-stambhini बगलामुखी is worshiped in the night time rite. The earthly मण्डल of बगलामुखी in Himachal is located in the form of the temple at the Guma near Mandi. Her shrine is located in a close spatial cluster with that of कामेश्वरी.

The two other देवी-s of the inner triad are apparently located far from each other: वज्रेश्वरी is located at a temple in Nagarkot in Kangra. This temple was ravaged by the Moslem vandal Mahmud Ghaznavi, but was restored more recently. भगमालिनी is located in the plains at the jalandhara-पीठ where abhinavagupta had studied under शम्भुनाथ. The great bhairava himself is located in a remote temple in the great heights of Kalpa in the Kinnaur district near the Kothi lake. Here the bhairava stands apart like the bindu standing apart from the 5 shiva-yuvati-s and 4 श्रिकण्ठ-s. Next to the temple of bhairava is an ancient temple of चण्डिका.

R states that all these temples are all figuratively distilled together in the 3-storied shrine of हादिविद्या त्रिपुरसुन्दरी located in the village of Naggar, overlooking the Kulu valley. It is like the कामेश्वरी temple of Mandi made of wood and apparently has yantra outlines carved into it.


