
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The जराबोधीय सामन्स्](https://manasataramgini.wordpress.com/2007/03/06/the-jarabodhiya-samans/){rel="bookmark"} {#the-जरबधय-समनस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 6, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/06/the-jarabodhiya-samans/ "3:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/ReuTsGbvrLI/AAAAAAAAAEY/yTaYjVGohmI/s320/sAman.png){width="75%"}
```{=latex}
\end{center}
```



shakti was the son of मैत्रावरुणि वसिष्ठ. The sons of shakti were the शाक्त्य-s like पराशर and गौरवीति. They were performing a याग in which pashu-पुरोडाशस् were offered. For this purpose the ऋषि गौरिवीति शाक्त्य had shot a deer. Just then garuDa came flying onto his quarry from above. He took aim with his arrow to shoot तार्क्ष्य सुपर्ण. सुपर्ण told him: "O ऋषि do not shoot me. I will help you to fulfill what desire you might have." गौरिवीति asked him: "so what do you think is my desire?" garuDa said: "You love the daughter of asita धाम्न्य; I will convey you to her. asita धाम्न्य was a jealous father who did not want his daughter's lover to get to her. He had a palace in atmosphere, where the asuras safe-guarded his daughter. garuDA daily awakened गौरिवीति with the जराबोधीय sAman, hid the ऋषि in a rolled up leaf and bore him to his lover. He made her pregnant and a son was born to her. The asuras saw the child of their sister and said this is a रक्ष and tearing him threw him away. गौरिवीति wished to revive him and used the जराबोधीय sAman to revive him. The revived boy was the ऋशि संकृति the founder of the eponymous gotra amongst the वसिष्ठस्.

The जराबोधीय is a dyad of सामन्स् composed on the ऋक् 1.27.10* of शुनःशेप Ajigarti, the son of the fallen भार्गव. These सामन्स् are a laud to rudra. Both are seven-fold सामन्स्. The stobha in the first one is au ho वा occurring in the second last bhakti. The second has 2 stobha-s oवा and iDA occurring the first and last भाक्तिस्. That stobha iDA is supposed to protect and increase his cattle. Shown above is an analysis of the सामन्स्, as recited by kauthuma शाकाध्यायिन्स् from थन्जावुर् who had served as the उद्गातर् and his assistance in recent soma sacrifices. One can note that in the 1st जराबोधीय jArA opens in a low tonal series but the duplicated bodha2 with the प्रेङ्क moves to the high tone. In the second sAman the whole bhakti जराबोधोवा is in the low tonal series. The nidhana of the first one is a high tone, while in the second one we notice the descending tone series '345' typical of many saman musical cadences.


