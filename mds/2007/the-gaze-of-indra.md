
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The gaze of indra](https://manasataramgini.wordpress.com/2007/10/26/the-gaze-of-indra/){rel="bookmark"} {#the-gaze-of-indra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 26, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/26/the-gaze-of-indra/ "8:45 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

||[ ]{style="font-style:italic;"}[सहस्राक्षा धियस्पती]{style="font-weight:bold;color:rgb(0, 204, 204);font-style:italic;"}||\
```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RyJTnrxjPKI/AAAAAAAAAPs/PzGC-xHaK9o/s320/indra_netra.png){width="75%"}
```{=latex}
\end{center}
```



click on image to enlarge

