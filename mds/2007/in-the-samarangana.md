
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the समराङ्गण](https://manasataramgini.wordpress.com/2007/04/08/in-the-samarangana/){rel="bookmark"} {#in-the-समरङगण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 8, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/08/in-the-samarangana/ "9:03 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The news reached that our most skilled सेनानी had captured the fort of अमुखास्थिक after a very fierce and prolonged struggle. In the thick of battle with shara-s raining all over it was not known who was friend or foe, but in the end he held his cool and we routed our foemen. At that point we turned our attention to personally lead the forces against a fort that had for long kept slipping from us, though we had planted our flag on that parvata before our foes. But even as we were closing into to make a large attack on it, our skilled सेनानी sent us a messenger that the young commandant who had fought with great distinction on the battle of शुल्बारि hala had been ambushed by a combine of our most dreaded shatru-s who possessed a large force. Our young commandant panicked greatly. But our सेनानी tried to calm him down and asked him to remain bold on the field. We saw the grim advance of our hated भ्रातृव्य and spent a sleepless night wondering over the battle plan. We drew our battle plans and to protect the front manned by our young commandant, when we faced another unexpected attack. The अभिचारय्मान-s had earlier slowed us down with a dreaded attack, which is another story in itself. Now they incited the खाण्डवन्स् into a rebellion. Just before we set forth for the other front we were probed by the खाण्डवन्स् and spent a while in a prolonged skirmish. While we drove them away ,like the भृगुस् driving away दीर्घजिह्वी, they were resorting to mitra-bheda to try to break up our allies. Suddenly we found ourself under a massive alignment of our enemies, all hoping that we never see the sun again. We turned to maghavan to confer victory on us.


