
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Past thoughts](https://manasataramgini.wordpress.com/2007/04/21/past-thoughts/){rel="bookmark"} {#past-thoughts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 21, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/21/past-thoughts/ "3:21 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Before the battle of दशान्त we were shackled, but in that fierce raNa, miraculously aided by विनायक we won a battle and obtained freedom. During the battle of द्वादशान्त we were completely free and memories go back to many warriors who fought in the pitched battle for fame and glory: चार्वाक, बृहत्-sphik, hrasvaroman, ashvamukha, शूद्रक, श्रीवैष्णव, क्षेत्र-धृत, shveta-dadhna and braindeya. बृहत्-sphik and ashvamukha hated us. In the battlefield of rasa-क्षेत्र we were trapped in a dangerous position, but Mis-creant supplied us with intelligence regarding their positions like a पञ्चाल spy and we were able make our escape. In the great charge of battle that followed we were weakened by a takman, even as the Assamese general against the Mogols. But in that great yuddha the deva-s bore us aid and we extirpated बृहत्-sphik and ashvamukha from our loka, as also their secret ally patita-putraka. During the fierce battle of पञ्चदशान्त our tapas aided us against the sena of स्त्री-s and we blew them away.

While the thoughts often linger on these battle, it was the multi-front war of सप्तदशान्त that posed an existential crisis for us. There were two major fronts and several minor ones. Our most hated shatru maireya-पायिन् faced us on the first major front and the hyona and rasagola warriors who even hated our shadow stood on the second. the maireya-पायिन् tried at first to detach the अमात्य and sachiva to corner us. We were at that time tied down with a takman and unable to act for a month. But then we retaliated in a big way and routed maireya-पायिन्-s men in a skirmish. Then we were wounded in our hand and unable to wield the bow, when maireya-payin's army bore down on us. We fought on with our wounded hand-- even though we could not prepare for battle, we emerged completely victorious after our auxiliary force \[we thought of Napoleon] helped to overthrow the वञ्ग allies of maireya-payin. But on the other front the hyona and rasagola warriors were scoring victories against our men and had surrounded them completely. The muni-s army had already been routed completely and was on the retreat. It was at that instance that Jx came to our aid with a large force in the foreign field. He smashed the म्लेच्छ warriors who aided the hyonas and rasagolas. Using the opening in the shatru-व्यूह created by Jx, we rushed in an scored an incredible sweeping victory.

Jx moved on the score several victories against the म्लेच्छ vIra-s and conquered the kingdom of hima-hrada. He was reigning supreme there and enjoying bhoga. When was caught in the bliss of bhoga two mishaps struck him. The chIna-s sent their spasha-s to probe him and he was caught unaware. He was then seized by a dreaded डामरिका. The डामरिका had kept him imprisoned and suffering till the virile तैत्तिरीय saved him from destruction. But in the mean time he lost the kingdom of hima-hrada to the chIna-s. He by his valor fled to भानु-कक्ष and carved himself a small estate there. In a long conversation with Jx then a little did we know that same डामरिका was waiting for us.


