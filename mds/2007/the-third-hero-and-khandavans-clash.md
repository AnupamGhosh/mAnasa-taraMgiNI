
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The third hero and खाण्डवन्स् clash](https://manasataramgini.wordpress.com/2007/11/12/the-third-hero-and-khandavans-clash/){rel="bookmark"} {#the-third-hero-and-खणडवनस-clash .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 12, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/12/the-third-hero-and-khandavans-clash/ "5:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There was nothing new in the hostile intention of the evil-minded खाण्डवन्-s. Jealous of the great power of the 3rd vIra they wanted to ambush him too. We were not very keen about this happening, because we precisely knew what the outcome would be and it did transpire. The 3rd vIra was attacked around noon by the खाण्डवन्-s. But he was protected by his large सेना with several fierce bowmen ready to rain arrows. He drew the खाण्डवन्स् into an ambush and attacked them. They suffered heavy losses. His vIra-s also dispatched two agents of the late Fourier to the council hall of chitragupta. But then as we expected the खाण्डवन्स् smarting from the arrows they received turned to attack us. Jx seeing that the खाण्डवन्स् might attack wisely retreated, saving his mantra-bala to fight another day. He too repulsed an agent of Fourier with his yoga. We were duly attacked by the खाण्डवन्स्. But we stood like महाव्रतिन्स् or कपालव्रतिन्स् unmoved by the blaze around us. After raging for a while\
the खाण्डवन्-s turned and made another attempt at the 3rd vIra, but shaken again by his large सेना, they retreated. From there they sent a fierce kukkura-मारण prayoga at us using a bhairava mantra. But we had performed the याग to the धूर्त. At mid-night the खाण्डवन् prayoga reached- it had 3 खाण्डस्: the दुर्णम, a terrible पिषाच and a vile mohini. The दुर्णम first hit us but we took steps to maneuver past it. Then the terrible, exceedingly black पिशाच reached us in the black of the night. We clearly saw it: To our own amazement, it hovered around us roaring and barking with its fangs bared but failed to pierce that brahma-varma. It was so close yet unable to strike us, blocked by the invisible skanda gahvara: OM श्रीं e i u na श्रीं rIM कुमाराय नमः | Failing utterly, the पिशाच fled as though it was struck by a shakti of हेमशूल.

We then showed the vajra mudra thinking of the slayer of महिष and तारक :\
OM || uta स्वानासो divi Shantv-agnes-तिग्मायुधा रक्षसे हन्तवा u || ह्रां ||\
made chid asya pra rujanti भामा na varante परिबाधो अदेवीः || ङं

Oh thundering son of agni who rushes at the अदेवीः hurling terrible weapons, like the rampant agni utterly destroying the proud lords of the forest, slay our foes. By that fury the mohini was extirpated.

While this attack was a sudden as that deployed on us when नील-lohita-मेकला of pretty smiles saved us, the कुमार mantra-s placed us in a much better situation than what we were in then.


