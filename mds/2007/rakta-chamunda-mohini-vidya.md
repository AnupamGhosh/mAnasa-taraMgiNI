
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [rakta-चामुण्डा मोहिनी विद्या](https://manasataramgini.wordpress.com/2007/10/11/rakta-chamunda-mohini-vidya/){rel="bookmark"} {#rakta-चमणड-महन-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 11, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/11/rakta-chamunda-mohini-vidya/ "5:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RwxpcsEvi6I/AAAAAAAAAPc/IA3reLQ0UX0/s320/devI.jpg){width="75%"}
```{=latex}
\end{center}
```



विश्वयोनीं शक्तिं आदिशक्तिं सनातनीं |\
मातङ्गीं मदिरामोदां vande जगदीश्वरीं |\
मोहिनीं सर्वलोकानां बीजां शाम्भवीं त्रयीं |\
सर्वार्थ साधिनीं बालां त्रिपुरां शाम्भवीं प्रियां |\
अभीष्टफलदां देवीं vande tAM जगदीश्वरीं ||

aiM IM sauM सकलसुरासुर sarva भ्रमरी sarva-संक्षोभिणी sarva-विद्राविणी sarva-क्लेदिनी sarva-मनोन्मादिनी bhakta-त्राण परायणी OM ह्रीं रक्तचामूण्डी kuru kuru आकर्षय OM ह्रीं क्रों परमयोगिनी परमकल्याणी पवित्री ईश्वरी स्वाहा ||


