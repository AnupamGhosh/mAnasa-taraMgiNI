
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The paschimaugha of the मानवौघ of the kaumaras](https://manasataramgini.wordpress.com/2007/05/26/the-paschimaugha-of-the-manavaugha-of-the-kaumaras/){rel="bookmark"} {#the-paschimaugha-of-the-मनवघ-of-the-kaumaras .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 26, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/26/the-paschimaugha-of-the-manavaugha-of-the-kaumaras/ "4:55 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were told that there was the lost temple of the pashchimaugha. After the end of the gupta-s the pashchimaugha seems to have declined in public prominence over time in the north-west, but is known to have continued quietly until at least the 1500s of CE. This is not the early lineage moved towards the [south-west which we alluded to earlier](https://manasataramgini.wordpress.com/2006/05/08/the-second-circle/). The vaiShNava during his peregrinations identified this lost temple and supplied me evidence regarding the time till which it was active. The vaiShNava met a learned paNDita from the लाट country, Gopal Vyas, in the days of his youth. The paNDita had discovered an old Ashrama about 3 Km from the hamlet of Gudha named खर्जूरिका in a dense forest track:

[खर्जूरिका](http://maps.google.com/maps/ms?ie=UTF8&hl=en&msa=0&msid=117413233370285242190.00000112a5368b75f62e2&amp;amp;amp;t=h&om=0&ll=25.387147,75.565681&spn=0.142057,0.233459&z=12)

There he showed the vaiShNava the ruins of 5 temples and some settlements that once comprised an Ashrama. Most of the temples are in utter ruin and only three could be identified. One was that of कङ्काली, the other of a bhairava (with an image of गोराक्ष-नाथ), and the third was the lost temple of the pashchimaugha of कुमार. In the कुमार temple the following inscription in nAgarI was noted by Pandit Vyas:\
[ॐ स्वामी कार्तिकेय अचलेश्वर योगि प्रसादत्[...] सेवक [स्]थिरतत्त्व योगी मूर्ति स्थापिते जातथ[...] शुभं भवतु ॥]{style="color:#99cc00;"}\
This shristhiratattva yogin who installed this कुमार temple was probably the last of the great north-western adepts of the कौमार pashchimaugha. The remnants of his Ashrama were seen by the vaiShNava in the vicinity of the above temple cluster. From another inscription of his from the vicinity of Bundi, we get a date as vikram संवत् 1561 (CE 1503?) suggesting he flourished around this period. This is consistent with the mention of the Rajput chief Rao Suraj Mal, who was around at least in the first half of the 1500s. This yogin was also an adept in the siddhAnta and bhairava tantras and gives his lineage thus: maheshvara-tattva-\>bhramara tattva->विचार tattva-\>dharma tattva-\>sthiratattva. He also built at least two other temples, one to kapileshvara (rudra) and the other to काल-bhairava with a pond with lotuses and a garden around it. He attained siddhi in the 6 rahasya kaumara mantras while performing japa in the forest of गर्गारण्य.


