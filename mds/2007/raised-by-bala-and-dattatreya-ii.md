
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Raised by bAlA and दत्तात्रेय-II](https://manasataramgini.wordpress.com/2007/01/27/raised-by-bala-and-dattatreya-ii/){rel="bookmark"} {#raised-by-bala-and-दतततरय-ii .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 27, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/27/raised-by-bala-and-dattatreya-ii/ "7:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A great नाथ yogi who worshiped दत्तात्रेय was eating curds and saw a boy roaming on the field. He offered the boy the curds but he refused, and it fell on his feet. He told him: "if you had eaten the curds you would have lived long and become the king of the world but now in your relatively short life where ever you set foot you will conquer. This curds is imbued with the might of दत्तात्रेय." The boy's parents nara-भूपाल and कौशल्यवती initiated him into the 3-syllabled bAlA mantra and asked him perform its japa. His father had already attained mantra siddhi of the hallowed षोडषी. For 24 years he performed its japa and he finally attained siddhi of it. He was given the signal by the ever youthful नित्याषोडषिका, the mistress of the 3-syllabled mantra, that he was destined for greatness. Prior to that, as a young man, he went to Bhaktapur, where he began his intense साधन after his nine fold krama दीक्ष. This led him to attain the grace of the queen of कुब्जीशान, the mistress of the the पस्चिमाम्नाय. Then he also attained the grace of our goddess, the dreadful mother of the उत्तराम्नाय. The देवी worshiped in the कुमारी gave him the प्रसाद instead of the राजन्. It immediately became clear that पृथिवी नारायण शाह deva was to be the राजन्. He traveled to वाराणसी and there performed a rite to उत्तराम्नायेश्वरी as siddha-लक्ष्मी and प्रत्यङ्गिरा. As a result he was able to mysteriously raise money and use to purchase first-class modern weapons from India and also obtain first hand intelligence on the designs of the East India Company and the subversive role of Catholic missionaries. He then arranged a variety of alliances with the neighboring Indian rulers and prepared on a large-scale for the unification of Nepal.

  - In his first strike पृथिवी नारायण conquered Nuwankot and attacked Kirtipur. In the battle with teja narasimha the king of Patan who controlled the town he was almost killed. But his mantra prayoga came to his aid and saw him through.

  - After two failures to take Kirtipur he sacked Lamji in a fierce battle, where the power of the war machine of पृथिवी नारायण became first apparent.

  - He then sacked Kirtipur after a six month siege and surged towards Patan. But a British army attacked him from the rear.

  - He cornered the Britons in the Tarai, where they were put to flight by a sudden strike.

  - King jayaprakasha malla of Kathmandu sought the help of captain Kinloch who marched with the British army into Nepal. In the battle of Sindhuli, पृथिवी नारायण smashed the Britons and beheaded the captain.

  - He then entered Kathmandu during the indra-dhvaja festival took the throne of the राजन्. He received the प्रसाद from the देवी invoked into the कुमारी, and was declared राजन्. The king of काठ्मण्डु जयप्रकाश malla fled in terror.

  - Then he moved on Makawanpur, whose ruler digbandhan sena sought the aid of the Jihadi adventurer Gurgin Khan to repulse पृथिवी नारायण. However, Moslems were put to sword and Makawanpur was taken. In the battle पृथिवी नारायण captured a rich haul of ammunition.

  - He then conquered Bhadgaon from rAjA रणजित् and Patan from teja narasimha and put them to flight .

  - By 1773 he overran the whole of Eastern Nepal driving and defeating the Kiratas.

  - Thus, did पृथिवी नारायण unify the kingdom of Nepal. Realizing the danger of Christian subversionists he drove out all Jesuit missionaries and British agents from his kingdom and upheld the Hindu dharma. He is one Hindu ruler who deserves the highest praise for his foresight in recognizing the need to ban Christian activity as the correct way of preventing the undermining of a heathen state. In his court was a noted tantric भगवन्तनाथ, who was a master of हठ yoga, and performed many notable tantric prayoga-s.

He initiated his eldest son प्रताप सिंह into the tantric lore. प्रताप सिंह compiled all this knowledge into the great tantric digest the पुरश्चर्यार्णव.\
\~*\~*\~\
R1's ancestor had associated with पृथिवी नारायण during his visit to वाराणसि and went to Nepal along with him. They thus obtained access to an inner circle with the राजन् with a krama दीक्ष of the derived form उत्तराम्नाय worship . This krama involves the worship of : 1) siddha-लक्ष्मी 2) गुह्यकाली 3) महाभीम-सरस्वती, 4) धूम्रा, 5) कामकलाकाली, 6) महाकाली, 7) कपालिनी, 8) महास्मशानकाली, 9) कालसंकर्षिणी, 10) प्रत्यङ्गिरा, 11) कालरात्री, 12) योगेशी, 13) सिद्धभैरवी, 14) दक्षिणा, 15) छिन्नमस्ता, 16) राजराजेश्वरी 17) सप्तकोटेश्वरी.


