
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [kaTuka](https://manasataramgini.wordpress.com/2007/11/18/katuka/){rel="bookmark"} {#katuka .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 18, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/18/katuka/ "4:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/Rzam9FySfOI/AAAAAAAAAQU/Fl2EbuS3sag/s320/kaTuka_root.png){width="75%"}
```{=latex}
\end{center}
```



We were drawn to compose an epistle on the kaTuka herb (genus [Picrorhiza]{style="font-style:italic;"}) on account of what we learned during our studies in the atharvan tradition. Extant tantric tradition records a rite directed towards a deity known as the Asuri दुर्गा . It is mentioned in it is common form in the mantra-mahodadhi of mahidhara. The formula given here is of the form:\
[ॐ कटुके कटुकपत्त्रे सुभगे आसुरि रक्ते रक्तवाससे अथर्वणस्य दुहिते अघोरे अघोरकर्मकारिके अमुकस्य गतिं दह दह उपविष्टस्य गुदम् दह दह सुप्तस्य मनो दह दह प्रबुद्धस्य हृदयं दह दह हन हन पच पच तावद् दह तावत् पच यावन् मे वशम् आनयसि स्वाहा ॥]{style="color:#0000ff;"}

The plant mentioned by mahidhara as being used for the oblation in this rite is the AsurI plant, with the whole herb including roots, stem, leaves, flowers and fruits being ground into a meal and used as a oblation.

Its roots go back to atharvan tradition, where a rite known as the AsurI rite is practiced. It occurs in two variants which are respectively attributed to the [shaunaka and पैप्पलाद schools](https://manasataramgini.wordpress.com/2007/09/10/some-notes-on-the-evolution-of-the-mantra-shastra/). These variants are respectively found in परिशिष्ट 35 (AsurI kalpa) and the पैप्पलाद आङ्गिरस षट्कर्म kalpa respectively. As we have [discussed before](https://manasataramgini.wordpress.com/2007/09/10/some-notes-on-the-evolution-of-the-mantra-shastra/) these mantra-s associated with the atharvan tradition are potential precursors of the later tantric mantra शास्त्र. This supported by the presence of a third stand alone text termed the AsurI kalpa which contains both the atharvanic as well as a "tantric" form of the mantra. This tantric form also used by practicing atharvans is specified as: [ॐ क्लीं ह्रीं श्रीं क्शौं क्शौं श्रीं ह्रीं क्लीं ॐ कटुपत्त्रे सुभगे आसुरी रक्तवाससे ऽथर्वणस्य दुहिते घोरे ऽघोरे स्वाहा ॐ क्लीं ह्रीं श्रीं क्शौं क्शौं श्रीं ह्रीं क्लीं ॐ ।]{style="color:#0000ff;"}

There are two features that suggest a close link between the AsurI rite to the older vedic aspect of the atharvan tradition: 1) The use of specific plant that is very typical of various atharvanic deployments and 2) The use of the term [अथर्वनस्य दुहिते]{style="color:#0000ff;"}. This connection supported by the atharvan tradition which mentions two ओषधि-s AsurI and kaTuka: the former is mentioned in सूक्त-s of both AV-S and AV-P, whereas the latter is mentioned only in the AV-P in सूक्त 7.19.3 on medicinal herbs.

In contrast to the version of the rite from the mantra-mahodaddhi , atharvan tradition records that in the proper atharvan form not just the AsurI plant but also several other plants are also deployed. Three of this are directly mentioned in the mantra itself : kaTuka, AsurI and subhaga, which later tradition takes to be a synonym of प्रियङ्गु. Several others too are deployed as per tradition. I was initially interested in possibility of one or more of these having some psychoactive properties as suggested by the effect on the targeted woman mentioned in the AsurI kalpa: Sexual arousal, "burning" of the mind and heart. However, given the "positive" deployments of the plants of the AsurI rite I broadened the study to include other pharmacological properties. kaTuka amongst these seem to have been biochemical explored to a certain degree, so I considered it in greater detail. In course of this, I became aware of an independent work on it by a Dutch scholar HS Smit. Combining his studies with mine inspired this whole piece.

Both charaka and sushruta mention kaTuka as an important ओषधि required for the preparation of many of the drugs in their saMhita-s. In particular charaka recommends it in preparations against helminths and skin infecting agents, whereas sushruta recommends it against wound infection, skin infection and dental sinuses. This is in line with the ancient mention of it in the AV-P 7.19.3 where kaTuka is said to block the body-harming ArAti-s. While HS Smit observed that kaTuka does not play major role in modern anti-arthritis treatments, the classical Hindu medical text, the dhanvantri निघण्ठु describes kaTuka as आमघ्नी i.e a treatment for आमवात --- arthritis. The ancient and medieval Hindu medical theorists had studied arthritis in considerable detail and realized that one of the elements of its pathology was the substance they termed Ama, which corresponds to protein or urate deposits in modern parlance. Thus, the recognition of kaTuka as आमघ्नी by the DN suggests that medieval Hindu physicians had already discovered its potential in Arthritis treatment. This appears to be related to the earlier inclusion of kaTuka in preparations against rakta-वात in sushruta's saMhitA --- रक्तवात being a precursor to आमवात in Hindu descriptions of the pathology of arthritis.

So are any of these effects of kaTuka for real? Several groups have invested effort in studying the role of kaTuka and extract compounds from them. At least some of these modern experiments seem to support the Hindu theories regarding kaTuka:

 1.  Smit's experiments on rodent models showed that di-ethylether kaTuka extracts potently inhibited the complement pathway, had anti-inflammatory effects and delayed onset of auto-immune arthritis, though it was not useful once arthritis was already established.

 2.  Experiments also showed that it inhibited lymphocyte proliferation.

 3.  Just published experiments by Maheshwari's group, again in rodent models, showed that kaTuka extracts favor epithelialization and angiogenesis during wound healing.

The results 1 & 2 support the Hindu theory of it is use in inflammatory disease and as a protective against arthritis. Result 3 again supports the Hindu claim of its use in conditions like wound-healing and skin infections. Several interesting compounds are found in this plant like cucurbitacins, iridoid glucosides and phenolic derivatives like apocyanin, but the specific pharmacology of few of these are properly known. An iridoid glucoside aucubin might give rise to potent protein cross-linkers and has been implicated to have an immuno-modulatory role.

Thus, it is possible that the venerated kaTuka, like gulgulu and haridra, was a notable discovery of the भृगु-अङ्गिरस् of yore.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/Rz6SV1IKzHI/AAAAAAAAAQc/bEKeWbNCxEc/s320/kaTuka_aucubin.gif){width="75%"}
```{=latex}
\end{center}
```



Left (a kaTuka cucurbitacin); right aucubin (a kaTuka iridoid glucoside)


