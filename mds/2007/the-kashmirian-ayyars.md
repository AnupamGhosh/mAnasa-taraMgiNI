
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The \"Kashmirian\" ayyars](https://manasataramgini.wordpress.com/2007/03/02/the-kashmirian-ayyars/){rel="bookmark"} {#the-kashmirian-ayyars .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 2, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/02/the-kashmirian-ayyars/ "7:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While there is some evidence from the early श्रीविद्या and siddhAnta tantric सुन्दरनाथ (तिरुमूलर्) was probably from काश्मीर or at least had teachers from काश्मीर, we know little of him and his affiliation with other स्मार्त ब्राह्मण-s of the drAviDa country. There a few वडम-s (Northern स्मार्त-s of the drAviDa country) who believe that they are from काश्मीर. That they were from there is of not much doubt, but whether they form a single migration or came to Tamil Nad (and Andhra) in multiple migrations has not been properly investigated. While eventually we may have the opportunity to investigate these issue with genetic markers, we cannot be so sure given that brahminical tradition is fast eroding in the Tamil country. The स्मार्तस् are amongst the most rapidly degenerating communities via वर्ण sankara, patita सावित्र and other पातकस्.

Looking at inscriptions we find the following information:

 1.  The oldest layer of वडमस् from the tirunelveli region contain one group of Kashmirian immigrants. This group appears to have come around 750-800 CE and settled specifically in the अग्रहार-s associated with the tiruvalishvaram temple. A pallavan incription in this temple clearly attests the settling of these ब्राह्मण-s in Tirunelveli. This group was particularly important because they were critical in transmission of the कुब्जिका tantras and the rahasya-s of the पश्चिमानाय. कल्हण mentions that the confiscation of अग्रहारस् by the king जयापीड in the dynasty of ललितादित्य resulted in many ब्राह्मणस् fleeing and this incident might have been responsible for their southward dispersal.


 2.  The second layer appears to have been related to the dispersal of an illustrious lineage of Kashmirian ब्राह्मण intellectuals who contributed in different realms throughout length of the country. This appears to have happened in the 1000s of CE. This general dispersal included 1) उवट, the great vedic commentator who studied vedic grammar and the shukla yajur veda (court of bhoja); 2) बिल्हण the writer and poet who settled in the चालुक्य court in कर्नाटक 3) the father-son-grandson trio: भास्कर (minister), सोठल (prime minister) and शारङ्गदेव (author of the primary सन्गीत-रत्नाकर, the epitome of तौरीय गान) in कर्णाटक and महराष्ट्र during the यादव reign; 4) somashambhu- the illustrious siddhAnta tantric who settled in the Tamil country and wrote one of the primary manuals used in the temple worship of shiva and ritual performance of the mantra-मार्ग shaivas of the Urdhva-srotas. 5) श्रीनिवास भट्ट- a savant of श्रीविद्या who settled in the drAviDa country and initiated several lineages of ब्राह्मणास् into श्रीविद्या. The ब्राह्मणास् associated with the latter have been incorporated both amongst वडंअस् and शिवाचार्य in the TN and south-western Andhra.

In between there were the पाशुपत काश्मीराचार्यस् who settled in the south, but do not appear to have contributed to the gene pool to the best of my knowledge. The final wave appeared during the vijayanagaran rule. This group included several lineages settled during the viceroyship of लक्ष्मण दण्डनायक settled in northern TN. These clans included several vedic experts who served as a priest in sacrifices performed by luminaries like appaya दीक्षित. Some these learned the atharva veda in the south and returned to काश्मीर.

Some वडमस् connected to the लाट country might have also originally included a few immigrants from काश्मीर. There is a small community of वडमस् long-settled in Gujarat whose exact connection with these migrations, if any, remains currently unknown to me. It appears that भास्कर-rAya मखीन्द्र's guru was of this lineage.


