
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Pierced](https://manasataramgini.wordpress.com/2007/10/07/pierced/){rel="bookmark"} {#pierced .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 7, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/07/pierced/ "3:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It came in the dark hours of morning and it looked bad. It was almost as if everything else that was said before it was being negated. The way the खाण्डवन्'s operated after it made it clear that it was acting in their favor. It completely weakened us before them. There was no way out of it at all in this plane over the next few days. We were suddenly pushed to a tense position even as the अमात्य warned us about it.


