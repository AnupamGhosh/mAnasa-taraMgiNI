
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [chaotic flows](https://manasataramgini.wordpress.com/2007/04/16/chaotic-flows/){rel="bookmark"} {#chaotic-flows .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 16, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/16/chaotic-flows/ "5:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RiMNZ-uJIVI/AAAAAAAAAGU/0xBU520vFE4/s320/chaotic.jpg){width="75%"}
```{=latex}
\end{center}
```



Attractors based on the chaotic flow equations discovered by [J. Sprott](http://sprott.physics.wisc.edu/pubs/paper212.htm).

