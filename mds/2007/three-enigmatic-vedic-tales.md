
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Three enigmatic vedic tales](https://manasataramgini.wordpress.com/2007/02/11/three-enigmatic-vedic-tales/){rel="bookmark"} {#three-enigmatic-vedic-tales .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 11, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/02/11/three-enigmatic-vedic-tales/ "7:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RdAbBu8rbrI/AAAAAAAAADQ/tcBoYZm5q8c/s320/mythic_network.jpg){width="75%"}
```{=latex}
\end{center}
```



The यज्ञ went away from the devas in the form of विष्णु, and entered the earth. The devas searched him by digging with their hands. indra passed over him. विष्णु asked: "who has passed over me?" \[indra]: "I am he who smashes forts; who are you?". [विष्णु]: "I am the one who captures what is in a fort". विष्णु said: "You are the one who breaks forts. There is this boar, the stealer of beautiful things, that keeps the wealth of the asuras, which is to be won, beyond the seven hills. Hammer him, if you are the one who demolishes forts". indra plucked out a bunch of darbha grass, shattered the seven hills, and killed the boar. He then said to [विष्णु who was hiding under the ground]: "You are called he who captures what is within forts" so bring the wealth/boar. So विष्णु in the form of the यज्ञ brought the यज्ञ for them; in that they won the wealth of the asuras which was to be won.

\~*\~*\~

The asuras indeed at first owned the earth, the gods had so much as one seated on the ground can see. The gods asked the asuras: "May we also have a share in the earth?" \[The demons said]: "How much shall we give you?" \[The devas said]: "Give us as much as this hyaena can three times run round." The demons agreed. indra taking the form of a hyaena thrice ran round on all sides the earth. So the devas won the earth.

\~*\~*\~

The deva-s were defeated by the asuras. The asuras thought : "this world definitely belongs to us alone assuredly" They said: "Well then, let us divide this world between us; and having divided it, let us live on it". They accordingly started dividing it with ox-hides from west to east. The devas then heard of this, and said: "The asuras are actually dividing this earth. Come, let us go to where the asuras are dividing it. For what would become of us, if we were to get no share in it?" Placing Vishnu, this very यज्ञ, at their head, they went to the asuras. The devas asked: "Let us have share of this earth along with yourselves. Let a part of it be ours" The asuras replied grudgingly: "We will give you only as much as this विष्णु lies upon, and no more" Now विष्णु was a dwarf. The gods, however, were not put off by this, but said:" Indeed they have given us much, who gave us what is equal in size to the यज्ञ". Having then laid विष्णु down eastwards, they enclosed him on the sides with the Chandas, saying on the south side: "With the gAyatrI metre I enclose you"; on the west side: 'With the त्रिष्टुभ् metre I enclose you"; on the north side: "With the jagati I enclose you".

Having thus enclosed him on all directions, and having placed agni on the east side, they went on sacrificing and performing rites with it. By this [यज्ञ] they obtained this entire earth; and because they obtained by it this entire (earth). Thereupon विष्णु became tired; but being enclosed on all sides by the Chandas, with agni on the east, there was no escape route. He then hid underground among the roots of plants. The devas wondered: "What has become of विष्णु? What has become of the यज्ञ?" They said: "On all sides he is enclosed by the Chandas, with agni to the east, there is no escaping: search for him in this very place." By digging shallow they accordingly searched for him. They discovered him at a depth ofthree thumb-lengths below \[the ground].


