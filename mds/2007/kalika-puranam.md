
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कालिका पुराणं](https://manasataramgini.wordpress.com/2007/10/20/kalika-puranam/){rel="bookmark"} {#कलक-परण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 20, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/20/kalika-puranam/ "5:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

With the holy nine nights under way we decided to talk about the कालिका पुराण. It may be considered a mixture of traditional पुराण and a tantra शास्त्र manual. The पुराण part is dominant in the first 45 chapters of the text. The tantra-शास्त्र portion with a clear spatial connection to the yoni पीठ of कामाख्या is seen in the remain 45 chapters. Together, the 90 chapters contain 8394 shloka-s. The story frame is in the form of the ब्राह्मण-s questioning the भार्गव मार्कण्डेय, who in turn narrates the stories as his is usual custom. In the later half it becomes a discourse between sagara and his preceptor the भार्गव aurva.

Chapter 1 contains an account of the emergence of उषा and काम from brahmA.\
Chapter 3 contains a description of the charms of rati.\
Chapter 4 contains a description of vasanta.\
Chapter 5 contains a description of how विष्णुमाया is invoked by brahmA to delude rudra. She is described as योगमाया. This description is consistent with एकानंशा seen in the हरिवंश and they are the same continuous deity. She rides on a lion and has a deadly sword, is dark of complexion like a mass of collyrium and has untied, free-flowing long hair. She is hence काली and thus the पुराण acquires its name कालिका as it goes on describe her glories.\
Chapter 6 contains the account of shiva's गण-s and also has a notable stava of कालिका.\
Chapter 7-18 contains the सती cycle. It begins with rudra being primed for the life of a गृहस्त by काम, followed by दक्ष invoking महामाया to be born as his daughter सती and the marriage of सती to shiva. Then dalliance of shiva and सती, followed by the दक्ष याग events leading to the death सती and the destruction of दक्ष's याग. The final part narrates the emergence of the shakti-पीठ-s from सती-s अङ्ग-s. The peculiar twist of the kA.P is that brahmA, विष्णु and Saturn enter into सती-s corpse and break it up into pieces. The involvement of Saturn in the tale suggests its relatively recent provenance.\
Chapter 19-23 brahmA narrates various tales to rudra like that of arundhati the wife of वसिष्ठ, the conflict of दक्ष and chandra over his नक्षत्र daughters, origin of तीर्थ-s like चन्द्रभाग and shipra.\
Chapter 24-29 material typical of the sarga section of पुराण. Chapter 25 mentions the origin of the वराह.\
Chapter 30 contains a stuti of विष्णु, followed by shiva assuming the sharabha form to battle the वराह. sharabha defeats the वराह\
Chapter 31 contains the famous पौराणीc motif of the origin of various यज्ञाङ्ग-s from the different parts of the वराह (parallels an orthologous paragraph in many texts).\
Chapter 32-35 more sarga material, the मस्त्यावतर of विष्णु, episodes of unexpected pralaya-s, recovery of the world after such a pralaya and rudra relinquishing the sharabha form after saving the world from the वराह-s ravages.\
Chapter 36-40 The partial cycle of naraka: His birth from the वराह and पृथिवि, his coronation in प्रग्ज्योतिष, his tapasya to obtain power and his rise to stupendous power.\
Chapter 40-45 The पार्वती cycle: The birth of काली as पार्वती to हिमवान्, the attempt of कामदेव to delude shiva and his destruction, पार्वती's tapasya and encounter with shiva coming to test her and their marriage.

The uttara-काण्ड or the tantric manual section of the कालिका पुराण comprises the remaining part of the text. It is a collection of vidhi-s and prayogas fitted into the narrative of aurva to student king sagara.\
Chapter 46 The narrative of the emanations of bhairava-s and वेताल-s from rudra. It also contains a stava to शङ्कर.\
Chapter 47-50 The incarnation of shiva and उमा as chandrashekhara and तारावती. It narrates a bizarre tale of how shiva and पार्वती fought and पार्वती ran away from him. shiva saw sAvitrI and thought it was उमा and went to her full of passion. She cursed him to be born as a man. To get back to shiva, उमा incarnated as तारावती. However, chandrashekhara and तारावती did not remember that they were respectively the deva and देवी. A lecherous brahmin named kapota tried to have sex with तारावती when he saw her coming out of a river after a swim. She evaded him by sending her sister, but he figured it out and laid a spell on her that she would be violated by a hideous bone-ornamented being. She mentioned this to chandrashekhara, who tried to protect her by placing her on high terrace. But duly, rudra assuming his original form inseminated her and two terrifying वानर-headed भूत-s were born to तारावती. chandrashekhara thought that a demon had raped his wife but a ब्राह्मण informed them it was shiva and he himself was shiva. They closed their eyes and realized their सायुज्य with the shivau. But once they opened their eyes they continued life as a मानव-s. They had an additional 3 sons whom chandrashekhara favored over the original two वानर-faced sons. But it was they who became bhuta-गणेश्वर-s of rudra's hordes.

Chapter 51 The tantric vidhi of वसिष्ठ for the worship of the पञ्चब्रह्म मूर्ति-s of rudra.\
Chapter 52-56 The वैष्णवी tantra or महामाया-kalpa. Expounds the वैष्णावी महामन्त्र: OM ह्रीं श्रीं वैष्णाव्यै नमः | It also describes the worship of the आवर्ण-s of वैष्नवी with 64 योगिनीस् and 8 योगिनीस्. These योगिनीस् are distinct from those of other आवरण-s and include देवता-s like गुप्तदुर्गा, भुवनेश्वरी, सप्तकोटेश्वरी, विन्ध्यवासिनी etc (in the 64 circuit) and those headed by स्कन्दमाता in the 8-circuit. The animal and human sacrifices for the योगिनी-s of वैष्णवी are also detailed. 56 gives a देवी-kavacha.\
Chapter 57 Expounds the कामराज mantra as per the uttara-tantra.\
Chapter 58 Expounds the worship of महामाया-योगनिद्रा-एकानम्शा as the primary deity of कामाख्या.\
Chapter 59 चण्डिका पुजा vidhi. Describes the worship of चण्डिका with her आवर्ण of 7 terrifying योगिनी-s: उग्रचण्दा, प्रचण्ड, चन्डोग्रा, चण्डनायिका, चण्डा, चण्डवती, चामुण्डा.\
Chapter 60 दुर्गा tantra. Describes the महिषमर्दिनि पुजा performed on महानवमी.\
Chapter 61 The procedures for the worship of the 18 handed उग्रचण्डा on the अष्टमि, the 16-handed भद्रकाली on the नवमी of नवरात्री and the 10-handed jaya-दुर्गा on the vijaya-dashami day.\
Chapter 62 The कामाख्या-पुजा-tantra. Details the procedures for the worship of the goddess in the form of the great yoni at कामाख्या.\
Chapter 63 The त्रिपुरा-tantra. Describes the worship of बटुक-bhairava and त्रिपुरा along with her योगिनी-s as per the lineage of the श्रीकुल path.\
Chapter 64 The कामेश्वरी tantra. Expounds the worship of कामेस्वरी in the षट्कोण yantra in the midst of the triad of पीठ-s of जालन्धर, ओड्डीयान and कामrUpa as per श्रीकुल tradition.\
Chapter 65 The shAradA-tantra. Expounds the worship of the 10-armed lion-borne vajra-shAradA, especially in the context of the नवरात्री rite.\
Chapter 66 Descriptions of नमस्कार-s and mudras.\
Chapter 67 Descriptions of animal sacrifices. The alternative of making पिष्ट-pashu-s is also suggested and detailed (e.g. with barley flour).\
Chapter 68 Making of Asana-s for installation of मूर्ती-s of देवी-s. Metal, stone, wood, and hide Asana-s are described, as well as decorations with gems.\
Chapter 69 Making of vastra-s for the goddess from cotton, wool, silk and flax is described. Then the specifications for धूप (incense), दीप (lamps), flowers and अञ्जन-s (ointments) are provided.\
Chapter 70 The preparation of naivedya-s is described, with injunctions for what are bhojya.\
Chapter 71 The performance of circumambulations and the final नमस्कार-s are described. Thus, chapters 66-71 cover the details of षोडशोपचार पूजा for the shakti.\
Chapter 72 A sectarian praise of कामाख्या with an anti-विष्णु slant. The goddess गरुडगामी throws विष्णु into the sea, and by worshiping कामाख्या he is relieved of his troubles. The कामाख्या कवचं is provided.\
Chapter 73 The मातृका न्यास for कामाख्या worship method is detailed.\
Chapter 74 various mudra-s, yantras and prayogas for sundry purposes are described.\
Chapter 75 The पुरश्चरण, kavacha and पूजाविधान of tripura-भैरवी is provided here.\
Chapter 76 The description of the types of mantra-s and वसिष्ठ expounds कामाख्या पूजा.\
Chapter 77-79 A geographic dilation on the पीठ of कामाक्या and the holy spots in its environs.\
Chapter 80  A description of the worship of other deva-s, particularly concentrating on the worship of विष्णु as ordained in the पाञ्चरात्र tantras.\
Chapter 81 A bizarre tale regarding the prevalence of वामाचार worship is narrated: rudra was asked by वसिष्ठ to bring कामrUpa in the control of yama. shiva sent उग्रतारा and his गण-s to drive everyone out of कामrupa. In the process they tried to drive out the powerful ब्राह्मण वसिष्ठ too. He cursed the गण-s that they would become unclean म्लेच्छ-s and shiva would become म्लेच्छ-priya. He cursed उग्रतारा that she would only receive वामाचर worship. As a consequence the whole of कामrUpa was occupied by barbarous म्लेच्छ-s and the पाञ्चरात्र tantras were lost in the land. Instead the वामाचर tantras became the norm, which eventually bore fruit for its practitioners. The tale also mentions how lauhitya, the son of brahmA, flooded the region in the form of a river.\
Chapter 82 Another tale of the birth the river lauhitya as the son of shantanu and अमोघा. He was placed in a Himalayan lake but he grew in size and burst forth flooding the plains "like a second sea". His waters cleansed रामो भार्गव of the sin of killing his mother.\
Chapter 83 The narrative of the exploits of रामो भार्गव.\
Chapter 84-85 A description of राजधर्म and dharma शास्त्र. This section is commonly cited in स्मृति manuals from eastern India.\
Chapter 86 The important shAkta ablution and पूज performed on a तृतिय in the constellation of पुष्य is described.\
Chapter 87 The performance of the indra festival on the द्वादशि of the month of प्रौष्टपद is described. A medieval version of the vedic festival of the erection of the dhvaja of indra is detailed.\
Chapter 88 More राजधर्म and injunctions for a sacrifice to विष्णु on a ज्यैष्ठ dashami and a puja of लक्ष्मी on श्रीपञ्चमी are provided.\
Chapter 89 The tale of खाण्डव dahana. The tale of the sons of the bhairava.\
Chapter 90 A tale of the sons of the वेताल. The praise of the कालिका पुराण.

iti परिसमाप्तं


