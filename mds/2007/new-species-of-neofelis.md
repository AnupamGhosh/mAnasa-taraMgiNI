
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [New species of Neofelis?](https://manasataramgini.wordpress.com/2007/03/17/new-species-of-neofelis/){rel="bookmark"} {#new-species-of-neofelis .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 17, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/17/new-species-of-neofelis/ "4:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RfuDF8cE74I/AAAAAAAAAFw/4j4ZzYdURng/s320/clouded_leopard_old.jpg){width="75%"}
```{=latex}
\end{center}
```



R brought to my attention that there was a considerable press attention being give all of a sudden to two papers published late last year in the tabloid Current Biology by the cat experts O'Brien and Kitchener's groups. The essence of the two papers is claim that the clouded leopards ([Neofelis]{style="font-style:italic;"}) from Borneo are a distinct species. Since new large mammal species are not found so often the media appears to be making a big deal of it. But as R herself pointed me following up the references from Kitchener's classic [The Natural History of the Wild Cats]{style="font-weight:bold;font-style:italic;"}(one of R's favorite books), the Indonesian clouded leopard has been known to science since the first half of the 1800s, when it was described by naturalists in catalog of zoological specimens from Sumatra and the vicinity. In fact standard references on cats show that the genus [Neofelis ]{style="font-style:italic;"}has several sub-species under the species [nebulosa]{style="font-style:italic;"}: [N.n.macrosceloides]{style="font-style:italic;"} -- NE India and surrounding regions; [N.n.nebulosa]{style="font-style:italic;"} -Indo-china; [N.n.brachyurus]{style="font-style:italic;"} -- island of Taiwan; [N.n.diardi]{style="font-style:italic;"} -- Borneo, Malay Peninsular. Now it is this [N.n.diardi]{style="font-style:italic;"} that was elevated to a new species rather sub-species. Basically what the O'Brien's team using DNA sequences showed is that the variation between the Borneo form and [N.n.nebulosa]{style="font-style:italic;"} is in the same range as between the species of [Panthera]{style="font-style:italic;"}, such as lions, tigers, jaguars, leopards and snow leopards. Kitchener et al likewise showed that it was morphologically distinct from [N.n.nebulosa]{style="font-style:italic;"}, especially in coat color, as from as seen in the pictures. But does this warrant a separate species?\
Species is such a subjective yet intuitive concept in biology that all this merely in the eye of the beholder and dependent on what you really want to do with your species. Though the situation is much better than in the prokaryotes it is hardly iron clad in animals. Humans too show great morphological variation in different parts of the world and until the past few hundred years there was hardly any mating between several human populations-- e.g. those of Australia and the New World with the rest of the Old World. They are not termed different species, so why should the Neofelis populations be called species? By this token it is possible the Indian version and the possibly extinct Taiwanese version that apparently had a short tail unlike other [Neofelis ]{style="font-style:italic;"}types could well be new species too.

The main point worth introspecting in all this is bio-geography of the genus [Neofelis]{style="font-style:italic;"}--- Molecular phylogenies clearly show that it is the earliest branching of the Panthera lineage. The rest of the [Panthera ]{style="font-style:italic;"}lineage is distributed widely in the world: leo was found in Africa, Eurasia and America; pardus was found in Africa and Eurasia, tigris in Asia, uncia in the Asian highlands and onca in America. This distribution taken together with the distribution of [Neofelis ]{style="font-style:italic;"}suggests that [Panthera ]{style="font-style:italic;"}lineage possibly arose in Asia close to the western boundary of the range. I suspect that in some ways [Neofelis ]{style="font-style:italic;"}may be close to the ancestral condition of this lineage (some what saber-toothed). [P.leo]{style="font-style:italic;"} and [P.onca]{style="font-style:italic;"} separated in northern Asia with both shortly there after heading off into the Americas while [P.leo]{style="font-style:italic;"} alone developed a unique social system for cats and also spread westwards and downwards to Africa. On its westward movement it was accompanied by its relative [P]{style="font-style:italic;"}.[pardus]{style="font-style:italic;"}. In contrast, [P.tigris]{style="font-style:italic;"} appears to have radiated mainly in Asia occupying a niche similar to [P.onca]{style="font-style:italic;"} in the Americas. Their distribution might have also been shaped in part by the presence of other felid predators of the [Smilodon ]{style="font-style:italic;"}lineage. As [Neofelis ]{style="font-style:italic;"}diverged first but was fixed in an strongly arboreal niche, I am not surprised that it has been conservative on morphology, whiles its different populations have accumulated genetic divergence comparable the later radiating [Panthera ]{style="font-style:italic;"}genus.


