
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Worth coming back](https://manasataramgini.wordpress.com/2007/01/31/worth-coming-back/){rel="bookmark"} {#worth-coming-back .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 31, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/31/worth-coming-back/ "6:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RcGNNGQtsJI/AAAAAAAAACg/ng7ZELJO_KQ/s320/sahasrara.png){width="75%"}
```{=latex}
\end{center}
```

\
```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RcGPA2QtsLI/AAAAAAAAACw/pNHOltntt8A/s320/sahasrara_green.png){width="75%"}
```{=latex}
\end{center}
```



She said: "We return to that halcyon year and a half. When the son of Brinda was toiling relentlessly, fueled by testosterone and goaded by his crafty, parents we set out the seek the truth. Beyond the row of 3 शूद्रस् were two: one a शूद्र and one a ब्राह्मण."\
I said: "We return indeed to that time in our walk. We were free, like never before and never after. We knew not fear like जटायु and सम्पाती reaching for the sun. It was then that I standing on the broken wall of the larger कन्दूक prastha. I saw you strolling into to smaller कन्दूक prastha and headed downwards to meet you. With you were 4 others so we exchanged words in code as कापालिक-s do."\
She said: "Yes indeed! The वात is blowing with pitiless fury. Sorrow and joy are but two pans of the balance. The wielder of the thunderbolt holds it aloft. Today into one pan he has cast a weight tomorrow in another he shall."

We mounted our gardabha-s and moved towards the base of the imposing pinnacle of कौन्डिन्य where the warm river washed the banks. With our legs in the shallow water we looked at the blue skies. We felt that link with bhArata, the land conquered by our ancestors, the waters, the sky, the mountains. We thought of the melody "somyam maa.." "hi-indrA..." came to the mind. We were talking of early archosauromorph cranial anatomy and of pterosaurs --- so much of what we knew was to change. We feared that the unholy कीकट or the harmful dasyu would break into our reverie. So we together drew that yantra with the feather and placing a feather on it invoked kandhara, to repel राक्षसस्. We also cast around us the यक्ष-s and यक्षिनी-s of dreadful क्षुद्र विद्या-s like विरोधिका, ऋतुहारिका and दुःसह, so that none may challenge us.

Thus, repelling the anindra-s we moved on experiencing the rasa-s that break down when emerging as words. We came back by passing through the सेना-prastha, beside the विघङ्गमीर, the dasyu-शाल and the cleft in the mountain. We then enjoyed महाभोग of bhojana and having refreshed ourselves we proceeded to perform our studies on the water. We saw that yonder world before us. May we be conquerors of that world with all its beings we wished.... We still wish. We spoke again through the veil that separates us. We connected with गुह्यसोमा and lost it again.


