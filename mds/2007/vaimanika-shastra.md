
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [वैमानिक शास्त्र](https://manasataramgini.wordpress.com/2007/07/23/vaimanika-shastra/){rel="bookmark"} {#वमनक-शसतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 23, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/23/vaimanika-shastra/ "5:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As kids we had a fascination for the वैमानिक शास्त्र. Our companions द्विपक्ष-kesha and कलशजा were endlessly fascinated by it. Later it almost became a refrain of ridicule used to deride Hindus who were sympathetic towards the knowledge of their ancients. It was used a strawman to argue that every claim of traditional Hindu knowledge was a hoax. Goaded by R and N we were keen to find out the reality about this unusual and notorious text. Our initial investigations several years ago the on वैमाक्निक शास्त्र led us to an edition of it by GR Josyer (This text has now been made available from the Sacred Texts Site). We were given a clue in the early 2000's by a knowledgeable Hindu author named Vishal Agrawal that a second transcript of it existed in the Royal library of Baroda. However, he was unable to furnish any further details, saying merely he had read so somewhere. Following on this with R and N we discovered a second earlier publication of the same text by the Arya Samaj of Dayanand and obtained a copy of it. Not surprisingly the Arya Samaj claimed that it was a proof for Dayanand's assertion that all knowledge lay in the vedas. However, an examination of the Arya samaj edition shows that it was not based on Josyer's transcript, but based on the Baroda transcript.

Thus, we have have the following history of the transcripts:

 1.  As per Josyer we learn that the स्मार्त paNDita, सुब्बराय shAstrI was a learned, poor ब्राह्मण from South India. He claimed to have "seen" an lost text titled the वैमानिक शास्त्र, fragment of the yantra sarvasva by भारद्वाज and began dictating it. A certain वेंकटाचल sharman copied this down in notebooks between 1918-1923. Eventually Josyer, a संस्कृत paNDita from Mysore got hold of these notebooks and published it with a translation in 1973.

 2.  A transcript of the text was made from an unknown source in 1918 and deposited in the Baroda Royal library.

 3.  Transcripts from Pune (I do not know where they are currently housed, but photographed by Arya Samaj and stored with them) have the marking "transcribed by go. वेंकटाचल sharman in 19/8/1919 and 3/6/1919" on them. The latter two transcripts were used in making the Arya Samaj edition of which Josyer seems to be blissfully unaware.

Now all the confirmed transcripts, while widely distributed over peninsular India, appear to date roughly from 1918 earliest. These point to a common source, which could be either सुब्बराय shAstrI or someone from whom he obtained it in turn. We may note that the around WWI the importance of air force and aviation was on the rise and catching popular imagination. It is possible that सुब्बराय shAstrI, a traditional Hindu, might have seen the parallel between the newly invented airplanes and the descriptions of विमानस् in old Hindu lore. So this might have inspired him to think in terms of a शास्त्र that Hindus might have had to make those विमानस् described in their lore. Josyer also mentions that सुब्बराय shAstrI had friend named सूर्यनारायण rao who had some interest in physics. This man published some journal on scientific topics in which सुब्बराय shAstrI wrote. It is not surprising if they developed an interest in airplanes and the like together, with rao providing shAstrI some rudimentary ideas about physics.

Some points of note:\
Even though it is termed a शास्त्र in reality it is structured unlike any traditional शास्त्र. While composed in shloka meter it actually contains sUtra-s attributed to भरद्वाज and a commentary on the सूत्रस् by a certain बोधानन्द. The sUtra-s themselves are very vague with most specificity coming from the commentary part. This is not true of genuine सूत्रस् (e.g shrauta sUtra-s or darshana मूल sUtra-s), which while laconic are not entirely obscure. The बोधानन्द वृत्ति part is laden with citations of many texts, the most important of which are never mentioned in any other Hindu work and are not found in manuscript form anywhere in India. Examples of these are: vimAna-चन्द्रिका, वाल्मीकि गणित, व्योमयान-tantra, yantra-kalpa, yAna-bindu, kheta-yAna प्रदीपिका, व्योमयानार्क-प्रकाश, amshu-bodhini, yantra-अङ्गोपसम्हार, ऋक्-हृदय etc. This is the main point which makes the whole वैमानिक a work of fiction rather than anything even close to a genuine piece of traditional knowledge. The invocation of shiva at the beginning and subsequent description of shiva as the founder of vaimanika vidya, suggests that shiva was the author's personal देवता.

The text is aware of tAntric prayoga-s for the purpose of flights, such as गुटिक-s and पादुक-s, which have a long history in India. But it does not detail any such prayoga-s other than mentioning a few by name like छिन्नमस्ता, भैरवी and भुवनेश्वरी. The author was clearly familiar to a certain extent with tAntric practices. The text does not have any principles of aerodynamics or physics, but is of a descriptive kind with long lists and enumerations. The author seems to follow the tantra and पुराण-s in a general sense in listing ingredients for vimAna making. In one place in describing the extraction of metal he lists: गून्जा, कङ्जल, tajadabhanga, कुञ्जर, and करङ्ज, prANa-kshara, hingoo,पर्पट, घोण्टिका, जटा-माम्सी विदाराङ्गिनि, and मत्स्याक्षि as plant material with which the metal is heated. The author has a vivid imagination enumerating devices like cameras and solar power, but not really providing constructional details in many cases. All in all these issues make it a piece of Hindu science fiction, albeit finding expression in a peculiar fashion as a शास्त्र.


