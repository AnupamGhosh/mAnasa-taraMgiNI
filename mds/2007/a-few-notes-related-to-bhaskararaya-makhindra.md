
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A few notes related to भास्करराय makhindra](https://manasataramgini.wordpress.com/2007/04/16/a-few-notes-related-to-bhaskararaya-makhindra/){rel="bookmark"} {#a-few-notes-related-to-भसकररय-makhindra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 16, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/16/a-few-notes-related-to-bhaskararaya-makhindra/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier reconstructed a narrative on the illustrious भास्करराय मखीन्द्र's life using रघुनाथ paNDita edition of the great man's biography by jaganनाथ paNDita. jaganनाथ paNDita's clan hailed not far from where we spent a good part of life and our mind wandered to those regions. We have always felt a special feeling on entering the two temples of the dreadful विनायक associated with them \[In the battle of दशान्त we received aid from ekadanta and our prayoga bore full fruit]. रघुनाथ paNDita states that there are 5 commentaries on the ललिता सहस्रनामम् in vogue: 1) that of विमर्षानन्द-नाथ the disciple of विमलानन्द-नाथ. 2) that of विद्यारण्य yati the student of अनन्तारण्य. 3) नारायण भट्ट's commentary. 4) That of a certain शङ्कर. 5) The सौभाग्य भास्कर of भास्करराय.

His biography is clear in stating that shiva-datta shukla, the madhyandina ब्राह्मण from Surat conferred on him पूर्णाभिशेक-\
[शिवदत्त शुक्ल-चरणासादित पूर्णाभिषेक-साम्राज्यः ।]{style="color:#99cc00;"}\
[गुर्जर-देशे विदधे जर्जर-धैर्यं स वल्लभाचार्यं ॥]{style="color:#99cc00;"}


