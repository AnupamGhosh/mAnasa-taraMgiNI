
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The battle of chatur-इषवः](https://manasataramgini.wordpress.com/2007/05/30/the-battle-of-chatur-ishavah/){rel="bookmark"} {#the-battle-of-chatur-इषव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 30, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/30/the-battle-of-chatur-ishavah/ "5:54 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our powerful and vicious भ्रातृव्य-s were bearing down on us in the battle of 4 arrows. But indra has conferred victory on us by sending his mAyA to cloud them and placing the vajra behind us!

You two heroes who trample the dasyu-s,\
the wide-strider who is the plunderer of forts,\
and the thunderer who shatters forts,\
delight in the full chalice of heavenly soma.

The two lords of soma! strike forth!\
Sink our भ्रातृव्य-s and raise us!\
O hero with your 3rd step send the hemispheres apart,\
make space for the vajra-bearer to strike.

With what heroism you slew kuyava,\
with what valor you killed the spidery और्णवाभ,\
with what fury you hurled shambara off a cliff,\
with that aid us in the thick of battle.


  - +-+-

We saw a remarkable sight- it was raining with thunders from great heights, but above the प्राच्य मयूख the moon was shining brightly at the same time. This is the उपाम्शु graha of soma. We uttered the वौषट्-s and with that performed a soma याग. indra and विष्णु had drunk the heavenly soma.


