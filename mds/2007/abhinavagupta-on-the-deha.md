
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [abhinavagupta on the deha](https://manasataramgini.wordpress.com/2007/10/11/abhinavagupta-on-the-deha/){rel="bookmark"} {#abhinavagupta-on-the-deha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 11, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/11/abhinavagupta-on-the-deha/ "5:23 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I was reminded of a statement of abhinavagupta in the तन्त्रालोक due to an exchange with a compatriot on the reason why matysendra is such a colossus in ancient thought.

he says:\
[एवं विश्वाध्व संपूर्णं काल-व्यापार-चित्रितं ।\
देश-काल-मय-स्पन्द-सद्म देहं विलोकयेत् ।\
तथा विलोक्यमानो ऽसौ विश्वान्तर्देवता-मयः ।\
ध्येयः पूज्यश्च तर्प्यश्च तदाविष्टो विमुच्यते ।]{style="color:#99cc00;"} (तन्त्रालोक 12.6-7)

Thus, the body is perceived as full of diverse pathways, marked by time-dependent processes (lit. temporal processes), and as the seat of vibrations in space and time. The \[body] perceived thus is composed of all देवता-s and meditated, worshiped and pacified \[quenched with तर्पणं-s]. He who enters that \[the body] is liberated.

This characteristically enlightened expression of abhinava to me represents an expression of the thought-stream of mIna. But was it entirely new ? Not really. Like many other things it goes back to vedic thought. For example the उपनिषद्'s statement on indra-yoni in the pineal is an early version of the same.


