
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Basal theropod phylogeny](https://manasataramgini.wordpress.com/2007/10/06/basal-theropod-phylogeny/){rel="bookmark"} {#basal-theropod-phylogeny .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 6, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/06/basal-theropod-phylogeny/ "5:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The base of the theropod tree has been shaky despite major advances in understanding avian origins and the evolution of coelurosaurs. A part of the problem has been the poor Triassic and early Jurassic record of the theropods. A key to unlocking the phylogeny of early theropods was a spectacular find from the Early Jurassic fossil beds of Antartica, [Cryolophosaurus]{style="font-style:italic;"}. Finally, a long needed osteological study of [Cryolophosaurus ]{style="font-style:italic;"}was conducted by ND Smith et al which offers and extraordinary view of theropod evolution. The early phylogenetics studies of theropods hinted suggested that there were two major clades within them the ceratosaurs and the tetanurans. Further, staurikosaurids and [Eoraptor ]{style="font-style:italic;"}were considered basal-most clades of theropods. But this has been questioned over the years with newer analysis of the "ceratosaurs" and the discovery of new fossils like the well-preserved [Zupaysaurus ]{style="font-style:italic;"}from South America and the fragmentary [Dracovenator ]{style="font-style:italic;"}from South Africa. The new analysis by Smith et al incorporates data from [Cryolophosaurus ]{style="font-style:italic;"}and improves the picture vastly.

In a nutshell it makes the following points:

  - - [Eoraptor ]{style="font-style:italic;"}and the staurikosaurids are basal saurischians rather than theropods.

  - - The basal-most theropod clade is a coelophysoid clade comprised of forms like Liliensternus, Coelophysis, Syntarsus and Segisaurus.

  - -[Zupaysaurus ]{style="font-style:italic;"}is a sister group of all other remain theropod groups.

  - Of the remaining theropods the basal-most clade is the dilophosaurid clade formed by [Dilophosaurus, Dracovenator, Cryolophosaurus]{style="font-style:italic;"}, and "Dilophosaurus" sinensis from the Lufeng formation.

  - The next clade which forms a sister group of the tetanurans is the neoceratosaurid clade which includes within it [Ceratosaurus, Elaphrosaurus]{style="font-style:italic;"} and the abelisauroids.

  - The basal-most tetanuran clade unifies the two south American forms [Condorraptor ]{style="font-style:italic;"}and [Piatnitzkysaurus]{style="font-style:italic;"}.

  - The major clades within tetanurae are: spinosauroids (including spinosaurids proper and [Eustreptospondylus]{style="font-style:italic;"}, [Torvosaurus ]{style="font-style:italic;"}and [Afrovenator]{style="font-style:italic;"}); allosauroids (including sinraptorids, carcharodontosaurids and Megaraptor, which emerges as a carcharodontosaurid); coelurosauria.


