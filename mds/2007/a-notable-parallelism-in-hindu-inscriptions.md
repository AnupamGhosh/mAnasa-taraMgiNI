
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A notable parallelism in Hindu inscriptions](https://manasataramgini.wordpress.com/2007/07/20/a-notable-parallelism-in-hindu-inscriptions/){rel="bookmark"} {#a-notable-parallelism-in-hindu-inscriptions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 20, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/20/a-notable-parallelism-in-hindu-inscriptions/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After the whirlwind of Islam under the dreadful Alla-ad-din Khalji had blown through India and flattened the Hindus, one of the first Hindu fight-backs began under Prolaya Vema Reddi and the Kamma chiefs Kaapaya Nayaka and Prolaya Nayaka in the Andhra country. In the North, in the 1400s a significant figure in rolling back the horrors of Islam was the great Rana Kumbha of Mewar. He pounded the Moslem tyrants of Gujarat and Malava. One thing that caught my attention was a striking parallelism in the language of the two Hindu liberators in describing their victories against the Moslems.

Prolaya Vema Reddi's inscription of Kondavidu remarks: "I am indeed an Agastya to the ocean which was made of the Muslim". He uses the phrase "म्लेच्छाब्दि (Moslem ocean) kumbhodbhava (the pot born)" referring to the two mythological elements: 1) Along with manA and वसिष्ठ agastya was born from a pot (=उर्वशी) into which was discharged the semen of mitra and वरुण. 2) When the asura-s fleeing from indra and other deva-s hid in the ocean, agastya drunk up the ocean to render the asuras vulnerable to the deva attack.

The famed kirtistambha of Rana Kumbha was erected to commemorate his victories over various Moslem rulers like those of Gujarat and Malava. It contains a long inscription describing these victories. In verse 171 there is a play on kumbha's name and describes him as the pot-born agastya who drunk up the armies of the सुरत्राणस् (लट एवं मालव सुरत्राणानां सेनाह्).

These parallels represent the remarkable cultural unity of Hindu expression even in a period when the Islamic violence fragmented the land.


