
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Our lessons from the subversion of Russia](https://manasataramgini.wordpress.com/2007/04/15/our-lessons-from-the-subversion-of-russia/){rel="bookmark"} {#our-lessons-from-the-subversion-of-russia .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 15, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/15/our-lessons-from-the-subversion-of-russia/ "4:59 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Begins [here](https://manasataramgini.wordpress.com/2007/04/14/the-subversion-of-russia/)\
This alliance of eka-स्वामिन्-s is the very same one operating against the deva-yajvan-s of bhArata. To begin to "see" this one must also ask questions like:

  -    Where did the Sikh terrorists who massacred Hindus get shelter and who gave them diplomatic support?

  -    What passport did the Moslem terrorist mastermind released during the IC 814 hijacking carry?

  -    When the "big cracker of Pokhran" and "Gujarat" happened who were the responders inside the desh receiving orders from the eka-svamin masters of navyarkapura or gaja-लण्डपुर? Who were tarred during these events?

  -    Why do these responders within the desh bleed in their hearts for ekasvamin-s but are mute as vacuum when Hindus are killed?

Some of this was murky to us when we were in the desh but became clear when we settled beside the Brazos river in क्रौञ्च द्वीप, just like our classmates, for the sake of artha. There the Avatsara and the आमावसव disrupted, most dramatically, the meeting of the तुरुष्क-s. In its aftermath we saw the open advertisements in the library supporting the तुरुष्क-s in their campaign to ravage काश्मीर, the land our Agamas.

The whole campaign has run in parallel with respect to Russia and bhArata. Our masses that rose up during the struggle against the then leaders of the Anglosphere suddenly went quiet with freedom and a new crop of history books appeared on the scene. Our maha-अमात्य चचाजि had already been subverted to socialism and with him the whole governance was subverted that way. An Islamic caliphate in the form of the terrorist state of Pakistan was created to deliver the other pincer grip.

The Hindu society always had a bicameral system of elites: ब्राह्मण-s and kShatriya-s. These elite were in part separated from economic enterprise and wealth generation-- the domain of the vaishya. The whole edifice stood on the base of the generalist शूद्र who could adapt to provide what ever kind of manufacturing base that was required. When the original kShatriya-s fell, the शूद्र-s arose to provide a new generation of honorary kShatriya-s. But the bicameral elite system was maintained- ब्राह्मण-s and the new शूद्र elite. This bicameral system was very important because it acted as an intrinsic system of checks and balances. The separation of the elite from wealth generation also did not concentrate all types of power in one center allowing Hindu society to survive assaults from its enemies \[A digression on this point: When Maharana Pratap rose to defend Hindus against the Moslem assault he was funded by the independently rich vaishya Bahman Shah. In addition to contributing an enormous wealth for the Hindu struggle Shah and his brothers personally joined Pratap's army and fought the Moslems. Thus, eventhough Chittor was taken and looted by the Moslems (the kShatriya wealth), the Hindus were able to sustain their struggle]. The army was secularized and made bereft of an ideology. No army can fight glorious battles if the only ideology it has is career advancement or regimental honor beyond a certain point. This nullified the kShatrIya dharma in the whole national army. So strong are the secular traditions in the army that people are afraid to think of it being ideologically indoctrinated into a Hindu force.

On the other hand the demonizing of the ब्राह्मण is only too well known to dilate upon here. Thus, for the first time the Hindu system with all its checks and balances has been breached: -The first step in this subversion was socialism (used to discredit ब्राह्मण-s especially and denude the Hindu religion; cf. Russian socialist revolution).

  - The parallel step was secularization of the army --- thus as we discussed before the India army's record is not the best (cf. the ideologically rudderless Russian army).

  - Stabs via Sikh and Moslem terrorists (cf. Moslem terrorist action on Russia).

  - Discredit national politicians and foster economic dependency, or economic despair.

The underlying theme in all this is the alignment of the eka-svamin powers, despite their internal squabbles, on each of these issues. While one may counter now Rus itself is an eka-svamin country, there are fine points that are lost in this argument. Ultimately, no eka-svamin power will come to terms with an independent bahu-svamin power center.


