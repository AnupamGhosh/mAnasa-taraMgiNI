
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Field of neurons](https://manasataramgini.wordpress.com/2007/01/15/field-of-neurons/){rel="bookmark"} {#field-of-neurons .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 15, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/15/field-of-neurons/ "7:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/Rasv49_ytoI/AAAAAAAAAA0/ww5v57oIR-E/s320/neurons.png){width="75%"}
```{=latex}
\end{center}
```



