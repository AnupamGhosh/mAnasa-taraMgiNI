
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Amniotes](https://manasataramgini.wordpress.com/2007/03/19/amniotes/){rel="bookmark"} {#amniotes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 19, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/19/amniotes/ "6:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/Rf4q8JLLu1I/AAAAAAAAAF4/o8dM87jHBIE/s320/amniotes.jpg){width="75%"}
```{=latex}
\end{center}
```



The "anapsids" and their place in reptilian evolution have long puzzled biologists. The first major advance in amniote phylogeny was because of the brilliant, but largely forgotten English zoologist [Edwin Goodrich](http://www.pubmedcentral.nih.gov/pagerender.fcgi?artid=1272740&pageindex=1#page), who showed that the basal division in the amniotes separated the theropsids (now synapsids) and the sauropsids (the reptiles including birds). DMS Watson also made several improvements to this original proposal of E Goodrich, especially with respect to early branching reptiles like the milleretids (millerosaurs) and procolophonids, but his scheme was damaged by his dogmatic adherence to Linnaean taxonomy. While these early attempts were actually capturing some real phylogenetic signal in the amniotes, it was not appreciated by many scientists due to the ascendancy of the Romer's un-natural typological scheme outlined in the classic work "Vertebrate Paleontology" (Despite its shortcomings I have some respect for this volume. When I read this work during a दीपावली vacation at the age of 6 1/2, it set the course for my future pursuits in life). The popular scheme of Romer was based on the temporal fenestra and artificially grouped all forms without fenestra together as the basal anapsids.

As a result of Romer's scheme, the idea that turtles were the only surviving representatives of the great Paleozoic radiation of anapsids was entertained till almost just a decade ago. As a result several investigators sought turtle origins amongst other anapsids-- Reisz believed they were related to procolophonids while studying [Owenetta]{style="font-style:italic;"}; Lee thought they were related to the parieasaurs; and others saw them as generalized "anapsids". However, this view was shaken by Rieppel's phylogenetic analysis that showed that turtles were diapsids. Modern molecular phylogenies strongly suggest that turtles are not just diapsids but belong to archosauromorpha and perhaps even archosauria proper. My own suspicion is that turtles might have even been derived from the basal archosauriforms. This put an end the Romerian fancies of fenestral classifications. Fenestra can be easily lost or gained in the amniote skull and are not such fool proof phylogenetic markers. In light of this the recent phylogenetic studies by Reisz and his camp are converging on a reasonably clear picture of early amniote evolution. The sister group of the amniotes proper including [Tseajaia]{style="font-style:italic;"}, [Limnoscelis ]{style="font-style:italic;"}and the widespread diadectids appear to be plainly anapsid.

The amniotes proper split, as envisaged by Goodrich, into synapsids on one side and reptiles on the other. In the synapsid clade right from the beginning we observe a single temporal fenestra and are yet to find any anapsid representatives. The reptilian clade splits up into parareptiles and eureptiles. The eureptiles include a crown diapsid clade with the archosauromorph and lepidosauromorph reptiles of today along with the basal sister groups which includes araeoscelids, younginiforms and perhaps forms like  [Coelurosaurvus]{style="font-style:italic;"}[ and ]{style="font-style:italic;"}[Palaeagama]{style="font-style:italic;"}. The two immediate sister groups of the diapsids are the ancient [Paleothyris ]{style="font-style:italic;"}and the captorhinids, both of which are anapsid. The parareptiles include an amazing diversity of forms like the aquatic mesosaurs with long needle-like teeth, the milleretids, the lanthanosuchids, bolosaurs (with the earliest bipedal animals like [Eudibamus]{style="font-style:italic;"}), procolophonids, nyctiphruretids, nycteroleterids and the pareiasaurs (which were large-bodied strongly armored herbivores). Many of these forms were anapsid, though there are the notable exceptions, like the milleretids, lanthanosuchids, [Acleistorhinus ]{style="font-style:italic;"}some procolophonoids ([Procolophon ]{style="font-style:italic;"}and [Candeleria]{style="font-style:italic;"}), and at least one nyctiphruretid ([Tokosaurus]{style="font-style:italic;"}), all of which might have had fenestra. This pattern would suggest that the temporal fenestra probably convergently evolved on multiple occassions in the amniote clades. Another possibility is that it was there in some form even in the ancestral amniote and closed on mulitple occassions early in amniote evolution and was strongly fixed only in the synapsids and later in the diapsids. A molecular developmental study of skull fenestration might go a long way in clarifying this problem --- but I simply do not have the resources to undertake it by myself.


