
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Australian entomofauna and arachnofauna](https://manasataramgini.wordpress.com/2007/03/08/australian-entomofauna-and-arachnofauna/){rel="bookmark"} {#australian-entomofauna-and-arachnofauna .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 8, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/08/australian-entomofauna-and-arachnofauna/ "7:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R and Marc had returned after long and were having a chat along with Sharada even though we should have been doing work. After all we are very like the Hindu kings who squandered their days in bhoga even when the तुरुष्क blizzard was blowing around them. We had many things to talk-(1) The mysteries of genetic relatedness of European, Indian and Central Asian populations and their implications for much earlier human migrations- like the clash between H.sapiens and H.neanderthalis. (2) The निःश्वास-tantra and the tantric discussion of ऋचीक and मतङ्ग (3) The synthesis of various alkaloids and the effects of DMT and other active amines. (4) The entomofauna and arachnofauna of Australia. R had spent an year and half studying these in the earlier days and was well familiar with them. She had a collection of documentaries on them some of which provide striking illustrations of some examples touched upon by T.Eisner in his book.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/Re-6ak4hNtI/AAAAAAAAAEo/qy-fvoe4CVw/s320/cicada.png){width="75%"}
```{=latex}
\end{center}
```




