
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [1998 redux](https://manasataramgini.wordpress.com/2007/09/01/1998-redux/){rel="bookmark"} {#redux .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/01/1998-redux/ "5:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

14th July 2007\
It looked as though the खाण्डवन्स् have launched the biggest attack yet. To put it simply it was devastating. The intelligence agents reported that there were only two paths leading from the point of the original encounter. The deficiencies on the kosha front would force a comparable encounter irrespective of the path chosen, sooner or later. Before we could rally our troops we saw the vile खाण्डवन्स् attack every where. They broke into our at two points treasury and were stealing our dhana. They attacked 3 of our prime allies and prevented them from merging with our forces. Then came the 1998 redux on the kosha front ! Truly the decoration on the ghost from the past! That more or less diverted our most of our sena which was in fighting : we deployed "haridra" on that front. That कृत्या of the खाण्डवन्स् moved on us like that प्रत्यञ्गिरस कृत्या of rudra approached ushanas काव्य. She was most horrid and terrifying --- her very sight sucked out our valor like a spider sucking out the juices of its victim. She caused us to lose our mind, even as we watched it slowly desert us, fully aware of the situation. She broke the power of our yoga like rudra utterly consuming that of काव्य. Our powers became submerged and we were unable to command our rudderless men. In the chaos of battle like the asura-s looking for shukra they came to us. But with our luster lost like that of agni before खाण्डव-dahana, we were unable to lead them against the surging खाण्डवन्स्. The खाण्डवन्स् who kill the very people who help, hurled flames at them in an attempt to extirpate them. The खाण्डवन्स् stole our money, bored into us like a malarial parasite making its way to the victim's liver and left us tossing like a ship heading towards wreckage in a storm. Like the tubercular actinomycete or an infestation of [Entamoeba ]{style="font-style:italic;"}or a simply a persistant dog that never releases its bite they refused to go away. We ran from pillar to post like madhurai नकीरर् trying to escape the wrath of the The god, but from every nook and cranny emerged the hideous face of the खाण्डवन् कृत्या threatening to swallow us like a krill entering a whale's maw. Thus, routed by the खाण्डवन्स् with our kingdom and triple wealth in disarray, with no deva or asura bearing us aid, we fled into the house of JC and SB to spend the night.

...\
13th March 2007\
9th June 2007\
...\
We much terror we remembered the visitation that came upon Charlotte on July 4th 1998. We had to go see Charlotte and return to our क्षेत्र within a matter of 1/2 hour, including a wait of 8 minutes at Charlotte's place. It was then that we were ambushed by that --- never was it more terrifying. We felt exactly like how दक्ष would have felt when when he saw वीरभद्र roaring into to his याग शाल from the skies accompanied by a hoard of pramatha-s and भूत-s. I returned to my क्षेत्र and 4 minutes latter drove away on the ratha with ekanetra and पर्शुदाढिक. 10 minutes later we rendezvoused with R. She who is like उर्वशी and having the mana radiating like a Maori queen (OK now don't kill me -- had to say it) had dismounted from her ratha and having lowered her trademark sunshades was pensively fiddling with them. She saw we uncouth ruffians dismount from our ratha and signaled to follow her ratha. ekanetra became frantic asking for ST, when I pointed out that she was there and seated next to R. I had forgotten about the ambush and was in the the exultant company of my fellows. We stopped at Hayasthanika's residence and moved in. पर्शुदाढिक and पिष्टमुख, as befitting their stations, started swilling liquors with Hayasthanika and her boisterous friends. ekanetra and me were huddled in one corner in making silent anthropological observations. R came up to us but she avoided eye contact still wearing her dark shades in the dim lit room of Hayasthanika. She asked to come out into the lawn and we did so. She raised her shades in the dark evening and made a statement (in संध्य भाष): "The seeker of the penguin and one who holds the skull of the phorusrhacid will flank you."\
I: "What a glorious moment that would be".\
R: "It will be without dimensions and remember nightmares can come true".\
I: "Nightmares?".\
R: "When we visited Charlotte's den, we felt the presence".\
I: "It will pass."\
R: "You will be tested in battle, and these are not going to be the mock battles we simulated lazing atop the वानर parvata or the hill of चण्डिका. This will be the real world".\
I: "But, when did you learn of these battles of the real world?"\
R: "These years when we did not see each other?"\
I: "But you never spoke of this when we were on the long drive by the Brazos"\
R: "Why spoil the mirth when we have the chance to experience it?"\
I: "When will the yuddha commence?"\
R: "It has begun today!"

These last words missed my attention then.

ekanetra dropped me off near the upward slope and drove away on his ratha. I walked away with my mind filled with pleasant anticipatory thoughts for the night. But what we saw was a fearsome dream, so much so that we really woke up in a sweat. A week and then a month passed we knew the ग्राहि was upon us. We were being borne away by the अरिष्टस् that followed like one attempting cross the river of yama. What seemed so close to us was now like on a different galaxy.

वरुण has brought us aid freed us from that deadly pasha.\
विष्णु revealed the mystery of the haridra prayoga.


