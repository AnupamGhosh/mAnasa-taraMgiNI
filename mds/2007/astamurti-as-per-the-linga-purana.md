
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [अSटमूर्ति as per the लिङ्ग पुराण](https://manasataramgini.wordpress.com/2007/11/21/astamurti-as-per-the-linga-purana/){rel="bookmark"} {#अsटमरत-as-per-the-लङग-परण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 21, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/21/astamurti-as-per-the-linga-purana/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

nandikeshvara in the लिङ्ग पुराण narrates the nature of the अष्टमुर्ति to सनत्कुमार. It records a unique shaiva tradition on the nature of the अष्टमूर्ति and their significance

[1) sharva :]{style="font-weight:bold;font-style:italic;"}\
चराचराणां भूतानां धाता vishvaMbharAtmakaH\
sharva ityuchyate देवः sarva-शास्त्रार्थ-पारगैः 2.13.3 ||\
विश्वंभरात्मनस्-tasya sarvasya parameShThinaH\
विकेशी kathyate पत्नी tanayo .अङ्गारकः स्मृतः 2.13.4 ||

wife=विकेशी; son=अङ्गारक, i.e. planet Mars; The lord of all and the entity pervading the universe.

[2) bhava :]{style="font-weight:bold;font-style:italic;"}\
bhava ityuchyate devo भगवान् veda-vAdibhiH\
saMjIvanasya लोकानां bhavasya परमात्मनः 2.13.5 ||\
उमा संकीर्तिता देवी सुतः shukrashcha सूरिभिः\
sapta-लोकाण्डक-व्यापी sarva-lokaika-रक्षिता 2.13.6 ||

wife=उमा; son=shukra, i.e. planet Venus; envelops the 7 world-spheres; protects the universe, infuses life in the universe.

[3) pashupati:]{style="font-weight:bold;font-style:italic;"}\
वह्न्यात्मा भगवान् देवः स्मृतः pashupatir बुधैः\
स्वाहा पत्न्यात्मनस्-tasya प्रोक्ता पशुपतेः प्रिया 2.13.7 ||\
षण्मुखो भगवान् devo बुधैः putra udAhR\^itaH\
samasta-bhuvana-व्यापी भर्ता sarva-शरीरिणाम् 2.13.8 ||

wife= स्वाहा; son= षण्मुख; envelops the universe; protects all life-forms, is manifest as fire.

[4) ईशान:]{style="font-weight:bold;font-style:italic;"}\
पवनात्मा budhair deva ईशान iti kIrtyate\
ईशानsya jagat-kartur-devasya पवनात्मनः 2.13.9\
शिवा देवी budair-उक्ता पुत्रश्चास्य manojavaH\
चराचराणां भूतानां सर्वेषां सर्वकामदः 2.13.10

wife= शिवा; son= manojava; the substance of living and non-living forms; confers all wishes, is manifest as air.

[5) bhIma:]{style="font-weight:bold;font-style:italic;"}\
व्योमात्मा भगवान् devo bhIma ityuchyate बुधैः\
mahAmahimno devasya भीमस्य गगनात्मनः 2.13.11\
disho dasha स्मृता देव्यः सुतः sargash-cha सूरिभिः\
sUryAtmA भगवान् देवः सर्वेषां cha विभूतिदः 2.13.12

wives= 10 directions; son= sarga; envelops the universe; basis of all manifestations in the universe, is manifest as space and as the sun.

[6) rudra:]{style="font-style:italic;font-weight:bold;"}\
rudra ityuchyate devair भगवान् bhukti-muktidaH\
sUryAtma kasya rudrasya भक्तानां bhakti-दायिनः 2.13.13\
सुवर्चला स्मृता देवी सुतश्चास्य shanaishcharaH\
samasta saumya वस्तूनां प्रकृतित्वेन विश्रुतः 2.13.14

wife= सुवर्चला; son= shanishchara, i.e. Saturn; gives liberation and pleasures; cause of devotion in devotees, present as the nature of all pleasant things, is manifest as the sun.

[7) महादेव:]{style="font-weight:bold;font-style:italic;"}\
सोमात्मको budhair devo महादेव iti स्मृतः\
somAtmakasya devasya महादेवस्य सूरिभिः 2.13.15\
dayitA रोहिणी प्रोक्ता budhash-chaiva sharIrajaH\
havya-कव्यस्थितिं kurvan havya-कव्याशिनां तदा 2.13.16

wife= रोहिणि; son= budha, i.e. Mercury; bearer of offerings to gods and ancestors, is manifest as the moon;

[8) ugra:]{style="font-weight:bold;font-style:italic;"}\
यजमानात्मको devo महादेवो बुधैः prabhuH\
ugra ityuchyate sadbhir ईशानश्चेति चापरैः 2.13.17\
ugrAhvayasya devasya यजमानात्मनः prabhoH\
दीक्षा पत्नी budhair उक्ता saMtAnAkhyaH sutas-तथा 2.13.18

wife= दीक्षा; son= saMtAna; manifests as the ritualist.

The concept of the अष्टमूर्ति-s of rudra go back to the vedic period: 1) In the गृह्य rite of शूलगव. The 8 oblations to rudra in the शूलगव are to the same 8 names of rudra. These are alluded to in the ब्राह्मण-s, गृह्य sUtra-s and yajur vedic mantra पाठ-s. 2) In the atharvavedic sacrifice to rudra prescribed in the व्रात्यकाण्ड of the AV-S (15.5) invokes the अSटमूर्ति and pacifies them to prevent "bhava, sharva or इशान" from harming cattle. 3) बौधायन dharmasUtra prescribes तर्पण-s to the अष्टमूर्ति-s, their wives and sons. 4) The offerings of the parts of the animal smothered in the great shrauta rites like ashvamedha are made to rudra with 8 names, which include among them rudra, bhava, sharva, महादेव and pashupati. The mantra used for this is KYV-TS 1.4.36: chitta: sinews, bhava: liver, rudra: pancreas, pashupati: diaphragm, agni: heart, rudra: blood, sharva: kidneys, mahadeva: the mesenteries, उषिष्ठहन: intestines and lungs.

This concept remained vivid in the post-vedic period, in पुराण-s, coeval with the rise of the पाशुपत based अतिमार्ग शैवं and the mantra-मार्ग शैवं. For example it is alluded to in the mahimna stava of पुष्पदन्त from the पौराणिc period (although not specifically attached to a particular पुराण):

"[भवः शर्वो रुद्रः पशुपतिर् अथोग्रः सह महान्]{style="font-style:italic;color:rgb(51, 204, 0);"}\
[तथा भीमेशानाव्-इति यदभिधानाष्टकम् इदम् ।]{style="font-style:italic;color:rgb(51, 204, 0);"}\
[अमुष्मिन् प्रत्येकं प्रविचरति देव श्रुतिर् अपि]{style="font-style:italic;color:rgb(51, 204, 0);"}\
[प्रियायास्मै धाम्ने प्रणिहित-नमस्यो ।अस्मि भवते ॥]{style="font-style:italic;color:rgb(51, 204, 0);"}"

"bhava, sharva, rudra, pashupati, also ugra with mahat, furthermore bhIma and ईशान, these eight appellations of the god, each of them the shruti individually expounds, My salutations are to the dear abode in which these names are laid (i.e. to one who bears these names)."\
Thus, the mahimna stava explicitly remembers the vedic origin of these names of rudra alluding to their exposition found in the ब्राह्मणस् (e.g. shatapatha6.3.1, kaushitaki 6.1-3).

In the latter two shaiva streams there was more emphasis on the पञ्चब्रह्म compared to the अष्टमूर्ति, though the siddhAnta stream did recognize the icons of the अष्टमूर्ति in their Agama-s.


