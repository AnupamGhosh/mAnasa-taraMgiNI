
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on the curriculum at तक्षशिल](https://manasataramgini.wordpress.com/2007/02/21/a-note-on-the-curriculum-at-takshashila/){rel="bookmark"} {#a-note-on-the-curriculum-at-तकषशल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 21, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/02/21/a-note-on-the-curriculum-at-takshashila/ "6:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ST asked me whether तक्षशिल was a bauddha or Hindu university --- ekanetra who has a rather large bookish knowledge of history told her that तक्षशिल was a buddhist vihAra. To drive home the point to our inquisitive lady we looked up the जातक-s that we periodically visit to acquaint ourselves with the musical vulgarity of pAli.\
In the सुसीम जातक (#163) the venerable तथागत was born as a ब्राह्मण. It is said that he flew to तक्षशिल in a day and learned the trayi-veda and गाजयुर्वेद in another day. He even corrected his teacher for wrong readings of the veda. He then returned in a day to वाराणसि and defeated other brahmins who challenged his knowledge.\
In the asadisa जातक (#181) the buddha states that a kShatriya youth asadisa-कुमार went at the age of 16 to तक्षशिल and studied the vedas under a renowned professor and also 18 subjects and dhanurveda.\
In the anabhirati जातक (#182) a young ब्राह्मण is supposed to have again gone to तक्षशिल and studied the veda-s, shrauta rituals and mantra-शास्त्र under a renowned teacher. He then is said to have returned to वाराणसि and taught the mantra-शास्त्र to many ब्राह्मणस् and क्षत्रियस्. However, upon his marriage, given to the ease of married life he forgot his mantras and he could repeat the veda-s correctly. The buddha admonished him asked to revive his clarity of mind to recite the veda-s appropriately.

So by the testimony of the तथागत himself तक्षशिल was a school of vedic learning and obviously also of other lore like हस्त्यायुर्वेद, dhanurveda and other subjects (18 mentioned by him)


