
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The fall of the नक्षत्र-दर्शा](https://manasataramgini.wordpress.com/2007/08/16/the-fall-of-the-nakshatra-darsha/){rel="bookmark"} {#the-fall-of-the-नकषतर-दरश .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 16, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/16/the-fall-of-the-nakshatra-darsha/ "4:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were coming out of the house of boredom with Blackie and those who were to be washed away by the flow of द्वादशान्त, like the enemies of सुदास on the परुष्णि. We saw दीर्घदन्त hrasvaroman, कुलुञ्चपुत्र and बृन्दापुत्र moving in the opposite direction. Blackie asked with characteristic doubt : "O ब्राह्मण which is the right path ?" We said: O कृष्ण-tvacha your path lies in the yonder region, near the shrine where the awful tusked son of अम्बिका sleeps."\
Verily Blackie never moved much from that place.

....

ST and Mis-creant where contending fiercely with she who is named after ruru's wife. But that नक्षत्र-दर्शा was one of the fiercest warriors, verily an Amazon with a sharp bow. Scorched by her arrows ST and Mis-creant fled in different directions. In one encounter she even overthrew the 3rd vIra. The third vIra wounded thus was fleeing, even as his sachiva was struck by a prayoga of trailokya स्तम्भिनी. Can a woman be such a great warrior ? We asked in wonderment seeing the arrows she rained on the armies of brave archers. Even as we were watching thus, ekanetra calmly declared with much certainty: "She will fall much like shveta and uttara in battle. She will not attain eko-मानुष आनन्दः like that other नक्षत्र-darsha."

Soon enough, a म्लेच्छ hero struck her down with his अङ्कुष and put an end to her string of victories. ekanetra smiled to ST and said go convey the news to the rest. We were touched to see our co-ethnic fall in battle, and wanted to go to her aid. But wise ekanetra smiled and said: "I too feel somewhat like you do. But let her be. She has been an enemy of the dharma in her moha. So let us not shed tears for her, despite that closeness we might feel."

ekanetra then smiled and added: "One day she will laugh and we will be crying. Glory is like a falcon that sits on his shoulder today and on that one there tommorow. So the Iranian of yore had said."

....

We felt like Dattaji Shinde on the northern plain;\
by the perverse खाण्डवन्स् our men lay slain;\
in the clasp of the foes our capital in waste had lain;\
our offerings have the gods decided to spurn?


