
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [vikrama enters the graveyard](https://manasataramgini.wordpress.com/2007/03/06/vikrama-enters-the-graveyard/){rel="bookmark"} {#vikrama-enters-the-graveyard .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 6, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/06/vikrama-enters-the-graveyard/ "7:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ततः शमशानम् संप्राप निःशङ्को bhuta-saMkulaM\
sarvApAyamayaM कायम् ivA .a.ayAsashatAshrayaM\
Then fearlessly he entered the crematorium which was full of ghosts\
It was like the body of all deaths, the abode of a hundred misfortunes.

मस्तिष्क-lipta-shubraasthi प्रकरं lohitAsavam\
AkrIDaM iva कालस्य कपाल-चषका-kulaM\
Full of heaps of white bones smeared with brains,\
Blood was the drink in the pleasure-ground of काल, with cups of skulls.

धूमान्धकार-malinam वीरेन्द्रा-राव-garjitam\
ca\~ncach-chitaagni-तडितं कालमेघम् ivo .atthitam\
Stained with the darkness of smoke, thundering with the cries of brave warriors (1),\
Flashing with the leaping funerary fires, it was like the rising cloud of death \[or rising black cloud].

गृध्राकृSटान्त्र-मालाभिः कृत-प्रालम्ब-vibhramam\
kAlyA iv.aotsavonmatta कृत्तिका नृत्त kampitaM\
The likeness of a pendant was formed by the garlands of entrails torn by vultures,\
The \[place] was reverberating by the frenzied dancing of the कृत्तिका-s in festival of काली.

जीर्णास्थि-nalakach-Chidra-क्षिप्र-शिञ्जान mArutaM\
samcharad योगिनी वृन्द नूपुरैर् iva rAvitam\
Through the long hollows of old bones the wind sharply whistled;\
The \[place] resounded as if it were the noise of the anklets of prancing योगिनी-s (1).

दिक्षु pratiphalad-ghora-pherava-स्फार huMकृतM\
tri-jagat-pralayaarambha-कृत्-ओंकारम् ivA a.ntakam\
The quarters echoed with the terrifying howls of jackals, like हुम्कार-s;\
Like the destroyer uttering the ओंकार to initiate the destruction of the 3 worlds.

मण्डितं muNDa-खण्डेन कङ्काल-kula-mAlinaM\
jvalitaa\~Ngara नयनं द्वितीयं iva bhairavaM\
Decorated with fragments of skulls, garlanded with clusters of bones,\
With glowing embers of eyes, it was like a second bhairava (2).

praty-agra-रुधिरापूर संपूरित वृकोदरM\
कर्ण shalyod-धृता-रावं दुःशासन-वधा-kulaM\
Fresh-flowing blood filled the satiated bellies of wolves;\
The ears were pierced by the cries from the execution of criminals.\
\[It was like कर्ण and shalya raising a cry at the scene of दुःशासन's killing,\
With fresh flowing blood filling the belly of वृकोदर-भीमसेन]

bahuch-छलं द्यूतम् iva स्त्री-chittam iva dAruNaM\
avivekam ivA .aneka शङ्कातङ्क-niketanaM\
It \[was filled] with uncertainty like the casting of dice; with ruthlessness like a woman's mind,\
The abode of indiscernible anxieties and apprehensions.

खरोत्कट जनस्थानं ghora शूर्पणखा vR\^itaM\
दण्डकाraNya सदृशं मारीच-ruchirAtaram\
It was a place of rough and fierce beings; filled with sharp-clawed demons,\
It appeared like a wasteland, \[feeling] more pungent than the taste of pepper.\
\[It was like the जनस्थान of the giant khara, with शूर्पणख roaming around;\
It appeared like the दण्डका forest pleasing to the demon मारीच].

भ्रान्ताकम्पन धूम्राक्ष मेघनाद vibhIShaNaM\
लङ्का daham ivo.अद्भूतं जीवद्-रावण-viplavam\
It was terrible because of the roar of thunder causing the confused and smoke-filled eyes to tremble;\
As though produced from the flames that burnt branches, causing the living to flee in tears.\
\[There roamed akampana, धूंरक्ष, मेघनाद and वीभीषण, as though sent forth by the burning of लङ्का that cause distress to the रावण even as he lived (3)]

samagra-दुःख-निलयं भूत-संघ-praहर्षणM\
bahuch-छिद्रं घनाश्लिSट-preta-राशि-nirantaraM\
It was the abode of all sorrows that caused the ghostly hosts to rejoice,\
Full of fissures with densely packed rows of corpses.

पलाश-shata-संबाधम् chitA-निःशेषित-drumam\
shivAbhir व्याप्तम् ashivam भ्रान्तान्तकम् anantakam\
Crowded with hundreds of पलाश trees, with wood being consumed non-stop in the pyres;\
Surrounded by jackals, inauspicious and vast with death roving about.

निष्कंप-kucha-कुम्भाभिर् vipula-श्रोणिभिर् muhuh\
digambarAbhir नारीभिः kalpitoch-चण्ड-ताण्डवM\
Incessantly, with firm pot-like breasts and great hips,\
Sky-clad women performed the terrific ताण्डव dance (4).

गृध्र-गोमायु-गहनं kAka-कञ्क-kulAkulaM\
pramatta-भूत-वेताल-लास्य-melaka-mAlitaM\
The place was thick with vultures and jackals, the roost of flocks of crows and egrets.\
Throngs of frenzied ghosts and goblins, dancing the लास्य dance surrounded the place like garlands.

पिशाच-डाकिनी-जुष्ट-रटड्-Damaru-मण्डलM\
spaShTaaTTahAsa-मृतकं क्रीडच्-चक्रेश्वरी-chayam\
There were band of resounding Damarus pleasing to पिशाचस् and डाकिनिस्.\
Amongst the Loud laughter of ghosts, bands of चक्रेश्वरी-s (5) sported.

भयंकरं भायस्यापि mohasyaapi vimohanaM\
tamaso .apy andha-तमसं कृतान्तसया .api kR\^intanaM\
The place was fear for fear itself and agitating for agitation itself.\
It was blinding darkness even for darkness and cutting down death itself.

दृष्ट्वा पितृवनं घोरं डाकिनी-गण-sevitaM\
kShAnti-शीलं वटतले so .apashyat कृत-मण्डलM\
He \[vikrama] saw in the terrible grove of the manes, served by डाकिनीस्,\
क्षान्तिशील at the foot of the [Ficus religiosa]{style="font-style:italic;"} tree, drawing a मण्डल.

  - क्षेमेन्द्र

\(1\) क्षेमेन्द्र had studied with abhinavagupta the great kula/krama/trika tantric and was evidently very familiar with the graveyard rituals of श्रीकुल and कालिकुल. He subsequently went over to the पाञ्चरात्र school of tantric vaiShNavism. I suspect the वीरेन्द्र-s here refer to the performers of वीरसाधन (even as it does in the ललिता-सहस्रनामम्- महावीरेन्द्र तनया) invoking yogini-s in the श्मशान. This वीरसाधन is clearly mentioned in the brahma यामल whose other elements may color this poem.\
(2) Even भट्ट somadeva uses a similar simile in his kathA-sarit-सागर which implies this comparison was there in the original वेताल-पञ्चविंशति. Of course, both क्षेमेन्द्र and somadeva, with the connections with trika and kula tantrism, would have seen the comparison as appropriate.\
(3) In the महाश्मशान दीक्ष given in the brahma-यामल we notice invocation of राक्षसस् headed by रावण and पिशाचस् in the आवर्णस् around bhairava holding the sword. This is also consistent with the rite in the निःश्वास-tattva tantra. Evidently this practice was well known amongst the kashmirian kula tantrics.\
(4) It is peculiar that the women dance a ताण्ड्य dance, but this reference to the sky-clad women dancing the श्मशान is clearly inspired by the kula ritual with the कुलाङ्गना-s described in the jayadratha-यामल. The श्रीकुल tantrics following jayaratha mention that the ritual intercourse with the कुलाङ्गना is preceded by her हर्षण into an ecstatic state.\
(5) The use of the word चक्रेश्वरी is again a clear allusion to the kula ritual sanctioned by the pichumata of the pichu-मण्डल performed in the श्मशान, where the great goddess manifests as कुलविद्या, the चक्रेश्वरी.


