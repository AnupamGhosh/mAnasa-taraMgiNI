
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The seven-fold agni and the seven-fold troops of the maruts](https://manasataramgini.wordpress.com/2007/11/23/the-seven-fold-agni-and-the-seven-fold-troops-of-the-maruts/){rel="bookmark"} {#the-seven-fold-agni-and-the-seven-fold-troops-of-the-maruts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 23, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/23/the-seven-fold-agni-and-the-seven-fold-troops-of-the-maruts/ "2:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the agnichayana/अग्निष्टोम the following sequence of rituals are performed:

 1. At the यजमान's गार्हपत्य the oblations to prajApati vishvakarman are made.

 2. The adhvaryu carries forth the fire in the eastern direction (agni-प्रणयन)

 3. The अध्वराहूति-s are performed in the fire established in the new आहवनीय altar.

In course of #2 the adhvaryu and hotar recite their yajushes and ऋक्स्, while the प्रस्तोता sings the sAman-s. In the agnichayana the मैत्रावरुण additional chants the war-like अप्रथिरथं to indra.

The fire is laid in the new आहवनीय with a long yajurvedic chant (KYV-TS 4.6.5), in course of which the connection between agni and the भृगु-s, the first fire sacrificers is remembered:

[अग्ने प्रेहि प्रथमो देवयतां चक्षुर् देवानाम् उत मर्त्यानाम् । इयक्षमाणा भृगुभिः सजोषाः सुवर् यन्तु यजमानाः स्वस्ति ॥]{style="color:rgb(51, 255, 51);font-style:italic;"}\
O agni, advance, first of deva-sacrificers, eye of the deva-s and mortals; advancing in unison with the भृगु-s, let the sacrificers attain light and well-being.

Then सविता is then invoked and the early अङ्गिरस् ritualist कण्व is also remembered:

[ताम् सवितुर् वरेण्यस्य चित्राम् आहं वृणे सुमतिं विश्वजन्याम् । याम् अस्य कण्वो अदुहत् प्रपीनाम् सहस्रधाराम् । पयसा महीं गाम् ॥]{style="font-style:italic;color:rgb(51, 255, 51);"}\
That excellent, many-formed, all-encompassing pleasantness of सविता, I choose; that mighty well-endowed cow of his which कण्व milked, with a thousand stream of milk.

The final part of this recitation is that of the heptads of agni and the marut-s.\
agni is first invoked, being described as a series of 7 heptads:

[सप्त ते अग्ने समिधः सप्त जिह्वाः सप्तर्षयः सप्त धाम प्रियाणि ।]{style="color:rgb(51, 255, 51);font-style:italic;"}\
[सप्त होत्राः सप्तधा त्वा यजन्ति सप्त योनीर् आ पृणस्वा घृतेन ॥]{style="color:rgb(51, 255, 51);font-style:italic;"}\
That is: 1) 7 fuel sticks; 2) 7 tongues of agni; 3) 7 ऋषि-s; 4) 7 dear abodes of agni; 5) 7 oblations; 6) the 7-fold worship of agni 7) the seven sources of agni, which are filled with ghee.

Of course this association of 7 with agni is often encountered right from the RV itself.

Then 5 sets of seven fold maruts are invoked and asked to come to the sacrifice of the ritualist and aid him:

[ईदृङ् चान्यादृङ् चैतादृङ् cha प्रतिदृङ् cha mitash cha sammitash cha सभराः |]{style="font-style:italic;color:rgb(51, 255, 51);"}\
[शुक्रज्योतिश् च चित्रज्योतिश् च सत्यज्योतिश् च ज्योतिष्माम्श् च सत्यश् चर्तपाश् चात्यम्हाः ।]{style="color:rgb(51, 255, 51);"}\
[ऋतजिच् cha satyajich cha senajich cha सुषेणश् चान्त्यमित्रश् cha dUre-amitrash cha गणः |]{style="font-style:italic;color:rgb(51, 255, 51);"}\
[ऋतश् cha satyash cha dhruvash cha धरुणश् cha धर्ता cha विधर्ता cha विधारयः |]{style="font-style:italic;color:rgb(51, 255, 51);"}\
[ईदृक्षास एतादृक्षास ऊ षु णः सदृक्षासः प्रतिसदृक्षास एतन ।]{style="font-style:italic;color:rgb(51, 255, 51);"}\
[मितासश् च सम्मितासश् च न ऊतये सभरसो मरुतो यज्ञे अस्मिन् ।]{style="font-style:italic;color:rgb(51, 255, 51);"}\
[इन्द्रं दैवीर् विशो मरुतो ऽनुवर्त्मानो यथेन्द्रं दैवीर् विशो मरुतो ऽनुवर्त्मान एवम् इमं यजमानं दैवीश् च विशो मानुषीश् चानुवर्त्मानो भवन्तु ॥]{style="font-style:italic;color:rgb(51, 255, 51);"}

1:

 1. ईदृङ् 2) अन्यादृङ् 3) एतादृङ् 4) प्रतिदृङ् 5) mita 6) sammita 7) sabharas\
2:

 1.  shukra-jyoti 2)chitra-jyoti 3) satya-jyoti 4) ज्योतिष्मान् 5) satya 6) अर्तपा 7) atyamhas\
3:

 1.  ऋतजित् 2)satyajit 3) senajit 4) सुषेण 5)antyamitra 6) dUre-amitra 7) गण\
4:

 1.  ऋत 2) satya 3) dhruva 4) धरुण 5) धर्ता 6) विधर्ता 7) vidhAraya\
5 (this last list is merely a plural representation of the first set, probably representing them with their troops:

 1.  ईदृक्ष-s 2) एतादृक्ष-s 3) सदृक्ष-s 4) प्रतिसदृक्ष-s 5) mita-s 6) sammita-s 7) sabhara-s

This is one of the few places in the veda where the marut-s are explicitly named. List 1 and 2 are again invoked in the KYV-TS in 1.8.13, in the same order, in rite of the mounting of the quarters in the राजसूय ritual. It is of interest to note that two of these names are those of the seizing विनायक-s of the विनायक शान्ति: mita and संमित. Thus, the link between the विनायक and the vedic sons of rudra may be established (first proposed by the great savant Pandurang Kane).

The number 7 is associated with the marut-s from the earliest vedic period down to the classical Hindu lore, as seen in the RV itself:\
RV 5.52.17: Here the maruts are said be 7*7 (sapta me sapta)\
RV 8.28.5: Here the maruts are described as being 7-fold, bearing 7 spears and 7 glories.\
RV 10.13.5:Here the maruts are again said to be 7 fold.\
Additionally RV 8.92.20 might have an oblique reference to the 7-fold troops of the maruts in the phrase: "saptá संस्áदः".

In both the KYV and SYV the association is reiterated in many ways:\
In SYV-माध्यम्दिन 9.32 (\~ख़्य़्V-TS 1.8.9) the maruts are said to win 7 domestic animals with the aid of the 7-syllabled mantra.\
In SYV-M 25.4 (\~ट्.S 5.7.21) the seventh rib of the ashvamedha horse is offered to the marut-s.\
In many yajurvedic ritual injunctions the maruts are made offerings on 7 कपाल-s.\
In KYV-TS 2.2.5, 2.2.11, 2.3.1, 5.4.7 it is stated that the marut-s are made offerings on 7 कपाल-s because they comprise of 7 गण-s.

Despite all these allusions the choice of 5 lists of 7 each is a bit puzzling, given that none of the above explicitly mention 5*7. The only possible mention is seen in RV 10.55.3:\
A रोदसी अपृणादोत मध्यं पञ्च देवान् ऋतुशः sapta-sapta |\
चतुस्त्रिंशता पुरुधा vi चष्टे सरूपेण ज्योतिषा vivratena ||\
This is from a सूक्त describing the universal form of indra, and is one of those cryptic सूक्त-s filled with hidden astronomical allusions. The essential sense of the first hemistich of the above mantra may be approximated thus: "He(indra filled the two hemispheres of the universe and all that is contained within them, encompassing the 5-fold gods in the 7 (or 7*7) fold orders." This appears to be a possible allusion to indra's companions the marut-s ordered thus, but this सूक्तं being so confounding one cannot be sure. There is a number 34 mentioned in the second hemistich (=35-1), which is traditionally interpreted in this context to mean 27 नक्षत्र-s+sun+moon+5 visible planets.

Finally, it should be mentioned that the "7" connection of the marut-s is also seen in their successor and paralogous deity, skanda. We had earlier discussed in our [note on the marut-s being being "para-skanda"](http://manollasa.blogspot.com/2007/07/maruts-as-para-skanda-and-other.html), how कुमार was made commander of the seven-fold troops of maruts. This connection is further seen in ancient triannual rite to कुमार, known as the धूर्त-homa and bali (the sacrifice to the roguish skanda) preserved in the बोधायन tradition. The recommended date for its performance is the shukla-saptami of phalguna month. Again, in an invocatory verse used in this rite कुमार is described as born on the the seventh day and being a manifestation of the seventh moon phase. These points further strengthen the connection between the marut-s and कुमार: before the more popular षष्टि connection over took the कौमार tradition there was a parallel tradition that connected him to saptami-s, consistent with the 7-fold nature of the marut-s.


