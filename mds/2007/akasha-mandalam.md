
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [आकाश-मण्डलं](https://manasataramgini.wordpress.com/2007/04/24/akasha-mandalam/){rel="bookmark"} {#आकश-मणडल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 24, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/24/akasha-mandalam/ "5:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/Ri2a-CAZGqI/AAAAAAAAAG0/ptyq0iYVpHQ/s320/kaumAram.jpg){width="75%"}
```{=latex}
\end{center}
```



```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/Ri2a-CAZGrI/AAAAAAAAAG8/Jo1P7wGZzVQ/s320/kaumAram2.jpg){width="75%"}
```{=latex}
\end{center}
```



