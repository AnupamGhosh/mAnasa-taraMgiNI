
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The orbits of beauty](https://manasataramgini.wordpress.com/2007/06/17/the-orbits-of-beauty/){rel="bookmark"} {#the-orbits-of-beauty .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 17, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/17/the-orbits-of-beauty/ "4:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RnS6rU5tW_I/AAAAAAAAALE/JfDoT6Ir3VE/s320/orbits1.jpg){width="75%"}
```{=latex}
\end{center}
```



