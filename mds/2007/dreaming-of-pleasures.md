
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Dreaming of pleasures](https://manasataramgini.wordpress.com/2007/11/25/dreaming-of-pleasures/){rel="bookmark"} {#dreaming-of-pleasures .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 25, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/25/dreaming-of-pleasures/ "6:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We sat in the train after a brief wait at the station. We received a signal from indra. We opened our eyes from the spell of somnolence into which we were lapsing. We saw two sets of विद्याधर-s, like those described by वाल्मीकि. Then our eyelids closed. We felt the thundering indra rend apart the rodasi. The cosmic nature of maghavan and imagery of his earth plays intermingled like waves interfering on a pellucid pond's surface. We had a brief dream of pleasure, like an island in the sea of sorrow.\
He sees not pleasure when he encounters विद्याधर-s,\
he sees not sorrow when he encounters यक्षिणी-s,\
but like the bow of the thunderer filling the atmosphere,\
the indra in his nerves props the rainbow of pleasure.

From the dream we emerged and found that we were back in the rough waves of the sea of sorrow, with sharks seeking to make a meal of us. In our house prowled the agents of धूमावती and assorted वेतालस् and पिशाची-s, as though in a स्मशान. Tormented by these we wandered. Thus, we arrived at the महायुद्धार्णव.\
ऋक्षबाहु and rasa-पिशाची attacked us, but the spell of आद्यलक्ष्मि neutralized them;\
The speckle-skinned vidarbhan attacked us, but by the spell of skanda we overcame him;\
The क्षुरिक-मुखा attacked us, but by the spell of bhIma we slipped past her;\
कालानन सुरापायिन्, with madonmatta attacked us, but the spell of indra blinded them;\
The bands of म्लेच्छ-s fell upon us, but the thunderer saw us through them, sending brahmins to fight in our ranks;\
But now we face the खाण्डवन्-s, who have been moving like कुम्भकर्ण against the वानर-s. Which deva will come to our aid?


