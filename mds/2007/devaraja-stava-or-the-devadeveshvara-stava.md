
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [देवराज stava or the devadeveshvara stava](https://manasataramgini.wordpress.com/2007/01/23/devaraja-stava-or-the-devadeveshvara-stava/){rel="bookmark"} {#दवरज-stava-or-the-devadeveshvara-stava .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 23, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/23/devaraja-stava-or-the-devadeveshvara-stava/ "6:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RbW6dy6_e0I/AAAAAAAAABg/idxmMbwdBTk/s320/indra.jpg){width="75%"}
```{=latex}
\end{center}
```



The ऋषि of the most glorious of stava-s is पुष्कर; the Chandas in अनुष्टुप् and the god of gods, indra is the देवता.

varastvindra-जितामित्र- वृत्रहन्-पाकशासन | deva-deva महाभाग त्वं hi वर्धिष्णुतां गतः ||\
ananta-tejo virajo yasho-vijaya vardhana | aprabhus-त्वं prabhur-nityam-उत्तिष्ठ सुरपूजित ||\
brahmA स्वयुम्भूर्-भग्वान्-sarva-loka-पितामहः | रुद्रः पिनाक-भृद् -दृप्तश्-चतसृद्वय संस्तुतः||\
yogasya नेता कर्ता cha तथा विष्णुरुरुक्रमः | tejaste vardhayan-tvete nityam-eva महाबलाः ||\
अनादि-nidhano devo brahmA स्रष्टा सनातनः | agnis-tejomayo भागो रुद्रात्मा पार्वती-सुतः ||\
कार्तिकेयः shakti-धरः ShaD-vaktrash-cha गदाधरः | शतं वरेण्यो varadas-tejo वर्धयतां विभुः ||\
देवः सेनापतिः स्कन्दः sura-pravara-पूजितः | आदित्या vasavo रुद्राः साध्या देवास्-tathaashvinau ||\
भृगुर्-आङ्गिरसश्-chaiva विश्वेदेवा मरुद्गणाः | लोकपालास्-trayash-chaiva चन्द्रः सूर्यो.analo.अनिलः ||\
देवाश्-cha ऋषयश्-chaiva यक्ष gandharva राक्षसाः || समुद्रा girayash-chaiva nadyo bhUtAni यानि cha ||\
tejas-तपांसि सत्यं cha लक्ष्मीः श्रीः कीर्तिर्-eva cha | pravardhayatu tat-tejo jaya shakra शचीपते ||\
tava चापि जयान्-नित्यं tviha संपद्यते शुभं | प्रसीद राज्ञां विप्राणां प्रानामपि सर्वशः ||\
tava प्रसादात्-पृथिवी नित्यं सस्यवती bhavet | शिवं bhavatu निर्विघ्नं शम्यं-तामीतयो भृशं ||\
namaste deva-devesha namaste वलसूदन | namuchighna namaste.astu सहस्राक्ष शचीपते ||\
सर्वेषां-eva लोकानां tvam-एका परमा गतिः | tvam-eva प्रमः प्राणः सर्वस्यास्य jagat-pate ||\
पाशो hyasi पथः स्रष्टुं त्वं अनल्पं purandara | tvam-eva meghas-त्वं वायुस्-त्वं agnir-vaidyuto.ambare ||\
tvamatra मेधावि-क्षिप्ता tvam me बाहुः प्रतर्दनं | vajram-अतुलं घोरं घोषवांस्-त्वं बलाहकः ||\
स्रष्टा tvam-eva लोकानां संहर्ता चापरजितः | त्वं ज्योतिः सर्वलोकानां tvam-Adityo विभावसुः ||\
त्वं mahad-भूतम्-आश्चर्यं त्वं rAjA त्वं सुरोत्तमः | त्वं विष्णुस्-त्वं सहस्राक्षस्-त्वं परायणं ||\
tvam-eva चामृतं devas-त्वं मोक्षः परमार्चितः | त्वं मुहूर्तः स्थितिस्त्वं cha lavas-त्वं cha पुनः क्षणः | shuklas-त्वं bahulash-chaiva कला काष्ठा त्रुटि-स्तथा ||\
संवत्सरर्तवो mAsA rajanyash-cha दिनानि cha | tvam-उत्तमा sa-गिरिचरा वसुंधरा sa-भास्करं तिमिरंबरं तथा ||\
सहोदधिः sa-तिमिङ्गिलस्-तथा सहोर्मिवान् bahumakaro झषा-kulaH |\
mahad-दशास्-tvamiha सदा cha पूज्यसे महर्षिभिर्-mudita-manA महर्षिभिः ||\
अभिSटुतः pibasi cha somam-adhvare हुतान्यपि cha हवीम्षि भूतये |\
त्वं विप्रईः सततं ihejyase फलार्थं भेदार्थेष्वष्तसु balaugha गीयसे त्वं ||\
tvadh-detor-yajana-पारायणा द्विजेन्द्रा-वेदाङ्गान्यधि-gamayanti sarva-वेदैः |\
vajrasya भर्ता bhuvanasya गोप्ता वृत्रस्य हर्ता namucher-निहन्ता ||\
कृष्णे वसानो vasane mahAtmA सत्यानृते yo vivinakti loke |\
yaM वाजिनं गर्भं-अपाम्-सुराणां वैश्वानरं वाहनम्-abhyupaiti ||\
नमः सदा.asmai त्रिदिवेश्वराय लोकत्रयेशाय पुरन्दराय |\
ajo.अव्ययः शाश्वत eka-रूपो विष्णुर्-वराहः पुरुषः पुराणः ||\
tvam-अनतकः sarva-हरः कृशानुः सहस्रशीर्षा शतमन्युरीड्यः |\
कविं sapta-जिह्वं त्रातारम्-इन्द्रं सवितारं सुरेशं ||\
हृद्याभि-शक्रं वृत्र-हणं सुषेणम्-अस्माकं vIrA uttare bhavantu |\
त्रातारम्-indrenriya-कारणात्मञ्-jagat-प्रधानं cha हिरण्यगर्भं ||\
लोकेश्वरं deva-वरं वरेण्यं chaananda-रूपं प्रणतोस्मि नित्यं |


