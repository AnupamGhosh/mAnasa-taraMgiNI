
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Headed to कुरुक्षेत्र ?](https://manasataramgini.wordpress.com/2007/11/18/headed-to-kurukshetra/){rel="bookmark"} {#headed-to-करकषतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 18, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/18/headed-to-kurukshetra/ "5:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We made arrangements for our rendezvous with ST and ekanetra. Both passed to us the information regarding the लौकीक भ्रातृव्य-s ranged against us. Knowing that deploying the frontier troops were critical for future survival we consulted regarding how the corps should be positioned. We realized that it was important that they be widely distributed to counter different points of attacks by the भ्रातृव्य-s. We decided to position the frontier command under the illustrious warrior पूर्व-मारुति. Below पूर्व मारुति we placed जायद्रथि, as suggested by ST, to ride out with a commando force against jIva-sUtra-अपर्णक. We also placed under the aforesaid warrior a force under पञ्च-nadi, who would also strengthen our yoddha धूम्र-tvacha. We also sent another force under druhyuka of krimi-क्षेत्र. We also mobilized our कास्यस्थ warrior against bahu-मूषक. But who knows the best laid human plans can fail with दुष्ट-भ्रातृव्य-s like दक्षिण. ST cryptically said with खाण्डवन्स् around कुरुक्षेस्त्र is not far away.

We had planned a move to meet with the सेना to build reinforcements. The news of the utter failure of their powerful mantra prayoga-s due to the power of कुमार had reached the खाण्डवन्-s. Their spies also got the news of our intended joining with the सेना. This immensely alarmed them and they decided to go for a direct strike. Gathering their troops they attacked twice before we could join with the सेणा. They also cut off our communication line with our सेना.We were hit by several sharp shara-s from them, but evaded their ambushes and reached the सेना. In the mean time our स्पष-s reported tremendous activity on their side : 1) They discovered the breach in the kosha front and decided to mass their forces their.

 2.  They decided to deploy a mantra prayoga on the muni, while seeking to destroy their rest of us by pumping troops through breach.

 3.  They tried to send troops to separate us from the सेना during a future conflict when the भ्रातृव्य-s might attack.

 4.  They might try to rob the other kosha and tie us down with an attacking कृत्या.\
Mn's अमात्य also obtained similar intelligence regarding #1 and communicated it to us. Jx obtained intelligence likewise regarding #4 but felt they were a held back by our mantra prayoga.\
The खाण्डवन्-s sent another prayoga at us today but only caused us minor harm due to the सुब्रह्मण्य याग. However, this promised to be a much more ferocious and merciless battle than that of the autumn of 2004 when we fatefully entered the खाण्डवन् conflict --- but those who live like kShatriya-s should be ready for a terrible death on the battle field.\
[Think back !](http://manollasa.blogspot.com/2004/11/further-proceedings-on-battle-front.html)

Jx was himself shaken by his attacker, but the third hero saved him. We now realized how he was in a different league. Just an year back the third hero was reeling under the "flight of the crane" which he came face to face with when we went to that distant shrine: the wall of बगलामुखी, they said. But all prayoga-s sent at him were blown to smithereens like has never happened before.

Our अमात्य deployed कर्ण-पिशाचि and got precise intelligence about the खाण्डवन् strike and deployed the shiva mantra


