
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The पुराण of the Maharatta ruler, the पट्टिश and other issues](https://manasataramgini.wordpress.com/2007/03/26/the-purana-of-the-maharatta-ruler-the-pattisha-and-other-issues/){rel="bookmark"} {#the-परण-of-the-maharatta-ruler-the-पटटश-and-other-issues .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/26/the-purana-of-the-maharatta-ruler-the-pattisha-and-other-issues/ "3:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Hindu पौराणिc style never died out. We know of the many responses seen in Sanskrit literature to the Mohammedan onslaught. We had earlier seen how क्षेमेन्द्र had described the [kali yuga before the coming of kalki](http://manollasa.blogspot.com/2005/03/kshemendra-on-kalki.html). The learned Ravlekar had furnished us with two texts of interest. One was the particular text titled the kalki-पुराण. In this it is narrated that demonic तुरुष्क or म्लेच्छ king named shashi-dhvaja (the moon-bannered one) whose capital lies in the desert will cause untold misery on the earth by ravaging Aryavarta and destroying the dharma. Ruling the world this shashi-dhvaja would spread the म्लेच्छ atrocities all over the world until he encounters kalki. In the fierce battle that would happen in a certain भल्लाटपुर kalki would behead shashi-dhvaja, destroy numberless dasyu-s, तुरुष्क-s and म्लेच्छस्, and relieve the earth from their dreadful grasp. However, only a few surviving forms describe the exploits of Hindu heroes in their struggle against the तुरुष्कस् and म्लेच्छस्. This was the topic of the second text: The shiva-bhArata of परमानन्द describing he exploits of the founder of the maharatta nation. परमानन्द says it is the bhArata of शिवाजी, even as the महाभारत of the bharatas.\
परमानन्द says (I translate it to roughly approximate परमानन्द's verse):\
[उष्णीषेणैव शुचिना व्यभाद्-उत्तम्स-धारिणा ।]{style="font-style:italic;color:rgb(0, 204, 204);"}\
[कश्मीरज-पृषद्-वर्ष-रंजितेन-अङ्गिकेन च ॥]{style="font-style:italic;color:rgb(0, 204, 204);"}\
[शिव-वर्मा बृश्बलः संवृतः शिव-वर्मणा ।]{style="font-style:italic;color:rgb(0, 204, 204);"}\
[तस्य वज्र-शरीरस्य किं कार्यं तेन वर्मणा ॥]{style="font-style:italic;color:rgb(0, 204, 204);"}\
[कृपाणं पाणि-नैकेन बिभ्राणोऽन्येन पट्टिशं ।]{style="font-style:italic;color:rgb(0, 204, 204);"}\
[स नन्दक-गदा-हस्तः साक्षाद्-धरिद्-उदैक्ष्यत ।|]{style="font-style:italic;color:rgb(51, 255, 255);"}

With a pure \[white] turban bearing a shining crest jewel,\
with his body raimented in a patterned kashmir silk garment,\
shiva-varman भृश्बल (शिवाजी bhosle) was encased in shiva's armor.\
With an adamantine body like his, what was the need of an armor?\
With a sword in one hand and bearing a पट्टिश* in the other,\
he appeared like hari himself with the nandaka and गदा.

\*The modern translators of पौराणिc texts often hopelessly mess up the translation of the term पट्टिश. Most common depictions of शिवाजी by contemporary and subsequent painters show him exactly as described by परमानन्द holding a sword in one hand and पट्टिश in the other. A modern day Maharatta will tell you that the पट्टिश is a "daNDa-paTTa" and several specimens of it are found in Maharatta weapon collections. I have even encountered a modern Maharattas ply the पट्टिश with great proficiency and cleanly slice a lemon placed on a pole with it. The पट्टिश is an ancient and unique Indo-Aryan weapon. The महाभरत repeatedly mentions it as being used in the great bhArata war. For example:\
भीष्म parvan 86 (critical edition)\
tena mAyA-मयाः कॢप्ता हयास्तावन्त eva hi .\
स्वारूढा राक्षसैर्-घोरैः शूल-पट्टिश-पाणिभिः .. 52..||\
In describing a battle of इरावान् and alambusha, the brahma-rAkShasa, it mentions the well-mounted terrible rAkShasa hordes wielding tridents and पाट्टिश-s.

Again in भीष्म parvan 92 (critical edition) in the list of traditional hindu weapons mentioned there the पट्टिश figures:\
samare patitaish-chaiva shakty-ऋष्टि shara-तोमरैः |\
निस्त्रिंशैः पट्टिशैः प्रासैर्-ayaH कुन्तैः परश्वधैः .. 56..||\
parighair-भिन्दिपालैश्-cha शतघ्नीभिस्-tathaiva cha .\
शरीरैः shastra-bhinnaish-cha समास्तीर्यत मेदिनी .. 57..||\
The weapons mentioned are shakti, ऋष्टि, shara (arrow), tomara, निस्त्रिंश, पट्टिश, प्रास, iron kunta, battle axe, parigha, भिन्दिपाल and shataghni.

The later देवी-माहात्म्यं mentions the same weapon (The महालक्ष्मि section):\
चचारासुर सैन्येषु वनेष्व्-iva हुताशनः |\
निःश्वासान्.h mumuche यांश्-cha युध्यमाना raNe.अम्बिका || 52||\
ta eva सद्यः सम्भूता गणाः shata-सहस्रशः |\
yuyudhuste parashubhir-भिन्दिपालासि-पट्टिशैः || 53||\
The गणस् emerging from the breath of अंबिका are described as fighting with battle axes, भिन्दिपाल-s, swords and पट्टिश-s

Looking for homologs of the पट्टिश used by the Maharatta in other parts of India one encounters the urumi or चुत्तुवाळ् of the कळरिपयत्तु, which probably is the only surviving ancient Hindu martial system. The urumi is a highly prized weapon used only by the best trained in the martial system. This shows that पट्टिश had a long continuous tradition in India that never went out of vogue until recently-- evidently it can be very effective with a skilled user. It should hence be conceived as a flexible, long two-edged razor foil, and not like the many faulty translations of this term. While the accounts of the killing of Afzal Khan differ in detail, at least some accounts mention that शिवाजी used the पट्टिश to dispatch him after stabbing him with the dagger or tiger claws.


