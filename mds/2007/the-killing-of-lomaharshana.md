
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The killing of लोमहर्षण](https://manasataramgini.wordpress.com/2007/01/30/the-killing-of-lomaharshana/){rel="bookmark"} {#the-killing-of-लमहरषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 30, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/30/the-killing-of-lomaharshana/ "6:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The भार्गव मार्कण्डेय told jaimini that he had no time to narrate to him lengthy tales as was his usual tradition, because he was starting his sacrificial satra and need to begin the specified oblations. Instead, he asked him to go the talking pelicans of the narmada to learn more tales. jaimini approached the brahminical pelicans in the cave in the lower vindhya-s splashed by the waters of the narmada to query them about the awful deeds of balabhadra. He was amazed to see 4 birds that were reciting mantra-s like vipra-s The pelicans having fanned jaimini to relieve him of the heat from his exertions in reaching their cave agreed to narrate to him the tales he wanted. They said:\
"कृष्ण has decided to side with the पाण्डवस् and I cannot do much about it given his bond with arjuna. I think things can be done better. I will not go with duryodhana because my brother is on the other side. But king duryodhana is my dear student in the use of the गदा and I will not go against him under any condition. Thus thinking, balabhadra detached his army and went to द्वारवति where he saw his people in great happiness and fed well. To relax he settled to have a bath at a spring and the fair रेवती placed a bowl of wine for him. Quaffing several bowls he and रेवती became inebriated, and in joyous delight decided to go to the raivata park that he had set up for his enjoyment. Clasping the अप्सरा-like रेवती he marched into the sylvan retreat in a stumbling walk along with his several girl-friends joining them with वारुणि beer and liquor. The park was glorious beyond description with all kinds of animals, trees and herbs. He in the pleasure-giving grip of रेवती surrounded by the bevy of beautiful girls, with his sunanda pestle in hand, was admiring his grove. Their intoxication was accentuated by the sweet cacophony of cuckoos, grebes, tittiri-s, mynas, warblers, tailor birds, weaver birds, cranes, wood-peckers, shirkes, crows, parrots, bee-eaters, pheasants and peacocks flitting amongst mango, amalaka, कोवीदर, ashoka, jackfruit trees and palms dotting the dvaraka sea-side, and water-lilies with broad leaves on the ponds. Enjoying various काम-keli-s and क्रीड-s with his girl-friends and wife he arrived a bower. There he saw a group of learned भार्गव-s, वैश्वामित्र-s, भरद्वाज-s and gotama-s collecting glorious पौराणिc narratives of old times from the सूत लोमहर्षण. The ब्राह्मणस् were clad in black antelope coats and seated on kusha grass seats with the सूत in their midst. Seeing संकर्षण swinging his pestle, with blood-shot eyes due to alcoholic inebriation the ब्राह्मण-s were terrified and hastily rose up and saluted the rampaging kShatriya, but the सूत alone remained seated. baladeva flew into fit of fury and with his red eyes rolling swung his pestle and smashed the skull of the सूत to pieces. The ब्राह्मण-s seeing this, fled in terror into the forest, laying a spell on him. balabhadra-s found his body developed a stench of blood that did not go away and immediately he realized he had committed a grave pApa and the displeasure of the vipra-s. To relieve himself of his sins he decided to go on a tirtha yatra culminating in pratiloma सरस्वती."


