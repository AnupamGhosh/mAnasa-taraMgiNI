
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Simple spherical harmonics](https://manasataramgini.wordpress.com/2007/07/05/simple-spherical-harmonics/){rel="bookmark"} {#simple-spherical-harmonics .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 5, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/05/simple-spherical-harmonics/ "6:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RoyMue2KpCI/AAAAAAAAAMk/KE48JOOl1UM/s320/orbital1.4.png){width="75%"}
```{=latex}
\end{center}
```



The first is the famous d orbital curve. The lower two were surfaces that I stumbled upon during my geometric flight of fancy in the days of yore. Visualizing them and understanding some properties of them sated my addictions immensely. The one on the left I named the सूची-stana and the second हृदयौ


