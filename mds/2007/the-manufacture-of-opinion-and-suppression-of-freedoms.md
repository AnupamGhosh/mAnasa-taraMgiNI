
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The manufacture of opinion and suppression of freedoms](https://manasataramgini.wordpress.com/2007/08/21/the-manufacture-of-opinion-and-suppression-of-freedoms/){rel="bookmark"} {#the-manufacture-of-opinion-and-suppression-of-freedoms .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 21, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/21/the-manufacture-of-opinion-and-suppression-of-freedoms/ "5:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many years ago I was being subject to an English examination by an American. The first question he asked me was what is freedom. I defined it for him. Then he asked if I came from a free country. I answered : "Sort of". A bit surprised he asked: "What do you mean?" I replied: "It is a free country roughly in the same sense as you all imagine your country to be free." He was a bit shaken, and said : "You mean I am imagining that my country is free. No, we really have freedom expression". I held my ground and said: "I am not so sure, is it is only seemingly so". Anyhow, to his credit, despite being rattled by a heathen alien, the said American passed me in English

The virile तैत्तिरीयक gushed in the past about his momentous landing on the shores of madhyama म्लेच्छदेश in क्रौञ्च द्वीप: "you truly feel like a free man in a free land the moment you are in the US". However, my experience in the very same shores of क्रौञ्च द्वीप was hardly comparable. I feel I have about just the same freedom of expression as I had or have on the shores of jambu-द्वीप- some matters I can talk more freely here and others in bhArata. My freedom of expression is explicitly curbed by the officialdom in both countries. The American universities have a certain degree of freedom with respect to Internet but impose political correctness especially on issues of intelligence, race and sexuality. Civilian, unclassified American government funded research institutes were earlier similar in their freedoms, but post 9-11 have become increasingly heavy-handed in smothering freedom of expression and access to information. On the whole Chacha Sham has become paranoid and more and more willing to compromise freedom of expression/access to information after 9-11. India on the other hand despite its large internal Moslem and Isaist populations and numerous terrorist strikes exhibits far less paranoia in the directions that Americans do. But the Indian system prevents free expression with its own flavor of political correctness called secularism. India, just like certain US government institutions has a despicable track record with respect to internet freedom, perhaps worse.

However, a far more sinister issue I rapidly learned about in the US was the manufacture of opinion at which म्लेच्छ-s are more notorious than anyone.

Simply defined "manufacture of opinion" is a process by which the certain opinions are manufactured via the medium of certain chosen academics and then presented as the correct, fashionable or proper opinion to be expressed by the masses. This might be carried out by the Anglosphere both on its own citizens, members of the extended Leukosphere and outsiders including other Abrahamists and heathens alike. There are numerous examples of manufacture of opinion:


 1.  The opinion was manufactured by some with vested agendas that the attack on Iraq was of paramount importance to the American future. So this became the majority opinion of the American people and they whole-heartedly supported their government's decision to go to war on Iraq and kill Saddam and his henchmen.

 2.  The opinion was created that Musharaff rather than being at the heart of international terrorism and the cause of 9/11, was instead the front line ally of the US and the guardian of US from the raging beards. With this the opinion was created amongst Americans that Pakistan was an ally and Iraq, Iran and North Korea formed an "axis of evil" who need to be invaded though they had hardly anything to do with 9/11.

 3.  BJP is a Nazi-like party which would make make Indians believe that all physics stemmed from ancient Hindu texts, and exterminate Abrahamists in India. In contrast, the GOP is a legitimate political party -- nobody quibbles too much about their evangelist fundamentalist support base which wants to convert all Hindus or insane policies in the Middle East (hey, how come no one is talking of Moslems dying there?) or the inclination to have intelligent design as an alternative to biology.

 4.  Jared Diamond's book GGS is a must read and its universalist history is the correct picture of the making of the modern world for a young high school student.

These manufactured majority opinions soon make any questioner look like fool --- in a city of nuts a sane man looks nutty. Along with these majority opinions certain norms or fashion are also encouraged in society such that everyone wants to conform to them --- the same hair styles, the similar hair colors, similar opinions on what is "cool".

We do not live in a perfect world, but it does amuse one to see how seriously educated adult people take their opinion on so many issues, without even suspecting that they have been manufactured for them.


