
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Jx evades a मारण](https://manasataramgini.wordpress.com/2007/05/29/jx-evades-a-marana/){rel="bookmark"} {#jx-evades-a-मरण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 29, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/05/29/jx-evades-a-marana/ "3:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Whenever we have executed that loop by instinct, thus far, some vighna has invariably come our way. This time again the mudgara-like blow of the trailokya-स्तम्भिनी caused us new trouble. But we received a signal to press on, undaunted. Jx who had been knocked out by the डामरिका strike after being routed by the chIna-s, scraped the rock-bottom of his existence and fled further from भानु-कक्ष to the township of indra-जाल. Here, the spirit of भृगु and अङ्गिर ancestors faintly possessed him. He suddenly recovered and performed a पुरस्चरण of 8 months. It was around the culmination of this period that the 3rd hero had broken forth. At that point a 3-pointed मारण attack was deployed: one prong of it seized the अमात्य but the muni deploying nidra-वार्तालि countered its fatal effects, even as the अमात्य got enough time to deploy the ओषधि prayoga. The other one headed towards to him who was dwelling in indra-जाल. With his पुरश्चरण complete the अनाहत chakra was filled with the presence of hamseshvara and हम्सेश्वरी surrounded by the 54 deities of the वायव्य मयूख-s. He was unable to grasp the full significance of this manifestation, but the spontaneous power of manifestation of the shikhin yugma became clear -- that fiery manifestation of shiva-शिवा in the circle surrounding the कं bIja. They verily say only one who has received the पूर्ण-दीक्ष can truly withstand the force of the mistress of the kula path without being confused. Past midnight the ब्रह्मास्त्र-विद्या crashed onto him with 4 successive strikes, but the power his own protective mantra was unbelievable the prayoga which was so certain to effect मारण simply rebounded off him.

-*-*-*-

That which had left Jx now pursued us in the form of the खाण्डवन् to compass our destruction. We had been expecting this any time. However, we evaded it after a skirmish that resembled in many ways our first encounter with the खाण्डवन्-s.


