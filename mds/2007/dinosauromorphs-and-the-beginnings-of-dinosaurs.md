
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Dinosauromorphs and the beginnings of dinosaurs](https://manasataramgini.wordpress.com/2007/07/21/dinosauromorphs-and-the-beginnings-of-dinosaurs/){rel="bookmark"} {#dinosauromorphs-and-the-beginnings-of-dinosaurs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 21, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/21/dinosauromorphs-and-the-beginnings-of-dinosaurs/ "4:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RqGmYR_WxfI/AAAAAAAAAMs/HA5HBG9_pew/s320/dinosauromorphs.jpg){width="75%"}
```{=latex}
\end{center}
```



The crocodilians and birds are the only the surviving representatives of the two great lineages of archosaurs that diverged in the Triassic. The "crocodile line" (depending on definition pseudosuchians or crurotarsans) appear to have undergone the first great adaptive radiation in the Triassic spawning an enormous diversity of predatory and herbivorous forms that mimicked the dinosaurs and later crocodylomorphs in many ways. By the Jurassic, the only survivors of the crocodile line were the crocodylomorphs, which radiated throughout the Mesozoic into a diverse array of forms --- both terrestrial and aquatic (freshwater and marine) predators and some herbivorous forms. On the other hand was the "dinosaur-line" (typically defined as ornithodirans) which included the pterosaurs, dinosauromorphs and dinosaurs. They too arose in the Triassic but were marginal in their ecological role to start with. But in course of of the Mesozoic, beginning from the Jurassic saw the meteoric rise of dinosaurs replacing all the older archosaurs and other amniotes as the dominant terrestrial vertebrates. There are several unclear issues concerning this macro-evolutionary phenomenon: 1) Are the pterosaurs really related to dinosaurs to exclusion of other archosauromorphs? 2) How did the dinosaurs replace the earlier dominant Triassic faunas? 3) What was the diversity of the closest sister group of the dinosaurs-- the dinosauromorphs ?

The dinosauromorphs are a fascinating group of archosaurs that are critical to the understanding of the origin of dinosaurs. Colloquially we are using the term here to include only non-dinosaurian dinosauromorphs. The first[ bona fide]{style="font-style:italic;"} dinosauromorph to be studied was [Lagerpeton ]{style="font-style:italic;"}from South America described by Alfred Romer. Subsequently Sereno named [Marasuchus]{style="font-style:italic;"}. The earlier described [Scleromochlus ]{style="font-style:italic;"}has also been suspected to be a dinosauromorph, though Benton believed it might be closer to the pterosaurs. Both Lagerpeton and Marasuchus had been described as small predators with a generalized morphology from which the more specialized dinosaurs emerged later. However, in the past few years the diversity and spread of dinosauromorphs is better understood. This is largely due to several new fossils: 1) The exquisite fossils of [Silesaurus ]{style="font-style:italic;"}from Poland. 2) [Sacisaurus ]{style="font-style:italic;"}from Brazil. 3) Most recently [Dromomeron ]{style="font-style:italic;"}from New Mexico.

An analysis by Irmis et al of [Dromomeron ]{style="font-style:italic;"}and several other forms from the Late Triassic Chinle formation suggests the following: 1) There was a wide spread and diversity of dinosauromorphs over the world. [Dromomeron ]{style="font-style:italic;"}was found to be closest to [Lagerpeton ]{style="font-style:italic;"}and another form unfortunately named [Eucoelophysis ]{style="font-style:italic;"}to be a sister taxon on [Silesaurus]{style="font-style:italic;"}. The later two are likely to have been herbivores similar to the ornithischians 2) They coexisted with basal theropod dinosaurs like [Coelophysis ]{style="font-style:italic;"}and even more primitive basal saurischians like [Chindesaurus]{style="font-style:italic;"}, a Herrerasaurid. 3) The appear to have persisted with dinosaurs from the Carnian through the Norian epochs of the Late Triassic period. A parallel analysis by Irmis, Nesbitt and co has shown that the North American late Triassic was bereft of ornithischians, though there were many pseudosuchians that imitated the former. In a parallel analysis of [Eocursor ]{style="font-style:italic;"}a very primitive ornithischian from the Norian of South Africa, by Butler et al also suggested that the great radiation of ornithischians was only after the Triassic. Based on this it is proposed that the replacement of dinosauromorphs by dinosaurs was gradual an asynchronous in different parts of the globe. It is also implied that at least in North America that the dinosauromorphs like [Eucoelophysis ]{style="font-style:italic;"}and "crocodile-line" archosaurs like [Revueltosaurus ]{style="font-style:italic;"}might have occupied the niches related to herbivory in place of the ornithischians.

However, there is a suggestion by Langer et al that the notch and anterior structure of the dentary in the [Silesaurus ]{style="font-style:italic;"}and [Sachisaurus ]{style="font-style:italic;"}like forms might be the precursor of the predentary, a synapomorphy of the ornithischians. This raises the possibility that after all these forms might represent very primitive ornithischians (?).


