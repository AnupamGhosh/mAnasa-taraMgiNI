
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Iterative circles](https://manasataramgini.wordpress.com/2007/10/07/iterative-cricles/){rel="bookmark"} {#iterative-circles .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 7, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/07/iterative-cricles/ "2:33 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/Rwju9kaSUgI/AAAAAAAAAO8/5Kvh00dfL4Q/s320/3spir1.png){width="75%"}
```{=latex}
\end{center}
```



```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/Rwju-kaSUiI/AAAAAAAAAPM/0g985PxrFZQ/s320/tetra.png){width="75%"}
```{=latex}
\end{center}
```



