
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On the समराङ्गण सूत्रधार: yantra-s](https://manasataramgini.wordpress.com/2007/01/21/on-the-samarangana-sutradhara-yantra-s/){rel="bookmark"} {#on-the-समरङगण-सतरधर-yantra-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 21, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/21/on-the-samarangana-sutradhara-yantra-s/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vimAna has been noticed by many since the publication of the समराङ्गण सूत्रधार (SS) of the great [rAja bhoja-deva परमार](http://manollasa.blogspot.com/2005/12/raja-bhoja-glory-and-tragedy-of.html). The description is certainly obscure in parts (especially given the corrupt manuscripts), but what can be made sense of in the account definitely describes a flying machine. Many years ago when we had talked about this and both Mis-creant and द्विपक्ष-kesha expressed their skepticism that anything like this could have ever been really [mentioned ]{style="font-style:italic;"}in the Hindu texts. After all, they chimed, there is enough reason to believe that the वैमानिक शास्त्र was a hoax of a text. While I was skeptic of the वैमानिक, I did know that bhoja-deva's work was real and so also the mention of the vimAna in it. So, I took these two to the library of Samskrit texts where I had a special inner access and showed them the text. Now many years later while planning new scripts for nATaka-s we ended up talking of it again- of course starting with the famous topic: did Hindus have an airplane?

The SS 31 clearly mentions a अम्बर्चारि-vimAna in addition to several other mysterious yantras which apparently include "robots" like द्वारपाल yantra, योधा yantra and gaja-yantra. In describing the vimAna the SS states the following SS 31.95-98:

[लघु-दारु-मयं महाविहङ्गं दृढ-सुश्लिष्ट-तनुं विधाय तस्य ।]{style="color:#99cc00;"}\
[उदरे रस-यन्त्रं-आधीत ज्वलाधारम्-अधो।अस्य छाग्नि-पूर्णं ॥]{style="color:#99cc00;"}\
[तत्रारूढः पूरुषस्-तस्य पक्ष-द्वन्द्वोच्-चाल-प्रोज्-झितेनानिलेन ।]{style="color:#99cc00;"}\
[सुप्त-स्वान्तः पारदस्यास्य शक्त्या चित्रं कुर्वन्-नम्बरे याति दूरं ॥]{style="color:#99cc00;"}\
[इत्थ्मेव सुरमन्दिर-तुल्यं सञ्चलत्य-लघु दारु विमानम् ।]{style="color:#99cc00;"}\
[आदधीत विधिना चतुरोऽन्तस्-तस्य पारद-भृतान् दृढ-कुम्भान् ॥]{style="color:#99cc00;"}\
[अयः कपालाहित-मन्दवह्नि-प्रतप्त-तत्-कुम्भ-भुवा गुणेन ।]{style="color:#99cc00;"}\
[व्योम्नो झगित्याभरण त्वमेति सन्ततप्त-गर्जद्-रस-राज-शक्त्या ॥]{style="color:#99cc00;"}

From the description its fairly clear, even factoring in the mention of rasa, that it is not a vimAna flying by mantra prayoga, but a real mechanical yantra. Amongst the scholars the learned VRR Dikshitar in his book on war in ancient India, and the great V. Raghavan in his essay on Hindu machines, are inclined to believe that vimAna was a real flying machine. But the other erudite scholar Vasudevasharan Agarwala who re-edited the SS with a new manuscript believes it is imaginary. While I fully admit that the description given by the king can hardly be used to make a vimAna today, one can glean some prominent features of it:

 1.  It was made of light wood- at least conceptually it was something light so as to fly. 2) It had a mercury-containing device in its "belly" and fire and at the rear end. 3) Its body was well-welded (or firmly joined) and had two wings and resembled a large bird. 4) Repeatedly the king says it runs by the "power" of mercury, which is described as contained in four firm pots/vessels. These are heated by a slow fire from an iron heating vessel. "Powered by mercury it roars into the sky".

Now the SS is fairly serious about this mercury power business: Immediately after the vimAna, king bhoja mentions a yantra powered by heated mercury again, which is used for दुष्ट-गजोच्चाटन (over-throwing of enemy elephants). It is supposed to create a roar like नृसिम्ह. The possibility of engine using mercury comes to mind -- between the 1930-1950 there were experimental engine designs (The William Emmet engines) using Hg as the working fluid used in power plants. While they were more efficient than the steam turbines, cost and hazards of mercury put an end to the endeavors. It should be remarked that one of the early models of James Watt's rotary engines also used Hg as a working fluid. However, the possibility of such devices powering flight is considered remote by engineers.

Having reached the end of this line of inquiry, we looked at comparative evidence from other texts and civilization. In the west the invention of the engine is normally attributed to a yavana sage Hero of Alexandria who composed on mechanics around 50-100 CE. One of his devices is the aeolipile, which literally means the door of wind (Aeolus and his 11 sons the Aeolii being wind deities). Interestingly, many of the machines of Hero are parallel to that of bhoja's yantra-s. His collections include water machines (Pneumatica), machines for creating wonders in temples like opening temple doors, statues that pour libations etc. (Automata), figures the move and perform drama (automata theater), directions for architects, including means of lifting heavy objects (Mechanica) and siege and war machines (Belopoeica). Likewise amongst bhoja-s yantra-s we have: 1) the vAri-yantra running on water flow, 2) yantra-s for creating adbhuta-s (wonders) like showing fire in water 3) "Robotic" parrot, elephant and men and women act together, automatic beating of drums. 4) A remote controlled door-keeper for the night who blocks thieves. 5) उच्छ्रय-yantra: a mechanical lift for raising objects. 6) Siege machines for defending forts and robotic door guards.

These parallels are reminiscent of such other similarities between the Greek and Hindu worlds. In particular the use of wonders in temples, which appears to be very important for Hero of Alexandria, is a critical aspect of the Neo-Platonic religious thought ([theurgy]{style="font-style:italic;"}), along with temple construction and image-veneration. This cultural element has so much in parallel with India that it is not surprising to find yantra-s in a Hindu work that otherwise deals with religious architecture in large measure. Another point to note in this context is the parallel between Hero's formula for the area of a triangle and brahmagupta's generalization for the the cyclic quadrilateral. Thus, irrespective of their place of origin, we may surmise that ideas for mechanical devices, along with many other scientific, pagan religious, cultural and mathematical ideas, were very much a part of the shared ancient thought system that encompassed bhArata and Greece. Given the aeolipile, it is not impossible that Hindu-s devised some version of an engine (perhaps rotary) with Hg as the working fluid (the howls of the skeptics and the toxicity of mercury notwithstanding). While I am personally skeptical about how such a device might have powered flight, we should at least credit bhoja with a rather prescient bit of thinking: using an engine to power an aircraft.

Finally, one yantra mentioned by king bhoja caught my attention --- the सूर्यादि-graha-gati-pradarshana-paraM gola-भ्रमणं.\
The king says (SS 31.61-62): [गोलश्-च सूचि-विहितः सूर्यादीनां प्रदक्षिणं ।]{style="color:#99cc00;"}\
[परि-भ्राम्यत्य्-अहोरात्रं ग्रहाणां दर्शयन् गतिं ।]{style="color:#99cc00;"}\
\[Images of] The sun and the planets are made to revolve without any connection and these revolutions show the occurrence of day and night and the motions of planets. Thus, bhoja-deva describes a yantra that simulates the solar system. This immediately brings to mind the recently described analysis of an ancient artifact that was discovered in a ship-wreck over 100 years ago- the [Antikythera mechanism]{style="font-style:italic;"} from Greece. It is interpreted as a mechanical device to simulate the sun, moon and probably planets and successfully compute eclipses. That, such devices were used in the yavana world, is confirmed by a statement by the Roman author Cicero who saw the device captured from Archimedes by the Roman general Marcellus. In his de Natura Deorum Cicero mentions that his friend Posidonius had made a machine in which "each one of the revolutions of which brings about the same movement in the sun and moon and five wandering stars as is brought about each day and night in the heavens." This last statement is indeed reminiscent of the record of bhoja-deva.

All things taken together, Neo-Platonic Western Eurasia and bhArata at the time of bhoja-deva's era were "technologically" rather developed (also note the remarkable description of the cranial surgery performed on bhoja-deva himself). But in both places the violent visitations of the Abrahamistic madness [obliterated](http://manollasa.blogspot.com/2006/07/emperor-julian.html) or [defaced](http://manollasa.blogspot.com/2006/08/makings-of-islamic-science.html) these old worlds. This is a point often overlooked by many, especially in the West.


