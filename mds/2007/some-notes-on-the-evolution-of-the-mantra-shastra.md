
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on the evolution of the mantra शास्त्र](https://manasataramgini.wordpress.com/2007/09/10/some-notes-on-the-evolution-of-the-mantra-shastra/){rel="bookmark"} {#some-notes-on-the-evolution-of-the-mantra-शसतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 10, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/10/some-notes-on-the-evolution-of-the-mantra-shastra/ "3:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A hall mark feature of the Hindu tradition right from its proto-Indo-Iranian antecedents has been the mantra शास्त्र. It is the mainstay of both the orthopraxy (आस्तीक matas) as well as most heteropraxies (namely bauddha and jaina matas) of the dharma. The technical basis of मन्त्रशास्त्र (a secret only understood by practicing insiders with a knowledge of the texts or शास्त्र-s learned with assistance from an आचार्य and experienced with assistance from a guru) has been remarkably conservative and stable since its earliest traceable history going back to the proto-Indo-Iranian period. It is on account of this stability of its foundational basis that many traditional adepts are not very particular in distinguishing the various historical layers of the mantra-शास्त्र. Thus, they exhibit a fluid syncretic tendency of melding different layers and traditions. Unfortunately, even in the modern era few adepts have acquired education in evolutionary analysis and Indo-European and Dravidian linguistics to produce hypothesis to analyze the historical developments in मन्त्रशास्त्र effectively. Yet, even a cursory glance of the मन्त्रशास्त्र illustrates two great categories within it: 1) the older vedic मन्त्रशास्त्र and 2) the later tantric mantra शास्त्र. Nevertheless, the latter exhibits many features which appear to have been derived from seeds in the former.

The vedic mantras probably right from the PI-Ir exhibit a diversification into 3 distinct forms simultaneously deployed in major shrauta rituals --- namely the ऋक्स्, yajushes and sAman-s. The ऋक्स् where composed in Chandas and declare their ऋषि, Chandas and देवता with great care. Special deployments like the सामिधेनी-s or kathayanta prayoga or the न्यून्क and निनार्द style recitations were often decorated with "bIja-s" like [ॐ, हिं षों, षोम्साव्^ॐ]{style="color:#99cc00;"} and अक्षर-s like [su, mat, pad, vag, de]{style="color:#99cc00;"}. These elements respectively are the precursors of the ऋष्यादि न्यास of the tantric mantras and resemble the insertion of bIja-s in mantra-s of the tantric mantra शास्त्र. The yajushes are often ritual formulae with elements like नमः, वौषट्, वषट् and स्वाहा (also फट् as per yajurvedic, atharvanic and सामवेदिc tradition), which also comprise the typical elements of most major tantric mantras. The तैत्तिरीय AraNyaka while laying out the अभिचार formulae for the deadly forms of agni and the" killing by the year" formula gives the following incantations to be used with them(TA 4.27.1) :\
[खट् फट् जहि । छिन्धी बिन्धी हन्धी कट् । इति वाचः क्रूराणि]{style="color:#99cc00;"}\
i.e. the calls KhaT फट् and jahi (slay) , छिन्धी (rend apart), bindhi (split up) , handhi (smite) and kaT are used in fierce (i.e. अभिचार) rites.\
Again the तैत्तिरीय AraNyaka supplies similar calls in the formula called "the placing in yama's jaws" (TA 4.37.1):\
[यदमुं यमस्य जम्भयोः । आदधामि तथ हि तत् । खण्-फण्-म्रसि ॥]{style="color:#99cc00;"}\
The फट्, jahi chindhi etc calls seen in these yajushes are typical of later tantric mantras with similar intent.\
The magical stobha-s in the mantra-s of the सामवेद also bear a resemblance to the bIja-s of the later tantric mantra शास्त्र.

However, the divide between the two is still apparent:


 1.  The tantric mantras are largely dominated by bIja-s and standalone non-semantic अक्षर clusters, whereas the vedic mantra-s are predominantly semantic in structure with the exception of the musical sAman where the underlying ऋक् is greatly distorted (except certain stobha only songs, which lack a ऋक्). Most vedic mantra-s embed a lot in the meaning of "sentences" and as a result are typically longer than a tantric mantra, whose semantically meaningful elements are very short (e.g. sudarshana chakra mantra: [ऒं सहस्रार हुं फट्]{style="color:#99cc00;"}; सहस्रार= thousand spokes -- that is all ! ) or may even be non-existent (e.g. the श्रीविद्या कादिमत: [ka e i la ह्रीं | ha sa ka ha la ह्रीं | sa ka la ह्रीं||]{style="color:#99cc00;"}).


 2.  The vedic canon is very well preserved with great fidelity and little admission of interpolation and syncretism of extraneous elements. In contrast, while the tantric mantra-शास्त्र admits to many "canonical" texts, these hardly exhibit anything close to the level of fidelity in vedic preservation, instead evolving rapidly while only preserving the mantra and intent. An extreme case being the rudra-यामल that is taken as a canon but is currently a jumble of several texts including some questionable attributions.


 3. The shrauta ritual is very standardized and provide a strict system of orthopraxis. The tantric rituals while admitting the concept of orthopraxis is more ready in admitting syncretic elements as a result of which different traditions exhibit much greater melding.

Now the big evolutionary question is: how did the transition occur from the vedic to the tantric form? The गृह्य rites with one fire and a स्थण्डिल and the early विधान rituals like those in the ऋग्विधान might offer clues in terms of the praxis. But that is not the issue I am seeking to explore here. I am primarily concerning myself about the mantras themselves.

In this regard one might note the following disparate elements that might offer clues:

 1.  The early post-vedic compositions like nAma-संग्रह-s for देवता-s (e.g. the celebrated विष्णु सहस्रनामम् from the महाभारत). These appear to be predecessors of the sahasra and shata नामावली-s that proliferate in the tantric period and form a major component of tantric worship, even if not as the primary mantras.


 2.  Not all tantric mantra-s are bIja mantra-s but a small subset of them are actually अनुष्टुभ्-s. One of the potentially oldest and most famous of these is the ugra विष्णु or मन्त्रराज अनुष्टुभ् ([उग्रं वीरं महाविष्णुं ज्वलन्तं सर्वतोमुखं । नृसिम्हं भीषणं भद्रं मृत्यु-मृत्युं नमाम्यहं ॥]{style="color:#99cc00;"})\
A collection of such अनुष्टुभ् mantras include: 1) ugra विष्णु 2) the कार्तवीर्यार्जुन नष्ट dravya लाभ mantra 3) vana-durga mantra 4) the shAstA mantra which identifies him with revanta. 5) The skanda अनुष्टुभ् mantra. Also related to this point are the gAyatrI mantras for specific deities that form an important, though not central aspect of tantric worship. These gAyatrI-s with the 3 pada-s bearing the words "vidmahe", "dhimahi" and "prachodayat" with 3 names of the deity are all modeled after the rudra gayatrI-s found in some कृष्ण yajurvedic saMhitA-s, which in turn is modeled after the ऋग्वेदिc savitrI of विश्वामित्र.


 3.  Mantras associated with circum-vedic traditions, but distinct from traditional vedic and tantric mantra-s: E.g. the AsurI kalpa, the उच्छुष्म kalpa and धूर्त kalpa of the atharva veda परिशिष्ठ which provide mantras and rites for the worship of AsurI दुर्गा, उच्छुष्म rudra and कुमार. It is this class of mantras that might offer the important bridge between the tantric mantras and yajushes which as noted above have elements similar to the tantric formulae.

Of these elements the concept of नामावली-s appear to go back to the proto-ईईर् period-- in the Iranian tradition we encounter the 100 names of ahurA-माज़्धा that clearly resemble the later नामावलीस् of Indic tradition. While there are no explicit नामावली-s in the veda, the vedic नमः mantras of the yajur (e.g. the शतरुद्रीय) appear to be an archaic relative of the नामावली. This become apparent when one compares it with the [early-post-vedic hymn of दक्ष to rudra](http://manollasa.blogspot.com/2006/02/younger-shatarudriya.html). Thus, the नामावली mantras appears to be an early concept in Indo-Iranian worship that appears to have been merely expanded greatly in the tantric realm.

The अनुष्टुभ् mantras clearly differ from all tantric mantras both in lacking बीजस् and in their form, which resembles ऋक्-s. Some of them like the mantra-rAja are clearly fairly ancient. We also observe such mantra-s in the pre-tantric circum-vedic texts like those of the [पाशुपत-s](http://manollasa.blogspot.com/2006/05/shaivas-pashupata-s.html) (e.g.[रुद्रं क्रुद्धाशनिमुखं देवानां ईश्वरं परं । श्वेत-पिङ्गलं देवेशम् प्रपद्ये शरणागतः ॥]{style="color:#99cc00;"}). These features suggest that the evolution of the mantra शास्त्र passed through a phase where new ऋक् like mantra-s, but mainly in the shloka meter were composed. This stage of shloka mantra-s might have also overlapped with the विधान period and the deployment of ऋक्स् in a tantricized context. For example the well-known yAm kalpayanti ऋक् of प्रतिचीन अङ्गिरस and the indra mantra "yata indra भयामहे..." are deployed in later AV tradition in a tantric form.

But it is in the परिशिष्ट kalpa-s of the AV that one finds the true precursors of the tantric mantras. In a sense these mantra-s are descendants of vedic mantras such as those of the arrows of rudra found in the AV and YV traditions (e.g. AV-shaunaka 3.26 and 3.27).\
Examples of अभिचारादि mantras to the उच्छुष्म rudras from the उच्छुष्म kalpa:\
[कालाय करालाय नमः स्वाहा ।]{style="color:#99cc00;"} (shatru नाशनं)\
[अमोघाय नमः स्वाहा ।]{style="color:#99cc00;"} (shatru आवेशनं)\
[वृषभाय नमः स्वाहा ।]{style="color:#99cc00;"} (शत्रूनाम् धनक्ष्यं)\
[उच्छुष्माय नमः स्वाहा ।]{style="color:#99cc00;"} (उन्मादनं)\
[उच्छुष्म-रुद्राय नमः स्वाहा ।]{style="color:#99cc00;"} (shatru दहनं)\
[अघोराय नमः स्वाहा ।]{style="color:#99cc00;"} (roga निवारणं)\
[सुवर्णछुडाय नमः स्वाहा ।]{style="color:#99cc00;"} (roga अभिबाधनं)\
[हाहाहीही नमः स्वाहा ।]{style="color:#99cc00;"} (go लाभ मन्त्रं)\
[महावक्त्राय पिङ्गलनेत्राय नमः स्वाहा ।]{style="color:#99cc00;"} (वृष्टि)\
[खनखनाय नमः स्वाहा ।]{style="color:#99cc00;"} (prasiddhi प्राप्ति)\
[घनघनाय नमः स्वाहा ।]{style="color:#99cc00;"} (yuddha vijaya)\
[महापशुपतये नमः स्वाहा ।]{style="color:#99cc00;"} (pashubhyo .अभिरक्षणं; from animals)

Other examples of longer mantras from the उच्छुष्म kalpa are:\
[नमः कट विकट कण्टे माटे पाटले विकले असौर्यासौ असौर्यासौ पृथिवीष्टका इष्टकाजिनात्यून्यो सौगलुं तिगलुं ते कटम् असि कटप्रवृते प्रद्विष रुद्र रौद्रेणावेशयावेशय हन हन दह दह पच पच मथ मथ विध्वंसय विध्वंसय विश्वेश्वर योगेश्वर महेश्वर नमस्ते ऽस्तु मा मा हिंसीः हुं फट् नमः स्वाहा ॥]{style="color:#99cc00;"} (a जपा mantra)

[शिवे जटिले ब्रह्मचारिणि स्तम्भनि जम्भनि मोहनि हुं फट् नमः स्वाहा ॥]{style="color:#99cc00;"} (an Atma-रक्षा mantra)

Interestingly, two of the mantras from one of the earliest tantric texts of the bauddha tradition known as the महामायूरी-विद्या-राज्ञी are orthologous to the above two longer mantras of the AV kalpa.

The AsurI kalpa mantra (shaunaka version):\
[ॐ कटुके कटुक-पत्त्रे सुभगे आसुरि रक्ते रक्तवाससे अथर्वणस्य दुहित्रे अघोरे अघोर-कर्म-कारिके [अमुकं] हन हन दहदह पच पच मथ मथ तावद् दह तावत् पच यावन् मे वशम् आनयसि स्वाहा ॥]{style="color:#99cc00;"}\
The AsurI kalpa mantra (पैप्पलाद version):\
[ॐ नमः कटुके कटुक-पत्त्रे सुभगे आसुरि रक्तवाससे अथर्वणस्य दुहित्रे घोरे घोर-कर्माणि-कारिके [अमुकस्य] प्रस्थितस्य मतिम् भज उपविष्टस्य भगं दह शयितस्य मनो दह प्रवृद्धस्य हृदयं दह दह दह तावद् दह दह पच पच मथ मथ तावत् पच यावन् मे वशम् आगचछेत् स्वाहा ॥]{style="color:#99cc00;"}

The shorter atharvanic kalpa mantra-s resemble the shorter mantra-s typical of the later tantra-s, while the longer ones from both AsurI and उच्छुष्म are closer to the pada-माल mantras of later tantric mantra-s. The mantras of the AV kalpa share certain specific major features of the tantric mantras:

 1.  Use of imperative terms like hana, daha and pacha. These words are already seen in the AV saMhitA mantras. e.g.:\
[छिन्ध्य् आ छिन्धि प्र छिन्ध्य् अपि क्षापय क्षापय ॥]{style="color:#99cc00;"} (AV-S 12.5.51)\
[वृश्च प्र वृश्च सं वृश्च दह प्र दह सं दह ॥]{style="color:#99cc00;"} (AV-S 12.5.62)\
Imperatives in mantra are seen right from the earliest layers of the vedic mantras i.e. from the RV itself. They increase some what in prominence in the AV tradition (see darbha सूक्तं and are extremely prevalent in tantric mantras, especially the माला mantras. An ancient example of such a mantra, albeit in the form of ऋक् is RV 10.87.23:\
"[विषेण भङ्गुरावतः प्रति ष्म रक्षसो दह ।]{style="color:#99cc00;"}"\
With poison turned against the vicious rakshas burn  away!


 2.  onomatopoeic forms like हाहा हीही and 3) the invocation of shaktis as seen in the आत्मरक्षा mantra of the उच्छुष्म kalpa and the AsurI mantra of the AsurI kalpa.

However, these kalpa mantras are very light on bIja-s, at best using the form OM and huM already known from vedic tradition. Thus, in the AV kalpa-s we see the ancestors of tantric mantras and connect them to their vedic mantra precursors. This also suggests that the major transition into classical tantric mantra-शास्त्र was accompanied by the innovation of the extensive use of bIja-s (an atavistic expression of the vedic linguistic tradition in a new form?).


