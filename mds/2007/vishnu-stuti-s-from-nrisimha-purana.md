
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [विष्णु stuti-s from नृसिंह पुराण](https://manasataramgini.wordpress.com/2007/10/09/vishnu-stuti-s-from-nrisimha-purana/){rel="bookmark"} {#वषण-stuti-s-from-नसह-परण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 9, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/09/vishnu-stuti-s-from-nrisimha-purana/ "4:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RwxhMMEvi5I/AAAAAAAAAPU/XjcdNMSRT7o/s320/nArAyaNa.jpg){width="75%"}
```{=latex}
\end{center}
```



The below stuti/ divya-nAma पारायणं was recited by rudra to praise विष्णु to incite him to slay हिरण्यकशिपु. A person who due to inability or lack of time cannot recite the विष्णु-सहस्रनाम स्तोत्रं on a day of laukika worship of विष्णु might recite this stotra instead. A person of the 4th वर्ण wanting a suitable stuti in deva भाष for his devotions might sincerely recite this stuti.

[॥ महादेव विरछित वासुदेव स्तुति ॥]{style="color:#0000ff;"}\
[विष्णुर्-जिष्णुर्-विभुर्-देवो यज्ञेशो यज्ञपालकः ।\
प्रभ-विष्णुर्-ग्रसिष्णुश्च लोकात्मा लोकपालकः ॥ १]{style="color:#0000ff;"}\
[केशवः केशिहा कल्पः सर्व-कारण-कारणं ।]{style="color:#0000ff;"}\
[कर्म-कृद्-वामताधीशो वासुदेवो पुरुष्टुतः ॥ २\
आदिकर्ता वराहश्च माधवो मधुसूदनः ।]{style="color:#0000ff;"}\
[नारायणो नरो हंसो विष्वक्सेनो हुताशनः ॥ ३]{style="color:#0000ff;"}\
[ज्योतिष्मान् द्युतिमान् श्रीमान् आयुष्मान् पुरुषोत्तमः ।\
वैकुण्ठः पुण्डरीकाक्षः कृष्णः सूर्यः सुरार्चितः ॥ ४\
नरसिंहो महाभीमो वज्रदंष्त्रो नखायुधः ।\
आदिदेवो जगत्-कर्त योगेशो गरुडध्वजः ॥ ५\
गोविन्दो गोपतिर्-गोप्त भुपतिर्-भुवनेश्वरः ।\
पद्मनाभो हृशिकेशो विभुर्-दामोदरो-हरिः ॥ ६\
त्रिविक्रंअस्-त्रिलोकेशः ब्रह्मेशः प्रीतिवर्धनः ।\
वामनो दुष्ट-दमनो गोविन्दो गोपवल्लभः ॥ ७\
भक्तिप्रियोऽच्युतः सत्यः सत्यकीर्तिर्-ध्रुवः शुचिः ।\
कारुण्यः करणो व्यासः पापहा शन्ति-वर्धनः ॥ ८]{style="color:#0000ff;"}\
[मेधावी शास्त्र-तत्त्वज्ञो मन्दार-गिरि-केतनः ।\
बदरी-निलयः शान्तः तपस्वी विद्युत-प्रभः ॥ ९\
भूतावासो गुहावासः श्रिनिवासः श्रियः पतिः ।\
तपोवासो दमोवासः सत्यवासः सनातनः ॥ १०]{style="color:#0000ff;"}\
[पुरुषः पुष्कलः पुण्यः पुSकराक्षो महेश्वरः ।\
पूर्णः पूर्तिः पुराणज्ञ्यः पुण्यज्ञः पुण्यवर्धनः ॥ ११\
शङ्खि चक्री गदी शार्ङ्गी लाङ्गली हली ।\
किरीटी कुण्डली हारी मेखली कवछी ध्वजी ॥ १२\
जिष्णुर्-जेता महावीरः शत्रुघ्नः शत्रुतापनः ।\
शान्तः शान्तिकरः शास्ता शङ्करः शन्तनुस्तुतः ॥ १३\
सारथिः सात्त्विकः स्वामी सामवेदः प्रियः समः ।\
सावर्णः साहसी सत्वः संपूर्णाशः संऋद्धिमान् ॥ १४]{style="color:#0000ff;"}\
[स्वर्गदः कामदः श्रीदः किर्तिद आर्तिनाशनः ।\
मोक्षदः पुण्डरीकाक्षो क्षीराब्धि धृतकेतनः ॥ १५]{style="color:#0000ff;"}\
[स्तुतः सुरासुरैरीशः प्रेरकः पापनाशनः ।\
त्वं यज्ञस्-त्वं वषट्कारस्-त्वम्-ओङ्कारस्-त्वम्-अग्नयः ॥ १६]{style="color:#0000ff;"}\
[त्वं स्वाहा त्वं स्वधा देवः त्वं सुधा पुरुषोत्तम ।]{style="color:#0000ff;"}\
[नमो देवातिदेवाय विष्णवे शाश्वताय च ॥ १७]{style="color:#0000ff;"}

[अनन्तायाप्रमेयाय नमस्ते गरुडध्वज ॥]{style="color:#0000ff;"}

The deva-s composed the below stuti in the connection with the incarnation of विष्णु as matsya. A person desiring victory over his foes may circle a sarvatobhadra मण्डल five times, like the one built by emperor विक्रमादित्य of the gupta-s at Deogarh, uttering the below stuti:

[॥ देव-विरछित विजयार्थ स्तुति ॥]{style="color:#0000ff;"}\
[नमस्ते देवदेवाय लोकनाथाय शार्ङ्गिणे ।\
नमस्ते पद्मनाभाय लोकनाथाय शार्ङ्गिणे ।]{style="color:#0000ff;"}\
[नमस्ते पद्मनाभाय सर्वदुःखापहारिणे ।\
नमस्ते विश्वरूपाय सर्व-देवमयाय च ।\
मधु-कैटभ-नाशाय केशवाय नमो नमः ॥]{style="color:#0000ff;"}


