
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The virile तैत्तिरीयक's jealousy](https://manasataramgini.wordpress.com/2007/07/15/the-virile-taittiriyakas-jealousy/){rel="bookmark"} {#the-virile-तततरयकs-jealousy .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 15, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/15/the-virile-taittiriyakas-jealousy/ "12:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

When we saw the putraka we noticed that he was the atavistic manifestation of the great Fourier of Mangalagrama. While Fourier of Mangalagrama was a dreaded भ्रातृव्य, he was still a man of extraordinary qualities. A mathematician, engineer, linguist and writer in his secular existence was a personified mantramahodadhi when he was still alive. His mantra-शास्त्र expertise could be countered by few-- he has crushed S's अमात्य with ease, he pierced MM's spells despite their great intensity and his valaga-s were un-defeated by most of us. My own astra-s withered before him like the वानर-s before कुम्भकर्ण. S and I were overthrown like सात्यकि and भीमसेन by bhagadatta mounted on supritika, the muni was bound, and the third vIra's ratha was pierced by his buried valaga-s. His deployment of यक्षिणी-s, मोहिनी-s and डावी-s were feared by all. But his mistimed shara was back-hurled by MM and he was paralyzed. Then the virile तैत्तिरीयक needled by Fourier's sharas also sent a prayoga his way, which finally sunk him. The putraka was indeed a multidimensional atavism of the old Fourier, but he did no possess an iota of the mantra शास्त्र of the old one. Hence, he was merely a large rat snake compared to the king cobra that Fourier was. With that Fourier's mantra शास्त्र lineage had come to a complete close.

However, even the virile तैत्तिरीयक could not protect his सूनु when Fourier picked him out with a strong direct hit. The clever third vIra having broken forth was victorious. Fourier's chera magician friend and buried valaga-s blocked the vIra twice. But the vIra who had fought in many breath-taking battle grounds powdered them and marched ahead. He was now standing like one possessed with the ojas of 10 वृषभ-s surveying. Seeing this the virile तैत्तिरीयिन् felt like a lion hearing the distant thundering roar of a rival lion. He hence turned hostile. Even though equipped with the मन्त्रशास्त्र he behaved like one unacquainted with शिष्टाचर and bellowed a boast in the sabha deriding the third vIra and me, like a bull profusely signaling on perceiving rival bulls. With this he hurt his ties with his own जनाः.


