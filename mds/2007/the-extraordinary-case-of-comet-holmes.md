
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The extraordinary case of Comet Holmes](https://manasataramgini.wordpress.com/2007/10/28/the-extraordinary-case-of-comet-holmes/){rel="bookmark"} {#the-extraordinary-case-of-comet-holmes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 28, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/28/the-extraordinary-case-of-comet-holmes/ "5:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A dhuma-gola was sighted near the head of ययाति, when the कृष्ण-पक्ष moon was passing through कृत्तिका\
```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RyQrs7xjPLI/AAAAAAAAAP0/7TALpsnNN4Y/s320/comet_holmes.png){width="75%"}
```{=latex}
\end{center}
```



My friend the आघमर्षण brought to my attention an extraordinary event this evening: The comet Holmes was visible to naked eye. He mentioned that it was so bright that it changed the shape of Perseus. This comet has a long and colorful history since its discovery on November 6th of 1892 by Holmes using a 12.6 inch from London in the constellation of Andromeda near the famed Galaxy of M31. It was seen by several observers shortly thereafter and confirmed as a fuzzy naked eye object resembling M31. The striking thing was that after its orbit was calculated it proved to be way past it is perihelion, and it was a short period comet with an orbit within that of Jupiter. It returned as predicted in 1899 and 1906 but was extraordinarily faint, recorded around the 16-17th magnitude. The comet was then lost and never seen. Marsden in 1963 using computational numerical integration managed to calculate its current orbit and showed that since its last observation in 1906 and the upcoming apparition of 1964, the orbital period had increased from 6.86 years to 7.35 years. With this it was recovered again during perihelion at the magnitude of 18 --- nowhere in the range of amateurs like us with really small instruments.

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RyQrs7xjPMI/AAAAAAAAAP8/S1-8e78Wlt8/s320/comet_holmes_orbit.png){width="75%"}
```{=latex}
\end{center}
```



However, in 2007 October it gave a repeat performance, even more spectacularly of the outburst at the time of discovery. Since July when it was seen at mag 14.5 it was fading steadily reaching around the 17th mag, when it began an extraordinary outburst on Oct 24th and had reached the 7th mag. Within that day it climbed dramatically to reach 2.8. When I saw it from behind my क्षेत्र it was around 2.8. The skies are bad here, and the prathama waning moon was up in कृत्तिका. When I began the observations, the moon was grazing the tail of the Pleiades. Yet even in these bad skies the comet was visible as the 3rd brightest entity in Perseus after Alpha and Beta Persei. Then seeing it with my binoculars it looked like a sight I have never seen before- a bright circular fuzzball forming a triangle with lambda and delta Persei. Extraordinary was the only word I had for it. It was the first time I was seeing a Jovian comet. The causes for this outburst are unclear but is something that has happened more than once with this comet. Is it some combination of heat build and destabilizing by Jupiter that causes it to puff out dust?

कलशजा's puruSha called to say that he too had sighted it and was in loss of words to describe it --- he said he would send pictures he was taking of it.

Update 28,29/10/2007: With the moon further away the nucleus of the comet could be clearly distinguished through the binoculars.

Update 1/11/2007: Following the comet from 27/10/2007 one trend is very clear --- while the comet has remained at approximately the same magnitude, it has grown more diffuse by the day. On 27/10 it was a dense ball, on 1/11 it had grown in size by almost 65% but retained the same brightness.


