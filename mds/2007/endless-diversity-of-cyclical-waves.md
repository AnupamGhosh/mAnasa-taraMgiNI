
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Endless diversity of cyclical waves](https://manasataramgini.wordpress.com/2007/06/14/endless-diversity-of-cyclical-waves/){rel="bookmark"} {#endless-diversity-of-cyclical-waves .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 14, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/06/14/endless-diversity-of-cyclical-waves/ "5:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RnDXu05tW8I/AAAAAAAAAKs/5meAImT9HBM/s320/cyclia_panel.jpg){width="75%"}
```{=latex}
\end{center}
```



