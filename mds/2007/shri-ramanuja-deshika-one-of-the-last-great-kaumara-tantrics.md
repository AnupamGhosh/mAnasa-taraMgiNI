
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [श्री रामानुज deshika, one of the last great कौमार tantrics](https://manasataramgini.wordpress.com/2007/01/14/shri-ramanuja-deshika-one-of-the-last-great-kaumara-tantrics/){rel="bookmark"} {#शर-रमनज-deshika-one-of-the-last-great-कमर-tantrics .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 14, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/14/shri-ramanuja-deshika-one-of-the-last-great-kaumara-tantrics/ "5:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The below note is neither meant as a sectarian effusion nor a historical account. It is a hagiography and just that.\
[सर्व-भूतामकं मन्मनोबोधकं सत्-कृपासागरं देवसेनापते पाहि मां पाहि मां पाहि मां देव तुभ्यं नमो देव तुभ्यं नमो देव तुभ्यं नमः ॥]{style="color:rgb(153,204,0);"}

The श्री-vaiShNava आचार्य, sudarshana सूरी, was proceeding to श्रीपेरंपुत्तुर् after visiting the old temple of विष्णु at वेन्कटगिरि (not Tirupati) and consecrating a shalagraman there. On his way back on his horse cart he had to pass by the shrine housing the great वायु लिङ्ग of the terrible rudra, कालहस्ति. rudra's presence was felt here in the form in which he had destroyed गजासुर. The zealous vaiShNava who did not recognize the greatness of The god, asked his henchmen to draw the curtains in his cart and he closed his eyes so that even by error he might not cast his eyes on the shrine of rudra. When he was a safe from कालहस्ति distance he opened his eyes but to his horror he had turned blind. Shocked he uttered the verse:\
[सर्वात्मता ते श्रुतिषु प्रसिद्धा सत्यैव सा कालकरीश नूनं ।]{style="color:rgb(153,204,0);"}\
[अपश्यतस्त्वा मधुना समस्तं यतो न मे दृष्टिपथं प्रयाति ॥]{style="color:rgb(153,204,0);"}

He was constantly tormented by भूत-s and agents of sharabha. He recalled the great vaiShNava tantric, the master of पाञ्चरात्र prayoga, rAma deshika and wanted to seek his aid. That night he had a dream that shri rAma deshika had already attained mokSha but he appeared in his dream and asked him to seek the aid of his illustrious student, the accomplished tantric shri रामानुज deshika. He met रामानुज deshika at Tiruttani who performed an appropriate rite to release sudarshana from the grip of rudra's wrath, even as विष्वक्सेन was brought back to life by काल bhairava, when he had drunk विष्णु's blood from the skull-bowl fashioned of brahmA's severed fifth head.

रामानुज deshika, was born in काञ्चिपुरं in family of devote vaiShNava ब्राह्मण-s of the ubhaya vedAnta stream who regularly sung the DP4000 and lit lamps at विष्णु shrines. रामानुज was a natural poet and mastered काव्य and अलंकार at an early age in addition to the study of the तैत्तिरीय school of the yajur veda. He then proceeded to study the mantra शास्त्र with the great tantric guru rAma deshika who had mastered the entire lore of पाञ्चरत्र mantras. Having initiated रामानुज into all of them he asked him to perform पुरश्चरण of the 104 mighty astra mantras of विष्णु, which leads one to highest पाञ्चरात्रिc siddhi. In order to do this he proceeded to a desolate spot near Tiruttani where he began his japa in seclusion. One day as he was wandering in the forests of the Nagari hills he saw mysterious वैखानस ब्राह्मण wearing a yellow dress. The ब्राह्मण saw him but did not speak anything but merely gestured to him the way कापालिक's do. That night he had a dream in which कुमार communicated to him the बगलामुखि विद्या-s. He did japa of those mantras and attained siddhi --- he had become the master of the साम्ख्यायन तन्त्रं. Then that same brahmin came by and asked him to accompany him to Tirupati. As they were ascending the slopes to have a view of the great नारायण, the brahmin gave him two mighty mantra-s, that of the goddess trailokya-विजया (along with its संपुतीकरण for the mantra of विलिस्तेन्गा) and विष्णु trailokyamohana. Then he asked him to proceed to Bellary after having seen the image of the deva on Tirumala.

As रामानुज was proceeding to Bellary a jaina mantravadin attacked him with the mantra of कुरुकुल्ला, but he retaliated with the trailokya-विजया spell and destroyed the jaina's entire mantra bala. रामानुज passed through to Bellary which was in a precarious condition due to the ravages of the green-robed, bearded, ruffianly Meccan demons. Here, after he had finished his mantra japa, he suddenly saw an apparition of the great war-god कुमार, with six heads wielding several terrifying weapons. He spontaneously composed two poems to कुमार in the दण्डक and भुजङ्ग meters. At that point he met the last कौमाराचार्य of the वङ्ग lineage (from what is now in the terrorist state of Bangladesh), कुमार-guru , who gave him all the कौमार mantras. He instantaneously attained the siddhi of all but two, which are the most secret -- the krodha-कुमार mantra and व्कृतग्रह mantra.

He was asked to go to नागार्जुनकोण्ड and perform their japa. As he was performing the पुरश्चरण he saw the dreadful skanda appearing with a single head and he uttered a 4-syllabled mantra. That mantra became वराह, who held in his hand a great sword dripping with the blood of हिरण्याक्ष, with an उपवीत of 3 intertwined snakes. Then from the snout of वराह emerged that awful emanation of कुमार, krodha-rAja कुमार, with 6 heads, who is irresistible. With these mantra-s he had attained the height of tantric perfection, he was called a deshika. He was the master of the कौमार lore, an impregnable mantra-वादिन्. After his marriage he was proceeding with his wife to कुमार parvata when he was attacked by the तुरुष्क-s on the way who tried to seize his wife. He deployed the krodha-rAja षण्मुख हृदय mantra and got ready to fight them with his small band. The महाशूल of कुमार entered his weapons and they killed 80 तुरुष्कस् sending the rest scampering over the country side. Having reached कुमार parvata he installed himself there for worshiping the deity.


