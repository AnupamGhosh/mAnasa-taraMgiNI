
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [mantra of त्रैलोक्यविजया](https://manasataramgini.wordpress.com/2007/01/16/mantra-of-trailokyavijaya/){rel="bookmark"} {#mantra-of-तरलकयवजय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 16, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/16/mantra-of-trailokyavijaya/ "7:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The mantra of the goddess त्रैलोक्यविजया is provided by the yuddha-जयार्णव तन्त्रं. A somewhat corrupt version of this tantra is incorporated into the agni पुराण. Stand-alone manuscripts of the YJA also exist, though they have not been edited or published. In addition to the version of the agni-पुराण I had access to a hand-written transcript on paper. The Dutch explorers have recovered a corrupt version of the same in their collection of Hindu mantras known as stuti-s and stava-s from Indonesia (now only surviving in Bali). All these with a single input from oral tradition help to obtain the critical version of the त्रैलोक्यविजया mantra as per the available traditions. त्रैलोक्यविजया is meditated upon as being deep blue in color, having 20 hands, and standing on corpses of slaughtered foes.

OM hUM क्षूं ह्रूं OM namo भगवती दंष्ट्रिणी भीमवक्त्रे महोग्ररूपे hili hili raktanetre kili kili महानिस्वने kulu OM vidyuj-jihve kulu OM nir-मांसे कट कट गोनसाभरणे chili chili shava-माला-धारिणी द्रावय OM महारौद्री सार्द्र-चर्मकृत्कृताम्बरे विजृम्भ OM नृत्य असिलताधारिणी भ्रुकुटी-कृतापाङ्गे विषम-netra-कृतानने vasA-medho-vilipta-गात्रे kaha 2 OM hasa 2 kruddha 2 OM नील-जीमूत-वर्णे megha-माला-धारिणी prajvala OM ग़्हण्टा-किङ्किङि विभूषित-शरीरे OM simhisthe अरुणवर्णे OM ह्राम् ह्रीं ह्रूं raudra-रूपे hUM ह्रीं क्लीं OM ह्रीं hUM OM आकर्ष OM धून 2 oM he haH खः वज्रिणि hUM क्षूं क्षां krodha-रूपिणी prajjvala 2 oM bhIma-भीषणे bhindi OM महाकाये Chindi oM करालिनी किटि 2 mahA-भूत-मातः sarva-दुष्ट-निवारिणी jaye OM vijaye OM trailokyavijaye hUM फट् स्वाहा


