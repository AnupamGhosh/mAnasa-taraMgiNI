
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The song and memory](https://manasataramgini.wordpress.com/2007/11/28/the-song-and-memory/){rel="bookmark"} {#the-song-and-memory .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 28, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/11/28/the-song-and-memory/ "5:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R is a great believer of the view that for every mood there is a song that matches and creates a resonance. Hence, R collects music by this criterion and may play the same song over and over again when in a certain mood, but will be simply unable to listen to it when not in that mood --- it is like enzyme specificity-- you need the right enzyme for the right reaction and enzymes can be exquisitely specific ! Mn was similarly inclined, believing in the match between the mood and the song. While I have little to no knowledge of any form of तौरीय गाण, it is clear that R's collection spans an great diversity of तौरीय गण. I was mentioning to R how I was dreading the planned rendezvous with ST and ekanetra, because the weather was threatening to be terrible. Given their disposition ST and ekanetra have planned a stroll in the evening in all the freeze and hima. R asked me to listen a song --- its effect was felt by me despite knowing nothing of its context. It moved me back to a different time --- I was lost in the old memories from a certain point almost 13 years ago. A particular vision came to mind:\
The archer was alone in the vast grassy plain with a young vigorous horse beside him. There was a target before him he was repeatedly shooting arrows at it like my illustrious ancestor the ऋषि jamadagni. He was disturbed by all manner of insects and birds, but he kept shooting, perfecting the strike. Sometimes shooting from the ground, sometimes shooting from his horse while in charge. He looked up and saw a golden eagle in its flight.\
This was an "internal" vision.\
There was an "external" one too:\
It was 9.00 PM or so. I was a man of boundless energy. We readied to light a bonfire. For some reason "ऋद्ध्यास्म havyair... came to mind". Around a bonfire ekanetra, वङ्ग paNDita, vaishya ज्योतिष, maithila, काcअ-netra, Igul the महिषि of the 3rd वर्ण, कलशजा, ST and me stood. It was a little off the path leading to the pinnacle of the fort of Rajmachi. ekanetra, वङ्ग paNDita and me were sequestered together, away from the rest of the folks. We were laying out an outline for a world history ancient and more recent. An ulka flashed in the firmament and we even heard a noise. Smiling to each other, we continued our discourse on the theories of history. The rest of the folks were creating a racket around the fire, playing precisely that song on the cassette box till the battery died. ST then kindly beckoned us to eat dinner --- as ever a dinner by her was a memorable event. While biting into a dainty edible, I thought of Khan Qabul.


