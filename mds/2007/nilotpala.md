
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नीलोत्पल](https://manasataramgini.wordpress.com/2007/01/01/nilotpala/){rel="bookmark"} {#नलतपल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/01/nilotpala/ "7:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RZmiQwvxKBI/AAAAAAAAAA8/taIqSr8SVvU/s320/nilotpala_lata3.png){width="75%"}
```{=latex}
\end{center}
```




