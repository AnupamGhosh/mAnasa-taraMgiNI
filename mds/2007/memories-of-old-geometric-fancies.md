
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Memories of old geometric fancies](https://manasataramgini.wordpress.com/2007/07/05/memories-of-old-geometric-fancies/){rel="bookmark"} {#memories-of-old-geometric-fancies .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 5, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/07/05/memories-of-old-geometric-fancies/ "4:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RoxRIe2Ko6I/AAAAAAAAALk/fjfcUfQPpX4/s320/superellipse.png){width="75%"}
```{=latex}
\end{center}
```

. It brought us much luck. We then toiled on in quest of the perimeter of the ellipse and got a glimpse of the elliptic integral. On the way we stumbled on the Weierstrass elliptic function and its fascinating double periodicity in both the real and the imaginary plane, while the trigonometric functions are mono-periodic only in the real plane.

But what really fascinated us was our rediscovery of the family of curves termed the superellipses (we did not learn until much latter that they were called Lame's curves; Lame of Lame's theorem fame of which we had unpleasant memories). These are defined in cartesian space by the equation:\
[[]{style="font-style:italic;font-weight:bold;"}[abs(x/a)]{style="font-style:italic;font-weight:bold;"}[]]{style="font-style:italic;font-weight:bold;"}[\^n+]{style="font-style:italic;font-weight:bold;"}[[]{style="font-style:italic;font-weight:bold;"}[abs(y/b)]{style="font-style:italic;font-weight:bold;"}[]]{style="font-style:italic;font-weight:bold;"}[\^n=1 ; n>0\
]{style="font-style:italic;font-weight:bold;"}n=0 yields a cross; n=1 yields a rhombus; n=2 an ellipse/circle; n=4 & a=b yields a squircle or hypercircle; n=3/2 yields a rhombic hypoellipse; n-\>infinity is a square/rectangle.\
They can be parametrically described as:\
x(t) = 3\*sgn(cos(t))*(abs(cos(t)))**(2/n)\
y(t) = 3\*sgn(sin(t))*(abs(sin(t)))**(2/n)

This form can be used to plot them easily.

There is something aesthetically fascinating about these curves that we decided to plot them again. A striking aspect we noticed was that they describe all commonly used closed curves with n ranging from 0 to a large number.\
[]{style="font-style:italic;font-weight:bold;"}


