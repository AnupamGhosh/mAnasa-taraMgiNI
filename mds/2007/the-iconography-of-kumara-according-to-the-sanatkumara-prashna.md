
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The iconography of कुमार according to the सनत्कुमार-prashna](https://manasataramgini.wordpress.com/2007/08/18/the-iconography-of-kumara-according-to-the-sanatkumara-prashna/){rel="bookmark"} {#the-iconography-of-कमर-according-to-the-सनतकमर-prashna .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 18, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/18/the-iconography-of-kumara-according-to-the-sanatkumara-prashna/ "4:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier discussed विद्या-s of the various specialized मूर्ति-s of [कार्त्तिकेय](https://manasataramgini.wordpress.com/2006/10/10/kumara-vidya-s/ "कुमार विद्या-s") and their visualizations. Continuing with that discussion we shall now look at the मूर्ति-s of the god as described by the कौमारागम known as the सनत्कुमार prashna. The prashna series of कौमारगम-s are comparable to the core saMhitA-s of the वैखानस-s with respect to providing the main scaffold for the temple rituals of skanda. The major ones known to the कौमार ritual experts are the agastya prashna, the kaushika prashna and सनत्कुमार prashna. Of these सनत्कुमार prashna is preserved in reasonably good manuscripts. The sharavanabhava पटल of the सनत्कुमार prashna outlines 16 icons of कुमार:


 1.  ज्ञान shakti\
black-colored garments; a garland of white arka flowers; golden jeweled crown; 4 hands with shakti, vajra, cock, skull-topped rod, white यज्ञोपवीत.


 2.  skanda\
cloud-colored garment, wearing gem-set ornaments and a flower garland, seated in padmAsana, hair in the form of a शिखा; smoky complexion; 3 eyes and 4 hands holding cock and vajra in right and left, the other two hands respectively showing abhaya and varada.


 3.  अग्निजात\
Of white and black complexion, with 2 heads and 8 hands holding sruva, अक्षमाल, sword, svastika mudra (in right hands) and cock, shield, vajra, ghee bowl (in left hands); performing vedic rites.


 4.  saubheya\
red lotus complexion; 4 heads, 8 eyes, 8 arms and seated on a lotus; left leg bent and placed on right leg. Holds shakti, hibiscus, flower arrows, abhaya mudra (right hands) and vajra, sugar-cane bow, and varada mudras (left hands).


 5.  गाङ्गेय\
scarlet complexion like fresh pomegranate flower; wearing a करण्ड मकुट (a turban with net-like weaving), gem-set ear-rings and a blue lotus garland; mounted on a gharial; One head with 3 eyes and 4 hands holding an axe and water pot (right) and cock and sacrificial fire drill (in left hands)


 6.  sharavanabhava\
complexion of rising sun; wearing golden hued clothes; mounted on a lion; one head with curly locks and 3 eyes and 6 hands holding flower arrows and sugar-cane bow, sword and shield, vajra and cock banner in the respective pairs of right and left hands.


 7.  कार्तिकेय\
complexion of the rising sun, garment of variegated colors, riding on a peacock, one head with a wreath of bilva leaves, 3 eyes with 10 hands holding shakti, trident, arrow, अङ्कुष and abhaya mudra (right), a tomara missile with launcher, lasso, conch vajra and vIra mudra (left)


 8.  कुमार\
two lotus flowers adoring the ears; wearing a crown; 1 head, 4 hands holding shakti and vajra (right) and abhaya and vara (left); accompanied by वरुण and वायु.


 9.  षण्मुख\
Attired with several jewels and all garments of yellow silk; dazzling ear-studs; 6 heads and 12 hands; riding a peacock; holding shakti, vajra, sword, chakra, lasso, abhaya mudras, cock, bow, shield, and a cudgel. Embracing two wives गजायी (जया) and गजवल्ली (विजया) on left and right.

गजायी: complexion of rising sun, right hand holding a lily, wearing a garland of मन्दार flowers.\
गजवल्ली: black in color; left hand holding a lotus, crown; wearing a jeweled bra on her firm breasts; blazing ear studs.


 10.  तारकारि\
riding a bull; wearing a braided जट, holding trident, hatchet, sword, abhaya mudra (right hands) and a lasso, a पैषाच (?), shield and varada mudra (left).


 11.  सेनानी\
Of black complexion; with a shikha; one head with 3 eyes; having a merciful face; smeared with sweet-smelling unguents; holding a conch and chakra and displaying abhaya and varada mudras.


 12.  guha\
White garments; merciful face; 3 eyes; golden gem-set crown; 4 arms with trident, vajra, abhaya and varada mudras. Accompanied on the left side by his wife जयायी.


 13.  brahmachAri\
crimson complexion; 3 eyes and two hands with cudgel in the right and vajra in the left. Standing with legs held slightly bent on a gem-set lotus foot rest. Wearing the marks of a brahmachAri like shikha, यज्ञोपवीत, मौञ्जि girdle and कौपीन.


 14.  shivadeha\
1 head wearing करण्ड मकुट; 6 hands holding a shakti, vajra, flag जपामाल abhaya and varada mudras; mounted on a peacock.


 15.  क्रौञ्चद्धारि (?)\
Seated on a lion-throne with a peacock वाहन; 3 eyes and 4 hands holding a flag, cudgel, abhaya and varada mudras. Beside him are seated his two wives जयायी and विजया on left and right respectively. He is shown demolishing the क्रौञ्च mountain.


 16.  शिखिवाहन\
With complexion of a pink lotus wearing deep blue (indra-नील) garments; 1 head with 3 eyes and with 4 arms bearing lasso, अङ्कुश, abhaya and varada mudras; having hair in a top knot that appears blazing like fire, with peacock वाहन.

In one type of कुमार temple the icon in the central shrine is recommended to be ज्ञान shakti, in the eastern sub-shrine is skanda; south -- saubheya; south-west- गाङ्गेय; west- a sub-shrine with two icons of sharavanabhava and षण्मुख; in the north-west कार्तिकेय, कुमार and तारकारि; in the northern sub-shrine- सेनानी, guha and shivadeha; In the north east- क्रौञ्चद्धारि, शिखिवाहन and brahmachAri.

Text also mentions the following division of offerings made at a कुमारालय: ब्राह्मण-s: recitation of vedic formulae and hymns; kShatriya-s: offer fowl; vaishya-s offer camphor and rock-sugar; शूद्र-s offer fruits and flowers during the कृत्त्का festival.


