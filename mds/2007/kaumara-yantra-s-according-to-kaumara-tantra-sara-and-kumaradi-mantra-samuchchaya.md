
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कौमार yantra-s according to kaumara tantra sAra and कुमरादि mantra samuchchaya](https://manasataramgini.wordpress.com/2007/08/19/kaumara-yantra-s-according-to-kaumara-tantra-sara-and-kumaradi-mantra-samuchchaya/){rel="bookmark"} {#कमर-yantra-s-according-to-kaumara-tantra-sara-and-कमरद-mantra-samuchchaya .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 19, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/08/19/kaumara-yantra-s-according-to-kaumara-tantra-sara-and-kumaradi-mantra-samuchchaya/ "10:38 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_ZhvcTTaaD_4/RsjHg_XcXfI/AAAAAAAAANM/Ybrs9He4CdI/s320/kaumara_magic_square.jpg){width="75%"}
```{=latex}
\end{center}
```

\
कौमार जाल yantra

OM वं shaM raM वं NaM भं स्कन्दाय khe khe नमः |\
OM वं वं shaM raM NaM भं स्कन्दाय khe khe नमः |\
OM raM वं NaM भं वं shaM स्कन्दाय khe khe नमः |\
OM NaM वं भं वं shaM raM स्कन्दाय khe khe नमः |\
OM NaM वं भं shaM raM वं स्कन्दाय khe khe नमः |\
OM भं वं shaM raM NaM वं स्कन्दाय khe khe नमः ||OM श्रीं क्लीं ह्रीं aiM IM naM laM taM sauM शरवणभव ||
```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/RsjH4fXcXgI/AAAAAAAAANU/GVO1EveItz0/s320/shaktyuta_मूर्ति_yantra.jpg){width="75%"}
```{=latex}
\end{center}
```

\
shaktyuta मूर्ति yantra

OM सों ईशान-मूर्ध्ने jagad-bhuve नमः |\
OM sIM tat-puruSha-वक्त्राय vachad-bhuve नमः |\
OM sUM aghora-हृदयाय vishva-bhuve नमः |\
OM saIM वामदेव-गुह्याय rudra-bhuve नमः |\
OM sauM सद्योजात-मूर्तये brahma-bhuve नमः |\
OM saH अस्त्राय sarvato-bhadra-वक्त्राय bhuvodbhuve नमः |


