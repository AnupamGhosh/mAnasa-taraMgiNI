
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [rudra विभूति stotra](https://manasataramgini.wordpress.com/2007/03/01/rudra-vibhuti-stotra/){rel="bookmark"} {#rudra-वभत-stotra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/01/rudra-vibhuti-stotra/ "6:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

नमः शिवाय रुद्राय कद्रुद्राय prachetase |\
मीढुष्टमाय सर्वाय शिपिविष्टाय-रंहसे ||\
namaste कालकालाय namaste rudra manyave |\
नमः शिवाय रुद्राय शङ्कराय शिवाय te ||\
ugrosi sarva-भूतानां नियन्तासि shivosi नः |\
नमः शिवाय शर्वाय शङ्करायार्तिहरिणे ||

ग्रहाधिपत्ये भगवान्-अभ्यषिञ्चद्-दिवाकरम् |\
वृक्षाणाम् ओषधीनां cha सोमं brahmA प्रजापतिः ||\
अपां cha वरुणं देवं धनानां यक्ष-पुङ्गवं |\
आदित्यानां तथा विष्णुं वसूनां पावकं तथा ||\
प्रजापतीनां दक्षं cha मरुतां shakram-eva cha |\
दैत्यानां दानवानां cha प्रह्लादं daitya-पुङ्गवम् ||\
धर्मं पितॄणाम्-अधिपं निरृतिं पिशितासिनाम् |\
रुद्रं पशूनां भूतानां नन्दीनां गणनायकम् ||\
वीराणां विरभद्रं cha पिशाचानां भयङ्करम् |\
मातॄणां chaiva चामुण्डां सर्वदेवनमस्कृताम् ||\
रुद्राणां deva-देवेशं नीललोहितम्-Ishvaram |\
विघ्नानां व्योमजं देवं गजास्यं tu विनायकम् ||\
स्त्रीणां देवीम् उमादेवीं वचसां cha सरस्वतीम् |\
विष्णुं मायाविनां chaiva स्वत्मानां जगतां तथा ||\
हिमवन्तं गिरीणां tu नदीनां chaiva जाह्नवीम् |\
[समुद्राणां च सर्वेषाम्-अधिपं पयसां निधिम् ॥]{style="color:rgb(255, 0, 0);"}\
[वृक्षाणां चैव चाश्वत्थं प्लक्षं च प्रपितामहः ॥]{style="color:rgb(255, 0, 0);"}\
gandharva-विद्याधर-किन्नराणाम्-ईशं पुनाश्-चित्ररथं चकार |\
नागाधिपं वासुकिम्-ugra-वीर्यं सर्पाधिपं तक्षकम्-ugra-वीर्यम् ||\
दिग्वारणानाम्-अधिपं चकार gajendram-ऐरावतम्-ugra-वीर्यम् |\
सुपर्णम्-ईशं पतताम्-अथाश्वराजानाम्-उच्छैःश्रवसं चकार ||\
सिंहं मृगाणां वृषभं गावां cha मृगाधिपानां शरभं चकार |\
सेनाधिपानां guham-अप्रमेयं श्रुती-स्मृतीनां लकुलीशम्-Isham ||\
अभ्यषिञ्चत्-सुधर्माणं तथा शङ्खापदं दिशाम् |\
केतुमन्तं क्रमेणैव hema-रोमाणम्-eva cha ||\
पृथिव्यां पृथुम्-ईशानं सर्वेषां tu maheshvaram |\
चतुर्मूर्तिषु सर्वज्ञं शङ्करं वृषभ-dhvajam ||

  - --\
The current version seems to have a corruption in area indicted in red. None of the printed or manuscript copies that I have accessed allow correction of this.

The विभूति स्त्रोत्रं begins with the mantras known as पञ्चाक्षरी vibhavas. These are the mantra-s typical of the लाकुलीश-पाशुपत school and differ in structure from those typical of the mantra-मार्ग school ("classical tantric mantras"). Then it enters a section of the leaders of various categories that are appointed by rudra. It finally concludes by stating that even as the leaders of each of these categories have been narrated so is maheshvara the lord of the universe. The chapter 1.158 of the लिङ्ग पुराण states that shiva anointed the leaders of these categories and gives the above list. These categories and their leaders are closely related to the विभूतिस् of विष्णु narrated by कृष्ण देवकीपुत्र in the bhagavad गीता. This suggests that there was such "विभूति" list prevalent amongst the hindus.


