
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Raised by bAlA and दत्तात्रेय-I](https://manasataramgini.wordpress.com/2007/01/27/raised-by-bala-and-dattatreya-i/){rel="bookmark"} {#raised-by-bala-and-दतततरय-i .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 27, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/01/27/raised-by-bala-and-dattatreya-i/ "6:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the hot plain to the north of the Wardha river, in the village of Dhamangaon, a band of Moslems led by their Kazi were chasing a young Hindu maid. Nothing unusual for the times, it was one more of the Moslem women-skimming raids. She ran into a shop of the local tailor, but he terrified by the charging Moslems with their upraised scimitars started running as fast as he could towards the house of the local headman Mankoji. All of a sudden the woman vanished and the Moslems, were unable to find her. Mankoji, who had just emerged from his daily worship of दत्तात्रेय saw the tailor panting and telling him of the incident that had just occurred. He asked the tailor to calmly return and call the women's clansmen to take her back. He did so and to his surprise he saw the Moslem ruffians lying beaten up with limbs broken staggering back on their horses and beating a hasty retreat. The Kazi was furious and sought the help of the commandant of the Yeotmal garrison. A band of 120 Mohammedan horsemen came charging down on the hamlet of Dhamangaon raising clouds of dust beneath the thundering hoofs of their horses. The Hindus started fleeing in terror. The head man Mankoji asked them to take cover behind the fortifications and he stood at the gate that lead into the fortifications mounting his horse and taking up his spear, sword and shield. He saw Moslem band charging fierce down the alley leading the gate. He stood on their path and he felt दत्तात्रेय appear before him as he did to lead the hordes of कार्तवीर्यार्जुन to war. He felt the trident of दत्तात्रेय enter his spear, the sword of विष्णु enter his own and on his forehead he felt the presence of the one who had single handed slaughtered the 30 sons of भण्ड even as शचिवेशी and दण्डनाथा had looked on. He fell upon the तुरुष्कस्, who had not even noticed him. So great was his fury that day that the 120 तुरुष्क-s attained यमालय at the edge of his sword before a muhurta was over.


