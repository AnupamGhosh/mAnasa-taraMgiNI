
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Chatting with ST and more averaging experiments](https://manasataramgini.wordpress.com/2007/02/14/chatting-with-st-and-more-averaging-experiments/){rel="bookmark"} {#chatting-with-st-and-more-averaging-experiments .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 14, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/02/14/chatting-with-st-and-more-averaging-experiments/ "2:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_ZhvcTTaaD_4/RdJ7Re8rbsI/AAAAAAAAADc/3lsuU-cDX4A/s320/madhyA_bindu_clean.jpg){width="75%"}
```{=latex}
\end{center}
```



We were hemmed in by the terrible fury of वात and rudra which gave us an excuse to squander time as we used to do in our days of youth with ST who was similarly holed up in our regions for two days. ST had been much enamored by our earlier averaging experiments to generate the average [Hindu स्त्री](https://manasataramgini.wordpress.com/2006/03/26/madhyamika-the-average-bharatiya-nari/) amongst our contemporaries using SQIRLZ-MORPH. She had built a larger parallel set for the purpose that included herself and few of our common acquaintances, in addition to a large body of her acquaintances (totally n=30). There were about 8 people overlapping with the earlier set. All the 4 वर्णस् of the Hindu society are represented here, though there appears to be a degree of bias in numbers towards the first वर्ण in the set. We did not resort to an average per वर्ण because the true kShatriya-s were particularly under-represented. But one point we noted was that the urban शूद्र-s in our data were on average closer to the 3 dvija-s वर्ण-s than a composite of purely अवर्ण and tribal representatives we had earlier made (data not shown). This seems to fit the genetic observations that the major division in Indian population separates tribal groups from the caste groups. Both ST and I agreed that a large number of Indian women of the older generation compared to ours suffered from facial dysmorphia. This was so, even amongst brahmin women, especially from the Dravida country. We also seemed to agree that the persistence of dysmorphia in our generation amongst brahmin women occurred at higher frequency in those from the Dravida country. We speculated that this might have to do with the protein poor-diets of people from this region. But this alone I feel is not enough to account for the dysmorphia in all Dravida brahmin women. On this point ST and I seem to disagree. My own feeling is that beauty in Hindu women is distributed normally but with a much greater standard deviation than in the case of the म्लेछ्च स्त्री-s. Thus, in the beauty tail (as opposed to the ugly tail) of the curve the best-looking स्त्री-s are amongst the Hindus, but in the mean area they might not fare as well. ST agreed but she and me differed in our theories to explain it. In course of our long chat spanning from lunch to dinner and beyond \[similar to the one we had on our walk in the pleasant town of my old residence in the peaceful days of 2003] we covered many topics. To both our memories almost convergently came the day when we climbed to the कौण्डिन्य caves and rested at ancient pillared temple on the way-- for a while we were transported to that condition, which only exists in the consciousness and breaks up if it comes out in words! But then like a dropped stone breaking the surface of a pellucid pond ripples burst through the the pond of the chit breaking up all images. Then I took advantage of ST's knowledge of the Dravida language to clarify certain points, the interest in which had been piqued by R's earlier questioning. Then we parted as the bleak exteriors threatened to eat us up. ST quoted some sentence from Conan Doyle-- I too recalled reading it but some how failed to register it.

To my mind came the following: "दण्डिन् of yore had made a 10 कुमार-s for his charita. Even so the 3 कुमारी-s had arisen. They stood on the three sides of the prism, and he stood in the middle. All the senses were inundated but none ever satisfied."

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_ZhvcTTaaD_4/ReTTMLullTI/AAAAAAAAAEM/Lu3ZL1oUPRM/s320/new_madhya3.jpg){width="75%"}
```{=latex}
\end{center}
```

\
Only ब्राह्मण with correction for aspect ratio


