
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Examining the Chandodarshana](https://manasataramgini.wordpress.com/2007/10/04/examining-the-chandodarshana/){rel="bookmark"} {#examining-the-chandodarshana .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 4, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/04/examining-the-chandodarshana/ "4:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier alluded to the Neo-vedic composition attributed दैवरात गजानन sharma aka दैवरात, a student of the great Sanskrit poet काव्यकण्ठ गणपति शास्त्री. A student of Sanskrit poetry will be definitely benefited by reading his masterly work on इन्द्राणी or उमा देवी, the उमा-सहस्रं, a 1000 verse composition in which each 25 verses are in different Chandas. Some of them bring out the wonders of dhvani in the classical meters. While I had heard of this neo-Vedic composition for long it took a while before I finally laid my hands on the Chandodarshana in nAgarI script along with its anvaya भाष्य to examine it.

श्री गणपति curiously comments that at the time of the production of the Chandodarshana the young दैवरात was not particularly well-versed in संस्कृत composition. He says these mantra-s came out of his mouth while he was meditating near the रेणुकांबा temple and he [गणपति muni] collected and recorded the full and clear ones. To me the whole piece seems to be गणपति muni's work with दैवरात apparently a mere instrument. He gives this away by cryptically alluding to his ancestor in the opening phrase "वसिष्ठो गिरः" Now the big question is it really a simulation of vedic? Below are the conclusions of my analysis:

  - स्वार rules: largely follow ऋग् vedic pattern, with कंप svarita-s and basic उदात्त accents generally similar to their vedic counterparts.

  - mythology and history: Very low mythological content compared to the real veda- वृत्र संग्राम, vala, namuchi etc receive little attention. Vedic heroes like दिवोदास athitigva, trasadasyu, सुदास etc receive no mention.

  - ritual: Limited references to ritual in general. References to the soma ritual are very few in comparison to the actual vedic corpus. The shrauta fire ritual too is minimally alluded to. Instead a ritual of drinking and offering पञ्च gavya is presented. Ritual words : e.g. juhomi, हवामहे, यजामहे are minimally used.

  - देवता-s: marut-s, अश्विना and मित्रावरुण have a relatively limited presence compared to the real veda. The देवता dvandva-s are also conspicuous by their absence or rarity. There is an enormous emphasis on sarasvati, way beyond what is seen in the RV -- in fact she is the most hymned deity in this collection. विष्णु is identified with the puruSha, something which is absent in the RV or the earlier vedic texts. सरस्वती is mainly hymned in her aspect as वाक् rather than as the deity of the water cycle and rivers, which is the dominant theme in the RV. rudra's wife is named as गौरी rather than पृष्णि, which is the name in the RV. ब्रह्मणस्पति is prominently connected with sarasvati- something which is typical of later hindu thought rather than in the RV.

  - influence of the nirukta: The nirukta scheme of ordering deities, and the terms and synonyms used by the nirukta play a dominant role in the refrains and organization of the hymns.

  - language: While there are archaisms that resemble the vedic language rather than classical संस्कृत, there are many features distinguishing it from the real RV language e.g. : use of classical dvandva i.e. ashvinau instead of RV अश्विना; use of the term puruSha throughout the corpus in the sense of person. puruSha is a late RV word coming to fore only in the मण्डल 10 in the puruSha hymn; use of the particle IM (usually meaning "now" or "indeed" -- like in ya IM शृणोत्युक्तम्) in an excess and implying its tAntric sense as a bIja of the देवी. In essence, different layers of RV dialects are homogenized. Use of व्याहृति-s in RV-styled verse is again not typical of the original. व्याहृतिस् are typical of yajushes.

  - philosophy: several upaniShadic elements seem to dominate. e.g.: prANa-s being compared to marut-s, Atman, puruSha etc.

So in short Chandodarshana is a modern author's emulation of the ऋग्, conditioned by the subsequent interpretive tradition and developments. Nevertheless, it is one of those rare modern examples of vedic-styled poetry that bears many features of the original. It does illustrate गणपति muni's tremendous poetic abilities both in the classical and vedic realm --- he would have truly been a ऋषि had he lived in the vedic period.


