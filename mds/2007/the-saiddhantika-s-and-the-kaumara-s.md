
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The सैद्धान्तिक-s and the kaumara-s](https://manasataramgini.wordpress.com/2007/09/24/the-saiddhantika-s-and-the-kaumara-s/){rel="bookmark"} {#the-सदधनतक-s-and-the-kaumara-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 24, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/09/24/the-saiddhantika-s-and-the-kaumara-s/ "1:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I was examining a कौमार manual used by priests from Kilmangalam (a village near the town of Periyakulam) for the worship at the अरुळ्मिगु बालासुब्रह्मण्य temple. The temple is itself was built around 1020 CE by one of the greatest Indian emperors, choLa rAjendra-I. The temple has an apocryphal legend associated with it: rAjendra was hunting boars and at this spot killed a boar sow which was feeding its piglets. He had a vision of कुमार feeding the orphaned piglets and keeping them alive. Struck by the vision rAjendra built a temple to honor कुमार. The कौमार manual used by the Kilmangalam priests to conduct rituals at this temple is from the 1500s of CE composed by a certain हालास्य. In its opening it has a salutation to great सैद्धान्तिक tantriks of yore. It names दुर्वास as the founder of the lineage and then lists the following historical tantriks:

 1.  sadyojyotis son of खेटक 2) बृहस्पति 3) नारायणकण्ठ 4) his son रामकण्ठ-II 5) aghora-shiva deshika.

The South Indian Urdhvasrotas (सैद्धान्तिक ) commentarial tradition remembers many great prior Acharya-s and siddha-s, but like its Kashmirian counterpart its memory starts with sadyojyotis and बृहस्पति, both of whom are termed siddha-s. After this it shifts to the great Kashmirian lineage which opens with भट्ट रामकण्ठ-I:\
रामकण्ठ-I -> विद्याकण्ठ-I -> नारायणकण्ठ ->रामकण्ठ-II -> विद्याकण्ठ-II and श्रिकण्ठ (probably).

Then it remembers the Tantrik-s of the मत्तमयूर lineage and its great patron, the Rajput king bhoja, from northern and central India (outside of Kashmir): brahmashambhu, mahArAja bhojadeva परमार, somashambhu and इशान-shiva.

In south India itself it remembers the great Tantriks-s of the greater Tamil country like:\
aghora-shiva, his student vaktra-shambhu, ज्ञान-shiva and trilochana-shiva associated with Chidambaram and Mysore.

An even earlier lineage of सैद्धान्तिक Tantrik teachers in the south is recorded in the inscriptions of the चालुक्य king विक्रमादित्य-I of बादामी. The rAja himself was initiated into the Urdhvasrotas mantras by सुदर्शनाचार्य and his school included the shaiva ब्राह्मण scholars: rudra-shiva, gAyatrI-shiva and parama-shiva. Similarly, the pallava king narasimhavarman-II also mentions attaining दीक्ष into the सैद्धान्तिक mantras in an inscription on the राजसिम्हेश्वर temple in Kanchi (\~700 CE), but we know little of his lineage beyond this. No scholarly works on the tantras from any these early South Indian lineages are currently known.

There is no evidence that the earlier सैद्धान्तिक-s from the pallava and चालुक्य courts contributed directly to the medieval siddhAnta tradition. The illustrious South Indian Acharya aghora-shiva deshika of Chidambaram carried out a major synthesis with the सैद्धान्तिक tradition by unifying the ritual prescriptions of mahArAja bhoja and somashambhu, with the Kashmirian commentaries of भट्ट नारायणकण्ठ and भट्ट रामकण्ठ on the core tantra-s of the Urdhvasrotas. It is fairly reliably known that aghorashiva was active around 1150 CE and he expresses tremendous veneration for his Kashmirian predecessors. This suggests that there was a new influx of the siddhAnta traditions from Kashmir and Malava (e.g. king bhoja's work) to the Tamil country prior to aghora-shiva. After he wrote his synthetic paddhati this became the standard tradition in the Tamil country with the Kashmirian Acharya-s providing theoretical base. One notable late change was the subtle transformation of the saiddhanta tradition with vedantic elements by the स्मार्त appayya दीक्षित.

In the Kashmirian tradition भट्ट विद्याकण्ठ-II was a contemporary of abhinavagupta, while रामकण्ठ-I was possibly closer to utpaladeva or just before him. sadyojyotis and बृहस्पति might have also been from Kashmir but we have little information about their lives. sadyojyotis was definitely pre-शञ्कर. In his polemics against advaitin-s he clearly attacks the pre-शञ्कर advaita like that of भर्तृ-प्रपञ्च who expounds परिणति-vedAnta in his बृहदारण्यकोपनिषद् commentary. One can imagine how शञ्कर having transformed vedAnta into मायावाद must have retaliated against these shaiva-s and demolished them in the debates that are supposed to have occurred in Kashmir and other places. The fightback against the मायावाद form of advaita is seen only later amongst the सैद्धान्तिक-s in the works of रामकण्ठ-II.

  - The main works of sadyojyotis that survive are : 1) the triad of works comprising the commentary on the raurava-sutra-संग्रह, namely the bhoga-कारिका, mokSha-कारिका and परमोक्षनिरास-कारिका: collectively the raurava-वृत्ति. 2) The commentary on the स्वायम्भुव-sUtra संग्रह named स्वायम्भुव-वृत्ति. 3) Two works on the nature of existence according to these two sUtra collections: tattva-संग्रह and the tattva-traya-निर्णय. 4) nareshvara-परीक्षा- a polemical work laying the siddhAnta shaiva position in contradistinction to the आस्तीक darshana-s, earlier पाशुपत shaiva-s and पाञ्चरात्रिc vaiShNava.

  - The siddhAnta Tantrik बृहस्पति-s works are poorly known and are :1) the raurava वार्तिक on the raurava-sutra संग्रह and 2) the shiva-tanu.

  - भट्ट नारायण-कण्ठ wrote the once famous sharan-निशा that is supposed to have clarified the tattva संग्रह of sadyojyotis like the moon shining on an autumn night. This work is remembered in both Kashmirian and Tamil tradition but does not seem to survive. He also composed new commentaries on the मृगेन्द्र tantra.

  - His son रामकण्ठ-II not only commented on the works of sadyojyotis but also on the मतङ्गपारमेश्वर tantra.

  - aghora-shiva in Tamil Nad composed commentaries building precisely on the works of नारायण-कण्ठ and रामकण्ठ and appears to follow their conservative path. This is quite clear from his epitome of the siddhAnta worship presented in the 100 verse stotra known as the पञ्चावर्ण-stava. Similarly, his student vaktra-shambhu also from TN seems to build on नारायण-कण्ठ's work on the मृगेन्द्र.

रामकण्ठ-II makes an interesting statement in the beginning of his commentary on the मतङ्गपारमेश्वर that the siddhAnta tantras have been interpreted according to the theories of the nyAya atomists, vedantins and kaula Tantriks. रामकण्ठ-II then seeks to restore what he calls the correct सैद्धान्तिक interpretation as per the path originally laid by sadyojyotis. Even the non-siddhAnta trika-kaula tantrics like abhinavagupta and the श्रीविद्या tantric jayaratha cite these siddhAnta tantra-s and use them as authoritative works. They also try to accommodate बृहस्पति and sadyojyotis within their scholarly framework. This shows that the siddhAnta tantra (and other such Agamic scriptures) had an independent position of respect (even if not on par with the shruti). This was probably in no small part due the prominent role they played in the domain of स्थापन (=temple establishment), prayoga (=goal-oriented tantric rites) and yoga. In general other streams of आस्तीक thought and नास्तीकस् too in a subversive sense sought seem to have a priori accepted the praxis and mantra-शास्त्र of these tantras, though within their own established contexts. Thus, well before appayya दीक्षित there were multiple attempts to treat the siddhAnta tantra-s from the stand point of other आस्तीक theoretical frameworks, including non-siddhAnta tantric models, rather than the one preferred by the shaiva tantriks.

As we have seen above the works of vaktra-shambhu and aghora-shiva suggests that the medieval South Indian authorities returned to the conservative path of interpretation of the Kashmirian shaivas by adopting रामाकण्ठ and नारायणकण्ठ as the standard. But what about their association with the कौमार path? As we know the temple of अरुळ्मिगु बालासुब्रह्मण्य was in place before aghora-shiva and kaumara temples were present in TN even before that. The कौमार path in TN is a much earlier transmission from North India --- it is possible that it was associated with the early pre-aghorashiva सैद्धान्तिक-s like those associated with the earlier चालुक्य and pallava kings. However, we have no evidence for that. In fact the चालुक्यस्, pallava-s and kadamba-s who began as feudatories of the pallava-s were [kumara worshipers](https://manasataramgini.wordpress.com/2005/10/28/royal-kumara-worshippers/) even before their kings started acquiring shaiva दीक्ष. The कौमार path also has no notable role in the siddhAnta works of Kashmirian lineage or that of bhoja. Hence, it becomes likely that the siddhAnta shaivas actually invaded the domain of the preexisting and took over their activities especially in the domain of स्थापन and प्रासाद utsava vidhi. In terms of the mantra-शास्त्र we clearly notice this imprint in the कौमार tradition of TN --- the पञ्च brahma mantra (a very key aspect of सैद्धान्तिक worship) has been exapted for कौमार worship. The installation of tridents and spears (trishula/shakti स्थापनं) in कौमार temples also follows a सैद्धान्तिक model. However, much of the other rites relating to सुब्रह्मण्य याग, and कुमार मन्त्रोद्धार follow the actual कौमार tradition. This take over by सैद्धान्तिक-s is not surprising given that they already possessed [स्थापन tantra-s for parivara देवता-s including कुमार](https://manasataramgini.wordpress.com/2006/06/23/siddhanta-tantrics-and-the-mainstream-brahminical-path/). However, it should be noted that it is erroneous to believe that the कौमार was an offshoot of the सैद्धान्तिक stream.

The technically educated कौमार-s are a vanishing group: they seem to be extinct in the North in [Rajasthan](http://manollasa.blogspot.com/2007/05/paschimaugha-of-manavaugha-of-kaumaras.html) (where again there was an association with the shaiva stream), and are present in small clusters in Tamil Nad, and the Kanada and Tulu countries (where a pristine secretive सुब्रह्मण्य मठ lineage survives). Their texts have rarely been published if at all and most lie as unedited and in some cases lacunose manuscripts from these regions. We are one remnant of this path. We have too much of a personal connection with this deva, but we know not what the roguish deva has in store for us.


