
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [symmetric icon attractors](https://manasataramgini.wordpress.com/2007/04/10/symmetric-icon-attractors/){rel="bookmark"} {#symmetric-icon-attractors .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 10, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/10/symmetric-icon-attractors/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/Rhs1LOuJITI/AAAAAAAAAGE/sElju4NH2Z8/s320/icon_attractor.jpg){width="75%"}
```{=latex}
\end{center}
```



```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_ZhvcTTaaD_4/RhxE6-uJIUI/AAAAAAAAAGM/QzzL6V4ukco/s320/icon_attractors.jpg){width="75%"}
```{=latex}
\end{center}
```



For several years we have been fascinated by the symmetric icon attractors popularized by the physicist Sprott. The above attractors discovered by Golubitsky and Field gain their symmetry because of the step r=(x+i\*y)\^n, where n is an integer power. Thus they spread the xn+1 and yn+1 values over the imaginary plane resulting in symmetry decided by the power n. The z-value is not used in the calculation of x and y so it appears rotationally symmetric around the z axis.


