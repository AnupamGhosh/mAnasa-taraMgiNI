
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Philosophizing on magnetic sight](https://manasataramgini.wordpress.com/2007/10/01/philosophizing-on-magnetic-sight/){rel="bookmark"} {#philosophizing-on-magnetic-sight .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/10/01/philosophizing-on-magnetic-sight/ "5:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I wanted to document the great victory of the Hindu armies in the land of Karroo. After the memories of 1983 which are faint, almost going back to our infancy, this is the only time we have displayed any notable valor in battle with the two Rajput warriors on the helm putting to sword the Anglo-Saxons, the peerless pirates from down under, the Voortrekkers and finally the bloody-eyed al-ghazis. But then I was talking to people who make little sense of cricket. But R and I had a long talk that starting from Steve Irwin's crocodiles, passed through a three part BBC documentary on reptiles, then through cryptochromes whose ancient origins we had nailed, and finally took a philosophical turn on to consciousness. I would have liked to capture the various elements of it, both scientific and philosophical, but for now just a few points for we are like the haunted man chased by a वेताल.

"vedA yo वीनाम् padam अन्तरिक्षेण पतताम् |" (शुनःशेप Ajigarti in RV 1.25.7a)\
[वरुण] knows the migratory path of the birds through the atmosphere

"सर्वा वा इयं vayobhyo नक्तं दृषे दीप्यते | तस्माद् imAM वयांसि नक्तं नाध्य् Asate | ya एवं विद्वान् अग्निं chinute praty eva तिष्ट्य abhi disho jayati |" TS 5.6.4\
A\
All this (earth) in the eyes of the birds shines at night, therefore at night birds do not rest here. He who knowing this piles a fire finds support, and conquers all the quarters.

How do the birds manage their great feats of navigation, which need no further dilation ? How do they manage to home so accurately?

I believe the main reason why we fail to understand this well is because we apparently lack the magnetic sense. We have sight so we recognize an eye when we see it another animal or for that matter even in an alga like [Chlamydomonas]{style="font-style:italic;"}. We smell and taste so we can recognize the chemoreception in other organisms, we hear and we feel various stimuli via skin and sense stretch thus we are able to recognize auditory and tactile receptors elsewhere in the living world. These senses tend to dominant our conscious experience. There are two other senses that are less obtrusive on the consciousness --- being filtered out most of the time. One of them strongly impinges on the consciousness, when altered --- the balance sense. The other one, proprioception, while apparently filtered out does seem to contribute to the sense of I, an important part of conscious experience. But when it comes to magnetic sense most of us do not know what that is, and hence lack that experience to easily recognize its receptors in other animals.

Yet we have come to realize over the ages that it is one of the primary forces behind avian migration and homing.\
continued ...


