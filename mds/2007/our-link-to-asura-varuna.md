
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Our link to asura वरुण](https://manasataramgini.wordpress.com/2007/04/01/our-link-to-asura-varuna/){rel="bookmark"} {#our-link-to-asura-वरण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 1, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/04/01/our-link-to-asura-varuna/ "6:46 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

अजीजनो hi वरुण स्वधावन्-नथर्वाणं पितरं deva-बन्धुं |\
तस्मा u राधः कृणुहि सुप्रशस्तं sakhA no asi परमं cha बन्धुः || AV-S 5.11

From father भृगु we have acquired that connection to the great asura medhira. As kavi अथर्वा had said to वरुण: we have walked the 7 steps of friendship and thus become connected. When atharvan desired to know that which lay beyond the rajas he asked वरुण, for the asura is the knower of that which is "paro rajasi" (kavi atharvan asks: "kiM rajasa एना paraH ?). When भृगु desired to acquire supreme knowledge he went to father वरुण (भृगुर् vai वारुणिः | वरुणं pitaram उपससार |). The wise asura answered : "annaM प्राणं cअक्षुः श्रोत्रं mano वाच miti | तग्ं ho वाच | yato वा इमानि bhUtAni जायन्ते | yena जातानि जीवन्ति | yat-prayanty-abhi-संविशन्ति | tad-विजिज्ञासस्व |..."\
These words inspired the founder of our clan in the quest for knowledge and they are what we continue to seek. These words of the Aditya are the ones that set us on the path of knowledge. When our ancient clansman शुनःशेप stood bound at the stake, facing certain death, with harishchandra dying in pain from the jalodara, he invoked the all-seeing Aditya:\
"ava te हेळो वरुण namobhir ava यज्ञेभिर् Imahe हविर्भिः |\
क्षयन्-nasmabhyam asura प्रचेता राजन्-नेनांसि शिश्रथः कृतानि ||"

We have this bond (paramam cha बन्धुः of atharvan) with वरुण to which we turn. When over-powered by the अभिचार of the foemen, with our enchanting यक्षिणि-s all dispersed, struck down by roga like harishchandra, we turned to rajan वरुण. We sought him like our ancestors who knew the veda did. We besought him of a 1000 snares to release us from the grip of the pAsha in which we were. The great god has bore us aid when we invoked him with his great mantra from the pit of suffering in which we were.\
" bibhrad द्रापिं हिरण्ययं वरुणो vasta निर्णिजम् | pari spasho ni Shedire ||"


