
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The third vIra](https://manasataramgini.wordpress.com/2007/03/10/the-third-vira/){rel="bookmark"} {#the-third-vira .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 10, 2007]{.entry-date}](https://manasataramgini.wordpress.com/2007/03/10/the-third-vira/ "10:16 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Whom shall वृत्रहन् make the master of the realm?\
Whom shall the wise asura of the skies favor?\
Whom shall the bright Aditya be the friend of?\
Whom shall father dyaus and mother पृथिवि raise to glory?\
Whom will the golden-handed deva सविता impel?

The third hero has broken forth.\
He has become the winner of the गविष्टि.\
Those who destroyed his ratha are dead.\
The अभिचार्यमानस् have run away from him\
The vajra of indra has broken his confinement.\
He has become a victorious hero, the Ishvara of his realms.\
They call him the bearer of the सुपर्ण.

The other two heroes only dreamed of such glory.\
Broken in battle, without friends and kinsmen,\
running from pillar to post, were sinking.\
Borne away by राक्षसिस् they were eaten.

The third hero was unfazed. Everyone knew he was wiser than all, and in this world it is wisest who wins. He was the master of dissimulation, in contrast to the last hero who knew nothing of this art. He was stationed there, where the one who had in his days attained eko मानुष आनन्दः was dragged away by the son of विवस्वान्. The third hero waited confidently in his lair with great patience, because he knew how to lure the prey. In contrast, the brash first hero rushed into the thick of battle and was hit by the ब्रह्मास्त्र विद्या. The third hero was hit many times but he was the one favored by the gods and he stood there hiding in the cave for his moment with his strung bow ready for action. He saw the first hero, who had exhausted his thunder, being carried away by a form like that of अयोमुखी. He gently slid back behind the shadows. In the silent evening, the asymmetric archer crawling behind an overgrown hedge saw the third hero hiding in the cave. He thought to himself this fellow is afraid. Nay, fear was not going to be part of the third hero's day but that of the asymmetric archer.

Two days went by-- "I see it" he said. It was there fearsome as a phantasmagorical vision in the pit where atri had fallen. He looked back the door was shut. A single dimension separated him from the great unknown.


