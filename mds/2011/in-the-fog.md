
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [In the fog](https://manasataramgini.wordpress.com/2011/08/30/in-the-fog/){rel="bookmark"} {#in-the-fog .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 30, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/08/30/in-the-fog/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The while we were being pierced by the various darts the दूरदृष्टि prayoga revealed an unexpected attack on the sachiva. We awoke somewhat shaken by the revelations. But we quickly engaged in the प्रतिक्रिया that saved the sachiva from coming to face to face with the red-eyed buffalo. The अमात्य informed us that every thing had happened exactly as we had seen in the दूरदृष्टि prayoga, and the conclusion changed only due to the प्रतिक्रिया and their own repeated deployment of rudra.

With the sachiva we had proceeded to the fortress one who wanders among the granite rocks. The अमात्य wished that we meet the bearer of the two torches. We observed that the hero was bound by the अरायी. Since we were enormously weakened, we told the sachiva not to attack and retreat silently. Contrary to our advice the sachiva became involved in the battle of ohas -- the out come was uncertain with respect to the sachiva but it only weakened the hero further. We then fulfilled the अमात्य's wishes but saw through the hollowness of the bearer of the two torches. We returned even further weakened from the adventure.

The yogin was waiting in धनुर्दारु-द्वीप. After years of struggle the दूती appeared before him. He did not know at first if she was a दूती or just a woman -- after all a yogin without the ज्ञान may not know that only through a दूती can siddhi be attained. The दूती sent him a signal. Verily the signals of दूती are as clear as the midday sun and he who receives his दीक्ष from her can be a mantra-siddha. He conjoined with her and attained siddhi even as a mantra of उच्छिष्ट gaNapati bearing fruit. The power of his yoga was complete -- even though they cut him up he was whole again.

They call that rAkShasa व्जृम्भिन्. He verily opens the doors for the rest. He prevented the यामल in the आज्ञ from placing the साधक beyond the realm of the sun, moon and fire. What is the secret to over come that rAkShasa, like विष्णु's underground stem?


