
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some considerations on Indian polity](https://manasataramgini.wordpress.com/2011/11/13/some-considerations-on-indian-polity/){rel="bookmark"} {#some-considerations-on-indian-polity .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 13, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/11/13/some-considerations-on-indian-polity/ "8:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Along with the failure to restore संस्कृत, one of the congenital defects of modern India was its failure to reacquire its tradition of political thought. While such visions might have existed in the world of the Lal-Bal-Pal trio, they were certainly denuded by the coming of mahAtmA-चाचा duo. Of the two, चाचा had the merest modicum of knowledge of any aspect of traditional India thought, as can be seen from his own writings of India history. On the other hand, though the faux mahAtmA was a firm Hindu, his understanding of the dharma was perverted by all manner of self-delusions that characterized many a Hindu of that age. In this back drop, the only man with a natural sense of action that characterized the true Hindu spirit was the mahAtmA's own संस्कृत student vallabh-bhAI -- sadly he did not remain alive long after independence. The effects of this start were truly pernicious -- none of the prominent founding leaders paid any attention to genuine Hindu political thought, and this neglect propagated through the whole education system of India. For instance, in the 8th or 9th class we were introduced to political constructs and our textbook waxed about the Magna Carta, English democracy, the French revolution, the American revolutionary war, Fourier, Engels, Marx and the Russian communists. However, there was not a word about the नीतिशास्त्र in translation or the original. Hence, it is not at all surprising that a Hindu might mouth political platitudes such as "[vasudhaiva कुटुम्भकं](http://bharatendu.com/2008/08/29/the-hoax-called-vasudhaiva-kutumbakam-1-hitopadesha/)" or that a front-line Hindu geopolitical analyst is more concerned about his peg of scotch than the नीतिशास्त्र. Even as whole new generations of Hindus were being trained in the neglect of (or even hostility towards) their own political tradition, a new generation of white indologists emerged in the Euro-American academia, such as Pollock, Heesterman, Bronkhorst, and Davidson, who took up the philological analysis of this tradition. While philologically reasonably competent, the productions of these indologists were ghastly and misplaced in spirit. Despite their apparently sincere protestations of having outgrown the old colonial paradigms, these white indologists labor under an implicit sense of superiority of the truth claims of their own systems, such Euro-American democracy and academic liberalism. We may see this a secularized version of the Abrahmistic truth claim concerning God. In the system of dharma we do not present the presence of an Ishvara as a truth claim; instead, our darshana-s try to make a case for its presence or absence using logic. Likewise, a Hindu thinker does not accept Euro-American democracy or liberalism or modernism as a truth claim -- we need to really compare its *theoretical* foundations, practical expressions and history with our own extensive corpus of the नीतिशास्त्र. There is no reason a priori to accept the Euro-American constructs as truth.

In contrast to their extant descendents, the Arya-s of yore stressed the importance political thought in no unclear terms. In his deathbed lecture to the victorious पाण्डु monarch, the grandsire said in his famous त्रिष्टुभ्-s:

[मज्जेत्त्रयी दण्डनीतौ हतायां सर्वे धर्मा न भवेयुर् विरुद्धाः ।]{style="color:#0000ff;"}\
[सर्वे धर्माश् चाश्रमाणां गताः स्युः क्षात्रे त्यक्ते राजधर्मे पुराणे ॥]{style="color:#0000ff;"}\
If the politico-legal system disappears the three-fold shruti will disappear, all dharma-s that inculcate the human duties are overturned. Indeed, if the ancient political system upheld by the kShatriya-s are forsaken, then all the duties in respect of all the modes of life, become lost.

[सर्वे त्यागा राजधर्मेषु दृष्टाः सर्वा दीक्षा राजधर्मेषु चोक्ताः ।]{style="color:#0000ff;"}\
[सर्वे विद्या राजधर्मेषु चोक्ताः सर्वे लोका राजधर्मान् प्रविष्टाः ॥]{style="color:#0000ff;"}\
In politics are realized all forms of renunciation, in politics are united all ritual initiations (दीक्ष-s); all kinds of knowledge are linked in politics; and the \[conduct of] all the worlds are based in politics.

[यथा जीवाः प्रकृतौ वध्यमाना धर्माश्रितानाम् उपपीडनाय ।]{style="color:#0000ff;"}\
[एवं धर्मा राजधर्मैर् वियुक्ताः सर्वावस्थं नाद्रियन्ते स्वधर्मम् ॥]{style="color:#0000ff;"}\
As the destruction of lives in natural calamities result the torment of those abiding in dharma, even so dharma-s uncoupled from political support causes \[people of] all professions scatter from their ordained paths.

The politics implied by the पितामह in these त्रिष्टुभ्-s is that which upholds all other dharma-s -- the राजधर्म -- the dharma that encompasses the administrative, the legislative, judicial and military systems of a state. Its perversion or loss is seen as a catastrophe by the Hindus -- something that results in the breakdown of all other dharma-s. Hence, even the dharma-s associated with knowledge, renunciations, rituals and the very operation of society are seen to rest upon it. The failure to heed such words is indeed evident in the unfolding fates of the ancient nations of Greece, Rome and the Egypt. The perversion and destruction of the राजधर्म in these nations by the pretamata or the rAkShasa-mata first resulted in the loss of their "veda" followed by the erosion and breakdown other foundations of civilization. Indeed, the fate of these nations is a preview of what awaits bhArata if its peoples were to continue acceding to the entry of the rAkShasa, preta and rudhira mata-s into its polity.

\~ओ^ऊ\~

First two of the above verses are inscribed on the frontispiece of Kashi Prasad Jayaswal, the vaishya scholar's monograph on Hindu polity. Jayaswal was traditionally educated in the देववाणी by a Hindu ascetic and subsequently, inspired by the Shri Lankan scholar Wickramasinghe, he applied his knowledge to several important studies in the rediscovery of the lost Hindu past. He was also a nationalist who supported the cause of liberating the Hindu nation from the fangs of the English. In process he came to study in depth the structural and operational features of old Hindu polity (i.e. those feature distinct from the statements of legal code laid down in the dharmashAstra-s). This was the first major development on the earlier studies on Hindu polity by Law and RK Mookerji. Indeed, we may see his work as the seed that initiated a vigorous discourse among Hindu scholars leading to the re-investigations of the field by likes of DR Bhandarkar, BK Sarkar, NC Vandyopadhyay, Mahalingam and Altekar. During these studies he befriended Rahula Sankrityayana and was deeply influenced by him. Hence, we see in his monograph on Hindu polity the following dedication:\
[*"To the memory of the republican वृष्णिस्, कठस्, वैशालस् and शाक्यस् who announced philosophies of freedom from devas, death, cruelty and caste."*]{style="color:#ff9900;"}\
Thus, despite the erudition and analytical clarity in Jayaswal's research, he had already sown the seeds for the befuddlement of the generations that would follow. Rather than building up, consolidating and practically implementing the results of the scholarly discourse initiated by his work, much of Indian expression in this direction (outside the relatively narrow range of academic scholars listed above) focused on the message of his unfortunate dedication. In a sense, his dedication betrays a certain internalization of the occidental framework during his study of law in England. In a strict sense one might point out that indeed certain ideas in the उपनिषद्-s and the teachings of the तथागत tried to sideline the deva-s, but neither of them even vaguely proposed to render them otiose, as implied by Jayaswal. As far as वर्ण went, the imputations of Jayaswal are even farther from the truth, in regard to the republics he mentions or even more generally. Hence, indeed worthwhile to re-approach the issues of Hindu polity with a different spirit, one which does not a priori accept the superiority of modern or Western constructs in this regard.

\~ओ^ऊ\~

One may question as to why ancient politico-legal Hindu thinking of any value at all in this day and age, so as to spend time re-approaching it from different angles ? First, Our interests in this regard are largely philological, philosophical and scientific. Second, while we are not particularly knowledgeable in geopolitical, military and strategic issues, it is not difficult to note that many of the so called front-line strategic thinkers in post-independence India have a very poor understanding of their own legacy, especially in the old language of the Hindu cosmopolis. We can easily see that this lacuna in their understanding results in disastrous formulations of strategy -- a rather sordid situation for the successors of भीष्म, vidura or विष्णुगुप्त. Instead,  we are provided with are strategic constructs informed by facile ideas, such as those held by चाचाजी and the mahAtmA. Thus, looking into classical rAja-dharma is a necessary corrective to avoid the catastrophic course taken by Greece or Egypt. Third, modern political constructs created in the west are based on Abrahamistic frameworks, which has resulted in मोहसूत्र-s such as: Equal before God; Equal before law; All people are equal; The state having more rights over your genes than yourself! These ideas are not in line with the biological ground situation: individuals are not blank slates and not genetically equal; Selective pressures have shaped different groups of humans differently; not every individual is equally important for society or even for his own family; When fitness of an individual is considered, included fitness cannot be neglected in the equation. Hindus of yore were unfettered thinkers being influenced by the biological ground situation much more than post-Abrahamistic western thought. Thus, we might find in Hindu political thought elements that might be more consonant with ground realities than the western constructs. This is important because a society that can come terms with, rather than clash with, its own sociobiology is less likely to be limited by deleterious policies.

Most importantly, राजधर्म was indeed the common denominator that allowed us to define the nation of the Hindus. So it is not surprising that it has been attacked by the enemies of the Hindu nation in all manner of inane ways. One common tactic has been that of inducing amnesia -- trying to claim that the राजधर्म is a construct for the exploitation of the vish (or in some alien narratives the शूद्र-s and अवर्ण-s) by the brahma-क्षत्र alliance. Here, the detractor causes selective memory loss with respect to the historical evidence and tries to obfuscate the temporal series of events by equating the spread of the memetic package with its alleged forcible imposition by its innovators. After the Arya-s established themselves in northern India they let loose the meme of राजधर्म -- evidently it was so attractive a meme in heathen societies that it expansively spread over the whole जम्बुद्वीप and beyond, even more than the genetic material brought by the Arya-s. In India, over a period spanning well over two millennia we observe three modes of transmission of राजधर्म: 1) The erudite transmission, which was largely spearheaded by the brAhma-क्षत्र combine. 2) Its incorporation into इतिहास-पुराण and transmission by both ब्राह्मण-s and सूत-s for general education of all वर्ण-s. 3) The regional transmission in desha-भाष-s, in many cases to people at the Indo-Aryan fringe, which was largely the work local authors, not necessarily belonging to the brahma-क्षत्र elite. In regard to the latter, we may point out the relatively early work in Tamil of तिरुवळ्ळुवर् belonging to the traditional drAviDa ritualist jAti. The तिरुक्कुरळ् represents a classical example of a concise yet complete transmission of dharma literature in a desha-भाष, wherein the teachings of manu and चाणक्य along with other dharma teachings are summarized. This phenomenon continued over the ages. As pointed out by Jayaswal one might consider the writings of रामदास् in महाराट्टी or those of guru gobind in hindi as such transmissions of राजधर्म that happened within the context of regional religious movements. In this regard, we are led to a noteworthy point by way of Jayaswal's work: He draws our attention to the political work नीतिवाक्यामृत by the naked jaina आचार्य somadeva सूरि who was a protege of the चालुक्य महासामन्त of the राष्ट्रकूट कृष्ण III. The jaina, not surprisingly, expresses considerable rivalry towards ब्राह्मण and kaula तान्त्रिक-s (as in his apologetic novel yashastilaka), but at the same entirely internalizes the राजधर्म issuing forth from great ब्राह्मण authorities like बृहस्पति and उशना काव्य. He, following old Hindu tradition, admits that राजधर्म was founded by these ancient ब्राह्मण-s, and lays out what is enjoined by such authorities as the norm in political wisdom. Likewise, the तथागत has incorporated verses from the मानव dharmashAstra into his dhamma पाद (e.g. 8.9 and 19.5) by rendering them word for word into vulgar pAli. In Tibet we find the tsa na ka rgyal po'i lugs kyi bstan bcos, which is a work on राजधर्म based on चाणक्य's maxims written by king bhojadeva परमार that considerably influenced the kings of Tibet. Similarly, we find multiple नीतिशास्त्र-s derived from संस्कृत originals in the old Indonesian language that were in vogue in the Hindu kingdoms of Indonesia. This shows that the meme of राजधर्म always traveled together with the Indian religious traditions, be it the classical shruti-based tradition or the heterodox shruti-virodhaka traditions or the more regional flavors that did not use the देवभाष. Hence, the राजधर्म meme is a major common thread rooted in the original Arya-dharma that runs across divergent traditions of the subcontinent, even encompassing groups with little Sanskritic learning or those with anti-Vedic tendencies. It is for this reason that secularism is detrimental to the Hindus -- it breaks the organic connection that exists between politico-legal thinking and religion in the Hindu world. As a result it criminalizes Hindu participation in politics on one side and on the other it estranges the different divergent dharma traditions by removing their common thread i.e., the राजधर्म. It was for this reason the policies of the English-approved Indian leaders in the last days of the the British rAj have had long-lasting destructive effects on Indian polity. Hence, every conscious Hindu must oppose secularism in a Hindu state.

\~ओ^ऊ\~

***Vedic election of the राजन् and the samiti***\
In the last part of this epistle we shall examine the validity of ideas first presented by Jayaswal on one of the oldest political institutions of the Arya-s -- the Vedic samiti. Jayaswal saw the samiti as an assembly of the whole people for the election of the राजन्. Thus, he believed that the Vedic samiti was the origin of Hindu democracy. As for the election of the राजन्, Jayaswal appears to be on relatively firm ground as, indeed, the kaushika sUtra of the atharvaveda lays out an ancient ritual in which the राजन् elected by the vish (people) is inaugurated. This is made clear in the mantra-s recited by the purohita on this occasion:\
[त्वां विशो वृणतां राज्याय त्वाम् इमाः प्रदिशः पञ्च देवीः ।]{style="color:#0000ff;"}\
[वर्ष्मन् राष्ट्रस्य ककुदि श्रयस्व ततो न उग्रो वि भजा वसूनि ॥]{style="color:#0000ff;"} (AV-vulgate 3.4.2)

The people have elected you to kingship, you have \[been established among] the five divine directions; at the summit of the state, situated at the pinnacle, from there endowed with mighty power distribute the wealth among us.

Now this is corroborated by another mantra deployed by the brahmA for a similar purpose:\
[आ त्वाहार्षम् अन्तर् अभूर् ध्रुवस् तिष्ठाविचाचलत् ।]{style="color:#0000ff;"}\
[विशस् त्वा सर्वा वाञ्छन्तु मा त्वद् राष्ट्रम् अधि भ्रशत् ॥]{style="color:#0000ff;"} (AV-vulgate 6.87.1; cf. RV 10.173.1)

I \[the brahmA] have brought you forth, you have entered, stand firm not unsteady; let all people choose you, let the the state not fall away from you.

This suggests that in the Vedic system the people might elect or choose a राजन्, but what is the relationship of this to the samiti? Here we need to take a much closer look at the Vedic texts to afford a better picture. We are informed of two distinct assemblies of the Arya-s in mantra-s deployed in the AV ritual for the two assemblies to reach a consensus with the यजमान:

[सभा च मा समितिश् चावतां प्रजापतेर् दुहितरौ संविदाने ।]{style="color:#0000ff;"}\
[येना संगछा उप मा स शिक्षाच् चारु वदानि पितरः संगतेषु ॥]{style="color:#0000ff;"} (AV-vulgate 7.12.1; cf. shatapatha ब्राह्मण 4.1.4.1 for the equivalent shukla यजुर्वेदीय ritual)

Let both the सभा and the samiti, the two daughters of prajApati reaching a consensus favor me. May he train me in \[the means] by which I achieve consensus \[in these assemblies], O ancestors may I speak well in these assemblies.

The two distinct assemblies, the सभा and the samiti, are considered daughters of prajApati (an embodiment of the state), and we learn that speeches are made in these assemblies with the aim of trying to convince them to reach a consensus with the individual applying to them. The same two, सभा and samiti, are mentioned in the same order in the AV royal ritual to aditi as विराज्. Here the सभा and samiti are described as becoming fit with the invocation of aditi into them (AV-vul 8.10.4-5). We further learn that the king is regulated by samiti, i.e. his decisions need arise from the concordance of the samiti. This is supported by a mantra to this effect in the first royal ritual mentioned above.

[ध्रुवोऽच्युतः प्र मृणीहि शत्रून् छत्रूयतोऽधरान् पादयस्व ।]{style="color:#0000ff;"}\
[सर्वा दिशः संमनसः सध्रीचीर् ध्रुवाय ते समितिः कल्पताम् इह ॥]{style="color:#0000ff;"}(AV-vulgate 6.88.3)

Firm, unmoving, may you \[i.e. the newly chosen राजन्] exterminate your enemies, make your enemies fall under your feet; may from all directions a consensus and concordance in action be reached with you; may the samiti reach a consensus for fixing \[your decisions].\
A similar mantra is seen in the rohita-राष्ट्र-भृत ritual:

[रोहितो यज्ञस्य जनिता मुखं च रोहिताय वाचा श्रोत्रेण मनसा जुहोमि ।]{style="color:#0000ff;"}\
[रोहितं देवा यन्ति सुमनस्यमानाः स मा रोहैः सामित्यै रोहयतु ॥]{style="color:#0000ff;"} (AV-vulgate 13.1.13)

The ruddy one is the generator and the mouth of the sacrifice; with speech, hearing and mind I make an oblation to the ruddy one, in the ruddy one the deva-s go with a beneficent mind, may he make me ascend, when the samiti rises.

These mantra-s are deployed, among other things, so that the samiti reaches a consensus to uphold or fix the राजन्'s decisions (See also RV 10.166.4, a mantra among other things for the samiti to hand one the mandate). This conclusion is also supported by the evidence provided by a mantra for the converse to happen. This mantra is from recitations used in the अभिचारिक ritual of the भृगु-s against the inimical vaitahavya rulers who had harmed them. Here a spell is laid to prevent the samiti from supporting them.

[न वर्षं मैत्रावरुणं ब्रह्मज्यम् अभि वर्षति ।]{style="color:#0000ff;"}\
[नास्मै समितिः कल्पते न मित्रं नयते वशम् ॥]{style="color:#0000ff;"}(AV-vulgate 5.19.15)

The rain of mitra and वरुण does not rain upon the ब्राह्मण-harmer, the samiti does not accord him a consensus, no friend comes under his influence.

There are several Vedic references of the king going to the samiti:\
[यत्रौषधीः समग्मत राजानः समिताविव ।]{style="color:#0000ff;"}(RV 10.97.6ab)

A \[physician] has an assembly of medicinal plants even as a king has a samiti.

Further:\
[परि सद्मेव पशुमान्ति होता राजा न सत्यः समितीर् इयानः ।]{style="color:#0000ff;"} (RV 9.92.6ab)\
The hotar seeks the pen rich in cattle, even as a true king attends the samiti.

Thus, we see that a true rajan is one who attends the assembly of the samiti. This is confirmed by references such as those in the चान्दोग्य ब्राह्मण of the सामवेद where the पञ्चाल monarchs (e.g. प्रवाहन jaivali) are described as attending the samiti-s of the पञ्चाल-s. It is from the description in the latter text and the sum-total of the Vedic evidence that we conclude that the samiti was not an assembly to elect the राजन्, as suggested by Jayaswal. Rather, it was an assembly that regulated the राजन्'s decisions by way of reaching a consensus or majority agreement. It was also a place for intellectual discussions. Thus, the samiti was a body that was critical to the government of the Arya-s in the highest level legislation rather than as a electoral assembly.

With respect to the सभा, Jayaswal is largely correct in noting that it was different from the samiti and that it had a primarily judicial function. This is indeed supported, as he points out, by the पारस्कार गृह्यसूत्र 3.13. This suggestion is further supported by the mantra RV 10.71.10:\
[सर्वे नन्दन्ति यशसागतेन सभासाहेन सख्या सखायः ।]{style="color:#0000ff;"}\
[किल्बिषस्पृत् पितुषणिर् ह्य् एषाम् अरं हितो भवति वाजिनाय ॥]{style="color:#0000ff;"}

The friends are delighted in the friendship of him who returns in triumph, having been declared the winner in the सभा; he is the remover of the accusation, the nourisher, he inclined and fit to bring victory.

The remover of the accusation phrase supports the idea that the सभा was the place where legal matters were settled by a majority. This is further clarified by सायण in his commentary on the अथर्ववेदीय mantra (also seen in the shatapatha ब्राह्मण) on the सभा:

[विद्म ते सभे नाम नरिष्टा नाम वा असि ।]{style="color:#0000ff;"}\
[ ye te ke cha सभासदस् te me santu सवाचसः ||]{style="color:#0000ff;"}(AV-vulgate 7.12.2)

We know your name, O सभा, the inviolable majority decision is your name; who ever sits in the सभा may they speak in my favor.

In explaining this सायण states that the name नरिष्टा derives from it being the inviolable majority opinion being reached in the assembly: [बहवः संभूय यद्य् एकं वाक्यं वदेयुस्...न परैर् अति-लङ्घ्यं अतः अनतिलङ्घ्य-वाक्यत्वात् नरिष्टेति ।]{style="color:#0000ff;"} This indeed suggests that the सभा was place where binding judicial decisions were reached. However, we do not hear of the राजन् being mentioned in connection with the सभा, unlike the samiti. This suggests that the सभा probably had a local role unlike the royal samiti. Alternatively, it might point to the separation of the judicial wing of the government from the sphere of the राजन्. In any case, the beginning of the rAja-dharma Arya-s can be seen as having several distinct departments which are clearly distinguished in the vaidika statements on these matters: 1) the राजन् -- the head of the state; 2) the samiti which worked with the राजन्; 3) the सभा which administered justice; 4) the सेना -- the army.Thus, we have been endowed with a long tradition of राजधर्म that goes back to the shruti itself -- to neglect and pervert it is truly reflective of our failure in re-acquiring our traditions.


