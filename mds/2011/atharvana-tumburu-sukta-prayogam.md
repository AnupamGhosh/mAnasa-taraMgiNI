
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [आथर्वण tumburu सूक्त प्रयोगं](https://manasataramgini.wordpress.com/2011/07/08/atharvana-tumburu-sukta-prayogam/){rel="bookmark"} {#आथरवण-tumburu-सकत-परयग .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 8, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/07/08/atharvana-tumburu-sukta-prayogam/ "7:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the आथर्वण tradition after having learned the mantra-s of the नीलरुद्र सूक्त one learns the tumburu सूक्त. The सूक्त of tumburu (AV-P 20.61.8-9; 62.1-6) goes thus:\
[न तत्र भवो हन्ति न शर्व इषुम् अस्यति ।]{style="color:#99cc00;"}\
[यत्र त्वं देव तुम्बुरो पर्वतेषु विरोहति ॥]{style="color:#99cc00;"}\
[कृष्याम् अन्यो विरोहति गिरेर् अन्योऽधिपक्षसि ।]{style="color:#99cc00;"}\
[त्रातारौ शश्वतां इमाव् आ गन्तां शिग्रु-तुम्बुरू ॥]{style="color:#99cc00;"}\
[यावत् पर्णं यावत् फलं यावन्तो ऽध्यऋक्षराः]{style="color:#99cc00;"}\
[ तावन्तः शुष्मास् tumburos tad u te विषदूषणं ||]{style="color:#99cc00;"}\
[रुद्रो जलाषभेषज इमां रोगम् अशीसमः ।]{style="color:#99cc00;"}\
[ये अवाजिज्वलन् इति ॥]{style="color:#99cc00;"}\
[अस्थाद् द्यौर् अस्थात् पृथिव्य् अस्थाद् विश्वम् इदं जगत् ।]{style="color:#99cc00;"}\
[वृषभस्येव कनिक्रदतो ऋषयं शमयामि ते ॥]{style="color:#99cc00;"}\
[सं मा सिञ्चन्तु मरुतस् सं वातो रोहिणीर् उत ।]{style="color:#99cc00;"}\
[ saM मायम् agnis सिञ्चतु प्रजया cha dhanena cha ||]{style="color:#99cc00;"}\
[दीर्घम् आयुष् कृणोमि ते नक्तं हरी मृगयते ।]{style="color:#99cc00;"}\
[दिवा सुपर्णा रोहितौ भवाय च शर्वाय चोभाभ्याम् अकरं नमः ॥]{style="color:#99cc00;"}\
[विशल्यस्य विद्रधस्य वातीकारस्य वालदे ।]{style="color:#99cc00;"}\
[भवाय च शर्वाय चोभाभ्याम् अकरं नमः ॥]{style="color:#99cc00;"}

During prayoga one first recites the वामदेव mantra from the पञ्चब्रह्म series. Then he installs the fire on the स्थण्डिल as per the tradition of the atharvan-s. There after reciting the tumburu सूक्त make oblations of drumstick and tumburu संइध्-s dipped in ghee.

Then he makes the four भगिनी याज-s of ghee with AV-P 20.21.6:\
[उदोजिष्ठां सहस्यां जयन्तीम् अपराजिताम् ।]{style="color:#99cc00;"}\
[लक्ष्मीर् याः पुण्याः कल्याणीस् ता अस्यै सवितः सुव ॥]{style="color:#99cc00;"}

Then he stands under the tumburu tree and offers तर्पणा-s to tumburu with the above सूक्त and the भगिनी-s with the above mantra (x 4). He may then prepare the secret tumburu drugs.

This सूक्त is of considerable importance for multiple reasons:

 1.  It helps understand the origin of the central deity of the वाम-srotas, tumburu rudra. Most modern Hindus unacquainted with the शास्त्र-s of the वामस्रोतस् are not familiar with this once popular system. Hence, tumburu to them usually stands for the commonly known gandharva, who is frequently mentioned in the रामायण and the महाभारत. However, the evidence for a well developed worship of tumburu as the deity of the वामास्रोतस् is apparent from number of texts. The विष्णुधर्मोत्तर suggests that he was being worshiped with his four sisters in the gupta era. The worship of tumburu and his sisters is also attested in the nAstika मञ्जुश्रिय मूलकल्प and a fragmentary Gilgit manuscript discovered by SDV. An early jaina yantra the वर्धमान विद्या has taken up the चतुर्भगिनी and replaced tumburu with the jina. His worship appears to have been fairly widespread in Asia, even if scattered, till around the middle of the 1200s. After that it has largely declined and only maintained within the स्रोताम्सि shaiva krama. Nevertheless, the origin of the tumburu as an emanation of rudra was rather poorly understood, given the apparently older presence of tumburu the gandharva with whom he shares a penchant for music. The AV-P सूक्त shows that the origin of the connection between tumburu and rudra is a much older one than the classical वामस्रोतस् texts and might have even predated the eponymous gandharva.

While the musical connection of tumburu is not apparent in the veda, there are other links which establish a continuity with the deity of the वामस्रोतस्. In the veda tumburu is a sylvan form of rudra associated with cures against diseases, which is an ancestral trait of the Indo-European rudra-like deities. Indeed, this trait is stressed in the वामस्रोतस्, where rituals to tumburu and the chaturbhagini are stated as being a counter to disease.\
e.g.:\
[मुच्यते च सदा रोगैर् मृत्युरूपैर् दुरासदैः ॥]{style="color:#99cc00;"} VT 92cd

[शतजप्तो जलेनापि ततो वा मुच्यते सदा ।]{style="color:#99cc00;"}\
[व्याधिघात समिद्भिस् तु व्याधिनात्यन्तपीडितः ॥]{style="color:#99cc00;"}\
[अष्टोत्तर शतेनैव आहुतीनां न संशयः ।]{style="color:#99cc00;"}\
[क्षीराक्तेन तु देवेशि रोगी रोगाद् विमुच्यते ॥]{style="color:#99cc00;"} VT 183-184

Further, in the AV-P the tumburu hymn occurs in the larger context of a number medical applications, including the administration of the ओषधि-s for the treatment of tertian malaria. This is reminiscent of the kashyapa saMhitA where rudra is invoked before treatment of malaria. Thus, it is clear that the worship of tumburu rudra has an hidden history from the AV period, with the shaiva आथर्वण tumburu prayoga विधान establishing the link with the वामस्रोतस्.
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-isD1Lg1tjiQ/ThaeGcEev_I/AAAAAAAACI8/GUNbEMTTkIU/s400/tumburu.jpg){width="75%"}
```{=latex}
\end{center}
```



The tumburu tree


 2.  This AV-P allusion offers the first reference to a celebrated dyad of plants that rise to considerable prominence in classical Hindu medicine- tumburu and shigru. The use of samidh-s from the tumburu tree are well-known in the early atharvavedic shaiva tradition: The उच्छुष्म kalpa of the AV परिशिष्ठ-s (AV-par 36.4.2) mentions the use of these samidh-s in उच्छाटन rituals. But the use in the AV-P text is clearly of a medical nature. This may be compared to the निघाण्ठु sUtra-s regarding these plants in classical Hindu medicine where they often occur together:\
[तुम्बुरुः पित्त-कृद् वात-कृमि दौर्गन्ध्य नाशनः ।]{style="color:#99cc00;"}\
[शिग्रुस् तीक्ष्ण लघुर् ग्राही वह्नि-कृत् कफ-वातजित् ।]{style="color:#99cc00;"}

Thus, the tumburu plant is described as being an augmenter of pitta, destroyer of parasitic infestation, a suppressor of bad body odor and a counter for the वात. The shighru plant is described as "light", "sharp", a constrictor of the gut, an augmenter of digestion and a counter to kapha and वात. It is interesting to note that in classical medicine these plants are both described as counters to वात. This should be seen in light of the term "वातीकार" in the tumburu hymn -- it appears that there was an ancient connection between the prescription of these plants and problems of "वात", even-though the tri-humoral theory was hardly accepted in its classical form in atharvanic medicine. There is no doubt that both these plants appear to have considerable promise in medicine. The nutritional value of the drumstick is well-known. However, it would be of particular interest to further develop certain key compounds from this plant: moriginine, spirochin and niazimicin. Likewise, several compounds from tumburu like tridecanonchelerythrine, conifegerol, canthin-6-one, asarinin, dictamnine and skimmianine are of potential interest.


