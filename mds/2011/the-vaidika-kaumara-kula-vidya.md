
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The vaidika कौमार kula विद्या](https://manasataramgini.wordpress.com/2011/04/19/the-vaidika-kaumara-kula-vidya/){rel="bookmark"} {#the-vaidika-कमर-kula-वदय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/04/19/the-vaidika-kaumara-kula-vidya/ "5:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier discussed the root mantra of the कुमार शासन as provided in the वैखानस mantra-prashna. The complete formulation, which includes the mantra-s of कुमार and देव्यः, represents an early form of the kula विद्या. Another early vaidika kula विद्या is provided by the बोधायन mantra prashna (mantra-s: 181-182). Its correct form is only known to those who understand the foundations of the कुमार-शासन.\
The कुमार mantra:\
[ॐ अघोराय महाघोराय नेजमेषाय नमो नमः ॥]{style="color:#99cc00;"}\
The mantra of the kula देव्यः:\
[आवेशिनी ह्य् अश्रुमुखी कुतुहली हस्तिनी जृंभिणी स्तंभिनी मोहिनी च ।]{style="color:#99cc00;"}\
[कृष्णा विशाखा विमला ब्रह्मरात्री भ्रातृव्य-संघेषु पतन्त्य् अमोघास् ताभ्यो वै मातृभ्यो नमो नमः ॥]{style="color:#99cc00;"}

The textual corruptions have resulted in some confusion among the readings of the mantra-s. The most common problem is the correct interpretation of the number of the मातृ-s of the कुमार kula. One तान्त्रिक interpretation notes 14 मातृ-s as part of a 14 pointed मण्डल. However, the version given above is the form preserved in the oral tradition of the तैत्तिरीयक-s who follow the बोधायन sUtra (like my maternal clan). The published बोधायन mantra पाठ (and its otherwise best-preserved manuscript) and the sUtra text have corrupt readings at different points which can be recombined to create a "critical" reading completely concordant with the oral version. In this reading the कुमार kula consists of only 12 मातृ-s. These are: 1) आवेशिनी (the possessor); 2) अश्रुमुखी (bloody faced); 3) कुतुहली (the eager one); 4) हस्तिनी (the elephant headed one); 5) जृम्भिणी (the stretching one); 6) स्तंभिनी (the paralyzer); 7) मोहिनी (the deluder); 8) कृष्णा (the black one); 9) विषाखा (having he form of विशाख, one of the 4 कौमार मूर्ति-s); 10) विमला (the unblemished one); 11) ब्रह्मरात्री (the epoch of universal dissolution); 12) भ्रातृव्य-संगेषु-patanti (one who falls upon the enemy hordes).\
The last name has been broken up into two separate names in certain interpretations but this is not the case in original vaidika form because it neither supported by manuscript nor oral tradition. Some might also interpret the अमोघाः as a further name of a मातृका. This interpretation is supported by the presence of the name अमोघा as one of the मातृका-s in the trayodasha-प्राकारीय पञ्चदशि कौमार chakra and the equivalent कौमार circuit described in the महाभारत. However, it being in plural, in the vaidika mantra provided above it is likely to be a description of all the मात्र्^इ-s of the kula (i.e. meaning they are infallible). The accentation (i.e. positions of the उदात्त-s) of few of the names in second hemistich of the देवीनाम् mantra is corrupt in the published form of the mantra prashna and needs to be known from oral tradition.


