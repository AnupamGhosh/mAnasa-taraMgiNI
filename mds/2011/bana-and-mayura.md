
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [bANa and मयूर](https://manasataramgini.wordpress.com/2011/09/14/bana-and-mayura/){rel="bookmark"} {#bana-and-मयर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 14, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/09/14/bana-and-mayura/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

bANa and मयूर are reputed to have had a major rivalry in the court of the great emperor हर्षवर्धन of स्थानीश्वर. We had earlier noted one of मयूर's greatest poetic master pieces the [मयूराष्टक](https://manasataramgini.wordpress.com/2006/04/16/mayurashtakam/) which was composed on baNa's beautiful wife leading to much tension between them. It was brought to our attention by the great paNDita that the verse regarding the [tripura-dahana](https://manasataramgini.wordpress.com/2010/04/11/the-richness-of-meaning/) that we had cited before was actually a work of bANa from amaru's collection of सुभाषित-s. We consider that verse one of the greatest miniature master pieces of the virtuoso. This lead us to stumble upon a verse of मयूर on the same topic that bears striking resemblance to that of bAna, suggesting that they possibly tried to out-do each other by producing similar verses. Both verses have the same striking blend of the amorous, terrifying and the tragic tropes:\
[संव्यानांशुक-पल्लवेषु तरलं वेणी-गुणेषु स्थिरं मन्दं कञ्चुक-सन्धिषु स्तन-तटोत्सङ्गेषु दीप्तार्चिषम् ।]{style="color:#99cc00;"}\
[आलोक्य त्रिपुरावरोधन-वधूवर्गस्य धूम-ध्वजं हस्त-स्रस्त-शरासनो विजयते देवो दयार्द्रेक्षणः ॥]{style="color:#99cc00;"}

संव्यान= upper garment; अंशुक=blouse; pallava=fringe of the dress; taralam=tremulously; वेणी-guNa=strands of tresses; स्थिरं=firmly; मन्दं=slowly' कञ्चुक-sandhi= borders of the bra; stana-तटोत्सङ्ग= the contours of the breasts; दीप्तार्चिषम्= blazes into a flame; Alokya=sees;\
त्रिपुरावरोधन-वधूवर्गस्य= Attacking the throngs of tripura ladies; धूम-ध्वजं= column of smoke; hasta= hand; srasta= drops; शरासनो= bow; vijayate= victory to; devo= the god; दयार्द्रेक्षणः= eye moist with pity.

Roughly: Victory to the god who with eyes most with pity looks on at the column of smoke attacking the throngs of tripura ladies, tremulously \[seizing] the fringes of the upper garments and blouses, firmly \[seizing] the strands of their tresses and slowly moving upon the borders of their bras and the contours of their breasts blazes into a flame.

Here is bANa's repeated for comparison:\
[क्षिप्तो हस्तावलग्नः प्रसभम् अभिहतो ऽप्य् आददानो ऽंशुकान्तं गृह्णन् केशेष्व् अपास्तश् चरण-निपतितो नेक्षितः संभ्रमेण ।]{style="color:#99cc00;"}\
[आलिङ्गन् यो ऽवधूतस् त्रिपुर-युवतिभिः साश्रु-नेत्रोत्पलाभिः कामीवार्द्रापराधः स दहतु दुरितं शाम्भवो वः शराग्निः ॥]{style="color:#99cc00;"}

क्षिप्तो=cast off; hastA= hands; अवलग्नः= adhering to; prasabham=forcibly; abhihato= pushed away; .apy= moreover ; आददानो= taking; अंशुक= dress; अन्तं=fringes; गृह्णन्=seized केशेष्व्=hair (locative plural); अपास्तश्=thrown off; चरण= foot; nipatito= fallen down; नेक्षितः= not looked at; संभ्रमेण=perturbed/fear (instrumental singular)

आलिङ्गन्= embrace; yo= which; .अवधूतस्= shaken off; tripura-युवतिभिः= tripura maids; साश्रु-नेत्रोत्पलाभिः= tear-laden eye-lotuses (instrumental plural); कामीव= like a lover; अर्द्रापराधः= transgression of an affair; sa=he; dahatu=burn; दुरितं=sins; शाम्भवो=of शंभु; वः= you; शराग्निः= arrow fire.

Roughly: Though cast off he grasped their hands, moreover though forcibly pushed away he took hold of the fringes of their dress, though thrown off he got into their hair; though in fearful awe he was not looked at, he fell at their feet like a lover guilty from the affair with another; thus, though shaken off by the tripura maids, with their lotus eyes laden with tears, did the fire from शंभु's missile embrace them; may he burn your sins.

Another effort on the identical theme is that of the obscure poet मङ्गल who is cited by many poets at least after the 900s of the CE:\
[सिन्दूर श्रीर् ललाटे कनक-रस-मयः कर्ण-पार्श्वे ऽवतंसो वक्त्रे ताम्बूल-रागः पृथु-कुच-कलशे कुङ्कुमस्यानुलेपः ।]{style="color:#99cc00;"}\
[दैत्याधीशाङ्गनानां जघन-परिसरे लाक्षिक-क्षौम-लक्ष्मीर् अश्रेयांसि क्षिणोति त्रिपुर-हर-शरोद्गार-जन्मानलो वः ॥]{style="color:#99cc00;"}

सिन्दूर= vermilion; श्रीर्= auspicious mark; ललाटे=on fore head; kanaka-rasa-मयः=gold and mercury burnished; कर्ण-पार्श्वे .अवतंसो= earrings of the earlobes; vaktre=on mouth; ताम्बूल-रागः=betelnut paste; पृथु-kucha-kalashe= broad pitcher-like breasts; कुङ्कुमस्यानुलेपः= smearings of कुङ्कुम; दैत्याधीशाङ्गनानां= of the women of the lord of the daityas; jaghana-parisare= around the hips; लाक्षिक-क्षौम-लक्ष्मीर्= rich carmine-dyed silk; अश्रेयांसि= infelicities; क्षिणोति= removes; tripura-hara-शरोद्गार-जन्मानलो=fire born from the discharge of the arrow that destroyed the three citadels; वः= your

May fire born from the discharge of the arrow that destroyed the three citadels, which \[burns] like the auspicious vermilion on mark on the forehead of the women of the lord the daitya-s, like the gold and mercury burnished earrings on their lobes, the betelnut paste on their mouths, like the कुङ्कुम smearings on their broad pitcher-like breasts and like the rich carmine-dyed silk around their hips, remove your infelicities.

This verse by मङ्गल, while ornate, at least to our tastes, seems a less smooth than those of the masters bANa and मयूर. In any case these verses offer some interesting lifestyle information about the early medieval period of Hindu India: 1) Three terms of female attire: कञ्चुक, अंशुक and संव्यान suggest that multiply layered upper garments were worn by women since at least the 600s of the CE contrary to the claims made based on the iconography.

 2.  मङ्गल's verse provides evidence for लाक्षिक, i.e. the carmine dye extracted from the scale insect *Laccifer lacca* being used to dye silk. Of course other uses of the लाक्ष (AV-vulgate 5.5), as a plaster for bone fractures, or in making flammable structures (the kaurava plot in the bhArata), or to extract medicinal compounds are widely seen in old Sanskrit literature.


