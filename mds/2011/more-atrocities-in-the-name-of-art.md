
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [More atrocities in the name of art :-)](https://manasataramgini.wordpress.com/2011/05/16/more-atrocities-in-the-name-of-art/){rel="bookmark"} {#more-atrocities-in-the-name-of-art-- .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 16, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/05/16/more-atrocities-in-the-name-of-art/ "5:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

*gada's child*\
```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh3.ggpht.com/_hjuA1bE0hBw/S-S2CHKeOkI/AAAAAAAABaw/E8qze7qWXGI/s800/modern_art.png){width="75%"}
```{=latex}
\end{center}
```



