
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Notes on हयग्रीव siddhi among the nAstika-s](https://manasataramgini.wordpress.com/2011/06/26/notes-on-hayagriva-siddhi-among-the-nastika-s/){rel="bookmark"} {#notes-on-हयगरव-siddhi-among-the-nastika-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 26, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/06/26/notes-on-hayagriva-siddhi-among-the-nastika-s/ "7:22 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While most extant मन्त्रवादिन्-s practicing पाञ्चरात्रिक apotropaic rituals have a predilection for the ratna-traya mantra-rAjA-s, namely नृसिंह, garuDa and chakra, some are well-versed in the secrets of हयग्रीव. Even less known is the साधन of हयग्रीव laid out in the context of the shaiva mantra-मार्ग, i.e. the tantra of the भूत-srotas known as हयग्रीवं (a member of the deities of the भूत-srotas along with खद्गरावण). The once vigorous practice of हयग्रीव, centered around the great पाञ्चरात्रिक पीठ of हयग्रीव-माधव (adjacent to the कामाख्या site), might have been the source of its transmission to the great schools of the nAstika-s in the vicinity. Few years ago, we read an amazing account of the practice of a हयग्रीव siddhi by a Lama at the Yeshe vihAra, Nyagrong, in occupied Tibet by a Canadian academic Marc des Jardins. He even had convincing photos of the Lama performing vahni-stambhana with the हयग्रीव mantra. From his account we may reconstruct the ritual thus:

For the sake of removing persistent illness affecting certain साधक-s living in the vihAra, the Lama decided to perform a ritual to हयग्रीव. After a homa with अपूप-s called "torma" in Tibetan, ghee and some secret plants the साध्य-s (those needing treatment) were led to the kitchen of the vihAra. There great fires were set up and being exalted further by blowing air. They were surrounded by a circular मण्डल drawn using colored powder. On one stove was a cauldron with a boiling potion in it. On the other stove, stones were being heated till they were glowing red. The Lama took his seat near the stove and began muttering the हयग्रीव मूलमन्त्र \[Footnote 1]. An uttara-साधक lifted a red-hot stone with a pair of tongs and put it on the open palm of the Lama. As soon as he did so there was a puff of smoke and fire rising upwards. He then twirled the red-hot stone over the heads of the साध्य-s and threw it into the potion in cauldron on the other fire, with much hissing and boiling over of the liquid. This was repeated five times. Then he dipped a juniper twig into the cauldron and sprinkled each of the साध्य-s. Strikingly, the Canadian examined the Lama's hands and found no signs of burns.

While हयग्रीव (or his nAstika reflex परमाश्व) is an important heruka figure with his योगिनी मारीची or वज्रवाराही in several मन्त्रयान साधन-s transmitted to Tibet, the ritual reported by des Jardins is from the Bön system. This is of interest because it often contains an early layer of transmission of Indic memes, distinct from that seen in the four main Tibetan bauddha schools, i.e., Nyingma, Sakya, Kagyu, and Gelug. While हयग्रीव entered the nAstika मण्डल-s before the yoga and योगिनी tantra-s, right in the चर्या tantra-s, many of the Sanskrit sources of this layer have been lost due to the Islamic depredations of India. This layer is also most likely to have a close link to the भूत-srotas of the shaiva mantra-मार्ग and earlier पाञ्चरात्रिक हयग्रीव साधन-s. Hence, it is of interest to study the mantra साधन-s of हयग्रीव (called Tamdrin in Tibetan) among the Bönpo and material pertaining to this देवता in the hagiography of Tönpa Shenrab Miwoche (ग्षेन्-rab mi-bo) who is considered the Adibuddha of the Bönpo.

It should be noted that हयग्रीव represents one of the early theriomorphic vibhava-s of विष्णु, who predates the नृसिंह form. Thus, we have an early macranthropic description of विष्णु as हयग्रीव which is preserved in both the महाभारत and the हरिवंश. The rise of नृसिंह appears to have displaced हयग्रीव to a degree in the पाञ्चरात्रिक texts. Nevertheless, the older presence of हयग्रीव (as in the नारायणीय section of the epic पाञ्चरात्र) and his connection to the वाजसनेयिन् form of the sun suggests that he was important in the early layer of the पाञ्चरात्रिक system (it should be remembered that the पाञ्चरात्रिक-s explicitly associated their tradition with the वाजसनेयी saMhitA). An examination of the macranthropic हयग्रीव, which might also be compared to the description of the ashvamedha horse in the veda, provides an early witness for key iconographic elements, several of which continued to survive in the nAstika manifestations of the देवता:\
Mbh 12.347\
[जहौ निद्राम् अथ तदा वेद-कार्यार्थम् उद्यतः ।\
ऐश्वर्येण प्रयोगेण द्वितीयं तनुम् आस्थितः ॥\
सुनासिकेन कायेन भूत्वा छन्द्रप्रभस् तदा ।\
कृत्वा हयशीरः शुब्रं वेदानां आलयं प्रभुः ॥\
तस्य मूर्धा समभावद् द्यौः स-नक्षत्र-तारका ।\
केशश् चास्याभवन् दीर्घा रवेर् अंशु संअप्रभः ॥\
कर्णाव् आकाशपाताले ललाटं भूतधारिनी ।\
गङ्गा सरस्वती श्रोण्यौ भ्रुवाव् आस्तां महोदधी ॥\
चक्षुषी सोम-सूर्यौ ते नासा संध्या पुनः स्मृता ।\
ऒंकारस् ते अथ संस्कारो विद्युज् जिह्वा छ निर्मिता ॥\
दन्ताश् च पितरो राजन् सोमपा इति विश्रुताः ।\
गोलोको ब्रह्मलोकश् च ओष्टाव् आस्तां महातमनः ॥\
ग्रीवा छास्याभवद् राजन् कालरात्रिर् गुणोत्तरा ।\
एतद् धयशिरः कृत्वा नानामूर्तिभिर् आवृतं ॥\
अन्तर्दधौ स विश्वेशो विवेश च रसां प्रभुः ।]{style="color:#0000ff;"}

Thus, विष्णु assuming his second form, i.e., that of हयग्रीव is supposed to have attacked the demons madhu and कैटभ and slain them in a fierce battle. A similar account is given in the हरिवंश regarding this battle were विष्णु's हयग्रीव form is described as encompassing all the gods (HV appendix 1.41.1421-130):\
[स्तूयमानश् च विबुधैः सिद्धैर् मुनिवरैस् तथा ।\
सस्मार विपुलं देहं हरिर् हयशिरो महान् ॥\
कृत्वा वेदमयं रूपं सर्व-देवमयं वपुः ।\
शिरो-मध्ये महादेवो ब्रह्मा तु हृदये स्थितः ।\
आदित्य-रश्मयो वालाश् चक्षुषी शशि-भास्करौ ॥\
जङ्घे तु वसवः साध्याः सर्वसंधिषु देवताः ।\
जिह्वा वैश्वानरो देवः सत्या देवी सरस्वती ॥\
मरुतो वरुणश् चैव जानुदेशे व्यवस्थिताः ।\
एवं कृत्वा तथा रूपं सुराणाम् अद्भुतं महत् ॥\
असुरं पीडयाम् आस क्रोधाद् रक्तान्त-लोछनः ।]{style="color:#0000ff;"}

Of note in these accounts is the role of विष्णु in recovering the lost veda. This might be compared to the legend of the वाजसनेयिन्-s where the veda lost by their founder याज्ञवल्क्य due to his teacher's curse was restored to him by the sun in a horse-headed form. This motif also occurs in an inverted form in the legend of the दानव हयग्रीव who stole the veda-s from brahma before he was killed by विष्णु and the shruti was restored. This solar connection is also evident in the iconography of हयग्रीव in the above accounts -- his horse mane is described as being comprised of the solar rays (also related to the name of his shaktI मारीची). Another aspect made explicit in the version found in the हरिवंश is his wrathful nature with fiery red eyes. This aspect is particularly emphasized in his nAstika manifestation.

Both his wrathful aspect as well as that of removing ignorance aspect are clearly present in the early चर्या tantra-s of the nAstika-s which appears to have been lost in their Sanskrit original. Some examples of texts translated by the चीनाचार्य-s, like Yixing, might be considered in this regard. The first is a translation of a massive collection of साधन-s by आचार्य atigupta for instructing the chIna-s known as the dhAraNI संग्रह made in 653 CE. The 6th chapter of atigupta's dhAranI is largely devoted to हयग्रीव साधन-s. Here a mantra of हयग्रीव is given which might be reconstructed in Sanskrit as:\
[नमो ऽद्विद्या-भक्षकाय ग्रस ग्रस वप वप स्फुट स्फुट ग्रस हयग्रीवाय स्वाहा । महाबलाय सर्व-ज्ञान-नेत्रे स्वाहा ।]{style="color:#0000ff;"}

The last part of this chapter has a दीक्ष vidhi with the हयग्रीव मण्डल, where he is identified with the bodhisattva avalokiteshvara. A study of this मण्डल suggesting that is bauddha appropriation of an Astika दीक्ष ritual. In the middle of the मण्डल an image of हयग्रीव is place. In the eastern quarter one of the एकादशमुख avalokiteshvara is placed. In the northern quarter the image of the अष्टभुज अमोघपाश is placed. In the south the the 8 नागराज-s are paced. The साधक should enter the bodhi मण्डल from the \[western side] and perform पूजा for हयग्रीव concentrating on avalokiteshvara. Then in the course of the पूजा the bodhisattva-s with siddhi-s will manifest. Another set of Chinese translations of mantra texts from the चर्या layer identified by the Dutch scholar van Gulik have mantra-s and rituals (Here we are not attempting to reconstruct them in completion but only provide the translations). One text is translated as the "Methods (उपाय) and rules (vidhi) for incantations (मन्त्राणि) and offerings (Ahuti) to effect the manifestation (आविष्करण ) of the great fierce king, the holy हयग्रीव". This तान्त्रिक text is of some importance to our current study because it provides background for the Bön rituals for हयग्रीव. It has the mantra:\
"[हयग्रीवाय स्वाहा || Able to destroy all the obstacles of mAra! It is through the उपाय of करुण that he yet manifests a krodha रूप.]{style="color:#008000;"}" \[translated from the Tibetan]

These are paralleled by the nAstika हयग्रीव साधन in the Gilgit manuscripts wherein वडवामुख हयग्रीव is invoked in an idol made of sandalwood at whose base is placed the triad of वज्रपाणि, lokeshvara and avalokiteshvara. The use of the term वडवामुख in this text offers a link to the descriptions of हयग्रीव in the महाभारत:\
[अहं हयग्रीवो भूत्वा समुद्रे पश्cइमोत्तरे ।\
पिबामि सुहुतं हव्यं कव्यञ् च श्रद्धयान्वितं ॥]{style="color:#0000ff;"}Mbh. 12.340

As also the tale in which the wrath of विष्णु made the ocean salty:\
[नारायणो लोकहितार्थं वडवामुखो नाम पुरा महर्षिर् बभूव।]{style="color:#0000ff;"} Mbh 12.342.60ab

In the Gilgit manuscript the mantra of हयग्रीव is stated as destroying rival अभिचारिक mantra-s (para-विद्या संभक्षण) and has the phrases such as "[खादखाद पर-मन्त्रं... पर-मन्त्र विनाशक... तन् सर्वन् वडवामुखेन निकृन्तय फट्]{style="color:#99cc00;"}" etc. This हृदय mantra is said to have been expounded by avalokiteshvara as is clear from the statement: "[तस्मै नमस्कृत्व इदम् आर्यावलोकितेश्वर मुखोद्गीर्णं हयग्रीव नाम परम हृदयम् आवर्तिष्यामि]{style="color:#0000ff;"}". As the great vaiShNava pointed out to us it might be noted that this statement is comparable to a mantra text for the worship of नीलकण्ठ (i.e. rudra) recorded by the Indian आचार्य vajrabodhi in the सिद्धं script in the चीनदेश: "[तस्मै नमस्कृत्व इदम् आर्यावलोकितेश्वर भषितं नीलकण्ठ नाम]{style="color:#0000ff;"}". Here again avalokiteshvara is described as the expounder of the हृदय for the worship of नीलकण्ठ. This, suggests that several astika devata-s were being incorporated into the ताथगत-mata as their mantra-s now have the sanction of being expounded by the bodhisattva himself. Subsequently, they were identified with the bodhisattva himself.

Together, these suggest that the worship of हयग्रीव was very prominent in the period around 500-700 CE in India. The nAstika-s adopted and transmitted हयग्रीव along with several several other deities, like नीलकण्ठ rudra, as suggested by the Gilgit and Chinese texts. It is possible that some of the तान्त्रिक material of the Bön texts derive from such transmissions of the साधन of Hindu deities via the medium of the ताथगथ-s.

Now let us take a brief look at the Bön texts themselves. In the hagiography of Tönpa Shenrab we find a detailed account of how he taught the साधन of हयग्रीव at काइलास parvata, to help साधक-s ward of vighna-s that were difficult to conquer. He did this by reciting the मूल-mantra of हयग्रीव \[Footnote 1] and emanated a nirmANakAya as flaming red हयग्रीव in the प्रत्यालीड pose with two hands and holding a blazing sword. He had the green horse-head and was ornamented in gold and was flying in the air emitting rays like those of the sun (a trait reminiscent of the earliest accounts of हयग्रीव found in the bhArata) that subjugate hostile beings. He then emanates hosts of भूत-s, मातृ-s, यक्ष-s and गण-s. The main Bön text compendium that deals with हयग्रीव (including the above mentioned burning stone ritual) come as a triadic package that combines his साधन-s with those of वज्रपानि and garuDa. This compendium includes rituals for removal of poisons in ritual क्षेत्र-s, individual rites to the above triad of देवता-s, followed by a rite to sipe ग्याल्मो, the primary shakti of the Bön system \[Footnote 2]. Regarding the burning stone rite these texts offer the following narrative. To benefit साधक-s in the kaliyuga, the great yogin पद्माकर went from the Kham region of Tibet to the region near bhArata to subjugate the ghosts known as srin living in the श्मशान-s. There he encountered a terrible lord of the ghosts known as the one-eyed Hadha. He took the form of a nine-headed boar and had a nine-headed rAkShasa on his back. Wandering around he spread greed and lust from his noses and mouths, madness and poison from his eyes, and warfare from his heads. But पद्माकर invoked the mantra-s हयग्रीव, वज्रपानि and garuDa in succession who completely subjugated this ghost. The ritual which is instituted in light of this event was the burning stone ritual narrated above in which all the three देवता-s are invoked in the actual ritual.

A striking feature of this ritual is the minimal involvement of bauddha elements and the invocation of these deities by themselves. This reminds one of the early texts in which the boddhisattva avalokiteshvara is an expounder of mantra-s to the old deities. We suggest that the early layer of the Bön system involved transmission of these elements of Astika ritual perhaps via the medium to the nAstika teachers who presented them as a being expounded by the boddhisattva-s. However, the striking तान्त्रिक stotra to sipe ग्याल्मो \[footnote 2] and her tremendous prominence in the Bön world raises the possibility that there might have been a direct transmission by Astika-s that has now been forgotten and bauddhaized at a later point. If this were the case, then the counter-illness and poison rituals might suggest a connection to the shaiva भूत-srotas in which हयग्रीव was a deity and the garuDa-srotas. Indeed the भूत-tantras and garuDa-tantra-s have tended to have a certain association with each other as suggested by later shaiva texts of these systems. Then could have been transmitted as a package, and the वज्रपानि could very well be indra who was prominent in the early gAruDa system. As circumstantial support for this, one might consider the Tibetan रामायण which appears to be largely independent in its transmission of the bauddha version and appears early in the Dunhuang texts, which was temporally closer to the apparent period of transmission to the Bön.\
...........................\
Footnote 1: From the Bön text this mantra appears to be:\
[व्सौः ॐ वज्र रक्ष क्रोध रव रव हयग्रीव हुं फट् ॥]{style="color:#0000ff;"}\
It appears to be a nAstika distortion of the mantra found in shaiva and vaiShNava तान्त्रिक contexts:\
[ॐ श्रीं ह्लौं ॐ नमो भगवते हयग्रीवाय रव रव रक्ष रक्ष हुं फट् स्वाहा ॥]{style="color:#0000ff;"}

Footnote 2: [sipe ग्याल्मो](https://manasataramgini.wordpress.com/2011/07/06/some-notes-on-the-goddess-sipe-gyalmo/) is a very interesting deity who has been poorly understood. Superficially her iconography with the ashvatara वाहन is reminiscent of the iconography of श्री देवी (Palden Lhamo) among the Tibetans. However, the origin of sipe ग्याल्मो is very distinct from श्री देवी and she appears to have influenced the later depictions of श्री देवी resulting the convergence observed today. The Bön compendium mentioned above records a remarkable stotra to sipe ग्याल्मो translated from a now lost tantra. This stotra has little bauddha about it, and appears to be directly derived from a shAkta-shaiva तान्त्रिक system. A key feature in the iconography of this great Bön deity is her relationship to the त्रिविद्या of the पश्चिमाम्नाय, which presents a combination of the shakti-त्रयं. But in the पश्चिमाम्नाय the three faces stand for अपरा, परापरा and parA; however, in the case of sipe ग्याल्मो the faces are depicted in the order of the three goddess of the पूर्वाम्नाय (trika) parA, परापरा and अपरा. It is not entirely surprising that the bauddha-s have made some attempts to demonize her.


