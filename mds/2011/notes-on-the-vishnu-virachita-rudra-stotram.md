
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Notes on the विष्णु-virachita rudra stotram](https://manasataramgini.wordpress.com/2011/04/28/notes-on-the-vishnu-virachita-rudra-stotram/){rel="bookmark"} {#notes-on-the-वषण-virachita-rudra-stotram .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 28, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/04/28/notes-on-the-vishnu-virachita-rudra-stotram/ "7:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The early history of the imagery of rudra within the Indo-Aryan tradition (as an ortholog of the deities elsewhere in the Indo-European world, like Apollo and Odin) can be traced through a large body of texts. These include in approximate chronological order:

 1.  The earliest layer in the ऋग्वेद -- RV 1.43; 1.114; 2.33 and 6.74 among others.

 2.  The नीलरुद्र and atharva-rudra सूक्त-s of the AV-P and and AV-vulgate respectively.

 3.  The शतरूद्रीय and the त्र्यंबक homa mantra-s of the yajur-veda saMhitA-s.

 4.  The व्रात्य rudra सूक्त (AV-vulgate 15.5).

 5.  The शूलगव mantra-s/कौशीतकी शूल्गव section.

 6.  The महानारायणोपनिषत्.

 7.  The श्वेताशवतरोपनिषत्.

 8.  The atharvashiras.

 9.  The AV परिशिष्ठ on the पाशुपत vrata.

 10.  The महाभरत -- the अजानन-virachita rudra stotra.

 11.  The हरिवंश -- विष्णु-virachita rudra stotra.

These offer an opportunity to examine the continuity and evolution in the emergence of the imagery of rudra. We shall now briefly examine the last of the above-mentioned texts. In several ways it is related to the अजानन-virachita rudra stotra attributed to दक्ष after he was revived with the head of a goat by rudra. It is found in the 87th chapter of the 3rd parvan of the vulgate text of the हरिवंश and is supposed to have been composed by विष्णु to praise rudra. The entire stotra is given below:

[नमस्ते शितिकण्ठाय नीलग्रीवाय वेधसे ।]{style="color:#0000ff;"}\
[नमस्ते शोचिषे अस्तु नमस्ते उपवासिने ॥३-८७-१३]{style="color:#0000ff;"}\
[नमस्ते मीढुषे अस्तु नमस्ते गदिने हर ।]{style="color:#0000ff;"}\
[नमस्ते विश्वतनवे वृषाय वृषरूपिणे ॥३-८७-१४]{style="color:#0000ff;"}\
[अमूर्ताय छ देवाय नमस्ते।अस्तु पिनाकिने ।]{style="color:#0000ff;"}\
[नमः कुब्जाय कूपाय शिवाय शिवरूपिणे ॥३-८७-१५]{style="color:#0000ff;"}\
[नमस् तुष्टाय तुण्डाय नमस् तुटितुटाय च ।]{style="color:#0000ff;"}\
[नमः शिवाय शान्ताय गिरिशाय च ते नमः ॥३-८७-१६]{style="color:#0000ff;"}\
[नमो हराय विप्राय नमो हरिहराय छ ।]{style="color:#0000ff;"}\
[नमो।अघोराय घोराय घोर-घोर-तराय छ ॥३-८७-१७]{style="color:#0000ff;"}\
[नमो।अघण्टाय घण्टाय नमो घटिघटाय छ ।]{style="color:#0000ff;"}\
[नमः शिवाय शान्ताय गिरिशाय च ते नमः ॥३-८७-१८]{style="color:#0000ff;"}\
[नमो विरूपरूपाय पुराय पुरहारिणे ।]{style="color:#0000ff;"}\
[नम आद्याय बीजाय शुचयेऽष्टस्वरूपिणे ॥३-८७-१९]{style="color:#0000ff;"}\
[नमः पिनाकहस्ताय नमः शूलासिधारिणे ।]{style="color:#0000ff;"}\
[नमः खट्वाङ्ग-हस्ताय नमस्ते कृत्तिवाससे ॥३-८७-२०]{style="color:#0000ff;"}\
[नमस्ते देवदेवाय नम आकाश-मूर्तये ।]{style="color:#0000ff;"}\
[हराय हरि-रूपाय नमस्ते तिग्मतेजसे ॥३-८७-२१]{style="color:#0000ff;"}\
[भक्तप्रियाय भक्ताय भक्तानां वरदायिने ।]{style="color:#0000ff;"}\
[नमो।अभ्रमूर्तये देव जगन्-मूर्तिधराय छ ॥३-८७-२२]{style="color:#0000ff;"}\
[नमश्-चन्द्राय देवाय सूर्याय च नमो नमः ।]{style="color:#0000ff;"}\
[नमः प्रधान-देवाय भूतानां-पतये नमः ॥३-८७-२३]{style="color:#0000ff;"}\
[करालाय च मुण्डाय विकृताय कपर्दिने ।]{style="color:#0000ff;"}\
[अजाय छ नमस्तुभ्यं भूतभावनभावन ॥३-८७-२४]{style="color:#0000ff;"}\
[नमोऽस्तु हरिकेशाय पिङ्गलाय नमो नमः ।]{style="color:#0000ff;"}\
[नमस्तेऽभीषुहस्ताय भीरुभीरुहराय च ॥३-८७-२५]{style="color:#0000ff;"}\
[हराय भीतिरूपाय घोराणां-भीतिदायिने ।]{style="color:#0000ff;"}\
[नमो दक्ष-मखघ्नाय भग-नेत्रापहारिणे ॥३-८७-२६]{style="color:#0000ff;"}\
[उमापते नमस्तुभ्यं कैलास-निलयाय छ ।]{style="color:#0000ff;"}\
[आदिदेवाय देवाय भवाय भवरूपिणे ॥३-८७-२७]{style="color:#0000ff;"}\
[नमः कपालहस्ताय नमो।अज-मथनाय छ ।]{style="color:#0000ff;"}\
[त्र्यम्बकाय नमस्तुभ्यं त्र्यक्षाय च शिवाय च ॥३-८७-२८]{style="color:#0000ff;"}\
[वरदाय वरेण्याय नमस्ते चन्द्रशेखर ।]{style="color:#0000ff;"}\
[नम इध्माय हविषे ध्रुवाय च कृशाय च ॥३-८७-२९]{style="color:#0000ff;"}\
[नमस्ते शक्तियुक्ताय नागपाशप्रियाय च ।]{style="color:#0000ff;"}\
[विरूपाय सुरूपाय मद्य-पान-प्रियाय छ ॥३-८७-३०]{style="color:#0000ff;"}\
[श्मशान-रतये नित्यं जय-शब्द-प्रियाय च ।]{style="color:#0000ff;"}\
[खर-प्रियाय खर्वाय खराय खर-रूपिणे ॥३-८७-३१]{style="color:#0000ff;"}\
[भद्र-प्रियाय भद्राय भद्र-रूप-धराय छ ।]{style="color:#0000ff;"}\
[विरूपाय सुरूपाय महाघोराय ते नमः ॥३-८७-३२]{style="color:#0000ff;"}\
[घण्टाय घण्टभूषाय घण्ट-भूषण-भूषिणे ।]{style="color:#0000ff;"}\
[तीव्राय तीव्र-रूपाय तीव्र-रूप-प्रियाय छ ॥३-८७-३३]{style="color:#0000ff;"}\
[नग्नाय नग्न-रूपाय नग्न-रूप-प्रियाय छ ।]{style="color:#0000ff;"}\
[भूतावास नमस्तुभ्यं सर्वावास नमो नमः ॥३-८७-३४]{style="color:#0000ff;"}\
[नमः सर्वात्मने तुभ्यम् नमस्ते भूतिदायक ।]{style="color:#0000ff;"}\
[नमस्ते वामदेवाय महादेवाय ते नमः ॥३-८७-३५]{style="color:#0000ff;"}\
[का नु वाक्-स्तुति-रूपा ते को नु स्तोतुं प्रशक्नुयात् ।]{style="color:#0000ff;"}\
[कस्य वा स्फुरते जिह्वा स्तुतौ स्तुतिमतां वर ॥३-८७-३६]{style="color:#0000ff;"}

[क्षमस्व भगवन्-देव भक्तोऽहं त्राहि मां हर ।]{style="color:#0000ff;"}\
[सर्वात्मन्-सर्वभूतेश त्राहि मां सततम् हर॥३-८७-३७]{style="color:#0000ff;"}\
[रक्ष देव जगन्नाथ लोकान् सर्वात्मना हर ।]{style="color:#0000ff;"}\
[त्राहि भक्तान् सदा देव भक्तप्रिय सदा हर ॥३-८७-३८]{style="color:#0000ff;"}

The attributes that can be traced to earlier texts are:\
शितिकण्ठ (the fair-throated one): YV saMhitA\
नीलग्रीव (the blue-throated one): YV saMhitA; AV-P\
मीढुषः (bountiful): RV saMhitA; YV saMhitA; Apastamba mantra-prashna\
वृष (the bull): RV saMhitA\
deva (the god): RV saMhitA; AV-vulgate; ताण्ड्य ब्राह्मण\
पिनाकिन् (the holder of the पिनाक bow): AV-vul; AV-P; YV saMhitA; AV-परिशिष्ठ\
shiva (the auspicious one): RV saMhitA; YV saMhitA; AV-vul; AV-P; श्वेताशवतर\
girisha (mountain rover): YV; AV-P\
hara (destroyer): मैत्रायणीय saMhitA; श्वेताशवतर; आश्वलायन गृह्यसूत्र; बोधायन mantraprashna; आपस्तंब mantraprashna\
aghora (benign): YV-मैत्रायणीय; महानारायण; AV-परिशिष्ठ\
ghora (terrible): YV-मैत्रायणीय; महानारायण; AV-परिशिष्ठ\
ghoratara (most terrible): YV-मैत्रायणीय; महानारायण; AV-परिशिष्ठ\
विरूप (many formed): YV-saMhita\
पुरहारिण् (destroyer of the 3 cities): as in त्रिपुरान्तक ब्राह्मण, i.e., YV-तैत्तिरीय ब्राह्मण\
अष्टस्वरूपिन् (8 formed): The 8 names come in the शूलगव mantra, e.g. Apastamba mantraprashna; कौशीतकी ब्राह्मण; shatapata ब्राह्मण\
पिनाकहस्त (holder of the पिनाक): YV-saMhitA\
कृत्तिवासस् (clad in hides): YV-saMhitA\
भूतानांपति (lord of the beings): as भूतपति in AV-Vulgate; AV-P; aitareya-ब्राह्मण\
muNDa (clean shaven): AV-परिशिष्ठ; as in vyuptakesha in YV-saMhitA\
kapardin (with knotted locks): RV-saMhitA; YV-saMhitA; आश्वलायन गृह्यसूत्र; AV-परिशिष्ठ\
aja (unborn): श्वेताशवतर\
harikesha (green/yellow haired): YV-saMhitA\
पिङ्गल (tawny): महानारायणोपनिषत्; AV-परिशिष्ठ (e.g. उच्छुष्म and गोशान्ति)\
दक्ष-makhaghna (destroyer of दक्ष's ritual): alluded in gopatha-ब्राह्मण as destroyer of prajApati's याग and AV-परिशिष्ठ in graha-संग्रह\
bhaga-नेत्रापहारिन् (destroyer of bhaga's eyes): gopatha-ब्राह्मण\
उमापति (husband of उमा): महानारायणोपनिषत्\
bhava (all existence): RV-saMhitA; RV-khila; AV-vul; AV-P; YV saMhitA; Apastamba mantra-prashna; कौशीतकी ब्राह्मण; shatapata ब्राह्मण; AV-परिशिष्ठ\
कपालहस्त (skull-wielder): as in कपालिन् AV-परिशिष्ठ\
tryambaka (the three eyed one): RV-saMhitA; YV-saMhitA; YV-ब्राह्मण-s; gopatha ब्राह्मण; AV-परिशिष्ठ\
dhruva (firm): श्वेताशवतर\
khara-priya/khara-रूपिन् (with donkeys): equivalent of the gardabhau mentioned in the AV-P\
वामदेव (the beautiful god): महानारायणोपनिषत्\
महादेव (the great god): AV-vul; AV-P; YV saMhitA; कौशीतकी ब्राह्मण; AV-परिशिष्ठ

The above analysis shows that the majority of epithets of rudra in this stuti are of Vedic provenance and cover the entire temporal spectrum of Vedic development all the way from the RV to the late उपनिषत् and परिशिष्ठ texts. However, the stuti shows some developments that have no apparent precedence in much of the surviving Vedic corpus:

 1.  rudra as the bearer of the शूल. Right in the RV, rudra is repeatedly described as तिग्मायुध or tigmaheti, i.e., one with a sharp weapon (may be a cognate of Odin's spear). However, nowhere is this weapon specified as being a शूल or a त्रिशूल. The word शूल itself occurs in the RV and means a stake, such as that used to roast sacrificial meat in the ashvamedha ritual (could be a doubtful cognate of greek Xulon). In the शूलगव ritual described in the गृह्य sUtra-s the term again is for the sacrificial stake. But as the name for the sharp weapon of rudra it becomes apparent only in the epic. This points to a semantic expansion of the word शूल to include a sharp weapon. With this expansion, it appears to have taken the place of the तिग्मायुध of the veda, perhaps via its use in the शूलगव of rudra. However, it should be noted that the vajra or the thunder-bolt is regarded as a weapon of rudra right from the RV itself. Similarly, tridents are archaeologically attested in depictions on Indus and Mesopotamian sites that overlap temporally with the Vedic period. The bronze age deities of the Middle East are routinely shown with tridents: e.g. Enlil. In many of these bronze age Eurasian icons the trident held by a deity actually stands for the thunder-bolt, which is an equivalent of the vajra (e.g. the iconography of the Hittite Tarhun=Teshub). Hence, it is possible that the त्रिशूल in the classical Agama/पुराण iconography of rudra is a conflation of the original vajra, which was historically shown as three-pronged, and the तिग्मायुध, which had come to be known as the शूल (as in the above stuti).


 2.  The खट्वाङ्ग/कपाल. The skull as an attribute of rudra is not mentioned in the core saMhitA-s of the shruti and only finds mention in the AV-परिशिष्ठ. Now, the खट्वाङ्ग or the skull-topped brand as an attribute of rudra is clearly mentioned for the first time in the texts of great epic layer, for example, as seen in this stuti. Apastamba states in his dharmasUtra (APS 1.28-29) that a murderer who has committed the great sin of killing a ब्राह्मण should use a कपाल as a bowl and carry a खट्वाङ्ग. Similar gautama states that a ब्राह्मण killer should live a celibate life for twelve years carrying a कपाल and a खट्वाङ्ग and proclaiming his sin while begging for food (GDS 22.4). A similar statement is found in the बौधायन dharmasUtra (BDS 2.1.3.1) along with the further injunction that the sinner lives in the cremation ground. The tale of rudra killing prajApati is seen in the RV itself and is repeatedly described in the ब्राह्मण-s. This act might have associated rudra, even in the vedic period, with ब्रह्महत्या, as it is done in the पुराण-s. This appears to have been the precedence for the transference of the brahmaghna's attributes to rudra. The celebration of the skull as a "positive" attribute of rudra rather than as a mark of his "dark" side emerges in the पुराण-s like the skanda पुरण (vulgate). In the अवन्तिखाण्ड there is tale of how कपाल-s repeatedly appear in the middle of the वेदी when the ब्राह्मण-s are performing a ritual to rudra. The ब्राह्मण-s place these skulls outside the यज्ञशाला each time one appears as they were thought to be desecrating the fire. This resulted in a pyramid of skulls accumulating outside the यज्ञशाला in a particular spot. The ब्राह्मण-s later discovered that these skulls were marking the site of the great लिङ्ग of महाकाल.


 3.  The cremation ground dweller/delighter, श्मशान-rati. In the पारस्कार गृह्यसूत्र (3.15) it is declared that one should invoke rudra with the initial ऋक्-s of the शतरुद्रीय, especially when one is passing by cross roads or a cremation ground. This suggests that there was an ancient link between rudra and the cremation grounds, among other wild places, which was more explicitly developed in the texts of the epic layer.


 4.  chandrashekhara. Right from the veda, rudra was identified with the sun and the moon. Thus, in the rudra recitation of the kaushitaki ब्राह्मण we have: "[यन् महान् देव आदित्यस् तेन ।]{style="color:#0000ff;"}" and "[यद् रुद्रश् चन्द्रमास् तेन ।]{style="color:#0000ff;"}". This is reproduced in the above stuti in: "[नमश्-चन्द्राय देवाय सूर्याय च नमो नमः ।]{style="color:#0000ff;"}". However, in the texts associated with the great epic we have for the first time an iconographic regarding rudra as the bearer of the lunar digit. A lunar association of rudra might be concealed under the vaidika dvandva "सोमारुद्रा". Also related to this link is the fact that in the Eurasian bronze age we find several horned deities. At least in Mesopotamia the bovine horns are taken to represent the crescent moon (e.g. on Nannar or Marduk's headgear). Hence, it is possible that the lunar crest of rudra is a more direct representation of the horns that are so common in the iconography of bronze age Eurasian deities.


 5.  The lover of liquors, madya-पान-priya. The use of psycho-active substances by rudra and his followers is not unknown in the veda itself. In the famous सूक्त of the keshin-s (RV 10.136) the keshin drinks the विष prepared from the कुनन्नमा seeds. While the above stuti explicitly associates liquor with rudra, we can trace the beginning of this association to a special ritual action in the preparation of the beer offered in the Vedic सौत्रमणी ritual. The shatapatha ब्राह्मण states (12.7.3.20) that the powdered hair of the lion, tiger and wolf are mixed into the beer because if he mixed the powder into the milk cups he would offer his cattle to rudra pashupati. The liquor is associated with rudra because it makes one "raudra" upon drinking. By putting the hair powder in the beer the ritualist unites the animals which belongs to rudra with him. Thus, pashupati only goes with the wild animals and spares the यजमान's domestic animals. In any case the explicit mention in this stuti is an important point because it marks a development in the direction of worship that was to become typical of the vama and bhairava srotas and their kaula evolutes.


 6.  kubja, the crooked one. This appellation is striking because it was to reappear much later in the tradition of the कुब्जिका-mata,where rudra is called कुब्जीशन. At first sight this might seem convergent and based on the name of the shakti कुब्जिका. But we strongly suspect this is not a coincidence. In fact we suspect that the name has a long history that goes back to the earliest Vedic layers. In RV 1.114 he is called वङ्कु that has a meaning similar to kubja. Thus, kubja in the context of rudra and his shakti कुब्जिका in the mantra-मार्ग appear to have descended from this old epithet.

The historical importance of this stuti is in establishing the relative era when the classical iconography of rudra became prevalent and also providing evidence for the emergence of certain attributes that were magnified in the non-सैद्धान्तिक स्रोतांसि. The सैद्धान्तिक srotas emphasizes the benign facets, and "clean" of rudra (now transcending his previous rudra forms as the supreme deity of the Urdhvasrotas, सदाशिव). The other स्रोतांसि emphasize the terrible, the ferocious and "unclean" aspects of rudra in different combinations and degrees or all of them together. There is hardly any doubt that in the veda the dread of rudra is repeatedly alluded to. To consider a few examples:


 1.  In the agnihotra ritual the hotar holds out the sruk two times to the north. With this he pleases rudra in his own direction (north) and rudra goes away pleased. If the यजमान were to stand to the north when the offering is being made he comes in the way of the fierce rudra and could be seized by him (kaushitaki ब्राह्मण 2.2, also mentioned in the kaTha and मैत्रायणीय saMhitA-s, gopatha ब्राह्मण of the AV tradition, and in the शाङ्खायन ब्राह्मण the northward offering is combined for rudra with Ursa Major). The kaTha and मैत्रायनीय texts explain that during this agnihotra offering rudra must be implored for mercy using the mantra-s that contain the special names of rudra, अनाभु (ruthless; अनार्भव among the North Indian kaTha-s) and धूर्त (roguish; subsequently this धूर्त mantra is transferred to कुमार). These names are said to be dreadful manifestations of rudra that destroy animals. By using these names he pleases rudra and pacifies him with respect to the यजमान. At the same time he destroys or drives away those who illegitimately partake of the offerings and harm his ritual.


 2.  In the tradition of the kaTha-s and the मैत्रायणीय-s a special agnihotra of 12 days is offered if rudra seizes his animals. In this ritual as per the kaTha-s he makes a recitation of an offering mantra where he replaces the name rudra with जातवेदस् -- an euphemism to avoid the dread of rudra. In the KS and MS it is mentioned that rudra smears the plants with poison and so the cattle are unable to eat it. The poison is removed by prajApati via the action of agni. This is given as reason for the samidh offered to prajApati, and at the same time evokes the dread to rudra.


 3.  The tradition of the aitareya ब्राह्मण the hotar introduces peculiar विकृति-s to the ऋक् RV 2.33.1 so that the fury of rudra does not fall on the यजमान:\
In place of the actual mantra he recites:\
[आ ते पितर् मरुतां सुम्नम् एतु मा नः सूर्यस्य सन्दृषो युयोथाः ।]{style="color:#0000ff;"}\
[त्वं नो वीरो अर्वति क्षमेत प्र जायेमहि रुद्रिय प्रजाभिः ॥]{style="color:#0000ff;"}\
This ब्राह्मण tradition holds that by using "त्वं नः" in place of the original "abhi नः" rudra is not directed toward the यजमान's family and cattle. By using rudriya instead of rudra he reduces the terror arising from the mention of the real name of rudra, who has just been described in the ब्राह्मण as having slain prajApati. Alternatively the hotar might entirely drop this mantra and substitute it with the gAyatrI to rudra composed by gotamo राहूगण (RV 1.43.6):\
[शं नः करत्यर्वते सुगं मेषाय मेष्ये । नृभ्यो नारिभ्यो गवे ॥]{style="color:#0000ff;"}\
The ब्राह्मण explains this substitution by indicating that this mantra does not mention rudra by name and thereby averts the terror arising from the mention of his name. Secondly, it begins with the positive word shaM, which indicates auspiciousness.

From these points a paradoxical point of note emerges: On one hand, the veda, while stressing the dread of rudra, avoids any detailed description of the dreadful and unclean forms of rudra and tries to primarily describe his benign facets. The tendency is amplified in the tradition outlined in the aitareya in the substitution followed in the rudra mantra. This is in keeping with the general tendency in the vedic ritual where there is a strong tendency to avoid the mention of dreadful aspects of rudra in ones vicinity. On the other hand, there appears to be a countercurrent (e.g. the agnihotra mantra-s of the KS and MS) to specifically mention the dreadful names of rudra as a means of appeasing his dreadful aspects (also seen to certain degree in the शतरुद्रीय of the YV saMhitA-s). This appears to represent an ancestral paradox in the rituals to rudra, a dimorphism that appears to have continued in course of the development of the sectarian shaiva system and Hindu [literature](https://manasataramgini.wordpress.com/2010/04/11/the-richness-of-meaning/). In the early layers of the shiva शासन, there was an emphasis on the benign face of rudra (e.g. his name shiva) and a stress on ritual purity in his worship. This trend is dominant in the श्वेताश्वतर, atharvashiras and the system of the पाशुपत vrata expounded in the AV परिशिष्ठ-s and the पाशुपत sUtra-s. It was this tendency that continued with the lakula-s and their कालामुख successors and eventually came to be the mainstay of the siddhAnta tantra-s of the mantra मार्ग with सदाशिव as their central देवता. The above stuti suggests that there was probably a counter-current wherein the emphasis was on the terrible and the unclean aspects of rudra (the stuti gives "both sides of the coin") even within the early shiva शासन. This is encapsulated in the names like कपालिन्, खट्वाङ्गिन्, श्मशान-rati, madya-पान-priya, nagna and making of frightening noises: घटिघट or तुटितुट (compare with names of early भूत tantra-s of the pashchima srotas: हाहाकारं, शिवारवं, घोराट्टहासं and घुर्घुरं). This led to the कापालिक tradition of the अतिमार्ग that was a mirror of the more purity emphasizing पाशुपत-s (that is the version of the अतिमार्ग based on the सोमसिद्धान्त texts).


