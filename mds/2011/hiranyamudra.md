
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [हिरण्यमुद्रा](https://manasataramgini.wordpress.com/2011/10/29/hiranyamudra/){rel="bookmark"} {#हरणयमदर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 29, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/10/29/hiranyamudra/ "6:05 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-9VDC2aTMjb0/TpnbJy4s1SI/AAAAAAAACQk/t53JHFUdiDM/s400/contextfree3.jpg){width="75%"}
```{=latex}
\end{center}
```



