
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [भवभूति's avifauna and flora](https://manasataramgini.wordpress.com/2011/11/02/bhavabhutis-avifauna-and-flora/){rel="bookmark"} {#भवभतs-avifauna-and-flora .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 2, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/11/02/bhavabhutis-avifauna-and-flora/ "7:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A good kavI of yore is supposed to have been a naturalist. We had earlier given an example of this in the form of वाक्पति's [characterization of the kingfisher](https://manasataramgini.wordpress.com/2010/07/30/the-kingfisher/). Now we shall provide an example from the collection of भवभूति of vidharbha, one the greatest poets of all times. Like वाक्पति he was also in the court of the neo-mauryan ruler yashovarman who was overwhelmed by the great Kashmirian conqueror ललितादित्य.

[काश्मर्याः कृतमालम् उद्गतदलं कोयष्टिकष् टीकते]{style="color:#0000ff;"}\
[नीराश्मन्तक शिम्बि चुम्बन-मुखा धावन्त्य् अपः पूर्णिकाः ।]{style="color:#0000ff;"}\
[दात्यूहैस् तिनिशस्य कोटरवति स्कन्धे निलीय स्थितं\
वीरुन्-नीड-कपोत-कूजितम् अनुक्रन्दन्त्य् अधः कुक्कुभाः ॥]{style="color:#0000ff;"}

काश्मर्याः=*Gmelina* trees; कृतमालम्=*Cassia* tree; udgata-दलं= spread-out leaves; कोयष्टिकष्=lapwing; टीकते=hops up; नीराश्मन्तक= river *Bauhinia*; shimbi=pod chumbana-मुखा= touching mouth; धावन्त्य्=run; अपः=water; पूर्णिकाः=hornbills ; दात्यूहैस्= purple moorhens; tinishasya= of *Dalbergia* tree; कोटरवति= with a hollow; skandhe= branch; निलीय=settles in; स्थितं= situated; वीरुन्-नीड= tree nesting; kapota=dove; कूजितम्=cooing; anukrandanty= cries; अधः=below; कुक्कुभाः= red jungle fowl.

Here we get a remarkable description of the diverse avifauna of India and some flora on a hot summer day. Any naturalist in India can identify with this scene painted by भवभूति:\
From the Gmelina trees the lapwing hops up \[to seek the shade of the] spread-out leaves of the Cassia tree; The hornbills having barely touched the river Bauhinia's pods with their beaks retreat to get water; A branch with hollow of the Dalbergia is made a residence by the purple moorhens; even as the red jungle fowl standing below answers the cooing of the doves nesting in the trees with its cries.

Thus, 5 species of birds and 4 species of trees are mentioned by भवभूति in this verse. As an aside one may note that the word नीड in the above verse is a cognate of Latin nIDus and nest in English -- a word of proto-Indo-European provenance.


