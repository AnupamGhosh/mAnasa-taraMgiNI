
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Questions which answer themselves](https://manasataramgini.wordpress.com/2011/08/31/questions-which-answer-themselves/){rel="bookmark"} {#questions-which-answer-themselves .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 31, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/08/31/questions-which-answer-themselves/ "6:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[कं संजघान कृष्णः ? कंसं जघान कृष्णः ।]{style="color:#0000ff;"}\
[का शीतलवाहिनी गङ्गा ? काशीतल-वाहिनी-गङ्गा ।]{style="color:#0000ff;"}\
[के दारपोष्णरताः ? केदार-पोष्ण-रताः ।]{style="color:#0000ff;"}\
[कं बल्वन्तं न बाधते शीतं ? कंबल्वन्तं न बाधते शीतं ॥]{style="color:#0000ff;"}\
Whom did कृष्ण kill? कंस.\
Which is the गङ्गा that flows smoothly? The गङ्गा flowing through the surface of kAshI.\
Who are those that delight in wife-suckling \[nourishing]? They who delight in nourishing their fields.\
Possessed of which strength is one not afflicted by cold? One possessed of a blanket is not afflicted by cold.

Sanskrit allows some unusual paronomasias that are not available in most other languages. Above are examples of such, where the question and the answer can be encoded in the same sentence with just a difference in the पदपाठ. The tradition of the पदपठ, which is an ancient Indo-Iranian tradition (lost early in the Iranian world), appears to have favored the emergence of such puns. The other श्लेष-s of संस्कृत appear to have their origins in the wide range of dvandva-s and hetero-semantic homonyms. Of these only the last are prominent in dominant IE language of the modern world i.e. that of our former conquerors from the nation of sea-going marauders.

Such literary structures are of great interest because several of them reoccur in various artistic and philosophical expressions which are widely scattered in space and time. For example, the concepts behind several of these Sanskritic figures of speech reappear several centuries or even millennia later in European art -- the creations of the Swedish artist Reutersvärd, the Dutch artist Escher and the English mathematicians, Roger and Lionel Penrose. It is precisely for this reason we singled out the above-provided expressions in Sanskrit. While we find parallels to several of the other Sanskritic figures of speech in artistic expressions like those of Escher, we are yet to find precise parallels to the questions which answer themselves in expressions elsewhere in the world.

Now let us see some parallels that do exist:\
[सर्वस्वं हर सर्वस्य त्वं भवच् छेदतत्परः ।]{style="color:#0000ff;"}\
[नयोपकार सांमुख्यम् आयासि तनुवर्तनम् ॥]{style="color:#0000ff;"} (मम्मट)\
A votary to rudra: O hara! You are the whole of all that exists, focused on cutting the bonds of subject-object existence. You have striven to attain that state of being, which presents a combination of assistance and guidance.\
A thief to his son: You must plunder everything, intending to break into everyone's treasure; deploy every device and counter-measure and you will overcome \[other] people's measures.

Here the duality of meaning is achieved at different levels: the word hara is either interpreted as an epithet of rudra or as a verb (imperative plunder). Similarly naya is interpreted as a noun (guidance) or as a verb (imperative deploy). The nouns bhava, संमुख्य is differentially interpreted via duality of meaning. The word tanuvartana is interpreted via different समास-s and different shades of the meaning of the verb आयासि are used in the two interpretations. We see this being comparable to an Escherian interpretation of space either using black or a white key in the below figure. Each set gives a totally different set of animals, yet, as in the above verse, both interpretations or meanings are there simultaneously.
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-oSpo13QmNdo/Tl8kPCNjw9I/AAAAAAAACMM/4ceoMFMUs9A/s400/escher_श्लेष.jpg){width="75%"}
```{=latex}
\end{center}
```



Now let us look at another device, which appear rather early in the history of संस्कृत, right in the veda itself.\
[अदितेर्-दक्षो-अजायत दक्षाद् वदितिः परि ॥ ऱ्‌V १०।७२।४c]{style="color:#0000ff;"}\
[अदितिर्-ह्य् अजनिष्ट दक्ष या दुहिता तव ।]{style="color:#0000ff;"} RV 10.72.5a\
दक्ष was born of aditi, and aditi was born of दक्ष. Indeed aditi brought forth, O दक्ष who is your daughter.\
This can be compared to what Douglas Hofstader calls a strange loop. He points to the presence of such paradoxical loops in various contexts ranging from the Greek pseudomenon or the liar's paradox, Bach's compositions, to the famous incompleteness theorem of Gödel. Indeed, Escher made good use of this in his art:\
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-YZQz6t50dws/TmHL_tjgMpI/AAAAAAAACMU/giSGcK39eSs/s400/escher_water_fall.jpg){width="75%"}
```{=latex}
\end{center}
```



The final parallel that we shall consider is the one offered by viloma काव्य:\
[श्री रामतो मध्यमतोदि येन धीरोऽनिशं वश्यवती वराद्वा ।]{style="color:#0000ff;"}\
[द्वारावती वश्यवशं निरोधी नयेदितो मध्यमतो ऽमरा श्रीः ॥]{style="color:#0000ff;"}

We may compare this with the below work of Escher:\
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-Ye6dfc6jBGY/TmHMEG8ODEI/AAAAAAAACMY/t6Ufm10Lbkw/s400/Escher_horsemen_viloma.jpg){width="75%"}
```{=latex}
\end{center}
```




