
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Further notes of the दोहा-s of सरहपाद](https://manasataramgini.wordpress.com/2011/09/08/further-notes-of-the-doha-s-of-sarahapada/){rel="bookmark"} {#further-notes-of-the-दह-s-of-सरहपद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 8, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/09/08/further-notes-of-the-doha-s-of-sarahapada/ "7:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

To [continue](https://manasataramgini.wordpress.com/2008/11/22/some-doha-s-of-mahabrahmana-sarahapada/):\
There are some interesting elements in the अपभ्रंश दोहा-s of saraha discovered by Haraprasada Shastri and Rahula Samkrityayana. These add to the enormously paradoxical nature of the दोहा-kosha of saraha, again pointing to the enormous debt of the ताथागत-s to the kaula systems emerging among the shaiva-s. Let us look at the following:\
[अक्खर-वण्णो परम-गुण-रहिओ]{style="color:#00ffff;"}\
[भणै ण जाणै ए मै कहिअओ ।]{style="color:#00ffff;"}\
[सो परमेसरु कासु कहिज्जै]{style="color:#00ffff;"}\
[सुरअ कुमारी जिम पडिवज्जै ॥]{style="color:#00ffff;"}

Bereft of sound, color, or the fundamental guNa-s, it cannot be spoken about or known, so I (i.e. saraha) say.\
How can the parameshvara be described? Like a girl's first experience of an orgasm.

Two terms are of note here: 1) This दोहा has a rather shaiva ring to it. It goes rather against all primary bauddha concepts to consider the entity beyond the guNa-s who cannot be described or known to be parameshvara. However, this parameshvara can be interpreted within the bauddha framework of a yogin, such as saraha, as representing a heruka deity such as बुद्धकपाल, hevajra or the चक्रसंवर. But the specific term parameshvara clearly indicates that this doha was acquired without much or any modification from a shaiva source. From iconographic and hagiographic sources its clear that सरहपाद, the euphemistic महाब्राह्मण, took on the emblems of the कापालिक type. We also know that the bauddha yogin-s frequented the same great श्मशान-s and पीठ-s as those used by the shaiva practitioners of the पाशुपत mata, the bhairava srotas, the kula-प्रक्रिया. It was probably in such क्षेत्र-s, the bauddha yogin-s acquired doha-s, along with elements of the चर्यागीति-s composed by kaula नाथ-s (e.g. the doha-s of the kaula नाथ-s collected by H.P. Dwivedi). 2) The sexual allegory used in doha is suggestive of the performance of maithuna as per the shaiva kaula ritual. In the kaula ritual the pleasurable activities are aimed at satiating the deities residing in the organs with the ultimately non-dual consciousness being experienced via maithuna, where yogin and the दूती's consciousness are one.

[णौ अणु णौ परमाणु विछिन्तजे]{style="color:#00ffff;"}\
[अणवर अ भावहि फुरै सुरत्तजे ।]{style="color:#00ffff;"}\
[भणै सरह भन्ति एत विमत्तजे]{style="color:#00ffff;"}\
[अरे णिक्कोली वुज्झै परमत्थजे ॥]{style="color:#00ffff;"}

Don't think it's the atoms or the fundamental particles; it is the unending orgiastic delight that pervades existence.\
saraha says that such \[i.e. atomic] false thinking is madness; arrey ! low-born one understand the ultimate reality.

The alliterative effects of saraha are at high point in this दोहा. A point of interest is his attack on atomism, which in some ways resembles the attack on atomism by the advaita वेदान्तिन्-s. Who are the atomists he is attacking? The atomic ideas could come from nyAya, वैशेषिक or even सांख्य, but the view the particles are fundamental is central to the former two. Given the evidence that shaiva-s, especially those of the अतिमार्ग (e.g. the [कालामुख](https://manasataramgini.wordpress.com/2005/03/23/kalamukhas-ii/)) and to a degree the [सैद्धान्तिक-s](https://manasataramgini.wordpress.com/2008/08/02/a-pashupata-inscription-and-some-thought-on-the-history-of-nyaya-vaisheshika/) were followers of the atomic doctrine, we suspect that this attack is directed towards their theories of existence.

[पण्डिअ सअल सत्थ वक्खाणै]{style="color:#00ffff;"}\
[देहहिं बुद्ध वसन्त न जाणै ।]{style="color:#00ffff;"}\
[अवणागमन ण तेण विखण्डिअ]{style="color:#00ffff;"}\
[तोवि णिलज्ज भणै हौं पण्डिअ ॥]{style="color:#00ffff;"}\
The pundit expounds the entire शास्त्र not knowing that the buddha dwells within his body.\
\[The cycles of ] coming and going are not shattered by that means but he shamelessly says: "I am a pundit".

This is not an attack on the Astika ब्राह्मण paNDita-s but his own fellow nAstika paNDita-s who taught at the centers like नालन्दा and वरेन्द्री. In some ways this is reminiscent of a text termed the bhaja गोविन्दं composed by the शंकराद्वैतिन्-s of south India influenced by the दत्तात्रेयन् ascetic tradition. There the पणिनिअन् rules are condemned. The bhaja गोविन्दं is wrongly attributed to आदिशंकर, who unlike the spirit expressed in the text strongly emphasized scholarly tradition. The anti-scholastic tendencies rose among the siddha-s' compositions (like that of सरहपाद) and diffused among the various ascetic groups before eventually even infecting the medieval शंकराद्वैत tradition -- however, unlike saraha they expressed their anti-scholastic thoughts in elegant Sanskrit.Importantly, the very survival of these doha-s was due their incorporation into the scholarly tradition of the nAstika-s -- the doha-s of saraha are preserved as a part of a Sanskrit commentarial tradition represented by the दोहा-kosha-पञ्जिका. We also know that saraha himself contributed to the scholastic tradition via his commentaries on the योगिनी tantra-s such as the buddhakapala तन्त्रं. This is in line with the तान्त्रिक work of other दोहा composers, like कृष्णाचर्य's commentary on the hevajra tantra. This raises the question as to what is the relationship between the tantra commentators and the दोहा composers -- have they been synonymized even as शंकर was made the author of the bhaja गोविन्दं and the तान्त्रिक texts like the सौन्दर्यलहरी or the प्रपञ्चसार? The case of the nAstika आचार्य-s appears to be very different from that of the वेदान्ताचार्य -- the former do really belong inside the nAstika योगिनी tantra tradition. The anti-scholasticism of these आचार्य-s appears to reflect their expression after the attainment of the sahaja state in which the other devices seem empty.

[आवन्त ण दीस्सै जन्त णहि अच्छन्त ण मुणिऐ ।]{style="color:#00ffff;"}\
[णित्तरङ्ग परमेसुरु णिक्कलङ्क धाहिज्जै ॥]{style="color:#00ffff;"}\
You don't see it coming nor going, you don't know it when it is there;\
parameshvara is without waves, blemishless and \[as though] washed clean.

This is दोहा is notable again because of its use of the shaiva term parameshvara as in the one mentioned above. The interesting point here is the use of the term "णित्तरङ्ग", i.e. without waves -- this might be considered along side the idea expressed in the shaiva kaula systems that the body of shiva (the parameshvara) is the "sky of consciousness" in which like in the ocean the universe emerges and ends through the conjunction and disjunction of waves which are the shakti-s (this is clearly expounded in the विरूपाक्ष पञ्चाशिक). This दोहा instead seems to insist on the converse trying to present the parameshvara as being free from such waves.

[आवै जाइ ण च्छड्डै तावहु ।]{style="color:#00ffff;"}\
[कहिं अपुव्वा विलासिणि पावहु ॥]{style="color:#00ffff;"}\
If you do not renounce the coming and going how can you attain the incomparable विलासिनी ?

This verse is particular interesting because it mentions the attainment of विलासिनी. We suspect that this is not just a casual name for the योगिनी attained upon renunciation of the saMsAra cycle, but is likely to represent a specific reference to the erotic kaula deity [विलासिनी](https://manasataramgini.wordpress.com/2008/05/25/upasana-of-the-1112-vilasini-s/). The विलासिनी-kula system is closely shared by both the Astika-s and nAstika-s. In the nAstika world we encounter the goddess in the vajra-विलासिनी-stotra of the Acharya विभूतिचन्द्र who extensively transmitted his तान्त्रिक lore to the Tibetans after the destruction of the Indian universities by the Moslems. The ताथागत-s have also incorporated the worship of विलासिनी taught by the early kaula siddha shabara in a vajrified form in the guhya-vajra-विलासिनी साधन.


