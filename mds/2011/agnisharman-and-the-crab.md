
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [agnisharman and the crab](https://manasataramgini.wordpress.com/2011/05/27/agnisharman-and-the-crab/){rel="bookmark"} {#agnisharman-and-the-crab .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 27, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/05/27/agnisharman-and-the-crab/ "5:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

agnisharman was on a pilgrimage to the temple of revanta in the magadha country. He entered a forest full of refreshing foliage and then reached a pleasant lake in the middle of it. There he took a bath and having refreshed himself continued his journey after filling his कमण्डलु. Around noon he reached the dry bed of stream where a crab was stuck and was dying by drying out in the sun. Filled with pity, the ब्राह्मण put the crab in his कमण्डलु and proceeded. At sunset he lay down to sleep under a tree. In tree lived a crow couple and a cobra which who were great friends. The female crow who was ovulating asked her mate to pluck out agnisharman's eyes and give it to her. He said it was not possible to do it when the ब्राह्मण was still alive. The cobra who heard this offered help and said he would bite the ब्राह्मण and once he died the crows could eat his eyes. The snake uncoiled himself and proceeded to bite him. The female crow came down eagerly next to the ब्राह्मण's head. Perceiving what was happening the crab came out decided to fight on behalf of his benefactor and grabbed the crow. Hearing her shriek violently, the male crow asked the snake to desist and slide back. The ब्राह्मण woke and realized that the crab had save his life. So he delivered the crab in a large lake with good water and proceeded on his pilgrimage.


