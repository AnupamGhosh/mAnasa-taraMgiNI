
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some cameos from elementary Hindu education](https://manasataramgini.wordpress.com/2011/06/20/some-cameos-from-elementary-hindu-education/){rel="bookmark"} {#some-cameos-from-elementary-hindu-education .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 20, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/06/20/some-cameos-from-elementary-hindu-education/ "6:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

An ancient anonymous textbook from my grandparents' time used by my aunt for her education in the देवभाषा is one of the best introductory elementary textbooks on the language I have seen. It not only introduced the student to the Arya-वाक् but also inculcated a sense of Aryatva in them via introduction the dharma.\
Below is an abstract from the same that educating a child on homonymy:\
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-qdt_hhlkxqg/Tf7srT1nw6I/AAAAAAAACIA/_fBlI-w2oEQ/s400/umAshaMkara.JPG){width="75%"}
```{=latex}
\end{center}
```

\
[श\~करः द्वारम् अङ्गुल्या प्रहरति।\
उमा: अङ्गुल्या कः कपटं प्रहरति ?\
शिवः: अहम् अस्मि प्रिये ।\
उमा: कः त्वं असि ?\
शिवः: अहं शूली अस्मि ।\
उमा: किं तव शूलरोगः वर्तते? तेन वैद्यं मृगय ।\
शिवः: प्रिये, अहं नीलकण्ठोऽस्मि ।\
उमा: किं त्वं नीलकण्ठो मयूरो।असि ? तदा एकां केकां कुरु ।\
शिवः: प्रेयसि, अहं पशुपतिर् अस्मि ।\
उमा: यदि त्वं पशुपतिर् असि तदा त्वयि कथं विषाणे न पश्यामि ?\
शिवः: नाहं वृषभः । अहं स्थाणुर् अस्मि ।\
उमा: यदि त्वं स्थाणुर् अर्थात् तरुर् असि । अथ कथं वदसि ? तरुर् हि न वदति ।\
शिवः: हे प्रिये, अहं शिवायाः प्राणेश्वरोऽस्मि ।\
उमा: आः! यदि त्वं शृगाल्याः पतिर् असि, अरण्यं गच्छ ।\
एवं पार्वती शब्दच्छलेन शङ्करं पराजयत ॥]{style="color:#99cc00;"}

The original verse on this उमाशङ्कर क्रीडा is:\
[कस्त्वं ? शूली, मृगय भिषजं, नीलकण्ठः प्रियेऽहम् ।\
केकाम् एकां कुरु, पशुपतिर् नैव दृष्टे विषाणे ॥\
स्थाणुर् मुग्धे, न वदति तरुर् जीवितेशः शिवायाः ।\
गच्छाटव्याम् इति हतवचाः पातु वश्-चन्द्रचूडः ॥]{style="color:#99cc00;"}

\~*\~*\~*\~\
Recently my पिताश्री inquired about an elementary problem presented by one of the greatest mathematicians of our tradition, भास्कर, who graced the seuna यादव court. While it is a simple quadratic problem it has a certain beauty in its versification in the मालिनी meter. We showed it to a few friends to take some delight in the educational devices in देवभाषा. Then it struck us that it would be a good test for R's legendary IQ. She took a few minutes with the संस्कृत, but not with the mathematics -- we think her abilities are undiminished on the whole :-)\
[अलि-कुल-दल-मूलम् मालतीम् यातम् अष्टौ निखिल-नवम-भागाश् च अलिनी भृङ्गम् एकम् ।]{style="color:#99cc00;"}\
[निशि परिमल-लुब्धम् पद्म-मध्ये निरुद्धम् प्रतिरणति रणन्तम् ब्रूहि कान्ते अलि-संख्याम् ॥]{style="color:#99cc00;"}

The square root of 1/2 a cluster of bees translocated to a mAlatI creep, there after went 8/9ths of the total number bees, remaining was a one queen bee, which buzzed to the other remaining drone which was trapped in the midst of a lotus that closed for the night. Tell me dear lady the total count of bees (भास्कर addressing his daughter लीलावती).

The answer to it may be obtained among other ways by setting the total number of bees=8\*x^2 and solving the ensuing quadratic: 4\*x^2-9\*x-9=0\
Hence, the ali-संख्या = 72


