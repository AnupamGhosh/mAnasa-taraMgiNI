
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on the Shrilankan pantheon](https://manasataramgini.wordpress.com/2011/09/11/some-notes-on-the-shrilankan-pantheon/){rel="bookmark"} {#some-notes-on-the-shrilankan-pantheon .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 11, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/09/11/some-notes-on-the-shrilankan-pantheon/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In 1588 CE Christian terrorists from Portugal launched a surprise attack on the Shrilankan temple town of Devinuvara. Their intention was to destroy the temple of विष्णु and impose the horrors of the Catholic version of the religion of love on the heathens of Lanka. One of their chief vandals de Couto describes the event with much glee:\
"The temple itself was vast in size, all the roofs being domed and richly carved; round it were several handsome chapels and over the principal gateway was a tall tower entirely roofed with copper, gilt in various parts. Within was a large square with verandahs and terraces with a handsome gate on each side, while all around were planted sweet-smelling flowers which were used during the processions. We burst in the gates and proceeded to destroy the idols of which there were more more than a thousand of different figures of clay and wood and copper, mostly gilded. We destroyed the domes and colonnades and sacked the stores where we found a vast accumulation of ivory, fine cloths, coffee, pepper, sandalwood, jewels, precious stones and all the fittings of the temple, which we plundered as we desired and set the rest on fire. As the greatest affront that could be offered to the place we slaughtered within some cows, this being a strain which could not be purified without the most elaborate ceremonies. We also burnt a magnificent wooden car built like a tower of seven stories and beautifully painted and gilt -- a magnificent vehicle in which they were accustomed to convey the chief idol around the city." Translated from Portuguese by Powell.

This act of cultural genocide ranks along with those of their cousins in the New World in erasing the native civilizations of those continents. These and related events have created a deep void in our understanding of the actual form the dharma in the southern island. This was compounded by the revivalist form स्थविरवाद that has sort to expunge or conceal the natural form of the dharma on the island. Our knowledge of the islanders' system comes in no small part due to the efforts of Paranavitana and Gananath Obeyesekere. The latter in particular was a remarkable student of the Shrilankan pantheon -- his work is rich in important original data, but it is sadly marred by his fascination to create western style pseudo-theories often based based on western dogmatic doctrines such as Freudism. These theories combined by his sub-current bauddha bias have made the interpretive aspects of work unsound, but they do not detract from the core value of his textual collections and ethnological observations \[We must point out that GO himself accepts that fact his work is a product of western conditioning].

The Lankan pantheon accommodates 6 deities in a primary position. These include:\
Two characters from रामायण- 1) the rAkShasa विभीषण. 2) saman: who is a complex syncretic evolute of the very early nAstika samantabhadra (i.e. prior to his full-fledged emergence in the mahAyAna form) and the रामायण's लक्ष्मण further identified with a solar deity. He is the deity of समन्तकूट and ratnapura.\
Two deities who were simultaneous enormously popular in the early Tamil and Andhra countries: 3) विष्णु -- the deity of Devinuvara. 4) skanda -- the deity of कतरगाम.\
A future buddha: 5) maitreya -- he is commonly known as नाथ in the Lankan tradition.

 6.  An enigmatic goddess known from the old Tamil country and humanized in the Tamil epic of the शिलपधिकारं -- पत्नी. She is the कण्णकी of the Tamil epic who is presented in it as being posthumously deified as पत्तिनी amman.

These deities occur alongside those inherited from the common pantheon of the followers of dharma and some others of uncertain provenance. However, most of these other deities barring indra, have considerably reduced in their roles in Lanka, though some might occupy local prominence. From the main six, different sets of tetrads have been drawn, and are widely worshiped as the four guardians of Lanka (the satara deviyo) and the saugata-mata practice on the island. The tetrads appear to be modeled upon the older bauddha tetrad of the 4 deities laid out in the आटानाटीय sutta \[Footnote 1]. Of these the cult of विभीषण was specially associated with the Lankan capital, as he was seen as the legendary ruler of Lanka having inherited the throne from रावण by siding with the invading force of rAma. His great राक्षसालय was in कल्याणी near modern Colombo. Unfortunately, his temple too was destroyed in the Portuguese cultural genocide. Though the shrine was rebuilt later, the textual traditions pertaining to his worship have been largely lost during the Portuguese occupation and the modern resurgence of स्थविरवाद has resulted in a further decline in his cult. Also occasionally associated with these big six is गणpati, who is worshiped as गण deviyo. A सिंहल mantra text to this deity recorded by Gananath Obeyesekere has been translated thus:

["O गण deviyo, godly king, divine listener, lord of the रुहुण country, bringer of all knowledge and intelligence, we seek blessings in your name."]{style="color:#ff9900;"}

While not enumerated among the big six his presence is seen in their rituals in more than one place. गणpati also appears in several combined invocations alongside some other deities, including members from the big six and other deities of the classical Indo-Aryan pantheon. For example at the beginning of certain ceremonies one might encounter a recitation such as:\
[In this blessed isle of लन्का laden with plenty]{style="color:#ff9900;"}\
[ we give various offerings and gifts]{style="color:#ff9900;"}\
[ to the beautiful and powerful]{style="color:#ff9900;"}\
[ prince skanda, the god]{style="color:#ff9900;"}\
[ O lord skanda]{style="color:#ff9900;"}\
[ऒ लोर्द् गण देविञ्दु]{style="color:#ff9900;"}\
[ऒ देवी सरस्वती]{style="color:#ff9900;"}\
[ O goddess महिकान्ता (i.e. पृथिवि)]{style="color:#ff9900;"}\
[ We utter this invocation today]{style="color:#ff9900;"}\
[ Give us blessings and kindness.]{style="color:#ff9900;"}

Or the invocation for boons:\
[You who bring victory to all beings]{style="color:#ff9900;"}\
[ गणेश and all ye other gods;]{style="color:#ff9900;"}\
[ prince skanda who is the guru of all the world]{style="color:#ff9900;"}\
[ Protect us and give us well-being, O god skanda.]{style="color:#ff9900;"}

[In the sky above the sun and moon hold sway,]{style="color:#ff9900;"}\
[ On the earth below the earth goddess holds sway,]{style="color:#ff9900;"}\
[ for the whole world prince skanda holds sway,]{style="color:#ff9900;"}\
[ for us here the seven पत्नी-s hold sway.]{style="color:#ff9900;"}

[पत्नी full of merit and chaste!]{style="color:#ff9900;"}\
[ O prince skanda, guru of the three worlds!]{style="color:#ff9900;"}\
[ We sing songs of the अङ्केलिय \[Footnote 2],]{style="color:#ff9900;"}\
[ And sing how diseases subside.]{style="color:#ff9900;"}

\~ओ^ऊ\~ओऒ^^ऊ\~ओ^ऊ\~

Footnote 1: The ताथागत-s have a confused several ancient Aryan deities in making up this tetrad. They include: 1) kubera, the lord of the यक्ष-s; 2) धृतराष्ट्र -- he is a nAga lord in classical tradition but the ताथागत-s have confused him as being the gandharva lord. 3) विरूपाक्ष -- he is traditionally a रक्ष lord, but the bauddha-s have confused him as being the nAga lord. 4) विरूढक, the lord of the कुम्भाण्ड-s.

Footnote 2: A vulgar श्रीलन्कन् game of jostling and pulling with hooked sticks. It might occur in the month of विशाखा or close to the autumnal equinox. It was once widespread among both the Tamil and सिंहल peoples but has now become largely obsolete. It is supposed to have been instituted by गणpati to bring about a truce between पत्नी (कण्णकि) and गोपाल (kovalan) who were its initial players. Its origins might have even been in the Tamil country, but it is entirely extinct there.


