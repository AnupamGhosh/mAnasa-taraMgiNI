
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The moon on the waves](https://manasataramgini.wordpress.com/2011/12/03/the-moon-on-the-waves/){rel="bookmark"} {#the-moon-on-the-waves .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 3, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/12/03/the-moon-on-the-waves/ "8:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had a lengthy conversation. She smiled, and said: After all the कापालिकौ can recognize each other even without the Chomma. When a स्त्री with tresses flowing like सरस्वती in her descent, stanau like कूर्म-पृष्टौ and nitambau like kapittha-phalau places her self on the patra, puruSha-s buzz around her by the thousands like माशिक-s. She feels that she is राज्ञी surrounded by a thousand भृत्य-s. But neither this secret nor the substance is there for all. We have seen it all and now the veil has burnt and what is of old splashes around like the many moons reflecting on the waves. Now as खण्डरोहा we are on the scorching cremation ground with मन्थानरुद्र, with the kapala hanging from our neck.

Now we are महाकाल, showing the mudra of the त्रिशूल, being Urdhvaretas we dally with you at the heart of the pentad of काम, whose yoni is the काल-chakra. The flames of the pyres with popping bones and hissing flesh lash around us. In the cave of the मूलाधार is the glowing उच्छिष्टोल्क and in the लिङ्ग is hasti-पिशाची, in the मणिपूर is skanda and in the हृदय is षष्ठी along the upward course of the सुवर्णबिन्दु. The lord of the क्षेत्र-s rules over the crown. She said the order of the signs would be thus: the त्रिशूल, I would come as the कुलाङ्गना, the क्षेत्रपाल, and pentad of काम with the कालचक्र. You shall move through the the facets of the pentad of manmatha in the great मण्डल, which is experienced by the chit, and then reach its center. Therein, you shall approach me becoming the शक्तिमान् and we form the yamala. Though a yamala to the perceivers of the objects, for the yugma locked in the उल्लास of rati it is advaya: सर्वशक्तिमान् and mantra-prayogaka. But only the yogin the can bear it, else he expires even as the तथागत in the embrace of चित्रसेना. After this he knows the reality of chit and observes प्रकृती as she unfolds.

[आथो दूति परिपृच्छ ।]{style="color:#99cc00;"}\
We say to the दूती:\
May we be shatrujit.\
We observed you unfolding.\
We saw the eka-s.\
They were parama-सूत्रखादक-s or parama-sUtra-कृत्-s or उपसूत्र-s.\
Can these exist without the larger jIva or did they first live the अणु-संग्रह emitted by deva सविता?\
Then we saw the yugma-s.\
Of them there are the agada-विष-s and the सूत्रान्तरच्छेदक-me-lekhaka-s.\
Just like them are the धनुर्विष-s and शक्तिविष-s with their agada-s.\
But they are jIva-hita rather than svahita.\
Then there are the यातुधान-s\
Then there are the jIva-s.\
They have the इन्द्रियाणि all made of अणु-s stemming from the भूतादि.\
When the eka, and the yugma-s, and इन्द्रियाणि are all अणु-s which we know are all your unfolding, where is the puruSha?\
Is he the sattva?


