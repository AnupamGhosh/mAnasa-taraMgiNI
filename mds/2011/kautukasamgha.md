
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कौतुकसंघ](https://manasataramgini.wordpress.com/2011/12/26/kautukasamgha/){rel="bookmark"} {#कतकसघ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 26, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/12/26/kautukasamgha/ "8:58 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The train ground to a stop and we stepped out into the cold silence swathed in armor saying "[कम्बल्वन्तं न बाधते शीतम्]{style="color:#0000ff;"}". None could recognize us beneath our armor. Since our mind was conditioned to see S we did not notice M though he passed us a couple of times in the entirely empty station. Finally, we realized that there was something unusual in seeing M, where he was not supposed to be, and stepped forward to greet him almost startling the ghost out of him. He said S had asked him to pick us up as she was still unable to achieve her objectives in सूपकल. When we reached there and our eyes adjusted to the light we were surprised to see CA and RC in the संघ. S remarked: "If only we had ST around how these struggles in the सूपगृह would have literally melted away. All we seem to achieving look like the legendary balls made large by Bhajji in that well known spoof". We agreed: "We wish those for the Hindu politicians". And then wondered aloud: "ST could have well come; for after all ekanetra is lost in the संभोग of his अभिसारिका like गौरिवीति in the realm of the asura-s. By the way how did you manage to assemble this impressive संघ ?" S: "I decided we should make it a point to keep out boring people so that we can have all the fun as we want." RC: "Wow that sounds elitist :-)". S: "You know, let the rest of the Hindus enjoy Bollywood or Hollywood or what ever the secular nonsense they want to do. We ensconce ourselves from the rest in our गुह्यसमाज". R interjected: "But let's have the music anyhow; who knows what thoughts it might inspire of distant times and places". We: "So what were you up to in the गुह्यसमाज?" R: "Certainly we spent a while on Megaphragma' neurons. I have closely observed some of the smallest of their ilk in the Indian species described by the great but forgotten B.R. Subba Rao in the 1960s. Apparently they were first discovered before that the intrepid Ramakrishna Ayyar in the 1930s, but apparently he did not describe them fully." We: "Ramakrishna Ayyar had suggested an interesting three way biotic association between cattle, dipterans and the wasp Bembex lunata. I recall you were revisiting it -- where you able to to confirm it?". R: "Certainly, they seem to be statistically significantly more frequent in the vicinity of cattle than elsewhere. However, I could not get a general count for all the biting flies in our observing grounds due to the updrava of the dasyu-s."

S asked us of our ontology for dharma. We course had to cite कणाद: [धर्म-विशेष-प्रसूताद् द्रव्य-गुण-कर्म-सामान्य-विशेष-समवायानां पदार्थानां साधर्म्य-वैधर्माभ्यां तत्त्वज्ञानान् निःश्रेयसं ॥]{style="color:#99cc00;"}

S pointed to a translation of this by Matilal: "The 'supreme good' (i.e. निःश्रेयस) is achieved through the knowledge of reality resulting from a special merit arrived at through the inductive method of agreement and difference of properties of the six categories, namely, substance, quality, action, generality, particularity and inherence."

We would say that this is a pretty good approximation of what कणाद conveys in his sUtra; of course कणाद had earlier pointed out that: "dharma is that which brings about upliftment in life (abhyudaya) and the supreme good (निःश्रेयस)." This might be contrasted with the view of the मीमाम्स sUtra-s, which define dharma as that which is denoted as the object or the supreme good by vaidika injunctions. Thus, in the वैशेषिक the arrow runs from the dharma to the veda -- the veda is an authority because the saMhitA-s teach dharma rather than the other way around. This point apart, after some explanation, it became clear that for all of us in the room the sUtra, which we cited in full above is what largely jibes with our thinking.\
S: "So the वैशेषिक you advocate is in its direction somewhat different from the course it took over the ages. Probably in large part influenced by the attacks of the bauddha-s, advaitin-s and the philosophical facets of सांख्य. You would advocate a more science-centric वैशेषिक."

We: "Yes. The path of वैशेषिक that we take is a continuation of the tradition that expresses itself in the medical saMhitA of charaka. A practical application of the theories of वैशेषिक to understand nature. It also incorporates the scientific side of सांख्य, which does not entirely conflict with वैशेषिक, i.e. the portions that are downstream of सांख्यन् परमाणु-वाद. At the end of the day we have always held out for navya-darshana that might incorporate certain elements of मीमांस, such as निरीश्वर-वाद, along with वैशेषिक. In this particular regard the original वैशेषिक of कणाद was very close -- it was indeed निरीश्वर. As per the yukti दीपिका of the सांख्यवादिन्-s the very idea of Ishvara came into the वैशेषिक realm due the the shaiva-s -- since, from an early period वैशेषिक was closely associated with the पाशुपत-s and their successors the सैद्धान्तिक-s."

CP: "So would you see the वैशेषिक of भट्ट jayanta as having already taken this turn, perhaps a negative one. Certainly, it seems so at least for udayana. But upon your goading I came to study the भट्ट more closely and agree that he was certainly more important to Hindu thought than the status usually accorded to आदिशंकर in this regard. But looking at his discussion of पुनर्जन्मवाद the भट्ट would seem as bad or worse -- in a sense he is making a mess of his otherwise splendid performance. But may be we can call him as one who clearly defined Hindudom"

We: "भट्ट jayanta has his issues, especially his need reconcile Ishvara and punarjanma into the system. However, I must confess my sympathies for the भट्ट in particular because he was like us an अथर्वण-शाखाध्यायिन् and defends the place of the AV with some force. We agree that he is one of the clearest in the self-definition of the followers of dharma, defending the the overall place of the tantra-s beside the original shruti. His work also illustrates how the Astika-s really viewed the ताथगत-s -- a sharp contrast to the spite seen in say तारनाथ's history. We may sympathetically interpret his Ishvara-वाद as just a terminology for the prAdhAna-कारण for the collisions between the particles to initiate the emergence of the universe. We also tend to look for the many positives in jayanta though his वैशेषिक was already in the path of the "philosophy"-heavy turn it had taken. For example, we see his position against the अनादि अपौरुषेयत्व of the shruti as a corrective to मीमाम्स. He states that the value of the veda as shruti is primarily due to it being spoken by reliable people, the inspired Arya seers, and not due its eternality. He clarifies that there are texts like the medical saMhitA-s whose validity rests on experimental results rather than authority. While his justification of the reliability of the ऋषि-s using inspiration of prajApati appears facile from our viewpoint, it is not fatal and can be reinterpretted. It is not that he is entirely bereft of the science of वैशेषिक: after all in explain sound he stresses the point that कणाद's theories present a superior understanding of the natural world to that of भट्ट कुमारिल. He also presents advances to the theory of sound over that of the मीमाम्सक-s by explaining transmission sound waves through media by initiation of vibrations. Again, ironically in the section leading to punarjanma and ईश्वरवाद he gives an account of the molecular constitution of a living body, like that of humans, and clarifies that it is no different in the basis of construction from other material dravya-s.

RC: Are the similarities between mythologies due to monophyly, convergence, lateral transfer or simply the result of spurious pattern matching on our part?\
For a hilarious extreme case let us consider the motifs we in the room tended to see in the English neo-mythology Harry Potter and go back to that question:

  - The battle between the snake-guild or snakes (Slytherins) and the sword-wielding wizards (Gryffindor): the battle between the nAga-s and the sword-wielding विद्याधर-s in the बृहत्कथ tradition, or the multi-generational battle between the nAga-s and the kuru-s in the frame story of the bhArata.

  - Six Weasley brothers of which one dies leaving 5 behind. 6 "कर्णादि" brothers of which five survive.

  - Of the six Weasleys one is on the side of the enemies: कर्ण of the 6 कर्णादि brothers is on the enemy side.

  - Two Weasley brothers are twins and specialist wonder workers: Twin माद्रेय-s sons of wonder-working ashvins (पुरुदंसस)

  - One of the twins dies: One of the Dioscuri, Castor is mortal while Polydeuces is immortal.

  - Ginny: Transfunctional goddess.

  - Potter's Slytherin connection : the nAga as a kauravya-s; इरवान् and the supremely powerful बब्रुवाहन half nAga/half kauravya.

  - Albus Dumbledore: भीष्म पितामह; नागिनी: the killer of परीक्षित्; Hagrid: घटोत्कच; Inferni: वेताल-s; horcrux and महिरावण-s "horcrux"; the sword of Gryffindor: the विद्याधर's magical sword and many more that we are now forgetting.

CP: Surely this comparative mythology stuff sounds like a whole lot of nonsense ?

Some of us: Yet, why does the motif of the war of the "wizards and snakes" and so many scattered motifs reappear in an English neo-epic.

CA: There is no statistical test to support this gibberish you all are getting into!

We: O CA of pretty smiles taking that statistical line has cause such delusion to so many in my science. You know what I am talking about.There are certain problems where statistics can give a positive answer, but an insignificant result does not always mean lack of relationship.

R: Talking of all this what happened to your story "मन्त्रनायक" of which you had narrated some parts ?

We: Well, when we read the above-named English neomythology at our mother's suggestion, we realized that stories should be done by real writers, even though there is apparently an audience for such stories and let it pass. At the end of the day even we were inspired the tales of somadeva, rAjA bhojadeva and padmagupta.

CP and S: You mentioned that bhoja-deva's work had tilted your opinion towards the existence of actual mechanical devices in place of wizardry. What was the story from which you and R derived the airplane for the script of that play?

We: We were not being too original we were merely imitating the tales of somadeva, दण्डिन् and (specifically in our case) their Tamil counter part.

CP: The airplanes often come in the context of the dohada-s of women; I wonder if in this day and age when there are real airplanes women have dohada-s of flying.

...\
The story of the aeronauts of काञ्चीपुरं (vide somadeva, क्षेमेन्द्र and the Tamil author whose name is lost):\
नरवाहनदत्त had a dream one night of an extraordinarily beautiful girl named कर्पूरिका in the city of कर्पूरसंभव. He with his friend gomukha set out to meet her. On the way the reached a village which was known as hemapura. After sometime they realized that hemapura was entirely a village of speechless robots. The men and women who were around in the marketplace were all robots moving in a clockwork manner controlled by an unseen being. Then they went to the central palace of hemapura, where they saw a single conscious being, who was served like a king by his robotic servants. In response to नरवाहनदत्त's inquiries he said his name was राज्यधर and narrated the following story: He lived in the city of काञ्चिपुरं in the drAviDa country, which was ruled by the king बाहुबल, along with his brother प्राणधर. They had mastered the lore of making robots and machines promulgated by the दानव maya and his daughter, the asurI सोमप्रभा. Of the brothers, प्राणधर was a ladies man and squandered both his and his brothers wealth on several public women. Having become bankrupt, he made a yantra, which by means of a wire would introduce two robotic geese into the treasury and steal jewels with their beaks. The king soon came to know of this and set up guards to catch the thieves. Seeing the robotic geese entering the treasury they cut the wire by which it was operated. Immediately, प्राणधर felt the tension sag in his controlling pin and knew that they would be caught as thieves. So he told राज्यधर that they should flee काञ्ची using the vimAna he had made. But by the time प्राणधर loaded his family into his airplane, there was no space for राज्यधर. Nevertheless, प्राणधर took off and his plane carried him 800 yojana-s away to a city in northern India. But राज्यधर had made his own वात-yantra-vimAna, which on its first flight took him 200 yojana-s and in a second flight took him another 200 yojana-s placing him in the middle of some abandoned village. There he made several robots and populated the village making it the hemapura, where नरवाहनदत्त had reached in his quest for कर्पूरिका. After resting for a day at hemapura, नरवाहनदत्त wanted to move ahead, when राज्यधर offered him his vimAna to fly him over the coast of west India to कर्पूरसंभव. On landing there, he and gomukha eventually located कर्पूरिका the daughter of the ruler of कर्पूरसंभव and नरवाहनदत्त married her. When नरवाहनदत्त wanted to return he found that his aircraft was too small. His father-in-law offered to fly him back by means of the flying saucer (yantra chakra) of his new engineer. Soon नरवाहनदत्त realized that this was none other than प्राणधर and took of on his large craft. They first landed in hemapura where प्राणधर gifted his brother some real female companions and then they headed back to कौशांभी. There नरवाहनदत्त added कर्पूरिका to his harem to join रत्नप्रभा and मदनमञ्चुका. Then प्राणधर flew back to the king of कर्पूरसंभव to inform him that his daughter had safely reached. Eventually, he went to the कौशांभी and settled under the patronage of नरवाहनदत्त. There प्राणधर is supposed to have made several planes for him and his wives. Thus, नरवाहनदत्त is supposed to enjoyed his aerial journeys as though in preparation for this ascent to the position of the lord of the wizards, which forms the next part of his adventures.

M: So now I can see where you guys got the zombie analogy long before Chalmers.

Thus we returned to the deep speculations on the teachings of the दूती.


