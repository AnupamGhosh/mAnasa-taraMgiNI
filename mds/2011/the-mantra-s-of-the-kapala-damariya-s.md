
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mantra-s of the कपाल-डामरीय-s and the early mantra-मार्ग](https://manasataramgini.wordpress.com/2011/06/08/the-mantra-s-of-the-kapala-damariya-s/){rel="bookmark"} {#the-mantra-s-of-the-कपल-डमरय-s-and-the-early-mantra-मरग .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 8, 2011]{.entry-date}](https://manasataramgini.wordpress.com/2011/06/08/the-mantra-s-of-the-kapala-damariya-s/ "6:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The tenth पटल of the गुह्यकाली khaNDa of the great महाकाल-saMhitA (MKS) preserves mantra-s pertaining to the worship of गुह्यकाली according the system of the कपाल-डामरीय-s. This काली-kula system is said to be that of the कापालिक-s transmitted by their first teacher, कपालडामर. Indeed, in 10.1037, in the context of the worship of गुह्यकाली in the 8-petalled मण्डल, it is stated thus:\
[कपालडामर प्रोक्तं कापालिकमतं शृणु ॥]{style="color:#0000ff;"}\
[वेदादि-कामिनी-पाशा इच्छा निर्वेद एव च ।]{style="color:#0000ff;"}\
[एते पञ्चपि वर्णाः स्युः सर्व-मन्त्रादिवर्तिनः ॥]{style="color:#0000ff;"}\
Here the 5 syllabled primal mantra for the worship of गुह्यकाली in the said मण्डल of the कापालिक tradition, as taught by कपालडामर is specified:\
वेदादि=OM; कामिनी=कं; pAsha=AM; इच्छ=फां; nirveda=फ्रीं\
[ऒं कं आं फां फ्रीं ॥]{style="color:#0000ff;"}

Some of the other काली-kula systems, which are also mentioned along side the कपाल-डामरीय-s, are the mauleya-s, the tripuraghna-s, the दिगंबर-s and the भाण्डिकेर-s. Further allusions to these traditions are available in the section starting at 10.1251. They are described as being kaula traditions. Here it is again clarified that the कापालिक-s root text was taught by कपालडामर. The root text of the naked दिगंबर-s, who wander in cemeteries, is said to be promulgated by bhairava. The mauleya-s are described as followers of the यामल texts, while the शाबरतन्त्र is said to be root text of the भाण्डिकेर-s. The दिगंबर-s and कापालिक-s are described as being sarva-भक्ष-s who eat human meat and beef and participants in incestuous sexual unions. They are clearly described as veda-बाह्य-s. So these कापालिक-s are evidently what one might term the "later" कापालिक-s who lie inside the kaula system rather than the early पाशुपत कापालिक-s. Nevertheless, it is clearly stated that all the above traditions undergo पाशुपत दीक्ष and are called shaiva, suggesting the continuity between the two.

One formulation of the kula-chakra, in the midst of which गुह्यकाली is worshiped in the system of कपाल-डामरीय-s (10.1000-1015), is the 12-petalled मण्डल (of course the दिगंबर-s and tripuraghna-s have their own cognates of the same). The system is interesting in that it incorporates a list of astra-s: 1) ब्रह्मास्त्र; 2) आग्नेयास्त्र; 3) वायवास्त्र; 4) ऐषिकास्त्र; 5) पर्वतास्त्र; 6) नागास्त्र; 7) प्रस्वापनास्त्र; 8) सौपर्णास्त्र; 9) मातङ्गास्त्र; 10) दानवास्त्र; 11) पैषाचास्त्र; 12) brahmashiras\
The bhairava-s are listed as: 1) उल्कामुख; 2) पिङ्गजट; 3) दावानल; 4) प्रेतासन; 5) शुष्कोदर; 6) ज्वालाकुल; 7) चण्डहास; 8) भूतोन्माद; 9) kulachakra; 10) मेघनाद; 11) विश्वरूप; 12) antagochara.\
These twelve bhairava-s are associated with the 12 astra-s. Their mantra-s are formed by:\
[कूर्छ-रावव्-अथास्त्रं छ नमः स्वाहा तथैव छ ।]{style="color:#0000ff;"}

कूर्च: hUM; राव: फ्रें; astra: फट्\
e.g. [उल्कामुखाय हुं फ्रें फट् नमः स्वाहा ॥]{style="color:#0000ff;"}\
In deployment they might be combined with the विशेष सप्ताक्षरी विद्या of गुह्यकाली:\
[तारत्रपे योगिनी छ रमाकामौ छ डकिनी ।]{style="color:#0000ff;"}\
[प्रलयश् चापि फेत्कारी बिजानि स्तैर्य-भन्ञ्जि हि ॥]{style="color:#0000ff;"}\
Thus the विशेष सप्ताक्षरी of the कपालडमरीय-s is:\
[ॐ छ्रीं श्रीं क्लीं ख्फ्रें ह्स्फ्रें ह्स्ख्फ्रें ॥]{style="color:#0000ff;"}\
The सामान्य सप्ताक्षरी is cryptically encoded.

In addition to the several details on the कापालिक practice of the कपाल-DAmara tradition, the text specifies a special ritual of the कापालिक-s known as the अशोकारोहण. It appears to have been performed on the chaitra shukla saptami and was marked by the worship of महिषासुरमर्दिनी by with ashoka flowers, followed by a पूजा with modaka-s, rice, कुङ्कुम, incense, camphor and several types of flowers. Given the details of the कापालिक practice in the MKS, it is clear that the text is recording a genuine tradition of these kaula कापालिक-s within the काली kula. Thus, we may infer that the कापालिक system of the early पाशुपत-s eventually merged into this tradition within kaula तान्त्रिक practice in north-eastern India (Bihar, Eastern UP and Bengal; in south India the ancestral कापालिक-s survived in part).

This inference of the absorption of the कापालिक-s through a merger with the काली kula also helps understand the situation with the other mata-s and tantra-s mentioned in the above-stated मतनीरुपण section of MKS. The only mata-s described as shaiva are the कापालिक-s, दिगंबर-s, mauleya-s and भाण्डिकेर-s and all of them are described as being kaula-s. Beyond these the text mentions याज्ञिक-s as the performers of vaidika rituals, the jaina-s, bauddha-s, चार्वाक-s, निरीश्वर सांख्यवादिन्-s, वेदान्तिन्-s, saura-s, vaiShNava-s, गाणेश-s, shrauta-s and स्मार्त-s. Thus, it is clear that the MKS is seeing the shaiva practice as being solely kaula (i.e. It does not even mention the सैद्धान्तिक-s). But how do these mata-s being described as kaula in MKS compare with the actual textual situation (especially in terms of the texts attributed to them)? Below is a chart of the traditions and their saMhitA-s as per the MKS:

![MKS_matas](https://manasataramgini.files.wordpress.com/2011/06/mks_matas.jpg){width="75%"}

Now we may compare this with the system of development of the shaiva शास्त्र-s:
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2011/06/shivashasana.jpg){width="75%"}
```{=latex}
\end{center}
```



Within the shaiva शास्त्र-s both tradition and an objective analysis point to a consistent picture. There are two major traditional classifications: One follows the srotas system (see above picture). The second, the पीठ system, is used in addition to the srotas system by the kaula-s. The latter is largely consistent with the layers of development within the bhairava srotas. What we observe is that the classical bhairava tantra-s are the root layer comprising the mantra-पीठ and correspond to what are termed the tantra-s of the दिगंबर-s according to the MKS. The next layer in the पीठ system is the विद्यापीठ which includes three streams in it. Of these the right and the middle streams are dominated by the यामल tantra-s. The jayadratha यामल which has absorbed tantra-s of the वाम-srotas belongs to the middle stream of the विद्यापीठ along with योगिनी-जाल-शंबर, विश्वाद्य, सिद्धयोगेश्वरी-mata and श्री-chakra. The left stream includes tantra of the वाम srotas like संमोहन, the DAmara tantra and others like महारौद्र. Thus, it appears that the the यामल-s in the right and middle stream of the विद्यापीठ map to the tantra-s that are termed as belonging to the mauleya-s in the MKS. A part of the left stream is the DAmara which belong to the कापालिक-s according to the MKS. The srotas classification reports that the शाबर tantra was a section of the garuDa srotas. We suspect that this mapping preserves within it a remnant of the relationship between the old ati-मार्ग systems and the mantra-मार्ग elements that evolved from it. Thus, not only the कापालिक-s but also the rest like the mauleya-s, दिगंबर-s and भाण्डिकेर-s are likely to represent streams originally within the ati-marga that eventually adopted the mantra-मार्ग. The above mapping might provide hints regarding how this might have happened. From the early layer of the siddhAnta tantra-s it is clear that the Urdhvasrotas had a close affinity with the various अतिमार्ग पाशुपत-s who primarily emphasized purity. However, they did not entirely exclude the elements of निःशङ्काचार such as the asidhara-vrata and लता-साधान. These latter elements were emphasized to differing degrees in the कापालिक and related stream of the अतिमार्ग. So it is quite possible that these were the precursors of the cognate streams of the mantra-मार्ग that included such practices. It also suggests that there might have been a core mantra-मार्ग of this type, i.e. that of the वाम tantra-s and bhairava tantra-s, while various miscellaneous तान्त्रिक practices developed by the transitioning ati-मार्ग schools were incorporated into these core mantra-मार्ग systems. In this light we suspect that the DAmara tantra-s containing early mantra material that may have been developed by the कापालिक-s might have been incorporated into the वाम srotas or the left stream of the विद्यापीठ. This is supported by an archaic core seen in the उड्डामरेश्वर tantra, which we will consider separately on a different occasion. Similarly, the bhairava tantra-s and the यामल tantra-s themselves might have been evolutes of the mantra systems of the other originally ati-मार्ग streams, the दिगंबर-s and mauleya-s. The भाण्डिकेर-s in contrast might have preserved a parallel redaction of the mantra material similar to that used by the कापालिक-s in their शाबर tantra-s. It is possible that these were also associated with the shabara tribes whose rituals were used in the shAkta context for दुर्गा (especially वनदुर्गा and विन्ध्यवासिनि) on the विजयदशमी day. They appear to have been finally incorporated into the garuDa srotas, probably on account of their general topical similarity to other material incorporated into this srotas, such as the herbal antidotes, use of avian feathers and certain sylvan rituals. Today, the rare surviving भाण्डिकेर-s in Southwestern India appear to have entirely lost their affinities to निःशङ्काचर and are followers of the शंकर मठ.


