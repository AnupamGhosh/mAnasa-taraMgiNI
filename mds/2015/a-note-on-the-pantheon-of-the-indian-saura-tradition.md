
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on the pantheon of the Indian Saura tradition](https://manasataramgini.wordpress.com/2015/08/12/a-note-on-the-pantheon-of-the-indian-saura-tradition/){rel="bookmark"} {#a-note-on-the-pantheon-of-the-indian-saura-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 12, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/08/12/a-note-on-the-pantheon-of-the-indian-saura-tradition/ "4:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Indian Saura-mata (or the Hindu sect of the Sun) is an amalgam of two distinct layers \[Footnote 1]: 1) The endogenous layer of solar deities going back to the Veda and 2) the neo-morphic layer of Iranic origin which was transferred to India as as a result of contact with Zoroastrians and to an extant non-Zoroastrian Iranians from a period spanning the beginning of the common era to the around the 6th-7th centuries of the common era. While primarily an आस्तिक tradition, the later period the नास्तिक traditions of the Bauddha-s and to some degree the Jaina-s also developed reflexes of the Saura system. Among Bauddha-s, the earlier manifestation crystallized around the goddess मरीची whose roots as a warrior goddess lay in the Saura pantheon. In the later phase, the high-point of the वज्रयान tradition, in the form of कालचक्र also incorporated several Saura elements.

The Veda, like other Indo-European traditions, preserves a strong element of worship of solar deities. It should be stressed that the solar deities are not necessarily the sun: they are associated with solar characteristics. This means they are inspired by the experience of the sun but go beyond the sun and are more general in their manifestations encompassing the stars, the laws of cyclicity and invariance, and light and darkness. While one could say that all Vedic deities simultaneously have a solar character, the most prominent among them are the आदित्यस् who were to play a major role in the Indian Saura-mata:

In the most ancient layers of the Veda six (or perhaps seven) of them are named together as a group:\
[इमा गिर आदित्येभ्यो घृतस्नूः सनाद् राजभ्यो जुह्वा जुहोमि ।]{style="color:#0000ff;"}\
[शृणोतु मित्रो अर्यमा भगो नस् तुविजातो वरुणो दक्षो अंशः ॥]{style="color:#0000ff;"}RV 2.27.01

इमास्= these \[feminine accusative plural]; gira= invocations \[feminine accusative plural]; आदित्येभ्यो= for the आदित्य-s; घृतस्नूः= dripping with ghee \[feminine accusative plural]; सनात्= always; राजभ्यो= for the royal; जुह्वा= with the जुहू ladle; juhomi= I offer; शृणोतु = each one hears; मित्रः; aryaman; भगः; nas= us; तुविजातः= widely manifest favor; वरुणः; अंशः.

Before we render a translation we should note three points: 1) जुहू ladle: The scholiast सायण explains that as the invocations are said to be dripping with ghee, it should be understood metaphorically, with the जुहू standing for the tongue that composes the said invocation. 2) While a plurality of आदित्य-s are named the imperative class-5 verb शृणोतु is in the singular. सायण explains that it implies that each one of the आदित्य-s starting from Mitra are called to hear the invocation \[as per सायण: [नः शृणोतु -> अस्मदीयास्ता गिरो मित्रादयः प्रत्येकं शृणोतु]]{style="color:#0000ff;"} -- it should be noted in this context that they are not named as a compound or with an enclitic 'ca' but simply as a list. 3) While the names of the 6 आदित्य-s are plain in this mantra, the word तुविजातः is traditionally taken as an adjective for वरुण. Indeed, it is a fairly common adjective in the ऋग्वेद for Mitra and वरुण, and more generally the आदित्य-s as also for Indra, बृहस्पति and Agni. सायण explains the word as one who widely manifests his favor to many nations. Going against the grain we suggest that it is possible that तुविजातः here is another आदित्य, i.e. विवस्वान्.

Hence we have:\
With the जुहू ladle \[which is my tongue], I perpetually offer to the royal आदित्य-s these ghee-dripping invocations. May each one of them, Mitra, Aryaman, Bhaga, the one with widely manifesting favor \[or विवस्वान्], वरुण, दक्ष and अंश, hear us!

The reason why the list of 7 is a possibility is because elsewhere in the ऋग्वेद the number of आदित्य-s is stated as 8 (RV 10.72), with 7 being the immortal gods and the 8th being the dead-egg मार्ताण्ड from which the rest of the universe was fashioned. A similar tale is elaborated in the ब्राह्मण sections of तैत्तिरीय संहिता 6.5.6 and तैत्तिरीय ब्राह्मण 1.1.9 in the context of the brahmaudana ritual (rice offering to the goddess Aditi). The still-born आदित्य मार्ताण्ड might have had old Indo-European antecedents as suggested by the dead solar deity Baldr in the northern Germanic tradition. Moreover, the आदित्य-s are recorded as numbering 8 even in the तैत्तिरीय आरण्यक 2.10.7. However, there मार्ताण्ड is replaced by Indra and दक्ष by his ectype धातृ. Thus the list runs as: Mitra, वरुण, Aryaman, Bhaga, विवस्वान्, धातृ, अंशु and Indra.

In the late Vedic period we also have statements that give the number of आदित्य-s is given as 12:

[स मनसैव वाचम् मिथुनं समभवत् । स द्वादश द्रप्सान् गर्भ्य् अभवत् । ते द्वादशादित्या असृज्यन्त । तान् दिव्य् उपादधात् ॥]{style="color:#0000ff;"} शतपथ ब्राःमण 6.1.2.8\
By his mind he \[god प्रजापति] came into copulation with speech; he became pregnant with twelve drops; they were emitted as 12 आदित्य-s; he placed them in the sky.

By the epic period the 12 आदित्य-s are specifically named as Mitra, वरुण, Aryaman, Bhaga, विवस्वान्, धातृ\[or दक्ष], अंश, पुषन्, सवितृ, त्वष्टृ, Indra and विष्णु.\
e.g. the महाभारत has:\
[अदित्यां द्वादशादित्याः संभूता भुवनेश्वराः ।]{style="color:#0000ff;"}\
[ये राजन् नामतस् तांस् ते कीर्तयिष्यामि भारत ॥]{style="color:#0000ff;"}\
[धाता मित्रो 'र्यमा शक्रो वरुणश् चांश एव च ।]{style="color:#0000ff;"}\
[भगो विवस्वान् पूषा च सविता दशमस् तथा ॥]{style="color:#0000ff;"}\
[एकादशस् तथा त्वष्टा विष्णुर् द्वादश उच्यते ।]{style="color:#0000ff;"}1.59.14-16a ("Critical")

From Aditi were generated 12 आदित्य-s, the lords of the universe, whose names, O king, I shall narrate:\
धातृ, Mitra, Aryaman, शक्र (i.e. Indra), वरुण, अंश, Bhaga, विवस्वान्, पुषन्, सवितृ, त्वष्टृ, and विष्णु.

In this regard we may point out a सूक्त from the Atharvaveda where the above 12 are all named, albeit along with other gods (AV 11.6). Taking the number of आदित्य-s as 12 also allows us to explain the traditional short count of male deities, i.e. 33 \[Footnote 2], which is seen in both the Veda and the इतिहास-s, in a straight-forward way. For example the रामायण has:

[अदित्यां जज्ञिरे देवास् त्रयस्त्रिंशद् अरिंदम ।]{style="color:#0000ff;"}\
[आदित्या वसवो रुद्रा अश्विनौ च परंतप ॥]{style="color:#0000ff;"}3.13.14c-15a ("critical edition")\
From Aditi were born the 33 deva-s, O foe-crusher (i.e. राम): the आदित्य-s (12), the Vasu-s (8), the रुद्रा-s (11) and the twin अश्विन्-s (2), O foe-scorcher. \[Traditional numbers in brackets]

One may note that the same counts as above for the आदित्य, Vasu-s and रुद्रा categories are given in the शतपथ ब्राःमण 6.1.2.6-8. Regarding these categories and the total count, in the ऋग्वेद we have:

[त्वम् अग्ने वसूंर् इह रुद्रां आदित्यां उत ।]{style="color:#0000ff;"}\
[यजा स्वध्वरं जनम् मनुजातं घृतप्रुषम् ॥]{style="color:#0000ff;"}\
[श्रुष्टीवानो हि दाशुषे देवा अग्ने विचेतसः ।]{style="color:#0000ff;"}\
[तान् रोहिदश्व गिर्वणस् त्रयस्त्रिंशतम् आ वह ॥]{style="color:#0000ff;"}RV 1.45.1-2\
You O Agni offer ritual here to the Vasu-s, the Rudra-s and also the आदित्य-s \[on behalf] of the people who are Manu's descendants, who perform proper rituals and pour the offering of ghee.\
O Agni and the gods, wise ones, do hear the worshiper: O you with a red-horse, delighting in Vedic chants, bring those 33 gods!

Thus, we find both the categories (आदित्यस्, Rudra-s, Vasu-s, and also अश्विन्-s), and the total number 33 to be an ancient one. Indeed this count 33 likely goes back to the Indo-Iranian period as the Zoroastrians also enumerate 33 yazata-s. Hence, it is possible that there was a system of counting even from the earlier Vedic period that already had 12 आदित्य-s, 11 Rudra-s, 8 Vasu-s, 2 अश्विन्-s, and it would suggest that the count 8 or lower for the आदित्य-s seen in the ऋग्वेद is a parallel tradition.

Accepting this proposal allows us to account for most major Vedic deities within those 3 categories plus the अश्विन्-s: The 12 आदित्य-s as listed above already includes a big fraction of the chief Vedic deities going back to the Indo-Iranian period. Indeed, such a number 12 for the chief gods might have ancient Indo-European antecedents for among the Hittites, Greeks and the northern Germanic people we either see pantheons with 12 chief gods or a category of deities with 12 gods \[Footnote 3].
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-5fezBV6EGfU/VcrKzZvesHI/AAAAAAAADUs/RZYgdUPDv5A/s800-Ic42/hittite_12_gods.jpg){width="75%"}
```{=latex}
\end{center}
```

*The 12 Hittite gods from Yazılıkaya*

The Rudra category originally also included the Marut-s who constitute a second block of major Vedic deities. The Vasu category includes Agni, वायु, Soma and Dyaus, among others who make another key set of Vedic deities. This way one can see why the Vedic pantheon is often described as triad of Vasu-s, Rudra-s and आदित्य-s. Finally, we have the twin अश्विन्-s and बृहस्पति, who are usually in the विश्वेदेव category. Finally, we may note that the functional principle of the triad of Vasu-s, Rudra-s and आदित्य-s also lurks behind the classical Hindu trinity: Dyaus, who is the old Indo-European father figure in the Vasu category, reemerges as the father-deity Brahman; शिव is the exemplar of the Rudra category; विष्णु the epitome of the आदित्य category. In this regard the महाभारत has: "[जघन्यजः स सर्वेषाम् आदित्यानां गुणाधिकः ॥]{style="color:#0000ff;"}" 1.59.16cd ("Critical") i.e. He [विष्णु], who is the last-born (of the आदित्य-s) is the most endowed of all of them. This marks the rise of विष्णु to the preeminent status in that category.

This position of विष्णु among the आदित्य-s, while a hallmark of वैष्णव ascendancy \[Note that elsewhere in the महाभारत, Indra or वरुण are mentioned as the foremost of the आदित्य-s], was also interiorized in part by the Saura-mata. We see this an iconographic representation (below), which was stolen from a Saura shrine in Madhya Pradesh, and auctioned in 2002 (Likely of the Chandela dynasty based on stylistic grounds). Here we can see that the central image of the आदित्य is accompanied by both a distinctive Saura pantheon (see below) and also Brahman and शिव on either side of his head. This implies that the central आदित्य is being implicitly identified with विष्णु.
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-uPkqE1PZI0c/VcrLsdPFHQI/AAAAAAAADVE/QQz6Cy8aF8Y/s800-Ic42/sUrya02.BW.jpg){width="75%"}
```{=latex}
\end{center}
```

*Central आदित्य implicitly identified with विष्णु; flanked by Brahman and Rudra*

Despite the above-noted parallelism with the वैष्णव-mata, the 12 आदित्य-s as a group were central deities of the Saura-mata, especially, in their solar aspect embodied by विवस्वान्. They were combined with the Iranic elements to give rise to a distinctive Saura pantheon. In iconographic terms this is represented by several images showing the 12 आदित्य-s as a group. We shall consider one of those, which was first described by the historian AL Shrivastava \[A Rare Representation of द्वादशादित्य; East and West, Vol 52], to illustrate the peculiarly Saura pantheon (below). On stylistic grounds this image can be considered a production of the राज्पूत् dynasty of the प्रतिहार-s, who were known to be major votaries of the Saura-mata: We have king Mihira Bhoja who was said to have been born upon the invocation of Mitra by his father, and the kings रामभद्र and महीपाल whose inscriptions described them as Saura-s. The image shows stylistic and material similarity to a तोरण with 12 आदित्य-s from Hinglajgarh, Madhya Pradesh, which was noted by AL Shrivastava as being housed in the Central Museum, Indore. Image was auctioned by the Sotheby's auction house and has been since lost to the public. Hence, it is possible that the image was stolen from the Hinglajgarh ruins or a temple renovated by the Chandela-s or परमार-s by one of the image thieves who have been operating at these sites for several years.
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-DtE9XJdEt1Q/VcrKzhLrqOI/AAAAAAAADUw/cbkUuHdlJIU/s800-Ic42/sUrya_madhyapradhesh.JPG){width="75%"}
```{=latex}
\end{center}
```

*The 12 आदित्य-s with the Saura pantheon*

Here the central आदित्य may be identified with his primary solar aspect, i.e. विवस्वान्. Forming a right triangle around around his head are the triad of आदित्य-s who come as a group from the ऋग्वेद: Mitra, वरुण and Aryaman who are also associated with the rising, setting and meridian solar aspects. The remain tetrads on either side of the "top-center" Aryaman account for the remaining 8 आदित्य-s of the dodecad.

In addition to the 12 आदित्य-s the image also depicts the distinctive Saura pantheon which includes:

**The twin अश्विन्-s:** These are horse-headed deities depicted at extreme lower corners of both images. The twins are ancient deities going back to the Proto-Indo-European period and are attested in most branches of the Indo-European tree. They may even go back to an earlier phase of human history being related to twin deities seen elsewhere in Eurasiatic cultures and their New World descendants. In the Hindu tradition they are said to be born of विवस्वान् and his wife संज्ञा when they assumed the form of horses. They are the physicians of the gods who are supposed to have transmitted the science of medicine to the भृगुस्

**संज्ञा:** On the right of the god is his wife संज्ञा also known as राज्ङी. She is said to have assumed the form of a mare in the realm of the Uttarakuru where विवस्वान् is a said to have mated with her in the form of horse to sire the अश्विन्-s.

**छाया or निक्षुभा:** The left of the god is his wife छाया the personification of the shadow or darkness. She is specifically known as निक्षुभा in the Iranized flavors of the Saura-mata and is central to the origin mythology of the Iranic Saura ritualists. The भविष्यत पुराण narrates the following tale in her regard:\
कृष्ण, the hero of the Yadu-s married जांबवती, the daughter of the great bear जाम्बवान्. Their son was the valiant साम्ब. As साम्ब grew up he secretly dallied with some of numerous wives of कृष्ण. Hence, कृष्ण cursed him with an incurable disease that disfigured his skin. To relieve himself of this curse साम्ब went to the banks of the river चन्द्रभागा, worshiped विवस्वान्, and honored the god by constructing a temple at मूलस्थान (what is today Multan, where the temple was destroyed by the Mohammedans). No local ब्राह्मण knew of the mysteries of his worship; hence, they could not take up priesthood at the temple. So साम्ब sought help of Gauramukha, the adviser of the Yadu chief, Ugrasena. Gauramukha asked him to go to शकद्वीप and obtain a special class of ritualists called मगाचार्य-s to worship सूर्य. साम्ब then asked regarding the antecedents of these worshipers of the sun. Gauramukha told him that the first of the ब्राह्मण-s amidst the शख-s was called Sujihva. He had a daughter of the name निक्षुभा. सूर्य was enamored by her took her as his wife. Thus, she gave birth to जरशब्द \[the Indianized Zarathushtra of the old Zoroastrians], who was the founding father of all the मगचार्य-s \[Indianized Magi]. The gotra he founded was hence termed the Mihira gotra. They are distinguished by the sacred girdle called the अव्यङ्ग \[Avestan: Aiwyanghana, i.e. Parsi kusti] that they wear around their waist. साम्ब there upon requested कृष्ण to send him गरुड and flying on the latter's back he landed in शकद्वीप. He collected the मगाचार्य-s, brought them back to भारत and installed them as priests of his सूर्य temple.

**पिङ्गल:** On the proper right of the god is a deity with a pen and inkpot. He is the attendent of विवस्वान् known as पिङ्गल. He is the deity of writing. Thus, he becomes the patron deity of the कायस्थ-s or the old Indian scribal guild. Sometimes, he is identified with Citragupta who records all the deeds of beings. In the later Saura tradition he came to be identified with Agni.

**दण्डिन्:** On the proper left of the god is the attendant deity दण्डिन्. He is depicted typically with a scepter of law. He is said to be the enforcer of law among the beings even as पिङ्गल records their deeds. In the later Saura tradition he came to be identified with Yama.

**स्रौष and राज्ञ:** Like पिङ्गल and दण्डिन् another pair of attendants of विवस्वान्, स्रौष and राज्ञ, are mentioned in the Saura tantra literature, like the tantra sections of the साम्ब पुराण. Their origins from the Mithraic branch of the Iranic tradition are transparent: In the Avesta, Mithra (cognate of Indic Mitra) is said to be flanked on either side by the deities Rašnu and Sraoša. They were Indianized as राज्ञ and स्रौष even as Mithra (Mihira) was identified with primary solar deity in the Saura-mata. In later tradition, राज्ञ is further identified with Indic Rudra and स्रौष with Skanda. The latter's identification is unsurprising as Indic Skanda and Iranic Sraoša are deities sharing an ancient common origin, as supported by two diagnostic iconographic features. The first is found in the Sraoša yasht of the Avesta:

[स्रओशहे अश्येहे तक्ष्महे तनु-म्ãथ्रहे दर्शि-द्रओश् âह्ûइर्येहे]{style="color:#993366;"}\
[क्ष्श्नओथ्र ýअस्न्âइcअ वह्म्âइcअ क्ष्श्नओथ्र्âइcअ फ़्रसस्तयêcअ ॥]{style="color:#993366;"}\
To the embodiment of universal law, the mighty Sraoša,\
whose body is made of mantra-s, the mighty-speared and lordly god,\
be propitiating ýasna offering, recitation, propitiation, and praise. \[Translation from Avestan modified by me based on Darmesteter]

Here we note that Sraoša's primary weapon like that of Skanda is the spear, a epithet repeated for Sraoša in the Iranian holy book the Fargard 18.

In Fargard 18.23 we have:\
[âat hô sraoshô ashyô aom merekhem frakhrârayeiti parô-darsh nãma spitama zarathushtra ýim mashyâka avi duzhvacanghô kahrkatâs nãma aojaite, âat hô merekhô vâcim baraiti upa ushånghem ýãm sûrãm |]{style="color:#993366;"}\
And then the universal-law embodied Sraoša awakens his rooster named Parodarsh, O Zarathushtra of the Spitama clan, which ill-speaking people call Kahrkatas, and the rooster lifts up his voice against the mighty Ushah \[demonized cognate of Indic dawn goddess the उषस्; Translation from Avestan modified by me based on Darmesteter].

Here were see that Sraoša's bird is the rooster, which is the same as the bird of Skanda.

This identification of Sraoša and Skanda might relate to the fact there is some evidence for the worship of Skanda by Iranians in the Indosphere in first few centuries of the common era \[Footnote 4].\
In this later period where Skanda is explicitly identified with स्रौष we observe दण्डिन् being replaced by Skanda or iconographically converging to Skanda. We can see that in the above image.

**महाश्वेता:** Below the solar deity rising from the pedestal is seen the image of a goddess named महाश्वेता, a key deity of the Saura-mata. She is identified sometimes with the white light emitted by the sun or more commonly with the deity of the earth i.e. पृथिवी.

**The मरीची-s:** Above the two wives are shown two archer-goddesses in the above image. Sometimes there might 4 such archer goddesses. They are known as the मरीची-s or the light goddess and sometime explicitly named as उषस् and प्रत्युषस् when present as a pair. They represent the darkness dispelling function of the sun with the arrow they shoot representing the rays of light. Their warrior nature was combined with elements drawn from another आस्तिक warrior goddess वाराही and essentialized in the bauddha tradition in the form of the solo goddess मरीची.

**अरुण:** The lame charioteer of the sun, the brother of the celestial eagle गरुड, is sometimes shown managing the seven horses of the solar chariot. He can be seen the first of the images shown above.

The Saura pantheon also includes other deities like Revanta and Yama the god of death and the netherworld, both of whom are the sons of विवस्वान्. They are rarely depicted in classic Saura images as those shown above. However, Revanta along with deities of his मण्डल, including पिङ्गल and दण्डिन्, where depicted in images specifically dedicated to him.

Footnote 1: Picture showing reconstructed evolution of Saura-mata\
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-_-Cy1QzETVU/VcrQLOjbqlI/AAAAAAAADVU/Bf9YQCqdb98/s640-Ic42/saura.jpg){width="75%"}
```{=latex}
\end{center}
```



Footnote 2: The short count is 33; the long count given in the Veda as 3339 (A lunar eclipse cycle number, as noted by R. Shamasastry) and subsequently later day Hindus hold that number to be 33 x 107.

Footnote 3: The significance of this number was also transmitted to Japan, where the Indo-Aryan deities inherited via the Bauddha was developed into a Nipponic pantheon of 12 gods drawn from different original categories (द्वादशदेवाः).
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-8WQOI351LrQ/VcrKzp9UGfI/AAAAAAAADU0/G3G78J9_IHY/s800-Ic42/dvAdashadevAH_japan_bW.jpg){width="75%"}
```{=latex}
\end{center}
```



The Nipponic द्वादशदेवाः are (left to right each row): पृथिवी \[Vasu], Soma \[Vasu], Kubera [यक्ष], वायु \[Vasu], वरुण [आदित्य], नैरृत [राक्षस], Brahman, विवस्वान् [आदित्य], Rudra \[Rudra], Indra [आदित्य], Agni \[Vasu], Yama

Footnote 4: For example: We have Skanda depicted on the early Iranic ruler Ayalisha (Azilises) in India. We also have an inscription of an Iranian general in the Kadamba army who records worshiping Skanda in South India and then building a temple for Skanda in Gandhara upon his return.


