
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Rudra's portion of the ritual offering, the कठ आरण्यक and the अथर्वशिरस्: a brief journey through early शैव thought](https://manasataramgini.wordpress.com/2015/11/04/rudras-portion-of-the-ritual-offering-the-ka%e1%b9%adha-ara%e1%b9%87yaka-and-the-atharvasiras-a-brief-journey-through-early-saiva-thought/){rel="bookmark"} {#rudras-portion-of-the-ritual-offering-the-कठ-आरणयक-and-the-अथरवशरस-a-brief-journey-through-early-शव-thought .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 4, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/11/04/rudras-portion-of-the-ritual-offering-the-ka%e1%b9%adha-ara%e1%b9%87yaka-and-the-atharvasiras-a-brief-journey-through-early-saiva-thought/ "2:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A version of this article was published first at [India Facts](http://indiafacts.co.in/rudra-a-brief-journey-through-early-saiva-thought/)

There is a persistent motif of the deva Rudra (शिव) being originally refused ritual offerings of the यज्ञ to which the other deva-s were entitled. Rudra eventually acquires his share of the offering, often via a violent confrontation with the other deva-s. One version of this legend, first seen in the ब्राह्मण texts like the Gopatha ब्राह्मण of the Atharvaveda, has Rudra destroy the ritual of प्रजापति and injure several of the deva-s upon being excluded from the ritual offering (Gopatha उत्तरब्राह्मण 2-4). In parallel in other ब्राह्मण texts there are other narratives of the confrontation between the progenitor deva प्रजापति and Rudra. One of these involves the slaying of प्रजापति by Rudra for the former's act of incest with his own daughter. An allusion to this legend is found in the ऋग्वेद itself where the deva-s are said to call upon Rudra to enforce natural law by preventing the incest of प्रजापति (RV 10.61.7). This is an astronomical legend that preserves memory of the ancient movement of the equinoctical colure towards रोहिणी ( the daughter of प्रजापति i.e. the orange star Alderbaran). In the sky the slain प्रजापति is represented by the constellation of मृगशिरस् (Orion), his semen by the Milky Way, Rudra by आर्द्र (Sirius) the brightest star in the sky (in apparent terms). The Invaka-s, three bright stars in a straight line (the belt of Orion), are said to represent the arrow of Rudra that pierced प्रजापति \[Footnote 1].

By the time of the महभारत the old legends underwent diversification and recombination. One version in the is closer to the Vedic version in the Gopatha ब्राह्मण another one is closer to those found in the पुराण-s. A version generally sharing elements with those found in the महभारत also forms the foundation of the narrative regarding the origin of diseases from the wrath of Rudra in the Hindu medical संहिता-s like those of Caraka. In the पुराण-s various versions of these legends emerge as products of a complex evolutionary process. This resulted in a "standard" version of the legend that is well-known to Hindus today. This combines four basic elements: 1) The suicide of Sati, the wife of Rudra in response to him being insulted by the प्रजापति at the ritual; 2) the retaliatory destruction of the ritual of प्रजापति by Rudra's agents like वीरभद्र; 3) an attack on the remaining deva-s and देवी-s by Rudra's agents resulting in serious injury to them; 4) the slaying of प्रजापति दक्ष himself Rudra or his agents.

The exclusion of Rudra, the destruction of the divine यज्ञ by him, and his subsequent accommodation have been interpreted by some as evidence for Rudra being an outsider to the Vedic religion and being acquired from a non-Aryan source. We hold that this idea is rather untenable. The Vedic religion keeping with the ancestral Indo-European religion had different domains for different gods, and Rudra was by definition a god associated with the dreadful aspects of existence that are placed to exterior to the cocoon of civilization. His exclusion merely reflects his belonging to that realm. His eventual inclusion is an explanation for how the Vedic ritual can make such dangers of the world exterior to civilization beneficent (शिव) to those inside it. Thus, Rudra is regent of disease and toxins, but at the same time he is also the regent of medicines.

That this nature of Rudra was an integral aspect of the Vedic religion is supported by the fact cognates might be found in other branches of Indo-European:

 1.  In the Greek Apollo is the cognate deity of Rudra. He is called both Loimios, i.e. the bringer of plague and Paion i.e. the healer. He is also invoked as Apotropaios meaning one who is called upon to averts the harm that he can inflict. This is reminiscent of the Vedic mantra: "rudrasya हेतिः pari vo वृणक्तु | (May Rudra's missile be averted)". Apollo is a fierce god who is the slayer of many demonic entities even as Rudra is the slayer of many दानव-s and Daitya-s. Finally, in the Homeric hymn to Apollo he is explicitly mentioned as being feared by the other gods as he approaches Olympos (the Greek svarga) with his bow and shining weapons -- a clear parallel to the Vedic situation with Rudra:

I will remember and not be forgetful of the archer Apollo,\
who by the gods is dreaded within Zeus' house as he enters.\
Straightaway all of them leap to their feet as he nearer approaches,\
Out of their seats, so soon as his shining weapons he levels.\
(Homeric hymn to Apollo 1-4; translated by WC Lawton)


 2.  In Germanic tradition Odin is the cognate of Rudra. In what survives of Northern Germanic legend there is an allusion to Odin being the god of the criminals or the outlaws. Likewise, in the long collection of mantra-s to Rudra known as the शतरुद्रीय from the Yajurveda we see him being described as manifesting as various criminals. In the Germanic tradition preserved in Denmark there a specific allusion to a tale where the other gods kept Odin out of the realm of the gods for ten years so that they may not be tarnished by his "dangerous" connections.

Given this brief background we shall consider in greater detail a less-known version of the encounter of Rudra with the deva-s and his eventual acceptance as the great god from the कठ branch of the Yajurveda. The कठ school of the Yajurveda was once widely practiced in Kashmir and hills of the Panjab/Himachal Pradesh. It is today sadly all but extinct due to the depredations of the Mohammedans in those regions. The text in concern comes from the आरण्यक of the कठ-s, which is rather distinct in parts from its cognate of the widely practiced तैत्तिरीय school:

[देवा वै रुद्रं स्वर्गं लोकं गतं न व्यजानन्न् आदित्यवर्णं चरन्तन् । ते ।अब्रुवन् को ।असीति ? अहं रुद्रो ।अहम् इन्द्रो ।अहम् आदित्यो ।अहं सर्वस्यावया हरसो दिव्यस्येति । ते ।अब्रुवन् निर्भजामैनम् इति । तान् रुवन्न् अभ्यवदत । तान् प्राध्रजत् । ते ।अब्रुवन् भवान् सर्वम् इति । यद् रुवन्न् अभ्यवदत् तद् रुद्रस्य रुद्रत्वम् । यद् भवान् इति तद् भवस्य भवत्वम् । यत् सर्वम् इति तच् छर्वस्य शर्वत्वम् । स शिवो ।अभवत् तच् छिवस्य शिवत्वम् । तेभ्यो ।अमृडत तन् मृडस्य मृडत्वम्। तं देवा अब्रुवन् भवस्य भूतस्य भव्यस्याधिपत्यम् इति । सर्वस्याधिपत्यं यजमानं गमयति ॥ क २।१००]{style="color:#0000ff;"}

Indeed the deva-s did not recognize Rudra who had entered the heavenly world wandering in with a solar luster. They said: "Who are you?". \[He replied]: "I am Rudra, I am Indra, I am the आदित्य, I am the arrival of all the divine luster. They \[i.e. other deva-s] said: We shall not offer a share to this one \[i.e. Rudra]. Roaring he \[Rudra] yelled at them. He rushed at them. They \[the other deva-s] said: "Sir, you are all of this". Because roaring he yelled at them that is Rudra's fierceness (rudratvam). Because they called him sir (भवान्) that is Bhava's lordship over existence. Because they said you are all this that revealed शर्व's \[prowess] as an archer. Because he then became favorable that is शिव's benevolence. Because he became kind to them \[the other deva-s] that is मृड's compassion. The deva-s said to him: "The overlordship of the present, the past and the future \[is yours]. \[If he knows this while performing the ritual, i.e. offering the portion for Rudra] it leads the ritualist to lordship over all.

In this आरण्यक section Rudra is given five names. In this it departs from the narratives in the कौशीतकि and शतपथ ब्राह्मण-s, where प्रजापति afraid of Rudra's wrath confers eight and nine names respectively on him. Some of these names correspond to the five names given in the कठ tradition. The ninth name in the शतपथ ब्राह्मण, कुमार, is taken to be that of Rudra's son. The five names are explained by some followers of the कठ tradition as corresponding to the five faces of Rudra whose mantra-s are found in the terminal section of the तैत्तिरीय आरण्यक. Thus, we have:

Rudra : तत्पुरुष (the eastern face associated with the aquiline deity गरुड)\
Bhava: सद्योजात (The western face associated the ghostly hosts of Rudra)\
शर्व: Aghora (The southern face associated with the terrifying Bhairava-s)\
मृड: वामदेव (The northern face associated with the goddess उमा or the शक्ति of Rudra)\
शिव: इशान (The upward face associated with the सदाशिव or the benign form of the god)

This narrative from the कठ आरण्यक provides clues regarding the origin of the frame-narrative of a key early शैव उपनिषद्, the अथर्वशिरस्. Three शैव उपनिषद्-s can be considered early with their probable temporal order being: 1) नीलरुद्र which is associated with the पैप्पलाद school of the Atharvaveda; 2) श्वेताश्वतर which is associated with the तैत्तिरीय (or perhaps originally Caraka) school of the कृष्ण-Yajurveda; 3) अथर्वशिरस् which is associated with the Atharvaveda, though its शख affiliation remains unknown. It survives today as four parallel recensions which share a similar frame narrative. The first two of these उपनिषद्-s contain mantra-s from their respective Vedic संहिता-s that are used in the rituals for Rudra. Of them, the श्वेताश्वतर, presents one of the earliest extant teachings of yoga. अथर्वशिरस् also presents an important early teaching of meditative yoga. Together these form the root of the later शैव yoga practices, which appear serially in traditions known as the अतिमार्ग starting with the पाशुपत सूत्र-s and the mantra-मार्ग, which is expounded in the numerous शैव tantra-s of various streams.

The अथर्वशिरस् opens thus (while the other 3 recensions are similar in this regard; I am using the one I follow in my practice):\
[देवा ह वै स्वर्गलोकम् आयंस् ते रुद्रम् अपृच्छन् को भवान् इति ।]{style="color:#0000ff;"}\
The deva-s indeed went to the heavenly world and they asked Rudra: "Sir, who are you?"

[सो ।अब्रवीद् अहम् एकः प्रथमम् आसीद् वर्तामि च भविष्यामि च नान्यः कश्चिन् मत्तो व्यतिरिक्त इति ।]{style="color:#0000ff;"}\
He \[Rudra] replied: "I am the sole and primal one who ever was, is, and will be; there is nothing else other than mine.

The text thus parallels the कठ आरण्यक but inverts the initial encounter between Rudra and the other deva-s with the other deva-s being the one going to the svarga to see Rudra and Rudra being the one who declares himself as encompassing the three stages of time. The text then continues to identify Rudra with various entities such as the directions, the Veda-s, the ritual fires, cows, and the essences of various entities like the ritual offerings, dharma, natural truth and the like. Then Rudra is said to vanish from the sight of the other deva-s and they are described as praising him identifying him with each of the deva-s. Thereafter, Rudra is connected with the essence of the insight attained upon drinking Soma in the ritual. It uses the following mantra, which is derived version of a ऋक् recited by the ritualist upon drinking Soma:

[अपाम सोमम् अमृता अभूमागन्म ज्योतिर् अविदाम देवान् । किं नूनम् अस्मान् कृणवद् अरातिः । किम् उ धूर्तिर् अमृत मर्त्यस्य सोम-सूर्यपुरस्तात् सूक्ष्मः पुरुषः ।]{style="color:#0000ff;"}\
(Note the aorist (लुङ्) 1st person plural forms of verbs are used, e.g. अपाम for पा= drink, implying context of the recently completed ritual libation)\
We have drunk Soma, we have become immortal, we have attained the light, we have seen the deva-s. What indeed can the evil-doers do to us? What is the mortal's roguishness to the immortal -- \[such is the power stemming from] the subtle being who existed before the sun and moon (i.e. Rudra).

Thereafter the अथर्वशिरस् narrates several functions of Rudra and the epithets corresponding to them and finally gives the teaching of meditative yoga and the teaching of the शैव ritual action of smearing oneself with ash.

The Vedic accounts, including the one in the कठ आरण्यक, are used to illustrate a ritual point or special "insight" by knowing which the ritualist is able to attain success or power. We see that this ancestral frame work is used in the अथर्वशिरस् to present central शैव teachings: 1) the primacy of Rudra; 2) the universal nature of Rudra; 3) finally the method of yoga. In the कठ आरण्यक we already see elements relating to the first two of these points and these are used as is or expanded upon in the उपनिषद्. Where the उपनिषद् reconfigures matters is relates to yoga. Both the आरण्यक and the उपनिषद् presents the knowledge of the nature of Rudra as the means of achieving power via the ritual (the latter accepts the attainment of such power upon performing the soma rite via the grace of Rudra). However, it additionally presents yoga as the means to an insight that releases one from a certain bondage that is characterized as the being similar to the bondage of a domestic animal (the पशु-पाश). It is this freeing insight that came to be key concern of subsequent शैव tradition. At the same time it should be emphasized that even later शैव tradition did not lose sight of the acquisition of power either. Thus, via the कठ आरण्यक we can trace how the old encounter of Rudra with the other deva-s evolved into the frame in which शैव teaching was presented.

Footnote 1: As the great patriot Lokamanya Tilak pointed out the three Invaka stars also represent the belt/girdle or उपवीत of प्रजापति. Indeed, even to this date when observers of Vedic tradition change their उपवीत-s they use a mantra in which the girdle of प्रजापति at the head of the ecliptic path of नक्षत्र-s is invoked.


