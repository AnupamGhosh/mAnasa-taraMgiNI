
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Exploring the history of Hindu festivals: the ancient strands of होलाका](https://manasataramgini.wordpress.com/2015/03/07/exploring-the-history-of-hindu-festivals-the-ancient-strands-of-holaka/){rel="bookmark"} {#exploring-the-history-of-hindu-festivals-the-ancient-strands-of-हलक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 7, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/03/07/exploring-the-history-of-hindu-festivals-the-ancient-strands-of-holaka/ "2:07 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Updated version of article published originally at ईन्दिअFअcत्स्](http://indiafacts.co.in/exploring-the-history-of-hindu-festivals-the-ancient-strands-of-holaka/)

In Hindu tradition there is a clear demarcation of at least three distinct classes of ritual observances: 1) The most conservative of these are the श्रौत rituals that deviate little from their Vedic prototype specified in the texts known as the ब्रह्मण-s and श्रौत सूत्र-s. 2) The next are the गृह्य or domestic rituals, which are associated with the major events in an individual's life such as birth, naming, studentship, marriage, setting up of a household, and death. These show a conservative core going back to the earliest Vedic age or earlier, specified in texts known as the गृह्य सूत्र-s, along with later accretions coming from texts known as the पुराण-s, local customs and sectarian traditions. 3) Finally, we have the festive observances, which are followed by the whole of Hindu society including the lay people. Examples of these include Indradhvaja, दीपावली, होलाका (commonly called Holi in Northern parts of India) and vasanta-पञ्चमी.

Of the three, the श्रौत rituals are practiced by very few people today and are largely unknown to the modern lay Hindus even though the foundations of their dharma lie in these rituals. The गृह्य traditions are somewhat more widely known, though they too are declining among the Hindus of urban India. In contrast, the festive observances are still widely known and practiced. However, unlike the श्रौत and गृह्य rituals the festive observance are much less tethered to the canonical texts and are greatly prone to local variations. Indeed, this distinction is clearly recognized by the great theorists of ritual in Hindu tradition, i.e. the commentators of the मीमांस system, who explicitly distinguish these festivals from the rituals ordained by the words of the Veda. Nevertheless, these festivals are likely to have been of great antiquity in the Indo-Aryan world because at least some of them correspond to festivals of comparable intent observed elsewhere in the Indo-European world. The earliest references to these festivals are seen in the सूत्र-s of the 18th परिशिष्ठ of the Atharvaveda (the Utsava-सूत्राणि), which provides a list of such observances that are to be supported by the state.

We believe it is important that the history of these rituals be closely studied as it provides clues to understand our past and the role they played in the well-being of the people. Indeed, it was for this reason the great king Bhojadeva परमार paid great attention to their description and observance. Two centuries later these observances were studied and described at length by the great encyclopedist हेमाद्रि in his Caturvarga-चिन्तामणि. Unfortunately, the loss of Hindu power to Islam and Christianity resulted in the memory of the old practices being forgotten to a great degree. In our times the systematic study of the early lay or social observances of भारत was done by the great Sanskritist V. Raghavan. His work was published with assistance of his successor S.Janaki because of his death before it saw print. Our intention here is to merely revive the study of these observances with an examination of the early history of होलाका. We must stress what we present here is largely indebted to Raghavan's work along with some additional observations.

The earliest mention of होलाका is in the 18th परिशिष्ठ of the Atharvaveda in the form a brief सूत्र:\
[अथ फाल्गुन्यां पौर्णमास्यां रात्रौ होलाका ॥ ]{style="color:#99cc00;"}AV 18.12.1\
Now on the night of the फाल्गुनि full moon is होलाका.

This continues to be its date of observance to the current day. The verse of the गाथसप्तशति of the Andhra king हाल refers to getting "dirty" in the फाल्गुणि festival:\
*[फाल्गुनोत्सव-निर्दोषं केनापि कर्दम प्रसाधनं दत्तम् ।]{style="color:#99cc00;"}*\
*[स्तन-कलश-मुख-प्रलुठत् स्वेद-धौतम् किमिति धावयसि ॥]{style="color:#99cc00;"}* 37/4.69 (provided in Sanskrit for easier understanding)

\[The man addressing his female friend says]:\
In the फाल्गुणि festival someone innocently colored you by throwing dust,\
Why are you trying to wash that away, when it has been washed,\
by the sweat flowing off the nipples of your pitcher-like breasts?

The preparation of powder for throwing in the festival is also alluded to in the same context in the गाथसप्तशति\
*[मुख-पुण्डरीकच्-छायायां संस्थितौ पश्यत राजहंसाविव ।]{style="color:#99cc00;"}*\
*[क्षण-पिष्ट-कुट्टनोच्छलित-धूलि-धवलौ स्तनौ वहति ॥]{style="color:#99cc00;"}*39/6.24

Look! Sitting in the shadow of the lotus which is her face,\
dusted by the powder thrown up as she grinds for the festival,\
are her two fair breasts sitting like a pair of royal swans.

Not unexpectedly, such frolicking in the festival could have negative consequences. Indeed, a महाराष्ट्री प्राकृत गाथ attributed to the same work of the Andhra monarch preserved only in the Telugu country sarcastically states:\
*[खण-पिट्ठ-धूसर-त्थणि महु-मअतंब्-अच्छि कुवल-आभरणे ।]{style="color:#33cccc;"}*\
*[कण्ण-गअ-चूअ-मंजरि पुत्ति तुए मंडिओ गामो ॥]{style="color:#33cccc;"}* 38/8.26

With breasts colored by the festival's powder,\
eyes showing intoxication by liquor,\
with a lotus as ornament and mango shoot behind the ear,\
you are, girl, a real honor to our village!

Thus, one may say that by the beginning of the Common Era when the Andhra-s held sway, the key elements which define होलाका were already in place: the color play and the drunken revelry. These are mentioned in authoritative medieval digests on festivals which collect material from earlier texts. For instance, the वर्षकृत्या-दीपिका says that the people smear themselves with ashes from a bonfire (see below) and color powders and prance about like पिशाच-s on the streets ([ग्राम-मार्गे क्रीडितव्यं पिशाचवत्]{style="color:#99cc00;"}).These are features of the festival that persist to the current day.

However, these are not the only elements that characterize the festival. हेमाद्रि in his account of the होलाका festival provides information from the now lost account of the भविष्योत्तर पुराण. This records an interesting tale that is not widely known among modern Hindus:\
*"When Raghu was the emperor of the इक्षवाकु-s at अयोध्या, the lord of लन्का was a राक्षस known as मालिन्. His daughter was a राक्षसी known as ढेण्ढा (In some texts ढुण्ढा). She attacked the city of अयोध्या and wrought much havoc by slaying the children in the city. Raghu advised by his preceptor वसिष्ठ asked the people, particularly the youngsters, to gather cow dung, leaves and logs, and place them at the center of a decorated enclosure. They then set these afire and went around the pyre shouting, singing and calling out obscene words including the names of male and female genitalia in देश-भाषा-s. Then they clapped their hands, made a noise by striking their open palm against the open mouth (bom-बोम्कार) and shouted out the words अडाडा and शीतोष्ण. Surprised by the obscene language ढेण्ढा started running and fell into the pyre and was burnt to death."*

In this account अडाडा is described as the mantra of होलाका by which the राक्षसी is driven away and the fire is said to be the homa in which this mantra is practiced to bring welfare to the settlement.

Several variants of this basic form of the festival are seen in medieval manuals for festivals. The Jyotir-nibandha specifies that the fire for the होलाका pyre should be brought by children from the house of a चण्डाल woman who has just given birth. It mentions an effigy of ढेण्ढा along with a five-colored flag being set up for burning. The पुरुषार्थ-चिन्तामणि additionally specifies a cattle race at midday for the होलाका festival. A paddhati from the Tamil country specifies that scorpions, snakes and centipedes are made out of molasses and thrown into the ढेण्ढा pyre.

The legend of ढेण्ढा has been recycled into two वैष्णव narratives which are more popular today: 1) She is known as होलिका, the sister of हिरण्यकशिपु, who loses her invulnerability to fire and perishes in an attempt to burn her nephew the daitya प्रह्राद. 2) The होलाका fire is supposed to commemorate the killing of the राक्षसी पूतना by कृष्ण देवकीपुत्र -- पूतना was originally a fierce कौमार goddess who was completely demonized in the वैष्णव narrative.

The common element in all these narratives is the protection of children from harm. Indeed the कौमार goddess पूतना is described as being a deity of pediatric illnesses, from which she provides relief upon being given ritual fire offerings and bali. The junction period between winter and summer in India is marked by several illness that afflict children. This might indeed have been the rationale behind this facet of होलाका. Likewise, in rural India, the coming of summer heralded the emergence of scorpions, centipedes and snakes from hibernation. This appears to have found expression in the ritual offering of images of these animals in the होलाका fire.

Unlike the वैष्णव-s, the शाक्त-s gives a positive color to the narrative of होलिका, wherein she is described as an incarnation or emanation of चण्डिका, who fought a great battle with a daitya known as वीरसेन, and slew him on this day. Thus, it is his effigy which is burned accompanied by the worship of होलिका देवी, followed by the शाक्त observance of the Vasanta-नवरात्री. Thus, it is symmetrically placed in the calender with respect to the exploits of the great trans-functional goddess celebrated in the autumnal नवरात्री. This account is elaborated in an eastern text known as the होलिकामाहात्म्यम्.

Thus, multiple elements have been melded together into the होलाका festival. Of these the element involving the color play and obscenity probably relate to it being an ancient festival of love. Indeed, an aspect of this is obliquely recorded in the नारद-पुराण by noting that it marks the burning of काम by Rudra -- a feature which survives to the current date in the form of the green twig representing काम being placed in the होलाका bonfire. In certain accounts the people from the आर्य वर्ण-s freely touched people from lower जाति-s on this occasion, and this action was supposed to help provide immunity from diseases. Thus, the festival might have additionally had an angle of establishing social cohesion.

Finally, right from the first few centuries of the Common Era, as indicated by the great मीमांस commentator शभरस्वामिन्, होलाका appeared to have had a patchy, regional pattern of observance. According to him it was observed only by easterners. Such a regionally restricted pattern is observed even today with the festival lacking prominence in much of the peninsular south. This is paradoxical because it appears to be an early festival alongside the ancient Indradhvaja and दीपावली. Moreover, it is attested in texts from all over India including places like कुम्भघोण in Tamil Nad where the festival in no longer observed. One possible explanation for this is that frivolous and obscene facets of the festival have resulted in being ignored in several parts of the nation \[Footnote 1]. On the other hand in other places it was "domesticated" to a degree and continued to be observed. However, in very recent times it seems to be resurgent in several places where it was previously not observed. Hence, it is possible that it can be used as a means to counter imported western observances that serve as conduits for Abrahamistic memes.

Footnote 1: In dhammapada 2.4 the buddha Gautama criticizes the festival celebrated in श्रावस्ती as one of fools making particular reference to the coarse language being used in the celebrations.


