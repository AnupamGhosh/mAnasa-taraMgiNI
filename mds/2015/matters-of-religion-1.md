
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion-1](https://manasataramgini.wordpress.com/2015/12/28/matters-of-religion-1/){rel="bookmark"} {#matters-of-religion-1 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 28, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/12/28/matters-of-religion-1/ "8:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Indrasena had arrived at the house of Somakhya and Lootika for learning mysterious matters of religion. Entering their fire-room he saw Somakhya seated beside the square altar on a mat of bovine skin and Lootika seated beside the circular altar on a similar skin with a yoktra girdle around her waist. Indrasena seating himself in the स्वस्तिकासन on a mat at the appropriate corner said: "Agnishad, the ब्राह्मण from Cerapada, was a notable सोमयाजिन्. Having performed the अग्निष्टोम he drank soma. But he apparently did not know the rahasya rituals. Hence, one day he was struck by the vajra of deva Rudra and was killed. I do not wish to go that way. O Somakhya and Lootika I seek the rahasya-s pertaining the mysterious rituals that you were to impart to me."

Somakhya: "These rituals are indeed mysterious. We cannot claim by any means to have understood their mysteries in completeness. They certainly lie beyond the reach of the nominal ब्राह्मण, who has not penetrated the depths of the श्रुति or merely carries the श्रुति as a donkey carrying sandalwood. We, following the path of the great Atharvan-s who have gone before us to the realm of the frightful Yama, have grasped the merest tip of the iceberg of mysteries placed in the श्रुति by the ऋषि-s of yore. You need not be told this O Indrasena, still let me repeat, such mysteries can only be understood by making the offerings in the realm that is known as Vivasvant's seat. When the यजमान-s make offerings without knowing Vivasvant's seat he is exposed to the danger of becoming a samidh in the offering to Vivasvant's black son before his time. Even if does know it, he remains in danger, for the darts of पशुपति are many. Now dear Lootika let us ensure that you are fit to be the mistress of the ritual mysteries in this instruction of our friend Indrasena: Expound the seat of Vivasvant."

Lootika: "In the सूक्त whose seer is unknown there is the त्रिष्टुभ् mantra:\
[यस्मिन् देवा विदथे मादयन्ते]{style="color:#0000ff;"}\
[विवस्वतः सदने धारयन्ते ।]{style="color:#0000ff;"}\
[सूर्ये ज्योतिर् अदधुर् मास्य् अक्तून्]{style="color:#0000ff;"}\
[परि द्योतनिं चरतो अजस्रा ॥]{style="color:#0000ff;"}\
(In which ritual assembly the deva-s are exhilarated,\
in the seat of Vivasvant they cause to uphold \[the laws]\
they placed the light in the sun and nights in the moon\
the two move the splendor in cycles unceasingly.)

The deva-s uphold the vrata-s or the laws which govern all existence in the seat of Vivasvant. To the old आर्य-s the glowing of the sun, the lunar phases, and the cyclic movement of the sun and the moon in the sky, were embodiments of these laws. That seat of Vivasvant is hence the universe. At its origin the deva-s manifested as those laws. But the seat of Vivasvant is also the ritual assembly where the deva-s are exhilarated with the offerings of the यजमान. Thus, when the ritualist performs a ritual the ritual arena becomes an abstraction of the seat of Vivasvant."

Somakhya: "Good, you are keeping with your founding father vipra दीर्घतमस् औचाथ्य. Indrasena would you back up Lootika's statement that the deva-s are upholding the vrata-s?"

Indrasena: "An ancient आङ्गिरस states:\
[यस्या देवा उपस्थे]{style="color:#0000ff;"}\
[व्रता विश्वे धारयन्ते ।]{style="color:#0000ff;"}\
[सूर्यामासा दृशे कम् ॥]{style="color:#0000ff;"}

(She in whose lap, deva-s\
cause to uphold all laws,\
for seeing the sun and moon.)

Here, we see a parallel to the mantra Lootika expounded and vrata is explicitly mentioned. Just as the seat of Vivasvant is the universe represented as the ritual arena, so also the lap of the goddess पृश्नि, the consort of Rudra and the mother of the Marut-s, is also a metaphor for the same, the female cognate of the same. Hence, Lootika was correct in supplying vrata-s in the previous mantra."

Somakhya: "Good. With the knowledge of Vivasvant's seat and its other cognates in place the ritualist must call upon Vivasvant so that he makes us fearless of the fears culminating in the approach of his son. So the ritualist utters the incantation:\
[विवस्वान् नो अभ्यं कृणोतु ।]{style="color:#0000ff;"}\
(May Vivasvant make us fearless.)

Now Indrasena drink the soma from the graha and utter the mantra of the 'avarodhana' (the descending bridge) to protect yourself from Yama Vaivasvata and expound it."

Indrasena: "The ऋषि कश्यप, who was one of the first Soma ritualists, said in his mantra:\
[अत्र राजा वैवस्वतो यत्रावरोधनं दिवः ।]{style="color:#0000ff;"}\
[यत्रामूर् यह्वतीर् आपस् तत्र माम् अमृतं कृधी इन्द्रायेन्दो परि स्रव ॥]{style="color:#0000ff;"}\
(Here where Vaivasvata is king, there is a descending bridge from the heaven,\
where the fresh, ever-flowing waters are, there make me immortal, may the Soma drop flow around for Indra!)

The bridge, which descends from the divine realm to that of Vaivasvata, should be seen as the stars in the region of the constellation of Orion, where the two dogs of Yama (Canis Major and Canis Minor) stand guard. The fresh ever-flowing waters there represent the Milky Way. The Indu in the ritual arena is the Ephedra juice but in the celestial realm is actually the moon, the celestial Soma. That is why the prefix pari is used with verb sru."

Somakhya then continued: "You have mastered the rahasya of the avarodhana and thus protected yourself. The ritualist then needs to be mindful of the link to the seat of Vivasvant, which is like an umbilical cord connecting him during the ritual, where he experiences the presence of the deva-s. This has been laid out by the prince Parucchepa दैवोदासि in his mantra thusly:\
[अस्तु श्रौषट् पुरो अग्नीं धिया दध]{style="color:#0000ff;"}\
[आ नु तच् छर्धो दिव्यं वृणीमह इन्द्रवायू वृणीमहे ।]{style="color:#0000ff;"}\
[यद् ध क्राणा विवस्वति नाभा संदायि नव्यसी ।]{style="color:#0000ff;"}\
[अध प्र सू न उप यन्तु धीतयो देवां अच्छा न धीतयः ॥]{style="color:#0000ff;"}\
(So be it, he shall hear! I place Agni in front through my mantra-thought,\
Now here we choose the divine host (Marut-s); we chose Indra and Vayu.\
When indeed we are swiftly linked to the nave in Vivasvant's \[seat],\
then may our mantra-thoughts travel forth, as if the mantra-thoughts are going towards the deva-s.)

Note that the astu श्रौषट् is one of the five ritual calls that are said to constitute the ritual itself and also stand for the five animals the old आर्य-s greatly valued. They are\
[ओ३ श्रावय]{style="color:#0000ff;"} (Make the देवता hear)\
[अस्तु श्रौषट्]{style="color:#0000ff;"}(So be it, he shall hear)\
[यज]{style="color:#0000ff;"}(worship \[the देवता])\
[ये३ यजामहे]{style="color:#0000ff;"}(we are those who worship) \[the देवता]\
[वौ३षट्]{style="color:#0000ff;"} (May \[Agni] bear the offering \[to the deva-s]

Thus, having known this, at the seat of Vivasvant he invokes the fierce Indra the lord of the battle."

Looking at fire at his altar Somakhya gave the call: "[इन्द्रम् आ३ वह ।]{style="color:#0000ff;"}"\
He then followed it with the ghee offering directly with the sruva: [ये३ यजामहे इन्द्रं स्वाहा। इदम् इन्द्राय न मम ।]{style="color:#0000ff;"}

Indrasena note that these स्वाहा offerings should be made with the देवता in the accusative case because the deity is the object of the verb यजामहे.

Somakhya invoked Indra: "\
[युधेन्द्रो मह्ना वरिवश् चकार]{style="color:#0000ff;"}\
[देवेभ्यः सत्पतिश् चर्षणिप्राः ।]{style="color:#0000ff;"}\
[विवस्वतः सदने अस्य तानि]{style="color:#0000ff;"}\
[विप्रा उक्थेभिः कवयो गृणन्ति ॥ श्रौ३षट्]{style="color:#0000ff;"}\
(In battle through his greatness Indra had made the wide space for the deva-s,\
he is the lord of all that exists, the leader of all the peoples.\
At the seat of Vivasvant these \[acts] of his\
the vipra-s, the kavi-s recite with their chants.)"

Then he said: "With this mantra of the great ऋषि विश्वामित्र we should invoke Indra, who through his great acts in battle made the universe that of the deva-s, and then with the name of विष्णु filling the sruk with the sruva make the offering.\
[ये३ यजामहे]{style="color:#0000ff;"}\
[ससानात्यां उत सूर्यं ससानेन्द्रः]{style="color:#0000ff;"}\
[ससान पुरुभोजसं गाम् ।]{style="color:#0000ff;"}\
[हिरण्ययम् उत भोगं ससान]{style="color:#0000ff;"}\
[हत्वी दस्यून् प्रार्यं वर्णम् आवत् ॥ वौ३षट् । इदम् इन्द्राय न मम ॥]{style="color:#0000ff;"}\
(Indra won the horses, and also he gained the sun,\
he gained the cow, the source of much food,\
and also the wealth of gold he conquered;\
Having slain the dasyu-s he aided the आर्य color.)

Thus, we must worship Indra so that we are sthavira-s in the battle with the dasyu-s."

Turning to Lootika, Somakhya called upon her: "[ऐन्द्र्या गार्हपत्यम् उपतिष्ठस्व ।]{style="color:#0000ff;"}"\
Lootika then clarified this point to Indrasena: "Note the use of the verb upa-स्था in the आत्मनेपद. This has been specified by the सूत्र: [उपान् मन्त्र-करणे ।]{style="color:#0000ff;"} of sage पाणिनि. When the when prefix upa is used with verb स्था in the context of ritual to the deva-s then the verb takes the आत्मनेपद.

The Lootika continued with the invocation at her altar:\
"[अहस्ता यद् अपदी वर्धत क्षाः शचीभिर् वेद्यानाम् ।]{style="color:#0000ff;"}\
[शुष्णम् परि प्रदक्षिणिद् विश्वायवे नि शिश्नथः ॥ श्रौ३षट्]{style="color:#0000ff;"}\
(When handless and footless the earth grew by her powers of wise means,\
you went clockwise around शुष्ण and for all life \[or for all the आयु-s ] you struck him down.)"

Then she made an offering with the जुहू ladle into the fire at her altar:\
[ye यजामहे\
व्य् आनऴ् इन्द्रः पृतनाः स्वोजा आस्मै यतन्ते सख्याय पूर्वीः ।]{style="color:#0000ff;"}\
[आ स्मा रथं न पृतनासु तिष्ठ यम् भद्रया सुमत्या चोदयासे ॥ वौ३षट् । इदम् इन्द्राय न मम ॥]{style="color:#0000ff;"}\
(The most mighty indra has pierced the enemy ranks;\
Many foremost \[warriors] go to him for friendship.\
\[O Indra\]mount your car as though for battle;\
you drive forth for \[bringing] fortune and favor.)

Then Somakhya stood up recited the praise of Indra:\
"[त्वम् आयसम् प्रति वर्तयो गोर्]{style="color:#0000ff;"}\
[दिवो अश्मानम् उपनीतम् ऋभ्वा ।]{style="color:#0000ff;"}\
[कुत्साय यत्र पुरुहूत वन्वञ्]{style="color:#0000ff;"}\
[छुष्णम् अनन्तैः परियासि वधैः ॥+ ॐ]{style="color:#0000ff;"}\
(You, the skillful one, spun away the metal stone of heaven led close to the cow;\
when for Kutsa, O much invoked one, winning over शुष्ण you surrounded him with endless deadly missiles.)

[पुरा यत् सूरस् तमसो अपीतेस्]{style="color:#0000ff;"}\
[तम् अद्रिवः फलिगं हेतिम् अस्य ।]{style="color:#0000ff;"}\
[शुष्णस्य चित् परिहितं यद् ओजो]{style="color:#0000ff;"}\
[दिवस् परि सुग्रथितं तद् आदः ॥]{style="color:#0000ff;"}\
(Before when the sun enters the darkness, hurl the missile at the cave, O one with the stones!\
Indeed, the might of शुष्ण which surrounded him, that you tore off from the heaven \[though] it was well-knotted around \[it].

[इन्द्रो दिवः प्रतिमानम् पृथिव्या]{style="color:#0000ff;"}\
[विश्वा वेद सवना हन्ति शुष्णम् ।]{style="color:#0000ff;"}\
[महीं चिद् द्याम् आतनोत् सूर्येण]{style="color:#0000ff;"}\
[चास्कम्भ चित् कम्भनेन स्कभीयान् ॥+ॐ]{style="color:#0000ff;"}\
(Indra is the image of heaven and earth,\
he knows all the \[Soma] pressings \[and] slays शुष्ण,\
indeed, earth and heaven he extended with the sun,\
He verily held up the world by the world-axis, he is the primary world axis.)"

Indrasena: "There are many mysteries here that are not easily grasped."\
Somakhya: "That is true. Some of them lie beyond our reach because the original aindra religion of our ancestors has been fogged by the later dominance of the divergent sects. Yet,we must admire the great effort of prime minister सायण who attempted to penetrate these mysteries as one of the first tasks when the nation of the Hindus was revived from the शुष्ण-like grip of the मरून्मत्त-s."\
Indrasena: "No doubt we must uphold the tradition of शौनक as that सोमपायिन् who followed the deva-dharma from the राष्ट्रक country had said to me when I was young. We must labor on with what survives, deprecating the kautsa ignorance that spread among the ritualists."

Lootika: "The lesser mysteries are those of grammar. Indrasena, you might have noticed that the reduplicated perfect form of the verb skambh as being चास्कम्भ. In every day language we use caskambha. This elongation of the first syllable in the श्रुति is accounted for by the great grammarian under the सूत्र: '[तुजादीनाम् दीर्घो 'भ्यासस्य ।]{style="color:#0000ff;"}'..."\
Indrasena: "Yes. Another well-known example of that would be the perfect verb in: '[स दाधार यः पृथिवीं द्याम् उत ।]{style="color:#0000ff;"}'"\
Lootika: "Correct. Further, because it is लिट् of skambh it refers to the ancient, unwitnessed past (i.e.[परोक्षे लिट्]{style="color:#0000ff;"}). Hence it refers to the time of the origin, when Indra made the universe expand. He is that which holds the boundaries of the universe wide apart; hence he is called the स्कभीयान्. Another grammatical point of note in this regard is the loss of the 's' from the word skambhana in the श्रुति after the cit. Thus, we have cit kambhanena. Indra is also the model of the universe itself; hence, [इन्द्रो दिवः प्रतिमानम् पृथिव्या]{style="color:#0000ff;"}."\
Indrasena: "Indeed, that is affirmed by ऋषि गृत्समद: [यो विश्वस्य प्रतिमानम् बभूव ।]{style="color:#0000ff;"}\
Somakhya: "Good. This, O Indrasena, is the rahasya the Atharvan is mindful off when he utters the incantation: [ॐ भूर् भुवः सुवर् जनद् वृधत् करद् रुहन् महत् तच् छम् ओम् इन्द्रवतो हवामहे ।]{style="color:#0000ff;"}

To continue on the matter of grammar that Lootika brought up, as a historical aside, it appears that the reduplicated लिट् was at best weak in the core Indo-European language. It appears to have developed as an obligate form, which we see in our language, only in the group that includes Graeco-Armenian and Indo-Iranian. We suspect that this supports a Graeco-Armeno-Aryan clade in the ancestor of which this development occurred in the Caucasus region. Under this hypothesis the satem development happened after the Indo-Iranian split off and then moved in proximity to the clade Balto-Slavic to influence it."

Indrasena: "The Graeco-Armeno-Aryan clade is also probably supported by the emergence of the prefix-augmented imperfects (लङ्) and aorists (लुङ्) occurring along side the unaugmented forms resembling them."

Somakhya: "I also tend to favor that view -- the simultaneous occurrence of the augmented and unaugmented forms is likely to be a synapomorphy of the Graeco-Armeno-Aryan clade. However, the repeated loss of it, both on the Greek and our side warrants us to be a bit cautious. While we are talking about these forms you might have noted that prati वर्तयः and vardhata in the deployed mantra-s, which are such unaugmented parasmai- and आत्मने-pada formations."

Lootika: "The grammar, with links to earlier IE clades, apart, the semantics are where the greater mysteries lie. One mystery pertains to what the 'growing of the of the Earth' means?"\
Somakhya: "The learned prime minister holds that it represents the growth of vegetation on the earth. Given that the दानव शुष्ण is said to be the demon of drought, when he enveloped the earth, the earth by her 'wise means' i.e. counter-माया made the vegetation grow in spite of him. But there are many other mysteries that are much harder to unveil."

Lootika: "What is the metal stone of the heaven, alluded to in multiple mantra-s? What is the cow which from which it was turned away? Why does Indra always have to go around or surround शुष्ण, as though circling the sacrificial animal, before killing him? What are the knots by which शुष्ण is well-tethered to the heavens?"

Indrasena: "These indeed seem rather mysterious when one compares it to the one good survival of the शुष्ण legend I am aware of from the ब्राह्मण portion of the काठक-संहिता:\
'The deva-s and the asura-s were at war. But अमृत was with the asura-s, within शुष्ण. Whenever a deva would be killed he could not be revived. But when the deva-s killed an asura, शुष्ण would breath upon him with the अमृत and revive him. Indra realized that the अमृत was with the asura-s within शुष्ण. So Indra took the form of a ball of honey and placed himself on शुष्ण's path. शुष्ण ate it up. Indra immediately became a golden eagle and seizing the अमृत flew out of शुष्ण's mouth. Hence, the golden eagle is the mightiest of the birds because he is one form of Indra. He who knows this possesses the अमृतविद्या.' There is some subliminal connection between this later reflex of the legend and the earlier ones in the आदिश्रुति. What could it be?"

Somakhya: "While not all the mysteries are clear to us it is indeed true that these legends have a connection. We posit that the शुष्ण cycle had many branches of which only one survives in any detail. There was an earthly version which was associated was probably associated with शुष्ण as the bringer of drought. There was a heavenly version, which took multiple forms itself: There are indirect allusions to शुष्ण's serpentine nature, which might relate to him tying himself around the heavens (also perhaps supported by Eastern Iranic etymologies for snake). There is also a form of the legend where Indra is said to steal the wheel of the sun in the sky during the शुष्ण battle. E.g. ऋशि Agastya says:\
[मुषाय सूर्यं कवे]{style="color:#0000ff;"}\
[चक्रम् ईशान ओजसा ।]{style="color:#0000ff;"}\
[वह शुष्णाय वधं]{style="color:#0000ff;"}\
[कुत्सं वातस्याश्वैः ॥]{style="color:#0000ff;"}\
O kavi \[Indra] steal the wheel, \[i.e.] the sun,\
he \[Indra] who by his might is the overlord,\
bear Kutsa with the horses of wind,\
for the slaying of शुष्ण

There is the allusion to the sun about head into the darkness of the cave when Indra deploys his missiles. We propose that the cow from which Indra turns away the metal stone is again the sun. We do think these allusions are indeed related to the later reflex of the legend of the काठक-s that you narrated. The ball of honey is the sun -- a metaphor given the yellowness of honey. Hence, we posit that mysteries of these legends of शुष्ण -- the stealing of the sun-wheel, the stone of heaven approaching the sun, शुष्ण eating the honey ball, Indra slaying शुष्ण just as the sun was entering the darkness of the cave -- are all associated with the phenomenon of the eclipse. I am sure you see the parallels to the even later reverberations of these through the sphere of legends."

Indrasena: "How could one not? In the विष्णु cycle the serpentine राहु swallows the sun; He is one among the asura-s who held the अमृत in his mouth; he achieved it by deploying his माया; the अमृत was stolen from the asura-s by विष्णु in his female form. विष्णु severs राहु's head with the cakra. This parallels another legend in which Indra too wields the sun as his cakra to slay Vala; गृत्समद states:\
[अवर्तयत् सूर्यो न चक्रम्]{style="color:#0000ff;"}\
[भिनद् वलम् इन्द्रो अङ्गिरस्वान् ॥]{style="color:#0000ff;"}"\
Lootika: "Thus, we can see many motifs come together in different ways. The eagle stealing the अमृत is of old Indo-European vintage. Moving further afield, the motif of stealing the आमृत from asura-s reappears in the later legend of आङ्गिरस Kaca stealing the संजीवनी from उशनस् काव्य while being within him, having entered via his beer. The same उशनस् काव्य is associated with the शुष्ण-hatya along with Kutsa आर्जुनेय; in this legend perhaps the Kutsa is a heavenly figure rather than the आङ्गिरस ऋषि. And then perhaps Indra going around or encircling शुष्ण is an allusion to circlings of the heavenly bodies in course of the phenomena like eclipses."

Somakhya: "Some of these are still at the edge of the veil; some mysteries still remain but mindful of the rahasya-s we do know we must thus perform the ritual to Maghavan."


