
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Comet Lovejoy, C/2014 Q2](https://manasataramgini.wordpress.com/2015/01/18/comet-lovejoy-c2014-q2/){rel="bookmark"} {#comet-lovejoy-c2014-q2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 18, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/01/18/comet-lovejoy-c2014-q2/ "7:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-2_gZwn1jB8Y/VLtkW4OZm_I/AAAAAAAADN4/gLP7HtjLOSM/s800/Lovejoy.JPG){width="75%"}
```{=latex}
\end{center}
```



On पौष कृष्णपक्ष 11, kali 5115 (16th Jan 2015) around 8.10 PM, braving the cold of the height of winter (felt like -5°C) we caught sight of श्वेतकेतु in the constellation of Taurus near the 6 mothers of our patron deity. It was a धूमगोल around the magnitude of 4.3 and clearly visible through our binoculars (20X70) and was about barely visible to naked eye close to the limiting magnitude from our bad observing site. It had a faint tail that was hardly discernible from our site via our instrument. Nevertheless we would place it as one of the brighter and memorable comets of our life. It could be located fairly easily by using the कृत्तिकाः as the signpost. Thus, it was like a Skanda-graha coursing through the welkin. Indeed, the ancient jaina-s imagined Skanda as a comet emerging from the कृत्तिकाः and coursing to भारतवर्ष to take the embryo of the future तीर्थंकर the nagna to place him in a क्षत्रिय womb after removing all the ब्राह्मण molecules from his body and replacing them with clean ones \[Indeed the ब्राह्मण hatred of the nagna-s began early!].

Orbit of Comet Lovejoy, C/2014 Q2\
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-0irTUhDJP-k/VLtkWqLWcMI/AAAAAAAADN0/9clwl5XqSiU/s800/Lovejoy_orbit.jpg){width="75%"}
```{=latex}
\end{center}
```



Update: On पौष कृष्णपक्ष 13, kali 5115 (18th Jan 2015) a new observation was made. The comet had cleared moved from Taurus into Aries. Much of the day there was heavy cloud cover and rain which suddenly cleared in the evening leaving behind excellent skies. The comet was more easily seen with naked eye on this day. It appeared to be at the same brightness with a faint hint of a tail in the direction of the Pleiades.

Below is a more zoomed out view of its orbit which gives a feel for the enormity of the distance of the Oort's cloud, where comets originate, from the planets and the Kuiper belt objects like Pluto, Eris and Makemake with their highly inclined orbits.\
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-nFZg94Zg1AE/VLxmS9xBO7I/AAAAAAAADOI/1nshWceYrxQ/s800/Lovejoy2.JPG){width="75%"}
```{=latex}
\end{center}
```




