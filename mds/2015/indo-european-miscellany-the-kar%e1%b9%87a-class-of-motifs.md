
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Indo-European miscellany: The कर्ण class of motifs](https://manasataramgini.wordpress.com/2015/06/06/indo-european-miscellany-the-kar%e1%b9%87a-class-of-motifs/){rel="bookmark"} {#indo-european-miscellany-the-करण-class-of-motifs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 6, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/06/06/indo-european-miscellany-the-kar%e1%b9%87a-class-of-motifs/ "7:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As we have said many times before most Hindus, despite being the last holders of the Indo-European tradition, have done little in recent times to use their unique position to develop further understanding of their ancestral system. In large part this stems from their: 1) unwillingness to study other IE languages and texts in detail; 2) laziness in putting the effort to understand the comparative method; 3) tendency to get caught up in moronic fads such as AIT denial (!), OIT and the like.

As a result much of the comparative work is done by western Indo-Europeanists, white Indologists and Dumézilianists. While these tend to see some parts of the picture, they are disadvantaged by no longer belonging to a real IE tradition. In particular, the Dumézilianists who saw the farthest tend to constrain themselves by overdoing the typological tripartite system proposed by Dumézil. Wikander, Hiltebeitel, and West, who also did related comparative work, suffer from the failure to see the deeper permeation of the IE tradition in Hindu lore beyond the महाभारत. There is also the ऒईटिस्त् Kazanas whose work in this regard is to a degree colored by his OIT frame work.

Here we shall examine the wider manifestations of what we call the कर्ण complex of motifs. This complex is intimately entwined with the distinct Arjuna complex in more than one part of the IE world, instances which will also be considered as appropriate.

  -  Gleaning motifs in the Celtic cycle\
A large body of Celtic IE material comes down to us in the form of the four Irish mytho-epic cycles in which the Ulster Cycle contains the legends of the great hero Cúchulainn who was famed for his battle frenzy known as the ríastrad. To better visualize the motifs we will first retell parts of his legend relevant to this topic.

  - His mother Deichtine was the daughter of the charioteer of the Ulster king Conchobar.

  - His father was the great god Lugh the Irish version of the pan-Celtic deity Lugus who is often described as the bright one with long hands. He is clearly distinct from the other great Celtic deity Taranis who combines the Indra and विष्णु class in him -- Taranis is iconographically similar to both those Indo-Aryan deities, being shown by the continental Celts with a vajra in one hand and a cakra in the other \[Footnote 1]. The epithets of Lugus namely his brightness and long hands are specifically matched by one Indo-Aryan deity: सवितृ who is described as being bright (चित्रभानु, RV 1.35.4) and long-handed (पृथुपानि, 2.38.2) and is clearly associated with the solar domain. Thus, Cúchulainn can be considered the son of a solar deity.

  - When the Ulster men led by Conchobar had gone hunting magical birds they were stranded by a snowstorm and had to take shelter in a house. Their host in the house fathers a son on Deichtine and reveals himself to be the great god Lugh and the child is was initially called Sétanta. In another versions Deichtine is already married to Sualtam and when Lugh reveals that he has placed a child in her womb during the hunting expedition it results in a scandal. Many suspect that Conchobar using his "right of first night" has impregnated her. But she appears to abort and go to Sualtam with virginity restored. But a child is reinstated in her womb then and Sétanta is born who is considered Sualtam's son but is actually by Lugh.

  - As a kid Conchobar and many other Ulster elite share the task of parenting the young Sétanta

  - Sétanta enters a game of the boy-troops of Ulster where the boys attack him for not first seeking their protection and permission to join. But Sétanta smashes them thoroughly displaying his battle frenzy for the first time. Conchobar ends the fight but it is the boys who now have to take Sétanta's protection. Then he kills a hound of the smith of Ulster, Culann, which was guarding his house. But he agrees to compensate the smith by guarding his house till he can rear a suitable dog for him. Hence the warrior druid Cathbad decided to name him Cú Chulainn -- Dog (Cú; cognate of Skt श्वन्) of Culann.

  - Then one day Cúchulainn spied on the druid Cathbad teaching sacred lore to his students. Cathbad was special in that he was not just a druid but also a warrior who had his own private fighting force with whom he had defeated and slain warriors of a previous Ulster king. One of Cathbad's students asked him what the day was auspicious for and Cathbad said that the one who bore arms on that day would become a great warrior with eternal glory. Overhearing that Cúchulainn immediately rushed to Conchobar and asked for weapons. All the weapons they gave him broke and could not bear his might until Conchobar gave him his own weapons. Just then Cathbad realized what had happened and informed him that he had not heard the remaining part of his statement: "The person who bore arms on that day would become a famous warrior but would die young."

  - Cúchulainn then wanted to marry a woman named Emer but her father opposed the match and sent him off to try to train under the warrior woman Scáthach. He was sure that Cúchulainn would be killed in the process as even entering her abode involved a difficult jump across a water body on to an island. However, Cúchulainn successfully managed this and she taught him many secrets of using a sword, spear, staff and lasso, as also various chariot feats. There a fellow trainee Ferdiad became his foster brother. Scáthach had a sister Aífe who was her great rival and attacked her when Cúchulainn was a student. He joins the fight on behalf of his teacher and the battle between him and Aífe is evenly poised. But he knew that Aífe valued her chariot and horses more than anything else. So he (apparently deceitfully) tells her in course of the battle that they have fallen off a cliff. She stops fighting on hearing this and Cúchulainn seized her right away. He releases her on the condition she would never fight with Scáthach again. Then he fathered a son on Aífe named Connla. He gave Aífe his ring and asked her to send Connla with it to seek him when he comes of age. Scáthach also offered the "friendship of her thighs" to Cúchulainn and eventually conferred her daughter Uathach as his wife. But Cúchulainn still sought to conquer Scáthach's realm from her and wages a battle with her. In midst of it both are tired and refresh themselves eating magical hazelnuts. As a consequence it is revealed to Cúchulainn that he could never defeat Scáthach and gives up. Scáthach also possessed the power of foretelling the future with a charm but she refuses to do so for Cúchulainn as she realizes that he would kill his son through her sister. Finally, before parting she confers on Cúchulainn, but not his foster brother, the use of a secret magical weapon known as the Gáe Bulga, which was a spear hurled by using the foot. Nobody could survive a strike from this spear.

  - Connla the young son of Cúchulainn and Aífe comes searching for his father with his ring. On reaching Cúchulainn's fort Connla defeats a warrior and shows his prowess. Cúchulainn then fights him and the two are evenly matched. His wife even warns him that Connla is his son by Aífe. Connla recognizes his father and lays aside his weapons but by then Cúchulainn has deployed the Gáe Bulga and kills Connla. As he dies he tells his father: "Together we would have carried the banner of Ulster to Rome and beyond."

  - In one of the legends Cúchulainn crushes the rival Irish kingdom of Connacht in a campaign. The queen of Connacht retaliates by ordering a great cattle raid on Ulster. At that point Cúchulainn was supposed to be guarding the cattle but he was busy enjoying himself with a woman. The other Ulster warriors are unable to fight due to a curse neutralizing them. Seeing this Cúchulainn leaves his woman and enters the battle single-handed challenging the foes to combats at fords where they have to fight one by one. He defeats many but after much fierce fighting he is severely injured and unable to fight, when the great god Lugh appears and heals him. But by then his companions from the boy-troops enter the battle and are slain by the Connacht cattle raiders. Cúchulainn enters into a tremendous battle frenzy slaughters the Connacht forces raising heaps of corpses. Seeing this the Connacht queen sends Cúchulainn's foster brother Ferdiad to fight him. Though both are described as having studied with Scáthach (the modest), Uathach (the terrible) and Aífe (the beautiful), each is said to have special gifts -- Cúchulainn his Gáe Bulga and Ferdiad his "horn-skin", a special impenetrable armor. The two are evenly matched neither yields in a great hand-to-hand battle fought for 3 days at a ford. They use up 8 swords, arrows, and spears, throwing-spears, lances, and heavy swords (or maces?). At the end of it Ferdiad seemed to be slightly gaining on Cúchulainn, who is without the Gáe Bulga which is in his chariot. But his charioteer Laeg, who is said to be king among charioteers, sends it to him by floating it down the river at whose ford they were fighting. Once he gets the Gáe Bulga, he first hurls a throwing spear at Ferdiad, who raises his shield to fend it off. Seeing that opening with his foot Cúchulainn discharged the Gáe Bulga which pierces Ferdiad and kills him. While falling Ferdiad thinks he was unfairly struck down and cries:

  - "O Cu of grand feats,\
Unfairly I'm slain!\
Thy guilt clings to me;\
My blood falls on thee!"*

Realizing he has killed his foster-brother Cúchulainn mourns him.

  - Cúchulainn kills many great warriors and wins several victories but the Connacht queen seeking revenge wages another war on Cúchulainn. Before this war he is tricked into eating dog meat which makes him lose his invulnerability. In the battle that follows he fights Lugaid who possessed 3 magical weapons. By hurling the first he kills Laeg the the charioteer-king who was driving Cúchulainn's chariot. With the second one he kills the great horse of Cúchulainn. Finally before he could use the Gáe Bulga with the third magical dart he strikes Cúchulainn. Despite being mortally wounded Cúchulainn ties himself to a menhir so that he dies facing his enemies and on his feet. When a crow sits on Cúchulainn they finally realize he has died and Lugaid advanced with his sword to cut off Cúchulainn's head. Then a bright light emerges from him and scorched Lugaid and Cúchulainn's sword fell from his hand cutting off Lugaid's hand. That light vanished only when they cut off Cúchulainn's right hand marking the end of the cycle.

• Apprehending the motifs\
Some motifs are found in the महाभारत but not necessarily linked to the कर्ण or Arjuna complex:

 1.  The motif of a hero being born or conceived while taking shelter/rest in a dwelling with an unfamiliar host during a hunt: This is seen in the legend of the birth of the founder of our nation, Bharata दौःशान्ति, where दुश्मन्त fathers him on शकुन्तला while visiting the आश्रम of कण्व for refreshment during a hunt.

 2.  The motif of providing a ring to the woman on whom the child is fathered before leaving so that it can be used to identify paternity later: This again appears in the tale of Bharata दौःशान्ति, where he uses the ring is used prove identity of the son to the father. In the Irish cycle it is decoupled from the former motif.

The remaining motifs are part of the classic कर्ण and Arjuna classes.

 3.  Being fathered on an earthy woman by a solar deity (कर्ण).

 4.  Parentage linked to the ruling king's charioteer -- राध and Deichtine (कर्ण).

 5.  Mother's mating with the solar deity happens out of wedlock -- Kunti and Deichtine (कर्ण)

 6.  Mother's virginity restored after the mating for real human marriage -- Kunti and Deichtine (कर्ण).

 7.  Learning something related to the use of weapons from a teacher of the priestly class in an unauthorized or unapproved fashion and the teacher pointing to (or cursing with) a fatal consequence as result: Cúchulainn overhearing Cathbad's teaching to his students and कर्ण lying about his वर्ण to रामो भार्गव. Consequently, both are saddled with the disadvantage of potential death despite their prowess (कर्ण).

 8.  Learning something related to arms from a warrior priest: ब्राह्मण रामो भार्गव and druid Cathbad (कर्ण).

 9.  The hero appears suddenly when other elite youth are engaging in sport/arms display breaking convention and causing a stir: Cúchulainn appearance before the boy-troops and कर्ण's appearance at the sporting/arms display of the Kurus (कर्ण).

The motifs relating to the warrior women while prominent in the Irish epic cycle are muted in the महाभारत. However, there are still several recognizable motifs that unite them. In the महाभारत they are all consistently associated with Arjuna and could be considered part of the Arjuna class.

 10.  Leaving the principle wife or woman on an exile-like journey to a far away land: Cúchulainn leaves behind Emer who is his primary wife and Arjuna leaving behind Draupadi (Arjuna).

 11.  The hero consorts with three women during this journey: Cúchulainn with Scáthach, Uathach and Aífe; Arjuna with उलूपी, चित्राङ्गदा and सुभद्रा (Arjuna).

 12.  Meeting one of the alien woman in the vicinity of a water body: Cúchulainn had to jump over a water body to reach Scáthach and Arjuna is captured by उलूपी on the banks of the गङ्गा and taken to her realm (Arjuna).

 13.  The alien women met by the hero live by themselves without their male partners before and after marriage; the hero leaves them after inseminating them. This is a marked departure from the traditional IE family structure: Scáthach and Aífe of Cúchulainn and उलूपी and चित्राङ्गदा of Arjuna (Arjuna).

 14.  The warrior woman offers sex to the hero out of her own initiative: Scáthach to Cúchulainn and उलूपी to Arjuna (Arjuna).

 15.  The woman/women train male sons in the use of arms all by themselves: Scáthach trains Cuare and Cet, while उलूपी trains इरावान् and बभ्रुवाहन (Arjuna).

 16.  The warrior woman gives the hero a gift of an invincible weapon or power associated with the aquatic realm: Scáthach gives Cúchulainn the Gáe Bulga which is made from the bones of a sea-monster which had died fighting another sea-monster. उलूपी gives Arjuna the power of invincibility over aquatic creatures.

The motif of the conflict between the hero and his son according to the studies of Joseph Campbell is rather widespread suggesting that it could be an ancient one. While Campbell and others have tried to give it Freudian overtones much of the verbiage expended in that direction does more to obfuscate than to explain. However, it is possible that it represents the quintessential problem faced by a male: the uncertainty of paternity, which in pre-modern times was never resolvable with certainty. But in our current case there are some specific features on top of the generic father-son conflict motif.


 17.  The hero fathers a mighty son on an independent woman during his exile/forced wanderings and leaves the son with the mother: Cúchulainn fathers Connla on Aífe and Arjuna fathers बभ्रुवाहन on चित्राङ्गदा (Arjuna).

 18.  The son through the warrior woman on growing up sets out to find his father: Connla seeking Cúchulainn; In the महाभारत this motif is not joined to the father-son conflict system. But it exists in the form of इरावान् journeying to meet his father.

 19.  Father and son don't recognize each as one sees the other intruding his domain and battle breaks out in which one is killed: In the Irish legend it is the son who is killed. In the Hindu one Arjuna invades the kingdom of मणिपुर where बभ्रुवाहन is the prince during the अश्वमेध rite of युधिष्ठिर. बभ्रुवाहन kills Arjuna in the battle that follows.

The battle motifs are shared between the कर्ण class and the Arjuna class though they might not come in the same character.

 20.  Possession of magical spear that can kill the opponent on whom it is deployed infallibly: Cúchulainn's Gáe Bulga and कर्ण's Indra शक्ति (कर्ण)

 21.  This infallible weapon does not help them in the final battle (कर्ण)

 22.  Weakened in final battle by a curse or breaking of taboo and a related ethical dilemma: Cúchulainn's ate dog meat because he could not avoid accepting hospitality and that broke his invincibility. कर्ण did not wake रामो भार्गव while being bitten by the alarka tick but in the process he had to reveal that he was not a ब्राह्मण leading to a curse. He broke the taboo by killing the cow (कर्ण).

 23.  Fights a solo battle in a cattle raid against several warriors, including the brother warrior, when other defenders are not available: Cúchulainn fights Ferdiad in the battle against the Connacht raiders and Arjuna fights कर्ण in the Kuru raid on विराट (Arjuna/कर्ण ).

 24.  Kills brother warrior in a great battle which is the or at least one of the main highlights of the conflict under consideration: Cúchulainn kills Ferdiad and Arjuna kills कर्ण (Arjuna/कर्ण).

 25.  One of the brother warriors has a special armor that confers him invulnerability but fails or is unavailable in the last encounter: Ferdiad's horn skin and कर्ण special kavaca with which he is born (कर्ण).

 26.  In their most critical encounter when the brother enemy gains the upper hand his charioteer makes a crucial move that saves the hero and eventually confers him victory: Laeg floats the Gáe Bulga to Cúchulainn. कृष्ण saves Arjuna by the chariot feat and later directs him to deploy the fatal अङ्जलिक missile when कर्ण is trying to lift up his chariot wheel (Arjuna/कर्ण).

 27.  The fatal weapon is supposed to be irresistible and pierce the very innards of the foe and said to be bathed in the blood and fat of the foe. In the Irish cycle we hear of: "*Cúchulainn saw his weapon \[Gáe Bulga] bloody and crimson from Ferdiad's body.*" Likewise the weapon of Arjuna said to be smeared in the blood and fat of the foe. Just as the might of the Gáe Bulga is emphasized, Arjuna's weapon is described as being powered by Indra and resembling the weapons of Rudra or विष्णु or an अभिचार of the भृगु-s and आङ्गिरस-s.


 28.  The killing of the brother warrior is considered an unfair act: Ferdiad while dying clearly declares his killing by Cúchulainn an unfair act probably referring to the use of the first spear to open his defense before using the Gáe Bulga. कर्ण before being killed objects to Arjuna trying to unfairly strike him when he is trying to set right his chariot wheel (कर्ण).

 29.  The charioteer of the hero in said to be a king among charioteers: Laeg and शल्य, who is the king of Madra (कर्ण)

 30.  Before being struck by the fatal dart the hero's chariot is incapacitated: This happens to both Cúchulainn and कर्ण albeit in different ways (कर्ण).

 31.  Upon their death their corpse is said to be endowed by luster: The solar luster of both the dead Cúchulainn and कर्ण is emphasized in the respective narratives (कर्ण).

  -  Consequences for Indo-European\
The above examination of motifs reveals what appears, at least to us, an unprecedented parallelism between the epic traditions of two very far-flung corners of the IE world -- India and Ireland, which mark in a certain sense (leaving out Tocharian) the extremities of the IE world. While some of these motifs appear elsewhere in the IE world, to our knowledge, and in terms of what has survived the assaults of Abrahamism, these parallels are are the strongest. For example, in the Greek world we have the tale of Ion and Creusa: Ion the founder of the Ionians (Skt: yavana) is fathered by a solar deity on Creusa before her marriage. She then abandons him in a casket with his divine ornaments and cloak and he is brought up by foster parents. He is said to devote his time to worship of the solar deity. Thus, we see motifs of the कर्ण class occurring in this legend but without the later military episodes. In both the Germanic and Iranian world we encounter parallel father-son conflicts (e.g Rostam and Sohrab) and other epic motifs. In the Irish world the कर्ण and Arjuna class motifs are mixed into Cúchulainn and some conferred on his most capable rival Ferdiad, but it represents the fullest expression of such motifs elsewhere in the IE world.

There are some other interesting specific parallels that are shared by the Celtic and Hindu worlds, like the words for king, rix and राजन्. Moreover, chariot warfare is important in the epics of both the Hindus and the Celts but not to that degree in the Greek or Roman epics. The Coligny calender of the Celts uses a system of synchronizing lunar and solar cycles similar to the वेदाङ्ग ज्योतिष. The are also mysterious later iconographic parallels like that of "Taranis : Indra-विष्णु" and the motifs of clearly Indian origin on the Gundestrup cauldron. While the Hindus preserve the largest body of the ancestral IE tradition, which has overlaps with each of the IE traditions some of these specific parallels with the Celts raise thorny and interesting questions: What is the significance of this survival of traditions at the two extremes of the IE world? Do they merely represent the survival of motifs at the fringes of the IE world after the radiation from the ancestral IE homeland or do they mean a later second transfer?

The year 2015 CE has been a landmark year in our understanding of the spread of IE into Europe. Ancient DNA sequencing has provided genomic data from Mesolithic, Neolithic, Bronze age humans both from western Europe and the steppes of Eurasia. This has thrown considerable light on the timing and direction of population movements that likely corresponded to the early Indo-Europeans. Briefly the data suggests the following. The most likely homeland of PIE was the Caspian-Pontic steppe. Part of the Early IE population, at least that part which invaded Europe, was associated with the Yamna Culture which occupied the steppes to the north of the Black and Caspian Seas. Archaeology suggests that this culture appears to have been pastoral with cattle, sheep, goats and horses, practiced agriculture in vicinity of rivers, and built defensive structures in the form of forts on hills. These features generally match what is expected for the Proto-IEans. The Yamna culture is dated approximately from 5500-4000 YBP. The genetic evidence suggests that by at least 5000 YBP or earlier they had expanded eastwards in the direction of Altai, South Siberia and Mongolia (the Afanasievo culture). These probably brought the precursors of Tocharian to the east. By 4500 YBP, peoples derived from the Yamna culture had launched a massive, rapidly moving invasion westwards into Europe. This invasion smashed the existing European system of Neolithic farmers and resulted in large-scale replacement by the old population by the invading Yamna-derived populations. It is most likely that these brought the precursors of Celtic, Italic and Germanic languages to Europe.

However, the currently available evidence points only to the presence of wagons not chariots in the Yamna and immediate descendant cultures of Europe. This has been used to suggest the absence of classical chariots with the spoked wheel in the PIE period. These type of chariots appear only later in the Sintashta culture, earliest at 4100 YBP. Based on this it has been proposed that Sintashta corresponds to the early Indo-Iranians because both their earliest texts mentions the spoked wheel chariot (e.g. the phrase अरान् na नेमिः occurs multiple times in the ऋग्वेद \[Footnote 2]). This proposal is further supported by the evidence for Uralic contact in Indo-Iranian and early Indo-Aryan given that the Uralic zone and Sintashta zone are in close proximity. The word for chariot, rátha, had उदात्त on first syllable unlike the reconstructed PIE form with उदात्त on second syllable. This seems to have marked the semantic shift from wheel in the ancestral state (e.g. retained in German and transferred to bicycle in recent times) to chariot. In this context a further wrinkle, which has emerged from the ancient DNA evidence, needs to be noted: There is some evidence that the Sintashta and its successor Andronovo culture are not directly derived from the earlier Yamna. Rather they appear to be a reverse eastern movement of the earlier Yamna peoples who went west into Europe and mixed with the local populations therein.

If this were the case then how do we account for the parallelism between the Celtic and Hindu epic traditions with a prominent role for the chariot and the charioteer?

Several possibilities present themselves:

 1.  The chariot tradition developed earlier in the PIE period itself but did not refer to the spoked chariot but the solid wheeled one (a Talagerian view mouthed uncritically by Hindu ऒईटिस्त्स्). Later it was transferred to the spoked wheel chariot.

 2.  Like above, the chariot tradition developed earlier in the PIE period itself but it has simply not yet been recovered in earlier steppe sites sampled to date. In this context we may point out that while the solid wheel depictions are more common in Sindhu-Sarasvati sites the spoked wheel appears there slightly before (may be \~100-300 years) it does at the Sintashta sites. Hence, there could have been an earlier presence of spoked wheels bring it close to the dispersal of IE into inner Europe.

 3.  The movement that gave rise to the Sintashta culture, which began deeper in Europe was the one in which the spoked wheel was indeed innovated. From this culture, the technology was transferred along with a specific class of linked legends pertaining to chariot warriors to the proto-Celts who were proximal to the earliest Sintashta people. These Sintashta people later bore these legends and the chariot to the Indian world. This last explanation might account for the unusual pattern of specific similarities.

This also has bearing on the timing of origin of the Indo-Aryans and the timing of the famous AI and thereby parts of the Vedic corpus. The ऋग्वेद has a clear memory of the Caspian region and the ancient DNA from the steppes (Yamna, Sintashta/Andronovo and ) do show links to the component termed "Ancestral North India" in the Indian ancestry. This puts the origin of the Indo-Aryans in the same general region. However, in the absence of ancient DNA data from India a direct estimate of the time cannot be done unlike what has been published now for Europe and Eastern Asia. If indeed the Sintashta were the early Indo-Iranians then it is likely that the Indo-Aryans reached India only after the end of the mature phase of the Harappan civilization. This the preferred position of the white Indologists. This would mean late dates (not older than \~4100 YBP but more like 3500 YBP) for the Vedic corpus more in line with what the white indologists have consistently believed. Now, we cannot entirely rule out that the Indo-Aryans instead reached India earlier via an extension of the early Maykop culture. The philological analysis of Hindu tradition points to more than one wave of invading Aryans in the form of the शल्व-s and पाण्डव-s. This raises the possibility of an earlier wave which brought the spoked wheel to the Harappan civilization and the later waves corresponding to groups like the शल्व-s and पाण्डव-s. Then there is also a similar scenario where the chariot culture reached them secondarily as a technological adaptation but this is not consistent with the Indian linguistic evidence. Thus, the timing of the AI still needs data to be properly resolved. But if Hindus neglect this field of investigation they are likely to consigned to the position of "secondary Indo-Europeans" by the white Indo-Europeanists -- a state much worse than the glorious OIT of their dreams.


Footnote 1: Lugus in contrast is shown as a tricephalic figure on the European continent.

Footnote 2: It is common for western Indo-Europeanists, including the less educated white indologists, to propose that main thing linking the Sintashta culture to the I-Irians is the presence of burials corresponding to rituals mentioned in the ऋग्वेद. This is however an exaggeration. Such burials do not have any specific features linking them to the funerary सूक्त-s of RV or Avestan tradition -- rather it is the fertile imagination of western archaeologists with little philological training.


