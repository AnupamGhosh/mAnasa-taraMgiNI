
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [शकधूम: Possible parallels in a meteorological tradition from India and South America](https://manasataramgini.wordpress.com/2015/02/15/sakadhuma-possible-parallels-in-a-meteorological-tradition-from-india-and-south-america/){rel="bookmark"} {#शकधम-possible-parallels-in-a-meteorological-tradition-from-india-and-south-america .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 15, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/02/15/sakadhuma-possible-parallels-in-a-meteorological-tradition-from-india-and-south-america/ "5:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This is an updated version of our earlier study: published first at [ईन्दिअFअcत्स्](http://indiafacts.co.in/possible-parallels-in-a-meteorological-tradition-from-india-and-south-america/)\
Research by Orlove, Cane and Chiang on the native weather forecasters in mountains of Peru and Bolivia has brought to light an interesting tidbit of traditional knowledge \[Footnote 1]. The traditional crop of these regions is the potato, from where it has spread the world over to become a major source of nutrition, a delight to the tongue, and, more perversely, a cause of obesity and related troubles among the well-fed. However, to farmers in this region a successful potato crop is crucial for their sustenance. Hence, these farmers forecast the auspicious time for planting potatoes by means of a unusual meteorological/astronomical observation. For a week around the summer solstice they start intently observing the skies. At midnight they climb up to the peaks and start observing the Pleiades (M45 open cluster; Sanskrit: कृत्तिकाः) a few hours before dawn, noting the apparent brightness and "sizes" of the stars in the cluster. Dimmer the Pleiades the less will be the rain in the area during winter. Hence, if the forecast is dry then the farmers delay planting their potatoes to reduce losses. The fluctuation in local rainfall is attributable to El निञो Southern Oscillation, a meteorological phenomenon associated with a cyclical pattern of warm and cold temperatures in the tropical Pacific. What the researchers found was that if an El निञो phase with low rainfall was to happen later in winter then it was accompanied by high cirrus clouds in the earlier summer. This in turn caused a dimming of the Pleiades when observed around the summer solstice. This use of the Pleiades as meteorological predictor is an interesting twist to its more general use, in many ancient traditions, as a calendrical marker to determine various seasonal phenomena and agricultural activities.

This research leads us to a new insight regarding the basis for an ancient tradition recorded in the Veda of the अथर्वाङ्गिरस-s in a ritual incantation known as the शकधूम सूक्तं, which to date has been poorly understood. In the vulgate Atharvaveda (AV) text (considered to be the शौनक शाखा ) the सूक्तं occurs as 6.128, while in AV पैप्पलाद शाखा (AV-P) the relevant mantra-s are AV-P 19.24.16-19. The पैप्पलाद version is also appended at the end of the नक्षत्र-kalpa (AV-परिशिष्ट-1) under the title: "[कृत्तिका-रोहिणी-मध्ये पैप्पलादा मन्त्राः]{style="color:#99cc00;"}". In both texts there are 4 ऋक्-s, though they differ somewhat between them. The कृत्तिका-s (Pleiades) have been known in Vedic tradition to possess watery names, which are individually spelled out in the Yajurveda for oblations made during the ritual known as the नक्षत्रेष्टि: अम्बा (watery), दुला (shimmering), नितत्नी (showering), अभ्रयन्ती (clouding), मेघयन्ती (clouding) [स्तनयन्ती, i.e. thundering in the मैत्रायणीय संहिता], वर्षयन्ती (raining) and चुपुणीका (bubbling). It has long been suspected that these names are indicative of their connection with the arrival of monsoons. But were the Pleiades specifically used in weather prognostication in Hindu tradition? Here is where the evidence from the शकधूम सूक्तं comes in. The word शकधूम is interpreted as smoke (धूम) from a dung-pat (शक) fire. But paradoxically we find the AV tradition remembering शकधूम as a weather-predictor. Indeed, this is how the late medieval Atharvavedins seem to have understood the word in ritual context as indicated in their paddhati-s. The word has also been rendered as a human weatherman by modern white translators such as Whitney and Bloomfield.

However, an examination of the word shows that the AV tradition originally hardly implied an earthly weatherman in the term शकधूम. The सूक्तं it self opens by explicitly mentioning शकधूम as being made the king of the नक्षत्र-s (AV-vulgate: "[शकधूमं नक्षत्राणि यद् राजानम् अकुर्वत ।]{style="color:#99cc00;"}" AV-P: "[यद् राजानं शकधूमं नक्षत्राण्य् अकृण्वत ।]{style="color:#99cc00;"}"). The purpose of him being chosen as the king was to ensure prognostication of fair weather ("[भद्राहम् अस्मै प्रायछन्]{style="color:#99cc00;"}"). The presence of the पैप्पलाद form of the text in the नक्षत्र-kalpa, a text with a slant towards prognostication, also reiterates the close connection of the weather-predictor शकधूम with the नक्षत्रस्. Realizing that a celestial entity is implied, some people have interpreted शकधूम to mean the moon (as राजा of the नक्षत्र-s) or the Milky Way (due to the "smoky" allegory). However, in no Hindu text known to date the term शकधूम has been used to describe the moon, nor is the Milky Way ever described as the leader of नक्षत्र-s. The original meaning of शकधूम becomes clear from the ऋक् found only in the AV-P version:\
"[यद् आहुश् शकधूमं महानक्षत्राणां प्रथमजं ज्योतिर् अग्रे ।]{style="color:#99cc00;"}\
[तन् नस् सतीं मधुमतीं कृणोतु रयिं च सर्ववीरं नि यच्छताम् ॥]{style="color:#99cc00;"}"\
Here शकधूम is plainly termed the first born of the great नक्षत्र-s and as being at forefront of the celestial lights. This shows that शकधूम was a constellation and the first in the list. In the AV नक्षत्र सूक्तं (AV-vulgate 19.7.2) the first in the list is कृत्तिका. The AV नक्षत्र-kalpa also explicitly states that कृत्तिका is the first of the नक्षत्र-s with whom Agni blazes forth: "[स नक्षत्राणां प्रथमेन पावकः कृत्तिकाभिर् ज्वलनो नो 'नुशाम्यताम् ।]{style="color:#99cc00;"}".\
So it is likely that the नक्षत्र implied by शकधूम was none other than कृत्तिका. This is further confirmed by Charpentier's finding that in the medieval देश-भाषा lexicon of the Jaina polymath Hemacandra-सूरी (the देशी-नाम-माला) he gives धूम as a synonym for कृत्तिका: [धूमद्-धय-महिसीओ कृत्तिकाः ।]{style="color:#33cccc;"} DN-5.63

This leads to one other reference to शकधूम which is found in the great brahmodya सूक्तं in the ऋग्वेद (RV 1.164):\
"[शक-मयं धूमम् आराद् अपश्यं विषूवता पर एनावरेण ।]{style="color:#99cc00;"}\
[उक्षाणं पृश्निम् अपचन्त वीरास् तानि धर्माणि प्रथमान्य् आसन् ॥]{style="color:#99cc00;"} (RV 1.164.43)"

Based on its deployment in the pravargya ritual some have commented that the शक-धूम here refers to the smoke from the fire on which the महावीर pot is being fumigated. This external interpretation is of course for the "un-enlightened", for the rahasya-s are concealed beneath the ritual actions described in the mantra. That is exactly what this whole सूक्तं is about -- rahasya-s, including several astronomical ones. This becomes very clear from use of a technical astronomical term -- विषूवत्. In Hindu tradition विषूवत् meant equinox \[Footnote 2] and in this context clearly means the vernal equinox which formed one of the central days of the yearly sattra. Thus, the ऋक् means:\
*"From far I saw the शक-धूम at the equinoctial point further off from this lower one.*\
*The heroes cooked the speckled bullock; these were the first stations."*\
Here, the constellation at the vernal equinox (विषूवान्) is being described as शकधूम. The lower one, the speckled bullock, the first (previous) stations appear to stand for Taurus. Taurus being the prior station stands for the position where the equinox lay prior to शकधूम, which is being described as currently being at the विशुवान् \[Footnote 3]. Here too शकधूम implies the Pleiades. This would also suggest that the brahmodya belongs to the same period as the core AV composition during which the Pleiades lay at the vernal equinox. Not surprisingly, a variant of RV 1.164 also occurs in the AV-vulgate as सूक्तं 19.10

***Finally it leads to the issue why the name शकधूम for the Pleiades?***\
In the AV context the term शकधूम is specifically applied in the context of predicting good weather. With high cirrus clouds it is quite likely that the Pleiades appeared as a smoky patch in the sky, which was then used for weather prognostication. Under this interpretation the AV शकधूम tradition is likely to be the earliest surviving record of weather prognostication based on the appearance of the Pleiades. Now, given the South American tradition we suspect that indeed a smoky Pleiades was also, a prognosticator in India. Unfortunately, the original AV tradition is completely dead, hence only experimental verification can test the effectiveness of the method.

While the original AV tradition does not survive, we know from the much later meteorological traditions recorded in the कृषि-पराशर (KP), a Hindu manual on farming, that knowledge derived from such observations did survive in some form. For example, the KP23 gives a rough formula to determine the nature of the El निञो effects. KP24-25 describes clouds associated with the cycle and mentions the पुष्कर (cirrus) clouds that appear to prognosticate droughts. KP33 indicates that the predictions were refined using wind-vanes to measure wind direction/speed and predict rain several months later.

Footnotes\
Footnote 1: An account of Orlove, Cane and Chiang' work:\
[http://earthobservatory.nasa.gov/Study/Sतर्स्Cलोउद्स्Cरोप्स्/](http://earthobservatory.nasa.gov/Study/Sतर्स्Cलोउद्स्Cरोप्स्/){rel="nofollow"}

Footnote 2: In his कारिका on ईश्वर-प्रत्यभिज्ञा the Kashmirian तान्त्रिक, Utpala-deva, makes the following statement:\
[प्राण-अपान-मयः प्राणः प्रत्येकं सूप्त-जाग्रतोः ।]{style="color:#99cc00;"}\
[तच्-छेद्-आत्मा समान-अख्यः सौषुप्त विषुवत्स्व्-इव ॥]{style="color:#99cc00;"}\
*The metabolism in both the sleeping and waking states is comprised of प्राण and अपान processes. Both are suspended in the deep sleep state when the समान process functions, like what happens on the equinox.*

Here the word विषुवन् (equinox) is used metaphorically to explain that state of equality or balance (समान) when the प्राण and अपान are suspended in the deep sleep state.

Footnote 3: The memory of older equinoctial positions in Taurus and beyond where first proposed by the great patriot Lokamanya Tilak in his work: "*The Orion or the antiquity of the Vedas*".


