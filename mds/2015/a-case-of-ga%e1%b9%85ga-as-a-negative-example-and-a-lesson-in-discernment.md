
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A case of गङ्गा as a negative example and a lesson in discernment](https://manasataramgini.wordpress.com/2015/10/27/a-case-of-ga%e1%b9%85ga-as-a-negative-example-and-a-lesson-in-discernment/){rel="bookmark"} {#a-case-of-गङग-as-a-negative-example-and-a-lesson-in-discernment .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 27, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/10/27/a-case-of-ga%e1%b9%85ga-as-a-negative-example-and-a-lesson-in-discernment/ "3:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A version of this article was originally published on [ईन्दिअFअcत्स्](http://indiafacts.co.in/despoiled-ga%E1%B9%85ga-as-a-metaphor-for-loss-of-hindu-discernment/)

◊◊◊◊◊◊◊

With the river Sarasvati going dry the plains watered by the गङ्गा became the focus of civilization in India. This civilizational phenomenon is philologically mirrored in Hindu tradition with the गङ्गा and cities along it course gaining prominence in the late Vedic period. The great historical empires that brought about the multi-step unification of India can all be seen as having their "birth" on the banks of the गङ्गा. So much so that even in deep south India the conqueror राजराज of the Coĺa clan established a town to commemorate his bringing of water from the गङ्गा in course of his conquests that reached the Gangetic plains. Indeed, the गङ्गा is typically associated with everything pure and auspicious in Hindu tradition.

However, we shall note one interesting departure from this where गङ्गा is presented as a negative example. We see this in the नीतिशतक, one of the triad of 100 verse-collections, composed by भर्तृहरि, a personage who is the locus of many a colorful legend in Hindu tradition. The other two of the triad being the शृङ्गारशतक and the वैराग्यशतक, which respectively cover the opposites of eroticism and asceticism respectively. The we are talking about is:

[शिरः शार्वं स्वर्गात् *[पतति शिरसस् तत् क्षितिधरं]]{style="color:#99cc00;"}\
[महीध्राद् उत्तुङ्गाद् अवनिम् अवनेश् च+अपि जलधिम् ।]{style="color:#99cc00;"}\
[अधो 'धो गङ्गेयं पदम् उपगता स्तोकम् अथ वा]{style="color:#99cc00;"}\
[विवेक-भ्रष्टानां भवति विनिपातः शतमुखः ॥]{style="color:#99cc00;"} (नीतिशतक 10)

alternative reading *[[पतति शिरसस् तत् क्षितिधरं]{style="color:#99cc00;"}]

शिरः= head (nominative, neuter singular); शार्वं= of शर्व, i.e. of god Rudra; स्वर्गात्= from the heavens; patati= she falls; शिरसः+tat= from that head (ablative, neuter singular); क्षितिधरं= the bearer of the earth, i.e. the mountain range, the Himalayas; महीध्राद्= from the mountain (ablative masculine singular); उत्तुङ्गाद्= high (adjective); avanim= plains (accusative, feminine singular) अवनेः= from the plains (ablative, feminine singular) ca+api= and then; jaladhim= ocean (accusative, masculine singular); अधः+अधः= lower and lower; गङ्गा इयं= this गङ्गा (nominative, feminine singular); padam= rank/status (nominative, neuter singular); उपगता= undergone/attained; stokam= gradually; atha वा= or so; viveka-भ्रष्टानां= those who have lost discernment (genitive, masculine plural); bhavati= it becomes; विनिपातः= fall (nominative, masculine singular); शतमुखः= hundred-faced (nominative, masculine singular) ||

She falls from the heavens to शर्व's head, from his head to the Himalayas,\
from the Himalayan heights to the plains, and then from the plains to the ocean;\
thus गङ्गा has gradually attained a lower and lower status;\
even so the fall of those deprived of discernment is hundredfold.

Here the descent of गङ्गा from the heavens all the way to the ocean is presented as mimicking the manifold fall of those who have lost their discernment. Sadly, the गङ्गा we see today literally gets worse in course of its descent: from its pristine glacial origins the river is serially polluted by everything from corpses to industrial effluents as it passes through the civilizational centers on its mid-course to its delta in the वङ्ग country. This grimly polluted गङ्गा of today is truly apposite to old भर्तृहरि's metaphor.

We would even go as far as to say that this despoiled गङ्गा is a key manifestation of the manifold dissipations of the Hindus arising from the loss of discernment, which was once abundant in the founders of their nation. Sacred geography is an important aspect of a heathen civilization. Hindus as the most expansive heathens in the modern world should be taking the lead in preserving the basis of what makes a particular geography sacred. Yet, barring few bright spots like the Prabhughat cleaning drive \[Footnote 1], what we are faced with most commonly is not just a dreadful neglect but even its willful desecration of sacred geography.

On another front Hindus face the possibility of being consigned to perdition due to their rank inability to wield नीति (politics) to their advantage. While analysts have expended lot of ink on the ten disastrous years of UPA rule, we would say that this is in no small measure a manifestation of viveka-क्षयः of the Hindus. If the UPA debacle were not enough for the macrocosm of India the microcosm of Delhi showed us the same lack of discernment yet again by choosing Kejriwal and his henchmen as their rulers.

But the lack of discernment among the Hindus is nowhere more apparent than in the matter which can be termed as "self versus nonself" discrimination. Key to survival of a nation is its ability to define itself in a sturdy fashion, and examination of the Hindu responses suggests that they have been struggling with this. Sometimes we see Hindus placing emphasis on autochthonism, which in certain cases extends to include racial or genetic identity. Other times we see them mouthing the famous adage "vasudhaiva कुटुंबकम् (the world is one family)", without realizing that early Hindu tradition frequently recorded this statement as a negative example \[Footnote 2]. Irrespective of their intrinsic merit, neither of these models of identity provide for robust discernment of what is self and what is non-self. In fact both models open the door for the invasive Abrahamisms and their secular derivatives such as leftism and liberalism. It hardly needs elaboration that giving these ideologies the proverbial inch will result in them grabbing an ell and much more at the expense of the Hindu. Thus, even as the immune system of an organism has manifold receptors to distinguish invasive material from self-substances, we need develop the civilizational apparatus that can finely discriminate that which can be accommodated within our heathen framework from that which cannot be.

Hence, if we intend to survive we need to collectively awaken to the big "if" in the statement of Monier-Williams, however harsh it might feel: "The knowledge of human nature displayed by the \[Hindu] authors, the shrewd advice they often give, and the censure they pass on human frailties -- often in pointed, vigorous, and epigrammatic language -- attest to an amount of wisdom which, if it had been exhibited in practice, would have raised the Hindus to a high position among the nations of the earth. \[emphasis mine]."

Footnote 1: [http://swarajyamag.com/lite/video-temsutula-imsong-on-cleaning-banaras-and-india/](http://swarajyamag.com/lite/video-temsutula-imsong-on-cleaning-banaras-and-india/){rel="nofollow"}

Footnote 2: For a detailed discussion of the same see: [http://bharatendu.com/2008/08/29/the-hoax-called-vasudhaiva-kutumbakam-1-hitopadesha/](http://bharatendu.com/2008/08/29/the-hoax-called-vasudhaiva-kutumbakam-1-hitopadesha/){rel="nofollow"} by Sarvesh Tiwari.


