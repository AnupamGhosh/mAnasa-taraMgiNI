
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [World War 2 and the like](https://manasataramgini.wordpress.com/2015/05/10/world-war-2-and-the-like/){rel="bookmark"} {#world-war-2-and-the-like .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 10, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/05/10/world-war-2-and-the-like/ "7:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Right away we should state that there is going to be some dilation -- hence "the like".
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/--Gq0uTa_xOI/VU8JZWLdIUI/AAAAAAAADTI/hMSdRrhWDBw/s800/American_propaganda.JPG){width="75%"}
```{=latex}
\end{center}
```



"In my generation, we lived under the impression that the term patriotism was poisoned during Nazi times. German history, unlike American or French history, did not allow the growing of patriotism in a natural way." Richard von Weizsaecker brother of Carl von Weizsaecker.

"I commemorate the 60 million people who lost their lives because of this war unleashed by Germany... I bow before the victims." Angela Merkel at Gdansk, Poland; formerly the German territory of Danzig.

"History is harsh. What's done cannot be undone. With deep repentance in my heart, I stood there in silent prayers for some time. On behalf of Japan and the Japanese people, I offer with profound respect my eternal condolences to the souls of all American people that were lost during World War II...Our actions brought suffering to the peoples in Asian countries. We must not avert our eyes from that. I will uphold the views expressed by the previous prime ministers in this regard." Shinzo Abe speaking to the congress, USA.

"The hard lessons of the World War II say that coexistence of humanity is not ruled by the law of the jungle. The politics of peace is the exact opposite of the aggressive hegemonic politics of force. The path of human development does not lay in the principle 'the winner takes it all,' not in zero-sum games," Vladmir Putin the lord of all Rus. (Addendum: "There are no longer any international security guarantees at all and the party responsible for the destruction of global collective security is The United States of America.")

" " Apology of the Americans to the Japanese for making them the first human victims of nuclear strikes. (Addendum: "We save innumerable human lives by swiftly ending the WW2 by nuking the Japanese." -- commonly heard from the mleccha-s of क्रौञ्चद्वीप)

" " Apology offered by British for their genocides of various peoples. (Addendum: "Today, with the balance and perspective offered by the passage of time and the benefit of hindsight, it is possible for an Indian Prime Minister to assert that India's experience with Britain had its beneficial consequences too." नीलोष्नीष क्लीब)

The above statements tell a tale that discerning men can immediately flesh out. However, as victors write history, we are fed a non-stop propaganda about the WW2, which the undiscerning swallow without reflection and eventually end up believing and propagating the same.

While the WW2 was primarily a war between mleccha-s, the Hindus were wittingly or unwittingly dragged into it (Those inspired by Subash Chandra Bose fought willingly to liberate their land from the mleccha-s or the wage Jihad on the Kaffirs. Others were dragged into it unwittingly, though some Hindu leaders like Savarkar felt it was good if the Hindus participated, even if on the side of their enemies, because it would give them battle experience for the impending conflicts with the mleccha-s and मरून्मत्त-s).

From a Hindu perspective, in some ways one can see it as a parallel to the great war at कुरुक्षेत्र in our national epic. At the heart of that war was a sibling rivalry between the Kuru and the पाञ्चाल, the two mighty Indo-Aryan kingdoms of the day. Likewise in WW2 at its heart was the rivalry between the sibling Germanic peoples, the continental शूलपुरुष-s and the island आन्गलिक-s. At कुरुक्षेत्र, there were other rivalries running in parallel: e.g. between the Kuru Somadatta clan on one side and the yadu-s कृष्ण and सात्यकि on the other; between two factions of Yadu-s, namely those who sided with the Kaurava-s and those who sided with the पाण्डु-पाञ्चाल alliance; between the राक्षस-s and the पाण्डु-s. All of these got sucked into the core sibling conflict at the kuru field. Likewise, in WW2 there were other parallel conflicts: the German-Slav conflict along ethnic lines; The intra-Slav conflict between the Poles and the Rus; The deep civilizational conflict between modernized heathen Japan and the Christian world of the mleccha-s; Economic conflicts involving groups close to the deep-scaffold of the mleccha world, which cannot even be mentioned openly to this date.
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-fElc8KwTie0/VU8JdSisL2I/AAAAAAAADTQ/AFAgsXslgp4/s800/English_trampling_Hindus.jpg){width="75%"}
```{=latex}
\end{center}
```



Moreover, while many people may think this to be frivolous, we also hold that there are some parallels to the First War of Independence and WW2 (interestingly, 7/8th May is what the victors of WW2 celebrate as the end of the war in Europe while 10th May is the start of the FWI).

In both these wars the prime villain was the fiendish nation of the आन्गलिक-s, which played its guileful game very well. In Europe they were alarmed by the rising might of their siblings on the continent. They knew well that if anyone could challenge their power it would be their cousins on the continent who possessed in ample measure the qualities, which had led to the success of the आन्गलिक-s, even as other European powers like Spain, Holland and Portugal waned despite their head-start. Hence, the शूलपुरुष-s had to destroyed at all costs. In WW1 they had achieved this partially, but it was clear that it was just a matter of time before the शूलपुरुष-s recovered. Hence, the आन्गलिक-s and their allies tried to curb the शूलपुरुष-s via economic warfare resulting in much damage to them. Importantly, they divided the old शूलपुरुष nation by giving part of their territory to their hated भ्रातृव्य-s, the Poles of श्रव blood and severing off Austria and other bits, granting them to France, Czechs and Slovaks. Finally, they built up the over-confidence of the Poles to such an extent that they thought it would be possible to defeat the शूलपुरुष-s all by themselves. The wild-card in all this was the Rus who were lead by the blood-thirsty Stalin who could nearly match the आन्गलिक-s in his cunning. But the आन्गलिक-s knew that if they played their game just right even Stalin and his vast Rus horde would be sucked into to a 0-sum game with the शूलपुरुष-s. In the extreme scenario, the आन्गलिक-s knew that if things still went south, they could count on their brothers from क्रौञ्चद्वीप, with their अक्षयपात्र of resources to shore them up in that situation. It is amazing that even today these very same mleccha-s play the very same game and minnow nations unerringly fall prey to it to their own detriment: We saw just in the past decade how the mleccha-s incited the Georgians and Ukrainians in conflict with the Rus much as they built up Poland's territorial ambitions with respect to the शूलपुरुष-s. Even as this is being written we are watching the consequences for Ukraine unfolding -- which will probably end up seriously damaged or even destroyed at the end of it.

Going back to WW2, it is clear that the Rus were the biggest contributors to the victory of the victors and that came an enormous human cost to them. The Rus were considered by the western European powers as something of an outcast. First, they were inheritors of the old multi-ethnic heathen state of the Khaganate of the Rus, where Slavic, Germanic, Finnic and Altaic people lived together with the Rus Khagans themselves being apparently of part Germanic ancestry. We have evidence from the discovery of a Hindu idol by Alexander Kozhevin in the middle Volga region raising the possibility that there were even Hindus in this mix. But with conversion of the Rus queen Olga to Christianity things started going down hill, and despite the vigorous efforts of Khagan Sviatoslav to uphold the Slavic version of the Indo-European religion, Vladimir the confused eventually fell victim to infection by the pretamata. He and his clan destroyed the old Slavic temples and "with fire and sword" forcibly converted the people of the Rus to the pretamata. However, Vladimir went to the 'other side' of the deep schism within the pretamata by accepting the orthodox church. This affiliation of the Rus even after conversion to the preta fold was the second factor that kept them apart from the western Europeans and their allies. Subsequently, the star of the Rus dimmed under the defeats at the hands of the Mongols. But being defeated by a heathen rather than Abrahamistic force meant that they retained their preta affinities through this dark phase until they were finally revived by the energetic czar Peter-I. He now put them on the path of competing with the powers of western Europe and they were quick to recognize this. However, they saw the Rus as uncivilized and still of a part Asian type who had rudely broken into the party of the Westerners like a bear entering a house to ransack the kitchen. Thus, they tried their best to keep the Rus out. Just as the west was plundering the Americas and Australia, the Rus too felt their urge of the great exploration and colonization. Being a traditional land power they did it their own way by expanding eastwards into Asia and eventually reaching Alaska, which they were coerced into ceding to the USA by आन्गलिक action.

In Europe the first great continental challenge to the आन्गलिक power emerged in the form of Napoleon Bonaparte, whose campaigns in many ways foreshadowed those of the शूलपुरुष-s in WW2. The Rus saw an opportunity to increase their power and waded into these affairs playing on both sides just as they did in WW2. In the process they expanded their own territory by conquering Georgia and the Derbent Khanate. The activity of the Rus, especially the tacit alliance with the English, alarmed the French who under Napoleon had risen to be the greatest power of continental Europe. Napoleon, now at the head of the biggest army which had ever been assembled in Europe, decided to silence to Rus for good by launching a massive invasion against them. While Czar Alexander and his generals took a heavy beating, and even saw Moscow along with the center of the orthodox church being reduced to ashes, they put up a resolute resistance using the strategic depth of Russia to finally inflict a disastrous defeat on the French.

continued...


