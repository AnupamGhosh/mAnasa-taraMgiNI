
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Polygonal recursion](https://manasataramgini.wordpress.com/2015/02/15/polygonal-recursion/){rel="bookmark"} {#polygonal-recursion .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 15, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/02/15/polygonal-recursion/ "8:07 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Also see [this](https://app.box.com/s/ubg4ved8capyhjrd241nqea4tlvmbxd5)
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-7umJMMnKCHI/VOD6P4nDJuI/AAAAAAAADPw/F_tTmHBUDmk/s800/cos_factorization01.jpg){width="75%"}
```{=latex}
\end{center}
```



