
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पाणिनि, Xuanzang, and तोल्काप्पियऩ्: some legends and history](https://manasataramgini.wordpress.com/2015/09/02/pa%e1%b9%87ini-xuanzang-and-tolkappiya%e1%b9%89-some-legends-and-history/){rel="bookmark"} {#पणन-xuanzang-and-तलकपपयऩ-some-legends-and-history .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 2, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/09/02/pa%e1%b9%87ini-xuanzang-and-tolkappiya%e1%b9%89-some-legends-and-history/ "5:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[A slightly modified version of article was originally published at ईन्दिअFअcत्स्](http://indiafacts.co.in/pa%E1%B9%87ini-xuanzang-and-tolkappiya%E1%B9%89-some-legends-and-history/)

पाणिनि stands at the pinnacle of Hindu intellectual achievement. His सूत्र-पाठ may be considered a monument in the same league as the invention of the शूण्य-based numeral system for which the Hindus are renowned. However, like several other Hindu figures of note, his life and times are the subject of divergent or contradictory narratives. To write about पाणिनि's times would be in large part merely a rehash of Vasudeva Sharana Agrawala's excellent monograph on the topic. Yet, we present some of the material on this matter (indeed partly just retelling what VS Agrawala has covered) for it might be of some interest to those not too well-acquainted with this topic.

A key pre-modern record of पाणिनि by a non-Indian is that given by Xuanzang, the Chinese scholar and agent of the Tang emperor Taizong. In his magnum opus, Da Tang Xiyuji, he provides a detailed report of his journey to and within India, which was presented at the Tang court in Chang'an before the emperor Taizong in 646 CE. While he was born in an orthodox Confucian family, Xuanzang became acquainted with bauddha texts early in his life from his brothers, and decided to study the bauddha-mata in greater depth. In 626 CE, the 24 year old Xuanzang became interested in the in depth study of Sanskrit as he was working through महायान texts and the associated योगचार literature. After having acquired a good grasp of the language after three years of study he had a dream, in which he saw himself being in India. This urged him to undertake a journey to India and directly study the texts of the bauddha-mata in their original language. He immediately set off for India having surreptitiously crossed the Chinese border with the Blue (Gök)Turk empire. The Khan of the Turks was pleased with his interaction with Xuanzang and gave his party a feast and assistance to reach the city of Tashkent and then enter the Iranian territory at Samarkand. The Iranian lord of Samarkand heard his lecture on महायान and provided passage to go southwards towards India.
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-T0XhIc4B7B4/VeaKY2MB2uI/AAAAAAAADWY/iSMJjPbBrhc/s800-Ic42/shalAtura.JPG){width="75%"}
```{=latex}
\end{center}
```

*Location of शलातुर*

Having entered India, he visited the visited the विहार-s in बाह्लिक and गन्धार and eventually reached शलातुर, which is today the village of Chota Lahore in the terrorist state of Pakistan (on the North-west bank of the Sindhu river, moving north from Attock). Xuanzang wrote in his Da Tang Xiyuji: "To the north-west of U-to-kia-han-c'ho at a distance of 20 li or so we come to the town of So-ls-tu-lo (i.e. शलातुर). This is the place where the ऋषि पाणिनि, who composed the Ching-ming-lun (the अष्टाध्यायि) was born... The children of this town, who are his disciples, revere his eminent qualities, and a statue erected to his memory still exists." Evidently this statue of पाणिनि has been destroyed by the Moslems who have since occupied those regions. However, the village Chota Lahore apparently has several elevated mounds corresponding to the ruins of the ancient town of शलातुर, which were briefly explored by a Belgian woman named Corbeau close the middle of the last century; many Gandharan artifacts from there have apparently been looted and sold to western markets by the Pakistanis. The monumental effort of पाणिनि is sometimes referred to in Hindu tradition as the शालातुरीय-matam after this now ruined town. Indeed, this is a stark reminder of how a site so important to the Hindu tradition, a center from which a monument of human thought emerged, can be so completely erased by the barbarism of the third Abrahamism -- a fate that can overtake the whole of जंबुद्वीप.

Then the Da Tang Xiyuji records an interesting tradition regarding the life of पाणिनि:\
"Referring to the most ancient times, letters were very numerous; but when in the process of ages, the world was destroyed and remained as a void, the Deva-s of long life descended spiritually to guide the people. Such was the origin of the ancient letters and composition. From this time and after it the source (i.e. the root language) spread and passed its (former) bounds. Brahma-deva and शक्र-deva established rules according to requirements. ऋषि-s belonging to different schools each drew up forms of letters. Men in their successive generations put into use what had been delivered to them; but nevertheless students without ability were unable to make use (of these teachings). And now men's lives were reduced to the length of a hundred years, when ऋषि पाणिनि was born; he was from birth extensively informed about things. The times being dull and careless, he wished to reform the vague and false rules -- to fix the rules and correct improprieties (of usage). As he wandered about asking for the right ways, he encountered ईश्वर-deva and recounted to him the plan of his undertaking. ईश्वर-deva said, "Wonderful! I will assist you in this." The ऋषि, having received instruction, retired. He then labored incessantly and put forth all his power of mind. He collected a multitude of words (i.e. the गण-पाठ), and made a book on letters which contained a thousand श्लोक-s; each श्लोक was of 32 syllables. It contained everything known from the first till then, without exception, respecting letters and words. He then closed it and sent it to the supreme ruler, who exceedingly prized it, and issued an edict that throughout the kingdom it should be used and taught to others; and he added that whoever should learn it from beginning to end should receive as his reward a thousand pieces of gold. And so from that time masters have received it and handed it down in its completeness for the good of the world. Hence, the ब्राह्मण-s of this town are well-grounded in their literary work, and are of high renown for their talents, well-informed as to things, and of a vigorous understanding."

VS Agrawala discusses this account in detail, and, in our opinion, correctly indicates that Xuanzang is recording a real Hindu tradition. As for the अष्टाध्यायि being in thousand श्लोक-s rather than the सूत्र-पाठ as we have it many divergent views are offered; however, Agrawala supports the view that it was merely an approximation of the actual number of syllables in the सूत्र-पाठ, which add up to approximately 32,000. Further, Agrawala holds the view that the presentation of the अष्टाध्यायि to the supreme ruler of India (चीन term: da wang) is related to by an account given by learned राज्पूत् author राजशेखर in the प्रतिहार court (\~late 800s-900s of CE):\
[श्रूयते च पाटलिपुत्रे शास्त्रकार-परीक्षा:]{style="color:#0000ff;"}\
[अत्रोपवर्ष-वर्षव् इह पाणिनि-पिङ्गलाव् इह व्याडिः ।]{style="color:#0000ff;"}\
[वररुचि पतङ्जली इह परीक्षिताः ख्यातिम् उपजग्मुः ॥]{style="color:#0000ff;"}

One hears that in पाटलिपुत्र (modern Patna) there was an examination of the authors of technical works: It was here that उपवर्ष, वर्ष, पाणिनि, पिङ्गल, व्याडि, Vararuci and पतङ्जलिन् were tested and thereby attained fame.

That राजशेखर was recording an ancient tradition becomes evident from the Greek philosopher Strabo(\~64 BC--24 CE)'s account of the court of पाटलिपुत्र (Book 15.1, section 39):

"...the population of India is divided into seven castes: the one first in honor, but the fewest in number, consists of the philosophers (the Brachmanes); and these philosophers are used, each individually, by the people making sacrifice to the gods or making offerings to the dead, but jointly by the kings at the Great Synod (महासभा at पाटलिपुत्र), as it is called, at which, at the beginning of the new year, the philosophers, one and all, come together at the gates of the king; and whatever each man has drawn up in writing or observed as useful with reference to the prosperity of either fruits or living beings or concerning the government, he brings forward in public; and he who is thrice found false is required by law to keep silence for life, whereas he who has proved correct is adjudged exempt from tribute and taxes."\
Hence, it is indeed likely that पाणिनि had presented his work at such a महासभा at the capital of the Magadhan empire and it was accepted by it. This was probably a long-standing practice in the region going back to the Mithilan court where the Janaka-s held such सभा-s (described in the ब्राह्मण literature) and eventually shifted to पाटलिपुत्र with rise of the Magadhan power.

This leads us to another Hindu tradition regarding पाणिनि: In the बृहत्कथा tradition, the scholars उपवर्ष, वर्ष, पाणिनि, पिङ्गल, व्याडि, Vararuci are all linked together by a web of connections. While at पाटलिपुत्र, Vararuci and व्याडि are said to have been students of वर्ष. Then Vararuci was enamored by the beauty of वर्ष's niece, उपकोशा, the daughter of उपवर्ष, and wooed her till उपवर्ष acceded to marry her to Vararuci. After Vararuci and व्याडि graduated, वर्ष got पाणिनि as a student from the northwest, whom Vararuci described as being an idiot. पाणिनि learned little, and one day वर्ष's wife being frustrated with him threw him out. The dejected पाणिनि is then said to have gone to the हिमालय to worship the god Rudra with austerities. Finally, please with पाणिनि, Rudra gave him a new grammar of the Sanskrit language. Armed with this पाणिनि is said to have returned to पाटलिपुत्र and challenged Vararuci in the सभा. Their debate raged for seven days and Vararuci felt he was close to defeating पाणिनि. But then on the eight day Rudra made a terrible noise from the skies and the Aindra grammar, which was taught in the days of the Veda by Indra, in which Vararuci was an expert, was erased. With that पाणिनि emerged the victor. Utterly dejected by this, Vararuci is then said to have left his wife with his mother, deposited his wealth with a वैश्य, and gone to the हिमालय to propitiate Rudra. In the mean time his wife उपकोशा is said to have been violently harried by the वैश्य and other men seeking to have extra-marital affairs with her. However, she tricked them by inviting them to her house and capturing them in a box and had them conveyed to emperor Nanda's court. Nanda then arrested them and exiled them. In the meantime, Vararuci succeeded in pleasing Rudra and returned upon receiving the same grammar as पाणिनि. He was elated to hear how his wife had outwitted the rival males and now lived a contended life with his newly acquired grammatical knowledge. His former teacher वर्ष too now expressed the wish to acquire the same grammar. He worshiped the god कुमार who then gave him the same grammar.

This narrative is contrasts that of Xuanzang. There, पाणिनि is a great genius, who is said to have been informed of all things right from his birth. In that narrative Rudra's teaching is only the start of a process of intense mental effort on part of पाणिनि. This is also consistent with the tradition that while पाणिनि obtained the माहेश्वर सूत्राणि from Rudra, the rest of the exposition was his own effort. Indeed, Xuanzang's story is more in line with the tradition of the पाणिनीय-s who acknowledge that पाणिनि was a supreme scholar. As Agrawala points out the काशिक commentary on पाणिनि plainly states:

[महती सूक्ष्मेक्षिका वर्तते सूत्रकारस्य ।]{style="color:#0000ff;"}\
The सूत्रकार (i.e. पाणिनि)'s insight is profound and subtle.

However, the narrative of the बृहत्कथा tradition attempts to consistently put down पाणिनि:

 1.  It suggests that पाणिनि was a sluggard who got his grammar purely due to divine intervention.

 2.  It further makes the point that even with the superior grammar he was unable to defeat Vararuci, the foremost proponent of the Aindra school of grammar -- even here his victory was solely due to Rudra's aggressive intervention on his behalf.

 3.  It then suggests that, although Vararuci and वर्ष eventually switched to the पाणिनीय school, they did not acquire it from पाणिनि. Rather, they independently obtained it respectively from Rudra and कुमार.

This suggests that the older Aindra school, while beaten, only grudgingly accepted the defeat. Their chief proponent Vararuci seems to have pushed his own story, where पाणिनि is not only put down but his role is also minimized -- he played no part in their eventual acceptance of the पाणिनीय system; rather, they all obtained the same grammar independently from the gods by themselves. This is keeping with the evidence that that the Aindra school was once influential, and would not have easily bowed out to पाणिनि's despite its apparent superiority. It seems to have lingered on in the peripheral zones. In the द्रमिड country, it was the Aindra grammar that was used as the model for the analysis of a language from a totally different family, तमिऴ् (a Dravidian language), resulting in its early grammar, the तोल्काप्पियम्. A preface appended to that work states:

[वट वेङ्कटम् तॆऩ् कुमरि]{style="color:#33cccc;"}\
[आयिटैत्]{style="color:#33cccc;"}\
[तमिऴ् कूऱुम् नल् उलकत्तु]{style="color:#33cccc;"}\
[वऴक्कुम् चेय्युऌउम् आयिरु मुतलिऩ्]{style="color:#33cccc;"}\
[एऴुत्तुम् चोल्लुम् पोरुऌउम् नाटिच् ५]{style="color:#33cccc;"}\
[चेन्-तमिऴ् इयऱ्‌कै चिवणिय निलत्तोटु]{style="color:#33cccc;"}\
[मुन्तु नूल् कण्टु मुऱैप्पट एण्णिप्]{style="color:#33cccc;"}\
[पुलम् तॊकुत्तोऩे पोक्कु अऱु पऩुवल्]{style="color:#33cccc;"}\
[निलम्-तरु तिरुविऩ् पाण्टियऩ् अवैयत्तु]{style="color:#33cccc;"}\
[अऱम् करै नाविऩ् नाऩ्मऱै मुऱ्‌ऱिय १०]{style="color:#33cccc;"}\
[अतङ्कोट्टु आचाऱ्‌कु अरिल् तपत् तॆरिन्तु]{style="color:#33cccc;"}\
[मयङ्का मरपिऩ् एऴुत्तु मुऱै काट्टि]{style="color:#33cccc;"}\
[मल्कु नीर् वरैप्पिऩ् ऐन्तिरम् निऱैन्त]{style="color:#33cccc;"}\
[तॊल्काप्पियऩ् ऎऩत् तऩ् पॆयर् तोऱ्‌ऱिप्]{style="color:#33cccc;"}\
[पल् पुकऴ् निऱुत्त पटिमैयोऩे। १५]{style="color:#33cccc;"}

व्ēङ्कटम् \[Tirupati hills] in the north, Kumari (Southern tip of peninsula) in south,\
where तमिऴ् is spoken in the good world,\
common and poetic usage, beginning with these two,\
analyzing syllables, words, and meanings,\
with the topic closely related to chaste Tamil,\
seeing the foremost text, devising it to be in proper form,\
a faultless discourse, he strung together\
in the assembly of the पाण्टियऩ् monarch Nilamtaru Tiruviṉ,\
with his tongue the shore of dharma \[ocean], knowledgeable in \[all] 4 Veda-s,\
अतङ्क्ōट्टु आचार्य examined and corrected errors,\
showing as per un-deluded tradition the formulation of syllables,\
full of the Aindra \[grammar] like the ocean of water,\
presenting his name as तोल्काप्पियऩ्,\
He is the standard (Skt: प्रथिमा) established in great fame.\
(Translation done with the help of a relative familiar with classical तमिऴ् usage)

There are some interesting features concerning this narrative regarding the तोल्काप्पियम्: First, it is said to have been presented at the पाण्टियऩ् court in a manner similar to what is said of the पाणिनीय grammar at the Magadhan court. Second, this act of presentation in the court, and it is apparent subsequent acceptance, seems to have been the basis of it being established as a standard. This again parallels the case of पाणिनि. Finally, it is of considerable significance to note that तोल्काप्पियऩ् is said to have studied the colloquial and poetic usage of तमिऴ् throughout the द्रमिड country, from Tirupati to Kanyakumari and analyzed it thoroughly to generate his grammar. This is an exact parallel of पाणिनि's creation of the famed गण-पाठ, which stands as one of the remarkable ethnological, geographical and linguistic explorations in the history of science. पाणिनि gathered comparable data from the greater Indo-Aryan realm from प्रकण्व (the Ferghana region of the modern Uzbek-Kyrgyz zone) to कलिङ्ग (Odisha) and सूरमस (Assam). This suggests that as the big Tamil kingdoms arose, several models, already perfected in the North, were transplanted to the southern courts, sparking similar endeavors in the पाण्टियऩ् realm. The fact that an Aindra grammarian carried out a comparable exercise as पाणिनि in South India suggests that this method had already been imbibed by the rival school or perhaps both पाणिनि and तोल्काप्पियऩ् inherited this methodological framework from the original Aindra tradition.

This finally leads us to the thorny question of the date of पाणिनि. Given that the बृहत्कथा tradition places him as a contemporary of the first Nanda, one might say that he lived around 350 BCE. This tradition is also supported by the bauddha क्रिया tantra, the ंअञ्जुśर्īय-मूलकल्प, where पाणिनि is described as a good friend of [महापद्म]-nanda. Most white indologists, who generally favor late dates for all Hindu texts, also not surprisingly find such a date appealing. Further, it is suggested that this also brings him comfortably close to the date of his second great commentator पतङ्जली (first being कात्यायन) who is placed in the शुङ्ग court. However, despite the existence of such a tradition, we recommend caution in ascribing such a late date to पाणिनि. The political geographical tradition that he records is clearly more consistent with several smaller एकराज monarchies and संघ republics belonging to a period prior to the rise of the large Magadhan empire of the Nanda-s. We might also note in this regard that there is tendency in काव्य literature to "telescope" history by combining characters of different eras into a single historical layer. We see this even in the later Gupta court, where भर्तृहरि, वराहमिहिर, शबर-स्वामिन् and कालिदास are all stated as existing side-by-side in विक्रमादित्य's court. Hence, we cannot uncritically accept पाणिनि as a member of the Nanda court. Nevertheless, we believe that tradition is correctly recording the role of the Magadhan scholarly assembly in peer-reviewing and accepting पाणिनि's monumental work.

Further reading: India As Known To पाणिनि by Vasudeva Sharana Agrawala


