
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Icons](https://manasataramgini.wordpress.com/2015/05/06/icons/){rel="bookmark"} {#icons .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 6, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/05/06/icons/ "5:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-D_bIGGHHjNw/VUmfgZ-5GkI/AAAAAAAADSk/Tlf8r-uQu30/s800/Chaos01.jpg){width="75%"}
```{=latex}
\end{center}
```



