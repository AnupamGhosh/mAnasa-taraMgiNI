
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Towers and pits](https://manasataramgini.wordpress.com/2015/09/23/towers-and-pits/){rel="bookmark"} {#towers-and-pits .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 23, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/09/23/towers-and-pits/ "6:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-L0en7aarzSg/VgJKKPExFhI/AAAAAAAADXY/f2F3AjZAe88/s800-Ic42/towers.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-pX6shVQhpEo/VgJKKD4oZCI/AAAAAAAADXU/lc3Yh3WS2bU/s800-Ic42/pits.jpg){width="75%"}
```{=latex}
\end{center}
```



