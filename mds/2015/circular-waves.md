
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Circular waves](https://manasataramgini.wordpress.com/2015/02/15/circular-waves/){rel="bookmark"} {#circular-waves .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 15, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/02/15/circular-waves/ "8:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-wl6e3aKTmMg/VOD1DIMV-pI/AAAAAAAADPU/QJDCOPgzAXE/s800/waves_sinous04.jpg){width="75%"}
```{=latex}
\end{center}
```



