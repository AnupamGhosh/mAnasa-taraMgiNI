
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Chaotic flows](https://manasataramgini.wordpress.com/2015/05/06/chaotic-flows-2/){rel="bookmark"} {#chaotic-flows .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 6, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/05/06/chaotic-flows-2/ "5:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-08os3I9siOk/VUmfgsK4SeI/AAAAAAAADSo/oK9257A8gJk/s800/Chaos02.jpg){width="75%"}
```{=latex}
\end{center}
```



