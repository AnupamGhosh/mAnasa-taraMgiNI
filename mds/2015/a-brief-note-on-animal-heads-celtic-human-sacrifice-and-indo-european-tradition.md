
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief note on animal heads, Celtic human sacrifice, and Indo-European tradition](https://manasataramgini.wordpress.com/2015/08/23/a-brief-note-on-animal-heads-celtic-human-sacrifice-and-indo-european-tradition/){rel="bookmark"} {#a-brief-note-on-animal-heads-celtic-human-sacrifice-and-indo-european-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 23, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/08/23/a-brief-note-on-animal-heads-celtic-human-sacrifice-and-indo-european-tradition/ "7:43 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our illustrious ancestor दध्यङ्च् आथर्वण is supposed to have possessed privileged knowledge from the great Indra that even the twin gods, the अश्विन्-s, sought to get it from him. However, speaking out this secret knowledge would have cost him his head as that was the condition under which Indra had imparted it to him. Hence, they surgically fitted him with a horse's head so that he could convey it to him. The great ऋषि कक्षीवान्, the son of दीर्घतमस्, the founder of the Gotama clan, alluded to this act of the अश्विन्-s in a mantra in the ऋग्वेद thus:

[तद् वां नरा सनये दंस उग्रम्]{style="color:#0000ff;"}\
[आविष् कृणोमि तन्यतुर् न वृष्टिम् ।]{style="color:#0000ff;"}\
[दध्यङ् ह यन् मध्व् आथर्वणो वाम्]{style="color:#0000ff;"}\
[अश्वस्य शीर्ष्णा प्र यद् ईम् उवाच ॥]{style="color:#0000ff;"} RV 1.116.12

O manly twins, that awful wonder-act of yours \[done] for gain,\
I make widely known, as thunder \[announces] rain,\
indeed दध्यङ्च् आथर्वण spoke to you two\
that which is "honey" through the head of a horse.

In शतपथ-ब्राह्मण 14.1.1.25 it is stated that the privileged knowledge possessed by दध्यङ्च् आथर्वण concerns the Pravargya ritual by which the Soma-याग is made "whole". The Pravargya ritual again refers to a severed head, that of Makha, विष्णु, or Rudra in different ब्राह्मण-s, which is represented by the central implement of the ritual, the Gharma pot. The शतपथ-ब्राह्मण 14.1.4.13 further elaborates this Madhu-विद्या and it is presented as the high teaching of the उपनिषत् in 14.5.5 (i.e. the बृहदारण्यकोपनिषत्).

This mythic nucleus serves as a locus for further elaboration in later Hindu tradition. The पुराण-s incorporate the frame of the Pravargya myth into a narrative to explain how the horse-headed विष्णु came into being. His head is described as being severed, even as it was in one of the Pravargya legend, and was then replaced with that of horse. This horse-headed विष्णु is then able to slay a horse-headed asura, who had invulnerability from everyone else, except another with a horse's head.

Interestingly, Russian archaeologists claimed that such a chimeric form, a man with a horse's head, was found in a kurgan of the Potapovka culture \[A sister of the more famous Sintashta culture] near the Samara Bend on the Volga steppes -- a place close the original homeland of the Aryans. However, subsequent dating showed that the human and horse skeletons belong to different ages and the superimposition of two separate burials human and horse at the same site separated by several hundreds of years had accidentally created the impression of a chimera.

Archaeological chimeras have nevertheless re-emerged recently. An Iron Age site from Dorset, UK has provided extensive evidence for chimeric creations by Britonic Celts. These include chimeras between horses and cows as well as burial of multiple heads of sacrificed animals. It is uncertain if these were purely Celtic innovations or have earlier Indo-European precedents. The human sacrifice at the site seems to have placed the human remains on various animals remains with a correspondence of the parts.

This is reminiscent of aspects of the funerary custom of our ancient आथर्वण ancestors (कौशिक-सूत्र 80-89): cremation of the human along with a sacrificed animal. An animal, typically a cow, is sacrificed and its parts are cut up carefully and placed on the corresponding parts of the आथर्वण's body along with all the ritual implements he had used while alive, smeared with goat butter. His right hand is made to hold a staff. The text specifies that in the case of a क्षत्रिय his bow is placed instead. Thereafter, it being a cremation, the equivalence diverges. The pyre is then lit using the fires in which he had made offerings during his life and an invocation of Yama is made, followed by the mantra honoring the ancient अङ्गिरस्-es and भृगु-s. Once he has burned his remains are collected sprinkled with milk and collected in an urn mixed with scented powders and unguents. Then the urn with the remains or just the remains are buried and a श्मशान is piled over it. A version of a similar cremation is narrated in the Greek epic by Agamemnon's ghost to Achilles about his own funeral:

*The daughters of the "Old Man of the Sea" stood around your \[Achilles] corpse lamenting bitterly. They wrapped your body in an imperishable shroud. And the nine Muses chanted your dirge, responding each to each in their sweet voices. There was not a single Argive to be seen without tears in his eyes, so moving was the clear song of the Muse. Immortal gods and mortal men, we mourned for you, seventeen days and nights, and on the eighteenth we delivered you to the flames, sacrificing herds of fattened sheep and spiral-horned cattle round you. You were burnt clothed as a god, drowned in unguents and sweet honey, and a host of Achaean heroes streamed past your pyre as you burned, warriors and charioteers, making a vast noise. And at dawn, Achilles, when Hephaestus' fires had eaten you, we gathered up your whitened ash and bone, and steeped them in oil and unmixed wine. Your mother gave us a gold two-handled urn, saying it was the gift of Dionysus, and crafted by far-famed Hephaestus himself. There your ashes lie, my glorious Achilles, mixed with the bones of the dead Patroclus, Menoetius' son, but separated from those of Antilochus, who next to dead Patroclus you loved most among your comrades. And on a headland thrusting into the wide Hellespont we, the great host of Argive spearmen, heaped a vast flawless mound above them, so it might be seen far out to sea by men who live now and those to come.*

The Greek account is notable in several ways: The parallels to the Atharvan version (and more generally the Vaidika version) is apparent. The use of sweet honey is interesting for the Atharvanic injunction for a क्षत्रिय's corpse is: "मधूत्सिक्तेन क्षत्रियस्यावसिञ्चति" (A stream of honey is poured over the क्षत्रिय's); thus, Achilles was being given a क्षत्रिय's funeral. Eating of Achilles by the fires of Hephaestus may be compared with the phrase of the corpse-eating Agni क्रव्याद in the Veda. The Kurgan custom of the steppes is also retained by the Greeks who pile a Kurgan for Achilles, even as described in the Atharvan and ऋग्वेदिच् Kurgan burial.

Irrespective of whether the chimeras and sacrificed animal heads at the Celtic site had any earlier Indo-European connection, we find references to both such animal heads and the chimeras in the Vedic ritual.

In the soma ritual at the base of the altar heads of five animals, including humans, are laid as a foundation. This is described thus in the ब्राह्मण section of the तैत्तिरीय-संहिता thus:

[प्रजापतिर् अग्निम् असृजत ।]{style="color:#0000ff;"}\
प्रजापति emitted Agni.

[सो 'स्मात् सृष्टः प्राङ् प्रादरवत् तस्मा अश्वम् प्रत्य् आस्यत् ।]{style="color:#0000ff;"}\
He (Agni) \[when] emitted ran away east from him (प्रजापति); he (प्रजापति) hurled a horse at him (Agni).

[स दक्षिणावर्तत तस्मै वृष्णिम् प्रत्य् आस्यत् ।]{style="color:#0000ff;"}\
He (Agni) turned to the south; he (प्रजापति) hurled a ram at him (Agni).

[स प्रत्यङ्ङ् आवर्तत तस्मा ऋषभम् प्रत्य् आस्यत् ।]{style="color:#0000ff;"}\
He (Agni) turned to the west; he (प्रजापति) hurled a bull at him (Agni).

[स उदङ्ङ् आवर्तत तस्मै बस्तम् प्रत्य् आस्यत् ।]{style="color:#0000ff;"}\
He (Agni) turned to the north; he (प्रजापति) hurled a goat at him (Agni).

[स ऊर्ध्वो 'द्रवत् तस्मै पुरुषम् प्रत्य् आस्यत् ।]{style="color:#0000ff;"}\
He (Agni) fled upwards; he (प्रजापति) hurled a man at him (Agni).

[यत् पशु-शीर्षाण्य् उपदधाति सर्वत एवैनम् अवरुध्य चिनुते ।]{style="color:#0000ff;"}\
Thus, he (the ritual specialist: adhvaryu) places the animal-heads, enclosing them all around, he piles \[the altar].

[एता वै प्राण-भृतश् चक्षुष्मतीर् इष्टका यत् पशु-शीर्षाणि ।]{style="color:#0000ff;"}\
These, the animal-heads, truly life-supporting \[and] possessed of sight are the bricks (i.e. of the ritual's foundation).

[यत् पशु-शीर्षाण्य् उपदधाति ताभिर् एव यजमानो 'मुष्मिन् लोके प्राणित्य् अथो ताभिर् एवास्मा इमे लोकाः प्र भान्ति ।]{style="color:#0000ff;"}\
Because he places the animal-heads, the ritualist lives (breathes) by means of them in that \[other] world; also indeed these worlds shine forth for him by them (the heads).

[मृदाभिलिप्योप दधाति मेध्यत्वाय ।]{style="color:#0000ff;"}\
Having smeared them (the heads) with mud for ritual purity, he places them down.

[पशुर् वा एष यद् अग्निर् अन्नम् पशव एष खलु वा अग्निर् यत् पशुशीर्षाणि ।]{style="color:#0000ff;"}\
Agni, indeed is \[embodied in] an animal; animals are food; verily the animal-heads are this Agni.

[यं कामयेत कनीयो 'स्यान्नम् स्याद् इति संतरां तस्य पशुशीर्षाण्य् उप दध्यात् कनीय एवास्यान्नम् भवति ।]{style="color:#0000ff;"}\
If he perhaps wishes that: 'May his food be less', he should lay his animals-heads more closely together; verily his food becomes less.

[यं कामयेत समावद् अस्यान्नं स्याद् इति मध्यतस् तस्योप दध्यात् समावद् एवास्यान्नम् भवति ।]{style="color:#0000ff;"}\
If he perhaps wishes that: 'May his food remain the same', he should lay his animals-heads medium \[spacing apart]; verily his food remains the same.

[यं कामयेत भूयो 'स्यान्नं स्याद् इत्य् अन्तेषु तस्य व्युदूह्योप दध्याद् अन्तत एवास्मा अन्नम् अव रुन्द्धे भूयो 'स्यान्नम् भवति ॥]{style="color:#0000ff;"}\
If he perhaps wishes that: 'May his food be more', he should lay his animal-heads widely-separated at the ends of the pit; verily by enclosing the food his food becomes more.

Thus, the different animal-heads being placed at the base of the soma ritual altar are reminiscent of the multiple heads of different animals found at the Celtic site. While in many modern performances clay or golden heads might be used, it is clear that in the earlier ritual actual heads were used. As for the human head it is apparent that it was not obtained by human sacrifice in the core श्रौत tradition. आपस्तम्ब clarifies that the corpse of a क्षत्रिय or वैश्य who has been slain in warfare by an arrow or struck dead by lightning is purchased for 7 or 21 units of cash and the head is severed at the time of the sale. The कठ-s clarify that a dead man's head is bought for 21 units of cash at a cemetery and severed from the corpse at the time of purchase.

Finally, coming to the issue of chimeric animals in the veda, we might cite the famous verse of वामदेव Gautama on the embodiment of Sanskrit language as chimeric animal:

[चत्वारि शृङ्गा त्रयो अस्य पादा]{style="color:#0000ff;"}\
[द्वे शीर्षे सप्त हस्तासो अस्य ।]{style="color:#0000ff;"}\
[त्रिधा बद्धो वृषभो रोरवीति]{style="color:#0000ff;"}\
[महो देवो मर्त्यां आ विवेश ॥]{style="color:#0000ff;"} RV 4.58.3

Four horns, his feet are three,\
his heads are two, his hands seven;\
bound thrice the bull roars,\
the great god has entered into the mortals.

The god Agni enters the mortals as the Sanskrit language comprised of:\
4 four horns: nominals, verbs, preverbs and particles.\
3 feet: the 3 accents of the old language.\
2 heads: the vocalized word and the inner word associated with the first person experience of meaning.\
7 hands: the 7 cases\
3 bonds: The articulations in the lungs, throat and head that express the language in spoke form.

We shall be elaborating on this and its significance to the performance of ritual in a separate story.


