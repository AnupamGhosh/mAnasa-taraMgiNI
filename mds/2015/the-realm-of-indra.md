
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The realm of indra](https://manasataramgini.wordpress.com/2015/08/01/the-realm-of-indra/){rel="bookmark"} {#the-realm-of-indra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 1, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/08/01/the-realm-of-indra/ "3:22 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A version of this article was published earlier at [India Facts](http://indiafacts.co.in/the-realm-of-indra/)

Indra, is the Indo-Aryan version of the preeminent deity of the ancestral Indo-European pantheon. In the Vedic texts we see remnants of what might have been an ancient tripartation of this deity, as manifest on the earth, in the atmosphere and in the heavens. In the first realm he is manifest a warrior who inspires men in battle and comes to the aid of his आर्य worshipers who offer him rituals -- in this form he is typically called Indra or Maghavan or वृत्रहन्. In his atmospheric form he is Parjanya, who abundantly pours down life-giving rain. In his celestial manifestation in the high heavens he is Dyaus. Already in the ऋग्वेद, the earliest surviving Indo-Aryan text, under the name Indra he comprehensively encompasses all three functions. Yet, the old memory of tripartation is still seen as both Parjanya and Dyaus are retained as distinct देवता-s with specific functions in the atmospheric and celestial domains. By the time of the महाभारत, Parjanya has already mostly merged into Indra and lost his distinctness and Dyaus is only faintly remembered within the ogdoad of Vasu-s \[incarnated on earth as the पितामह भीष्म]. In the rest of the Indo-European world we see a similar consolidation of the ancestral tripartation into a single figure, but in each case a different strand of original triad is chosen for consolidation. In the Greek and Roman world we have the cognate deities Zeus and Iuppiter (Ius+piter being father), where both are cognates of Dyaus. In the Baltic and Slavic world we have Perkunas and Perun, both of which are cognates of Parjanya. In Lithuania, the name of the place featuring the great shrine of Perkunas with a holy oak tree from pre-Christian times was पेर्कूनिज -- a cognate of Parjanya. However, in Lithuania the cognate of Indra continued to survive as Indraja, the name of the deity of the planet Jupiter. In the Indosphere and among the Kalasha we see the consolidation under Indra.

While Indra is the preeminent deity of the ancient Vedic rituals, in the later period his prominence was challenged by sects centered on other deities like प्रजापति (ब्रह्मा), बृहस्पति, Rudra, विष्णु and कुमार. This challenge becomes visible first in the latter Vedic texts, especially the ब्राह्मण-s (including their terminal उपनिषद्-s) and becomes rather ubiquitous in the epics and the पुराण-s. Such challenges to the preeminence of the Indra-like deity are also seen in other branches of the Indo-European world: In the Iranosphere Ahura Mazda, a cognate of the Indian वरुण, takes over as the supreme deity. In the Germanic world Odin, belonging to the Rudra-class of deities takes over while also subsuming few elements of वरुण in his persona. While the Veda-s abound in mantra-s to Indra, they are allusive in nature. Hence, unlike the deities who became prominent later, some of legendary materials, similar to those encountered in the पुराण-s for deities like Rudra, विष्णु and कुमार, have been forgotten. Yet, the रामायण and महाभारत still retain some memory of the system in which Indra is preeminent and consequently preserve some such legendary material of interest. Indeed, such material seems to have been known to the ऐतिहासिक-s, as suggested by the specific reference to them by Skandasवाmin, the old commentator of the ऋग्वेद. One such is the description of Indra's realm to युधिष्ठिर and his brothers by the sage नारद. On account of it being one of those pieces from the great epic plainly retaining the ancient preeminence of Indra we present it below in full (Mbh 2.7.1-26ab):

[शक्रस्य तु सभा दिव्या भास्वरा कर्मभिर् जिता ।]{style="color:#99cc00;"}\
[स्वयं शक्रेण कौरव्य निर्मित+अर्क-समप्रभा ॥]{style="color:#99cc00;"}

Indeed, the divine and radiant assembly hall of शक्र was achieved by his own deeds. O Kauravya, with splendor like that of the sun, it was built by शक्र himself.

[विस्तीर्णा योजन-शतं शतम् अध्यर्धम् आयता ।]{style="color:#99cc00;"}\
[वैहायसी काम-गमा पञ्च-योजनम् उच्छ्रिता ॥]{style="color:#99cc00;"}

It is laid out as a rectangle, 100 yojanas in breadth, 150 yojana-s in length and 5 yojana-s in height. It is suspended space and can go anywhere at will.

[जरा-शोक-क्लमापेता निरातङ्का शिवा शुभा ।]{style="color:#99cc00;"}\
[वेश्मासनवती रम्या दिव्य-पादप-शोभिता ॥]{style="color:#99cc00;"}

Driving away weakness of age and anguish, free from affliction, benevolent and auspicious is provided with chambers and seats and decorated with divine trees.

[तस्यां देवेश्वरः पार्थ सभायां परमासने ।]{style="color:#99cc00;"}\
[आस्ते शच्या महेन्द्राण्या श्रिया लक्ष्म्या च भारत ॥]{style="color:#99cc00;"}

O पार्थ, in that assembly hall, on the foremost of thrones sits the lord of the gods with शची, who, O भारत, is the the great इन्द्राणी \[also known as] श्री and लक्ष्मी.

[बिभ्रद् वपुर् अनिर्देश्यं किरीटी लोहित+अङ्गदः ।]{style="color:#99cc00;"}\
[विरजो'म्बरश् चित्रमाल्यो ह्री-कीर्ति-द्युतिभिः सह ॥]{style="color:#99cc00;"}

Bearing an indescribable form, crowned, and with ruby-red bracelets, wearing pure attire, with beautiful garlands, \[he sits] accompanied by the goddesses of modesty, fame, and majesty.

[तस्याम् उपासते नित्यं महात्मानं शतक्रतुम् ।]{style="color:#99cc00;"}\
[मरुतः सर्वतो राजन् सर्वे च गृहमेधिनः ।]{style="color:#99cc00;"}\
[सिद्धा देवर्षयश् चैव साध्या देवगणास् तथा ।]{style="color:#99cc00;"}\
[मरुत्वन्तश् च सहिता भास्वन्तो हेममालिनः ॥]{style="color:#99cc00;"} \[double अनुष्टुभ्]

O king, in that hall, the with Marut-s all around, all of whom are the receivers of the offerings in the गृहमेध ritual, the siddha-s, देवर्षि-s, साध्य-s, hosts of gods, all accompanied by the Marut-s and shining forth with golden garlands continually worship the mighty one, the god of a hundred acts.

[एते सानुचराः सर्वे दिव्यरूपाः स्वलंकृताः ।]{style="color:#99cc00;"}\
[उपासते महात्मानं देवराजम् अरिंदमम् ॥]{style="color:#99cc00;"}

These, his attendants, of divine form and well-ornamented, worship the great soul, the king of the gods, the crusher of foes.

तथा देवर्षयः sarve पार्थ शक्रम् उपासते |\
अमला धूत-पाप्मानो दीप्यमाना इवाग्नयः |\
तेजस्विनः somayujo विपापा vigata-क्लमाः ||

Then, O पार्थ, all the देवर्षि-s, clean, rid of their evil, blazing like fires, full of luster, united with soma, sinless, having overcome all weakness, worship शक्र

[पराशरः पर्वतश् च तथा सावर्णिगालवौ ।]{style="color:#99cc00;"}\
[एकतश् च द्वितश् चैव त्रितश् चैव महामुनिः ।]{style="color:#99cc00;"}\
[शङ्खश् च लिखितश् चैव तथा गौरशिरा मुनिः ॥]{style="color:#99cc00;"} [3-footed अनुष्टुभ्]\
[दुर्वासाश् च दीर्घतमा याज्ञवल्क्यो 'थ भालुकिः ।]{style="color:#99cc00;"}\
[उद्दालकः श्वेतकेतुस् तथा शाट्यायनः प्रभुः ॥]{style="color:#99cc00;"}\
[हविष्मांश् च गविष्ठश् च हरिश्चन्द्रश् च पार्थिवः ।]{style="color:#99cc00;"}\
[हृद्यश् चोदरशाण्डिल्यः पाराशर्यः कृषीवलः ॥]{style="color:#99cc00;"}\
[वातस्कन्धो विशाखश् च विधाता काल एव च ।]{style="color:#99cc00;"}\
[अनन्तदन्तस् त्वष्टा च विश्वकर्मा च तुम्बुरुः ॥]{style="color:#99cc00;"}\
[अयोनिजा योनिजाश् च वायुभक्षा हुताशिनः ।]{style="color:#99cc00;"}\
[ईशानं सर्वलोकस्य वज्रिणं समुपासते ॥]{style="color:#99cc00;"}

पराशर, Parvata, सावर्णि, गालव, Ekata, Dvita, Trita the great sages, शङ्ख, Likhita the white-headed sage, दुर्वासस्, दीर्घतमस्, याज्ञवल्क्य,भालुकि, उद्दालक, श्वेतकेतु, master शाट्यायन, हविष्मान्त्, गविष्ठ, king हरिश्चन्द्र, हृद्य, Udara-शाण्डिल्य, कृषीवल the पाराशर्यः, वातस्कन्ध, विशाख, विधाता, काल, Anantadanta, त्वष्टृ the divine architect, Tumburu, those not born from wombs and those born from wombs, those who feed off air and those who feed of fire, together worship the wielder of the Vajra weapon, the supreme lord of all the worlds.

[सहदेवः सुनीथश् च वाल्मीकिश् च महातपाः ।]{style="color:#99cc00;"}\
[समीकः सत्यवांश् चैव प्रचेताः सत्यसंगरः ॥]{style="color:#99cc00;"}\
[मेधातिथिर् वामदेवः पुलस्त्यः पुलहः क्रतुः ।]{style="color:#99cc00;"}\
[मरुत्तश् च मरीचिश् च स्थाणुश् चात्रिर् महातपाः ॥]{style="color:#99cc00;"}\
[कक्षीवान् गौतमस् तार्क्ष्यस् तथा वैश्वानरो मुनिः ।]{style="color:#99cc00;"}\
[मुनिः कालकवृक्षीय आश्राव्यो 'थ हिरण्यदः ।]{style="color:#99cc00;"}\
[संवर्तो देवहव्यश् च विष्वक्सेनश् च वीर्यवान् ।]{style="color:#99cc00;"}\
[कण्वः कात्यायनो राजन् गार्ग्यः कौशिक एव तु ॥]{style="color:#99cc00;"} \[double अनुष्टुभ्]

O king, Sahadeva, सुनीथ, वाल्मीकि of great asceticism, समीक, सत्यवान्, the Praceta-s who are epitomes of truthfulness, मेधातिथि, वामदेव, Pulastya, Pulaha, Kratu, Marutta, मरीचि, स्थाणु and Atri, all of great asceticism, कक्षीवान्, Gautama, तार्क्ष्य, the fire sage, sage कालकवृक्षीय, आश्राव्य, हिरण्यद, संवर्त, Devahavya, विष्वक्सेन of great virility, कण्व, कात्यायन, गार्ग्य and also कौशिक \[are all attending to Indra]

[दिव्या आपस् तथौषध्यः श्रद्धा मेधा सरस्वती ।]{style="color:#99cc00;"}\
[अर्थो धर्मश् च कामश् च विद्युतश् चापि पाण्डव ॥]{style="color:#99cc00;"}\
[जलवाहास् तथा मेघा वायवः स्तनयित्नवः ।]{style="color:#99cc00;"}\
[प्राची दिग् यज्ञवाहाश् च पावकाः सप्तविंशतिः ॥]{style="color:#99cc00;"}\
[अग्नीषोमौ तथेन्द्राग्नी मित्रो 'थ सवितार्यमा ।]{style="color:#99cc00;"}\
[भगो विश्वे च साध्याश् च शुक्रो मन्थी च भारत ।]{style="color:#99cc00;"}\
[सर्वेषां मरुतां मान्या गुरुः शुक्रस् तथैव च ।]{style="color:#99cc00;"}\
[विश्वावसुश् चित्रसेनः सुमनस् तरुणस् तथा ॥]{style="color:#99cc00;"} \[double अनुष्टुभ्]\
[यज्ञाश् च दक्षिणाश् चैव ग्रहाः स्तोभाश् च सर्वशः ।]{style="color:#99cc00;"}\
[यज्ञवाहाश् च ये मन्त्राः सर्वे तत्र समासते ॥]{style="color:#99cc00;"}

O पाण्डव, the celestial waters, plants, the goddesses श्रद्धा, मेधा and सरस्वती, artha, dharma and काम, lightning, monsoon clouds, other clouds, winds, thunders, the eastern direction, 27 fires bearing ritual offerings, the gods Agni, Soma, Agni who is conjoined with Indra, Mitra, सवितृ. Aryaman, Bhaga, the विश्वेदेव-s, and the साध्य-s, and O भारत the शुक्र and the Manthin \[Soma cups], all those respected ones of the Marut-s, Jupiter and Venus, thereafter \[the Gandharva-s] विश्वावसु, Citrasena, Sumana, तरुण, the rituals, the ritual-fee, the soma-cups, the magical vocalization of the सामन् chants, the ritual fires, all the mantra-s attend to Indra therein \[in his assembly].

[तथैव+अप्सरसो राजन् गन्धर्वाश् च मनोरमाः ।]{style="color:#99cc00;"}\
[नृत्य-वादित्र-गीतैश् च हास्यैश् च विविधैर् अपि ।]{style="color:#99cc00;"}\
[रमयन्ति स्म नृपते देवराजं शतक्रतुम् ॥]{style="color:#99cc00;"} [3-footed अनुष्टुभ्]

Then O King, the enchanting Apsaras-es and Gandharva-s, by means of various dances, instrumental and vocal music, and comedies gladden the king of the gods, the performer of a hundred acts.

[स्तुतिभिर् मङ्गलैश् चैव स्तुवन्तः कर्मभिस् तथा ।]{style="color:#99cc00;"}\
[विक्रमैश् च महात्मानं वल-वृत्र-निषूदनम् ॥]{style="color:#99cc00;"}\
[ब्रह्म-राजर्षयः सर्वे सर्वे देवर्षयस् तथा ।]{style="color:#99cc00;"}\
[विमानैर् विविधैर् दिव्यैर् भ्राजमानैर् इवाग्निभिः ॥]{style="color:#99cc00;"}\
[स्रग्विणो भूषिताश् चान्ये यान्ति चायान्ति चापरे ।]{style="color:#99cc00;"}\
[बृहस्पतिश् च शुक्रश् च तस्याम् आययतुः सह ॥]{style="color:#99cc00;"}

In addition to the above, all the ब्राह्मण sages, the sage-kings and divine sages, adorned with garlands, come and go in their diverse, flying machines which blaze forth like celestial fires, praising the great one, the destroyer of Vala and वृत्र, with auspicious praises, by rituals and acts of valor. बृहस्पति and शुक्र had gone there together!

[एते चान्ये च बहवो यतात्मानो यतव्रताः ।]{style="color:#99cc00;"}\
[विमानैश् चन्द्र-संकाशैः सोमवत् प्रियदर्शनाः ।]{style="color:#99cc00;"}\
[ब्रह्मणो वचनाद् राजन् भृगुः सप्तर्षयस् तथा ॥]{style="color:#99cc00;"}\
[एषा सभा मया राजन् दृष्टा पुष्करमालिनी ।]{style="color:#99cc00;"}

O king, many other self-restrained souls, firmly observant of ritual vows, भृगु and the seven sages \[go forth] upon recitation of mantra-s \[to Indra] by means of their moon-shaped flying machines, which are beautiful as moons. O King, this assembly hall of Indra, the पुष्करमालिनी was \[thus] seen by me (i.e. नारद).

Notes

 1.  The motif of an assembly hall of the great Indra-like deity is likely an ancestral Indo-European one. We encounter it in the Germanic world in the form Valhalla of Odin or Bilskirnir of Thor; both are described as celestial halls of enormous size. The meaning of the latter is lightning-streak, which relates it directly to the hall of Indra. Just as Indra's hall endowed with several chambers (वेश्मन्), the Bilskirnir is said to have 540 rooms (a number related to the precession of earth's axis), where he is said to hold court with his wife the goddess Sif (parallel to इन्द्राणी). Just like Indra's realm, Odin vast hall the Valhalla is said to welcome extraordinary individuals (not the common dead) who proceed there due their valor in battle. Indeed memorial stones depicting the ascent of fallen heroes to Valhalla of the Indra-realm, with beckoning Valkyries or Aparas-es are seen respectively in the Nordic and Indic world. The Valhalla, like Indra's hall, is said have divine trees (Glasir and Læraðr are named in particular) and is said to be shining and golden comparable to the above account. In the Greek world too Zeus is said to have an assembly hall in the heavens where all the gods meet with Zeus at the head, much like what is described in the case of Indra's hall (e.g. Illiad 20.5).


 2.  The Indra-सभा is not just seen as a hall but is conceived more like a cuboidal space station which can freely move about in space. It also receives traffic of arriving and departing space-crafts which are said to bear the visiting sages. Thus, it is conception is a truly celestial and "futuristic" one relative to the Greek and Germanic versions which seem more static. This illustrates a less-appreciated point: space travel has long existed in human thought, well before the first operational space-crafts were sent up. Thus, it is the continuity with epic visions such as these, which might be seen as the archetypal inspiration for human flight and space voyages.


 3.  Marut-s figure prominently among the gods mentioned in this account. While the Marut-s had faded in prominence by the time core महाभरत, this account clearly preserves the older Vedic system with the troops of Marut-s as the primary companions of Indra, especially in his battles with the demons. Importantly, this account uses the term गृहमेधिन्, i.e. Marut-s as the recipients of the offerings in the गृहमेध which is part of the larger साकमेध rite, the autumnal ritual in the yearly चातुर्मास्य cycle. Most other Vedic deities are listed with the exception of Yama, वरुण, प्रजापति, विष्णु, and perhaps Rudra. These were perhaps consciously omitted by the composer or the redactor of the text because: 1) the first three of these are described as having their own celestial realms that are described later. 2) विष्णु, and Rudra were probably left out because with their rise in the late Vedic and Epic period their votaries did not want to see them as being secondary to Indra. However, it is uncertain if स्थाणु in the above list stands for Rudra or someone else. Whatever the case, the above narrative is emphatic in placing Indra as the supreme deity using terminology parallel to the Rathantara सामन् of the Vedic ritual: "ईशानं sarvalokasya वज्रिणं समुपासते".


 4.  The goddess इन्द्राणी is identified with श्री and लक्ष्मी following an ancient pattern. Indra is also said to be accompanied by other goddesses such as the personified modesty, fame, and majesty, and श्रद्धा, मेधा and सरस्वती, who are like the Muses in Zeus court. There are also Gandharva-s and Apsara-s who provide music, dance and comedy in the court. This music in the सभा seems to have been an ancient feature for it is paralleled by comparable music in the court of Zeus in the Olympian realm. For example we have Pindar describe it thus:\
*Golden lyre, possession and advocate of Apollo*\
*and the Muses with their violet hair,*\
*the dance step which begins the festivity hear you,*\
*and singers obey your signals*\
*when you quiver and fashion the preludes*\
*of the proems that begin the dance.*\
*You quench even the pointed lightning \[of Zeus]*\
*with its ever-flowing fire. On his scepter*\
*the eagle of Zeus sleeps*. \[Translation by K. A. Morgan]


 5.  A long list of sages and sage-kings, although anachronistic, stretching from the ancient Vedic patriarchs to the authors of the रामायण are all provided as a list. This is to illustrate the point that extraordinary individuals upon death attain the Indra realm, where they engage in his direct worship as they had done formerly in their earthly fire and soma rituals. This is further emphasized by pointing out that the normally antipodal बृहस्पति and शुक्र are seen together worshiping Indra in line with their Vedic persona, where both sages are composers of mantra-s to Indra.


 6.  Several terms specifically refer to the Vedic rituals where Indra reigns supreme. Some of these represent celestial entities and phenomena: The 27 fires represent the 27 नक्षत्र-s of the Vedic ecliptic: the deities corresponding to these receive offerings in the ritual known as the नक्षत्रेष्टि. Then there are the graha-s, which represent both the cups in which soma is offered to the gods and the planets. Specifically the pair of cups used in the great soma ritual are mentioned: शुक्र and Manthin. These two cups commemorate a cunning trick by which the Deva-s overthrew शन्द and Marka, two partisans of the Asura-s and रक्ष-s, who tried to steal the Soma. Also mentioned are the stobha-s, which are magical vocalizations of the सामन् chants (au ho वा, इडा etc).


