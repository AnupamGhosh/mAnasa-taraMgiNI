
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The alien cave of metallic brachiopods](https://manasataramgini.wordpress.com/2015/01/20/the-alien-cave-of-metallic-brachiopods/){rel="bookmark"} {#the-alien-cave-of-metallic-brachiopods .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 20, 2015]{.entry-date}](https://manasataramgini.wordpress.com/2015/01/20/the-alien-cave-of-metallic-brachiopods/ "7:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-jo53QWHz5M0/VL4BUgdtepI/AAAAAAAADOY/rf3RpqkkLlA/s800/metallic.jpg){width="75%"}
```{=latex}
\end{center}
```



