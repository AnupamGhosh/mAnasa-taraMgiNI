
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The sixteen-fold पुरुष](https://manasataramgini.wordpress.com/2024/06/16/the-sixteen-fold-puru%e1%b9%a3a/){rel="bookmark"} {#the-sixteen-fold-परष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 16, 2024]{.entry-date}](https://manasataramgini.wordpress.com/2024/06/16/the-sixteen-fold-puru%e1%b9%a3a/ "9:00 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The पुरुष-सूक्त, or the chant of पुरुष-नारायण, is a late Vedic composition with multiple variants that occupies a focal position in the history of the Indo-Aryan strand of the Indo-European religion. On one side, it features an ancient motif of cosmogony by sacrifice/dismemberment of a cosmic humanoid figure (the पुरुष) that goes back to at least the Post-Yamnayan IE assemblage: it is attested as the myth of Ymir among the (North) Germanics, that of Gayō Maretan among the Iranians, and that of the पुरुष among the Hindus. A version of this myth was evidently transmitted by an eastern branch of the Indo-Iranians to what became China as the myth of Pan-Gu (who is still worshiped by some modern Chinese/Taiwanese). With regard to the presence of this myth in Yamnayan IE, one could point to faint echoes of the myth in the Greek tradition. The well-known version is seen in the form of the cosmogonic dis-"member"-ment of Ouranos by Kronos. A lesser-known reflex of this myth occurs in the form of the dismemberment of Dionysus in the Rhapsodic Theogony, whose cosmogonic significance is laid out by one of the last of the classical Platonists, the younger Olympiodorus. Thus, the पुरुष mytheme could potentially have a much deeper provenance (c.f., the origin myth of the Austronesian Ceram tribe from Indonesia involving the dismemberment of the cosmogonic girl). On another side, the पुरुष-सूक्त is featured as the central incantation in the reenactment of this primordial sacrifice in the Indo-Aryan ritual of the human sacrifice (पुरुषमेध), which had already become otiose and "domesticated" (using the logic that cannibalism is proscribed) by the time of the classical श्रौत ritual.

A further aspect of this सूक्त is the tacit connection of the पुरुष with विष्णु, [a god with deep IE roots](https://manasataramgini.wordpress.com/2020/01/02/the-roots-of-vai%e1%b9%a3%e1%b9%87avam-a-view-from-the-numerology-of-vedic-texts/) and a prominent presence in the post-Yamnayan IE traditions (Germanic: Víðarr, (aspects of) Celtic: Taranis, Iranic: Rasnu and Indian: विष्णु; also note वराह-like aspects of Chinese Pan-Gu). This is expressed by the parallel between the three world-encompassing strides of विष्णु and the same of the पुरुष: [ततो विष्वङ् व्य् अक्रामत् साशनानशने अभि ॥]{style="color: #0000ff"} RV 10.90.4 (Therefrom, he strode forth into everything, into those that eat (living beings) and those that do not). Before this statement, the पुरुष is described with the term त्रिपात्, which is a double entendre implying both three-fourths and three steps. This connection suggests that the सूक्त was evidently composed during the rise of विष्णु along with the spread of the आध्वर्यव tradition. Notably, this went hand-in-hand with विष्णु's identification with the यज्ञ (sacrifice), which is also the case with the पुरुष:\
E.g., [अग्निना वै देवतया विष्णुना यज्ञेन देवा असुरान् प्रव्लीय वज्रेणान्व् अवासृजन् ।]{style="color: #0000ff"} मैत्रायणीय संहिता 1.6.6.5 (Verily, with Agni as the deity, \[and] विष्णु as the sacrifice, the gods pressed against the demons and with the vajra drove them away one after another.)\
C.f., [तं यज्ञं प्रावृषा प्रौक्षन् पुरुषं जातम् अग्रशः । तेन देवा अयजन्त साध्या वसवश् च ये ॥]{style="color: #0000ff"} AV-vulgate 19.6.11 (That sacrifice, they sprinkled with \[purifying] rains, the first-born पुरुष. With him the gods, the साध्य-s, and the Vasu-s performed the sacrifice).

The connection to विष्णु was to have a bright future, making the पुरुष-सूक्त a major litany for विष्णु in the विधान and post-Vedic tradition. A further aspect of this development was related to the legendary promulgator of the पुरुषमेध ritual, नारायण:

[पुरुषो ह नारायणो। अकामयत अतितिष्ठेयं सर्वाणि भूतान्य् अहम् एवेदं सर्वं स्याम् इति । स एतम् पुरुषमेधम् पञ्चरात्रम् यज्ञ-क्रतुम् अपश्यत् । तम् आहरत् तेनायजत तेनेष्ट्वात्य् अतिष्ठत् सर्वाणि भूतानीदं । सर्वम् अभवद् । अतितिष्ठति सर्वाणि भूतानीदं सर्वम् भवति य एवम् विद्वान् पुरुषमेधेन यजते यो वै तद् एवम् वेद ॥]{style="color: #0000ff"} SB 13.6.1.21\
पुरुष नारायण desired thus:"I should surpass all beings! I alone should be everything here (this universe)!" He saw this five-night ritual performance, the पुरुषमेध; he took it and sacrificed with it. Having sacrificed with it, he surpassed all beings. He became everything here. Verily, he who, knowing this, performs the पुरुषमेध, or even who knows this, surpasses all beings and becomes everything here.

[तस्य त्रयोविंशतिर् दीक्षाः द्वादशोपसदः पञ्च सुत्याः । स एष चत्वारिंशद् रात्रः सदीक्षोपसत् कश् चत्वारिंशद् अक्षरा विराट् तद् विराजम् अभिसम्पद्यते "ततो विराड् अजायत विराजो अधि पूरुष" इत्य् एषा वै सा विराड् एतस्या एवैतद् विराजो यज्ञम् पुरुषं जनयति ॥]{style="color: #0000ff"} SB 13.6.1.22\
Its (the पुरुषमेध's) consecrations are twenty-three; it has twelve Upasad-s and five \[Soma]-pressings. Thus, this being a ritual \[of a total of] forty nights, including दीक्स्हा-s and Upasads, maps to a विराज्, for the विराज् consists of forty syllables. \[Hence, it is recited that]: "Thence the विराज् was born and from the विराज्, the पूरुस्ह." This, then, is that विराज्, and from out of that विराज् \[the ritualist] generates the पुरुष, the sacrifice.

This पुरुष नारायण, who surpassed and became everything in the universe, came to be identified as a name of विष्णु. This process began suggestively in the late Yajurveda tradition with an addendum known as the Uttara-नारायण to the पुरुष-नारायण chant, i.e., the core पुरुष-सूक्त (along with the corresponding Uttara-नारायण श्रौत ritual that follows the पुरुषमेध). Here, the पुरुष is described as having ह्री and श्री as his wives. The identification of पुरुष नारायण with विष्णु was further developed within the early वैष्णव tradition of पञ्चरात्र as expounded in the नारायणीय section of the महाभारत. Notably, the name of this tradition, पञ्चरात्र, directly stems from the description of the पुरुषमेध as the rite with five nights of Soma pressings. The पञ्चरात्र tradition affiliates itself with the [शुक्ल-Yajurveda](https://manasataramgini.wordpress.com/2010/07/06/tritonal-recitations-of-vaishnava-s/) (SKY), where the पञ्चरात्र पुरुषमेध likely originated. The argument for the origin of the पञ्चरात्र पुरुषमेध in the SKY tradition goes thus: the full account of the पञ्चरात्र rite performed by पुरुष नारायण is only found in the शतपथ ब्राह्मण (SB). Correspondingly, the पुरुष-सूक्त is embedded right within the संहिता-s of the SYV (माध्यंदिन and काण्व). In contrast, the पुरुष-सूक्त is not found in all collections of the कृष्ण-yajurveda (KYV). In the तैत्तिरीय tradition, it is only found in the late collection of that tradition, i.e., the आरण्यक (TA). In the कठ tradition, it is found in a supplementary ऋक्-संहिता used by the कठ-s to supply those mantra-s not found within their core संहिता (or lost ब्राह्मण texts). Thus, as the popularity of the पञ्चरात्र पुरुषमेध grew, it appears to have spread laterally across the आध्वर्यव tradition. Hence, the case may be made that of the two strands of the early वैष्णव tradition emerging from the आध्वर्यव matrix, पञ्चरात्र emerged among the SYVs and वैखानस among the KYVs (who also adopted the पुरुष-सूक्त for their ritual).

The name नारायण is etymologized in later tradition (e.g., महाभारत, हरिवंश and Manu-स्मृति) on the basis of his emergence from the waters ("nara-s") and the generation of हिरण्यगर्भ in these primordial waters. This immediately indicates that नारायण as the पुरुष was probably originally conceived within the framework of a प्राजापत्य myth seen in the SB, where प्रजापति is identified with the पुरुष emerging from the waters:

[आपो ह वा इदम् अग्रे सलिलम् एवास । ता अकामयन्त कथं नु प्रजायेमहीति । ता अश्राम्यंस् तास् तपो'तप्यन्त तासु तपस् तप्यमानासु हिरण्मयम् आण्डं सम्बभूव+अजातो ह तर्हि संवत्सर आस । तद् इदं हिरण्मयम् आण्डं यावत् संवत्सरस्य वेला तावत् पर्यप्लवत ॥]{style="color: #0000ff"}SB 11.1.6.1

Verily, in the beginning of this (universe) were the waters, only an ocean. They \[the waters] desired, "Now, how can we be reproduced?" They strove hard and performed tapas. By that tapas, they, becoming heated, produced a golden egg. The year, indeed, was then not in existence; this golden egg floated about for as long as the duration of a year.

[ततः संवत्सरे पुरुषः समभवत् । स प्रजापतिस् तस्माद् उ संवत्सर एव स्त्री वा गौर् वा वडबा वा विजायते । संवत्सरे हि प्रजापतिर् अजायत स इदं हिरण्मयम् आण्डं व्यरुजन्न् आह तर्हि का चन प्रतिष्ठास । तद् एनम् इदम् एव हिरण्मयम् आण्डं यावत् संवत्सरस्य वेलासीत् तावद् बिभ्रत् पर्यप्लवत ॥]{style="color: #0000ff"}SB 11.1.6.2

In a year's time, the पुरुष came into being therefrom. He was प्रजापति. Hence, a woman, a cow, or a mare gives birth within a year. प्रजापति, indeed, was born in a year and broke open this golden egg. Then, indeed, there was no resting place: only this golden egg, bearing him, floated about for the duration of a year.

Thus, we have: Waters  $\to$  Golden Egg  $\to$  पुरुष प्रजापति. This would make the पुरुष to be नारायण with respect to the waters (Nara-s) supporting the original प्राजापत्य origin of this myth.

Finally, there is a less-appreciated connection between the पुरुष-सूक्त and the यज्ञ encoded in its structure itself. This is acknowledged in texts of both the SYV and KYV traditions but is fully developed only in the SYV, again supporting the expansion of the पुरुष-centric theology within the SYV. While versions of the पुरुष-सूक्त are found across all Vedic संहिता-s, they are notable for their divergence from each other despite being a late composition. Despite this divergence, the versions in the RV, AV-vulgate, AV-P, SYV संहिता-s and कठ-ऋक्-संहिता-s have 16 verses (15 अनुष्टुभ्-s+ a final त्रिष्टुभ् in the RV version). The version in the सामवेद संहिता-s is shortened, keeping with the way the ऋक्-s are chosen for गाना composition. The version in the KYV तैत्त्रीय आरण्यक has an insert after verse 15 containing two त्रिष्टुभ्-s (the second being hypermetrical), extending it to an 18 verse form. These two त्रिष्टुभ्-s appear to be a clear secondary duplication and transposition from the Uttara-नारायण that follows. Thus, its conserved core remains a 16-verse composition as in the other non-SV संहिता-s. This indicates that the number 16 was a critical feature of the पुरुष-सूक्त. Hence, it is notable that the ब्राह्मण tradition describes the पुरुष as 16-fold. The तैत्तिरीय ब्राह्मण states in the section pertaining to the राजसूय ritual:\
[षोडशभिर् गृह्णाति । षोडश-कलो वै पुरुषः । यावान् एव पुरुषः । तस्मिन् वीर्यं दधाति । षोडशभिर् जुहोति षोडशभिर् गृह्णाति । द्वात्रि̐शत् संपद्यन्ते । द्वात्रि̐शदक्षरा+अनुष्टुक् । वाग् अनुष्टुप् सर्वाणि च्छन्दा̐सि । वाचा+एवैन̐ सर्वेभिश् छन्दोभिर् अभिषिञ्चति ॥]{style="color: #0000ff"} in TB 1.7.5.5 (also see parallel text in the ब्राह्मण from the कठ संहिता 21.8.2, which is, however, presented in the context of the sin-effacing वैश्वकर्म oblations).

With sixteen, he takes hold. The पुरुष indeed has sixteen parts. However much the पुरुष is, in that he places the essence. With sixteen, he sacrifices \[and] with sixteen, he takes hold. \[Hence,] thirty-two are put together. The अनुष्टुभ् meter is of thirty-two syllables. Speech (वाक्) is अनुष्टुभ्, \[encompassing] all meters. With speech, verily, he consecrates \[him] with all meters.

Thus, the 16-fold nature of the पुरुष directly related to both the sacrifice and the 32-syllabled अनुष्टुभ् (16  $\times$  2), which respectively also map onto the number verses in the पुरुष-सूक्त and the primary meter in which it is composed.

In the same section describing the origin of the पुरुष प्रजापति from the waters and the Golden Egg (see above), the SB elaborates on this 16-fold connection, providing a mapping of the homologies of the anatomy of the पुरुष and the ritual:

[अथाध्यात्मम् । पञ्चेमे पुरुषे प्राणा ऋते चक्षुर्भ्यां । त एव पञ्च प्रयाजा चक्षुषी आज्यभागौ ॥]{style="color: #0000ff"}\
Now, as to one's own self (body): In the पुरुष, there are these five प्राण-s (physiological processes) excluding the eyes. These are the five प्रयाज-s (fore-offerings); the two आज्यभाग-s (ghee-offerings) are the eyes.

[अयम् एवावाङ् प्राणः स्विष्टकृत् । स यत् तम् अभ्य् अर्ध इवेतराभ्य आहुतिभ्यो जुहोति तस्माद् एतस्मात् प्राणात् सर्वे प्राणा बीभत्सन्ते ।अथ यत् स्विष्टकृते सर्वेषां हविषाम् अवद्यति तस्माद् यत् किं चेमान् प्राणान् आपद्यत एतम् एव तत् सर्वं समवैति ॥]{style="color: #0000ff"}\
The स्विष्टकृत् is the excretory (downward) process (प्राण). Because he offers that (स्विष्टकृत्), as though, separately from the other oblations, therefore all the \[other] प्राण-s stay away (literally: are scared by) from that प्राण. Now, because for the स्विष्टकृत् he cuts portions from all the offering, therefore everything that passes through these processes (प्राण) meets in that process (प्राण) (An account of excretory physiology and its homology with the expiatory स्विष्टकृत् oblation).

[त्रीणि शिश्नानि त एव त्रयो ।अनुयाजाः । स यो ।अयं वर्षिष्ठो ।अनुयाजस् तद् इदं वर्षिष्ठम् इव शिश्नं तं वा अनवान् अन्यजेद् इत्य् आहुस् तथो हास्यैतद् अमृध्रम् भवतीति ॥]{style="color: #0000ff"}\
The three अनुयाज-s (after-offerings) are the three genitalia. That which is the main after-offering is like the central genital. "He should offer it without taking an in-breath," they (the rival teachers of the वाजसनेयिन्-s) say, "because it thus becomes unfaltering for him."

[स वै सकृद् अवान्यात् एकं ह्य् एतस्य पर्वाथ यद् अपर्वकं स्यात् प्रतृणं वैव तिष्ठेल् लम्बेत वा तस्माद् एतद् उच्च तिष्टति पद्यते च तस्मात् सकृद् अवान्यात् ॥]{style="color: #0000ff"}\
However, he should indeed take a breath once (before making the अनुयाज oblations) because it (the central genital) has one articulation; if it were unarticulated, it would either just stand erect or would just dangle down. Because it both stands erect and falls (flaccid), he may therefore inhale once.

[द्वौ बाहू द्वा ऊरू त एव चत्वारः पत्नीसंयाजाः प्रतिष्ठायम् एव । प्राण इडा यत् तां नाग्नौ जुहोति यत् साप्रदग्धेव तस्माद् अयम् अनवतृणः प्राणः ॥]{style="color: #0000ff"}\
The two arms and the two thighs are the four पत्नीसंयाज-s: verily the support. \[The undivided/central] प्राण is the इडा offering. As that (इडा) is not offered in the fire but remains unburnt, therefore this प्राणः is undivided (Foreshadowing the role of the spinal cord in the later तान्त्रिक traditions).

[अस्थ्य् एव याज्यानुवाक्याः । मांसं हविस् । तन् मितं छन्दो यद् याज्यानुवाक्यास् तस्माद् उ समावन्त्य् एवास्थीनि मेद्यतश् च कृश्यतश् च भवन्त्य् अथ यद् भूय इव च हविर् गृह्णाति । कनीय इव च तस्माद् उ मांसान्य् एव मेद्यतो मेद्यन्ति मांसानि कृश्यतः कृश्यन्ति । तेनैतेन यज्ञेन यां कामयते देवतां तां यजति यस्यै हविर् भवति ॥]{style="color: #0000ff"}\
The bones are the याज्य incantations. The flesh is the offering material. As the याज्य incantations are measured by meters, therefore the bones remain constant \[be they] of a fat and a thin person. However, even as he takes more \[or less], offering material, the flesh of a fat person fattens, and the flesh of a thin person thins. Thus, he performs this sacrifice to any deity he desires \[and] for whom there is a sacrificial offering.

[ता वा एता अनपोद्धार्या आहुतयो भवन्ति । स यद् धैतासाम् अपोद्धरेद् यथैकम् अङ्गं शृणीयात् प्राणं वा निर्हण्याद् एवं । तद् अन्यान्येव हवींष्य् उप चाह्रियन्ते ।अप च ह्रियन्ते ॥]{style="color: #0000ff"}\
These are the offerings not to be omitted. If one were to omit any of them, it would be as if he may cut off some organ or may remove some metabolic process. Then, the other oblations, indeed, are either added to or omitted.

[ता वा एताः षोडशाहुतयो भवन्ति षोडश-कलो वै पुरुषः । पुरुषो यज्ञस् तस्मात् षोडशाहुतयो भवन्ति ॥]{style="color: #0000ff"} SB 11.1.6.29-36\
These, indeed, are the sixteen offerings, for the पुरुष consists of sixteen parts. The sacrifice is the पुरुष; therefore, there are sixteen offerings.

  - ------------------------------------------------------------------------------------
  Ritual offering         Number                   Bodily Mapping

  - ---------------------- ------------------------ ------------------------------------
  प्रयाज-s                 5                        Core metabolic processes

  आज्यभाग-s                1 ([×]{.math .inline}\   Eyes (vision)
                          
 2.                        

  स्विष्टकृत्                 1                        Excretory process

  अनुयाज-s                 3                        Genitalia

  पत्नीसंयाज-s              4                        4 limbs

  इडा                     1                        Undivided/central process (Spinal\
                                                   cord)

  याज्यानुवाक्य-s+हविष्       1                        Bones and muscle

  - ------------------------------------------------------------------------------------

Hence, in conclusion, we posit that the 16-fold structure of the पुरुष-सूक्त was intended to reflect homology between the anatomy of the पुरुष and the structure of the ritual right from the time of its composition, or in the least, this connection was established early in its deployment. Even though the above SB section indicates that it emerged in a प्राजापत्य context, the establishment of an intimate homology between the ritual and the पुरुष helped cement his identity with विष्णु, given the latter's identification with the यज्ञ in the developing श्रौत tradition. This also probably provided the foundation for other related homologies, such as that of the sacrificial boar (यज्ञ-वराह; also identified with विष्णु) and the ritual, which originated in the कठ tradition.

