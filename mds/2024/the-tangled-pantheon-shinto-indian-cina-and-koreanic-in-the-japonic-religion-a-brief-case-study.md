
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The tangled pantheon: Shinto, Indian, चीन and Koreanic in the Japonic religion: a brief case study](https://manasataramgini.wordpress.com/2024/09/16/the-tangled-pantheon-shinto-indian-cina-and-koreanic-in-the-japonic-religion-a-brief-case-study/){rel="bookmark"} {#the-tangled-pantheon-shinto-indian-चन-and-koreanic-in-the-japonic-religion-a-brief-case-study .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 16, 2024]{.entry-date}](https://manasataramgini.wordpress.com/2024/09/16/the-tangled-pantheon-shinto-indian-cina-and-koreanic-in-the-japonic-religion-a-brief-case-study/ "3:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While heavily influenced by the Sinitic transmissions, Japan retained its own unique traditions throughout the historical period. While the curtain of history lifts up late in Japan compared to the Middle Kingdom, the Japanese followed the latter's tradition in terms of good record keeping. Thus, we have rather detailed material to reconstruct the history of the syncretism between the endogenous Shinto tradition and the Bauddha tradition, at least at some of the sacred sites. However, there are also downsides when trying to apprehend the evolution of East Asian religions. The general challenges of understanding the evolution of the Japanese religion are enormous for an outsider, even if he might have the vantage point of comparative analyses that might not be available to the common emic practitioner (c.f., a typical modern day श्रौत ritualist). When a student pampered by the luxuries of studying the evolution of Indo-European religion approaches the Japanese religion, he is left flattened by the enormity of the questions. While one might sight some type of relationship -- homology or analogy -- to Koreanic or Sinitic religions, one is often left without a clue of its ancestral state. The contributions from the Jomon, the Northeast Asian Ainu-like peoples, and the Yayoi all seem too tangled like a Gordian knot. Further, beneath the Bauddha patina, dimly lit tropes peer forth -- familiar yet too divergent to place in an evolutionary scheme. Thus, a man trying to understand the evolutionary history of the Japanese religion might feel rather bewildered. However, on some occasions, with some study, we can unravel the various strands of the puzzling tangle.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/tenkeisei2.jpg){width="75%"}
```{=latex}
\end{center}
```



*Tenkeisei smashing and devouring epidemic demons (Heian period, 1100s of CE).*

As an example, we could consider the case of the Japanese "Raumya-s." The Japanese tradition holds that the human body has 84000 pores/hair follicles, in each of which dwells a deity known as a zokushin. Among other things, these gods are said to control each "moment" of the 360 days of the old Japanese year. This "moment" would be approximately 6 minutes. This number, 84000, also corresponds to the deities known as the shikigami, who comprise the retinue of the Japanese deity Tenkeisei. The name Ten-kei-sei itself can be traced back to the Chinese तिआन् Xíng xईन्ग् thus:\
तिआन् (天) = "heaven'' or "god''\
Xíng (刑) = "punishment''\
xईन्ग् (星) = "star''\
"The Star of Heavenly Punishment"\
This indicates that Tenkeisei originated from an old Chinese astral deity who receded in the homeland but developed extensively in Japan. A part of this was the rise to prominence of him and his retinue as epidemic deities with rituals specifically directed at his capacity to consume disease demons "ekiki/ekki''. Further, given Tenkeisei's origin as a malevolent astral deity on the mainland, he was also viewed as a deity governing human fate, dispensing fortune or misfortune based on individuals' actions. One of the epidemic demons that he is described as devouring or subduing is the buffalo-headed Gozu Tennō (to be discussed further below), which probably acted as a conduit for his absorption into the Bauddha तान्त्रिक tradition. Here, he was unsurprisingly (given the associations of Gozu Tennō with King Yama and the buffalo/bull) associated with the deity यमान्तक (Japanese: Daiitoku Myōō), who, in turn, in the Bauddha tradition, is seen as a manifestation or ectype of मञ्जुश्री. Further, in some Japanese traditions, he came to be identified with Gozu Tennō himself. One example of this is the divinatory text Hoki Naiden, which the Japanese believe was transmitted from India via the Tang empire.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/gozu_tenno.jpg){width="75%"}
```{=latex}
\end{center}
```



*Enthroned Gozu Tennō (Yoshino मण्डल, Edo period).*

Gozu Tennō has more mysterious roots and appears recognizably in the Japanese religion around the 700s of CE. In terms of iconography, he might have a bull/buffalo head (along with an anthropomorphic head) or multiple (originally 5; sometimes 11) heads. He is associated with a distinct mythology that places him in conflict with the शाक्यमुनि Buddha -- he is even supposed to have killed the Buddha in an encounter by sending a disease to afflict the latter. Like Tenkeisei, he is also said to have a retinue of of 84654 deities associated with control of time and pestilence. Faure provides the below outline of his mythology with 6 elements \[Footnote 1]:\
1. The birth and characteristic appearance of Gozu Tennō with multiple faces or a buffalo face of red hue and bearing white fangs.\
2. He goes south to obtain a wife.\
3. He is refused lodging by the wealthy Bauddha householder Kotan when he seeks a dwelling.\
4. He is offered shelter by Somin Shōrai the poor brother of Kotan.\
5. He marries Harisaijo, the daughter of the dragon king, and through her fathers 8 children known as the princes. Some traditions also mention them having a daughter, the snake-goddess, known as Jadokkeshin (a deity with parallels to the Hindu goddess मानसा).\
6. While returning, Gozu Tennō destroys Kotan's house or kills him after giving Somin Shōrai's daughter an amulet of miscanthus reeds or willow wood to protect her and Somin Shōrai's descendants.

Finally, Gozu Tennō is also identified in some traditions with the Shinto stormy deity Susanoo. This suggests a complex history for the deity. The spread of the Gozu Tennō cult from the West and its appearance around the 700s suggests a possible Koreanic transmission to Japan. This is supported by the similarities between the Korean deity Ch'ŏyong and Gozu Tennō; however, there is not much survival of the cult of this deity on the Korean side. This was followed by the syncretism of Gozu Tennō with the old Shinto Haya Susanoo, the younger brother of Amaterasu. Shortly thereafter, or around the same time, there was a potential antagonistic encounter with the spreading Saugata religion. This is indicated by the failure of Kotan's bauddha rituals performed by 1000 श्रमण-s sent by the Buddha himself to protect him from Gozu Tennō's attack. Finally, this was followed by reconciliation with the bauddha-s and further syncretism via a merger with Tenkeisei, the Japanese evolute of the Chinese astral deity तिआन् Xíng xईन्ग्. Following reconciliation with the Saugata-mata, he is called a Buddha himself (a manifestation of the cosmic Buddha महावैरोचन), and in his 11-faced iconography, one of the faces is a pacific Buddha face. In some narrations, his syncretism with Tenkeisei is presented as happening in India: here, Tenkeisei incarnated as Gozu Tennō, the king of Magadha. In this cycle, a bird informs Gozu Tennō of the daughter of the नागराज (dragon king) -- a motif going back to the श्रुति, where, in the सामवेद-ब्राह्मण-s, गरुड takes the वासिष्ठ, गौरिवीति शाक्त्य, to his lover in the रक्षस्' realm. In other accounts, he is described as being born on the slopes of Meru. As part of the reconciliation with the Bauddha-mata, Gozu Tennō instructs his entourage of 84000-odd deities not to harm Somin Shōrai's descendants unless they discard the Buddha-s and the gods.

Among the textual sources for the worship of this deity, the Gozu tennō kōshiki transmitted at a subcomplex of the Tsushima shrine is of great interest. It offers the following curious myth: Gozu Tennō first manifested as a king named Mutō Tennō in a kingdom of northern India along with his eight sons and 84654 attendant deities. They became the protectors of the Jetavana-विहार in India. Here, Kotan and Somin Shōrai are presented as Indians, and the event happened in a former kalpa. After 75,580,000 years, Gozu Tennō reincarnated in China as Emperor Shennong around 2800 BCE (in the current kalpa) and discovered tea (chai). Then, at the time of Kōrei Tennō (290-215 BCE), he crossed the sea to reach Japan, where he stayed in the West in the Taichō province for 780 years. Then, in the first year of the reign of Kinmei Tennō (539 CE), he moved to the Tsushima Island. The text also offers a myth where the deity Gozu Tennō appeared as an old man before the great goddess Amaterasu standing on a reed. This reed is said to have become the islands of Japan. His amulets representing those given to Somin Shōrai are found in both Japan and Korea, and Somin Shōrai himself is worshipped by some Korean clans. He had major cultic centers at Hiromine, Gion, Tsushima and Ise.

[![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg){.size-full .wp-image-14679 .aligncenter attachment-id="14679" comments-opened="0" image-caption="<p>ucm_li_1982_002_18, 8/4/05, 2:57 PM,  8C, 7783×4716 (0+3078), 100%, Default Settin,  1/12 s, R72.3, G60.0, B68.7</p>
" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"ucm_li_1982_002_18, 8\/4\/05, 2:57 PM,  8C, 7783x4716 (0+3078), 100%, Default Settin,  1\/12 s, R72.3, G60.0, B68.7\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="yamAntaka" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg" orig-size="564,1100" permalink="https://manasataramgini.wordpress.com/2024/09/16/the-tangled-pantheon-shinto-indian-cina-and-koreanic-in-the-japonic-religion-a-brief-case-study/yamantaka/" height="1100" sizes="(max-width: 564px) 100vw, 564px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg 564w, https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg 77w, https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg 154w" width="564"}](https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/yamantaka.jpg)

यमान्तक masterpiece (late 1200s of CE around the time of the Mongol invasions)

Returning to Tenkeisei, we find that his spread throughout Japan (as indicated by amulets) parallels that of Gozu Tennō with whom he came to be identified. However, as noted above, he is shown subjugating or devouring Gozu Tennō. This suggests that just as the Bauddha-s had an antagonistic interaction with Gozu Tennō, the Chinese-origin Tenkeisei cult also likely had a clash with that of Gozu Tennō as their domains closely overlapped. However, this was resolved in two ways -- either by making Tenkeisei overcome Gozu Tennō, now painted as a demon of pestilence or by syncretizing the two by virtue of their natural similarities. We find that the cult of Tenkeisei was associated with several secret rituals transmitted via texts paralleling the Bauddha tantra-s that were transmitted to Japan. Faure points to two of these Tenkeisei gyōhō and the Tenkeisei gyōhō shidai as teaching secret आवेश rituals. This is particularly interesting because of the Bauddha identification of Tenkeisei with यमान्तक. Parallel to this, Gozu Tennō is identified with King Yama in some Japanese traditions, according to Faure. While यमान्तक is seen as an emanation of the bodhisattva मञ्जुश्री, he is essentially a Bauddha absorption of the Hindu Rudra, even retaining his epithet as is. Not surprisingly, his iconography parallels that of Rudra with a bull mount. Interestingly, the Japanese mythic history holds that before her invasion of Korea, the source of epidemics, the Japanese Empress जिन्गू saw the manifestation of a divine bull/ox, which was believed to be a sign of the god यमान्तक.

आवेश rituals were an old feature of the शैव tradition and were retained in the Bhairavasrotas as a notable practice. A शैव आवेश text was translated into Chinese by Amoghavajra in the 700s of CE, titled Suji liyan Moxishouluo tian shuo (jialouluo) aweishe fa: The (गरुड) आवेश rite explained by the god महेश्वर which swiftly establishes its efficacy. This text was transmitted to Japan by कूकै in 806 CE. It is likely a शैव गरुड-srotas text and is presented as a teaching of Rudra to विष्णु. It presents a rite of using young kids as spirit mediums via आवेश.

Both Tenkeisei and Gozu Tennō possess several features indicative of a "Rudrian" character: 1. The lord of pestilence (Gozu Tennō); 2. The punishing star Tenkeisei; c.f., the astral myth of Rudra (आर्द्र, Iranic Tishtrya) punishing प्रजापति; 3. The bull/buffalo association. 4. The emanation of 84000 odd attendant deities (c.f., गण-s). Further, their association with the roma-कूप-s (hair-pits) reminds one of the Raumya-s -- गण-s emanated by Rudra/वीरभद्र from the roma-कूप-s (e.g., शन्तिपर्वन् 284 of महाभारत-vulgate). The Raumya-s are invoked in a secret ritual relating to the mantra known as the वीरभद्रास्त्र. Thus, given that a शैव आवेश text had reached China and Japan by the 700s-800s of CE, it is likely that such material served as a nexus for interactions between the endogenous rites related to deities with a Rudrian character like Tenkeisei and Gozu Tennō and their absorption into यमान्तक. We posit that this facilitated the incorporation of आवेश in the rites of Tenkeisei. The association of Rudrian deities with epidemics in Japan is also indicated by the Matarajin rite, which is centered on the मातृ-s transmitted via the Bauddha-s (derogatorily called राक्षसी-s in early ताथागत tradition) to Japan.

Another key connection indicative of an Indian transmission is the connection between the hair follicles and the moments of time. This is a concept going back to the ब्राह्मण-s of the श्रुति. For instance, we have the शतपथ ब्राह्मण of the शुक्लयजुर्वेद state a teaching of वार्कलि thus:\
[दश च वै सहस्राण्य् अष्टौ च शतानि संवत्सरस्य मुहूर्ताः। यावन्तो मुहूर्तास् तावन्ति पञ्चदश-कृत्वः क्षिप्राणि। यावन्ति क्षिप्राणि तावन्ति पञ्चदश-कृत्व एतर्हीणि। यावन्त्य् एतर्हीणि तावन्ति पञ्चदश-कृत्व इदानीनि। यावन्ति+इदानीनि तावन्तः पञ्चदश-कृत्वः प्राणाः। यावन्तः प्राणास् तावन्तो ।अक्तनाः। यावन्तो ।अक्तनास् तावन्तो निमेषाः। यावन्तो निमेषास् तावन्तो लोमगर्ताः। यावन्तो लोमगर्तास् तावन्ति स्वेदायनानि। यावन्ति स्वेदायनानि तावन्त एते स्तोका वर्षन्ति॥]{style="color: #0000ff"} SB 12.3.2.5॥

10800 (360  $\times$  30) are the muhurta-s (=48 minutes) in a year. As many muhurta-s exist, so there are 15 times that number of क्षिप्र-s (quick intervals= 3.2 minutes). As many quick breaths, so there are 15 times that number of etarhi-s (=12.8s). As many etarhi-s, so there are 15 times that number of इदानि-s (=0.8533...s). As many इदानि-s, so there are 15 times that number of प्राण-s (in-breaths). As many प्राण-s, so are the number of aktana-s (out-breaths). As many aktana-s, so are the number of निम्ēष-s (0.056888...s). As many निम्ēष-s, so are the number of hair pores. As many hair pores, so are the number of sweat glands. As many sweat glands, so are the number of \[drops] that rain down in \[an year].

A parallel passage, albeit with inflated numbers, is found in the Gopatha ब्राह्मण of the Atharvaveda:\
[दश च ह वै सहस्राण्य् अष्टौ च शतानि संवत्सरस्य मुहूर्ता इति । एतावन्त एव पुरुषस्य पेशशमरा इति । अत्र तत् समम् । यावन्तो मुहूर्ताः पञ्चदश कृत्वस् तावन्तः प्राणाः । यावन्तः प्राणाः पञ्चदश कृत्वस् तावन्तो ।अपानाः । यावन्तो ।अपानाः पञ्चदश कृत्वस् तावन्तो व्यानाः । यावन्तो व्यानाः पञ्चदश कृत्वस् तावन्तः समानाः । यावन्तः समानाः पञ्चदश कृत्वस् तावन्त उदानाः । यावन्त उदानाः पञ्चदश कृत्वस् तावन्त्य् एतादीनि । यावन्त्य् एतादीनि तावन्त्य् एतर्हीणि । यावन्त्य् एतर्हीणि तावन्ति स्वेदायनानि । यावन्ति स्वेदायनानि तावन्ति क्षिप्रायणानि । यावन्ति क्षिप्रायणानि तावन्तो रोमकूपाः। यावन्तो रोमकूपाः पञ्चदश कृत्वस् तावन्तो वर्षतो धारास् । तद् एतत् क्रोशशतिकं परिमाणम् ॥GB 1.5.5]{style="color: #0000ff"}

10800 are the muhurta-s in a year. These are equivalent to the \[number of] interstitial spaces in the flesh (पेश-शमराः) in a person. In this, they are equivalent. As many मुहूर्तस् as there are, fifteen times that number are the प्राण-s (breaths). As many प्राण-s, fifteen times that number are the अपान-s (excretory processes). As many अपान-s, fifteen times that number are the व्यान-s (circulatory movements). As many व्यान-s, fifteen times that number are the समान-s (assimilative functions). As many assimilative functions, fifteen times that number are the उदान-s (nervous processes). As many उदान-s, fifteen times that number are एतादि-s. As many एतादि-s, so many are the etarhi-s. As many etarhi-s, so many are the sweat glands. As many sweat glands, so many are the quick movements (क्षिप्रायण-s). As many quick movements, so many are the hair follicles. As many hair follicles, fifteen times that number are the drops that fall as rain. This is the measure equivalent to a hundred क्रोश-s.

These numbers, 546,750,000 in the SB and 123,018,750,000 in the GB are much higher than the 84000 seen in the Japanese tradition. However, the number 84000 seems to be favored by the bauddha-s and appears in several contexts as a count for different things in their texts (importantly, the 84000 doors to निर्वाण already seen in the vulgar पाऌई). Thus, a merger of the substratum H tradition regarding roma-कूप-s and the fierce Raumya गण-s born of Rudra's hair follicles with distinct deities of Sinitic and Koreanic origins gave rise to the Japanese tradition regarding Tenkeisei-Gozu Tennō. While there is a natural inclination to associate तिआन् Xíng xईन्ग् with the planet Saturn, his murky past raises the possibility that he was an early transmission of an Indo-Iranian Rudrian astral deity (c.f., आर्द्र/Tishtrya) from a steppe Indo-Iranian group to the north. While we have transmission of the Rudrian iconography for आर्द्र in the later Hindu astral tradition to China, we currently lack the data to test the conjecture of an earlier influence.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/09/rudra_ardra_goma_rodan_yo.jpg){width="75%"}
```{=latex}
\end{center}
```



Archon of आर्द्र from the Homa text Goma Rodan yō

  - -----------------------------------------------------------------------

Footnote 1: Gods of Medieval Japan: Rage and Ravage, Faure

