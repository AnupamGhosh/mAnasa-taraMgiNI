
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An attractor generating the projection of a 3D torus on a plane](https://manasataramgini.wordpress.com/2024/12/22/an-attractor-generating-the-projection-of-a-3d-torus-on-a-plane/){rel="bookmark"} {#an-attractor-generating-the-projection-of-a-3d-torus-on-a-plane .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 22, 2024]{.entry-date}](https://manasataramgini.wordpress.com/2024/12/22/an-attractor-generating-the-projection-of-a-3d-torus-on-a-plane/ "9:21 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The attractor in consideration was probably rediscovered by multiple explorers of chaotic maps in the early days of the computational exploration of such objects. We first discovered it in our 14th or 15th year when access to computers was just beginning to become a little easier in our regions. We are recording it here as part of an attempt to collate the story of our exploration. The generator of this attractor is a simple 4-parameter trigonometric map defined thus:

 $$x_{n+1}=d \sin(a x_n) - \sin(b y_n)$$ 

 $$y_{n+1}=c \cos(a x_n) + \cos(b y_n)$$ 

All attractors in this exploration were initiated with  $x_0=0.45, y_0=0.56$ . The parameters  $c, d$  have similar effects on the shape of the attractor; hence, we hold  $d=-1$  and vary  $c$ . At lower absolute values of  $a, b$ , approximately  $-3.5 \le a \le 3.5$  and  $-2.85 \le b \le 2.85$ , we tend to get a variety of strange attractors, which we do not consider further in this note. Thus, we fix  $a=3.5$  and  $b=2.85$  for the below runs. Interestingly, while  $0 \le c \le 1$ , we find that the attractor takes the form of the projection of a 3D torus on the XY plane. The surface of the torus shows some "painting" of structure determined by  $a, b$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/12/sven01.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad Attractors for  $0 \le c \le 1$ 
```{=latex}
\end{center}
```


A torus is a surface defined by the parametric equations (Figure 2):\
 $x= (R+r \cos(v)) \cos(u)$ ,

 $$y= (R+r \cos(v)) \sin(u),$$ 

 $z= r\sin(v)$ ,\
where  $R$  is the major radius (the distance from the center of the tube to the center of the torus) and  $r$  is the minor radius (the radius of the tube).

[](https://manasataramgini.wordpress.com/2024/12/22/an-attractor-generating-the-projection-of-a-3d-torus-on-a-plane/torus1/){border="0" itemprop="url"}

![Torus1](https://i0.wp.com/manasataramgini.wordpress.com/wp-content/uploads/2024/12/torus1.jpeg&ssl=1 "Torus1"){width="75%"}

[](https://manasataramgini.wordpress.com/2024/12/22/an-attractor-generating-the-projection-of-a-3d-torus-on-a-plane/torus2/){border="0" itemprop="url"}

![torus2](https://i0.wp.com/manasataramgini.wordpress.com/wp-content/uploads/2024/12/torus2.jpeg&ssl=1 "torus2"){width="75%"}

Figure 2. Tori with  $R=r$  and  $R=2r$ 

With regard to the above map, we observe that it is the projection of the torus with  $R=r$  on the XY plane. Further, we observe that the projection represents the rotation of the "horizontal" plane of the torus with the X-axis as the axis of rotation. One can establish that the angle of rotation is  $\theta = \arcsin(c)$ ; thus, at  $c=0, \theta=0$  the torus is projected "side-on" and at  $c=1, \theta=\tfrac{\pi}{2}$  it is projected face-on. More generally, whenever the  $|d| = |c|$ , the attractor always takes the form of a torus projected face-on with  $R=|c|=|d|, r=1$  (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/12/torus3.jpeg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad Face-on projection of torus.
```{=latex}
\end{center}
```


When  $d=-1$  and  $c> 1$ , the attractor takes the form of the planar projection of a torus with  $R= |c|, r=1$ . This torus is oriented such that its axis of rotation is the Y-axis (passing through the body of the torus rather than the aperture as above). It is rotated such that the projection of the outer edge of the tube touches the projection of the inner edge of the tube directly opposite to it (Figure 4). As a non-mathematician, the open question to us is how exactly one converts this 2D map into an actual 3D map whose attractor sits on a torus, thereby explaining the cause of its form.

![sven02](https://manasataramgini.wordpress.com/wp-content/uploads/2024/12/sven02.png){width="75%"}Figure 4. Attractors for  $c \ge 1$ 

