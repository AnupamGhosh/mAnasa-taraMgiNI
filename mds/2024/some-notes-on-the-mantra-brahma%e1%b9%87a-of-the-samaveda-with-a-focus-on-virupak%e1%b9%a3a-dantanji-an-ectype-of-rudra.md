
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on the Mantra-ब्राह्मण of the सामवेद with a focus on विरूपाक्ष दन्ताञ्जि, an ectype of Rudra](https://manasataramgini.wordpress.com/2024/11/25/some-notes-on-the-mantra-brahma%e1%b9%87a-of-the-samaveda-with-a-focus-on-virupak%e1%b9%a3a-dantanji-an-ectype-of-rudra/){rel="bookmark"} {#some-notes-on-the-mantra-बरहमण-of-the-समवद-with-a-focus-on-वरपकष-दनतञज-an-ectype-of-rudra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 25, 2024]{.entry-date}](https://manasataramgini.wordpress.com/2024/11/25/some-notes-on-the-mantra-brahma%e1%b9%87a-of-the-samaveda-with-a-focus-on-virupak%e1%b9%a3a-dantanji-an-ectype-of-rudra/ "5:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Of the सामवेदिच् schools, the Kauthuma-s follow the श्रौतसूत्र of लाट्यायन and the गृह्यसूत्र of Gobhila. The राणायनीय-s follow the श्रौतसूत्र of द्राह्यायण and the गृह्यसूत्र of खादिर (also attributed to द्राह्यायण). Their respective श्रौतसूत्र-s are dependent on closely related collections of गान-s composed on the ऋक्-s from a संहिता that is usually termed the Kauthuma-संहिता and the specifications of the पञ्चविंश-ब्राह्मण. Their respective गृह्यसूत्र-s are dependent on mantra-s drawn from the छान्दोग्य-ब्राह्मण. The Kauthuma-s and राणायनीय-s are clearly closely related schools as their गान collections and संहिता-s are very similar or the same. This is also reflected in their गृह्यसूत्र-s being very close. The जैमिनीय school is more divergent and is marked by a distinct eponymous संहिता, गान-s and a sprawling ब्राह्मण. These inform their श्रौतसूत्र, the जैमिनीय-श्रौतसूत्र. Their गृह्यसूत्र, the Jaimini-गृह्यसूत्र is more divergent than Gobhila and खादिर. However, of the two, it is closer to Gobhila, suggesting that these two preserve more of the ancestral state of the गृह्यसूत्र of the सामवेदिन्-s. Notably, the Jaimini-गृह्यसूत्र provides several of the mantra-s found in the छान्दोग्य-ब्राह्मण in full within the body of the text. Hence, it is conceivable that the जैमिनीय-s did not have cognate of the छान्दोग्य-ब्राह्मण (although they might have possessed an equivalent मन्त्रपाट्ःअ, rumored to exist among जैमिनीय-s of the द्राविड country). Further, the Jaimini-गृह्यसूत्र shows close parallels to the बौधायन-गृह्यसूत्र-s and गृह्य-परिशिष्ठ-s.

The छान्दोग्य-ब्राह्मण or छान्दोग्य-उपनिषद्-ब्राह्मण is text in 10 प्रपाठक-s of which 1-2 form the Mantra-ब्राह्मण, and the remaining 3-10 form the छान्दोग्य उपनिषद्. The former comes with two medieval glosses attributed to the scholiasts सायण, the Vijayanagaran prime minister, and भट्ट गुणविष्णु. The below discussion will focus on the Mantra-ब्राह्मण, which has homologous elements in the Jaimini-गृह्यसूत्र. The main dilation in this note would be on the deity विरूपाक्ष दन्ताञ्जि who is worshiped in the Agnihotra or daily गृह्य fire offering of the सामवेदिन्-s and related Rudrian deities. Briefly, the contents of the Mantra-ब्राह्मण go thus:\
1.1. The preliminary fire ritual of marriage to various deities. Here त्वाष्टृ is described as three-horned (त्रिशृङ्ग).\
1.2. The marriage rite. This खण्ड contains the incantations of the famous ritual of seven steps. In several rituals, the ritualist impersonates the famed steps of विष्णु. The seven steps taken in the Indo-Aryan marriage rite can be seen as one such. The incantations accompanying it go thus in the सामवेदिच् tradition:\
[एकम् इषे विष्णुस् त्वा नयतु । द्वे ऊर्जे विष्णुस् त्वा नयतु । त्रीणि व्रताय विष्णुस् त्वा नयतु । चत्वारि मायोभवाय विष्णुस् त्वा नयतु । पञ्च पशुभ्यो विष्णुस् त्वा नयतु । षड् रायस् पोषाय विष्णुस् त्वा नयतु । सप्त सप्तभ्यो होत्राभ्यो विष्णुस् त्वा नयतु ।]{style="color: #0000ff"}\
The first step for food, may विष्णु lead you; the second step for strength, may विष्णु lead you; the third step for the observance, may विष्णु lead you; the fourth step for enjoyment, may विष्णु lead you; the fifth step for cattle, may विष्णु lead you; the sixth step for the increase of wealth, may विष्णु lead you; the seventh step for the seven-fold होतृ-hood, may विष्णु lead you;

[सखा सप्तपदी भव सख्यं ते गमेयँ सख्यात् ते मा योषँ सख्यान् मे मा योष्ठाः ॥]{style="color: #0000ff"}\
With the seventh step, be my friend. May I attain companionship with you. May I not be separated from your companionship. May you not break your friendship with me.

1.3. Ritual done after the core marriage rite.\
1.4 Ritual done by the bride on the 4th day after marriage and the rite for progeny.\
1.5 Perinatal rites such as सीमन्तोन्नयन, मेधाजननम् and नामकरण.\
1.6 Tonsure. Upanayana.\
1.7 Sacrifice to various deities for protection.\
1.8 Sacrifice for the protection of cattle. Invocation of Bhava and Indra.

2.1 Sacrifice to the king of the snakes (Sarpa-राज) and related pacificatory offerings (c.f., [installation of the snake idol in the later सामवेदिच् Gautama-गृह्य-परिशिष्ट ritual attributed to भागुरि](https://manasataramgini.wordpress.com/2022/12/27/origins-of-the-serpent-cult-and-bhaguris-snake-installation-from-the-samaveda-tradition/)).\
2.2 Animal sacrifice, protection of cattle and protection at night.\
2.3 श्राद्ध and ancestor spirit rituals.\
2.4 Worship performed at Agnihotra or the गृह्य-ritual fire rituals and various incantations for काम्य homa-s.\
2.5 Ritual to counter snakes and parasites; a ritual for glory; a ritual to the sun-boat.\
2.6 Ritual for increased prosperity, countering poisons and imprecatory incantations\
2.7 Incantations against parasitic worms and flies. In these incantations attributed to Jamadagni, Atri, Gotama and भरद्वाज, we find the first knowledge of the agents of myiasis:\
[कृमिम् इन्द्रस्य बाहुभ्याम् अवाञ्चं पातयामसि । हताः कृमयः साशातिकाः स-नीलमक्षिकाः ॥]{style="color: #0000ff"}\
We hurl down the worm with the arms of Indra. Slain are the worms with the cattle bots (आशातिक-s) and the blue bottle flies (नीलमक्षिक-s).

आशातिक-s are also mentioned in the तैत्तिरीय आरण्यक:\
[आशातिकाः क्रिमय इव । ततः पूयन्ते वासवैः । अपैतं मृत्युं जयति ।]{style="color: #0000ff"}\
The आशातिक-s are like worms, i.e., the myiasis-causing larvae. Remarkably, the word persists in मराठी as आसाडी (c.f. पाऌइ आसाटिक), allowing us to understand what was meant by it.

2.8 Ritual for consecration of the cow.

**The विरुपाक्ष and काम्य incantations in Mantra-ब्राह्मण 2.4**\
In 2.4 of the Mantra-ब्राह्मण we find a peculiar incantation known as the वैरूपाक्ष. The initial part of it (ॐ तपश् ca तेजश् ca ... prapadye ||) is known as the Prapada. The sutra-s of Gobhila instruct the ritualist thus regarding their deployment:\
[वैरूपाक्षः पुरस्ताद् धोमानाम् ।]{style="color: #0000ff"}\
The वैरूपाक्ष \[is recited] before all homa-s.\
[काम्येषु च प्रपदः ।]{style="color: #0000ff"}\
And at the काम्य rites the Prapada.\
[तपश् च तेजश् चे 'ति जपित्वा, प्राणायामम् आयम्या ।अर्थमना वैरूपाक्षम् आरभ्यो ।अच्छ्वसेत् ।]{style="color: #0000ff"}\
ॐ तपश् ca तेजश् ca etc. he \[mentally] recites while holding his breath, and should, fixing his thoughts on the object (of his homa), exhale, when beginning the \[core] वैरूपाक्ष incantation.

The Jaimini-गृह्यसूत्र teaches the use of the same incantation in the context of the Agnihotra rite:\
[प्रणीताः पुनर् आहारम् आज्यस्य सकृद् यजुषा द्विस् तूष्णीं उत्तरतो ।अग्नेः प्रणीताः प्रणीय दर्भैः प्रच्छाद्य दक्षिणतो अग्नेः प्रस्तरं निधाय प्रस्तरस्य+उपरिष्टात् पवित्रे निधाय विरूपाक्षं जपति ।]{style="color: #0000ff"}\
Having taken up the प्रणीता (rectangular ritual trough with handle) again, he offers the ghee once with the यजुष् and twice silently at the north of the fire altar. Having taken forth the प्रणीता, having covered it with darbha grass, having placed the prastara (ritual grass bundle) to the south of the fire altar, and having placed the pavitra (grass ring) on the prastara, he recites the विरूपाक्ष.

This ritual injunction corresponds to the actions at the beginning of the Agnihotra rite and maps to the corresponding recitation at the beginning of a गृह्य rite.

Prapada:\
[ॐ तपश् च तेजश् च श्रद्धा च ह्रीश् च सत्यं चा क्रोधश् च त्यागश् च धृतिश् च धर्मश् च सत्वं च वाक् च मनश् चात्मा च ब्रह्म च तानि प्रपद्ये तनि माम् अवन्तु । भूर् भुवः स्वर् ॐ । महान्तम् आत्मानं प्रपद्ये ॥]{style="color: #0000ff"}\
The heat (of asceticism), splendor, reverence, modesty, truth, wrath, forsaking, constancy, observance, goodness, speech, mind, consciousness, and mantra-power, in these I take my refuge, may these help me. The three व्याहृति-s, ॐ. In the great soul, I take my refuge.

The scholiast गुणविष्णु states that this nigada is dedicated to Agni in the form of Rudra with परमेष्ठिन् as its ऋषि. He states that one mediates on the self as "रुद्राग्निरूप" evidently while performing प्राणायाम with it. सायण clarifies that exhaling with "भूर् भुवः svar ॐ" one meditates on the self (evidently with the final part of the incantation that follows) and starts reciting the वैरूपाक्ष proper incantation. Similarly, the observers of the half-monthly rite go to the forest and do japa of the Prapada (तथा ca अर्धमास-व्रती पौर्णमास्याम् ity upakramya+अरण्ये प्रपदं japet ।). Regarding this rite, Gobhila states the following in his सूत्रस्: 1. He does japa of the Prapada incantation sitting on a mat of Darbha grass. 2. The tips of the grass leaves are turned eastwards if he desires brahmavarcas. 3. They are placed northwards if he desires sons and cattle. 4. They are placed in both directions if he desires both.

वैरूपाक्ष proper:\
[विरूपाक्षो 'सि दन्ताञ्जिस् तस्य ते शय्या पर्णे गृहा अन्तरिक्षे विमितँ हिरण्मयम् । तद् देवानाँ हृदयान्य् अयस्मये कुम्भे 'न्तः संनिहितानि । तानि बलभृच् च बलसाच् च रक्षतो 'प्रमणी अनिमिषतः सत्यम् । यत् ते द्वादश पुत्रास् ते त्वा सँवत्सरे-सँवत्सरे कामप्रेण यज्ञेन याजयित्वा पुनर् ब्रह्मचर्यम् उपयन्ति । त्वं देवेषु ब्राह्मणो 'स्य् अहं मनुष्येषु ब्राह्मणो वै ब्राह्मणम् उपधावत्य् उप त्वा धावामि । जपन्तं मा मा प्रतिजापीर् जुह्वन्तं मा मा प्रतिहौषीः कुर्वन्तं मा मा प्रति-कार्षीः । त्वां प्रपद्ये । त्वया प्रसूत इदं कर्म करिष्यामि । तन् मे राध्यतां तन् मे समृध्यतां तन् म उपपद्यताम् । समुद्रो मा विश्वव्यचा ब्रह्मानुजानातु तुथो मा विश्ववेदा ब्रह्मणः पुत्रो 'नु-जानातु । श्वात्रो मा प्रचेता मैत्रावरुणो 'नु-जानातु । तस्मै विरूपाक्षाय दन्ताञ्जये समुद्राय विश्वव्यचसे तुथाय विश्व-वेदसे श्वात्राय प्रचेतसे सहस्राक्षाय ब्रह्मणः पुत्राय नमः ॥]{style="color: #0000ff"}\
You are the odd-eyed one with gleaming tusks (विरूपाक्ष दन्ताञ्जि). Your bed is made of leaves, and your house in the atmosphere is made of gold. There and in a metal vessel are placed the hearts of the gods. बलभृत् and बलासद् carefully watch these without ever blinking. Those who are your twelve sons sacrifice to you, year after year, a wish-fulfilling sacrifice, and then go back to their study. You are the ब्राह्मण among the gods, while I am among the men. Verily, the ब्राह्मण resorts to the ब्राह्मण-s, and I take refuge in you. While I'm uttering incantations, please do not utter counter-incantations; when I am sacrificing (or invoking deities), please do not sacrifice against me; when I am acting, please do not act against me. I take refuge in you. Impelled by you, I will perform this ritual. Let it be successful for me; let it be beneficial for me; let it be fulfilled for me. May the Brahman power, which is all-encompassing like the ocean, allow it for me. May Tutha, the all-knowing one born of Brahman power, allow it for me. श्वात्र, the inspirer born of Mitra and वरुण, allow it for me. To that विरूपाक्ष दन्ताञ्जि, the all-encompassing ocean, the all-knowing Tutha, श्वात्र, the inspirer, the thousand-eyed one, and son of Brahman power, obeisance.

The scholiast सायण states that it is a nigada-रूप-यजुष् to Rudra-रूपाग्निः. Both he and गुणविष्णु cite the following अनुष्टुभ् as a description of this deity (comparable to भगवद्गीता 13.14):\
[सर्वतः पाणि-पादश् च सर्वतो ।अक्षि-शिरो-मुखः ।]{style="color: #0000ff"}\
[विश्वरूपो महान् अग्निः प्रणीतः सर्व-कर्मसु ॥]{style="color: #0000ff"}\
With hands and feet everywhere, with eyes, heads and faces everywhere, the all-formed great Agni is laid down in all rituals.

Regarding the epithet दन्ताञ्जि we have the following comments by the scholiasts:\
गुणविष्णु: [दन्ताञ्जिः दन्तान् अनक्ति व्यञ्जयति+इति औणादिक इः [उणादि ५७१] -- अञ्जू व्यक्तिश्-लक्षण-कान्ति-गतिषु -- व्यक्तदन्त इत्य् अर्थः ।]{style="color: #0000ff"}\
सायण: [तथा दन्ताञ्जिः । अञ्जू व्यक्तिस्-लक्षण-कान्ति-गतिषु । दन्तैर् अनक्ति प्रकाशयति+इति दन्ताञ्जिः । दीप्रदन्त इत्य् अर्थः ।]{style="color: #0000ff"}

Both provide similar explanations; गुणविष्णु cites the उणादि-sutra-s to explain the formation of the word via the इः suffix. They state that अञ्जू indicates appearance, a characteristic, beauty or gait. Thus, the word दन्ताञ्जि is used in the sense of having clearly visible or gleaming teeth.

[अक्तं रिहाणा व्यन्तु वयः ॥]{style="color: #0000ff"}\
May the birds having licked the anointed \[ritual grass] go their ways.

This incantation is used to anoint the ritual darbha with ghee.

[यः पशूनाम् अधिपति रुद्रस् तन्तिचरो वृषा ।]{style="color: #0000ff"}\
[पशून् अस्माकम् मा हिंसीर् एतद् अस्तु हुतम् तव स्वाहा ॥]{style="color: #0000ff"}\
He who is the lord of the cattle, Rudra, the bull led by the cord.\
Harm not our cattle; may this oblation be yours. स्वाहा.

This incantation is used in the darbha-mat fire rite to Rudra. Two oblations, respectively of ghee and a स्थालीपाक are made with it.

Rudra-putra:\
[सहस्रबाहुर् गौपत्यः स पशून् अभिरक्षतु ।]{style="color: #0000ff"}\
[मयि पुष्टिं पुष्टिपतिर् दधातु मयि प्रजां प्रजापतिः । स्वाहा ॥]{style="color: #0000ff"}\
May the thousand-armed son of the lord of the cows protect the cattle.\
May the lord of nourishment grant me nourishment. May the lord of the folks grant me progeny. स्वाहा.

This incantation is used to make offerings in the काम्य homa to the thousand-handed son of Rudra. It is performed in front of the water trough in the cow pen with oblations of rice and barley. It represents an interesting proto-form of Skanda. सायण states this represents the Marut-s or the Vayu-s, connecting the son of Rudra mentioned here with the Marut-s in the संहिता-s.

[कौतोमतँ सँवननँ सुभागं करणं मम ।]{style="color: #0000ff"}\
[नाकुली नाम ते माताथाहं पुरुषामयः ।]{style="color: #0000ff"}\
[यन् नौ कामस्य विच्छिन्नं तन् नौ संधेह्य् ओषधे ॥]{style="color: #0000ff"}\
Kautomata, completely winning and fortunate, is my action.\
नाकुली (the mongoose goddess) is the name of your mother; now, I am endowed with men.\
Whatever is broken apart in our desires, bring those together in concordance, O plant.

This incantation is for the offering of consecrated fruits of great fig trees to gain association with a person (according to सायण for gaining assistance or friendship). Gobhila states that he should keep an even number of fruits with himself, which counts to one more than the number he gives to a person with whom he seeks to associate for help or friendship. The word Kautomata in this incantation is enigmatic. गुणविष्णु states that it is the name of this incantation as its ऋषि is Kutomant. सायण, however, etymologizes it thus: "[कुतः कुत्रापि नास्ति अमतम् असम्प्रतिपन्नं यस्य तत् कुतोमतम् उदुम्बरादि । तस्य संबन्धि फलं कौतोमतम् ।]{style="color: #0000ff"}" He sees it that which is not accepted as existing anywhere (kuto .amatam), i.e., the flower of the udumbara (the fig emerges without a flower as understood by the lay people); hence, he takes kutomata to be the great figs like udumbara and kautomata as its fruit.

The following 5 ऋक्-s are used in multiple vrata-s. The first is a peculiar one for obtaining land: After fasting for a fortnight or only eating rice gruel, in which he can see his reflection, once a day, he goes to a perennial pool on the full moon night. Plunging into it up to his navel, he sacrifices with each of the five verses, casting fried grains with his mouth into the water with a स्वाहा. In the next rite, with the first verse, he sacrifices facing the sun within the sight of a rich person from whom he desires a donation. With the second verse, he sacrifices when there is a halo around the sun to increase his stock of horses and elephants. With the third verse, he sacrifices when there is a halo around the moon to increase his cattle. With the fourth verse, he worships facing the sun to acquire wealth and return safely. With the fifth verse, he worships facing the sun to return safely to his home.

[वृक्ष इव पक्वस् तिष्ठसि सर्वान् कामान् भुवस्पते ।]{style="color: #0000ff"}\
[यस् त्वैनँ वेद तस्मै मे भोगान् धुक्ष्वाक्षतान् बृहन् ॥]{style="color: #0000ff"}\
Like a ripe tree, you stand; \[fulfill] all my desires, o lord of the world. For those who thus know you, to me give everlasting pleasures, o great \[one].

[ऋतँ सत्ये प्रतिष्ठितं भूतं भविष्यता सह ।]{style="color: #0000ff"}\
[आकाश उपनिरज्जतु मह्यम् अन्नम् अथो श्रियम् ॥]{style="color: #0000ff"}\
The natural law is established in truth; the past comes with the future. May the space be bound; for me, food and fortune.

[अभिभागो 'सि सर्वस्मिँस् तद् उ सर्वं त्वयि श्रितम् ।]{style="color: #0000ff"}\
[तेन सर्वेण सर्वो मा विवासन विवासय ॥]{style="color: #0000ff"}\
You are a portion of everything; hence, all rests in you. With this entirety, one who is all, banishing, do not banish me.\
(Note parallel in the epithet sarva to the Iranic deity Zurvan)

[कोश इव पूर्णो वसुना त्वं प्रीतो ददसे धनम् ।]{style="color: #0000ff"}\
[अदृष्टो दृष्टम् आभर सर्वान् कामान् प्रयच्छ मे ॥]{style="color: #0000ff"}\
Like a treasury full of wealth, you being pleased, give riches. The invisible one brings the visible; fulfill for me all desires.

[आकाशस्यैष आकाशो यद् एतद् भाति मण्डलम् ।]{style="color: #0000ff"}\
[एवं त्वा veda yo वेदेशानेशान् prayaccha me ||]{style="color: #0000ff"}\
This is the space of space, which is the sphere shining there. Who know you thus, o lord (ईशान) of the Veda-s (or knowledge), provide for me lordships \[over various things].

The significance of विरूपाक्ष दन्ताञ्जि\
The name विरुपाक्ष is a rare but specific name for Rudra found in other Vedic texts. While rare, the name has early IE connections: comparable ocular features are seen in the Germanic cognate of Rudra, Óðinn (he is one-eyed, thus, odd- or different-eyed mapping on to विरुपाक्ष: c.f., सामवेदिच् name of Rudra, एकाक्ष). Probably the oldest use of विरुपाक्ष is in the incantation against rival debaters in the Atharvaveda पैप्पलाद-संहिता 20.55.10:\
[शर्वेण नीलशिखण्डेन भवेन मरुतां पित्रा ।]{style="color: #0000ff"}\
[विरूपाक्षेण बभ्रूणा वाचं वदिष्यतो हतम् ॥]{style="color: #0000ff"}\
By शर्व, नीलशिखण्ड, Bhava, the father of the Marut-s,\
By विरूपाक्ष (odd-eyed), Babhru (brownish), may the speech of those who speak \[opposing us] be smitten.

The second occurrence is in the तैत्तिरीय आरण्यक 10.41 in the famous मृत्युलाङ्गल mantra, used in the daily worship of Rudra, which is presented in a semi-grammatical form:\
[ऋतँ सत्यं परं ब्रह्म पुरुषं कृष्ण-पिङ्गलम् ।]{style="color: #0000ff"}\
[ऊर्ध्वरेतं विरूपाक्षं विश्वरूपाय वै नमो नमः ॥]{style="color: #0000ff"}\
\[I salute] The natural law, truth, the foremost, mantra-power, the पुरुष, the dark yellow (blackish yellow) one, with an upward seminal flow, the odd-eyed one. Verily, repeated obeisance to him of all forms.

It is notable that in both cases, the god is described as being of similar color (brownish) or dark yellow. While the सामवेदिच् attestation of विरूपाक्ष does not mention his color, it presents multiple epithets that are reminiscent of epithets of Rudra in the Yajurveda:\
1. His golden house is reminiscent of his golden associations, e.g., TA 10.40:\
[नमो हिरण्यबाहवे हिरण्यवर्णाय हिरण्यरूपाय हिरण्यपतये ।अम्बिकापतय उमापतये पशुपतये नमो नमः ॥]{style="color: #0000ff"}\
2. His leafy bed is reminiscent of the term पर्णशद्य (heap of fallen leaves) used in the शतरुद्रीय.\
3. The hearts of the gods kept in the metal vessel in his dwelling is reminiscent of the phrase [नमो वः किरिकेभ्यो देवानाँ हृदयेभ्यः ।]{style="color: #0000ff"} applied to the Rudra-s in the शतरुद्रीय.\
These observations strongly support the scholiasts' identification of विरूपाक्ष in the सामवेदिच् texts with Rudra.

While the second epithet of the deity दन्ताञ्जि is, to our knowledge, not found anywhere outside these सामवेदिच् texts, we believe that this epithet has major significance for the evolution of गणपति, a popular deity in the later Hindu tradition. Within the late सामवेदिच् tradition, their परिशिष्ट text (Chandoga-परिशिष्ट or कर्मप्रदीप) attributed to Gobhila (it is also called Gobhila-स्मृति) furnishes one of the early examples of गणेश worship, wherein he is invoked together with the लोकमाटृ-s (गौरी, पद्मा, शची, मेधा, सावित्री, विजया, जया, देवसेना, स्वधा, स्वाहा, धृति, पुष्टि and तुष्टि) at the beginning of all rites:\
[गौरी पद्मा शची मेधा सावित्री विजया जया । देवसेना स्वधा स्वाहा मातरो लोकमातरः ॥]{style="color: #0000ff"}\
[धृतिः पुष्टिस् तथा तुष्टिर् आत्मदेवतया सह । गणेशेणाधिका होता वृद्धौ पूज्याचतुर्दश ।]{style="color: #0000ff"}\
[कर्मादिषु च सर्वेषु मातरः सगणाधिपाः । पूजनीया: प्रयत्नेन पूजिताः पूजयन्ति ताः ॥]{style="color: #0000ff"}

This establishes that there was continuity within the सामवेदिच् tradition of गणेश worship.

There are several lines of connections, of differing strengths between विरूपाक्ष दन्ताञ्जि and the later day गणेश:\
1. His most definitive feature is his clearly visible or gleaming teeth -- this implies that the Rudra विरूपाक्ष दन्ताञ्जि was seen as a tusked or fanged figure. While a trunk or an elephant face is not mentioned explicitly, the presence of this dental peculiarity likely served as a locus for further development towards गणेश.\
2. Like गणेश, विरूपाक्ष दन्ताञ्जि is worshipped at the beginning of rites and for their success. He is associated with the divine ब्राह्मण: a parallel is seen in the god ब्रह्ंअणस्पति, the divine ब्राह्मण, whose incantation has the earliest use of the epithet गणपति.\
3. Like गणेश, विरूपाक्ष दन्ताञ्जि is invoked to not obstruct the rite that is being performed.\
4. There is an allusion to the sons of Rudra, who enact a wish-fulfilling sacrifice: this provides a launching pad for गणेश as the son of Rudra.\
5. The काम्य rituals that follow this section invoke the son of Rudra, suggesting that this concept was readily available in the tradition for "crystallization."\
6. Finally, the peculiar element of his epithet अञ्जि is uniquely associated with the Marut-s, the sons of Rudra, in the ऋग्वेद. For example, we have:\
[ये पृषतीभिर् ऋष्टिभिः साकं वाशीभिर् अञ्जिभिः । अजायन्त स्वभानवः ॥]{style="color: #0000ff"} RV 1.37.2\
Here अञ्जि is typically translated as "unguent", a word of old IE pedigree with respect to that etymology. However, it is mentioned here in the context of the weapons of the Marut-s: ऋष्टि (spear) and वाशी (battle pickaxe). Another example of the same is:\
[ये अञ्जिषु ये वाशीषु स्वभानवः स्रक्षु रुक्मेषु खादिषु ।]{style="color: #0000ff"} RV 5.53.4ab\
Here again, अञ्जि is mentioned with the battle pickaxe, armor, gauntlets and garlands. Thus, विरूपाक्ष दन्ताञ्जि bears a tusk/fang as an अञ्जि just as the Marut-s bear their अञ्जि. The अञ्जि of the Marut-s is also notably said to be brilliant white in color. So, it could mean the lighting weapon/form or an ornamentation of the Marut-s. This makes it a potential analogy for the brilliant tooth of विरूपाक्ष दन्ताञ्जि and indicates a further link between the conception of the sons of Rudra in the old tradition and this deity.

Taken together, these pieces of evidence suggest the early precursors of गणेश coalescing around this deity of the सामवेदिच् tradition.

  - -----------------------------------------------------------------------

Also see: [https://x.com/blog_supplement/status/1401062734301642754](https://x.com/blog_supplement/status/1401062734301642754){rel="nofollow"}

