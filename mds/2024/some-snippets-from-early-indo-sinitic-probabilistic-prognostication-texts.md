
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some snippets from early Indo-Sinitic probabilistic prognostication texts](https://manasataramgini.wordpress.com/2024/01/10/some-snippets-from-early-indo-sinitic-probabilistic-prognostication-texts/){rel="bookmark"} {#some-snippets-from-early-indo-sinitic-probabilistic-prognostication-texts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 10, 2024]{.entry-date}](https://manasataramgini.wordpress.com/2024/01/10/some-snippets-from-early-indo-sinitic-probabilistic-prognostication-texts/ "7:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The below is the barest sketch of a topic that has already been treated in multiple book-length works by learned scholars of the Orient. However, none of these works were entirely familiar with the Hindu side of the tradition leaving lacunae in their analyses. We have long studied this material and wanted to write a brief note on it. This may be seen as a sampler or a precursor of such.

At least since the Neolithic, humans can be seen as having two complementary (even if implicit) views of life: The first is probabilistic. In this view, there are certain probabilities with which future events will happen to a person. The person tries to estimate those probabilities and tailor his actions accordingly. Built into this idea is the subjective updating of probabilities (called by some as Bayesian inference after the eponymous theorem). Thus, one starts off with a certain prior probability distribution, which might be very crude, but as one encounters events in life, one acquires an intuitive sense of likelihood of those events. One then computes a distribution of the likelihood across the probability interval. Then by multiplying these two distributions and normalizing by their sum, one gets a new probability distribution, the posterior, which updates our probability of the event occurring. While this formal method might not appear straightforward to the majority of people, they carry out such updating intuitively. The only problem in real life is accurately computing the likelihood distribution for doing the update. The second view of life is deterministic. In this, one sees oneself as inexorably drawn towards a pre-determined fate. Whatever one does is already determined, and all actions and the sense of free action are merely an illusion.

With some introspection, one may arrive at the conclusion that these two views are ultimately complementary. Because one cannot infer one's fate though it is predetermined, one acts as though it is probabilistic and the estimation of probabilities is merely a technical attempt to determine the course it would take. Given this, not surprisingly, over the ages, humans have sought prognostication methods. Interestingly, those methods show a comparable bifurcation along the lines of probabilistic and deterministic approaches. This is well illustrated in the Hindu prognostication. Strong determinism is exhibited in the texts of astrological prognostication (partly influenced by Greek tradition) --- uranomancy (prediction by celestial phenomena) and hemerology (calendrical prognosis, with मुहूर्त-s, शुभदिन-s, etc.). Here, it is held that the future may be predicted by reading celestial patterns at birth and the subsequent, entirely predictable, movement of celestial bodies. In contrast, a probabilistic approach is encoded in the cleromantic traditions (prognostication by dice or lottery) that were once common in old India.

Evidence for the early spread of cleromancy across Asia from India is suggested by parts of the enigmatic Bower manuscript. This manuscript has a colorful history. It was purchased in 1890 CE by the English soldier Bower, who was chasing down a Mohammedan murderer all the way from Kashmir to the ancient oasis town of Kucha in central Asia. Its seller was a local treasure-hunter, and his agents had found the manuscript at a supposed bauddha ruin. The decipherment of the Bower manuscript by the German scholar Buhler and its eventual editing and translation by another German scholar, Hoernle, finally brought it to the public between 1893-1897 CE. While modern "mainstream" white indologists date the texts to the 500s of the Common Era, the text itself is written a late ब्राह्मी script that was used in India during the early Gupta age. This would suggest an earlier date for the manuscript. The language of the manuscript is a grammatically degraded register of Sanskrit, forms of which are often termed "Buddhist Hybrid Sanskrit." Parts 4 and 5 of this manuscript contain two distinct cleromantic texts. The first of these, part 4, opens with a remarkable set of mantra-s:

[नमो नन्दि-रुद्रेश्वराय । नमो आचार्येभ्यः । नम ईश्वराय । नमो मणिभद्राय । नमः सर्वयक्षेभ्यः । नमः सर्व-देवेभ्यः । शिवाय नमः । षष्ठ्यै नमः । प्रजापतये नमः । रुद्राय नमः । नमो वैश्रवणय । नमो मरुद्भ्यो नमः । प्रासकाः पतन्तु इमा अस्य+अर्थस्य कारणा हिलि हिलि कुम्भकारी-मातङ्ग-युक्ता पतन्तु ।]{style="color: #0000ff"}\
[यत् सत्यं सर्व-सिद्धानां यत् सत्यं सर्व-वादिनां तेन सत्येन सत्य-समयेन नष्टं विनष्टम् क्षेमाक्षेमं लाभालभं जयाजयं शिवानुदर्शय स्वाहा ।]{style="color: #0000ff"}\
[सत्य-नारायणे चैव देवते ऋषिषु चैव सत्यम् मन्त्रं वृतिः सत्यं समक्षाः पतन्तु स्वाहा ।]{style="color: #0000ff"}\
[सत्यं चैव तु द्रष्टव्यं ...]{style="color: #0000ff"}\
[नि--------[मणि?]-मन्त्रौषधीनां च निमित्त-बलम् अनन्तरम् मृषतायां देवतायां विष्णु नविकायां चण्टयाण्ट ।]{style="color: #0000ff"}\
[नमः पुरुष-सिंहस्य प्रसन्नस् ते जनार्दनः निहताः शत्रवः सर्वे...]{style="color: #0000ff"}

Obeisance to नन्दिरुद्रेश्वर. Obeisance to the आचार्यस्. Obeisance to ईश्वर. Obeisance to माणिभद्र. Obeisance to all the यक्षस्. Obeisance to all the devas. To शिव obeisance. To षष्ठी obeisance. To प्रजापति obeisance. To Rudra obeisance! Obeisance to वैश्रवण. Obeisance to the Marut-s. Obeisance.\
May these dice fall for this \[prognostic] objective! hili! hili! May they fall set to work by कुम्भकारी, the मातन्ग woman!\
That truth which is of all the Siddha-s, that truth which is of the professors of all paths; by that truth and by that true consensus, what is lost and preserved, welfare and lack thereof, gain and loss, victory and defeat, may शिव make that apparent स्वाहा!\
In the true नारायन, and in the deity, and also in the ऋषि-s lies the truth of the incantation, the choosing (i.e., the choice of prognosis by dice fall). Truth! May the dice fall together! स्वाहा! And may the truth be seen!\
...the strength of \[amulets?], incantations, medical herbs, and augury are uninterrupted by error. The deity विष्णु \[unclear]. Obeisance of the the Man-lion; be you pleased. जनार्दन has slain all enemies...

In the above text, we have not reproduced Hoernle's transcription but have attempted to restore, where possible, the original mantra-s. A reader can compare it with his transcription, which is close, though in parts, displays its characteristic tumbled-down Sanskrit. Hoernle also provided a translation with his transcription, which was well done given the state of the text. We provide our own, which we believe sometimes hews closer to the intention of the text. Notwithstanding specific objections one may have to this translation or restoration, the mantra-s are rather striking for the following reasons:

1. The opening salutation is to the deity नन्दिरुद्रेश्वर. This name is rare in the शैव literature and thus serves as a rather specific marker. The name is used for the tetracephalic Rudra worshiped in Kashmir at the नन्दीश-क्षेत्र associated with the Sodara spring. Indeed, the Nandi-क्षेत्र-माहात्य्म specifically mentions Nandirudra as one of the four faces of the Rudra associated with the भूतगणेश्वर Nandin:

[शर्व-नन्दि-महाकाल-देवी-वदन-मण्डितम् ।]{style="color: #0000ff"}\
[भूतेश्वरं भूतपतिं डृष्ट्वा मर्त्यो विमुच्यते ॥]{style="color: #0000ff"}\
[पश्चिमे वदने वीर मम वत्स्यसि यत्सहे ।]{style="color: #0000ff"}\
[भूतेश्वरः सर्वभूतः सुतीर्थान्तर्गतो विभुः ॥]{style="color: #0000ff"}\
[श्रीकण्ठः पूर्ववदने महाकालो 'थ दक्षिणे ।]{style="color: #0000ff"}\
[पश्चिमे नन्दिरुद्रस् तु देवी सौम्ये प्रतिष्ठिता ॥]{style="color: #0000ff"}

Adorned by शर्व-, Nandin-, महाकाल- and goddess- faces,\
having seen \[that] भूतेश्वर, the lord of ghosts, mortals are liberated.\
The mighty भूतेश्वर \[who] is all beings resides in this holy ford.\
I permit you, o hero \[Nandin], to reside in my western face.\
श्रीकण्ठ is established in the eastern face, महाकाल in the southern face,\
Nandirudra in the western face and the goddess in the direction of Soma (northern face).

The same deity is worshiped in several other Kashmirian texts like the नीलमतपुराण and the हरचरितचिन्तामणि. This establishes that the compound नन्दिरुद्रेश्वर is specifically the शिव of नन्दिक्षेत्र --- he who is the ईश्वर of Nandirudra. Hence, we can say with confidence that Part 4 of the Bower manuscript had its provenance in a Kashmirian text composed in the vicinity of this क्षेत्र. Thus, it finding its way to Kucha almost mirrors the journey of Bower in quest of the Mohammedan criminal.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/01/nandirudra-1162774520-e1704945935318.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/01/nandirudra_bhuteshvara-3090582654-e1704871011487.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad An image based on नन्दिरुद्रेश्वर installed by a bauddha सर्वाधिकारिन्. The first picture shows the western Nandin face.
```{=latex}
\end{center}
```


2. It shows some archaisms, such as the worship of the कौमार goddess षष्ठी, the prominence of the Kaubera cult, the use of the name प्रजापति, importantly, the worship of the Marut-s. Contra Hoernle, we believe that the term पुरुष-सिंह is an archaic version of the popular vibhava of विष्णु, नृसिंह. In the actual prognostications associated with the dice falls, we find the invocation of the अश्विन्-s and repeated invocation of the deity Maruta --- it is not clear if this meant the singular deity with an अकारान्त name or vulgar plural of Marut. As we shall see below, Rudra, Marut-s and विष्णु had a persistent connection to dice prognostication.

3. This is perhaps the earliest attestation of the विष्णु देवता Satya-नारायण.

The second cleromantic text from the Bower manuscript, part 5, is attested in India in the form of later-day variants going under the name पाशक-केवली. However, part 5 is again marked by interesting opening mantra-s. These mantra-s are more lacunose than those in part 4. Hence, we again provide a restoration based on Hoernle's transcription with a separation of the mantra-s:

[---- नमस्यामि लोकनाथं जनार्दनं येन सत्यम् इदं दृष्टं य दिव्य ---------------------------- ।]{style="color: #0000ff"}\
[प्राहु तत् सद्भि ह दृषा । ताला भाला का सुखं दुःखं जीवितम् मरणं तथा । इह सर्वम् मनुष्यानाम् मरुद्भिः समुदीरितम् । ऋषिभिर् निर्मिता --- ।]{style="color: #0000ff"}\
[मेरु-वासम् प्रयोजिता -- । इमा विद्या ततस् तेषां हृष्टा वै मरुत्-आदयः । तद् यथा । विमले विमले निर्मले देवि देवि व-- यत् सत्यं यत् सु-...]{style="color: #0000ff"}\
[तं तत् सर्व दर्शय अपेतु मानुषं चक्षु दिव्यं चक्षु प्रवर्ततु । अपेतु मानुष-श्रोत्रं दिव्य-श्रोतम् प्रवर्ततु । अपेतु मानुषं गन्धं दिव्यं गन्धं प्रवर्ततु । अपेतु मानुषा जिह्वा दिव्या जिह्वा प्रवर्ततु । मालि मालि स्वाहा ।]{style="color: #0000ff"}

The terms ताला and भाला, which Hoernle saw as implying the palm and forehead augury, are also noteworthy --- it might be one of the earliest hints of palmistry in the Indosphere --- suggesting that links between the disparate mantic traditions were already emerging. As one can see, it shares certain key features with the text of Kashmirian provenance in part 4. Both texts invoke the Marut-s and विष्णु. The consistent invocation of the Marut-s, who were prominent in the Vedic register of the religion, along with the other archaisms in part 4, raises the possibility that this cleromantic text had deep roots going back to the Vedic period. Indeed, it is conceivable that the Marut-s, who were seen as the force moving the planets or as metabolic forces in the body, were also conceived as the forces moving "luck" in the form of the dice throws. While the text in part 4 invokes कुम्भकारी-मातङ्ग-युक्ता, the part 5 text invokes the goddess Vimala, who in the बोधायन-मन्त्रपाठ appears as one of the goddesses in the कौमार circle. Again the deity मालि could be a variant of the name of the goddess मालिनी of the पूर्वांनाय. Notably, the later पाशक-केवली recensions from different parts of India contain an archaic prognostic incantation to कूष्माण्डिनी दुर्गा, who is remarkably also termed मातङ्गिनी in the mantra:

[ॐ नमो भगवति कूष्माण्डिनी सर्व-कार्य-प्रसाधिनीइ सर्व-निमित्त-प्रकाशिनि एह्य् एहि त्वर त्वर वरदे हिलि हिलि मिलि मिलि मातङ्गिनी सत्यम् ब्रूहि स्वाहा ॥]{style="color: #0000ff"}

This suggests that the invocation of a कौमार goddess and/or मातङ्गिनी was a likely part of the old cleromantic tradition. Further, the prognostications in part 5 invoke महेश्वर महादेव --- those who worship him have a good prognosis; those who do not, fail to receive the desired goods. This links these cleromantic traditions to the महेश्वर divination, which was recovered in a manuscript from Cave 17 at Mogao. This text links the Hindu cleromancy to चीन, Turkic and Tibetan cleromancy and will require a separate treatment. However, we believe that thematically part 5 also throws light on an interesting चीन text that has been the topic of some debate.

In the mid-400s, a चीन, Hui-Chien from Jian Kang (today Nanking), produced a multi-volume magical work that he presented as a translation of an Indian work. It includes a volume termed the "divination by \`spirit-tablets' revealed by the god Brahman'' and sanctioned for use by the तथागत. Some sinologists have claimed that it is a possible चीन aprocryphon because no comparable Indian Sanskrit text has been found. However, it is likely that it has a Sanskrit original, as stated by the author of the चीन volumes. This could have been composed in Central Asia, probably Kucha rather than core India. Indeed, in the past, it was suspected that these volumes were produced by the Kuchean मन्त्रवादिन् श्रिमित्र who visited Jian Kang. In this practice, the 100 prognostications specified in the text were written bamboo tablets or silk strips and placed in a multicolored silk pouch. Then, the साधक abstains from alcohol, meat, and the five foul-smelling Alliums (onion, garlic, leek, etc.) and rinses his mouth before the performance. He takes out 3-7 strips and reads the prognostications on them. Below are a couple of good prognostications (all translations below are by Strickmann edited by Faure):

27. How great the strength of this person's felicity!\
In all things, he receives the god's protection.\
All he desires shall be as he wishes,\
Nothing not secure and safe.\
Of a certainty, no dangers will impede him\
And his fame will flow forth far and wide.\
Spoken by the mouths of शक्र and Brahman,\
This means all happiness, without deception.

83. I am concerned for this deserted orphan,\
who will spend his whole life in the army.\
Far separated from his old ancestral village,\
Going this way and that, in pursuit of an alien wind \[or, foreign customs]\
Although he may be in other, far-off regions,\
Brahma and Sakra will nonetheless raise him up.\
In time he will return to safety and security,\
And his friends and kindred will all rejoice.

One could also get bad prognostications:

31. You are an ill-omened person\
And so have been made to dwell in this place.\
In it are maleficent phantoms and demons\
Which are constantly coming and loitering.\
Your three hun and your seven po\
Are bound and fettered to a vacant mountain.\
You are in confusion and unsettled\
And in the end will fall into the deep abyss.

66. Earlier, when you took a wife,\
You thought yourselves a pair of mandarin ducks.\
In harmonious union you established a household\
Which would surely endure safely for a long time.\
Suddenly, in the middle of the road,\
You have begun to do one another harm.\
There is no truce to your wranglings and disputes,\
And your goods and chattels are also all dispersed.

In our opinion, the parallels in the prognostic tenor between the part 5 पाशक-केवली-related text and the चीन divination of Brahman suggest that, indeed, there was a Sanskrit equivalent of it that was accessed by either Hui-Chien or श्रिमित्र and it is no aprocryphon. This is supported by multiple other circumstantial lines of evidence: 1) The discovery of the महेश्वर divination at Mogao indicates that there is a body of texts of ultimately Hindu origin that reached the चीन-s via the bauddha medium. Most of these were lost in India itself. 2) The divination of Brahman places itself against a Hindu background: as per the text, the तथागत approves of it because while the Hindus have such texts and benefit from it, the ताथागत-s are not able to avail of such benefits. 3) Further, the beneficial effects come via Hindu gods शक्र and Brahman, much like the role played by महेश्वर, अश्विन्-s, and Marut(s) in the Bower texts. We have an example from Kashmir of an image of Rudra modeled after नन्दिरुद्रेश्वर installed by a bauddha सर्वाधिकारिन् (Figure 1). This indicates how bauddha-s were incorporating Hindu material into their fold, especially when it came to practical traditions such as medicine and prognostics.

