
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some relationships involving the triangle incenter and circumcenter](https://manasataramgini.wordpress.com/2024/02/17/some-relationships-involving-the-triangle-incenter-and-circumcenter/){rel="bookmark"} {#some-relationships-involving-the-triangle-incenter-and-circumcenter .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 17, 2024]{.entry-date}](https://manasataramgini.wordpress.com/2024/02/17/some-relationships-involving-the-triangle-incenter-and-circumcenter/ "10:58 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While triangle centers (e.g., incenter, centroid circumcenter) are captivating to the amateur and mathematician alike, their serious investigation is a relatively modern pursuit. A major push in their study began with the great Leonhard Euler and was continued by the geometer Jacob Steiner. Down to the studies of the last 100 years, this has been a fertile topic of mathematical inquiry. Here, we list some interesting relationships that came up in the course of our explorations as a mathematical layman. At their heart is one which was apparently first reported by Steiner, and the rest are ones that we built upon the former. The incenter  $I$  of a  $\triangle ABC$  is determined by the intersection of the angle bisectors of its 3 angles, and the incircle of radius  $r$  is tangent to the 3 sides of the said triangle. The circumcenter  $O$  is determined by the intersection of the perpendicular bisectors of the 3 sides of the triangles, and the 3 vertices of the triangle lie on the circumcircle of radius  $R$ . Let  $d$  be the distance between  $I$  and  $O$  then we get the well-known relationship:

 $$r=\dfrac{R^2-d^2}{2R}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/02/circum_in_relationships_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Consider a point  $X$  on the circumcircle of  $\triangle ABC$ . Draw a line connecting  $X$  to incenter  $I$ .  $\overleftrightarrow{XI}$  will intersect the circumcircle again at  $X'$ . With  $X'$  as the center, draw a circle through  $I$ . This circle will intersect the circumcircle at  $Y$  and  $Z$ . Draw the lines  $\overleftrightarrow{YX}$  and  $\overleftrightarrow{ZX}$ . Then  $\overleftrightarrow{YX}$  and  $\overleftrightarrow{ZX}$  with be tangents to the incircle! Thus, we get two triangles,  $\triangle ABC$  and  $\triangle XYZ$ , sharing the same incircle and circumcircle. Let  $d_1=\overline{XI}$  and  $d_2=\overline{X'I}$ , then we get the relationship:

 $$R \cdot r = \dfrac{d_1 \cdot d_2}{2}$$ 

This is the basic Steinerian relationship, based on which we obtain the below results.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/02/circum_in_relationships_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


In the above construction, we had drawn the circle with  $\overline{X'I}$  as its radius. In addition to  $X'$ , the line  $\overleftrightarrow{XI}$  will intersect this circle at  $X''$ . We next ask the question: what will be the locus of  $X''$  as point  $X$  moves along the circumcircle of  $\triangle{ABC}$ ? We determined it thus: draw line  $\overleftrightarrow{OI}$ . On this line, mark point  $I'$ , such that it is at the same distance as  $I$  but in the opposite direction. Thus,  $\overline{OI}=\overline{OI'}=d$ . Then, the said locus of  $X''$  will be a circle (colored red in Figure 2) with its center at  $I'$  and radius equal to  $2R$ . Let  $d_3=\overline{IX''}$ , then we have:

 $$R \cdot r = \dfrac{d_1 \cdot d_3}{4}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2024/02/circum_in_relationships_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Line  $\overleftrightarrow{OI}$  cuts the incircle at points  $F, J$ ; it cuts the circumcircle at points  $L, K$ ; it cuts the above-described circle with center  $I'$  and radius  $2R$  at points  $M, N$  (Figure 3). Then we have the following 4 lengths:  $l_1=\overline{FL}, l_2= \overline{JK}, l_3= \overline{LM}, l_4= \overline{KN}$ . For these, we have the below relationships:

 $r^2 = l_1 \cdot l_2 \; \implies$  Inradius is the geometric mean of the separations between the incircle and circumcircle along the incenter-circumcenter line.

 $$r \cdot R = \dfrac{l_3 \cdot l_4}{2}$$ 


