
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Two sides of the phone](https://manasataramgini.wordpress.com/2010/03/15/two-side-of-the-phone/){rel="bookmark"} {#two-sides-of-the-phone .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 15, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/03/15/two-side-of-the-phone/ "6:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ST was describing her readings of a chIna authoress (I totally forgot her name) writing in English and how they touched a "nerve" in her even though the author is from an alien cultural setting. The points which ST mentioned as impinging on her was the sense of déjà vu, the sense of vaivasvata's haunting presence, the sense of foreboding and the sense of what Conan Doyle had termed the genius of locus. ST said she was wandering aimlessly by the sea as the day was an "indra-datta" enjoying the gift of maghavan and conspiring an elaborate feat of sUpa-kala that would rival the feats of वृकोदर in the विराटन् court. But she declared that she had hit a conceptual barrier that she was unable to break past. As she was walking along in this state of an uncoupled engine she told me of an insect that she was seeing and started describing it to me. It sounded like a neuropteridan, but that is merely a conjecture since verbal devices of communication only go that far. Then ST all of a sudden queried about a term used on these pages: the रामपूरीय असिपुत्रिका.\
I: When we talk the म्लेच्छ दीर्घश्रवस्, who is steeped in adharma, listens in like a कर्णपिशाचिनी. So for you to understand that let me use the "words of the twilight". Remember that night when we were camping on the foot of the great massif of the on which stood the maharAshTrI राजन्'s "राजमाचि". The chief of the shastra-भृत्-s of our dala caressed a few asi-पुत्रिका-s. Ah' the intrepid ardor of youth, where is it now?\
Then ST went on to tell me of her peregrinations in the Northern lands at the largesse of her master. We simultaneously converged on the name Snorri Sturluson -- A déjà vu said ST? Nothing to be surprised by I thought. ST asked why did you think of him?\
I: Some how the word असिपुत्रिका reminded me of the constructs of the Hattatal.\
ST: Touche, you will not believe what was in my mind.\
I: Though deluded by the कीलित-preta there was something of kavI still left in old Snorri.\
ST: In those regions the past still linger as genii of the locus under the grime of the unmatta-s. I could still connect with Steinunn, the connection to the Indo-European stem?\
I: Yes, indeed despite being centuries apart and miles apart and seeming different to the undiscerning eye a common skein runs through the frills of Snorri and the contortions of the paNDita-rAya.

This conversation was to send ST on the path of a philosophical discovery. Unlike, ST I am rarely prone to the déjà vu. Hence, her proclivity for it contrasted with my own gave me a window into a common misunderstanding about consciousness that we might discuss sometime later using ST's philosophical discovery as a कथामुख. But one thing to note at this point is that there could be a genetic foundation for the proclivity for déjà vu as ekanetra too, like ST, experiences it strongly. But there is another more subtle phenomenon that sort of resembles the déjà vu that we do experience that relates to the same class of cognitive processes. What we are referring too is the phenomenon of feeling like being at some other place and time that we feel we might have been through before. This is often induced by novel auditory cues and sometime by a more complex set of simultaneous environmental stimuli -- visual, tactile and auditory flowing through the different ज्ञानेन्द्रिय-s.


