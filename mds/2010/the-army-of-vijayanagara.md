
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The army of vijayanagara](https://manasataramgini.wordpress.com/2010/09/15/the-army-of-vijayanagara/){rel="bookmark"} {#the-army-of-vijayanagara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 15, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/09/15/the-army-of-vijayanagara/ "6:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh6.ggpht.com/_hjuA1bE0hBw/TJBnYMMV3GI/AAAAAAAAB8A/38gsv_NufiI/s800/hindu_army.JPG){width="75%"}
```{=latex}
\end{center}
```



The memories of the Hindu response to the तुरुष्क depredation of our land (captured by the उपवीर at vijayanagara). Note the camels and the musket-wielding infantry.

