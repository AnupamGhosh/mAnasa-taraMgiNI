
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Imaginary ratios](https://manasataramgini.wordpress.com/2010/08/13/imaginary-ratios/){rel="bookmark"} {#imaginary-ratios .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 13, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/08/13/imaginary-ratios/ "6:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh3.ggpht.com/_hjuA1bE0hBw/TI0dO6cWecI/AAAAAAAAB7U/DmvN2TQRzK4/s400/incendia.jpg){width="75%"}
```{=latex}
\end{center}
```



