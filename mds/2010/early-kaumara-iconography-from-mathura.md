
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [early कौमार iconography from Mathura](https://manasataramgini.wordpress.com/2010/02/10/early-kaumara-iconography-from-mathura/){rel="bookmark"} {#early-कमर-iconography-from-mathura .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 10, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/02/10/early-kaumara-iconography-from-mathura/ "4:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2010/02/kaumara_grahas.jpg){width="75%"}
```{=latex}
\end{center}
```

\
The holy city of Mathura is one of the great sthala-s for the member of the कुमार शासन. Above are a sampling of images of what survives of the early कौमार iconography after the several Mohammedan assaults on the city. They are mostly from the शुङ्ग, though some could be from the कुशान period.

You can access a related note by श्री sarvesha तिवरी at his site


