
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [ब्राह्मण-s and nAstika-s](https://manasataramgini.wordpress.com/2010/01/17/brahmana-s-and-nastika-s/){rel="bookmark"} {#बरहमण-s-and-nastika-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 17, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/01/17/brahmana-s-and-nastika-s/ "7:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had discussion with our friend as to whether the ब्राह्मण-s subverted the nAstika mata from within. In a sense, this is not a new theory: Astika narratives state that rudra sent बृहस्पति to delude the asura-s by posing as the तथागत, the peddler of false शास्त्र-s. Alternatively they state that नारायण incarnated as the तथागत and also generated the nirgrantha to delude the asura-s with fake teachings. We also have the narrative about भट्ट कुमारिल, who poses as a student of the nAstika-s for long and then attacks their doctrines from within and defeats them. Of course, there are other ways of looking at it --- as we have mentioned before, there could have very well been a brahminical brain drain due to the intellectual attractions of the nAstika cult and its suitability for pursuing a certain ease of life. Consequently, this brain-drain could have resulted in a passive imitation of the brahminical style by the nAstika-s and an inundation of their system with brahminical memes. One could also argue that there was really nothing strange about the brahminical participation the nAstika cults. Some would point out that even today in anti-Astika movements like socialism (including institutional sepoy leftism of Western academia and its implants), "liberalism" and secularism there is a significant population of people of formerly ब्राह्मण pedigree. We also see a prominent brahminical presence in modern बाबाइस्म्स् --- fatal attraction for पाषण्ड-s. Likewise, in the past some ब्राह्मण-s became bauddha-s. While distinct, these alternatives are not mutually exclusive and there could be a bit each in action. Indeed, there is also the reverse possibility that the nAstika-s were attempting to subvert the ब्राह्मण-s. After all we know well that the तथागत himself was the master at this. So the presentation of ब्राह्मण-s as purveyors of the nAstika-mata could be a means of trying to say: "See the ब्राह्मण-s themselves after studying all the veda-s are endorsing this stuff, so better follow it." After all we also know that nAstika Acharya-s, searching for amenable कुलाङ्गना-s, posed as shaiva guru-s among अवर्ण-s and claimed to teach them the kalottara tantra. A basic problem with this line of argument is a lack of understanding of how the वर्ण system operates. Demonstrating what happened objectively from textual studies is not entirely easy and we need to create a proper framework of measurement for it. One feature that could be used to measure this is the frequency with which the nAstika texts introduce statements that reduce the divergence between the substratum of the Astika and nAstika narratives (e.g. the role of the veda). In this regard we found a corrupt narrative in the मञ्जुश्रीय-मूल-kalpa to be of interest, though puzzling. The MMK has a long section that is like the भविष्यत पुराण of the Astika-s, which "predicts" future events of kings and teachers. Certainly, I do not understand all of this narrative as it stands due to the corruption and the lacuna on top of the vulgar form of the भाष.\
MMK 53.887-895 (I am now using the gaNapati shAstrI edition as against vaidya's):

[मन्त्र तन्त्राभियोगेन ख्याताः कीर्तिकराः स्मृताः ।\
अधुना तु प्रवक्ष्यामि द्विजानां धर्मशीलिनाम् ॥\
मन्त्र-तन्त्राभियोगेन राज्य-वृत्तिम् उपाश्रिता ।]{style="color:#0000ff;"}\
This part speaks of ब्राह्मण-s, who are upholders of dharma, who will be engaged in stately or royal activities by means of mantra-s and tantra-s.

[भवति सर्वलोके ।अस्मिं तस्मिं काले सुदारुणे ॥\
वकाराख्यो द्विजः श्रेष्ठः आढ्यो वेदपारगः ।]{style="color:#0000ff;"}\
These verses state that there will be a dreadful time in the world when a great ब्राह्मण will be born, with his name containing the syllable va (an allusion to vajra) rich and completely knowledgeable in the veda-s.

[सेमां वसुमतीं कृत्स्नां विचेरुर् वाद-कारणात् ॥\
त्रि-समुद्र-महापर्यन्तं पर-तीर्थानां विग्रहे रतः ।]{style="color:#0000ff;"}\
This ब्राह्मण is said to go around the land till the three oceans for debates and delight in disputations with rival scholars.

[षडक्षरं mantra-जापी tu abhimukhyo hi वाक्यतः ||\
कुमारो गीत-वाह्य् आसीत् सत्त्वानां हित-काम्यया ।]{style="color:#0000ff;"}\
He will keep doing the japa of the षडक्षर mantra, muttering it and meditating on the deity; he will bear the song of कुमार for the benefit of the good people.

[एतस्यै कल्प-विसरान् महितं बुद्धितन्द्रितः ?! ॥]{style="color:#0000ff;"}\
This half-verse is particularly puzzling --- if there is no deletion in the extant text then the etasyai is certainly out of place. It is probably a scribal or editorial misreading for etasya referring the ब्राह्मण with the name containing a 'va' in the earlier verses: masculine genitive singular: etasya will be more suitable than an unaccountable feminine dative singular. Then there is the compound: बुद्धितन्द्रितः which would mean he was of lazy intelligence or a मूढ. But this is completely out of character with the description in the rest of the text, assuming no break occurred. So perhaps here again the word is erroneous and the original was intended to mean the opposite (?). In any case it is likely that the text implies 'va' dvija was renowned for his knowledge of numerous texts (kalpa-s) or knowledge of mantra-s.

[जयः सुजयश् चैव कीर्त्तिमान् शुभमतः परः ।\
कुलीनो धार्मिकश् चैव उद्यतः साधु माधवः ॥]{style="color:#0000ff;"}\
Here other कुमार mantra साधक-s are mentioned: jaya, sujaya, shubhamata and the good माधव born of a noble, Dharmic family.

[मधुः सुमधुश् चैव सिद्ध ...ह्...। नमस्तदा ।\
रघवः शूद्र-वर्णस् तु शक-जातास् तथापरे ॥]{style="color:#0000ff;"}\
Then there will be madhu, sumadhu, the siddha (break in the text), and राघव of the 4th वर्ण who will born in the midst of the shaka-s.

[ते ।अपि जापिनः सर्वे कुमारस्येह वाक्यतः ।\
ते चापि साधकः सर्वे बुद्धिमन्तो बहु-श्रुतः ॥]{style="color:#0000ff;"}\
All these are said to do japa, muttering the कुमार mantra (i.e. the षडक्षरि) and these साधक-s will all be intelligent and learned.

[आमुखा मन्त्रिभिस् ते च राज्य-वृत्ति समाश्रिता ।\
तस्यापरेण विख्यातः विकाराख्यो द्विजस् तथा ॥]{style="color:#0000ff;"}\
They will portray themselves as mantrin-s and be involved in business of kingdom.Then it goes on with the narrative of the dvija born with a 'vi' syllable in name, who comes there after.

[Kashmirian मञ्जुकुमार (from Norton Simon collection)]{style="color:#0000ff;"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh3.ggpht.com/_hjuA1bE0hBw/S1P_kGnjk_I/AAAAAAAABOs/3hKcgp_zJ6g/s400/manjushrI_कुमार.jpg){width="75%"}
```{=latex}
\end{center}
```



Though there are problems with the text, we can easily discern the basic meaning of what is being conveyed. A key point to note is that if this textual fragment were shown to someone one without the context one could almost mistake it to be an Astika narrative. There are some key words such as the para-तीर्थ-s which betrays the work to be nAstika one. There is another more subtle point of note --- the mantra practitioners here are not buddha-bhikShu-s i.e. monks cut off from society. They are apparently politically highly engaged individuals with interests in the deployment of the mantra-शास्त्र for kings, and engaging in duties of states. They are well-read scholars who are lead by dvija-s who have learned the veda-s, but also include शूद्र-s. While both the तथागत and the nagna were most probably active mantra-वादिन्-s, and they had some contact with rulers, their respective mata-s still started as monastic institutions. The monks did engage in अभिचार on each other and the lay populace, but they were hardly involved in royal activities like an अथर्ववेदी ब्राह्मण expert's राजकर्माणि. So in this we see a conscious imitation of the ब्राह्मिनिcअल् activities. Further, these nAstika mantrin-s like the 'va'-named one are not described as specially learning any nAstika texts but more generally veda-s (as an Astika would do) and tantra-s and their mantra-s. Even the mantra in this section is couched in general terms --- the षडक्षरि could easily be the कौमार षडक्षरि of the Astika world, though what is meant the nAstika मञ्जु षडक्षरि -i.e. [a ra pa cha na धीः ||]{style="color:#0000ff;"}. The mantrin-s are said to bear the song of कुमार. These could well be कार्त्तिकेय-s stuti-s as well, but probably what is implied is some version of the मञ्जुश्री nAma संगीती.

It is in the context of this connection to the कौमार system that we get to a generally unnoticed point regarding the bauddha-ब्राह्मण convergence we are discussing. We have previously discussed how in the early phase of the development of the bauddha mantra-शास्त्र (the क्रिया tantra-s) there was absorption of the कौमार system to give rise to that of मञ्जुश्री. In the MMK, the various siddha along with 'va' are described as chiefly practicing the विद्या of कुमार. This विद्या is described as the षडक्षरी mantra which is normally taken to be the one of मञ्जुश्री cited above but this is not specified here in the MMK. However, it should be noted that in the Astika parlance the मूल षड्कषरी is the कौमार mantra: [वछद्भुवे नमः ॥]{style="color:#0000ff;"} (e.g. expounded in the old कौमार tantra, the skanda-सद्भाव). The literal meaning of the root word in this mantra, वचद्भू is that कुमार is the one born of speech. This connection is important because मञ्जुश्री is also remembered as the deity of speech (e.g. वादिराट्), and in the above narrative the worshipers of कुमार are described as engaging in verbal debates. We do observe an old appropriation of कुमार by the nAstika-s --- e.g. there is the tale that indra sent his army chief skanda as नागसेन to convert the yavana milinda to the bauddha-mata. The nagna-s likewise hold that indra sent नेजमेष to root out the ब्राह्मण molecules from the body of वर्धमान and make him a pure kShatriya. However, in the context of the MMK role of कुमार is distinct. In fact the तथागत-s take the backseat while the dvija-s and शूद्र-s actively worship कुमार --- i.e. while add the façade that he is doing the work of the तथागत, it is कुमार and not the tathagata whose active worship they are conducting --- the spirit of which is maintained in the मञ्जुश्री-nAma-संगीती. Now, we should place these in the context of various events happening in the Astika world: 1) Even here कुमार-s role as the victor in philosophical debates is remembered by tradition. The famous भट्ट कुमारिल is remembered as an अवतार of कुमार who arrived to defeat the nAstika-s in debate. 2) Grammars and other forms of extraordinary scholarly achievements are acquired through the invocation of कुमार (this tale of vararuchi's enlightenment forms an important component of certain versions of the बृहत्कथ, e.g. that narrated much latter by भट्ट somadeva). 3) In the Astika realm कुमार was also invoked for the victory of rulers in wars and several dynasties make mention of royal worship of कुमार. In some cases, as in the yaudheya republic, कुमार was even seen as the real ruler of the kingdom on whose behalf the council administers the kingdom.

Reflections of these elements are seen in the above snippet and also abundantly elsewhere in the MMK. The MMK started crystallizing over a period of time beginning during the peak of कौमार शासन and the emergence of national as well as regional powers all over भारतवर्ष whose kShatriya-s invoked कुमार as a key deity. These observations suggest that this phase of nAstika-ब्राह्मण convergence was due to the drive within the bauddha-mata to compete for roles of Astika ब्राह्मण mantra-वादिन्-s in royal circles. While formerly the nAstika-s occupied a role of receiving the largess of kShatriya-s like ashoka, now they wanted to interact with kShatriya-s not just as बिक्षु-s but also as members of the court who could perform mantra services. The MMK suggests that the major move in this direction happened in the milieu of the कुमार-worshiping kingdoms of the early Tantric age. The drive we suspect allowed ब्राह्मण-s and others including शूद्र-s to be within the bauddha fold and perform roles closer to their traditional Astika roles in a royal context and allowed convergence.


