
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A discourse on a shiva mantra and आर्ष usage in it](https://manasataramgini.wordpress.com/2010/09/13/a-discourse-on-a-shiva-mantra-and-arsha-usage-in-it/){rel="bookmark"} {#a-discourse-on-a-shiva-mantra-and-आरष-usage-in-it .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 13, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/09/13/a-discourse-on-a-shiva-mantra-and-arsha-usage-in-it/ "6:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[स्तोमं वो अद्य रुद्राय शिक्वसे क्षयद्वीराय नमसा दिदिष्टन ।\
येभिः शिवः स्ववान् एवयावभिर् दिवः सिषक्ति स्वयशा निकामभिः ॥ ऱ्‌V१०।९२।९]{style="color:#99cc00;"}

स्तोमं=a chant (accusative singular); वः= personal pronoun; adya= today (adverb); रुद्राय= rudra (dative singular); shikvase= skilled-one (*आर्ष* dative singular); क्षयद्वीराय= lord of heroes (dative singular); नमसा (instrumental singular); दिदिष्टन= to present; येभिः= pronoun plural; शिवः= shiva, i.e. auspicious (nominative singular) स्ववान्= good protector (nominative singular); एवयावभिः=swift-moving (instrumental plural); दिवः= sky (ablative singular of dyaus); सिषक्ति= accompanied by; स्वयशा= evidently famous (nominative singular); निकामभिः= eager ones (instrumental plural).\
Present your chant today that pays obeisance to the rudra, the skillful one, the lord of heroes;\
shiva, the good protector, of great fame, comes from the sky accompanied by those one who are swift and eager.\
Who are the swift and eager ones? They are the horses yoked to the chariot that bears rudra to the ritual where the chant is being recited. An alternative, although to me less likely, interpretation is that they are the marut-s who are accompanying rudra. The only element in support of this the term evaya which sometimes might be used for marut-s.

Some words of note:\
क्षयद्वीर -- this term is commonly used for rudra. The great Vijayanagaran commentator सायण notes that क्षयेण means ऐश्वर्येण. Some have interpreted क्षयद्वीर as destroyer of heroes. This does not seem to be appropriate especially when the epithet is used for पुषण् or even indra. But सायण is evidently correct because this क्षय is the Indo-Aryan cognate of the Iranian ख्षय् -- the root meaning to rule. This is attested in the Avestan and later ख्षायथिय for lord or king (also related to क्षत्र).\
shikvas- The declension of this word in dative singular as shikvase is the point of interest. In classical संस्कृत, the masculine noun of the form shikvas would tend to decline in dative as शिकुषे. However, the vaidika shikvas declines as shikvase following the same formula as its twin noun in the shruti, shikvan, which declines in dative as shikvane. My interlocutors in bhArata called this declension of shikvas as an element of the आर्ष language. Of course it was a regular for the declension of such nouns in the Vedic layer, though they seem to have been replaced by the -van suffixes in the later layers of the language. The -vas/van suffix twins are similarly seen in other cases in the veda: ऋभ्वन्/ऋभ्वस् which also means skilled and the somewhat similar case of dhanuSh/dhanvan. Thus, if a -vas suffix masculine noun is encountered in the veda it will decline similarly to the van suffix twin but retaining the 's' in place of 'n'.

A common misconception among the unerudite is that the name shiva is not applied to rudra in the shruti. The above is one instance showing that to be wrong.


