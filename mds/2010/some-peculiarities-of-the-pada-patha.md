
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some peculiarities of the pada पठ](https://manasataramgini.wordpress.com/2010/06/28/some-peculiarities-of-the-pada-patha/){rel="bookmark"} {#some-peculiarities-of-the-pada-पठ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 28, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/06/28/some-peculiarities-of-the-pada-patha/ "6:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Years ago while learning RV 2.12.13 I encountered a peculiarity of Vedic usage that I asked my teacher about. He described the two alternatives: 1) going with पतञ्जलि's recommendation that the pada पाठ should be reconstructed as per पाणिनि, ignoring the received पदपाठ; or 2) going with the received पदपाठ and accepting its peculiar rules. He also pointed out that a more fundamental study of the basis the पदपाठ would require a comparison of the AV-vulgate कण्ड 20 पदपाठ with that of the RV. He mentioned the special vaidika rule to explain what we saw in 2.12.13 but I did not register it. Later, श्री PR pointed me to a pamphlet (pada-पाठ charcha) by the famous vaidika scholar in the city of my youth KV Abhyankar on the pada पठ, wherein I found discussion on issues my teacher had mentioned.

Tradition has it that the deva indra himself in his revelation grammar to the ऋषि-s had specified the sUtra:\
[अर्थः पदं ।\
]{style="color:#99cc00;"} This sUtra is the foundation on which the पदपाठ is generated. KVA explains that this sUtra implies that a word or a part thereof is defined as pada, only if it possesses a sense that is distinctly conveyed by it. Thus, a compound is a pada if the sense (artha) inheres from the compound state rather than the individual fragments. If the individual components of a compound convey the intended sense separately by themselves then they are separated into two pada-s. In संस्कृत some nouns are formed by an affix added to another noun, known after the example taddhita (i.e. tat+hita). Even their retention or dissolution in the pada पठ is governed by this sUtra.

The next sUtra in this regard as per यास्क is:

[चत्वारि पद-जातानि नामाख्याते चोपसर्ग निपाताश् च ।]{style="color:#99cc00;"}\
There are four kinds of pada-s recognized by the makers of the पदपाठ: nouns, verbs, prepositions and indeclinables. KVA further clarifies these with the sUtra:

[तानि द्विविधानि केवलानि समस्तानि छ ।]{style="color:#99cc00;"}\
Each of these kinds is again of two types: Single or connected. i.e. where two fragments are linked into a single word that bears a single उदात्त.

[तन्नाम येनाभिदधाति सत्त्वं ।]{style="color:#99cc00;"}\
Nouns are those words which denote an entity which has been perceived. It includes pronouns and is declinable: e.g. अग्निं, यज्ञस्य etc.

[तदाख्यातं येन भावं स धातुः ।]{style="color:#99cc00;"}\
Verbs (आख्यात) are those which specific action (भवं, e.g. vardhate, like the verb lists of the निघण्टु and its Avestan parallel) and denote the nature of the activity (धातु or the root, e.g. वृध्, like the lists in the धातुपाठ of पानिनि).

[क्रिया विशेष वाचिन उपसर्गाः ।]{style="color:#99cc00;"}\
Words denoting specific features and directions of the actions denoted by the roots are the prepositions. Typically they precede immediately the verb or noun derived from the root that they qualify. However, in the saMhitA पाठ they can be intervened by one or more words. If they immediately precede a verb which has retained its accent then they form a compound with the verb. These form the foundations of pada पठ interpretation. Now there are many more sUtra-s that go into the details but let us consider the peculiarities:\
saMhitA (RV 1.10.11): [आ तू न इन्द्रः ।]{style="color:#99cc00;"}\
pada: [आ । तु। नः । इन्द्रः ।]{style="color:#99cc00;"}-> why is tU-> tu?\
निपात-s (indeclinables) may be lengthened for Chandas in saMhitA but they are restored to their normal form in pada.

  - Now there are some peculiarities of dvandva-s that become immediately apparent when the पदपाठ is recited. Especially frequent in the veda are देवता dvandva-s that are also found in Avestan, Greek, Latin and Hittite suggesting it might have been there from common ancestor of all Indo-European lineages. Rule:\
[देवता-द्वन्द्वेषु वनस्पत्यादिषु च युगपद् द्वावुदात्तौ ।]{style="color:#99cc00;"}\
The authority for this sUtra-s is पाणिनि 6.2.140-141 and the प्रातिशाख्य 2.48 of the shukla-yajurvedin-s. It states that देवता dvandva-s and compounds of the form vanaspati retain an उदात्त of each word of the compound. Thus we have: mi\[tra]-\[va]रुण; [द्या]वा-पृथि[वी]; \[va\]nas\[pa]तिः; \[in]द्रा[बृ\]has \[pa\]tI.\
However, this rule is violated if the said nouns are in the vocative case, in which case there is a single उदात्त on the first syllable: \[in\]dra-वायू; \[so\]ma-rudra.

  - In many cases the संहितापाठ has a Na or Sha in place of the actual na or sa. These are restored to na and sa in the पदपाठ:\
e.g.: saMhitA: [मो षू ण इन्द्रात्र पृत्सु देवैरस्ति हि ष्मा ते शुष्मिन्-नवयाः ।]{style="color:#99cc00;"}\
pada: [mo iti | su | na | indra | atra | पृत्.su | देवैः | asti | hi | sma | शुष्मिन् | ava.yAH ||]{style="color:#99cc00;"}\
Note the transforms of the above kind and also the shortening mentioned earlier of ष्मा-\>sma.

  - Now we come to that one which puzzled me most:\
saMhitA:[शुनश्-चिच्-छेपं निदितं सहस्रात् ।]{style="color:#99cc00;"} (RV 5.2.7)\
pada: [शुनः।शेपम् । चित् । निदितम् । सहस्रात् ॥]{style="color:#99cc00;"}\
Note that in the पदपाठ the 'chit' which was inserted inside the noun शुनःशेप was pushed after the restored form of the word in which it was inserted. This is not only true of this one instance but there are other such examples which have been explained by the great Kashmirian commentator उवट, who lived in bhoja-deva's court.\
However note the following:\
saMhitA: [द्यावा चिद् अस्मै पृथिवी नमेते ।]{style="color:#99cc00;"} (RV 2.12.13)\
pada: [द्यावा । चित् । अस्मै । पृथिवी इति । नमेते इति ॥]{style="color:#99cc00;"}\
Now even though this is apparently very similar to the शुनःशेप example above. But here the dvandva द्यावा-पृथिवी is not restored. Why? This was what puzzled me. My teacher had said the given RV पदपाठ of sthavira शाकल्य deploys the proposal of [युग-पद्-अधिकरण-वछनता]{style="color:#99cc00;"}. Now KVA explains that this proposal holds that both the halves of certain compounds are by themselves capable of conveying the sense of the whole compound. As examples of the above proposal elliptical dvandva-s are invoked by a पाणिनि वार्त्तिक-कार: just द्यावा or पृथिवी stand for the whole dvandva or पितरा stands for माता-पितरा in the सूक्त to the ऋभु-s. So apparently the sthavira शाकल्य proposed that द्यावा and पृथिवी in examples like 2.12.13 are cases of yuga-pad-अधिकरण-वचनता and each fragment stands for the whole. Now, since the पदपठ's objective is meaning (first sUtra above), such split dvandva-s interpreted under the above proposal should be accordingly used in interpretation of the ऋक् in question. Is this really the case?


