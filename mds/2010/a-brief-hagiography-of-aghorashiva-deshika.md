
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief hagiography of aghorashiva deshika](https://manasataramgini.wordpress.com/2010/09/03/a-brief-hagiography-of-aghorashiva-deshika/){rel="bookmark"} {#a-brief-hagiography-of-aghorashiva-deshika .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 3, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/09/03/a-brief-hagiography-of-aghorashiva-deshika/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The सैद्धान्तिक tradition or the Urdhvasrotas of the shaiva mantra-मार्ग has an extensive exegetical tradition that covers a variety of topics. Of these the doctrinal sections are mainly of interest to the practicing shaiva insider. This material has some historical value in terms of understanding the origin of the mantra-मार्ग from earlier पाशुपत traditions of the ati-मार्ग variety. The mantra praxis and yoga are of wider interest for they even inform the स्मार्त ritualist in this regard. The temple ritual and installation of the सैद्धान्तिक-s is again of considerable general interest. Another point of interest are the philosophical leanings and vaidika scholarship among the early सैद्धान्तिक-s of both northern and southern India. In particular the सैद्धान्तिक-s appear to have continued the tradition of the पाशुपत-s in terms of their leanings towards the वैशेषिक and nyAya systems. In light of all this it is fruitful for a स्मार्त to study the works of the learned सैद्धान्तिक exegetes. The सैद्धान्तिक-s had a prolific line of तान्त्रिक-s starting from the early sadyojyotis and बृहस्पति and including the [Kashmirian school](https://manasataramgini.wordpress.com/2008/02/08/the-kalatman-mandala-at-angkor-wat/), mahArAja bhojadeva परमार and the south Indian school with aghorashiva as its luminary ([we have briefly discussed this before](https://manasataramgini.wordpress.com/2007/09/24/the-saiddhantika-s-and-the-kaumara-s/)). Here we shall attempt to provide a brief hagiography of aghorashiva deshika.

The primary source on the life of aghorashiva is a text called the gotrasantati which is not very well preserved. It appears to have to been intended as a biographical appendix to his magnum opus, the 7 volume क्रियाक्रमद्योतिका. As per this text he finished this work in 1157 CE. Tradition holds that he was born in a family of shaiva ब्राह्मण-s near Madurai or Naraiyur or in Chidambaram itself in the Tamil country, which followed the तैत्तिरीय shAkha and belonged to the कौन्डिन्य pravara of the वसिष्ठ gotra. He was born under the name parameshvara and received his सैद्धान्तिक दीक्ष from the noted shaiva तान्त्रिक हृदयशम्भु, who was his paternal great-grandfather's brother's son and thus assumed the दीक्ष name aghorashiva. In this preceptorial lineage were the famous shaivas: 1) somashambhu who was active in Varanasi and in the haihaya kingdom at Dahala. He composed the famous somashambhu paddhati and was also a scholar of the bhairava tantra-s of the दक्षिण srotas. 2) ध्यानशिव, was from Bengal and a preceptor of the sena rulers. 3) श्रीकण्ठ-shiva, who was known as the वङ्ग-वृष्भ and a famous scholar from Varendra. 4) सर्वात्म-shiva who was an आचार्य stationed in the Elephanta cave. 5) विद्यान्त-shiva. 6) पूर्ण-shiva; both the preceding आचार्य-s were rAja-guru-s of the kalachuri-s at Varanasi. 7) brahma-shiva who was an Acharya from Gujarat. 8) उत्तुङ्गशिव who was originally from the सोमनाथ पीठ in Gujarat but settled in Kalyani in Karnataka and wrote a तान्त्रिक ritual manual, which aghorashiva refers to in his own works. This lineage of सैद्धान्तिक was known as the Amardaka (औण्ढा नागनाथ in modern Maharashtra) after its original मठ founded by one of the great early सैद्धान्तिक-s of that name. This मठ was close to that of the glorious center of the कापालिक-s, namely चण्डिकाश्रम, founded by तापस. One of Amardaka's students was the famous purandara who founded the mattamayura lineage whose आदिमठ was in the Punjab. The Amardaka lineage spread to the south, especially in the Konkan, Andhra and TN and in the east in Bengal. aghorashiva was also a doctrinally inclined towards the works of the Kashmirian school of सैद्धान्तिक-s of भट्ट रामकण्ठ's lineage. He calls भट्ट रामकण्ठ-II the महाकण्ठ कण्ठीरव, whose leonine roar is supposed to defeat the rival philosophies in debate. aghorashiva became the head Acharya of the Amardaka lineage's मठ in Chidambaram during the reign of the choLa king कुलोत्तुङ्ग-I. The location of this मठ is in the suburbs of the modern Chidambaram and the current structure that is present at this spot appears to have been rebuilt after the Mohammedan invasion of Maliq Kafur. His students hailed him as a second shiva and there was a tradition that the founder of the shaiva tantra-s, दुर्वासस् himself arrived in Chidambaram to crown him as the आचार्य of the मठ. He was supported in his endeavors by the fierce Tamil warrior नरलोकवीरन्, who led the choLa armies into the Godavari delta and captured Orissa, and supported many shaiva and vaiShNava temples. A statue of aghorashiva along with those of several other आचार्य-s in his parampara is found in the राजराजेश्वर temple in Darasuram complex of Kumbhakonam.

aghorashiva and his students state that that he had deeply studied the grammatical tradition, nyAya-वैशेषिक and the vaidika texts and had written on them but, to my knowledge, we do not have any of those works. He was also a notable Sanskrit poet who composed secular poems such as आश्cअर्यचार on a magical journey and काव्यतिलक, a play named the abhyudaya and a polemical poem titled the पाषण्डापजय that had some thematic similarity to the play of jayanta-भट्ट.

His doctrinal shaiva work is encapsulated in his 8-volumed work, the अष्टप्रकरण-वृत्ति, which are commentaries on 8 shaiva works:

 1.  The tattva-प्रकाश of mahArAja bhojadeva परमार

 2.  The tattva-संग्रह of sadyojyotis

 3.  The tattva-traya-निर्णय of sadyojyotis

 4.  The नादकारिका of भट्ट रामकण्ठ-I

 5.  The भोगकारिका of sadyojyotis

 6.  The ratna-traya-परीक्षा of sadyojyotis

 7.  The mokSha-कारिका of sadyojyotis

 8.  The परमोक्षनिरास-कारिक of sadyojyotis\
aghorashiva's commentaries on the last two works do not survive.

His work on the tantra-s themselves include a दीपिका on the वृत्ति of the मृगेन्द्र tantra by भट्ट नारायण कण्ठ of the Kashmirian school. He wrote his own वृत्ति-s on the द्विशतिकालोत्तर, सर्वज्ङानोत्तर and मोहशूरोत्तर Agama-s. The complete dhyAna of सदाशिव, the highest देवता of the सैद्धान्तिक system with his retinue of देवता-s is provided by aghorashiva in his [पञ्चावर्ण stava](https://manasataramgini.wordpress.com/2008/02/26/the-panchavarna-stava/) that we have discussed earlier. After these come his greatest work, the क्रियाक्रमद्योतिका, whose clarity in the exposition of ritual and mantra practice reaches delectable heights on occasions. Its seven volumes were:

 1.  nityakarman; 2) naimittika-karman, पवित्रारोपण and damanakotsva; 3) दीक्ष, अभिषेक and अन्त्येष्टि; 4) shiva-प्रतिष्ठ-vidhi; 5) सुब्रह्मण्य-प्रतिष्ठा-vidhi; 6) mahottsava-vidhi; 7) प्रायश्चित्त-vidhi. So definitive was this work that to this date we see the rituals in any shiva temple in South Indian being performed as per its prescriptions. In this the memory of aghorashiva lives on, even if all we see is a pale reminder of the choLa glory.

As a concluding remark, I would like to point out that in aghorashiva's ritual system shAstA was also being absorbed into the shaiva realm and was worshiped along with skanda and विनायक. The worship of shAstA appears to have followed the ajita-महातन्त्र, but sadly a part of the shAstA section of this tantra is lost in the surviving versions of the text.


