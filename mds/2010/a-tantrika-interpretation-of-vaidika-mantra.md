
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A तान्त्रिक interpretation of vaidika mantra](https://manasataramgini.wordpress.com/2010/08/18/a-tantrika-interpretation-of-vaidika-mantra/){rel="bookmark"} {#a-तनतरक-interpretation-of-vaidika-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 18, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/08/18/a-tantrika-interpretation-of-vaidika-mantra/ "6:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having completed the second circle we mediated up on the might of the great archer invoking him with the mantra whose ऋषि is haryata the son प्रगाथ of the clan of the काण्व-s and whose meter is the gAyatrI:\
[अन्तर् इच्छन्ति तं जने रुद्रं परो मनीषया । गृभ्णन्ति जिह्वया ससम् ॥]{style="color:#99cc00;"} RV 8.72.3

antar= within; इच्छन्ति=desire/seek; taM=him; jane= men(locative); रुद्रं= rudra; paraH= beyond; मनीषया= mind (instrumental); गृभ्णन्ति = seize; जिह्वया = tongue (instrumental); sasam= plant/grass/grain

The word sasam needs further examination. Typically, it is taken to mean a grain -- a shortened version of the more prevalent form sasya (or shasya) which means grain. However, we need to examine occurrence of this word elsewhere in the RV. Based on its occurrence in other places in the RV it appears that sasa unlike sasya tends to mean plant or grass or pasture. For example consider the ऋक्:\
[प्र मातुः प्रतरं गुह्यम् इच्छन् कुमारो न वीरुधः सर्पद् उर्वीः ।\
ससं न पक्वम् अविदच् छुचन्तं रिरिह्वांसं रिप उपस्थे अन्तः ॥]{style="color:#99cc00;"} RV 10.79.3

Here agni is said to be like a कुमार (child) who seeking out his mothers secret place creeps over the widespread vegetation. Like burning food he seeks the sasa (i.e. the plants) and licks deep into innards of the earth. This occurrence supports the sense plant for sasa. But this ऋक् also indicates that sasa is explicitly identified as the food eaten by agni (i.e. पक्वं)

Hence, we may render the original verse in it is Vedic sense thus:\
Him, rudra, who is beyond conception by thought, they \[i.e. deva-s] seek in the midst of men; they seize the plants \[as food] using him as the tongue.

To be able to interpret the ऋक् in the तान्त्रिक sense need to first take up its मीमाम्सक explanations. Now in the basic literal sense the sasa (grass) consumed by rudra the could be taken to mean the kusha grass dipped in ghee that is offered in the fire as the share of rudra. This is laid out as the final offering in the context of the parvan sacrifices by the gobhila गृह्य sUtra of the सामवेद tradition with the mantra:\
[यः पशूनाम् अधिपती रुद्रस् तन्तिcअरो वृषा, पशून् अस्माकं मा हिंसीर्; एतद् अस्तु हुतं तव, स्वाहा ॥]{style="color:#99cc00;"} GGS 1.8.28\
Of our cattle you are the lord rudra, the bull roaming near the line \[to which calves are tied], do not harm our cattle; this oblation is for you, hail!\
But the significance of the verse goes beyond that because it is not merely alluding to the grass offering to rudra but the fact that he is the tongue by which the deva-s eat the food which is being offered by men. To understand the precise significance of this we must turn to the तैत्तिरीय saMhitA 2.6.6 which provides the ब्राह्मण regarding the offering of the स्विष्टकृत् cakes:

[अग्न्íर् अम्úष्मिन् लोक्á Áसीद् यम्ò ऽस्म्íन् : त्é देव्Á अब्रुवन्न् éतेम्áउ व्í प्áर्यूहाम्éति : अन्न्Áद्येन देव्Á अग्न्íम् उप्Áमन्त्रयन्त : राज्य्éन पित्áरो यम्áं : त्áस्माद् अग्न्íर् देव्Áनाम् अन्नाद्ó : यम्áः पितृण्Áं र्Áजा : य्á एव्áं व्éद प्र्á राज्य्áम् अन्न्Áद्यम् आप्नोति : त्áस्मा एत्áद् भागध्éयम् प्र्Áयच्छन् य्áद् अग्न्áये स्विष्टकृते ऽवद्य्áन्ति : य्áद् अग्न्áये स्विष्टकृते ऽवद्य्áति भागध्éयेनैव्á त्áद् रुद्र्áं स्áमर्धयति : सकृत्-सकृद् áवद्यति : सकृद् इव ह्í रुद्र्áस् : उत्तरार्ध्Áद् áवद्यत्य् : एष्Á व्áइ रुद्र्áस्य द्íक् : स्व्Áयाम् एव्á द्íशि रुद्र्áं निर्áवदयते : द्व्íर् अभ्íघारयति चतुरवत् : त्áस्याप्त्यै पश्áवो व्áइ प्Úर्वा Áहुतयस् : एष्á रुद्र्ó य्áद् अग्न्íस् : य्áत् पूर्वा Áहुतीर् अभ्í जुहुय्Áद् रुद्र्Áय पश्Úन् áपि दध्यात् : अपश्úर् य्áजमानः स्यात् : अतिह्Áय प्Úर्वा Áहुतीर् जुहोति पशून्Áं गोपीथ्Áय ॥]{style="color:#99cc00;"}

agni was in the other world, yama in this. The deva said: 'Come, let us interchange them'; with food the gods invited agni, with the kingdom the पितृ-s \[invited] yama; therefore is agni the food-eater of the gods, yama the king of the पितृ-s; he who knows thus obtains the kingdom and food. To him \[agni] they gave that share \[of the पुरोडाश cake], which they cut off for agni स्विष्टकृत्. In that he cuts off a share for agni स्विष्टकृत्, he offers rudra a share. He cuts off one by one \[of the two cakes in the offering], for indeed rudra is one. He cuts off \[the piece] from the northern half, for indeed this is rudra's direction; verily he pacifies rudra in his own direction. He sprinkles \[each cake] twice, to make \[the offering] partitioned into four. The fore-part of offerings [ i.e. पूर्व part as against the uttara part from which the cut for rudra is made] are cattle, agni is rudra here; if he were make the oblation with the fore-offerings, he would give the cattle to rudra. The ritualist would become bereft of cattle. \[Hence, the ritualist] offers leaving the fore-offerings aside, to protect the cattle.

The key points from the above ब्राह्मण passage that concern the ऋक् of haryata under discussion are the following:

 1.  agni was in the other world but he was exchanged for yama by the deva-s and brought to this world. This explains the "jane antar इच्छन्ति रुद्रं", i.e. the deva-s are seek in agni in this world, amidst men.

 2.  The deva-s do this to make agni their food-eater, i.e. eat the oblations of men. Hence, this explains the "गृभ्णन्ति जिह्वया sasam", i.e. the use agni as the tongue to eat the ritual fuel, which is their food. The ब्राह्मण adds that the ritualist who performs the rite knowing this becomes possessed of food.

 3.  Importantly, the ब्राह्मण explains that this form of agni, who is agni स्विष्टकृत्, is none other than rudra; this explains why in the ऋक् in discussion rudra is referred to. Thus, it is clear agni who becomes the tongue to provide food for the deva-s. Now this play on sasa as implying both the vegetable fuel as well as prepared ritual food is consistent with the offering under discussion. While the direct evidence for it is not apparent, one could potentially include soma, a plant, under what is implied by sasa.

One phrase that remains is the term "paro मनीषया", i.e. beyond the means of the mind". This is an importantcommon link to the shrauta and तान्त्रिक explanations of the mantra. We interpret it that rudra is beyond mental conceptualization -- he has to be directly consciously experienced in first person. This experience is the one which both the shrauta ritual and the तान्त्रिक साधन produce.

Turning to the तान्त्रिक explanation we have to first note that the cognates of vaidika ritual are internalized within the human body. Hence the "jane antar इच्छन्ति रुद्रं" is literally inside the body of the man. It is here that the deva-s are stationed. In this regard we might examine the opinion of the great तान्त्रिक abhinavagupta (तन्त्रालोक 12.6-7; let me repeat it here):\
[एवं विश्वाध्व संपूर्णं काल-व्यापार-चित्रितं ।\
देश-काल-मय-स्पन्द-सद्म देहं विलोकयेत् ।\
तथा विलोक्यमानो ऽसौ विश्वान्तर् देवता-मयः ।\
ध्येयः पूज्यश्च तर्प्यश्च तदाविष्टो विमुच्यते ।]{style="color:#99cc00;"} (तन्त्रालोक 12.6-7)

Thus, the body is perceived as full of diverse pathways, marked by time-dependent processes (lit. temporal processes), and as the seat of vibrations in space and time. The \[body] perceived thus is composed of all देवता-s and meditated, worshiped and pacified \[quenched with offerings]. He who enters that \[the body] is liberated.

Thus, the kaula vision can be interpreted as implying that all देवता-s are stationed within are are receiving their worship and importantly the quenching offerings right within the body itself. Hence, in the तान्त्रिक interpretation of this mantra the deva-s are experienced within the body itself, with all its physiological processes, receiving their oblations. To move further with तान्त्रिक interpretation let us examine the kaula practice of manthana as described by abhinavagupta by which the great energy arises which manifests as the three shakti-s of the trika tantra-s. The point of note is the initiation of this dhyAna laid out in the 5th chapter of the तन्त्रालोक (5.22-23):\
[सोम-सूर्यअग्नि-संघट्टं तत्र ध्यायेद् अनन्यधीः ।\
तद् ध्यानारणि-संक्षोभान्-महाभैरव-हव्यभुक् ॥\
हृदयाख्ये महाकुण्डे जाज्वलन् स्फीततां व्रजेत् ।\
तस्य शक्तिमतः स्फीत शक्तेर् भैरव-तेजसः ॥]{style="color:#99cc00;"}(तन्त्रालोक 5.22-23)

With an undistracted mind he meditates on the rubbing together of soma, sUrya and agni. By the friction of such a fire-drill \[the साधक] becomes the eater of the oblation of माहाभैरव in the great fire-altar in the form of the heart, blazing brightly, attains success. This success of his, possessed of mighty energy is the bhairava luster.

Thus, in the kaula meditation of the churning of the fire-drill of soma, sUrya and agni the heart is the ritual fire where in the साधक identified with महाभैरव consumes his oblation. This provides the link to the rudra within men who is the consumer of oblations in the ऋक्.

The reference to the tongue immediately reminds one of the famous yogic practice incorporated into the kaula system, namely the खेचरी mudra. To understand the connection the mantra under discussion we shall first see how the tongue is seen as the means of consuming the food/elixir of the deva-s. This is made rather clear in the yoga compendium of the medieval yogin स्वात्मारम in his हठयोग प्रदीपिक:

[गोमांसं भक्षयेन् नित्यं पिबेद् अमर-वारुणीम् ।\
कुलीनं तम् अहं मन्ये चेतरे कुल-घातकाः ॥\
go-शब्देनोदिता जिह्वा tat pravesho hi तालुनि |\
गो-मांस-भक्षणं तत् तु महा-पातक-नाशनम् ॥\
जिह्वा-प्रवेश-सम्भूत-वह्निनोत्पादितः खलु ।\
चन्द्रात् स्रवति यः सारः सा स्याद् अमर-वारुणी ॥]{style="color:#99cc00;"} HYP 3.47-49

He should constantly eat the meat of the cow and drink the beer of the immortals. I consider him to be a Kaula, while the others are destroyers of the kula. By the word cow the tongue is meant, which is inserted above the palate. Hence indeed the eating of beef destroys great sins. The tongue's insertion generates the fire verily \[causes] the juice to flows from the moon, which is the beer of the gods.

The points to note here are :1) the implicit identification of the tongue with the fire and 2) that the tongue is used to feed on the elixir of the deva-s that is allegorically referred to as beer. To see the connection between the खेचरी practice and rudra we must turn to the खेचरी-विद्या, a tantra of the kaula stream. This tantra repeatedly informs us the practice of खेचरी results in the identification of oneself with shiva (ख़्ह्V 2.7, 2.8, 2.12, 2.29). In the highest stage of खेचरी practice one performs the great meditation of the five-fold shiva in the center of the brain, the hypothalamus, known to the तान्त्रिक-s as the vajrakanda. The description of this meditation is rather complex and we shall lay it out as its described in the ख्V:

[अतः परं प्रवक्ष्यामि परामृत महापदम् ।\
वज्रकन्दं ललाटे तु प्रज्वलछ् छन्द्र संनिभम् ॥]{style="color:#99cc00;"}\
Now I shall teach regarding the great state of the ultimate ambrosia. The "diamond bulb" i.e. the hypothalamus in the forehead shines like the moon.

[लं गर्भं चतुरस्रं च तत्र देवः परः शिवः ।\
देवताः समुपासन्ते योगिनः शक्ति-संयुतम् ॥]{style="color:#99cc00;"}\
In it is the syllable laM in the center of a square मण्डल. The deity there is the paraH shiva. The gods and yogin-s worship him together with his shakti.

[चूलितले महादेवि लक्ष-सूर्य-समप्रभम् ।\
त्रिकोण-मण्डलं मध्ये देवं लिङ्गात्मकं शिवम् ॥]{style="color:#99cc00;"}\
At the चूलितला O महादेवि , bright as one hundred thousand suns is a triangular मण्डलं, in the middle of which is a shiva in the form of a लिङ्ग़

[रं गर्भ-मध्यमं देवि स्वशक्त्यालिङ्गितं परम् ।\
देवता-गण संजुष्टं भावयेत् परमेश्वरि ॥]{style="color:#99cc00;"}\
In its middle he visualizes the syllable raM, O goddess, with shiva embracing his shakti and accompanied by all the gods.

[दक्ष-शङ्खे महाभागे षड् बिन्दुवलयान्वितम् ।\
यं गर्भं धूम्र-वर्णं च तत्र देवं महेश्वरम् ॥]{style="color:#99cc00;"}\
In the right temple, O most fortunate goddess, is that which is encircled by six dots, containing the syllable yaM and smoke-coloured; there he should visualise, O goddess, the god maheshvara

[लिङ्गाकारं स्मरेद् देवि शक्ति-युक्तं गणावृतम् ।\
वामशङ्खे ऽर्धचन्द्राभं सपद्मं मण्डलं शिवे ॥]{style="color:#99cc00;"}\
In the form of a लिङ्ग together with his shakti and surrounded by his गण-s. In the left temple, O goddess, is a मण्डल, like a half--moon together with a lotus.

[वं गर्भं च दृढं मध्ये तत्र लिङ्गं सुधामयम् ।\
गोक्षीरधवलाकारं शरच्-चन्द्रायुत-प्रभम् ॥]{style="color:#99cc00;"}\
It contains the syllable वं its core and in the middle there is a firm लिङ्ग with nectar, as white as cow's milk and with the radiance of the autumn moon.

[स्वशक्ति-सहितं सर्वदेवता-गण-सेवितम् ।\
एवं देवि चतुर्दिक्षु स्थानान्य् उक्तानि वै मया ॥]{style="color:#99cc00;"}\
Together with his shakti he is served by the entire host of gods and goddesses. Thus have I described stations in the four directions, O goddess.

[तेषां मध्ये महावृत्तं हं गर्भं तत्र पार्वति ।\
परमेशः परः शम्भुः स्वशक्तिसहितः स्थितः ॥]{style="color:#99cc00;"}\
In the middle of them is a great circle, O पार्वति, which contains the syllable haM . There the supreme lord shambhu together with his shakti is stationed.

[लिङ्गाकारो गणयुतः सूर्य-कोटि-समप्रभः ।\
पृथिव्य्-अधिपतिर् भाले पश्चिमे सूर्य-नायकः ॥]{style="color:#99cc00;"}\
He is in the form of a लिङ्ग, together with गण-s, and is as bright as crores of suns. At the forehead is the lord of Earth, at the back of the head is the lord of fire.

[दक्षशङ्खे ऽनिलपतिर् वामे जलपतिः शिवे ।\
मध्ये व्योमाधिपः शम्भु स्थानाः पञ्च मयोदिताः ॥]{style="color:#99cc00;"}\
In the right temple is the lord of air, in the left is the lord of water, o goddess; In the middle is the lord of vacuum. Thus I have elucidated the five stations of shambhu .

[व्योमाधिपस्य देवस्य शिरोर्ध्वे चतुरङ्गुलम् ।\
ज्योतिर्-मण्डल-मध्यस्थं कोटि-छन्द्र-समप्रभम् ॥]{style="color:#99cc00;"}\
Above the head of the god, the lord of vacuum is a vessel full of the divine ambrosia, four fingers broad, in the middle of an orb of light, as bright as ten million moons.

[दिव्यामृत-मयं भाण्डं मूल-बन्ध-कपाटकम् ।\
ऊर्ध्व चन्द्रं महाशैलम् अभेद्यम् अमृतास्-पदम् ॥]{style="color:#99cc00;"}\
The pot filled with ambrosia with a door closing its base within a great rock with the moon above it, thus is the impenetrable seat of the ambrosia.

[शीतलामृत-मध्ये तु विलीनं लिङ्गम् ईश्वरि ।\
त्रसरेणु प्रतीकाशं कोटि-चन्द्र-समप्रभम् ॥]{style="color:#99cc00;"}\
Immersed in the cool अमृत is a लिङ्ग, O lady, like an atomic triad, as bright as crore moons

[हेयोपादेय-रहितम् अज्ञान-तिमिरापहम् ।\
अतीत्य पञ्च स्थानानि पर-तत्त्वोपलब्धये ॥]{style="color:#99cc00;"}\
Without the nature of mAyA dispelling destroying the darkness of ignorance he has to go beyond the five stations, in order to obtain the highest substance.

[परामृतघटाधार-कपाटं कुम्भकान्वितम् ।\
मनसा सह वागीशाम् ऊर्ध्ववक्त्रां प्रसारयेत् ॥]{style="color:#99cc00;"}\
\[To do so] holding the breath, he should extend his tongue \[allegorically described as the goddess of Speech with her mouth upwards] together with mind, to the door at the base of the pot of the highest ambrosia.

[निरुद्ध प्राण-संछारो योगी रसनयार्गलम् ।\
लीलयोद्घाटयेत् सत्यं संप्राप्य मनसा सह ॥]{style="color:#99cc00;"}\
Restraining his breath, truly attaining it along with his mind, the yogin should playfully open the bolt with his tongue.

[शीतलेक्षु-रस-स्वादु तत्र क्षीरामृतं हिमम् ।\
योगपानं पिबेद् योगी दुर्लभं विबुधैर् अपि ॥]{style="color:#99cc00;"}\
The yogin drinks the yoga-juice tasting like iced milk-effused sugar-cane juice, which is hard\
for even the gods to obtain.

[तत् सुधा तृप्ति संतृप्तः परावस्थाम् उपेत्य च ।\
उन्मन्या तत्र संयोगं लब्ध्वा ब्रह्माण्डकान्तरे ॥]{style="color:#99cc00;"}\
Quenched by the satisfaction of that nectar and having entered the supreme state,\
he should obtain union with the state of consciousness beyond the mind, within the skull \[identified with the celestial sphere].

[नाद-बिन्दु-मयं मांसं योगी योगेन भक्षयेत् ।\
एतद् रहस्यं देवेशि दुर्लभं परिकीर्तितम् ॥ख़्ह्V४९-६८]{style="color:#99cc00;"}\
The yogI thus eats by means of yoga, the meat consisting of नाद and bindu. This secret lore has thus been proclaimed, O mistress of gods.

Thus, the साधक who has identified with shiva meditating on the great five-headed shiva who is at the source of the ambrosial drink and meat of नाद and bindu consumes that from the कपाल with his tongue, feeding the hosts of gods in his body and attains conjunction with the state beyond the mind.


