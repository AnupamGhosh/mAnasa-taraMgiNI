
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The "left-handed" verse of महिषमर्दिनी](https://manasataramgini.wordpress.com/2010/04/23/the-%e2%80%9cleft-handed%e2%80%9d-verse-of-mahishamardini/){rel="bookmark"} {#the-left-handed-verse-of-महषमरदन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 23, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/04/23/the-%e2%80%9cleft-handed%e2%80%9d-verse-of-mahishamardini/ "6:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Kashmirian तान्त्रिक gonanda was in the lineage of a now largely lost lineage of उपासन of चण्डिका. His mantra-verse outlines the heart of this system:

[शूलाहत-महिषासुर-रुधिरच्-छुरिताधराम्बरा गौरी ।\
पुष्पवतीव स-लज्जा हसित-हर-निरीक्षिता जयति ॥]{style="color:#99cc00;"}

The साधन is only for the vIra -- a brave man of strong body like a great bowman or swordsman. The bold votary becomes the fierce rudra and his दूती bhavAnI. He who completes it dwells in the middle of the waves of spanda -- the crest on one side and the trough on the other. He himself is motionless in the middle of the heart, where चण्डिका, who is the rAja-हंसी moves restlessly generating the sense of multiplicity. He sings like the आचार्य भगीरथ had done:

[भव-जलधि-जलावलम्ब-यष्टिर् महिष-महासुर-शैल-वज्र-धारा ।\
हर-हृदय-तडाग-राज-हंसी दिशतु शिवं जगतश् चिरम् ॥]{style="color:#99cc00;"}\
A pillar of support in the waters of the ocean of existence; a shower of thunderbolts on the great mountainous asura महिष; a royal swan roving in the lake of hara's heart show weal to the world for ever.


