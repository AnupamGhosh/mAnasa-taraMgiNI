
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [tris-sapta-पञ्च-I](https://manasataramgini.wordpress.com/2010/07/21/tris-sapta-pancha-i/){rel="bookmark"} {#tris-sapta-पञच-i .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 21, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/07/21/tris-sapta-pancha-i/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh6.ggpht.com/_hjuA1bE0hBw/TDDXGPqBEBI/AAAAAAAABfc/Z70aE_bZX6M/s800/contextfree.jpg)
```{=latex}
\end{center}
```



