
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The दानवी mantra](https://manasataramgini.wordpress.com/2010/10/06/the-danavi-mantra/){rel="bookmark"} {#the-दनव-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 6, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/10/06/the-danavi-mantra/ "6:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There are two विद्या-s of विलिस्तेङ्गा. The first is the ओषधि prayoga which is known to the भार्गव-s as per the अथर्वण shruti (AV-vulgate 7.38.2). But this lore is only for women. The other is the celebrated दानवी or AsurI mantra which is known to a proper मन्त्रवादिन्. He may use it with just an oblation of ghee or with the secret plant depending on the circumstance:\
[या तिरश्ची निपद्यतेऽहं विधरणी इति । तां त्वा घृतस्य धारया यजे विलिस्तेङ्गाम् अहम् स्वाहा ॥ वसिष्ठायै स्वाहा । आसुर्यै स्वाहा । इन्द्रप्रियायै स्वाहा ॥]{style="color:#99cc00;"}\
He then stirs out of his ritual enclosure. If he sees a beautiful woman shortly there after it is a signal of his success.

If he wishes to perform the great prayoga he shall have 4 beautiful women as uttara-साधक-s. He shall first consecrate himself as per the received lore and then consecrate the uttara-साधक-s with the appropriate names. He shall hold a shara reed in his left hand offer with a darvi held in his right hand. He shall then deploy the below form of the विलिस्तेङ्गा विद्या:\
[या इन्द्रप्रिये तिरश्ची निपद्यतेऽहं विधरणी इति तां त्वा घृतस्य धारया यजे विलिस्तेङ्गां वसिष्ठां आसुरीम् मायाम् अहम् स्वाहा ॥]{style="color:#99cc00;"}\
Then he shall deploy aindra माधव mantra:\
[रूपं रूपं प्रतिरूपो बभूव तदस्य रूपं प्रतिचक्षणाय।]{style="color:#99cc00;"}\
[इन्द्रो मायाभिः पुरु-रूप ईयते युक्ता ह्यस्य हरयः शता दश ॥]{style="color:#99cc00;"}\
He shall offer after this verse with a: [फट् ।]{style="color:#99cc00;"}\
Then he shall intertwine the above form of the विलिस्तेङ्गा विद्या with the aindra collection known to the practitioners of the तैत्तिरीय shruti as atharvashiras. He shall offer oblations after each mantra of the atharvashiras with a: [फट् ।]{style="color:#99cc00;"} Then he shall deploy the mantra:\
[त्वं मायाभिर् अप मायिनोऽधमः स्वधाभिर्ये अधि शुप्ताव् अजुह्वत ।]{style="color:#99cc00;"}\
[त्वं पिप्रोर् नृमणः प्रारुजः पुरः प्र ऋजिश्वानं दस्यु-हत्येष्व् आविथ ॥]{style="color:#99cc00;"}\
He offers the shara reed with: [खट् फट् मृत् ॥]{style="color:#99cc00;"}\
Then he deploys that which the तैत्तिरीयक-s call the प्रत्यङ्गिरसं.


