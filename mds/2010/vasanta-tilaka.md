
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [vasanta-tilaka](https://manasataramgini.wordpress.com/2010/04/11/vasanta-tilaka/){rel="bookmark"} {#vasanta-tilaka .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 11, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/04/11/vasanta-tilaka/ "1:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![](https://i0.wp.com/lh5.ggpht.com/_hjuA1bE0hBw/S8Cr_DTAH9I/AAAAAAAABYU/sRPHO4Kbw2s/s800/march_april2.jpg){width="75%"}

[प्रफुल्ल चूताङ्कुर तीक्ष्ण सायको द्विरेफ माला विलसद् धनुर् गुणः ।\
मनांसि भेत्तुं सुरत प्रसङ्गिनां वसन्त योद्धा समुपागतः प्रिये ॥\
द्रुमाः सपुष्पाः सलिलं सपद्मं स्त्रियः सकामाः पवनः सुगन्धिः ।\
सुखाः प्रदोषा दिवसाश् च रम्याः सर्वं प्रिये चारुतरं वसन्ते ॥\
ईषत् तुषारैः कृत शीत हर्म्यः सुवासितं चारु-शिरश् च चम्पकैः ।\
कुर्वन्ति नार्यो ।अपि वसन्त-काले स्तनं सहारं कुसुमैर् मनोहरैः ॥\
वापी जलानां मणि-मेखलानां शशाङ्क-भासां प्रमदा-जनानाम् ।\
छूत द्रुमाणां कुसुमान्वितानां ददाति सौभाग्यम् अयं वसन्तः ॥]{style="color:#0000ff;"}

Post-cyanobacterial life is all about making light make more life. So indeed the yoni of the ऋतुसंहर are the cyanobacteria. We have become विद्वान्-s of new rahasya-s about them that none knew before us.


