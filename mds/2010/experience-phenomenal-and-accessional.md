
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Experience: phenomenal and accessional](https://manasataramgini.wordpress.com/2010/03/16/experience-phenomenal-and-accessional/){rel="bookmark"} {#experience-phenomenal-and-accessional .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 16, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/03/16/experience-phenomenal-and-accessional/ "6:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ST said that after seeing the presence of vaivasvata she wanted to widen her experience. She felt that by experiencing the existence of a diverse range of people and situations she will shake off the images of the महिष-वाहन who had passed by. She also felt that through these wanderings she will discover the core of her own self -- for she had always been a wanderer. She had mentioned these epic wanderings to the South to me but somehow I never got a chance to hear the full account of these wanderings. Each time we would get started we would be distracted by something else. On the occasion related in the earlier dispatch we got distracted by déjà vu and her lesser travels to the North. But this déjà vu thing was because she had that sense with respect to the wanderings of our dala in the महाराट्ट country. We pondered about the रमपूरीय असिपुत्रिका held by the feral ब्राह्मण of the race of the शाण्डिल्य-s and its three-stage expansion, our nearly forgotten former friend from magadha who wielded a nalika that had to be plied in a peculiar angle, and of Mis-creant's bouts of fastidiousnes :-). At the end of the day we were into the utility of ST's déjà vus for investigating cognition. Being taken in by this ST decided to make herself the subject of such a study at the same time determined that she should give me a detailed account of her great wandering in पाताल. So she decided to consult her extensive notes on the good times of the "past" when our dala had gone on the wanderings in which I had occasionally participated, then the bad times when chitragupta had made his presence felt and finally the great wandering.\
In paraphrase --\
ST: "In course of the desultory journey we reached that place in "पाताल" where the murderous म्लेच्छ-s had fed millions of कृष्णवर्ण-s and रुधिरवर्ण-s to mouth of मृत्यु from greed of rajata and yashada. Seeing that vast grimy slope which was like the face of काल itself we felt that there was nothing special about death."\
"At the point all the pleasure of काम, bhojana, उल्लास and हास्य lost their meaning."\
"Then we stood upon the peak of the ज्वालामुखि and suddenly the उल्लास of the experience of nature that we have often shared streamed forth."\
"I glanced back at my diary from the "good old days" of the wandering of our dala. In fact there were only complaints on most of the days -- Mis-creant was having a ball of a time, while we were suffering; the torments of विड्भुक् and sphigmukha; being away from home etc. But as the end was glorious -- so clean a sweep in the final battle that even Mis-creant was left watching with her jaw falling off -- that it was registered as the "good old days". Then I moved to the dairy of the bad times -- the record on most of the days was one of high exuberance till the last 25 days when we had our encounter with vaivasvata. The period is remembered as one of dissolution. Had my journey ended in the mine of death in पातल I may have slid from one trough to another. But then the vivid ending turned it into a great crest and a return of the exuberance."

"I introspected on this strange paradox and realized that there are two different types of first person experiences or consciousness: one which is the immediate one that depends on the sense organs and the other the past one that subsists on memory. The very same passage in life is recorded as very different, even diametrically opposite first person experiences."

"All this while I had never paid attention to what the big deal was with respect to consciousness. Suddenly I felt I should understand it. Many years ago I had got a book for cheap by authors known as Damasio (well-know experimental physicians), who claimed to explain consciousness. R who was around described it missing the point. R said that there is no point claiming that you have "explained" something when you more or less simply state that consciousness is consciousness. Taken in by her authoritative dismissal I cast the book aside. But going back I realized yes indeed they state: "Consciousness is an organism's awareness of its own self and its surroundings". The two words consciousness and awareness are merely different terms for the same thing. Now it appeared that even the definition was not clear of what this consciousness was. So I was not sure if my perception of two first person experiences -- the current and the past could be explained."

It was at this point that I gave ST the clue that there is no duality in consciousness -- there is just one and perceiving it can be tricky. Indeed among the म्लेच्छ-s over the past 30 years itself there have been numerous philosophers who have failed to even apprehend consciousness while supposedly working on it for their bread and butter.\
At the bottom of the ladder (in the sarva-darshana-संग्रह sense :-)) we have a fellow like Dennett who denies that consciousness needs explanation -- he has not even reached the level of the ताथागत-s. Beside him are the people like the Damasios who fail to even provide a definition for what they believe they are studying. We then have the Adarsha म्लेच्छ John Searle realizes the problem here are states that his fellow म्लेच्छ-s are wrong in denying it but then goes on to offer suggestions which at their best reach the पूर्वपक्ष presented in udayana's nyAya-कुसुमाञ्जलि (1.5) and वाचस्पति mishra's भामती (3.3.53). A similar situation is seen with the प्रेताचरिन् Koch and his friend the old Crick, and also the Hameroff/Penrose cluster. The only one who sees the issue in part is Chalmers, but it is ST's duality that illustrates the apparent confusion even in him.

This is not to say that the darshana भाष्यकार-s had all answers -- in fact there are deep problems with the vedAnta of भामति or the शारीरक भाष्य. But they had these problems over 1200 years ago, some which the म्लेच्छ-s are just now coming to. But again there are some brilliant insights of the nyAya-वैशेषिक-वादिन्-s like udayana (his explanations in NK 1.15, though he has his own set of problems elsewhere) which were available over 1000 years ago. It is by combining these insights with the missteps of वाचस्पति (i.e. the answer in भामती 3.3.53) along with an attempt to explain paradoxes like those ST perceived that we arrive near a more precise apprehension of the chit itself. It is from this manthana we shall move towards a navya-darshana.


