
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A discursion on the सनत्कुमारीय पञ्चब्रह्म (siddha-शंकर) tantra and the early evolution of the दक्षिणशैव system](https://manasataramgini.wordpress.com/2010/01/05/a-discursion-on-the-sanatkumariya-panchabrahma-siddha-shamkara-tantra-and-the-early-evolution-of-the-dakshinashaiva-system/){rel="bookmark"} {#a-discursion-on-the-सनतकमरय-पञचबरहम-siddha-शकर-tantra-and-the-early-evolution-of-the-दकषणशव-system .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 5, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/01/05/a-discursion-on-the-sanatkumariya-panchabrahma-siddha-shamkara-tantra-and-the-early-evolution-of-the-dakshinashaiva-system/ "7:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm5.static.flickr.com/4031/4259390457_2140b69189.jpg){width="75%"}
```{=latex}
\end{center}
```



**Diversity and syncretism in the bhairava or दक्षिण srotas**\
The bhairava-srotas of the shaiva मन्त्रमार्ग was clearly one of the most speciose branches of the shaiva-शासन. From the ancestral bhairava-srotas base that itself most probably arose from the कापालिक-oriented streams of the underlying shaiva अतिमार्ग the system spawned several distinctive branches: 1) An early branch was the दक्षिणशैव stream centered around the worship of स्वच्छन्द bhairava; 2) the system of कपालीश bhairava mainly taught in the archaic brahma-यामल (distinct from the later brahma यामल of the drAviDa country). The influence of the योगिनी-kula system, an early representative of which was the kaula-ज्ञान-निर्णय of matsyendra, resulted in several distinct kaula systems of the bhairava-srotas (these could be colloquially termed "shAkta" influenced): 3) the trika system originally with the triadic bhairava-s (ratishekhara, bhairava-सद्भाव, नवात्मन्) and their shakti-s (परापरा, parA and अपरा) or the bhairava शब्दराशि and his shaktI मालिनी. This is expounded in the tantra-s beginning with the सिद्धयोगेश्वरी-mata. 4) The कालीकुला or the krama system which is associated with tantra-s such as कलीकुल-पञ्चशतक and the mantra system of the early पाञ्चरात्र-influenced deity कालसंकर्षणी (who is one of the evolutes of the old एकानंशा). 5) The कुब्जिका-mata taught in the system stemming from the संवर्तामण्डल sUtra-s. This stream shared an early evolutionary history with the त्रिका system. 6) The श्रीकुल system which has many distinct varieties such as the nine नित्या-s and कामशिव, 11 विलासिनी-s, त्रिपुरभैरवी, bAlA, त्रिपुरसुन्दरी and the 16 नित्या-s and Ananda bhairava, कमेश्वरी and कामेश्वर. 7) In addition to these, the bhairava srotas underwent a syncretic development with a much earlier independent stream of the mantra-मार्ग, i.e. the worship of मृत्युञ्जय. This resulted in the system of अमृतेश्वर bhairava and अमृतेश्वरी which is expounded by the netra tantra. Also in this stream is the special worship of अमृतेश्वर and स्वच्छन्द bhairava (in one of two forms, sakala-भट्टारक or निष्कल-भट्टारक) with their shakti-s in the bhairava chakra-s drawn from the स्वच्छन्द system. Further, the netra-tantra has a secret chapter known as महाभिचार for the performance of fierce अभिचार rituals that is not present in the published version. This section of only taught within the tradition of the दीक्षित-s of the netra system and primarily concerns the homa-s to a combined form of अमृतेश्वर and स्वच्छन्द known as महाभैरव in the tradition. Other syncretic traditions of the bhairava srotas include: 8) the system of आकाश bhairava, which represents a combination of the bhairava srotas with the earlier garuDa-srotas and 9) the "extant" jayadratha यामल which presents itself as a version of the original शिरश्छेद that was transmitted as a combination of the वाम and bhairava streams (rather than the pure वाम system as taught in the वीणाशिखा tantra with tumburu and his 4 sisters, i.e. the ancient chatur-bhagini system). The voluminous yoga-वासिष्ठ also alludes to the combined worship of tumburu of the वाम-srotas along with bhairava in the midst of the मातॄ chakra with योगिनी-s like अलम्बुशा, suggesting that this वामा-bhairava syncretism was also once popular (nirvANa प्रकरण 18.26; chapter 18 has a description of shiva and the मातृका-s). Likewise, certain lineages of the bhairava-srotas, mainly that of स्वच्छन्द bhairava, also influence the Urdhvas-srotas or the siddhAnta tradition. For example, ईशानशिव, somashambhu and विश्वनाथ in their ritual manuals use the prescriptions of the स्वच्छन्द-bhairava tantra. 10) The raseshvara system is an offshoot of the older स्वचछन्द bhairava system that was centered on the tradition of rasa-siddhas. Its central deity rasabhairava and his shakti रसाञ्कुशी are emanations of स्वच्छन्द bhairava and his shakti अघोरेश्वरी. 11) Some texts like the brahma-यामल hold a view that there is an additionally अधः srotas which deal with the vaiShNava tantra-s pertaining to नृसिंह. Some of these appear to be influenced by the bhairava srotas via the more ancient rudra-नृसिंह equivalence. In another definition the अधः srotas is supposed to contain the tradition of हाटकेश्वर. A text called the आनन्दाधिकार tantra is said to be a part of this tradition by abhinavagupta but it does not apparently survive. This srotas seem to be a later addition to the original system, mainly within the traditions arising from the original bhairava srotas.

**The पीठ-s and links of the bhairava srotas to अतिमार्ग**\
Within the traditions stemming from the bhairava srotas, e.g. the jayadratha यामल, there is an internal classification that tries place its tantra-s in पीठ-s, which throws some light on the evolution of the system. The first पीठ is the mantra पीठ which primarily include tantra-s pertaining to the worship of the various forms of bhairava. This पीठ begins with the स्वच्छन्द-bhairava tantra. The विद्या पीठ is the next includes the यामल-s, सिद्धयोगेश्वरी-mata, योगिनी-जाल-शंबर, सर्ववीर, and समायोग among others. This set clearly concerns the early shaktI oriented tantra including those of trika and other early kaula works. The 3rd division is said to be the mudrA पीठ whose tantra-s are said to be those like हृद्भेद, मातृका-bheda and कुब्जिका-mata. Clearly, works like कुब्जिका-mata show a certain posterior temporal relationship to the early विद्यापीठ tantra-s suggesting that there might be an evolutionary stratification of the tantra-s in these three पीठ-s within the bhairava srotas. The 4th पीठ the मण्डल पीठ is not described in any detail and is mentioned as having the "shata-मण्डल" taught by पञ्चशेखर bhairava as its root sUtra but is largely found as a part of the other पीठ-s. In the exposition of the पीठ-s provided by the jayadratha यामल the first narrative provided by bhairava is that of the स्वच्छन्द-bhairava tantra. He states that the 8 bhairava-s taught this lore first to ananta. This was then taken up by the early teachers, namely श्रीकण्ठ and लकुलीश. लकुलीश transmitted it to vaidika ऋषि-s like वामदेव gautama and musalendra. His best student was musalendra who then transmitted his teaching further to the world. Thus of all the tantra-s within the bhairava srotas, it appears that स्वच्छन्द tradition is directly linked to the लकुलीश tradition of the अतिमार्ग of the shaiva-शासन. Similarly, in the lists of tantra-s belonging to each srotas in the shaiva tradition, the दक्षिणा srotas or that of bhairava usually begins with the tantra of स्वच्छन्द bhairava \[The only exception is the मृगेन्द्र of the Urdhva srotas that begins the list with असिताङ्ग; but we know from the jayadratha यामल account that the असिताङ्ग is an independent transmission of the स्वच्छन्द tradition]. From this we may infer that the स्वच्छन्द was one of the primary tantra-s of the दक्षिण srotas and that even the jayadratha यामल, which temporally followed the स्वच्छन्द, remembered a certain link it had with the underlying पाशुपत system of लकुलीश. Further, while one may interpret the links to the स्वच्छन्द bhairava tradition seen in the later siddhAnta ritual manuals as being a lateral influence, we must keep in mind that this influence was allowed in the first place due a certain core similarities between the two systems. These similarities are best interpreted as stemming from the common ancestor of the mantra-marga before its rapid radiation into the 5 srotas. This link also suggests a certain atavistic connection between the स्वच्छन्द and the origins of the mantra-marga tradition. The idea here is to further explore this link.\
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm5.static.flickr.com/4032/4264498146_71a8153b0a_o.jpg){width="75%"}
```{=latex}
\end{center}
```



**aghora and स्वच्छन्द**\
Now the bhairava srotas as whole is supposed to have emanated from the aghora face of rudra. The लिङ्ग पुराण 2.26 expounds the worship of aghora (अघोरार्चन vidhi) to be performed by a स्मार्त: The invocation of aghora might be done in a स्थण्डिल but performing it in a लिङ्ग is said to be crore times better (this preference is of course a peculiarity of the लिङ्गपुराण). It states that the ब्राह्मण after performing his bath and संध्या rituals dusts himself with ash from the agnihotra fire. He purifies himself with the shiva पञ्चाक्षरी and then performs अघोरार्चन. For this the mantra used is the brahma mantra or the बहुरूपी ऋक् as provided in the तैत्तिरीय AraNyaka. However, it is strange that this form of the mantra is given because later in the dhyana the 32-syllabled version, as given by the कौण्डिन्य recension of the पाशुपत sUtra, is alluded to. In any case the न्यास which follows does specifically consider the TA form rather than the 32 syllabled form: This suggests that dhyAna was perhaps simply adapted from a ritual text that originally used the 32-syllabed version.\
mantra:\
[अघोरेभ्यो ।अथ घोरेभ्यो घोरघोरतरेभ्यः ।\
शर्वेभ्यः सर्वशर्वेभ्यो नमस्ते अस्तु रुद्ररूपेभ्यः ॥]{style="color:#99cc00;"}\
न्यास:\
[अघोरेभ्यः प्रशान्त-हृदयाय नमः ।\
अथ घोरेभ्यः सर्वात्म ब्रह्म-शिरसे स्वाहा ॥\
घोर-घोरतरेभ्यः ज्वालामालिनी-शिखायै वषट् ।\
सर्वेभ्यः सर्व-शर्वेभ्यः पिङ्गल-कवचाय हुम् ॥\
नमस्ते अस्तु रुद्ररूपेभ्यः नेत्रत्रयाय वषट् ।\
सहस्राक्षाय दुर्भेदाय पाशुपतास्त्राय हुं फट् ॥]{style="color:#99cc00;"}\
similarly for अङ्ग-न्यास.\
The dhyana for aghora in this mantra runs thus:\
[वामादिभिश् च सहिते मनोन्मन्याप्य् अधिष्ठिते ।\
शिवासनेत्ममूर्तिस्थम् अक्षयाकार रूपिणं ॥\
अष्ट-त्रिंशत्-कला-देहं त्रि-तत्त्व-सहितं शिवम् ।\
अष्टादश-भुजं देवं गज-चर्मोत्तरीयकम् ॥\
सिंहाजिनांबर-धरम्-अघोरं परमेश्वरम् ।\
*द्वात्रिंशाक्षर-रूपेण* द्वात्रिंशच्-छक्तिभिर्-वृतम् ॥\
सर्वाभरण-संयुक्तं सर्व-देव-नमस्कृतम् ।\
कपाल-मालाभरणं सर्व-वृश्चिक-भूषणम् ॥\
पूर्णेन्दु-वदनं सौम्यं छन्द्र-कोटि समप्रभम् ।\
चन्द्र-रेखा-धरं शक्त्या सहितं नीलरूपिणम् ॥\
हस्ते खड्गं खेटकं पाशम्-एकेरत्नैश्-चित्रं चाङ्कुशं नागकक्षाम् ।\
शरसनं पाशुपतं तथास्त्रं मुण्डं च खट्वाङ्गम् अथापरे च ॥\
तन्त्रीं च घण्टां विपुलं च शूलं तथापरे डामरुकं च दिव्यम् ।\
वज्रं गदां टङ्कम् एकं च दीप्तं स मुद्गरं हस्तम् अथास्य शंभोः\
वरदाभय-हस्तं च वरेण्यं परमेश्वरम् ॥]{style="color:#99cc00;"}

This dhyana is more or less equivalent to the dhyana of स्वच्छन्द bhairava given in the eponymous tantra (2.82-2.93). In fact several of the words/phrases are identical between this part of the SVBT and the LiP. Importantly, aghora here shares with स्वच्छन्द bhairava specific features such as 18 hands and the attributes in each of them, the distinctive scorpion ornaments. The bodies of both are supposed to be composed of the 38 mantra fragments of the पञ्च-brahma mantra-s. Both are invoked in the respective texts using the aghora brahma mantra. But the aghora मन्त्रराज as it appears the SVBT terms is the exact अनुष्टुभ् of 32 syllables as mentioned in the dhyAna in the LiP 2.26. This clearly establishes स्वच्छन्द bhairava as an ectype of aghora, the deity of the बहुरूपी ऋक् of the old पाशुपत system. An iconic depiction of this deity in an ithyphallic form \[footnote 1], made by the चन्द्रात्रेय clan (chandella) from the core Khajuraho period, can be seen in the निलकण्ठ temple in Kalinjar fort in modern Uttar Pradesh.

This link to the aghora mantra (including its vaidika origins) is not just restricted to the स्वच्छन्द bhairava tantra, but remained in memory through the development of the bhairava srotas all the way to the कुब्जिका tantra-s. The कुब्जिकामत gives a detailed description of the aghora क्रिया in its chapters 8 and 9. In chapter 8 the aghora mantra is introduced as the अनुष्टुभ् mantra rAja. In the procedure described here the देवी कुब्जिका is first invoked in the yoni-मण्डल after digbandha etc and thereafter स्वच्छन्द bhairava is invoked as the शिखाशिव in the shikha of the goddess. The पुजा and homa of स्वच्छन्द bhairava is done using the mantra rAja. This is followed by the aghora digbandha, न्यास and प्राणायम. Then the साधक begins the kula meditation on the aghora mantra combined with 5 प्रणव-s on either side that stand for the 5 deities from सादाख्य (सदाशिव) to brahmA. The साधक conceives aghora अष्टाकपाल in the brahmarandhra and from the 32 syllables of the mantra emanate 32 देवी-s who are alluded to both in the LiP and the SVBT. This aghora surrounded by these 32 shakti-s who are worshipped as laid out in the कुब्जिका tradition, then showers nectar in every direction. This मण्डल of aghora is termed the संपूर्णमण्डल. Then the chapter 9 of the कुब्जिकामत explains the link between aghora and agni. This link is a reminiscence of the shrauta deployment of the aghora mantra in the मैत्रायणीय tradition, where it is used after the chiti has been piled for the agnichayana. This chapter expounds that the knower of the agni-aghora link and the worshiper of the शिखाशिव who has this awareness is the true आहिताग्नि (probably a कापालिक connection). Thus, it is clear that both the early सैद्धान्तिक and bhairava tantra-s deployed a पञ्चब्रह्म mantra (or mantra-s) as the central mantra-s in their system. This connection to the primary mantra-s of the underlying पाशुपत systems suggests that the proto-मन्त्रमार्ग was probably centered on specialized mantra deployments of the पञ्चब्रह्म \[Footnote 2]. Given that the बहुरूपी ऋक् additionally has the special position of being an early mantra within the पाशुपत system itself (found in the YV saMhitA-s and independently of the सद्योजात, वामदेव and Ishana mantra-s in the AV-परिशिष्ठ 40), it is quite possible that, from rather early on, within the पाशुपत system there was a distinct stream that focused on the aghora, from which the bhairava srotas emerged \[of course, even the degenerate modern aghori-s, who are largely only nAma-mAtra still deploy the aghora mantra].\
Another point is the worship of aghora in the form of a लिङ्ग that is laid out in the vidhi provided in the LiP. This is again reiterated in the context of the royal apotropaic ritual in LiP 2.50 \[Footnote 1] where aghora might be invoked either in the sacrificial fire or in a bANa-लिङ्ग stone. This invocation of the deity in the लिङ्ग appears to have been another feature shared by the siddhAnta tantra-s (very prominent) and the early bhairava srotas with ancestral ati-मार्ग \[the nidhanapataye etc mantra-s of लिञ्गस्थापन are originally ati-मार्ग mantra-s provided with the पञ्चब्रह्म in some interpolated transmissions of the तैत्तिरीय AraNyaka]. This feature is not entirely forgotten in later evolutes of the bhairava srotas. For example, कुब्जिकामत narrates the tale of the लिङ्ग manifestation of the bhairava as उद्यान bhairava right at the beginning of its frame story. In fact, there appears to have been a now lost bhairava tantra, the तिलकोद्यान bhairava tantra that appears to have dealt with the worship of the bhairava in the लिङ्ग.More generally, both the early siddhAnta and bhairava tantra-s share certain elements that are commonly not associated with the former. These include the yoni साधन with a beautiful दूती (e.g. described in the section 11.41 onwards in the large मतङ्गपारमेश्वर of the सैद्धान्तिक-s) and the crematoria rituals of the निःश्वास-saMhitA tattva section. These suggest that such features were already present in the ancestral mantra-मार्ग, which in turn developed these from the ati-मार्ग precurors.

**The सिद्धशंकर or पञ्चब्रह्म tantra**\
In the context of the early shaiva tantra-s containing साधन-s/prayoga-s deploying the पञ्चब्रह्म mantra-s the सिद्धशंकर or the सनत्कुमारीय पञ्चब्रह्म tantra is interesting. The text itself is a small and lacks the systematic structure typical of a typical large tantra such as स्वच्छन्दभैरव, मतङ्गपारमेश्वर or कुब्जिकामत. It also does not have the distinction of being mentioned (to my knowledge) in the commentaries of great authorities northern or southern. Its presentation is also not contiguous --- the initial part is presented as a teaching of सनत्कुमार transmitted by व्यास to the world. The later part is presented as a typical conversation between the bhairava and his shakti. These features would incline one to take it as a work of rather secondary significance. Yet, it has certain distinctive features that suggest, while it was forgotten and probably supplanted in the main bhairava srotas, it clearly preserves an early system of तान्त्रिक prayoga- based on the पञ्चब्रह्म mantra. The work as it survives comprises of 5 पटलस् and was recovered and edited by a तान्त्रिक श्यामसुन्दर तिवारी from Moradabad (Uttar Pradesh) from a fragment from Nepal and a manuscript from Varanasi (I have not been able to locate these in any catalog) in the late 1800s of CE. It is not clear if this is the entire text as it stands.\
पटल 1: पञ्चब्रह्माख्यान (व्यास transmitting सनत्कुमार); 45 verses\
पटल 2: पञ्चब्रह्म-विधान (begins with व्यास and moves to सनत्कुमार and then shiva); 33 verses\
पटल 3: tat-puruSha-विधान (शिवा to उमा); 16 verses\
पटल 4.1: aghora-kalpa (शिवा to उमा); 1-23 verses\
पटल 4.2: वामदेव-kalpa (shiva to उमा); 24-42 verses\
पटल 4.3: सद्योजात-kalpa (shiva to उमा); 43-55 verses\
पटल 4.4: Ishana-kalpa (shiva to उमा); 56-77 verses\
पटल 4.5: योगादि विचार(shiva to उमा); 78-124 verses\
पटल 5.1: पञ्चाङ्ग shuddhi, mantra-siddhy-उपाय (shiva to उमा); 1-14 verses\
This part has a verse from a certain कुलार्णव tantra that is a cognate of the first verse tagged on before the actual first verse of the text. At least I am not able to find this verse in the printed version of the कुलार्णव tantra \[Incidentally other citations to this tantra by श्रीविद्या तान्त्रिक-s are also not traceable. But some of them seem to be present in a text called कुलप्रदीप whose fragment श्री SDV kindly provided me]. It is not clear if श्री तिवारी did this or it was tagged by some exegete in a manuscript.\
पटल 5.2: siddhi-लक्षणं; 3 long verses.\
Total: 235 verses

The पटल 1 is presented as a transmission of the teaching of the सनत्कुमार by व्यास. Further, this पटल largely deals with the worship of aghora conjoined with the goddess अघोरेश्वरी or parA in a लिङ्ग or a स्थण्डिल or via a mediation of aghora in ones own body. This point is of interest because even the लिङ्गपुराण mentions that the worship of aghora was first taught by nandin to brahmA's son (typically सनत्कुमार) and then communicated by him to व्यास (LiP 2.49.16-17). Similarly, LiP chapters 2.49-50 deal with the worship of aghora in a लिङ्ग, स्थण्डिल or in oneself:\
[बाण-लिङ्गे ऽथवा वह्नौ दक्षिणामूर्तिम् आश्रितः ळिP २।५०।१४cद् ;\
अघोरार्चाविधानं च लिङ्गेवा स्थाण्डिले ऽपि वा ळिP २।२६।२६ cद् \~\
स्थण्डिले वाऽपि लिङ्गे वा पश्चाद् यजनम् आरभेत् । SSट् १।९;\
लिङ्गे वा स्थण्डिले वाऽपि सर्व-काम-फलप्रदं ।SSट् १।३७ अब्]{style="color:#99cc00;"}

[कालाग्नि-कोटि संकाशं स्वदेहम् अपि भावयेत् ळिP २।५०।१९cद् \~\
माहेश्वरं वपुः कृत्वा जटा-मुकुट-भूषणं... स्वशरीर ततः पूज्य पुष्प-धूप-विलेपनैः SSट् १।६&१।८]{style="color:#99cc00;"}\
Moreover, both these texts, like the earlier पाशुपत sUtra-s and AV-pari 40 पाशुपत vrata, use a phrase "[देवस्य दक्षिणमूर्तेः]{style="color:#99cc00;"}" (frequently encountered in the SST) e.g.: [देवस्य दक्षिणमूर्तेर् जपेद् आस्तम् अनादवेः । SSट्२।१६अब्]{style="color:#99cc00;"}\
This is taken to mean to the south side of the image of the deity, the लिङ्ग or the ritual fire. Given that the south face of the rudra is aghora it appears that it was an old shaiva practice to perform the rite facing aghora. This connection might explain the importance of the aghora tradition in the evolution of the shaiva-शासन. Further in the पाशुपत sUtra-s, the description of the anti-social practices is mainly described in the section associated with the aghora mantra. This might provide the old link between these practices and the socially transgressive trends, such as the crematoria rituals seen in the bhairava srotas (For example, note the ritual of the aghorI-मण्डल performed in the crematorium as per the brahma यामल).

Another point of interest is the position of the SST with respect to the beings such as डाकिनी-s, वेताल-s and विनायक-s:\
[ग्रह-वेताल-डाकिन्यो दुष्टश् चान्ये सहस्रसः ।\
प्रलयं यान्ति ते सर्वे मैत्रीम् आयन्ति मानवाः ॥ ष्ष्ट् १।३५]{style="color:#99cc00;"}\
Here the deployment of the स्वच्छन्द mantra is said to destroy evil beings like डाकिनी-s, graha-s and वेताल-s.\
Again we have:\
[शिवस्यैव हि निर्माल्यं सहस्रेणाभिमन्त्रितं ।\
धूपनं तेन रक्षांसि भूत-वेताल-सुग्रहाः ।\
विनायकास् तु डकिन्यो ज्वरअपस्मर संभवाः ।\
नश्यन्ति सकला रोगाः सत्यं सत्यं वदाम्यहं॥SSट् ४।११]{style="color:#99cc00;"}\
Here it is stated that the fumigation with the निर्माल्य of shiva on which the mantra of aghora has been chanted a 1000 times is verily a means to destroy रक्ष-s, भूत-s, वेताल-s, graha-s विनायक-s, डाकिनि-s and various diseases. This negative view of the डाकिनि-s and भूत-s presented by the SST is comparable to a similar negative view of them taken by the बोधायनोक्त महान्यास, a तान्त्रो-vedic ritual.\
In the लिङ्ग पुराण we have:\
[भूतैः प्रेतैः पिशाचैश्च डाकिनीभिश्च राक्षसैः ।\
संवृतं गजकृत्त्वा च सर्पभूषणभूषितम् ॥ २।५०।२४]{style="color:#99cc00;"}\
Here we note that भूत-s, preta-s, पिशाच-s, डाकिनि-s and rAkShasa-s are mentioned as being in the retinue surrounding aghora.

Thus, to us it appears that there is a more positive or rather accepting view of these beings in the latter, which is consistent with most of the latter tantra-s where the डकिनी-s in particular are seen as a form or agents of the great shakti. More tellingly, the SST has only a negative view on the विनायक-s, who by the time of most of the later tantra-s have been subsumed in the विनायक as a positive deity. We take this to mean that the core of the SST is actually a fairly early text from the early mantra मार्ग. In contrast to the above beings the यक्षिणी-s are viewed more acceptingly. The SST 3.2-3 describes a nocturnal ritual with the japa of the tatpuruSha mantra holding the ear of a goat performed to the south of a लिङ्ग in order to have a beautiful यक्षिणी manifest. Similarly, several other prayoga-s for the manifestation of यक्षिणी-s are given. These include one with the सद्योजात mantra that is used to manifest the famous वट-यक्षिणी (SST4.53).

In general terms the text includes several crematoria rituals, animal products and makes rather extensive use of bovine products. However, it does not prescribe purity-defiling prayoga-s --- those involving sexual practices and कुण्डगोल or mada. In fact it suggests purity and brahmacharya for the deshika and suggests purifications due to impurities from inappropriate food, contact with excreta etc. In this respect it is closer to the लाकुलीश-s and कालमुख-s and also the bulk of सैद्धान्तिक practice. Some prayoga-s of note include: 1) making of a collyrium for vanishing with a toad extract in a perforated skull while uttering the aghora mantra to the southern face of the image of the deity (SST 4.5-6). 2) The ritual of animation of the corpse: The तान्त्रिक translocates to a crematorium and collects a fresh corpse of a person who has not been injured or diseased. Then he washes the corpse with scents and installs it in a secret मण्डल. Then it is decorated with flowers and scents and covered with a white shroud. Then he performs an aghora japa placing his hand on the shroud and makes 800 oblations with the butter of a piebald cow. The corpse then rises up as a zombie and seeks the orders of the तान्त्रिक. Upon completing what ever he wishes it to do it falls lifeless again SST 4.12-16. 3) A homa with करवीर flowers dipped in ghee for the bewitching of women and also several other rites including some with bones of a vulture or preparation of guggulu pills and using them in a rite for attaining the siddhi-s. It should however be noted that an element of non-duality is hinted in the statement:\
[सर्वावस्था-गतो नित्यं यः पूजयति शङ्करं ।\
अपवित्रः पवित्रो वा स्वच्छन्दस् तेन कीर्तितः ॥ SSट् १।१४]{style="color:#99cc00;"}\
The one who irrespective of purity or lack there of in all states worships rudra is said to be the spontaneous worshiper.

Main mantra साधन of this text is that of aghora explicitly identified as स्वच्छन्द and that of अघोरेश्वरी explicitly identified as parA and is provided in the first पटल.The aghora mantra, as in the SVBT and the KMT is termed the mantra-rAja, right at the beginning of the text. The features of note are:

  - the न्यास is done in a simple ancestral shaiva fashion by appending the ह्रां, ह्रीं etc bIja-s with the fragments of the अनुष्टुभ् mantra [[अघोरेभ्यो ऽथ घोरेभ्यो घोरघोरतरेभ्यः । सर्वतः शर्वेभ्यो नमस्ते अस्तु रुद्ररूपेभ्यः ॥]{style="color:#99cc00;"}] thus: ह्रां+4 syllables+हृदय+नमः; ह्रूं+4 syllables+shiras+स्वाहा; ह्रीं+8 syllables+शिखा+वषट्; ह्रईं+8 syllables+kavacha+huM; ह्रौं+8 syllables+netra+वषट्; astra+फट्\
Then after invoking aghora in ones own self, one performs the aghora digbandha with fragments of the mantra.

  - The dhyAna of aghora is a simple one, unlike the 18-handed forms of the SVBT and the LiP:\
[कपाल-मालाभरणं चतुर्-भुजं वक्त्रेक्षणं शूलधाराभयप्रदं ।\
त्रिलोचनं खण्ड-शशाङ्कधारिणं ध्यायेद्धरं यः स लभेत ईप्सितं ॥]{style="color:#99cc00;"}

  - The invocation of \[a]घोरेश्वरी is performed in a हृल्लेख manifesting in the middle of aghora meditated upon in the heart. She is clearly identified with parA:\
[दण्डं शशिकरं चैव अङ्कुशं कर्त्तरी ध्वजौ ।\
सविसर्गा परा देवी अघोरा परिकीर्तिता ॥ ष्ष्ट्१।१४]{style="color:#99cc00;"}\
Her attributes mentioned here are somewhat different from the conventional ones associated with parA: a rod, a hook, battle scissors and flag with the classical lunar insignia.

  - The मूल mantra of अघोरेश्वरी is given as: [अघोरेशी ह्रीं हुं फट् ।]{style="color:#99cc00;"}\
A further mantra of her is also alluded to but no details are given of this mantra: [सौः अघोरेशी ह्रीं हुं फट् ।]{style="color:#99cc00;"}\
It states that if the mantra of (a)घोरेश्वरी is performed along with the mantra-rAja then the benefits are doubled. The core of these mantra-s have a certain relationship to one of the काण्ड of the 32-syllabled samaya-विद्या taught in the चिञ्चिनी-mata. They are also intimately related to the mantra-s of परापरा in the trika tantra-s. Thus, the shakti of aghora/स्वच्छन्द is clearly foundation from which the central shakti of both trika (parA; also परापारा based on the mantra) and of the पश्चिमांनाय (कुब्जिका) have emerged. This also seems to be the foundation of the goddess अघोरा described in the योगिनीsaMchAra tantra.

  - The text teaches a specialized mudra of घोरेश्वरी (SST 1.31-34) where the hands are folded into a complex pose and used to close the eyes. Then the text mentions that one must carry out chalana of the tongue. We take this to mean the performance of the churning as in the खेचरी practice.

In conclusion, we take the core of the SST to be a relatively old text of the early mantra मार्ग that lead to the bhairava srotas --- its primary deity and mantra is that of aghora though the remaining पञ्चब्रह्म are used. Given that the structure of the tantra often recapitulates its evolution, the incorporation of aghora/स्वच्छन्द in the पश्चिमांनाय is seen as coming from this original tradition. It is also important to note that while this text emphasizes the पञ्चब्रह्म just like the पाशुपत and सैद्धान्तिक works it also clearly introduces an early form of the shakti mantra and mudra emphasizes its importance. It is this shakti that was to develop extensively in the subsequent tantra-s of the विद्यापीठ within the bhairava srotas. Thus, we see the first 3 chapters and chapter 4 till the end of the ईशान kalpa as forming the archaic core of this text, but the terminal portion of chapter 4 and 5 could well be later accretions. In this context we should note that the brahma यामल states that the विद्यापीठ of the दक्षिण-srotas contains along with several bhairava tantra-s and यामल-s the following additional tantra-s: mantra-मालिनी, अघोरेशी*, aghoreshvara*, क्रीडाघोरेश्वरी*, लाकिनी-कल्पा, मरीची, महामरी, उग्रविद्यागण, two बहुरूपी-s* and अघोरास्त्र*. We posit that the one marked with a '*' were tantra-s specially pertaining to the old aghora and अघोरेशी mantra-traditions, which might have been related to the material presented briefly in the SST \[Footnote 3]. The अघोरास्त्र tantra most likely taught the famous astra mantra that is dedicated to aghora and is an अङ्ग of the mantra-rAja.

Footnote 1: The ithyphallic depiction of the deity is not mentioned in the classical visualizations of स्वच्छन्द or aghora. However, there is a possible allusion to this form in the royal apotropaic aghora ritual provided in LiP 2.50, which is prescribed for a kShatriya after the model of a ritual conceived by ushanas काव्य for the daitya हिरण्याक्ष. Here, the राजन्य is supposed to visualize aghora in his own body and in the dhyAna the term digambara etc might be suggestive of the ithyphallic form.

Footnote 2: We should note that in parallel the early पाञ्चारात्र tantra-s had a corresponding vaiShNava पञ्चब्रह्म. However, none of these vaiShNava पञ्चब्रह्म mantra-s are exactly found in any vedic text (of course the विष्णु gAyatrI is a late interpolation in some vedic material).Thus, the vaiShNava पञ्चब्रह्म was most possibly evolved to have a parallel for primary mantra-s i.e., the पञ्चब्रह्म, in the early shaiva tantra-s. One might also see a parallel between the aghora मन्त्रराज and the उग्रविष्णु मन्त्रराज (in fact, in some पाञ्चरात्रिक prayoga-s the संपुटिकरण of the two mantra-s is explicitly done).

Footnote 3: R1's father mentioned that there is a manuscript of a text termed the aghora-मूल-tantra survived in Nepal until recently that he suspects is indeed one of the root tantra-s of aghora from which the SST could be drawn. It could be aghoreshvara mentioned by the brahma-यामल.


