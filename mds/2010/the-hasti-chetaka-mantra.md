
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The hasti-चेटक mantra](https://manasataramgini.wordpress.com/2010/03/21/the-hasti-chetaka-mantra/){rel="bookmark"} {#the-hasti-चटक-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 21, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/03/21/the-hasti-chetaka-mantra/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh5.ggpht.com/_hjuA1bE0hBw/S3IzjqrsJyI/AAAAAAAABSg/D4JMAjdk5Po/s400/camera2.jpg){width="75%"}
```{=latex}
\end{center}
```



The fierce hasti-चेटक is animated by invoking the terrible विनायक in a charma of an animal offered as bali (see above) with the mantra:\
[ॐ ग्रीं ग्रूं हस्तिपिशाचाय नमः स्वाहा ।]{style="color:#99cc00;"}\
He makes oblations with the dried root of the पिशाच plant (*Valeriana jatamansi*) with the above mantra.


