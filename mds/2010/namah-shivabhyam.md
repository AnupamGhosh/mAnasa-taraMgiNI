
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नमः शिवाभ्यां](https://manasataramgini.wordpress.com/2010/01/15/namah-shivabhyam/){rel="bookmark"} {#नम-शवभय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 15, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/01/15/namah-shivabhyam/ "7:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm5.static.flickr.com/4072/4276187944_f1d9169b41.jpg){width="75%"}
```{=latex}
\end{center}
```



[उमायाः पतये छैव हिरण्यपतये सदा   ।\
हिरण्यबाहवे तुभ्यं वृषाङ्काय नमो नमः ॥]{style="color:#99cc00;"}

The image is supposedly from very close to the time of reign of राघवदेव who ousted the chIna-भोटादि from the land of himAlaya-s.  There is one element of this image that only the true knowers of the deva-s can comprehend.


