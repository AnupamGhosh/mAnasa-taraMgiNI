
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [यवनानां त्रिशिरास्-त्वाष्ट्र: Geryon Trikephalos](https://manasataramgini.wordpress.com/2010/05/25/yavananam-trishiras-tvashtra-geryon-trikephalos/){rel="bookmark"} {#यवनन-तरशरस-तवषटर-geryon-trikephalos .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 25, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/05/25/yavananam-trishiras-tvashtra-geryon-trikephalos/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh5.ggpht.com/_hjuA1bE0hBw/S_taiQjo67I/AAAAAAAABdA/ENMFjF92UOs/s800/herakles_geryon.jpg){width="75%"}
```{=latex}
\end{center}
```



A vase depicting the battle with Geryon
```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh4.ggpht.com/_hjuA1bE0hBw/S_taoHd8jpI/AAAAAAAABck/SaQtz2FbA_s/s800/Geryon_herakles1.jpg){width="75%"}
```{=latex}
\end{center}
```



A temple fragment from Cyprus showing the cattle raid on Geryon

RV 10.8.7-9\
[अस्य त्रितः क्रतुना वव्रे अन्तर् इछन् धीतिं पितुर् एवैः परस्य ।\
सछस्यमानः पित्रोर् उपस्थे जामि ब्रुवाणायुधानि वेति ॥]{style="color:#0000ff;"}\
trita, in the midst of the grotto, through his powers seeking to meditate on the primal father, concealed in his parents lap, addressing the weapons' kin, goes forth.

[स पित्र्याण्य् आयुधनि विद्वन् इन्द्रेषित आप्त्यो अभ्य् अयुध्यत् ।\
त्रिशीर्षाणं सप्त-रश्मिं जघन्वान् त्वाष्ट्रस्य चिन्निः ससृजे त्रितो गाः ॥]{style="color:#0000ff;"}\
Knowledgeable in the use of his father's weapons, the Aptya, impelled by Indra, gave battle. trita smote the three-headed seven-rayed त्वाष्ट्र, and released and led his cattle.

[भूरीद् इन्द्र उदिनक्षन्तम् ओजोऽवाभिनत् सत्पतिर् मन्यमानम् ।\
त्वाष्ट्रस्य चिद् विश्वरूपस्य गोनाम् आचक्रणस् त्रीणि शीर्षा परा वर्क् ॥]{style="color:#0000ff;"}\
Into many pieces, indra, the leader of true \[warriors], rent him apart, who sought power and was much regarded. Forcibly seizing विश्वरूप त्वाष्ट्र's cattle \[indra] hacked off his three heads.

From Hesiod's Theogony[![](https://i0.wp.com/lh4.ggpht.com/_hjuA1bE0hBw/S_tk96Wt2hI/AAAAAAAABdI/kgABKMdN-_s/s800/geryon.jpg){.aligncenter height="236" width="434"}](http://picasaweb.google.com/lh/photo/VpTLybXhhtskvuYXpZTT7w?feat=embedwebsite)

And Chrysaor, conjoining in coitus with Kallirhoe, the daughter of the glorious Oceanos, begot the three-headed Geryon, who was slain by Herakles's might beside his rolling-footed cattle in the sea-girt Erythea on the day when he \[Herakles and his assistant \~ trita] drove the broad-browed cattle to holy Tiryns, after he crossed over the strait of the Ocean and killed Orthus (\~वृत्र) and Eurythion the guardian of the cattle in the dark cave (=stable) beyond the glorious Oceanos.

While reams can be written on this proto-Indoeuropean myth we will restrict ourselves to one philological speculation. The name of the tricephalos in yavana world is Geryon or Geryoneus. It is connected to the root Geros in Greek which is the ortholog of गिरः in Sanskrit -- both mean a chant or a recitation. Another similar sounding root is Gera in Greek which is the cognate of jara in Sanskrit -- both mean decrepitude (e.g. as in geriatric). We believe that the name Geryon is from the former and there by retains an ancient memory of त्रिशिरास् त्वाष्ट्र being a priest or one who recites chants. In one of the Indo-Aryan versions of the myth while posing as a hotar of the deva-s he used one of his three heads to recite mantra-s to the deva-s, the second to recite to the asura-s and with the third he drank soma. The great indra having heard him recite to the asura-s severed his three heads. Like विश्वरूप being the son of an asura woman, Geryon is the son of Kallirhoe, a Titanic female. The RV version of the myth preserves core archaic element of the myth -- the cattle raid of indra along with an assistant trita. A Greek commentator, probably Apollodorus, remarks that Herakles split mountains to create the straits of Gibraltar as he proceeded to raid the cattle of Geryon. This splitting of the mountain is again the transfer of an old myth of the indra-like deity (splitter of mountains) to his earthly representative Herakles. Then he was attacked by Orthrus whom he smashes with his uagros -- again a transfer of the smashing of वृत्र with the vajra by indra. What the above artistic depictions of the Greek myth show is that Herakles was accompanied by an assistant, just like trita in the vaidika version. In the Indo-Aryan world too there was a projection of the same myth on to the human realm, albeit without much elaboration. One of the रक्षस् slain by rAma दाशरथि, the earthly manifestation of indra, was trishiras --- a straightforward mapping of the shruti in the इतिहास.


