
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Tritonal recitations of vaiShNava-s](https://manasataramgini.wordpress.com/2010/07/06/tritonal-recitations-of-vaishnava-s/){rel="bookmark"} {#tritonal-recitations-of-vaishnava-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 6, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/07/06/tritonal-recitations-of-vaishnava-s/ "7:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

KRK astutely observed that the tritonal recitation of the नालायिर दिव्यप्रबन्धं (DP4000) by the श्रीवैष्णव-s might be a survival of an even earlier custom of the tritonal recitation of the पाञ्चरात्र texts. The evidence from the आगमडम्बर (AD) of jayanta भट्ट in support of this seems rather clear. In the AD an orthodox स्मार्त ritualist severely condemns the vaiShNava-s and shaiva-s in a conversation with a स्मार्त teacher. In the 4th act of the play the ऋत्विक् states:\
[यद् अमी पाञ्चरात्रिका भागवता ब्राह्मनवद् व्यवहरन्ति । ब्राह्मण समाज मध्यम् अनुप्रविश्य निर्विशङ्कम् अभिवादय इति जल्पन्ते ।विशिष्ट स्वर वर्णानुपूर्वीकतया वेद-पाठम् अनुसरन्त इव पञ्चरात्र ग्रन्थम् अधीयते। "ब्राह्मणाः स्मः" इत्य् आत्मानं व्यपदिशन्ति व्यपदेशयन्ति च ।]{style="color:#0000ff;"} \[The attack on vaiShNava-s]\
[शैवाद्यस् तु न चतुर्वर्ण्य मध्य पतिताः श्रुति स्मृति विहितम् आश्रमम् अवजहतः शासनान्तर परिग्रहेणान्यथा वर्तन्ते । एते पुनर् "आ जन्मन आ सन्ततेर् ब्राह्मणा एव वयम्" इति ब्रुवानास् तथैव चतुराश्रम्यम् अनुकुर्वन्तीति महद् दुःखम् ॥]{style="color:#0000ff;"}\[Attack on shaiva-s]

In his attack on the पाञ्चरात्रिक vaiShNava-s the स्मार्त states that they move around in the midst of ब्राह्मण-s using the same अभिवादनं as them. They are also said to recite their पञ्चरात्र texts with the special svara-s as thought they are a veda recitation. The call themselves ब्राह्मण-s and expect others to do so likewise. With respect to the shaiva-s he says that they fall outside the varNAshrama dharma and follow their own authoritative texts (he means the shaiva tantra-s) and claim they are ब्राह्मण by birth and lineage. They imitate the four-fold Ashrama-s (or does he include वर्ण in this?) of the स्मार्त-s.

So it appears that KRK is indeed correct in observing that tritonal recitation of the DP4000 in imitation of the veda is likely to have been further based on the model of the recitation of the पाञ्चरात्र texts. In this light, we should note that certain Kashmirian पाञ्चरात्रिक-s who were temporally relatively close to jayanta भट्ट (e.g. the great क्षेमेन्द्र) mentioned that they were practitioners of the एकायन shAkhA (of the shukla yajurveda), which is believed to be the root of the पाञ्चरात्रिक texts. So was this supposed recitation of पाञ्चरात्र texts in imitation of the veda actually a hold over from an older "एकायन shAkhA"? One line of reasoning could be: given that the एकायन is mentioned in the latest layers of the Vedic texts (namely the teaching of skanda in the चान्दोग्य उपनिषद्), it is possible that a body of early vaiShNava material under this name existed in that period and it could have been indeed accented. A model for such accented early vaiShNava material could be the वैखानस mantra-पाठ which includes several accented vaiShNAva mantra-s (including the famous आत्मसूक्त which have described on these pages) affiliated with the तैत्तिरीय shAkhA of the कृष्ण yajurveda. Likewise, the तैत्तिरीय AraNyaka and AV auxiliary literature include a vaiShNava text, the नारायण सूक्त (valli), which is recited in an accented form, though TS version has words with atypical accents. It is possible that the shukla yajurvedin-s had a vaiShNava movement parallel to the KYV वैखानस-s, known as the एकायन-s, who were the precursors of one of the strands of the later पाञ्चरात्र. Of course, it should be noted that एकायन is not mentioned in any of the चरण lists as a distinct shukla यजुर्वेदीय shakha-s. Furthermore, within the पाञ्चरात्र tantra-s themselves, though the एकायन is mentioned as a collection of यजुष्-s, the SYV shakhA with which they are associated is explicitly given as काण्व or in rare cases as also including the माध्यन्दिन-s:\
सात्वत 25.16:\
[एकायनान् यजुर्मयान् आश्रावितम् अन्तरं ।]{style="color:#0000ff;"} (एकायन as a collection of यजुष्-s)

जयाख्य saMhitA 1.109 (manuscript):\
[काण्वी शाखाम् अधीयानाव् औपगायन-कौशिकौ ।]{style="color:#0000ff;"}

Ishvara saMhitA 21.554:

[काण्वी शाखाम् आधीयानान् वेद-वेदान्त-पारगान् ।\
सस्कृत्य दीक्षया सम्यक् सात्वताद्युक्त-मार्गतः ॥]{style="color:#0000ff;"}

पाद्म saMhitA चर्यापाद 21.4:\
[औपगायन पूर्वासते नानागोत्रा मुमुक्षवः ।\
अधीयानाः कण्व-शाखा तथा माध्यन्दिनाह्वयम् ॥]{style="color:#0000ff;"}\
Also of note is the fact that the पाद्म saMhitA places its frame story in the Ashrama of कण्व and the text as a lecture delivered by कण्व.

Hence, the एकायन collection was probably a mantra collection somewhat like the वैखानस mantra पाठ rather than being a shAkhA by itself. As noted before on these pages, in support of this we find that the महाभारत mentions a पाञ्च्रात्र system of the एकायन type that certainly preceded the full-blown तान्त्रिक version of पाञ्चरात्र. This Mbh section alludes to a महोपनिषद् of these पाञ्चरात्रिक-s that could have been from the एकायन collection (perhaps a parallel to the नारायण valli). This vaiShNava section of the Mbh mentions the certain early vaiShNava groups like एकान्तिक-s and chitra-शिखण्डिन्-s. Now some पाञ्चरात्र tantra-s (e.g. विष्णु saMhitA, 2nd पटल) mention the types of vaiShNava-s to be: 1) वैखानस-s, 2) shikhin-s, 3) एकान्तिन्-s and 4) सात्त्वत-s, 5) मूलक-s. Of these, the वैखानस-s are the equivalents of their modern versions.  The shikhin-s are the चित्रशिखण्डिन्-s and एकान्तिन्-s the followers of the एकान्तिक dharma of the Mbh -- they were vaiShNAva equivalents of the shaiva अतिमार्ग ascetics.

Based on the evidence from the महाभारत we hold that the सात्वत-s are the vaiShNava-s who originated the chatur-व्यूह system and were associated with the सात्वत confederacy -- the deification of their heroes कृष्ण and balabhadra or their identification with a 4-fold vaiShNava pantheon was probably at the heart of this system. We also hold that the bhagavad-गीत is a text originally belonging to the early सात्वत-s that acquired a wider currency in जम्बुद्वीप. Now the मूलक-s are likely to be the proto-पाञ्चरात्रिक-s who worshiped विष्णु independently of the व्यूह-s. Now, we should note in this regard that some early पाञ्चरात्र tantra-s mention the vaiShNAva पञ्चब्रह्म which clearly points to an alternative to the व्यूह-style of worship. The long vaiShNava interpolation in the ऋग्विधान also appears to be the work of relatively early vaiShNava-s possibly belonging to this vaiShNava system. It is not impossible that the एकायन was originally prevalent among these vaiShNava-s. In this regard, as we pointed out before, the very name पञ्चरात्र is not understood in most of the later Tantric saMhitA-s of this system. In fact they give different "folk" etymologies to try to explain this name. However, it should be noted that this term is more naturally explained as a deriving from an ancient Vedic ritualist term i.e. the 5 nights rite. Indeed, the पञ्चरात्र of puruSha नारायण is currently known from the shukla yajurveda tradition (shatapatha ब्राह्मण). It is likely that this was indeed the origin of the term. Subsequently, the मूलक-s and सात्वत-s appear to have amalgamated to give rise to the classical पाञ्चरात्र system.

Hence, while the evidence is wholly circumstantial it is possible that the later पाञ्चरात्र recitation might have itself followed this early recitation pattern of the accented एकायन material.

We may point out that some vaiShNava mantra-s recited by संकर्षण in the आगमडम्बर in the famous रणस्वामिन् temple in Kashmir are found with some variations in the appendices of a medieval shukla यजुर्वेदीय paddhati in the context of विष्णु worship:\
[नमः क्रम समाक्रान्त छित्र त्रैलोक्य सद्मने ।\
कुक्षि-कोणैक देशाम्श-लीन विश्वाय विष्णवे ॥]{style="color:#0000ff;"}

On philological grounds we can rule out that this mantra was early: it has the word "koNa" which is a loan into Sanskrit from technical Greek (xenolog of Greek gona; ortholog of Sanskrit जानु). So the mantra has to be post-Hellenic, but the link between the SYV and the पाञ्चरात्रिक-s might be real.


