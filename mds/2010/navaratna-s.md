
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [navaratna-s](https://manasataramgini.wordpress.com/2010/03/23/navaratna-s/){rel="bookmark"} {#navaratna-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 23, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/03/23/navaratna-s/ "5:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Hindu lore has it that there are nine gems. For example shaiva tantra-s mention these as the material from which लिङ्ग-s are made:\
[माणिक्य-मुक्ते त्वथ वज्र-नीलौ वैडूर्य संज्ञं हरिताश्मना च ।\
तत् पुष्यरागं स्फटिकं प्रवालं नवैव रत्नानि भवन्त्य् अमूनी ॥]{style="color:#99cc00;"}\
माणिक्य: ruby; mukta: pearl; vajra: diamond; नील: sapphire; वईडूर्य: beryl; हरिताश्मन (marakata): emerald; पुष्यराग: topaz; स्फटिक: quartz; प्रवाल: coral.

Some Indic words for gems have traveled far: Beryl comes from the Prakritic form of वैडूर्य (veruliya) and smaragdus (emerald) from a Dravidianized form of marakata via a Semitic intermediate. Other such lateral transfers into Middle Eastern languages are also a possibility. But what is the etymology of वैडूर्य itself ?

The main investigation in this regard is that of AK Biswas, who adduces several explanations for the origin the names of the beryl family of minerals. However, his arguments are to me not entirely supported, suggesting that the matter is not yet resolved.\
Biswas posits that the Prakritic veruliya is a loan from Dravidian. He suggests that the Dravidian precursor was actually a place name like Belur or Velur, which were beryl processing centers. He suggests that पतञ्जलि's etymology that वैडूर्य comes from the place where it was cut supports the origin from Velur or Belur. While superficial this Dravidian theory sounds nice it actually has several problems on scrutiny.

continued ...


