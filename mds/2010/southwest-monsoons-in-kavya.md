
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Southwest monsoons in काव्य](https://manasataramgini.wordpress.com/2010/06/15/southwest-monsoons-in-kavya/){rel="bookmark"} {#southwest-monsoons-in-कवय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 15, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/06/15/southwest-monsoons-in-kavya/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The life line of bhArata, the southwest monsoon wind, appears to have been an object of attention of the Hindu poets over the ages.

One example presented by विद्याकर, whose author remains unknown to me, goes thus:\
[वाति व्यस्त-लवङ्ग-लोध्र-लवली-कुञ्जः करञ्ज-द्रुमान् आधुन्वन्न् उपभुक्त मुक्त मुरला तोयोर्मि-माला-जडः ।\
स्वैरं दक्षिण-सिन्धु-कूल-कदली-कच्छोपकण्ठोद्भवः कावेरी-तट-ताडि-ताडन-तटत्कारोत्तरो मारुतः ॥]{style="color:#99cc00;"}

While the smooth and delicate alliterative effects of this verse are not easily rendered from the देवभाष, one may roughly see it thus:\
The southwest monsoon wind blowing from the waves of the ocean of the मुरला land (northern Kerala) which,  it has tasted and abandoned,  shakes up the trees of clove, Symplocos racemosa, star fruit and Millettia pinnata beans. The wind born in the southern sea slopes with banana groves blows at will rustling and beating the palms on the banks of the कावेरी.

Several poets resort to evoking शृङ्गार as the primary rasa as they describe the southwest monsoon winds. One example is a poem of unknown authorship recorded by विद्याकर:\
[भुक्त्वा चिरं दक्षिण-दिग्-वधूम् इमां विहाय तस्या भयतः शनैः शनैः ।\
स-गन्ध-सारादि-कृताङ्ग-भूषणः प्रयात्य् उदीचीं दयिताम् इवानिलः ॥]{style="color:#99cc00;"}

Here the southwest monsoon wind is described as having the southern direction as a wife whom he has enjoyed for long. He then silently steals away from her with his body laden with the perfumes of the south to the northern direction which is like his lover (दयिता).

Yet other poets work the classification of women of the subcontinent typical of the कामशास्त्र into this expression of the शृङ्गार rasa. Two of the most famous of these are of the bauddha vasukalpa and the shaiva श्रीकण्ठ-shiva.\
vasukalpa's verse:\
[अन्ध्री-नीरन्ध्र-पीन-स्तन-तट-लुठनायासमन्द-प्रचाराश् चारून्-नुल्लासयन्तो द्रविड-वर-वधू-हारि-धम्मिल्ल-भारान् ।\
जिघ्रन्तः सिंहलीनां मुख-कमल-मलं केरलीनां कपोलं चुंबन्तो वान्ति मन्दं मलय-परिमला वायवो दाक्षिणात्याः ॥]{style="color:#99cc00;"}

It states that the fragrance bearing winds blowing from the south roll and slow down after striking the surfaces of the firm and closely set breasts of the andhra girls, then they delight among heavy flower-decked hairdo of the wives of the Tamil country, then they smell the fresheners in the lotus mouths of the girls of श्री लन्का and finally kiss the cheeks of the girls of kerala.

श्रीकण्ठ's verse:\
[कान्ता-कर्षण-लोल-केरल-वधू-धम्मिल्ल-मल्ली-रजश् चौराश् चोड-नितम्बिनी-स्तन-तटे निष्पन्दताम् आगताः ।\
रेवा-शीकर-धारिणो ऽन्ध्र-मुरल-स्त्री-मान-मुद्राभिदः वाता वान्ति नवीन-कोकिल-वधू-हूङ्कार-वाचालिताः ॥]{style="color:#99cc00;"}

Here the southwest monsoon winds are described as stealing the jamine pollen from the hairdo of the kerala wifes which have been loosened by the pulling by their paramours. Then the winds grow motionless upon colliding with the breasts of the women of the Tamil country endowed with good hips. Then they are said to bear the streams of water from the रेवा peak and remove the self-consciousness of the women of the andhra and murala countries, shrieking like a young female cuckoo.

All these are good examples of tropes working शृङ्गार into the description seasonal and weather patterns, a common theme in Hindu poetry though they could offend the western-conditioned modern Indian mind.


