
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [ज्येष्ठा in शिवालय-s](https://manasataramgini.wordpress.com/2010/06/23/jyeshtha-in-shivalaya-s/){rel="bookmark"} {#जयषठ-in-शवलय-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 23, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/06/23/jyeshtha-in-shivalaya-s/ "6:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some of the major सैद्धान्तिक प्रतिष्ठ tantra-s give an account of the installation of अलक्ष्मी or ज्येष्ठा along with her daughter rakta-ज्येष्ठा or मान्या and her son वृष who in shaiva tradition is bull-headed. Unfortunately, this section has not been completely preserved in one of the most authoritative प्रतिष्ठ tantra-s of south India, the ajita-महातन्त्र. So we do not have the origin mythology for ज्येष्ठा unlike that provided for विनायक, skanda and others. From the suprabheda, कारण and extant स्थापक manuals we can infer that the mantra-s of these deities are stereotypic in the manner of other मूल mantra-s given in the सैद्धान्तिक स्थापन vidhi-s:\
[ॐ ज्यें ज्येष्ठायै नमः । ॐ मं मान्यायै नमः । ॐ वृं वृषाय नमः ।]{style="color:#99cc00;"}\
The gAyatrI of only ज्येष्ठा is extant:\
[ॐ फलप्रदायै विद्महे । पापहन्त्र्यै च धीमहि । तन् नो ज्येष्ठा प्रचोदयात् ॥]{style="color:#99cc00;"}\
However, the स्थापन procedure in the ajita declares that there were gayatrI-s for वृष and मान्या which were supposed to be deployed in their पञ्च-gavya baths. In the centre अलक्ष्मी is placed, her daughter to her left and to her right her son. In water pots her attendants are worshiped around her: tama, mohA, क्षुधा, निद्रा, मृत्यु, mAyA, jarA and भया successively with sandal paste, flowers and the like. Within an exclusive ज्येष्ठा temple in 8 pots the deshika should install 8 fierce shakti-s around the central triad: कराली, कपिलाक्षी, विमला, विभुजा, भास्वरा, वितता, कीकसा and kadru from east to north-east. Then images of a crow and a vessel are placed in a large pot in the vedi for ज्येष्ठ and her children. In a few south Indian temples one can still see this triad, though they have been largely forgotten amongst the lay. However, their invocation continues in certain syncretic traditions of धूमावती among the दशमहाविद्या-s.


