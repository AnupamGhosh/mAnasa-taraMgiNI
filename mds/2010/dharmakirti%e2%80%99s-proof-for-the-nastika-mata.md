
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [धर्मकीर्ति's proof for the nAstika mata](https://manasataramgini.wordpress.com/2010/04/22/dharmakirti%e2%80%99s-proof-for-the-nastika-mata/){rel="bookmark"} {#धरमकरतs-proof-for-the-nastika-mata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 22, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/04/22/dharmakirti%e2%80%99s-proof-for-the-nastika-mata/ "1:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

That learned पाषण्ड among the ब्राह्मण-s, आचार्य धर्मकीर्ति, is said to have once enjoyed the beauty of a woman. His protégé asked him if he was ensnared by the wonderfully unattainable object of his vision. He remarked that he was struck not just by the pleasure of his visual sensation but by the vindication of the nAstika mata. How so his protégé asked. He responded with this ornate verse:\
[याता लोचन-गोचरं यदि विधेर् एणेक्षणा सुन्दरी नेयं कुङ्कुम-पङ्क-पिञ्जर-मुखी तेनोज्झिता स्यात् क्षणं ।\
नाप्य् आमीलित-लोचनस्य रचनाद् रूपं भवेद् ईदृशं तस्मात् सर्वम् अकर्तृकं जगद् इदं श्रेयो मतं सौगतम् ॥]{style="color:#99cc00;"}\
Roughly: If this doe-eyed beauty, with a golden face and vermillion mark had come in the range of vision of the creator then he would not let her go from his sight even for a second;\
If he had closed his eyes then he could not have created a form with such features; hence this entirely world is uncreated; the saugata-mata is superior.

While belonging to the पाषण्ड camp it must be admitted that धर्मकीर्ति was a formidable opponent of the Astika-s and made some genuine contributions to our larger philosophical sphere.

Legend has it that the great पाणिनि similarly encountered a great स्त्री. When asked what he saw, his expression was one of the pure rasa, with no philosophical undercurrents. But then पणिनि's words are for another day.


