
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The culmination of the तान्त्रिक education of an आथर्वण](https://manasataramgini.wordpress.com/2010/01/22/the-culmination-of-the-tantrika-education-of-an-atharvana/){rel="bookmark"} {#the-culmination-of-the-तनतरक-education-of-an-आथरवण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 22, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/01/22/the-culmination-of-the-tantrika-education-of-an-atharvana/ "8:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After having changed his यज्ञोपवीत, the dvija who has studied the shruti of the भृगु-s and the अङ्गिरस् goes to the preceptor to unlock the great secret of the tantra. He recites the त्रिषप्तीय which is the foundation of all bIja-s and in the morning recites the सूक्त of the मैत्रावरुणि beginning with "प्रातर् अग्निं pratar इन्द्रं...". He then invokes the उषा-s as ordained in the tradition of the atharvan-s and performs an Achamana. Then he invokes रात्री. Having done this he prepares for the worship of the glorious कुब्जीशन who is in the embrace of mahA-प्रचण्ड-कुब्जिका-प्रत्यङ्गिरा with the highly secret mantra known as the raudra-atharvashiras, the hidden secret crest of नीलरुद्र, only passed by the anugraha of shiva to the student. He who knows this mantra is considered the master of the atharvan lore. His preceptor then unfolds before him the two supreme chakra-s known as the mahA-कुब्जीशान मण्डल, which is more complex in its construction than even the श्री-chakra, and the mahA-प्रत्यङ्गिरा chakra.

Then the preceptor asks him to recite the कुब्जिका सूक्त:\
[शन्नो देवी...अभि स्रवन्तु नः ॥]{style="color:#99cc00;"}

[कुब्जिका परमेष्ठिनी वाग्देवी ब्रह्म-संश्रिता ।\
ययैव ससृजे घोरं तयैव शान्तिर् अस्तु नः ॥\
इदं यत् परमेष्ठिनं मनो वां ब्रह्म-संश्रितं ।\
येनैव ससृजे घोरं तेनैव शान्तिर् अस्तु नः ॥\
इमानि यानि पञ्चेन्द्रियानि मनः षष्ठानि मे हृदि ब्रह्मण संशृतानि ।\
यैरेव ससृजे घोरं तैरेव शान्तिर् अस्तु नः ॥\
अष्टाविंशानि शिवानि शग्मानि सह योगं भजन्तु मे ।\
योगं प्रपद्ये क्षेमं च क्षेमं प्रपद्ये योगं च नमो ऽहोरात्राभ्याम् अस्तु ॥]{style="color:#99cc00;"}

Then he goes to the कर्णमोट tree, the kula वृक्ष, and recites the माधवी विद्या to worship कुब्जिका there in:\
[मधुमन् मूलं मधुमद् अग्रमासां मधुमन् मध्यं वीरुधां बभूव ।\
मधुमत् पर्णं मधुमत् पुष्पमासाम् मधोः संभक्ता अमृतं घृतम् अन्नं दुहुतां कुब्जिकात्मकं ॥]{style="color:#99cc00;"}

Then he focuses on his breath and invokes विष्णु there in and कुब्जिका emerging as the power of his mAyA:\
[अजपा वैष्णवी माया जप्येन साजिता परा ।\
कुब्जिका ब्रह्मविद्या सा या वेदैर् उपगीयते ॥\
अनाख्येयम् इदं गुह्यं योगी-केश-समं निभं ।\
हंसस्य गतिविस्तारं भुक्ति-मुक्ति-फल-प्रदं ॥]{style="color:#99cc00;"}

Then he perceives his oneness with indra, विष्णु, prajApati and the whole universe and utters:\
[त्वम् इन्द्रस् त्वं महेन्द्रस् त्वं लोकस् त्वं प्रजापतिः ।\
तुभ्यं यज्ञो विजायते तुभ्यं जुह्वति जुह्वतस् तवेद् विष्णो बहुधा वीर्याणि ॥]{style="color:#99cc00;"}\
Then the preceptor gives him the two कूट-s and decodes them for him:\
[प्रानेशः सुग्रीवः संवर्तको वैकुण्ठो धरा त्रिधातुर् वह्निर् व्यापको महाविद्येश्वरी संयुतं ।]{style="color:#99cc00;"}This is the कूट of कुब्जीशान.\
[सहजः परमात्मा नृसिंहं कालरुद्रं खड्गी शङ्करं वह्निवर्णं चांउण्डा तूर्य-स्वर-भूषितं नाद-बिन्दु-युतं ।]{style="color:#99cc00;"} This is the कूट of महाप्रत्यङ्गिरा.

Then he gives him the mantra weapon with which he battles अभिचार:\
[चक्षुषो हेते मनसो हेते ब्रह्मणो हेते तपसश्-च हेते ॥]{style="color:#99cc00;"}\
He also gives him the ब्रह्मण parimara mantra for the same purpose.\
Then he gives him the para-काय-प्रवेशण mantra of the भृगु-s:\
[तं त्वा प्रपद्ये तं त्वा प्रविशामि सर्वगुः सर्व-पुरुषः सर्वात्मा सर्व-तनूः सह यन्मे ऽस्ति तेन ॥]{style="color:#99cc00;"}\
If appropriately initiated he can even animate a dreadful वेताल with this mantra.\
Then he gives him the mantra of अथर्वण-bhadra-काली:\
[काली कराली छ मनोजवा छ सुलोहिता या छ सुधूम्रवर्णा ।\
स्फुलिङ्गिनी विश्वरुची च देवी लेलायमाना ॥]{style="color:#99cc00;"}

Then he gives him the पूर्ण-दीक्ष with all the प्रत्यङ्गिरा mantra-s at pinnacle including the 9-syllabled mantra सिद्धिलक्ष्मी and the deva न्यास.

\[While not imparted to ब्राह्मण-s today, the final दीक्ष in this series technically is the cremation ground ritual along with the दूती. One should impart to the दूती the secret न्यास and take a war drum and go to the श्मशान. There with the crematory fire one makes oblations to प्रत्यङ्गिरा with atharvanic mantra-s. Then facing west he and the दूती draw the प्रत्यङ्गिरा bhairava-chakra of the 8 special bhairava-s of this system, with avicephalic and therocephalic forms. In the middle they install प्रत्यङ्गिरा and then enjoying the high rati of maithuna they invoke the great goddess with her secret mantra with the 4 special bhakti-s. Then he recites प्रत्यङ्गिरा सूक्त. With this he becomes the master of the प्रत्यङ्गिरा lore --- even if धूमावती or बगलामुखी were deployed against him he will back-hurl them.]


