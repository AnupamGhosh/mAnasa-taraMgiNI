
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The gold rush](https://manasataramgini.wordpress.com/2010/08/13/the-gold-rush/){rel="bookmark"} {#the-gold-rush .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 13, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/08/13/the-gold-rush/ "5:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh5.ggpht.com/_hjuA1bE0hBw/TGTOou1ufaI/AAAAAAAAB4Y/8-L9l7qrWAM/s400/julia001.jpg){width="75%"}
```{=latex}
\end{center}
```

\
It is every true man's dream that he may find a nugget of true gold as this, though most are destined only to possess pyrites or a virtual dream as this.

