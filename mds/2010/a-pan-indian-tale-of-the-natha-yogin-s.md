
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A pan-Indian tale of the नाथ yogin-s](https://manasataramgini.wordpress.com/2010/02/10/a-pan-indian-tale-of-the-natha-yogin-s/){rel="bookmark"} {#a-pan-indian-tale-of-the-नथ-yogin-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 10, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/02/10/a-pan-indian-tale-of-the-natha-yogin-s/ "7:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The lore of the नाथ siddha-s is of pan-Indian distribution. Its colloquial forms are fast evolving like genetic loci involved in host-parasite interactions. Yet, these colloquial forms of the tradition have such a grip on the lay masses that in many cases one could hardly believe that in its original form this system was not exactly practiced as it is today. The roots of the system deep in the shaiva-शासन, particularly the kaula tradition, need a more detailed exposition to be properly elucidated. But for now we will note a tale of the नाथ-s that I suspect may have acquired pan-Indian distribution but I have no clear way to establish this.

The story of the story begins long back: In the year before first great war, Mis-creant and I on our gardabha-s descended down the sloping road that curled below the hill of चण्डिका and made our way towards the महिष-hrada. There was a temple of the three-headed one that I used to visit. So tying our gardabha to a post I decided to enter the temple. Curious, Mis-creant also came along. I used to be tremendously fascinated by a painting in that chaitya that displayed the great मच्छन्द vibhu along with his successors headed by जालन्धर and गोरक्ष, and also had the दत्तात्रेय manu inscribed on it. At that point I had not known Mis-creant to have any kind of शास्त्र-ज्ञान -- I was not even aware that she was a female version of the "चुपा-rustam". So I was surprised when she named those siddha-s, albeit using their Hindustani names. For me they stood as the link to the पश्चिमांनाय -- a connection that only the insiders know. I was also aware of their presence in the colloquial cults of महाराट्ट, drAviDa and Andhra countries. But I was not much aware of the colloquial links to the north. So I quizzed Mis-creant about what she knew -- she was rather terse and taciturn then. But it gave me a hint. Today hemmed in by the hima-pralaya we had a little chat when she narrated an interesting tale that she swore was a native one. I remembered reading a close variant of it in महारट्ठी pamphlet picked up near one of the नाथ shrines. I am also aware in translation of a similar work in the वङ्ग भाष written by a number of वङ्ग-s, including a तुरुष्क convert to the dharma Shaikh Faizullah. A narrative of this form might be seen in:

 1.  The महाराट्ठी pamphlet named nav-नाथ्-kathA most likely of very modern provenance based on an earlier yogi-संप्रदायाविष्कृति.\
Medieval to early modern:

 2.  Any of the several गोरक्ष-vijaya-s from the वङ्ग country. The currently accessible ones are those by कबिराज् भींअसेन्, विद्यापति and Faizullah.

 3.  mIna-chetana of श्यमादस् sen also from the वङ्ग country.

 4.  gopichandra-nATaka from Nepal.\
Of uncertain provenance:

 5.  The Tamil chittar mahimai also a pamphlet-like publication.

 6.  R's narrative apparently from a version of the tale floating around in the Punjab and the Himachal.\
I follow the महाराट्ठी in large part.

In the beginning there was only शूण्यत. From this manifested a bubble that was the हिरण्यगर्भ. In that हिरण्यगर्भ the unmanifest puruSha manifests as आदिनाथ. आदिनाथ performs tapasya for an enormous length of time. Thereafter from him emerged a देवी named केतकी \[Footnote 1]. From her mouth arose brahmA, from her crest arose विष्णु and from her womb arose rudra. The three deities were taking a stroll along the celestial गङ्गा when they encountered a corpse on its banks. The corpse was actually आदिनाथ who had placed himself there for the emanation of the माहायोगिन्-s. The gods look at the corpse -- विष्णु and brahmA declare it is defiling and suggest that they leave it alone and move on. However, rudra declared that no corpse should be left uncremated and that he would perform the task of cremation. For this purpose विष्णु becomes the firewood; rudra hauls the corpse who is आदिनाथ, places him atop the pyre and set is alight with the brahmA in the form of the funerary fire. As the corpse is burning away from its from its navel first arose matsyendra, from its जट emerged गोरक्ष (colloquially gorakh), from its bones emerged जालन्धर (hence, he is also called colloquially as हड्डिपाद; हड्डि is bone in महाराट्ठी; he is हाडीपा in the वङ्ग version), from the limbs emerged चतुरङ्ग and from the ears emerged कृष्ण (some times called काणेरीनाथ in the colloquial tradition and conflated with another siddha Aryadeva. Hence, we have the folk etymology kAn -- ear for his origins. In the वङ्ग version he is called कान्ह-pA) \[Footnote 2]. Upon his emergence चतुरङ्ग (colloquially चौरङ्गी) ascends to found the siddha-loka from where he would return later to perform magic on earth as the son of the legendary Andhra king शालिवाहन. Then rudra takes केतकी as his wife and the four emergent नाथ-s who are left behind are taken as their children. Of them, matsyendra acquired the mantra and yoga शास्त्र-s first by hiding in a fish and hearing their transmission in the conversations between shiva and केतकी. He became the guru of जलान्धर, who in turn initiates गोरक्ष who further transmitted them to the others. Then केतकी, also called दुर्गा or गौरी, tests their yoga-निष्ठ by attempting to delude them. All of them, except गोरक्ष who sees her as his mother, fail the test and are variously cursed: matsyendra was to lose his interest in performing the feats of yoga and instead retire to a grove of banana plants, where he was to enjoy dalliance with 1600 beautiful women. कृष्ण was exiled to wander amongst म्लेच्छ-s and other unclean tribes (the वङ्ग version says to a country called Dahuka identified by some as श्री-लन्का). जालन्धर right away became a sweeper who had to clean the royal toilets. But all of them were to recover their yogic splendor under after undergoing tribulations. In their recovery गोरक्ष was to be central hero and other great yoga practitioners both male and female were to emerge in the process. For his yoga-निष्ठ गोरक्ष was rewarded with a son who supposed to have been born out the water wrung out of his कौपीन. This son was शर्वरिनाथ (more commonly called चर्पटि or चर्बरिपा in some eastern sources). R mentions that a लिङ्ग supposedly installed by him is particularly famous in Himachal not far from her ancestral regions. He was later to attain the state of complete yoga mastery.

Then the narratives diverge somewhat but their central theme in each case revolves around the magical feats of गोरक्ष. All of them showcase one of these as his major feat -- the retrieval of matsyendra from the grip of the women in the banana grove. Under the curse of केतकी देवी, matsyendra is lost in the कदली-vana. He takes the Amazonian queen of कदली, kamalA, as his partner and becomes the king of that land. Due to the outward flow of his retas he loses his yogic immortality and is sliding downwards towards death. This is perceived by कृष्ण-नाथ who conveys it to गोरक्ष. Seeing that matsyendra is just 3 days away from death, गोरक्ष tries to rush into the banana-grove. But he is prevented from entering the pleasure garden where matsyendra is in dalliance. So he assumes the form of a female dancer and goes in. There he sees matsyendra lost in maithuna. There, in the form of the dancer गोरक्ष sounds his Damaru and matsyendra comes out of his delusion partially. He finds something familiar in the song being sung by गोरक्ष, who is urging him to regain his yogic body and release himself to the path of immortality. Then गोरक्ष kills his son बिन्दुनाथ who was born from his dalliance with kamalA and hangs his corpse from a hook. Still in the grip of his delusion matsyendra and kamalA lament the death of बिन्दुनाथ. But गोरक्ष restores 108 बिन्दुनाथ-s. At this point matsyendra regains of his own self and remembering his yogic powers leaves the banana-grove with गोरक्ष. The women of the grove try to attack गोरक्ष but he turns them into birds and they fly away.

A key feature of these tales is the ascendancy of गोरक्ष. While all the accounts accept गोरक्ष to be a disciple of matsyendra, the latter is clearly downgraded and needs the superior yogin गोरक्ष to save him. Thus, they retain the memory of the old order but give गोरक्ष the edge. Moreover, by placing the simultaneous origins of the major नाथ-s close to that of the world itself they to a degree neutralize the chronological and preceptor-pupil hierarchy between the नाथ-s. We suspect that these features found in the above narrative record (in the language of myth) a major evolutionary shift in the नाथ system even as it branched out of its old roots in the shaiva शासन to disperse throughout India into a series of cults that assimilated all manner of influences -- a process that is ongoing to this date. This mythic narrative is reflected to a degree in the textual material pertaining to the नाथ-s. Most of the work attributed to matsyendra is in संस्कृत (e.g. kaula-ज्ञान निर्णाय). In the case of गोरक्ष he has a few major संस्कृत works to his name but the bulk of the material attributed to him is अपभ्रंश poetry collected by HP Dvivedi in his comprehensive work नाथ् संप्रदाय्. In contrast, matsyendra has at best a rare अपभ्रंश poem attributed to him in Dvivedi's collection. Further, in the earliest sources that mention any नाथ-s (e.g. trika tantra-s of the kaula stream) we find matsyendra with his wife कुम्कुमांबा and their several sons as the founders of the tAntra-शास्त्र in this yuga. Consistent with this abhinavagupta invokes only matsyendra in his exposition of trika and he is placed next to बटुक in the northern door of the siddha-chakra of trika. A similar siddha-chakra with matsyendra is used in रसायन system of the kaula marga as expounded in its central work the रसार्णव tantra. His successors like गोरक्ष start coming to fore in the रसायन stream as siddha-s, but here again they are not accorded any primacy relative to matsyendra.

The ordering with a primal matsyendra is also supported by the nAstika-s of Tibet. A Tibetan work called the grub thob, which is a translation of the work on siddha-s by abhayadatta from the 1000s of CE gives the following names:\
The list begins with matsyendra. Others of note are: गोरक्ष (9); चौरङ्गि (10); कान्ह (17)/कर्णरि (18); जालन्धर (46); चर्पटि (64); kanthali (71); कपाल (72);\
These are mixed along with various authors of bauddha mantra prescriptions collected in the साधनमाला and composers of certain चर्यागीति-s. The fact that some of the nAstika siddha-s in the list were composers of चर्यागीति-s in an Eastern अपभ्रंश suggests the अपभ्रंश works collected by Dvivedi might have a kernel of truth in being associated with नाथ-s, albeit in considerably mutated form due to prolonged oral transmission. But we do not find most of our Astika नाथ-s as authors of any nAstika mantra-साधन-s. So it is quite likely that they were purely Astika figures from the shaiva शासन who were absorbed by the nAstika-s rather than the other way around. The catalog of the material in the Tanjur produced by the misguided communist scholar Rahula Sankrityayan suggests they did have Tibetan translations of the अपभ्रंश गीति-s of गोरक्ष, चौरङ्गि and चर्पटि. It would be of interest to examine if these are homologous to those collected by Dvivedi. In the हठयोग प्रदीपिक we see a similar list with matsyendra as the first siddha after आदिनाथ, who is shiva. But the remaining order is slightly different with गोरक्ष coming down the line.In both the bauddha and the हठयोग-प्रदीपिक list गोरक्ष does not come immediately after matsyendra but he is preceded by certain number of separating siddha-s (e.g. he is 7th in HP list with आदिनाथ as 1st). Even if the siddha-s are apocryphal this separation between matsyendra and गोरक्ष is also corroborated by the Tamil chittar mahimai where matsyamuni (matsyendra) and korakkar (गोरक्ष) are separated by at least 2 siddha-s. Thus it appears that the weight of the evidence is in favor of them not originally being a teacher-student pair. There is another enigmatic नाथ called mIna who is often placed before गोरक्ष in several traditions throughout India, including the HP. mIna is distinct from matsyendra. In some traditions he is called matsyendra's son. Historically, he might have been the real teacher of गोरक्ष who was eventually conflated with the older matsyendra. Subsequently, गोरक्ष came to be listed as the student of the conflated mIna-matsyendra. This mIna-गोरक्ष conflation seems to be the case in the रसायन work known as the Anandakanda. Based on all this it appears that गोरक्ष was a siddha in the hoary tradition of matsyendra but coming much after him. He probably did compose in northern desha-भाष-s and consequently came to be more widely known across society than the founder matysendra whose real works were only known to erudite तान्त्रिक-s. Consequently, गोरक्ष appears to have enjoyed much greater folk popularity throughout India.

But the above story that we narrated explicitly tries to present गोरक्ष as superseding his guru matysendra, who is condemned as being lost in amorous pursuits. We believe that this is not a coincidence at all, but a reflection of an internal revolution going on within the kaula tradition. To understand this we have turn to the definitive kaula tantra dealing with रसायन, namely the रसार्णव. It opens thus:\
[देव-देव महादेव काल कामाङ्ग-दाहक ।\
कुल-कौल-महाकौल- सिद्धकौलादि-शासन ॥]{style="color:#99cc00;"}

Here उमा begins to request shiva to discourse on रसायन. She calls him the instructor of the kaula streams including siddhakaula that matsyendra explicitly identifies himself with. The text also has the siddha-chakra which follows the पूर्वांनाय (i.e. trika) pattern.\
Then we interestingly see the following (RA1.9-12):\
[पिण्डपाते च यो मोक्षः स च मोक्षो निरर्थकः ।\
पिण्डे तु पतिते देवि गर्दभो ।अपि विमुछ्यते ॥\
yadi muktir bhaga क्षोभे kiM na मुञ्cअन्ति गर्दभाः |\
अजाश्-च वृषभाश्-चैव किं न मुक्ता गणाम्बिके ॥\
तस्मात् संरक्षयेत् पिण्डं रसैश्-cऐव रसायनैः ।\
शुक्र-मूत्र-पुरीषाणां यदि मुक्तिर् निषेवणात् ॥\
किं न मुक्ता महादेवि श्वान-शूकर-जातयः ।\
षड्दर्शने ऽपि मुक्तिस् तु दर्शिता पिण्डपातने ॥\
करामलकवत् सापि प्रत्यक्षं नोपलभ्यते ।]{style="color:#99cc00;"}

Here shiva clarifies that the mokSha that comes when the corporeal frame falls off (as indicated by the 6 darshana-s; RA 1.12) is not mokSha at all. Even asses get that mokSha. It should be apparent like an amlaka in the hand. He then condemns the practices such as excitement of the bhaga, and the use of विण्-मूत्रादि as paths of mokSha -- verily bulls, goats, asses, dogs and swine will get their mokSha by such means. Instead he says the body should be preserved with the rasa-s of रसायन.

Also:\
[मद्य-मांस-रता नित्यं भग-लिङ्गेषु ये रताः ।\
तेषां विनष्ट-बुद्धीनां रसज्ञानं सुदुर्लभम् ॥]{style="color:#99cc00;"}

Thus, shiva states that the fools who indulge in the pleasures of intoxicants, meat and sex are not destined for attaining knowledge of chemistry.

He then states (RA 1.20):\
[ज्ञानान् मोक्षः सुरेशानि ज्ञानं पवन-धारणात् ।\
तत्र देवि स्थिरं पिण्डं यत्र स्थैर्ये रसः प्रभुः ॥]{style="color:#99cc00;"}

Through ज्ञान one attains mokSha and for ज्ञान one needs to hold the prANa-s (i.e. pavana or the yogic practice of प्राणधारण). For that one needs a firm frame and this firmness is achieved through the substances prepared by रसायन.

Thus, what we see in this central tantra of the रसायन stream, which explicitly identifies itself with the kaula tradition of matsyendra, is a conscious dissociation from the "makara-based" rituals and an emphasis on the development of the body through yoga and रसायन. While the RA covers the latter, the हठयोग facet of this process is emphasized in the हठयोग प्रदीपिक, which is another offshoot of the same tradition. However, the connection to the old kaula tradition with its sexual ritual was not completely lost despite its condemnation in the RA. The practice of रसायन necessarily needs a female partner (e.g. the काकिनी) without whom there is no success. She is the equivalent of the दूती in the older kaula system. While we have no explicit evidence that गोरक्ष was behind this new emphasis, we know on the evidence of the रसार्णव that certainly it happened within the tradition. As गोरक्ष was the more recent नाथ of the tradition, and also one who had a wide popular following, he was probably presented as the upholder of this revised yogic tradition. On the other hand matsyendra being the older founder of the system was clearly associated with the ancestral elements such as the sexual ritual of the old kaula-s. Hence, in the above story he is shown as engaging in debauchery.

Thus, within the रसायन and हठयोग tradition we believe that there was an internal transformation of the kaula system. However, in the case of the श्रीकुल we have evidence suggesting that the parallel movement away from the "makara-based" ritual happened due to extrinsic interactions with the स्मार्त supporters of the शङ्कर tradition. Interestingly, after this transformation, we see that the नाथ system started devolving into various local "miracle" siddha cults along with numerous syncretic interactions.

Footnote 1: केतकी is an unusual name because it is the word for the flower that is specifically proscribed in the worship of rudra. Originally rudra used to wear this flower on his head. When rudra manifested as the लिङ्गोद्भव मूर्ति the flower betrayed the identity of rudra to brahmA. So it was condemned by rudra and not used in his worship.

Footnote 2: Note the reappearance of the vaidika mythic motif of the origin of the भृगु-s and the अङ्गिर-s from fire in which the semen of prajApati slain by rudra was burnt.


