
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Three of the heroes had fallen](https://manasataramgini.wordpress.com/2010/02/15/three-of-the-heroes-had-fallen/){rel="bookmark"} {#three-of-the-heroes-had-fallen .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 15, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/02/15/three-of-the-heroes-had-fallen/ "7:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

He had become a powerful yogin but had not acquired ज्ञान. He had acquired ज्ञान but lacked the power of yoga. So they wandered the earth never reaching their desired destination. But the third one, who only observes, said none of this matters; it is only contingent on fate whether one reaches the destination. The fourth asked what is fate? The third one answered it is not what people normally know by that word. Look in why the one with yoga had no ज्ञान and the one with ज्ञान had no yoga.

From the high perch we beheld the path which the third hero had led us to in the days of yore. Nowadays the third hero was shy to talk of any such thing from the past. When we reminded him of it he felt great discomfort. Only the 2nd and the 4th stood there on the perch as they beheld the path along which we had ascended to the skies in the former yuga. The inum narrated his dreadful tale -- it was one like the last wick of भैरवानन्द. The inum in his bitterness said that none of the former heroes mattered. But we said that the whole picture should be unemotionally viewed. First hero was the first to venture on that path. He stumbled and fell with the wheel on his head. After him the second hero fell. Then the 4th hero put up a bold fight, but we learned while on the high perch that he too had fallen. Of the third hero we knew nothing, but at end of the day he was the only winner in round one. But is seemed that the line of the brahman was dead. We said aloud: "We feel like Proclus at the end of the era". There was no one to hear us but the four walls.

On the top of the hill they invoked the powerful दंष्ट्र-यक्षिणी and waited for her to manifest. She came but with her was the dreadful kauberaka known as विरूपाक्ष. Rather than receiving the favors of दंष्ट्र-यक्षिणी they were possessed by विरूपाक्ष. They continue to wander in that state.


