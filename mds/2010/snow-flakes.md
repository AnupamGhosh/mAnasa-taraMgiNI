
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Snow flakes](https://manasataramgini.wordpress.com/2010/07/06/snow-flakes/){rel="bookmark"} {#snow-flakes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 6, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/07/06/snow-flakes/ "4:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh5.ggpht.com/_hjuA1bE0hBw/TDEkGMTnNEI/AAAAAAAABgA/CMiLhfyDLKI/s800/snow_flakes.png){width="75%"}
```{=latex}
\end{center}
```



