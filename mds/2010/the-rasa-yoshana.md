
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rasa-योषणा](https://manasataramgini.wordpress.com/2010/03/09/the-rasa-yoshana/){rel="bookmark"} {#the-rasa-यषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 9, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/03/09/the-rasa-yoshana/ "7:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The रसायन texts developing in the bhairava-srotas of the shaiva-शासन furnish a remarkable myth about the extraction of mercury in the darada country. One instance of this myth is found in the Anandakanda:\
Anandakanda 1.1.53cd-1.1.62ab

[प्रथमार्तव सुस्नाता सुरूपा शुभ-लक्षणा ॥ ५३cद्\
शुद्धाम्बरधरा माल्या गन्ध-लिप्ता सुभूषिता ।\
उत्तमाश्व समारूढा रति-सङ्ग-विवर्जिता ॥ ५४]{style="color:#99cc00;"}

Having bathed after her first menstruation, a good looking maiden with auspicious features, clad in clean garments, wearing garlands, smeared with perfumes, well-adorned and abstaining from sexual activities mounts a good horse.

[अभ्य् अर्छ्य गणनाथं छ भैरवं छ गुरुं पुरा ।\
रसेन्द्र-भैरवं ध्यात्वा कूप-स्थं पारदं प्रिये ॥ ५५]{style="color:#99cc00;"}

Having first worshipped the lord of the गण-s, bhairava and preceptor at the inception one meditates on rasendra-bhairava in the mercury situated in the \[mercurial] well

[तदानीम् आहरेत्-ततु कुमारी संजिघृक्षया ।\
कूप मध्यात् समुत्पत्य सो।अनुधावति तां प्रति ॥ ५६]{style="color:#99cc00;"}

At that instance, from the midst of the mercurial well, the Hg seized with the desire to grab the maiden, springs forth and rushes after her.

[पश्येच्-छीघ्रं ततो गच्छेत् न पुनः पृष्ठम् ईक्षयेत् ।\
एषा योजन-मात्रा कुमारी हयसाधना ॥ ५७]{style="color:#99cc00;"}

On seeing the Hg, without looking behind again, quickly the maiden rides away on her horse for the distance of a yojana.

[यावद् योजनम् आगत्य पुनः कूपे विशेत् क्षणात् ।\
परितः कृत गर्तेषु तेषु तेषु च संस्थितम् ॥ ५८\
तं रसेन्द्रं शुचिर् भूत्वा गृह्णीयाद् रस-देशिकः ।]{style="color:#99cc00;"}

After reaching a yojana (in her pursuit) the Hg rapidly subsides into the mercurial well. While making the return it gets trapped into hollows. Having become pure the rasa-deshika takes possession of that mercury.

[गौरवाद् अग्नि-वदनात् पतितो दरदाह्वये ॥ ५९\
देशे स सूतो भूलीनः तन्त्रज्ञै रसकोविदः ।]{style="color:#99cc00;"}

The alchemical master by means of tantric insights knows that due its weight the Hg fell from the mouth of fire in the country known as darada and was absorbed by the earth.

[निक्षिप्य मृत्तिका यन्त्रे पातनाख्ये समागतः ॥ ६०\
पारदो गृह्यते देवि दोष-हीनस् स उच्यते ।]{style="color:#99cc00;"}

Having cast that ore (see below) into the retort assembled for distillation, O goddess he obtains the Hg that said to be free from impurities.

[एवम् एव तत्र तत्र सिद्ध-विद्याधरैस् सदा ॥ ६१\
निक्षेपितः पारदेन्द्रो विद्यते देवि सिद्धिदः ॥ ६२अब्]{style="color:#99cc00;"}

O goddess, verily, wherever the lordly mercury is thus extracted by the siddha-विद्याधर-s it is known to be the granter of success.

Though wrapped in a mythical form the tale gives an important insight into the geographical source of mercury for the Hindu तान्त्रिक-s. It is well-known that despite the fame of रसायन in India, mercury is extremely rare and the modern Indian government issued a statement that it possesses no workable mercurial deposits after commissioning an exploration for the same in the 1960-70s. This fact has been exploited by the white sinophilists to claim that रसायन came to India from the chIna-s. However, we have the evidence of the chIna-s themselves that their daoistic mercurial chemistry was influenced by the bhArata-s. So the big question was where did the Hindus get their mercury from? The above narrative refers to the source of mercury known to the तान्त्रिक-s to be the darada-desha. A similar narrative is also found in the रसेन्द्रचूडामणी suggesting that it was a persistent theme. Now कल्हण in describing the conquests of the great कार्कोटक emperor ललितादित्य mentions that the darada country lies to the north of the kambhoja-s of the upper Punjab, the Tibetans and तुखार-s. The descriptions of उत्तरापथ in the पुराण-s also generally match with this location of the darada-s. This is also confirmed by the location the तुखार-s described by the chIna scholar/spy Xuanzang. This, is consistent with the darada-s lying in what is today Tajikistan. A relatively obscure fact is that Tajikistan contains a rather rich deposit of antimony-mercury (in some cases also with gold) that the Russian geologists had established to be of hydrothermal origin. Several features of the above तान्त्रिक myth might fit such a deposit: Firstly, the wells mentioned in the account could correspond to the hydrothermal springs that are central to the deposition of elemental mercury from such ore accumulations. Secondly, the geothermal conditions could also achieve sufficiently high temperatures for the normally stable cinnabar and metacinnabar (sulfides of mercury) to partially dissociate releasing mercury. Thirdly the hydrothermal conditions also allow two means of mercury transport from greater depth to the surface -- namely as dry vapor or as aqueous Hg0. This is transport is what is alluded to as the leaping of the mercury from the well when the maiden rides by. The falling of Hg due to its weight from the mouth of agni is again an allusion to the deposition of liquid elemental mercury following transport from a hydrothermal system.

Finally, what does the horse-riding mercury-magnet-maiden mean? Is it just a myth or a cryptic allusion to an alchemical process we fail to understand?


