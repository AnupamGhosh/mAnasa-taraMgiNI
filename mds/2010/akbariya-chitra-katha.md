
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [अक्बरीय chitra kathA](https://manasataramgini.wordpress.com/2010/03/11/akbariya-chitra-katha/){rel="bookmark"} {#अकबरय-chitra-katha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 11, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/03/11/akbariya-chitra-katha/ "7:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Mogol tyrant Akbar gave the old tradition stemming from the mankha and यमोघण्ठ-s of yore an unexpected revival when it had nearly died under the depredations of the rAkShasa-mata. In the later phase of his career post 1579 CE the once ardent Ghazi started losing his faith in the cult of मरून्मत्त and started turning to a more Hindu way of thought. He commissioned illustrated versions of various Hindu texts since he had a problem with reading and writing. One such is the famous रामायण (The Freer रामायण, now preserved in the Smithsonian collection). While painted by both Hindu and Mohammedan artists, it shows a marked Abrahamistic influence throughout -- notice the lack of the यज्ञोपवीत-s for kShatriya-s and ब्राह्मण-s and the horned demons. Several key figures are painted in the image of Akbar himself.

[Click to see image](https://photos.app.goo.gl/MecJ3FmjpEQRT1d27)


