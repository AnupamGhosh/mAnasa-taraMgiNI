
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mystery goddess of the शुङ्ग period](https://manasataramgini.wordpress.com/2010/01/25/the-mystery-goddess-of-the-shunga-period/){rel="bookmark"} {#the-mystery-goddess-of-the-शङग-period .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 25, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/01/25/the-mystery-goddess-of-the-shunga-period/ "7:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh4.ggpht.com/_hjuA1bE0hBw/S11JyYMhMsI/AAAAAAAABPM/SpnM5r47ml4/s400/camera1.jpg){width="75%"}
```{=latex}
\end{center}
```



Many a time memes expressing themselves in the philological medium and those that express themselves in the material medium of art and architecture share several common motifs. Yet these two sets of memes can show a striking disconnect in many phases of history. This aspect is only all too well-known in the great problem of the Indo-European homeland and subsequent migrations. The vast land of bhArata is home to several such memetic disconnects that puzzle the student of unadulterated heathen thought. We have earlier discussed the remarkable case of the horned deity. In terms of material evidence this meme widely propagated throughout Eurasia through cultures transcending various language superfamilies. It is also encountered in one of the earliest textual traditions, namely the shruti, starting from the ऋग्वेद itself --- there is the term तिग्मशृङ्गिन् which occurs in the veda. Yet, the horned deity of the SSVC is an enigma, who cannot be unequivocally linked to any of the hoary textual traditions that survive in bhArata. On the other hand there are many motifs shared between the texts and the material depictions over vast distance in space and time . His ithyphallic form, tricephaly, and yogic Asana are motifs that appear much later in both the material medium as well as in texts in the context of the deity rudra who is of vedic provenance. But there is no direct evidence linking these shared motifs in these temporally separated expressions in a clear phylogenetic scheme.

Likewise, the powerful goddess depicted above is a great enigma of Indic expression. Around the शुङ्ग period following the demise of the first mauryan-s we see an enormous number of depictions of this goddess throughout North India. While her first images started emerging in the वङ्ग country, it has now become clear that she was widely depicted throughout northern India probably all the way to Afghanistan --- which corresponds to the western thrust of the शुङ्ग-s in driving out the yavana आक्रान्त-s. While वङ्गदेश produces some of the finest examples of this goddess, especially in Chandraketugarh, there are great replicas available from Bihar, Uttar Pradesh and further Northwest and Southwards. They are found even in the old कौमार-क्षेत्र of Mathura along side some of the earliest कौमार images. One can see numerous different images of this deity from around north India at Naman Ahuja's website (the latest student of this mystery goddess) or in the book of the white Indologist Bautze or in the Chandraketugarh website or the museums of Kolkota, Ashmolean at उक्षतिर्थ or the Metropolitan in navyarkapura. There may at least a hundred or more of these pratima-s still around all from around 200-50 BCE. But who is this might देवी? Clearly her identity is somewhat unclear to modern observers, who have offered various opinions: 1) A यक्षिणी; 2) "mother goddess"; 3) woman with barbaric hairdo (Harle !); 4) woman with hairpins; 5) अप्सरा पञ्चचूडा (late Stella Kramrisch); 6) दुर्गा; 7) श्री; 8) royal/dancing woman; 9) the vedic deity सिनीवाली !. More importantly despite the obvious stereotype in her depictions many works have failed to recognize the different versions of her even within the Gangetic Doab. So it appears that the continuity or the links to the material depiction of this deity have been lost (we will return to the issue of her identity later).

Yet, as in the case of the Indus horned deity, we see a number of motifs associated with her which are pervasive in Indic or even Eurasian heathen thought. Firstly, the depictions very plainly convey her essential nature --- she is an erotic warrior goddess. This essential motif is itself pervasive across time. In the Bronze Age Sumerian and Semitic worlds we have Innana and Ishtar/Ashtart who shares not only the above attributes but also some iconographic features with our Indic goddess: The prominent depiction of the yoni as well as weapons which are shown like rays. In the case of Ishtar the weapons usually are shown symmetrically on both sides, while in the case of the Indic goddess they emanate from the hairdo either symmetrically or only on one side. Nevertheless their pose of standing face on with the weapons radiating from the upper part of the body is very similar. At least some weapons, the trident, the staff and arrows are shared by both of them. Both have a special banner or scepter which might bear a star-like pattern (at least centrally in the case of the Indic deity). Sometimes on both their cases this symbol might be shown on top of the head. Another special iconographic similarity between these two deities is that they might be depicted between two pillars while showing their grace to their votaries. In one of the Middle Eastern epics it is explicitly mentioned that Ishtar/Innana has a large headgear. Now both she and our goddess might be depicted with prominent headgears. Both these goddesses share an affinity for big felines --- the Middle Eastern goddess rides a lion whereas our version might be depicted with a leopard/tiger and in some cases perhaps with a lion. In many cases Ishtar is shown with wings. While this feature is not common in our goddess, there is a terracotta fragment from Chandraketugarh which I believe is the same goddess with wings.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh4.ggpht.com/_hjuA1bE0hBw/S2TD870wCLI/AAAAAAAABQM/FeJt8XlpYpk/s400/ishtar.jpg){width="75%"}
```{=latex}
\end{center}
```



[Ishtar/Innana: note the trident, weapons radiating on either side and the lion]{style="color:#ffff00;"}

Many of these pervasive motifs that are shared between these goddesses in the medium of material representation from two parts of Asia over a period of more than 2500 years are also seen repeatedly emerging in memes transmitted in the philological medium in the Indo-Aryan world. Throughout the Eurasian world the great goddess was often trans-functional and also warrior. So in itself this motif of the warrior goddess is seen throughout much of Bronze Age Eurasia -- Ishtar, सरस्वती and her Iranian ortholog अरेद्वी sUrA anAhitA and Athena. But as a specifically erotic warrior goddess we see the meme reoccurring many centuries later in the crest jewel of the श्रीकुल texts -- the ललितोपाखायन, which describes the great deeds in war of the goddess कामेश्वरी (she is also specifically termed शृङ्गार-नायिका and शृङ्गार-rasa-संपूर्णा to emphasize her erotic nature) who was generated by the gods to end the काम pralaya and slay भण्डासुर and his hordes. In the Middle Eastern lore the descent of Ishtar to the netherworld was supposed to result in a situation similar to the कामप्रलय of the ललितोपाख्यान. Her affinity for large felines is also a pervasive motif in the textual medium. It emerges right in the श्रुती itself. The supreme goddess is described thus in a famous सूक्त of the अथर्वाङ्गिरस-s:

[सिंहे व्याघ्र उत या पृदाकौ त्विषिर् अग्नौ ब्राह्मणे सूर्ये या ।\
इन्द्रं या देवी सुभगा जजान सा न एतु वर्चसा संविदाना ॥\
या हस्तिनि द्वीपिनि या हिरण्ये त्विषिर् अश्वेषु पुरुषेषु गोषु या ।\
इन्द्रं या देवी सुभगा जजान सा न एतु वर्चसा संविदाना ॥]{style="color:#99cc00;"} (AV-vulgate 6.38.1-2)\
Here she is associated with lions, tigers, cheetahs and leopards (of course in addition with the elephant, the horse and human).

This motif continues in some of the early hymns to the trans-functional goddess as दुर्गा or AryA.\
E.g. the AryA stava-1 in the हरिवंश states:\
[सिंहैर् व्याघ्रैस् समाकुला ।]{style="color:#99cc00;"} (8b)\
Thus, both in the Middle East and the Indo-Aryan world this link between the great goddess and the large cat is rather persistent.

Some of the earliest incantations devoted दुर्गा are seen as inserts into the महाभारत and as parts of the हरिवंश where there are at least two major AryA stava-s. These stava-s have some relationship with the emergence of her worship in the late sections of the बोधायन गृह्य परिशिष्ठ-s (दुर्गा-कल्पा) and correspond to the fact that her (as कुमारि, a name used in all these texts) worship in shrines in the city are mentioned in the अर्थशास्त्र of विष्णुगुप्त. Further, her earliest stava-s describe her as being associated with vAsudeva and संकर्षण (i.e. identification with एकानंशा), being the daughter of nanda (i.e. association with Mathura) and being worshiped in विन्ध्याचल where the विन्ध्या range intersects the Gangetic region. The association with Mathura is also clearly related to the कौमार angle: In one case she takes over a deed original performed by skanda, i.e. the killing of महिषासुर. Secondly she is described as षष्ठी, रेवती, पूतना (! in the हरिवंश itself) and शकुनी-देवी --- all skanda-मातृका-s. This trend continues in the early skanda-पुराण विन्ध्यवासिनी section (Nepal/Bhattarai recension) where she is described as generating various skanda-मातृका-s as her assistants in the war against sumbha and nisumbha. Given that the archaeological finds of our mystery goddess are from the Gangetic belt and Mathura (including the archaeological association with early कौमार finds), and these stava-s and विष्णुगुप्त point to her worship in that region, we should examine these stava-s for any motifs related to the mystery goddess. There are three major stava-s: 1) to दुर्गा in the महाभारत by युद्धिष्ठिर before going to विराट's land; 2) to AryA in the हरिवंश by वैशम्पायन in the विष्णु parva; 3) to AryA in the हरिवंश by aniruddha when he was trapped in bANa-s abode. There is an additional recension (recorded much later: 703 CE) of the 2nd of these stava-s preserved in metrical Chinese translation by the चीनाचार्य Yijing. Of these it appears that there many unique motif matches in the stava from the महाभारत, although a few more are obtained if include all of the stava-s together.

We list these matches below; although some of them are very generic:\
From the दुर्गा-stava of the bhArata:\
[पूर्ण-छन्द्र-निभानने --]{style="color:#99cc00;"} one with a full moon-like face. The terracotta depictions try to provide a clear view of the face the deity to meet this description.\
[पीन-श्रोणि-पयोधरे --]{style="color:#99cc00;"} one with full hips and breasts. The शुङ्ग depictions are rather unequivocal in illustrating this point.\
[मयूरपिच्छ-वलये केयूराङ्गद-धारिणि-]{style="color:#99cc00;"}one with bangles of sapphires and wearing upper arm ornaments and epaulets. I agree with KM Ganguli's opinion that मयूरपिच्छ here means the gem rather than the peacock feather itself. The attribute valaya is rarely used in later literature, and is one item that is very prominent in the iconography of our शुङ्ग era deity.\
[बिभ्रती विपुलौ बाहू शक्र-ध्वज-समुच्छ्रयौ-]{style="color:#99cc00;"} Bearing two arms like indra's flag poles. While these stava-s may talk of chatur-bhuja, and अष्टादश-bhuja the dual suggests that there was also a conception with two arms, which is consistent with our deity's image.\
[पात्री च पङ्कजी घण्टी ... पाशं धनुर् महाचक्रं विविधान्य् आयुधानि च-]{style="color:#99cc00;"} Of these set of attributes we encounter the vessel, the lotus, the bell and perhaps the chakra in a banner and of course "vividha Ayudha-s".\
[त्रिशाखं शूलम् उद्यम्य दानवान् विनिक्ऱ्‌न्तसि --]{style="color:#99cc00;"} trident -- an attribute very frequently seen in our goddess.\
[पादौ नूपुर केयूरैर् घण्टाभरण-भूषितौ --]{style="color:#99cc00;"} legs with anklets, a feature always seen wherever the legs are preserved in the images.\
[कुण्डलाभ्यां सुपूर्णाभ्यां कर्णाभ्यां च विभूषिता-]{style="color:#99cc00;"}The prominent ear-ornament alluded to here are very much a feature of our goddess.\
[मुकुटेन विचित्रेण कश-बन्धेन शोभिना-]{style="color:#99cc00;"} elaborate headdress and hairdo mentioned here are very specific feature of our शुङ्ग goddess.\
[भुजंगाभोग-वासेन श्रोणि-सूत्रेण राजता।\
विभ्राजसे चाबद्धेन भोगेनेवेह मन्दरः ॥]{style="color:#99cc00;"}\
This verse mentions snake dress or ornaments, which I have suspected to be present in some depictions of the शुङ्ग deity, though I must admit these are not very clear (e.g. the lower right hand side of picture above at higher magnification shows the scales of the snake, see original from Ashmolean Museum). But importantly the verse also mentions the श्रोणि-sUtra or the brilliant hip-girdle which is one defining feature of this deity.\
[ध्वजेन शिखि-पिच्छानाम् उच्छ्रितेन विराजसे-]{style="color:#99cc00;"} The erect banner of peacock feathers. This one is seen in a few depictions of the शुङ्ग deity, e.g. the one on the lower left-hand side in the picture above (original in the Metropolitan Museum).\
[महा-सिंह-रथारूढे महा-शार्दूल-वाहिनि-]{style="color:#99cc00;"}having the lion and tiger as vehicles --- association with big felines.

The AryA stava-1 of the हरिवंश:\
[मयूर-पिच्छ-ध्वजिनी]{style="color:#99cc00;"}-- Same as above\
[कुक्कुटैश्-छागलैर्-मेषैस्-सिंहैर्-व्याघ्रैस्-समाकुला ।]{style="color:#99cc00;"}\
The first three animals are कौमार animals which appear to have been absorbed by the देवी, the while the last two are the big felines as above.\
[त्रिशूली पट्टिशधरा सूर्य-चन्द्र-पताकिनी ।]{style="color:#99cc00;"}\
The weapons and the flag with the sun and moon symbol. We at least see a sun-like symbol in some of the banners of the शुङ्ग goddess.\
[नक्षत्राणां च रोहिणी-]{style="color:#99cc00;"} Of the stars you are alpha-Tauri. This is a hint of a stellar connection, which is also seen in Ishtar. It is possible that some of the banners/standards of our goddess bear a star on them.

These are the matches -- some distinctive and some generic. Yet there is one somewhat puzzling feature. In all the three stava-s the देवी is clearly described as:\
[कौमार-व्रतछारिणि ; "कौमारव्रतम् आस्थाय"; "ब्रह्मचर्यं"; "ब्रह्मछारिणी"; "ब्रह्मछारिणीम्";]{style="color:#99cc00;"}\
This is consistent with the यज्ञोपवीत seen on some of the शुङ्ग images. At the same time it also means one who is unmarried/celibate. This attribute is apparently contradictory with her erotic image on that is some pervasive in the शुङ्ग depictions. There is only one apparent exception to this (see below). Now all the stava-s are not averse to explicitly naming her association with other "makara-s": e.g.: [सीधु-मांस-वसा-प्रिये]{style="color:#99cc00;"}-- one fond of rum, meat and fat; [सुरा-मांस-बलि-प्रिया]{style="color:#99cc00;"}-- one fond of offerings of beer and meat; "[मदिरां]{style="color:#99cc00;"}" and "[सुरां]{style="color:#99cc00;"}" -- personifications of various liquors. But only one of them (the हरिवंश AryA stava-1) to my knowledge touches on the erotic aspect and that too fleetingly:\
[कन्यानां ब्रह्मछर्य-त्वं सौभाग्यं प्रमदासु छ ।]{style="color:#99cc00;"}\
You are the chastity of maidens and also the conjugal bliss of erotic women.\
It is possible that this last aspect was down-played in the canonical texts but much more prominent in the lay religious expression. Thus, if we were to accept the reasonable match in epithets of the early stava-s to AryA and the iconic depictions of the शुङ्ग goddess, then we may conclude that they are indeed the same.

There are few other points regarding the शुङ्ग icons that we might further look into. In several icons the शुङ्ग deity is shown as a holding a bag of money or disbursing कार्षापण-s to votaries. This has been used to identify the देवता as श्री. However, we should note that in these early AryA stava-s she is explicitly identified as श्री, so this aspect is not incompatible with the connections with AryA noted above. The version in the Ashmolean museum from Tamluk in the वङ्ग country is unique in having four beings on the thighs of the goddess. The figures of beings are very small -- they appear male and are seated in squatting pose, but anything beyond that is hard to discern what they represent. We do not have any immediate memetic evidence from the stava-s themselves regarding this point. It is possible that they represent children and are depicted as being born. In this regard clearly the हरिवंश stava-s describe the goddess as the mother of कुमार (siddhasena). A much latter text the vyapohana stava of the shaiva सैद्धान्तिक-s has the following line in the description of महादेवी:\
[नैगमेयादिभिर् दिव्यैश् चतुर्भिः पुत्रकैर्-वृता ।]{style="color:#99cc00;"}\
One accompanied by the four divine sons starting with naigameya(नेजमेष). Is it possible that this textual motif has a link to the iconic motif in the Tamluk exemplar? At present the data remains limited to fix such a connection.

Overall, we feel that the evidence does point to the same female deity being depicted throughout north India in this period from around 200-50 BCE. The greatest congruence in motifs is currently seen between the iconography of this goddess and the early textual description of AryA, suggesting that this identification is the one that should be cautiously accepted. More generally the motifs related to the goddess are seen in memes transmitted by both textual and iconic means spread across a wide swath of space and time in both in bhArata and the rest of Eurasia. This highlights a key issue often ignored by archaeologists and philologists alike. The appearance of a motif in one of their favored media does not imply that it is the first appearance of the motif in the memetic sphere. Some early observations regarding the persistence of various memetic motifs in bhArata were made by DD Kosambi, a learned but misguided and subversive thinker from the city of my youth. However, his adherence to baseless and delusive ideologies inherited from his father resulted in him not correctly grasping their history. These indeed need to be revisited with a proper evolutionary framework that should take heed of what  happens in the study of biological evolution and paleontology.


