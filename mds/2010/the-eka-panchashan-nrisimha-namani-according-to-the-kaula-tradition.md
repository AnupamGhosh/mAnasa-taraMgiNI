
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The eka-पञ्चाशन्-नृसिंह-नामानि according to the kaula tradition](https://manasataramgini.wordpress.com/2010/07/24/the-eka-panchashan-nrisimha-namani-according-to-the-kaula-tradition/){rel="bookmark"} {#the-eka-पञचशन-नसह-नमन-according-to-the-kaula-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 24, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/07/24/the-eka-panchashan-nrisimha-namani-according-to-the-kaula-tradition/ "6:37 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As per the shaiva tradition of the brahma-यामल that expounds the lore of काप्लीश bhairava and रक्तचामुण्डा, the sixth srotas is that of the नृसिंह tantra-s. Another tradition like that recorded by abhinavagupta and jayaratha state this srotas was that of हाटकेश्वर. Tradition holds that the homology of हाटकेश्वर with नृसिंह was expounded by the now lost आनन्दाधिकार tantra. Further evidence for this is suggested by the eka-पञ्चाशन्-नृसिंह-नामानि of the काली kula. R1's father stated that the याग of नृसिंह was expounded in one of the khaNDa-s of the पञ्चाशत् साहस्र्यां महाकाल saMhitA that still survived as a manuscript in काठ्माण्डू in his days. From memory I think this पटल gave the same or similar set of 51 names as those recorded in the नृसिंह न्यास of the कामकला-काली khaNDa of this पञ्चाशत् साहस्र्यां.\
[ज्वालामाली करालश् च भीमश् चैवापराजितः ।\
क्षोभणश् च तथ सृष्टिस् स्थितिः कल्पान्त इत्य् अपि ॥\
अनन्तश् च विरूपश् च वज्रायुध परापरौ ।\
प्रध्वंसनश् च विज्ञेयो विश्वमर्दन इत्य् अपि ॥\
उग्रो भद्रश् च मृत्युश् च सहस्रभुज इत्य् अपि ।\
विद्युज्-जिह्वो घोर-दंष्ट्रो महाकालाग्निर् एव च ॥\
मेघनादश् च विकटस् तथा पिङ्ग-जटोऽपि च ।\
प्रदीपो विश्वरूपश् cha vidyud-dashana eva cha ||\
विदारो विक्रमश् चापि प्रचण्डस् सर्वतोमुखः ।\
वज्रो दिव्यश् च भोगश् च मोक्षो लक्श्ंईर् अपि क्रमात् ॥\
विद्रावणः कालचक्रः कृतान्तस्तप्त-हाटकः|\
भ्रामकश् च महारौद्रो विश्वान्तक-भ्यङ्करौ ॥\
प्रतप्तो विजयश् चापि सर्वतेजोमयस् तथ ।\
ज्वालाजटालश् च खरनखरो नाद-दारुणः ॥\
निर्वाण नरसिंहश् चेत्य् एक-पञ्चाशदीरितः ।]{style="color:#000080;"}KK of PSMS 8.69-76ab

These 51 नृसिंह-s are placed by the साधक on his body as per the usual practice of the 50-fold अङ्ग-न्यास of Agamika tradition. Then he meditates upon the 51 नृसिंह-s as below, forming a circle around the great कामकला:

[उद्यन्-मार्ताण्ड-कोट्य्-अंशु-समारुण-तनु-प्रभाः ।\
उलूकाकार-पृथुल-नेत्र-त्रितय-भूषिताः ॥\
विदारि-सृक्क-निर्गच्छद्-दंष्ट्रा-चन्द्र-कालन्विताः ।\
विदीर्ण-विकरालास्य-निर्यज्-जिह्वा-विराजिताः ॥\
विमुक्त-चामराकार-सटा-केशर-मण्डिताः ।\
आबद्ध-योग-पट्टान्ता जानुन्य्-अस्त-कराम्बुजाः ॥\
कोटि-कल्पान्तार्क-समा भीम-दंष्ट्राट्ठासिनः ।\
कौस्तुभोद्-भासि-हृदयाश् श्वेत-पद्मोपरि स्थिताः ॥\
किरीट-हार-केयूर-किङ्किण्य्-अङ्गद-शोभिताः ।\
मुखैः कल्पान्त-कालाग्निं वमन्तस् सर्वतोमुखाः ॥\
कराल-भृकुटी-दृष्टि-सन्त्रासित-जगत्-त्रयाः ।\
नख-निर्भिन्न-दैत्येन्द्र-रुधिरोक्षित-बाहवः ॥\
विपाटितान्त्र-निर्गच्छद्-वसा-लिप्ताङ्क-कुक्षयः ।\
अस्त्रैर्-विस्भूषितान् दीर्घान् भुजान् षोडश बिभ्रतः ॥\
शरं चक्रं गदां खड्गं पाशम् अङ्कुशम् एव च ।\
वज्रं विदारणं चापि दक्षिणेन क्रमाद् अपि ॥\
धनुः शङ्खं च पद्मं च खेटकं मुशलं तथा ।\
परशुं पट्टिशं चापि विदारणम् अतः परं ॥\
वामेन धारयन्तस् ते रत्नाकल्प-विराजिताः ।\
वाम-जङ्घासन्-निविष्ट-लक्ष्मीकास् सिद्धि-दायिनः ॥]{style="color:#000080;"}

The नृसिंह-s are contemplated as 16 handed with लक्श्ंई-s on their left laps. They hold in their 8 right arms: arrow, discus, mace, sword, lasso, hook, thunderbolt and claws with which they are tearing the daitya chief. In their 8 left arms they hold: bow, conch, lotus, shield, club, axe, foil-blade and claws with which they are tearing the daitya chief.


