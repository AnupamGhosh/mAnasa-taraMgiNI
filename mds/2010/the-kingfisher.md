
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The kingfisher](https://manasataramgini.wordpress.com/2010/07/30/the-kingfisher/){rel="bookmark"} {#the-kingfisher .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 30, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/07/30/the-kingfisher/ "6:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[The great क्षेमेन्द्र in his characterization of the qualities of a kavI had mentioned how he should be a good naturalist.](https://manasataramgini.wordpress.com/2008/02/13/thinking-of-kshemendra/){target="_self"} This is an old quality whose germs are seen in वाल्मीकि himself. Below is a little word-painting by the great वाक्पतिराज in this tradition. He was kavi in the court of yashovarman, the neo-maurya, who surging from madhyadesha cut down the वङ्ग-s, only to be conquered himself by ललितादित्य of Kashmir.

[उत्प्लुत्य दूरं परिधूय पक्षाव् अधो निरीक्ष्य क्षण-बद्ध-लक्ष्यः ।\
मध्ये-जलं बुड्डति दत्त-झम्पः समत्स्यम् उत्सर्पति मत्स्य-रङ्कः ॥]{style="color:#0000ff;"}\
utplutya- springing; दूरम्- far up; परिधूय- shaking; पक्षौ- two wings; अधः- below; निरीक्ष्य- examining; क्षण-baddha-लक्ष्यः- connecting to the target in an instance; madhye-जलं-into the water; बुड्डति -- dives; datta-झम्पः -- gives a jump; samatsyam- with a fish; utsarpati -- rises up; matsya-रङ्कः -- kingfisher.

Springing far upwards, fluttering its two wings, examining below, and in an instance connecting to its target, it dives with a jump into the water, with a fish rises up the kingfisher.


