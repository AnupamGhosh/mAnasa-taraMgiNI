
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Earthly heroes and divinities](https://manasataramgini.wordpress.com/2010/01/16/earthly-heroes-and-divinities/){rel="bookmark"} {#earthly-heroes-and-divinities .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 16, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/01/16/earthly-heroes-and-divinities/ "7:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm5.static.flickr.com/4019/4278486398_992a75b7a1.jpg){width="75%"}
```{=latex}
\end{center}
```



The three figures to the left are identified as the 3 पेश्वा-s:\
बालाजी vishvanath: The hero who took the saffron dhvaja back to indraprastha which had been defiled by the तुरुष्क-s for 600 years.\
बाजिराव्: The hero who brought the तुरुष्क-s to their heels.\
चिम्नाजी अप्पा: The hero who sent the प्रेताचरिन्-s to enjoy vaivasvata's company and restored the वज्रेश्वरी temple.

Of course further to the right are नृसिंह rending the son of diti and महिषमर्दिनी laying another of them low. Above her stands rudra.


