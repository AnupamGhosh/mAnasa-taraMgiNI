
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The experience: transient and eternal](https://manasataramgini.wordpress.com/2010/03/12/the-experience-transient-and-eternal/){rel="bookmark"} {#the-experience-transient-and-eternal .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 12, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/03/12/the-experience-transient-and-eternal/ "7:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having reached the ford I called on indra for aid and also that great देवी who had birthed him, who is praised in the श्रुती of the atharvan-s. The great deva-s and ग्ना-s had granted us the victory we sought and in great pleasure we crossed ford and moved on. Then in the श्मशान we were in the embrace of the glorious दूती. One side we were thawed by the embers of an अन्त्येष्टि, while on the other by the warm pleasures of rati. The heavy tresses of her hair framed her face of ivory complexion like the blackness of the clearest night framing the heavenly cup of soma. We lay there conjoined at the eternal cusp, in the विशुवान् of consciousness, drinking the soma trickling from the great lotus that abode of rati-shekhara conjoined with the aroused मालिनी, who coursed along the kulapatha. The pIna-stana-s of दूती, the fount of all pleasure, shone like the निशाकर and the तिग्मांशु that shine in the sky of the consciousness. Her eyes sparkled, lit by the flames of the पितृवन, like of the color of beta Cygni at the head of the swan. We perceived the haMsa that courses through all of us, even as अभिप्रातरिन् काक्षसेनी and shaunaka kapeya had seen the mighty वायु, who reigns over the bright स्वती that was rising to the highest summit. He who has mastered the yoga dwells in that state as the drinker of soma.

We moved to the next ford beyond which lay the fort. We again invoked indra and विष्णु like सुदास on the banks of the swollen river. The didyu-s flashed on our behalf and our victorious horses bore the वाज impelled by the marut-s. By the deva-s we had crossed the unimaginable coils of वृत्र, even as we had done when we moved on to our first campaign. Then, we moved to the capital fort and reviewed information from our दूत-s. We saw the effects of an अभिचार which surprised us. We understood its significance after we obtained intelligence of an unexpected "mosquito proboscis" operation by the vairi. We experienced a stambhana due to the combined आक्रमण-s. But then we knew that indra could bring us aid against the ari but the यातुधान was even more fearsome.

Meditating on the drinking of soma we recited the secret sAman composed on the ऋक् of प्रगाथ ghaura काण्व:\
[अपाम सोमम् अमृता अभूमागन्म ज्योतिर् अविदाम देवान् ।\
किं नूनम् अस्मान् कृणवद् अरातिः किमु धूर्तिर् अमृत मर्त्यस्य ॥]{style="color:#99cc00;"}


