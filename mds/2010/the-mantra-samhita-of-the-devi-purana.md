
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mantra saMhitA of the देवी पुराण](https://manasataramgini.wordpress.com/2010/06/25/the-mantra-samhita-of-the-devi-purana/){rel="bookmark"} {#the-mantra-samhita-of-the-दव-परण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 25, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/06/25/the-mantra-samhita-of-the-devi-purana/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The देवी पुराण is one of the several dedicated shAktA पुराण-s such as देवी-bhAgavata, महाभगवत and कालिका. The sections of other पुराण-s like the proto-skanda पुराण (the विन्ध्यवासिनी section), the मार्कण्डेय पुराण (the देवी माहात्म्य section) and the ललितोपाख्यान of the ब्रह्माण्ड पुराण can also be considered mini shAkta पुराण-s. The dedicated shAkta पुराण-s and the ललितोपाख्यान section contain distinctive तान्त्रिक material beyond the core Pauranic story frame. This material is greatly developed in the कालिका पुराण and the ललितोपाख्यान. Both of these have a regional flavor with the locus of कालिका being eastern India centered on the कामाख्य पीठ and that of the ललितोपाख्यान is south India centered on काञ्चि. In a sense the ललितोपाख्यान is similar to the Tantric विन्ध्याचल माहात्म्य, but found is way into an older महापुराण as the uttara bhAga. Its main objective is incorporation of the कादिविद्या lineage of the उपासन of त्रिपुरसुन्दरी and the नित्या-s from the श्रीकुल तान्त्रिक system inside a स्मार्त system mainly followed by the south Indian तैत्तिरीयक-s. The कालिका appears to be more plainly तान्त्रिक expounding the उपासन of the श्रीकुल goddesses, दुर्गा-चण्डिका and वैष्णवी-महामाया. The देवी पुराण is centered mainly on the उपासन of चण्डिका-विन्ध्यवासिनी the slayer of the asura ghora who assumed the form of a buffalo. She is described as riding a lion comprised of the combination of all other deva-s rather than being formed by all the deva-s (as in the देवी माहात्म्य). She stations herself in the vindhya-s as a beautiful virgin and is sought by ghora. In course of a fierce battle ghora assumed the form of buffalo and was beheaded by the goddess. So her name became महिषमर्दिनि. Thus, it appears that the देवी पुराण preserves a parallel version of the famed shAkta myth of the killing of the buffalo demon. The central तान्त्रिक विद्या taught in the देवी पुराण is the पदमाल mantra of चण्डिका, which has many different mantra pada-s for different viniyoga-s (objectives).

Interestingly, the देवी पुराण also preserves a small mantra saMhitA of complete Vedic mantra-s (not just प्रतीक-s) for the performance of the daily Vedic ritual (chapter 56). This mantra saMhitA is primarily composed of ऋक्-s but also contains some यजुष्-es and a late ब्राह्मण-like terminal portion. This saMhitA is of interest because it appears to present several variant readings from those seen the well-known Vedic ऋक् saMhitA-s: the शाकल saMhitA, the AV-S 20, RV-khila, the आश्वलायन saMhitA which incorporates the खिलानि into the main ऋक् saMhitA equivalent to the शाकल collection, the hautra परिशिष्ठ of the Apastamba तैत्तिरीयक-s, the small ऋक् saMhitA of the हिरण्यकेशिन् तैत्तिरीयक-s and the संअवेद saMhitA पाठ-s. I must state that I have not examined any complete manuscripts of the देवी पुराण but the editor used at least 4 of them from eastern India and some of these variant readings in the ऋक्-s are uniformly seen across all the manuscripts used in the printed edition. The देवी पुराण was also known in late medieval South India among श्रीकुल adepts. It is mentioned by the great भास्करराय in his सौभाग्य-भास्कर (commentary on LS 200). However, I have not been able to locate any South Indian manuscript or publication of this पुराण -- this would have been useful in determining of those variant readings were also prevalent in the south.

Given that these reading drastically differ from all the extant homologs of the given mantra we may postulate the following distinct but not unrelated explanations:


 1.  These readings were acquired from a now extinct shAkhA that was isolated from the mainstream shrauta tradition early on, thereby diverging drastically from the others.


 2.  The readings are from the known शाकल text but due to memetic drift arising much later isolation, which prevented correction through meme conversion with the mainstream texts, the readings greatly diverged (analogs of the evolutionary processes occurring in genes). This isolation could have been spatial, i.e. geographical isolation, perhaps in eastern India, beyond the zone with a high concentration of vaidika practitioners. Or else it could have been traditional i.e. being a तान्त्रिक tradition and text the देवी पुराण mantra saMhitA transmission was not subject to correction from the mainstream vaidika tradition.

While the grammatical irregularities in this mantra saMhitA might support the less interesting alternative #2, we still suspect that these differences might in part indeed reflect a distinctive Vedic tradition, even if not a distinct shAkhA tradition. Evaluating this is hampered by the fact the only RV tradition that survives is the शाकल shAkhA. More recently collection supposed to be the आश्वलायन shAkhA was published. However, this saMhitA is identical to the शाकल saMhitA, except that it has the काश्मीर पाठ of the khila inserted into the saMhitA itself. From various sources it was clear that the बाष्कल shAkhA showed differences in the arrangement of the उपमण्डल-s of the first मण्डल (i.e. the gautama-कक्षीवान्-परुच्छेप-kutsa-दीर्घतमस् order) and had an additional सम्ज्ञान सूक्त in the 10th मण्डल which ended with the ऋक् "tach छंयोर् आवृणीमहे ...". Of course there is also the traditional variability between these ऋक् collections in terms of the inclusion or not of काण्व-s of the वालखिल्य section of मण्डल 8. What we really do not know is whether the readings of the cognate ऋक्-s in बाष्कल had substantial differences from those in शाकल. Now some workers have taken the reference to बह्वृच by आचार्य-s like कुमारिल भट्ट to mean that there was an eponymous RV shAkhA. There is no support for this view -- 1) we do not find this name as a shAkhA in the चरणव्यूह-s, which typically only name seven recensions. 2) पतञ्जलि says: "एकविंशतिधा बह्वृच्याम्" i.e. there are 21 shAkhA-s of the RV -- he means RV by बह्वृच. Yet the term बह्वृच might point to a connection between certain RV practitioners and the shAkta system because there is a श्रीकुल उपनिषद् known as the बह्वृच उपनिषद्. Whether these were connected to the compilers of the mantra saMhitA in the DP remains to be seen.

Some examples of variant readings:\
\~ऱ्‌V 1.11.1:\
[इन्द्रं विश्वा अवावृषं समुद्रव्यचसं गिरः ।\
रथीतमं रथीनां वा राजानां शतपतिं पतिम् ॥]{style="color:#0000ff;"}

\~ऱ्‌V 10.123.6:\
[नाके सुपर्णम् उपत्यान्त हवा रेवन्तो अभ्य चक्षत त्वा ।\
हिरण्याक्षं वरुणस्य दूतं यमस्य भुरण्युं योनौ शकुनं ॥]{style="color:#0000ff;"}

\~ऱ्‌V 5.64.1:\
[वरुणं बोधिपादम् ऋचाम् इन्द्रं हवामहे ।\
परि व्रजेछ् छ बाह्वोर् जगन्धसो स्वर्णवम् नरं ॥]{style="color:#0000ff;"}

\~आV-पैप्पलाद 20.5.8 [\~ऱ्‌V khila 3.22.4.1]:\
[आदित्यं देवं सविता मन्योः कवि क्रतुम् अर्छामि ।\
सत्यसवं रत्नधाम् अभि प्रियम् अतितरम् कविं ॥]{style="color:#0000ff;"}

\~आV-पैप्पलाद 20.5.9 [\~ऱ्‌V khila 3.22.4.2]:\
[ऊर्ध्वा यस्या मतिभा आदिद्युतत् सवीमनि ।\
हिरण्यपाणिर् त्वमिमीते सुक्रतुः कृपापयः ॥]{style="color:#0000ff;"}

\~ऱ्‌V 10.141.3:\
[सोमं राजानम् भवते अग्निम् अन्वारभामहे ।\
आदित्यं विष्णुं सूर्यं ब्राह्मणं च बृहस्पतिम् ॥]{style="color:#0000ff;"}

\~ऱ्‌V 1.164.41:\
[गौरीर् मिमाय सलिलानि तक्षत्य् एकपदी द्विपदी सा चतुष्पदी ।\
अष्टापदी नवपदी भूमीं सहस्राक्षं वा परमे व्योमन् ॥]{style="color:#0000ff;"}

\~ऱ्‌V: 1.114.8:\
[मा नस्तोके तनये मा न आयूषि मा नो गोषु मा नो अश्वेषु रीरिषः ।\
मा नो वीरान् रुद्र भामितो वधीर् हविष्मन्तः सदमित् त्वा हवामहे ॥]{style="color:#0000ff;"}

These examples (by no means the entire set of पठ bheda-s in this collection) clearly illustrate the grammatical irregularities and the meaning-deforming mutations that are likely indicative of reflect poor preservation. Nevertheless, it should be noted that the DP tradition was founded by a genuine vaidika tradition with knowledge of a ऋक् saMhitA, including the khila mantra-s, as well as a यजुष् tradition. In light of the changes in word order found in many of the mantra-s it is possible that these represent genuine पाठ-bheda-s in this tradition with respect to the mainstream शाकल tradition. However, we would be cautious, in light of the above discussed difficulties, about proposing that they inhere from a lost eastern Vedic recension.

After providing the core mantra-s for the स्थालीपाक oblations the section has an appendix imitating ब्राह्मण-like prose which makes this tradition distinct:\
[एवं वेदिकोऽग्निः संतुष्ठो भवति । अथातः परिस्तरण देवताः कथ्यन्ते । परिसमूहने काश्यपः । उपलेपने विश्वे-देवाः । उल्लेखने मित्र-वरुणौ । उल्लिखने पृथ्वी । अम्बुक्षणे गन्धर्वाः । अग्निः सादने शर्वः । परिसमूह्य् ओपलिख्य् ओल्लिखन कर्म विधिर् उच्यते । दक्षिणासादने ब्रह्मा । उत्तरतः पूरणे सागराः । आस्तरणे त्रसाः । अथावसादने शतक्रतुः । पवित्र बन्धने पितरः । प्रोक्षणी संस्कारे मातरः । जुह्वते स्रुचे स्रुवापाश् च ब्रह्मा-विष्णु-महेश्वराः । आज्यातपने वसवः । अधिश्रवणे वैवस्वतः । पर्यग्नि-करणे मरुतः । उद्वासने स्कन्दः । उत्पन्न प्रत्युत्पवने चन्द्रः द्यौः । आज्यावेक्षणे दिशः सर्वाः । पवित्र धारणे प्रणीताः । यामुमा देवी । इध्मे च लक्ष्मी । विश्वस्य विश्वाभूतानि ॥]{style="color:#0000ff;"}


