
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कुमारानुजाय नमः ||](https://manasataramgini.wordpress.com/2010/09/11/kumaranujaya-namah/){rel="bookmark"} {#कमरनजय-नम .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 11, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/09/11/kumaranujaya-namah/ "11:55 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh3.ggpht.com/_hjuA1bE0hBw/TIwSQ2ojVEI/AAAAAAAAB6U/MufxZkFGslg/s400/vinAyaka_chaturthI.jpg){width="75%"}
```{=latex}
\end{center}
```



[कुम्भ-स्थली रक्षतु वो विकीर्ण\
सिन्दूर-रेणुर् द्विरदाननस्य\
प्रशान्तये विघ्न-तमश्-छटानां\
निष्ठ्यूत-बालातप-पल्लवेव ॥]{style="color:#99cc00;"}

