
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Of dogs and underdogs](https://manasataramgini.wordpress.com/2010/02/14/of-dogs-and-underdogs/){rel="bookmark"} {#of-dogs-and-underdogs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 14, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/02/14/of-dogs-and-underdogs/ "7:19 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While being hemmed in by the hima pralaya we spoke to the वङ्ग paNDita. We concurred that there was not really anything new to comment on geopolitics, for what ever we had to say had been said and things were going as per the script. If you are leading a real dog's life you at least get to go out and howl and bark. But if you are a pretty underdog in a rich म्लेच्छ's keep, castrated and collared, you not even get to yelp, but have to silently wag your tail and accept a biscuit tossed towards you by Tom, Dick or Harry. The song goes thus:

The म्लेच्छ-s are verily ruled by dambhin-s,\
Among the chIna-s a band of taskara-s are supreme,\
the रोमाक-s place विध्वंसक-s at the forefront,\
but in bhArata we have chosen नपुंसक-s to rule us.

We thought we were lions,\
But we learned they were passé,\
Then we said we were tigers,\
But no roar was ever heard from us,\
There after we trumpeted of being elephants;\
Verily a ciliate is an elephant in world of a coccus.

In yonder gandhAra the म्लेच्छ-s are rampaging.\
But the bhArata-s want to go to the Mogols kissing.\
We then hear the तुरुष्क-s and म्लेच्छ-s calling:\
Hey महावध्रिका-s! How is it going?

Not a lion, not a tiger, not a peacock nor an elephant,\
But seedless underdogs are we.

The news of the म्लेच्छ sainya launching a major offensive in gandhAra really hurts deep. In 1760 CE the marATha army under रघुनाथ rav placed the saffron flag of the Hindus on the fort of Attock. In 1837 hari singh नाल्व the great panjAbI general placed his flag squarely inside gandhAra. Now, under the guidance of the म्लेच्छादिपति-s we are asked go kiss the Mogols in the hell-hole of TSP, even as sundry Ghazi-s launch murderous attacks on our cities. All this happens when the म्लेच्छ-s are at our door steps (the English army leads the attack in gandhAra and बाह्लिक! What business to they have there?). Truly it is the surest symptom of underdog-hood that we have regressed in military power relative to the 1700-1800s. We have no say in our own historic lands and instead let the म्लेच्छ-s decide what happens there. Indeed, only an underdog boasts of his "soft-power" in gandhAra in the form of Bollywood flicks casting roguish तुरुष्क-s. Then a नपुंसक अध्यक्ष states that तुरुष्क-s have only struck "soft targets", so there has been no intelligence failure. Yes, in soft state you only get soft targets.

More chilling is the point that the म्लेच्छ-s will eventually retreat from the तुरुष्क lands and end up empowering them continue their war against the Hindus. Then the वङ्ग paNDita laughed and said: Do not worry. The only consolation is that our ancient linguistic and cultural cousins, the yavana-s, are doing much worse than us. Bankrupt beggars, with a little patch of land plagued with fires, the yavana-s are verily paying the price for having forsaken the way of their deva-s for the preta cult.


