
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Examining Emily Blanchard West's thesis](https://manasataramgini.wordpress.com/2010/04/16/examining-emily-blanchard-wests-thesis/){rel="bookmark"} {#examining-emily-blanchard-wests-thesis .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 16, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/04/16/examining-emily-blanchard-wests-thesis/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

EBW, a classicist, published a paper presenting evidence for an evolutionary link via descent from a common ancestor between the Homeric Cyclopeia narrative of the Greek world and the three great rAkShasa-vadha episodes of the महाभारत, namely the killing of baka, हिडिंब and किर्मीर by भीमसेन. We have always supported the possibility of this evolutionary connection, and we think EBW has certainly presented case in a much better way than we had worked it out. So we were a bit amused that someone else out there had thought very similarly and presented this case so strongly, especially from the Greek side of the affairs. However, we must remark that being outside the Indo-European heathen tradition EBW falters in her interpretation of the narrative under the theoretical framework of a "brahma-क्षत्र" conflict. Nevertheless, her core thesis should certainly be closely examined as it offers important information on the development of the IE mythological motifs. Any student of Indo-European mytho-historical narratives realizes the persistence of several distinctive motifs. But in our opinion formed after our initial researches over 15 years ago the yavana-s and Arya-s share special synapomorphies which include not just the detailed motifs but also the style of the epic narratives and the knowledge systems transmitted therein. An example of a synapomorphic motif is the one common to the tale of kunti and कर्ण in the Hindu world and that of Creusa and Ion in the Greek world. Synapomorphic features of the narrative include the tense in which epics are narrated and several words (इषु:eis; vajra:uagros etc.). In terms of ideas the parallels between Greek and Hindu scientific thought have been outlined by us before. In this context one may also study Bharat Gupt's comparative study of Greek and Hindu theatrics. This is very important to understand the development of Indo-European, especially the interaction between the Greeks and Aryans probably after the Balto-Slavics had branched off from the Aryans.

We do not wish the rehash EBW's article here, but discuss the issues in a larger context of the parallel motifs running through certain epic heroes, rAkShasa-s and narrative constructs describing particular incidents. Several of these points were noticed by EBW and others apparently not. Some these have been independently noted by the yavana Kazanas in his article on a similar topic and by some others.

*Heroes:* In Odysseus the Greek composers chose to combine the images that might be separated into separate heroes in the Indo-Aryan world -- as Campbell had put it a hero with a 1000 faces. Odysseus is characterized by metis -- a certain skill or craft that overlaps with mAyA of the Indo-Aryan world. This facet is primarily developed in कृष्ण in the great epic. Both advise their protégés to use trickery to gain the final victory in their respective wars. Both are not the primary leaders of the military expedition in which they are involved (the Trojan and the कुरुक्षेत्र wars), though both are great warriors of their own right. Yet they are central to their party gaining victory. But on the other hand Odysseus is also a brutal warrior characterized by bloody and fierce deeds (क्रूरकर्मन्) -- in this capacity he occupies the facet of the hero typified by भीमसेन in the Mbh. भीमसेन and hanumat in the other इतिहास are the sons of वायु, and both are characterized by immense strength and specialize in killing rAkShasa-s. It is this aspect of Odysseus that comes to the fore in his Cyclopeian adventure. He too has his connection with the deity of wind. He is said to have been the descendent of the deity of the wind Aeolus and in Odyssey 10 he is supposed to have received the favor of Aeolus, who gave him a balloon of winds to help his voyage. Finally, the Odysseus is also the archetypal bowman, a type of hero shared specifically by the Aryans and Greeks. In this resembles the facet of the hero occupied by arjuna. A specific motif shared by them is the archery contest for a wife involving stringing of a heavy bow and shooting arrows through a narrow aperture. The bowman hero and the stringing of the bow for winning a woman is a persistent motif that in the Hindu world also appears in the context of the hero of the रामायण.The encounter of the heroes with the रक्षsa-s or their homologs always occurs when they are on a dangerous journey while they have been forced out of their homeland or kingdom.

*The रक्षsa-s:* Central to EBWs thesis is the homology between the राक्षसस् and the yavana Cyclopes. While there are many specific similarities between the narrative of the Cyclopeia (Od. 9) and the राक्षवध-s in the Mbh, these similarities are not limited to that epic -- it actually has cognates, which in some cases are even closer, in the रामायण. The key points of note are: 1) Both the rAkShasa-s and Polyphemus are eaters of human meat and are described as eating up their victims. This is very apparent in the case of baka. In the case of the rAkShasa विराध, वाल्मीकि informs us thus:\
[ददर्श गिरि-शृङ्गाभं पुरुषादं महास्वनं ।]{style="color:#0000ff;"}

 2.  This leads us to the second point of note. The yavana composers describe Polyphemus thus:\
"For he was a savage marvel, not resembling a bread-eating man but the wooded peak of the lofty mountains, when seen apart from others." (Od. 9.190-2; EBW's translation). Again the same phrase is used in describing the Laestrygonean ogress in Od. 10.113.\
This phrase is remarkably close to the[गिरि-शृङ्गाभं]{style="color:#0000ff;"}seen above. In the bhArata we again encounter the same again in the description of baka: adri-कूट-सदृशं; in the description of किर्मीर:[गिरिर् इवाछलः]{style="color:#0000ff;"} or [मैनाक इव पर्वत[ (like the मैनाक mountain).]{style="color:#004000;"}]{style="color:#0000ff;"}

 3.  In both the account of Polyphemus and those of the rAkShasa-s they tend to be ghastly and deformed and might have some ocular peculiarity. Here, the rAkShasa-s of the रामायण, encountered by rAma and लक्ष्मण in the forest, are much more cyclopean in their disposition than those encountered by वृकोदर in the Mbh.\
For example वाल्मीकि's account of विराध emphasizes it thus:\
[गभीराक्षं महा-वक्त्रं विकटं विकटोदरं । बीभतसं विषंअम् दीर्घं विकृतं घोरदर्शनं ॥]{style="color:#0000ff;"}\
His irregularities and ghastly nature along with the huge eye are emphasized (though it is not clear if it was one or two). However, in the case of the second rAkShasa, kabandha, encountered later in their journey his homology with the cyclopean Polyphemus is very clear:\
[रोमभिर् निशितैस् तीक्ष्णैर् महागिरिम् इवोच्छ्रितं । नील-मेघ-निभं रौद्रं मेघ-स्तनितनिः स्वनं ॥\
अग्नि-ज्वालानि-काशेन ललाट-स्थाने दीप्यता । महापक्षेण पिङ्गेन विपुलेनायतेन च ॥\
एकेनोरसि घोरेण नयनेन सुदर्शिना । महादंष्ट्रोपपन्नं तं लेलिहानं महामुखं ॥]{style="color:#0000ff;"}\
Here again we note that kabandha is compared to a great mountain. But more importantly he is said to be cyclopean with single large circular eye on his forehead which is endowed with a great eyelid and blazes forth like a fire. Then kabandha goes on to seize the two इक्ष्वाकु heroes to try to eat them, just as Polyphemus seized pairs of Odyesseans and ate them. Thus, the cyclopean imagery is clearly an ancestral motif inherited by the yavana-s and Arya-s. Hence, the omission of this important point that only further supports this case by EBW is surprising.

Several rAkShasa-s in Hindu lore are known by the name विरूपाक्ष, e.g. the friend of the crane killed by the fallen ब्राह्मण, the charioteer of घटोत्कच (also called गभीराक्ष, just like विराध), a general of रावण. The यक्ष who kills the पाण्डव-s at the lake is also termed विरूपाक्ष and also a baka or a crane. EBW translates विरूपाक्ष as one-eyed. There is no support what so ever from the Sanskrit sources for this claim. However, it does suggest that the rAkShasa-s were routinely characterized as having an ocular irregularity even if not being cyclopean. EBW notes the repeated reference to cranes in the context of rAkShasa-s: 1) the name baka, 2) the यक्ष of the death-pool being called baka; 3) the crane friend of विरूपाक्ष. Based on this she theorizes that the one-eyed nature is related the crane which in profile might appear one-eyed. Somehow, this does not seem sensible at all, though we agree there might have been some connection between rAkShasa-s and cranes. Further, she also connects the name ekachakra of the village conquered by baka with word "Cyclopes". Indeed chakra : cyclos are orthologous terms. Opes is from eye and eka means one; while this part of the compound is different she believes that the compounds as a whole are comparable. A tantalizing connection but we are on tenuous grounds here.

Finally, both the rAkShasa-s and the ogres of the yavana world come in two types. We have those like baka, हिडिंब, किर्मीर, विराध and kabandha who rove the forests and live as individuals or with a small group of attendants eating animals and humans. For example हिडिंब is described as living in a शाल tree and किर्मीर roaming carefree in the forest eating whomever he can catch. Similarly, Polyphemus is described in the Od 9 as a carefree herder who does not cultivate any plants lives of what ever the gods might bestow on him. In contrast rAkShasa-s like रावण, विभीषण and मेघनाद live as rulers in an island kingdom. Likewise, in the Greek world we have the Laestrygones living in a more organized fashion in an island kingdom under a great ogre king Antiphates. Thus, it appears that even ancestrally two types of ogres were distinguished. From all this information we can reconstruct the ancestral ogre of the Indo-Greek world.

*Additional motifs:* There are a bunch of additional motifs that appear to link the encounters between heroes and rAkShasa-s/ogres in the Hindu and Greek worlds. These include:

 1.  Food and/or drink is taken from the house of one belonging to the priestly caste to the place of the rAkShasa by the hero. In the Hindu world this is the ब्राह्मण family with whom the पाण्डु-s had sought refuge supplies the food and drink for baka. In return for the ब्राह्मण's hospitality the पाण्डव-s agree to help them by slaying baka. In the Greek narrative Odysseus helps a priest of Apollo, Maron and his wife and son. In return Maron gives him a flask of wine which Odysseus takes with him to Cyclopean island.

 2.  The hero eats the food of the ogre/rAkShasa. Both Odysseus and भीमसेन devour the food of the rAkShasa before their encounter with them.

 3.  There might have also been a motif of people taking turns to be eaten by the रक्ष/ogre in both the epics.

 4.  A tree is used as a weapon in maiming/killing the ogre/rAkShasa in both epics. While both Odysseus and bhIma are great bowmen and might also be famed in the use of other weapons like the sword or the mace in these encounters they primarily depend on a tree trunk as their weapon.

 5.  The dying or wounded ogre/rAkShasa utters loud yells and his attendants come out to see what has happened.

 6.  The concealment of the hero's name: In the case of Odysseus he tells the drunken Polyphemus that his name was nobody. In the case of bhIma, the पाण्डु-s instruct the ब्राह्मण to tell everyone that some unknown ब्राह्मण had come and slain the rAkShasa.

 7.  Polyphemus laments that his blinding at the hand of Odysseus had been foretold by a soothsayer known as Telemos. While such a prediction is not there in the encounters of bhIma, we do encounter such predictions in the case of both विराध and kabandha in the रामायण. This prediction of them being killed by rAma is a central aspect to the story in both those cases. EBW tries to force fit a prediction into the Mbh encounters, but clearly there is no evidence in the Mbh for such a thing.

 8.  EBW believes that there are several sacrificial motifs in the form किर्मीर being killed like a pashu in a याग, or the अरणि-s being stolen by the deer in the encounter with the यक्ष baka of the deadly pool. She compares them with the sacrifice to the gods performed by Odysseus on entering the realm of Polyphemus. None of these speculations are supported by the available evidence and I do not think there was any sacrificial motif in any of this.

In conclusion it is not just the Mbh rAkShasa encounters, but also those in the Ram that share motifs with the Greek Cyclopeia. We believe that both the Indo-Aryan इतिहास-s and the Greek Odyssey drew from a common pool of ancestral motifs concerning the encounter of a hero with a rAkShasa. This encounter had some specific elements and that were reused along with a more general set of features concerning the players in the narrative. In any case the ancestral rAkShasa/ogre was seen as one with some ocular irregularity, large in size (compared to a mountain), ghastly in appearance, a cannibal and often a freebooter living in the forest. There were other such entities who lived in island cities.


