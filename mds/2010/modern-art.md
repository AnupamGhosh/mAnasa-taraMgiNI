
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Modern art :-)](https://manasataramgini.wordpress.com/2010/04/21/modern-art/){rel="bookmark"} {#modern-art-- .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 21, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/04/21/modern-art/ "5:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh4.ggpht.com/_hjuA1bE0hBw/S8ph2Zao-1I/AAAAAAAABZc/gd_nh4_fQTY/s400/abstract001.png.jpg){width="75%"}
```{=latex}
\end{center}
```



There are certain things that pass of as art in the modern world but do not appeal to my pre-modern Hindu sensibilities. One day we saw a group of navya-विद्वान्-s discussing what they felt was art, like the monkeys of विष्णुशर्मन् sitting around the firefly hoping to ignite a pyre on a cold winter morning. So we decided to participate in their moha and produced the above piece for their discussion.


