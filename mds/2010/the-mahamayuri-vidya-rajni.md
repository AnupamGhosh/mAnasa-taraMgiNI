
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The महामायूरी-विद्या-राज्ञी](https://manasataramgini.wordpress.com/2010/05/10/the-mahamayuri-vidya-rajni/){rel="bookmark"} {#the-महमयर-वदय-रजञ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 10, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/05/10/the-mahamayuri-vidya-rajni/ "6:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh5.ggpht.com/_hjuA1bE0hBw/S_lmujepAFI/AAAAAAAABbg/GV0iEUfv7-A/s400/temp1.jpg){width="75%"}
```{=latex}
\end{center}
```



We had earlier mentioned how the महामायूरी-विद्या-राज्ञी (MVR), an early क्रिया tantra of the ताथागत-s, is an important text to understand the evolutionary transition in the mantra-शास्त्र from the vaidika to the तान्त्रिक state. Gathering texts that illustrate this transition is an important part of the study of the origins of the आगमशास्त्र. We were prompted to revisit the MVR when SRA, that purogava of the 4th वर्ण, sent us a copy of the shaiva-shAkta text termed the महाविद्या-स्तोत्रं. He correctly noticed that it might be an early text with considerable bearing on the emergence of the vana-दुर्गा system. We agree and feel that its core belongs to that basic old layer of texts going back to the transitional state (Hopefully at some point we will return to the discussion of this old layer of texts, including the महाविद्या stotra, one of the earliest तान्त्रिक पाञ्चारात्रिक mantra-s known as विष्णुमाय, the कौमार and the मातङ्गी mantra-s of the अष्टाङ्ग संग्रह and kAshyapa saMhitA, mantra-s of the अर्थशास्त्र, and the links to the mantra-s of the उड्डीश tantra tradition among others).

Many people have an incorrect understanding of the तथागत. They see him through the lenses of their own making -- a pure philosopher disinterested in ritual, a proto-Marxist, a social reformer or simply a saint in the mold of a much later version. But he was in reality quite different -- he saw himself as an insider to the vaidika tradition of the Arya-s who was redefining its structure, texts and worldview from within, albeit in a rather radical manner. As part of this activity he was on one hand a philosopher who debated with his rivals of मीमाम्सक and other परिव्राजक traditions and on the other hand, like them, he too was मन्त्रवादिन्. He sought to defeat them not just in philosophical debates but also emerge as a superior mantra-वादिन् with more powerful mantra-s. This latter aspect is apparent in his battle with the ब्राह्मण uruvela जटिल kAshyapa or the skull-tapping ब्राह्मण वङ्गीश. While in his subversive philosophy departed even farther from the mainstream than the मुण्डक उपनिषत्, it is likely that he was more conservative with his मन्त्रशास्त्र. While the तथागत probably subtly tried to encourage his own worship, in mantra practice for practical gains he still had to stick to the basics -- this is in fact reflected in his advice to the councilors of the वृजि गणराज्य to continue their rituals as ordained to the deva-s and to the ब्राह्मण-s to continue studying their veda. The MVR, coming early in the Sanskritic tradition of the ताथागत-s, is in large part an adaptation of the Astika मन्त्रशास्त्र. In this regard it tries to imitate both the proto-तान्त्रिक as well as the late vaidika systems.

The revelation of the MVR by siddhArtha occurred at the jetavana grove in the outskirts of श्रवास्ति when he was living in the vihAra built by his rich patron अनाथपिण्डक, in the midst of a vast assembly of disciples and bodhisattva-s. He had been joined by a new disciple स्वाति who decided to procure some wood to fuel the sauna bath of the bhikShu-s known as the जेन्ताक. While breaking a piece of dry wood a great black snake emerged and bit the thumb on his right foot. As a result he fell frothing from the mouth, with eyes rolling inwards and lost consciousness. Seeing him fallen thus Ananda, the cousin of the तथागत, rushed to the buddha and told him about the fate of the young new बिक्षु. To revive him तथागत revealed to Ananda the mantra-s known as the महामायूरी-विद्या-राज्ञी.Not only is the MVR notable for its antiquity among the nAstika tantra-s, but also for its importance in the context of the spread of the मन्त्रशास्त्र over Asia. In translation, the MVR acquired early popularity among the चीनाचर्य-s due to the efforts of कुमारजीव. Then it went on further to Korea and Japan. In the west a peculiar phenomenon is observed -- fragments of the MVR were absorbed within the syncretic Abrahamistic-Iranian framework of a strain of Manichaeism promulgated by the mAr ammO, the student of mani. Here the यक्ष-s of the MVR are invoked in an amusingly eclectic company with deities such as mani, the Messiah yEsu and a farrago of other Abrahamistic angels that would certainly impress a modern Hindu moron स्वामी. Not surprisingly, in this tradition mani himself was considered the bodhisattva maitreya. Thus, texts like the MVR might have had a key role in the introduction of तान्त्रिक concepts to the Middle East.

Right in the beginning after the customary invocation of the tathagata, the संघ, the dharma, the arhat-s and the bodhisattva-s headed by maitreya the MVR get to business by providing mantra-s to the actual deities required for its objectives. For example:

[कालि करालि कुम्भाण्डि शंखिनि कमलाक्षि हरीति हरिकेशि श्रीमति हरि हरिपिङ्गले लंबे प्रलंबे कालपाशे कालशोदरि यमदूति यमराक्षसि भूतग्रसनि प्रतीच्छथ मां गन्धं पुष्पं धूपं बलिं च दास्यामि रक्षथ मम सगण-परिवाराणां सर्व-सत्वानांश् च सर्व-भयोपद्रवेभ्यः जीवतु वर्ष-शतं पश्यतु शरदा-शतं सिध्यन्तु मे मन्त्रपदाः स्वाहा ॥]{style="color:#99cc00;"}\
While the 17 female deities invoked in this formula in the typical Astika पञ्चोपचार style are considered to be राक्षसि-s by the nAstika-s, it is clear that they are a primitive form of the system of 17 कालि-s that was to come of its own much later in the कालिकुल.

Then further ahead as the तथागत begins convey the mantra-s to Ananda we encounter formulae such as:\
[रात्रौ स्वस्ति दिवा स्वस्ति स्वस्ति मध्यंदिने स्थिते ।\
स्वस्ति सर्वं अहो-रात्रं सर्वबुद्धा दिशन्तु मे ॥]{style="color:#99cc00;"}

This mantra is a clear example of the nAstika imitation of a vaidika mantra which is duly termed svastyayana so that it could take the place of the vaidika svastyayana (Also compare with the pada of the trita Aptya mantra in RV 10.7.1: [स्वस्ति नो दिवो अग्ने पृथिव्या]{style="color:#99cc00;"}; also note the inscription of the above nAstika mantra in a blue porcelain jar of the Xuande period from chIna).

Then after mantra-s to female deities with flourishes of onomatopoeic low complexity \[e.g.[ॐ (मिलि + तिलि + छुलु + मुहु + मुलु+ हु + हुलु + वा + पा + जाल) * १०]{style="color:#99cc00;"}] the tathagata gets on with the worship of snakes. Here the nAstika-s are drawing from an old template that is seen in the sarpa-bali of the vaidika tradition (e.g. mantra-s of Apastamba mantra पाठ 2.17). Two prominent sarpa-s that are invoked in the MVR, धृतराष्ट्र and ऐरावत are also among the first to be invoked in the vaidika sarpa-bali. Likewise, another notable sarpa invoked in both the MVR and the sarpa-bali mantra-s is तक्षक. The MVR sarpa section has a mantra:\
[पृथिवीचराश् च ये नागास् तथैव जलनिश्रिताः ।\
अन्तरीक्षचरा ye cha ye cha meru समाश्रिताः ||]{style="color:#99cc00;"}

This clearly appears to be a nAstika paraphrase of renowned vaidika sarpa-bali mantra (or its ऐतिहासिक equivalent in the महाभारत):\
[नमो ऽस्तु सर्पेभ्यो ये के cअ पृथिवीम् अनु ।\
ये अन्तरिक्षे ये दिवि तेभ्यः सर्पेभ्यो नमः ॥\
ये ऽदो रोcअने दिवो ये वा सूर्यस्य रश्मिषु ।\
येषाम् अप्सु सदस् कृतं तेभ्यः सर्पेभ्यो नमः ॥]{style="color:#99cc00;"}

The below mantra from the MVR with the ma हिंसि also resembles similar vaidika formulae including those used in the initial fire ritual of the sarpa-bali:\
[मा मे अपादका हिंस्युर् मा मे हिंस्युर् द्विपादकाः ।\
मा मे चतुष्पदा हिंस्युर् मा मे हिंस्युर् बहुपादकाः ॥]{style="color:#99cc00;"}

Furthermore, we could also connect the नमः formulae ending the MVR sarpa section to the shveta vaidarva नमः formulate at the end of the sarpa-bali (with the irregularities of nAstika संस्कृत):\
[नमो ।अस्तु मुक्ताय नमो ।अस्तु मुक्तये ।\
नमो ऽस्तु शान्ताय नमो ऽस्तु शान्तये ।\
नमो विमुक्ताय नमो विमुक्तये ॥]{style="color:#99cc00;"}

This is followed by a group of low complexity mantra-s replete with onomatopoeic excesses that sound more like Gullah of the Carolinas (of course I mean this in half-jest). These are believed to stem from the peacock king who lives on the southern side of the Himalayas. Their possible emergence in the Tamil country is hinted by internal statement of "द्रामिडा मन्त्रपदाः" as well as specifics of the mantra:

[इलि मित्ति तिलि मित्ति तिलि मिलि मित्ति तिलि मिले मिलि तिलि मित्ति चिलि मिलि मिलि चिलि मिलि मिलि तिलि मिलि सुतुम्बा तुम्बा सुवच चिलिकिसिय भिन्न मेडि नमो बुद्धानां चिलिकिसि प्राप्तमूले इतिहारा लोहितमूले तुम्बा सुतुम्बा कुट्टि कुनट्टि तिल कुञ्ज नट्टि अडकवात्यायां वर्षतु देवो नव मासान् दश मासान् इति इलि मिलि किलि मिलि केलि मिलि केतुमूले दुदुम्बे सुदुम्बे सुदुमोडे दलिमे सन्तुवट्टे बुसवट्टे वुसर वुसर धनवस्त्रके नर्कला नर्कलिमे खलिमे घोषे रखिले इति सज्जले तुम्बे सुतुम्बे अट्टे नट्टे प्रनट्टे अणनट्टे अनमाले वर्षतु देवो नवोदकेन सर्वतः समन्तेन नारायणि पारायणि हरितालि कुन्तालि इलि मिस्ति किलि मिस्ति इलि किलि मिस्ति इलि मे सिध्यन्तु द्रामिडा मन्त्रपदाः स्वाहा ॥]{style="color:#99cc00;"}

\[elements like कुट्टि, अडकवात्यायां, अट्टे etc could be of Dravidian origin]

After more of such mantra-s, the tathagata is said to expound the famous विद्या of मातङ्गी. This goddess emerges early in a mantra in the kAshyapa saMhitA of the medical tradition. Subsequently, she has a long history in the Astika world as उच्छिष्ठ चण्डालिनि, a deity in the dasha महाविद्या system and also as the संगीत-योगिनी of the श्रीकुल system:\
[बले बल्कले मातङ्गि चण्डलि पुरुष निचि निचि निगौरि गन्धारि चण्डालि मातङ्गि मालिनि हिलि हिलि आगति गति गौरि गन्धारि कौष्ठिका वचरि विहारि हिलि हिलि कुङ्जे स्वाहा ॥]{style="color:#99cc00;"}\
The phase "gauri गन्धारि" also emerges in an early mantra of the पाञ्चरात्रिक-s, the विष्णुमाया.

Then the तथागत is said to launch into an enumeration of the यक्षसेनापति-s who govern the various तीर्थ-sthala-s of जम्बुद्वीप and provides one of the early expressions of the sacred geography of greater India, albeit through a nAstika lens. These यक्षसेनापति-s are in large part Astika temples and the specific deities housed in them. For example: वज्रपाणि (indra) was housed in a shrine in राजगृह (Bihar); garuDa in vipula; maheshvara in the किरात country; बृहस्पति in श्रावस्ति; महाकाल in वाराणसि; विष्णु in द्वारक; a famous कार्त्तिकेय shrine in rohitaka and another one of नेजमेष at पाङ्चाल; another indra at vaidisha; there was a lambodara (a विनायक?) in Orissa.

An onomatopoeic mantra attributed to indra from the MVR:\
[जला जन्तुले चापेटि जन्तुले मथनि घटनि ग्रसनि हरि हरि शिरि द्योति शिरे ततरु ततरु णबति सिंहा हा हा हा हा हा सिंहे धिति धिति कुरु कुरु शबरे वज्रे ज्योति तुट तुटसि बट बटसि सिलि सिलि कपिले कपिलमूले हा ही हूम् सर्वदुष्ट प्रदुष्टानां जंभन करोमि हस्त-पादाङ्ग-निग्रहं करोमि सह त्रिदशेहि देवेहि डटङ्गिनि सुरपतिवर्ति वज्र वज्र वज्र वज्र वज्र वज्र वज्रपतये स्वाहा ॥]{style="color:#99cc00;"}

One point to note in the MVR is the set of mantra-s known as the daNDa परिहार mantra-s. These mantra-s invoke यक्ष-s in the cardinal directions and call upon them to remove the daNDa-s bearing upon the beneficiary. The directional invocation follows an ancient pattern of mantra-s laid down in the veda wherein deities are invoked in different direction to protect the यजमान. But the daNDa परिहार mentioned in the MVR is specifically related to the mantra-s to ward off the effects of various divine daNDa-s seen in the वनदुर्गा tradition and its precursors. Further the same set of mantra-s also talk about the performance of "सीमाबन्धं" and "धरणीबन्धं" which are also paralleled in the mantra-s of the वनदुर्गा tradition in the form of elaborate dig-bandha-s.

Another point of note in the MVR is the repelling of various toxins by means of the weapons of the gods. The structure of this mantra clearly indicates that it was acquired from the Astika world with superficial buddhification by adding certain elements:\
[[हतं विषं निहतं विषं बुद्ध-तेजोहतं विषं प्रत्येक-बुद्ध-तेजोहतं विषं अर्हत्-तेजोहतं विषं अनागामि-तेजोहतं विषं सकृद्-आगामि-तेजोहतं विषं स्रोतापन्न-तेजोहतं विषं सत्य-वादि-तेजोहतं विषं]]{style="color:#99ccff;"}[ब्रह्म-दण्ड-तेजोहतं विषं इन्द्र-वज्र-तेजोहतं विषं विष्णु-चक्र-तेजोहतं विषं अग्नि-तेजोहतं विषं वरुण-पाश-तेजोहतं विषं असुर-माया-हतं विषं नाग-विद्या-हतं विषं रुद्र-शूल-तेजोहतं विषं स्कन्द-शक्ति-तेजोहतं विषं महामायूरी-विद्या-हतं विषं भूम्या संक्रामतु विषं स्वस्त्ययनं भवतु ॥]{style="color:#99cc00;"}

The first part is clearly a nAstika header which lists the various bauddha entities whose luster is invoked to smite the venom, such as: 1) the buddha; 2) the pratyekabuddha or the self-taught buddha who wanders like a rhinoceros; 3) the arhat who destroys his bonds to the world and attains nirvANa upon death; 4) the अनागामि who will reincarnate in a pure world and then attain nirvANa; 5) सकृद्-AgAmi who will be born once more on earth and then attain nirvANa upon death; 6) स्रोतापन्न who has broken three fetters recognized by the nAstika-s. These have been imposed upon the ancestral mantra that invokes various Astika देवता-s and their weapons to smite the venom.

Such adaptations of Astika material are rather common in the early nAstika तान्त्रिक material. One example of interest is the वसुधारा dhAranI, which the तथागत is supposed to have expounded as per mahAyAna tradition in कोशाम्बि. At its heart it is an adaptation of a लक्ष्मी साधान padded with a lot of nAstika periphrase:

[मूलमन्त्र: ॐ श्रिये श्रीकरि स्वाहा । ओम् धनकरि धान्यकरि रत्न-वर्षणि स्वाहा ॥\
साध्य-मन्त्र: ऒं वसुधारे स्वाहा ॥\
हृदयम्: ॐ लक्ष्म्यै स्वाहा ॥\
उपहृदयम्: ॐ लक्ष्मी भूतल-निवासिने स्वाहा । सं यथा दं ॐ यानपात्रावहे स्वाहा ॥\
मा दूरगामिनी अनुत्पन्नानां द्रव्यानाम्-उत्पादिनि उत्पन्नानां द्रव्यानां वृद्धिंकरि त्रुटे २ लिटे २ इत २ आगच्छागच्छ भगवति मा विलम्बं मनोरथं मे परिपूरय ॥\
दशभ्यो दिग्भ्यो यथोदक-धारा परिपूरयन्ति महीं\
यथा तमांसि भास्करो रश्मिना विध्यापयति चिरंतनानि\
यथा शशी शीतांशुना निष्पादयत्य् औषधीः ॥]{style="color:#99cc00;"}

[इन्द्रो वैवस्वतश्-cऐव वरुणो धनदो यथा।\
मनोनुगामिनी सिद्धिं cइन्तयन्ति सदा नृणाम् ॥]{style="color:#99cc00;"}

[तद्यथा। सुट २ खट २ खिटि २ खुटु २ मरु २ मुञ्च २ मरुञ्च २ तर्पिणि २ तर्जनि २ देहि २ दापय २ उत्तिष्ट २ हिरण्य-सुवर्णं प्रदापय स्वाहा ॥ अन्नपानाय स्वाहा । वसुनिपाताय स्वाहा । गौः स्वाहा । सुरभे स्वाहा । वसु स्वाहा। वसुपतये स्वाहा ॥\
इन्द्राय स्वाहा । यमाय स्वाहा । वरुणाय स्वाहा । वैश्रवणाय स्वाहा । दिग्भ्यो विदिग्भ्यः स्वाहा ॥]{style="color:#99cc00;"}

Here the nAstika retains the typical Astika deva-s to be worshiped in the cardinal directions rather than deploying the mahArAja-s.

Another later example is seen in the Arya-sarva-तथागतोष्णीष-सितात-पत्रा-नामपराजिता प्रत्यङ्गिरा:\
[नमो लोके अर्हतानाम् । नमः स्रोतापन्नानाम् । नमः सकृद्-आगामिनाम् । नमो अनागामिनाम् । नमो लोके सम्यग्-गतानाम् । नमः सम्यक्-प्रतिपन्नानाम् । नमो देवर्षीणाम् । नमो देव-ब्रह्मणे । नमो बुद्धाय । नमो भगवते रुद्राय उमा-सहिताय । नमो वरुणाय । नमो भगवते नारायणाय । महा-पञ्च-मुद्रा नमः नमस्कृताय । नमो भगवते नन्दिकेश्वर-महाकालाय । त्रिपुर-नगर-विद्रावण-कराय। अविमुक्तिक-कश्मीर-महा-श्मशान-निवासिताय । नमो मातृ-गण-सहिताय ।]{style="color:#99cc00;"}

Here the nAstika mixes in the Astika देवता-s rudra, वरुण, नारायण and specifically adapts a number of shaiva elements such as the pair of nandikeshvara and महाकाल and also the epithets of rudra such as the destroyer of the tripura-s, the one who lives in the great graveyards of Kashi and Kashmir and one accompanied by the hosts of मतृ-s. Thus, the above examples show how the MVR and related nAstika material displays an early phase of recycling of Astika material that continues along similar lines in the later phases of the nAstika development. Further, in addition to the देवता-s, we have several other elements of the Astika 'pantheon' also liberally incorporated in the MVR such as मातृका-s, ऋषि-s, पिशाची-s, राक्षसी-s and the like. In fact, the MVR list might be considered a survival of a relatively early version of a मातृका list from which the sapta-/अष्ट- मातृका list crystallized. The MVR list, explicitly described as द्वादश मातरः, goes thus:

[ब्राह्मी रौद्री कौमारी वैष्णवी ऐन्द्री वाराही कौवेरी वारुणी याम्या वायुव्या आग्नेयी महाकाली चेति ।]{style="color:#99cc00;"}

We observe that the great epic and its appendix (including the AryA-stava-s and the accounts of एकानंशा) do not mention the 7/8 मातरः at all. This clearly suggests that as a defined group they emerged after the महाभारत and हरिवंश. However, it should be noted that the महाभारत has long lists of मातृका-s associated with skanda. Another list of 13 मातृका-s are mentioned in a गृह्य ritual promulgated by the gobhila dharmaशास्त्र of the सामवेद tradition (1.11-12). Finally, we have an old list of goddess invoked in the ritual to the ग्ना-s and पत्नी-s in fr ऋग्वेद:

[उत ग्ना व्यन्तु देवपत्नीर् इन्द्राण्य् अग्नाय्य् अश्विनी राट् ।\
आ रोदसी वरुणानी शृणोतु व्यन्तु देवीर् य ऋतुर् जनीनाम् ॥]{style="color:#99cc00;"} (RV 5.46.8); See also (RV 1.22.12).

In the above mantra, the goddesses इन्द्राणि, अग्नायि, अश्विनी, rAT, रोदसी (the wife of the marut-s) and वरुणानी are invoked. Thus, various lists of मातरः, especially as female versions of the deva-s, have a long tradition in the Hindu world going back to the RV itself. The MVR appears to preserve one such list that was current in the post-Vedic period but prior to the crystallization of the Pauranic 7/8 list. The MVR list includes, in addition to the female forms of the usual vedic deva-s, those of the trinity, kubera, कुमार and importantly वराह's shakti as a distinct मातृ. In this respect it marks a step towards the emergence of the 7/8 मातृका list. It is from such a list, we suspect, that the 7/8 मातृका list arose by recombining the concept of the list of goddess with another old concept, i.e. the specific group of 7 goddesses. This latter concept is also is also seen multiply in the RV, e.g.:

[सप्त स्वसारो अभि मातरः शिशुं नवं जज्ञानं जेन्यं विपश्चितम् ।]{style="color:#99cc00;"} RV 9.86.36ab

Beyond the veda, this group of seven sister goddesses had a pervasive presence throughout जम्बुद्वीप in the form of the sapta bhagini cult amongst others. By addition of the primary trans-functional Indo-European goddess to the group of 7 we get the typical number 8. The remaining 7 were then filled up with the goddess drawn from a list like that in the MVR as per their primacy in the Pauranic pantheon. Thus we get: the trans-functional goddess 1) चण्डिका/महालक्ष्मी; the female forms of the trinity: 2) रौद्री 3) वैष्णवी 4) brAhmI; the female form of कुमार: 5) कौमारी; the only representative from the old Vedic list, the female form of indra: 6)ऐन्द्री; the distinctive female deities emerging in list like that in the MVR: 7) वाराही 8) चामुण्डा (=माहाकाली).

Another point of note in the MVR is the invocation of नक्षत्र-s. This pattern continues a tradition, which, as we have seen before, began in the vedic ritual of the नक्षत्रेष्टि. Thus, the ताथागत-s have composed rough parallels to the नक्षत्र सूक्त-s in their mantra material:\
[कृत्तिका रोहिणी चैव मृगशिरार्द्रा पुनर्वसुः ।\
पुष्यो मङ्गल-संपन्नो ऽश्लेषा भवति सप्तमी ॥\
इत्य् एते सप्त-नक्षत्राः पूर्व-द्वारिकास्थिताः।\
ये पूर्वां दिशं रक्षन्ति परिपालयन्ति ॥]{style="color:#99cc00;"}

Interestingly, a parallel नक्षत्र-सूक्त was transmitted to the Mongols. In the ruins of the Olon Sueme site in Inner Mongolia (now aggrandized by the chIna-s) we find fragments of such hymns. An example translated from Mongolian by Walther Heissig:\
Golden Suendi, who makes a single man into a hundred men ...;\
Star Buravabadara, who makes of one mare a thousand mares...;\
Star Aslis, who makes a simple sheep into a thousand white sheep...;\
Star Urukini, who makes a single head of cattle into a hundred red cattle...;\
Star Aburad, who makes one single camel into ten black camels ...;\
Star Raradi, who from one vegetable makes nine fields...;\
Star Molbar, who makes a poor man into a rich one ....

From behind the Mongolian we see faint echoes of their Indo-Aryan origin: Buravabadara= पूर्व-bhadra/प्रोष्ट-पाद; Aslis= अश्लेष; Aburad= अम्बुराट् (शतभिषक्?). This is also supported by the Mongol myth that the name of the mountain on which the polestar is anchored is supposed to be sumeru.

We finally come to two general issues raised by the MVR. The first of these is regarding the name itself -- why is this venom-countering विद्या named after a peahen deity. In जम्बुद्वीप, which abounds in venomous organisms, it has been long observed that several birds make a meal of these very venom producers. Of these the birds that are well-known to kill and feed on snakes and poisonous arthropods are the peacock and the crested serpent eagle. Not surprisingly both of them provide archetypes for the deities that are invoked to counter venom -- महामयूरी and garuDa. The former has a long history in this regard that goes back to the सूक्त-s of the RV and AV:\
[त्रिः सप्त मयूर्यः सप्त स्वसारो अग्रुवः ।\
तास्ते विषं वि जभ्रिर उदकं कुम्भिनीरिव ॥]{style="color:#99cc00;"} (RV 1.191.14)

Here not just the 21 peahens but also the 7 sister goddess are invoked to neutralize the venom. We believe that there is more to this mantra than meets the eye in the obvious sense.

[अदन्ति त्वा पिपीलिका वि वृश्चन्ति मयूर्यः ।\
सर्वे भल ब्रवाथ शार्कोटम् अरसं विषम् ॥]{style="color:#99cc00;"} (AV-vulgate 7.56.7)

[मयूरो ऽत्र वृश्चिकं मयूरं वयं विद्मसि ।\
तं परि परिजम्भनं वृश्चिक जम्भनम् असि ॥]{style="color:#99cc00;"}(AV-P 19.47.2)

[यस् तनुः पृथुर् वीणा वध इव प्रसर्पति ।\
मयूरः किल ते विषं कृकवाकुश् च जक्षतु ॥]{style="color:#99cc00;"} (AV-P 20.38.3)

Here peahens (and ants in the first verse) are invoked to render the venom ineffectual or eat up either of two different kinds of scorpions, the शर्कोट and the वृश्चिक. In the last verse the peacock is invoked along with the Grey junglefowl to consume the poison.

Thus, the MVR preserves a post-Vedic layer of the long tradition of venom-countering विद्या-s that originated in the vedic period. This poison-countering मयूरी tradition (including other fowl, like the Grey junglefowl mention in the AV mantra) appears to have existed parallel to the more famous garuDa tradition. It is in this regard it may be noted that a mantra of एकानंशा from the बर्बरीकोपाख्यान (skanda महापुराण) contains the epithet महामयूरी. Hence, we posit that behind the nAstika façade the MVR is at its core a text of the मयूरी tradition that was prevalent until around the 300s of the CE in Greater India. Early on the मयूरी tradition underwent syncreticism with the कौमार tradition. Perhaps the roots for this connection go back its Vedic antecedents via the link to the seven sisters, who in their new number six reappear in the कौमार system. In any case the peacock was taken up as the vehicle (मयूरवाहन) and the Grey jungle fowl or its relative the Red fowl as the banner of कुमार (कृकवाकु-dhvaja or कुक्कुट-dhvaja) from the earliest layers of the कौमार-शासन. Indeed, this link is also seen in the nAstika महामायुरी deities as in male or female form their iconography is rather close to that of कुमार or कौमारी.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh3.ggpht.com/_hjuA1bE0hBw/S_lvZe_AlrI/AAAAAAAABbo/g8ZJisOchcE/s400/महामायूरी.jpg){width="75%"}
```{=latex}
\end{center}
```



The second general issue is the geographic/ethnonym-based nature of deity names found in the MVR. The same names are also encountered in several Astika mantra-s:\
गन्धारी (from what is today Afghanistan); गौरी (from the Himalayas); द्राविडी/ द्रामिडी (from the Tamil country); शबरी/पर्ण-शबरी/ atharva-शबरी (from the forest of shabara tribes or the leaf-shabari); जांगुली (from the jungle); मातङ्गी (from the elephants); चण्डाली (from the outcastes). Such names have been interpreted in a very literal sense by several white indologists and their imitators. They have gone as far as to say that the mantra-शास्त्र originated from Dravidians, or due to western influence filtering through Afghanistan or from the "oppressed" forest tribes whose lands and holy spots were being "appropriated" by the "rapine" Hindus or that the outcastes like the chaNDAla-s were the pioneers of the tantra. Certainly both Astika and nAstika mantra-s have incorporated few elements of provincial languages like Tamil over the ages (as noted above). However, the above interpretations entirely miss the spirit of these words and deity names in the mantra-s. In fact, we may conclude that some of these white indologists and their shishya-s were specifically aiming at downgrading the mainstream Indo-Aryan culture and attempting to deny its creativity and accommodativeness by these claims. In reality we may observe that in the yajurveda itself we have rudra as being described as associated with various distant tribes such as niShAda-s, पुञ्जिष्ठ-s (tribal hunters) and other rapine tribes in the शतरुद्रीय. In the atharvaveda we have the discovery of new medicinal herbs being associated with forest tribes. So when these tribes are mentioned it is to illustrate the inclusive presence of a particular deity even in untamed zones beyond the "high civilization". Since these tribes might discover medicinal plants in the forest, they have a close association with the antidotes for poisons providing the archetypes for deities like मातङ्गी, the various शबरी-s and जंगुली who specialize in countering poison and curing disease. It should be noted that the deity atharva-शबरी is specifically derived from the archetype of the tribal girl (a कैरातिका कुमारि) from a mountainous region, who is described as discovering medicinal substances in the atharvaveda:

[कैरातिका कुमारिका सका खनति भेषजम् ।\
हिरण्ययीभिर् अभ्रिभिर् गिरीनाम् उप सानुषु ॥]{style="color:#99cc00;"} AV-vulgate 10.4.14= AV-P 16.16.4

So it is not the origin of mantra-शास्त्र from the tribals, rather imagery of tribal elements providing archetypes for the Indo-Aryans in their mantra शास्त्र. However, it is quite clear that as the tribes and the Indo-Aryans interacted with regard to trade of forest products the former to acquired elements of the mantra-शास्त्र and develop their own imitations of it. In terms of the toponymic deity names, such as द्राविडी and गन्धारी the main objective is to capture the sacred geography of Greater India -- both to indicate that a deity is present all over this region and also to specify that the deity is worshiped, perhaps in specific shrines, even in the border-zones (Afghanistan in the North and the Tamil country in the south) that are distant from the "middle country". Such toponymic descriptions of deities might also be observed in the fire hymns of the Mongols where the fire is invoked as the one which is worshiped in India or in China.


