
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [trayodasha-प्राकारीय पञ्चदशि कौमार chakra](https://manasataramgini.wordpress.com/2010/08/09/trayodasha-prakariya-panchadashi-kaumara-chakra/){rel="bookmark"} {#trayodasha-परकरय-पञचदश-कमर-chakra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 9, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/08/09/trayodasha-prakariya-panchadashi-kaumara-chakra/ "6:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh6.ggpht.com/_hjuA1bE0hBw/TA85M-TXh6I/AAAAAAAABew/8a8_DDxiXOY/s400/g3130.png){width="75%"}
```{=latex}
\end{center}
```



In the ancient series of विद्या-s of the कौमार-mata, one of the most celebrated, whose realization flashes like an immense wave of beauty and energy, is the expanded kula-विद्या. The याग of the expanded kula-विद्या is the worship of षण्मुख in the midst of the 13 प्राकार-s, each occupied a kula of 15 देवी-s. The central मण्डल is the षडश्र of skanda which encapsulates the षडक्षरी विद्या "OM vachadbhuve नमः". The six arNa-s of this मूल manu occupy the six points of the षडर chakra. The प्रणव occupies the bindu and the tryashra. The 15 देवी-s of each of the 13 kula-s occupy the dala-s of the 195-petalled padma (the देवी-s belonging to each kula are listed below). These mighty कुमाराङ्गना-s are conceived as young, shapely, joyous ladies of white, black, smoky, crimson, golden and coppery colors, with narrow waists, wearing white clothes. Some have beautiful anthropomorphic faces, whereas others are therocephalic or avicephalic. Additionally, outside the outermost of the 13 kula प्राकार-s, the देवयाजिन् invokes the following मातृ-s: यामी, रौद्री, saumyA, कौबेरी, वारुणी, माहेन्द्री, आग्नेयी, वायवी, कौमारी and brAhmI respectively in their assigned cardinal direction and at the zenith and nadir. These देवी-s are worshipped as assuming forms like beautiful अप्सरा-s \[Footnote 1]. These goddesses are, the मातृ kula-s, are said to dwell in trees, town squares, crossroads, caves, cremation grounds, mountains and springs \[Footnote 2]. It is in these places the ritualist places bali-s for them. Their voices are sweet like that of cuckoos, they are rich like kubera, powerful as indra and impetuous in battle like him, swift as वायु and blazing as agni.

The ritualist draws the मण्डल of 13 aratni-s in size and decorates it with leaves, flowers and mirrors and utters the following mantra-s as per the अथर्वण परंपर:\
With the following mantra-s he invokes कुमार:

[यं वहन्ति हयाः श्वेता नित्य-युक्ता मनोजवाः ।\
तम् अहं श्वेत-संनाहं धूर्तम् आवाहयाम्य् अहम् ॥]{style="color:#99cc00;"}\
Here he is conceived as riding on a white horse.

[यं वहन्ति गजाः सिंहा व्याघ्राश् cआपि विषाणिनः ।\
तम् अहं सिंह-संनाहं धूर्तम् आवाहयाम्य् अहम् ॥]{style="color:#99cc00;"}\
Here he is conceived as coming with a herd of elephants, lions, tigers and reptiles.

[यं वहन्ति मयूराश् cअ cइत्रपक्षा विहंगमाः ।\
तम् अहं चित्रसंनाहं धूर्तम् आवाहयाम्य् अहम् ॥]{style="color:#99cc00;"}\
Here he is conceived as coming riding a peacock with fowl of variegated hues.

[यं वहन्ति सर्व-वार्णाः सदायुक्ता मनोजवाः ।\
तम् अहं सर्वसंनाहं धूर्तम् आवाहयाम्य् अहम् ॥]{style="color:#99cc00;"}\
Here is seen coming on a chariot with horses of many colors.

[यस्यामोघा सदा शक्तिर् नित्यं घण्टापताकिनी ।\
तम् अहं शक्तिसंनाहं धूर्तम् आवाहयाम्य् अहम् ॥]{style="color:#99cc00;"}\
Here he is conceived as coming with a spear, bell and banners.

[यश् cअ मातृगणैर् नित्यं सदा परिवृतो युवा ।\
तम् अहं मातृभिः सार्धं धूर्तम् आवाहयाम्य् अहम् ॥\
यश् cअ कन्यासहस्रेण सदा परिवृतो महान् ।\
तम् अहं सिंहसंनाहं धूर्तम् आवाहयाम्य् अहम् ॥]{style="color:#99cc00;"}\
With the above two mantra-s he is invoked as coming together with the kula-s of मातृ-s laid out above.

[आयातु देवः सगणः ससैन्यः सवाहनः सानुचरः प्रतीतः ।\
षडाननो ऽष्टादश-लोcअनश् cअ सुवर्ण-वर्णो लघु-पूर्ण-भासः ॥]{style="color:#99cc00;"}\
He is finally worshipped with his hordes and mounts in the 6-headed 18-eyed form.

[आयातु देवो मम कार्त्तिकेयो ब्रह्मण्य-पित्रैः सह मातृभिश् cअ ।\
भ्रात्रा विशाखेन cअ विश्वरूप इमं बलिं सानुcअर जुषस्व ॥]{style="color:#99cc00;"}\
He is offered a bali along with his parents, brother विशाख and his troops.

[संविशस्व वरघण्टाप्सरःस्तवे यत्र सुभुजो हि निर्मिताः ।\
संविष्टो मे धेहि दीर्घम् आयुः प्रजां पशूंश् cऐव विनायकसेन ॥]{style="color:#99cc00;"}\
Finally with this mantra he is installed in the मण्डल middle of the मण्डल.

For performing the yogeshvara साधन, the ritualist then meditates on one of the षडक्षरी-s: "[ऒं वछद्भुवे नमः]{style="color:#99cc00;"} (as stated above)" or "[ऒं भुवे नमः स्वाहा]{style="color:#99cc00;"}" or if he has learned the अथर्वण shruti  "[OM ]{style="color:#99cc00;"}[स्वच्छन्दाय स्वाहा]{style="color:#99cc00;"}" or the सद्योजात mantra in its कौमार form.

Footnote 1: This is the oldest surviving system of the मातृका-s. They were modelled upon an old system of दिक्पाल-s. In this system the positions of the देवी-s are thus: माहेन्द्री (E); वारुणी(W); यामी(S); saumyA (N); रौद्री (NE); वायवी (NW); आग्नेयी(SE); SW(कौबेरी); brAhmI (Z); कौमारी (Nd). The बोधायनोक्त महान्यास has a similar system, except that it has निरृत्ति in the SW.

Footnote 2:\
[वृक्ष-चत्वर-वासिन्यश्-चतुष्पथ निकेतनाः ।\
गुहा श्मशान-वासिन्यः शैल-प्रस्रवणालयाः ॥]{style="color:#99cc00;"}\
देवी kula-s:\
प्राकार 1

 001. प्रभावती; 002)विशालाक्षी; 003)पलिता; 004)गोनसी; 005)श्रीमती; 006)बहुला; 007)bahu-पुत्रिका; 008)apsu-जाता; 009)गोपाली; 010)बृहद्-अम्बालिका; 011)जयावती; 012)मालतिका; 013)dhruva-रत्ना; 014)भयङ्करी; 015)vasu-dAmA;\
प्राकार 2

 016. सुदामा; 017)विशोका; 018)नन्दिनी; 019)एकचूडा; 020)महाचूडा; 021)chakranemi; 022)उत्तेजनी; 023)जयत्सेना; 024)कमलाक्षि; 025)शोभना; 026)शत्रुञ्जया; 027)क्रोधना; 028)शलभी; 029)खरी; 030)माधवी;\
प्राकार 3

 031. shubha-वक्त्रा; 032)तीर्थ-nemi; 033)गीत-प्रिया; 034)कल्याणी; 035)कद्रुला; 036)अमिताशना; 037)मेघस्वना; 038)भोगवती; 039)सुभ्रू; 040)कनकावती; 041)अलाताक्षी; 042)वीर्यवती; 043)विद्युज्जिह्वा; 044)पद्मावती; 045)सुनक्षत्रा;\
प्राकार 4

 046. कन्दरा; 047)बहुयोजना; 048)सन्तानिका; 049)kamalA; 050)महाबला; 051)सुदामा; 052)bahu-dAmA; 053)सुप्रभा; 054)यशस्विनी; 055)नृत्य-प्रिया; 056)शतोलूखल-मेखला; 057)shata-घण्टा; 058)शतानन्दा; 059)bhaga-nandA; 060)भागिनी;\
प्राकार 5

 061. वपुष्-मती; 062)chandra-shItA; 063)bhadra-काली; 064)सङ्कारिका; 065)निष्कुटिका; 066)भ्रमा; 067)chatvara-वासिनी; 068)सुमङ्गला; 069)स्वस्तिमती; 070)वृद्धि-कामा; 071)jaya-प्रिया; 072)धनदा; 073)सुप्रसादा; 074)भवदा; 075)जलेश्वरी;\
प्राकार 6

 076. एडी; 077)भेडी; 078)सुमेडी; 079)वेताल-जननी; 080)कण्डूति; 081)कालिका; 082)deva-mitrA; 083)लम्बसी; 084)केतकी; 085)चित्रसेना; 086)balA; 087)कुक्कुटिका; 088)शङ्खनिका; 089)जर्जरिका; 090)कुण्डारिका;\
प्राकार 7

 091. कोकलिका; 092)कण्डरा; 093)शातोदरी; 094)उत्क्राथिनी; 095)जरेणा; 096)महावेगा; 097)कङ्कणा; 098)मनोजवा; 099)कण्टकिनी; 100)प्रघसा; 101)पूतना; 102)खशया; 103)चुर्व्युटि; 104)वामा; 105)क्रोशनाथा;\
प्राकार 8

 106. तडित्प्रभा; 107)मण्डोदरी; 108)तुण्डा; 109)कोटरा; 110)मेघवासिनी; 111)सुभगा; 112)लम्बिनी; 113)lambA; 114)vasu-चूडा; 115)विकत्थनी; 116)ऊर्ध्ववेणी-धरा; 117)पिङ्गाक्षी; 118)loha-मेखला; 119)पृथु-वक्त्रा; 120)मधुरिका;\
प्राकार 9

 121. madhu-कुम्भा; 122)पक्षालिका; 123)मन्थनिका; 124)जरायु; 125)जर्जरानना; 126)ख्याता; 127)दहदहा; 128)धमधमा; 129)खण्डखण्डा; 130)पूषणा; 131)मणिकुण्डला; 132)अमोघा; 133)लम्बपयोधरा; 134)वेणु-vINA-धरा; 135)पिङ्गाक्षी;\
प्राकार 10

 136. लोहमेखला; 137)शशोलूक-मुखी; 138)कृष्णा; 139)खरजङ्घा; 140)महाजवा; 141)शिशुमार-मुखी; 142)श्वेता; 143)लोहिताक्षी; 144)विभीषणा; 145)जटालिका; 146)कामचरी; 147)दीर्घजिह्वा; 148)बलोत्कटा; 149)कालेडिका; 150)वामनिका;\
प्राकार 11

 151. मुकुटा; 152)लोहिताक्षी; 153)महाकाया; 154)hari-पिण्डी; 155)एकाक्षरा; 156)सुकुसुमा; 157)कृष्ण-कर्णी; 158)क्षुर-कर्णी; 159)चतुष्-कर्णी; 160)कर्ण-प्रावरणा; 161)चतुष्पथ-निकेता; 162)गोकर्णी; 163)महिषानना; 164)खरकर्णी; 165)महाकर्णी;\
प्राकार 12

 166. भेरी-svana-महास्वना; 167)शङ्ख-kumbha-स्वना; 168)भङ्गदा; 169)महारवा; 170)गणा; 171)सुगणा; 172)भीति; 173)कामदा; 174)चतुष्पथ-ratA; 175)भूरि-तीर्था; 176)अन्यगोचरा; 177)पशुदा; 178)वित्तदा; 179)सुखदा; 180)महायशा;\
प्राकार 13

 181. पयोदा; 182)गोमहिषदा; 183)सुविषाणा; 184)प्रतिष्ठा; 185)सुप्रतिष्ठा; 186)रोचमाना; 187)सुरोचना; 188)गोकर्णी; 189)सुकर्णी; 190)ससिरा; 191)स्थेरिका; 192)एकचक्रा; 193)megha-रवा; 194)मेघमाला; 195)विरोचना;


