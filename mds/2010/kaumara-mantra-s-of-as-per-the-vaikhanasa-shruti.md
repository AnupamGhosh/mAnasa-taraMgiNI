
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [kaumara mantra-s of as per the वैखानस shruti](https://manasataramgini.wordpress.com/2010/07/25/kaumara-mantra-s-of-as-per-the-vaikhanasa-shruti/){rel="bookmark"} {#kaumara-mantra-s-of-as-per-the-वखनस-shruti .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 25, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/07/25/kaumara-mantra-s-of-as-per-the-vaikhanasa-shruti/ "3:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/lh3.ggpht.com/_hjuA1bE0hBw/TE0r_w2GkzI/AAAAAAAAB3Q/S_HsF2qH3I0/s800/skanda.JPG){width="75%"}
```{=latex}
\end{center}
```



The कौमार-शासन holds that its mantra-s are of two types name shruti-प्रकृति and Agama-प्रकृति. The former mantra-s are those of vaidika origin. The [rahasya trishati](https://manasataramgini.wordpress.com/2009/09/15/kumara-trishati/) is said to specify mantra-s of both the classes.

[भूतपतिर् गतातङ्को नील-चूडक-वाहनः ॥ ३४/२८७]{style="color:#0000ff;"}\
The terminal quadrantistich of this shloka "नील-चूडक-वाहनः" encodes the shruti-प्रकृति-mantra known as the [अष्टाक्षरि विद्या](https://manasataramgini.wordpress.com/2007/05/30/the-code-of-the-ashtakshari-vidya/) (AV-Vulgate 20.132.16)

[वचद्भू रुद्रभूश् चैव जगद्भूर् ब्रह्मभूस् तथा । ३५/२९१\
भुवद्भूर् विश्वभूश् चैव मन्त्रमूर्तिर् महामनुः ॥ ३५/२९५]{style="color:#0000ff;"}\
This shloka is said to specify shruti-प्रकृति skanda महामन्त्र-s. What is immediately apparent is that it encodes the six षडक्षरि mantra-s that are associated with the [shaktyuta मूर्ति yantra](https://manasataramgini.wordpress.com/2007/08/19/kaumara-yantra-s-according-to-kaumara-tantra-sara-and-kumaradi-mantra-samuchchaya/). But what is the shruti-प्रकृति being referred here? The answer is of considerable interest both in terms of the mantra-शास्त्र and also in terms of understanding the early transmission of the कौमार शासन to the southern country. The shruti alluded to here is the वैखानस shruti. The वैखानस mantra prashna (6.24.1-25.1) gives the six basic mantra-s for the वैखानसोक्त skanda याग that is performed on the bright षष्ठि in the months of फाल्गुनि, अषाढ and कृत्तिक:

[जगद्-भुवं बहुतो हुतं यजद्-भुवन् [वचद्-भुवन्] नमस् ते अस्तु विश्व-भुवे स्वाहा ॥ ६।२४।१\
जगद्-भुवो ऽधिपतिस् सेनानीर् मयूर-प्रियष् षडाननाङ्गारः सञ्जयाह नमस् ते ऽस्तु सुखावहाय स्वाहा ॥ ६।२४।२\
सुब्रह्मण्यो बृहस्पतेस् सुतायास्य पद्म-योनेः यस्यात्मा वहने वहति स्वाहा ॥ ६।२४।३\
सुब्रह्मण्यो रुद्र-भुवो ब्रह्म-भुवः भवोद्भवो ममामृताय स्वाहा ॥ ६।२४।४\
जगद्-भुवस् सुब्रह्मण्यः कृत्तिका-सुतः षष्टिकाय स्वाहा ॥ ६।२४।५\
जगद्-भुवो यो यजद्-भुव स्कन्द-विशाख-नेता हव्यम् अस्मात् षण्मुखाय स्वाहा ॥ ६।२५।१]{style="color:#0000ff;"}

Then the offerings are made to the kula of skanda-मातृ-s (the mantra-s are given after the दुर्गा homa mantra-s in the VMP 6.27.1-28.1):\
[नन्दिन्या मूलवत्या जगतो वसत्या ज्येष्ठाया अस्तु सुहुत(ग्)ं हुतम् स्वसुर् देव्या धरित्र्या रजत-प्रियायास् स्वाहा ॥ ६।२७।१\
आपो विवेश प्रजया ब्रह्म-दारास् स्कन्द-प्रबर्हं भरणे विद्म देव्याः वेगिनी देवी शर्म देयात् सदा नस् स्वाहा ॥ ६।२७।२\
शाखा-भूत सुशाखया दिवो भेत्तुम् अयूयुजत् धाता देव्या यजाम होमाग्र्यां तत्र वाहिन्य् उदेतु स्वाहा ॥ ६।२७।३\
देवी प्रवाहिन्य् अनिशं पाति-भूत(ग्)ं सर्वं छन्दो विकीरयन् भजतो मे शिवं धत्तां जया वृत्तं जरयत्व् आधिम् एषा स्वाहा ॥ ६।२७।४\
बिसिनी भूता जननीवा सृजन्त्य् उतावनीता ब्रह्मण्य् अध्य् उपास्ते तां विद्युता(ग्)ं सम्यग् उ तर्पयामि स्वाहा ॥ ६।२७।५]{style="color:#0000ff;"}\
[प्रविद्युताया सुमनो-धरा चामुं विरञ्जत्य् अत एष पन्थाः तस्याभवत् विन्दु-करानुरञ्जो जुहोमि सा नो शिवदा विधेया स्वाहा ॥ ६।२८।१]{style="color:#0000ff;"}

arghya is offered with:\
[तत्-पुरुषाय विद्महे महासेनाय धीमहि । तन् नष् षण्मुखः प्रcओदयात् ॥८।१।१९५।१]{style="color:#0000ff;"}

In the first mantra the editions prepared by the कुंभघोणं वैखानस-s as well as that by the American scholar Resnick read as "yajad-bhuvan". However, at least one वैखानस from the Tamil country reads this as vachad-bhuvan. The reading vachad-bhuvan is also seen in the form of this mantra preserved within the सुब्रह्मण्य प्रतिष्ठ of the shaiva सैद्धान्तिक tradition. Hence latter reading could have also been a valid, especially given that it specifies the base of one of the six षडक्षरि mantra-s in its typical form ([ऒं वछद्भुवे नमः ॥]{style="color:#0000ff;"}). In the sextet of षडक्षरि-s one of the 6 mantra-s is typically given as ["भुवद्भुवे नमः ॥]{style="color:#0000ff;"}" in later तान्त्रिक sources. In all manuscripts and the oral tradition of the वैखानस-s the cognate name is read as bhavodbhava. This corresponds to the same name used in the mantra "[सद्योजातं प्रपद्यामि...]{style="color:#0000ff;"}" which is a skanda mantra in the gopathokta अथर्ववेदीय skanda याग. Similarly, the bali mantra deployed in the बोधायनोक्त धूर्त homa of the classical तैत्तिरीयक-s uses the name bhavodbhava. These points suggest that the original name was probably bhavodbhava, which was transformed to bhuvadbhuva to fit the form of the remaining mantra-s to constitute the षडक्षरि sextet. While mantra-s related to rituals to skanda and षष्ठी were widespread across different shAkhA-s in the late Vedic period, the earliest indications of the कौमार मूल षडक्षरि-s are currently observed only within the तैत्तिरीय tradition. An indication that these mantra-s were based on "भू" is supported by the form the षडक्षरि of the classical तत्तिरीयक-s for offering oblations in the धूर्त homa: "[भुवे नमः स्वाहा ॥]{style="color:#0000ff;"}". But what makes the वैखानस mantra-s important is that they are the earliest indication of the complete base forms of the षडक्षरि sextet. Further, this set of six mantra-s specify the 31 कला-s of the कौमार mantra-शास्त्र that are used in the सर्वाङ्गन्यास, just as the कला-s of the पञ्चब्रह्मा are used in the shaiva tradition.

The origin of these celebrated कौमार mantra-s in association with the वैखानस-s is also consistent with other elements of कौमार worship recorded in their tradition. As we noted [before](https://manasataramgini.wordpress.com/2007/02/19/vaikhanasa-childhood-rites-pertaining-to-kumara/), the वैखानस-s invoke कुमार during the नामकरण blessings and in their specific ritual of the child's first outing, where it is taken to the कुमार shrine. These points are of particular interest when considered in relation to the fact the वैखानस-s were the earliest innovators within the तैत्तिरीयक tradition who adopted the iconic worship of विष्णु in temples. They appear to have moved to the south carrying with them an early form of the vaiShNava-mata and appear to have been involved in the emergence of early विष्णु shrines in south India during. From a historical perspective, we find a geographical association between the iconic worship of विष्णु (in the पाञ्चरात्रिक form and probably also in the वैखानस form) and that of कुमार at several sites where early temples have been recorded: 1) Mathura in Aryavarta and 2) The region centered on Nagarajunakonda during the Andhra, इक्ष्वाकु and विष्णुकुण्डिन् period in दक्षिणापथ. In light of these observations it appears likely that the vaiShNava-s were in proximity with the कौमार-s in this period. Even earlier evidence for such a link is also seen in the early कौमार myths recorded in the महाभारत. For example, in the कौमार narrative of मार्कण्डेय (the oldest extant narration of the कौमार myth) one of the names of कुमार is vAsudeva-priya. Similarly, in the narrative of the [discomfiture of प्रह्लाद](https://manasataramgini.wordpress.com/2005/08/07/the-fall-of-prahlada/) we find that विष्णु and कुमार have an "understanding" and recognition of each others might.

Today in the drAviDa country most कौमार shrines are officiated by सैद्धान्तिक shaiva-s. However, there are reasons to believe that this is unlikely to have been the case in earlier times: 1) the worship of कुमार was already in place even before the सैद्धान्तिक-s had become a dominant force in the Tamil country. There are pallava kings with कौमार names even before the first pallava to receive the सैद्धान्तिक दीक्ष (In fact we find विष्णु and कुमार to be the main deities in the names of the early pallava-s, much like the rulers further north in the Andhra country. A similar association is also seen in the inscriptions of their later rivals, the चालुक्य-s, e.g. the Surat copper plates of श्र्याश्रय शिलादित्य.). 2) We find that कुमार was a prominent deity in the relatively early layers of drAviDa poetry. The dating of the drAviDa anthologies is controversial. However, as KAN Shastri noted, we find that the pallava-s are hardly noticed in the early drAviDa collection termed the एट्टुत्तोकै (the 8-fold collection), which includes the परिपाटल्. Though the परिपाटल् is considered to be one of the latest texts in this collection, it is still likely to have been composed in large part before the pallava dominance of the drAviDa country and its proposed dates of \~250-400 CE seem likely. This was certainly before any south Indian ruler known to receive सैद्धान्तिक दीक्ष. The परिपाटल् has two prominent deities कुमार (8 poems) and विष्णु (6 poems), again reinforcing their old association. The exquisite कौमार poems of this text encapsulate versions of the mythology known from various sources in the देवभाष and importantly mention the पाण्टियन् ruler's visit to a कौमार shrine near Madurai (identified as the original temple at तिरुप्परङ्कुङ्रम्). 3) The survival of the skanda-सद्भाव tantra in the Tamil country. This is an early कौमार tantra with iconographic features typical of the gupta age, which does not show any सैद्धान्तिक influence. This suggests that there is memory of a pre-सैद्धान्तिक system of temple worship of कुमार.

In the north in the post-हर्षवर्धन period there appears to have been an explosive expansion of the मन्त्रमार्ग shaiva streams at the expense of the कौमार शासन (perhaps even catalyzed by the former via emphasis on negative myths). Hence, in the north the सैद्धान्तिक-s largely sought to absorb कुमार within the shaiva frame work rather than explicitly promote him. For example, in the Chandrarehe stone inscription (verse 10) from the kalachuri kingdom of Tripuri the सैद्धान्तिक Acharya prabodha-shiva deshika is compared repeatedly with कुमार, who is described as faithfully carrying out the duties in worshiping his father shiva. Following this absorption, in the north we find that there was a decline in the popularity of कुमार along with the concomitant rise of विनायक (as assessed by inscriptions). Finally, the Islamic depredations largely ended his memory in large parts of the North. But in the south due the strong presence of multiple कुमार shrines from even before the establishment the सैद्धान्तिक-s he was not displaced, but simply co-opted via the action of the सैद्धान्तिक deshika as a सर्वाधिकारी. An important player in this process was the great सैद्धान्तिक scholar aghorashiva deshika (1100s of CE) who composed a manual known as the सुब्रह्मण्य प्रतिष्ठाविधि, which provides the कौमार temple installations under a सैद्धान्तिक framework. In this framework the षडक्षरि sextet is combined with the faces of the 5 brahma mantra-s:\
[ॐ इशान-मूर्धाय जगद्भुवे नमः ।\
ॐ तत्पुरुष-वक्त्राय वचद्भुवे नमः ।\
ॐ अघोर-हृदयाय विश्वभुवे नमः ।\
ऒं वामदेव-गुह्याय रुद्रभुवे नमः ।\
ऒं सद्योजाय-मूर्तये ब्रह्मभुवे नमः ।\
ऒं सर्व-वक्त्र भुवद्भुवे नमः ।]{style="color:#0000ff;"}

Likewise other सैद्धान्तिक elements are worked into various aspects of the कौमार ritual.

In light of the above discussion the links of the कौमार शासन to the वैखानस-s assumes considerable significance. It appears likely that an offshoot of the वैखानस-s or a related parallel stream of ब्राह्मण-s specializing in the temple worship established the system of कौमार temple worship in south India. Like the वैखानस-s who bore the temple worship of विष्णु to the south this group is also likely to have built on the household iconic worship of कुमार as specified in the general तैत्तिरीय tradition to develop it for temple worship. It is possible that Mathura was a common center where the temple worship developed among both the vaiShNava-s and कौमार-s. From there these groups appear to have pushed further south together reaching the region around Nagarjunakonda and finally the Tamil country. This would explain the origin of the early of the skanda shrines in the southern country along with those of विष्णु referred to in the songs of the परिपाटल्. Finally, it may also explain the faint folk memories that the main वैखानस shrine at Tirumala is alternatively a कौमार shrine. Indeed the वैखानस officiants appear to acknowledge this in their अष्टोत्तर-shata-नामावलि of वेङ्कटेश्वर, where he is called: "[कार्त्तिकेय-वपुधारिणे नमः ।]{style="color:#0000ff;"}". It is not impossible that at some point there was also a presence of the वैखानस associated कौमार-s in this place.


