
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The oldest surviving planetary ritual of the Hindus](https://manasataramgini.wordpress.com/2010/02/18/the-oldest-surviving-planetary-ritual-of-the-hindus/){rel="bookmark"} {#the-oldest-surviving-planetary-ritual-of-the-hindus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 18, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/02/18/the-oldest-surviving-planetary-ritual-of-the-hindus/ "7:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Certain white Indologists and their Japanese imitators have often held the dishonest view that Hindus were unaware of planets until the yavana-s informed them of their existence in the sky. This view was demolished by the great patriot of the Hindu nation Lokamanya Tilak about a century ago. In fact he showed that the proto-Indo-Europeans (leaving out the Hittites) certainly knew of the brightest planet in the sky. Of course this is encoded in the form the venas recitation with the shukra and manthin graha-s. But a question still remains: Does any relatively early vedic rite specifically directed at the planets exist and survive. One example of this ग्रहेष्टि of the kaTha yajurvedin-s:

**ग्रहेष्टि ब्राह्मण**

[देवाश् च वा असुराश् च समावद् एव यज्ञे 'कुर्वत यद् एव देवा अकुर्वत तद् असुरा अकुर्वत ते देवा एतानि ग्रहहविंष्य् अपश्यं स् तैर् इन्द्रम् अयाजयंस् तद् असुरा नान्ववायंस् ततो देवा अभवन् परासुरा अभवन् य एवं विद्वान् एतानि ग्रहहवींसि यजते भ्रातृव्यस्य् आनन्ववायाय भवत्य् आत्मना परास्य भ्रातृव्यो भवत्य् आदित्याय घृते चरुं निर्वपेत् तेजस् तेन परिक्रीणाति शौक्रं चरुं ब्रह्मवर्चसं तेन परिक्रीणाति बृहस्पतये नैवारं पयसि चरुं वाक्पत्यं तेन परिक्रीणाति बुधाय नवकपालं बुद्धिं तेन परिक्रीणाति भौमायैककपालं यशस् तेन परिक्रीणाति सौराय पललमिश्रं घृते चरुं सुरभिं तेन परिक्रीणाति चन्द्रमसे पञ्चदश कपालं आयुस् तेन परिक्रीणाति राहवे चरुम् अभयं तेन परिक्रीणाति केतवे चरुम् अनपरोधं तेन परिक्रीणात्य् एतान्य् एव सर्वाणि भवति य एवं विद्वान् एतया यजेत।\
आज्येनोपहोमाञ् जुहोत्य् आशिष् आम् अवरुद्ध्या एतया यजेत यः कामयेत तेजस्वी भ्राजस्वी वाक्पतिर् बुद्धिमान् यशस्वी सुरभिर् आयुष्मान् अभय्यनपरोधी स्याम् इत्य् एकचक्रम् उदयाद् भ्राजमानम् इत्य् अष्टादश याज्यानुवाक्या भवन्ति सरूपत्वायाग्निर् हिरण्यं सोमो हिरण्यम् इत्य् आज्यभागौ प्रेद्धो अग्न इमो अग्न इति सं याज्ये उच्चैर् यजत्य् एषा वै वाचाम् उत्तमा योच्चैर् उत्तमः समानानां भवत्य् आदित्यस् तेजस्वीत्य् उपहोमञ् जुहोति सर्वस्याप्त्यै सर्वस्यावरुद्ध्यै य एवं विद्वान् एतया यजेत॥]{style="color:#0000ff;"}

**ग्रहेष्टि mantra-s**

[अदित्यस् तेजस्वीत्य् उपहोमाञ् जुहोति।\
आदित्यस् तेजस्वी तेजो अस्मिन् यज्ञे यजमाने दधातु॥१॥\
शुक्रो भ्राजस्वी भ्राजो अस्मिन् यज्ञे यजमाने दधातु॥२॥\
बृहस्पतिर् वाक्पतिर् वाचो अस्मिन् यज्ञे यजमाने दधातु॥३॥\
बुधो बुद्धिमान् बुद्धिम् अस्मिन् यज्ञे यजमाने दधातु॥४॥\
अर्को यशस्वी यशो अस्मिन् यज्ञे यजमाने दधातु॥५॥\
सौरः सुरभिः सुरभिम् अस्मिन् यज्ञे यजमाने दधातु॥६॥\
चन्द्रमा आयुष्मान् आयुर् अस्मिन् यज्ञे यजमाने दधातु॥७॥\
राहुर् अभयम् अभयम् अस्मिन् यज्ञे यजमाने दधातु॥८॥\
केतुर् अनपरोध्य् अनपरोधम् अस्मिन् यज्ञे यजमाने दधातु॥९॥\
ध्रुवो धैर्यवान् धैर्यम् अस्मिन् यज्ञे यजमाने दधातु॥१०॥\
अगस्त्यो वीर्यवान् वीर्यम् अस्मिन् यज्ञे यजमाने दधातु॥११॥\
प्रेद्धो अग्ने दीदिहि पुरो नो यत्र देवाः पूर्वे पुराणाः ॥\
यत्रासौ वैश्वानरः स्तोकातिथिस् तत्रेमं यज्ञं यजमानं च धेहि॥१॥\
इमो अग्ने वीततमानि हव्या प्रेदं हविः प्राष्ट्रेमान् स्तोकान्।\
स्तोकातिथिः स्तोकजूतिः पतत्र्य् अथा ह्य् अग्ने अमृतत्वं च धेहि॥२॥]{style="color:#0000ff;"}

The above ritual is of interest because it provides an important link in the evolution of planet related rituals in the vedic system. To understand this we have to look a little more into the worship of astronomical entities in vedic rites. In the ऋग्वेद we do not have any सूक्त dedicated to the नक्षत्र-s. However, in the तैत्तिरीय ब्राह्मण we have a long new सूक्त composed for the purpose of the नक्षत्रेष्टि. Comparable सूक्त-s were composed in the atharvanic tradition, whereas the kaTha-s merely culled mantra-s from the saMhitA to create the liturgical recitations for their नक्षत्रेष्टि ritual (as we have seen before on these pages). An important point to note is that these नक्षत्र rituals begin with कृत्तिका at the spring equinox. However, from Tilak's studies, and from other related lines of evidence, we can see that the core of the RV was not associated with the कृत्तिका-s, but with earlier asterisms of रोहिणी or मृगशीर्ष. Hence, it does appear that the नक्षत्र liturgy was an innovation of the कृत्तिका period. However, the structure of the soma ritual suggests that there was an earlier नक्षत्र recitation in the consecration of the pebbles of the asterisms in laying the fifth layer of brick of the chiti. Since the recitations for this act are found in the YV and again begin with कृत्तिका one could argue that this and perhaps a good part of the chiti recitations were post-RV compositions of the adhvaryava tradition of the YV. None of this indicates that the नक्षत्र system came into being de novo in the core YV period. We do have the Iranian list of 28 नक्षत्र-s that was originally used, before being supplanted by the zodiacal system, probably after the Iranians moved into the Middle East. While the complete Iranian नक्षत्र list only survives in the Pahlavi bundahishn, we have some avestan names supporting their old origins. Further we can reconstruct that at least some of the names of asterisms probably predated the Indo-Iranian split. Thus, based on this we can infer the following: 1) There was a नक्षत्र list predating the Indo-Iranian divergence. 2) Both in the core RV and Avesta period there was no dedicated ritual to the नक्षत्र-s. However, they were collectively or by specific names referred to within recitations in this period. 3) In the beginnings of the elaborate shrauta ritual of the agni chiti-s they were incorporated into the ritual of laying of pebbles. 4) In the core YV/later AV period full-fledged rituals dedicated to the deities in conjunction with their associated नक्षत्र-s emerged. 5) In parallel, the Iranians developed later liturgy for specific asterisms (perhaps as euphemisms for the deities associated with them, because the deities themselves were demonized by ज़रथुष्ट्र). We see these developments as internal to the Indo-Iranian world. At least in the Indo-Aryan world the development was largely a consequence of the increasing calenderical emphasis in the shrauta ritual.

Understanding this evolution of the नक्षत्र rituals is important because the case of the planets in Indo-Aryan rituals is similar in many ways though the details might differ. Firstly, unlike the नक्षत्र-s at least two planets had dedicated सूक्त-s to them in the older saMhitA collections. The most well-known of these are the सूक्त-s to vena in the RV (10.123) and AV (vulgate 2.1) and that to both vena and बृहस्पति in AV (vulgate 4.1). While बृहस्पति has additional non-planetary dimensions and is a central deity in several rituals, vena appears to be a purely invoked in specific ritual actions. For example the primary use of the vena सूक्त from the RV was in the deployment of the shukra and manthin soma cups in the सोमयाग. As per AV tradition vena is invoked in the upavastha rite, the gharma recitation in the atharvanic pravargya, and the laying down of the gold biscuit in the agnichayana ritual. In the longer gharma recitation given in the AV-पैप्पलाद saMhitA (AV-P 16.150) we take the word graha to mean planet as it occurs with terms like shukra and पञ्चारे chakra in the same सूक्त. Two sAman-s to shukra/vena are deployed during the piling of the altar during the agnichayana. In a royal atharvavedic ritual of leading the king to his palace a recitation is deployed which invokes the planets as a group along with राहु and the ketu-s (AV-vulgate 19.9). Even though there might have been early सूक्त-s dedicated to at least two planets, it should be noted that their primary original role was, as with the नक्षत्र-s, in special actions in the shrauta rituals. Thus, neither नक्षत्र-s nor planets had a dedicated ritual in the earliest layers, but they were certainly known and referred to in ritual contexts.

However, in the case of graha-s we see a profusion of rituals appearing the late vedic period as a part of the terminal sections of the गृह्य texts. Examples of these include the जैमिनीय गृह्यसूत्र 2.9; वैखानस गृह्यसूत्र ग्रहशान्ति of 1.4.13; and the shanti-kalpa of the atharvan-s; graha homa of the kaTha-s usually appended after the ग्रहेष्टि as a गृह्य rite; the बोधायनीय गृह्य-sheSha sUtra 1.6. Their late character is established by the nature of the ritual, which often has elements of the स्मार्त सर्वोपचार पूज and use of images made of wood or metal or drawings of the planetary archons. They sudden appearance across the गृह्य texts as late sections lent fuel to the indological assertion that they were Babylonian or Greek imports that were brought into the Indic world along with Greek/Babylonian astrology and weekdays. However, the kaTha ब्राह्मण and mantra-s with distinct names of the planets and also older ritual structure suggests that what ever the role of the Middle Eastern influence, the graha rituals certainly had an endogenous element.Importantly, it shows that just as in the case of the नक्षत्र-s the dedicated shrauta ritual to the planets arose as a part of the new set of shrauta performances that were instituted during the compilation of the yajur collections. Thus the planetary and नक्षत्र rites follow the same pattern -- their mantra-s had an early role as accessories rather than as deities in the early shrauta ritual, but the in the late shrauta ritual they might receive full-fledged rites.

Finally, we must note that the names of the planets are some what atypical in the kaTha ritual and appear to represent the older form of their names. The name arka for Mars is intriguing. It should be noted that this name is often used for the sun, but clearly in this case it means Mars and not the sun. The word arka is also used for Mars in the बोधायनीय गृह्यशेष sUtra-s. This raises the possibility that indeed other usages of arka in the shruti could also signify Mars rather than the sun. Indeed, I suspect that this is the case in certain RV mantra-s. Most likely candidates are the references found in the सूक्त-s to बृहस्पति in the मण्डल 10 by the ऋषि अयास्य आञ्गिरस. In RV10.67.5 we encounter:\
[विभिद्या पुरं शयाथेम् अपाचीं निस् त्रीणि साकम् उदधेर् अकृन्तत् ।\
बृहस्पतिर् उषसं सूर्यं गाम् अर्कं विवेद स्तनयन्न् इव द्यौः ॥]{style="color:#0000ff;"}\
Here बृहपति is described as splitting the fort and the "sleeping places" in the west (of vala) as he cut off three joints of the "water holder". Doing this and roaring like dyaus बृहस्पति "discovered" (viveda) the उषा-s, sUrya, the cows and arka. While this ऋक् is extremely obscure in terms of its cryptic allusions there are several motifs that clearly indicate it as concealing an astronomical picture. Further, the बृहस्पति of this सूक्त does have a strong astronomical dimension as the planetary deity. In any case the point of importance to us in this context is that it clearly distinguish arka from sUrya, thus indicating that arka is not the sun, but could be Mars, who is named thus in the ग्रहेष्टि. This gets further support in a ऋक् in the subsequent सूक्त also to बृहस्पति:\
[आप्रुषायन् मधुन ऋतस्य योनिम् अवक्षिपन्न् अर्क उल्काम् इव द्योः ।\
बृहस्पतिर् उद्धरन्न् अश्मनो गा भूम्या उद्नेव वित्वचं बिभेद ॥]{style="color:#0000ff;"}\
While there are obscure elements in this ऋक् too, it is said that बृहस्पति drew out the cows from the rock and split the earth like the skin of a cloud even as arka hurled down a meteor and showered the source of the ऋत with honey. This meteor fall refers to the night sky and not the day sky of the sun supporting the identification of arka in this context as Mars.


