
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Visualizing शिवाजी's स्वराज्य](https://manasataramgini.wordpress.com/2010/08/29/visualizing-shivajis-svarajya/){rel="bookmark"} {#visualizing-शवजs-सवरजय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 29, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/08/29/visualizing-shivajis-svarajya/ "7:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Long ago in my school days I accessed a book by a marATha scholar in which he had carefully catalogued all the battles of शिवाजी. These battles comprise a most remarkable series of events in the struggle of the Hindu nation against the army of Islam. So I decided to visualize them in different ways and drew a series of graphs. I hope to recreate some of these here to review that journey.

The first graph illustrates the chronological density of the battles that were fought during the foundation of the स्वराज्य.

![](https://spreadsheets.google.com/oimg?key=0AmaG9ycvsc7tdC1oX0F3UWVCMWZjVVpYdXFNRWRrU2c&oid=1&zx=fat7hu-3tj9l){width="75%"}

In 1644, शिवाजी, still in his teens, conquered the stronghold atop the mountain of कौण्डिन्य and followed it up seizing रोहिदा. This रोहिदा is not too far away from the hill of Rairi atop which sits the old shiva temple of राइरेश्वर.The current structure is from the marATha period but in the interior one can see an old temple of at least यादव period. It was on the लिङ्ग here in Rairi that शिवाजी performed an abhisheka with his own blood to take the vrata of the war for स्वराज्य.We know that shortly after this he adopted his ambitious seal with the shloka in संस्कृत:\
[प्रतिपच्-चन्द्र-रेखेव वर्धिष्नुर् विश्व-वन्दिता ।\
शाहसूनोः शिवस्यैषा मुद्रा भद्राय राजते ॥]{style="color:#0000ff;"}\
Like the sliver of the first day moon will grow, thus \[will grow] the universal respect for this seal of Shaha's son shiva which shines forth as a mark of protection.

The early quiescence might be related to lack of data on his early battles in महाराष्ट्र between 1650-55. We known that he was strengthening his defences by building new forts such as Vijaydurg that was completed in 1653 and noticed and took in the full breadth of the Isaistic threat from the Europeans. To counter this he initiated building of a navy with great foresight. He also tried to diplomatically negotiate with hostile parties like सम्भाजी mohite, the black Africans of Janjira, and Mogols to stave off any attack during this phase of build up. He also sent a letter with a दक्षिण to a ब्राह्मण named siddheshvara भट्ट of Chakan to perform अभिचार rituals on his behalf. Starting 1656 with his military build up complete he initiated a wide range of campaigns. The period of quiescence of 1663 marks the invasion of महाराष्ट्र by the Mogol army under Shaista Khan who occupied most of the marATha territory forcing शिवाजी to retreat and play the waiting game till he suddenly launched his commando attack to strike the Khan. The next phase of quiescence was at the end of 1667 and in 1668 when jaisingh was poisoned to death by Awrangzeb and शिवाजी successfully negotiated the submission and payment of tribute of the Adil Shah (Sep 1667), the Portuguese (Dec 1667) and the year long ceasefire with the Mogols in (Mar 1668).

The second graph gives a break-down of the battles as per शिवाजी's principal adversaries

![](https://spreadsheets.google.com/oimg?key=0AmaG9ycvsc7tdC1oX0F3UWVCMWZjVVpYdXFNRWRrU2c&oid=2&zx=1p5t3i-iqi1yp){width="75%"}

The third graph gives a picture of how शिवाजी's military focus changed over his reign.

![](https://spreadsheets.google.com/oimg?key=0AmaG9ycvsc7tdC1oX0F3UWVCMWZjVVpYdXFNRWRrU2c&oid=5&zx=e08c04-tqwenj){width="75%"}

The above two pictures show that शिवाजी devoted roughly equal energy in terms of battles fought against his two principal Mohammedan adversaries -- this, of course as they say, is history. However, while he struggled constantly with the local Adil Shah, the struggles with the Mogols were more episodic. While he devoted relatively lower energy to the Isaistic Europeans, he clearly realized their potential to cause danger and at the same time appreciated trade with them. The conflict with the Europeans has been underplayed in histories written during the Isaist occupation of bhArata. In 1659 he initially negotiated for a peaceful trade agreement with the Portuguese during which he bought an expensive German-made sword from them that served him well in many encounters. But their waging holy war on the Hindus and demolition of temples all the way from Shri Lanka to the Gujarat coast made शिवाजी exert himself to protect Hindus from their monstrous atrocities and wished to eventually sweep them off Goa. Later in 1659 he occupied positions close to Bassein and Chaul and sent warships to launch a sortie on the Portuguese. The viceroy of Goa sent an alarm to the king of Portugal after this raid on their outposts. In 1660 he built the sea fort of  Suvarnadurg to launch further attacks on the Isaists and recruited the black naval officer Siddi Misri to strengthen his naval artillery. The English intelligence dispatch sent by Abbe Carre mentions that शिवाजी had started a cartography division to prepare accurate maps of both the land and coast in preparation of occupying strategic geography along the coastline. In early 1660, शिवाजी attacked the port of Dabhol and conquered it from the Adil Shahi governor Mohammed Sharif. But Sharif and Fazl Khan took 3 ships belonging to Afzal Khan, who had been killed earlier, with the help of the British commandant Revington to their coastal outpost of Rajapur. With the English siding with the Moslems शिवाजी sent a force under दोरोजी to attack Rajapur. He rapidly defeated the English force and Revington fled for his life on a ship, while his assistant Gifford was arrested and released upon paying a fine. Few months latter Revington returned to the scene as a collaborator of the black ghazi Siddi Jauhar (in those days apparently there was no white identity?) and helped the Moslem forces with an artillery squadron and powerful bombs to undermine the fortification of Panhala. In course of the battle they planted the English flag proudly on the flanks of Panhala, despite शिवाजी sending them a message to stay away from Indian conflicts as foreign traders. In 1661 despite being attacked severely from the north by the Mogol horde, शिवाजी decided to teach the British a firm lesson. He dispatched his ब्राह्मण officer सोम्नाथ् पण्डित् to take Rajapur and capture the English officials. Having crushed the English and destroyed their settlement he arrested Revington and his officers. Revington fell ill and शिवाजी released him for treatment, but he died on reaching the English trading post in Surat. Finally, two years later after paying 24,000 hons the English were released under the condition that they would only stick to trade.

The fourth graph gives the distribution of the number of battles in which शिवाजी and his generals led as the principal field commander in different phases of his career.

![](https://spreadsheets.google.com/oimg?key=0AmaG9ycvsc7tdC1oX0F3UWVCMWZjVVpYdXFNRWRrU2c&oid=6&zx=bmleya-31yl7m){width="75%"}

The mark of a true king is that he leads his men in person. Throughout his career the राजन् led his men in person being the field commander in numerous battles. In his earliest days, he even fought personally on the field and even killed or struck down adversaries with his own hands -- e.g. the encounter with Musa Khan and his remarkable commando attack on Shaista Khan. The above picture shows the succession of his commanders: In his earliest days रघुनाथ् कोर्डे, a senior warrior assisted him considerably especially in campaigns against renegade Hindus, like the mauryan at Javli. Then नेताजी पाळ्कर् became the chief of staff until he was captured, converted to Mohammedanism and dispatched to Afghanistan. Then प्रताप्रव् gujjar became the next chief of staff, who died in the mad charge at Bahlol Khan at Nessari. In the interim शिवाजी appointed his half-brother आनन्द्राव् bhosle to lead the forces. आनन्द्राव् had the unusual command of a haft-हज़ारि, though he was not officially ever made the chief. However, in many encounters he was the de facto field commander but often not the official one. In the final phase of शिवाजी's career he appointed hambirrav mohite as the chief of the army and he continued even after शिवाजी's death till his own death in the great battle of Wai against the Mogols. Early in his career he dismissed the inept shamrao पण्डित् and replaced him with moro त्र्यंबक् पण्डित्. This move was most fruitful, as the ब्राह्मण prime minister proved effective in many aspects, an administrator, fort-designer and field commander leading शिवाजी's armies to some of their best victories. It is interesting to note that the school textbooks in my days completely downplayed his role. The reasons for this are hardly surprising to the astute.


