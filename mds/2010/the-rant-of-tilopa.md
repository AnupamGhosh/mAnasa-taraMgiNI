
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rant of तिलोपा](https://manasataramgini.wordpress.com/2010/05/08/the-rant-of-tilopa/){rel="bookmark"} {#the-rant-of-तलप .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 8, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/05/08/the-rant-of-tilopa/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The nAstika तिलोपा from the वङ्ग country is highly regarded among Tibetans. He was an aggressive subversionist of his former dharma. He says in अपभ्रंश:\
[बम्हा विह्णु महेसुर देवा । बोहिसत्त्व म करहु सेव । देव म पूजहु तित्य ण जावा । देवपूजाहि ण मोक्ख पावा ॥]{style="color:#99cc00;"}

Here तिलोपा says: bodhisattva, do not worship the deva-s brahmA, विष्णु and maheshvara; do not do पूजा to deva-s do not go to तीर्थ-s. One does not get mokSha by doing पूजा to deva-s. Earlier, the स्थविरवादिन् buddhaghosha (born a ब्राह्मण in magadha) who calls upon the nAstika-s to desist from hearing the रामायण or the bhArata. Actually, this strain is an original aspect of the ताथगत-s that passed unchanged through the transition of the yAna-s. Umakant Mishra points to a लिङ्ग in the Soro village of Orissa that has been defaced by the carving of a nAstika dharaNi on it. So the पाषण्ड-s were ready to put these words into practice. Yet we are repeatedly told that it is the Astika-s who were the buddha-busters (to borrow a favorite term of the Mohammedan: bhut-shikhan).


