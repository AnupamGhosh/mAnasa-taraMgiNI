
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Society and biological constraints](https://manasataramgini.wordpress.com/2010/04/07/society-and-biological-constraints/){rel="bookmark"} {#society-and-biological-constraints .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 7, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/04/07/society-and-biological-constraints/ "6:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For long we have remained a साक्षी on the ethology of Homo sapiens. We have observed with how the man-made constructs interact with biology. We realized that constructs that conflict with strongly selected biological elements bring grief to man. In our youth we heard people that progressive and liberal thinking, modernization and technology has uplifted us from the state of being animals to being "human". We were often called to rise about animal instincts and strive for more "noble" pursuits. Somehow all this seemed rather incongruous since we were animals. Hence, we declared that mokSha lay in realizing and embracing the fact that you are a pashu rather than denying it. An elder asked why is it that these good-looking and well-educated women chose these guys with such "bad habits". They imbibe माध्वी-पान, sniff various धूम-s and ओषधि-s and are engaging in maithuna with so many women. There are so many sincere puruSha-s working hard and so well-behaved, yet these स्त्री-s have no interest in them. In the past when we knew little of the behavior of स्त्री-s we had directly asked the two स्त्री-s, who like us, can be साक्षी-s. They plied me with insights that immediately fitted into the large scheme of human ethology. This resulted in us rediscovering the handicap principle that had long ago been discovered by Zahavi. Firstly, by imbibing injurious substances these males strongly signal their overall quality. Secondly, the male who was described as hard-working and well-behaved is perceived as being lower in social status than the rough and crude male who is engaging in maithuna with many women. By engaging in multiple maithuna-s he has signaled two things in one shot: 1) he is a भूरिरेतस् who has a plentiful supply of bIja. 2) He is desired by many women, so he must be of good quality. By his roughness he signals that he might be an alpha male ready to fight and suppress other males. This is highly sought after by females. Also she knows that by lending his genes to her offspring he is likely to confer those behavioral traits on the offspring and increase their survival chance.

Here is where the point of human constructs i.e. maladaptive memes comes in. Popular sociology or social engineering by the vested interests projects that women basically are after goody-goody males who will participate as an equal partner as the female in a nuclear family. But this is not at all consonant with human ethology. In reality a female is much more willing to have a quick rendezvous with an alpha to receive his retas for her अण्डानि than to spend her life with that lower ranked "hard-working" male. When it comes to the open battlefield, that hard-working tag really does not work anywhere near real biological signals exuded by an alpha male. But human social constructs have offered an even-more insidious option for the female. She could use the hard-working but biologically low-ranked male as service-provider while slipping out at the peak of her fertility in the monthly cycle to have a brief secret encounter with an alpha male to receive his retas. Consequently, the hard-working male is reduced being a provider for a जारगर्भ -- a condition no better than a parasitoid wasp's victim.

It is in this context that some experiments in Drosophila are of interest. One can breed flies through artificial selection for increased male fitness or female fitness. That is males who do better at spreading their genes or females who do better at spreading their genes than the average for the population. Interestingly, these actually reduce the fitness of their offspring of the opposite sex. Thus there is a major conflict between the traits that increase the fitness of one of the sexes. This conflict in a natural population keeps the traits at a point of balance but this point can be altered by artificial influences. We are of the opinion that indeed such a bipolar effect of selection is operating due to the influence of modern society that might have interesting consequences by sliding the point. In our circles we have also speculated that such an effect might have formerly acted in the South Indian ब्राह्मण communities, especially those from the drAviDa regions and is having its serious consequences. Of course all of this needs more empirical investigation to test.


