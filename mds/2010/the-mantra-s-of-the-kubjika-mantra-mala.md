
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mantra-s of the कुब्जिका-mantra-माला](https://manasataramgini.wordpress.com/2010/09/28/the-mantra-s-of-the-kubjika-mantra-mala/){rel="bookmark"} {#the-mantra-s-of-the-कबजक-mantra-मल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 28, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/09/28/the-mantra-s-of-the-kubjika-mantra-mala/ "6:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The कुब्जिका mantra माला is one of the remnants of the once active कुब्जिका-mata system that currently survives in southern India. Text is a collection of mantra-s which are deployed like a stotra by the votary in worshiping कुब्जिका. The average तान्त्रिक typically does not know of the mantra साधन-s of the individual mantra-s and simply uses them as though the whole mantra माला were a stuti to accompany the worship of दुर्गा. Further, the published texts of the mantra माला have several corrupt readings -- at least those in drAviDa and nAgarI lipi-s; I do not know of the state of the versions published in the Andhra lipi. Again some उपासक-s state that they use it prior to the worship of वनदुर्गा and others state that they use it before the worship of चण्डिका (the देवता of मार्कण्डेय's सप्तशती). Tradition has it that these mantra-s were from the lost गौरी tantra of the कुब्जिका kula tradition.

I am attempting to correct the readings as per my tradition:\
[ॐ श्रूं श्रूं श्रूं शं फट् ऐं ह्रीं क्लीं ज्वलोज्वल प्रज्वल ह्रीं ह्रीं क्लीं स्रावय स्रावय शापं मोचय मोचय श्रां श्रीं श्रं जूं सः आदाय आदाय स्वाहा ॥ १\
ॐ श्लों हुं ग्लों जूं सः ज्वलोज्वल मन्त्रान् प्रबलय प्रबलय हं सं लं क्षं स्वाहा ॥ २\
ॐ अं कं चं टं तं पं सां बिन्दुर्-आविर्भव बिन्दुर्-आविर्भव विमर्दय विमर्दय हं क्षं क्षीं स्त्रीं जिवय जिवय त्रोटय त्रोटय जम्भय जम्भय दीपय दीपय मोचय मोचय हुं फट् ज्रां वौषट् ऐं ह्रीं क्लीं रञ्जय रञ्जय संजय संजय गुञ्जय गुञ्जय बन्धय बन्धय भ्रां भ्रीं भ्रूं भैरवी भद्रे संकुच संकुच संचल संचल त्रोटय त्रोटय म्लीं स्वाहा ॥ ३]{style="color:#99cc00;"}

[नमस्ते रुद्र-रूपायै नमस्ते मधु-मर्दिनि ।\
नमस्ते कैटभार्यै नमस्ते महिषमर्दिनि ॥ ४\
नमस्ते शुंभ-हन्त्र्यै च निशुंभासुर-सूदिनि ।\
नमस्ते जाग्रते देवि जपे सिद्धिं कुरूष्व मे ॥ ५\
ऐंकारी सृष्टिरूपिण्यै ह्रींकारी प्रतिपालिका ।\
क्लींकारी कामरूपिण्यै बीजरूपे नमो।अस्तु ते ॥ ६\
छामुण्डा छण्ड्घाती छ यैकारी वर-दायिनी ।\
विछ्छे नो।अभयदा नित्यं नमस्ते मन्त्र-रूपिणि ॥ ७\
धां धीं धूं धूर्जटेः पत्नी वां वीं वूं वागीश्वरी तथा ।\
क्रां क्रीं क्रूं कुब्जिका देवि श्रां श्रीं श्रूं मे शुभं कुरु ॥८\
हूं हूं हूंकार-रूपिण्यै ज्रां ज्रीं ज्रूं भालनादिनी ।\
भ्रां भ्रीं भ्रूं भैरवी भद्रे भवान्यै ते नमो नमः ॥ ९\
पां पीं पूं पार्वती पूर्णा खां खीं खूं खेछरी तथा ।\
म्लां म्लीं म्लूं मूलविस्तीर्णा कुब्जिकायै नमो नमः ॥ १०]{style="color:#99cc00;"}

This कुब्जिका mantra-माला has close resemblance to the siddha-कुन्ञ्जिका stotra (SKS) which is used with the मार्कण्डेय सप्तशती and belongs to the vana-दुर्गा tradition. The SKS exists in two recensions which are respectively supposed to have come from the गौरी-tantra (like the above stotra) and the DAmara tantra. On the face of it both the कुब्जिका mantra माला and the two versions of the SKS have a stotra section that clearly describes चण्डिका of the मार्कण्डेय पुरण. In the case of the SKS from the गौरी tantra the mantra-s at the beginning of the stotra are from the चण्डिका tradition based on the नवाक्षरी विद्या ([ऐं ह्रीं क्लीं छांउण्डायै विच्छे ॥]{style="color:#99cc00;"}). Hence, it would seem that the southern कुब्जिका tradition at some point "recycled" and incorporated this text from the vana-दुर्गा tradition. The SKS's association with the सप्तशती appears to have also resulted in the votaries believing that the कुब्जिका mantra माला is used before the सप्तशती just as the कुञ्जिका text.


