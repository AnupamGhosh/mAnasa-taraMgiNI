
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mantra of अघोरा according to the tradition of the काली-kula](https://manasataramgini.wordpress.com/2010/07/27/the-mantra-of-aghora-according-to-the-tradition-of-the-kali-kula/){rel="bookmark"} {#the-mantra-of-अघर-according-to-the-tradition-of-the-कल-kula .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 27, 2010]{.entry-date}](https://manasataramgini.wordpress.com/2010/07/27/the-mantra-of-aghora-according-to-the-tradition-of-the-kali-kula/ "6:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The देवी अघोरा, as we have seen [before](https://manasataramgini.wordpress.com/2010/01/05/a-discursion-on-the-sanatkumariya-panchabrahma-siddha-shamkara-tantra-and-the-early-evolution-of-the-dakshinashaiva-system/){target="_self"}, emerged first as the shakti of aghora in the root tantra-s of the bhairava srotas. She was described under several related names such as अघोरेश्वरी and appears to be the foundation from which the primary देवी-s of the trika and [पश्चिमाम्नाय](https://manasataramgini.wordpress.com/2006/12/05/paschimamnayeshvari-and-shiva-of-the-shikha/){target="_self"} emerged. Under the name अघोरा itself she was worshiped in the tradition emerging from the glorious योगिनी-जाल-शंवर tantra. The worship of the great bhairava योगिनी-जाल-शंवर and his परिवार is taught as an auxiliary अङ्ग of the system of the brahma-यामल, which gives his highly secret mantra as a means of achieving खेचरी siddhi and this yoga is declared to be "अमितात्मक", i.e. of unlimited nature. This tradition was expanded in the योगिनी-जाल-शंवर tantra and from that text transmitted to the योगिनी-सम्चार tantra. The tradition of this latter tantra was then incorporated into the composite jayadratha-यामल. The योगिनी-saMchAra lays out worship the देवी अघोरा in the company of the great bhairava. The YS states that the तान्त्रिक after undergoing दीक्ष and अभिषेक performs vrata-s such as the bhairava-vrata, the कपाल-vrata or the चामुण्डा-vrata. In the rite of दीक्ष which is performed in the श्मशान the आचार्य wearing an osseous garland and smeared with the bhasma from the funeral pyres draws the मण्डल of the great bhIma-vikrama bhairava (योगिनी-जाल-शंवर) conjoined with the देवी अघोरा. Then the student after a bath is lead to the मण्डल where the आचार्य touches him on the head with a skull and performs the न्यास on his body with the खट्वाङ्ग. Then in the typical तान्त्रिक fashion the student is lead to the image of shiva with face covered and is asked to cast flowers on it, standing to its right, after a प्रदक्षिण. Based on which face the flower falls he is given his दीक्ष kula. After receiving the mantra he finally worships अघोरा accompanied by the 63 योगिनी-s of this system.

The देवी अघोरा in this aspect as a श्मशान deity with "कापालिक" attributes has been retained as is in the काली kula. Here mantra is taught in the कामकला काली khaNDa (8.277-283) of the पञ्चाशत् साहस्र्यां महाकाल saMhitA. But here her iconography has some unusual features:

[अथाघोरा मनुं वक्ष्ये येन सिद्ध्यन्ति साधकाः ।\
कराम्लक-वद् विश्वं यस्य संस्मरणाद् अपि ॥\
माया-रमाऽङ्कुशानङ्ग-वधु-वाग्भव-गारुडैः ।\
योगिनी-शाकिनी-काली-फेत्कारी-क्रोध-बीजकैः ॥\
संबोधनम् अघोरायाः सिद्धिं मे देहि छोद्धरेत् ।\
ततश् च दापयेत् युक्त्वा स्वाहान्तो मुनिर् इष्यते ॥\
पञ्चविंशत्य् अक्षरो ऽयं मन्त्रो वाञ्चित सिद्धिकृत् ।\
अथ ध्यानं व्याहरामि येन मन्त्रः प्रसिद्ध्यति ॥\
सुस्निग्ध-कज्जल-ग्राव-तुल्यावयव-रोचिषं ।\
विषाल-वर्तुला-रक्त-नयन-त्रितयान्वितां ॥\
श्वेत-न्रस्थि-कृताकल्प-समुज्ज्वल-तनुc-छविं ।\
दिगम्बरां मुक्तकेशीं नृ-मुण्ड-कृत-कुण्डलां ॥\
शवोपरि समारूढां दंष्टा-विकट-दर्शनां ।\
द्वि-भुजां मार्जनी-शूर्प-हस्तां पितृवन-स्थितां ॥]{style="color:#99cc00;"}

The 25-syllabled mantra of अघोरा specified here is:\
[ह्रीं श्रीं क्रों क्लीं स्त्रीं ऐं क्रौं छ्रीं फ्रें क्रीं ह्स्ख्फ्रें हुं अघोरे सिद्धिं मे देहि दापय स्वाहा ॥]{style="color:#99cc00;"}\
The देवी is meditated upon as residing in the cremation ground with limbs glistening like a moist mountain of black collyrium, with three large red eyes, with a shapely body decorated with sparkling ornaments of white human bones, space-clad, with free-flowing tresses, with earrings made from human skulls, standing on a corpse, with dreadful saber teeth and with two arms holding a broom and a winnowing fan. While the general description of her matches the ancestral state as in the tradition of the YJS, the broom and the winnowing fan are unusual. This appears to be the prototype on which emergent deities like धूमावती were partly based.


