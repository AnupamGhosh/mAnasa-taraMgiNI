
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Devil in the detail](https://manasataramgini.wordpress.com/2008/10/18/devil-in-the-detail/){rel="bookmark"} {#devil-in-the-detail .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 18, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/18/devil-in-the-detail/ "5:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3242/2945758785_dc39f26049_b.jpg){width="75%"}
```{=latex}
\end{center}
```



