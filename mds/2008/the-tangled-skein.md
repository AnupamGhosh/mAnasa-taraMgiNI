
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The tangled skein](https://manasataramgini.wordpress.com/2008/08/09/the-tangled-skein/){rel="bookmark"} {#the-tangled-skein .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 9, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/09/the-tangled-skein/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3245/2745336587_75870fe2ac_o.png){width="75%"}
```{=latex}
\end{center}
```



He who knows the tangled skein will see all in the realm of life

