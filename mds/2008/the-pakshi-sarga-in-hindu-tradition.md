
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The पक्षि sarga in Hindu tradition](https://manasataramgini.wordpress.com/2008/07/04/the-pakshi-sarga-in-hindu-tradition/){rel="bookmark"} {#the-पकष-sarga-in-hindu-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/04/the-pakshi-sarga-in-hindu-tradition/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[![Figure 1](https://manasataramgini.files.wordpress.com/2008/07/pakshi_evolution.jpg){.size-medium .wp-image-942 aria-describedby="caption-attachment-942" attachment-id="942" comments-opened="0" image-caption="<p>Figure 1</p>
" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\"}" image-title="pakshi_evolution" large-file="https://manasataramgini.files.wordpress.com/2008/07/pakshi_evolution.jpg" medium-file="https://manasataramgini.files.wordpress.com/2008/07/pakshi_evolution.jpg" orig-file="https://manasataramgini.files.wordpress.com/2008/07/pakshi_evolution.jpg" orig-size="894,521" permalink="https://manasataramgini.wordpress.com/2008/07/04/the-pakshi-sarga-in-hindu-tradition/pakshi_evolution/" height="174" width="300"}](https://manasataramgini.files.wordpress.com/2008/07/pakshi_evolution.jpg)

Figure 1

Hindu tradition posits that all living organisms have descended from a common ancestor. The common ancestor of all birds is said to be ताम्रा. Her sisters like kadru or सुरसा are the ancestors of the reptiles. The origin of birds from ताम्रा is narrated in the पक्षि sarga that is found in both the इतिहास-s and many of the महापुराण-s (see below). Collation of the पक्षि sarga-s from several major texts followed by construction of a parsimony network allows us to follow its evolution through these texts (see figure 1). The पक्षि sarga in the epics states that ताम्रा spawned 5 branches, which in turn spawned several other lineages of birds. The agni पुराण preserves a degenerate version of this account. The remaining पुराण-s increase the number of lineages emerging from ताम्रा to 6. However, in the majority of them the apparently zoologically-challenged सूत makes the sixth lineage one of mammals- i.e. ungulates. However, the ब्रह्माण्द पुराण and padma पुराण correct this to make the 6th lineage additional avian lineages. The Pauranic versions themselves can be divided into two categories: 1) using the term सुता for the descendents of तांरा and 2) using the term कन्या for the same. The पक्षि-sarga from all major texts with is given below with a summary of their views on the avian "phylogeny" shown as a network (Figure 2).

The word भास is commonly translated as vulture or a bird of prey. It is an old vedic word (e.g. भास sAman). भासी is the founder of an avian lineage according to 11 of the 12 texts. Included in the भासी lineage (considering all the 12 texts) are various birds, such as the भास, मयूर (peacocks), कुक्कुक्ट (fowl), tittiri (patridge) and several other birds. Occasionally उलूक-s (owls) and kAka-s (crows) are included in the भासी clade but shyena-s and गृध्र-s are not. Only the padma and matsya पुराण-s consider the bhasi as a lineage of falconiform birds and place the kurara-s (osprey?) in it. From the view point of Indo-European faunal elements we find that Greek and Indo-Iranian share some specific isoglosses:

 1.  Grk: iktinos; Avs:saena; Skt: shyena => hawk

 2.  Grk: ortuks; Skt: vartaka => quail

 3.  Grk:kerkos; Avs: kahrka; Skt:कृकवाकु =\>rooster\
In light of this one wonders if भास and Greek phasianos/phasis are cognates and भास originally probably pheasant rather than a falconiform in Old Indo-Aryan.

From the graph one can infer that the original Hindu system of avian classification included the श्येनी, भासी and गृध्री clades for certain. Additionally, the शुकी clade seems to have been most likely present. The शुची clade might have been created originally from a mutation of the शुकी clade, but it was intentionally reassigned to remain largely distinct from शुकी by most पौरणिc authors. The क्रौञ्ची, kAkI, भासी, शुची, शुकी and धृथराष्ट्री clades show overlap in contents, suggesting that they represented the major bulk of the doubtful relationships -- an attempt to fit the remaining bird groups into the remaining 2-3 lineages. However, a large water-bird lineage and the owl-nightjar lineage are frequently associated with them. Finally, सुगृध्री appears only in the padma पुराण -- an apparent attempt to correct the zoological gaffe (i.e. सुग्रीवी) occurring in the ancestral पुराण sarga narrative.

[![Figure 2](https://manasataramgini.files.wordpress.com/2008/07/pakshi.jpg){.size-medium .wp-image-943 aria-describedby="caption-attachment-943" attachment-id="943" comments-opened="0" image-caption="<p>Figure 2</p>
" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\"}" image-title="pakshi" large-file="https://manasataramgini.files.wordpress.com/2008/07/pakshi.jpg" medium-file="https://manasataramgini.files.wordpress.com/2008/07/pakshi.jpg" orig-file="https://manasataramgini.files.wordpress.com/2008/07/pakshi.jpg" orig-size="932,974" permalink="https://manasataramgini.wordpress.com/2008/07/04/the-pakshi-sarga-in-hindu-tradition/pakshi/" height="300" width="287"}](https://manasataramgini.files.wordpress.com/2008/07/pakshi.jpg)

Figure 2

महाभारत (Pune "critical" edition)\
ताम्रा tu सुषुवे देवी पञ्चैता loka विश्रुताः || 54||\
उलूकान् सुषुवे kAkI श्येनी श्येनान्-व्यजायत |\
भासी भासान् ajanayad गृध्रांश्-chaiva जनाधिप || 55||\
धृतराष्ट्री tu हंसांश्-cha कलहंसांश्-cha सर्वशः |\
चक्रवाकांश्-cha भद्रं te प्रजज्ञे sA tu भामिनी || 56||\
शुकी विजज्ञे धर्मज्ञ शुकानेव मनस्विनी |\
कल्याण guna सम्पन्ना sarva-लक्षण पूजिता || 57||

रामायण (Baroda "critical" edition)\
क्रौञ्चीं भासीं तथा श्येनीं धृतराष्ट्रीं तथा शुकीम् || 3.13.17||\
ताम्रापि सुषुवे कन्याः पञ्चैता लोकविश्रुताः |\
उलूकाञ् janayat क्रौञ्ची भासी भासान्-व्यजायत || 3.13.18 ||\
श्येनी श्येनांश्-cha गृध्रांश्-cha व्यजायत su-तेजसः |\
धृतराष्ट्री tu हंसांश्च कलहंसांश्-cha सर्वशः || 3.13.19 ||\
चक्रवाकांश्-cha भद्रं te विजज्ञे सापि भामिनी |\
शुकी नतां विजज्ञे tu नताया vinatA सुता || 3.13.20 ||\
...\
vinatA cha शुकी पौत्री कद्रूश्-cha सुरसा स्वसा || 3.13.31||\
कद्रूर्-nAga-सहस्क्रं tu विजज्ञे dharaNI-dharam |\
dvau putrau विनतायास्तु गरुडो.अरुण eva cha || 3.13.32||\
तस्माज्-jAto.aham अरुणात्-सम्पातिश्-cha ममाग्रजः |\
जटायुरिति mAM viddhi श्येनी-putram-arindama || 3.13.33||

agni पुराण (VSP edition)\
श्येनी भासी तथा क्रौञ्cई ध्ऱ्‌तराष्ट्री शुकी तथा |219.009ab

हरिवंश (Gita Press text)\
ShaT-सुताः su-mahA-सत्त्वास्-ताम्रायाः परिकीर्तिताः\
kAkI श्येनी cha भासी cha सुग्रीवी shuchi गृध्रिका ||1.3.106 ||\
kAkI kAkAn ajanayad उलूख़ी प्रत्युलूककान् |\
श्येनी श्येनांस् तथा भासी भासान् गृध्रांश्-cha गृध्र्य्-api ||1.3.107||\
shuchir औदकान् पक्षि गणान् सुग्रीवी tu paraMtapa |\
अश्वान्-उष्ट्रान्-गर्दभांश्-cha ताम्रा वंशः प्रकीर्तितः ||1.3.108 ||

brahma पुराण (P. Schreiner's text)\
ShaT-सुताः su-mahA-भागास् ताम्रायाः परिकीर्तिताः || 3.92|\
क्रौञ्ची श्येनी cha भासी cha सुग्रीवी shuchi गृध्रिका |\
क्रौञ्ची tu जनयामास उलूक प्रत्युलूककान् || 3.93|\
श्येनी श्येनांस् तथा भासी भासान् गृध्रांश्-cha गृध्र्य्-api |\
shuchir औदकान् पक्षि गणान् सुग्रीवी tu द्विजोत्तमाः || 3.94|\
अश्वान् उष्ट्रान् गर्दभांश्-cha ताम्रा वंशः प्रकीर्तितः |

garuDa पुराण (Sanskrit Pratishthan text)\
ShaT-सुताश्-cha mahA-सत्त्वास् ताम्रायाः परिकीर्तिताः |\
शुकी श्येनी cha भासी cha सुग्रीवी shuchi गृध्रिके || 1.6.56 ||\
शुकी शुकान् ajanayad उलूकी प्रत्यलूककान् |\
श्येनी श्येनांस् तथा भासी भासान् गृध्रांश्-cha गृध्र्य्-api || 1.6.57 ||\
shuchy-औदकान् पक्षि गणान् सुग्रीवी tu व्यजायत |\
अश्वान्-उष्ट्रान्-गर्दभांश्-cha ताम्रा वंशः प्रकीर्तितः || 1.6.58 ||

विष्णु पुराण (VSP edition)\
ShaT सुताः su-mahA-सत्त्वास्-ताम्रायाः परिकीर्तिताः |\
शुकी श्येनी cha भासी cha सुग्रीवी shuci गृद्ध्रका || 1.21.15 ||\
शुकी शुकान् ajanayad उलूक प्रत्युलूकिकान् |\
श्येनी श्येनांस् तथा भासी भासान् गृर्ध्यांश् cha गृध्र्य्-api || 1.21.16 ||\
shuchy-औदकान्-पक्षि गणान् सुग्रीवी tu व्यजायत |\
अश्वान्-उष्ट्रान्-गर्दभांश्-cha ताम्रा वंशः प्रकीर्तितः || 1.21.17 ||

ब्रह्माण्ड पुराण (VSP edition)\
ShaT कन्यास्-tvabhi-विख्यातास्ता म्रायाश्-cha विजज्ञिरे || 2.7.445 ||\
गृध्री भासी शुकी क्रौञ्cई श्येनी cha धृतराष्ट्रिका |\
अरुणस्य cha गृद्री tu वीर्यवन्तौ महाबलौ || 2.7.446 ||\
संपातिं cha जटायुं cha प्रसूता पक्षि-sattamau |\
संपातेर् विजयाः पुत्रा द्विरास्याः prasahash-cha ye || 2.7.447 ||\
जटायुषः पुराः पुत्राः kaka-गृध्राश्-cha कर्णिकाः |\
भार्या garutmatash-chaiva भासी क्रौञ्cई तथा शुकी || 2.7.448 ||\
धृतराष्ट्री तथा श्येनी तास्वपत्यानि vachmite |\
शुकी गरुत्मतः पुत्रान्-सुषुवे ShaT परिश्रुतान् || 2.7.449 ||\
सुखं सुनेत्रं विशिखं सुरूपं सुरसं balam |\
तेषां पुत्राश्-cha पौत्राश्-cha गरुडानां महात्मनाम् || 2.7.450 ||\
chaturdasha सहस्राणि पुराणां पन्नगाशिनाम् |\
putra-pautra-विसर्गाच्छ तेषां vai वंश विस्तरैः || 2.7.451 ||\
...\
shaila शृङ्गाणि व्याप्तानि गारुडैस्तैर्-महात्मभिः |\
भासी-पुत्राः स्मृता भासा उलूकाः kAka-कुक्कुटाः || 2.7.455 ||\
मयूराः कलविङ्काश्च कपोता-lAva-तित्तिराः |\
क्रौञ्cआ वाध्रीणसाः श्येनाः कुरराः सारसा बकाः || 2.7.456 ||\
ity-एवमादयो .anye .api क्रव्यादा ye cha पक्षिणः |\
धृतराष्ट्री tu हंसाश्-cha kala-हंसांश्-cha भामिनी || 2.7.457 ||\
चक्रवाकांश्-cha विहगान्-सर्वांश्-chaiva उदकान्द्विचान् |\
श्येन्यनन्तं विजज्ञे tu putra-पौत्रं द्विजोत्तमाः || 2.7.458 ||

matsya पुराण (Raghavan text)\
ShaT-कन्या जनयामास ताम्रा मारीच-बीजतः |\
शुकी श्येनी cha भासी cha सुग्रीवी गृध्रिका शुचिः || 6.30 ||\
शुकी शुकान् उलूकांश्-cha जनयामास धर्मतः |\
श्येनी श्येनांस् तथा भासी कुररान् apya-जीजनत् || 6.31 ||\
गृध्री गृध्रान् कपोतांश्-cha पारावत-विहंगमान् |\
haMsa-सारस-क्रौञ्cआंश् cha प्लवाञ्-Shuchir-अजीजनत् || 6.32 ||\
अजाश्व-मेषोष्ट्र-खरान् सुग्रीवी चाप्य्-अजीजनत् |\
एष ताम्रान् वयः prokto विनतायां nibodhata || 6.33 ||

padma पुराण (Setzer edition)\
ShaT-कन्या जनयामास ताम्रा मारीच-वीर्यतः ||1.6.62||\
शुकीं श्येनीं cha भासीं cha सुगृध्रीं शुचिं |\
शुकी शुकान् उलूकांश्-cha जनयामास धर्मतः ||1.6.63||\
श्येनी श्येनांश्-cha भासी cha कुररान् apya-जीजनत् |\
गृध्री गृध्रान् su-गृध्री-cha पारावत-विहंगमान् |\
haMsa-सारस-कारण्ड-प्लवाञ्-Shuchir-अजीजनत् || 1.6.64 ||\
ete ताम्रा सुताः प्रोक्ता विनताया निशामय || 1.6.65 ||

कूर्म पुराण (VSP edition)\
ताम्रा cha जनयामास ShaT कन्या dvija-पुङ्गवह् |\
शुकीं श्येनीं cha भासीं cha सुग्रीवाङ् गृध्रिकां shuchim || 1.17.11 ||

bhAgavata पुराण (Gita Press edition)\
ताम्रायाः श्येण-गृध्राद्य muner-अप्सरसं गणाः || 1.6.27cd||


