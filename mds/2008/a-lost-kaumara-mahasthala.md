
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A lost कौमार महास्थल](https://manasataramgini.wordpress.com/2008/02/19/a-lost-kaumara-mahasthala/){rel="bookmark"} {#a-lost-कमर-महसथल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 19, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/19/a-lost-kaumara-mahasthala/ "12:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the late 1800s our predecessor was returning to his home base after attending to secular business in the town of Guntur in the Andhra country on his horse. It was late evening and he had reached the eastern fringe of the village of Chebrolu. He decided to visit the temple of rudra known as भीमेश्वर, which lay right on his path. Having paid his respects to the deva he wondered whether he should visit a स्मार्त whom he knew in vicinity or simply spend the night in the open as the weather was excellent. As it was late and it felt it might not be appropriate to disturb the Brahmin he decided to sleep in the open on an elevation to the north east of the temple just at the boundary to the expanses of rice fields. It was here that in his dream he witnessed the कौमार विद्या of 14 syllables with the intertwining of the square, the hexagon, the triangle and the bindu. He felt he was inside a कुमार गृह and the मण्डल lay upon him. He saw an enormous shrine of कुमार with its spire pointing to the skies with a cock-banner fluttering atop it. The next day he went to visit the स्मार्त who lived in the hamlet and asked him if there was once a kumara क्षेत्र in the village. The ब्राह्मण said that there were some tales of कुमार being worshiped there in the past, but he was not sure. Then obtaining some essentials from the ब्राह्मण he returned to the mound and performed a सुब्रह्मण्य याग as ordained in the guha-paddhati. The ब्राह्मण from the village accompanied him and asked him after performance of the याग to initiate him into a mantra. Our predecessor after some thought agreed and initiated him into the विद्या of the queen of the kula path with which one enchants all. He asked the ब्राह्मण to keep worshiping the queen of the 3 कूट-s and said that after 4 generations one in his line will attain eko-मानुष आनन्दः and will automatically have a one-time siddhi of the कौलविद्या.

\~*\~*\~

Armed with the records of Epigraphica Indica and MR Rao's collection of inscriptions we note:

  - In 1006 CE Bayalanambi the commander of the army of Western चालुक्य king सत्याश्रय invaded the Vengi to conquer the चोड-Eastern चालुक्या combine. He destroyed the forts of Dannada and Enumandala and on his way he stopped at Chebrolu and made gifts to कुमार whose was the patron deity of the village.

  - In 1076 गोण्कय, the commander of the वेलनाटि चोडस्, conferred ornaments to the image of कुमार and also that of rudra housed in the मूलस्थान-महादेव temple.

  - In 1115 CE गण्डमांबा the wife of the वेलनाटि चोड ruler made gift to कुमार.

  - In 1118 a certain सूरन from a family of temple artisans became a general in the army of the कोण्डपडमटि war-lord manda and conquered Vijayawada. He made several gifts to the temple of कुमार at Chebrolu.

  - In 1145 CE a lamp tower was gifted by the कोण्डपडमटि war-lord भीमराज to the कुमार temple, which had 300 staff involved in its upkeep and 300 for performance of dance dramas.

  - In 1213 CE, gaNapati-deva who was return from the conquest of the south gifted the village to the commander of his army, जयसेनापति, the author of a famous treatise on Hindu dance. जयसेनापति repaired and whitewashed all the temples in the village. He erected a gold kalasha atop the कुमार shrine. He also covered the whole central image of कुमार with gold and made utsava-मूर्ति-s of कुमार and his wives सेना and गजायी. He built an enclosure around the temple, a मण्डप in front of it and gopura 3 stories high. He also built another मण्डप where the utsava-मूर्ति of कुमार and his wives would rest after the makara-शन्करान्ति festival and the hunting festival. He commissioned paintings in this मण्डप which depicted the heroic deeds of कुमार in the battles of devas and दानवस्. He also restored the लिङ्ग-s at मूलस्थान-महादेव and a forest shrine of rudra, repaired भीमेश्वर and fortified the town. The inscription mentions that an astronomer and a physician were attached to the staff of the कुमार temple.\
The rampage of the army of Islam under the Bahmanid sultan Muhammad ravaged the temples of this region and appears to have desecrated the कुमार temple.

  - In 1553 CE the vijayanagaran governor devabhaktuni kondana reinstalled the temple of कुमार.

The great कुमार shrine of Chebrolu appears to have been built by the चालुक्य king yuddhamalla. It no longer exists today. It appears to have been completely destroyed during the invasion of the Adilshahi army.


