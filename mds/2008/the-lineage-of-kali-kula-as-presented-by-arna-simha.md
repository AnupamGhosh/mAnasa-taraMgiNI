
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The lineage of कालि-kula as presented by arNa-सिंह](https://manasataramgini.wordpress.com/2008/03/24/the-lineage-of-kali-kula-as-presented-by-arna-simha/){rel="bookmark"} {#the-lineage-of-कल-kula-as-presented-by-arna-सह .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 24, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/24/the-lineage-of-kali-kula-as-presented-by-arna-simha/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

paNDita arNa-सिंह is today a poorly known teacher. From his two works the खचक्रपञ्चक stotra and महानय प्रकाश, we glean that he was once part of an illustrious कालि-kula lineage, which began with शिवानन्द-नाथ-I who he calls ज्ञान-netra or ज्ञान-चक्षु. He even suggests that the खचक्रपञ्चक's मन्त्रशास्त्र was first "brought down" by शिवानन्द. He names a lineage of कालिकुल teachers to which he belongs which appears to intersect with the lineage mentioned by jayaratha the commentator on abhinavagupta. अर्णसिम्ःअ's lineage is:

                     शिवानन्दनाथ
              female teacher केयूरवती
                         वामन
                       चक्रभानु
                      /         \
     female teacher ईशानिका | prabodha
                nandaka      | jaiyaka
                sajjana      | पङ्क
                someshvara   | nAga
                       \        /
                        अर्णसिंह
     

After चक्रभानु the कालि-kula tradition branched into two lineages which unified again under अर्णसिम्ह.\
खचक्रपञ्चक stotra is stotra full of power -- good stotra-s are of different kinds. Some have good poetry and are easily followed by all साधकस्, such as those attributed to शङ्कर, some have deep mantra embeddings, and some are only for advanced users and convey rahasya-s of a tantric system to them. The ख़्ह्PS is of the latter type and also conceals within it an immense mantra power. It is not easy to expound it but it needs to be "experienced" as a first person experience.\
This is sensed right in the 2nd verse itself:\
[एका देवी परसरति परा व्योमहंकार-घोषा]{style="color:#99cc00;"}\
[चक्रं रौद्रं गगन-सदृशं रश्मि-पुञ्जं समग्रं ।]{style="color:#99cc00;"}\
[व्योमाकारअकलित-कलनानन्द-भासा-प्रछण्डं]{style="color:#99cc00;"}\
[भाषातीतं विस्मय-विषयं देवी-चक्रं नमामि ॥]{style="color:#99cc00;"}\
The one goddess, the supreme, spreads the sound the अहंकार [*1] of space -- the terrifying chakra [*2] which appears like the sky, the collection \[of all things], the mass of rays. Whose form is like pure space, formidable with the light of the bliss of the unmanifest spot (or embryo) [*3]. I salute the chakra of the goddess whose sphere is wondrous and beyond language.\
[1] The term अहंकार here should be understood in the सांख्य sense as expounded in the भृगु स्मृति.\
[2] It must be understood that the chakra referred to twice in this verse is multifunction in that it refers to the मण्डल or yantra of भानवी-कौलिनी and its projection in the form of the world with the sun coursing through the sky and the projection into consciousness.\
[3] kalana often means a spot or the zygote in embryology. Both these meanings are woven together in this case --the bindu of the yantra as the spot and the unmanifest existence is likened to the zygote which represent the "unmanifest" form of the organism that will develop from it. It also represents the basic बीजमन्त्र of the kula: खफ्रें.\
This is a succinct the description of भानवी-कौलिनी the mistress of the khachakra. As we noted above the stotra itself uses the term "भाषातीतं" beyond language. Then starting with विनायक and his shakti the stotra outlines the complex मण्डल of the देवी with the hosts of योगिनी-s in each circle.


