
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [मृत्युजित् and व्याधिनाशन yoga](https://manasataramgini.wordpress.com/2008/08/17/mrityujit-and-vyadhinashana-yoga/){rel="bookmark"} {#मतयजत-and-वयधनशन-yoga .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 17, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/17/mrityujit-and-vyadhinashana-yoga/ "6:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R1's father mentioned that a good तान्त्रीक is one who can prognosticate and disease and block it using a prayoga that involved the deployment of the mantra of अमृतेश्वर bhairava or the त्र्यंबक mantra. When R1 was afflicted by a strange roga on account of अभिचार he applied this procedure. I heard the outline of procedure he followed and he also mentioned a संपुटिकरण with the heart of tripura-sundarI. The outline, I much later realized, follows the pattern of dhAraNa-s mentioned in the सिद्धयोगेश्वरीमत, मलिनीविजयोत्तर and the दीक्षोत्तर tantra-s on one hand and the prayoga-s of the दक्षिणामूर्ति saMhitA, ज्ञानार्णव and कौलावली-निर्णय on the other.

The primary साधन is given the सिद्धयोगेश्वरीमत (11.3cd-12) and is known as the मृतुञ्जय साधन:\
[मृतुञ्जयम् समासेन कथ्यमानं शृणु प्रिये ॥\
आकाशम् भूत-निलयम् तत्र पद्माकृतिं स्मरेत् ।\
दलाष्टक समोपेतम् कर्णिकादि स्थितं सितम् ॥\
स्वच्छ-स्फटिक संकाशं प्रालेयावनि संनिभं ।\
सर्वामृत-मयं दिव्यं चन्द्र-कल्पित-कर्णिकं ॥\
तादृशेनैव रूपेण भूपदं तु मनोरमम् ।\
तस्मिन्श्-चैवोपविष्टस् तु सम्यग् न्यासकृतस् ततः ॥\
प्रालेयाभम् ततोऽत्मानं शुद्ध-स्फटिक-सम्प्रभम् ।\
एवं विछिन्त्यन्-आत्मानं पस्छाद् ध्यानं विछितयेत् ॥\
व्योम-पद्मे तु यछ् छन्द्रं कर्णिकायां व्यवस्थितम् ।\
तत् स्थां विछिन्तयेद् देवीं परां सुरभि-रूपिणीं ॥\
स्वच्छ-स्फटिक-सप्रख्यां समन्ताद् अमृत-स्रवं ।\
स्रवन्तीं सामयं दिव्यम् मन्त्र-नादान्तसर्पिणिं ॥\
सा शक्तिर् देवदेवस्य परमात्मा अमृतवाहिनी ।\
सा स्रवन्तीं परम् क्षीरम् यत् तत् सत्यं सुनिर्मलं ॥\
तत् पतद् ध्यातमनो मूर्तिम् समन्ताछ् छ विछिन्तयेत् ।\
विशद्-ब्रह्मबिले भान्ह्यो प्लावयेद्-धृध्-गुहाश्र्यात् ॥\
एवं प्रतिदिनं ध्यायेज् जपेन् मन्त्रोत्तमोत्तमम् ।\
षन्-मासाज्-जिन्वते मृत्युम् इति शास्त्रस्य निश्चयं ॥]{style="color:#99cc00;"}

Here the मृत्युञ्जय yoga of trika is laid out. First the साधक meditates upon an 8-petalled lotus with a white pericarp and of crystalline form emerging from the vacuum \[above his head] like glittering frost. In its midst he visualizes the moon disc in its midst with the lotus firmly rooted in the ground \[the spinal cord of he साधक is the stem]. He sees himself of crystalline form and glittering like frost. In the moon in the midst of the vacuum lotus he sees the देवी parA of clear complexion who sends down the stream of nectar with the meditation upon her mantra (the single syllabled सौः). This nectar streams through the brahmabila or brahmarandhra and streams through the अमृतवाहिनी nerve and fills the cavity of his heart. The yogin who practices this dhyAna doing japa on the mantra of parA देवी for six month attains मृत्युजित्. This deployment of parA देवी's mantra in the मृत्युजित् साधन is expanded in the stream of the कालीमत outlined in the दक्षिणामूर्ति saMhitA. Here, the combination of the parA mantra-s with the मृत्युजित् mantra-s gives rise to the mantra देवता-s known as the सिम्हासन देवी-s of त्रिपुरसुन्दरी. These देवी-s are मृतसंजीवनी and मृत्युञ्जय-parA whose mantras are respectively (as given by विद्यारण्य yati):\
मृतसंजीवनी:[ह्रीं \ हंसः \संजीवनि\ जूं सः \ कुरु कुरु\ सौः सौः \स्वाहा ।]{style="color:#99cc00;"}\
Here the first element is the हृल्लेख which is the essence of the "peaks" of the triad elements of the त्रिपुरसुन्दरी पञ्चदशाक्षरी or श्रीविद्या. हंसः is the Ajapa mantra already known in the पाशुपतब्राह्मणोपनिषद् or seen by dvija-s as the essence of the vaidika दुरोहण-s. संजीवनी + kuru kuru is the prayoga khANDa or the imperatives for deployment. जूं saH is the अमृतेश्वर or मृत्युञ्जय mantra, the तान्त्रीक equivalent of the मृतसंजीवनी ऋक् which can be used by dvija-s. Finally सौः is the bIja of parA देवी.

[मृत्युञ्जय-परा: वद वद वाग्वादिनि \ ह्सैम् क्लिन्ने क्लेदिनि\ महाक्षोभं कुरु कुरु \ह्स्रीं \ ॐ \मोक्षं कुरु कुरु\ ह्सौः ॥]{style="color:#99cc00;"}\
Here the first element is parA in her primal form वाग्वादिनि worshiped in the सौन्दर्यलहरी 15. hsaim klinne kledini + ह्स्रीम् OM comprise the mantra of अमृतेश्वरी or अमृतलक्ष्मी conjoined with अमृतेश्वर bhairava; she is क्लिन्ना and kledini because अमृतेश्वरी is seen as sending the moist nectar. The प्रणव is अमृतेश्वर whose worship is laid out in the netra tantra. The prayoga khANDa-s are the two imperatives of which the second one "मोक्षं kuru kuru" derives is power from the final element which is a version the parA mantra i.e. ह्सौः. This become clear because the प्रपञ्चसार (9.42) states while explaining the त्रिपुरभैरवी विद्या that one who concentrates on its terminal bIja attains mokSha. This bIja is the parA mantra under the त्रिपुरभैरवी transform.

The श्रीकुल tradition also states that the shakti bIja of the bAlA mantra is mediated as the मृत्युजित् विद्या. Now, parA is the heart of त्रिपुरसुन्दरी and her mantra i.e. सौः thus becomes the shakti bIja of the bAlA mantra as the hallowed नित्याषोडषिकार्णव states: "shakti बीजे parA shaktir इच्छैव शिवरूपिनी". This is expounded briefly in the great नित्याषोडषिकार्णव and expanded in the ज्ञानार्णव (19.29-31) (or equivalently in कौलावली-निर्णय of the Eastern श्रीकुल tradition):\
[शरिरे चिन्तयेत् परां ।\
स्रवत् पीयूषधार्भिर् वर्षन्तीं विषहारिणीं ॥\
हेम-प्रभा-भासमानं विद्युन्-निकर-सुप्रभां ।\
स्फुरच्-चन्द्रकला पूर्ण कलषं वरदाभयौ ॥\
ज्ञानमुद्रां च दधतीं साक्षद् अमृतरूपिनीं ।]{style="color:#99cc00;"}\
Here parA is mediated as sending down the nectar from the full moon pitcher while doing japa of her mantra सौः.

The practical aspects of the meditation are further expanded by the मलिनीविजयोत्तर (16.48-58a), giving details on how diseases are prognosticated and countered.\
मलिनीविजयोत्तर 16.55-58a \[somadeva vAsudeva's partial edition]:\
[बद्ध्वा पद्मासनं योगी पराबीजम् अनुस्मरन्\
भ्रुवोर् मध्ये न्यसेछ् छित्तं तद्बहिः किं छिद् अग्रतः\
निमीलिताक्षो हृष्टात्मा शब्दालोकविवर्जिते\
पश्यते पुरुषं तत्र द्वादशाङ्गुलम् आयतम्\
तत्र छेतः स्थिरं कुर्यात् ततो मासत्रयोपरि\
सर्वावयव-संपूर्णं तेजोरूपम् अचञ्चलम्\
प्रसन्नम् इन्दुसंकाशं पश्यति दिव्यचक्षुषा\
तं दृष्ट्वा पुरुषम् दिव्यं कालज्ञानं प्रवर्तते\
अशिरस्के भवेन् मृत्युः षण्मासाभ्यन्तरेण तु\
वञ्चनं तत्र कुर्वीत यत्नात् कालस्य योगवित्\
ब्रह्मरन्ध्रोपरि ध्यायेच् चन्द्रबिम्बम् अकल्मषम्\
स्रवन्तम् अमृतं दिव्यं स्वदेहापूरकं बहु\
तेनापूरितम् आत्मानं छेतोनालानुसर्पिणा\
सबाह्याभ्यन्तरं ध्यायन् दशाहान् मृत्युजिद् भवेत्\
महाव्याधि विनाशे ऽपि योगम् एनं समभ्यसेत्\
प्रत्यङ्ग व्याधि नाशाय प्रत्यङ्गम् अनुस्मरन्\
धूम्र-वर्णं यदा पश्येन् महाव्याधिस् तदा भवेत्\
कृष्णे कुष्ठम् अवाप्नोति नीले शीतलिकाभयम्\
हीन-चक्षुषि तद् रोगं नासा-हीने तदात्मकम्\
yad yad अङ्गं na pashyeta tatra तद्व्याधिम् Adishet\
आत्मनो वा परेषां वा योगी योगपथे स्थितः\
वर्षैस् तु पञ्चभिः सर्वम् विद्या तत्त्वान्तम् ईश्वरि\
वेत्ति भुङ्क्ते च सततं न च तस्मात् प्रहीयते]{style="color:#99cc00;"}

First the साधक sits in padmAsana doing japa of the parA mantra, focusing on a point at the level of his eyebrows but just outside of it to the front. When he stills his mind he sees the puruSha measuring 12 digits. He focuses on the puruSha and continues his साधन for 3 months and then he perceives the puruSha of the form of moon-light possessed of all limbs. Upon seeing this puruSha thus he acquires कालज्ञान or the knowledge of time. If the vision becomes headless then it means that the साधक would die within 6 months. So he has to strive to perform the cheating of काल (कालस्य वञ्चनं) by meditating on the lunar disc above the ब्रह्म्रान्ध्र streaming forth nectar copiously to fill the yogin's body. The one meditates upon the internal and external inputs to consciousness being filled with the nectar flowing through the 3rd ventricle of the brain \[the chetonala: the realization of the importance of the chetonala was a major discovery of the yogins]. Then in 10 days the yogin becomes मृत्युजित्. The मालिनीविजयोत्तर goes on to reveal how this procedure may be used to prognosticate and eliminate disease in the yogin himself or even in someone else -- if while visualizing the body in meditation a particular organ is missing then a disease is prognosticated in it. When the organ is seen in a smoky color then it is indicative of a major disease -- for example black is indicative of mycobacterial infection and blue of smallpox. To destroy or preempt the disease from striking the yogin meditates upon that nectar flowing through the afflicted organ.

A similar procedure of meditating upon the lunar nectar streaming in through the ventricle into the vagus nerves into the हृत् chakra is outline in the स्वच्छन्द tantra (7.217-226) belonging to a different lineage of the bhairava srotas (the mantra-पीठ). The presence of such meditations in the tantra of the mantra-पीठ and also a tantra of the सैद्धान्तिक-s (दीक्षोत्तर) suggest that as the basic मृत्युजित् yoga it was present in the ancestral shaiva yoga system. But the combination with the meditation upon the goddess parA is a part of the kula system starting from the trika tantra-s.


