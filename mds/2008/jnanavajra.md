
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [ज्ञानवज्र](https://manasataramgini.wordpress.com/2008/07/13/jnanavajra/){rel="bookmark"} {#जञनवजर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 13, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/13/jnanavajra/ "4:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![Zanabazar (ज्ञानवज्र)](https://i0.wp.com/farm3.static.flickr.com/2268/2478022493_24b13bd8d4_o.jpg){width="75%"}

ज्ञानवज्र (Mongolian: oender-gegen zanabazar), descendent of Chingiz Kha'Khan via Tusheet Khan and Kandjamts Khatun was undoubtedly one of the greatest figures in Central Asia after his illustrious ancestors. ज्ञानवज्र saw himself as the merger of two lineages of the tradition of the bauddha kaula tradition. The first of these is traced to the brahminical nAstika kaula tantric वागीश्वरकीर्ति from विक्रमशील in the 800s of CE, who composed several stotra-s to tArA and founded the मृत्यु-हारक tArA prayoga. From him the tradition eventually passed to the kShatriya tantric दीपाङ्कर-श्रीज्ञान, who transmitted it to the उपचीन-s of Tibet. He composed the famed ekavimshati tArA stotra, one of the sublime stotra-s to the देवी. From his Tibetan successors it passed to the Mongol ज्ञानवज्र, who was aged 14 at the time of his दीक्ष. His kula-दूति was dorjiinnal-jirmaa. ज्ञानवज्र also believed that he received a second transmission of the kaula doctrine innately because he felt he was a reincarnation of the Tibetan Lama तारनाथ, who himself traced his lineage from दीपाङ्कर-श्रीज्ञान. तारनाथ had written the famous work on the origin of the tArA tantra-s. Beyond being one of the greatest of the Mongolian tantrics, ज्ञानवज्र's role was profound in spreading Sanskrit scholarship in Mongolia, composing several new stotra-s in Sanskrit, contributions to Mongolian music, discursions on tantric texts and the invention of a new Mongolian script, Soyombo, to allow accurate accommodation of Sanskrit phonetics in the Mongolian context. ज्ञानवज्र also transmitted a peculiar tantric text termed the गुह्याग्निचक्र that is now lost in India in its original. This text preserves material from the now lost ancient कौमार tantra-s. He often visited the Burkhan Khaldun Mountain, where his ancestor Chingiz Kha'Khan often journeyed to seek the aid of the original Mongol deity Koeke Moengke Tengri.

The real significance of ज्ञानवज्र was his art -- it was one of the last of the great examples of the artistic expression of the mantra-शास्त्र. This was something lost in part in even in the Hindu world outside of Nepal. ज्ञानवज्र's remarkably prefect productions of images of देवता-s following the dhyAna shlokha-s of their साधन-s literally help the dhyAna come alive. A key to the practice of the mantra at one level is the dhyAna or dhAraNI producing a persistent image of the देवता for the साधक. A साधन मूर्ति made per the specifications can produce this effect directly to the practicing deshika and can raise the साधान towards perfection by its very presence. It may be considered a direct expression of the mantra and is a tremendous -- only a few master artists can produce such साधन मूर्ति-s that can produce the effect of the mantra on their own. ज्ञानवज्र's work it the epitome of such productions -- being a great tantric himself, he was able to achieve this perfection that is one of its kind. His greatest pieces were of course the 21 forms of tArA as invoked in the ekavimshati tArA stotra (including that identified with कर्णमोटिनि), चक्रसंवर conjoined with वज्रवाराही and vajradhara.

Of these the image of चक्रसंवर conjoined with वज्रवाराही is of great importance to the Astika deshika, because of the closeness of the early laghushamvara tantra, the चक्रसंवर tantra, and the later चक्रसंवरोदय tantra followed by ज्ञानवज्र's and his predecessors to the early योगिनी tantra-s of the kula path and the shaiva bhairava tantra-s. A study of these tantra-s reveals their derivation from the bhairava tantra-s that were collected in the mantra पीठ alluded to by abhinavagupta. It becomes clear that the depiction of चक्रसंवर and वज्रवाराही is the cognate स्वच्छन्दभैरव conjoined with अघोरेश्वरी from the स्वच्छन्द-bhairava tantra. Their मण्डल of योगिनी-s is drawn from the सिद्धयोगेश्वरी-mata. The prayoga-s of वज्रवाराही and her depictions closely follow those of the glorious kaula tantra the योगिनी-जाल-शंबर. In this context it is interesting to note that the name of the composer of the primary prayoga paddhati of चक्रसंवर conjoined with वज्रवाराही (i.e. वज्रवाराही साधन) is उमापतिदेव. The perfect साधन-मूर्ति-s of स्वच्छन्द-bhairava in the form conjoined with अघोरेश्वरी have become exceptionally rare or extinct in India -- but in ज्ञानवज्र's चक्रसंवर we have a template to conceive the iconography of this देवता if form quite close to the original.

[Mongolian recitation of the करण्डव्यूह and avalokiteshvara शडक्षरी सम्पुटिकरण](https://app.box.com/s/vqap31malovx4pws83rtdqidbewewrkm)


