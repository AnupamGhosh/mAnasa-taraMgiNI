
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A notable ब्राह्मिनिcअल् clan](https://manasataramgini.wordpress.com/2008/06/25/a-notable-brahminical-clan/){rel="bookmark"} {#a-notable-बरहमनcअल-clan .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 25, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/25/a-notable-brahminical-clan/ "5:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some research into the ब्राह्मण-s of the drAviDa country of the वडम sect and the भार्गव gotra leads to an interesting family that was active in the Middle Vijayanagaran period in the court at Hampe. The patriline belonged to the जामदग्न्य vatsa भार्गव gotra.\
अरुणगिरिनाथ-I composed a work on human anatomy, classification of constitutions and drugs termed the शारिरक-sutra-guNa-पाठ. He was in the court of देवराय-II

  - His uncle दिण्डिम-भट्ट was a kavi in the court of देवराय-I. He was a reputable श्री-विद्या Tantric and composed a commentary on the सौन्दर्यलहरी

  - राजनाथ दिण्डिम-II composed the सालुवाभ्युदय.

  - दिण्डिम सार्वभौम composed the रामाभ्युदय

  - अरुणगिरि-II composed the virabhadravijaya narrating the destruction of दक्ष.

  - राजनाथ-III composed the अच्युतरायाब्युदय\
Their works are important primary sources of Vijayanagaran history.

![](https://manasataramgini.files.wordpress.com/2008/06/dindima.png){width="75%"}


