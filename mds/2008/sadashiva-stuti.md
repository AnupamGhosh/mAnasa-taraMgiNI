
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सदाशिव stuti and other issues](https://manasataramgini.wordpress.com/2008/10/16/sadashiva-stuti/){rel="bookmark"} {#सदशव-stuti-and-other-issues .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 16, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/16/sadashiva-stuti/ "6:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From मिठ्ठु's citation we get this version of the सदाशिव stuti:

[*ॐ एकाक्षराय रुद्राय त्वकारायात्म-रूपिणे ।\
उकारायादिदेवाय विद्या-देहाय वै नमः ॥ १\
तृतियाय मकाराय शिवाय परमात्मने ।\
सूर्याग्नि-सोम-वर्णाय यजमानाय वै नमः ॥ २\
नमस्ते भगवान् रुद्र भास्करामित-तेजसे ।\
भीमाय व्योम-रूपाय शब्द-मात्राय वै नमः ॥ ३\
महादेवाय सोमाय ह्य अमृताय वै नमः ।\
ॐ काररुपिणे देव नमस्ते विश्वरूपिणे ॥ ४\
नमो देवादि-देवाय महादेवाय वै नमः ।\
ardhanArI-शरीराय साञ्ख्य-yoga-parvartine || 5\
वेद-शास्त्रार्थ-गम्याय शाश्वताय नमो नमः ।\
दीनात्-त्राण-कर्त्रे च नमस्ते दिव्य-चक्षुषे ॥ ६\
नमः सशस्र-शीर्षाय नमः साहस्रिकाङ्घ्रये ।\
नमो मन्त्राय छिद्-व्योम-वासिने परमात्मने ॥ ७*]{style="color:#99cc00;"}

A citation by मिठ्ठु on the blue तिरस्करिणी:\
[*नीलं हयं समधिरुह्य पुरं प्रयान्ती नीलांशुकाभरण-माल्य-विलेपनाड्या ।\
निद्रापटेन भुवनानि तिरोदधाना खद्गायुधा भगवती परिपातु भक्तान् ॥*]{style="color:#99cc00;"}\
According to this Agama cited by मिठ्ठु तिरस्करिनी is a blue robed, blue ornamented goddess mounted on a blue horse with a sword in hand.

As per the श्रीकुल tradition of the कादि stream in the दक्षिणाम्नाय the mantra of तिरस्करिणी is given as:\
*[a]{style="color:#99cc00;"}[इं ह्रीं श्रीं ऐं क्लीं सौः ॐ नमो भगवत्यै तिरस्करिण्यै महामाये महानिद्रे सकल-पशु-जन-मनश्-चक्षुश्-श्रोत्र-तिरस्करणं कुरु कुरु स्वाहा ॥]{style="color:#99cc00;"}*\
This invocation the deity matches with the description given by मिठ्ठु to his wife in the section on तिरस्करिणी in the हंसविलास. He also mentions ऐन्द्रजालि (illusions) and mentions the visions emerging from mantra and drugs (औषधि) in this context.

Additionally, the श्रीकुल tradition also has two other mantra-s to a goddess related to तिरस्करिणी known as इन्द्रजालि, which are deployed in the दक्षिणाम्नाय.\
The first is the mAyA इन्द्रजालि:\
[ॐ ह्रीं ॐ नमो नमो भगवत्यै महामायायै मनोमये जगत्क्षोभिणी वर-वरदे सर्वजनं मोहय मोहय ईं ह्रीं स्वाहा ॥]{style="color:#99cc00;"}

The second is अमृत-इन्द्रजालि:\
[वं सं झ्रं झं जुं रं ह्रीं श्रीं मों भगवति चित्रविद्ये महामाये अमृतेश्वरी एह्येहि प्रसन्न-वदने अमृतं प्लावय अनलं शीतलं कुरु । सर्व-विषं नाशय । ज्वरं हन हन । उन्मादं मोचय मोचय । उष्णं शमय शमय । सर्वजनं मोहय मोहय मां पालय मों श्रीं ह्रीं रं जुं झं झ्रं सं वं स्वाहा ॥]{style="color:#99cc00;"}


