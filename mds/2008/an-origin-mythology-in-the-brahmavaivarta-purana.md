
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An origin mythology in the brahmavaivarta पुराण](https://manasataramgini.wordpress.com/2008/09/14/an-origin-mythology-in-the-brahmavaivarta-purana/){rel="bookmark"} {#an-origin-mythology-in-the-brahmavaivarta-परण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 14, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/14/an-origin-mythology-in-the-brahmavaivarta-purana/ "11:19 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

brahmavaivarta पुराण 1.8.1-9:

  - [ब्रह्मा विश्वं विनिर्माय सावित्र्यां वरयोषिति ।\
छकार वीर्याधानं छ कामुक्यां कामुको यथा ॥१॥\
सा दिव्यं शतवर्षं च धृत्वा गर्भं सुदुस्सहम् ।\
सुप्रसूता च सुषुवे चतुर्वेदान् मनोहरान् ॥२॥\
विविधाञ् शास्त्र-संघांश्-च तर्क-व्याकरणादिकान् ।\
षट्-त्रिंशत् संख्यका दिव्या रागिनीः सुमनोहराः ॥३॥\
षड् रागान् सुन्दरांश्-चैव नानाताल-समन्वितान् ।\
सत्य-त्रेता-द्वापरांश्-च कलिं च कलह-प्रियम् ॥४॥\
वर्ष-मासम्-ऋतुं चैव तिथिं दण्ड-क्षणादिकम्।\
दिनं रात्रिं च वारांश् (?) च संध्याम् उषसम् एव च ॥५॥\
पुष्टिं च देवसेनां च मेधां च विजयाम् जयाम् ।\
षट्-कृत्तिकाश्-च योगां-श् च करणं च तपोधनः ॥६॥\
देवसेनां महाषष्ठीं कार्त्तिकेय-प्रियां सतीम् ।\
मातृकासु प्रधाना सा बालानाम् इष्टदेवता ॥७॥\
ब्राह्मं पाद्मं च वाराहं कल्पत्रयम् इदं स्मृतम् ।\
नित्यं नैमित्तिकं चैव द्विपरार्धं च प्राकृतम् ॥८॥\
चतुर्विधम् च प्रलयम् कालम् वै मृत्यु-कन्यकाम् ।\
सर्वान् व्याधिगणंश्-चैव सा प्रसूय स्तनं ददौ ॥९॥]{style="color:#99cc00;"}*

After brahmA had generated the world, into sAvitrI, the excellent maid, he ejaculated his semen, just like a male lover into the female lover. For 100 years she held that divine fetus that is difficult to bear. Then she gave birth and sent out: the four fascinating veda-s, various collections of शास्त्र-s, logic, grammar and the like; divine encaptivating रागिनि-s numbered thirty six, six beautiful rAga-s along with various musical scales; the satya, त्रेता, द्वापर and kali which is the age of strife, the year, month, season, tithi, daNDa (=60 विकाल-s, a water clock unit), seconds and the like, days, nights, week, twilights and dawns; पुष्टि, देवसेना, मेधा, विजया, जया, the six कृत्तिका-s, the marker stars (of नक्षत्र-s), the divisions of the day and the wealth of tapas, देवसेना, who is the great षष्ठी, the dear wife of कार्त्तिकेया, the mothers who are the supreme chosen deities of the children; the 3 kalpa-s known as brahma, padma and varaha; the quotidian and occasional events; the 50 years of brahmA (द्विपरार्ध) and the emanations of प्रकृति, the four-fold deluges, who are verily the daughters of death, and also all the groups of diseases. Having given birth to these she gave them her breast \[to suckle].


