
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पाकशास्त्र: Hindu and Hinduized alien food](https://manasataramgini.wordpress.com/2008/12/13/pakashastra-hindu-and-hinduized-alien-food/){rel="bookmark"} {#पकशसतर-hindu-and-hinduized-alien-food .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 13, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/13/pakashastra-hindu-and-hinduized-alien-food/ "5:52 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The field of पाकशास्त्र is well worthy of study as it is dying along with many other शास्त्र-s. Unfortunately, I no longer have access to some remarkable पाक texts that I examined in the city of my youth. Towards the very end of the last decade in bhArata, ekanetra inspired by ST induced me to take up the issue. ST, who is the living tradition of पाकशास्त्र, reminded me of those texts again and I only put down some notes relating to the discussion. Not having most manuscripts with me I cannot give the precise indices for the same.

There is a text called the पाकशास्त्र that I examined. The manuscript transcribed in shaka 1598 (1677 CE) is in legible nAgarI form and to my knowledge has never been published. Comparing it with the मानसोल्लास suggests that it is a late text. The language is late संस्कृत. It has 3 sections: 1) भक्तादि-प्रकरण; 2) phala-शाकादि प्रकरण 3) bhojana-प्रकरण. The 2nd and much of 3rd were stained by some liquid and not easily readable at all. The first section gives vidhi-s for preparation of the following dishes that my माताश्री was able to identify and has full knowledge of:\
पायस (milk-lentil variety); पोलिका; लड्डुक; bindu-modaka; dugdha-करञ्जिका (actually it was made very rarely, and only in my maternal lineage); जलेबी; पूरिका; वटक (the one with the hole in the middle); sUpa; पापर (the rice variety which is fried and sprinkled with pepper. I cannot forget the immense resistance we would face from our elders when we would want to eat this in the famous fair in the कर्नाट country).

ST was able identify the लापासी and कसार as गोधूम dishes that are still extant in the लाट country. But ST mentions that the wheat precipitate (which my maternal clansmen mentioned as requiring a few hours in the old days to generate), which was in the originally, is not used in the modern form. ST as a proper molecular biologist said that precipitating the material with ammonium chloride works, but the prospect of having ammonium chloride remaining, even after washes is less appetizing (?). ST has also identified and successfully reproduced फेनिका, कोहरी, बहुरी and कसार. These last ones, along with the jalebi, are actually of तुरुष्क origin. These things entered the शास्त्र and since then have become a part of Hindu existence. Keeping with the Hinduization of the तुरुष्क-bhojana we actually heard वातव्याधि indignantly declare that we were discrediting Vedic cooking :-).

Are there older, pre-मानसोल्लास पाक texts surviving? There were two dateless manuscripts that I examined which might be pre-मानसोल्लास texts. However, the condition of the manuscript as well as the writing precluded me from making a very detailed study. The first of them is the सूदशास्त्र. The सूदशास्त्र is cited by the चक्रपाणिदत्त, the great physician from Meghalaya, who wrote his medical works around 1070-1080 CE. The सूदशास्त्र manuscript had a description of the preparation of some quintessential dishes that define a Hindu: "dAl", modaka and लड्डुक. Additionally, I noticed that this text had a cake known as शष्कुली made from a large white rice and sesame and the well-known अपूप that survives in classical form only in the drAviDa country. The citation of चक्रपाणिदत्त relates to the modaka supporting that this सूदशास्त्र indeed (at least in part) is the one from before his times. In support of this at least the parts of the manuscript I could examine had no alien foods typical of the later texts. The other manuscript was समीरण-सूनु's sUpa-tantra (i.e. भीमसेन's work on cooking). This also appears pretty archaic because it has no mention of the alien foods or substances and has certain preparations that are not known today. Something recipes that could be easily identified as being still extant were: lime and tamarind rices, dal-s of different types, पायस-s and pAnaka-s. He also mentions some weird curries, such as one from the neem tree berries prepared in a special way to counter its innate bitterness. This was supposed to have remarkable curative properties.


