
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The actions of the vairi-s](https://manasataramgini.wordpress.com/2008/10/18/the-actions-of-the-vairi-s/){rel="bookmark"} {#the-actions-of-the-vairi-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 18, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/18/the-actions-of-the-vairi-s/ "4:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

On the 8th of the month the vairi sent the little imp, a चेटक, to needle us. The चेटक was repulsed and sent back. On 14th the चेटक reappeared and broke through the first line of defense and was repulsed. On 15th the ari sent a powerful stambhana prayoga at us. We were paralyzed by the prayoga and could not take any counter measures. Taking advantage of this the चेटक broke through and attacked us directly. After a strong fight we finally repulsed the चेटक. The next day the ari sent the ग्राहिन् at us which seized at night after 9.30 PM. We were shaken by the ग्राहिन्'s intense attack -- we feared we might need to deploy the indra-shakti. But like कर्ण we could use it only once, and then we would be defenseless. So were concerned about using it. We again deployed the "rudhira shara-s". But even as were tackling the ग्राहिन्, the चेटक broke through our prime fort and came again to attack us. At the beginning of the attack we felt we might be taken by both the agents. But tryambaka rudra helped us to keep the ग्राहिन् at bay, while we fought the चेटक ourself. The चेटक pierced us and flew away after we drove it with our last reserves of strength. However, since our fort had been breached we stood exposed to repeated चेटक attacks. We were wondering if we should raise our sruk and sruva to should launch a counter-prayoga against vairi-s.


