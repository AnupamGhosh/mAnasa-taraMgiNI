
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [shabdamegha](https://manasataramgini.wordpress.com/2008/07/16/shabdamegha/){rel="bookmark"} {#shabdamegha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 16, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/16/shabdamegha/ "4:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Wordle, the word cloud generator of Jonathan Feinberg offers a delectable means of generating some art as well as capturing the essence of certain texts in a pictures. It uses the visually pleasing Gentium font to render Unicode. It can render Devanagari too, but I had already begun in Romanized transliteration and had deleted to source files before running the conversion scripts. Below is a display of the first 4 upa-मण्डल-s of the ऋग्वेद. Not surprising pronouns and particles dominate the field. Yet beyond their clutter interesting patterns become apparent.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3283/2672542759_315d8c9221_o.jpg){width="75%"}
```{=latex}
\end{center}
```



[*मधुच्छन्दस् the son of विश्वामित्र

  - ]{style="color:#99cc00;"}

indra is the dominant deity of his compositions. We also note that he has a proclivity to use the word "gira"/girvan to describe his invocations.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3019/2673363418_f4dfe08bc9_o.jpg){width="75%"}
```{=latex}
\end{center}
```



*[मेधातिथि काण्व]{style="color:#99cc00;"}*

Here the deity agni seems dominant. He predominantly uses the root 'hve' in his invocatory phrases. With sandhi it should correctly be marudbhir -- the marut-bhir is an artefact of my processing macro.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3131/2673363878_5b49df14f0_o.jpg){width="75%"}
```{=latex}
\end{center}
```



*[शुनःशेप आजिगर्ति]{style="color:#99cc00;"}*

As one would expect for शुनःशेप, along with indra, वरुण also plays a prominent place in his hymns. We also note that he tends to use some repetitive phrases due to the over-representation of rare words like shubhri (great brightness) and शंसय (invoke). He tends to use नमः as an invocatory phrase, which usually others do not. He also tends to use ava (protect).
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3277/2673362056_08c3a2590b_o.jpg){width="75%"}
```{=latex}
\end{center}
```



[हिरण्यस्तुप आङ्गिरस]{style="color:#99cc00;"}

agni, indra, ashvinau, savitar are his primary देवता-s. We immediately notice that he has composed hymn/s with a distinctive style either addressing a god repeatedly in second person or frequently using the primitive Indo-European construct of tripartation (त्रिः). vajra is also relatively over-represented in his compositions. Interestingly we also note the over-representation of the word हिरण्य in his सूक्त-s relative those of others. Could this be a cryptic signature of his own name?

Beyond the idiosyncrasies which come out quite well, we see some pervasive elements of ऋग्वेदिc composition. Except हिरण्यस्तूप we note that the remaining 3 vipra-s tend to use terms related to the soma rite (soma, पीतये, suta etc.) suggesting that they were composed in that context. Not that हिरण्यस्तुप is unaware of it (one can find soma) but perhaps his सूक्त-s were predominantly used in a distinct ritual context like the fire offering (e.g the oblations to agni and savitar).
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3167/2672544501_91e71d087f_o.jpg){width="75%"}
```{=latex}
\end{center}
```



This text is to be guessed by the reader: A reasonable test for basic familiarity with Sanskritic thought :-)


