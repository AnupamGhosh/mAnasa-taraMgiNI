
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The chants of the rudra-s: a commentary based on our discussions](https://manasataramgini.wordpress.com/2008/11/09/the-chants-of-the-rudra-s-a-commentary-based-on-our-discussions/){rel="bookmark"} {#the-chants-of-the-rudra-s-a-commentary-based-on-our-discussions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 9, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/09/the-chants-of-the-rudra-s-a-commentary-based-on-our-discussions/ "7:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[*भुवनस्य पितरं गीर्भिर् आभी रुद्रं दिवा वर्धया रुद्रम् अक्तौ ।\
बृहन्तं ऋष्वम् अजरं सुषुम्नं ऋधग्-घुवेम कविनेषितासः ॥*]{style="color:#99cc00;"}

With these chants we exalt rudra, the universe's father, by day and by night;\
We specially invoke him, bright, lofty, un-aging, and well-disposed, inspired by the wise one.\
\[kavi= the wise one, i.e. rudra. In modern tantrika parlance the सुषुम्न mentioned in this ऋक् is taken to mean the सुषुम्न nerve path, but it is clear that in the vaidika context in simply meant well-disposed. This word (i.e. sumna) is often used in the context of rudra. For those who are insiders of the mantra-शास्त्र and those who are endowed with the ज्ञान, certain words reveal the deep underlying inspiration in the mantra. They who recognize the import these words are able to instantly or gradually "connect" to the mantra and cognize or see the देवता, unlike a lay practitioner, or an indologist, or one merely filled with ignorant bhakti. In the case of vaidika mantra-s this knowledge has declined among most practitioners. In the case of this mantra the key words are "bhuvanasya पितरं" and "बृहन्तं ऋष्वम्"]

  - [रुद्रस्य ये मीळ्हुषः सन्ति पुत्रा यांश् चो नु दाधृविर् भरध्यै ।\
विदे हि माता महो मही षा सेत् पृश्निः सुभ्वे गर्भम् आधात् ॥]{style="color:#99cc00;"}*

*T*hey who are sons of the bounteous rudra, whom the one capable of bearing bore;\
The mighty ones whose fetus, the great mother पृश्नि, is known to have borne for the good purpose.\
\[In the vaidika world-view rudra impregnated the mighty goddess पृश्नि (often called the cow of the heavens) with the seed that gave rise to the marut-s. The दाधृवि, which I believe is a hapax legomenon, signifies पृश्नि, the only one capable of bearing rudra's seed. In the शूलगव ritual rudra, his wife and son are invoked in 3 bovines. In association with the bull dedicated to rudra an offering is made calling rudra मीढुष, while in association with the cow dedicated to his consort an offering is made by calling her मीढुषी. The rahasya of this mantra is not easy and is apprehended via the word पृश्नि. The mantra offer the "sight" of पृश्नि.]

[*त इद् उग्राः शवसा धृष्णुषेणा उभे युजन्त रोदसी सुमेके ।\
अध स्मैषु रोदसी स्वशोचिर् आमवत्सु तस्थौ न रोकः ॥*]{style="color:#99cc00;"}

When, with fierce valor and powerful weapons, they had brought together the well-formed heavenly hemispheres;\
\[The goddess] रोदसी stood shining with her own brightness, in the midst of these impetuous ones who were like shining lights.\
\[There are two forms of the word रोदसी in the Vedic language: 1) {ro}dasI, with an उदात्त on the first syllable. This word in most cases means the two heavenly hemispheres. Very occasionally it might also mean the common wife of the marut-s (e.g. RV 5.46.8; 1.64.9; probably 10.76.1). 2) roda{sI} with an उदात्त on the last syllable. This word is always the name of the goddess who is the common wife of the marut-s. It is a good example of how tonality, which was still present in old Indo-European could alter the meaning of a word. The maruts are both conceived as the force that expands the universe hemispheres as well as holds them together: compare the above mantra with : "रोदसी hi marutash chakrire वृधे" (RV 1.85.1c) Key to grasping this mantra's rahasya-s are the phrases: "yujanta रोदसी sumeke" and "रोदसी स्वशोचिः". The later is particularly difficult.]

  - [तं वृधन्तं मारुतं भ्राजद् ऋष्टिं रुद्रस्य सूनुं हवसा विवासे ।\
दिवः शर्धाय शुचयो मनीषा गिरयो नाप उग्रा अस्पृध्रन् ॥]{style="color:#99cc00;"}*

That exalted troop of marut-s, with blazing spears, the sons of rudra, I seek with invocation; \[these] pure compositions are to receive that celestial army, the fierce ones who have battled, like mountains \[receiving] the rain.\
\[bhrajad-ऋष्टि=blazing spears. This is a common epithet of the maruts. They are seen as the army of the gods or the देवसेना. Compare दिवः shardha with: देवसेनानाम् abhi-भञ्जतीनां जयन्तीनाम् maruto yantv agram (RV 10.103.8cd). In this context it should noted that in the शूलगव or इशान-bali ritual an oblation is made to the देवसेना. The word मनीष implies a work of the intellect i.e. the chant composed by the vipra. The phrases "भ्राजद् ऋष्टिं" and "दिवः शर्धाय" help in unlocking the rahasya-s]

The graspers of the mantra-rahasya-s, who have studied these mantra-s, will be able to see a certain distant partial continuity between a vision encoded in them with the vision of certain tantrika elements. One of these elements is most explicitly articulated in the विज्ञान bhairava tantra:\
[*जलस्य्-एव्-ओर्मयो वह्नेर्-ज्वालाभङ्ग्यः प्रभा रवेः ।\
ममैव भैरवस्य्-ऐता विश्वभङ्ग्यो विभेदिताः ॥*]{style="color:#99cc00;"} विज्ञान bhairava tantra 110 ||\
Just as waves arise in water, waves of flames in the fire, and rays of light in the sun, so also the waves of the universe in their differentiated forms have arisen from me the bhairava.

While I keep referring to the rahasya-s repeatedly, I do not expound them openly here. That is because they are for other दीक्षित-s to discover by themselves. That is why only certain hints are provided. In an earlier phase of life with some zeal I used to openly discuss such rahasya-s, but these neither benefited me nor everyone one who listened -- after all we do not live solely amongst Arya-s. It taught me that rahasya-s of the kavI-s have remained so for a reason. They are to be disclosed only within the secret circle of discussants.


