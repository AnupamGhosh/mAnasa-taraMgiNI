
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [3 कौमार yantras with tangent circular curves](https://manasataramgini.wordpress.com/2008/01/28/3-kaumara-yantras-with-tangent-circular-curves/){rel="bookmark"} {#कमर-yantras-with-tangent-circular-curves .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 28, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/01/28/3-kaumara-yantras-with-tangent-circular-curves/ "6:52 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_hjuA1bE0hBw/R54jTVPQosI/AAAAAAAAAB8/4roD8EDG2aE/s320/kumAra_vRiddhi.png){width="75%"}
```{=latex}
\end{center}
```

\
The वृद्धि chakra (a star formed with 2 smooth fixed-with curves)

 

![](https://i0.wp.com/bp2.blogger.com/_hjuA1bE0hBw/R54jTlPQotI/AAAAAAAAACE/WP9O1tiFul4/s320/kumAra_jaya.png){width="75%"}

The shatru-jaya chakra


[](http://bp3.blogger.com/_hjuA1bE0hBw/R54jT1PQouI/AAAAAAAAACM/YjY5LyMaYGc/s1600-h/kumAra_सर्वार्थ.png)

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp3.blogger.com/_hjuA1bE0hBw/R54jT1PQouI/AAAAAAAAACM/YjY5LyMaYGc/s320/kumAra_सर्वार्थ.png){width="75%"}
```{=latex}
\end{center}
```



सर्वार्थ-दायक chakra

