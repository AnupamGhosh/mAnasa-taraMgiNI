
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The wives of Mars and the Carmen Arvale](https://manasataramgini.wordpress.com/2008/10/04/the-wives-of-mars/){rel="bookmark"} {#the-wives-of-mars-and-the-carmen-arvale .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/04/the-wives-of-mars/ "2:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In our earlier discussion we had argued based on structural homologies the marut-s of the vedic system are "para-skanda". We speculated that कुमार himself descends from a para-vedic Indo-Iranian system, which contained an ancestral version of the deity, who in that system was the homolog of the marut-s of the vedic system. This para-vedic skanda was laterally transferred to the vedic system in the late vedic period of India. This transfer most probably occurred by an independent Indo-Iranian invasion from the Northwest frontiers of India, prior to the historical Iranian invasions of the Achaemenids, Sakas and Kushana-s. By developing this line of argument further we accepted the possible proto-Indo-European (probably excluding Hittite) origin of the deity. In this light we are reasonably comfortable with the homology of Mars and the Maruts: Given that the older Italic form Mars is Mavors, their proto-IE ancestor can be accepted as \*Mawort. The only philological problem that does bother us a little is the वृद्धि of the initial vowel in the Italic branch.

There are a few other interesting features that more tenuously connect these deities and skanda in a tangled network, which we did not discuss in our earlier exposition of this topic. One of these is the issue of the wives of Mars. The only more archaic data in this regard comes from the Roman author Aulus Gellius in his book Noctes Atticae (13.23.2). In describing "deva-patni" pairs he mentions that he has collected such pairs from the "libri sacerdotum populi Romani" (i.e. ritual texts of the Romans) and "plerisque antiquis orationibus" (i.e. मङ्गलाचरण equivalents found in the beginnings or ends of ancient orations). Two wives of Mars are mentioned in the phrases: Moles Martis and Nerienemque Martis /Nerio Martis. These dual names parallel the homologous Indo-Aryan system, where the name of the goddess is mentioned before the god (e.g. उमा-maheshvara or Italic Maia Volcani). Both these wives have interesting etymologies: Moles means the throngs or army like सेना in संस्कृत. She might have been synonymized or syncretized with Bellona (meaning war) who is also later depicted as the wife or sister of Mars. Nerio or Neriene was later identified with and syncretized with Minerva. But her name may be derived from the PIE root \*ner, which in one direction gives rise to forms like nara or नृम्ण in Sanskrit. It might also mean valor in this context or warlike nature. In the Indic world सेना or देवसेना the wife of skanda and the army of the gods, a term specifically associated with the marut-s or the rudra-s might be seen as parallels.

Finally, we may also take a look at one of the most archaic Latin chants recited in a sacrificial ritual of Mars that has survived to date -- the Carmen Arvale. The translation of this Carmen is really not perfect because: 1) it is in old Latin and 2) Modern White translators, cut off from their heathen roots, miss certain nuances. It was recited by the Arval priest -- the emperor of Rome was by default an Arval priest. It was probably during the ritual of Mars during the Ambarvalia rite in May. In this chant, along with Mars, two other sets of deities who had become obscure even in Roman times are invoked:\
Carmen Arvale without the triplicated or pentaplicated elements:\
[एनोस् ळसेस् इउवते ]{style="color:#99cc00;"}\[Invocation of the deities called Lares; note the Latin verb iuvate [=aid/support] in the imperative form comparable to the imperatives found in Sanskrit mantra-s]\
[ *neve lue rue Marmar sins incurrere in pleoris*]{style="color:#99cc00;"} \[invocation of Mars]\
*[ satur fu, fere Mars, limen sali, sta berber]{style="color:#99cc00;"}* \[The term satur fu appear to be a functional analog of the Sanskrit act of तर्पण; verbal root तृप्. Thus, it may be seen as a तर्पण of Mars]\
[सेमुनिस् अल्तेर्नि अद्वोचपित् चोन्च्तोस्]{style="color:#99cc00;"} \[Here the deities called the Semones are invoked]\
[एनोस् ंअर्मोर् इउवतो]{style="color:#99cc00;"} \[Mars is called for help again]\
*[त्रिउम्पे!]{style="color:#99cc00;"}*\[victory: the analog of the Sanskrit invocation jaya!]

These two sets of plural deities the Lares and Semones are troops of gods who accompany Mars and thus appear to be cognates of the marut-s in the plural sense. The Semones are etymological connected to fallen seeds. This might parallel the fertilizing seed of rudra which was the source of skanda.


