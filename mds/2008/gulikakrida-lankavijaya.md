
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [गुलिकाक्रीडा: लङ्काविजय](https://manasataramgini.wordpress.com/2008/08/28/gulikakrida-lankavijaya/){rel="bookmark"} {#गलककरड-लङकवजय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 28, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/28/gulikakrida-lankavijaya/ "5:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The sunlit shores of लङ्का, just like the beaches of Southern Saxony where the peerless pirates of emperor Ponting roam like fork-tongued monitors, have been a charnel ground for the Hindu armies over the ages. They have been thrashed over and over again by the great island pirates led by the likes of the corpulent heroes, दुलीप mendisa and रणतुञ्ग, and the mauler from Matara, जयसूर्य. Just as in history the armies and navies of कुलोत्तुङ्ग and कुमार कंपण faltered at the island frontier, so also modern नौसेना-s in the आन्गलीक गुलिकाक्रीडा had sunk like the efforts of kha'khan Kublai before the kamikazes of Nippon. In the recent past the Hindu armies have been involved in uncertain encounters in the extended battles under their veteran field gunner of the कर्नाट country. In course of these encounters the four great warriors of the yesteryears, the rAjA of Mumbai, the rAjA of Bengaluru, the वङ्ग king and the Andhra chieftain declined due to injury and age and were unable to hold their own in the extended battles of गुलिकाक्रीडा. The young warriors like the bald सुग्रीव and the swaggering युवराज also failed to live up entire to their potential as they were undermined by the Australian mole chappal. It was in these trying times that the young राजन्य mahendra सिंह of Jharkhand came to the fore. He first showed his abilities as a leader of men in the great raid in South Africa where he lead the Hindu army to a glorious victory against the green banner of Islam. Then when the Anglo-Saxon pirates pressed hard on the Hindus from their Southern outpost, which they had secured after annihilating the aborigines, mahendra सिंह led the सेना to an incredible victory against the arrogant श्वेतवर्ण-s.

In the most recent invasion of लङ्का the extended battles under the leadership of the old field gun of Bengaluru had failed with the forces of स्थविरवाद fanaticism led by great lords like jayavardhana, कुमार सङ्गकार and anjantha, and hindu mercenaries like muttiah running amok amongst the Hindu सेना-s. Then rAjA mahendra सिंह was called to lead a series of short attacks to reverse the humiliation we had just faced. In the first such attack mahendra सिंह's men were put to sword by the fury of the लञ्कापति-s. Then he regrouped his armies and led the Hindu-s to conquest of the final frontier. In the last of these battles fought at koyumbu the स्थविरवादिन्-s attacked fiercely with their three front line cannonries under tushara, चमिण्ड and kulashekhara. The Hindus wilted under their bombardment with their warriors like गम्भीर, the chief of dillika and विराट्, the kohli chief being slain after an initial thrust. Things turned worse as चमिण्ड knocked down the arrogant rAjpUt charger युवराज of पञ्चनद. With defeat staring on their face king mahendra सिंह and suresha a chief from the पञ्चल realm came together to battle the surging islanders. The islanders sought to destroy the Hindu king by sending their ace gunners ajantha and the Hindu mercenary muttiah. But the Hindus, after evading cautiously for a while, counter-attacked -- mahendra luring the लङ्कापुरि-s to an ambush and suresha slaughtering them with his repeated cavalry charges. Then the Hindus sent forth the assorted तुरुष्क-s mercenaries under Munafa and Khan to attack the islanders. As their ranks wavered under the heavy shelling of the तुरुष्क-s, the ground started giving way under the fading light. Undaunted the great लञ्का chief जयसूर्य counter-attacked and took the battle right back to the Hindus wreaking havoc like नरान्तक on the वानर army. At this point the Hindus deployed the foul-mouthed sniper haribhajana from पञ्चनद who broke up the लङ्कन् ranks with his well aimed shots at their leaders including the king jayavardhana. At this point युवराज rose back to consciousness and had his revenge by putting to sword the rest of the लङ्का army. With that the saffron flag fluttered on the त्रिकूट hill for the first time.

Other narratives of struggles in गुलिकाक्रीडा: before you get too hot remember it is just a game:-)

  -  [Frontline warriors](../2003/09/06/frontline-warriors/)

  -  [The mother of all battles](../2003/12/16/the-mother-of-all-battles/)

  -  [The Trojan horse?](../2005/11/12/the-trojan-horse/)

  -  [The sub-continental stirrings](../2005/12/18/the-sub-continental-stirrings/)

  -  [The clash of Saxons](../2005/08/28/the-clash-of-saxons/)

  -  [सुग्रीव and the prince of B'lore](../2006/06/11/surgriva-and-the-prince-of-blore/)

  -  [The victory of the prince of the Bangalore](../2006/07/10/the-victory-of-the-prince-of-the-bangalore/)


