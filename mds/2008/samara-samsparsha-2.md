
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [samara-संस्पर्ष-2](https://manasataramgini.wordpress.com/2008/04/14/samara-samsparsha-2/){rel="bookmark"} {#samara-ससपरष-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 14, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/14/samara-samsparsha-2/ "2:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our जनाः clashed with the ari-dala as expected. Verily these vile खाण्डवन्स् are like ahi full of wiles. They began by needling our sena as per the game plan relayed from their supreme headquarters. Our सेना retaliated strongly and a pitched battle was fought for several hours with our सेना keeping up the arrowy showers against the दुष्ट-s of खाण्डव. The ari-dala realized they could not pierce our व्यूह and decided to resort to their time-tested tactics of कूट-yuddha for which they were renowned as the गन्धारन् shakuni. These taskara-s spread a smokescreen against our सेना and sent forth phantom figures to engage them. Our जनाः engaged them not knowing that they had been tricked as the खाण्डवन्स् escaped from this pointed attack of our men and made away laying traps and snares for our men in case they followed them.

The making use of the time they gained with their smoke-screen the कुलुञ्च-s made an audacious plan to attempt an attack on our fort and the second kosha and steal our dhana. To back it up they also dispatched a prayoga against us that we noticed hitting us.


