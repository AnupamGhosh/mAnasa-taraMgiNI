
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mind in a whirlpool](https://manasataramgini.wordpress.com/2008/10/14/the-mind-in-a-whirlpool/){rel="bookmark"} {#the-mind-in-a-whirlpool .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 14, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/14/the-mind-in-a-whirlpool/ "6:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3250/2940186485_6c0d538f0b.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3195/2940185667_965e76c2b6.jpg){width="75%"}
```{=latex}
\end{center}
```



