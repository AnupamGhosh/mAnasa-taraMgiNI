
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A yogin and his दूती](https://manasataramgini.wordpress.com/2008/11/04/a-yogin-and-his-duti/){rel="bookmark"} {#a-yogin-and-his-दत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/04/a-yogin-and-his-duti/ "6:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Being fooled by the outward appearance of a prescription in a tantra, or by his instincts, or in modern times by pop culture, the common practitioner is incapable pairing with a दूती for successful attainment of yoga. For such a common practitioner the kaula path, like the kula of विलासिनी-s, is not available. The kaula practice with a दूती is only fruitful for the true yogin. For this yogin the दूती truly manifests as his shakti. The yogin who is free from disease, who is possessed with the virility of 3 or more bulls, and who is in complete control of his retas and unmoved by the appearance of sundarI-s will alone be easily able to recognize a दूती. To such a yogin the true दूती will automatically appear as an अभिसारिका. In a dark, quite place at night, ideally a श्मशान or a grove, he will meet her and mentally communicate without saying anything. The success in this communication will indicate the successful discovery of the दूती. He will then go away and meet her again after a while, all the while controlling his senses. After this he is ready to rove in company of the दूती to attain yogic perfection.


