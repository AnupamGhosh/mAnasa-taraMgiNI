
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Long gone cephalopods](https://manasataramgini.wordpress.com/2008/11/12/long-gone-cephalopods/){rel="bookmark"} {#long-gone-cephalopods .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 12, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/12/long-gone-cephalopods/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3175/3023696809_f5b1e20030.jpg){width="75%"}
```{=latex}
\end{center}
```



Over the ages cephalopods have attained large sizes on many occasions. The earlier Paleozoic radiations of cephalopods were dominated by the various lineages called nautiloids. Of these, one lineage, which was prevalent from the Ordovician to the Silurian, was Endoceratida, and these included giant nautiloids like *Endoceras* which reached up to 4 meters and *Cameroceras* which is reputed to have reached over 10 meters (in shell size). Thus, even before the vertebrates came to the fore as a major faunal component these molluscs had reached enormous sizes, and were the largest invertebrate predators of all times. But even after the dominance of the vertebrates was established in course of the Devonian large forms continued to emerge in other nautiloid lineages. The best known of these is *Rayonnoceras* of the Actinoceratida clade that attained shell sizes of 2-2.5 meters during the Carboniferous. However one of the most successful lineages was Orthoceratida, which in turn is believed to have spawned the Bactritida from which in turn the coleoids and ammonites are believed to have emerged. The ammonites attained large sizes with forms like *Parapuzosia* from the Cretaceous Campanian reaching shell diameter of 2.5 meters. Even larger but apparently unpublished ammonites have been reported in certain Canadian sites. A Cretaceous coleoid *Tusoteuthis* probably reached about 8 meters including tentacle length. Today the largest coleoids, the squids, like the *Mesonychoteuthis* and *Architeuthis* are believed reach 13-14 meters including tentacles are amongst the most impressive marine predators. *Enteroctopus* with tentacle length considered has been recorded to reach over 6 meters. Thus, large cephalopods from different lineages have been seen in practically every era, though different lineages have declined or expanded over time. It is interesting to note that the largest fishes and sharks do not vastly exceed the size of the largest cephalopods if at all. However, tetrapods that returned to the sea, like ichthyosaurs, plesiosaurs and whales gave rise to some of the largest ever animals. This might be related the inherent metabolic limits of extracting oxygen from water as against using air.


