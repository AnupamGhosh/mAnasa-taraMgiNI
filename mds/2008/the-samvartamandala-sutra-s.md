
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The संवर्तामण्डल sUtra-s](https://manasataramgini.wordpress.com/2008/03/05/the-samvartamandala-sutra-s/){rel="bookmark"} {#the-सवरतमणडल-sutra-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 5, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/05/the-samvartamandala-sutra-s/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[॥ ऒं नमो अकुलवीराय ॥]{style="color:#0000ff;"}

We beheld him, he who stands with the trident in the midst of the blaze of the dissolution of the universe. We beheld the 62 taijasa मयूख-s shooting forth! We worshiped nandin at ऒड्डियान, कार्त्तिकेय at पूर्णगिरि and vighna at kAmarUpa.

\~*\~*\~

[तव स्वाधिष्ठाने हुतवहम् अधिष्ठाय निरतं]{style="color:#0000ff;"}\
[तम् ईडे संवर्तं जननि महतीं तां छ समयां ।]{style="color:#0000ff;"}\
[यदालोके लोकान् दहति महति क्रोध कलिते]{style="color:#0000ff;"}\
[दयार्द्रा यद्दृष्टिः शिशिरम् उपचारं रचयति ॥]{style="color:#0000ff;"} saundaryalahari 39

O geneatrix, I worship in your स्वाधिष्ठन, संवर्त presiding over the fiery realm, and also that great समया, whose glance moist with mercy initiates the cooling winter to the worlds when his (संवर्त) wrathful eruption \[fire from the 3rd eye] burns the worlds.

The core of the समया-kula doctrine is provided by the संवर्तामण्डल sUtra-s with which the principal tantra-s of the पश्चिमाम्नाय open. The संवर्तामण्डल sUtra-s of the कुब्जिका-mata tantra are shorter than those of the षट्साहस्र-saMhitA, hence I provide the latter -- the बृहत्-संवर्तामण्डल sUtra-s. I had a deep discussion of these and the गोरक्षसंहिता with R1's father who provided me with a text of them. A printed version was published by the late Dutch scholar Jan Schroterman using 3 Nepali manuscripts. R1's father and his father had access to multiple Nepali manuscripts, which might have even perished since. They also studied the commentary the संवर्तार्थ-प्रकाश which deals with the बृहत् version. R1's father explained that the commentarial tradition records 9 human teachers of the doctrine of different वर्ण-s. This choice of nine of the teachers seems to be an attempt to keep in line with the bhairava's name नवात्मन्.

***ब्राह्मण teachers:***\
trivikrama deshika, a सामवेदिन् from ओड्डियान.\
दामोदर deshika, a ऋग्वेदिन् from the Konkans.\
govinda from मथुरा\
विश्णुशर्मन् from kAshI.\
***kShatrIya teacher:***\
उदयराज from Orissa\
***vaishya teacher:***\
dehila, a merchant from Andhra (श्रीशैल)\
***शूद्र teachers:***\
बाहिल from Assam\
sehila from Kashmir\
माहिल, a liquor-store owner from the कुन्दापुर sector of ओड्डियान

An interesting biographical tale is narrated regarding trivikrama deshika: He was born in a learned Brahmin family in ओड्डियान, where he learned the सामवेद and as well as prayoga-s from all other veda-s and became a notable scholar. trivikrama was sent as an ambassador to Shri Lanka, where he was converted to the थेरवाद bauddha school. He then became a governor of a Lankan province and began persecuting Hindus with much vigor. Once he encountered a मन्त्रवादिन् named उन्मनिशनाथ in a graveyard performing साधना. He tried to wake him up but failed. Finally, that mantrin awoke and looked at trivikrama and he had a sight of the glory of समया and aghora (as in saundarya-lahari 34/39/41). He returned to the Hindu fold and became a teacher of the kula doctrine. He was attacked by पाषण्ड mantra-वादिन्-s who followed the doctrine of shuddhodhana-putra at कान्यकुब्ज, but he decimated them using the खादकास्त्र mantra combined with the नृसिंह or the trivikrama mantra. He then assumed the name व्योमानन्दनाथ.

In both the कुब्जिकामत and षट्साहस्र saMhitA the sUtra-s are worked into verse approximating the स्रग्धरा Chandas. I have marked the individual sUtra boundaries with '..'

The बृहत् संवर्तामण्डल sUtra-s (षट्साहस्र saMhitA)\
[॥ हुं शिवाय श्री-कुल-गणाधिपतये नमः ॥]{style="color:#0000ff;"}

[। संवर्तामण्डलान्ते क्रम-पद-निहितानन्द-शक्तिः सुभीमा ।]{style="color:#0000ff;"}\
[। सृष्टि-न्याये चतुष्कं अकुल-कुलगतं । पञ्चकं cआन्य-षट्कं ।]{style="color:#0000ff;"}\
[चत्वारः पञ्चकोऽन्यः पुनर् अपि चतुरः । । षोडशाज्ञाभिशेकं ।]{style="color:#0000ff;"}\
[ . देव्याष्टौ मूर्ति-madhye . . \*ha sa kha pha ra कला बिन्दुपुष्पं* . . kha-mudrA . ||1||]{style="color:#0000ff;"}

[। बालं कौमार-वृद्धं । । परम-शिव-कला चक्रदेवी क्रमानां ।]{style="color:#0000ff;"}\
[। श्रीनथम् चन्द्रपुर्यां नव-नव-कलितं युग्म भेदैस् ति सारं । ।]{style="color:#0000ff;"}\
[। सिद्धास् त्रीण्य् अवतारं । । प्रथम कलियुगे कोङ्कणे चाधिकारं ।]{style="color:#0000ff;"}\
[। तेषं वै पुत्र-शिष्या । । नव-पुरुष-क्रमास्-तेषु मध्ये द्विराष्टौ । ॥२॥]{style="color:#0000ff;"}

[। सन्तानं गोत्र-पिण्डं क्रम-पद-सकलं षोडशान्तरं क्रमान्तं]{style="color:#0000ff;"}\
[शेषा वै मण्डलाणां परि-भ्रम-विमलं पूजय-सद्भाव-वृन्दं । ।]{style="color:#0000ff;"}\
[। आदाव् अष्टादशान्तं कुल-क्रम-सकलं मण्डलोत्थान-पूर्वं ।]{style="color:#0000ff;"}\
[। संस्कारं त्रिह्-क्रमोत्थम् पशु-मल-क्षय-कृत् पिण्ड-शुद्धिः शिवाग्नौ । ॥३॥]{style="color:#0000ff;"}

[। मध्ये विश्राम-भूमौ प्रसरम् अनुभवं प्रत्ययं स्वाधिकारं]{style="color:#0000ff;"}\
[संसृष्टं येन तस्मै नमथ गुरुवरं भैरवं श्रीकुब्जेशम् । ॥४॥]{style="color:#0000ff;"}

[। वृत्त्याध्युष्टक्रमार्थं रचितम् अनुभवं खञ्जिनी-मूर्ति-पूर्वं ।]{style="color:#0000ff;"}\
[। दिव्यौघं देव-संज्ङं पुनर् अपि अपरं मानवं त्रिः-क्रमौघं । ।]{style="color:#0000ff;"}\
[। भेदानेकैर् विभिन्नं सकल-पद-क्रमं मण्डलं षट्प्रकारं ।]{style="color:#0000ff;"}\
[। सङ्केतं कादिपूर्वं । । सकल-गुण-युतं मण्डलं भैरवं तम् । ॥ ५ ॥]{style="color:#0000ff;"}


