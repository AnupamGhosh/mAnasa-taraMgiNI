
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Another piece of काव्य by the muni](https://manasataramgini.wordpress.com/2008/08/16/another-piece-of-kavya-of-the-muni/){rel="bookmark"} {#another-piece-of-कवय-by-the-muni .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 16, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/16/another-piece-of-kavya-of-the-muni/ "5:33 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The muni and me, who are like one atmAn in two different sharIra-s; hence I took the liberty of posting another of his literary productions. Unfortunately the muni has neither used Itrans nor unicode.

***[Asthapada and Kesin, being an ode to Rati and Manmatha]{style="color:#009900;"}***

[*There lived in a dusty corner Asthapada.*\
* Silent, unnoticed, and brooding, like a monk*\
*in his cave, did he bear out his days in the dusty, grey lair.*\
* Many passed that way everyday, rounding the corner,*\
*hurrying on towards the call of duty, their minds fully occupied.*\
* Thus it happened that none saw Asthapada, crouched in his web.*\
*And in a way this was all for the good, for if they saw him*\
* they would slay him with a broom, as is the wont of men.*]{style="color:#ff9900;"}

[*Only one among many did see that which escapes men's eyes.*\
* For he was a bard who saw, by putting mind and heart together,*\
*that which lies beyond the dull glare of shrouded objects.*\
* It is indeed destined by the gods that from the eyes of a few*\
*shoot forth radiant lances that pierce the surrounding patina,*\
* striking the weak spot in Virinci's armour, as it were,*\
*and exposing the hidden wonders of his cavernous realms ---*\
* not just by tors and tarns, sedges and swards, but even in the grey dust.*]{style="color:#ff9900;"}

[*And thus it was that he saw the hirsute Asthapada, in his grim, grey corner.*\
* In no hurry was he, nor any oppressive thoughts clouded his mind.*\
*So, time was all his to talk and make merry with all creatures, big and small.*\
* He was the friend of them all, the knower of their language and their minds.*\
*He addressed Asthapada thus : "O Asthapada of fierce aspect, how is it that thee livest*\
* in such a place, where Pusan's rays scarce enter, and not to speak of food.*\
*For, all the masterful insects prowl by the lights. What will you get here?*\
* Besides, the delights of women this lair can't give, for none pass by this lonely spot."*]{style="color:#ff9900;"}

[*"Tell me Asthapada, are you a Sanyasin or what? Have you no need for food and love?*\
* Come outside and look at the wide world --- all buzzing with bees and wasps,*\
*their sentries guarding the fortresses of madhu, the whirring flies, droning dung beetles,*\
* Silent moths, mosquito hordes and dragonflies, the streaming lines of soldier ants,*\
*as warrior band sets out to conquer, just as the Bharatas in search of cattle,*\
* Teeming centipedes or fireflies, beneath crevice and under log or upon tree and by street-lamp*\
*Every corner of the rich earth they occupy, where prey is plenty and women abound.*\
* Take your own cousins, Asthapada, in the fur of dogs and horses they make merry."*]{style="color:#ff9900;"}

[*"Beneath the damp rock lies the Deadly One, with sting held aloft,*\
* Verily like the club of Yama, waiting to slay curious adventurer.*\
*Asthapada, you have not seen the deadly mantis, concealed amid the green blades.*\
* Although lost in prayer he seems, he is a knight of valor beyond compare.*\
*Yet you live on in this remotest of lairs, Asthapada,*\
* and what chance have you of feasting and mating like them?*\
*Even the all pervading Matarishvan seems not to care for your dark, lonely world. "*\
* Asthapada heard all this in silence, as if to say, "Wait and see, you stripling"*]{style="color:#ff9900;"}

[*The next day he passed that way again, and wonder of wonders :*\
* In Asthapada's web lay entangled something, furiously struggling.*\
*But the swift Asthapada nailed him in trice with his powerful legs.*\
* Sucked dry, the carcass was hurled, a few hours later.*\
*And seeing the wonderous sight, he thought to himself,*\
* "How is it that some quarry should chance to wander*\
*into that realm which seemed so totally uninhabited, and of all things,*\
* fall into Asthapada's den? What propelled him there?"*\
* *\
*Many days passed on and the incident was forgotten.*\
* Ascetic Asthapada lived on, his monastic ways unwavering,*\
*Absolutely unconcerned about anything, just waiting and watching.*\
* One gloomy day when the clouds mounted the heavens and all*\
*was dark, and even the vibrant bard despondent he addressed his friend thus :*\
* "Your life is a waste, Asthapada, as is mine. Here we will bear out our days*\
*util Yama takes us. And we will leave behind nothing --- not even our memories. *\
* Sometimes fate plays tricks by placing people in such stations*]{style="color:#ff9900;"}

[*It would seem that Rati and Manmatha in highest heaven heard these words.*\
* For, Manmatha, of floral bow, glancing at his doe-eyed consort said :*\
*"Let's have fun, Rati. Surely, Asthapada deserves better luck. Come."*\
* So they mounted their chariots and set forth towards the dark realm.*\
*But on the way they met Dharmaraja riding on his buffalo.*\
* "Salutations, O Kama. Wither are you going?," says Yama.*\
*"To give meaning to Asthapada's life." said Manmatha and hurried on.*\
* "But his time is up, O Manmatha." said Yama.*]{style="color:#ff9900;"}

[*The next day the poet happened to pass by the desolate corner.*\
* And lo behold! But what should he see in the lonely monastery of strands?*\
*Asthapada was not alone. But now there were two more like him.*\
* There was the beautiful Kesin, prancing about the silver strands,*\
*with her hairy legs, like an impatient charger prancing about before a charge.*\
* Besides her, was her other suitor, Sahasraksha, with his many eyes jutting out from his forehead.*\
*The sight filled him with both joy and surprise.*\
* "How on earth did they get there? How did they seek him out here? " he wondered*]{style="color:#ff9900;"}

[*But there was no time for wonder, for soon a most fierce duel broke out.*\
* Asthapada was the larger, and the fiercer is true,*\
*But Sahasraksha was not to be left far behind, for he was deft and virulent.*\
* Asthapada lunged forward and tried to hold Sahasraksha in his grip.*\
*But he nimbly veered away, and struck out at Asthapada.*\
* Kesin watched on breathlessly : the stronger, better one would win the day and her hand.*\
*Asthapada reeled and retreated to a farther corner.*]{style="color:#ff9900;"}

[*The whole silvery web seemed to throb as the mighty contestants clashed,*\
* Just as Suyodhana and the son of Kunti, with their maces, in the days of yore.*\
*Asthapada presently made another rush and speared Sahasraksha.*\
* Even as Sahasraksha was thus impaled, Asthapada came down heavily upon him.*\
*A decisive blow it was-- Sahasraksha's limb was severed and it fell, throbbing in the mesh.*\
* Still the valiant, inexorable Sahasraksha fought on, hobbling on seven now.*\
*Finally, Asthapada held him firmly in his iron grasp and shook him vigorously,*\
* like Anjaneya shredding a tree. Sahasraksha fell lifeless.*]{style="color:#ff9900;"}

[*His deflated corpse, sucked dry of all vitality was hurled away in disdain.*\
* His maiden having thus been won, the bard left the spot,*\
*expecting him to celebrate his victory by throbbing in nuptial frenzy with Kesin,*\
* inevitably struck by Manmatha's dart, deluded utterly by her charms,*\
*and falling head over heels in his own web!*\
* A fairy tale ending : a great duel, victory, a wife, and then living happily ever after.*\
*But Yama was right afterall : an utter shock was to greet the poet's eyes.*\
* The next day, came the bard as usual, his old friend to meet.*\
* *\
*But what should he see at the now familiar resort? All empty and desolate!*\
* Both Sahasraksha and Asthapada lay dead in the web ---*\
*a great resemblance in their grey lifelessness,*\
* only, Sahasraksha's corpse more decayed than Asthapada's.*\
*The bloated Kesin had slain Asthapada, her sole work having been done.*\
* She deposited a large sack and left. Such are the ways of the impregnators.*\
*By sheer chance did Kesin come across Asthapada.*\
* Impelled by Kama did Asthapada finally defy even Yama: a hundred Asthapadas in that sack!*]{style="color:#ff9900;"}


