
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The atharvashiras preserved in the purANic texts](https://manasataramgini.wordpress.com/2008/06/16/the-atharvashiras-preserved-in-the-puranic-texts/){rel="bookmark"} {#the-atharvashiras-preserved-in-the-puranic-texts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 16, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/16/the-atharvashiras-preserved-in-the-puranic-texts/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I just decided to record this note even though I did not complete the earlier one on the PBU. Upon reading my note on the PBU (as it currently stands), ekanetra made an inquiry regarding the atharvashiras उपनिषद्(ASU), which alludes to the ancient पाशुपत vrata. ekanetra correctly noticed that at the heart of the ASU is the ऋक् on which the बृहत् sAman is composed. He wondered if this might have a connection with the sAman-s songs recited by the कापालिक-s as per the testimony of the sarva-siddhAnta viveka. Currently two versions of this famous पाशुपत उपनिषद् are known, namely one preserved by the medieval commentator नारायण and another by the commentator शङ्करानन्द. But there is third version that has been much ignored. This is the version found in sections 2.17-18 of the लिङ्ग पुराणं. This provides evidence, independent of these medieval commentators, that this उपनिषद् was indeed prevalent amongst the पाशुपत-s. This पौराणिक version also preserves the ऋग्वेदिc mantra on which the बृहत् is sung, suggesting that it was an integral part of the original माहेश्वर system of the atharvashiras.


