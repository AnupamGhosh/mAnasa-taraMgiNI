
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [King harihara-II praised as a portion of skanda](https://manasataramgini.wordpress.com/2008/09/22/king-harihara-ii-praised-as-a-portion-of-skanda/){rel="bookmark"} {#king-harihara-ii-praised-as-a-portion-of-skanda .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 22, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/22/king-harihara-ii-praised-as-a-portion-of-skanda/ "6:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The nallur plate of the vijayanagaran king harihara-II praises him as a portion of skanda i.e. स्कन्दाम्श. The part of the inscription that can be made out reads thus:

  - [धर्मेण रक्षति क्षोणीं वीर-श्री-भुक्क-भूपती ।\
निरातङ्काभयात्-तस्मिन् नित्य-भोगोत्सवाः प्रजाः ॥\
गौरी-सहचरात्-तस्मात् प्रादुरासीन् महेश्वरात् ।\
शक्त्या प्रतीत स्कन्दांशो राजा हरिहरेश्वरः ॥]{style="color:#99cc00;"}*\
When noble valiant king bhukka protected the earth with dharma, his subjects unafraid due to absence of terror (i.e. Islamic depredations) continually enjoyed festivities.\
From that great lord who moved with गौरी (the name of bhukka's queen), was born with power the king lord harihara who was considered a portion of skanda \[this essentially plays on the terms maheshvara -- king bhukka or shiva, गौरी -- पार्वती or the queen and shakti -- the power of harihara-II or the weapon of कुमार].


