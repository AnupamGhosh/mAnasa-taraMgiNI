
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [shrauta ritualists 1900-2008- an approximate lower bound estimate](https://manasataramgini.wordpress.com/2008/02/27/shrauta-ritualists-1900-2008-an-approximate-lower-bound-estimate/){rel="bookmark"} {#shrauta-ritualists-1900-2008--an-approximate-lower-bound-estimate .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 27, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/27/shrauta-ritualists-1900-2008-an-approximate-lower-bound-estimate/ "3:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I have seen a bunch of estimates by Kashikar et al, Kannan and via various personal contacts. Drawing from all of them I come up with some numbers. These numbers can only be considered the rough lower bound

Tamil Nad (स्मार्त-s and srI-वैष्णवस्): 145 (found also secondarily displaced to Kashi e.g. Purushottama Dravid, a great soma ritualist)\
Kerala (नम्बूथिरिस्): 125\
Karnataka (mainly havayaka and some tulavas): 50\
Andhra (mainly वैदीकि स्मार्त-s): 160\
Maharashtra (deshashtha, कोन्कणस्थ and कर्हाडे): 100 (more than half for them live outside महाराष्ट्र in Kashi); Maharashtra also had the only शूद्र who had performed a shrauta ritual in recent memory.\
Rajasthan (ब्राह्मण-s and kShatriya-s): 10 (Rajasthan is the only place were royal आहिताग्नि-s existed until recently. Some ब्राह्मणस् of Rajasthani origin live in Kashi)\
Himachal: 5\
Gujarat: 8\
UP (mishra-s and other native UP brahmins): 8\
Some say that the greatest modern ritualist is the Andhra स्मार्त: Renduchintala Venkatachala Yajulu. He performed numerous extraordinary and rare rites. Amidst the स्मार्त-s of TN, Narayanaswami Dikshitar, also उद्गातर्, a clansman on my maternal side was the greatest recent performer of rare and complex याग-s.

We provide a normalized table below using the approximate estimates of V1s per state:

    |STATE            |  # |N/10^6 |
    |-----------------|----|-------|
    |Uttaranchal      |   3|   0.74|
    |Himachal Pradesh |   5|   3.64|
    |Uttar Pradesh    |  95|   4.32|
    |Delhi            |   3|   1.78|
    |Maharashtra      | 100|   8.89|
    |Rajasthan        |  10|   1.82|
    |Gujarat          |   8|   1.47|
    |West Bengal      |   2|   0.27|
    |Andhra Pradesh   | 160|  37.83|
    |Karnataka        |  50|  20.45|
    |Tamil Nadu       | 145|  80.39|
    |Kerala           | 125| 212.60|


