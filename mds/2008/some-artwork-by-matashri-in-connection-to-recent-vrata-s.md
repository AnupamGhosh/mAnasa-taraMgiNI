
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some artwork by माताश्री in connection to recent vrata-s](https://manasataramgini.wordpress.com/2008/11/05/some-artwork-by-matashri-in-connection-to-recent-vrata-s/){rel="bookmark"} {#some-artwork-by-मतशर-in-connection-to-recent-vrata-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 5, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/05/some-artwork-by-matashri-in-connection-to-recent-vrata-s/ "7:40 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some of the family images in display
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3036/3005400187_85ae3e2d03_o.jpg){width="75%"}
```{=latex}
\end{center}
```



[शिव-पूजा]{style="color:#99cc00;"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3289/3006235604_ff19f6a644_o.jpg){width="75%"}
```{=latex}
\end{center}
```



[देवी-पूजा]{style="color:#99cc00;"}
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3159/3005400217_ca21454d6f_o.jpg){width="75%"}
```{=latex}
\end{center}
```



[लक्ष्मी-पूज]{style="color:#99cc00;"}

