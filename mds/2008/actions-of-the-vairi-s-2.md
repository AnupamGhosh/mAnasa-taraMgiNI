
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Actions of the vairi-s 2](https://manasataramgini.wordpress.com/2008/10/19/actions-of-the-vairi-s-2/){rel="bookmark"} {#actions-of-the-vairi-s-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 19, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/19/actions-of-the-vairi-s-2/ "4:36 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Yesterday, the ग्राहिन्, which had been kept at bay for a day came back to needle us. We called upon rudra to bear us aid in the second hour past mid-night and used the अप्सरा to relieve us from the ग्राहिन्. Today (19th) around 11.25 AM we were attacked by the चेटक again, who also tried to sneak in the ग्राहिन् on us. Having draw-up the draw-bridge we remained cloistered in our fort. We could hear the चेटक repeatedly banging on our doors. After about 45 minutes the चेटक retreated.

The cycle repeated itself today. The चेटक kept a vigorous attack on us and the उष्ण also failed to our disadvantage. We resolutely eluded the चेटक, but the ग्राहिन् grabbed us. By the afternoon of our day we were so overwhelmed that we had to use the indra-shakti, the six arrows on which we hung. The ग्राहिन् was not shaken by the first of these arrows, we had only 5 more. We felt worse than कर्ण -- at least he had killed घटोत्कच in the end. Was it the final मारण? Journey to the realms of vaivasvata is not so much a cause of fear as reaching there slowly.


