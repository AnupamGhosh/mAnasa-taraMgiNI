
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [atharva-वेदीय नक्षत्र kalpa सूक्तं](https://manasataramgini.wordpress.com/2008/07/20/nakshatra-kalpa-suktam/){rel="bookmark"} {#atharva-वदय-नकषतर-kalpa-सकत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 20, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/20/nakshatra-kalpa-suktam/ "8:09 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Below are the नक्षत्रेष्टि mantra-s of the atharvanic tradition. We use them as the याज्य verses with the ones from the shaunaka saMhitA काण्ड 19 forming the invocatory verses. They are not found in the extant पैप्पलाद text and appear to represent an independently preserved tradition only found in नक्षत्र kalpa. The oral recitations has minor differences from the published text. There is an extraordinary mention of the 108 shvetaketu-s associated with श्रविष्ठ. Also the sarpa-s are recognized as the sons of जरत्कार. Strangely the two dogs of yama -- श्याम and shabala are mentioned in connection with the apa-भरणि-s, when in reality they lie much farther away in the sky.

It is clear the the AV tradition composed a new नक्षत्र सूक्तं for the नक्षत्र rite, even as the तैत्तिरीयक-s composed their own नक्षत्र सूक्तं. This is in sharp contrast to the kaTha-s, who rather than composing a नक्षत्र सूक्तं merely compiled preexisting mantra-s into their नक्षत्र liturgy. See our earlier account of this [here](https://manasataramgini.wordpress.com/2007/12/07/nakshatra-homa-as-per-the-kathaka-yajurvedins/). In an interesting contrast, the kaTha tradition has composed its own late mantra-s for the graha homa, whereas the तैत्तिरीयक-s and atharva vedin-s have drawn a series of unrelated mantras from their saMhitA-s for their versions of the graha homa-s. In general we know that the graha homa-s are a very late accretion to the Vedic tradition. But what about the clearly earlier नक्षत्र homa-s. This raises the questions: 1) Was there an ancestral नक्षत्रेष्टि ? 2) Did the नक्षत्र rites convergently evolve in the different vedic traditions relatively late in their development? 3) What is the connection between the कृत्तिका period and the emergence of these rites.

We know that in the core agni-chayana there is the famous ritual of laying of the नक्षत्र bricks. It was in this context the original नक्षत्र liturgy emerged. It appears possible that these original compositions in combination with new compositions were recycled for the later emerging नक्षत्र homa-s. We suspect that the original compositions alone can be be considered belonging to the actual कृत्तिका period.

-*-

[*अग्निर् देवो यज्वनः कृष्णवर्त्मा वैश्वानरो जातवेदा रसाग्रभुक् ।\
स नक्षत्राणां प्रथमेन पावकः कृत्तिकाभिर् ज्वलनो नो ऽनुशाम्यताम् ॥ ०१*]{style="color:#99cc00;"}

[*प्रजापतिर् यः ससृजे प्रजा इमा देवान्त् स सृष्ट्वा विनियोज्य कर्मसु ।\
स सर्वभुक् सर्वयोगेषु रोहिणी शिवाः क्रियाः कृणुतां कर्मसिद्धये ॥ ०२*]{style="color:#99cc00;"}

[*विद्याविदो ये अभिशोचमानवा अर्चन्ति शक्रं सह देवतागणैः ।\
स नो योगे मृगशिरः शिवाः क्रियाः श्रेष्ठराजः कृणुतां कर्मसिद्धये ॥ ०३*]{style="color:#99cc00;"}

[*देवं भवं पशुपतिं हरं कृशं महादेवं शर्वम् उग्रं शिखण्डिनम् ।\
सहस्राक्षम् अशनिं यं गृणन्ति स नो रुद्रः परिपातु न आर्द्रया ॥ ०४*]{style="color:#99cc00;"}

[*या विप्रैः कविभिर् नमस्यते दाक्षायणी देवपुरादिभिर् नृभिर् ।\
सा नः स्तुता प्रथमजा पुनर्वसुः शिवाः क्रियाः कृणुतां कर्मसिद्धये ॥ ०५*]{style="color:#99cc00;"}

[*यस्य देवा ब्रह्मछर्येण कर्मणा महासुरं तिग्मतयाभिछक्रिरे ।\
तं सुबुधं देवगुरुं बृहस्पतिम् अर्चामि पुस्येण सहाभिपातु मा ॥ ०६*]{style="color:#99cc00;"}

[*या नः स्तुतः परिहिणोमि मेधया तप्यमानम् ऋषिभिः कामशोचिभिः ।\
जरत्कारसूनोर् ऋषिभिर् मनीषिभिस् ता अश्लेषा अभिरक्षन्तु नोरगैः ॥ ०७*]{style="color:#99cc00;"}

[*ye देवत्वं पुण्यकृतो .abhichakrire ye चापरे ye cha pare महर्षयः |\
अर्चामि सूनुर् यमराजगान् पितॄंश् छिवाः क्रियाः कृणुतां च नो मघा ॥ ०८*]{style="color:#99cc00;"}

[*यो योजयन् कर्मणा चर्षणीधृतो भूमिं चेति भगः प्रजाः प्रसादयन् ।\
तद्देवत्ये शिवतमाम् अलंकृते फल्गुन्योर् ईडे भजनं च पूर्वयोः ॥ ०९*]{style="color:#99cc00;"}

[*स्तुतं पूर्वैर् अर्यमणं मनीषिभिः स्तौमि देवं जगति वाचम् एरयन् ।\
तद्देवत्ये शिवतमाम् अलंकृते फल्गुन्यौ न उत्तरे देवतातये ॥ १०*]{style="color:#99cc00;"}

[*श्यावैर् युक्तः शितिपाद्-धिरण्ययो यस्य रथः पथिभिर् वर्तते सुखैः ।\
स नो हस्तेन सविता हिरण्यभुग्-घिरण्यपाणिः सविता नो ऽभिरक्षतु ॥ ११*]{style="color:#99cc00;"}

[*त्वष्ट्रे नमः क्षितिसृजे मनीषिणे भूत-गोप्त्रे परम-कर्मकारिणे ।\
सा नः स्तुता कृणुतां कर्मसिद्धये चित्रां देवी सह योगेन रूपभृत् ॥ १२*]{style="color:#99cc00;"}

[*यः प्राणिनां जीवयन् खानि सेवते शिवो भूत्वा मातरिश्वा रसाग्रभुक् ।\
ध्वजो ऽन्तरिक्षस्य स सर्वभूतभृद् वायुर् देवः स्वातिना नो ऽभिरक्षतु ॥ १३*]{style="color:#99cc00;"}

[*याव् ईडिताव् आत्मविद्भिर् मणीषिभिः सहितौ यौ त्रीणि सवनानि सामगौ ।\
इन्द्राग्नी वरदौ नमस्कृतौ विशाखयोः कुर्वताम् आयुषे श्रीः ॥ १४*]{style="color:#99cc00;"}

[*विश्वे देवा यम् ऋषिम् आहुर् मित्रं भरद्वाजम् ऋषितः प्रसामवित् ।\
तं जगत्या गाथया स्तौम्य् उग्रैः स माम् अनूराधाभिर् भृतकण्वो ऽभिरक्षतु ॥ १५*]{style="color:#99cc00;"}

[*शतक्रतुर् यो निजघान शम्बरं वृत्रं च हत्वा सरितः प्रसर्जतः ।\
स नः स्तुतः प्रीतमनाः पुरंदरो मरुत्-सखा ज्येष्ठया नो ऽभिरक्षतु ॥ १६*]{style="color:#99cc00;"}

[*या धारयत्य् ओजसातिदेवपदं माता पृथिवी च सा सर्वभूतभृत् ।\
सा नः स्तुता कृणुतां कर्म-सिद्धये मूलं देवी निरृतिः सर्व-कर्मसु ॥ १७*]{style="color:#99cc00;"}

[*पर्जन्य सृष्टास् तिसृणीभिर् आवृतं यास् तर्पयन्त्य् अभितः प्रवृद्धये ।\
ताः स्तौम्य् आपो वारुणीः पूर्वा आषाढा स्वधयास्तु योजने ॥ १८*]{style="color:#99cc00;"}

[*यास् त्रिंशतं त्रींश् च मदन्ति देवा देवनाम्नो निर्मितांश् च भूयसः ।\
ता नो ऽषाढा उत्तरा वसो विश्वे शिवाः क्रियाः कृणुतां सुरमताः ॥ १९*]{style="color:#99cc00;"}

[*यः सर्वज्ञः सर्वकृत् सर्वभूतभृद् यस्माद् अन्यन् न परं किं चनास्ति ।\
अनिर्मितः सत्यजितः पुरुष्टुतः स नो ब्रह्माभिजिता नो ऽभिरक्षतु ॥ २०*]{style="color:#99cc00;"}

[*स्थानाच्युते स्थानम् इन्द्राय पातवे देवेभ्यश् च य ईरयंस् त्रिर् विचक्रमे ।\
तं स्विद् धि स्वर्गं नाक पृष्ठं विश्वं विष्णुर् देवः श्रवणेनाभिरक्षतु ॥ २१*]{style="color:#99cc00;"}

[*अष्टौ शतानि श्वेतकेतूनां यानि त्वं च स त्वं निजघान भूयसः ।\
अनादेशेनोभयतश् च वीडिताः श्रविष्ठाभिर् नो ऽभिरक्षन्तु वाजिनः ॥ २२*]{style="color:#99cc00;"}

[*वाजा देवी देवमृणानि-काकुभाव् उभावाजस्य नतकर्मणा शिवा ।\
तव व्राजं स्तौमसि देवभोजनौ प्रत्यग्भिषक् शतभिषक् शिवौ नः ॥ २३*]{style="color:#99cc00;"}

[*शुनासीरौ नः प्रमुमूतु जिह्मसौ तौतौ पितृभ्यो ददतुः स्तनौ शुभौ ।\
तौ पूर्वजौ कृणुताम् एकपाद् अजः प्रतिष्ठानौ सर्व-कामाभयाय च ॥ २४*]{style="color:#99cc00;"}

[*सर्वार्थाय कृणोमि कर्मसिद्धये गविष्टुतायानेक-कारिणे नमः ।\
सो ऽहिर् बुध्न्यः कृणुताम् उत्तरौ शिवौ प्रतिष्ठानौ सर्वकामाभयाय च ॥ २५*]{style="color:#99cc00;"}

[*यं महाहेमम् ऋषितः प्रसामविद् भरद्वाजश् चन्द्रमसौ दिवाकरम् ।\
सजुष्टानाम् अश्वयुजौ भयाय च स नः पूषा कृणुतां रेवतीं शिवाम् ॥ २६*]{style="color:#99cc00;"}

[*जीर्णं सन्तं यौ युवानं हि चक्रतुर् ऋषिं धिया च्यवानं सोमपौ कृतौ ।\
तौ नश् चित्तिभिर् भिषजाम् अस्य सत्करौ प्रजाम् अश्विन्याम् अश्विनौ शिवौ ॥ २७*]{style="color:#99cc00;"}

[*यस्य श्याम-शबलौ रक्षतः स्वधा दुष्कृत् सुकृद् विविधा चर्षणीधृतौ ।\
तौ सवित्र्या च सवितुर् धर्मचारिभिर् यमो राजा भरणीभिर् नो ऽभिरक्षतु ॥२८*]{style="color:#99cc00;"}


