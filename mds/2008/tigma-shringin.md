
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [tigma-शृङ्गिन्](https://manasataramgini.wordpress.com/2008/07/21/tigma-shringin/){rel="bookmark"} {#tigma-शङगन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 21, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/21/tigma-shringin/ "8:15 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}


  - [यस् तिग्म-शृङ्गो वृषभो न भीम एकः कृष्टीश्-च्यावयति प्र विश्वाः ।\
यः शश्वतो अदाशुषो गयस्य प्रयन्तासि सुष्वितराय वेदः ॥ ऱ्‌V ७।१९।१]{style="color:#99cc00;"}*
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3146/2684503352_e486087c91_o.jpg){width="75%"}
```{=latex}
\end{center}
```



In our youth we were reading an ACK comic on कुम्भकर्ण, which had a memorable cover of the great रक्षस् wearing a horned head-gear slaying ब्राह्मणस् in the दण्डक vana. But at the same time in the background of my mind a ऋग्वेदिc mantra I had just learnt was playing:\
"[वि ज्योतिषा बृहता भात्य् अग्निर् आविर् विश्वानि कृणुते महित्वा ।]{style="color:#99cc00;"}\
[प्रादेवीर् मायाः सहते दुरेवाः शिशीते शृङ्गे रक्षसे विनिक्षे ॥]{style="color:#99cc00;"}" RV 5.2.9

The paradox prompted me to investigate and over time I realized that ऋग्वेदिc deities were frequently conceived as being horned. On at least 19 distinct occasions in RV मण्डल-s 1, 5,6,7,8,9 and 10, different gods have been obviously described as being horned. The deities who are described as horned are: agni (1 time identified with rudra) -- 6 times; indra -- 8 times; soma -- 4 times; बृहस्पति -- 1 time. Around the same time I was introduced by माताश्री to look into Sumerian, Assyrian and other Mesopotamian mythologies. There to my surprise I found again most deities being depicted as horned. Most striking was a Mesopotamian seal (see above, with Ea, Anu, Inanna, Enlil and Shamash all shown as being horned. Soon I became aware of a much larger horizon of horned deities who were roughly coeval: the unknown Harappan deity who was popular throughout the Indian chalcolithic, the Hittite Tarhunda or Teshub, Ba'al and Marduk. This feature also appears to have had a latter expression in the form of the occasionally depicted horns of Zeus. Further west we may also see a link to the horns of the Celtic deity Cernunnos (He may be depicted with longer antlers of cervids or antelopids rather than bovids on occasions, though the Gallo-Roman altar at Rheims is much closer to the Indo-Mesopotamian imagery of a horned deity). Another possible horned deity with a similar form is the Slavic rudra-like deity Veles. However, very little is known of his iconography if any to be sure. We also realized that such ceratocephalic deities are seen elsewhere in Eurasia like the famous horned deity in the Altaic shamanistic realm with the horns of a reindeer. However, we distinguish such reindeer-like apparitions from the quintessentially post-chalcolithic depictions where the horns are typically: 1) bovine and 2) placed as accouterments to a basically anthropocephalic form. This also sets them apart from the other ceratocephalous deities like Pan in the Greek world with goat horns (ortholog of पुषण्, who rides a goat) and the Egyptian Ammon Ra with horns of a ram. Further, in modern India we have a central Dravidian tribal community known as the Maria Gonds, who are famous for the dancing head-gear that closely resembles that of the Harappan deity or his Middle Eastern counterparts. In light of these far-flung motifs of ceratocephaly I was unsure if it was a case of convergent evolution or an ancient meme spreading through disparate cultures of Eurasia.

It was then that ST, in course of a discussion with me and ekanetra, brought to my attention to a work by Margaret Murray, which is very popular in western neo-Pagan circles. Murray and a bunch of others speculated that there was a single prototype behind all horned deities who was the god of the original pagans- the horned gods. The main problem with this theory is that the horned deity is not merely Indo-European or Afro-Asiatic but is distributed between these cultures in a patchy way. In the more strict sense in which I characterized the horned deities above, they find a relatively narrow distribution in the Middle East, Indian chalcolithic and its successors and to a lesser extant Europe and are not in any direct sense linkable to the older Paleolithic examples that Murray invokes. Further, these deities are obviously distinct across different cultures and as we see in Sumer and it successor-states multiple deities might have horns at the same time. It was in this context that I decided to revisit the ऋग्वेदिc imagery of horned deities.

continued...


