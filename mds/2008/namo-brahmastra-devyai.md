
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [namo ब्रह्मास्त्र-devyai](https://manasataramgini.wordpress.com/2008/11/26/namo-brahmastra-devyai/){rel="bookmark"} {#namo-बरहमसतर-devyai .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 26, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/26/namo-brahmastra-devyai/ "6:50 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3192/3058395462_18f296414c.jpg){width="75%"}
```{=latex}
\end{center}
```



