
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some points regarding the Rewa stone inscription of मलयसिंह](https://manasataramgini.wordpress.com/2008/08/09/some-points-regarding-the-rewa-stone-inscription-of-malayasimha/){rel="bookmark"} {#some-points-regarding-the-rewa-stone-inscription-of-मलयसह .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 9, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/09/some-points-regarding-the-rewa-stone-inscription-of-malayasimha/ "9:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The inscription was made for malaya-सिंह, a general of the Tripuri kalachuri king vijaya-सिंह-deva and several of his officials like a certain hari-सीम्ह. The Sanskrit looks pretty strange to me in some places but it has some interesting information. It was most likely made on 30th July 1193 CE, even as the rampage of the army of Islam was occurring further north and east -- around the very same time Bhaktiar ud-din Khalji was destroying Nalanda and slaughtering its community. But malaya-सिंह launches into a long celebration of his great deeds of archery and the achievements of his ministers. It seems to appear that malaya-सिंह and his panegyrist were blissfully unaware of the coming storm that was to end his all too familiar world. Earlier, vijaya-सिंह's father jaya-सिंह defeated Shihab-ud-din Ghori in around 1180 CE during one of his early incursion towards the Indian heartland. But squandered his power in battles with the chandellas and other kalachuris where he was badly beaten despite his fiery archery. It was during that invasion of the तुरुष्क that the सैद्धान्तिक vimala-shiva had supposedly performed अभिचारिक rites for his victory.

There is evidence from medieval manuals of mantra-s that मञ्जुगोष was worshipped by the Astika-s. The Rewa inscription provides one clear cut example in this regard from central India. It is clear the composer was an Astika because he declares himself to be a वैदीक in rather immodest terms:\
[तर्के ज्ञात मतीव यस्य चतुर शब्दार्थ शास्त्रे तथा मीमाम्साधिगतो विपश्चिद्-अभवद् वेदान्त-योगादि धी ।\
वेदाभ्यासरत सदा सुविदुषां मूर्ध्नि प्रबद्धाञ्जलिर् विप्र श्री पुरुषोत्तमो भुवि महान् बुद्ध्या च वाचस्पति ॥]{style="color:#99cc00;"}\
Here, he states that he is well-versed in logic, grammar and language, मीमाम्स, vedAnta, yoga and devotes himself to vedic study. It is interesting that he terms himself वाचस्पति -- could it be an allusion to मञ्जुगोष the deity with which he opens the inscription and feels some personal connection?

The invocation of मञ्जुघोष runs thus:\
[अSटार-चक्राकृति पूर्णचन्द्र पद्मासनस्थ हिम-शैल-गौरं ।\
सव्येतरा पाणिग [ओर् सव्येतरपणिग?] खड्ग पुस्त वक्ष्यामि नत्वा खलु मञ्जुघोषं ॥]{style="color:#99cc00;"}\
Here, मञ्जुघोष is described as being white as the himAlaya, seated in the lotus seat of the full moon of the form of a wheel with 8 spokes. He bears a sword in right and book in his left hand. While bauddha-s were extensively patronized by the kalachuri-s, मलयसिंह and his men seem to be predominantly Astika-s. In fact vedAnta is described as being upheld with great fervor in intellectual debates in his city. This suggests that by end of the 1100s an integration of the nAstika pantheon, which was once so virulently anti-Astika, into the larger unified Hindu fold was well underway.

Further, understanding of this assimilation of मञ्जुघोष (मञ्जुश्री) in the Astika mantra-शास्त्र might be obtained by comparing his iconography in the bauddha and Astika realms. It is well-known that the early development of मञ्जुश्री in the nAstika realm was highly influenced by the now faded कौमार शासन (one may look at our earlier note on the मञ्जुश्री-मूल-kalpa and its acquisition of कौमार mantra-s). But this is not the issue we are considering here. We are looking at his later development, well after he had evolved beyond the मञ्जुश्री मूल-kalpa. In the historical/archeological sense we observe such developments relatively late, especially in the kalachuri and adjoining चालुक्य realms. Beyond the Rewa inscription we find another striking illustration of the Hindu मञ्जुश्री in the form of an utsava मूर्ति in the Baroda museum -- possibly from the kalachuri realm of king कर्ण. Here the central figure of मञ्जुश्री is flanked on either side by विनायक and विष्णु. In the south in Karnataka we find temples of मञ्जुघोष termed मञ्जुनाथ (also in Nepal, e.g. Kathmandu मञ्जुनाथ), which are associated with the चालुक्य-s, coastal पाण्ड्य-s and southern kalachuri-s in their origin. We see that the मञ्जुघोष of Rewa inscription does not closely match the iconography of मञ्जुघोश as represented in his साधन-s in the popular bauddha Tantric compendium the साधन माला. Here we find that मञ्जुघोष is golden in color and holding a lotus and व्याख्यान mudra and may be depicted as riding a line. Again in the description of the dharma-धातु-वागीष्वर मण्डल where he is prominent, he is depicted as having attributes like the sword and book, but with 8 hands and 4 multi-colored heads. Instead, we find that the मञ्जुघोष of the Rewa inscription has elements closer to the dhyAna-s of other forms of मञ्जुश्री in the bauddha tantra-s, namely that of dharma-धातु-वागीश्वर, मञ्जुवज्र and arapachana. With all of them Rewa मञ्जुगोष shares the lotus-moon seat and usually tend to hold a book and sword, though they may have multiple heads or differ in color.

However, when we turn to the Astika tantra-s we find a close match to Rewa मञ्जुघोष in their dhyAna shloka-s of मञ्जुघोष. We find two major references to मञ्जुघोष in the medieval tantric manuals: the श्रीविद्यार्णव of विद्यारण्य yati and the tantra sAra of आगमवागीश from the वञ्ग country. Here the मञ्जुघोष mantra-s are variants of the famous nAstika mantra rAja: arapachana धीः || It is cited as being derived from older lost tantra-s known as the bhairava-tantra (possibly a general reference to the bhairava-tantras?), Agamottara and the kukkuteshvara tantra \[Though R1's father informs me that kukkuteshvara manuscripts were possibly extant in Nepal]. In these Astika tantra-s we find two dhyAna-s.\
The श्रीविद्यार्णव dhyAna 1:\
[संपूर्ण-माण्दल-तुषर-मरीचि-मध्ये बालं विचिन्त्य धवलं वर-खड्ग-हस्तं ।\
उद्दाम-केश-निवहं [कङ्कण-वहं] वर-पुस्तकाध्यं प्रौढं [नग्नं] जपेत् क्षतज-पद्म-दलायताक्षं ॥]{style="color:#99cc00;"}\
In this dhyAna we note he is seated on a full moon, holds the book and the sword and is white in complexion, thus largely matching the form in the inscription.

The second dhyAna is interestingly is related to the मञ्जुघोष mentioned in a single arapachana साधन composed by the nAstika Tantric ajita-mitra and also inserted into a मञ्जुवज्र stotra of another nAstika adept named मञ्जुगर्भ.\
Astika version:\
[शशधरम् इव शुभ्रं खड्ग-पुस्ताङ्क-पाणिं सुरुचिरम् अति-शान्तं पञ्च-चूडं कुमारं ।\
पृथु-तर-वर-मुख्यं पद्म-पत्रायताक्षं कुमति-दहन-दक्षं मञ्जुघोषं नमामि ॥]{style="color:#99cc00;"}

bauddha form:\
[शशधरं इवे शुभ्रं खड्ग-पुस्ताञ्क-पाणिम् सुरुचिरम् अति-शान्तं पञ्चचीरं कुमारम् ।\
पृथु-रति [तर]-वर-मोक्षं पद्म-पत्रायताक्षं कुमति-दहन-दक्षं मञ्जुघोषं प्रणम्य ॥]{style="color:#99cc00;"}

While parts of this dhyAna are unclear (e.g. what does पृथु-tara-vara-मुख्यं mean?) this form too largely resembles the one in the inscription, although he is not explicitly stated as sitting on the moon. Finally, Lokesh Chandra shows that in Tibet we have a depiction of such a form of मञ्जुश्री associated with the arapachana mantra. Thus, even though the traditional bauddha iconography of मञ्जुघोष surviving in texts like the मञ्जुघोष साधन-s of the साधन माला does not match the Astika version, it appears from the above bauddha dhyAna-s related to the Astika versions that other forms of मञ्जुश्री, namely arapachana and मञ्जुवज्र also tended to be identified as मञ्जुघोष. It appears that the Astika tradition conception of मञ्जुघोष evolved from such a system of identification. Hence, it adopted derivatives of the arapachana mantra as the mantra of मञ्जुघोष rather than using traditional bauddha mantra of मञ्जुघोष, i.e.:[ॐ वागीश्वर मूः ॥]{style="color:#0000ff;"} What the Rewa inscription suggests is that the iconography of मञ्जुघोष followed by the medieval mantra-शास्त्र manuals and its sources like the कुक्कुटेश्वर tantra was indeed the one prevalent amongst Astika-s.

Finally, it may be noted that the variants of the arapachana mantra in the श्रीविद्यार्णव are: [अरवछनधीं, अरवछन धीः]{style="color:#0000ff;"} and [अरवंछलधी]{style="color:#0000ff;"}\
The pa-\>va transform seen in these forms is suggestive of a possible drAviDa influence in the transformation. द्राविडि Prakrits tend to conflate internal pa with va (e.g. Skt pApa -> पाव). Also in the Grantha script these two letters are similar in shape leading to potential confusion.


