
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [raNa दीक्षा](https://manasataramgini.wordpress.com/2008/08/24/rana-diksha/){rel="bookmark"} {#rana-दकष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 24, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/24/rana-diksha/ "4:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3090/2790957793_6e7ccf3cf4_b.jpg){width="75%"}
```{=latex}
\end{center}
```



Both वैदीक and mantra-मार्ग shaiva traditions have similar rituals for consecration of the warrior and weapons. In the Vedic tradition the main mantra-s used for this purpose are those from the protective spell of the भरद्वाज-s from the ऋग्वेद (सूक्त RV 6.75). This सूक्त contains formulae to consecrate the main implements of proto-Greco-Indo-Iranian warfare, namely the bow, arrows (including poisoned points), the quiver, the horses, the reins, whips, chariots, chariot-carrying रथवाहन-s, armor, and gauntlets. Importantly, the ब्राह्मण sprinkled soma on the warrior with the formula:\
[मर्माणि ते वर्मणा छादयामि सोमस्त्वा राजामृतेनानु वस्ताम् ।\
उरोर्वरीयो वरुणस्ते कृणोतु जयन्तं त्वानु देवामदन्तु ॥]{style="color:#99cc00;"}(RV 6.75.19)\
In this mantra, after the warrior is armored by the ब्राह्मण, soma is called upon to confer अमृत (immortality) on the warrior. In addition to soma, aditi, brahmaNaspati and पूषन् were also invoked and made offerings [पूषन्: Note the link to the Greek Pan; Pan was invoked by the Greeks in the critical battle of Thermopylae against Iranians. The roar of Pan is supposed to have been the one which inspired the Greeks to victory. This military connection of पुषन् was already forgotten in the bhArata as suggested by arjuna's peculiar comment on military deities.].

A तान्त्रीक version of a similar rite is provided by the रणदीक्षाविधानं retro-attributed to भीष्म शान्तनव. It belongs to the larger tradition of the netra tantra as it invokes the primary deity of this tradition -- अमृतेश्वर with his famous तान्त्रीक mantra (मृतसंजीवनी विद्या): [ऒं जूं सः ॥]{style="color:#99cc00;"}. This invocation may be seen as showing continuity with the much earlier vedic tradition in which soma is invoked for conferring अमृत -- अमृतेश्वर depicted as bearing a soma/अमृत pot. In this shaiva rite the warrior/king, after a bath, in a clean condition first made donations of horses or other items. Then he invoked वेताल-bhairava and his family देवी (which might also indicate a private manifestation of प्रत्यङ्गिरा worshiped in royal households as in Nepal, Orissa, Gujarat, certain Tamil kingdoms). He applies a tilaka on the forehead marks his fingers with black ink is sprinkled with अक्षत-s mixed with rakta-chandana and is given the रणदीक्ष with the अमृतेश्वर mantra and its appropriate न्यास. He then consecrates the weapons depicted in the above figure with the respective mantra-s. He then performs naivedyam with various eats like 36 modaka-s, blood-rice, and meats to the weapon deities and betel-leaves. Then garlands of the red करवीर flowers were placed on the necks of the warriors and their foreheads anointed with vermillion tilaka-s. Then the warriors were asked to utter the \[five] "mahA" words to hail the king.

As the shrauta-performing मीमाम्सक-s and atharvavedic brahmA ritualists waned in the context of royal military rituals, the shaiva ritualists of various mantra-मार्ग and पाशुपत streams started taking up such performances. The raNa-दीक्ष ritual is an example of the parallel ritual they created along the model of the earlier vedic rituals but deploying a different set of mantra-s and देवता-s.

Ayudha mantra-s:\
OM खां खीं खूं खैं खौं खं खः रौद्रमूर्तये खड्गाय नमः ||\
OM ह्रां ह्रीं ह्रूं ह्रैं ह्रौं ह्रः श्रीभैरवरूपाय रक्षायुधाय फलकाय नमः ||\
OM lAM lIM lUM लैं लौं लः इन्द्रायुधाय धनुषे नमः ||\
OM स्रां स्रीं स्रूं स्रैं स्रौं स्रः ब्रह्माद्य-पञ्च-देवतामूर्तये पञ्चबाणस्वरूपिणे bANa-फलकाय नमः\
OM क्रां क्रीं क्रूं क्रैं क्रौं क्रः शूलिमूर्तये भैरवायुधाय कुन्ताय नमः ||\
OM aghore Churike चामुण्डे छ्रां छ्रीं छ्रूं छ्रईं छ्रौं छ्रः भगवतीमूर्तये कात्यायन्यै छुरिकायै नमः ||


