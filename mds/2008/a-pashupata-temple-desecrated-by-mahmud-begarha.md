
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A पाशुपत temple desecrated by Mahmud Begarha](https://manasataramgini.wordpress.com/2008/08/06/a-pashupata-temple-desecrated-by-mahmud-begarha/){rel="bookmark"} {#a-पशपत-temple-desecrated-by-mahmud-begarha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 6, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/06/a-pashupata-temple-desecrated-by-mahmud-begarha/ "1:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3007/2736507265_fb582211db_b.jpg){width="75%"}
```{=latex}
\end{center}
```



The southern face of of the lakulisha mahadeva temple in Champaner (note the eye sores in the form of modern Indian squalor flanking the temple).
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3070/2737342098_3e01f9911f_b.jpg){width="75%"}
```{=latex}
\end{center}
```



Detail of the southern wall: Note the image of aghora-mukha दक्षिणामूर्ती surrounded by the entire pantheon of deities.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3228/2736507183_3cba123eab_b.jpg){width="75%"}
```{=latex}
\end{center}
```



Fragments of the demolished temple lying about.

We had earlier pointed how the destruction of the Hindu kingdoms and temples by the Moslems resulted in the demise of the पाशुपत-s and their texts in both north and south India. Here we shall provide a graphical case study of the same. The Muzaffarid Turks of Gujarat were notorious for their bigotry and vandalism of Hindu structures. We had earlier narrated how they had destroyed the famous rudra-महालय temple. The लकुलीश महादेव temple was a great center of the atharva-veda- affiliated पाषुपत-s of Gujarat and was shielded from the army of Islam by the southern छाहमानस् who reigned in this region from 1297 CE. In 1483 CE the Muzaffarid Turk Mahmud Begarha was strengthened by his alliance with the Ottomans which provided him with new panoply of guns. The Sultan launched a Jihad on the Hindu kingdoms of Girnar and Champaner. जयसिंह छाहमान was besieged in the fort of Pavagad with a relatively short supply of guns and powder. Yet he defended Champner against the Moslems with great courage by economically using his guns against the repeated Turkic assaults. Finally after an year of battle he ran out of supplies and was left with only 700 defenders against the Turks. The Sultan graciously offered जयसिंह the choice between his foreskin and his head. But the rAjpUt-s refused to give up their dharma and decided to put up the final fight. The Sultan entered into negotiations with the राजन्, and, even as they were on, he murdered the rAjput ruler and his people to attain the Ghazi title. With that the army of Islam savaged Champaner and demolished temples of लकुलीश महादेव and bhadra-कालि on the nearby mountain. Mahmud was a monstrous individual -- like other members of his dynasty he had an appetite for seizing Hindu women to add to his harem. But most notable was his enormous gluttony -- he is interestingly reported to have downed over 100 mutton Samosas with large pot of ghee on a daily basis.


