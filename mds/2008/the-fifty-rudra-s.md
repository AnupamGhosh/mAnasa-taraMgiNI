
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The fifty rudra-s](https://manasataramgini.wordpress.com/2008/03/04/the-fifty-rudra-s/){rel="bookmark"} {#the-fifty-rudra-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/04/the-fifty-rudra-s/ "1:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

कलशजा had asked me the list of the rudra-s and their shakti-s associated with the स्मार्त शब्दराशी मण्डल. Each rudra is worshiped with their particular mantra during the installation of the शब्दमण्डल of equilateral triangles and then the yantra is turned around and the shakti-s are invoked. Then it is placed back in the rudra configuration and worshiped as the whole शब्दराशी. The common स्मार्त system of nomenclature goes as appended below, and is seen in two nearly congruent to the lists provided by the प्रपञ्चसार and shArada-tilaka (2.29-35) tantra-s. The rudra-s are meditated as having two hands respectively with a trident and skull. Their shakti-s are mediated as having two hands bearing a red lotus and skull in each. They are both conceived as being the color of scarlet lead (Pब्३ऒ४).

The 50/51 rudra-s (and their shakti-s) of the trika system are mentioned in the मालिनी-vijaya 3.13 beginning with श्रीकण्ठ as a part of the मालिनी-gahvara in the na-\>pha configuration. The 50/51 rudra-s/shakti-s of the पश्चिमाम्नाय (कुब्जिका) tantras are similar and are mentioned in the कुब्जिकामत tantra (24.1-57; कुलालिकाम्नाय version). Similar lists of the कुब्जिका tradition are also mentioned in the agni पुराण 145 and 293. These are in relationship to the tadgraha structure of the पश्चिमाम्नाय, which is homologous to the मालिनी-gahvara. The rudra-s in the lists of trika and पश्चिमाम्नाय, while closely resembling the स्मार्त list, are arranged in prayoga to suit the मालिनी order. The associated shakti-s however differ considerably vis-à-vis the स्मार्त equivalent (the पश्चिमानाय list is appended after the स्मार्त list. As श्रीकण्ठ is the first rudra of these lists the न्यास-s in these traditions which place the rudra-s on various organs are called श्रीकण्ठादि न्यास. The सैद्धान्तिक system of 50 rudra-s is provided by the मकुटागम, चर्यापाद 6.14-20. This सैद्धान्तिक list while having most of the names seen in the above systems is different in its order to correspond with the gahvara of this srotas. Kasmirian tradition records another list that appears to be associated with अमृतेश्वर or tryambaka rudra of the netra-tantra---it has names that are very different and have been appended after the पश्चिमांनाय list.

1 श्रीकण्ठ = पूर्णोदरी\
2 ananta = विरजा\
3 सूक्ष्म = शाल्मली\
4 त्रिमूर्ति = लोलाक्षी\
5 amareshvara = वार्तुलाक्षी\
6 अर्घीश = दीर्गघोण\
7 भावभूति (भारभूति) = सुदीर्घमुखी\
8 tithi = गोमुखी\
9 स्थानु = दीर्घजिह्विका\
10 hara = कुण्डोदरी\
11 झिण्टीश = Urdhvakeshi\
12 bhautika = विकृतमुखी\
13 सद्योजात = ज्वालामुखी\
14 anugraheshvara = उल्कामुखी\
15 अक्रूर = श्रीमुखी\
16 महासेन = विद्यामुखी\
17 क्रोधीश = महाकाली\
18 चण्डेश = सरवती\
19 पञ्चान्तक = sarvasiddhi\
20 shivottama = गौरी\
21 ekarudra = त्रैलोक्यविद्या\
22 कूर्म = मन्त्रात्मशक्ती\
23 ekanetra = भूतमाता\
24 chaturmukha = लम्बोदरी\
25 ajesha = द्राविणी\
26 sharva = nAgarI\
27 someshvara = वैखरी (खेचरी)\
28 लाङ्गली = मञ्जरी\
29 दारुक = रूपिणी\
30 अर्धनारीश्वर = वीरिणी\
31 उमाकान्त = कोटरी (काकोदरी)\
32 आशाढी = पूतना\
33 दन्डिन् = भद्रकाली\
34 adri = योगिनी\
35 mIna = शङ्खिनी\
36 मेष = गर्जिनी\
37 lohita = कालरात्री\
38 shikhin = कुब्जिनी (कुर्दनी)\
39 छगलण्द = कपर्दिनी\
40 दिव्रण्ड = महावज्रा (वज्रिनी)\
41 महाकाल = जया\
42 कपाली (valI) = सुमुखेश्वरी\
43 भुजङ्गेश = रेवती\
44 पिनाकी = माधवी\
45 खड्गीश = वारुणी\
46 baka = वायवी\
47 shveta = रक्षोपधारिनी (रक्षोविदारिनी)\
48 भृगु = सहजा\
49 नकुली (लकुली) = लक्ष्मी\
50 shiva = व्यापिनी\
51 samavartaka = mAyA

पश्चिमाम्नाय list\
1 श्रीकण्ठ = वागेश्वरी\
2 ananta = आमोटी\
3 सूक्ष्म = mAyA\
4 त्रिमूर्ति = guhyashakti\
5 amareshvara = mohani\
6 अर्घीश = प्रज्ञा\
7 भारभूति = शान्ति\
8 tithishvara = शान्ति\
9 स्थानु = शान्ति\
10 hara = शान्ति\
11 झिण्टीश = ज्ञानी\
12 bhauktin = क्रिया\
13 sadyadeva = gAyatrI\
14 anugraheshvara = sAvitrI\
15 अक्रूर = शुक्रा\
16 महासेन = इच्छा\
17 krodha = कङ्कटा\
18 चण्ड = काली\
19 प्रचण्ड = शिवा\
20 shivesha = घोरघोशा\
21 ekarudra = खिर्वीरा\
22 कूर्म = चामुण्डा\
23 एकाक्ष = पूतना\
24 chaturmukha = जयन्त्या\
25 ajesha = झङ्कारी\
26 sharman = कुर्दनी\
27 someshvara = कपालिनी\
28 लाङ्गली = पूर्णिमा\
29 दारुक = विनायकी\
30 अर्धनारीश्वर = lAmA\
31 उमाकान्त = नारायणी\
32 आशाढी = tArA\
33 दन्डिन् = ग्रसनी\
34 धातर् = दहनी\
35 mIna = प्रियदर्शना\
36 मेष = नादिनी\
37 lohita = पावनी\
38 shikhin = फेट्कारिका\
39 छगलण्द = वज्रिनी\
40 दिव्रण्ड = भीषण्या\
41 महाकाल = महाकाली\
42 valI = वायुवेगा\
43 भुजङ्गेश = दीपनी\
44 पिनाकी = छगलण्दा\
45 खड्गीश = शिखिवाहिन्या\
46 baka = कुसुमा\
47 shveta = लम्बोदरा\
48 भृगु = परापरा\
49 लाकुली = अम्बिका\
50 samavartaka = सम्हारी

1 अमृत\
2 अमृतपूर्ण\
3 अमृताभ\
4 अमृतद्रव\
5 अमृतौघ\
6 अमृतोर्मि\
7 अमृतस्यन्दन\
8 अमृताङ्ग\
9 अमृतवपु\
10 अमृतओद्गार\
11 अमृतास्य\
12 अमृततनु\
13 अमृतसेचन\
14 अमृतमूर्ति\
15 अमृतेश्वर\
16 सर्वामृतधर\
17 jaya\
18 vijaya\
19 jayanta\
20 अपराजित\
21 sujaya\
22 jayarudra\
23 जयकीर्ति\
24 जयावह\
25 जय्मूर्ति\
26 जयोत्साह\
27 jayada\
28 jayavardhana\
29 bala\
30 atibala\
31 balabhadra\
32 balaprada\
33 बलावह\
34 बलवान्\
35 बलदाता\
36 baleshvara\
37 nandana\
38 sarvatobhadra\
39 भद्रमूर्ति\
40 shivaprada\
41 sumanas\
42 स्पृहण\
43 durga\
44 भद्रकाल\
45 manonuga\
46 kaushika\
47 काल\
48 vishvesha\
49 sushiva\
50 kopa\
The रुद्रानी-s are merely feminized forms of the rudras.


