
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Why this thought?](https://manasataramgini.wordpress.com/2008/03/09/why-this-thought/){rel="bookmark"} {#why-this-thought .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 9, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/09/why-this-thought/ "6:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For some reason suddenly a train of thought arose in our mind which as stirred by the crashing waves of the रसार्णव. It was about something from the ancient past -- from around the time vidruma and lambaka had fallen. In our mind's eye we saw that eddy which had sucked in satya-druha. We looked into it and it was completely dry now. In fact a hot stream of air emanated from it. We thought of the great German poet JW von Göthe. Then for some reason we vaguely remembered the lines of his poem An Belinden. The lines, while not perfectly coming to mind, kept sticking there, even as we heard the howling of the wind and the mirthful laughter of revelers around us. So I looked it up. Later I found an approximate verse translation of the same (appended). For some reason we again felt that poetic connection with Herr von Göthe.

Warum ziehst du mich unwiderstehlich,\
Ach, in jene Pracht ?\
War ich guter Junge nicht so selig\
In der öden Nacht ?

Wherefore drag me to yon glittering eddy,\
With resistless might?\
Was I, then, not truly blest already\
In the silent night?

Heimlich in mein Zimmerchen verschlossen,\
Lag im Mondenschein,\
Ganz von seinem Schauerlicht umflossen,\
Und ich dämmert ein;

In my secret chamber refuge taking,\
'Neath the moon's soft ray,\
And her awful light around me breaking,\
Musing there I lay.


