
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कृत्या is seen again](https://manasataramgini.wordpress.com/2008/08/28/the-kritya-is-seen-again/){rel="bookmark"} {#the-कतय-is-seen-again .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 28, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/28/the-kritya-is-seen-again/ "6:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were in their midst -- full of excitement and mirth. But even now we were not alone -- we were shadowed by that कृत्या, like कपालिन् shadowed by the maid ब्रह्महत्या. In the corner hero stood -- it was his day of victory; we moved up to him and gave us his benedictions. Then we moved on surrounded by clansmen and friends, and absorbed everything into our consciousness. Most took us to be a fool and yet others even took us to be a corpse. One of them was the तैत्तिरीयक. But he was a ghost of his past self; he still perceived himself as great, but the xvarena departed from him even as it forsakes an Iranian king in his decline. Left literally in the dark he cast about and suddenly took notice of me and tried to prove his might, which had once shaken our sachiva. But we saw through all of that -- he was sunk in mAyA as though bound by the pashu-pAsha-s. But two men saw through our natural disguise and saw our true self lodged within. They informed some others and they all started to notice us. Realizing what we cast a cloak and continued our anthropological investigations by casting the web of indra. They all moved like puppets on strings except one who was above all those bound by the nooses of mAyA. On seeing that one we realized our own fragility as she could easily pierce our coat, so we cast an even thicker screen and continued roving un seen by all. Moving in the passage of the प्रासाद, like the one of the रक्ष of श्री-लन्का, we took all of it in. We realized then why our अमात्य had been so terrified by the owner of the प्रासाद.

After roving thus we returned to the lair of the mighty यक्षिणी नीललोहित-मेखला. We were lost in saundarya of her captivating ivory thighs and enjoying her embraces, which was to the senses like the sweetened soma engulfing the tongue. In that state we asked her to prognosticate. She said: "In the ninth hour the कृत्या will seize you like the gandharva seizing yavakri as it did on the 26th of April 2008. Deploy the bear's fruit."

Those आततायिन्-s who had long wished us nothing but harm tried many attacks on us including the hacking of the earlier version of these pages. In the continuing battle they launched several कृत्या-s on us. The one of 26th April 2008 was in several ways like the one of the March of 2007. Against that former one we first deployed the mantra of वरुण, which over came it after a nearly 25 days of heavy fighting. But after a month or so that कृत्या returned. This time we used our दूरदृष्टि prayoga with the यक्षिणी to see the उपाय against this prayoga. The deva who gave us the answer was नारायण who sleeps on the coils of the काल-sarpa in the midst of the great ocean. We attacked the enemy with the foot of विष्णु. O विष्णु you had sent the demons to पाताल overcame our attacker. Shortly after that in August of 2007 they sent a terrible agent of बगलामुखी, a श्रोणिका at us. But again that foot of विष्णु protected us. Then after a week of अक्षिस्पन्द we were attacked by that frightful ग्राहिन् sent by आततायिन्-s on April 26th 2008. We first deployed the astra of बगलामुखी on that. First the astra seemed to press against the ग्राहिन्, but to our enormous surprise the ari-s over came it (was it a दक्षिणकाली prayoga?). Then from our माताश्री we obtained a second ब्रह्मास्त्र of even greater strength and deployed it on the ग्राहिन्. Even that did not completely neutralize the attack. Finally, we invoked skanda to bear us aid and sent 48 shara-s one after the other on the ग्राहिन्. As we went to the beautiful seashore we were relieved of the ग्राहिन् by the deva. Then in the beginning of Aug 2008 they sent the same श्रोणिका at us against. But again the aids of विष्णु and skanda repulsed this attack. Suddenly around midday of 22nd we were attacked again by that deadly ग्राहिन् which had attacked us a few months ago. "The bear's fruit".\
The यक्षिणी said: "Beware of the scholar whose shadow seizes one like an unmatta-graha when you are staying up late"

28th Aug, 9.00 AM: The मारण attack has been deployed. This definitely exceeded the past मारण-s its intensity except perhaps the swift blow that fell upon us during the journey to the northern realm. It seemed to have been timed well because it was accompanied by the second attack directed via the sachiva and अमात्य. We were so overwhelmed by the powerful शूल, like one hurled by कुम्भकर्ण, that we sought to use the last ब्रह्मास्त्र we had. But we waited and ground out the suffering from the attack. Then to control the situation we redeployed the shara-s as earlier and the यक्षिणी's substance. We then we offered the sacrifice to चतुष्पथ-निकेता and tried to prepare ourselves for even vaivasvata.


