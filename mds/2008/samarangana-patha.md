
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [समराङ्गण patha](https://manasataramgini.wordpress.com/2008/04/05/samarangana-patha/){rel="bookmark"} {#समरङगण-patha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 5, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/05/samarangana-patha/ "10:34 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our ari-s kept up the incessant attacks on us. Now the ari-s sent their प्रणिधि-s to watch on us and intercepted some crucial intelligence as we were checking some fortifications -- same ones we tested during the start of this drawn saga, whose history we would narrate if we survive the last battle. But as we were distracted by the deployment of प्रणिधि-s, the दुष्ट-s stole up and launched the कुलुञ्च operation against us. Our prayoga-s were disrupted. But we sought the aid of the great guha, who revealed the movements of दुष्ट-s by the end of the day and we could launch patra-त्रात to minimize our losses. Ever since the defensive patra-त्रात operation was staged the खाण्डवन्स् had been confounded and casting around. We knew they might do other things and were on alert as yoddha-s are supposed to be. They tried to make our vasas into a naraka by means of the ati-daghda strike. But, like रामचन्द्र braving the forest, we concentrating on shiva remained unfazed. They tried to surprise us by sending an arm of the सेना against ंPअ. In the past the same सेना had taken advantage of our ंPअ's naiveté and managed to break through their defenses completely. As a result we also suffered a long invasion from the twin सेना-s of the ari-dala. We held a long kuriltai with our जनाः and warned them to be prepared as they would also be targeted. We told them: "call रक्षोहा agni to your aid, he who utterly burnt the dasyu-s. Be ready for both prayoga and yuddha." We then realized that a stambhana prayoga was being used on us to incapacitate us. "The wall of the crane" we thought and sought the sought the aid of उत्तराम्नायेश्वरी. As we were struggling through that an extraordinary डामारिका attack, far exceeding the previous ones, was launched on us. For some time we simply were at loss as to what to do ... We then turned to the wielder of the spear again who showed us the path out of it.

Subsequently, Pंअ were subject to further attacks by the खाण्डवन्स्, who tried to resort to all kinds of कूट-yuddha vanishing like मेघनाद in the clouds and reappearing and showering arrows on us. Moving their सेना to attack \*both* the kosha-s. We secured the fort on south and created the path to it. We had to now defend that path against the direct खाण्डवन् assault as well as sabotage on the supply line.\
O skanda may the dear friends attain their gati: विपरीतार्थ.


