
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The पञ्चावर्ण stava](https://manasataramgini.wordpress.com/2008/02/26/the-panchavarna-stava/){rel="bookmark"} {#the-पञचवरण-stava .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 26, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/26/the-panchavarna-stava/ "3:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great siddhAnta tantric aghorashiva deshika from the mid-1100s of the CE from the Tamil country composed an epitome of siddhAnta worship of the 5- headed सदाशिव and the आवरणस् of his मण्डल. This is the पञ्चावर्ण stava, which resembles other siddhAnta compositions like the shiva-पूजा stava of ज्ञानशंभु deshika of वाराणसि (originally also from TN) and the paddhati termed the dhyAna-रत्नावली by trilochanashiva, the student of aghorashiva. The स्मार्त appaya दीक्षित recommends it for nitya पूजा. A superior edition of the पञ्चावर्ण stava has been produced by the efforts of Institut Francais de Pondichery that supersedes versions printed previously in the Tamil script with the usual atrocities of tamil pronunciation. This text is in multiple classical meters -- the most common of which are अनुष्टुभ्, vasanta-tilaka, उपजाति and शालिनि.\
A summary of its contents:\
1-2 शिवसूर्य: shiva is invoked within the solar orb. The सूर्यनार् kovil in Thanjavur represents शिवसूर्य.\
3-5 8 graha-s\
5 (end) तेजश्चण्ड. तेजश्चण्ड is the equivalent of the agent of rudra named चण्डेश्वर in the context of saura-shaiva worship. This section represents the सूर्योपासन element of the shaiva-s.\
6-8 विनायक, सरस्वती and गजलक्ष्मी are worshiped on the sides and the top of the entrance to the मण्डल.\
9-10 nandin and गङ्गा on the right door\
11-12 महाकाल and यमुना on the left door\
13 the astra indicated in the mantra: OM haH अस्त्राय फट्\
14-16 brahmA as वास्तुपाल (South West), विनायक in the NW, महालक्ष्मी in the N and the 7 divyaugha guru-s of the सैद्धान्तिक stream in the NE: सदाशिव, ananta, श्रीकण्ठ, अंबिका, skanda, विष्णु and brahmA.\
17 Adarashakti in कूर्म stone\
18 ananta in brahma stone\
19-20 dharma, ज्ञान, वैराग्य and aishvarya as the feet of the सिंहासन\
21 mAyA and विद्या as the cushions in the सिंहासन\
22 the 8-petaled lotus of the मण्डल -- शुद्धविद्या\
23-24 the 9 shaktis from वामा to manonmani standing on the kesaras and the ovary of the मण्डल.\
25 the sUrya-मण्डल containing brahma, the Chandra-मण्डल containing विष्णू, and the agni-मण्डल containing rudra.\
26 shakti मण्डल containing Ishvara\
27 The योगपीठ\
28-42 सदाशिव on the throne.\
43-46 shakti\
47-54 the पञ्च-brahma: ईशान, tatpuruSha, aghora, वामदेव, सद्योजात.\
55-59 6 अङ्ग mantras\
60-66 the vidyeshvara-s ananta, सूक्ष्म, shivottama, ekanetra, ekarudra, त्रिमूर्ति, श्रीकण्ठ, शिकण्डिन्\
67-74 the भूत-गणेश्वर-s and वाहन: nandin, महाकाल, भृन्गिन्, विनायक, वृष, कुमार, उमा and चण्डेश्वर\
75-85 the devas: indra, agni, yama, निरृति, वरुण, वायु, kubera, ईशान, brahmA, विष्णु\
86-96 daivi astras: vajra, shakti, daNDa, खड्ग, pAsha, dhvaja, गदा, त्रिशूल, padma, chakra\
96-97 शिवाग्नि: the five-headed manifesation of the shaiva ritual fire born from वागीशी and सदेशान\
98 bhairava (क्षेत्रपाल) along with rudras, गणस्, मातृका-s, grahas, daitya-s and other beings.\
99 सैद्धान्तिक sva-guru\
100 shambhu\
101 चण्डेश्वर\
102 phalashruti


