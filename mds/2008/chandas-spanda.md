
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [chandas-spanda](https://manasataramgini.wordpress.com/2008/04/03/chandas-spanda/){rel="bookmark"} {#chandas-spanda .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 3, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/03/chandas-spanda/ "1:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2102/2383552659_b93b7dec09_o.png){width="75%"}
```{=latex}
\end{center}
```




 1.  gAyatrI 2) उष्निख़् 3) अनुष्टुभ् 4) बृहति 5) pankti 6) त्रिष्ठुभ् 7) जगती 8) अतिजगती 9) शक्वरी 10) अतिशक्वरी 11) अत्यष्टि 12) अतिधृति

