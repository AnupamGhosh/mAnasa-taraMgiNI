
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [One waves of the samaramahodadhi](https://manasataramgini.wordpress.com/2008/03/09/one-waves-of-the-samaramahodadhi/){rel="bookmark"} {#one-waves-of-the-samaramahodadhi .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 9, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/09/one-waves-of-the-samaramahodadhi/ "3:11 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

On the darsha day we sat to perform our kratu. But we noticed that there was an error. We quickly regrouped and performed a प्रायश्चित्त. But some fear lingered still if something might slip past. It was sent our way just after the meridian hour. By the aid of the deva-s we back-hurled the initial strike by brahma hour. We noticed that the ari was immediately facing the effects of the rebound. The ari wised up and rapidly to recourse to their prayogin-s. We faced a sudden strike and were reeling under it for some time. But because of the earlier mantra-s we were stabilized by the deva-s and managed to get pass its effects. We watched like that Atman watching the sharIra. As in Conan Doyle's "Final Problem" they stood together in the narrow corridor. We had seen the मारण strikes before but this was different --- It utterly stupefied us. Suddenly the opening mantra-patra-s with the सामिधेनि-s were missing from the kosha. We had put them in there just after the प्रायश्चित्त. Everything else was intact. We realized that we were under the gaze of a चेटकी. Then the धुमावती strike entered our मणिपूर chakra. As we said this was not aimed at मारण but it inactivated us -- we simply could not deploy anything on the ari. We were reduced to utter powerlessness -- neither mantra-s nor क्रिया-s seemed possible; our sight was blinded from seeing our ari. We remembered where it all began: Like कुम्भकर्ण moving against the वानरस् in his last battle it advanced! We were tunneled back in time against our control. We stood in the realm of the first sorrow. We were at the place on which we turned our back effortlessly when the prayoga of कामेश्वरी saw us through. It was a partly gloomy day just as today. The muni was then vulnerable and we needed to protect him. We had taken out that rahasya grantha of रसायन with a green cover. At that point out of nowhere "Pandey" appeared. We were distracted for a few moments and the रसायन grantha was gone just as the mantra-patra-s. We stood there as though paralyzed inside a श्मशान with a वेताल relentlessly holding on to us. Like vipula we entered Jx to escape for a while. A full circle we thought as we emerged from him. By the mantra of कुमार we were stabilized.

*-*-*

We are not easily swayed by the unreal despite our use of colorful language. But the धूमावती attack launched on the मणिपूर chakra is something which will make even a skeptic of true abhichara sit up and observe. In Hindu martial tradition striking a blow at the level of the मणिपूर chakra is supposed to be highly effective in knocking the wind out of the opponent. When targeted via अभिचार its effects are distinctly different from those of attacks targeting other chakra-s. An अभिचार strike on the आज्ञ chakra, while pretty destructive in the immediate aftermath, can be more easily controlled by counter-prayoga (or so we have felt in the long samara who history has only be partially narrated). But that targeting the मणिपूर (alone or in combination with another chakra) can also "knock wind out" and really suppress the ability to perform counter-prayoga, allowing the ari to take control. It is only if one has performed a long previous पुरश्चरण one could survive the effects of such an attack without assistance. One might discount the existence of things like the मणिपूर chakra -- even if one is not a yogi, but faces such an attack one will be convinced of its existence. In the case of Jx it was different but he first learned of chakra-s due to such a prayoga. Jx had been struck in the आज्ञ by Fourier and never really faced the strike of the kind we did. But now Fourier was gone and his अभिचार-s completely broken so the third vIra and Jx were free.

After some examination we discovered that the valaga-s from which strikes were launched were in green and blue bags -- so strong was the effect that even after they were destroyed the चेटकी-s were not curtailed. संध्य: the old name has changed after the दूरदृष्टि attack.


