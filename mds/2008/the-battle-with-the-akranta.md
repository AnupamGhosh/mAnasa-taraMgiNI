
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The battle with the आक्रान्त](https://manasataramgini.wordpress.com/2008/06/29/the-battle-with-the-akranta/){rel="bookmark"} {#the-battle-with-the-आकरनत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 29, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/29/the-battle-with-the-akranta/ "6:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The ari-s had always wished evil to befall us and tried a dreadful prayoga that unleashed the ग्राहिन् on us. We were tormented by the ग्राहिन् even as we headed to the rudhira-sIma. But we invoked skanda to come to our aid. One the first day the ग्राहिन् of our ari-s seemed to keep us down. But on the second the darts of कुमार expressed their miraculous effects and the ग्राहिन् could not even near us. Seeing this the ari-s decided to try the other subversive attack that they had been plotting for long against us. For two days our दूरदृष्टि had been hampered, but again कार्त्तिकेय enabled us to regain this. At this point we were able to uncover the ari-s evil moves. The slow movements of the अमात्य and the sachiva were a matter that was bogging us to a degree. We also obtained intelligence that the ari-s were working heard to revive the ग्राहिन् around 1.30 AM.

As expected we were attacked by the revived ग्राहिन् of the pashu-s. With the aid of Jx we reached the temple of कार्त्तिकेय and duly propitiated the fierce god. In the coming few days we expect the full-fledged clash with the आततायिन् developing even as the अमात्य and sachiva are retreat to kantapura amongst the आन्गलीक-s.


