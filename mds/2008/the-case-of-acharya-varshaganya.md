
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The case of आचार्य वार्षगण्य](https://manasataramgini.wordpress.com/2008/01/21/the-case-of-acharya-varshaganya/){rel="bookmark"} {#the-case-of-आचरय-वरषगणय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 21, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/01/21/the-case-of-acharya-varshaganya/ "7:56 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is extremely common for ancient Indian आचार्य-s to remain mere names, with little if anything else known about them. As a result their dates and biographies are subject to wild speculations and debates. Such debates, often spearheaded by White indologists and their oriental imitators, show a general tendency to mistrust Hindu sources, obfuscate matters and apply an ad hoc system of decisions to decide whether someone is early or late. It would seem that their general their rule of the thumb is to provide as late dates as possible for most Hindu sources, and try to make it appear that most philosophical development happened after the Buddhists or the contact with the Greeks.

One Acharya of considerable interest in the development of Hindu thought is वार्षगण्य, the great proponent of the सांख्य-yoga school. He could perhaps be credited with being the first to formulate a version of the law of conservation of mass (energy) and thus becomes an important individual in the history of Hindu science. He is cited as an ancient authority in the व्यास-भाष्य on the sUtra-s of पतञ्जलि. वाचस्पति मिष्र suggests that he as the author an ancient सांख्य text termed the षष्ठी-tantra from which the व्यास-भाश्य draws its citations. In the least it is certain that in discussing sUtra-s of पतञ्जलि the commentator व्यास had access to a text by वार्षगण्य. He is also mentioned in a commentary named युक्तिदीपिका on Ishvara-कृष्ण's कारिक-s as a prior authority of सांख्य.

Multiple Chinese sources also describe वार्षगण्य as an authority of the सांख्य school. One of these is the Indian Acharya परमार्थ who settled in Nanjing, China (around 550 CE) and composed a biographical work on the philosopher vasubandhu of the kaushika gotra from school of पुष्पपुर (modern Peshawar in the Islamic Terrorist state). In this work he states that the great sage of the सांख्य school was वार्षगण्य who was king of the nAga-s living at the base of the Vindhya mountains. His student विन्ध्यवास reworked and revised the सांख्य doctrine and composed a collection of shloka-s containing this revised version of सांख्य. He participated in a philosophical debate with the bauddha आचार्य buddhamitra in Ayodhya, presided over by the king विक्रमादित्य. विन्ध्यवास routed buddhamitra in this debate and as result won a great reward from king विक्रमादित्य and thus brought the ascendancy of सांख्य. It was in order to counter विन्ध्यवास that the bauddha-s rallied under vasubandhu to compose the polemical work परमार्थ-saptati against सांख्य.

The चीनाचार्य Hsuen-Tsang's student Kuei-chi considered the positions of the सांख्य theorists as the पूर्व-पक्ष in defending the bauddha mata. Here, he mentions that the chief सांख्य teacher was a certain ba-li-sha in chIna-भाषा, which meant "rain" in deva-भाषा. His followers were known as the "rain-host" (which would be a Chinese translation of varSha=rain; गण=host). Their chief work is supposed to have been some thing called the हिरण्य-saptati, which was used in debating against Indian bauddha Acharya-s who were the teachers of the chIna-s.

Thus, the available Chinese and Hindu material clearly indicate that वार्षगण्य was a major सांख्य teacher who had influenced the course of its development.

परमार्थ mentions that buddhamitra was vasubandhu's teacher. Now the White Indologists and their fellow travelers concluded that since वार्षगण्य was a teacher of विन्ध्यवास, वार्षगण्य should have in turn been a senior contemporary of vasubandhu and place him at around 350 CE. This claim has since accepted and used over and over again to date व्यास भाष्य, the yoga-sUtra etc. However, this claim when examined closely has problematic issues and arises from the indological habit of accepting certain traditional presentations as facts over others, to suit the late dates typically favored by White Indologists.

 1.  The Chinese work of परमार्थ is clearly mythologizing वार्षगण्य- it calls him the king of the nAga-s. 2) It calls buddhamitra as the teacher of vasubandhu during विक्रमादित्य's reign. But this is contradicted by the accounts of Hsuen-Tsang and Kuei-chi who state that vasubandhu was ब्राह्मण manoratha's student and do not establish a direct connection between the bauddha Acharya defeated by the सांख्य-s and vasubandhu. 3) परमार्थ skips from विक्रमादित्य to बालादित्य in his narrative (around 470 CE) suggesting his account of gupta chronology was contrived. 4) The Chinese sources also conflate the हिरण्य-saptati with the कारिक of Ishvara-कृष्ण -- something for which we have no evidence from any Indian source. In conclusion we see 1) the general sketchiness of परमार्थ's narrative from the historical viewpoint, 2) its variance from other chIna sources, and 3) the tendency of both परमार्थ and Kuei-chi to mythologize वार्षगण्य, though to different degrees. Thus they can hardly be used to conclusively anchor the date of वार्षगण्य to the exclusion of evidence from all other Indian textual sources.

In the Hindu sources वार्षगण्य is associated with the tradition of the सामवेद. There was a major सामवेदिc redactor of that name who is mentioned in the जैमिनीय गृह्य sUtra, and there might have even been a shAkha of the सामवेद (or sub-school of जैमिनीय) by that name. The वंश ब्राह्मण of the सामवेदिc tradition provides his full name as sushravas वार्षगण्य and places him as a student of प्रातराह्न kauhala. A certain वार्षगण्य is also referred to in bharata's नाट्य शास्त्र as the founder of a technique of discharging a weapon. The most important mention of वार्षगण्य is in the महाभरत (vulgate/Ganguli edition 12[शान्ति parvan].319). The specific context of his mention is notable: Chapters 12.302-12.316 (vulgate) contain an elaboration of different forms of सांख्य thought -- including some of the high points of Hindu thought, namely early formulations of the evolutionary theory of life. This is followed by chapters 12.317-13.320 that delve into yoga. The yoga presented here (12.317), while brief, is definitely related to the presentation in पतञ्जलि's sUtra-s. It mentions the प्राणायम, the eight limbs of yoga, the attainment of siddhi-s and the state of samAdhi. However, it is important to note that there is no mention what so ever of पतञ्जलि. Instead in the subsequent chapter (12.319) in a brahmodaya between याज्ञवल्क्य and the gandharva विश्वावसु we are given a list of yoga-सांख्य teachers, which includes the name वार्षगण्य, along with other सांख्य teachers like kapila, jaigishavya and पञ्चशिख but not पतञ्जलि. This, together with the general indication of the epic सांख्य preceding the classical सांख्य, suggests that पतञ्जलि, unlike kapila, was a later figure and was systematizing in his sUtra-s the yoga already presented in the epic. It also shows that वार्षगण्य was an early pre-पतञ्जलि teacher of the सांख्य-yoga school rather than a senior contemporary of vasubandhu of the abhidharma.

In all likelihood given वार्षगण्य's mention in the Mbh, he was a pre-bauddha teacher of an atomic सांख्य theory. Perhaps this also explains why the व्यास भाष्य is termed so. It was probably based on a सांख्य text of वार्षगण्य associated with the महाभारत and thereby व्यास. Hence, it could have inherited that name, although it is entirely possible that the author was simply named व्यास and conflated with the Epic author (e.g. वाचस्पति in his commentary calls the yoga-भाष्य's author veda-व्यास). Ultimately, in all accounts it was the followers of his school who revised the original teachings of वार्षगण्य that the bauddha-s debated, rather than वार्षगण्य, consistent with him being earlier than the dates of those debates.

We may reconstruct the following from the prior discussions the following relative temporal series:\
vedic ब्राह्मण authors->(याज्ञवल्क्य and his school)-> kapila-> पञ्चशिख-> jaigishavya and वार्षगण्य-> पतञ्जलि-> व्यास of भाष्य + other members who follow वार्षगण्य school-> Ishvara-कृष्ण (classical सांख्य)-> गौडपाद-> शङ्कर-> वाचस्पति mishra

The four following the vedic ब्राह्मण authors, along with the parallel tradition of the भृगु स्मृति, constitute the संख्य of the महाभारत. We would place all आचार्य-s prior to Ishvara-कृष्ण as being before 400 CE.


