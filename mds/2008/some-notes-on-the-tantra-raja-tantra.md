
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on the tantra-rAja tantra](https://manasataramgini.wordpress.com/2008/11/07/some-notes-on-the-tantra-raja-tantra/){rel="bookmark"} {#some-notes-on-the-tantra-raja-tantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 7, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/07/some-notes-on-the-tantra-raja-tantra/ "7:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The tantra-rAja is a key श्री-kula tantra that forms the basis for many of the practices of the कादिमत. The पटल 34 lists the key विद्या-s which are very important for the attainment of various puruShArtha-s of the kaula practitioners. The study of पटल 34 with its mantra-s, their rahasya-s and ओषधि-s are important for a deshika in the making as a part of his basic curriculum in the mantra-शास्त्र. The mantra-s are: siddha-सारस्वत; मृत्युन्जय; त्रिपूटा; garuDa; अश्वारूढा; annapUrNA-देवी; नवात्मन् and कुब्जिका as नवात्मिका; देवीहृदय; गौरी; लक्षदा; निष्कत्रयप्रदा; इष्टदायिनी; मतञ्गिनी, रज्यलक्ष्मी; महलक्ष्मी, सिद्धलक्ष्मी and the multiple गोपाल mantra-s. Then the chapter describes ओषधि-s which are used to produce visions of various देवता-s.

  - [शुचिः स्वेन ततो माया वियद्-दाहस्व वह्नियुक् ।\
हम्स हृत् तेजसां योगाद्-द्युति-दाह-चरस्वकैः ॥\
पुनश्च हंस-हृद्-धंस-द्वय-दाहवनैर् अपि ।\
समायैर्-उदिता विद्या पञ्चार्णामृत-विग्रहा ॥]{style="color:#99cc00;"}* (TR 34.5-6)

The commentary of प्रकाशानन्द-नाथ deshika:\
[शुचिर् इत्यादिना विग्रहेत्यन्तेन श्लोक-द्वयेन सिद्ध-सारस्वत-विद्या-स्वरूपं उपदिशति । तत्र शुचिः एकारः स्वेन बिन्दुना सहेति शेषः(=ऐं)। ततः अनन्तरम्-अक्षरं इति शेषः । माया हृल्लेखा(=ह्रीं)। वियद्-दाहस्व वह्नियुक् रेफ ईकार बिन्दु सहितः शकारः श्री-बीजं इत्य् अर्थः(=श्रीं)। हंस-हृत्-तेजसां योगाद्-द्युतिदाहचरस्वकैः हकार-सकार-स्वकार-फकार [एमेन्देद् फ़्रोम् रेफकार] -रेफ-एकार-बिन्दु-सप्तभिर्-अनन्तरं बीजम्-इत्य्-अर्थः[ (=h-s-sv-phrem)]{style="color:#ff9900;"}। पुनश्-च अनन्तरम्-अक्षरं । हंस-हृद्-धंस-द्वय-दाहवनैर्-अपि समायैः हकार-सकार-हकार-द्वय-रेफ-औकार-विसर्जनीयात्मभिः सप्तभिर् अक्षरैः पञ्चमं बीजम्-इति यावत्[(=h-s-h-ह्रौः)]{style="color:#ff9900;"}]{style="color:#99cc00;"}[। अमृत विग्रहा सप्त-विंशति पटले प्राग्-उक्त-अमृतेश्वरी विद्यायाः प्रोक्त ध्यान विग्रहा । प्रयोग विशेषे ध्यान विशेषः प्रोक्तश्च तत्रैव्-एत्य्-अर्थ ॥]{style="color:#99cc00;"} [\
siddha-सारस्वतं= aiM ह्रीं ह्स्रीं h-s-sv-phrem h-s-h-ह्रौः ||]{style="color:#99cc00;"}

In actual practise R1's father indicated the use of aiM ह्रीं ह्स्रीं h-s-kh-phrem h-s-h-ह्रौः which would require an emendation of the svakara to khakara, but it could also be a parallel tradition. The rare bIja h-s-sv-phrem is also encountered in the श्रीकुल त्रिपुरार्चन krama while showing the खेचरी mudra.

For dhyAna the form of अमृतेश्वरी is described in the earlier पटल is recommended. Even though प्रकाशानन्द says that it is in the sapta-विंशति पटल, in reality the dhyAna is found in the षड्विंशति पटल of the extant tantra-rAja text. This means he has either made a mistake or had a different text with one extra chapter with him.\
अमृतेश्वरी ध्यानं:

  - [शुद्ध-स्फटिक-सङ्काशां मौलि-वद्-धेन्दु-निर्गतैः ।\
अमृतैर्-आर्द्र-देहास्तास्त्व्-अमलाभरणान्विताः ॥\
कुमुदं पुण्डरीकङ्कञ्च राकेन्दुम्-अमृतं करैः ।\
दधानाः स्मेर-वदना मुक्ताभरण-भुषिताः ॥]{style="color:#99cc00;"}* (TR26.64-65)

It is interesting to note that the siddha-सारस्वतं mantra is just prior to that of अमृतेश्वर bhairava or the मृत्युञ्जय ([ऒं जूं सः]{style="color:#99cc00;"}) in the TR. This suggests that they come as a pair from a system like that of the netra tantra. Also we find that the dhyAna given for this mantra is that of देवी अमृतेश्वरी. This dhyAna is a cognate of the देवी parA, a link we have discussed earlier. Importantly, the prayoga of the siddha-सारस्वतं involves giving a girl with blemishless body a drink of milk charmed by the mantra. This is said to result in her speaking like सरस्वती and know the past, present and future. This is reminiscent of the deployment of the parA-related mantra like bAlA in gaining poesy as per the suggestion of क्षेमेन्द्र. It might also be related to the कन्याकुमार (probably कन्याकुमारि) mantra of राजशेखर recommended for the same purpose.

The dhyAna for अमृतेश्वर bhairava for the मृत्युञ्जय mantra is given thus:\
[*आद्य-मद्य-तृतीयार्ण-नाल-पद्म-सुकर्णिके ।\
समासीनं सुधार्द्राङ्गं मौलाबिन्दु-कला-युतं ॥\
सुसितं हरिणाक्ष-स्रक्-चिन्ता-पाश-करं हरम् ।\
स्वैक्येन भावयेन्-नित्यं जय-दीर्घअयुरेधते ॥*]{style="color:#99cc00;"}TR 34.13-14

The first letter (OM) is conceived as being on the stalk of a white lotus, the second letter (जूं) on the petals and the third saH on the pericarp. Seated on this lotus is conceived अमृतेश्वर of excellent white complexion, with his body imbrued by nectar and bearing a crest moon on his head, holding a deer, rosary, displaying the chinta (व्याख्यान) mudra, and a lasso. The ritualist who mediates thus, conscious of his oneness with hara attains victory and a long life.


