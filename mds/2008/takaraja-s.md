
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [टाकराज-s?](https://manasataramgini.wordpress.com/2008/10/05/takaraja-s/){rel="bookmark"} {#टकरज-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 5, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/05/takaraja-s/ "6:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After long we spoke to Sharada. She mentioned reading our note on लोलिम्बराज. She described her own little walk through medieval Hindu history in relationship to the development of medicine. She mentioned that her father had a couple of medical manuscripts, one of which was attributed to the टाकराज मदनपाल. It was called the madanavinoda. It is apparently a typical Hindu निघण्टु that contains synonyms and properties of different ओषधि-s and other medicinal substances. Sharada brought up the fascinating historical puzzle of the टाकराज-s. They were apparently great patrons of ब्राह्मण-s and Hindu learning, who by their own records ruled a small principality in काष्टा on the banks of the यमुना \[to the north of Delhi]. The last verse of the madanavinoda states that it was completed on the date corresponding to Monday 8th Jan 1375 CE. Sharada was puzzled by the fact how could a patron of ब्राह्मण-s and a Hindu state exist in this place at this date?

Investigations in this direction are first confounded by the puzzle of where exactly is काष्ट. Apparently some people thought it might काष्टमण्डप (Kathmandu) in Nepal given that it was a safe Hindu haven in this period even as the तुरुष्क-s were raging in and around Delhi. But this seems unlikely because the टाकरज-s mention being on the bank of the यमुना and nobody has ever mentioned a यमुना near Kathmandu (to the best of my knowledge). The other candidate, the one preferred by Sharada, is modern Kathgodam. Her own family had some connections with this region in the distant past. There is also ample evidence that it was a fertile Hindu haven and the local kings were able to fend off Islamic takeover well until the Mogol period. This is supported by the survival of several Hindu temples to date in this region. One of the टाकराज-s who composed a Sanskrit-Prakrit poem mentions that their family worshiped केदार---this is consistent with their location in a mountain station like Kathgodam. This sub-Himalayan or Panjab hill location is also consistent with the mention of the टाक-s in connection to Kashmirian history by कल्हण in the राजतरम्गिणि. The only catch with this hypothesis is that Kathgodam is not on the यमुना. It is instead on a river called gaula that does not joint the यमुना either. Thus, while an attractive candidate we are left with the same problem as Kathmandu. This finally leaves us with two obscure villages in the vicinity of Mirat (Meerut), namely Kathauli and perhaps Kithaur --geographically more apt, but somewhat unlikely for being the seat of the टाकराज-s given other textual connections to the Himalayan regions. One point of note in this regard is the fact that the record of the invasion of Timur-i-leng mentions a strong Hindu resistance in this region. This is consistent with the presence of a Hindu stronghold in the mountains quite close to the heart of the Delhi Sultanate that could fend of their depredations.\
The literary activity in the टाकराज court that we were able to gather:\
मदनपारिजात: A work on dharma based on मानव dharma शास्त्र by ब्राह्मण गुणाकर vishveshvara भट्ट.\
आतङ्कदर्पण: A medical work based on माधवनिदान by वाचस्पति, father of vishveshvara.\
तिथिनिर्णयसार: A calenderical work of vishveshvara भट्ट.\
स्मृति-कौमुदी: A general dharma-शास्त्र work by vishveshvara.\
madanavinoda: The medical निघण्टु work composed by king मदनपाल with assistance from vishveshvara.\
वासनार्णव: A work of king मदनपाल on construction of gnomons, some trigonometry, and Hindu cosmography and calender.\
Yantra-प्रकाश: A work by मदनपाल on construction of time-keeping devices, astrolabes, and devices to measure planetary longitudes.\
महार्णव: Apparently a medical treatise composed by vishveshvara jointly with मान्धातर्, the younger son of मदनपाल.\
शिशुरक्षारत्न: A pediatric treatise composed by पृथिविमल्ल, the elder son of मदनपाल.\
रसरत्नप्रदीप: A रसायन text composed by रामराज, a descendent of मदनपाल.\
भावशतक: A Sanskrit-Prakrit puzzle poem by नागराज, an ancestor of मदनपाल.

So the obscure टाकराज-s appear to have been rather active in upholding Hindu learning in North India right in the midst of the तुरुष्क incubus over quite a reasonable stretch of time in medieval India. We observe that मदनपाल took on the title abhinava-bhoja, just like another more famous and mighty ruler from southern India, कृष्ण-deva-rAya of vijayanagara. Thus, the inspiration of the great bhojadeva परमार did seem to linger on amongst great and small kings keeping alive the Hindu spark even in the declining years.


