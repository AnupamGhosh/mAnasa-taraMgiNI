
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Into the आशाछिद्र](https://manasataramgini.wordpress.com/2008/09/30/in-to-the-ashachidra/){rel="bookmark"} {#into-the-आशछदर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 30, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/30/in-to-the-ashachidra/ "1:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The chakra of the yuga had turned,\
Life by five संवत्सर-s had onward moved,\
Does आयुवृद्धि also mean yama has closer approached.

The past seems partly forgotten and partly telescoped,\
Impelled by the mind's diktat we had ahead foraged,\
In saying so had we the stochasticity of neurotransmission ignored?

From place to place like कापालिक we had wandered,\
Like a toad we had from nook to nook we had saltated,\
Like indra the realms we had conquered.

But lay in the hazy future, we wondered,\
For indeed वसिष्ठ said no man has the future cognized!


