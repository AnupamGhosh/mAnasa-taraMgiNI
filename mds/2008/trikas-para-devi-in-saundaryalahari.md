
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [trika's parA-देवी in सौन्दर्यलहरी](https://manasataramgini.wordpress.com/2008/07/26/trikas-para-devi-in-saundaryalahari/){rel="bookmark"} {#trikas-para-दव-in-सनदरयलहर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 26, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/26/trikas-para-devi-in-saundaryalahari/ "9:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R had asked me if trika's देवी is encoded in the सौन्दर्यलहरी. Just as how the कुब्जिका-mata's यामल is encoded in the SL, likewise the anuttara lineage's त्रिका देवी is also encoded in the सौन्दर्यलहरी. The verse is SL15\
sharaj-ज्योत्स्ना शुभ्रां shashi-yuta जटा-जूट मकुटां\
vara-त्रास-त्राण स्फटिक-गुटिका पुस्तककराम् |\
सकृन्-नत्वा नत्वा katham-iva सतां sannidadhate\
madhu-क्षीर-द्राक्षा-madhurima-धुरीणाः फणितयः ||\
In our tradition we take the reading गुटिक (अक्षमाल) against लक्ष्मीधर's घटिक or cup/pitcher.

In this form she is linked to the glorious lipi-भैरवी who is the manifestation of the great मालिनी in the श्री-kula system.\
\[As an aside, the very sight of the अक्षर-s of lipi-भैरवी are enough to inspire even the fading mind- is there any doubt that our co-ethnic आद्यनाथ achieved total तत्त्वावेश with her विद्या]


