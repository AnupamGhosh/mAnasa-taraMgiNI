
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [graha प्रतिगरं](https://manasataramgini.wordpress.com/2008/04/05/graha-pratigaram/){rel="bookmark"} {#graha-परतगर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 5, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/05/graha-pratigaram/ "11:15 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We invoked the terrifying son of पिनाकिन्, the seizer, the awful विनायक with a gleaming tusk dripping with the blood of देवद्विट्-s gored in hand-to-hand combat. He held a frightful axe still unwashed with the blood of his foes slain in the battlefields of heaven and earth. He embraced the lovely मदद्रवा with her eyelids gently shaded and with eyes of the form of long lotus petals casting stupefying side-glances. Seated on his lap, she grasped a wondrous lotus and tightly embraced the fiery ulka. Around them stood all manner of hideous goblins holding tridents, गदा-s, भिन्दिपाल-s and axes.

Next we called upon the one of large goblin-like form, the ugra-भूत, who held a heavy crooked staff and a sharp straight sword. He rode a white swift horse and was embraced by his wives पूर्णा and पुष्कला of pretty smiles. He was accompanied by 8 horse borne गण-s with deadly bows.

Then we humbly called upon the lord of the graha-s to bear us aid against the आततायिन्-s. The noise was terrifying -- there were all manner of hideous roars and shrieks and cries and clangs. There was loud music filled with vIra-bhAva. There we saw a multitude of awful graha-s -- some had the heads of tigers, others of elephants, others of rams and ibexes, yet others of birds and still others with bovid, rhinocerotid and reptilian heads. There were extreme fear-inspiring स्कन्दापस्मर-s and वीरबाहु-dala-s. There were male and female twins मूजिक and मिञ्जिका with maces and lances. There was bewitching मुखमण्डिका of pretty hips roving around with bands of कुमार-मातृ-s. In their midst was चतुष्पथ-निकेता, जातहारिणी, शकुनीदेवी, सरमादेवी and लोहितायनी of great violence, decorated with bones and venomous snakes as ornaments. There was the frightful shishu holding a trident and shAkha and विशाख with spears. In their midst sat the illustrious धूर्त ensnared by the fair arms of the all-fascinating षष्ठी of firm breasts. We showed the त्रिशूल mudra and the shakti mudra. We sought सायुज्य with him so that we may become ghora to our भ्रातृव्य-s.


