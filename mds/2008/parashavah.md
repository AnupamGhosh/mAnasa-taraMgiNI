
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [परशवः](https://manasataramgini.wordpress.com/2008/09/03/parashavah/){rel="bookmark"} {#परशव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 3, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/03/parashavah/ "1:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This was inspired by two sights from long ago. In 1987 we visited the then rarely open State Archaeological Museum in the city of our birth which housed an enormous collection of Indian arms assembled by a म्लेच्छ official. Then in April of 2000 we visited the art museum at Chicago which had an impressive wall of European battle axes and hammers. Some of those stuck well in our sub-cranial contours.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3261/2822816569_f313b3ee6f_b.jpg){width="75%"}
```{=latex}
\end{center}
```




