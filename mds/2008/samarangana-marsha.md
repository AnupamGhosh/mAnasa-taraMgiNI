
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [समराङ्गण marsha](https://manasataramgini.wordpress.com/2008/01/27/samarangana-marsha/){rel="bookmark"} {#समरङगण-marsha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 27, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/01/27/samarangana-marsha/ "6:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

khaT फट् jahi !\
In the tales of Sherlock Holmes many a time the great narrative of Dr. Watson could see the light of day only after a long silent interlude. Even so perhaps the whole history of the great struggle will be narrated only in the future. For that the only person who can be the narrator should remain alive at the conclusion of the story. Hence, there are some stories which never have an ending. With such thoughts we wondered how it might be possible to send the signal even if we were to be sent to vaivasvata's domain. Three past statements came to mind: 1) The learned Acharya before going on the southward path had said: "All my विद्या-s will become divided among you all. Each with some, but none with all." 2) When we stood there before the greatest of the great indra, he said: "Chose the choice of मार्कण्डेय. There is glory in it but only one who lives the life of मार्कण्डेय can complete it" 3) pretty मानसिका said: "You will hear me call you on the night of the turning of the 9th year of the decade of that मानुश yuga. A ग्राही of the terrible kind would have seized you then. You would turn back to look who called but you will see no one."

I continued walking on that weary path after मानसिका made that statement. After having gazed at pita द्यौः with our Aptavarga we shook off that above-said ग्राही by a great याग where we saw the thunderer as the eagle. We kept walking on that path. When PM was routed for his final journey by the अभिचार of Fourier of मण्गल-grAma we were seized by the second ग्राही.

The अभिचार-s of Fourier had probably gone with him. It was prayoga against the "secular ari". Much had happened between the great strike of the दुर्णम, a terrible पिषाच and a vile mohini at mid-night. Those events will form part of the great narrative if comes to pass. It was a quite night and we had just rebuilt our fort which had smashed to smithereens by the ari. Suddenly we sighted the troops of the दुष्टा trailing us. With the usual taskara-like slyness, they slipped through our defenses to take a stab at us. We had placed a decoy that they promptly pummeled with all their hate. This gave enough time for our dUra-दृष्टि prayoga to detect their line of action. They had broken through our second wall in the mean time. We knew where the कृत्या was by then. We immediately reacted from our secret fort and carried out an immediate उच्छाटन on her. While retreating she however placed two valaga-s against us and was lying in wait in the nest. We invoked विनायक to protect us against her and escaped to the northern nest. Seeing this she retreated to the southern lair. We feared the strange Apya-valaga that she had placed right in the path -- we are still a bit puzzled why it was still unbroken after so many hours.

khaT फट् jahi !


