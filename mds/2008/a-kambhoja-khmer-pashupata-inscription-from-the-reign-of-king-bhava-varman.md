
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A Kambhoja (Khmer) पाशुपत inscription from the reign of king bhava-varman](https://manasataramgini.wordpress.com/2008/08/03/a-kambhoja-khmer-pashupata-inscription-from-the-reign-of-king-bhava-varman/){rel="bookmark"} {#a-kambhoja-khmer-पशपत-inscription-from-the-reign-of-king-bhava-varman .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 3, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/03/a-kambhoja-khmer-pashupata-inscription-from-the-reign-of-king-bhava-varman/ "6:06 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A Sanskrit inscription from the Phnom Prah Vihar hillock in Cambodia, copied by the French researcher Coedes (IC. 3), provides a further illustration of the point discussed our earlier note on the link between nyAya-वैशेषिक and the पाशुपत-s. It also underscores the point of how the पाशुपत-s appear to have dispersed these philosophical systems widely over Asia. The inscription is associated with king bhava-varman of Kambhoja. A large number of inscriptions appear to be of the king bhava-varman-I who founded a new Kambhoja kingdom by annexing Funan and Vietnam to his Cambodian base. This would place it around 590 CE when he was at his peak. There was a second bhava-varman, his successor 3rd in line (in the earlier half of 600s of CE). But the lack of mention of bhava-varman-II's father इशान-varman makes it likely that the king of this inscription was bhava-varman-I the founder of the dynasty. The inscription is in the AryA Chandas and refers to a पाशुपत आचार्य विद्यापुष्प. The extant of the surviving portion of the inscription is 9 verses and is broken towards the end. I just provide a few excerpts.

[जयति इन्दु रवि व्योम वाय्वात्म्[अ/आ]-क्ष्मा जलअनलैः ।\
तनोति तनुभिश्- शम्भुर्-योष्टाभिर्-अखिलञ्-जगत् ॥ (१)]{style="color:#99cc00;"}\
A praise of shambhu in his universal manifestation.

[राजा श्री-भव-वर्मेति भवत्य् अधिक-शासनः ।\
सोम-वंशयोऽप्य् अरि-ध्वान्त प्रध्वंसन दिवाकरः ॥ (३)]{style="color:#99cc00;"}\
A praise of the king bhava-varman of the lunar race.

[तस्य पाशुपताचार्यः विद्यापुष्पाह्वयः कविः ।\
शब्द-वैशेषिक-न्याय-तत्त्वार्थ-कृत-निश्चयः ॥ (४)]{style="color:#99cc00;"}\
This verse describes the पाशुपताचार्य विद्यापुष्प who is mentioned as a scholar of shabda (=grammar) and वैशेषिक and nyAya.

Then the inscription goes on to describe the dream manifestation of rudra to the Acharya and his endowment to the shiva shrine of cows, land, gold and attendants
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3113/2729115138_f5cddb2a88_o.jpg){width="75%"}
```{=latex}
\end{center}
```



Possible ruins from विद्यापुष्प's shrine


