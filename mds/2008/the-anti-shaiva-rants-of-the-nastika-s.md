
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The anti-shaiva rants of the nAstika-s](https://manasataramgini.wordpress.com/2008/05/25/the-anti-shaiva-rants-of-the-nastika-s/){rel="bookmark"} {#the-anti-shaiva-rants-of-the-nastika-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 25, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/25/the-anti-shaiva-rants-of-the-nastika-s/ "3:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The below may offend Hindus; Hence viewer discretion is advised.

Both the bauddha and jaina nAstika-s saw shaiva-s of all hues as major competitors. It is interesting to note that despite the popularity of पाञ्चरात्र tantra-s much of bauddha hate is particularly directed at the shaiva-s rather than vaiShNava-s. While shuddhodana-putra was a subversionist, he cannot be described as having special hate for his Astika rivals. But his successors definitely had a considerable hate for Astika-s in general and shaiva-s in particular. In contrast, we do not see this level of directed spite and hate on the Astika side. The unidirectionality of it suggests that the nAstika-s indeed had an inferiority complex and were vigorously competing with Astika-s and did everything to absorb key elements of their new innovations. So it not at all surprising when we hear of tales of nAstika-s oppressing Astika-s. The lurid and obscene details of the nAstIka hate for Astika-s has often been concealed from audiences due their sheer repulsiveness. However, we felt that some of this may be made public for the sake of information. It must be mentioned that some of the promulgators of these nAstika शास्त्र-s were ब्राह्मण-s with a new convert's zeal. But in practical terms I was reminded of R1's father's special experience in Nepal in the form of the mantra-battle he had fought in his youth, which I described earlier.


 1.  sarva-तथागत-tattva-संग्रह; 6th पटल\
The buddha vairochana seated on the top of meru asked the bodhisattva वज्रपाणि to enter his मण्डल. He said he would not enter until he had subjugated all the Astika देवता-s. So vairochana started uttering incantations to bind and drag all देवता-s before his abode on meru. He also dragged the terrible maheshvara and his गण-s there, whom the other तथागत-s had till then failed to conquer. The देवता-s were commanded by वज्रपाणि to accept the nAstika-vrata-s and convert to the bauddha-mata. महादेव was enraged and declared that he is the supreme deity and source and end of the universe, who will not take orders from a mere यक्ष like वज्रपाणि. वज्रपाणि retorted that he who eats corpses and whose clothes, bed and food are funeral ashes should surrender. But maheshvara refuses and displayed his terrifying महारुद्र and महाभैरव forms. Upon seeing those awful forms of rudra, vairochana uttered the mantra "OM nisumbha vajra huM फट्" and वज्रपाणि utters a loud "hUM". rudra is killed by this mantra and the other देवता-s beg the buddha and bodhisattva to spare them. They declared that they did not understand the bauddha-dharma and it would be un-bauddha of a merciful bodhisattva to kill them. So वज्रपानि restored them and made them his attendants. He then uttered "वज्रायुः" and brought महादेव back to life but he refused to convert and declared that he would die rather than convert to the bauddha-mata, and again showed his terrifying महारुद्र form. There upon वज्रपानि bound him and his shakti उमा with his mantra-s and tramples them under his left and right foot respectively uttering the formula "OM वज्राविश hanaya traM त्रट्. He then laughed loudly, even as the three worlds declared वज्रपाणि the victor. By the touch of the feet of वज्रपाणि and the power of vairochana's mantra "OM buddhamaitri वज्ररक्ष hUM" acting on महादेव he became a new buddha known as bhasmeshvara-निर्घोष. He became the presiding buddha of the realm known as भस्माच्छन्न in a distant universe from this triloka. Then वज्रपाणि trampled successively नारायण, कुमार, brahmA and indra and they too were transformed into tutelary deities of the मण्डल after being given \*new names*.


 2.  guhyagarbha, 15th पटल\
maheshvara starts propagating tantra-s that negate the concepts of शूण्यत and अनात्मन्. He was sent to different naraka-s for this by the तथागत but he escapes and emerges as rudra who terrorizes the world with diseases. In order to "rescue" rudra from the ocean of saMsAra the तथागत spawns a कुलाञ्गना named क्रोधेश्वरी. As a result of their coitus a host of देवता-s is born to conquer rudra and his गण-s. Then the तथागत himself assumes the form of heruka with 3 heads, six hands and four legs and tramples rudra and उमा under his feet. But rudra and चण्दिका escape and return to take control of the triloka. So he becomes an even more dreadful heruka with 9 heads, 18 hands and 8 legs and with his host seizes rudra and his गणस् and disembowels them. Then he cuts their limbs to pieces and eats their flesh and drinks their blood. Then he uses their skulls as ritual bowls and bones as ornaments. He digests rudra and his host but discards their hearts and sense organs and finally excretes them into a giant ocean of crap that is then drunk up by his attendant उच्छुष्म-krodha. Then maheshvara and his गण-s are revived and seated as attendant deities in heruka's yantra. The wives, daughters and mothers of rudra and his गण-s are given to heruka, उच्छुष्म-krodha and others as sexual partners for their kula rites.


 3.  A rant of the old school Tibetan Lama Bu ston from around 1300 CE\
maheshvara at the head of asuras, नागस्, यक्षस् and रक्षस् takes control of 24 holy पीठ-s of भारतवर्ष. As महादेव was in maithuna with his shakti कालरात्रि he asks his hordes to set up and worship 24 लिङ्ग-s. The primal buddha vajradhara assumes the form of heruka on meru and emanates 24 डाकिनी-s and vIra-s with whom he subjugates the देवता-s and crushes the लिङ्ग-s at the 24 पीठ-s. In their place he introduces the practice of the bauddha-kaula doctrines.


