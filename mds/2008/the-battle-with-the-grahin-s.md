
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The battle with the ग्राहिन्-s](https://manasataramgini.wordpress.com/2008/05/04/the-battle-with-the-grahin-s/){rel="bookmark"} {#the-battle-with-the-गरहन-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/04/the-battle-with-the-grahin-s/ "3:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The return to the mantra of skanda suppressed the second ग्राहिन्. The first ग्राहिन् appeared to be suppressed but returned with a vengeance to spread fear. Then out of the blue a चक्षुर्मन्त्रिन् and a कण्डूयन-पिशाचिनी were dispatched. For 10 minutes it the former sent us into a state of terror but the prayoga of rudra drove it away without further harm. Then the पिशाच्\~आन्गना gripped us along with the first grahin which had returned to torment. The dart of विष्णु had finally thrown away the पिशाची.


