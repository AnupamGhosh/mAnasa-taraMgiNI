
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [श्मशानाब्धि](https://manasataramgini.wordpress.com/2008/07/19/shmashanabdhi/){rel="bookmark"} {#शमशनबध .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 19, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/19/shmashanabdhi/ "4:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3154/2681760758_fe455fff47_o.jpg){width="75%"}
```{=latex}
\end{center}
```



We sought to experience the might of the great aghora, the god of the gods, the great bhairava. कुब्जीशान is coupled with the great देवी समया, who as कामेश्वरी manifests as the षोडशी विद्या and who as वक्रेश्वरी shines in the midst of the सम्वर्तामण्डला. His bIja appearing as the bIja of the 9 tattva-s in the निःश्वास-tattva saMhitA of the Urdhva-srotas, manifesting as a bIja in the circle of the frightful कपालीश bhairava conjoined with rakta-चामुण्डा of the brahma यामल from the विद्या-पीठ, as a bIja of the circle of स्वच्छन्द bhairava in the स्वच्छन्द tantra from the mantra-पीठ, a bIja of Ananda-bhairava in the श्रीकुल tradition and as the bIja of the splendid aghora in the पश्चिमाम्नाय.

aghora's whole form, a manifestation of power, is apparent in the glorious mantra: ह्स्क्ष्म्ल्व्र्यूं. Hence he is called नवात्मन्. The great भैरवी as his counterpart is seen in the mantra: ह्स्क्ष्म्ल्व्र्यीं. वक्रेश्वरी manifesting as the mistress of chandrapura manifests as the vAg-bhava. Hence her second mantra is aIM. Manifesting as चिञ्चिनी she is seen as the great mantra ह्स्ख्फ्रें. Hence by संपुटीकरण we get her magnificent tri-crested mantra aIM ह्स्ख्फ्रें ह्स्क्ष्म्ल्व्र्यीं.

To experience aghora conjoined with वक्रेश्वरी we headed from the great flat plateau in the सह्याद्रि with our companions to that great row of cliffs filled with the चिञ्च trees beneath which rests the great देवी सम्वर्ता. Then we arrived at the above पितृवन, where one can clearly see the calcinated bones whitening the banks of the lake. At its edge is situated a grove of चिञ्च trees and beneath one such tree we took our station, surrounded by osseous fragments and ash. Then performing पुरश्चरण and dhyAna on the secret yantra hidden in the संवर्तामण्डल sUtra-s we attained the state of yoga and सायुज्य with the यामल. We saw in the लिञ्ग under the चिञ्च tree उद्यान-bhairava, the sight of whom is the temporary condition of all-knowledge. In a state of inspiration we uttered the दण्डकं of chandrapura:\
"पादार्गलैस् te .api tvan-nAma-सङ्कीर्तनाद् devi मुञ्चन्ति\
ghorair mahA-व्याधिभिः संस्मृत्य पादारविन्द-द्वयं te\
mahA-कालि कालाग्नि-तेजःप्रभे skanda-govinda-brahmendra-चन्द्रार्क-\
पुष्पायुधैर् mauli-माला-lasat-padma-किञ्जल्कसत्-पिञ्जरैः sevyase\
sarva-वीराम्बिके भैरवी bhairavas te शरण्यागतो .अहं"


