
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Fractal lacework](https://manasataramgini.wordpress.com/2008/11/25/fractal-lacework/){rel="bookmark"} {#fractal-lacework .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 25, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/25/fractal-lacework/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3050/3044204895_e82ec62739.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3167/3036110721_e353f80f58.jpg){width="75%"}
```{=latex}
\end{center}
```



