
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [AV विकृति पाठ](https://manasataramgini.wordpress.com/2008/08/21/av-vikriti-patha/){rel="bookmark"} {#av-वकत-पठ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 21, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/21/av-vikriti-patha/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

With the उपाकर्म and veda-vrata having gone by last full-moon we took the opportunity to illustrate the विकृति पाठ-s of the AV-shaunaka. The विकृति पाट्ःअ-s of the AV are largely extinct. But the AV-shaunaka reciters do preserve portions of the different काण्ड-s in विकृति पाट्ःअ. Some recordings made by a Maharashtrian AV paNDita Ratate are available. But the जटापाठ, which as far as I know, is practiced only for कण्ड 17 is largely unknown among most chanters. These mantra-s are used in the atharvanic rite to इन्द्राविष्णू in autumn for the offering of the sweet cake known as Aditya-मण्डक. My आचार्य knew several विकृति-s of different parts of the AV entirely mentally. I wrote down his svara-s in part that I learned. He pointed out that while the ब्रह्माण्ड पुराण gives व्याडी as the authority of the जटापाठ, the AV विकृति recitation follows the rules of an authority called हयग्रीव. These rules come into play in the जटा only, because the rest is specified by the प्रातिशाक्य grantha-s like the चतुराध्यायिक. The rules concern those sequences known as vyutkrama-s where the order is reversed. Hence they are not a part of आर्स्ष language and are called अनार्ष for which the grammar needs to be reworked. The rule of हयग्रीव is thus given:\
[अभिक्रमे क्रमशास्त्रं प्रधानं स्याद् व्युत्क्रमे व्याकरणं प्रधानं ।\
उक्तः क्रमे व्युत्क्रमणे विशेषः स्वराः प्रकर द्वितियेऽप्याभिन्न ॥]{style="color:#99cc00;"}\
There must have been a text of हयग्रीव, but we have no evidence if it survived in entirety. In the cd of the above verse we see the term "उक्तः krame vyutkrame विशेषः" meaning that the difference between krama vyutkrama has been stated indicating that this text earlier discussed the definitions of krama vyutkrama in a जटा sequence.

The learned श्री PR, the knower of RV विकृति-s, stated to me that they too follow the rules of हयग्रीव.

[saMhitA पाठ recitation for AV-S 17.1](ftp://ftp.ncbi.nih.gov/pub/aravind/temp/av-s_saMhitA_17.1.mp3)\
[pada पाठ recitation](ftp://ftp.ncbi.nih.gov/pub/aravind/temp/av-s_pada_17.1.mp3)\
[जटा पाठ recitation](ftp://ftp.ncbi.nih.gov/pub/aravind/temp/av-s_jaTa_17.1.mp3)


