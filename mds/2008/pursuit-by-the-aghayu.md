
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Pursuit by the अघायु](https://manasataramgini.wordpress.com/2008/12/14/pursuit-by-the-aghayu/){rel="bookmark"} {#pursuit-by-the-अघय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 14, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/14/pursuit-by-the-aghayu/ "7:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Like the Xvarena departing from an ancient asura-worshipping king our powers forsook us in the great struggle. Yet we kept fighting like भीमसेन against राधेय on the fateful 15th day. In some ways it was like the fight of वेण्कटाद्रि and श्रीरन्ग against the तुरुष्क-s after the fall of vijayanagara or that of प्रताप सीम्ह after हल्दिघाटि. In utter frustration we thought of the days of days of our youth when our arrows rarely missed their targets. We deployed to formula of the ताण्ड्य-s:\
"yo .adya saumyo vadho .अघायूनाम् उदीरते विषूकुहस्य धन्वनाप tAn वरुणो .apa dhamatu ||"\
Long ago the fierce sphigshiras, hrasvaroman, golamukha and others rushed at us but we overthrew them in the battle of द्वादशान्त when our ratha did not stop in midst of battle. The शूल of the बाधमान was tormenting us despite a prolonged attack using the rakta-वर्ण shara-s and several ब्रह्मास्त्र-s and even the कृत्यास्त्र. Mustering our remaining strength we prepared for a fierce battle. We realized that we had been bound by the kautsa अभिचार. We knew if we could once deploy the spell of विलिस्तेङ्गा then we could come out of the ambush. But it had one mystery that kept eluding us, for he who lacks it is seized by निरृति. Who indeed escapes the spell of विलिस्तेङ्गा ?


