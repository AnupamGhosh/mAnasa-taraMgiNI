
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The yuddha resumes](https://manasataramgini.wordpress.com/2008/07/24/the-yuddha-resumes/){rel="bookmark"} {#the-yuddha-resumes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 24, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/24/the-yuddha-resumes/ "3:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

dhumaketu: The अमात्य and sachiva were also attacked but the spasha-s say that they stood firm. The agent of the दुष्ट-s lured us for a direct battle. This happened after the below events.

The sachiva and अमात्य were unsure if anything was going to happen at all. Indeed, they mentioned this triumphantly to the अध्यक्ष-s of the 3rd vIra. We obtained intelligence from that remaining vIra that MM had foolishly turned traitorous. That vIra himself was bound by his own problems and could do little. But we knew for sometime that in the field of mantra prayoga as well as the लौकीक yuddha, we could not depend on others. We also knew that the final yuddha in all its अङ्ग-s will be fought on the strength of our own arms. If we survive we will rule again, if we fall in battle we would merged with the भूत-s. So MM becoming traitorous was of little consequence to us thought it did distract us. We wanted to invade the new front of eka-शुल्बारीय hala. But we faced a strange distraction and felt the effects of the prayoga upon us that had triply broken our विनायक prayoga. It was the नेत्राग्नि. We were laid low by it in a state of slumber. With the अमात्य and the sachiva separated from the main सेनामुख we faced an unexpected attack from the ari-s who since that fateful encounter of the autumn evening were doggedly pursuing us. A big yuddha had opened -- it threatened to sap our existence when so many other things needed attention.\
"The वानर-s hurled their rocks on कुम्भकर्ण but he moved on unaffected."


