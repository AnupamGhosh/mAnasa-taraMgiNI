
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [पुष्पवान् प्रजावन् bhava](https://manasataramgini.wordpress.com/2008/07/27/pushpavan-prajavan-bhava/){rel="bookmark"} {#पषपवन-परजवन-bhava .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 27, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/27/pushpavan-prajavan-bhava/ "5:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3257/2705188118_e4d4405426_b.jpg){width="75%"}
```{=latex}
\end{center}
```



We wandered like a कापालिन् on the path to avimukta,\
कपाल in our hand, the दूती in our embrace.\
We reached the गोलाश्मन्वति in the mada of soma,\
there we picked the first four ओषधि-s that lay in out path.

