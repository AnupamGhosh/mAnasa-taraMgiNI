
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Precession of the world axis](https://manasataramgini.wordpress.com/2008/12/03/precession-of-the-world-axis/){rel="bookmark"} {#precession-of-the-world-axis .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 3, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/03/precession-of-the-world-axis/ "7:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3216/3079590902_5883529ebd.jpg){width="75%"}
```{=latex}
\end{center}
```

\
Kindly provided by valguli the "उग्रिका": you created that title for yourself :-) (It is interesting that a number of people from your minuscule राष्ट्र are interested in our राष्ट्र -- may be it is statistically significantly above chance?)

