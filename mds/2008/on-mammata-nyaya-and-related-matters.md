
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On मम्मट, nyAya and related matters](https://manasataramgini.wordpress.com/2008/04/18/on-mammata-nyaya-and-related-matters/){rel="bookmark"} {#on-मममट-nyaya-and-related-matters .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 18, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/18/on-mammata-nyaya-and-related-matters/ "6:18 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Kashmirian efflorescence was a remarkable, even if a brief phase, in the intellectual development of Hindus. Almost simultaneously we witness the emergence of several great savants, even as Kashmir defied the Islamic onslaught of the accursed Mahmud of Ghazna and his successors. It was one of the last blazes of Hindu productivity before it was seized and dented by the savagery of the barbaric Mohammedans. We see many great figures like abhinavagupta, somadeva, क्षेमेन्द्र and मम्मट. The last of these has possibly been more influential in erudite Sanskrit circles than few others. राजानन्क मम्मट the author of the काव्य-प्रकाश (KP) belonged to an scholarly clan: His father जय्यट was a co-author along with वामन भट्ट of the famous grammatical work काशिका. His first brother कय्यट wrote a once well-known commentary on the महाभाष्य of पतञ्जलि. His second brother was the famous उव्वट, a protégé of rAjA bhojadeva परमार, who is renowned for his commentary on the shukla yajurveda and its shrauta deployment.

The learned scholar Ganganath Jha brings attention to a statement that used to be prevalent in among ब्राह्मण-s whose traditional education required the study of मम्मट's work:\
[काव्यप्रकाशस्य कृता गृहे गृहे टीका तथाप्येष तथैव दुर्गमः ।]{style="color:#0000ff;"}\
टीक-s on the काव्यप्रकाश have been composed in every house yet it remains difficult to understand as ever!

Consonant with this statement apparently 49 commentaries are known to exist in different states of survival for the KP. The difficulties aside, मम्मट's work presents something interesting for every Hindu -- after all it was consider an integral part of traditional education. A few points that struck me run closely in parallel with the other great kavi from those regions, क्षेमेन्द्र (we had earlier described his words from the कविकण्ठाभरण on these pages), especially in terms of the description of the attainment of kavitvam. Another point of note that roused my interest early one while reading Ganganath Jha's commentary on the work was the respect the KP had among the later न्यायायिक-s like गदाधर and जगदीश. This is particularly interesting in light of the fact that jayanta भट्ट, an earlier Kashmirian vedicist, had expressed the view that the आस्तीक thought should primarily be modeled after nyAya thought rather than advaita vedAnta. This implied that among the Kashmirian vedicists there was a strand that was primarily affiliated with nyAya, as against vedAnta, which was quite popular among the vedicists of the drAviDa regions. Via the KP we obtain evidence that मम्मट was very much a न्यायायिक in thought.\
In the prose expansion on the first कारिक in the first chapter (उल्लास) in comparing the real world to the imaginations of the poets मम्मट states:\
"[नियति-शक्त्या नियत-रूपा सुख-दुःख-मोह-स्वभावा परमाण्व्-आद्युपादान-कर्मादि-सहकारि-कारण-परतन्त्रा षड्रसा न च हृद्यैव तैः तादृशी ब्रह्मणो निर्मितिर्-निर्माणम्]{style="color:#0000ff;"}"\
The construction constructed by brahman (the world) is controlled by the natural laws, form governed by laws, has the subjective experiential nature of pleasure, pain and illusion, and its existence is the consequence of matter and its interactions, respectively the atomic particles and forces; it has only 6 tastes (rasas) and these too not always of a pleasant nature.\
We find मम्मट expressing a rather deep stream of thought here:

  - He is firstly contrasting the unconstrained nature of poetic expression to the real world, which he described as being governed by natural laws. The word he uses is niyati, i.e. niyati शक्त्या niyata-रुपा, to express the natural laws and the form of the universe developing by natural laws. This word niyati from the ब्राह्मण period has been used to describe the natural laws (e.g. in the कौषितकि/शाखायन ब्राह्मण and the श्वेताश्वतर उपनिषत् e.g. "[कालः स्वभावो नियतिर् यदृcचा भूतानि योनिः पुरुषेति cइन्त्यम् ।]{style="color:#0000ff;"}" SU 1.2).

  - Then मम्मट goes on to describe the basis for the real world: 1) The substantial cause or matter (उपादान) is comprised of the evolutes of the fundamental particles i.e. परमाण्वदि. 2) The causes governing cooperation or interactions (सहकारि-कारण) which are the forces i.e. कर्मादि.

  - Finally he uses a pun to mention that in our sensory sphere we distinguish 6 tastes, in contrast to the 9 rasa-s typical of poetic imagination.\
Thus, we see that the naturalistic theory for the basis of the universe governed by natural laws in the form of fundamental particles and their interactions, so succinctly presented by मम्मट, is clearly that of nyAya-वैशेषिक thought.

Then मम्मट goes on to describe the essentials for being a kavi:\
[शक्तिर्-निपुणता लोक-शास्त्र-काव्याद्यवेक्षणात् ।]{style="color:#0000ff;"}\
[काव्यज्ञ-शिक्षयाभ्यास इति हेतुस्-तदुद्भवे ॥ ३]{style="color:#0000ff;"}

[शक्तिः कवित्वबीजरूपः संस्कार-विशेषः यां विना काव्यं न प्रसरेत् प्रसृतं वा उपहसनीयं स्यात् । लोकस्य स्थावर-जङ्गमात्मक-लोक-वृत्तस्य । शास्त्राणां छन्दो-व्याकरण-अभिधान-कोश-कला-चतुर्वर्ग-गज-तुरग-खड्गादि-लक्षण-ग्रन्थानाम् । काव्यानां च महाकवि-संबन्धिनाम् । आदिग्रहणाद्-इतिहासानां च विमर्शनाद्व्य्-उत्पत्तिः ।]{style="color:#0000ff;"}\
[काव्यं कर्तुं विचारयितुं च ये जानन्ति तद्-उपदेशेन करणे योजने च पौनः-पुन्येन प्रवृत्तिर्-इति । त्रयः समुदिताः न चु व्यस्तास्-तस्य काव्यस्योद्भवे निर्माणे समुल्लासे च हेतुर्-न तु हेतवः ॥]{style="color:#0000ff;"}

The कारिक first summarizes the essentials:\
"Poetic genius, knowledge gained from study of the world, of scholarly works, of poetry, study and practice of the teachings of those well-versed in काव्य; these together compose the basis of poetry."\
Then he goes one to elaborate in prose: "Poetic genius (कवित्वं) is the seed of poetry. This is a peculiar ability, without which there would either be no poetry or, if there were, it would be laughable."\
Like क्षेमेन्द्र, मम्मट describes कवित्वं to be a pre-requisite for काव्य. But apparently unlike the क्षेमेन्द्र he sees this as an innate natural ability that has to be there in the first place -- he does not describe any means of mantra-prayoga here to acquire it (unlike the सरस्वती and bAlA mantras recommended by क्षेमेन्द्र). Apparently unlike क्षेमेन्द्र he also does not see कवित्वं as coming from human effort. मम्मट describes that as a distinct pre-requisite.\
Then he goes on to elaborate on the study of the world and scholarly works:\
"Poetic ability is developed by a study of the world, the organisms and inorganic objects, and the ways of the universe; \[also through the study of] शास्त्र-s, prosody, grammar, thesauri and lexicons, the कला-s, the matters of the 4 puruShArtha-s, texts dealing with study of animals (e.g. horses, elephants etc.) and weapons (swords etc.); \[also by] studying काव्य-s and keeping company of great poets. The word Adi (i.e. its use in the above series) implies the इतिहासस् and also the \[knowledge] emerging from critical examination/discussion of these topics; \[also by] frequently practicing writing poetry under the guidance of those capable of writing and discussing it. The above three (i.e. innate genius, study of the world through observation and scholastic endeavor and practice of writing with guidance) conjointly, not singly, comprise the origin, the formation and brilliance of poetry; they are the source, not sources \[of poetry].

Agreeing with क्षेमेन्द्र, मम्मट sees the kavi as being a naturalist and scholar who acquires knowledge both through new observation of the world as well as by scholastic study. He stresses that genius, study/observation and practice should combine into a single entity for काव्य to shine. Here, it appears that he envisions these abstract entities as almost combining into a single entity -- काव्य, like the atoms of nyAya combining into a single molecule of a substance. Thus, again as in the case of क्षेमेन्द्र we observe that the medieval Hindu kavi maintained continuity in spirit with the kavi-s of the veda, even though the veda was no longer counted along with classical काव्य -- they were not mere versifiers but holders of knowledge.


