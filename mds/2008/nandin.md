
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [nandin](https://manasataramgini.wordpress.com/2008/02/27/nandin/){rel="bookmark"} {#nandin .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 27, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/27/nandin/ "7:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many modern Hindus, especially in colloquial speech, refer to the वाहन of rudra as nandin. This is actually very erroneous. Technically nandin was always the mighty गण of rudra and not the वाहन, which is वृष(bha). Since the vedic period rudra has a वृष्भ वाहन (the white bull; though in some vedic representations he has a white horse). The भूत-गणेश्वर nandin was either bull-faced or ape-faced. His ape-faced form is alluded to in the रामायण (uttara-काण्ड and at least some recensions of the सुन्दरकाण्ड). In the skanda-पुराण (one of the early versions) nandin is termed as कपीन्द्र-vadana. In the सौरपुराण (42.20) he is termed वानरास्य. Throughout the rudra saMhitA of the shiva महापुराण (Venkateshwara Steam Press edition; the rudra saMhitA was the core of the proto-shiva पुराण) nandin is only described as a गणेश्वर and not as a वाहन. In the ajita महातन्त्र of the सैद्धान्तिक tradition his iconography is described thus:

[शैलादिस्तु प्रकर्त्वयस् त्रिनेत्रश् च चतुर्भुजः ।]{style="color:#0000ff;"}\
[जटा-मकुट-संयुक्तः शूलाभय-करान्वितः ॥]{style="color:#0000ff;"}\
[वामे दण्डाक्षमालाभ्यां अलंकृत-करस् तथा ।]{style="color:#0000ff;"}\
[दंष्ट्रा-करालवदनो हरि-वक्त्रोऽथव भवेत् ॥]{style="color:#0000ff;"}AMT 36.349-350\
शैलादि (nandin) should be made with 3 eyes and four arms, wearing a crown of dread-locks with a trident and abhaya pose (right hands). In the left hands he holds a rod and a rosary. He should have fierce face with fangs or that of lion.

In terms of real examples of iconography, the real nandin (not the bull) is depicted as:

 1.  A bull-faced anthropomorphic form (e.g. Madurai मीनाक्षी-sundareshvara temple or the earlier pallava कैलाशनाथ temple)

 2.  An ape-faced form (e.g. pallava rock temple of Kunnattur and chera temple of Kottukal in Kerala)

 3.  An entirely anthropomorphic representation (e.g Darasuram ऐरावतेश्वर temple)

 4.  With a lion face with dread locks in Nepali shiva-परिवार sculptures. This adherence to the AMT prescription mentioned above is interesting for today the AMT tradition survives only in the south where the depictions of nandin do not follow its prescriptions.

 5.  The Kashmirian depictions of maheshvara where nandin appears as the fourth rear face and is typically shown as a fierce figure with a prominent beard. This is laid out in the भूतेश्वर माहात्म्यम् as:\
[शर्व-नन्दि-महाकाल-देवी-वदन-मण्डितम् ।]{style="color:#0000ff;"}\
[भूतेश्वरं भूतपतिं दृष्ट्वा मर्त्यो विमुच्यते ॥]{style="color:#0000ff;"}\
[भूतेश्वर] is depicted with faces of sharva, nandin, महाकाल, and उमा; having seen भूतेश्वर, the lord of the ghosts the mortal is released.


