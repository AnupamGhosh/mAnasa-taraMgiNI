
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [skanda सद्भाव: A distinctive कौमार tantra?](https://manasataramgini.wordpress.com/2008/07/27/skanda-sadbhava-a-distinctive-kaumara-tantra/){rel="bookmark"} {#skanda-सदभव-a-distinctive-कमर-tantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 27, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/27/skanda-sadbhava-a-distinctive-kaumara-tantra/ "4:59 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Even though there are कुमार shrines ranking amongst the richest of the Indian temples, कौमार worship can be generally characterized as being in the decline, especially in terms of the ancient forms. In South India the majority of priests at कौमार shrines are actually सैद्धान्तिक's who have taken over the worship of कुमार within the सैद्धान्तिक system. We have earlier touched upon this phenomenon and shown how the सैद्धान्तिक mantra-s have been inserted and overlaid upon some basic kaumara mantra-s. Another smaller but still notable fraction of the कौमार officiants in South Indian temples are mAdhva-s, who are vaiShNava-s in their personal practice. It is likely that they were originally कौमार-s and were converted to the mAdhva-mata, but retained their role as कौमार officiants. They are organized under some major सुब्रह्मण्य मठ-s and are very secretive about their rituals. However, direct observation suggests that their temple rituals are similar to what mAdhva-s perform with respect to the विष्णु temples in their control. This raises the question as to whether in the कौमार temple system there are any remnants of worship prior to the सैद्धान्तिक and much later mAdhva influence.

The one text that we have found to throw some light on precisely in this regard is the obscure tantra named skanda-सद्भाव (संग्रह). To our knowledge the text has never been published and may currently only influence a minority of कौमार shrines in the drAviDa and andhra countries. It does appear to be one of those major कौमार tantra-s whose core shows minimal सैद्धान्तिक or any other later form of influence. The text, as I have examined, has 18 chapters, and internally states that it is an abbreviation of the larger skanda-सद्भाव:\
The frame of the Tantric narrative is set in कैलास, where nandin asks maheshvara about the rare and pristine शास्त्र named skanda सद्भाव. In reply महादेव says:

[शृणु लोक हितार्थं मे तन्त्रम् एतद् अशेषतः ।\
श्लोकैर् द्वादश साहस्रैर् मुनीनां कथितं पुरा ॥\
संस्क्षेपात् तव वक्ष्यामि स्कन्द-सद्भावं उत्तमम् ।\
अध्याय-अष्ट दशोपेतं स्कन्द-सद्भाव संयुतं ॥]{style="color:#99cc00;"}1.3-1.4 (the manuscript from which I entered this has no numbers of shloka-s; *a very likely emendation of the unmetrical elements in above has been kindly suggested by the learned scholar Somadeva Vasudeva:\
[शृणु लोकहितार्थं मे & तन्त्रम् एतद् अशेषतः /\
श्लोकैर् द्वादशसाहस्रैर् & मुनीनां कथितं पुरा //\
संक्षेपात् तु प्रवक्ष्यामि & स्कन्दसद्भावम् उत्तमम् /\
अध्यायाष्टादशोपेतं स्कन्दसद्भावसंयुतम् //]{style="color:#3366ff;"}* )

Here, shiva in response to nandin's query states that in the interest of the world he would narrate the tantra in totality. He mentions that earlier it was narrated to the muni-s in a form with 12000 shloka-s (a typical Tantric number for the older longer texts), but he is narrating it now in an abbreviated form, the skanda-सद्भाव (SS) in 18 chapters.

Then the text goes on to given an index of the 18 chapters: 1) संग्रह; 2) संभव; 3) मन्त्रोद्धार; 4) archana; 5) अग्निकार्य; 6) दीक्ष; 7) समयाचर; 8) प्रासाद vidhi; 9) अधिवास vidhi; 10) स्थापन; 11) प्रतिमा-लक्षणं; 12) snapana vidhi; 13) utsava vidhi; 14) bali-karma; 15) प्रायश्चित्त; 16) यात्रा; 17) pravesha; 18) समुदाय.\
From this it is clear that it follows the common place pattern of स्थापन tantra-s seen in the सैद्धान्तिक, वैखानस and पाञ्चरात्र realm. While such स्थापन tantra-s are quite stereotypic in subject matter, it is precisely in the realm of स्थापन that सैद्धान्तिक influence has been dominant on the कौमार system. Hence, the skanda-सद्भाव helps us to glean the elements of the कौमार स्थापन system outside of the सैद्धान्तिक sphere.

The second chapter of the skanda-सद्भाव called संभव is of some interest in establishing its affinities and origins. It is a condensed पुराण-like narrative, giving the कुमार-संभवं, which follows the same general frame seen in several Sanskrit पुराण-s and the इतिहास-s. Here, कुमार is said to be born in the himAlaya-s (गौरी-giri) from the tejas (euphemistically for retas) of shiva swallowed by agni and then cast into गङ्गा by him, and then by her into the shara-vana. Here he develops into 6 children who are then suckled by the कृत्त्का-s. Then he has the encounter with indra who is alarmed by this new being. This potentially disastrous encounter is stopped by brahmA and agni and finally skanda is taken by agni to rudra. Then he goes to war against तारक, the great enemy of the gods and kills him in a fierce encounter with his shakti. While the narrative is common place, there are few points to note: 1) It entirely follows the popular account given in Sanskrit tradition and not the Tamil tradition. In the Tamil tradition the main enemy of the deva-s killed by कार्त्तिकेय is शूरपद्म, with तारक merely being an assistant of his. Amidst the सैद्धान्तिक-s of the द्रमिड country who took over the कौमार system this Tamil tradition is widely adopted, even in their entirely Sanskrit liturgy. For example, forms of the shatru-संहार-trishati or the सुब्रह्मण्य भुज्ङ्ग प्रयात stotra, which they use, refer to the killing of शूरपद्म. This silence regarding the slaying of शूरपद्म might place the SS in the realm of the classical Sanskrit पौरानिc tradition. 2) The incident of the encounter with indra: indra seeing him blazing like the fire mercilessly strikes skanda with his vajra. Then he becomes two boys; upon the second strike he becomes 4 boys and upon the 3rd strike he becomes 6 boys. The six of them resembling lighting then wage war on indra. shAkha, विशाख and nejamesha are not mentioned by name and this peculiar series of fission into 2-4-6 is unique to this narrative.

The 3rd chapter gives the कुमार मन्त्रोद्धार (shiva begins by stating: अथः paraM प्रवक्ष्यामि मन्त्रोधार vidhi क्रमं). The manuscript I have examined has some lacunae destroying a few words in this section. Yet, it is complete enough to state that the entire मन्त्रोद्धार does not have any सैद्धान्तिक overlay, such as incorporation of the shaiva पञ्चाक्षरी or the पञ्च-brahma mantra. The main मूल mantra it teaches is the celebrated skanda षडक्षरी mantra that it extracts using the drawing of the मातृक chakra (Shown below). Before the mantra the प्रणव is said to be appended. The mantra is the same as the one provided by the shAradA tilaka tantra (also known to chennas-नारायण nambuthiri-pAD):

[अथः परं प्रवक्ष्यामि मन्त्राणां प्रसरं पुनः ।\
सप्तमस्य चतुर्थं तु तृतीयाद्यम् अथः परं ॥\
पञ्चमे प्रथमं भूयः केवलं स्वर-वर्जितं।\
पुनः षष्ठे चतुर्थम्स्यात् पञ्चम स्वर-संयुतं ॥\
चतुर्थं सप्तमे भूयः स्वरेणैकादशेन तु ।\
पञ्चमान्तं पुनर् दद्यात् षष्टान्तम् स-विसर्गकं ॥\
अयं षडक्षरो मन्त्रः साक्षात् स्कन्दः सनातनः ।]{style="color:#99cc00;"} (SS 3.7-10ab)
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3058/2708727044_31f5463ee7_o.jpg){width="75%"}
```{=latex}
\end{center}
```



Hence (applying sandhi): ||OM vachadbhuve नमः || the skanda षडक्षरी

\[Disclaimer: Though typically a 50 मातृका is used, I am simply illustrating the 51 मातृका form I have inherited in my tradition, because I use it for a number of purposes. The SS merely states अकारदि-क्षकारान्तां]

maheshvara then specifies the bIja-s of the मूर्ति mantra-s and says that they must be conjoined with their respective names. The names are not provided here, but are found in the 4th chapter on archana. Using them we can reconstruct these mantra-s as well as the directions in which these मूर्ति-s are placed in the petals of the कुमार मण्डल:\
saM सनत्कुमाराय नमः || (E)\
स्कं स्कन्दाय नमः || (S)\
baM बाणाय नमः || (W)\
haM हेमचूडाय नमः || (N)\
भं भद्रसेनाय नमः || (NE)\
भं भवपुत्राय नमः || (SE)\
daM देवसेनायै नमः || (SW)\
daM देवयान्यै नमः || (NW)

Then shiva also goes on to give the following auxiliary mantra-s:\
वाहन: maM मयूराय नमः ||\
dhvaja: कं कुक्कुटाय नमः ||\
धूर्तसेन (a major कुमार गण in this system): धं धूर्तसेनाय नमः ||\
vighnesha (the receiver of the निर्माल्य in this system): gUM गणपत्ये नमः ||\
Then shiva provides the uddhara of the second षडक्षरी mantra -- manuscript is somewhat lacunose here but one can easily read the essentials of he uddhara to extract the mantra:\
OM नमः षण्मुखाय ||\
It also gives a distinctive कुमार gAyatrI of the form: OM शक्तिहस्ताय vidmahe षडाननाय dhimahi | tan-नः स्कन्दः prachodayat ||

The 8th chapter on कुमार temples states that they may be constructed in many places but specifically mentions the kadamba forest to be associated with a गुहालय. This connection to the kadamba tree is an ancient one which even mentioned the महाभारत स्कन्दोपाख्यान of the आरण्यपर्वन्. The following emblems of कुमार which are made of gold: मयूर (installed in the East); shakti (South); कुक्कुट (West); vajra (installed in the N); elephant (middle). The loka-pAla-s are installed around the temple in the respective direction and two द्वार-palas at the door posts with fierce appearance, good dress and holding shakti-s and vajra-s. A distinctive feature of this tantra is the installation of vedic ऋशि-s in मण्टप-s at different spots of specified by the squares of the 64-squared वास्तु-मण्डल of the कुमार temple: अञ्गिरस (बृष), भृगु (vitatha), वसिष्ठ (भृङ्ग), atri (सुग्रीव), नारद (वरुण), gotama (शोषण, emended in original from शोष), भरद्वाज (mukhya), agastya (soma) and विश्वामित्र (jayanta). Additionally smaller shrines are built for vighna (निरृति) and shiva-भुताधिपति (roga), a fire-ritual altar (agni) and a store room for flowers (ईशान). A vajra is installed in the gopura facing the gates, a peacockis placed in front of the deity and beyond it is placed the बलिपीठ. There after a copper-crested cock is placed atop a tall dhvaja-stambha.


