
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कौमार material in the अष्टाङ्गसंग्रह: a rambling discussion](https://manasataramgini.wordpress.com/2008/09/08/kaumara-material-in-the-ashtangasamgraha-a-rambling-discussion/){rel="bookmark"} {#कमर-material-in-the-अषटङगसगरह-a-rambling-discussion .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 8, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/08/kaumara-material-in-the-ashtangasamgraha-a-rambling-discussion/ "2:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**The provenance of the अष्टाङ्गसंग्रह**\
Before we go on to a survey of the कौमार material incorporated into the अष्टाङ्ग संग्रह (AS) we shall consider some issues related to the provenance of this text. The अष्टाङ्ग संग्रह is a text in mixed prose and metrical form. There is a closely related text the अष्टाङ्ग हृदय saMhitA (AH) which is in metrical form and an abbreviation of the former. This latter text is the one typically listed as the last of the वृद्ध-trayi. The medical practice along the lines of the AH is a living tradition in India and is maintained to this date in the chera country by certain families known as the अष्टावैद्य lineages. These families have a tradition of daily पारायण of sections of the AH. The AH was also transmitted to the Moslems and Tibetans and is the basis of one of the foundational texts of the medical tradition of the latter. Despite this fame we have little knowledge of the authors of the AH and AS or the time of its composition. Much useful work on this particular topic has been done by two renowned scholars Priyavrat Sharma and G.J. Meulenbeld, which might be looked into for a more scholarly exposition on the topic. I am merely providing here a summary of my own inferences, which are limited by my more cursory study of the entire medical corpus relative to the more detailed study of the कौमार material.

Clearly the AS is the ancestor of the AH because of the presence of two other texts cited by the famed medieval eastern physicians nishchala and शिवदाससेन known as मध्यवाग्भट and स्वल्पवाग्भट. The surviving quotations of the madhya text show that it was also a mixture of prose and verse, but some of the prose text from the AS has become verse in it. This suggests that the AH evolved from the AS through a process of serial versification via madhya and svalpa intermediates. Tradition ascribes the authorship of both AS and AH to वग्भट. However, we have internal evidence for the author only in the case of the AS, where the author says he is वाग्भट, the son of सिंहगुप्त, and the grandson of another वाग्भट a great physician. He says he was a native of the saindhava region (modern Sindh, currently occupied by the Mohammedan terrorist state) and compiled the संग्रह, based on his own study of numerous prior works, and what he learned from his teacher avalokita and more so his own father. Beyond this we have little concrete information about this famous medical author. We can infer that despite the many archaisms he was well after the charaka saMhitA and sushruta saMhitA were compiled, given that he cites portions including दृढबल's redaction of the former and appears to follow the latter even in the uttara tantra (corresponds to uttara स्थान in AH). He was definitely before 1190 CE, the beginning of the era of the extraordinary Hindu polymath हेमाद्रि the prime minister of the यादव kingdom who wrote a commentary on the AH. Given that either the AS or the AH (we simply cannot be sure which one) was probably alluded to by the Chinese traveler I-ching (690s CE) and the Moslem translator ibn Duhn al-Hindi (780-800 CE) some form of the text was present before this period. Priyavrat Sharma correctly notices certain similarities to the बृहत् saMhitA of वराहमिहिर and the AV परिशिष्ट-s and suggests a temporal link. But this is not entirely convincing, especially given that Hindu authors are well-known to use material from much older texts quite freely (e.g. charaka itself in the case of the AS). Texts resembling AH or AS are found in the garuDa पुराण, विष्णु dharmottara and agni पुराण, which, taken together with the evidence for presence of madhya and svalpa versions between the AS and AH, suggest a history of floating texts within the अष्टाङ्ग tradition, undergoing duplication and divergence, with little certainty as to when the original author AS operated.

A more decisive feature that might provide a hint regarding the date of the AS is the presence of multiple bauddha mantra prayoga-s like आर्यावलोकितेश्वर, आर्यापर्णशबरी, आर्यतारा, आर्यापराजिता, भैषज्यगुरु, महामायूरी and तथागतोष्नीष. While some of these deities are seen emerging relatively early in the Common Era, others are attested only a little later\
\[some approximate dates of attestation: avalokiteshvara in सुखावतीव्यूह with amitAbha and महास्थमप्रप्त -200 CE; अमितायुर्-dhyAna sUtra 423 CE in chIna; 500s of CE mantra-s of एकादशानन avalokiteshvara translated into chIna; saddharma-पुण्डरीक sUtra 550 CE; भैषज्यगुरु: Dun-huang 624 CE; भैषज्यगुरु-sutra 457 CE in chIna; आर्यतार\~५००स् CE; महामायूरी \~350 CE; पर्णशबरी \~500s of CE]. Based on this we can provide an approximate window of 300 to 600 CE for the composition of the AS, which ironically matches the time frame of वराहमिहिर suggested by Priyavrat Sharma. Thus, we can say with some certainty that the कौमार material in the AS represents the development of the mantra मार्ग of कौमार शासन before or in this period. It probably represents the condition of the कौमार sect in the yaudheya-gupta-वाकाटक era during which the worship of skanda was very prevalent. Though given the overlap with the mantra-s from sushruta it may also include some material from an earlier phase.

**A possible link of AS to atharvavedic tradition**\
The AS and AH show an interesting mélange of आस्तीक and bauddha elements occurring side-by-side. This has led to the major debate as to whether वाग्भट was an आस्तीक or a nआस्तीक. We have वग्भट's own testimony that his teacher was avalokita and this name makes it almost certain that his teacher was a bauddha. We also observed from वराहमिहिर and विष्णुधर्मोत्तर that the आस्तीक-s in that period were pretty eclectic and incorporated bauddha elements within their framework. This could explain वाग्भट's bauddha elements, though given his repeated reference need for reverence towards the veda-s, ब्राह्मण-s and वैदीक devata-s it is certain that he was dvija himself. As Priyavrat Sharma notices, he seems to have a certain affinity to AV-परिशिष्ट-s. This is reinforced by the fact that he declares Ayurveda to be an upaveda of the AV. In the first chapter of the उत्तरस्थान he mentions that a ब्राह्मण practicing the atharvaveda should perform shantikarman for 10 days upon child birth. The features circumstantial support वाग्भट's alliance with the AV tradition. The कौमार rituals of the AS are associated with certain archaic mantra-s that appear to be of late vedic provenance. In light of the above of we suspect that AS preserves and draws in part from mantra-s found in lost परिशिष्ट-s of alternative AV traditions (perhaps पैप्पलाद as against the predominantly AV shaunaka tradition that survives in large part in the extant परिशिष्ठ-s). Given the presence of the skanda याग in the extant परिशिष्ट-s, it is not unreasonable to suspect that the beginnings of the कौमार tantra were in such AV परिशिष्ट-s. This is especially so given that they also contain elements of other proto-tAntric mantra-शास्त्र systems like Asuri kalpa and the उच्छुष्म kalpa. In this context it should be noted that even the kAshyapa tantra, which incorporates considerable कौमार material is derived from the original work of a भार्गव of the clan of ऋचीक. One of the chapters containing कौमार material in the kAshyapa tantra is the famed रेवती kalpa, which also contains archaic language reminiscent of the late vedic layer rather than the classical period. An important point to note is that large parts of the AS or the kAshyapa tantra do not have the archaisms of mantra-s or language associated with the sections enriched in कौमार material \[though the kAshyapa tantra in general appears to be a bit more archaic than AS]. Thus, it is unlikely that this is an intentional imitation of an archaic style but actual preservation of an old layer of कौमार material \[In the case of the kAshyapa saMhitA it might even be a late lost ब्राह्मण].

**An outline of the कौमार material in the AS**\
The कौमार material in the AS that of our interest is found in the उत्तरस्थान, wherein chapters 1-6 cover pediatrics and chapters 7-8 भूतविद्या. Chapter 1 mentions a rite performed to agni and skanda in the 4th month after birth, when the child leaves the birthing chamber; this is not mentioned in any of the other texts. Beyond this the main material of interest is found in chapters 3-6. Chapter 3 (bAla-graha-विज्ञानीय) provides the story of how the graha-s emerged. In the long past days, rudra had generated 5 male and 6 female graha-s as the guardians of skanda. The male graha-s are: 1) skanda himself, 2) विशाख, 3) नैगमेष, 4) shvan, 5) पितॄ. The female graha-s are: 1) शकुनी, 2) पूतना, 3) शीतपूतना, 4) अन्धपूतना, 5) मुखमण्डितिका, 6) रेवती, and 7) suSkaरेवती. After skanda had attained maturity and became the leader of the forces of the deva-s, rudra as the graha-s to infect children in the house of parents who refuse to observe rituals to the deva-s and पितृ-s. There are described as assuming the form of bears, owls or cats and can only be seen with the knowledge of the mantra-शास्त्र as they infect children:\
*[आविशन्तश्च लक्ष्यन्ते केवलं शास्त्र-चक्षुष शुद्धेन देहं बालानां गन्धर्व इव योषितां ॥]{style="color:#99cc00;"}*\
Their entry into children, even as the gandharva seizes a maiden, can only be seen by means of the pure eye of the शास्त्र \[remember the vedic allusion of the gandharva seizing the daughter and wife of पतञ्जल काप्य].\
Chapter 4 describes the creation of a yantra on birch bark and tying it with the pratisara mantra which invokes indra, yama, kubera, वरुण, rudra, skanda, brahmA and the transfunctional goddess under many names. This is followed by a description of an associated बलिहरण rite. Then the description of a distinctive मण्डल for invoking कुमार as भूतपति accompanied by the graha-s as his परिवार-s along with sapta मातृ-s is provided. Then the satya-vachas mantra for pacification of कुमार graha-s is performed along with a bali offering. Then the celebrated kula-विद्या mantra for the worship and pacification of the 13 skanda-दूती-s in conjunction with षण्मुख is described. Then the अग्निदण्ड mantra along with prognostication using it is described.\
Chapter 5 Has the famous कील and snapana rite in the kaumara context similar to that in the रेवती kalpa of the kAshyapa tradition. While the rite in general resembles the rite of bathing in the AV परिशिष्ट 42, it includes a distinctive fire offering to skanda, agni and the कृत्तिका-s. It also describes the preparation of a medicinal bath for children during this ritual.\
Chapter 6 has a description of the rituals, mantra-s, drugs to be prepared and their deployment for each of the 12 graha-s described in chapter 3.

**Survey of particular prayoga-s**\
***The मण्डल rite AS uttara-स्थान 4.41-49***\
[*अथऽऽपतित गोवर्cअः प्रलिप्ते दर्भ संसृते ।\
वृत्ते वा चतुरस्रे वा मण्डले कुसुमोज्वले ॥\
नाना-ग्रह-परिवारं भिषग् भूतपतिं लिखेत् ।\
तं प्रति प्राङ्मुखो विद्यां पठन्-नुपहरेत् बलिम् ॥\
namo विमारकस्य \[perhaps corrupt; manuscripts need to be examined] |\
नमः कुमाराङ्गाय ।\
नमः सप्तानां मातॄणां ।\
नमः स्कन्दाय ।\
एष परिग्रह मन्त्रः ।\
मातॄणां आवाहनं च।\
हिलि हिलि निमापटलिनि स्वाहा ।\
अनेन सप्त-कृत्वः परिजपितेन पानीय चुलुकेनात्माभ्युक्षयितव्यः । एवं आत्मरक्षा कृता भवति ।*]{style="color:#99cc00;"}

The place is plastered with cow dung and strewn over with darbha straw. Then the मण्डल is drawn either of a square or a circular form and decorated with flowers. The physician then writes भूतपति in the middle with the many surrounding graha retinues. Then facing east the bali is offered with the provided mantra-s to कुमार and his hordes and the seven mothers. The seven goddess are invoked with a formula which is reminiscent of the formula in the offering to the goddess मातङ्गी in the kAshyapa tantra's रेवती kalpa. The mantra is muttered seven times and water is taken in cupped palms and sprinkled on oneself/the child. With that one protects oneself/the child. After the offering of the bali mantra is recited known as satya साधन.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3114/2847681320_7266275231_m.jpg){width="75%"}
```{=latex}
\end{center}
```



*इन्द्राणी, one of the 7 मातृ-s with a bAla (kindly provided by श्री Sarvesh Tiwari)*

An important feature of this rite is the connection of कुमार with seven mothers. This connection is seen in the early narratives of the कौमार tale, both in the vana-parvan and shalya-parvan of the महाभारत (e.g. the reference to seven मातृ-s: kAkI, हलिमा, रुद्रा, बृहली, AryA, पलाला and mitrA in the former and sapta मातृगणश् chaiva ... in the latter). Iconographically this depiction appears prominently in the gupta era cave temples in Madhya Pradesh where the seven goddesses come with a large image of कुमार. In the earlier Mathura temple ruins we find remnants of the 7 देवी-s, like इन्द्राणी holding bAla-s, in the vicinity of the old कौमार shrines. Interestingly, a mention of this association also appears in the inscription of Talagunda inscription of king मयूरशर्मन् the kadamba, where mentions कुमार and the sapta मातृ-s bearing him aid to defeat his pallava enemies. It is also seen in the damaged Bihar Stone Pillar inscription of skandagupta. Several early चालुक्य inscriptions mention how कुमार and the sapta मातृ-s protects their clan. Thus, it appears that the famous seven mothers who later as the 8 mothers become a central feature of the shaiva and shAkta tantric systems had their beginnings in this early layer of the phase 2 of कौमार मन्त्रमार्ग (see essay on बालग्रह-s for the phases). Eventually, विनायक was to displace कुमार as the companion of the seven mothers. The seven mothers have been inherited by कुमार from his father agni, who right from the RV is described as a child nursed by 7 mothers (RV 1.141.2; RV 3.1.6; RV 9.102.4, RV 10.5.5). Even the RV, agni is sometimes termed guha in the सूक्त-s mentioning these mothers -- an epithet that was later inherited by कुमार. Interestingly, right from RV 1.191.14 these seven sister goddess are invoked along with 7 peacocks to counter toxins.

***The skanda कुलविद्या mantra AS uttara-स्थान 4.50-60***\
After performance of the above मण्डल ritual and recitation of the सत्यसाधन mantra, the kula-विद्या mantra is prescribed:\
[ *tअतश्चानु पठेदेनां कुलविद्यां समाहितः ।\
औला नीला खला काला नकुला ग्रहकारिका ॥\
सेना सुसेना माता च महासेना यशस्विनी ।\
कृतमालाक्षतशिरा एतास्तु ग्रह-मातरः ॥\
कुमार-वचनाच्-छीघ्रं प्रमुञ्चन्तु शिशुन्त्विमम् ।\
काल-कल्पा ज्वलच्-छक्तिः किङ्किणी-जाल-भूषिता ॥\
कुमार-वचनाद्-दूती सम्प्राप्ता मुच्यतां शिशुः ।\
पताका कुकुक्टः छत्रं घण्टा-बर्हधरः शिखी ॥\
शरस्तम्भश्-च ते मुद्रा मुच्यतां मातरः शिशुः ।\
गन्धं धूपं च माल्यं च कुमारस्ते प्रयच्छति ॥\
न स्थातव्यं तदाग्राय स्वां गतिं गच्छ मा चिरं ।\
धात्र्यः सर्वाः कुमारस्य सङ्गता गन्धमादने ॥\
रक्त-ध्वज-पताकस्य रक्तांशुक-निवासिनः ।\
कुमारस्यालयं तत्र यात बालं विमुञ्चत ॥\
षण्मुखो द्वादशाक्षो वै तस्मिन् काले वृतो वरं ।\
वीरः कुमाराधिपतिस्तव पुत्रोऽभिषिच्यतां ॥\
ब्राह्मणो ब्राह्मणानान्तु सर्व-वेद-विदां वरः ।\
यक्ष-राक्षस-नागानां गन्धर्वासुरयोर् अपि ॥\
पिषाच-भूत-सङ्घानां व्यदीनां च स ईश्वरः ।\
आज्ञापयति वः स्कन्दो गंयतां गन्धमादनं ॥\
ग्रहो यः कोपयेदाज्ञां कुमारस्य यशस्विनः ।\
स्थानात् च्युतः स वै क्षिप्रं बह्य परिषदो भवेत् ॥*]{style="color:#99cc00;"}

A bali is offered with this mantra followed by the kindling of agni and a homa with various items (flowers, food and blood?) with 108 samidhs of khadira or chandana. This mantra known as the kula विद्या invokes the 13 दूति-s of skanda to release the child and proceed to the regions of gandhamadana under the orders of कुमार. It is perhaps one of the earliest uses of the term kula in the context of a mantra of a family or cluster of goddesses: here they are 13 graha-मातृ-s namely 1) aula; 2) नीला; 3) खला; 4) काला; 5) nakulA; 6) ग्रहकारिका; 7) सेना 8) सुसेना 9) mAtar 10) महासेना 11) यशस्विनी 12) कृतमाला 13) क्षतशिरा. We believe that the use of the term kula for a family of देवी-s is was directly transmitted from such systems of the early कौमार mantra मार्ग to the early classical tantric kaula system. Evidence in this regard comes from the kaula-ज्ञान-निर्णय (KJN) of the illustrious siddha of the kaliyuga, मत्स्येन्द्रनाथ. The KJN appears precede the other great kula systems that were to emerge like those of कुब्जिका, काली, त्रिपुरा and trika. In the KJN, the kula is the family of the 64 yogini-s, who are arranged 1 per petal, on the 8 petals of the 8 chakra-s of this system. Importantly this kula of yogini-s is supposed to rove in the world in various avimorphic and theriomorphic states (KJN23.2-5ab; emended partially by me from PC Bagchi's edition):

  - [मर्त्येऽस्मिन् देवतानान्तु संचारं शृणु भामिनि ।]{style="color:#99cc00;"}\
[कपोतिका तथा गृध्री हंसी चैव नखी तथा ॥\
खञ्जःखेटा भाSई tu कोकाभाSई tu sundari |\
उलुकी पेछकी वा तु सारसी वल्गुली तथा ॥\
शृगाली अजा महिषी उष्ट्री मार्जारी नकुली तथा ।\
व्याघ्री हस्ती मयूरी छ कुकुटी न ...[ब्रेअक्] ॥\
अन्यानि यानि रूपाणि संस्थितानि महीतले ।]{style="color:#99cc00;"}*\
These forms like that of a dove, vulture, goose, eagle, wagtail, pheasant, bustard, owl, nightjar, crane, bat, jackal, goat, buffalo, camel, cat, mongoose, tiger elephant, peacock and rooster are typical of the zoomorphic मातृ-s of skanda named in large numbers in the कुमारोपाख्यान of the shalya parvan of the महाभारत. Further, it is precisely with some of these names that the forms of रेवती are described in the archaic कौमार material in the magico-medical kAshyapa tantra. Finally we find the skanda himself the center of the कुलविद्या of the AS is subsumed under the name वटुक (the boy) in the KJN. This is one of the earliest texts where the वटुक is explicitly described as 6-headed (albeit in an ungrammatical sandhi, "aisha language", marked below) KJN 16.52:

  - [अहं त्वं च विशालाक्षि चन्द्र-द्वीप-समागतौ ।\
[*षट्-मुखो] वटुको जाताः क्षेत्रपाल-कुलागमे ॥]{style="color:#99cc00;"}*\
This is further clarified in bali mantra of वटुक given in the KJN:\
[*ह्रीं वटुकाय कपिल-जटाय पिङ्गल-नेत्राय देवी-पुत्राय मातृ-पुत्राय\
इमां बलिं ममोपनीतां गृह्ण गृह्ण चुरु मुरु ह्रीं ॥*]{style="color:#99cc00;"}\
Here वटुक is explicitly called देवीपुत्र and मतृपुत्र. This is again confirmed by another statement in the same chapter of the KJN where वटुक is said to be कार्त्तिकेय (KJN 16.27):

  - [अहं छैव त्वया सार्द्धं छन्द्र-द्विपं गतो यदा ।\
तदा वटुकरूपेण कार्तिकेयः समागतः ॥]{style="color:#99cc00;"}*

Thus, in addition to the sapta मातृ-s, we can state that the kaula system's family or cluster of yogini-s also emerged from the matrix of the early कौमार शासन.

***The rite of lustration***\
The bath rite performed on the child and his mother/nurse in the AS is comparable to the रोहिनी स्नान described in the रेवती kalpa of the kAshyapa tantra (more generally it follows the pattern of the bath in the AV tradition, परिशिष्ठ 42 and the पुष्य bath of वराहमिहिर, बृहत्संहिता 47). However, here several parvan days under different stars are offered: 1) रेवती 2) श्रवण 3)स्वाती 4) रोहिणी 5)मृगशीर्ष 6) पुष्य 7) मूल. In the AS the authority is described as Atreya. It may be done in a cowshed, elephant shed, confluence of rivers, cross-roads, or a cemetery. Plants of different types including दूर्वा grass are used. A मण्डल with 3 colors and 4 doors in the form of a square is constructed and decorated with banners and spread with दूर्व and barley shoots. In the four cardinal directions of the मण्डल (E-S-W-N) the deities indra, yama, वरुण and kubera are inscribed along with their weapons and वाहन-s. Beyond yama's post, the place of the मातृ is installed and nandikeshvara is stationed along with a bull. A mud image of विनायक is also installed. Beyond the station of वरुण, the 1000 spoked (सहस्रार) chakra, the weapon of विष्णु is installed. Between the station of kubera and indra, श्री is installed with a lotus. The remaining अष्ट-मङ्गल symbols are then installed in order: the नन्द्यावर्त, अङ्कुष, fish, svastika, शङ्ख and श्रीवत्स. In the interstitial directions the परिवार of indra (NE), yama (SE), वरुण (SW) and kubera (NW) are stationed. Then the lambini group of राक्षसी-s are installed in the मण्डल thus: लम्बोदरी, लम्बभुजा and लम्बकर्णी are installed at the gate of indra; pralambini at the gate of yama; लम्बकेशी at the door of वरुण; लम्बनासिका at the door of kubera. Then 6 कील-s (stakes) of iron, 7 made of 3 other metals, and two of khadira wood are made and sprinkled with sandal water and transfixed into the ground all around the मण्डल. 8, 16 or 32 pitchers of different colors marked with svastika-s are filled with pure water and various medicinal plants, and placed symmetrically at the 4 doors of the मण्डल. Also added to the pots depending on the direction are gold pieces, pearls, seeds of herbs, and mustard and rice. Pure soil from various places such as temples, elephant stables, horse stables, mountain peaks, crossroads, termite mounds, river banks, courtesan's dwellings, king's dwelling, secret passages, sea shores should be brought along with kindled fire, samidh-s of latex-bearing plants, garlands, perfumes. Honey, ghee, rice, fried rice, five colored rice dishes and sesame readied for oblations.

The child and the mother/nurse are made to sit down in the central lotus figure of the मण्डल on an audumbara or पालश पीठ strewn with green darbha grass. Then the fire is installed in the northern quarter of the मण्डल by the physician and he begins making oblations to skanda, agni and the कृत्तिक-s with the mantra-s:\
[*अग्नये स्वाहा । कृत्तिकाभ्यश्-च स्वाहा । स्वाहा । नमः स्कन्दाय देवाय ग्रहाधिपतये नमः ।\
शिरसा त्वाभिवन्देहं प्रतिगृह्णीष्व मे बलिं ।\
नीरुजो निर्विकारश्च शिशुर्-भवतु सर्वदा । स्वाहा ॥*]{style="color:#99cc00;"}

After these oblations are completed with the different oblation materials the physician clothed in white, having observed the requisite fast of 3 nights and having controlled his senses begins the lustration with the soil, herbs and the water from the pitchers. The pitchers from each door are consecrated with mantra-s to the respective deities (indra, yama, वरुण and kubera) and then oblations are made to these gods and the child is bathed. Then a series of oblations are made to prajApati, rudra, कुमार and the योगिनी-s along with the calling of various कुमार graha-s, भूत-s and other agents. The oblations to कुमार are offered with the formula:\
[नमो भगवते कुमाराय पिलि पिलि खिल्लि खिल्लि खिणि खिणि स्वाहा ॥]{style="color:#99cc00;"}\
The yogini-s are offered oblations with the formula:\
*[नमो भगवतीभ्यो महायोगीश्वरीभ्यो निमि-निमि मेनु-मेनु तुरु-तुरु स्वाहा ॥]{style="color:#99cc00;"}*

After the rite the child should not eat meat for 5 days and should avoid excesses for the same period. While the rite confers freedom from disease, its performance without adherence to the विधी kills the physician and the child.

The rite of lustration as seen in the AS appears to have been adapted from the AV-परिशिष्ठ tradition for a kaumara ritual. The similarly adapted bath, i.e. the रोहिणी bath, in the रेवती kalpa of the kAshyapa tantra also includes a rite for the installation of images of कुमार, षष्ठी and विशाख made of gold, silver, root or धूर्व grass in front of the sacrificial fire. Then they are offered oblations of ghee in the fire. Given that the रेवती kalpa is a linguistically ancient text it is likely that this bath ritual was adapted early in the development of the कुमार शासन. The AV-rite was also adapted by वारहमिहिर for his rite of the पुष्य bath, but here the context is an auspicious bath for the king. In the case of the पुष्य bath of the बृहत् saMhitA too skanda and विशाख are deities among the deities stationed in the मण्डल:\
[*ग्रहांश् च सर्व-नक्शत्रै रुद्रांश् cअ सह मातृभिः ।\
स्कन्दं विष्णुं विशाखं च लोकपालान् सुरस्त्रियः ॥*]{style="color:#99cc00;"}\
However, beyond that there is not specific rite involving कुमार, and several other deities receive oblations down the line. Hence, the version of वराहमिहिर most probably followed a trajectory independent of the कौमार system.

The mention of the योगिनी-s in the AS version and मातङ्गी in the kAshyapa saMhitA do add additional support to the old link between the कौमार system and the goddess who were to become elements of the kaula systems. But, another interesting point seen in the snapana ritual of the AS appears to have been linked to the development of कुमार in the nआस्तीक tradition as वज्रकुमार. In the AS rite 15 कील-s or stakes are transfixed into the ground. In the nआस्तीक tradition वज्रकुमार is often known by an alternative name as वज्रकील and is depicted as a stake or a vajra combined with a stake. The reason for this connection between वज्रकुमार, who is otherwise a rather straight forward nआस्तीक adaptation of कुमार, and the stake was previously largely mysterious to most students of these matters. However, in light of the distinctive use of कील-s in the above मण्डल it is quite likely that वज्रकुमार's alias वज्रकील is a remnant of this ancient feature of a कौमार ritual.


