
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The महासावित्री and the महानारायन उपनिषद् of the atharvan-s.](https://manasataramgini.wordpress.com/2008/09/16/the-mahasavitri-and-the-mahanarayana-upanishad-of-the-atharvan-s/){rel="bookmark"} {#the-महसवतर-and-the-महनरयन-उपनषद-of-the-atharvan-s. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 16, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/16/the-mahasavitri-and-the-mahanarayana-upanishad-of-the-atharvan-s/ "3:48 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The महानार्यानोपनिषद् is a text of considerable importance because it provides the shruti प्रमाण for several basic rituals of dvija-s like the संध्योपासन, त्रिसुपर्ण ritual, शिवोपासन using the पञ्चब्रह्म mantra-s, shaucha rites, कमोर्कार्षीत् इत्यादि japa, प्राणाग्निहोत्र, परिसेषण, the daily vaishvadeva offerings and the final विरजा homa with the teaching of renunciation. It is the only genuine vedic उपनिषद् that is peculiar in belonging to two veda-s -- the तैत्तिरीय कृष्ण yajurveda as the final section of the तैत्तिरीय AraNyaka (TA 10) and to the atharvaveda. The family tradition amongst the atharvan-s holds that it belongs to the lost AraNyaka of the atharvaveda that was cited by शङ्कर भगवत्पाद, the famed अद्वैताचर्य. This lost AraNyaka was most probably associated with either the पैप्पलाद shAkha or the original shaunaka as some mantra-s in it resemble the पैप्पलाद text as against the vulgate which is commonly known as the shaunaka. The AV recension, while similar to the TA 10 version, has several differences that help in establishing the archaic core of this text. I have a handwritten transcript with intonations, while an intonation-less version, with some errors, was published by Colonel George Jacob with नारायण's notes (Jacob was an interesting guy, who lived not far my childhood home. Though he was an English warrior who fought our armies during the great struggle, and the son and son-in-law of accursed Isaist subversionists, he turned pro-Hindu and started pressing on the English government to preserve the literary traditions of "the old shAstrI's -- those living encyclopedias of learning".).

One important mantra used by the atharvan-s, called the महासावित्री, is the mantra with the oblations are made during the first उपाकर्म and in subsequent recitation during ब्रह्मयज्ञ. The source for this mantra is the अथर्ववेदीय MNU:\
*[ॐ भूः । ॐ भुवः । ॐ स्वः । ॐ महः । ॐ जनः । ॐ तपः । ॐ सत्यं । ॐ तत्-सवितुर् वरेण्यं भर्गो देवस्य धीमही । धियो यो नः प्रचोदयात् । ओमापो-ज्योती-रसोऽमृतं ब्रह्म भूर्-भुवस्-स्वरों । ॐ भूर्-भुवः-सुवर्-महर्-जनस्-तपः सत्यं मधु क्षरन्ति । तद्-ब्रह्म तद्-आप आपो ज्योती-रसो। अमृतं ब्रह्म भूर्-भुवस्-स्वरों। ॐ तद्-ब्रह्म । ॐ तद्-वायुः । ॐ तद्-आत्मा । ॐ तत्-सर्वं । ॐ तत्-पुरों नमः ॥]{style="color:#99cc00;"}*

The AV version of the उपनिषद् also contains the नारायण सूक्त which gives the text its name. The AV form of this chant clarifies a few otherwise minor points that are constant topics of debate amongst south Indian ब्राह्मण-s: 1) The सूक्त in the AV form also ends with the mantra "ऋतं सत्यं...", which suggests that this mantra was present in the ancestral form of the text. This corresponds to the oral तैत्तिरीय text of the ब्राह्मण-s from the Andhra and drAviDa countries as against the printed MNU from the TA, which often elides this mantra. Thus, the oral text is most probably preserving the original form. Secondly, in the final part of the chant there is a version prevalent in South India that lists the trinity brahma, shiva and hari and then indra (sa brahma sa शिवः sa हरिः sendra ...). It has been alleged that this inclusion of hari is a recent interpolation by shaiva-s who want to establish the नारायण here as the puruSha, rather than विष्णु. It is countered that the vaiShNava-s do not include sa हरिः in their version of the recitation and that the inclusion of sa हरिः breaks the otherwise perfect अनुष्टुभ् meter. The AV version of the MNU lacks sa हरिः strongly suggesting that it is indeed a shaiva-inspired late interpolation into the TA version of the text. As an example of how the उपनिषद् is recited by the atharvan-s I provide a MP3 version of this सूक्त:\
[अथर्ववेदीय नारायण सूक्त MP3](https://app.box.com/s/65xmbh2ojgiexd0ldnxb)

Finally, we will discuss some aspects of textual evolution and some parallels with molecular evolution using the two MNUs as a case study.\
Continued ...


