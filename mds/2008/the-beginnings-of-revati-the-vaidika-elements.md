
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The beginnings of रेवती: The vaidika elements](https://manasataramgini.wordpress.com/2008/12/04/the-beginnings-of-revati-the-vaidika-elements/){rel="bookmark"} {#the-beginnings-of-रवत-the-vaidika-elements .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/04/the-beginnings-of-revati-the-vaidika-elements/ "7:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[*पथ्या रेवतीर् बहुधा विरूपाः सर्वाः सङ्गत्य वरिवस् ते अक्रन् ।\
तास् त्वा सर्वाः संविदाना ह्वयन्तु दशमीम् उग्रः सुमना वशेह ॥*]{style="color:#99cc00;"}*A*V 6.4.7; AV-P 3.1.7\
The रेवती-s of the path of several diverse forms, have all assembled and made extensive wealth for you;\
Let them all in agreement call you, reside here with good mind and strength to the tenth decade of life.

[स्वस्ति पथ्ये रेवति ।]{style="color:#99cc00;"}RV5.51.14b\
May रेवती of the path confer safety on us.

In the kAshyapa saMhitA/tantra we see रेवती emerging as a major goddess associated with skanda and with several distinctive features. This raises the question of her earlier evolution -- was she from a "para-vedic" Aryan system (i.e. Indo-Aryan, Iranian or a lost branch not contributing to the vedic saMhitA-s or ब्राह्मण-s), as we argued for कुमार, (i.e. marut-s as "para-skanda")? Or was she directly drawn from the vedic stream? All indications are that she was a well-known but less-hymned deity in the vedic saMhitA-s. Scattered mantra-s to her are found in the RV, the AV-saMhitA-s, mantra-प्रष्न-s of yajurveda and the mantra-ब्राह्मण of the सामवेद. She is invoked and made oblations in certain key shrauta and गृह्य rituals consistently across all vedic traditions.

Main rite involving रेवती in the shrauta cycle is प्रायनीयेष्टी that is performed after the यजमान has attained दीक्ष. It involves 5 oblations, which in ऋग्वेदिc and shukla yajurvedic tradition are (e.g. कौशीतकि ब्राह्मण 7.6-7.8; aitareya ब्राह्मण 2.1-5; shatapatha ब्राह्मण 3.2.3): In the north to रेवती, called as पथ्या svasti, in the east an oblation of ghee is made to agni, in the south to soma, in the west to savitar and then a charu is offered to aditi, who rules over zenith. In the concluding उदयनीयेष्टी he makes similar offerings, however beginning with agni, then soma, then savitar, then रेवती and finally aditi. Then an oblation is made to the marut-s. The यजमान should make the याज्य verses of the प्रायनीयेष्टी into the पुरोनुवाक्य verses of the उदनीयेष्टी and vice versa else he is liable to be killed. In the कृष्ण yajurvedic form of the ritual the offering of पथ्या स्वस्ती is associated with the East, agni with the south, soma with the west, savitar with the north and aditi with the zenith (तैत्तिरीय saMhitA 6.1.5). In this case the recommended mantra-s used in the invocation and offering to रेवती are RV 10.63.15-16. A similar oblation to रेवती in the प्रायनीयेष्टी is also recorded in the atharva vedic shrauta tradition in the वैतान सूtra (vai.सू 13.2) but uses the above AV mantra for रेवती. The AV-vulgate 6.4.7 for रेवती is also deployed राजन् in the राजकर्मन् related to installation of the राजन् on the throne.

Another notable shrauta oblation made to रेवती was in the human sacrifice -- the पुरुषमेध ritual. In the ashvamedha before the horse is sent off to wander for an year an oblation is performed to पूषन् पथिकृत् to protect the horse. In the पुरुषमेध, in place of this पूषन् ritual, one is performed to the 3 देवी-s, अनुमती, रेवती and aditi. This is recorded in both ऋग्वेदिc and atharvavedic tradition (शाङ्खायन shrauta 16.10.11; वैतान shrauta सूtra 37.10.20).

In the गृह्य tradition of the सामवेद a ritual to रेवती and other goddesses is recorded in the context of the श्रवण and the आग्रहायणी sacrifices (gobhila गृह्यसूत्र 4.8). Here after making the main offerings of fried rice in household fire the ritualist leaves a reminder of rice for the further oblations to the goddesses. He goes out of his village in the eastern or northern direction and at a four-cross (चतुष्पथ) he installs a स्थण्डिल and kindles the fire. Then he makes oblations with the reminder of the rice with the mantra from the mantra ब्राह्मण (2.6.2-5) to राका, सिनीवाली and रेवती. The विष्णु स्मृति, which is derived from the kaTha tradition of the कृष्ण yajurveda mentions a rite for the pacification of the goddess रेवती, which is performed every month when the moon is in the asterism of रेवती (vi.स्मॄ 90.26). Here ब्राह्मण-s are feed with a sweet pudding and the pleasing of रेवती confers beauty on the ritualist.

In the core vedic period we note a connection between पुषन्, the ancient deity of the paths, and रेवती called पथ्या or the goddess of the paths. This connection plays out in many contexts in overt and some times subtle ways. Earlier we noted how in the पुरुषमेध the इष्टि to पुषा is replaced by that to the 3 goddess including रेवती. In the recitation for the नक्षत्रेष्टि from तैत्तिरीय ब्राह्मण (3.1.27) we note that the constellation named रेवती is associated with पुषन् and they are called upon to protect the paths, animals and food:\
*[पुषा रेवत्य् अन्वेति पन्थां । पुष्टिपती पशुपा वाजबस्त्यौ । इमानि हव्या प्रयता जुषाणा । सुगैर् नो यानैरुपयातां यज्ञं । क्षुद्रान् पशून् रक्षतु रेवती नः । गावो नो अश्वाग़्ं अन्वेतु पूषा । अन्नग़्ं रक्षन्तौ बहुधा विरूपं । वाजग़्ं सनुतां यजमानाय यज्ञम् ॥]{style="color:#99cc00;"}*\
In the atharvavedic chant of the gods' wives recited by the अग्नीध्र, रेवती as पथ्या is mentioned as the wife of पुषन् (gopatha ब्राह्मण 2.2.9):\
[प्र्^इथिव्य् अग्नेः पत्नि । वग् वतस्य पत्नि । सेनेन्द्रस्य पत्नि । धेन ब्र्^इहस्पतेह् पत्नि । *पथ्य पुस्ह्नह् पत्नि* । गयत्रि पस्हुनम् पत्नि । त्रिस्ह्तुब् रुद्रनम् पत्नि । जगत्य् अदित्यनम् पत्नि । अनुस्ह्तुम् मित्रस्य पत्नि । विरद् वरुनस्य पत्नि । प\~न्क्तिर् विस्ह्नोह् पत्नि । दिक्स्ह सोमस्य रज्\~नह् पत्नि ।|]{style="color:#99cc00;"}[\
]{style="color:#99cc00;"} The astronomical connection between पूषन् and रेवती suggested by the नक्षत्रेष्टि ritual might in some ways mirror the later connection between skanda and रेवती (i.e. the source of the connection is astronomical -- the constellations depicting skanda, नेजमेष and रेवती lie next to each other in the [sky.](http://photos1.blogger.com/photoInclude/x/blogger2/6438/855/1600/418809/नक्षत्र_nyAsa1.png) [ ](http://photos1.blogger.com/photoInclude/x/blogger2/6438/855/1600/418809/नक्षत्र_nyAsa1.png)Even in the AV रेवती and कृत्तिका are mentioned together). This astronomical connection seems to persists in later times where we see a goddess with pair of fish throughout India depicted in terracottas and in the शुङ्ग and chera coins. This pair of fish stands for the राशी of matysa which corresponds to the नक्षत्र रेवती. Thus, the goddess might be cautiously identified with रेवती. Thus But there are some very subtle astronomical references relating पूषन् which I tentatively and speculatively point to as the beginning of the connection with कुमार. These are found in the RV itself in the mantra-s of मेधातिथि काण्व (RV 1.23.13-15):

  - [आ पूषञ् चित्रबर्हिषम् आघृणे धरुणं दिवः । आजा नष्टं यथा पशुम् ॥\
पूषा राजानम् आघृणिर् अपगूळ्हं गुहा हितम् । अविन्दच् चित्रबर्हिषम् ॥\
उतो स मह्यम् इन्दुभिः षड् युक्तान् अनुसेषिधत् । गोभिर् यवं न चर्कृषत् ॥]{style="color:#99cc00;"}*\
While the complete interpretation of these ऋक्-s is a story in itself one may ponder upon the words: चित्रबर्हिष्, guhA and ShaD युक्तं.


