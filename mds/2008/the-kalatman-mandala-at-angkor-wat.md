
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कालात्मन् मण्डल at Angkor Wat](https://manasataramgini.wordpress.com/2008/02/08/the-kalatman-mandala-at-angkor-wat/){rel="bookmark"} {#the-कलतमन-मणडल-at-angkor-wat .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 8, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/08/the-kalatman-mandala-at-angkor-wat/ "7:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp0.blogger.com/_hjuA1bE0hBw/R65rFKfEbEI/AAAAAAAAACU/15bQeMsDM24/s320/कालयाग.png){width="75%"}
```{=latex}
\end{center}
```



The basic geometry of the कालात्मक yantra

The inscription records of Angkor Wat by the French archaeologists points to a rare tantric prayoga of the siddhAnta srotas, whose performance appears to barely survive today in the drAviDA country (perhaps until relatively recently in Nepal). The inscription in question is from the period of the 1300s apparently from the period of the reign of indrajayavarman. The relevant statement from the inscription is:

"tasmin kuru mahad यागं यथोक्तं पारमेश्वरे ... कृत्वान् sa mahad यागं काल-यागं iti श्रुतं | सरस्वती याग युतं loka-pAla-समावृतं ||"

It describes a great याग which has been laid down in the siddhAnta tantra known as the पारमेश्वर Agama. This great याग is known as काल-यागं. In its performance the mantra-वादिन् is supposed to include the यागा of सरस्वती and surround it by that of the loka-pAla-s. So what is this rite being described by the Angkor Wat inscription? An examination of the ritual prayoga-s of the Chidambaram दीक्षित-s reveals that it refers to the prayoga of the exalted व्योमव्यापिन् mantra. This mantra as per the कामिकागम is used in the final step of the दीक्ष into the siddhAnta srotas along with the मातृका mantra-s signifying the circle of सरस्वती before it. However, the ritual in question here is the one laid out by the 9th पटल of the guhya section of the archaic निश्वास tantra. It is also found in the now partially lost पारमेश्वर tantra, confirming the statement in the Angkor inscription. It is also alluded to in the स्वायम्भुव sUtra-s as the form of महादेव as कालात्मा (the version edited by Pierre-Sylvain Filliozat may not contain the original reading) commented upon by the great आचार्य sadyojyotis. The निश्वास guhya described the याग as the worship of shiva as "संवत्सर-शरीरिणः" or with his body as the year. The rite was connected with the construction of the कालात्मन् मण्डल, which is being implied implicitly by the word याग in the Angkor inscription (the याग being the fire rite accompanying the installation and worship of the मण्डल). The basic geometry of the मन्दला has a dodecagonal plan with 12 spokes leading to the outer rim with petals (circles). On each of these 12 spokes and petals 30 अक्षर-s of the व्योमव्यापिन् mantra were laid out (=360). The remaining 8 अक्षर of the व्योमव्यापिन् were laid on the inner octagonal nave. This was surrounded by a decagonal rim on which were laid, 5 per side, the 50 मातृक-s composing the अक्षर-स्वरूप of सरस्वती (this is clearly mentioned in the पारमेश्वर). Outside of it the 8 direction was a भूपुर with loka-pAla stationed therein starting with indra. The याग of महादेव in the form of the कालात्मा (embodied in this मण्डल) involved the ritual to the central shiva with the व्योमव्यापिन् mantra arranged as a manifestation of the year, to sarasvati in the surrounding मातृक chakra and to the loka-pAla-s in their आवरण.

This construction of the मण्डल is different from the नवनाभ मण्डल also constructed based on the व्योव्यापिन् mantra. The latter is described by the great Kashmirian tantric भट्ट रांअकण्ठ deshika (रामकण्ठ-II) in his composition known as the व्योमव्यापिन् stava. The medieval ब्राह्मण वेदज्ञ (commenting on रामकण्ठ, and following trilochana-shiva) from टिरुवाटुतुरै in the Tamil country states that this yantra was first promulgated by the भार्गव ऋषि ruru (Hence, his collection form the tantra of the Urdhva srotas known as raurava) and the व्योमव्यापिन् mantra is supposed to have emerged from the ईशान face of shiva. The features of the नवनाभ yantra are derived based on the kalottara and मतङ्गपरमेशवर texts. This मण्डल has a nonagonal symmetry with 40 अक्षर-s in each of the 9 sectors and 8 arranged in a central nave. The कालात्मन् envisages shiva as encompassing time and the नवनाभ envisages the deva as encompassing space.

These conceptions of shiva express a continuity with the vedic tradition where rudra is invoked at the end of the piling of the altar in the agnichayana. In the yajur vedic rite rudra is invoked as encompassing space as the 4 cardinal directions and the vertical axis and time in the form of the 5 संवत्सर-s of the vedic 5 year cycle. Amongst these rudra is said to stand like a fierce tiger. Likewise, the mention of the काल मण्डल in the Cambodian inscription, which arranges the व्योमव्यापिन् to correspond to the 12*30 day months of the संवत्सर, reminds one of the astronomical constants embedded in the main temple of Angkor Wat. For example, the outline of the base-plan of the upper elevation of the temple has 12 projections and the sum of the lengths of the N-S and E-W axes is 365.37 Cambodian cubits (described in Eleanor Mannikka's work). While, the main temple of Angkor Wat is one dedicated to विष्णु, the basic ideas of मण्डल geometry inhere to both siddhAnta and पाञ्चरात्र temple constructions. After all the mantra and the yantra are the deity.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp1.blogger.com/_hjuA1bE0hBw/R65rFafEbFI/AAAAAAAAACc/WRgH8uudER4/s320/ANGKOR.jpg){width="75%"}
```{=latex}
\end{center}
```



The Angkor Wat upper elevation


