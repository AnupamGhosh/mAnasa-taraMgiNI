
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mysterious कुचुमार](https://manasataramgini.wordpress.com/2008/06/30/the-mysterious-kuchumara/){rel="bookmark"} {#the-mysterious-कचमर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 30, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/30/the-mysterious-kuchumara/ "4:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A modern Hindu might be deeply offended to learn about कुचुमार. He is a mysterious fellow mentioned in the कामसूत्र by वात्स्यायन as a teacher of the औपनिषदिक section (i.e. the 7th division of the कामशास्त्र) of the version of the कामशास्त्र composed by बाभ्रव्य पाञ्चाल. Some say that this actually meant that he was the promulgator of the secret sexual teachings -- on aphrodisiacs (वाजिकरण) and seduction (वशीकरण). Others suspect that it may be teaching like that found in the कामशास्त्र section of the बृहदारण्यक उपनिषद्. But did this authority ever remain in the memory of Hindu tradition beyond the कामशास्त्र? The answer to this lies in a text termed the कुचिमार-tantra (कुचुमार-tantra) that has remained unpublished. We had the opportunity to quickly study the contents of this tantra which was in the possession of the village school teacher -- we unfortunately have no idea what its current fate is.

The work has 8 पटल-s, some of which show a few vulgar प्रकृत् (देशी) nouns while describing certain recipes. The main focus of the initial section is on increasing the size and firmness of लिङ्ग-s in puruSha-s (termed indrivardhana) and the kucha-s and श्रोणि-s in स्त्री-s. Some preparations might incidentally also increase size hands and ears (?). Then there is description of lubricants to increase pleasure for स्त्री and puman while engaged in maithuna. Then स्त्री-वशीकरण is described using mantra-s combined with चूर्ण-s, which are slyly dusted on the target, and rasa-s. Interestingly, the mantra-s are drawn both from the पाञ्चरात्र tradition as well as that of कपालीश bhairava of the brahma-यामल. Then recipes for वाजिकरण reminiscent of those in charaka's work are seen. Then there are descriptions of preparations for delaying the emission of retas during maithuna and stimulating the स्त्री. Then there is a description of preparations and vidhi-s for the tightening of the yoni in स्त्री-s. Then the text describes contraceptives and preparations to cause quick abortion. Finally, there is section on fertility and birthing. It may be noted that the ancient Hindu might have preferred the absence of loman in the upastha because multiple preparations are given for loma-नाशन or loma-शातन in स्त्री-s. In my quick survey I did not find the *Mylabris* beetle in the कुचुमार-tAntra (which is mentioned by the KS as being used by the drAviDa-s; the cantharidin producing beetle which in my youth was still abundantly seen in the कर्णाट country), but it shares the *Vernonia* species with the KS.

It is possible that it preserves an old tradition of कुचुमार with an overlay of tantric material drawn eclectically from different Agamic streams.


