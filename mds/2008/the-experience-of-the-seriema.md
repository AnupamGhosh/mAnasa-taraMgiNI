
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The experience of the seriema](https://manasataramgini.wordpress.com/2008/07/05/the-experience-of-the-seriema/){rel="bookmark"} {#the-experience-of-the-seriema .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 5, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/05/the-experience-of-the-seriema/ "6:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2008/07/seriema1.jpg){width="75%"}
```{=latex}
\end{center}
```

\
I had recently taken my clansmen to the zoo where I came face to face with the seriema. It was a tremendous experience. For years we had only heard of this bird -- the last representative of a great radiation of extraordinary birds from the Americas. We made eye contact with the seriema -- we felt like being in a special presence. For the first time we felt we were seeing all the long gone glorious phorusrhacids in flesh and blood through the vision of that seriema before us. For a student of evolution some organisms truly produce what is closest to the experience of brahman as Hindus would say. R told me that she felt like using almost the identical language when she saw the seriema many years before me (I think she said it was like the experience of the khechari विद्या -- in time I guess).\
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2008/07/phorusrhacids1.jpg){width="75%"}
```{=latex}
\end{center}
```

\
Phorusrhacids as per Alvarenga and Hoefling


