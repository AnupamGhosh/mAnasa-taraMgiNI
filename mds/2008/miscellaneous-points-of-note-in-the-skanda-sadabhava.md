
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Miscellaneous points of note in the skanda-सदभाव](https://manasataramgini.wordpress.com/2008/07/31/miscellaneous-points-of-note-in-the-skanda-sadabhava/){rel="bookmark"} {#miscellaneous-points-of-note-in-the-skanda-सदभव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 31, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/31/miscellaneous-points-of-note-in-the-skanda-sadabhava/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**कौमार दीक्ष:** The SS is one of the few surviving texts giving an account of the कौमार दीक्ष. In its general pattern it follows the दीक्ष-s seen in the पाञ्चरात्र and सैद्धान्तिक systems. In a pure spot a मण्टप is erected with 4 doors and a fire altar is installed on a shukla पञ्चमि. The मण्टप is anointed with पञ्चगव्य and paddy is spread on the floor. The rite occurs after a clean bath and by wearing clean clothes. Water is then sprinkled by hand from a pitcher, and the pitcher then filled up and is taken to the NE and installed on a spread of paddy. There the guru invokes कुमार using the मूल mantra into the pitcher and worships the deva with flowers and the like. In the middle in the fire altar the fire rite to कुमार is performed. Then to the west of the altar a charu is cooked with rice and milk. The guru then cuts the charu into 4 equal portions. One portion is offered as a naivedya to the deity, the second portion is offered as an oblation into the sacrificial fire with the मूल mantra, the third portion is offered as a bali and the fourth portion is eaten by the student and the guru. Then they perform Achamanam and the guru gives the student a fresh toothbrush. Then they observe where the toothbrush falls -- if it falls in the indra, वरुण, soma or इशान directions then the initiation is unsuccessful and a प्रायश्cइत्त is performed with oblations of the मूल mantra. If it falls in the other directions then the initiation continues. The student sleeps in the मण्टप with his head facing east. \[The next day] the आचार्य asks the student his dreams. If he has beheld kings, ब्राह्मण-s, young women, a milk pail, flowers and fruits, a tree with fruits, cows or fire then the initiation is a success. The guru then draws the कौमार मण्डल and beginning the initiation into the कौमार ritual.The initiation is available for all 4 वर्ण-s and is done for each वर्ण in each of the 4 main seasons starting in spring beginning with the ब्राह्मण. Spring may also be used for the remaining वर्ण-s.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3097/2721966612_3c61582a41_o.png){width="75%"}
```{=latex}
\end{center}
```



**The instructions for constructing the कौमार मण्डल at the time of initiation and the ensuing rite of initiation are thus:**\
It should be made of one, two or four spans in length. One desiring prosperity should never make it raised. It is made with strings and colored sand. First square is made and divided into 16 equal squares. In the central 4 squares a circle is inscribe. In the circle a lotus is made. This lotus is trisected by 3 filaments or kesara-s. The angles between them are further trisected by 3 more kesaras. In the center of the lotus a pericarp is made in the form of a circle \[in tradition one usually places a bindu in the middle though it is not mentioned in the text of the SS]. Then one inscribes a circle in the big square. Inside it touching the sides is constructed a six-petalled lotus figure. Outside 4 doors are made with the doors determined by the size of the lotus. The pericarp and filaments are painted yellow. The मूल-matra is written in the pericarp. The lotus petals are painted blue. The intervening spaces are then painted smoky color. The grid lines are colored red with sand. The 4 bounding lines of the grid are painted yellow, white, red and black. Thus, is constructed the कौमार मण्डल that confers all siddhi-s.

The मण्डल is constructed on a पीठ by the guru and skanda is invoked into it. The god is worshiped with flowers, scents, incense, charu and भक्षण-s. On the 4 margins in the 4 directions water pitchers with flower wreaths are placed. Flower garlands are draped around and free flowers are scattered all over. Then the fire oblations are made in the कुण्ड. The calm student is then led blindfolded with a flower in hand (specific for the respective वर्ण-s?). Then the student is tapped with a bundle of darbha grass, sprinkled with water and honorably led forward. Then he is touched on the head, endowed with the गुणस्, and taken to the west door and asked to circumambulate in प्रदक्षिण order and let the flower fall on its own. Depending on where the flower a [दीक्ष] name is chosen for him. Then the blindfold is removed and he is allowed to see the मण्टप and worships silently. Then he performs प्रदक्षिण and नमस्करं and is raised by the आचार्य. Then he is initiated into the procedures of the fire rituals with the कौमार मूल mantra-s. Finally the student pays the आचार्य his fees. This ritual in its basic form is close to the tantric initiations seen in the सैद्धान्तिक and पञ्चरात्र systems with the कालात्मन् and चक्राब्ज मण्डल-s, the मूल mantra-s of those systems like व्योमव्यापिन् and mantra-rAja and the generation of a new दीक्षित identity based on the flower falling.

**Installation of the कुमाराग्नि:** The sacrificial fire is consecrated as the कुमाराग्नि as per the tradition of the skanda-सद्भाव. The general procedure follows the तन्त्रीक-agnimukha which is common to all तान्त्रीक traditions. The allowed samidh-s are those of वैकङ्कत palasha and khadira. The oblations may be made of dry or cooked rice, sesame, cakes (अपूप), coarsely ground meal, lotuses, fruits, and दूर्व and darbha grasses.

**Deities receiving bali as per skanda-सद्भाव:** 1)lokapAla-s; vasu-s; rudra-s; Aditya-s; rivers; indra पार्षद-s; loka-मातृ-s; अष्ट-मातृका-s; skanda graha-s and diverse graha-s; वेताल-s; विनायक; virabhadra; श्याम and shabala; shAstA and his पार्षद-s; वीरभद्र; ananta-nAga; गण-s of the following groups: 1) kumuda with the kumuda-s, शङ्कुकर्ण, पुण्डरीक, वामन, sarvanetra, sumukha; 2) धूर्तसेन and 1008 skanda troops; 3) शूरसेन, ugrasena, shikhisena, jayasena, dyumatsena, चण्डसेन, कुन्डासेन, वीरसेन, पिङ्गल and skandasena.


