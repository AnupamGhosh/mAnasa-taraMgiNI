
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Moonlight revelry, moonlight suffering and मिट्ठु](https://manasataramgini.wordpress.com/2008/10/15/moonlight-revelry-moonlight-suffering-and-mitthu/){rel="bookmark"} {#moonlight-revelry-moonlight-suffering-and-मटठ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 15, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/15/moonlight-revelry-moonlight-suffering-and-mitthu/ "6:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

My पिताश्री reminded that me that many moons ago on this particular full moon we had lazed with our friends and clansmen on the terrace drinking flavored milky hoping that शिवाशङ्करौ would pass over head asking if we were awake. Today, on this full moon night we were passing through the dark alley, with the white disc above our head streaming the nectarine beams. One corner of the mind was filled with suffering of the grip of grahin, shadowed incessantly by the आततायिन्, and the terrifying memory of the मारण prayoga that had rattled us before the आततायिन् probed with a needle. The joys of the milk-drinking sessions on the terrace seemed to be a distant, nay, a dream. We indeed wished for सोमारुद्रा to bear us aid. Before leaving we read a couple of posts by the learned scholar SDV on the work of मिट्ठु (I felt a bit of a coincidence here). Some time back, in response to a question by SRA and also that jewel of the स्त्री-varga I had looked up मिट्ठु's हंसविलास for extracting a mantra-shloka to महादेव. R told me that they called a shloka cited by मिट्ठु to be the सदाशिव stuti and is recited to worship him after the invocations with the प्रासाद and/or चिन्तामणि mantra-s. This मिट्ठु was an interesting character---he was a learned सामवेदिन् स्मार्त from the लाट country. As SDV is lucidly translating [मिट्ठु's autobiographical verses](http://sarasvatam.blogspot.com/){target="_self"} on his site, I will say no more on that (SDV feels there might be some similarity between the नीलाम्बरस् mentioned by भट्ट jayanta, the nyAya philosopher, and मिठ्ठु's system. Of course one should be very cautious of any such relationship as the former are rather obscure, and linkage of rather diverse courses such as स्मार्त, संगीत and काम in मिठ्ठु's system was not out of character amongst the medieval तान्त्रिक-s with a स्मार्त background. The rAsa part was perhaps more extreme and might remind some of the नीलाम्बर-s, but reading भट्टतिरि of the chera country perhaps this was not unknown either).

I learned of him in a peculiar way. We had just a post or two ago alluded to GK, who happened to be a successor of the famed तान्त्रिक लक्ष्मण राणडे, a coastal ब्राह्मण of the महराट्ट country. It was GK who told us of a grotesque shrine owned by some Gujjus and asked us to visit it during our journey to the गोदावरी, कपालीश and tryambaka on the day after दीपावली in 1990. We were then boarding in a Gujju rest house of gargantuan proportions, attached to a peculiar mall-like temple (the image of कुमार there was hidden behind an enclave so that women might not see him even by mistake and there was a guard posted there to ensure that their curiosity did not get better of them.). We were having hell of time, also known as काकक्रीडा :-), with the riotous, mirth-loving people of the लाट country for a good part of the day -- fire-works, ghee-dripping food and more. But मातापितरौ, detached from such revelry of ours were seized by the pious instinct of needing to visit some shrines. Our hosts were able to give directions to reach that shrine mentioned by GK. It was a neo-tantric shrine and its स्थापक-s were either successors or biological descendents of मिट्ठु of हंसविलास fame who practiced a syncretism of श्रीविद्या with the savitrI. Of course the लाट श्रीकुल tradition is of considerable importance from a parochial viewpoint because of its transmission amongst a stream of my coethnics. In the drAviDa country there are many different traditions of श्रीविद्या surviving and generally secret. One of the highly influential lineages is that of the illustrious भास्करराय मखीन्द्र or भासुरानन्दनाथ. He acquired his पूर्ण-दीक्ष in षोडषी and beyond from shivadatta shukla of the लाट country. He was also known for his emphasis of the sAvitrI-पञ्चदशी syncretism and was also called a वामाचरिन् by those more oriented towards the traditions preserved by the शङ्कर मठ. The हम्सविलास's author मिट्ठु appears to belong to a similar tradition.

At an earlier time we were experiencing the bhoga of the कामिका, we hence turned to those verses cited by मिट्ठु like:

  - [भगस्य मैथुने देवि यत् सुखं मम जायते ।\
na tat सुखं japair homair na dAnais-तपसा.pi वा ||]{style="color:#99cc00;"}*(of course it is much longer प्रशंस of maithuna that might not be appreciated by a prudish modern Ayatollah)

But now we looked at the sky -- there was neither the milky beverage in our hand nor the दूती in our embrace; only the pain of the ग्राहिन्'s grip. The pains of naraka seemed closer than the pleasure of 9 types of chumbana or 8 types of आलिङ्गन. We had become a renunciate. Will the last 6 arrows be needed to save us?


