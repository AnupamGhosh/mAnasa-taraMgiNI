
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [samara-संस्पर्श](https://manasataramgini.wordpress.com/2008/04/09/samara-samsparsha/){rel="bookmark"} {#samara-ससपरश .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 9, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/09/samara-samsparsha/ "2:41 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Just as we were about to deploy रक्षोह-agni the कुलुञ्च action of the ari destroyed our prayoga. We were briefly surprised but took another route via शूलिनी and indra-वज्रबाहु. Then realizing that the ari would launch a mid-night attack we deployed कर्ण-पिशाचिनी. The first कर्ण-पिशाचिनी prayoga went through. The second one, which was more important appeared to first go through but was smashed by the ari counter-attack. So we then risked an attack and the 3rd kp prayoga was deployed. This appeared to go through. Then there was an audacious pre-dawn attack. Kp was called upon to feed even this time. Managing the thing was stressful though. Time alone would tell if they really worked. ari-s returned to attempt the stambhana and मारण; at least temporarily we averted them.


