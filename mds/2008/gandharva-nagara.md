
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [gandharva nagara](https://manasataramgini.wordpress.com/2008/12/01/gandharva-nagara/){rel="bookmark"} {#gandharva-nagara .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 1, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/01/gandharva-nagara/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ST said that she had returned from South America. She had gone there in the quest of Ananda even as ekanetra had found his own Ananda right in his own क्षेत्र. We began by reminiscing how in the year before us both left the shores of भारतवर्ष we had spent a riotous mirthful night in the stinky city near some of the spots where the तुरुष्क-s had recently rampaged. Both ST and me having seen the buffalo-rider from close quarters wondered how "सहस्रं अयुतं pAshA मृत्यो मर्त्याय hantave". Feeling the presence of the pAsha that drags one, even as the सारमेयौ are harshly barking, I used the word buffalo-rider. ST told me how in her peregrinations in Peru of all places she was repeatedly listening to a Hindi song that had a visual of a buffalo-rider. She played it on the phone and it sounded positively vulgar. But for pretty obvious reasons it triggered memories of my friend across the pond in कामसेतु. I made some remark. ST said verily you may take the journey into the gandharva nagara: "First locate the paNDita. Then find my image in the mirror. Follow my image. You will find the drAviDA named "impropriety". Follow that one. You travel deep into the gandharva nagara as though it were real. Then having gone down endless paths in the gandharva nagara you will seek the real thing. You will glimpse unbelievable objects of pleasure but your attempts to enjoy them will be tempered by the fear of death. The drive for the experience will overcome your fear. But you will know not at the end whether you have been seized by chitragupta or not."

We reached out to the object of pleasure as prajApati had done in the days of yore. But the sight of audumbara sent a shiver down our spine. We retreated backwards and found ourselves trapped in the gandharva nagara --- in the world between the living and the dead not knowing which way we may come out.

ST said while we live we could talk anyhow. She asked if the truffle and the morel were abhojya. On this matter we adopt the principle of strict interpretation. I have seen citation in a स्मार्त compendium from the brahma पुराण (never located it actually) which said that the fungi like the globular kukunda, the चैत्याकर and the छत्राकर were born of the daitya. Now manu has stated that the chatraka is prohibited and मेधातिथि expands that the kavaka is included but implies that the kukunda is not included in this. The other texts also leave out the चैत्याकर fungi. Thus, the morel which is the चैत्याकर and the truffle which is the kukunda escape the ban and so I suggest that they are bhojya.


