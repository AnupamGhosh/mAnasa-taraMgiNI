
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Construction of the geya-chakra](https://manasataramgini.wordpress.com/2008/01/12/construction-of-the-geya-chakra/){rel="bookmark"} {#construction-of-the-geya-chakra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 12, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/01/12/construction-of-the-geya-chakra/ "2:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/bp2.blogger.com/_hjuA1bE0hBw/R4guWtBuc0I/AAAAAAAAAB0/cL7OpgM6TV0/s320/geya_chakra.png){width="75%"}
```{=latex}
\end{center}
```

\
[The geya-chakra of संगीत-योगिनी]{style="font-style:italic;"}

Very few good specimens of the geya-chakra of श्री-विद्या are currently available. There are a few historical yantras of श्यामला available and they are often inaccurately drawn. However, I happened to obtain access to see some pristine forms that were prepared in वाराणसि and nepAl. These versions clearly indicate the symmetries and the geometric ideas involved. As per the tradition of the mantra-mahodadhi of the medieval tantric mahidhara, we also have evidence that the yantra of सङ्गीत-योगिनी was congruent to that of मातङ्गी in the dasha-महाविद्या series.

Like the श्री-chakra the geya-chakra encodes the Golden ratio rather extensively, even if more directly: It has an innermost golden triangle of the श्रीचक्र type which makes contact with the inner pentagon of a pentagonal star polygon. This star polygon of course encodes the Golden ratio in the construction of its sides with respect to the pentagon in which it is inscribed. The pentacle in turn is inscribed within a circle which in turn is bounded by an अSठ-dala padma and a षोडश-dala padma. They are all enclosed within a square भूपूर. The entire construction as shown above can be achieved using a compass and straight-edge. Such a construction was performed using C.a.R. to generate this ideal representation of the yantra.


