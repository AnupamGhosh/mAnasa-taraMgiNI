
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Strange memories of German poems](https://manasataramgini.wordpress.com/2008/07/23/strange-memories-of-german-poems/){rel="bookmark"} {#strange-memories-of-german-poems .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 23, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/23/strange-memories-of-german-poems/ "10:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R was pretty excited at having procured the कप्फिण-abhyudaya for only \$5 and stated her plan to work through it over the next several months as a part of her program to stave of mental aging and keep her astronomical IQ intact. Her mention of this suddenly reminded me of our Acharya's free-form commentary on the same. Commenting on the famous battle scene he described it as an example of the supposed आग्नेयास्त्र prayoga by the तथागत in defense of the इक्ष्वाकु-s against the invading कप्फिण. He actually saw it as a reason to believe that indra-जाल and astra prayoga were an intrinsic aspect of the तथागत's ways to spread his dharma. At least going by the कप्फिण-abhyudaya this seemed likely. I heard R say something to Marc in German. I was reminded of something she told me shortly after दशान्त, may be a few days after the cracker box यक्षिनी story: How I was studying the language of these awful म्लेच्छ-s, while she was laboring over deva-भाष, claiming to understand the wonderful shloka-s that महाडम्बर was composing to entertain us :-). With some glee I conveyed it to Marc, much to his amusement. But later we compared notes on respective courses of study: she was able pursue her साधन more deeply due to her laboring on deva-भाष. But what did we get from our laboring on the म्लेच्छ-भाष -- well, we gained some understanding of the evolutionary trends in Indo-European. But in the process some poems stuck in our intra-cranial landscape for unknown reasons and we were reminded of them due to a quotation by Marc. Both the muni and I, for inexplicable reasons, were enormously impressed by Heinrich Heine, in particular by "Die Grenadiere". For some reason "Babylonische Sorgen" which we found in पिताश्री's German text book was also hovering in our head. It has a weird reference to विनायक: "Wie Gott Ganesa in Hindostan..."


