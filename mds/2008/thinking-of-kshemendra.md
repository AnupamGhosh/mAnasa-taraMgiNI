
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Thinking of क्षेमेन्द्र](https://manasataramgini.wordpress.com/2008/02/13/thinking-of-kshemendra/){rel="bookmark"} {#thinking-of-कषमनदर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 13, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/13/thinking-of-kshemendra/ "7:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The deva-भाष is the language of the kavI-s right from the days of the hoary vipra-s from the misty past of the ऋग्वेद saMhitA. In the galaxy of literature in the deva-वाणी, which is filled with extraordinary luminaries, क्षेमेन्द्र from the crest of bhArata shines like Rigel on a clear winter night. He comes from that time, which produced a striking constellation of encyclopedic writers, just at the twilight of the Hindu sun. There was abhinavagupta the foremost of the tantrics of his era, king bhojadeva-परमार and क्षेमेन्द्र. क्षेमेन्द्र represents the primeval spirit of the kavI, similar to that of the ऋग्वेदिc ऋषि-s.

On विष्णु:\
[अशेष-विश्व-वैचित्र्य-रचनारुचये नमः ।\
माया-गहन-गूडाय नानारूपाय विष्णवे ॥]{style="color:#99cc00;"} (दशावतार स्तुतिः 1)\
On kalki:\
[स्वच्छन्द-प्रोच्छलन्-म्लेच्छ-तिमिरोद्भेद-सच्-छविः ।\
कर्कि-विष्णुः प्रकाशाय प्रभातार्क इवास्तु वः ॥]{style="color:#99cc00;"} (DS 12)

This from his work on the incarnations of विष्णु from the period when he appeared to have became oriented towards the पाञ्चरात्र tantra-s.

From his earlier work the kavi-कण्ठाभरण we note that he was a practitioner of श्री-विद्या as he mentions the bAlA mantra and yantra. He says that poetry might come as a result of mantra siddhi (divya) or due to human effort and scholarly study. The route via siddhi is described as requiring the worship of sarasvati in the form of the मातृका chakra and deploying the bAlA mantra with the worship of the 3 associated देवी-s.\
KK1.6-14:\
[ॐ स्वस्त्यङ्कं स्तुमः सिद्धमन्तराद्यम्-इतीप्सितं ।\
उद्यदूर्ज-प्रद्रं देव्या ऱ्‌^ॠऌॡ-निगहनं ॥]{style="color:#99cc00;"}

[एकम्-ऐश्वर्य-सम्युक्तम्-ओजो-वर्धनम्-औषधं ।\
अन्तरान्तः कला-खण्ड-गलद्-धन-सुधाङ्कितं ॥]{style="color:#99cc00;"}

[चन्द्रोच्छलज्-जलं प्रोज्-झद-ज्ञानं टठ-संयुतं ।\
डम्बर-प्रौढ-किरण-तथतां दधदुन्नतं ॥]{style="color:#99cc00;"}

[परं फलप्रदं बद्ध-मूलोद्भव-मयं वपुः ।\
रम्यं लघुवरम् शर्म वर्षत् सर्वसहाक्षरं ॥]{style="color:#99cc00;"}

[एतां नमः सरस्वत्यई यः क्रिया-मातृकां जपेत् ।\
क्षेमम्-ऐन्द्रं स लभते भवोऽभिनव-वाग्भवं ॥]{style="color:#99cc00;"}

[श्वेतां सरस्वतीं मुर्ध्नि चन्द्र-मण्डल-मध्यगां ।\
अक्षराभरणा ध्यायेद् वाङ्मयामृतवर्षिणीं ॥]{style="color:#99cc00;"}

[त्रिकोण-युग-मध्ये तु तडित्-तुल्यां प्रमोदिनीं ।\
स्वर्ग-मार्गोद्गतां ध्ययेत् परमाम्-अमृतवाहिनीं ॥]{style="color:#99cc00;"}

[निर्विकारां निराकारां शक्तिं ध्यायेत् परात्परां ।\
एष बीज-त्रयी वाच्या त्रयी वाक्-काम-मुक्ति-सूः ॥]{style="color:#99cc00;"}

[काव्य-क्रियेच्-छाङ्कुर-मूल-भुमिं-अन्विष्य विश्रान्ति-लवेन मोक्षः ।\
अन्यावधाने मदनस्य मोक्षस्-तृतीय-बीजे सकलेऽस्ति मोक्षः ॥]{style="color:#99cc00;"}

By "त्रिकोण-yuga" क्षेमेन्द्र is alluding to the bAlA yantra with interlocked triangles. He specifies the bAlA mantra as "[एष बीज-त्रयी वाच्या त्रयी वाक्-काम-मुक्ति-सूः]{style="color:#99cc00;"}" i.e. [ऐं क्लीं सौः ॥]{style="color:#99cc00;"}\
In क्षेमेन्द्र's scheme the three देवी-s associated with the three divisions of the bAlA mantra are the lighting-like प्रमोदिनी, the moon-like अमृतवाहिनी and the formless shakti परात्परा. This formulation with the 3 देवी-s appears to be a unique one that does not appear to survive in the extant bAlA deployments associated with the कादिमत. A parallel is, however, seen in the form of the triad of bAlA mantra-s known as the tri-हृल्लेखा-s:\
[ह्रीं ह्रीं ह्रीं प्रौढ-त्रिपुरे आरोग्यम्-ऐश्वर्यं देहि स्वाहा ।\
ह्रीं श्रीं क्लीं त्रिपुरा मदने सर्वं शुभं साधय स्वाहा ।\
ह्रीं श्रीं क्लीं परात्परे त्रिपुरे सर्वेप्सितं साधय स्वाहा ।]{style="color:#99cc00;"}\
However, the idea of bAlA as a deity of wisdom continues in extant कादिमत tradition.

Finally, we come to क्षेमेन्द्र's recommendation for the aspiring poet who wishes to achieve kavitva through human effort. This famous statement has been quoted numerous times by many people (like Andrew Schelling's translation of Sanskrit poetry) but let me do it again in the original संस्कृत.

KK 2.10-11\
[आलोक पत्र-लेख्यादौ गोष्ठी-प्रहसनज्ञता ।\
प्रेक्षा प्राणि-स्वभवानां समुद्र-अद्रिस्थितीक्षणं ॥]{style="color:#99cc00;"}

He \[the aspiring kavi] should examine the form of leaves and their veins, know how to make people laugh, study the behavior of living organisms and observe the features of oceans and mountains.

[रवीन्दु-तारा-कलनं सर्वर्तु-परि-भावनं ।\
जन-संघाभिगमनं देश-भाषोपजीवनं ॥]{style="color:#99cc00;"}

The motion of the sun, moon and stars and all the seasons he should closely contemplate on. He should move among different peoples and states and \[examine] their languages and occupations.

In KK 2.2-23 we find an entire gamut of recommendations for attainment of kavitvam. These start with the worship of विनायक and the याग to सरस्वती and untiring effort to acquire the ability of discernment. They include, among other things, being in the company of great kavi-s and intellectual Arya-s and through them imbibing (क्षेमेन्द्र uses the Sanskrit word "chewing" or "tasting" like food) the meaning of great काव्य:\
[सहवासः कवि-वरईर्-महाकाव्यअर्थ-छर्वणं ।\
आर्यत्वं सुजनैर्-मैत्री सौमनस्यं सुवेषता ॥]{style="color:#99cc00;"}\
He also mentions observing the skills of sculptors, warriors in battle, graveyards and forests, and hearing the sorrowful lamentations:\
[शिल्पिनां कौशल-प्रेक्ष वीर-युद्धावलोचनं ।\
शोक-प्रलाप-श्रवणं श्मशानारण्य दर्शनं ॥]{style="color:#99cc00;"}

Thus, in क्षेमेन्द्र we see the eternal naturalist, linguist and sociologist who is immersed in the study of the world around him. It after all encapsulates what it has always meant for one to be a kavI from the days of the RV -- i.e. the philosophy of the kavI as a naturalist and observer. It is not without reason that क्षेमेन्द्र wants the aspiring kavI to be a scholar of astronomy, medicine, linguistics, कामशास्त्र and other things -- after all even the ऋग् vedic vipra-s say "[कवयो मनीषाः]{style="color:#99cc00;"}". It defines structure of the knowledge system which is followed by the erudite Hindu -- many of the contradictions or paradoxes that haunt the Western mind are instead in harmony here.


