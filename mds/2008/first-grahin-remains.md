
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [First ग्राहिन् remains](https://manasataramgini.wordpress.com/2008/05/05/first-grahin-remains/){rel="bookmark"} {#first-गरहन-remains .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 5, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/05/first-grahin-remains/ "5:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The first ग्राहिन् pursued us like the gandharva pursuing the भरद्वाज marked for destruction by the kAshyapa. It was spreading terror much like that ग्राहिन् sent against us in the same context last year around the same time. That one was killed with the haridra prayoga which was like the sword of विष्णु. But this one seated in the secret realm was evading our prayoga despite having been targeted.


