
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Retrospections on the curtain-raiser to the long past Great War 1](https://manasataramgini.wordpress.com/2008/02/13/retrospections-on-the-curtain-raiser-to-the-long-past-great-war-1/){rel="bookmark"} {#retrospections-on-the-curtain-raiser-to-the-long-past-great-war-1 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 13, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/13/retrospections-on-the-curtain-raiser-to-the-long-past-great-war-1/ "2:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It was a year before the great अभिचार of दशान्त had struck us. We noticed that our forgettable acquaintance satya-druha who had been cursed by the deva-s was caught by something, much like bhIma by the serpentine नहुष. We soon came across that tantalizing elixir of atibala, drinking which we were committed to the long journey that finally ended in shadows of raurava. Immediately after we had drunk that elixir we experienced that very night the highs of महोल्लास, but on the other hand we were also seized by the dreadful ugra-दाढिक. Due to his leonine roars, the geometric intuitions which we had acquired till then deserted us, much like head of the makha leaving the deva-s. We were spiraling towards a pit of ignorance, surrounded by fiendish companions like palita-tvacha, बृहत्-sphik, sphig-shiras and पाण्डु-कृश. Then, the primary inspirations filtered in like first rays of dawn banishing the last dark hour:

 1.  We began to enter the minds of our ancestors the mantra दृष्ट-s of yore for the first time. The deva-s started unveiling themselves: the upward blaze of agni in the three stations, the power of वायु, the all-encompassing indra, the natural conservations of mitra, the natural laws whose violations are punished by वरुण, the exhilaration of knowing the natural law in aryaman, the swift transmission of the maruts, the dissolution of natural bonds through their father, the archer rudra, the light of सविता, the warmth of पुषा, distribution of energies through bhaga, the flow of time in विष्णु, the Milky way and the earthly flows of सरस्वती, the chemistry of the ashvins, the clear reflection by बृहस्पति, the revolutions and the cycles of the observable cosmos in soma and त्वष्ट with the ऋभुस्, the world axis and its precessions in Aja-एकपाद् and ahir-budhnya, the origins in aditi and then the mysterious prajApati called ka, and national identity in the 3 goddess ilA, सरस्वती and भारती


 2.  Then the exploits of the great पिनाकिन्-महादेव came to us. Having escaped from the worldly pursuit of riches for which our अमात्य had much hope, we found ourselves in the dark defile between the two stacks where the wondrous deeds of rudra were to reveal themselves from the rudra saMhitA -- the original core of the shiva पुराण. The power of rudra was felt by us. We caught a glimpse of the greatness of kumara then, but we had to wait some more time till the great skanda विद्या manifested in all glory to us.


 3. We began a ceaseless exploration of the secrets of life -- relationships between numerous single-celled eukaryotes, a primitive form of the animal-fungus hypothesis, lateral gene transfer from bacteria to eukaryotes, relationship of various DNA viral lineages. A full realization of these was to come only later, some of them as recently as a few months ago and others if we survive in the future.


 4.  We continued our exploration of the heavens -- the observation of SS Cygni crossing into the 8th magnitude through our 3 inch refractor was our first true encounter with a cataclysmic variable- a dwarf nova. It was a heroic struggle, unaided by our companion the vaishya श्रेष्ठ, from Rho Cygni we advanced to 75 Cygni. Then we marked the 3 faint stars forming the comparison for SS Cygni -- they were at the limit of our scope -- barely visible. It was in their midst that SS Cygni flared up.

Then we went daily to the place where the greatest patriot in the struggle against the Britons, Tilak, had taken the first steps towards national education. Our purpose was to practice arms for the impending war. We wandered friendless for some time until we ran into two mundane but kindly souls, lambaka and vidruma. Both asked me about that mysterious whirlpool which had sucked in satya-druha. We decided to lead them to it and soon found ourselves in the path leading to the temple of The god. We saw the gleaming trident rising from its spire and the horns of शैलादि at its base. There we overtook the whirlpool. Due to our vrata-s we stood firm, but lambaka saw the very face of the black son of विवस्वान्, while vidruma was brushed by the broomstick of ज्येष्ठा. We stood still for a moment wondering whether to proceed on or try to salvage our companions. Within us our decision-making process showed us the image of काल, with lambaka and vidruma rushing into, even as arjuna saw the सूत-putra, droNa and other rushing into its mouth. We left them to be and moved on. We heard a voice from the abyss that our turn would come too.

We rode our swift ashva and moved rapidly in the vigor of our youth. As we stuck to our vrata, we were able to quell the dangers of the whirlpool. Then the period at Tilak's buildings came to an end. We were told that the lines for the real war were being drawn -- we saw our foes headed by sphig-shiras and पाण्डु-कृश raring for battle. Just then we were seized by a terrible अभिचार. For 30 days we were assailed by it. We realized that all our prowess and brahman power had seeped out of us even as the xvarena departing from an ancient Iranian king. We were in utter fear and even unable to lift our bow, leave alone marching to the battle field. Hence we humbly approached out first आचार्य. He lead us to the mantra of that mistress of the kula path -- we felt the flow of प्रकृति. The resplendent मयूख-s emanating from the विद्या filled us with utter amazement --- we could glimpse but a fraction of the full glory of the kula path experienced by the adepts, but that was enough for us to appreciate its greatness. Very gradually, and only after साधन, and acquisition of higher and higher states of knowledge did we see the unfolding of some of the मयूख-s: उड्डीश्वरी, who shines in the ओड्डीयान पीठ, the dazzling beauty of जलेश्वरी who shine in jalandhara, the full satisfaction of पूर्णेश्वरी who blazes in पूर्णगिरि, कामेश्वरी with upwelling of the शृन्गार-rasa, the all encompassing गगना with the great knowledge in the sky, स्वरसा with that knowledge which led us on the higher quests and mati. It was then we also had a glimpse of the rahasya-s of चन्डोग्र-काली, the great शूलिनी, mahA-नीला with her fierce hordes and the terrifying शिवदूती.


