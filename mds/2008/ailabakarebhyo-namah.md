
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [ऐलबकारेभ्यो नमः](https://manasataramgini.wordpress.com/2008/12/21/ailabakarebhyo-namah/){rel="bookmark"} {#ऐलबकरभय-नम .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 21, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/21/ailabakarebhyo-namah/ "7:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Facing south we sung the जराबोधीय and sprinkling water with a clump of darbha from the pot into which had been stirred in the black tila. We place the पियारु-s in the mouths of the howling dogs of rudra:\
"*[रुद्रस्यैलबकारेभ्यो ।असंसूक्तगिरेभ्यः ।\
इदं महास्येभ्यः श्वभ्यो ऽकरं नमः ॥]{style="color:#99cc00;"}*"\
rudra's howling dogs that devour their prey whole\
To these dogs with mighty maw I humbly offer obeisance.


