
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [AdhAra ध्यानं](https://manasataramgini.wordpress.com/2008/03/20/adhara-dhyanam/){rel="bookmark"} {#adhara-धयन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 20, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/20/adhara-dhyanam/ "4:49 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![](https://i0.wp.com/farm3.static.flickr.com/2189/2347948762_2e9f80125b_o.jpg){width="75%"}

***इन्द्राय नमः***

