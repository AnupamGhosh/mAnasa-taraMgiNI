
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The काली-krama](https://manasataramgini.wordpress.com/2008/03/26/the-kali-krama/){rel="bookmark"} {#the-कल-krama .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/26/the-kali-krama/ "5:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The khachakra as described in the ख़्ह्PS has an octagonal circuit in which are seated the 8 योगिनी-s who are rejoicing at the great festival of the sport of yoga at the graveyard in करवीर. Inside their circuit are the 5 circuits of the 17-sided polygons (the Gaussian polygon) which shine with the 85 yogini-s. From them emanate another circuit of 85 yogini-s, together forming the 170 yogini-s of the khachakra. These योगिनी-s behold in the middle the mighty काली, who is known as महामङ्गला in the midst of an unimaginable blaze. One of her 9 syllabled mantra-s is hence "नमः श्रीमहामङ्गलायई". They praise the supreme काली, the mistress of the khachakra, who is full of wonder (महाश्चर्य) thus:

खफ्रेकार-धामस्थे धाम-रूपे निरञ्जने |\
chid-धाम para-धामाख्ये महाधाम pare kale ||\
गगनाम्बर-संकाशे निराधार-padoj-jhite |\
नभोव्योमान्तराकारे मन्थानान्ते namo.astu te ||\
You who reside in the abode of the bIja खफ्रें, whose form is the abode \[of all] , the stainless, the abode of consciousness known as the primal abode, the great abode and primary unit of time. You who are like the garment of the space, one who is free from even the state without any supporting foundations, you who is within space and vacuum, you who is within the churning one [*1], salutations to you!

[*1] मन्थान: this is an allusion to the great bhairava who in a different kula tradition, i.e. that of the पश्चिमाम्नाय is called the most great मन्थान bhairava.

Finally महामङ्गला manifests as 5 sequences: The first two are series of the 13 काली-s, and the remaining 3 are the काली-s of dikchara, gochara and bhuchara circuits -- this forms the heart of the काली kula. In the last circuit she is seen as the वैष्णवी shakti-s --- कालसंकर्षिणी and नारसिंही, and as the deva-shaktI-s too .

The two 13 काली sequences:

 01. rashmi-काली

 02. mantra-काली

 03. chandra-काली

 04. नबः-काली

 05. DAmarA

 06. Yasha-काली

 07. ogha-काली

 08. yama-काली

 09. eka-काली

 10. shabda-काली

 11. वर्ण-काली

 12. ऋद्धि-काली

 13. teja-काली


 01. शृष्टि-काली

 02. stithi-काली

 03. संहारिक-काली

 04. rakta-काली

 05. ध्वंशु-काली

 06. yama-काली

 07. मृत्यु-काली

 08. bhadra-काली

 09. मार्ताण्डेशी

 10. para-काली

 11. काल-काली

 12. वर्ण-काली

 13. ghora-चण्ड-mahA-काली


