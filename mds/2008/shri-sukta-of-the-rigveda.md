
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [श्री सूक्त of the ऋग्वेद](https://manasataramgini.wordpress.com/2008/04/22/shri-sukta-of-the-rigveda/){rel="bookmark"} {#शर-सकत-of-the-ऋगवद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 22, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/22/shri-sukta-of-the-rigveda/ "2:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The celebrated श्री सूक्त has spread widely beyond its original role in the vedic गृह्य liturgy and prayoga-s of the vedic विधान-s. Its earliest occurrences are seen in the khila of the ऋग्वेद and the बोधायन and वैखानस mantra पाठ-s of the कृष्ण yajur veda. It is also related to the षष्ठी सूक्त of the मानव गृह्य sUtra. The tantric literature also mentions it---the most prominent mention is in the लक्ष्मी tantra (LT 36.121-140 and LT 50.1-237). LT50.1-2367 gives a detailed account of the पाञ्चरत्रिc prayoga of the श्री सूक्त in worshiping लक्ष्मी. The लक्ष्मी-ratna kosha is another text providing its deployment in the worship of श्री. In the कादिविद्या stream of श्री-kula tradition a संपुटिकरण of the सूक्त and the पञ्चदशी (and other bIja-s like mAya, श्री, काम, वाणी and त्रिपुरशेखरान्त) is a favored mode of deployment. The श्री सूक्त has even been retained by the bauddha nAstika-s and this tradition was until recently alive in Nepal and Bhutan. The pure vedic form of both the yajurvedic and ऋग्वेदिc traditions are no longer commonly practiced, especially in South India. Instead, what one has is a vulgate that is widely used in the drAviDa, Andhra and कर्नाट countries. It may be recited in a pseudo-yajurvedic form without following the rules of yajurvedic phonetics (e.g. हिराण्यवर्णां हरिनीं सुवर्ण ... instead of the correct यजुष् form: हिराण्यवर्णा{\m+} हरिनी{\m+} सुवर्ण ... as is specified, for example, in the बोधायन mantra पाठ) or in a form typical of ऋग्वेदिc recitation. Original ऋवेदिc version is often termed the काश्मीर-पाठ because it appears to have been retained in those regions. The learned श्री Ravlekar gave me this original ऋग्वेदिc form. Based on his oral tradition and the कश्मीर-पठ of the khila text we have restored this text of the श्री सूक्त:

[श्री सूक्त as PDF](https://manasataramgini.files.wordpress.com/2008/04/shri_suktam_kp.pdf "श्री sUktraM"){target="_self"}

The oral tradition used in the restoration is also provided as a recording: श्री सूक्त as a MP3.


