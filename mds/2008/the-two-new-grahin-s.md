
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The two new ग्राहिन्-s](https://manasataramgini.wordpress.com/2008/04/30/the-two-new-grahin-s/){rel="bookmark"} {#the-two-new-गरहन-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 30, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/30/the-two-new-grahin-s/ "4:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Having evaded the वामग्राहिन् we accomplished the immediate objective with reasonable success. Then on the second day after that the वामग्राहिन् seemed to lose its grip like a leech touched by tobacco. Then two new frightful ग्राहिन्-s were sent against us. Evading our mantra-s like the रक्ष मेघनाद becoming invisible in the sky they struck us. Both the ग्राहिन्-s were utterly unexpected and nothing like what we had seen before. We just then obtained intelligence regarding the prayoga sent against our पक्ष. We also realized the जनाः were completely enmeshed it is जाल --- so much so that they did not even know what had hit them. Of course they could not be blamed entirely because our own astra-s were baffled by these two ग्राहिन्-s. They looked like a troublesome क्षुद्र-विद्या at the first sight, but the very fact two of them broke through, one after the other, was truely baffling.


