
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [lashunotsava or gandha-maha](https://manasataramgini.wordpress.com/2008/06/24/lashunotsava-or-gandha-maha/){rel="bookmark"} {#lashunotsava-or-gandha-maha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 24, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/24/lashunotsava-or-gandha-maha/ "2:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Hindu tradition mentions the following tale regarding the discovery and the use of garlic. The ब्राह्मण sushruta's curiosity was aroused by a foul-smelling bulb and he made enquiries regarding it to the renowned medical expert दिवोदास, the king of kAshI. दिवोदास told him that it was lashuna and described its medicinal properties and therapeutic and dietary uses to him. The resulting discourse became the prototype of the lashuna-kalpa of the magico-medical tradition of the Hindus. Since the स्मृतिस् have proscribed the use of lashuna by the dvija a predicament arises here. Two apparent solutions are suggested by tradition. One of these is to make the garlic "pass through the cow". The other is its consumption in a special periodic festival known as the lashunotsava or the gandha-maha. This festival is recorded in the lashuna-kalpa preserved in both the kAshyapa tantra and the Bower manuscripts. On the day of the lashunotsava even dvija-s could eat garlic untainted by the pAtaka. The festival is laid out by rudra in a discourse to भद्रकाली. When rudra was roaming in the देवदारु forest the wives of the vipra-s were afflicted with infertility. They called on him to relieve them of the condition. It was then that he instituted the garlic festival to cure the wives.


