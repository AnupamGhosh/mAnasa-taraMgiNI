
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some thoughts on medieval Sanskrit authors amongst our co-ethnics](https://manasataramgini.wordpress.com/2008/03/11/some-thoughts-on-medieval-sanskrit-authors-amongst-our-co-ethnics/){rel="bookmark"} {#some-thoughts-on-medieval-sanskrit-authors-amongst-our-co-ethnics .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 11, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/11/some-thoughts-on-medieval-sanskrit-authors-amongst-our-co-ethnics/ "5:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our co-ethnics represent one of the last great waves of brahminical settlers to the Southern country -- a rather continuous process happening over 2500 years. Among them were many students of the शास्त्र-s, creators of साहित्य-s, vedic ritualists and more secretly transmitters of tantric lore. They were at the heart of what might be called one of the final waves of Sanskrit creativity before the transition to the "modern" era. Their Sanskrit creativity partially overlaps in temporal terms with that of their more westerly neighbors the Nambuthiri-s, whose efflorescence spawned one of the greatest endogenous scientific and mathematical developments in Hindu thought. Their activities also partially overlapped in temporal terms with the Sanskrit creativity in the वङ्ग country resulting in the evolution of nyAya thought in a peculiar new direction -- the नवद्वीप school. There are several names among our medieval co-ethnics, whose productivity spans a wide range of topics. Some of these represent interesting and unusual mental feats. One such is the form of poetry known of viloma-काव्य -- i.e. a poem which makes sense when read in both forward and reverse direction.\
For example:\
Forward (describes रामचन्द्र):\
taM भूसूता-मुक्तिं उदार-हासं vande yato bhavya-भवं दया-श्रीः |\
Reverse (describes कृष्ण):\
श्रीयादवं bhavya-bha-toya-देवं संहार-dA-muktim उतासुभूतां ||

Two such works शब्दार्थचिन्तामणि by chidambara kavi of Thanjavur (and in it from our same ancestral अग्रहार) and यादव-राघवीय of वेण्कटाध्वरिन् (notable vedic ritualist who performed soma rites in addition) from Kanchi were produced in the 1600s.

One of the greatest names among these was appayya दीक्षित -- the center of many an apocryphal tale, to which we will add. \[This is not apocryphal] His grandfather नृसिंह \[acchan] दीक्षित was an adviser of the emperor कृष्णदेव rAya of vijayanagara. कृष्णदेव was a man of many women, including scholarly ones among them who could write decent Sanskrit poetry. With one such wife he had gone to see the image of विष्णु in the temple of वरदराज at Kanchipuram. acchan saw the beauty of कृष्णदेव's wife and composed verse. In this he stated that having seen कृष्णदेव's wife विष्णु looked for a moment at the kaustubha to make sure that श्री had not left him to stand by कृष्णदेव. So कृष्णदेव named him वक्षस्थालाचर्य after the वक्षस्थल where the kausthubha rests. He composed Sanskrit lyrics on विष्णु at वरदराज which were apparently used in the spring festival at Kanchi. He was an advanced vedic ritualist and performed multiple soma sacrifices. His second wife was a श्रीवैष्णव woman of the family of श्रीवैकुण्ठाचार्य, which included devote vaiShNava-s and some expert पाञ्चरात्र tantrics. Through her he had 8 sons who were learned ब्राह्मण-s and founded अग्रहार-s.

His 5th son, रन्गराज-adhvarin was a great soma sacrificer and extraordinary student of the शास्त्र-s. He adopted an interesting philosophical position which accommodated वैषेशिक, nyAya, मीमाम्स and advaita with the last as the overarching structure. He attacked bauddha philosophies and also was quite strong in his criticism of सांख्य. He modified yoga in his own way to accommodate it within his philosophical framework. He performed the most grand vishvajit soma sacrifice.

His elder son was विनायक-सुब्रह्मण्य, who became famous as appayya. He overlapped with the reign of वेण्कटपति of Penukonda who was in the midst of a life and death struggle against the Islamic Jihad that we have discussed in length before. वेण्कटपति had patronized appayya and honored him at his court. This in no small measure illustrates the importance of the post-Talikota struggle of the last Vijayanagaran rulers in providing a safe haven for Hindu intellectual activity. His grand-nephew was the illustrious Sanskrit writer and minister नीलकण्ठ दीक्षित. One of his sons was गीर्वणेन्द्र who was imparted a कौमार rahasya by my ancestor. गीइर्वणेन्द्र wrote a कौमार साहित्य known as कार्त्तिकेय-विजयं.


