
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A द्रमिड prashasti concerning shAstA](https://manasataramgini.wordpress.com/2008/07/25/a-dramida-prashasti-concerning-shasta/){rel="bookmark"} {#a-दरमड-prashasti-concerning-shasta .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 25, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/25/a-dramida-prashasti-concerning-shasta/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One topic of some interest in the evolution of the mantra-शास्त्र is the emergence of the shAstA tantra-s the most voluminous of which is the कुलाल-tantra. We had earlier shown how shAstA has emerged from an earlier equestrian deity revanta who was a part of the saura sect. A memory of this is preserved in the Tantric compendium of chenas-नारायनन् nambutiripad as also the North Indian mantra manuals such as the mantra-mahodadhi. But Arya was established as a distinct deity as early as the 200s of CE and certainly by the 600s of CE in the drAviDa country. Given the large स्थपना-tantra like material in the कुलाल तन्त्रं of which I have seen a bunch of manuscripts in the drAviDa country I was curious as to investigate their provenance.

For this purpose I started looking at Tamil prashasti-s from the choLa country. There was one from the north wall of the विष्णु temple of वैकुण्ठ-पेरुमाल् in Uttaramallur concerning a large temple of shAstA. It is dated to the 50th day of the reign of पार्थिवेन्द्राधिपति-varman who defeated the पाण्ड्य-s of madhurai. Its officiants are the members of the assembly of the ब्राह्मिनिcअल् village of the uttarameru-chaturvedi-मङ्गलं. The temple was built in a large piece of land in the south of the village of uttarameru. The inscription was written by a certain ब्राह्मण शिवदासन् brahmapriyan and the rite of consecrating the temple land was performed by a certain शात्तान् ब्रह्मकुट्टन् (probably a Prakrit form of shAstA brahmagupta) from the पुळियङ्गुडि village. The personal name of this ritualist itself contains the name of the deity and he is described as performing the पूर्वाचरं. The inscription also mentions that the assembly intends the performance of श्री-bali and अर्चनाभोग, both of which are mentioned with vidhi-s for performance in the कुलाल-tantra. In the inscription the deity is termed both महाशास्ता and Arya.

Who was this पार्थिवेन्द्राधिपति-varman? We notice on the eastern wall of the वैकुण्ठ-पेरुमाल् kovil an inscription which says: पार्थिवेन्द्र Aditya-varman who took the head of vIra-पाण्द्य. This concordance establishes the two to be same king. He comes after गण्डरादित्य and राजादित्य, which suggests that he might be aditya varman-II करिकाल. But we have no evidence whether Aditya करिकाल really occupied the throne. Further, there are inscriptions which talk of the 9th year of his reign. From what we know aditya karikala was murdered before he was युवराज for any such prolonged period. So we can only place the inscription as being between 950 and 985 of CE, during which a great temple of shAstA was built, but at least I am unsure of this choLa ruler. Of course today nothing more than ruins of the temple appear to exist.


