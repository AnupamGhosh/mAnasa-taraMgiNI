
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The अण्डाकार मण्डल of विष्णु श्रीपति](https://manasataramgini.wordpress.com/2008/11/19/the-andakara-mandala-of-vishnu-shripati/){rel="bookmark"} {#the-अणडकर-मणडल-of-वषण-शरपत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 19, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/19/the-andakara-mandala-of-vishnu-shripati/ "6:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3057/3036227882_81b7b1c370.jpg){width="75%"}
```{=latex}
\end{center}
```

\
Construction of the oval by the method of tangent circular arcs.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3251/3035392647_a8c5f4f694.jpg){width="75%"}
```{=latex}
\end{center}
```

\
अन्डाकार मण्डल

Two genuine oval constructions:

 1.  The locus of the mutually perpendicular lines with respect to the centers of the two eccentric circles and the points of intersection of the radius.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3168/3042323485_a8b91f00a3.jpg){width="75%"}
```{=latex}
\end{center}
```




 2.  The inversion of an ellipse on a circle.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3161/3042323541_cac379e006.jpg){width="75%"}
```{=latex}
\end{center}
```




