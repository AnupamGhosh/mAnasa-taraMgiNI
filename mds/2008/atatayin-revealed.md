
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [आततायिन् revealed](https://manasataramgini.wordpress.com/2008/03/18/atatayin-revealed/){rel="bookmark"} {#आततयन-revealed .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 18, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/18/atatayin-revealed/ "2:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We wondered who the आततायिन् was,\
the ari who was being a वृत्र to our kratu-s,\
the vile कुलुञ्च who was prowling among our possessions.\
We sought the aid of the son of agni, the slayer of तारक,\
to make visible to us the unseen डामारिका.

We spread forth the mantra of the one with spear.\
With the passing of the day the son of rudra uncovered the veil,\
and showed to us who the ari was!

It was the same vile डामारिका that had haunted us for long.\
We launched the operation patra-त्रात.\
We repeatedly called the 6-headed one to perform उच्छाटन\
of the शिश्ण-देवता-s who were वृत्र-s to our kratu-s.


