
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Abhyantara dhyAna of the श्रीकुल system.](https://manasataramgini.wordpress.com/2008/10/01/abhyantara-dhyana-of-the-shrikula-system/){rel="bookmark"} {#abhyantara-dhyana-of-the-शरकल-system. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 1, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/01/abhyantara-dhyana-of-the-shrikula-system/ "6:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[*तडिल्-लेखा-तन्वीं तपन-शशि-वैश्वानर-मयीं\
निषण्णां षण्णाम् अप्य् उपरि कमलानां तव कलाम् ।\
महा-पद्माटव्यां मृदित-मल-मायेन मनसा\
महान्तः पश्यन्तो दधति परमाह्लाद-लहरीम् ॥ Sऔ२१ ॥*]{style="color:#99cc00;"}\
With a body of the form of a lightning streak, constituted of the sun, the moon and fire, verily seated above the six lotuses is your कला. Roving in the great lotus with minds having suppressed the filth and delusion the great ones who see \[you] obtain the wave of the highest bliss.\
Series of successive dissolutions goes thus:\
ka -- विषु; e- मूलाधार; i- मणिपूर; la-स्वादिष्ठान; ह्रीं-अनाहत; ha-vishuddhi; sa-लम्बिकाग्र; ka-आज्ञा; ha-indu; la-ardhachandra; ह्रीं-रोधिनी; sa-नाद; ka-नादान्त; la-shakti; ह्रीं-व्यापिका->समाना->उन्मना-\>kulapadma\
The dissolution of the final syllable of the kula-विद्या occurs in a triangular yoni with its vertices marked by sUrya, agni and soma in the व्यापिका. The sUrya then dissolves into the agni in the समाना, then agni dissolves in to soma in the समाना, then soma dissolves into the lightning-like parA-कला in the उन्मना. When all are dissolved the yogin realizes the conjoining of kaulini with अकुलवीर in the kulapadma.

The one who understands this realizes why the पञ्चदशाक्षरी was a major discovery by the yogin-s within the श्रीकुल system that represented a new development beyond the ancestral system of bAlA. Of course, within the श्रीकुल system there was another parallel system, namely that of tripura-भैरवी that represents another distinct path of discovery.


