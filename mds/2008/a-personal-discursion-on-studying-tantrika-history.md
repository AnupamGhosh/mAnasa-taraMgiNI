
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A personal discursion on studying तान्त्रिक history](https://manasataramgini.wordpress.com/2008/07/07/a-personal-discursion-on-studying-tantrika-history/){rel="bookmark"} {#a-personal-discursion-on-studying-तनतरक-history .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 7, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/07/a-personal-discursion-on-studying-tantrika-history/ "6:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For some reason I was prompted to put down these useless words after an exchange with ekanetra. Many years ago, early in my youth the muni and I had created a series of historical impromptu dramas in which we had a series of तान्त्रिक ब्राह्मन characters as rAja-guru-s and the like. These dramas were strange in the sense we felt we were literally playing out lives which had been our own. There was some spontaneity with which these characters and their actions unfolded. There was much realism in a direction that written Indian histories have hardly paid much attention to -- including the great narrative tomes edited by RC Majumdar (of course history as a national narrative is different from history as a science...). This was because we were embarking on the study of the तान्त्रिक practice as insiders of the mantra शास्त्र tradition. A secular offshoot of this mantra-जिज्ञासन was the early realization that a very important facet of Indian medieval history was the interlock of the कौमार traditions, the kaula traditions like that of the कुब्जिका lineage, and the different shaiva srotas with royal power, city building, proto-chemistry and medicine. We saw this as extension of the brahma-क्षत्र alliance that developed in the classical shrauta period of the vedic layer of the dharma -- in particular the primary veda of my clan, the भृगु-veda. One discovery that intensely fascinated us was the herbal grids of the कुब्जिका tradition. We also eventually intend to outline some remarkable discoveries of related kaula traditions like that of the corvocephalous goddess kAka-चन्डेश्वरी, such as the use of frog-derived substances in medicine. Most of our early discoveries we incorporated into the above dramas. In the early stages we discussed these matters with the muni, R1's father (our Acharya on the secrets of the kaula texts) and the vaiShNava. Then we discussed them with ekanetra and a close friend of mine; we have also immensely benefited from the discussion with R, KRK and, to some degree, SRA on these topics. At some point we were tempted to compose a comprehensive history of this role of the तान्त्रिक traditions, but realized that it needed more focus and attentive study -- another distinct domain of our research needs greater attention these days. We hence started placing small notes summarizing various observations on these pages with the hope of composing the comprehensive monograph if we remain alive long enough and are not killed by the valaga-s. Our idea was to lay out the interlock between medical and तान्त्रीक tradition as one large section of this monograph with subsections covering shaiva, kaula, vaiShNava, saura and kaumara contributions. Unbeknownst to us the English महापण्डित Alexis Sanderson had also been pursuing research on some of the same topics. We eventually became acquainted with many of his scholarly works and were surprised by the convergences in many of the findings (though we totally disagree with his historical conclusions regarding लक्ष्मण deshikendra). It is interesting to see that a person from a totally different cultural background and working within a different academic framework has reached several comparable conclusions. Now that Sanderson's scholarly works have been published we felt that there was no point hurrying with a monograph on the topic. After some thought we decided that we will still do it -- actually we see this act of synthesis as a साधन in itself and the पुरश्चरण will be long and go for some time on these pages. \[ekanetra and me were recently accused by some of our co-ethnics and co-religionists of being Brown Sahibs subverted by Western indologists for asking them to read Alexis Sanderson and some of his successors' works like that on the मलिनीविजयोत्तर. Subverted or not we insist that these scholars set good standards for the study of the classical deva-भाष and also neglected शास्त्र-s. This is the point missed by our detractors.].

This was supposed to be a preamble to a brief note on the role of सैद्धान्तिक तान्त्रिक-s in the evolution of Hindu medicine -- a topic we will discuss next.


