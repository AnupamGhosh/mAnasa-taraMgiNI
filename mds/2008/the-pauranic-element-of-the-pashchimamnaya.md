
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The "पौराणिc" element of the पश्चिमाम्नाय](https://manasataramgini.wordpress.com/2008/03/04/the-pauranic-element-of-the-pashchimamnaya/){rel="bookmark"} {#the-परणc-element-of-the-पशचममनय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/04/the-pauranic-element-of-the-pashchimamnaya/ "3:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the days after the fierce battle of द्वादशान्त, after we had overthrown the कृत्यास्त्र that struck us, we were resting in the city of the कर्नाट-s. There while meditating on the aghora mantra we experienced that connection with समया and कुब्जीशान. That great bhairava, who is नवात्मन्, appeared in the लिङ्ग briefly manifesting that indescribable luster of the पश्चिमाम्नाय. Through the utter darkness we saw the path of the mantra विद्या.

*\~*\~*

The atharvavedins developed a connection with the पश्चिमानाय, as a result of which they acquired certain practices related to their tantric worship as well as लौकीक worship and mythography. The quest for these aspects of worship leads us to what may be termed the "पौराणिc" aspects of the पश्चिमाम्नाय. While the agni-पुराण has assimilated a large amount of पश्चिमाम्नाय material it does not really preserve any of the unique and distinctive पौराणिc material of this tradition. The पौराणिc material of कुब्जिकामत represents a variant that is not found in any of the surviving पुराण-s I have acquainted myself with. Instead the source of these materials is the कुब्जिकामत tantra (कुलालिकाम्नाय version), where they are provide as a frame material for the tantric narrative as well as 3 stotras that are used by the atharvavedins in लौकीक stuti of कुब्जिका. Below are some पौराणिc tales of the tradition briefly summarized:

**The tale of the origin of the पीठ-s**\
The mountain lord himavant lived in his great realm of सन्तानभुवन. The bhairava (नवात्मन्) appeared in this realm and stood in silence for a while. The coming of कुब्जीशान terrified the denizens of सन्तानभुवन who appeared along with himavant to bow to him. himavant praises the bhairava with a stotra (the कुब्जीशान स्तोत्रं) and asks to make himself comfortable as everything is the bhairava's abode. He also offers that his daughter कालिका would attend to his needs. After कालिका spends a while with नवात्मन्, he grants her a boon. She asks to become his wife, which the bhairava grants. Then कुलेश्वरी asks him the rahasyas, in reply to which bhairava reveals to her the siddhakrama, which he says is their common possession. He asks her to establish the tradition in भारतवर्श by generating many successors. He asks कुलेश्वरी to go to कौमारपर्वत and then the bhairava vanishes.

Struck by his disappearance and to search for her husband कुलेश्वरी proceeds to chandraparvata and ascends a beautiful rocky spur and absorbs to whole of existence into a लिङ्ग-like form called उद्यान-bhairava. Now नवात्मन् is irritated by the absorption of the universe and shouts that only chandraparvata is now left. So he goes there and praises the लिङ्ग, but she does not emanate. So, he praises her with the कुब्जिका दण्डकम् and she emerges from the लिङ्ग as समया or कुब्जिका. She is surprised as to who can stand looking, saying that it is as difficult as facing a dreadful cobra. She then grants a boon to the bhairava, who asks her to keep her word to go to कौमारपर्वत and generate the successors who would promulgate the पश्चिमाम्नाय throughout bhArata-varSha. While she refuses initially, the bhairava eventually convinces her to do so.

Going to कौमार parvata she creates a great forest with several लिङ्ग-s installed therein and with her toe she draws a line which becomes a great river. The she wandered in त्रिकूट, किष्किन्ध and himagahvara and founded the 4 पीठ-s of ओड्डियान, जालन्धर, पूर्णगिरि and कामrUpa. She then created a fifth पीठ of मातञ्ग (which is not on earth as per some accounts, in Himachal as per others). Then she returns to the bhairava after touring whole of bhArata-varSha. Then the bhairava asks her to promulgate the पश्चिमाम्नाय starting from ओड्डियान in the extreme north-west of bhArata-varSha.

**The** **कुमार tale of the पश्चिमाम्नाय**\
As समया was wandering over bhArata-varSha a drop of her sweat fell on the ground and generated the dreaded asura क्रौञ्च who caused much terror. The deva-s asked काम to strike bhairava to induce him to dally with कुब्जिका. While he burns काम, he does dally with his shakti and as result कुमार is born. Then the spear armed कुमार slays क्रौञ्च.\
\[Note the emphasis on क्रौञ्च as against तारक or महिष of the usual कौमार tradition.]

**The narrative of the bhairava or the core tantra of the कुब्जिका tradition**\
The core elements of the कुब्जिका tradition are provided in the form of a usual tantric narrative embedded within the पौराणिc frame, which recedes into the background once the core issues are discussed. These include:

  - The अSटविम्शति krama: the 12 shloka-s, the पञ्च ratna-s, the tadgraha and the 6-formed न्यास.

  - The key mantra of practice of the समया विद्या is the complex mantra known as the उमामाहेश्वर-chakra KMT 5.1-33. The mantra itself has 3 sections. This is some ways like what the व्योमव्यापिन् mantra is for the Urdhvasrotas सैद्धान्तिक-s.

  - In attacking अभिचारिक practice the follower of the पश्चिमाम्नाय uses the जुष्टचण्डाली विद्या, which is one of the most effective prayogas.

  - In the defensive mode he uses the rudra-कुब्जिका prayoga. As per the आVइन्स् associated with the कुब्जिका tradition rudra-कुब्जिका is the manifestation of प्रत्यङ्गिरा.

  - As we have mentioned before one of the highest साधन-s of the पश्चिमाम्नाय is the prayoga of the सम्पूर्ण-मण्डल in which the great aghora अष्टाकपाल is worshiped. Here every syllable of the mighty aghora mantra expresses itself.


