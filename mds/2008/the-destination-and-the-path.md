
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [रेवती-chakra](https://manasataramgini.wordpress.com/2008/09/14/the-destination-and-the-path/){rel="bookmark"} {#रवत-chakra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 14, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/14/the-destination-and-the-path/ "6:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3198/2844086597_b126714b99_b.jpg){width="75%"}
```{=latex}
\end{center}
```



[रेवती chakra]{style="color:#ff0000;"}

We finally completed the note on the अष्टाङ्ग संग्रह and the कौमार शासन, which marks the completion of one section of the multilayered technical study of the evolution of a much neglected stream of the dharma with which we are closely connected. We worship that great mistress of the chakra who crushes the दानव-s in battle, who appears in the sky beside [nandikeshvara, skanda and नेजस्मेष.](https://manasataramgini.wordpress.com/2006/11/29/on-to-shri-parvata/){target="_self"}


