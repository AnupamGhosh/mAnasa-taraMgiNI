
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Musings on some alternative grammarians](https://manasataramgini.wordpress.com/2008/03/23/musings-on-some-alternative-grammarians/){rel="bookmark"} {#musings-on-some-alternative-grammarians .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 23, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/23/musings-on-some-alternative-grammarians/ "12:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

दुर्गसिंह the commentator on the nirukta of यास्क states:\
"[व्याकरणं अष्टधा ।]{style="color:#99cc00;"}".\
The physician and grammarian vopadeva from the medieval yadava court expands on the same theme naming the eight forms of संस्कृत grammar:\
[इन्द्रश्-चन्द्रः काशकृत्स्न्ऽ-आपिशली शाकटायनः ।]{style="color:#99cc00;"}\
[पाणिन्य्-अमर-जैनेन्द्रा जयन्त्य अष्टादिशाब्दिकाः ॥]{style="color:#99cc00;"} (in धातुपाठ discussion)

Despite the tremendous productivity of संस्कृत thinkers on grammar it appears that the rest have not matched पाणिनि's success and have to greater of lesser degrees been washed aside. somadeva, the author of that great literary masterpiece the कथासरित्सागर apocryphally but colorfully narrates this process in his story on the debate of कात्यायन and पाणिनि. Of course tale goes back to somadeva's source material the भृहत्-katha.

Of these, jinendra, despite his name, is a bauddha and the last in the old line of grammarians. He is supposed to have lived in the वङ्ग country and been a bodhisattva. He appears to have been patronized the pAla kings around 725-750 CE. He is cited by the great vedic commentator haradatta mishra from the drAviDa country and criticized by the much latter भट्टोजी दीक्षित from महाराष्ट्र who appears to have clearly been aware of jinendra's bauddha biases in interpreting the vaidika aspect of पाणिनि. In any case this is still illustrative of his widespread influence as a major grammarian both in spatial and temporal terms. jinendra himself cites काशकृत्स्न and Apishali and thereby preserves fragments of their lost works. Another renowned nAstika paNDita, chandragomin, is usually identified as the author of the chandra grammar by tradition. bauddha tradition holds that the founder of चन्द्रव्याकरण was a kShatriya from varendra in the वङ्ग country, and probably lived roughly around the same time as jinendra. He is supposed to have debated other ताथागत-s like the nAstika महापण्डित चन्द्रकीर्ति with the aid of his mantra prayoga-s to invoke avalokiteshvara. He is reputed to have been a मन्त्रवादिन् of great siddhi-s, who journeyed from नालन्दा to the Andhra country, where he was locked in prayoga battles with Astika मन्त्रवादिन्-s who deployed सैद्धान्तिक, bhairava and कुब्जिका mantra-s to counter him. He is said to have composed several long stotra-s to मञ्जुघोष, avalokiteshvara and the kula goddess सितातपात्रा. While visiting गवलकुण्ड, R1's father brought out an old file of handwritten papers and showed me a prayoga stotra to प्रत्यङ्गिरा composed by chandragomin along with some nAstika mantra-s to the same deity.

chandragomin identifies प्रत्यङ्गिरा with his chosen deity the kula-mistress सितातपात्रा, whom he calls the queen of चन्द्रद्वीप. A tale is narrated where an irate ruler tries to drown chandragomin, but he is carried by the देवी to चन्द्रद्वीप and saved. This is of considerable interest because चन्द्रद्वीप is also the primal क्षेत्र of कुब्जिका. कुब्जिका too is identified with प्रत्यङ्गिरा as ghora रुद्रकुब्जिका-अथर्वण-भद्रकाली in the tradition of the उत्तराम्नाय and the कुब्जिका उपनिषद्. chandragomin begins his invocation to प्रत्यङ्गिरा by using the compound name उष्णीष-सितातपात्रा-प्रत्यङ्गिरा, which is parallel to another stotra in which he identifies अपराजिता with सितातपात्रा. Another text of chandragomin (of course given as a dialog between shuddhodhana-putra and his disciples) showed by R1's father was the dhAraNI of महाप्रत्यङ्गिरा, which had a picture of the देवी with numerous arms holding all kinds of weapons. Another text of chandragomin had a long nAstika-प्रत्यङ्गिरा माला mantra which seeks protection against all kinds of dangers.


