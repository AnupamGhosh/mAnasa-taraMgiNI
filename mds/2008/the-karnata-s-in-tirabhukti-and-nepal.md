
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The कर्नाट-s in Tirabhukti and Nepal](https://manasataramgini.wordpress.com/2008/05/19/the-karnata-s-in-tirabhukti-and-nepal/){rel="bookmark"} {#the-करनट-s-in-tirabhukti-and-nepal .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 19, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/19/the-karnata-s-in-tirabhukti-and-nepal/ "5:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Sometimes an exploration of the evolution of the mantra-शास्त्र leads to a historical digression. So it was when we were trying to trace the history of the नवदुर्गा class of texts in north-eastern India. We were urged to record a note in regard to the activities of the चालुक्य revival that spread out of Kalyani. After the fall of the original चालुक्य empire, which had carved a place for itself in history with the activities of pulakeshin-II, it was revived again under taila-II. From then on, till the beginning of their decline due to the crushing defeats at the hands of the काकतीय chiefs of the Telugu country, the चालुक्य-s where one of the greatest powers of India. Sadly, due to geography and possibly confused thinking of Hindu rulers the might of the चालुक्य-s was rarely directed against Moslem threat that was beginning to make its presence felt. Rather, the चालुक्य-s waged spectacular wars against other powerful Hindu rulers and advanced their might. Right from their revival the चालुक्य-s started expanding in both north and south. taila-II beginning around 975 CE invade the seized territories from the choLa uttama in the Tamil country, in the West took the Konkans from the shilahara-s and in the north invaded Gujarat, Malwa and Chedi seizing territories from them, and killed the परमार king मुञ्ज. His successor सत्याश्रय defeated the great राजराज of the Tamil country in the battles of Kurnool and Guntur. His son जयसिंह-II defeated the combined attack of two of the greatest Indian kings, rAja bhojadeva परमार and rAjendra choLa, from North and South. His elder sister अक्कदेवी was one of the few female warriors who actually led the चालुक्य armies in battle on the southern front. Then came the great चालुक्य-choLa struggle in which the choLa brothers राजाधिराज and राजेन्द्रदेव ground the चालुक्य-s to dust (though the former was killed on the field). राजेन्द्रदेव's son वीरराजेन्द्र also smashed the चालुक्य-s repeatedly and almost destroyed their kingdom when he ironically gave them a second life. He married his daughter to विक्रमादित्य-VI, one of the young sons of his चालुक्य enemy someshvara-deva-I. विक्रमादित्य-VI with his father-in-law's help took over the चालुक्य throne after defeating his elder brother who occupied the throne (1076-1126 CE). There he ruled for 50 years studded with numerous campaigns in an attempt to unify the whole of India under the कर्नाट empire. He sent his armies to invade Gujarat, Rajasthan, Sindh, the Moslem occupied territories of the Punjab, Vidarbha, Bihar, Bengal and Nepal. He also sent a navy to invade Shrilanka. Several of inscriptions describe him as conquering these regions, and ever since till the end of these चालुक्य-s we notice the kings mentioning such a list. However, modern historians have doubted these as bombastic claims.

Nevertheless, I realized that the solitary but long reign of चालुक्य विक्रमादित्य-VI was indeed the last attempt at the unification of the whole of greater India by a Hindu ruler before the Islamic interlude (his son was someshvara-deva-III of मानसोल्लास fame). There are several points in support of this contention:

 1.  बिल्हण, the Kashmirian poet joined the court of vikrama, and describes him as the foremost Hindu ruler of the era. His coming from Kashmir to the South Indian court at this time was largely due to the opportunities being available rather than invasions of the Mohammedans as in later times. He composed the biographical विक्रमाञ्क-charita on vikrama.

 2.  We see an interlude in the eloquence of the historians of the Islamic jihad and a surprising loss of the Islamic foothold in certain regions of the Southwestern Punjab. We posit that this and the delay in any further Islamic incursions into India corresponds to the चालुक्यn offensive on the तुरुष्क-s recorded in their inscriptions and the narrative of बिल्हण.

 3.  The sena dynasty in Bengal and the कर्नाट dynasty in Tirabhukti and Nepal were founded by kShatriya commanders from Karnataka.

 4.  There is a coeval rise of navadurga worship in the कर्नाट and Nepal regions in this period and a sudden influence of चालुक्यn Hindu iconography in Mithila, Nepal and Bengal.

 5.  The widespread popularity of mitAkShara as a system of Hindu law -- it was composed by विज्ञानेश्वर in vikrama's court.

In the early 1090s चालुक्य विक्रमादित्य-VI appears to have dispatched a large army under his young general नान्यदेव to invade the North East. Upon reaching Bihar the brilliant कर्नाट general conquered Tirabhukti and made it is his administrative and military outpost. From there he launched a remarkable campaign in Nepal, first taking Patan, then Katmandu and Bhatgaon and unified the whole of Nepal under his control in 1094 CE. With the conquest of Nepal, नान्यदेव expunged once and for all any kind political control that Tibet attempted to exercise over Nepal and brought it firmly within the Hindu sphere. नान्यदेव was a scholar on Hindu theatrics and composed a commentary on the नाट्यशास्त्र known as the bharata-भाष्य. It contains one of the only surviving records of some of the older rAga-s before divergence of Northern and Southern classical styles in Indian music and musical compositions known as पाणिका. नान्यदेव's successors who eventually went on to rule a separate kingdom in Tirabhukti where also maternal ancestors of the malla kings who eventually came to be the main rulers of Nepal in the medieval period.

Another कर्नाट army under a general of the sena family invaded Bengal from the north and planted themselves firmly in modern W.Bengal. One of their young warriors, vijayasena, then conquered Bengal to found the sena dynasty there. Thus, the generals of विकर्मादित्य-VI were founders of future dynasties of northern India. The possible role of the unification under विक्रमादित्य-VI in transmitting the पैप्पलाद shAkha of AV to these regions needs to be considered. This is especially so in light of the comment in the प्रपञ्च-हृदय that the पैप्पलाद shAkha was found in southern India.


