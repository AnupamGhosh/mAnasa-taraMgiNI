
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A practical guide to Vedic svara-s in recitation](https://manasataramgini.wordpress.com/2008/09/01/a-practical-guide-to-vvedic-svara-s-in-recitation/){rel="bookmark"} {#a-practical-guide-to-vedic-svara-s-in-recitation .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/01/a-practical-guide-to-vvedic-svara-s-in-recitation/ "11:04 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Due to multiple requests we decided to entirely redo and update our previous note on the subject. The whole thing was re-encoded using Unicode which hopefully allows cross-platform usability. The text is provided as a pdf. Yes, I am aware that a lot is freely available on the topic, but much of it, in our humble opinion, is confusingly presented.

[A practical guide to vedic svara-s in recitation.](https://manasataramgini.files.wordpress.com/2008/09/svaras_new.pdf){target="_self"}


