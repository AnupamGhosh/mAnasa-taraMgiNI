
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Asymmetric lotuses](https://manasataramgini.wordpress.com/2008/09/20/asymmetric-lotuses/){rel="bookmark"} {#asymmetric-lotuses .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 20, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/20/asymmetric-lotuses/ "7:09 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3063/2873453730_1ff2bd5acc_o.png){width="75%"}
```{=latex}
\end{center}
```



