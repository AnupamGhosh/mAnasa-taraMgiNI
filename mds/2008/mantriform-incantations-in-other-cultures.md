
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Mantriform incantations in other cultures](https://manasataramgini.wordpress.com/2008/07/20/mantriform-incantations-in-other-cultures/){rel="bookmark"} {#mantriform-incantations-in-other-cultures .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 20, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/20/mantriform-incantations-in-other-cultures/ "3:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A part of the study of the development of mantra-शास्त्र is a comparative analysis with similar productions in other cultures. I fear such comparisons at least in the preliminary stages of study are entirely befuddled by subjective comparisons. Several investigative techniques can be used to reduce this subjectivity: 1) machine-learning methods or neural networks to recognize and classify. From my own experience in another area of study I feel these approaches are heavily dependent on good training sets. 2) Construction of syntax abstractions like profiles or Markov models. This might work better. I can hardly claim to have applied such ideas with any kind of rigor. But I still feel that there are some mantra-शास्त्र-like elements in several Eurasiatic ritual traditions that I see as being structurally related to the ones in the Indo-Aryan cultural sphere (mantriform incantations). These proposals definitely need more detailed investigation and can be only considered to be preliminary.\
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2008/07/nit.jpg){width="75%"}
```{=latex}
\end{center}
```

\
In the Egyptian world one finds several examples, of which spell of goddess Nit (Neith; above) from around 500 BCE is worth reproducing:\
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2008/07/neith_glyph.gif){width="75%"}
```{=latex}
\end{center}
```



  - hail, mother great, not has been uncovered your birth \[This sentence has been rendered above as hieroglyphs. Sound: a= hail (shown in red), mut=mother, ur=great, an=not, sefekh=has been uncovered, mesu-s=your birth. Note the hieroglyph for "\[a=hail]" which is repeatedly used throughout the incantation];

  - hail, goddess (netert?) great, within the underworld which is doubly hidden;

  - you unknown one;

  - hail, you divine one great, not has been uncovered your veil;

  - hail, uncover your veil;

  - hail, hidden one, not is given my way of entrance to her, come, receive you the 'ba' of Osiris, protect it in your two hands;

To us the "hail" in this Egyptian incantation is a functional analog of स्वाहा or नमः in the Indo-Aryan mantra-शास्त्र.

Another great example is the Greek text, the so called "Mithras liturgy" (Papyrus 574 of the Bibliotheque Nationale, Paris). While it has been called that, it does not appear to have been composed specifically for the worship of Mithra. Rather, it is a tract of the generic neo-Platonic kind with several syncretic influences, including the polytheistic strain Judaists. The main deity appears to be Helios who might be syncretized with Mithra, Zeus and even Yahveh, but Mithra is rarely mentioned by name. It that sense parallels can be seen with the early saura tantra-s of the Indic world which actually borrowed material from a common source, given the role of the of मगाचार्य-s or शकद्वीपि ब्राह्मण-s \[As an aside Jan Puhvel compares it to the blabbering of a mentally ill patient of Carl Jung. My own suspicion is Jung retrofitted his patients blabbering into the text. Frits Staal has a similar view that the Indo-Aryan mantras are comparable to the productions of the mentally diseased---indeed Hindus must be mentally diseased].\
Some features in it are:

 1.  Two series of 7 incantations respectively to troops of 7 goddesses and 7 gods. These incantations use the terms χαίρε which I see as a functional analog of स्वाहा or नमः in the Indo-Aryan world. The descriptions of the 7 male gods in the series resembles the marut-s in the Indo-Aryan realm, and mirrors their 7-fold oblations.
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3082/2685819651_4c3351812c_o.jpg){width="75%"}
```{=latex}
\end{center}
```




 2.  Further, the text gives 3 similar incantations with χαίρε to Helios which has been translated as:\
Hail, O Lord, O Master of the water!\
Hail, O Founder of the earth!\
Hail, O Ruler of the wind!

 3.  The text also describes the deployment of an explosive sound like फट्.

Related to the "Mithras" liturgy is the larger collection of texts belonging to the Neo-Platonic Egypto-Greco-Roman syncretic religion. While these are diverse collection of texts in both Greek and the layer of Egyptian termed Demotic, they are ideologically quite similar and best understood in the context of the greater Neo-Platonic synthesis. They show signs of definitive influence from the polytheistic strains within Semitism (which were constantly sidelined and suppressed by the normative Abrahamistic strains). The work of the great German scholars Hans Dieter Betz and Karl Preisendanz (Papyri Graecae Magicae. Die Griechischen Zauberpapyri) provides us with much of the raw material for this study. While a detailed comparison of this material with the Indic Tantric material is far from complete, I do believe that the two should be seen in comparative light. The rise of neo-Platonism in the West coincides with the beginnings of the तान्त्रीक mantra-मार्ग in greater India. Along with their approximately coincidental beginnings we also see certain other commonalities in terms of the definitions of revelation and revealed texts (Agama-s), use of geometric मण्डल-s in conjunction with mantra-s, and the importance of non-semantic sound and syllable collections in incantations (मातृक-s and bIja-s) in both the Tantric and Neo-Platonic traditions. However, around the time Neo-Platonism was completely destroyed by the violence of Isaism, the tantra-s began their first explosive efflorescence in India. Hence in my opinion the evolution of these two systems should be seen as a part of a larger development happening all over Asia and the Classical world. As an illustration of this commonality let us take the example of the fever spell and the associated fever amulet (a "yantra").
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2008/07/neoplatonic_jvara.jpg){width="75%"}
```{=latex}
\end{center}
```



"O Tireless One, KOK KOUK KOUL, save Tais whom Taraus bore from every Shivering Fit, whether Tertian or Quartan or Quotidian Fever, or an Every-other-day Fever, or one by Night, or even a Mild Fever, because I am the ancestral, tireless God, KOK KOUK KOUL! Immediately, immediately! Quickly, quickly!" \[PGM XXXIII.1-25]

Note use of imperatives immediately and quickly, just as in Tantric mantras. Also note the description of fevers which exactly mirror the Ahnika, द्व्याह्निक, त्र्याह्निक and other jvara-s mentioned often in Tantric mantra-s.

Another point of note is the use of a "deshi" type language in the Greek magical papyri. Some spells have clear Hebrew names of deities such as the god Yahveh and the goddess Adonai. A good example of a hybrid Greek incantation is PGM VII.643-51 with a simultaneous invocation of Athena, Osiris, Yahveh and Adonai. The latter two are in a Hebrewized insert. Such syncretism was common in the Middle East and Mediterranean for a long time (e.g. the Mittani inscription mention Vedic deities along with many others). This suggests a participation from cosmopolitan polytheistic Hebrews along with lay Greeks and Romans (unlike their "orthodox" monotheistic cousins) in the Neo-Platonic system using Greek as a medium but incorporating their own deities and using a certain hybrid kind of language.

People over the ages have noted the non-canonical Sanskrit in some early tantras -- early Hindu authors attributed it to the idiolect of shiva and उमा (i.e. aisha) or by Buddha (who might not have liked Sanskrit too much :-) ) Some have said that what is seen in early tantra-s is a kind of hybrid language -- some even like the term Buddhist hybrid Sanskrit in the context of nAstIka tantra-s. Indologist Törzsök has suggested comparison of aisha to pidgins. But we feel this comparison is not the best one. In India it is most likely that the proper Sanskrit was the domain of orthoprax (e.g. मीमाम्सक-s, वेदान्तिन्-s, वैशेषिक-s etc. ) who placed great emphasis on language. Others probably lacking a rigorous training in Sanskrit might have tended to simply overlay Sanskrit vocabulary on the morphology of Prakrit-s or that language of fools (अपब्रह्ंश) -- but these aisha productions were by no means pidgins. Many Hindu-s today and in the past have been bilingual -- hence, they can appreciate it. At one end of the spectrum they have their own flavors of आन्गलीक, which might be Hinduized in different ways -- like in my inner circle I use a kind of a Sanskritized आन्गलीक. It is interesting that this phenomenon of hybridization (though distinct in specifics) apparently happened in both the Neo-Platonic Mantriform world as well as Indian mantra-शास्त्र. This was followed by a canonization and improvement of language in both streams (e.g. Emperor Julian's productions or abhinavagupta's production of anuttara-trika. The latter's language is definitely a pleasure to savor and is distinct from the शास्त्र-s he follows).


