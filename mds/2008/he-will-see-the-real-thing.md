
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [He will see the real thing.](https://manasataramgini.wordpress.com/2008/02/28/he-will-see-the-real-thing/){rel="bookmark"} {#he-will-see-the-real-thing. .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 28, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/28/he-will-see-the-real-thing/ "7:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Mis-creant and we rode past the 7 betelnut trees that stood in a row on our ashva-s. Beyond it was the realm of मृत्यु, where those dragged by the दूत-s of vaivasvata were making their last journeys. Those 7 betelnut trees reminded us of the seven Sal trees that रामचन्द्र ऐक्श्वाकव had pierced to prove to the कपीन्द्र that he could slay वालिन्. My ashva was a slow one, while Mis-creant had a swift sapti, so she briefly waited in the yonder ground for me to catch up. We were alone in the ground by the river. On the other side of the river there were some flickering flames of अन्त्येष्टी-s. She spelt out the संध्योपदेश:\
You shall have the mAyA,\
you shall have a 100 इन्द्रजाल-s,\
with it you shall make 100 loka-s\
with it you shall summon a 100 यक्षिनी-s\
He shall be filled with wonder,\
He shall behold the इन्द्रजाल-s,\
He will think the mAyA is all real !\
The kavi shall have none of the mAyA,\
So how shall he survive?\
He will have one mantra,\
It will give him siddhi only once!\
But with it he shall make real those lokas,\
Those which had sprung like maNi-s\
in the splendid इन्द्रजाल-s.\
For him send the spell of aryaman.


