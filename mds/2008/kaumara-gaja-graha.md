
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कौमार gaja graha](https://manasataramgini.wordpress.com/2008/06/24/kaumara-gaja-graha/){rel="bookmark"} {#कमर-gaja-graha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 24, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/24/kaumara-gaja-graha/ "4:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We observed a description of the the terrifying elephant graha of कार्त्तिकेय in the kAshyapa saMhita of the कौमार tantra-s. The same graha and the underlying mythology are alluded to in the section 129 of the kAshyapa तन्त्रं, the magico-medical text, which we had alluded to on these pages. skanda having emerged from the semen of rudra, red in complexion, was lying atop the great mountain and uttering loud roars. Then the lord of the gods, the resplendent indra arrived to see him. The fiery son of agni demanded that he be given ऐरावत, the four-tusked elephant of indra. indra was taken aback by the insolence of the roguish god and protested. Then skanda grabbed ऐरावत by his trunk and started seizing him. Seeing this indra decided to pacify the young god by generating a new, terrifying elephant named दुःसह and gave him to कुमार. Then from the right side of दुःसह emerged a terrifying कौमार vibhava known as shAkha and from the left side another known as विषाख. Gratified with this they made the terrible celestial elephant the lord of the upagraha-s in महासेन's army.

When a person, especially a child, is seized by दुःसह he gets nightmarish prodromal dreams of being chased and crushed by a fearsome elephant. Then he suffers an outburst of erysipelas known as the gaNDa-व्यूह. A number of भेषज-s expounded by the भार्गव ऋचिक can relive a victim of the seizure by दुःसह. He may also perform a mantra prayoga on every पञ्चमी to be released from the hit-list of दुःसह. A mantrin may deploy the mantra of the कौमार gaja to torment his foes. He may also invoke दुःसह along with a circle of awful skanda मातृ-s in mahA-कुमार chakra vidhi. These मातृ-s are:

 1.  वश्या 2) kula-kshaya-karI, 3) पुण्यजनी, 4) पौरुषादिनी, 5) संदंशी, 6) कर्कोटकी, 7) इन्द्रवडवा, and 8) वडवामुखी in the directions. The prayoga-s of इन्द्रवडवा and वडवामुखी can be used to counter भ्रातृव्य-s.

The mantra of दुःसह graha: duM hasti-grahaya huM फट् ||


