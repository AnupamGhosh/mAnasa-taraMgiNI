
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A sign from the deva](https://manasataramgini.wordpress.com/2008/07/13/a-sign-from-the-deva/){rel="bookmark"} {#a-sign-from-the-deva .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 13, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/13/a-sign-from-the-deva/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2008/07/collage.jpg){width="75%"}
```{=latex}
\end{center}
```



We felt we received a sign from the lord of षष्ठी, accompanied by रेवती and other hosts. It happened while exploring:\
Z\[n+1]=Fn1(Z\[n+1]); Z\[n+1]=Z\[n]-((P1\*Z\[n]^4 -P2\*Z\[n]+P1)/(4\*P1\*Z\[n]^3));\
Fn1=ArcSin or Sinh; P1=1+i; P2=0\
The षट्कोण yantra manifested in the middle of the spanda.


