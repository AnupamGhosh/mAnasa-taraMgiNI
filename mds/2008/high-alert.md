
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [High alert](https://manasataramgini.wordpress.com/2008/11/21/high-alert/){rel="bookmark"} {#high-alert .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 21, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/21/high-alert/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Between Oct 19th to Oct 25th the last six arrows were used up. We had repulsed he ग्राहिन् but were attacked by other weapons. Now we were without weapons. Till the evening of Oct 15th we held an upper hand. A mishap then appears to have given them a chance to attack with the ग्राहिन् again. We had a faint hint of the ग्राहिन्. On 13th there was again a hint of an attack but we gained an upper hand. Today a much larger force was sent and it was confirmed that the ग्राहिन् was back. Unlike in the past we were absolutely lacking in one great astra-s. Anything could happen.


