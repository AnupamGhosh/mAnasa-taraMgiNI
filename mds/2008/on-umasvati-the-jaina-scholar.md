
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On उमास्वाति the jaina scholar](https://manasataramgini.wordpress.com/2008/04/21/on-umasvati-the-jaina-scholar/){rel="bookmark"} {#on-उमसवत-the-jaina-scholar .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 21, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/21/on-umasvati-the-jaina-scholar/ "3:42 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Though the nआस्तीक mata-s in their reactionary fervor chose to keep away from deva-भाष, its lure could not be avoided. The precision and abilities of deva-भाषा, as well as the need to keep up with the शास्त्र-s and disputations of their आस्तीक rivals, made them eventually accept देवभाषा. This was facilitated to a great extant as the आस्तीक brain drain into these heterodoxies gained momentum with ब्राह्मण and kShatriya scholars converting to these mata-s. Thus, deva-भाष become the language of the educated from gandhara to पाण्डुरण्ग in Champa (central Vietnam) सुवर्णद्वीप in the sea (Java). But in the larger nआस्तीक sphere the anti-Sanskrit undercurrent linked closely to the anti-आस्तीक sentiment was never fully lost. This attitude was stronger among the jaina nआस्तीक-s, and persisted much longer than in their bauddha counterparts -- they tended to use the देशी medium much more, with Sanskrit works becoming prevalent only somewhat later. This is also corroborated by the jaina story of the Acharya siddhasena who wanted to render their देशी and Prakrit works into देववाणी, but was made ब्रष्ट for that suggestion, until he performed प्रायश्चित्त. Yet, due to the ब्राह्मण brain drain even the jaina-s adopted संस्कृतानुसार quite early for the purposes of philosophical discourse around the first 2 centuries of the CE. One of the first to do this was the great jaina philosopher उमास्वाति (उमास्वामी according to the digambara-s). वाचक उमास्वाति's work the तत्त्वार्थाधिगम sUtra-s (=तत्त्वार्थ sUtra-s; TAS) is one of the important hallmarks in the history of Indian thought that is comparable to the आस्तीक darshana sUtra-s. Its role as the primary pillar of the jaina mata is clear from the fact that it has been studied across sectarian lines among them and studied and commented by several notable जैनाचर्य-s -- siddhasena, haribhadra सूरी, devagupta, अकलङ्क भट्ट among others. Among the आस्तीक-s, even the medieval period, TAS was studied as a पूर्वपक्ष and cited by विद्यारण्य in his sarvadarshana संग्रह.

Of वाचक उमास्वाति we know not much and whatever accounts exist are confused and conflicting. At least one jaina account mentions that he belonged to the brahminical harita gotra but another contradicts this and mentions his gotra as कौभिषण (or unknown origin). There is some agreement that he was a teacher in the jaina school of पाटलीपुत्र. A study of the TAS reveals certain key points about उमास्वाति-s origins:

 1.  The main stay of the early jaina mata were the vaishya-s and their royal kShatriya patrons. This is clearly supported by the fact that the vaishya occupations are considered the best options for a jaina. Secondly, this is also consistent with the use of the देशी medium which was indeed the prevalent speech of the vaishya-s and possibly also some the earlier kShatriya dynasties like maurya and nanda. In contrast to this trend, the TAS is the first recorded instance of a jaina author composing a text in Sanskrit -- few other jaina texts were composed in Sanskrit for a while after the TAS.

 2.  उमास्वाति uses the sUtra style closely mirroring that used in the आस्तीक systems authored by ब्राह्मण philosophers. Thus, the TAS also represents the first time the jaina-s used the style of sUtra-s similar that prevalent from the vedic period among the आस्तीक-s. In its organization the TAS consciously mirrors the foundational sUtra-s of various आस्तीक darshana-s like those of कणाद, अक्षपाद, पतञ्जलि and बादरायण. The TAS has a closely linked commentary (by उमास्वाति himself according to श्वेताम्बर tradition) that resembles the yoga sUtra-s and their integrally linked भाष्य, the व्यास भाष्य.

 3.  The contents and philosophy of the TAS are clearly jaina, yet they have several elements that definitely reflect आस्तीक thought. The TAS positions itself as a शास्त्र for mokSha-dharma -- this is same as what vedAnta does (although their ideas of mokSha might differ). It presents restraints and control of the senses (e.g. 7th अध्याय) which resemble those of आस्तीक ascetic practices enjoined by the yoga tradition (e.g. ahimsa, satya, asteya, brahmacharya, aparigraha etc.). Importantly, the TAS mirror nyAya and वैशेषिक thought in its analysis of substance and the atomic theory (अध्याय 5).\
Thus, it appears quite likely that उमास्वाति was a ब्राह्मण convert to the jaina mata, who brought in the Sanskritic sUtra tradition as well as a certain philosophical elements (this will definitely offend hardline jainas who want to separate themselves from the rest of Hindu tradition). In the process he ended up writing what were probably the first sUtra-s of the jaina-mata, equivalent to those of the आस्तीक darshana-s, and thereby set the jaina darshana on level ground with the अस्तीक for the battles to ensue.

उमास्वाति's sUtra-s includes a feature that is not common in the principal आस्तीक darshana-s, namely the sUtra-s that describe geography/cosmography and types of deva-s (अध्याय-s 3 and 4). The description of the types of deva-s including multiple indra-s and their vimAna-s ("spaceships") is primarily to establish an alternative system that purposely differs from that of the आस्तीक-s from whom they branched off (we have noted before examples of how the bauddha-s as well as jaina-s did this on these pages). In contrast, the principal आस्तीक darshana-s affiliated with the veda did not need to include such sections because their versions were the standard versions which were already provided by their shruti and original इतिहास-पुराण. By creating such parallel versions the jaina-s (and other nआस्तीक-s) could now set up cosmographies and theographies that could claim a hierarchical superiority over the standard model. For example by creating a system of multiple serial indra-s they could trivialize the foremost deva of the vedic आस्तीक-s. However, the nआस्तीक-s were not unique (and possibly not original) in this regard -- the sectarian अस्तीक-s too set up such systems to supersede earlier systems in their cosmographic/theographic hierarchy. For example, among the shaiva-s, the लाकुल-s who succeeded the veda-affiliated original पाशुपत-s (who have no cosmographic sutra-s) added a new system of shiva-s (e.g. vidyeshvara-s) to supersede the original rudra-s of their predecessors. This process continued with the siddhAnta tantras with new shiva-s being added on top of the लाकुल system till सदाशिव is reached. Finally, the trika and श्रीविद्या systems even place preta-s of सदाशिव and others in the throne of their respective देवी-s.

However, the most important intellectual contribution of वाचक उमास्वाति was his analysis of life forms and matter. It is in this he preserves some of the early aspects of Indian scientific thought. We had earlier discussed on these pages the zoology of उमास्वाति and his classification of animals based on various anatomical and reproductive characters (अध्याय 2). In अध्याय 5 of the TAS we find the early exposition of jaina atomism:\
[नाणोः ॥]{style="color:#99cc00;"} 5.11\
There are no further divisions in the ultimate atom of matter.\
[अणवः स्कन्धाश्-च ॥]{style="color:#99cc00;"} 5.25\
Matter may exist in the form of either pure atoms or clusters there off (स्कन्धाः -- molecules). The jaina authorities in elaborating on this sUtra mention niyama-sAra 6 which declares the परमाणु to be that particle which cannot be divided further, and is itself the beginning, the middle and the end of the particle. The associated भाष्य of the TAS also cites an ancient unattributed verse which is reminiscent of मम्मट's opening of the काव्यप्रकाश which declares the परमाणु to be the ultimate cause, minute and indestructible.\
A remarkable feature of उमास्वाति's atomic theory is the postulation of two forces to mediate the interactions between the आणु-s:\
[स्निग्ध-रूक्षत्वाद् बन्धः ॥]{style="color:#99cc00;"} 5.32\
"snigdha" and "रूक्ष" are the two properties that mediate atomic combinations.\
He then formulates a system of rules involving these forces that mediate atomic combinations.\
[न जघन्य-गुणानां । गुनसाम्ये सदृशानां । द्व्याधिकादिगुणानां तु । बन्धे समाधिकौ पारिणामिकौ ॥]{style="color:#99cc00;"} 5.33-5.35\
अणु-s possessing the minimum one degree of snigdha or रूक्ष cannot combine; अणू-s which have the same degree of snigdha or the same degree of रूक्ष cannot combine; two snigdha अणु-s or two रूक्ष अणु-s can combine if the snigdha or रूक्ष of one is two or more degrees higher than the other; during combination the अणु with equal or higher degree of रुक्ष or snigdha transforms the property of the dissimilar अणु to its own. Thus, उमास्वाति postulates the existence of charge-like properties to explain atomic combinations.


