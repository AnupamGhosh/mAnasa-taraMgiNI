
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The experience of a mantra](https://manasataramgini.wordpress.com/2008/04/07/the-experience-of-a-mantra/){rel="bookmark"} {#the-experience-of-a-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 7, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/07/the-experience-of-a-mantra/ "6:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There are certain things which only make sense to the insiders of the mantra-शास्त्र. To others it might appear to be a purely subjective matter, while to the practitioners there is a clear objectivity to it. The link between yoga and mantra-शास्त्र lies at this point. The correctly educated Hindu, as in real science, does not accept anything as a matter of faith. I was incited to mention this because R mentioned to me how she found some sincere Hindu साधक-s made deprecatory comments about ritual or karma. Instead, they said that they had a preference for "mystical" and "philosophical" interpretations. But this is not correct because nothing can be accepted as a matter of faith -- one has to perform the experiment and study the result. The karma (ritual) and/or yoga indeed comprise the only experiments in this realm -- hence, without ritual or yoga there is absolutely no way of gaining knowledge. In another domain of knowledge being a scholar does not make you a scientist. You may know all the knowledge in the books of science, but you are not yet a scientist. You become a scientist only when you make reproducible discoveries that increase your (and others) knowledge. Often in science a pre-requisite is that you acquire knowledge of methods like a good sense of observation and mathematics or computation, and accumulate actual factual knowledge to be an effective scientist. Similar to science one needs to know the शास्त्र-s and study them in depth. Then one needs to do the experiments in the form of rituals --karma or yoga otherwise knowledge is not produced. Like in science one must realize that one could be wrong or that one may have incomplete knowledge. From approximation one proceeds to preciseness.

For a practitioner of mantra-शास्त्र the experience of mantra-s is very important. Sometimes these are like difficult experiments -- they need many components to be carefully put together. Yet from time to time the साधक might have an experience that provides a remarkable verification and clarifies many lose ends. Yet, this being the mantra-शास्त्र the parallels to science end here in terms of a description. I cannot describe such experience using the same terminology of used in science and I also have resort to a different language -- is there a human language at all -- or is it just the language of the mantra-s. So in human language to most onlookers it sounds like pure madness.\
*\~*\~*\
It was the great mantra of मातङ्गी -- संगीत-योगिनी -- mantra-नायिका invoked right in the middle of the geya-chakra with the pentagonal star and the golden triangle surrounded by the band of योगिनी-s rising like a blazing sun over the ओड्डियान पीठ. The effect is so intense and immediate! The effect is the pure essence of the geya chakra, the essence of the great योगिनी. You do not see her or योगिनी band, but you are caught by the योगिनी जाल. You are present immediately in ओड्डियान. You course through the world in the dimension of time and then in space. Each bIja's expression is plainly seen by the साधक and they comprise the form of the great मन्त्रिनी. The unity of forms is seen.


