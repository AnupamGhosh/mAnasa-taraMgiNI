
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [महासमर](https://manasataramgini.wordpress.com/2008/12/28/mahasamara/){rel="bookmark"} {#महसमर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 28, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/28/mahasamara/ "7:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Update 4 (27-28th Dec): In course of our expedition, unexpectedly the prati-prayoga again destroyed our "shield of कार्तवीर्य", but this time comprehensively. We then went into the hands of sleep, the dark maid. We awoke at the end of a strange dream: We had biked down a long road that was vaguely familiar. At the end of the road we were given the choice by someone who looked like a traffic warden of returning and taking another route or crossing a bridge across an arm of the sea to get to our destination. We saw a bunch of guys who looked vaguely familiar. We thought they might be the guys from the past whom we thought had been washed away by the force of karma, but we could not identify them by name. But now they were back, at least in the dream. They all told me its alright to cross via the bridge as it was much faster. So we all told the traffic warden our intention to cross the bridge. He gave us each a "pass" and opened the door for entering the bridge. The first part of the bridge was an excellent closed tunnel and we all rapidly rode our bikes through. At that point one of the fellow riders mentioned that some friend of his had crossed this part of the bridge but just before completing the journey was washed away into the sea never to be seen again. We asked: "How could that be? This passage seems well-protected from the sea." Just then the bridge came to an end and we descended two flights of steps down and a door opened onto a wooden pier facing the raging sea with a mangrove forest in the yonder shore. Below the pier a flimsy set of wooden planks, partially submerged and constantly agitated by the waves, constituted the only path to the other side. Two of our fellow riders deftly lifting their bikes made it to the other side timing their entry on the planks to avoid the crashing waves. The rest of us were wondering if we could survive this passage. With the planks wildly bobbing I feared I could not and decide to return all the way back. Some of my fellow riders told me that it might not be possible because the door to the bridge opened only one way. I still felt I should take a chance. They said may be we should wait till the tide changes. Just then a strange man came down the stairs. He was vaguely familiar -- I thought he was a long deceased paleontologist who worked on synapsids. He looked at me and said: "you might still be able to reach the other shore", and walking around my bike opened a new door that directly lead to a short concrete pathway reaching the other side. At that moment I awoke and realized I had been seized by the frightful ग्राहिन्. We were utterly frustrated that our human means had failed -- the fears of a long journey to avimukta lay before us. Was there a signal we were missing?

Update 3 (24-25th Dec): After the 9th hour our astra-s seemed to be repulsed by the ari-संकल्प. We felt the lasso of vaivasvata might prematurely tighten on us. We deployed a new type of astra as suggested by the अमात्य of the 3rd hero. We did not know if we had shaken off the noose or not as our armor was pierced around 1.5h after the middle hour. So it appeared the danger had not passed and we were rather curtailed. Was our fate to live and conquer or fade into oblivion remained unknown.

Update 2 (21 Dec): For a while our astra-s kept failing but we kept fighting in the midst of the सेना with little respite. While we were performing a rite to rudra, the पृतन came against the sachiva with full force but he firmly stood his ground and kept the battle going though confused by the distractionary moves of the ari. We then returned to the battlefield with the firm resolve that we will either feed the dogs of rudra or fall in the fight ourselves. We joined forces with the sachiva to keep the fight on. The battle hung in balance we as we moved to the shortest night when the powers of the निशाचर-s is exalted.\
...\
Update 1 (19-20 Dec): We performed an unusual prayoga in which we first invoked tryambaka to break the "inverse of कार्तवीर्य". Then we deployed कार्तवीर्य again and met with remarkable success. While the अघायु-s raged we successfully defended against them in large part with our revived astra-s. We had brought the situation under reasonable control until about 2.5 hrs past the midnight hour a कृत्या deployed on us. We initial thought our astra-s should cover the कृत्या and indeed for two hours we were relatively safe. But then this terrible कृत्या broke through the defenses and attacked us fiercely. We were unable to figure out how this कृत्या broke through and that remains the weak link.\
...\
Dec(18th):\
"yaM क्रन्दसी संयती vihvayete pare.avara उभया अमित्राः |\
समानं chid ratham आतस्थिवांसा nAnA havete sa जनास इन्द्रः ||"

It was literally like Conan Doyle's legendary encounter staged at the Reichenbach falls. It was like the सूत-putra and the 3rd kaunteya on their last encounter in कुरुक्षेत्र. The ब्रह्मास्त्र was blocked, the other shara-s of old were blown to pieces and the power of the mantra-s were all silenced even as the shamitar silences the pashu at the offering of वायु. The वर्ंअन् was pierced and the sapatna-s advanced like मेघनाद in the defense of लङ्का. The "inverse of कार्तवीर्यारुज" deployed against us and it completely rattled us. Till the 7th hour after the meridian we kept fighting on the field even as another great astra was sent at us. This astra could completely sink the greatest of our defenses, though it could still not kill us. But they were fully aware and backed it with another deadly astra that could actually take us along to vaivasvata even as the पाशुपत of arjuna took out jayadratha and वृद्धक्षत्र at the same time. It was clear they were going for the kill. The muni could not be informed that he needed to take our place if we were killed and the others did not matter any more. We had with us only one long-standing yoddha, more important than all the other vIra-s we have mentioned. Due to his aid we obtained the indra-shakti and पाशुपतास्त्र for the final battle. The indrashakti was deployed but it was like the सूतपूत्र striking घटोत्कच. All is decided by the flow of काल and it shall make its decision.


