
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [रावण tantra-s](https://manasataramgini.wordpress.com/2008/10/10/ravana-tantra-s/){rel="bookmark"} {#रवण-tantra-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 10, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/10/ravana-tantra-s/ "6:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I had the opportunity to examine a series of related manuscripts which were called रावण tantra-s or graha tantra-s with श्री Ravlekar's contact, the remarkable GK. Unfortunately, I could only examine them and do nothing more. I only have in my possession a single the fragment of an obscure early कौमार tantra that we shall discuss at some later point in time. They point to the a remarkable class of literature that lies at the boundary between the early कौमार tantra-s and भूत-tantra-s, keeping in line with our thesis that the early कौमार tantra-s in addition to covering topics related to the worship of skanda and his मातृ-s and गण-s was directly linked to pediatric issues.

 1.  लङ्काधिपति-कृत-trayodasha योगिनी विद्या: This fragment attributed to the rAkShasa king of लङ्का has invocations of 13 योगिनी-s for pediatric relief. A parallel set of योगिनी-s are seen in उड्डीश tantra also attributed to रावण. The number of योगिनी-s (13) and their names suggest that they are linked to the 13 मातृ-s in the कौमार kula-विद्या. The योगिनी-s are: मातृका, स्तनदा, पूतना, मुखमण्डिका, बिडाली, नाकुली, जम्भला, अजिता, डाकिणी, पतिनी, भद्रकाली, षष्टिका and रेवती.

 2. रावण-tantra: Has mantra-s to 11 मातृका-s and skanda: मातृनन्दा, सुनन्दा, रेवती, मुखमण्डिका, बिडाली, शकुनी, पूतना, शुष्का, आर्यका, जम्भका, पिलिपिच्छिका and skanda. I saw a book by PC Bagchi translating a Nepali text named द्वादश-graha-शान्ति which is related to this text. A nAstika scholar dharmadeva from Nalanda journeyed to China around the 800-900 of CE and translated the रावण tantra into chIna-भाष. The Chinese text still survives. Unfortunately, I was never able to obtain Bagchi's book ever again to do a more detailed comparison.

 3.  दशग्रीव tantra: This text describes attacks by 12 मातृका-s of the कौमार type that can cause bAla-roga-s. They are all countered interestingly by a series of mantra-s to रावण. The much later medical authority trimalla-भट्ट states that विष्णु should be invoked along with रावण for the same roga-s.


