
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The tradition of वाग्भट in the drAviDa and Andhra countries](https://manasataramgini.wordpress.com/2008/10/12/the-tradition-of-vagbhata-in-the-dravida-and-andhra-countries/){rel="bookmark"} {#the-tradition-of-वगभट-in-the-dravida-and-andhra-countries .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 12, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/12/the-tradition-of-vagbhata-in-the-dravida-and-andhra-countries/ "6:38 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier briefly alluded to the living tradition of वाग्भट in the chera country, which is practiced by the अष्टवैद्य-s. We currently believe this might be an early transmission of the great physician's tradition to the south of the subcontinent that got fixed there (given that the evidence suggests that वाग्भट himself lived in the sindhu region, i.e. modern Sindh). We wondered if such a tradition was also transmitted to the Tamil country during the original transmission to the chera country. While we found no direct evidence for this, we learnt of another medical tradition derived from that of वाग्भट to have flourished in the Tamil and Telugu countries. We must mention that our investigations cannot be regarded as complete as we have insufficiently examined the relevant texts. The principal text in concern is the parahita-saMhitA (PS) of श्रीनाथ paNDita. The one printed in the Tamil country has the Sanskrit in the Telugu script with a Telugu translation, so were unable to do anything with this. The one printed in Andhra is only partial, and seems quite full of printing errors which made it quite hard to follow. Then there are several Vijayanagaran inscriptions that are quite enlightening which connected to this story in course of an investigation on the medical traditions amongst my medieval co-ethnics.

The primary transmission of वाग्भट's tradition to the Telugu and Tamil countries appears to have been redacted and recompiled into a comprehensive work known as the parahita-saMhitA. This work was done by a learned ब्राह्मण named श्रीनाथ-paNDita apparently of the Andhra country. The PS has three काण्ड-s called the साधारण, अष्टाङ्ग and rasa. The रसकाण्ड is an innovation of श्रीनाथ-paNDita, but was unavailable the version I examined -- hence it is unclear if it actually includes material that was derived from a specialized derivative the kaula तान्त्रिक tradition -- the रसायन. While stressing the fact that we have not systematically studied the text, it appeared to me that the treatment of burns (dagdha chikitsa) and tendonitis were some novel features of this text (of course they could be derived from any of the earlier texts in the वाग्भट tradition that we have not examined).

We believe that श्रीनाथ-paNDita was the founder of the tradition followed by two unrelated but connected families of physicians mentioned in the Andhra inscriptions. The first of these were of an older layer of Telugu स्मार्त-s of the Atreya gotra. The first Atreya inscription mentions the parahita Atreya as being the प्राणाचर्य or court physician of the kamma chieftain सिङ्गयनायक of कोरुकोण्ड who was one of the warlords in the alliance that repulsed the army of Islam during the southern thrust of the Tughlaqs. The next inscription of this clan was of parahita Atreya son of कालनाथ भट्ट. He was the court physician of the रेड्डि princess of Rajahmundry, anatalli, the sister of कुमारगिरि, who gave him the village mentioned in the inscription. The second family of physicians was वडम स्मार्त-s of the drAviDa country belonging to the kAshyapa gotra. They appear to have settled in the Andhra country in Ponnupalli and their lineage is given as: periyavilla parahita->भास्करार्य->विल्लनार्य->सिङ्गनार्य. We have inscriptions of भास्करार्य and सिङ्गनार्य. Both families mention the founder of the parahita tradition, which is श्रीनाथ-paNDita, to have been a snake-doctor. He successful treated a snake with a blockage in its alimentary canal and was given the title सर्पव्याधिचिकित्सक.


