
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Transmission of the कालि kula tantras according to the chinchini-mata sAra समुच्छय](https://manasataramgini.wordpress.com/2008/03/13/transmission-of-the-kali-kula-tantras-according-to-the-chinchini-mata-sara-samuchchaya/){rel="bookmark"} {#transmission-of-the-कल-kula-tantras-according-to-the-chinchini-mata-sara-समचछय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 13, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/13/transmission-of-the-kali-kula-tantras-according-to-the-chinchini-mata-sara-samuchchaya/ "2:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There was a muni named शीलाचिति in the शिवपीठ of श्रीशैल. His son was a siddha named विद्यानन्द who looked like a शाबर tribesman. He lived in श्मशान-s and performed nightly वीरसाधन by meditating on his chakras. He went to the mountain range to the north of श्रीशैल and there in a gold mine [*] dear to the gods he worshiped, desirous freedom. A tantric निष्क्रियानन्द was pleased with him and initiated him into the कालि krama (CMSS 7). He then transmitted the tradition of कालि krama in भारतवर्ष.

  -  "This cave of gold" might be identified as either one of 3 caverns recognized today: दत्तात्रेय cave, "Akkamahadevi" cave or उमामहेश्वर cave.


