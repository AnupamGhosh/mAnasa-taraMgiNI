
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some icons from Nepal](https://manasataramgini.wordpress.com/2008/06/07/some-icons-from-nepal/){rel="bookmark"} {#some-icons-from-nepal .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 7, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/07/some-icons-from-nepal/ "5:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![](https://i0.wp.com/farm4.static.flickr.com/3072/2559543403_59d75f9193_o.jpg){width="75%"}

In the order shown in the figure

 1.  कुरुकुल्ला 2) indra 3) महिषमर्दिनि

 4.  वसुधरा/लक्ष्मी 5) indra 6) वज्रपाणी

 7.  shveta-tArA 8) रुद्राक्ष 9) indra

