
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [स्वायुध](https://manasataramgini.wordpress.com/2008/08/08/svayudha/){rel="bookmark"} {#सवयध .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 8, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/08/svayudha/ "11:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3004/2742825055_81a17ea7cf_o.png){width="75%"}
```{=latex}
\end{center}
```

\
With our bodies seized by disease,\
Our minds filled with anxiety of foes,\
curtailed by अभिचार prayoga-s,\
and shadowed by आततायिन्-s\
we approach the terrible one -- saH देवः

