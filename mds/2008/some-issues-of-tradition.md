
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some issues of tradition](https://manasataramgini.wordpress.com/2008/03/13/some-issues-of-tradition/){rel="bookmark"} {#some-issues-of-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 13, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/03/13/some-issues-of-tradition/ "5:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In discussions with R and some others a few topics have repeatedly come up. While I am tempted to delve into them in depth, I feel my study of the data on these matters is inadequate to present as definitive a view as I would like. But I still cannot hold back the temptation to place few points here, despite my incomplete study. The main topics in question are:

 1.  What is the origin of श्रीविद्या and what were the original forms of the श्रीविद्या mantra-s? The पञ्चदशी and its derivative were definitely not the earliest forms, and we have briefly discussed pre-पञ्चदशी श्रीविद्या and bAlA which were earlier. In a sense the initiation pattern starting with bAlA and going to higher पञ्चदशी derivatives actually mirrors history. The now declined tripura-भैरवी form of श्रीविद्या with 9 नित्या-s (e.g. as in shArada-tilaka) was an intermediate element in historical development. This is true of many tantric traditions where ritual and initiation patterns follow historical development (as in biology we observe ontology following evolution). abhinavagupta in developing anuttara-trika from the existing trika systems actually consciously follows this pattern. The roots of श्रीविद्या lie in the early kula texts, which also spawned other kula traditions like कालि-kula, समया-kula and the poorly studied त्वरिता.

 2.  Is प्रपञ्चसार's श्रीविद्या the पञ्चदशी? While the commentarial tradition might hint the tripura-भैरवी form, the root tantra itself seems to primarily follow the bAlA mantra.

 3.  Was mokSha the original goal of श्रीविद्या? It was just one of the many goals generally acknowledged by kaula systems.

 4.  Was advaita vedAnta connected to श्रीविद्या from its inception? Is the महापदुक mantra a "genuine" aspect of श्रीविद्या? Are शङ्कर and गौडपाद really connected to श्रीविद्या?\
advaita vedAnta has its origins in one set of the diverse ideas presented by philosophers from the vedic period. Its subsequent development stood on the great philosophical exegesis of शङ्कर's school. Tradition also connects शङ्कर's school with a certain form of श्रीविद्या, which tends to ignore or remove the kula doctrines and this is certainly the form of श्रीविद्या practiced by modern initiates affiliated with शङ्कर's tradition. But there are some issues amongst these initiates: 1) Many of the modern initiates while very knowledgeable about their paddhati-s and mantra prayoga-s have a relatively poor understanding of the root sources: the diverse kula texts including the root tantra-s of श्रीविद्या. 2) Many aspirants as well as lay devotees actively practice texts like ललिता-सहस्रनामं and सौन्दर्यलहरी but do not recognize or in some cases deny the kula doctrine at their core. 3) They pay tremendous importance to the महापदुक mantra which incorporates upaniShadic महावाक्य-s [*1]. There is no evidence that the महापदुक mantra was central to any kula teaching. But it does resemble the ene-mene-dapphe-दडप्फे [*2] of bauddha-s being incorporated as a mantra.

From early times the ब्राह्मण-s migrated out of their स्मार्त baseline to develop new systems of philosophy or knowledge. In some cases they converted entirely to these systems, like the nAstIka-matas, or in other cases created versions that spanned a spectrum from purely स्मार्त to something which might contradict स्मार्त norms[*3]. Likewise, in श्रीविद्या's development from early on there were forms in line with स्मार्त norms (e.g. प्रपञ्चसार and shAradA-tilaka) as well as those transgressing स्मार्त norms (e.g. परशुराम kalpa sUtra-s; though from the very adoption of a मीमांसक style is indicative of the brahminical origins of the PKS), both systematized by Brahmins of ultimately स्मार्त origin. The पञ्च-makara might not necessarily be adopted by those who remain स्मार्त because their norms are violated by the 5 ma-s, but the principle of कामकल worship and the ShaT-chakra-s, both of which are drawn plainly from the original kula doctrine, are retained at the heart of श्रीविद्योपासन by even these स्मार्त-s. But nowhere in any of their early sources do we find the महापदुक and upaniShadic statements. Now the स्मार्त-s appear to have created another set of Agama-s much closer to their own pattern of worship -- the शुभागम पञ्चक. But interestingly these hardly have any popularity compared to the root tantra-s which follow the unadulterated kula doctrine. Now I have only seen fragments of these and these are clearly later in provenance than the early kula texts.

advaita of the early kaula-s of matsyendra's successors does not mean the same as the vedantic advaita. There a-dvaitam appears (at least to me) to be interpreted as the lack of duality in worship -- thereby allowing the more kaula elements (the pure-impure distinction breaks down). So the vedantic advaita does not in anyway appear to be inherent to the kula doctrine.

So, in conclusion, I believe the evidence favors the advaitins of the शङ्कर tradition have only secondarily adopted श्रीविद्या and are behind the creation of the महापादुक mantra. They have even gone to the extent of claiming that one can get the guru-status or a higher level मन्त्राधिकार only with this महापादुक दीक्ष. My personal opinion is that every one is entitled to their own tradition. So, if some one is affiliated with a शङ्कर मठ they may follow their मठाधिपति and Acharya-s, but the claims regarding महापदुक are not binding on all तान्त्रीक-s.

[*1] प्रज्ञानं brahma; tat-tvam-asi; अयं AtmA; brahma aham ब्रह्मास्मि; .\
[*2] धर्मत्रात rendering the words of the तथागत in देवभाष states in the उदानवर्ग: ene mene तथा dapphe दडप्फे cheti | सर्वस्माद् विरतिः पापाद् दुःखस्यान्तो niruchyate || 26.18\
[*3] Thus we see Brahmins of originally स्मार्त origin diversifying in their practice into, for example, non-mantra मार्ग पञ्चायतन शिवोपासन with otherwise purely स्मार्त norms, स्मार्त mantra-मार्ग शिवोपासन (as in प्रपञ्च-sAra), लाकुल-s (and their कालामुख successors) who observed स्मार्त norms but additional unique forms of शिवोपासन, सैद्धान्तिक-s who followed shivadharma in addition to all the स्मार्त norms (though considering shivadharma primary) and भैरवाचार mantra-mArgin-s who transgressed स्मार्त norms. Thus, in these diverse movements the scholastic tradition emerged from the स्मार्त ranks.


