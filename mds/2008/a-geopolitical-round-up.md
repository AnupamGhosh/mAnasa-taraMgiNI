
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A geopolitical round up](https://manasataramgini.wordpress.com/2008/08/12/a-geopolitical-round-up/){rel="bookmark"} {#a-geopolitical-round-up .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 12, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/12/a-geopolitical-round-up/ "5:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

An elderly white American Hindu activist was going to a temple. On the way she had an "accident" that nearly killed her. Was it in accident? Some people know the real answer. वङ्ग paNDita smiled and said there are certain things in geopolitics that cannot be said in public -- they are too dangerous. In my early days in the capital of the म्लेच्छ desha, when there was a much freer environment there, I discovered certain things that are pretty obvious, but most observers simply fail to notice. My lost friend, the non-जामदग्न्य vatsa, from the southern realms of म्लेच्छ-desha had also, interestingly, discovered some of these things. वङ्ग-paNDita cautioned him of the extreme danger in publicly discussing some of those issues, but he did not desist and promptly faced the consequences. वङ्ग paNDita had long predicted certain geopolitical events with a time line. Hence, I could not help thinking of him when the Caucasian flare up occurred over the past week. In our long discussion, he had correctly predicted certain geopolitical alignments and predicted how a certain alliance will begin the second push against the Khaganate of the Rus. वङ्गीश and me watched with childish excitement that shocked the other adults around us a documentary on the battle of Stalingrad where the Khaganate of the Rus finally destroyed the Germans, signaling the end of their empire. At the end of it वङ्गीश remarked: "The Rus will be pressed hard again. The Americans would send troops to hold a joint military exercise with the Georgians and this would trigger the Khaganate to respond in an unprecedented way. The Russian air force and navy might have weakened but their land army has always been their strength --that is the lesson of Stalingrad. Georgia and such places are the best options for a show of strength to the Americans because it will be the land army operating in their backyard. After all the bases where the Georgians are letting in the Americans were built by the Russians. This will be pay back time for Kosovo..." Not surprisingly, the Americans sent 1000 men to participate in a joint exercise with the Georgians. Buoyed by this vacuous display (remember, one of the American soldiers in this exercise said in his interview that he did not know where Georgia was on the map of the world -- he only knew of an American state with that name!) very provocatively named "Exercise Immediate Response 2008" the Georgians decided to put their lessons to immediate practice by attacking Russian citizens in adjacent ethnically distinct enclaves. We are now watching the results unfold.

Why would the Americans impel their allies to display such brinkmanship? When वङ्गीश and me exchanged notes we found had the answer for this.

In the meantime in the desh we are witnessing several events in need of some attention. The worshipers of the big black stone launched two attacks: one in the city of my birth, and another in the लाट country. There was no real response, instead the dasyu rulers of the land want to equate the SIMI with RSS. After all king युधिष्ठिर had said that people might see the son of विवस्वान् visit others houses but will think they are safe till the moment he arrives, noose in hand, at their own. The other event concerns the rebellion of amareshvara -- Hindus cannot even control their own temples in their own land. Here again we see that the taskara-राज्य is siding with the तुरुष्क-s -- it is amazing how the tail wags the dog! The world hardly gets to learn the reality of these atrocious events in jambupura -- only the Hindi language press has been reporting the shocking reality -- the नपुंसक Indian army and police are harassing and killing Hindus. The Anglophone Hindu elite will be jolted out of their complacence if they go and see the Hindi newspapers for graphic pictures of these atrocities. Truly जोनराज's view that this is all an outgrowth of the sapling of हर्षराजा seems right seeing the Indian army and law enforcement acting like in an Islamic state. Will these events incite the Hindu martial ardor to erupt? Unlikely, but I will happy to be proven wrong. It is striking to note how the Hindus used the "democratic process" to bring upon themselves a government of taskara-s and कुलुञ्च-s who are वेश्यापुत्र-s.

"[*दुग्धाब्धि धवलं तेन सरो दूर-गिरौ कृतं ।\
अमरेश्वर यात्रायं जनैर् अद्यापि दृश्यते ॥*]{style="color:#000080;"}"\
कल्हण's राजतरंगिणि 1.267


