
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [skanda-सद्भाव:observations on iconography](https://manasataramgini.wordpress.com/2008/07/30/skanda-sadbhavaobservations-on-iconography/){rel="bookmark"} {#skanda-सदभवobservations-on-iconography .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 30, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/30/skanda-sadbhavaobservations-on-iconography/ "5:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Chapter 11 of the skanda-सद्भाव describes the making of images of skanda. 8 substances are recommended for making images:\
[सुवर्णं रजतं ताम्रं मणयः कंस एव छ ।\
मृत्तिका दारु पाषण इत्य् अषटौ योनः स्मृताः ॥]{style="color:#99cc00;"}\
i.e.: gold, silver, copper, gemstone, bell-metal, clay, wood and stone.

[सौवर्णा पुष्टिदाज्ञेय राजती कीर्तिवर्धना ।\
धन पुत्र प्रदा प्रोक्ता ताम्रजा प्रतिमा सदा ॥\
सौख्य प्रदातु मणिजा कंसजा पाप नाशिनी ।\
मृण्मयी सर्वदात्वर्च्या दारुजा लक्ष्मि-वर्धना ॥\
आयुर्-बल प्रदा नित्यं शैलजा प्रतिमानघ ।\
आभिछाराय विहिता प्रातिमासु तदा मयी ॥\
प्रतिमा द्विविध प्रोक्ता स्थावरा जंगमेति छ ।\
कर्तव्यं मणि लोहाभ्यां जंगमा प्रतिमा बुधैः ॥\
कर्तवयं स्थावरा नित्यं प्रतिमा सर्व योनिभिः ।]{style="color:#99cc00;"}

As is obvious from above the different materials are supposed to confer different benefits. Movable icons are made of gemstone or metals, whereas the fixed images may be made using any of the materials. The use of mud is an atypical element for स्थावर icons, as it entirely went out of vogue in later iconopoiesis. Then there is detailed account of iconometry. Basic iconography is summarized in the below figure:
```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3264/2715525689_352f86b0bf_o.jpg){width="75%"}
```{=latex}
\end{center}
```



Two major types of icons are described:

 1.  the shaishava type: 2 hands; holding pot and bell; sky-clad or with any other vesture; peaceful countenance; legs decorated with tinkling anklets; shaven head; leaf bracelets, lotus \[garland] and darbha grass; wearing यज्ञोपवीत.

 2.  guha: 2 or 4 hands; always 4-handed if hold vajra and shakti; of the age 16; wearing red-yellow pants; vermillion in color; कटक and केयूर bracelets and auspicious necklaces.

Thus the basic iconography of these कौमार temples is rather simple in comparison to the iconography typical of many extant south Indian कौमार temples. The simple shaishava form is a precursor of what is seen in Palani (the current temple and icon is saiddhanta-influence though). The Pazhamadhurcholai temple main icon resembles the above guha form.

Icons of the accompanying देवी-s (I have not seen such an arrangement in any of the south Indian कौमार shrines I have visited):

 1.  देवी प्रज्ञाभा: To the north of कुमार; color: like a champakA flower (light yellowish green); well-adorned in ornaments.

 2.  देवयानी: To the south of कुमार; color: like a pomegranate flower (crimson); well-adorned with ornaments.

 3.  विद्या and देवसेना: said the stand to the back of कुमार with चामर-s and well-adorned.\
On the sides of the archana-पीठ or pedestal:

 4.  धृति: to the south; colored red.

 5.  मेधा: to the north; colored black.\
Both are described as "घूर्णमाने ubhe सदा".\
Icons of the vipra-s:\
They are seated in बद्धवर्मासन on lotus pedestals.\
अङ्गिरस- red; भृगु- white; वसिष्ठ- golden (अक्षर-वर्ण); atri- white; नारद- dark green; gotama- silver; भरद्वाज- yellow; agastya- black; विश्वामित्र- red.\
Icon of धूर्तसेन: red; welcoming gesture in one hand and other hand placed on hip.\
Icons of द्वारपाल-s: red in color.

Another distinctive element of the iconographic chapter of the skanda-सद्भाव is the mention of chitra-kala or picture-making. The chitra-kala section in the विष्णुधर्मोत्तर पुराण describes at some length the making of paintings including those of कुमार. The classification of the "pictures" here is in 3 types namely chitra, ardha-chitra and चित्राभास, ranked in the order of their respective effectiveness. By comparison to kAshyapa-shilpa and its late derivative श्री-कुमार's shilpa ratna, we may understand chitra to actually mean a 3D icon, ardha-chitra to be a relief and चित्राभास to be the actual painting. This is consistent with a similar classification also found in the पाञ्चरात्र tantra-s and सैद्धान्तिक-aligned shilpa manuals. Others have alternatively interpreted chitra and ardha-chitra respectively as high-relief and low-relief. The SS is also parallel to the kAshyapa shilpa and पाञ्चरात्र texts in stating that ardha-chitra-s might only use clay, wood and stone, whereas chitra-s may use all material. This is consistent with their interpretation as 3D icons and reliefs.


