
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The cracker box यक्षिणी](https://manasataramgini.wordpress.com/2008/07/17/the-cracker-box-yakshini/){rel="bookmark"} {#the-cracker-box-यकषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 17, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/17/the-cracker-box-yakshini/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It was 1989, when we were still young. कृष्ण शूद्र noticed the violent sphigshiras approaching us and panicked. He believed that we might have hatched a plot that dashed his chances of snaring the eye-freezing kamala-नयना चम्पकनासा. Hence, he sought his revenge. We rapidly took to our "gardabha-s" and fled from the scene. We then applied ourselves to the powerful feral ब्राह्मण शाण्डिल्य to assemble forces for a defensive operation. A show of arms occurred on the foothills of the वानर-parvata and our पक्ष emerged victorious. Flushed with the joy of victory we went to wander in the expanses of the वानर-parvata and विष्ट-parvata beside it. While descending from the stinky mala-parvata, our eyes fell upon आर्जवकेशी मार्जारनेत्री whose tale we have alluded to early in the history of these pages. My companions were transfixed; I was intrigued. They moved towards her to make conversation -- each taking their chance as a Mongol horseman aiming at a distant mark on the horizon. But that day it seemed there were no Khasars amongst them. We stood in a neutral state, observing the strictures of dharma. Just then a stately ratha arrived and was parked in the shade of the tree. From it emerged she who was like the bahu-chitraka-matsya on the banner of अनङ्ग swimming in the midst of लतामणी-प्रवाल in the दक्षिणाम्बोधि. Even we were forced to utter: "एशा rolamba-bindu-जाल loma-जालम् अनुरागाकारो निशाकरो vijita kamalam मुखं; कामद्वजायमानो matsyo भार्यायुतो.अस्क्षि-युगलं; sakala-सेनाबल वीरो पश्चिमाम्बोधि-pavano निःश्वासः pathika हृद्-bheda करवालः प्रवालश्-चाधर वाग्दलं|"

Turning to आर्जवकेशी she said some weighty words. Neither आर्जवकेशी nor most of our companions were able to understand much of what she said. But our companions' manliness was aroused and tried to claim some understanding of her words. We understood her profound words and wanted to say something to her but she was hardly impressed with our band and asking आर्जवकेशी to get into her ratha departed, leaving us all thunderstruck. Then the शूद्र golashiras and the feral ब्राह्मण शाण्डिल्य turning to us said that life had lost its meaning to them. Looking at their faces I thought they were ready to perform the पुरश्चरण of स्कृक् Chindi all the way to its conclusion if I had only given it to them. I thought myself to be an achala, but even I was drawn like a star towards a black hole. Then the skirmishes of the western hall took place and I caught another glimpse of that लीलावती-kula-चूडामणी and was in the state of a bear gazing at large bee-hive atop an enormous rain tree. But performing our yoga and reining in our senses we moved on to savor the दीपावली vacations.

Our diligent companions engaged in guzzling the enormous texts of the curriculum in expectation of the great struggle on the field of द्वादशान्त. But we entirely lacked their proclivities, instead engaging in activities that to them seemed bordering on the insane -- making balls of out the gum of the rain tree pods and playing with fireworks as befitted the season. It was during one such session that ज्येष्ठदन्त hrasvaroman came to our regions on his new ashva and gave us a fire cracker box. It was different from all others I had seen and contained a few crackers of the micro-लक्ष्मी type. I tied the wicks of all the crackers in the box together and set off an ear-splitting fulmination, which impressed even the महरट्ट high-explosives masters who reigned in our habituation. As the कोलाहल from the pyrotechnics settled down, my attention turned to the cracker box again. I took it and examined it carefully and was transfixed by it. I realized that it contained the secret to महासुख, the cushion which saved ययाति in his fall. We placed it in the balcony and waited for the fumes of दीपावली that polluted the air to clear and let in the light from the quivering कृत्तिका-s adorning the eastern welkin. On the first clear night we observed lambda Tauri and tired from the exertions at our 4 inch reflector we settled for some rest. Our eyes fell upon the cracker-box -- being brazenly superstitious we knew we had received a signal. We made it the object of our dhyAna and invoked the glorious यक्षिनी abhi-हृष्टि in it. After intense साधन the form on its cover came alive and the bewitching अभिहृष्टि came forth to us and in voice putting the lyrebird to shame told us to proceed on our ashva in a circuit between वानर-parvata and the hill of चण्डिका. She told us that we would first encounter a terrifying डावी and then after having transcended her we would encounter that one who delights. Following अभिहृष्टि's instructions we arrived at the lane where मनाभिरुचिता lived. She was surrounded by several others. Some them recognized me and asked what we had done the previous day. We answered them in earnest. Most were amused with a sense of Schadenfreude; lambashiras was alarmed, but with her there was a resonance. She came over to our पक्ष. With that being achieved she suggested that we should perform that experiment we had long thought about but never performed. What we did is part of the story named "reanimation of the corpse of the great Joseph von Fraunhofer".


