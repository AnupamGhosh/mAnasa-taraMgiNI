
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The train of thoughts to the horned gods](https://manasataramgini.wordpress.com/2008/07/10/the-train-of-thoughts-to-the-horned-gods/){rel="bookmark"} {#the-train-of-thoughts-to-the-horned-gods .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 10, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/10/the-train-of-thoughts-to-the-horned-gods/ "5:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our aim was to finish the next installment on the potential medical activities of the सैद्धान्तिक-s but it did not happen (आरम्भशूर indeed). Some thoughts of from a long past time settled upon us like the deluding mada of surA, pushing away the thoughts of the complicated operations required in the connection of the Brazilian collaborators. We wondered how emperor ययाति might have felt on the last days in svarga. Then we wondered if their might be some one like a माधवी somewhere to send us back to svarga after our headlong plunge from the heady heights of dyaus. Our reverie was broken by a call from ST. In संध्य भाष we expressed the following: "He passed by the diamond shop with not a single कार्षापण in his pocket. All he could do was to see the diamonds". ST replied: "He did not even see the diamonds in the shop. It was merely his delirium in the dehydration of the desert". Then there was a resonance between us -- we both drifted to the long past days. Having mounted my "ashva" with ST we went to the little complex of houses where कृशमुख and ghanodara lived and parked it there. We then climbed up the hill overlooking the विद्यापीठ, which bore the shrine of चण्डिका. We first stopped at the shrine of the awful विनायक of orange form and meditated upon his mantra. Then we moved upwards and reached the shrine of चण्डिका and meditated upon the विद्या of the great goddess. Having exited via the southern door we moved upwards past the लिङ्ग to कपालक्षेत्र. The नक्षत्र of the day was about to be taken to the realm of पूषण्, when we reached the circlet of stones. The rays of the sinking orb diffracted off ST's कर्णाभरण casting rainbows all around us. I began narrating the tale of the climb and the fall of vidruma and कनिष्ठ शूद्र to her. ST in turn began telling me of a book by Octavio Paz -- The Monkey Grammarian (Yes we visited [Octavio Paz](https://manasataramgini.wordpress.com/2007/02/15/the-greatest-temple-of-shiva/) again). After that we descended on the other side of the giri and visited a little shack where we sated our thirst with इक्षु-कीलाल served in dirty glasses.

As we lingered in the कीलाल-शाल ST asked me this profound question: Why do so many Eurasiatic gods have horns? That mystery figure on the Indus seals, Marduk, Teshub, नेजमेष, Ashu-garuDa, Cernunnos, Enlil, indra in the RV(?). The same question had come to me in the following experience: With the पूर्वज we had gone to see our clansman who was the makhin. He led us to the archaeological museum of his institution. There we saw an extraordinary pot from the Chalcolithic of Gujarat which contained an image of this horned Indus deity. The horns remained in my head. I discussed this with a friend again several years later. We must "study" this pan-Eurasian phenomenon in iconography I remarked to ST.


