
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [15th August: A journey](https://manasataramgini.wordpress.com/2008/08/15/15th-august-a-journey/){rel="bookmark"} {#th-august-a-journey .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 15, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/15/15th-august-a-journey/ "12:40 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

61 years ago the Hindus ended the म्लेच्छ conquest of their land. Today most of us feel the victory was incomplete and many others feel it might have even been empty. Within the true Hindu elite (not the modern Indian elite) there has been the following realization which I have articulated many times on these pages: We were in the process of reversing the earlier invasion i.e. that of the तुरुष्क-s and were close to rolling back Islam. The process had inflicted heavy damage on us, and we were not in a position to take on a second invasion. It was then that म्लेच्छ-s fell upon and we shamefully succumbed to their assault, and it took us another 140 years to revert that. But in the process the तुरुष्क-s got a new lease of life and seized our western and eastern lands. The process cut deep into our identity because it removed the sindhu from our consciousness, divided the land of the five rivers in the midst of genocide, removed the land of पाणिनि from our sight. It also drove the final nail into the coffin of Kashmir, the land of some of our greatest thinkers. To add to these physical disfigurations, we remain in the state of mental colonization that ironically John Woodroffe so eloquently diagnosed. To add to this, there was the third invasion by a variant of the same meme as Mohammedanism or Isaism, namely Marxism. This invasion recently claimed Nepal the land of many of our Agama-s. These depredations were associated with the loss of deva-भाष the vehicle of our civilization and also the "light of Asia" itself. Our inability to restore देवभाष and reacquire the spirit of its traditions is manifest in the post-1947 depravity of the land and the reigns of the taskara राजन्-s and dasyu-s that over took the land.

I had conversations in the past with my late grandfathers of the period of म्लेच्छ rule through which they had lived. For them it was a tumultuous transition -- verily a fulfillment of the पौराणिc prophesies. A generation before them was one of great agnichit-s, the smoke of whose sacrificial fires reached the skies, महामहोपाध्याय-s who had delved into the intricacies of काव्य, मीमाम्स and nyAya-वैशेषिक and able मन्त्रवादिन्-s. But for them it was a grim struggle for survival. Applying the abilities inherited from their ancestors to secular pursuits they were mainly engaged in salvaging themselves -- they had reached the proverbial state of "does it matter if it is rAma or रावण who is reigning". Thus, the very presence of the म्लेच्छ-s was a thing of the background. But in all this at least my maternal grandfather briefly felt a surge of nationalism due to the words and actions of two ब्राह्मण warriors who hardly get any attention in the official histories of the great संग्राम -- Vanchinathan and Varahaneri Venkatesa Subrahmanya Iyer.

Vanchinathan letter was particularly a source of inspiration; an excerpt from which run thus: "3000 youths of this brave country have taken an oath before goddess काली to send King George to naraka once he sets his foot on our motherland. I will kill Ashe, whose arrival here is to celebrate the crowning of King George in this glorious land which was once ruled by great संरट्-s. This I do to make them understand the fate of those who cherish the thought of enslaving this sacred land."

Upon the death of VVS Iyer the great patriot VD Savarkar wrote: "For indeed he was a pillar of strength, a Hindu of Hindus, and in him our Hindu race has lost one of the most exalted representatives and perfect flower of our Hindu civilization -- ripe in experience, and mellowed by sufferings and devoted to the service of men and God, the cause of the Hindu Sanghatan was sure to find in him one of its best and foremost champions in Madras."

\~*\~

We had earlier narrated the tale of the predecessor who located the lost temple of कुमार in Chebrolu and initiated that ब्राह्मण, whose descendent was to attain "eko मानुष आनन्दः", into the विद्या of त्रिपुरा. He himself was initiated into the विद्या of त्रिपुरा by a great mantra-वादिन् from the outskirts of Madhurai. This मन्त्रवादिन् it is believed was in the line of the मधुराज-yogin and आद्यनाथ, who themselves were in the line of the great abhinavagupta. That mantra-वादिन् remarked to him that unless the first वर्ण was to assume its natural role of leadership as the head of the puruSha from the himaparvata-s to the mahodadhi the land would not recover from the म्लेच्छ-s. It is striking that Tilak was to use nearly the same language some years down the line.


