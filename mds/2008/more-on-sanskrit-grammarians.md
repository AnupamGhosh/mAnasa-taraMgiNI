
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [More on sanskrit grammarians](https://manasataramgini.wordpress.com/2008/04/26/more-on-sanskrit-grammarians/){rel="bookmark"} {#more-on-sanskrit-grammarians .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 26, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/26/more-on-sanskrit-grammarians/ "4:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The existence of aindra grammar which apparently declined in due course due to पाणिनि's efforts has been discussed extensively in the past by Burnell in his monograph on the topic. While things might have changed in certain directions in our understanding of the evolution of formal grammar of संस्कृत, the existence of the aindra system as gleaned by Burnell is not in much doubt. In our opinion there is also not much doubt that the earliest Tamil grammar, the तोल्काप्पियं was modeled after the aindra व्याकरण of संस्कृत. However, there have been a number of related controversies on these issues that keep coming up: 1) Doubts on the age of the तोल्काप्पियं -- most of its major commentaries are medieval. Hence, some have declared that it is not at all an early text but actually a late text closer to the medieval period. 2) Age of the aindra grammar itself -- here again there are claims that it is a post पाणिनिअन् innovation rather than predating him as Hindu tradition generally holds.

As we mentioned earlier दुर्गसिंह, learned commentator of the nirukta was clearly aware of the aindra grammar and uses it in his analysis which is generally consistent with सायण's own knowledge of it. Hindu tradition also links the transmission of the aindri वाक् to the god कुमार (related to the famous tale in the बृहत्-kathA/kathA-sarit-सागर). Apocryphal versions of this (still not without value) are also offered by the learned Tibeto-Mongol Lama तारनाथ in his history: He states that there was a ब्राह्मण named saptavarman, who a contemporary of नागार्जुन and कालिदास \[a typical example of तारनाथ's pseudo-history]. He asked षण्मुख to reveal the aindra grammar to him, and upon skanda merely uttering the first sutra of the grammar "[सिद्धोवर्णसमाम्नायः ।]{style="color:#99cc00;"}" the rest of the grammar of 25,000 verses was miraculously transmitted to him. He also mentions that a ब्राह्मण named indradhruva obtained the same from indra. The enormous size mentioned by the Lama तारनाथ is also repeated by other late Hindu writers like devabodha who compares the aindra to the sea whereas पाणिनि to the depression of left by the cow's hoof in size. तारनाथ is also conversant with the apocryphal belief common in India that the पतञ्जलि of the महाभाष्य was the अवतार of the snake sheSha (likewise the eponymous author of the yoga sUtra is also considered an incarnation of the same snake). Thus, on one hand we see a lay belief, prevalent in greater India at the time, being transmitted by the Tibetan Lama to Mongolia. On the other hand, little earlier than तारनाथ, the great vedic commentator सायण preserved the one crucial piece of evidence that firmly places the origins of the aindra व्याकरण in the ब्राह्मण layer of the shruti. This lost ब्राह्मण cited by सायण appears corrupt (at least in my copy) but is beyond doubt a ब्राह्मण passage of vedic provenance (especially given that the majority of सायण's ब्राह्मण citations are entirely verifiable): [वाग् वै पराच्य्-अव्याकृता ऽवदत् ते देवा इन्द्रं अब्रुवन्न् इमम् नो वाचम् व्याकुर्व् इति । सो ऽब्रवीद् वरं वृणे मह्यं चै ।एवै ए।ष वायवे च सह गृह्याता इति तस्माद् ऐन्द्रवायवः सह प्रगृह्यते । ताम् इन्द्रो मध्यतो ऽवक्रम्य व्याकरोत् । तास्माद् इयं व्याकृता वाग् उद्यत इति ।]{style="color:#99cc00;"}

A good candidate is the lost ब्राह्मण section of the charaka saMhitA of the कृष्ण yajurveda from which सायण has made other citations. It appears that even in the ब्राह्मण period the general assumption was the speech or language was originally not systematized via grammar. The deva indra generated a grammar that allowed the analysis of speech. So the idea of the aindra-व्याकरण existing is indeed pre-पाणिनिअन् and present in the shruti itself. Such a system might actually go back to an earlier Indo-European model, which at least predates the split of Greek+Balto-Slavic+Indo-Iranian and might be related to the earlier Indo-European concept of the divine speech which survives in the Indic world in the form of संस्कृत being deva-भाषा.


