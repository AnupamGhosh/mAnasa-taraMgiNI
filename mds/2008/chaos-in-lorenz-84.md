
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Chaos in Lorenz-84](https://manasataramgini.wordpress.com/2008/12/12/chaos-in-lorenz-84/){rel="bookmark"} {#chaos-in-lorenz-84 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 12, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/12/chaos-in-lorenz-84/ "7:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3061/3102267416_af29de9b27.jpg){width="75%"}
```{=latex}
\end{center}
```



