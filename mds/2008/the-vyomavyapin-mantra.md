
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The व्योमव्यापिन् mantra](https://manasataramgini.wordpress.com/2008/02/10/the-vyomavyapin-mantra/){rel="bookmark"} {#the-वयमवयपन-mantra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 10, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/10/the-vyomavyapin-mantra/ "11:11 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The celebrated mantra of the Urdhva srotas, the व्योम्व्यापिन् comprises of 368 syllables. Below is the version I am familiar with. Examination of 3 manuscripts vy1 from Chidambaram, vy2 from Madurai and vy3 from Tiruvatuturai reveals certain variations from version I have. I have thus far not been able to lay my hands on the surviving Kashmirian manuscripts of the same. The deletion in the same place in the vy1 and vy2 manuscripts appear to result from a break in the original leaf from which both were likely to have been copied. Some of these variants might probably be used validly. An oral version of a shaiva priest I observed had certain further peculiarities, which could all be attributed to errors due to the linguistic distortions produced by the influence of drAviDa भाष.

*OM vyoma-व्यापिने vyoma-रूपाय sarva-व्यापिने शिवाय अनन्ताय अनाथाय अनाशृताय ध्रुवाय [1] शाश्वताय योगपीठ-संस्थिताय nitya-yogine dhyAna-hArAya OM नमः शिवाय sarva-prabhave शिवाय ईशान-मूर्धाय tat-puruSha-वक्त्राय aghora-हृदयाय वामदेव-गुह्याय सद्योजात-मूर्तये [2] OM namo नमः गुह्यातिगुह्याय goptre अनिधनाय sarva-योगाधिकृताय sarva-विद्याधिपाय ज्योतिरूपाय parameshvara-पराय [3] अचेतनाचेतन vyomin vyomin व्यापिन् व्यापिन् अरूपिन् अरूपिन् prathama prathama tejas-तेजः ज्योतिर्ज्योतिः arUpa अनग्निः अधूम abhasma अनादे na na na na dhU dhU dhU dhU OM भूः OM भुवः OM सुवः shiva sharva [4] anidhana nidhana nidhanodbhava [5] परमात्मन् [6] maheshvara महादेव सद्भावेश्वर [7] महातेजः योगाधिपते मुञ्च मुञ्च prathama prathama sharva sharva bhava bhava bhavodbhava sarva-भूत-sukha-prada sarva-सान्निध्यकर [8] brahmA-विष्णु-rudra-bhuvana-त्रयार्चित अर्चितार्चित असंस्कृतासंस्कृत पूर्व-stitha पूर्व-stitha साक्षिन् साक्षिन् kuru kuru पतङ्ग पतङ्ग पिङ्ग पिङ्ग ज्ञान ज्ञान shabda shabda सूक्ष्म सूक्ष्म shiva sharva sarvada [9] OM नमः शिवाय*

[1] vy2= शिवाय अनन्ताय अनाथाय अनाशृताय ध्रुवाय missing\
vy1= अनाथाय अनाशृताय ध्रुवाय missing\
vy3= अनन्ताय<->आनन्दाय\
[3] vy3= just परमेश्वराय\
[2] vy2= additional नमः\
[4] vy3= shiva sharva missing\
[5] vy3= has shiva sharva here instead\
[6] vy2= परमात्मक\
[7] vy3= sarveshvara\
[8] vy3= brahmA-विष्णु-rudra-परार्चित अस्तुतास्तुत\
vy1= brahmA-विष्णु-rudra-para अनर्चितानर्चित\
[9] vy2=OM namo नमः OM namo नमः\
vy1=OM namo नमः OM शिवाय namo नमः\
vy3=OM नमः शिवाय OM namo नमः


