
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [उपासन of the 11/12 विलासिनी-s](https://manasataramgini.wordpress.com/2008/05/25/upasana-of-the-1112-vilasini-s/){rel="bookmark"} {#उपसन-of-the-1112-वलसन-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 25, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/25/upasana-of-the-1112-vilasini-s/ "3:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the (apparently) extinct and distinct branches of the kula system is the kula of the विलासिनी देवी-s. Few modern उपासक-s appear to be aware of it; in part this might be due to the extreme secrecy observed in the kula circles in imparting this system. It is closely allied with श्री-kula: kaula bAlA उपासन in some ways and regular कमेश्वरी in others (note the mention in the LSS). Once it might have had a pan-Indian (or at least widespread distribution) distribution with centers in Kashmir-Himalayan zone, Bengal and the drAviDa country. The विलासिनी kula was also transmitted to the nAstIka-s whole-scale and incorporated with little change (retaining the description of the देवी-s and also the consort bhairava without even an attempt at altering the shaiva iconography).

The first lineage of विलासिनी उपासन is that of विरूप. She is invoked in the middle of her chakra either as: 1) two-handed holding a kulisha and a lotus 2) two handed with a कपाल and a vajra. She is red in color like that of dawn and has three eyes, two of which are half-closed expressing the supreme bliss of the शृन्गार rasa and bordered by pretty eyelashes and brows. Her midriff is lean and well-proportioned with three pretty folds (त्रिवलिः). Her form is that of a passionate maid the prime of her youth. She may also be worshiped in conjunction with aghora-bhairava, whom she embraces by entwining her arms around him, even as he, the knower of her bewitching thighs, hugs her ample breasts. She turns away from the crescent moon on his head. Her secret mantra incorporates the vagbhava, mAyA and manmatha bIja-s.

She is surrounded by the band of 10 विलासिनी-s associated with the 5 महाभूत bIja-s:\
पृथिवि bIja: 1) पातनी -- chakra and कपाल; 2) लोचना -- kartari and खट्वाङ्ग; both are yellow in color.\
jala bIja: 3)मारणी- kulisha and कपाल; 4)मामकी -- kartari and खट्वाङ्ग; both are black in color\
agni bIja: 5) आकर्शणी -- lotus and कपाल; 6)पाण्डारवासिनी -- kartari and खट्वाङ्ग; both are red in color, but the later wears white clothing.\
वायु bIja: 7) नर्तेश्वरी -- खड्ग and कपाल; 8) tArA -- kartari and खट्वाङ्ग; both are green in color.\
आकाश bIja: 9) पद्मज्वालिनी -- 4 hands, अङ्कुश, brahmA's head, कपाल and दण्दायुध, grey in color; त्रिमुखी- 3 heads, kartari and pAsha, grey, red and white color.

The second lineage of विलासिनी is that of the siddha shabhara विद्यानन्दनथ from the cave near श्रीशैलं. This is the महाश्रिङ्गार-विलासिनी or गुह्यविलासिनी. He himself was initiated into this kaula practice by the siddha करुणानन्दनाथ. महाश्रिङ्गार-विलासिनी is always invoked in conjunction with her bhairava consort narteshvara, who is two armed holding the पिनाक and the पाशुपत missile and bright red in color. He is conceived as eternally young and the epitome of male handsomeness, wearing a bright pearl necklace. His eyes are half-closed in the bliss of चिदानन्द arising from maithuna with his कुलाङ्गना. गुह्यविलासिनी is invoked in a highly erotic form with two arms holding a kartari and a pAsha, wearing a pearl necklace and अङ्गना-priya flowers as decoration. She is in a tight embrace with her husband and trembling in the intoxication of शृङ्गार-bhoga with her face lit up by bewitching smiles. They are thus conceived as residing on the pinnacle of the beautiful mountain of चित्तविश्राम. She is invoked by the secret mantra of 5 syllables which are like the पञ्च-तन्मात्र सायक of कमेश्वरी. One who performs her साधन and attains siddhi of her mantra is able to perform षट्कर्म-s and also attains अञ्जन-siddhi, गुटिकासिद्धि and स्त्री-वशिकरणं. It is also the sure-shot path to the experience of samAdhi with which the object and subject distinction dissolves into a condition of bliss. विलासिनी and narteshvara are surrounded in their मण्दल by two आवरण. In the first of them resides the highly erotic विद्याधरी. She has 3 heads and carries a कपाल from which she drinks wine and a खट्वाङ्ग. She is brilliant red in color and clothed in only a garland of नागकेश flowers and bears long thick flowing untied tresses, with her youthful body excited with erotic pleasure. Her secret mantra is similar to that of Chinnamasta. In the next आवरण are invoked the 10 विलासिनी-s as in the first vidhi of विलासिनी.

The deshika performing the महाश्रिङ्गार-विलासिनी prayoga chooses a beautiful secluded house, garden or mountain and retires there with his initiated कुलाङ्गना on an अष्टमी night. After having a bath and perfuming themselves they invoke रम्भा and तिलोत्तमा to confer दीक्ष for the साधन on them. Then, he and his दूती invoke the देवता-s of the आवरण and prepare themselves for maithuna by performing the नवपुष्पी. Then the कुलाङ्गना unifies her identity with विलासिनी and the deshika with narteshvara. Having done so, they both invoke the मूल mantra and see the blazing mayukha-s emanating from the mantra and melting the whole universe into a uniform sea of blood in the middle of which the deshika and his कुलाङ्गना are unified with the bhairava and the देवी. Then they begin the special cyclic japa of the विलासिनी मूल mantra: the 5 blazing bIja-s of the mantra are conceived as emerging from the yoni of the दूती and entering the लिङ्ग of the deshika and passing through his body they emanate through his nose. Then they are taken in by the दूती through her nostrils and passing through her body again emerge from her yoni. This cyclical japa is continued for 500 times, followed by the reciprocal deployment of the vajroli mudra by both the दूती and the deshika. At the conclusion of this they experience the merger of their selves and experience the universe whirling around them and melting into a liquid gold. The unified दूती and deshika experience total oneness of the universe and see it dissolve into the vyoman. The vyoman itself dissolves into the pure chit and there only the experience of samAdhi.


