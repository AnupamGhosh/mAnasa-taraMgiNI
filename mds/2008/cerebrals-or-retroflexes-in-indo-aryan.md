
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Cerebrals or retroflexes in Indo-Aryan](https://manasataramgini.wordpress.com/2008/05/15/cerebrals-or-retroflexes-in-indo-aryan/){rel="bookmark"} {#cerebrals-or-retroflexes-in-indo-aryan .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 15, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/15/cerebrals-or-retroflexes-in-indo-aryan/ "4:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A question from R prompted me to look more closely at the formation of cerebrals or retroflexes in Indo-Aryan (i.e. T, Th, D, Dh, N, L, Sh). This is an important problem that Hindus should have studied but have tended to ignore. This is particularly compounded by the boorishness of several dim-witted modern Hindus who deny the Indo-Aryan incursion into जम्बुद्वीप or, still worse, deny the Indo-European monophyly. Hence, the pioneering studies in this regard were conducted by the म्लेच्छ linguists J.Wackernagel (who authored the great work Altindische Grammatik), T. Burrow and H.W. Bailey. Wackernagel correctly recognized several such cases like:\
Ati > ATi (Ati is the vedic form for 'duck' as seen in the तैत्तिरीय saMhitA)\
atati > अटति (wander, atati in ऋग्वेद)\
udumbara > उडुम्बर (a type of fig, former attested in vedic texs)\
pattana > पट्टन (a city, former attested in महाभारत)\
In each of these cases the word has a long history inside Indo-Aryan and there is no evidence that it was borrowed from Austro-Asiatic or Dravidian in the retroflex/cerebral form. Rather the original dentals appear to have become cerebrals within the history of Indo-Aryan. The more archaic form (usually vedic) is always dental with the late Sanskrit forms acquiring cerebrals. In a case like pattana we see this transition apparently happening between Epic Sanskrit and later classical Sanskrit. Wackernagel and Burrow also correctly noticed that in some cases this phenomenon also happened even earlier during the divergence of vedic from its other Indo-European sisters. For example:\
monile (Latin) => maNi (Skt) : jewel\
स्तूना (Avestan) => स्थुणा (Skt) : pillar

Such cases are also noted within the evolution of Indo-Aryan as Prakrit and Pali diverged from Old-Indo-Aryan. In the vulgar Pali we observe the following cases:\
उदार > उळार (great/liberal)\
budbuda > बुब्बुब्ळ (bubble)\
आशातिक > आसाटिका (insect larva; further आसाडी in Maharatti).

While there are numerous examples of this phenomenon that span the entire evolution of Indo-Aryan, right from its origin, it is clear that it is sporadic and not at all uniform. This differentiates it from other regular sound changes typical of other linguistic evolutionary events. Its distribution throughout the evolution of Indo-Aryan strongly argues against Prakritization of Sanskrit- a point correctly noted by Burrow. The dental archaisms are most prevalent in the RV suggesting an acceleration in this phenomenon during the transition from vedic to classical Sanskrit.

Once this phenomenon is recognized, it becomes clear that many of the words claimed to be loans from Dravidian and Austro-Asiatic into Sanskrit are actually endogenous developments via bifurcation of original dentals into a mixture that retained the old dental or became cerebrals. It also strengthens certain etymologies that were rejected before by Indo-Europeanists. Of the former the most important ones are:\
paNDita- The old linguists believed that it is a loan word derived from the Dravidian root paND- to mature. In support of this it was pointed out that Telugu had a word paNDa and Kannada a word पण्डे both of which meant 'wisdom'. However, they missed the point that panda as 'wisdom' was already there in Sanskrit from which Telugu and Kannada appear to have borrowed it because we do not find it in Tamil or other branches of Dravidian. In contrast, the dental form pand meaning 'wise counsel' is preserved in Persian. So it has retained meaning in Indo-Iranian rather than in the case of the Dravidian paND root. So paNDita emerged in Indo-Aryan from the paND root meaning wisdom/wise counsel; hence paNDita.

khaNDa, काण्ड, पटल, पाठ: Certain Indologists have claimed that these words, which are often first used in the indexing of vedic texts are of non-Aryan origin and might have either come from the Dravidians or the original Sindhu-Sarasvati peoples. But there are hardly any satisfactory etymologies given in support of this. Instead the cerebralization explains many of these. khad --'to break into pieces' is a root found in Avestan. This is seen without the nasal infix in खड्ग. पाणिनि and पतञ्जलि indicate the presence of the root in the perfect form चखाद. Thus, khaNDa is likely to come from an Indo-Iranian khad root than Dravidian or Austro-Asiatic.

काण्ड which is used to denote sections of texts also more generally means node or joints (काण्ड or दूर्व grass in yajurveda; अङ्गुलि-काण्ड for finger joint). Burrow noted the presence of the Greek ortholog Kondoi meaning 'knuckle' suggesting that काण्ड is a purely Indo-European word developing internally within Sanskrit. Likewise, पाठ and पटल are likely to be internal developments in Sanskrit rather than loans from Dravidian.\
Early Indologists had ridiculed the idea that the Sanskrit word kalevara 'body' had anything to do with the Latin word cadaver. But the vulgar dialect of the Bauddha tantra-s records the word कडेवर (meaning both body and corpse) and Pali has कळेवर which taken together support the presence of the form कडेवर which is likely to have emerged from \*kadevara through the cerebralization phenomenon noted by Burrow. So Latin cadaver and Sanskrit kalevara are indeed orthologs rather than the former being a "ka-ku-ki" type loan from the mystery old Indian language.

An evolutionary speculation: ऋग्वेद in particular and vedic in general have fewer instances of cerebralization and often preserve the older dental Indo-European condition. Many cerebrals that are found in later layers of Sanskrit can be established as intrinsic developments mainly through presence of Iranian counterparts but they are rarer or even absent in the core vedic layers (e.g. paNDita, pANi, मण्डल, खड्ग). We also note some other loans from Iranian into Indic like मरीस, दूस and सोढ for milk (the later again cerebralized). This suggests that the development of classical Sanskrit just prior to the epic layer was marked by a massive rise in these cerebral forms. The Iranian connection noted above suggests that a prominently cerebralizing dialect of Sanskrit emerged in the boundary zone between the Iranians and Indians and rapidly moved inwards influencing the Sanskrit of the kuru-पाञ्चल heartland. We suspect this movement was linked to the historical पण्डव-s and perhaps were even precursors of the great Megalithic movement through the sub-continent (thought this is less certain). Further tests of this hypothesis might yield useful understanding of early Indian history.


