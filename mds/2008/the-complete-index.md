
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The complete index](https://manasataramgini.wordpress.com/2008/11/12/the-complete-index/){rel="bookmark"} {#the-complete-index .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 12, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/12/the-complete-index/ "2:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[The complete index](https://manasataramgini.wordpress.com/the-complete-index/)

Of course it should be kept in mind that my views have evolved on certain issues over the past several years.

