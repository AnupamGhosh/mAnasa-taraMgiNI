
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The invocation of Ananda-bhairava](https://manasataramgini.wordpress.com/2008/11/09/the-invocation-of-ananda-bhairava/){rel="bookmark"} {#the-invocation-of-ananda-bhairava .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 9, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/09/the-invocation-of-ananda-bhairava/ "2:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[*h-s-क्ष्-m-l-v-r-यूं आनन्दभैरवाय वषट् ||\
s-h-क्ष्-m-l-v-r-यीं सुधादेव्यै वौषट् ||\
अईं क्लीं सौः ॥*]{style="color:#99cc00;"}*\
[ब्रह्माण्ड-रस-संभूतम्-अशेष-रस-सम्भवं ।\
आपूरितं महा-पात्रं पीयूष-रसम्-आवह ॥\
अखण्डैक-रसानन्द-करे पर-सुधात्मनि ।\
स्वच्छन्द-स्फुरणं मन्त्रं निदेह्य्-अकुल-रूपिणी ॥\
अकुलस्थामृताकारे सिद्धि-ज्ञान-कलेबरे ।\
अमृतत्वं निधेह्य् अस्मिन् वस्तुनि क्लिन्न-रूपिणी ॥\
तद्-रूपेणैक-रस्यं च कृत्वा तैस्तत्-स्वरूपिणी ।\
भूत्वा परामृताकारा मयि चित्-स्फुरणं कुरु ॥]{style="color:#99cc00;"}*

It is thus that Ananda-bhairava along with his shakti Ananda-भैरवी is meditated upon and invoked in the श्री-kula system at the time of the offering of the scents and flowers (स्मार्त practice) or kula-dravya (old kaula practice). The point of note here is that Ananda-भैरवी is explicitly identified with सुधा-देवी, the ambrosial goddess and offers a clear parallel to parA of trika and its evolutes and अमृत-श्री of the netra-tantra system. This is an indicator of one of the primary facets of the देवी and also that of rudra in the ancestral tantrika system. Those who are conversant with both the vaidika and tantrika mantra-शास्त्र would of course recognize the even earlier precursor of this tantrika imagery as springing from an even earlier vaidika substratum.


