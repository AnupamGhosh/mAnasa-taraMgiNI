
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The story of the nAstIka attack on रामेश्वरं](https://manasataramgini.wordpress.com/2008/06/20/the-story-of-the-nastika-attack-on-rameshvaram/){rel="bookmark"} {#the-story-of-the-nastika-attack-on-रमशवर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 20, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/06/20/the-story-of-the-nastika-attack-on-rameshvaram/ "5:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

An exchange with my acquaintance SRA on apotropaic rites related to temples prompted me to record this brief note. The light of the थेरवाद bauddha-s, the mighty king पराक्रम-बाहु the Great (i.e. पराक्रम-बाहु-I), came to power in Shri Lanka around 1153 CE. Few years after he came to the throne, he sent emissaries to other थेरवाद bauddha kings in Asia to announce his devote support for the sangha. नराथु, the king of Myanmar scorned the Lankan messengers and tortured them. He also imposed economic sanctions on Lanka. In the meanwhile पराक्रम-बाहु was busy in local wars in course of which he conquered the whole of श्री-लन्का and unified it under a single bauddha banner. In 1164 CE पराक्रम-बाहु decided avenge the insult of the Lankans and launched a massive attack on Myanmar with a formidable fleet equipped with an year's supply of grain for his large navy, an amphibious landing force of war-elephants and uniquely crafted long-range poisoned arrows. Despite cyclones and loss of multiple ships पराक्रम-bahu's fleet under admiral nagara-गिरी and chera mercenaries established a bridgehead at Kusumiya and captured the city with the amphibious landing force. Then the Lankan sea-borne army invaded Myanmar and penetrated as far as the capital Arimardhanapura. The Burmans resisted with much fury but पराक्रम-बाहु ordered his navy and army to keep pressing on till they destroyed the killed the king of Myanmar. The ports were blockaded by the Lankans and their army finally stormed Arimardhanapura and killed नराथु.

This successful adventure tempted पराक्रम-बाहु to invade and conquer south India. He found a great excuse for this program in the form of the internal struggle between two पाण्ड्य contenders for the throne and the choLa allies of one them (kulashekhara). He claimed to act in support of the other party (वीरपाण्ड्य) against choLa-s and kulashekhara. पराक्रम-bahu dispatched his powerful admiral लन्कापुर to deal with the Tamil kingdoms. लन्कापुर conquered Ramanathapuram near the setu and built a heavily fortified camp named पराक्रमपुर after his king. From here लन्कापुर brought in a large lankan land army and sent it towards Madhurai, where he besieged and defeated kulashekhara. Then the Lankan army placed वीरपाण्ड्य as puppet in Madhurai and controlled it from a fortified camp they built named पण्डुविजय. They used this fort in tandem with पराक्रमपुर and continued the war on the choLa-s, inflicted many blows on them, and even sent Tamils captured in India to build monuments in Lanka. Around 1171 CE parakarama-बाहु probably filled with bauddha zeal ordered the conquest of रामेश्वरं. The lankan navy blockaded the temple city and an amphibious landing force with elephants launched a direct attack on the temple. They first uprooted the huge temple doors and carried it away. Then finding their way to the temple treasury they seized all its treasures and took control of the shrine and prevented the worship of shiva.

The choLa king राजधिराज-II was shaken by the desecration of the temple and called upon a learned तान्त्रीक of the Urdhva-srotras, ज्ञानशिव to perform अभिचार rites to destroy the lankans who had desecrated the jyotir-लिङ्ग. ज्ञानशिव began an elaborate अभिचार prayoga invoking the terrifying 5-headed, 18 handed form of shiva, who wears a garland of 108 skulls. An image of shiva in this form is also found in the chandella fort near Kajuraho where they routinely invoked him before doing battle with the तुरुष्क-s. ज्ञानशिव performed the rite unfazed for 28 days. The choLa army had been hammered by the lankans in multiple battles till that point, but is said that the fury of महादेव entered them. राजाधिराज-II's army fell upon the desecrating bauddha-s in रामेश्वरं and smashed them in a quick assault. The lankan admiral लन्कापुर himself was leading the desecration and was struck by an arrow. The choLa-s beheaded him and nailed his severed head on the gates of the पाण्डुविजय camp as a befitting offering to महादेव.

The details of this event are recorded in the आर्पाक्कं inscription 18 Km from Kanchi.


