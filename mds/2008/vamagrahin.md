
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [वामग्राहिन्](https://manasataramgini.wordpress.com/2008/04/24/vamagrahin/){rel="bookmark"} {#वमगरहन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 24, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/24/vamagrahin/ "5:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This had to be noted:\
When the अराया came the वामग्राहिन् seized us.\
When the अराया's shadow was away the वामग्राहिन् seized us again !\
Then the mantra of श्रीविद्या showed its power and took us over the वामग्राहिन्. We never fully understood whether or not to complete the 5th circuit of the mantra and to take the day long walk. But the consequences seem to have given the answer. We realized that we should not deploy that mantra or it may turn like the mace of श्रुतायुध in the combat with कृष्णार्जुनौ. On that occasion the rebounding mantra completely burnt down the prayoga of the RV mantra "a-d-v-i-s-p". We had to deploy a different mantra --- one which remains inside us. So many things were parallel but the signal was clear: use a mantra but do not use the old one. We were also puzzled by the fact that then we failed to deploy the one of aryaman from the अथर्वण shruti. Like the mantras failing कर्ण we had been forsaken in the last moment but indra inside "t-i-a-i-h-h-s-sh-i" stood by along with agni who is दुर्गहा. We had to cross the पञ्चमि and deploy whole scale on षष्ठी.


