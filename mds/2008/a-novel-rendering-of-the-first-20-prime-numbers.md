
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A novel rendering of the first 20 prime numbers](https://manasataramgini.wordpress.com/2008/04/02/a-novel-rendering-of-the-first-20-prime-numbers/){rel="bookmark"} {#a-novel-rendering-of-the-first-20-prime-numbers .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 2, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/04/02/a-novel-rendering-of-the-first-20-prime-numbers/ "11:54 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm3.static.flickr.com/2050/2383034970_abc23268c9_o.png){width="75%"}
```{=latex}
\end{center}
```



