
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The mysterious conflict between भृगु-s and gotama-s according to the tradition of the Urdhva-srotas](https://manasataramgini.wordpress.com/2008/07/14/the-mysterious-confliction-between-bhrigu-s-and-gotama-s-according-to-the-tradition-of-the-urdhva-srotas/){rel="bookmark"} {#the-mysterious-conflict-between-भग-s-and-gotama-s-according-to-the-tradition-of-the-urdhva-srotas .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 14, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/14/the-mysterious-confliction-between-bhrigu-s-and-gotama-s-according-to-the-tradition-of-the-urdhva-srotas/ "6:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the ऋग्वेद the gotama-s repeatedly and respectfully mention the भृगु-s as the founders of the fire rite, which is consonant with their position at the head of the shrauta gotra-pravara lists.\
For example:\
दधुष् ट्वा भृगवो मानुषेष्वा रयिं na चारुं सुहवं जनेभ्यः |\
होतारम् agne अतिथिं वरेण्यं मित्रं na शेवं दिव्याय janmane ||RV 1.58.6\
Here नोधा gautama declares that the भृगु-s first established the ritual fire in the midst of men. In fact नोधा gautama goes ahead to mention this act of the भृगु-s again in RV 1.60.1:\
वह्निं यशसं vidathasya केतुं सुप्राव्यं दूतं sadyo-artham |\
dvi-जन्मानं rayim iva प्रशस्तं रातिं bharad भृगवे मातरिश्वा ||\
Here the transmission of the ritual fire from मातरिश्वन् to भृगु, the first founder of the rite amongst men is mentioned.\
The other gautama, कक्षिवान् dairghatamasa, also mentions the भृगु-s in positive light:\
pra yA घोषे भृगवाणे na shobhe yayA वाचा yajati pajriyo वाम् |\
प्रैषयुर्न विद्वान् || RV 1.120.5\
Here he compares his own chant to the ashvinau to that of the भृगु-s.\
His father दिर्घतमाः औचाथ्य states:\
yamerire भृगवो विश्ववेदसं नाभा पृथिव्या bhuvanasya मज्मना |\
अग्निं taM गीर्भिर्हिनुहि sva A dame ya eko vasvo वरुणो na राजति || RV 1.143.3\
Here he states that the mighty भृगु-s first established the fire sacrifice in the nave of the earth.\
A similar view is also shared by the other great vipra amongst the gotama-s, वामदेव gautama:\
ayam iha prathamo धायि धातृभिर् होता यजिष्ठो अध्वरेष्व् ईड्यः |\
yam अप्नवानो भृगवो viruruchur वनेषु चित्रं विभ्वं vishe-vishe || RV4.7.1\
and\
आशुं दूतं vivasvato विश्वा yash चर्षणीर् abhi |\
A जभ्रुः ketum Ayavo भृगवाणं vishe-vishe ||RV 4.7.4\
Here, अप्नवान and his भृगु-s are described as having initiated the fire rite that spread throughout the peoples.\
वामदेव gautama also states:\
eved इन्द्राय वृषभाय वृष्णे ब्रह्माकर्म भृगवो na रथं |\
nU chid यथा नः सख्या वियोषद् asan na ugro .अविता तनूपाः || RV 4.16.20\
Here, वामदेव says that he fashions a hymn for indra even as the भृगु-s had made a car. Earlier in the same same सूक्त वामदेव states that he is fashioning a hymn for indra even as the भार्गव उशनाः काव्य had done in the past. Thus from all accounts in the ऋग्वेद it appear that the gotama-s like their other अङ्गिरस cousins the कण्व-s and भरद्वाज-s considered the भृगु-s as their models, and founders of their ritual system.

Given this vedic precedence I was puzzled by a short पौराणिc narrative provided by the siddhAnta tantra, मृगेन्द्र's विद्यापाद (13.102-105) regarding the Pauranic द्वीप known as gomeda:

gomede gopatir nAma राजाभूद् गोसवोद्यतः ||\
याज्यो .अभूद् vahni कल्पानाम् औतथ्यानां मनोः kule |\
In gomeda there was a king named gopati who wanted to perform the gosava. The ritual was performed by the autathya-s (i.e. the Epic/Pauranic form of the vedic name औचाथ्य or the descendents of दीर्घतमस् the founding father of the gautama-s) who were like fire.

sa तेषु hari यज्ञाय प्रवृत्तेषु भृगून् गुरून् ||\
vavre taM गौतमः कोपाद् ashapad agamat क्षयम् |\
He chose the भृगु-s as his officiants while \[the gotama-s] were performing a hari ritual \[soma ritual, taken by some as a ritual to विष्णु]. The gautama out of anger cursed him and he went to his destruction.

यज्ञवाटे .asya tA गावो दग्धाः कोपाग्निना मुनेः ||\
tan मेदसा mahI चन्ना गोमेदः sa tato .abhavat |\
The cows in the ritual enclosure were burnt by the flames of ब्राह्मण-s wrath. The earth was convered by the fat of those cows and द्वीप gomedah came to be known.

The persistence of this tale in the shaiva world became clear from its presence in an abbreviated form in the पराख्य tantra (D. Goodall edition):\
Gomedas tat paraM द्वीपं yatra gautama-शापतः |\
गवां shate hate मेदः प्रवृतं प्रचुरात् तदा || (पराख्य 5.102)\
Beyond that is gomedas where a hundred cows were killed because of the curse of gautama and their fat flowed out profusely \[forming the continent].

This conflict between the gautama-s and भृगु-s nor the king gopati is mentioned in any other पौराणिc source. I suspect that it is one of those rare cases where the पौराणिc material within the tantra-s is preserving something absent in the classical tantra-s themselves. In this sense it would compare to the पौराणिc material found in the कुब्जिका-मता and ShaT-साहस्र. Its antecedents are indeed puzzling. The ब्राह्मण narratives of the gosava mention the ritualist being sprinkled with cow milk -- it may be performed by a vaishya or a sthapati who is elected as a governor by his people. This gopati could be such a character as no such king figures in the major पौराणिc or vedic genealogies.


