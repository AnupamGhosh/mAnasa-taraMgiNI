
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [महासमर-2](https://manasataramgini.wordpress.com/2008/12/30/mahasamara-2/){rel="bookmark"} {#महसमर-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 30, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/12/30/mahasamara-2/ "6:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Update (30-31 Dec): The ari-dala sent a huge force on the sachiva with all their commanders to taking to the field leading their respective divisions. On us they deployed अभिचार to keep us knotted. We sent the signal to the sachiva that there was only the option to fight unto death. The sachiva summoning his yogic abilities remained absolutely calm even as they poured in from all directions and confused their initial surge through feign retreat from a narrow defile. He then deployed the whole part of our सेना under his command for an all out frontal assault. He saw for the first time in his military career the महासमर in its raw form. He ably maneuvered the सेना and prevented the joining of the two main ari divisions. Having done this he broke up both the divisions by accurate artillery fire. The third ari division bashed into their own retreating colleagues of the other two divisions and dispersed in disarray. But they were not to be quite as our spasha-s informed us that they were finally trying to deploy "the beheading of नागार्जुन" to destroy us. We knew the ari-s would try their best to destroy us so we signaled to the अमात्य make preparations to open a new front. Code: "The charge of सात्यकि and भीमसेन".

All the vIra-s had been attacked, but only the 3rd vIra had really survived the attacks. The first hero was, after an initial victory, defeated at himahrada and forced into an endless retreat. We broke through the ranks of the म्लेच्छ ari-dala-s rather quickly but were confronted by the same आक्रान्त as the one faced by the first hero. We were scarred but survived that initial attack and executed a relatively successful escape strategy. Our successor had some of our own problems. He was bogged down at a preliminary stage itself by an ari whom both the first hero and I had defeated with relative ease. We could not relieve his division as we were locked in the म्लेच्छ conflict and then the first hero was crushed successively in the battles of himahrada and अर्कग्राम. But by grinding away our successor finally overcame that ari and went on to found his own kingdom. All the while he carefully observed the campaigns of the first hero and me, and observed our failings. He decided to correct those failings and launched himself into an elaborate campaign. There he was tied down not knowing its enormity, and it is not clear whether he would live or die. Then the first hero was attacked by the ग्राहिन्. But after two weeks of fighting he overcame his ग्राहिन् completely. Exactly 5 months and 15 days later we were attacked by the unexpected ग्राहिन्. We fought a long battle, but in course of that encounter we appear to have run out of luck like a Hindu army in the quagmire of history. We realized we could meet our end. With our successor also locked in a gripping struggle, we wondered if any one may bear the torch at all. In course of all this the 3rd hero watched all our mistakes. However, right from an early stage he decided to chart his own path as he heart-of-heart felt we were fools. He was also lucky to be endowed relative to most of us with considerable wealth and a supremely able अमात्य. He first defeated the ari in bhArata in a massive way, far exceeding the victories of the first hero and me. He then made common cause with the म्लेच्छ-s, unlike the first hero and me, and with their artillery won some spectacular victories. Then he carved his own kingdom, still remaining close to the म्लेच्छ-s. He was then attacked by the counter-कार्तवीर्य prayoga. But he took the aid of his powerful दूती's magic to secure his defenses. With these in place he attacked the fort on the White Mountains :-) and secured it. The MM saw that he was the most useful of the vIra-s and revealed some secrets to him. He used those successfully to bypass the ग्राहिन् and gain unprecedented victory. With his telescope he spied us from afar. He realized that probably the first hero and I might sink. He also saw that the 4th hero was tied down in his own campaign which kept him out of the picture entirely. At that point he swooped down to seize all the territory lost by us and became supreme राजन्. With his अमात्य's aid he also destroyed the power of the descendents of Fourier of मन्गलग्राम completely. It was clear now that the third hero was the sole winner. There was only one frontier that remained unconquered -- the success of the virile तैत्तिरीयक. He advanced towards that in the late autumn of 2008 with his दूती aiding him with magic. We were just then attacked by both the ग्राहिन् and the आततायिन् and were unable to even hold the territory which was ours. Instead we found ourselves running without even a fort to protect us.


