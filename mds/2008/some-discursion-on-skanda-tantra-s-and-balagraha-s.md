
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some discursion on skanda tantra-s and बालग्रह-s](https://manasataramgini.wordpress.com/2008/09/03/some-discursion-on-skanda-tantra-s-and-balagraha-s/){rel="bookmark"} {#some-discursion-on-skanda-tantra-s-and-बलगरह-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 3, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/03/some-discursion-on-skanda-tantra-s-and-balagraha-s/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3184/2828977611_2ea439e712_o.jpg "नेजमेष"){width="75%"}
```{=latex}
\end{center}
```

\
Over the years we have been collecting several observations on the कौमार sect, its texts and evolution on this site. While all this would be worth synthesizing, that would need much greater effort. We identify 3 basic phases in the development of mantra-मार्ग कौमार शासन: 1) The earliest phase appearing in the late vedic period characterized by the celebrated नेजमेष mantra of the ऋग्वेद khila, the skanda याग and धूर्त-bali of the AV and YV traditions, and the षष्ठी सूक्त of the मैत्रायणीय tradition. 2) The middle phase comprising of the origin of the early tantric system of the कौमार शासन and its development through the gupta period and beyond. This is an extended phase with texts like the षण्मुख kalpa at the earlier end, skanda-सद्भाव in the middle and सुब्रह्मण्य kalpa in the later end. 3) The symbiosis with the Urdhvasrotas of the shaiva शासन, a phase which was mainly prevalent in South India. We have evidence and reasons to believe that the phase 2 of the evolution of mantra-मार्ग कौमार शासन had considerable role in fertilizing or providing seeds for the development of several other streams, such as the shaiva शासन and the तान्त्रीक stream in the nAstIka realm. In this context three main points of interest emerge:

 1.  The absorption and integration of the early कौमार elements into: 1.1) भूत tantras of the shaiva realm, 1.2) the system of deities in kaula streams like those of the पश्चिमांनाय and 1.3) the vaiShNava पाञ्चरात्र tantra-s as the deity विष्वक्सेन.

 2.  The absorption of कौमार elements into the bauddha शासन (we had earlier given a brief illustration of this using the mantra-s and मण्डल-s of the मञ्जुश्री मूल kalpa). This parallels the earlier absorption of कुमार as the bauddha "missionary" amongst the deva-s beginning from the janavasabha sutta of the दीघनिखाय of the Pali canon. Of particular interest in the current context is also the early development of the deity वज्रकुमार in the nAstIka realm. His original association appears to be with the महायोग tantra-s of the bauddha-s, but he seems to have been marginalized with the ascendancy of the yogottara and योगिनी-tantra-s amongst them. We have some evidence that Indian मन्त्रवादिन्-s translated his prayoga-s into chIna-भाषा in the early 700s of CE and these were extensively used in अभिचार-s of the nAstIka-s. One such text the वज्रकुमार tantra in Chinese translation of the original Sanskrit is of great importance. This Chinese text in many places even fails to add the 'vajra-' prefix and simply calls him कुमार and uses derivative of कौमार mantra-s. A critical rite is prescribed on a day in the कार्त्तिक mAsa to वज्रकुमार just as in the case of कुमार. वज्रकुमार's आवरण devata-s include a notable collection of पिशाची-s who are avicephalous or therocephalous just like the स्कन्दमातृ-s in the आवरण of कार्त्तिकेय. One of the attendants of वज्रकुमार is a roguish देवी called रेमती who sits a seat of a flayed skin of a child and is the ruler of the diseases of children. Thus, she is an ortholog of the रेवती of the कौमार tantra-s. The shakti of वज्रकुमार is दीप्तचक्रा-देवी who is very similar in all features to देवसेना. Interestingly, वज्रकुमार is called the great son and the japa माला used in his rite is made of रुद्राक्ष-s with 6 faces. Thus, it is very clear that the bauddha वज्रकुमार tantra-s, like key parts of the मञ्जुश्री मूल kalpa arose as adaptations of कौमार works, even as the योगिनी tantra-s of the nAstIka-s are derived from their shaiva counterparts. Likewise the early development of the deity महामायूरी also appears to have been fertilized by कौमार elements pertaining to his shakti षष्ठी.


 3.  The कौमार शासन's development from the beginning was closely linked to pediatrics in traditional hindu medicine. The link to childbirth and diseases of children is seen in the late vedic layer. E.g.: 1) the नेजमेष mantra of the RV khila (child birth); 2) the formulae for relief from कुमार graha-s in पारस्कार गृह्यसूत्र (1.16.24); It continues in the महाभारत: 3) The extensive discussion of कुमार graha-s in the vana parvan narrative of the स्कन्दोपाख्यान (Vulgate 3.229); 4) skanda- graha-s and मातृ-s in the shalya parvan (Vulgate 7.44-46); finally we also see an inheritance of these elements by the nAstIka-s followers of the nirgrantha in the tale of his nativity: 5) jaina कल्पसूत्र (2.22 onwards). Here, the nirgrantha's followers, in a typical anti-brahminical rant, state how originally the nirgrantha was an embryo in the womb of his biological mother देवानन्दा, a ब्राह्मण woman of the जालन्धरायण gotra. But because nirgrantha तीर्थंकर-s are never born in brahminical wombs indra, but in इक्ष्वाकु, यादव or at least a rich kShatriya families, indra summoned हरिणेगमेषिन्, the commander-in-chief of the deva armies, and asked him to transfer the embryo of the nagna to a kShatriya womb. The commander of the gods flew to bhArata-varSha like a comet and on the 83rd day after conception extracted the embryo out of देवानन्दा's womb when the moon was in uttara-phalguni. He then removed the "brahminical atoms" of he embryo and replaced them with "clean atoms" he had collected on the way. Then he transferred the embryo of the future वर्धमान to the womb of his surrogate mother त्रिशला a kShatriya (See picture above). He then took the embryo in her womb and placed in that of देवानन्दा and flying thousands of yojana-s in each leap he reached indra's abode to inform him of the successful transfer. The jaina नेमिनाथ charita (NC 7) provides a comparable narrative of how हरिनैगमेषिन् gave कृष्ण a son via सत्यभामा upon his extensive propitiation.

These jaina narratives strikingly parallel the implication of नेजमेष mantra-s of the RV-khila and their associated rituals in different vedic traditions:

[*नेजमेष parA pata सुपुत्रः punar A pata |\
अस्यै मे पुत्रकामायै गर्भम् आ धेही यः पुमान् ॥\
यथा इयम् पृथिवी मह्य्य् उत्ताना गर्भम् आदधे ।\
एवम् तम् गर्भम् आधेहि दशमे मासि सूतवे ॥\
विSणोश् श्रैSठ्येन रूपेण अस्याम् नार्याम् गवीन्याम् ।\
पुमाम्सम् पुत्रम् आधेहि दशमे मासि सूतवे ॥*]{style="color:#99cc00;"}\
O नेजमेष fly away and fly back bringing a good son to my wife here, who desires a son, give an embryo, a male one.\
Just as the wide earth stretched out conceives the embryo, so give that embryo that is born in the tenth month.\
Place a male child, with विष्णु's great form, to be born in the tenth month into the womb of this woman.

In ऋग्वेदिc tradition (आश्वलायन and शाखायन) they are used in the rite of simnatonnayana. In the तैत्तिरीयक tradition of the yajur veda they are deployed by the male before having sex with his wife if they desire a son (Apastamba गृह्य 8.13). In the मैत्रायणीय and kaTha traditions of the yajurveda these mantra-s are deployed in the पुत्रकाम rites. If a man desires a son then for a whole year he performs the षडाहुतं or the 6 fold oblations to नेजमेष at the beginning of each lunar पक्ष using these mantra-s and 6 ghee oblations. If this fails, then he performs the rite of the नैजमेष स्थालीपाक by making a स्थालीपाक and offering it to the god just as in the षडाहुतं ritual.

In traditional Hindu medicine pediatrics is sometimes termed k\[a]उमार tantra. This leads question as to whether the eponymy with the tantra-s of the कौमार शासन is merely incidental or if there is a genuine connection. A clear link to the कौमार system is present in the classical medical saMhitA of sushruta in the uttara tantra section. Another major early medical work, the kAshyapa tantra, as we have shown before, also incorporates a large amount of कौमार material. An examination of the uttara tantra of sushruta is of considerable interest in this regard: chapters 27-37 of the uttara tantra are called कुमार tantra. Chapter 27 of the uttara tantra provides an overview of the graha-s. They are listed as: 1) skanda 2) स्कन्दापस्मार 3) शकुनी 4) रेवती 5) पूतना 6) अन्धपूतना 7) शीतपूतना 8) मुखमण्डिका and 9) नैगमेष. The prescriptions for countering their effects in UT 27.18-21 are predominantly dependent on mantra-s and rituals, suggesting a link to a tantric tradition. The subsequent chapters take this up in greater detail: Chapter 28 deals with the seizure by skanda. In addition to medications using different ओषधि-s, we observe that 28.8 prescribes offering of a bell and a skanda bali of a good कुक्कुट. Then having had nocturnal baths on 3 nights on the 4th there is a treatment with rice and barley. A daily skanda homa is prescribed to be performed by the pediatrician (28.9-14): The fire is lit using the gAyatrI and व्याहृति-s and oblations are made to skanda using the following अनुष्टुभ् mantras:\
[*तपसां तेजसं चैव यशसं वपुषां तथ ।\
विधाता यो।अव्ययो देवः स ते स्कन्दः प्रसीदतु ॥\
ग्रह-सेनापतिर्-देवो देवसेनापतिर्-विभुः ।\
देवसेना-रिपुहरः पातु त्वां भगवाण् गुहः ॥ + स्वाहा\
देव-देवस्य महतः पावकस्य छ यः सुतः ।\
गङ्गोमा-कृत्तिकानां च स ते शर्म प्रयच्छतु ॥+स्वाहा\
रक्तमाल्याम्बरः श्रीमान् रक्त-चन्दनभूषितः ।\
रक्तदिव्य-वपुर्-देवः पातु त्वां क्रौञ्चसूदनः ॥+स्वाहा*]{style="color:#99cc00;"}

The chapter 29 deals with स्कन्दापस्मार and identifies this seizure with विषाख. Other a series of drugs a naivedyam of meat, blood and rice offering under a banyan tree and a bath at a cross-road are recommended. The mantra used in the ritual is given as:\
[*स्कन्दापस्मार-संज्ञो यः स्कन्दस्य दयितः सखा ।\
विषाख-संज्ञ्श्च शिशो शिवोऽस्तु विकृताननः ॥*]{style="color:#99cc00;"}\
Likewise both drugs and mantra-s are prescribed for all other seizures in the subsequent chapters.\
E.g. मुखमण्डिका mantra:

  - [अलङ्कृता रूपवती सुभगा कामरूपिणी ।\
गोष्ठ-मध्यालयरता पातु त्वां मुखमण्डिका ॥]{style="color:#99cc00;"}*\
The goddess is conceived as being in the middle of a cowshed, which is incidentally the site for her for bali.\
E.g. नैगमेष mantra:

  - [अजाननश्-चलाक्षिभ्रूः कामरूपी महायशाः ।\
बालं बाल-पिता देवो नैगमेषोऽभिरक्षतु ॥]{style="color:#99cc00;"}*\
His bali is made under a banyan tree on a षष्ठि day.

The concluding chapter of the कुमार tantra section of sushruta contains the tale of कुमार संभवं similar to that in the vana parvan of the महाभारत to explain graha-s. They are described as being generated by the कृत्तिका-s, उमा, agni and rudra to protect the new-born कुमार in the sharavana even though he was protected by his own tejas. There of course seems to be an interpolation (UT 37.10) where the author tries to reconcile the benevolent and roguish tendencies of skanda in a clumsy way.

In charaka (shArIra स्थान, chapter 8) too we find a mantra to कार्त्तिकेय that is whispered into the ear of the woman in the context of protecting the child at birth:

  - [क्षितिर्-जलं वियत्-तेजो वायुर्-विष्णुः प्रजापतिः ।\
सगर्भां त्वां सदा पान्तु वैशल्यं च दिशन्तु ते ॥\
प्रसूष्व त्वम् अविक्लिष्टमविक्लिष्टा शुभानने ।\
कार्त्तिकेयद्-द्युतिं पुत्रं कार्त्तिकेयाभि-रक्षितं ।]{style="color:#99cc00;"}*|

But hardly any graha material or mantra-s pertaining to them is found in charaka. In fact charaka's chapter is paralleled to a large extent by sushruta's own shArIra स्थान chapter 10, where graha-s are mentioned only in passing and a reference is made to the uttara tantra for further details. The classics of Ayurveda do contain several invocations and recommendations for mantra prayoga-s (much to the delight of modern "rationalists" who are keen to show Ayurveda as rubbish). Yet, when one scans even the uttara tantra alone for mantra prayoga-s one finds that they are relatively low with the exception of two sections, namely the कुमार tantra and the भुतविद्या (which is clearly related to the भूत tantra-s of the paschima srotas of the shaiva tantra-s). These observations would suggest that though there was a general practice to invoke skanda in the context of child birth (seen in the even earlier vedic elements or charaka's single mantra), the कुमार tantra is a special mantra- and ritual- rich accretion to the Hindu medical tradition.

The much latter अष्टाङ्ग संग्रह of वाग्भट the son of simhagupta, the last of the big three in Hindu medicine (i.e. it clearly comes after charaka and sushruta because it recognizes several elements of full-blown bauddha mantra-शास्त्र like महामायूरी, आर्यापर्णशबरी and आर्यापराजिता) also preserves similar mantra material in its कौमार tantra in the उत्तरस्थान. In fact the अष्टाञ्ग संग्रह provides much more old material in this regard than the कुमार tantra of sushruta, including certain graha-s who are not mentioned in sushruta but definitely existed even before: for example, the shva-graha (उत्तरस्थान 6.26). This kerberomorphic graha already appears in the mantra used in the पारस्कार गृह्य sUtra on occasion of a कुमार affliction. While the अष्टाङ्ग संग्रह has a greater proclivity towards mantra prayoga that charaka or sushruta (for example it even alludes to a shAstA mantra but does not give it in sUtra स्थान 3.119), two of the most mantra-rich sections of this text are the कौमार tantra and भूत विद्या sections of the uttara स्थान. The अष्टाङ्ग संग्रह not only preserves what is found in sushruta's कुमार tantra but much more additional old कौमार mantra-s and rituals. We also find that such कौमार material independently occurs in the other ancient medical treatise the kAshyapa tantra. All the above observations taken together, it is less likely that this well-developed material was built up by the kAshyapa tantra and the अष्टाञ्ग संग्रह from a core represented by the sushruta saMhitA. Instead, it is likely that sushruta, KT and the अष्टाङ्ग संग्रह were drawing from a common source containing such material. This source we believe were the texts of the early कौमार tantra of the कौमार शासन (beginning of phase 2 in the above note evolutionary scheme for the कौमार शासन).

In support of this we shall in the future present an analysis of 3 texts namely, the अष्टाङ्ग संग्रह, fragments of a text that appears to be a lost कौमार tantra and a much later कौमार tantra from the drAviDa country.


