
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [समरव्यूह-1](https://manasataramgini.wordpress.com/2008/10/02/samaravyuha-1/){rel="bookmark"} {#समरवयह-1 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 2, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/02/samaravyuha-1/ "6:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In life as one wages various fierce yuddha-s the historical analogies sometimes come up providing a vivid imagery in the mind's eye. In course of the great war we observed something interesting, even if shocking: we lost track of the passage of time. There are whole years that we simply fail to account for. It is not an amnesia; if we put in the effort we remember the events and the individual battles of the great war that took us no where. We also remember the few great victories we scored on other fronts aided by some of our ablest warriors on the field. But the collective narrative does not readily exist in our mind in fact the existence of those years is something that we do not seem to readily recall: was it one, two or three? As a result there is an ahistoricity -- this reminded us of the ahistoricity that came upon the Hindu masses even as they went through the horror centuries of Mohammedan or Isaistic assaults. There is something strangely paradoxical about it: sometimes there is contraction of time because of lack of ready recall of the years of war, on the other had there is a time dilation -- some relative small stretches of time appear to be a long duration. So we look on beyond the veil and see the "good old days" beyond with greater temporal clarity, but the old pleasures of then seem to have lost their edge or in fact appear even unbelievable.

The ari-s pressed on to fight the big battles on their terms. The first मारण which was sent our way on April 26th and was repulsed with great help from the अमात्य. The first ब्रह्मास्त्र of ours was broken by their counter-attack. Then our अमात्य provided a second ब्रह्मास्त्र, which in combination with the rudhira shara-s helped us to repulse the attack after a month of prayoga. The second identical मारण attack came upon us on the 21st of August. We resisted with minor prayoga-s for 6 days when around 9 AM we realized we might be on the southern road. The attack was so severe that were shaking from it like a palm in a hurricane. Then at 4.00 AM we performed the prayoga of the ब्रह्मास्त्र given by गीर्वाणेन्द्र दीक्षित's clansman. This magical weapon like बगलामुखी herself saved us from the noose, but we could only use it two times, and we had exhausted one shot of it. But we were not sure if we had cheated the मारण entirely, as even after the astra we faced the aftermath for a while. The ari-s taking advantage of this pressed home a strong attack. Our sachiva intercepted and overthrew it. In the mean time the sachiva was distracted on the front of the last hero, to whose aid the 3rd hero and that hero who was struck by the ari-s in himahrada also rushed. Our army was isolated at that point, and the ari-s decided to advance on us with the main body of their army. They received a major windfall as our biggest fort was imperiled by the धुमावती acting via the म्लेच्छ-s. This put us in the potential danger of encirclement.


