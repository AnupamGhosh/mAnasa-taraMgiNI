
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The other vIra appears](https://manasataramgini.wordpress.com/2008/02/24/the-other-vira-appears/){rel="bookmark"} {#the-other-vira-appears .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 24, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/02/24/the-other-vira-appears/ "7:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

All the vIra-s had been attacked by the prayoga-s of the ari-s as implied in their cryptic message on the phala-s of the नॄ trees. The ज्येष्ठ vIra had then been tied down by a कामिनि sent by the ari-prayoga. The third vIra despite his bhIma-like battle fury had been tied down by the dasyu-s, while the other vIra-s faced their own downward fates. As we were floundering in the battle with the म्लेच्छ-s the thundering indra took us to victory. But then came the second round when we were bound in chains by the ari-s and placed in internment in आराम and राजक्षेत्र. The great vIra was then beaten twice in battle, once at shishira-grAma due to the कामिनि, and then when he reached sUrya-स्थान by another ari prayoga. Having lost his troops and kingdom, he was slipping into oblivion, when his guru's prati-prayoga saved him the jaws of destruction. Then there was the other vIra who alone briefly understood the code which we conveyed: "If Doldrum is dead Dildrum is the king of the cats". This vIra had taken a severe hammering from the prayoga-s, and smashed to bits, even as we were deep in the yuddha of म्लेच्छ-varSha. The other vIra appeared in incognito briefly and conveyed to R that which lay under the code Jaimal and Patta. R relayed that to us, but it was too late, and we were capture by the ari-s and taken to आराम. The third vIra who even deployed dUra-दृष्टि on us then broke forth and became a conqueror. But we remained nervous that until the other vIra broke the prayoga of the ari-s our larger fate was sealed, especially if we fell fighting in battle. With that becoming more of a possibility we were concerned if there might ever be a "pratardana दैवोदासि" to raise us like the kAshi राजस्.

But finally that vIra ('inum' nAma योद्धा) took to battle -- it was a fierce encounter with the ari-s who smashed his first two prayoga-s. He was pushed back by some distance like the bold सात्यकी being pressed back by the vicious भूरिश्रवस्. He had many mantra-s in his possession but had forgotten them like राधेय at कुरुक्षेत्र. But on the day of the deva-s he deployed that mighty mantra of his ancestors from the अथर्वाञ्गिरस shruti, which due to our negligence we had failed to deploy. We sent the अमात्य and the sachiva to aid him in war. They were intercepted by the ari-s and took a heavy shelling. Bereft of mantra-s and unable to call the deva-s to their aid and were crumbling on the रणाङ्गण. Then they saw a cloud of dust appearing on the eastern horizon. It was the सेना of the other vIra. Out of nowhere he appeared with the spirit of the भृगु spells were manifest in him. His prayoga was like a charm laid by ऋचीक or अप्नवान himself or like an unstoppable mantra of च्यवान or स्यूमरश्मी. The अमात्य and the sachiva merged forces, and the ari-s were defeated and scattered by the other vIra not far away from मण्ग़ल-grAma. But we sent the signal: "king aryaman still needs his bhAga".


