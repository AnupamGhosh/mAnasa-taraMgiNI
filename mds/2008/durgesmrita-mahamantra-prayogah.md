
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [दुर्गेस्मृता-महामन्त्र प्रयोगः](https://manasataramgini.wordpress.com/2008/08/22/durgesmrita-mahamantra-prayogah/){rel="bookmark"} {#दरगसमत-महमनतर-परयग .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 22, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/22/durgesmrita-mahamantra-prayogah/ "11:32 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3187/2786858615_dc02484057_b.jpg){width="75%"}
```{=latex}
\end{center}
```



durge स्मृता iti mantrasya हिरण्यगर्भ ऋषिः | उष्णीक् छन्दः | श्रीमहामाया देवता |\
शाकंभरी शक्तिः | दुर्गा बीजं | श्रीं वायुस्तत्त्वं mama chaturvidha-puruShArtha-siddhaye jape विनियोगः ||\
अथः न्यासः ||\
durge स्मृता ... अङ्गुष्ठयोः हृदये || स्वस्थैः स्मृता ... तर्जन्योः shirase स्वाहा || yadanti yachcha दूरके भयं vindati मामिह मध्यमयोः शिखायै वषट् || पवमानवितज्जहि अनामिकयोः कवचाय huM || दारिद्रय ... कनिष्ठिकयोः netra-त्रयाय वौषट् || सर्वोपकार ... करतलकरपृष्ठयोः अस्त्राय फट् ||\
अथः ध्यानं\
केनोपमा bhavatu te.asya पराक्रमस्य रूपं cha shatru-bhaya-कार्यतिहारि kutra |\
chitte कृपा समरनिष्ठूरता cha दृष्टा tvayeva devi varade bhuvanatraye.api ||\
iti ध्यात्वा मानसोपचारैः संपूज्य yoni मुद्रया नत्वा ||\
अथः मनुः ||\
OM aiM ह्रीं क्लीं चामुन्डायै vichche | OM ह्रीं श्रीं क्लीं कांसोस्मितां हिरण्य-प्राकाराम्-आर्द्रां ज्वलन्तीं तृप्तां तर्पयन्तीं | padme स्थितां पद्मवर्णां तामिहोपह्वये श्रियं || OM ह्रीं श्रीं क्लीं OM ह्रीं श्रीं क्लीं |\
durge स्मृता harasi भीतिमशेषजन्तोः स्वस्थैः स्मृता मतिमतीव शुभां ददासि | yadanti yachcha दूरके भयं vindati मामिह | पवमानवितज्जहि | दारिद्र्य-दुःख-bhaya-हारिणि kA त्वदन्या सर्वोपकार-करणाय सदा.a.अर्द्रचित्ता || OM ह्रीं श्रीं क्लीं OM ह्रीं श्रीं क्लीं कांसोस्मितां हिरण्य-प्राकाराम्-आर्द्रां ज्वलन्तीं तृप्तां तर्पयन्तीं | padme स्थितां पद्मवर्णां तामिहोपह्वये श्रियं || OM ह्रीं श्रीं क्लीं चामुण्डायै vichche || mahat-कार्ये लक्षम्-अयुतं सहस्रं अष्टोत्तर-शतं जपित्वा sakala-कार्य-siddhir-bhavati | दशांश-क्षीराज्य हवनं | दशाम्श तर्पणमार्जन-सुवासिनी-ब्राह्मण-भोजनानि


