
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A meditational aid for parA साधन](https://manasataramgini.wordpress.com/2008/08/18/a-meditational-aid-for-para-sadhana/){rel="bookmark"} {#a-meditational-aid-for-para-सधन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 18, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/18/a-meditational-aid-for-para-sadhana/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3159/2773049695_013b3d6376_b.jpg){width="75%"}
```{=latex}
\end{center}
```



Among extant साधक-s it is popular to merely concentrate on तत्त्वारोहण to reach parA देवी. This apparently follows the tradition of सहजानन्दनाथ and is transmitted by certain successors of the Maharashtrian तान्त्रीक उमानन्दनाथ. But the complete form of the साधन does require complete construct of trika with the three देवी-s and the bhairava-s. The middle prong of the त्रीशुल has parA देवी with भैरवसद्भाव of the form as in the saundaryalahari ( sharaj-ज्योत्स्ना शुभ्रां); the right prong has the exceedingly fierce अपरा देवी with नवात्मन् (equivalent to कुब्जिका of the पस्चिमाम्नाय of the kula tradition); the left prong has the fierce परापरा with ratishekhara-bhairava. The five-headed सदाशिव of the siddhAnta tantra-s lies at the base of the prongs of the त्रिशूल, just as mattress of त्रिपुरा in श्रीकुल. हूहुक-bhairava encompasses the पञ्च तन्मात्र-s and their receptors feeding into consciousness. All this is more completely laid out in the and trika tantra-s and expounded by the Kashmirian greats, but in extant practice the more complete parA साधन is only known to some साधक-s succeeding श्रीनिवास भट्ट.


