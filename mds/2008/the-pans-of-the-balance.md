
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The pans of the balance](https://manasataramgini.wordpress.com/2008/08/04/the-pans-of-the-balance/){rel="bookmark"} {#the-pans-of-the-balance .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 4, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/04/the-pans-of-the-balance/ "5:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had long ago said that he had attained eko-मानुष आनन्दः, but he was not happy. He remained vigorous in the quest of new wealth and new glory. The gifts of विष्णुपत्नि mattered to him above all. He welcomed her to his house with his efforts, but behind श्री came the hideous ज्येष्ठा. He and family were seized by ज्येष्ठा. Unlike his most illustrious ancestors, the smoke from whose fire altars reached the skies, he had no mantra-s. So he could not keep अलक्ष्मी away when she came and his Ananda was ended. The wanderer somasharman said "all is transient" and moved on bearing the कपाल in his hand.

The other श्रेष्ठ arrived full of expectation. He looked at somasharman and lectured at some length. Then he declared, surely this somasharman must be a goner. Could there be someone so singularly interested in the obscure -- obscure organisms, obscure molecules and obscure texts, that to in a "dead language". Surely this somasharman is an obscurantist! Leaving the श्रेष्ठ without any reaction he wandered on, with his कपाल. He passed the bower where he and his good friend of yore, rati-vallabha-paNDita had rejoiced in the halcyon days. He said to himself again "all is transient", rati-vallabha is gone. Just then he saw मृगनयनिका pass by riding a gardabha. As she passed him she turned not to look at him, but shouted: "let it be known to Doldrum that Dildrum still lives."


