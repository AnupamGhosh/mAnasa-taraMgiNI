
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Disclaimers and all that](https://manasataramgini.wordpress.com/2008/07/19/disclaimers-and-all-that/){rel="bookmark"} {#disclaimers-and-all-that .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 19, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/19/disclaimers-and-all-that/ "4:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I never knew I would end up writing something like this on my own pages. In many ways I will actually be repeating what I have written in different forms several here. But the steep build up in angry inputs over the past month or so calls for this again.

Some tell me I am being like an Indologist supporting the famous Aryan Invasion theory. Yes, I have no doubt that Indo-European languages (i.e. progenitor of Indo-Aryan and perhaps Bangani) entered India from outside. Others (apparently protégés of a famous तान्त्रीक guru) tell me I am denigrating the tradition of श्री-विद्या by denying the role of शङ्कर of the उपनिषद् भाष्य-s as a major guru in this tradition. kaula Tantrism has evolved like any other textual system, and there hardly any doubt that its beginnings had precious little to do with the advaita guru. Hindus need to study texts sometimes as साधकस् and sometimes (or simultaneously) as evolutionists. I do not think that the कादिविद्या भाष्य or the ललिता trishati भाष्य are productions of the great advaita guru. In a recent discussion R pointed a useful parallel: After all bauddha-s attribute their own mantra-शास्त्र revelations to the तथागत. But the historical shuddhodana-putra was mainly contending with Vedic ritualists and not Tantrics. Likewise the jaina-s attribute their mantra-शास्त्र to nirgrantha-s from महावीर backwards.

Others accuse me of denigrating the darshana-s and Hindu philosophy. The old darshana-s belong in the domain of history of Indic thought. There is much there in which needs to be studied and much which is useful in a modern context, but we must keep in mind our current scientific development relative to the early composers of the darshana grantha-s. There is no reason not to be innovative and have newer revised darshana-s.

Others accuse me of accusing the Abrahamists. The damage caused by the 2nd and 3rd Abrahamisms to the Hindus will become apparent to anyone who open-mindedly studies the history of India, and tours North India and South India to note a certain contrast.

Others wrathfully accuse me of calling quintessential aspects of modern Indian culture -- जातक astrology, Zilebias, Jahangiris, Halvas, Samosas and Kulfis to be imports from yavana-s and तुरुष्क-s. I cannot help it they are acquisition from without. Nothing new, Hindus always took what was useful to them where ever they found it (only that astrology was a bad choice!). At some point I mused that eukaryotes acquired several highpoints of their proteinic specialities from bacterial sources. It is always like that in life. At least we thoroughly internalized these things, just like the violin in South Indian classical music.

Finally, my double standards: I am accused of writing things similar to what indologists say despite claiming to be a Hindu. My main criticism of certain white indologists and their diverse fellow travelers is political. Their political activism is detrimental to Hindus as minority in क्रौञ्च द्वीप and to our people in jambu-द्वीप. If they can sign a letter supporting a German 'scholar', a full professor at a renowned school, who in a public forum called (Hi)ndus of (N)orth (A)merica as "हीन"; associated North American Hindus with Nazi-s "Since they won't be returning to India, \[Hindus immigrants to the USA] have begun building crematoria as well"; and supporting the anti-Hindu system of the म्लेच्छ subversionists (note their letter contained citations from the State Department Cold Warriors who are notorious for their anti-India stance over decades). In this yuddha my पक्ष is clear. Yes, the political views of the Indologists might affect their work, but it is not a reason to shut your eyes to it offhand. In fact, there might be useful stuff in their research. It is a well-known fact that many modern Hindus do not want to spend time studying deva भाषा or the प्रकृत्-s and cannot handle much of their literature even at a rudimentary level. So they need turn to these indologists. Like taking the Zilebia and the Samosa from whom your opponent you need to take what is useful even from the adversarial indologists. Become a नृहरी to the हिरण्यकशिपु of their political agenda but support other Indologists who are apolitical or supportive like प्रह्लाद.

What a waste of time -- the evils of अहम्कार! Indeed someone had remarked that sometimes Indians think too much about what others say about them. Sadly I fit the trend.


