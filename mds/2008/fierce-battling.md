
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Fierce battling](https://manasataramgini.wordpress.com/2008/05/12/fierce-battling/){rel="bookmark"} {#fierce-battling .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 12, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/12/fierce-battling/ "4:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Fourier's men surrendered to the अमात्य and sachiva. The second one of Fourier confirmed that the back-hurl performed by MM on behalf of PM (before he himself was taken out) had found it is mark. The blow was so severe to the second one of Fourier that their division of the army was extirpated. But the other one struck us with all available force. The power of the ग्राहिन् attacking us was so strong that we were shaken for a week. Then we knew only one remedy would work against it -- the great विद्या trailokya-स्तम्भिनी of the शान्खायन tantra. Strangely, either the ari's दूरदृष्टि or an unfortunate coincidence had thrown a small spanner on that wheel. But we pushed on undaunted with great confidence knowing the might of this deployment. With 1/2 hour of the prayoga it shattered the ग्राहिन् like वीरभद्र's गण breaking the ऋभु-s our ancestor. We thought we would have repulsed the grahin entirely after some fighting. But mysterious that small break relating to the quantity of dravya used in the prayoga allowed the ari to revive the ग्राहिन् even though it seemed destroyed. Sadly, for us our prayoga was like that of सूत-putra's shakti from indra --- it could not be used again. We were not repulsed and used the dravya of rudhira-काली to continue fighting the possibility of the ग्राहिन् surging. We realized we again had to go to the precise field old "Gaugamela" where our travails had begun in 2003. This time our अमात्य and sachiva joined us with their forces. We took precautions against the "kosha" front being stormed and marched boldly. For about 2 hours our forces held well in "Gaugamela". Then we obtained signals that we might be taken by the ग्राहिन्. But our bold sachiva came to our aid with his forces by pointing a subterranean relief point and we drove back the enemy. Then we thoroughly flattened the grounds of the old "Gaugamela" and were about to return when for the first time we got the hint that the ari might have reactivated the ग्राहिन्. We soon returned to our fort and obtained relief by deploying the last elements of the trailokya-स्तम्भिनी prayoga.

But then next the ग्राहिन् escaping attacked us again. Our षष्ठी पुजा was broken but we called on tryambaka rudra to aid us. The weather was awful but the sachiva and अमात्य pointed out that we would need to send troops to aid them in the battle close to the place where we were captured and interred by the पिशाची 3 years ago. We took the needful precautions to prevent our flank being shattered by the ग्राहिन् and marched there. We held pretty well purely relying on the रुधिरकाली dravya but after we returned to our fortress we were attacked by ग्राहिन् again. Now we did not have much cover from the trailokya-स्तंभिनी, but the अमात्य and sachiva wanted to march to aid the "friend of काम" who was preparing for the great battle. There, the virile तैत्तिरीयक had earlier been hit by this ग्राहिन् but his situation was not surprising because at that point he had no mantra-defense. We told the अमात्य and sachiva that we our shakti having been exhausted like that of कर्ण we could be finished if a मारण is launched at us at this point. We asked them to place the muni on the throne and continue the battle in this event --- after all the muni had made a major sally, not unlike the third vIra. Not for no reason we have our अमात्य and sachiva --- despite them having been outwitted by this ari on several occasions. The sachiva was unafraid and asked us to keep fighting and not think of death. The sachiva said that he had gone to the vana where he saw a vision of महादेव, whom we had invoked earlier in the morning. We used that substance of rudra along with that with which विष्णु had sent us aid exactly 1 year ago when a similar terrifying ग्राहिन् had seized. This was the third such attack the first being the long attack that began in July of 1998 shortly after ekanetra and me had met R in the evening rendezvous. Then then अमात्य also asked to boldly take to the battlefield and pointed to a secret means that would allow a second deployment of the शाखायन tantra prayoga. Though shaken we returned to the battlefield to face the ग्राहिन् with the substance of rudra and विष्णु.


