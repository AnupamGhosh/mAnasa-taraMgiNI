
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Story of siddha-haima](https://manasataramgini.wordpress.com/2008/08/20/story-of-siddha-haima/){rel="bookmark"} {#story-of-siddha-haima .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 20, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/08/20/story-of-siddha-haima/ "6:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In 1100s in the चालुक्य court of जयसिंह सिद्धराज was a nAstika (a worshiper of the nirgrantha) genius named hemachandra सूरी of the third वर्ण. His father was a shaiva and his mother a jaina. The vaishya-s saw in him a कुलचूडमणि, a rival to the ब्राह्मण-s, of whose intelligence they were perpetually jealous. So, they trained him under jaina scholars so that he might challenge ब्राह्मण-s in scholarship. The चालुक्य monarch जयसिंह invaded the परमार capital of धार and carried home the entire library of the great bhoja-शाला to लाट. Having, added the books to his own library he was one day piqued by the sight of a scholarly treatise on language written by the rival of the चालुक्य-s bhoja-deva-परमार (it was probably the सरस्वती-कण्ठाभरण though the story does not mention its name). He asked his court विद्वान्-s if anybody could produce a rival to it. The only one who stood up to the challenge was the young hemachandra-सूरी. But he declared that he needed books on the 8 traditional grammars of संस्कृत from the shAradA पीठ in Kashmir. So सिद्धराज sent his emissaries with उत्साहपण्डित to pravarapura in the Himalayan lands for an inter-library loan. There they worshiped shAradA with stotra-s. The देवी then uttered the statement that she was manifest in hemachandra and hence the books should be provided to him. The king was informed by उत्साहपण्डित that shAradA was deeply pleased with hemachandra. Then the जैनाचर्य composed his own text after consulting these works called the siddha-haima. It not only covered Sanskrit but also Prakrits and apabhramsha. It has a lengthy प्रशंस of the चालुक्य kings and this pleased the king who allotted a sum of 3*10^5 coins for is propagation. Copies with explanations were sent to libraries all over India. I only name some places mentioned in the story text to give an impression of its spread: अङ्ग, वङ्ग, कलिङ्ग, गौड and kAmarUpa in the east; जालन्धर, पारसीक, कश्मीर (20 copies were given to their library for their help), नेपाल and kAshi in the north; choLa, श्रीलन्क and कर्णाट in the south.


