
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Throwing my axe into the sea I made land arise :-)](https://manasataramgini.wordpress.com/2008/07/20/throwing-my-axe-into-the-sea-i-made-land-arise/){rel="bookmark"} {#throwing-my-axe-into-the-sea-i-made-land-arise-- .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 20, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/07/20/throwing-my-axe-into-the-sea-i-made-land-arise/ "12:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3075/2683265207_7a68f030e9_b.jpg){width="75%"}
```{=latex}
\end{center}
```



