
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Quriltai of 2008](https://manasataramgini.wordpress.com/2008/05/19/quriltai-of-2008/){rel="bookmark"} {#quriltai-of-2008 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 19, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/05/19/quriltai-of-2008/ "1:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We wondered if the मारण might finally catch up with us and we may enter the realm of vaivasvata before the time really wished it to happen. We were not like in the state of the other Quriltai where the जनाः assembled around us even as the ब्राह्मण-s around रामो भार्गव. There were many vIra-s, including the fiery third vIra, and people were praising them, but one alone stood out amongst them. Some said he was दुर्वासस् who had returned, yet others said he was datta who had returned. We went and saw that vIra. We wondered instead was this great punarvasu or the illustrious श्यावाश्व or the splendid अर्चनानस् or गविष्ठिर who had returned. We decided he was half a श्यावाश्व who had come back in the kali age, though his पार्षद-s thought he was punarvasu who had returned. We said to ourselves, this is how श्यावाश्व would have been. This was a real vIra -- when you seen one you know one -- one realizes he will be a deshikendra. As the foremost of the vipra, the गाथिन, had seen शुनःशेप and said he will be a ऋषि we too realized that the vIra will be a ब्रह्मवादिन्. Our clansman pointed the vIra out to us. Amongst the throngs of men claiming to be learned we could connect only with him, like at harischandra's sattra the kaushika connected with शुनःशेप, who was already a vipra. Will he rise to be a देवरात? The fate of men remains unknown. After all, the vigorous tree could be struck down by lighting or the farmer could dump acid thinking it is a fertilizer. I remarked to ekanetra when you see a kavi you know it, the rest are mere men.

\~*\~

Never had a quriltai of this size ever been held. Due to ग्राहिन् relentlessly shadowing us we did not lead the troops. The "friend of काम" and the virile तैत्तिरीयक asked our अमात्य and sachiva to aid their troops along with SJ's अमात्य. The third hero was also asked to participate directly in the battle. Depleted thus, we faced the ग्राहिन् alone but we kept the ग्राहिन् at bay. Just then Fourier's जनाः along with the notable मन्त्रवादिन् arrived. One from Fourier's party, who had attacked काम-mitrA and the तैत्तिरीयक, had been smashed by PM and MM's counterattack. That one appeared to surrender before the अमात्य and sachiva. We urged them to accept them to accept the surrender and end hostilities. A little after midnight, when we retired to slumber, the first vIra stole out from our tent as a spasha. He saw the ardha-श्यावाश's स्वसॄ in revelry with our clanswoman. In the guise of speaking with those ladies he espied our sachiva and his अमात्य negotiating a truce between the Fouriers and काममित्र. But the virile तैत्तिरीयक who had been hammered by the ग्राहिन्, just as I was, grew excited, and along with काम-mitra attacked the Fouriers violently. The powerful मन्त्रवादिन् from Fourier's camp who lay in wait cast an अभिचार. The next morning the first vIra reported all of this to us. While we were discussing matters with ardha-श्यावाश्व the तैत्तिरीयक and काममित्रा demanded that we should help them in a counter-prayoga. We did some minor vidhi-s but had to excuse ourselves because we along with SJ had to return to our fort aided by the सेनानी of the third vIra. While we were crossing the Susquehanna River we were seized again by the ग्राहिन् and tormented. We returned to our fort alive, but it looked we were back in square one.


