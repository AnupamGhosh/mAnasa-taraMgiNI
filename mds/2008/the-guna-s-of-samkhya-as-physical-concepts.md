
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The guNa-s of सांख्य as physical concepts](https://manasataramgini.wordpress.com/2008/01/25/the-guna-s-of-samkhya-as-physical-concepts/){rel="bookmark"} {#the-guna-s-of-सखय-as-physical-concepts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 25, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/01/25/the-guna-s-of-samkhya-as-physical-concepts/ "7:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

See also:\
[Elements of सांख्यन् theories of atomism and consciousness in early Hindu thought](https://manasataramgini.wordpress.com/2008/01/20/elements-of-samkhyan-theories-of-atomism-and-consciousness-in-early-hindu-thought/ "Permanent Link to Elements of सांख्यन् theories of atomism and consciousness in early Hindu thought"){rel="bookmark"}\
[The case of आचार्य वार्षगण्य](https://manasataramgini.wordpress.com/2008/01/21/the-case-of-acharya-varshaganya/ "Permanent Link to The case of आचार्य वार्षगण्य"){rel="bookmark"}

A pervasive element of Hindu thought is the concept of the tri-guNa-s. Their use is common in colloquial discussion: food might be classified in terms of guNa-s or even people might be categorized based on their dominant guNa. The same tri-guNa terminology might also be used to describe other abstract entities such as organismal behavior, moral qualities of a person or properties of material. The concept of tri-guNa-s while intuitively grasped by a Hindu is a very fluid in terms precise definition. Despite its pervasiveness in modern Hindu discourse its origins lie in the late- and post- vedic developments leading to सांख्य thought. Because of the imprecise and intuitive understanding of guNa-s in general usage there has been much confusion amongst many modern students of Hindu thought when the guNa-s are used in the context of technical ancient literature. In course of my study of the सांख्य-yoga system, I became increasingly aware of this issue.

Amongst modern Hindus one of the first to correctly grasp the technical meanings of the guNa terminology was the scholar from वङ्ग, Brajendranath Seal. He correctly saw that the early सांख्य thinkers considered guNa-s to be not just "qualities" but real physical concepts. That this was the original understand becomes clear from the व्यास-भाष्य. In explaining the sUtra 3.44 of पतञ्जलि the VB clearly explains each term of the sUtra as an increasingly basic description of matter -- evolute of the original प्रकृति. The VB telling uses atypical descriptors for the 3 guNa-s, namely ख्याति, क्रिया and स्तिथिशीला. These terms along with the context clearly imply that guNa-s of सांख्य were physical concepts. The sattva (=ख्याति) in the most mysterious in modern terms by a distinctive feature of सांख्य -- it might be taken to mean "information" or that aspect of matter that responds to/interacts with the puruSha (of सांख्य) or consciousness. rajas (=क्रिया) is energy and tamas (=स्तिथिशीला) is mass. That these were viewed as physical entities becomes assumes great significance in the context of a सांख्यन् conservation principle guNa-s. In its formulation this principle resembles the modern physical conservation principle of mass/energy. This is provided by the व्यास भाष्य in the commentary on sUtra 2.19 of पतञ्जलि:\
[विशेष-अविशेष लिङ्ग मात्र-अलिङ्गानि गुण-पर्वन् ॥]{style="color:#99cc00;"}\
VB: [गुणास्तु सर्व धर्मानुपातिनो न प्रत्यस्तम् अयन्ते न+उपजायन्ते । व्यक्तिभिर् एव-अतीत-अनागतव्यय-आगमवतीभिर् गुण-अन्वयिनीभिर् उपजननअपायधर्मका इव प्रत्यवभासन्ते ।]{style="color:#99cc00;"}\
"Though guNa-s go through all kinds of transformations/evolution (*अनुपात्) they are neither destroyed nor created. They appear to as if they had properties that come into being and disappear on account of the various forms of matter formed by the गुणस् past and yet to come (i.e. the forms of matter) those come and go out of existence."

As mentioned above the व्यास- भाष्य in describing the guNa-s clearly conceives them as the basis of matter. When we take that with above explanation in the context of sutra 2.19 it becomes apparent that the व्यास-भाष्य is implying the physical conservation of guNa-s. Some points of note are: 1) The word dharma here is used in conjunction with the word "अनुपतिनः". In technical Sanskrit this word is used to imply arithmetic progression or a series. So what is meant by dharma are the states of matter that form via the सांख्यन् transformation or evolutionary process from the guNa-s of प्रकृति to the परमाणु-s of the भूत-s. 2) In this light the word vyakti in the next sentence implies the various manifestations or forms of matter/energy. Thus, the सांख्यन् statement of the principle of conservation parallels the modern physical principle: "Matter+energy is neither created nor destroyed; it is only transformed from one form to another". Is this explanation provided the भाष्य and its physical interpretation as presented above compatible with the original content of the sUtra of पतञ्जली? It strongly appears this is the case because the व्यास भाष्य does consistently show a close understanding of the sUtra text. While the sUtra text is hard to render in a modern language, it does seem the भाष्य is very much in the right context. We can with some trepidation render 2.19 thus: "The evolutes of the guNa-s \[which are the basis of matter] are particular and unparticular, which are differentiated and undifferentiated \[matter]."

As far as I know the संख्य statement is the most sophisticated statement of the conservation principle in the ancient world. Amongst the Greek equivalents of the Hindus Thales had a conservation principle but it was much less sophisticated as far as I can see.


