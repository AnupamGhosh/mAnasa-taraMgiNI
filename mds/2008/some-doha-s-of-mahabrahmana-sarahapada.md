
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some दोहा-s of महाब्राह्मण सरहपाद](https://manasataramgini.wordpress.com/2008/11/22/some-doha-s-of-mahabrahmana-sarahapada/){rel="bookmark"} {#some-दह-s-of-महबरहमण-सरहपद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 22, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/11/22/some-doha-s-of-mahabrahmana-sarahapada/ "6:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3210/3049815678_d75898bb43_o.jpg){width="75%"}
```{=latex}
\end{center}
```



A recent exchange with Sटि reminded me of my attempts with the vulgar अपभ्रंश in which a rich literature of the सहजयान nAstika-s exist. While the fallen ब्राह्मण सरहपाद's works are reprehensible in a certain sense to one entrenched in स्मार्त praxis, there is still something in the fellow's free spirit that one cannot but help smile and chuckle at.

  - [दोहाकोश-गीति 96-97: (in अपभ्रंश for which Itrans is adapted just as for Prakrit -- but not suitable to conversion to nAgari):

  - [ ethu se sarasa-i सोवणाह् ethu se गङ्गा sA-aru |\
वाराणसि pa.Aga ethu se चान्द divA.aru ||\
khetta पिट्ठ u.a.पिट्ठ ethu ma.i bhami.a समिट्ठ.u |\
देहा-sarisa tittha ma.i सुण.u Na दिट्ठ.u ||]{style="color:#ff9900;"}*]{style="font-style:normal;"}

  - \
What saraha is telling us is that तीर्थ-s of bhArata are right in your body: the sarasvati संगम, the गङ्गा-सागर, वाराणसि and प्रयाग and even the moon and the sun. He roamed all the क्षेत्र-s, पीठ-s and upa-पीठ-s right here in the body and has not heard of or seen a तीर्थ that compares with the body.


