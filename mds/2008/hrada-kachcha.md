
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [hrada-कच्छ](https://manasataramgini.wordpress.com/2008/10/18/hrada-kachcha/){rel="bookmark"} {#hrada-कचछ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 18, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/10/18/hrada-kachcha/ "6:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3009/2681760996_19ee4ce31d.jpg){width="75%"}
```{=latex}
\end{center}
```



It was in the summer of 1995, just before I left the shores of जम्बुद्वीप for क्रौञ्चद्वीप. The boundless energy and false idealism of youth filled us then. The ब्राह्मण from the लाट country, who was then my friend, and me were walking down the garden path in the city of my birth discussing a scientific problem that was in the years to come give one of the most profound insights into life --probably the high point of my life. We ran into the naturalists महाश्मश्रू, धूम्रमुख and the म्लेच्छ-स्त्री वल्गुली of sweet smiles from the "ugra-desha". They told us of their adventures and the journey the lake near the abandoned देवालय. We wished to go there too and conspired to achieve it with ST who was then working in a lab near mine. Knowing that ST was going, the above-mentioned लाटजन was filled with the urge to come along, to mill around in her company, probably not to enjoy the nature (though his skills in ecology were not trivial). Since we planned it on a civil holiday ekanetra also joined us.\
Many years later...\
I was in the house of ekanetra and ST. ST and ekanetra said with some Schadenfreude: "O वात्स्य you showered favors on her that is most ill-mannered and showed so much courtesy to her who is a typical rascal from sindhupattana". I replied: "it is राजनीति, friends, in the vanguard of समीरा's सेना I will make her attack the long-coveted fort. She alone has the astra-s to breach that fort: Odysseus and Philoctetes". Then ST opened a plastic wrap to show how she had achieved her greatest success -- the bhojya version of a yavana cheese called manouri. It was like balancing on a knife-edge between molds and bacteria. While savoring this delight worthy of the deva-s I showed them the above photo taken by the first hero to ST and ekanetra. Their jaws nearly dropped...

The reason: the hero of himahrada had chosen exactly the same view for the photo as the view that captivated us on that journey to the lake. We spent a while wandering about looking at rather interesting plants, many of which were to vanish in that city the coming years due to the apathy of our peoples. I used this session to identify some ओषधि-s mentioned in the शास्त्र-s as befits an अथर्वण. Some weed and herbs of interest that were worthy of future investigation that I never did were: 1) A particular acrid agnigarbha *(Ammannia baccifera*) which caused burns on rubbing on the skin. There were claims that it could counter fungal infections in topical applications. 2) krimibandha (*Drosera burmanni*) it is one of the fastest acting Droseras that was brought to our attention by ST. It traps insects within a few seconds against others that might take a while. Many claims have been made for it in countering broncho-constriction and inflammation and the like. We do not know if any of this is true. 3) नीलोष्ठ (*Oxytropis*) is claimed by some to be a component of the a concoction of our ancient ancestor च्यवान. It might be a selenium source.4) agasti (*Sesbania grandiflora*) is claimed by some to help in epilepsy. 5) shyonaka (*Oroxylum*) is supposed to relieve pains of the body.


