
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [शडर chakra](https://manasataramgini.wordpress.com/2008/09/22/shadara-chakra/){rel="bookmark"} {#शडर-chakra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 22, 2008]{.entry-date}](https://manasataramgini.wordpress.com/2008/09/22/shadara-chakra/ "12:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/farm4.static.flickr.com/3190/2877424022_2ba74c01b8_o.jpg){width="75%"}
```{=latex}
\end{center}
```



