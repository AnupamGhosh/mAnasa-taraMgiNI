
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Platonic cnidaria, brachiolaria and tornaria](https://manasataramgini.wordpress.com/2013/04/20/platonic-cnidaria-brachiolaria-and-tornaria/){rel="bookmark"} {#platonic-cnidaria-brachiolaria-and-tornaria .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 20, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/04/20/platonic-cnidaria-brachiolaria-and-tornaria/ "7:10 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-CBij-ppAckA/UXLiQjFDkqI/AAAAAAAACpg/ulISJNUw7DY/s800/mandel_bulb.jpg){width="75%"}
```{=latex}
\end{center}
```



