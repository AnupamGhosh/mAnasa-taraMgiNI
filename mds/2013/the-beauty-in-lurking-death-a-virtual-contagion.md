
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The beauty in lurking death: A virtual contagion](https://manasataramgini.wordpress.com/2013/10/15/the-beauty-in-lurking-death-a-virtual-contagion/){rel="bookmark"} {#the-beauty-in-lurking-death-a-virtual-contagion .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 15, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/10/15/the-beauty-in-lurking-death-a-virtual-contagion/ "6:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-g_QKYw-BUTU/Ulzg6bzINeI/AAAAAAAACwI/TBUnAjx3sDM/s400/virus_small.jpg){width="75%"}
```{=latex}
\end{center}
```



