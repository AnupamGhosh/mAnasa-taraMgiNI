
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A Sayyid kills a Kashmirian yogin](https://manasataramgini.wordpress.com/2013/07/22/a-sayyid-kills-a-kashmirian-yogin/){rel="bookmark"} {#a-sayyid-kills-a-kashmirian-yogin .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 22, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/07/22/a-sayyid-kills-a-kashmirian-yogin/ "6:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Kashmirian ब्राह्मण जोनराज wrote a राजतरंगिणि describing the rule of various Moslem tyrants in Kashmira after the end of Hindu rule. He is rather laudatory in his description of the Sultan Zayn al Abidin (reign 1418-1470 CE), who is described as being rather pro-Hindu and non-iconoclastic. However, this did not mean that mayhem did not occur in his reign. The incident we present below from जोनराज's memoir has reverberations in the incidents that have happened even in the last 25 years even as the मरून्मत्त assault is aided and abetted by the म्लेच्छ-s and chIna-s.

[मक्क-देशागतो जातु पुस्तकाडम्बरं वहन् ।]{style="color:#99cc00;"}\
[सैदाल-नामा यवनो राजेन्द्रं तम् उपागतम् ॥]{style="color:#99cc00;"}\
A Mohammedan named अली the Sayyid who came from the Mecca country, carrying ever-empty verbose texts (i.e. the rAkShasa-pustaka and the recollections of the Adyunmatta) approached the sulTAn (Zayn al Abidin).

[गुणान् विकत्थमानं तं गुणिरागी नरेश्वरः ।]{style="color:#99cc00;"}\
[उपागच्छत् प्रतिदिनं दर्शनायेतरो यथा ॥]{style="color:#99cc00;"}\
As he proudly declared his ancestry (i.e. from the Adyunmatta), the sulTAn, attracted by endowed men, went to see him every day as if he was the Sayyid, and the Sayyid was the ruler.

[स तस्य पटहस्येव राजापश्यत् क्रमाद् असौ ।]{style="color:#99cc00;"}\
[अन्तः सारविहीनत्वं परीक्षायां विचक्षणः ॥]{style="color:#99cc00;"}\
In due course the discerning sulTAn realized by examining \[the Sayyid] that the Sayyid was bereft of substance as an empty kettle-drum (That is he was all talk with little substance).

[म्लेच्छ-मस्करिणि क्षोणिप्राणेशो निर्गुणे ऽपि सः ।]{style="color:#99cc00;"}\
[प्रेमाणं नामुछत् पुत्रे पितेव करुणार्णवः ॥]{style="color:#99cc00;"}\
Even though the Mohammedan godman was without merits, the ruler of the land, an ocean of compassion, did not take away his kindness to him like a father from his son.

[प्रदोषस्येव तमसां दुर्घनस्येव विद्युताम् ।]{style="color:#99cc00;"}\
[दोषाणां बहुता तस्य प्रजाः समुदवेजयत् ॥]{style="color:#99cc00;"}\
His many vices the bothered the people, as the darkness of the evening hour or the lighting flashes from a dark cloud.

[तस्मिन्न् अवसरे कश्चिद् योगिराजो जितेन्द्रियः ।]{style="color:#99cc00;"}\
[न्यविक्षतोन्नते स्तम्भे योगाभ्यासस्य सिद्धये ॥]{style="color:#99cc00;"}\
At that time a certain, master of yoga who had controlled his senses sat down on a tall pillar to perfect his yoga practice.

[स्तम्भोपरि नवाहानि निराहारम् अपश्यतः ।]{style="color:#99cc00;"}\
[तस्याशिषैव महिषी राज्ञः पुत्रम् अजीजनत् ॥]{style="color:#99cc00;"}\
He \[performed yoga] atop the pillar for nine days not bothered by his abstinence from food. Indeed by his blessings the bIbI gave birth to a son.

[तपस्यतस् तथा तस्य तत्र तन् नवमं दिनम् ।]{style="color:#99cc00;"}\
[राज्ञस् त्व् अनवमं पुत्र-जन्म-काल-महोत्सवैः ॥]{style="color:#99cc00;"}\
This ninth day of the yogin's tapasya there [ atop the post] was indeed however, a day of great celebration for the sulTAn on the occasion of his son's birth.

[हेमन्तान्ते पतति तुहिनं फाल्गुणे वल्गु वल्गद् ।]{style="color:#99cc00;"}\
[दीपस्यापि स्फुरति सुतरां शान्तिकाले ऽतिदाहः ॥]{style="color:#99cc00;"}\
At the end of the cold season in the फाल्गुण month the snowflakes dance beautifully,\
a lovely dance. Just before a lamp dies out it flares up greatly.

[शैत्योद्रेको वहति वहति ग्रीष्मकाले तुषारं]{style="color:#99cc00;"}\
[वस्तु प्रायः प्रथयति निजं धर्मम् आयाति नाशे ॥]{style="color:#99cc00;"}\
The coming of a cold wave during summer brings snow. When destruction is coming, things commonly display their innate intuition \[for catastrophe].

[तपः कवचम् अस्पृष्ट्वा तम् अधो मार्गणैर् गतम् ।]{style="color:#99cc00;"}\
[प्रहर्तुर् इव निर्देष्टुं भविष्यन्तीं तथा गतिम् ॥]{style="color:#99cc00;"}\
Avoiding contact with the yogin, whose armor was his tapasya, he \[the Sayyid] went below, and stationed himself aiming to assail \[the yogin].

[राजमान्यं तम् आलोक्य कुप्यन्तं शान्त-मानसः ।]{style="color:#99cc00;"}\
[निर्णयाद् अछलद् वर्णी नयान् न तु भयोदयात् ॥]{style="color:#99cc00;"}\
\[The yogin] of controlled mind saw the frenzied Sayyid who was honored by the sulTAn. Then out of prudence the yogin acted; but certainly not from fear \[of the Mohammadan].

[स स्तम्भान् न महाधैर्याद् अवारोहन् महामनाः ।]{style="color:#99cc00;"}\
[नैवारोहज् जगद् दृष्टिं सद्यो द्याम् अपि योगवित् ॥]{style="color:#99cc00;"}\
This high-minded yogin did not descend from the pillar due to his great courage. Nor did he ascend to heaven, from where he could see the world.

[अतिथिं योगपथिकं मा वधीर् माम् इति ब्रुवन् ।]{style="color:#99cc00;"}\
[म्लेच्छमस्करिणा वर्णी खड्ग-घातैर् अचूर्ण्यत ॥]{style="color:#99cc00;"}\
"I am a guest traveling on the yoga path, do not kill me!" he said. But the Mohammedan godman hacked him into pieces with strikes of his sword.

[अत्य् अर्थ-दर्शन-द्वेषान् मदिरा-मद-मोहितः ।]{style="color:#99cc00;"}\
[स म्लेच्छ-सहितो योगिराजं तम् अवधीच् छरैः ॥]{style="color:#99cc00;"}\
Intoxicated by the liquor of hate towards what he saw \[the yogin] the Mohammedans who accompanied him \[Sayyid] struck the yoga-master with arrows.

[संतप्तैर् मलिनैः स्थूलैर् जनानां तद् विलोकनात् ।]{style="color:#99cc00;"}\
[भूतले पतितं वाष्पैर् अपवादैश् च राजनि ॥]{style="color:#99cc00;"}\
Having seen that \[the killing of the yogin] people shed hot, stained, heavy tears on the ground, conveying with that their censure to the sulTAn.

[पृथ्वी-नाथो ऽथ तच् छ्रुत्वा शुद्ध्य् अर्थम् इव मग्नवान् ।]{style="color:#99cc00;"}\
[भी-ह्री-शोक-क्रुधाश्चर्य-कृत्य-चिन्तार्णवेषु सः ॥]{style="color:#99cc00;"}\
Having heard of that the lord of the land as if to purify himself, dipped himself into an ocean of fear, shame, sorrow, anger, bewilderment and brooding over what to do.

[प्रथमोद्भूत पुत्रे ।अपि तस्मिन्न् अह्नि महीभुजा ।]{style="color:#99cc00;"}\
[नास्नायि नाभ्यवाहारि न व्यवाहारि नाकथि ॥]{style="color:#99cc00;"}\
Even though it was the day his first son was born, the ruler of the land did not bath, eating, hold court or talk.

[अन्येद्युर् भूपतिः पृष्ट-स्मृति-ज्ञ-गुरुकोविदः ।]{style="color:#99cc00;"}\
[हन्तुर् दण्डं वधं शृण्वन् करुणायन्त्रितो ऽभवत् ॥]{style="color:#99cc00;"}\
On the next day, when he was told by his learned guru-s who knew the स्मृति-s that \[the Sayyid] had to be sentenced to death he was overcome with compassion.

[प्रतीपं खरम् आरोप्य प्रतिहट्टं परिभ्रमम् ।]{style="color:#99cc00;"}\
[नर-मूत्राभिषिक्तस्य कूर्चस्य परिकर्तनम् ॥]{style="color:#99cc00;"}\
As punishment he sentenced the Sayyid to be seated on a donkey and paraded around the market place. Human urine was poured on him and he was shaved off completely.

[ष्ठीवनं सर्वलोकानां प्रेतान्त्रैर् बाहुबन्धनम् ।]{style="color:#99cc00;"}\
[जीवन्-मरणम् आदिक्षद् दण्डं तस्य कृशायतेः ॥]{style="color:#99cc00;"}\
Everyone was to spit on him and he was tied up with the entrails of corpse. Thus, it was to be a bad fate of living death for him.

[राजनि म्लानि-हीनानि दिक्-सौगन्ध्य-वहानि छ ।]{style="color:#99cc00;"}\
[अपतन् नाक-पुष्पाणि पौराशीर्-वचनानि च ॥]{style="color:#99cc00;"}\
Then heavenly flowers, unwilting carrying fragrances from all corners showered on the sulTan and also the praise of the people.

There are some things one could read between the lines:

 1.  The Sayyid though very empty was greatly honored by the sulTAn. The excessive honoring of sharifs by Indian सुल्टन्स् in the pre-Mogol period has been noted in other cases too (example Mohammad ibn Tughlaq placing himself at the foot of a Sayyid). They obviously held a tremendous prestige to the point that the Turkic sultans prostrated themselves before these Arabs. This account shows that Zayn was very much taken in by the prestige of this Arab despite his Hinduizing tendencies.

 2.  Ultimately, there is no real safety for Hindus under Mohammedan rule, however, pro-Hindu he might be a Hindu might be murdered as long as there are blood thirsty Meccans around. The sulTan took the murder of the yogin by the Mohammedans seriously enough to punish the Sayyid strongly. Yet this and other actions of his \[footnote 1] do not mean in any way that even a "good" Mohammadan rule is a safety net for Hindus. For after all his successors can restore true Mohammedanism even if he does not.

Footnote 1\
जोनराज says:\
[हिन्दवः सूह-भट्टेन बलाद् ये पीडिता भृशम् ।]{style="color:#99cc00;"}\
[परदेशं गतास् ते तु बिभ्रतो वेष-कल्पनाम् ॥]{style="color:#99cc00;"}\
Hindus were violently oppressed by सूहभट्ट (who converted to Mohammedanism as Saif-ad-dIn or the sword of Islam and aided Sikandar bhUt-shikan in his persecution of Hindus). They fled to foreign lands or they took on the disguise of Muslims.

[निजाचारे रता नित्यं तदाचार-द्विषो हृदा ।]{style="color:#99cc00;"}\
[स्वम् आचारम् अनुष्ठातुं प्रेरिताश् च द्विजैर् बलात् ॥]{style="color:#99cc00;"}\
Yet in their hearts they were true to their own tradition \[i.e. Hindu dharma; that is why one encounters several Moslem surnames among Kashmirian Hindus] and hated Mohammadanism. The twice-born strongly inspired them them to uphold their own tradition \[It is important to note that the Kashmirian ब्राह्मण-s despite being the target of much oppression struggled hard to uphold the Hindu dharma].

[दारिताः स्वत्राणभयाद् उत्कोछार्पण तत्पराः ।]{style="color:#99cc00;"}\
[रक्षिता भूमिपालेन तद् उपद्रव-वारणात् ॥]{style="color:#99cc00;"}\
Tortured and from the fear of protecting themselves they applied themselves to find ways to exit from Kashmir \[note that Saif-ad-dIn had posted forces to track down refugees exiting Kashmir and capture them and convert them to Islam]. However, the sulTAn Zayn intending to dispel the troubles inflicted by the trouble-elephant \[Saif or Sikandar] gave them protection \[He enabled Hindus to return to Kashmir; R suspects that her ancestors might have fled via Jammu to Kangra during Sikandar's reign].


