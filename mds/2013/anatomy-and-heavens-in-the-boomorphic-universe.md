
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Anatomy and heavens in the boomorphic universe](https://manasataramgini.wordpress.com/2013/11/08/anatomy-and-heavens-in-the-boomorphic-universe/){rel="bookmark"} {#anatomy-and-heavens-in-the-boomorphic-universe .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 8, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/11/08/anatomy-and-heavens-in-the-boomorphic-universe/ "7:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The bovine species lay at the center of the existence of the early Indo-Aryan. After all he owed his very success in history to the strength of milk. Hence, rather appropriately payas means both milk and strength in संस्कृत. Not surprisingly, he tended to literally see the world as a bovine expression of cosmic proportions, replete with homologies or संबन्ध-s between the macrocosmos and microcosmos of the bovine body. In a sense this persists in the image of bhArata even today: videshin-s instinctively associate bhArata with the "holy cow", even though a deracinated, urban, modern Hindu might try to protest that India is not just about cows. All this said, the evidence suggests that the spread of the Indo-Aryans within भारतवर्ष itself was accompanied by the spread of their bovine herds and at the genetic the spread of a certain strain of [lactose tolerance](https://manasataramgini.wordpress.com/2009/02/12/genetic-determinism-sociology-and-history/ "Genetic determinism, sociology and history"). Not surprisingly, bovine symbolism is central to Aryan ritual and thought, be it in the plaint of the Iranian zaotar zarathushtra, i.e., geush urvan or the many ritual incantations of the atharvaveda. Indeed, the strong bovine connection to the atharvan-s (and their Iranian counterparts) emerges as the central element of the famous legend of our clan: the theft of the family cow leading to the multi-generational feud which ends in the great war between the भृगुस् and the haihaya-s (alluded to in the brahma-gavi spell of the atharvaveda). Thus, in the AV bovine rites we sense a special connection to our forebears, rites which were done by भृगु and paulomi, the clan-furthering च्यवान and सुकन्या, the famed अप्नवान and ruchi, ऋचीक and satyavati, jamadagni and रेणुका.

Among others the taurocentric Weltanschauung of the atharvan-s is best captured in a yajush incantation from the vulgate atharvaveda (AV 9.7), which provides a remarkable parallel to the Iranian legend of the gav-aēvō-दाता or the [ratu](https://manasataramgini.wordpress.com/2012/06/16/the-broken-chain-and-the-chain-of-knowledge/ "The broken chain and the chain of knowledge") of the bovine (eventually slain by angra mainyu). This incantation is part of the offering verses belonging to a now largely forgotten ritual performed by the भृगु-s and अङ्गिरस-s of the atharvan tradition. These rituals belong to a category know as the आथर्वण sava-s which are only performed by the atharvavedin-s and differ from the sava-s performed by the ritualists belonging to other vaidika traditions. These sava-s are performed usually in the उत्तरायण period and may be done only if the ritualist's female partner is initiated in the appropriate mantra lore and has had an earlier AV ritual दीक्ष with the yoktra girdle tied around her waist. Before the sava a reddish-brown bull should have been sacrificed to the terrifying महादेव and its organs offered thus with the names of rudra:\
chitta: the sinews;\
bhava: the liver,\
rudra: the pancreas\
pashupati: the stomach\
agni: the heart,\
rudra: the blood\
sharva: the kidneys:\
महादेव: the marrow of the ribs\
औषिष्ठहन्: intestine and colon\
Its complete hide is then prepared as a seat for the ritualist and his पत्नी.

The two then perform the sava दीक्ष. Here sitting on the bull hide with its neck facing east and its hairy side upwards they offer oblations of ghee into the fire with all the mantra-s from AV-vulgate 6.114-124 (known as the deva-हेडनं recitation). When in course of this rite they make oblations to yama they touch water after each स्वाहा. On completion they observe the vrata for 3 days during which they abstain from all sexual activity. They bow to all the directions and thereafter the mortar, pestle and winnowing fan are purified with mantra-s. They churn out and establish the fire with the incantations specified in the kaushika sUtra chapter 60. The पत्नी cooks a plate of rice on the ritual fire and mixes it with ghee and fresh milk. One part is cut out and kept for offering to the ancestors, one part is used to feed ब्राह्मण-s and the third part is used for the daiva oblations. For the rite the ritualist and his पत्नी wear identically colored garments. If he desires cattle he uses the bovine incantation (AV 9.7) to make oblations to the deities imagining the whole universe to be a gigantic bovine:

[प्रजापतिश् च परमेष्ठी च शृङ्गे इन्द्रः शिरो अग्निर् ललाटं यमः कृकाटम् ॥१॥]{style="color: #0000ff"}\
prajApati and परमेष्ठिन् are the two horns, indra is the head, agni the forehead, yama the cranio-cervical joint.

[सोमो राजा मस्तिष्को द्यौर् उत्तरहनुः पृथिव्य् अधरहनुः ॥२॥]{style="color: #0000ff"}\
King soma the brain, the heaven (dyaus) the upper jaw and the earth (पृथिवि) the lower jaw.

[विद्युज् जिह्वा मरुतो दन्ता रेवतीर् ग्रीवाः कृत्तिका स्कन्धा घर्मो वहः ॥३॥]{style="color: #0000ff"}\
Lighting the tongue, the marut-s the teeth, the रेवती-s the neck, कृत्तिका the shoulders and heat (or the pravargya pot) the cervico-thoracic joint.

[विश्वं वायुः स्वर्गो लोकः कृष्णद्रं विधरणी निवेष्यः ॥४॥]{style="color: #0000ff"}\
वायु the all encompassing lungs, the dewlap the heavenly world, the cyclone the diaphragm.

[श्येनः क्रोडो ऽन्तरिक्षं पाजस्यं बृहस्पतिः ककुद् बृहतीः कीकसाः ॥५॥]{style="color: #0000ff"}\
The heavenly eagle the manubrium, the atmosphere the sternal body, बृहस्पति the hump and the बृहती incantations the vertebrae.

[देवानां पत्नीः पृष्टय उपसदः पर्शवः ॥६॥]{style="color: #0000ff"}\
The goddesses the zygapophyses, the upasad ritual the ribs.

[मित्रश् च वरुणश् चांसौ त्वष्टा चार्यमा च दोषणी महादेवो बाहू ॥७॥]{style="color: #0000ff"}\
mitra and वरुण the clavicles, त्वष्टृ and aryaman are the humeri and महादेव the forearms.

[इन्द्राणी भसद् वायुः पुछं पवमानो वालाः ॥८॥]{style="color: #0000ff"}\
इन्द्राणी the sacrum, वायु the tail, and the purifying soma the tail whisk.

[brahma cha क्षत्रं cha श्रोणी balam UrU ||9||]{style="color: #0000ff"}\
ब्राह्मण-s and kShatriya-s the ilia and strength the femora.

[धाता च सविता चाष्ठीवन्तौ जङ्घा गन्धर्वा अप्सरसः कुष्ठिका अदितिः शफाः ॥१०॥]{style="color: #0000ff"}\
धातृ and सवितृ the patellas, the gandharva-s the calves, apsaras-s the dew claws and aditi the hoofs.

[चेतो हृदयं यकृन् मेधा व्रतं पुरीतत् ॥११॥]{style="color: #0000ff"}\
Consciousness the heart, intelligence the liver, and vrata-s the pericardium.

[क्षुत् कुक्षिर् इरा वनिष्ठुः पर्वताः प्लाशयः ॥१२॥]{style="color: #0000ff"}\
Hunger the belly, food the rectum, and the mountains the pancreas.

[क्रोधो वृक्कौ मन्युर् आण्डौ प्रजा शेपः ॥१३॥]{style="color: #0000ff"}\
anger the kidneys, fury the gonads, the beings the genitals.

[नदी सूत्री वर्षस्य पतय स्तना स्तनयित्नुर् ऊधः ॥१४॥]{style="color: #0000ff"}\
The rivers the blood vessels, the breasts the lord of rain (parjanya), the thunder the udders.

[विश्वव्यचास् चर्मौषधयो लोमानि नक्षत्राणि रूपम् ॥१५॥]{style="color: #0000ff"}\
The bounds of the universe the skin, the plants the hairs, the stars comprise the form.

[देवजना गुदा मनुष्या आन्त्राण्य् अत्रा उदरम् ॥१६॥]{style="color: #0000ff"}\
The god-folks in the large intestine, the men in the intestines and animals in the uterus.

[रक्षांसि लोहितम् इतरजना ऊवध्यम् ॥१७॥]{style="color: #0000ff"}\
रक्ष-s in blood, the यक्ष-s (literally other beings) in the digestive secretions.

[अभ्रं पीबो मज्जा निधनम् ॥१८॥]{style="color: #0000ff"}\
The clouds the fat and death in the marrow.

[अग्निर् आसीन उत्थितो ऽश्विना ॥१९॥]{style="color: #0000ff"}\
When resting \[the bovine] is agni, when standing \[the bovine represents] the ashvin twins.

[इन्द्रः प्राङ् तिष्ठन् दक्षिणा तिष्ठन् यमः ॥२०॥]{style="color: #0000ff"}\
indra when standing eastwards and yama when standing southwards.

[प्रत्यङ् तिष्ठन् धातोदङ् तिष्ठन्त् सविता ॥२१॥]{style="color: #0000ff"}\
धातृ when facing west and सवितृ when facing north.

[तृणानि प्राप्तः सोमो राजा ॥२२॥]{style="color: #0000ff"}\
When seeking grass \[the bovine] is soma.

[मित्र ईक्षमाण आवृत्त आनन्दः ॥२३॥]{style="color: #0000ff"}\
When observing \[the bovine] is mitra when running away felicity.

[युज्यमानो वैश्वदेवो युक्तः प्रजापतिर् विमुक्तः सर्वम् ॥२४॥]{style="color: #0000ff"}\
\[The bovine ] is of the vishve devas when being yoked, prajApati when yoked and to all when freed.

[एतद् वै विश्वरूपं सर्वरूपं गोरूपम् ॥२५॥]{style="color: #0000ff"}\
The is the omniformed, bearing all forms, in the form of a cow.

[उपैनं विश्वरूपाः सर्वरूपाः पशवस् तिष्ठन्ति य एवं वेद ॥२६॥]{style="color: #0000ff"}AV-vulgate (9.7)\
Omniformed cattle, of all forms attend on him who knows thus!

At first sight this collection of mantra-s epitomizes the macranthropic (or macrotherian) theme. Seen abundantly in Hindu traditions from the veda onwards, which we have earlier discussed on [these pages](https://manasataramgini.wordpress.com/2012/09/21/macranthropy-and-the-sambandha-s-between-microcosm-and-macrocosm/ "Macranthropy and the संबन्ध-s between microcosm and macrocosm"). However, a closer examination shows that it is not just any macrotherian theme but a specific version, which we may term stellar macranthropy. In this form we have the explicit equation of parts of the macranthropic entity with stars/constellations in the sky. This interpretation overturns the assertion made in the last century by the American indologist Whitney who translated the AV and is largely followed by white indologists and their fellow travellers thereafter:\
"*The रेवती-s and कृत्तिका-s are two asterisms in Pices and Taurus; their connection with the parts to which they are assigned is, as in nearly all the other cases in this hymn, of the most purely imaginary and meaningless kind.*"

However, the statement in AV 9.7.15 that the stars comprise the form (नक्षत्राणि रूपम्) of the bovine suggest that the allusion to these constellations is not purely meaningless as the indologist would have us believe. Rather it indicates the macranthropic bovine was indeed meant to span the stars and the mention of the two constellations is not entirely without meaning. This class of stellar macranthropic constructs are repeatedly encountered in the veda. As the Hindu scholar Narayana Aiyangar had pointed out 115 years ago in his "Essays on Indo-Aryan mythology", these stellar constructs occur at least three times in distinct forms in the तैत्तिरीय shruti: The starry prajApati (तैत्तिरीय ब्राह्मण 1.5.2.2); the starry dolphin or शिशुमार (तैत्तिरीय AraNyaka 2.5.13) and the उत्तरनारायण or the puruSha (तैत्तिरीय AraNyaka 3.13). This tradition of stellar puruSha continued down to the पुराण-s (e.g अग्निपुराण chapter 196) and is also specified in detail in the 105th chapter of the बृहत्-saMhitA of the great naturalist वराहमिहिर. In this later version the नक्षत्र puruSha is also invoked as विष्णु (शाम्भवायनीय ritual) during every month of the year. The ritual is supposed to be performed by both men and women for greater sexual accomplishment. In the parallel later बादरायणीय ritual, the stellar prajApati is comprised of राशी-s instead of नक्षत्र-s. Returning to the yajurvedic versions of the rite we find that the उत्तरनारायण is the least detailed in terms of equivalences. However, that it represents the नक्षत्र puruSha is clear because it has the explicit statement, same as that seen in AV 9.7.15 ([नक्षत्राणि रूपम् ॥]{style="color: #0000ff"}). This is followed by the only explicit association it makes:\
[अश्विनौ व्यात्तम् ॥]{style="color: #0000ff"}\
His open mouth are the twin ashvin-s. This choice is strange at first sight but makes sense if it is taken to indicate the position of the beginning of the नक्षत्र-s on the ecliptic: a proper parallel to the statement in the तैत्तिरीय ब्राह्मण, where the नक्षत्र lists begin with क्ऱ्‌^त्तिका, that कृत्तिका is the mukham (mouth) of the नक्षत्र-s. As correctly inferred by Tilak, this indicates that the नक्षत्र at the vernal equinoctical point marked the beginning of the list and was termed the mouth. Now, when the ashvin-s are at the mouth it clearly indicates a period after the क्ऱ्‌^त्तिका period as ashvayajau the नक्षत्र of the ashvinau is two नक्षत्र positions away from क्ऱ्‌^त्तिका. Thus, the उत्तरनारायण might be inferred as being approximately 1100-900 BCE in age (see figure below; \[Footnote 1]).
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-GnY3Fm9Yo6E/UocmJ3luhaI/AAAAAAAACxI/Ovoh3fM5AOk/s640/ashvayuja.jpg){width="75%"}
```{=latex}
\end{center}
```

*The equinox at ashvavyuja around 1000 BCE*

Thus, it clings to the Vedic corpus at its fag end and is consistent with its late hallmarks, namely the rise of puruSha नारायण, who was superseding prajApati in most of his roles. This also provides the transition between the vaidika नक्षत्र puruSha and पौराणिक one clearly identified with विष्णु. This period might correspond to the rise of the नारायणीय or epic पाङ्चरात्र which alludes to these mantra-s as the महोपनिषत्. This it negates the nonsensical and obviously biased assertion of the American indologist Whitney maintained to this date among Abrahamistic white indologists and their imitators:\
*"The Hindus borrowed their नक्षत्र system from Mesopotamia and would probably have retained it in that form \[i.e. with कृत्तिका at the vernal equinox of 2300 BCE] until the present day but for the revolution wrought in their science by Greek teaching."*\
Clearly even within the vaidika period (including वेदाङ्ग ज्योतिष) the Hindus had made multiple changes for precession not just at the time of वराहमिहिर after interactions with yavana-s.

The vaidika stellar macrotherian, the शिशुमार, also hints a specific astronomical position: it describes the constellation of Draco with the deva-s, ritual entities and atri the primordial astronomer among the vipra-s identified with various stars of the constellation. It is described as containing the pole star dhruva and is associated with the ritual where the यजमान recites this incantation while gazing at the north pole. This was correct for alpha Draconis at 2800 BCE (below).
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-GmYGUPCZXGU/UofM-4Eo05I/AAAAAAAACxY/H385SZRAqxw/s640/शिशुमार.jpg){width="75%"}
```{=latex}
\end{center}
```


  - 

Now returning to the macrotherian bovine we observe that कृत्तिका and रेवती do not occupy the head of the bovine. Rather at the "forefront" of the bovine is prajApati who is identified with the horns. In vaidika tradition prajApati is traditionally associated with रोहिणी (alpha Tauri; [प्रजापते रोहिणीवेतु पत्नी ।]{style="color: #0000ff"}) and the Hyades cluster from which the horns of the bovine emerge. So, we propose that the forefront of the stellar macrotherian bovine is the fore part of Taurus and क्ऱ्‌^त्तिका and रेवती are placed behind it for a reason, contra-Whitney. Now one could protest that we are bringing in a राशी of the Bull, which the dull-witted Hindus are not supposed to have known in the Vedic period, rather adopting it much latter from their erudite West Asian and Greek teachers. However, we, like several before us, argue that the Indo-Aryans were very much aware of constellations distinct from नक्षत्र-s that were used primarily for a lunar purpose. Starting from Tilak, it has been proposed the that dogs (Canis Major and Canis Minor), Bear, Draco, Hunter (in Hindu world moved to Canis Major on occassions); मृग and other constellations were known and figured distinctly from the ecliptic divisions in the form of the नक्षत्र-s. Indeed the bovine associated with Taurus seems to be one such. Several lines of evidence support this view:

 1.  We had earlier argued this with respect to the [सौचीक agni mantra](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/).


 2.  The atharvaveda states:\
[या रोहिणीर् देवत्या गावो या उत रोहिणीः ।]{style="color: #0000ff"}(AV-vulgate 1.22.3ab)\
Of the cattle which indeed are red, of them रोहिणी is the deity.\
Thus, रोहिणी (alpha Tauri) the नक्षत्र is linked to cattle.


 3.  A mantra of कक्षीवान् of the gotama clan to the ashvin-s states:\
[रेवद् उवाह सचनो रथो वां वृषभश् च शिंशुमारश् च युक्ता ॥]{style="color: #0000ff"}\[RV 1.116.18cd]\
The chariot that bore you two brought beautiful riches: a bull and a dolphin were yoked together.\
This absurd yoking of the bull and the dolphin to the car of the ashvin-s has greatly mystified later observers and practitioners of this mantra. However, it can be resolved, if as noted above the शिशुमार is the constellation of Draco which marks the cycle of the chariot that revolves daily around the north pole. The yearly revolution was the one corresponding to the bull or Taurus. Around the time the vernal equinox was in रोहिणी, there was also a northern polestar in the form of alpha Draconis, the dhruva of शिशुमार (above picture). This further strengthens the link between the नक्षत्र रोहिणी and a constellation equivalent to Taurus overlapping with it.


 4.  The above is further supported by a mantra to the ashvin-s composed by agastya that states:\
[प्र वां शरद्वान् वृषभो न निष्षाट् पूर्वीर् इषश् चरति मध्व इष्णन् ।]{style="color: #0000ff"}\
[एवैर् अन्यस्य पीपयन्त वाजैर् वेषन्तीर् ऊर्ध्वा नद्यो न आगुः ॥]{style="color: #0000ff"}(RV 1.181.06)\
Approximately: The autumnal bull of you two, like a mighty one comes forth, sending from the east, sending forth honey. Let them swell with the other ways and strength: the heavenly speeding rivers (high rivers) have come to us.\
Here the bull of the ashvin-s is again alluded to as rising in autumn in the east (which would correspond with Taurus at the vernal equinox) and the heavenly rivers are said to come along. While some interpret this as rain. We suspect it is the Milky Way which rises just ahead of Taurus.


 5.  Another allusion comes from the mantra of वामदेव gautama to agni:\
[स जायत प्रथमः पस्त्यासु महो बुध्ने रजसो अस्य योनौ ।]{style="color: #0000ff"}\
[अपाद् अशीर्षा गुहमानो अन्तायोयुवानो वृषभस्य नीळे ॥]{style="color: #0000ff"}(RV 4.1.11)\
In dwellings first he came into being, at great base \[of heaven], and in this atmosphere's womb; Footless and headless, concealing both his ends, drawing himself together in the bull's station.\
The first hemistich refers to the 3 manifestations of agni: the ritual fire, the celestial fire and the atmospheric fire. Here, as Santillana and von Dechend argued in the Hamlet's Mill the [heavenly fire (वैश्वानर)](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/ "The path of fire") is associated with the [equinoctial colure](https://manasataramgini.wordpress.com/2006/11/26/the-crossing-of-ashmanvati/ "The crossing of ashmanvatI"): This is implied in this mantra by the statement that his ends come together -- i.e. the ecliptic ends meet and this happens in the station of the bull. Here again we see an allusion the vernal equinox in a constellation conceived as Taurus, i.e. the रोहिणी period.

In light of these connections the stellar bovine of the atharvaveda indeed seems to represent in part the celestial path with its start at boviform constellation of Taurus. This appears to be a memory of time when the vernal equinox lay at that position (Figure below). The association of the constellation with prajApati continues into the classical siddhAnta period (सूर्यसिद्धान्त 8.20) where the stars brahma-हृदय (heart of brahma or prajApati) and prajApati are described as being in Taurus.
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-rLKGivsHwnc/Uombbwr8dYI/AAAAAAAACxo/HSuVDJjzvo4/s640/Taurus.jpg){width="75%"}
```{=latex}
\end{center}
```

 *The vernal equinox at रोहिणी with the taurine constellation of वृषभ*

The AV stellar bovine joins the long list of the macranthropic or macrotherian depictions, which we have discussed before, a common feature throughout classical Hindu tradition. As we have posited before these are symbolic representations of the theory that the same forces and elements which run and constitute the microcosm denoted by the animal body also run the cosmos. Thus, they are the earliest depictions of the underlying unity that pervades the various levels of nature, the understanding which we continue to seek to this date -- this one capture the two great sources of knowledge regarding this unity -- the understanding of an animal's structure and working on one end and the understanding of the astronomical realm at the other.

Footnote 1: Like a white indologist, one might object that we are choosing the beginning ashvayujau for our reckoning rather than the end. Yes, but on other grounds, namely relative chronology based on continuity with earlier and later Hindu traditions, the possible taboo of naming the former yama नक्षत्र भरणी, and the relatively old position of the उत्तरनारायण in the vaiShNava material, as suggested by its pan-yajurvedic presence, we are justified in presenting the early ashvayujau as the likely temporal layer.


