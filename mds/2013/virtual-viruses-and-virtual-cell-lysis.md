
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Virtual viruses and virtual cell lysis](https://manasataramgini.wordpress.com/2013/07/12/virtual-viruses-and-virtual-cell-lysis/){rel="bookmark"} {#virtual-viruses-and-virtual-cell-lysis .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 12, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/07/12/virtual-viruses-and-virtual-cell-lysis/ "7:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-fpnC9DhhgH4/Ud-oqWJPRTI/AAAAAAAACso/APRQfhzgock/s640/viruses_cells.jpg){width="75%"}
```{=latex}
\end{center}
```



