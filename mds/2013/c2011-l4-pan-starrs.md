
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [C/2011 L4 (PAN-STARRS)](https://manasataramgini.wordpress.com/2013/03/12/c2011-l4-pan-starrs/){rel="bookmark"} {#c2011-l4-pan-starrs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 12, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/03/12/c2011-l4-pan-starrs/ "8:00 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-NizmbpQ_nF4/UUAbJowV0_I/AAAAAAAACm0/nidIdjIr8bM/s800/PANSTARRS_MAP.jpg){width="75%"}
```{=latex}
\end{center}
```



On the शुक्लपक्ष प्रथमी of the month of फाल्गुन in the year nandana, 5113 of the kaliyuga, a shvetaketu-putra was sighted by us .625 मुहूर्त-s after sunset. The shikhin was in the quarter of awful निरृति to the south of both the sun and moon and due to its low altitude needed a binoculars to sight it. Both the moon and the comet where in our नक्षत्र. Yet it was pretty bright and and its magnitude by comparison with the sliver of the प्रथमी moon could be placed as around .25 or so. Thus, we would place it among our memorable shikhin-s: Hale-Bopp, Hayakutake and Halley, which was the first ever we saw in our lives. The moon was a like the sword of rudra with comet above its southern horn with a prominent tail and bright nucleus.
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-W1rZwZoDO3c/UUAbTc-stpI/AAAAAAAACm8/GOuRrbWEZ3E/s400/PANStarrs.jpg){width="75%"}
```{=latex}
\end{center}
```




