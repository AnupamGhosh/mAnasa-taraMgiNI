
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [घटोत्कच](https://manasataramgini.wordpress.com/2013/05/05/ghatotkacha/){rel="bookmark"} {#घटतकच .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 5, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/05/05/ghatotkacha/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In महाभरत the fiercest fighting takes place on the 14th day and व्यास excels himself in the description of this awe-inspiring conflict on the kuru field. After the killing of jayadratha by arjuna during the evening solar eclipse the rage spilled over and rather than stopping at sundown the battle raged through the night. One of the great battles of the night was that between कर्ण and घटोत्कच after the latter had beheaded अलंबुष. Finally, कर्ण slew घटोत्कच with the shakti given to him by indra in return for his kavacha and कुण्डल-s.

संजय described घटोत्कच thus to धृतराष्ट्र:

[लोहिताक्षो महाकायस् ताम्रास्यो निम्नितोदरः ।]{style="color:#0000ff;"}\
[ऊर्ध्वरोमा हरिश्मश्रुः शङ्कु-कर्णो महाहनुः ॥]{style="color:#0000ff;"}\
He was with: red eyes, and a massive body a coppery hued face and and a sunken belly; with hair pointing upwards, a tawny mustache, pointed ears and a massive jaw.

[आकर्णाद् दारितास्यश् च तीक्ष्णदंष्ट्रः करालवान् ।]{style="color:#0000ff;"}\
[सु-दीर्घ-ताम्र-जिह्वोष्ठो लम्बभ्रूः स्थूल-नासिकः ॥]{style="color:#0000ff;"}\
With a mouth extending from ear to ear and terrible, sharp teeth; with coppery hued long tongue and lips, long eye brows and a large nose.

[नीलाङ्गो लोहित-ग्रीवो गिरि-वर्ष्मा भयंकरः ।]{style="color:#0000ff;"}\
[महाकायो महाबाहुर् महाशीर्षो महाबलः ॥]{style="color:#0000ff;"}\
He was blue in color with with a red neck tall as a mountain peak and terrifying; of gigantic body, great arms, a big head and of great strength.

[विकचः परुष-स्पर्शो विकटोद्बद्ध-पिण्डिकः ।]{style="color:#0000ff;"}\
[स्थूलस्फिग् गूढनाभिश् च शिथिलोपचयो महान् ॥]{style="color:#0000ff;"}\
With hairless, rugged skin, and hair on head tied into a terrifying knot. With great hips, a deep navel and a great mass of flexible muscle.

[तथैव हस्ताभरणी महामायो ऽङ्गदी तथा ।]{style="color:#0000ff;"}\
[उरसा धारयन् निष्कम् अग्निमालां यथाचलः ॥]{style="color:#0000ff;"}\
With arm-guards, possessed of great magical powers, he also bore shoulder guards. On this chest was an ornament like a blazing volcanic ring.

[तस्य हेम-मयं चित्रं बहु-रूपाङ्ग-शोभितम् ।]{style="color:#0000ff;"}\
[तोरण-प्रतिमं शुभ्रं किरीटं मूर्ध्न्य् अशोभत ॥]{style="color:#0000ff;"}\
His head was decorated by a golden, beautiful diadem endowed with many beautiful links, shining like an arch.

[कुण्डले बाल-सूर्याभे मालां हेम-मयीं शुभाम् ।]{style="color:#0000ff;"}\
[धारयन् विपुलं कांस्यं कवछं छ महाप्रभम् ॥]{style="color:#0000ff;"}\
With ear-rings like the rising sun, and beautiful chains made of gold. He wore on his body gigantic armor of bell-metal that shone greatly.

[किङ्किणी-शत-निर्घोषं रक्त-ध्वज-पताकिनम् ।]{style="color:#0000ff;"}\
[ऋक्ष-चर्मावनद्धाङ्गं नल्व-मात्रं महारथम् ॥]{style="color:#0000ff;"}\
With a hundred tinkling bells and blood-red flags; with seats covered with bear-skins was his giant battle car the size of a nalva.

[सर्वायुधवरोपेतम् आस्थितो ध्वजमालिनम् ।]{style="color:#0000ff;"}\
[अष्ट-चक्र-समायुक्तं मेघ-गम्भीर-निस्वनम् ॥]{style="color:#0000ff;"}\
Equipped with all kinds of weapons, it had a tall standard with garlands; equipped with eight wheels it made the deep noise of thunder-clouds.

[तत्र मातङ्ग-संकाशा लोहिताक्षा विभीषणाः ।]{style="color:#0000ff;"}\
[काम-वर्ण-जवा युक्ता बलवन्तो ।अवहन् हयाः ॥]{style="color:#0000ff;"}\
It was borne by horses that were like elephants, red-eyed and terrifying, assuming any color they wished, endowed with great speed and might.

[वहन्तो राक्षसं रौद्रं बलवन्तो जित-श्रमाः ।]{style="color:#0000ff;"}\
[विपुलाभिः सटाभिस् ते हेषमाणा मुहुर् मुहुः ॥]{style="color:#0000ff;"}\
They bore that terrible rAkShasa imbued with great might and without fatigue with long manes and neighing repeatedly.

[राक्षसो ऽस्य विरूपाक्षः सूतो दीप्तास्य-कुण्डलः ।]{style="color:#0000ff;"}\
[रश्मिभिः सूर्य-रश्म्याभैः संजग्राह हयान् रणे ॥]{style="color:#0000ff;"}\
A rAkShasa of terrible eyes, was his charioteer, with a blazing mouth and ear-rings; with reins shining as rays of the sun he lead the horses into battle.

[स तेन सहितस् तस्थाव् अरुणेन यथा रविः ।]{style="color:#0000ff;"}\
[संसक्त इव छाभ्रेण यथाद्रिर् महता महान् ॥]{style="color:#0000ff;"}\
With that \[charioteer] he came to battle like the sun borne by अरुण. [घटोत्कच] appeared like mighty mountain peak shrouded by clouds.

[दिव-स्पृक् सुमहान् केतुः स्यन्दने ऽस्य समुच्छ्रितः ।]{style="color:#0000ff;"}\
[रक्तोत्तमाङ्गः क्रव्यादो गृध्रः परम-भीषणः ॥]{style="color:#0000ff;"}\
The tall standard erected in his \[car] was like a comet across the sky; A most terrifying flesh-eating vulture with blood-red body \[was pictured on it].


