
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [विधिना avarodhita](https://manasataramgini.wordpress.com/2013/09/07/vidhina-avarodhita/){rel="bookmark"} {#वधन-avarodhita .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 7, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/09/07/vidhina-avarodhita/ "7:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[छित्वा पाशम् अपास्य कूट-रचनां भङ्क्त्वा बलाद् वागुरां]{style="color:#99cc00;"}\
[पर्यस्ताग्नि-शिखा-कलाप-जटिलान् निःसृत्य दूरं वनात् ।]{style="color:#99cc00;"}\
[व्याधानां शरगोचराद् अतिजवेनोत्प्लुत्य गच्छन् मृगः]{style="color:#99cc00;"}\
[कूपान्तः पतितः करोति विगुणे किं वा विधौ पौरुषम् ॥]{style="color:#99cc00;"}

Having broken the snare, avoided the concealed traps, and forcibly broken nets,\
escaping bundles of flaming fire brands \[running] far from the forest;\
Though in within the range of the huntsmen's arrows bounding away with great speed, goes the deer,\
And it has fallen in a well, so indeed what can a man do when fate is adverse!

  - A verse by the great emperor ललितादित्य of काश्मीर

It is interesting that ललितादित्य composed this verse. It was even a matter of seriousness that it came from him -- after all he was one of the great heroes of our land who had brought to a stop the march of the मरून्मत्त-s exploding from the west and the chIna-s pouring from the east. And in his own home turf he brought to an end the neo-maurya yashovarman, the destroyer of the rising Bengali pride, praised by the great kavi वाक्पति rAjA. Then he went beyond to subjugate the proud कर्णाट-s and कलिङ्ग-s of the east limits of भारतवर्ष. But in the end he knew well how all this क्षत्र-glory could come to naught and even his land cease to exist. It emphasized to us, as our old Iranian cousins and rivals would say, that the lordly Xvarena is like falcon that sits on ones shoulder today and on another's tomorrow.

As we lay like the valiant दत्ताजी shinde, we thought: "If we survive we will fight again!". Then we wondered if the grip of मृत्यु may over take us before the highest conquests are achieved. Glory does not lie far away to those who are given luck by the deva-s but it is only few who grasp it thereafter, the rest sinking much like ललितादित्य's deer. This indeed we call fate. After all, भर्तृहरि had said in the days of yore:\
[खल्वाटो दिवसेश्वरस्य किरणैः संतापितो मूर्धनि]{style="color:#99cc00;"}\
[छायाम् आतपवैरिणीम् अनुसरन् बिल्वस्य मूलं गतः ।]{style="color:#99cc00;"}\
[तत्राप्य् आशु कदाचिद् एव पतता बिल्वेन भग्नं शिरः]{style="color:#99cc00;"}\
[प्रायो गच्छति यत्र भाग्य-रहितस् तत्रापदां भाजनम् ॥]{style="color:#99cc00;"}

The baldy scalded on his head by the rays of the lord of the day,\
seeking the sun-shielding shade went to the foot of a bilva tree;\
there, then by chance indeed swiftly fell a bilva fruit which cracked his skull;\
Perhaps wherever the luckless goes, there he attains misfortune!

Why indeed are we in these straits we wondered? In the great back to back battles of रावण's heads and the Aditya-s we rode past the smoke and dust leaving behind many who were perishing or being defeated. Having surpassed our striving rivals as any man must, we sat beside the collyrium-hued tribesman with his ouija board. He plied the ouija and uttered some right forecasts, like the fall of the [नक्षत्रदर्षा](https://manasataramgini.wordpress.com/2007/08/16/the-fall-of-the-nakshatra-darsha/). We then asked a question at the goading of our clansman. The answer came: "When you conjoin with her who is the cloud you will complete the [first circle](https://manasataramgini.wordpress.com/2006/05/07/the-first-circle/)". While cryptic to most, we understood what it meant, and so did our clansman. Hence, he remarked go forth to the तीक्ष्णत्रिपथिका, who is among rarest of the rare. We turned to him and said: "She will be a चतुष्पथिका not a तीक्ष्णत्रिपथिका. May be the तीक्ष्णत्रिपथिका is for you" Then the tribal looked up and said: "He will enjoy the pleasure of several but not the तीक्ष्णत्रिपथिका nor वरावन्तिका." That night we went to the कुलाङ्गना and mentioned the forecast. She laughed and said: "Hey, ouija boards cannot tell you the future." We pressed her: "You seem to know more than that statement belies." She spoke like völva to the old German Ariovistus : "I wish you had understood when the niShAda's board called out nabhas. These battles of the enemies of वसिष्ठ or the months will be nothing when the time of that great battle comes. You will be tested like never before and the cloud shall descend upon the battlefield. You would need to continue fighting even after your ratha has broken up and you have been wounded by the great astra-s. In the cremation ground संमेलन when you conjoin with me in the great churning of the bhairava the dreadful योगिनी-s of the जाल-शंबर will come to give you the sword that will take you to victory in battle and we'll be omniscient in yoga. In the Ragnarok-like future, you will hasten to the cremation ground for the संमेलन with me, bewitching as ever, but you will be struck by the पाशस्तंभिनी prayoga of बगलामुखी."

Then:\
While we prepared to fight, we were attacked by the mighty force. It felt like the unstoppable कुंभकर्ण demolishing हनूमत्:\
[हनूमाञ् शैल-शृङ्गाणि शिलाश् च विविधान् द्रुमान् ।]{style="color:#99cc00;"}\
[ववर्ष कुम्भकर्णस्य शिरस्य् अम्बरम् आस्थितः ॥]{style="color:#99cc00;"}\
hanumAn flying in the sky showered mountain tops, rocks, and a variety of trees on कुम्भकर्ण's head.

[तानि पर्वत-शृङ्गाणि शूलेन तु बिभेद ह ।]{style="color:#99cc00;"}\
[बभञ्ज वृक्ष-वर्षं च कुम्भकर्णो महाबलः ॥]{style="color:#99cc00;"}\
The mighty कुंभकर्ण smashed to pieces those mountain peaks and the shower of trees with his trident.

[ततो हरीणां तद् अनीकम् उग्रं]{style="color:#99cc00;"}\
[दुद्राव शूलं निशितं प्रगृह्य ।]{style="color:#99cc00;"}\
[तस्थौ ततो ।अस्यअपततः पुरस्तान्]{style="color:#99cc00;"}\
[महीधराग्रं हनुमान् प्रगृह्य ॥]{style="color:#99cc00;"}\
Seizing his sharp trident he rushed against the fierce army of the monkeys. There stood hanumAn in front of him holding a mountain peak even as he advanced.

[स कुम्भकर्णं कुपितो जघान]{style="color:#99cc00;"}\
[वेगेन शैलोत्तम भीमकायम् ।]{style="color:#99cc00;"}\
[स चुक्षुभे तेन तदाभिभूतो]{style="color:#99cc00;"}\
[मेदार्द्र गात्रो रुधिरावसिक्तः ॥]{style="color:#99cc00;"}\
Enraged, with great velocity he struck कुंभकर्ण, who was endowed with a terrible body like a great mountain. He [कुम्भकर्ण] was agitated when humiliated by that blow and his limbs were wet with fat and blood.

[स शूलम् आविध्य तडित्-प्रकाशं]{style="color:#99cc00;"}\
[गिरिं यथा प्रज्वलिताग्र-शृङ्गम् ।]{style="color:#99cc00;"}\
[बाह्वन्तरे मारुतिम् आजघान]{style="color:#99cc00;"}\
[गुहो ऽचलं क्रौञ्चम् इवोग्र-शक्त्या ॥]{style="color:#99cc00;"}\
Striking with that trident emitting electrical discharge, resembling an erupting volcanic peak, he smote मारुति between his arms, even as कुमार had struck the unshakeable क्रौङ्च with his fiery shakti.

[स शूल निर्भिन्न महाभुजान्तरः]{style="color:#99cc00;"}\
[प्रविह्वलः शोणितम् उद्वमन् मुखात् ।]{style="color:#99cc00;"}\
[ननाद भीमं हनुमान् महाहवे]{style="color:#99cc00;"}\
[युगान्त मेघ-स्तनित-स्वनोपमम् ॥]{style="color:#99cc00;"}\
As the trident cleaved him between his great arms in that great battle, hanuman vomited blood from his mouth, and uttering a great cry like the thunder from a cloud at the end of a yuga, and he lost consciousness.

[ततो विनेदुः सहसा प्रहृष्टा]{style="color:#99cc00;"}\
[रक्षोगणास् तं व्यथितं समीक्ष्य ।]{style="color:#99cc00;"}\
[प्लवंगमास् तु व्यथिता भयार्ताः]{style="color:#99cc00;"}\
[प्रदुद्रुवुः संयति कुम्भकर्णात् ॥]{style="color:#99cc00;"}\
Seeing him thus felled the रक्ष-s ranks rejoiced and erupted in cheers. The wounded monkeys thus afflicted with fear of कुंभकर्ण fled from the battle.

As we lay thus with a part of our consciousness curtained by the pain in our mind flashed the words: "गायत्रं gAyatrI वरुणाय". We thought of our ancient clansman शुनःशेप. Could we continue in this great battle? But then the man who stops fighting dies.


