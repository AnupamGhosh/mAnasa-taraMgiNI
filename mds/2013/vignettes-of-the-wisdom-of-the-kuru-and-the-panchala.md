
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Vignettes of the wisdom of the kuru and the पञ्चाल](https://manasataramgini.wordpress.com/2013/05/14/vignettes-of-the-wisdom-of-the-kuru-and-the-panchala/){rel="bookmark"} {#vignettes-of-the-wisdom-of-the-kuru-and-the-पञचल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 14, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/05/14/vignettes-of-the-wisdom-of-the-kuru-and-the-panchala/ "6:36 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The defining element of our identity was the formation of the राष्ट्र by the kuru and the पञ्चाल, the foremost of the bhArata clans. It is the legacy of this राष्ट्र of the bhArata-s, rather than documents like the constitution of India, that forms the foundation on which our modern state rests. A म्लेच्छ gentleman, evidently intending to pay a compliment, told us that he was impressed by fact that our "founding fathers" had managed to come up with a constitution that has managed to hold our country together, which by all logic should have broken up by now. We told that he was right about the "founding fathers" part but only that they go much farther back in time. Indeed, the farther our people recede from the wisdom of the kuru and the पञ्चाल enshrined in the "fifth veda" the more effete and irrelevant we are becoming in the world. Hence, those who still bother about such things might peruse of some of it:

Thus spoke nakula the fourth पाण्डव:

[राज्ञः प्रमाद-दोषेण दस्युभिः परिमुष्यताम् ।]{style="color:#99cc00;"}\
[अशरण्यः प्रजानां यः स राजा कलिर् उच्यते ॥]{style="color:#99cc00;"}\
The ruler whose delusion results in his people being plundered by dasyu-s, and \[the ruler] who does not offer protection to his people \[from dasyu-s] is said to be the manifestation of kali.

Thus spoke द्रौपदी the princess of the पञ्चाल-s:

[नादण्डः क्षत्रियो भाति नादण्डो भूतिम् अश्नुते ।]{style="color:#99cc00;"}\
[नादण्डस्य प्रजा राज्ञः सुखम् एधन्ति भारत ॥]{style="color:#99cc00;"}\
Without punitive legislation a kShatriya cannot shine forth, without punitive ability he cannot attain power. The people of a ruler without punitive ability cannot attain happiness.

[असतां प्रतिषेधश् च सतां च परिपालनम् ।]{style="color:#99cc00;"}\
[एष राज्ञां परो धर्मः समरे चापलायनम् ॥]{style="color:#99cc00;"}\
Restraining the evil doers, cultivating the good, and showing agility in war: these are the highest duties of rulers.

[कुरुते मूढम् एवं हि यः श्रेयो नाधिगच्छति ।]{style="color:#99cc00;"}\
[ धूपैर् अञ्जनयोगैश् cha nasya-karmabhir eva cha |]{style="color:#99cc00;"}\
[भेषजैः स चिकित्स्यः स्याद् य उन्मार्गेण गच्छति ॥]{style="color:#99cc00;"}\
One who due to stupidity \[wishes to abandon the upholding of his state for asceticism as युधिष्ठिर wished to do upon realizing that he caused the death of कर्ण and others] will never attain any felicity. \[Such a person should] be diagnosed as one who has taken the path of madness and should be given medical treatment using inhalants, medicinal pastes, drips delivered via the the nose, and other drugs.

\*It is interesting to note that the methods for drug delivery seen in the medical saMhitA-s for the treatment of उन्माद are recognized here by द्रौपदी -- their earliest forms likely arose in the kuru-पञ्चाल state.

Thus spoke भीमसेन the second पाण्डव:

[द्विविधो जायते व्याधिः शारीरो मानसस् तथा ।]{style="color:#99cc00;"}\
[परस्परं तयोर् जन्म निर्द्वंद्वं नोपलभ्यते ॥]{style="color:#99cc00;"}\
There are two types of disease -- of the body and of the mind. Each give rise to the other neither exists independently of the other.

[शारीराज् जायते व्याधिर् मानसो नात्र संशयः ।]{style="color:#99cc00;"}\
[मानसाज् जायते व्याधिः शारीर इति निश्चयः ॥]{style="color:#99cc00;"}\
The mental diseases arise from bodily morbidity without doubt; likewise from mental disease emerge bodily diseases -- this is certain.

[शारीर मानसे दुःखे यो ऽतीते अनुशोचति ।]{style="color:#99cc00;"}\
[दुःखेन लभते दुःखं द्वाव् अनर्थौ प्रपद्यते ॥]{style="color:#99cc00;"}\
He who keeps pondering on past bodily or mental afflictions has new sorrow emerging from his thinking of old sorrows; thus he \[ends up] saddled with two tragedies

\*Here भीमसेन recognizes that mental disease ultimately originates from a bodily problem. He also states that depression can give rise to a physical condition. Thus, he calls upon युधिष्ठिर to cast aside his depression and move on in life.

Thus spoke arjuna the third पाण्डव:

[क्षत्र-धर्मो महा-रौद्रः शस्त्र-नित्य इति स्मृतः ।]{style="color:#99cc00;"}\
[वधश् च भरत-श्रेष्ठ काले शस्त्रेण संयुगे ॥]{style="color:#99cc00;"}\
It is known that the duty of the kShatriya is very fierce involving the constant use of weapons. O foremost bhArata, \[it is known] that when their time comes they are killed in battle while engaging with weapons.

[ब्राह्मणस्यापि चेद् राजन् क्षत्र-धर्मेण तिष्ठतः ।]{style="color:#99cc00;"}\
[प्रशस्तं जीवितं लोके क्षत्रं हि ब्रह्म-संस्थितम् ॥]{style="color:#99cc00;"}\
Even a ब्राह्मण who is firm in following the duties of a kShatriya leads a praiseworthy life as after all in this world क्षत्र power is also stationed on a foundation of brahma power.

[क्षत्रियस्य विशेषेण हृदयं वज्र-संहतम्।]{style="color:#99cc00;"}\
[जित्वारीन् क्षत्र-धर्मेण प्राप्य राज्यम् अकण्टकम् ॥]{style="color:#99cc00;"}\
In particular a kShatriya's heart is adamantine. By applying the क्षत्र-dharma he conquers foes and acquires a dominion free from trouble-makers.

Thus spoke व्यास कृष्ण-द्वैपायन:

[यज्ञो विद्या समुत्थानम् असंतोषः श्रियं प्रति ।]{style="color:#99cc00;"}\
[दण्ड-धारणम् अत्युग्रं प्रजानां परिपालनम् ॥]{style="color:#99cc00;"}\
[वेद-ज्ञानं तथा कृत्स्नं तपः सुचरितं तथा ।]{style="color:#99cc00;"}\
[द्रविणोपार्जनं भूरि पात्रेषु प्रतिपादनम् ।]{style="color:#99cc00;"}\
[एतानि राज्ञां कर्माणि सुकृतानि विशां पते॥]{style="color:#99cc00;"}\
Fire rituals, learning, exercise, being unsatisfied with \[limited] prosperity, upholding punitive legislation, fierceness in battle and working for the welfare of ones people. Knowledge of the veda and rituals, performance of tapas, good conduct, acquisition of plentiful wealth and its distribution to deserving individuals -- these are the duties of a kShatriya to be performed well O ruler of the people.

[तेषां ज्यायस् तु कौन्तेय दण्डधारणम् उच्यते]{style="color:#99cc00;"}\
[बलं हि क्षत्रिये नित्यं बले दण्डः समाहितः ॥]{style="color:#99cc00;"}\
Amongst these, kaunteya, the wielding of punitive power is said to be the foremost. Strength must always be there in a kShatriya as punitive power depends on strength.

the gAthA of बृहस्पति अञ्गिरस:\
[भूमिर् एतौ निगिरति सर्पो बिलशयान् इव ।]{style="color:#99cc00;"}\
[राजानं छाविरोद्धारं ब्राह्मणं छाप्रवासिनम् ॥]{style="color:#99cc00;"}

Just as a snake gobbles a burrowing rodent \[the ways of] the world swallow a king who given to excessive non-violence and a ब्राह्मण who is excessively devoted to one place \[i.e. patron].


