
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Ephemeral Hexads](https://manasataramgini.wordpress.com/2013/12/01/ephemeral-hexads/){rel="bookmark"} {#ephemeral-hexads .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 1, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/12/01/ephemeral-hexads/ "5:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-ZT0UC65pHFw/UprMD76JJwI/AAAAAAAACx8/X5TWrUyjGAo/s640/weaver2_small.jpg){width="75%"}
```{=latex}
\end{center}
```



