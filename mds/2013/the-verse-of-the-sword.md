
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The verse of the sword](https://manasataramgini.wordpress.com/2013/12/29/the-verse-of-the-sword/){rel="bookmark"} {#the-verse-of-the-sword .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 29, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/12/29/the-verse-of-the-sword/ "8:19 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[chitrabandha-s](https://manasataramgini.wordpress.com/2013/01/06/between-the-rasarnava-and-the-chitrabandha-s/ "Between the rasArNava and the chitrabandha-s") are widely represented in classical works on काव्य. The bandha-s of the form of the sword find mention in agni-पुराण, the काव्यालंकार of रुद्रट and the praise of chaNDI by the other Kashmirian Anandavardhana. Sword-shaped yantra-s comparable to the poetic chitrabandha-s are to our knowledge seen first in the कुब्जिकामत texts in the पश्चिमांनाय of kaula tradition. This sword with the mantra's arNa's on it was said to be like the विद्याधर's sword. Such inscriptions on swords were also transmitted to other cultures which acquired tantrika traditions -- for example, the inscription of the मरीची mantra on the swords in Japan \[the solar goddess मरीची was homologized on occasions to the native deity Amaterasu]. Below is the verse of the sword, a shAkta praise of उमा as the supreme deity.
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-f8IgjaGktmY/UsJWg2NzJ-I/AAAAAAAAC1I/ijGcMbdQhvY/s640/sword_durgA.jpg){width="75%"}
```{=latex}
\end{center}
```



[मारारि-शक्र-रामेभमुखैर् आसार-रंहसा]{style="color:#0000ff;"}\
[सारारब्धस्तवा नित्यं तद् आर्ति-हरण-क्षमा ॥]{style="color:#0000ff;"}\
[माता नतानां संघट्टः श्रियां बाधित-संभ्रमा]{style="color:#0000ff;"}\
[मान्याथ सीमा रामाणां शं मे दिश्याद् उमादिमा ॥]{style="color:#0000ff;"}

मारारि= the enemy of death (shiva); shakra= indra; rAma=the dark one(विष्णु) ibhamukha=the elephant-headed god; आसार-रंहसा=by them who are rapidly assailed; सारारब्धस्तवा-\>sAra-Arabhdha-स्तवा= One whom them eloquently commence to praise; नित्यं= always; tad= then; Arti-हरण-क्षमा= one who is the pain-removing forgiving one.\
माता नतानां=Mother to the supplicants; संघट्टः श्रियां=collection of riches; बाधित-संभ्रमा: one who removed confusion; मान्याथ sImA रामाणां= moreover venerable as the chastity of auspicious women; shaM me दिश्याद्= may confer felicity to me; उमादिमा->उमा-आदिमा= उमा the primal goddess\
\[I would like to thank श्री विश्वासो वासुकिपुत्रः for pointing a proper reading for an element of this verse]

Approximately:\
The forgiving one, who when the enemy of death (shiva), the powerful one (indra), the dark one (विष्णु) and the elephant-headed one (विनायक) are rapidly assailed, commence to eloquently praise always removes pain; mother to the supplicants; richly endowed removeress of perplexity; moreover one venerable as the chastity of auspicious women; may उमा the primal goddess confer felicity on me.

[A biochemical aside: We had earlier compared some chitrabandha-s as the linguistic analogs of the histone-tails and such other low complexity sequences such as silaffins which constitute a protein component of diatom shells, where many different meanings can be written via epigenetic modifications.](https://manasataramgini.wordpress.com/2013/01/06/between-the-rasarnava-and-the-chitrabandha-s/ "Between the rasArNava and the chitrabandha-s") Such examples, like the sarvatobhadra, are by definition low in complexity but different meanings are obtained by means of differential संधि splits, which act like "epigenetic" information superimposed on the underlying low complexity. Extending such analogies, one might compare chitrabandha-s, such as the one provided in this note, with well-structured RNAs such as त्ऱ्‌णास्, र्ऱ्‌णास् or riboswitches, which enforce regions to maintain complementarity (stem) and those that do not (loops). These loops in such RNAs contain the key binding sites: In त्ऱ्‌णास् the anticodon loop supported by a rigid stem is central to reading the genetic code by complementarity. In riboswitches one or more loops or bulges might come together to form a binding site for a ligand (see figure below). This may be compared to this class of chitrabandha-s wherein the stem like constraints like the sword haft support the structure of the sword (the loop).
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-EoFEykgS5S4/UsCCmMsk36I/AAAAAAAAC0Q/mRNY0pzITEc/s640/riboswitches.jpg){width="75%"}
```{=latex}
\end{center}
```




