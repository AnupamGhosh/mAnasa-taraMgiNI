
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [सुस्मेरतया saha संमेलन](https://manasataramgini.wordpress.com/2013/06/30/susmerataya-saha-sammelana/){rel="bookmark"} {#ससमरतय-saha-समलन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 30, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/06/30/susmerataya-saha-sammelana/ "7:00 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Holding a trident he arrived at the great स्मशाण at the foot of kollagiri from the subterranean temple of नृसिंह where he had practiced the mantra of the adhomukha. It was in the midst of a storm: the trees in the cemetery shook as though they had been struck by the fist of the great ape वालिन्. The shadows of their moving branches made the spectacle look like the dance of the great कराला holding severed heads and diverse weapons in her hands. The clatter of the rain and the occasional hail was like the drumming of the गण-s during the ताण्डव of sharva. As each strike of lighting was followed by the great roar of parjanya he uttered the वषट् call. He proceeded to the shrine housing the क्षेत्रपाल upon which the great कराला, also known as चण्डाख्या, looked upon. Seated at his chosen corner he silently witness a corpse being consumed by the fierce क्रव्याद, with hissing and crackling noises even as the long bones and skull exploded in the fury of yamavaha. In his mind he was intoning the ancient बहुरूपी mantra of the southern face even as he turned the beads of his रुद्राक्ष rosary. Some time after the anti-meridian hour the sky cleared completely revealing the silver soma cup of the heaven. There was a rustling noise and shadows indicated a new presence. The silence of the night was interrupted by some voices. He heard the familiar voice of an Apastambin ritualist who was negotiating with the cemetery guard for a fresh human head to place beneath the chiti for his impending agnichayana. As the guard was thus distracted in his duties, someone else slipped into to the स्मशाण. He could tell that this was someone very different from the dancing shadow and the enlivening scents of a female unguents though she hid herself behind a कर्णमोट tree. Raising his arm he showed the Chomma of the bull-horns. She responded with the Chomma of the lion-claw, which he picked up via the shadow cast in the shrine's courtyard. He then displayed the Chomma of the ghost. She responded with that of the of the great reptile. He then showed the Chomma of eagle and she that of the many-formed rudra. Finally they both simultaneous displayed that of the "conjunct-churner". She then came out into the open: He was shocked as though he had come face to face to चामुण्डा herself, even as the breeze blew her flowing locks in all direction revealing a skull in place of the face. She laughed and raised her mask revealing her beautiful face beneath.

*He called out:* "O सुस्मेरता come forth we have been waiting for you hoping to attain siddhi. Why did you come thus masked ?"\
*सुस्मेरता:* "Wandering from the द्रविडदेश we arrived here and felt that the the cemetery guard might block out way. Hence, we planned to get in by either behaving like an unmatta-स्त्री or frightening him with that mask of ours. But to our luck we ran into an old acquaintance, the Apastambin named mAra सोमयाजिन्, who in the path of the secret shaiva शास्त्र was a student of our own teacher विजयाङ्का from the कर्णाट country who conquers like सरस्वती."\
*He said:* "Indeed, you need to conceal yourself. What would people think and say if they saw a respectable ब्राह्मण girl, the daughter of a great वैयाकरण uddyotana, wandering thus in the midst of taverns and cemeteries, when she should be tending the fire at her hearth. O conduit of siddhi, we seek the 11-fold kaula path and its foundation of bhairava-s."\
*सुस्मेरता:* "O vIra first reveal your former course in full."\
*He said:* "We come from anahilapattana and are among the few who attained complete siddhi of the practice of the कार्त्तिकेय mantra-s that hardly anyone achieves. We then proceeded to study the oceanic शास्त्र of the योगिनी-s in the realms of कश्मीर. There we dreamt that we should proceed from towards the south and that you would come from the draviDa country to take us on the path of attaining the state of a विद्याधर. O सुसुमेरता, mistress of the संमेलन, we seek the yoga of क्लिन्नान्वय."\
*सुस्मेरता:* "We shall first embark on the 8-fold path which form the base of the great throne of क्लिन्ना."

There after she began revealing him the 8-fold mantra paths of: निष्कल-स्वच्छन्द, sakala-स्वच्छन्द, बहुरूप-bhairava, अघोरीश, the व्याधिभक्ष-bhairava, chandragarbha-bhairava, vij˜नानभैरव, tumburu, and अमृतेशवर. For an year he practiced the mantra-s devoted to constant पुरश्cअरण and the offering of agni-क्रिया-s on the specified days. There after सुस्मेरता summoned him one darsha night and said the test for your mantra attainment is will be via a mantra contest. He on the path of the विद्याधर should not be afraid of one. She took out her रावणहस्त and played the following song:

  - "Bent on delighting in siddha-bhoga\
the विद्याधर-s pour surabhi's घृत\
into the fire of the great क्लिन्ना's त्रिकोण.*

In the space known as the kha-vyoman,\
though the space is within vyoma-व्यापिन्,\
the विद्याधर worships the wild vyoma-loman.

I am the one in the midst of the योगिनी-जाल,\
exhibiting the high magic of the shambara,\
the sole sun blazing in the मयूख-chakra.

I dance along with wild band of हृल्लेखा and क्लेदिनी,\
with nandA, मदनातुरा and the restive क्षोभनी,\
then with the glorious निरञ्जना and रागवती

With खेकला and the bewitching मदनावती,\
ecstatically with द्राविनी, speedily with वेगवती,\
I am the great rudra of the churning!"

He felt an uncontrollable force tearing him apart. He felt lifted of the ground and hurled as though he was shambara being hurled by maghavan from atop his mountain fort. Coursing in the sky he flew like a विद्याधर passing over the many hued lands of the triangle of southern जम्बुद्वीप, occasionally bypassing a vulture and other times a flock of waterfowl. Finally, he landed in an unfamiliar port town. He saw some sights he had seen in the port near his own home town of anahilapattana but this was an entirely different place. It seemed to be in the chera country. Some वण्जिअ-s were offloading merchandize from Africa whereas others were loading commodities to set out to Egypt or Iran. He wondered how he would make his way here and why he had landed there in the first place.

  - An excerpt from "mantra-नायक"


