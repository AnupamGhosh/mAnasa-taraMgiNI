
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Translating an ancient mantra: ऋग्वेद 2.34.09](https://manasataramgini.wordpress.com/2013/10/11/translating-an-ancient-mantra-rigveda-2-34-09/){rel="bookmark"} {#translating-an-ancient-mantra-ऋगवद-2.34.09 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 11, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/10/11/translating-an-ancient-mantra-rigveda-2-34-09/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Sometimes translating even a single vaidika mantra can take some effort. In large part this is due to three factors: 1) The vocabulary- there are several words that have become otiose in classical Sanskrit and modern Indian languages; 2) verb conjugations which are not found in the classical language, and often retain a form closer to the proto-IndoEuropean state; 3) Metrical adjustments which need to be distinguished from archaisms. In light of all this, one cannot but admire the colossal performance of the great prime minister of the early Vijayanagaran empire in his commentary on the RV. Notably सायण was operating without the advantage of comparative linguistics of IE languages. No doubt his project marked a highpoint in the Hindu revival after the fall of bhArata to the तुरुष्क-s -- it played the same role in the intellectual sphere as the blades of the swords of the संगम clan did in the military sphere.

Let us consider one example of vaidika mantra that is superficially simple but takes some effort to translate correctly. The mantra is from a सूक्त by the ऋषि गृत्समद shunahotra shaunaka भार्गव, the founder of the kevala भार्गव clan of the shaunaka-s. The सूक्त is primarily to the marut-s who are in some of its mantra-s are invoked along with other deva-s like rudra, vasu-s and विष्णु. It is in the jagati meter which is 4*12 syllables. The first pada is 11 syllabled and is restored by taking मर्त्यः as मर्तियः

[यो नो मरुतो वृकताति मर्त्यो रिपुर् दधे वसवो रक्षता रिषः ।]{style="color:#99cc00;"}\
[वर्तयत तपुषा चक्रियाभि तम् अव रुद्रा अशसो हन्तना वधः ॥]{style="color:#99cc00;"} RV 2.34.09

yaH= who (pronoun); नः= us (pronoun); मरुतः= marut-s (vocative plural); वृकताति= in the midst of wolves (locative singular); मर्त्यः=mortal (nominative singular); रिपुः= enemy (nominative singular); dadhe= placed (3rd person perfect atmane ); वसवः= vasu-s (vocative plural); रक्षत+= protect (imperative 2nd person plural; elongated metrically); रिषः= harm (ablative singular of रिष्) |

vartayata= spin (imperative 2nd person plural); तपुषा= fiery (adjective); चक्रिया= with discus (instrumental singular of चक्री); abhi= against (goes with verb as abhi-vart); tam= him (accusative, 3rd person pronoun); ava= away; रुद्रा= rudra-s, i.e. marut-s (vocative plural); अशसः= hater (ablative singular of ashas); hantana+= strike (imperative 2nd person plural; elongated metrically: ava-hantana); वधः= weapon (accusative of neuter noun vadhas; compare with neuter noun vadhar) ||

The key points of note here are:

  -  वृकतात्: an unusual Vedic word meaning among wolves.

  -  Both the 2nd person plural imperatives रक्षत and hantana are metrically elongated to fit the guru syllable scan of the jagati.

  -  hantana is an unusual form of the 2nd person imperative plural of han, the like of which is only seen in the vaidika language. It is an inheritance from proto-IndoEuropean because in Hittite we encounter such -ten extensions which correspond to the -tan base in hantana: compare with the form kuenten- slay in Hittite. Other comparable vaidika imperative forms are: ब्रवीतन, etana.

  -  चक्री is used instead of the more common chakram and its instrumental is given in a fully resolved form, i.e. चक्रिया instead of chakrya which would be typical. The discus is used as a weapon by indra, पुषन्, marut-s in the veda and वायु in the avesta ([वाइयुम् ज़रण्य्ô-chakhrem ýazamaide |]{style="color:#00ffff;"}). In RV 8.96.9 a battle between the demons and the deva-s is mentioned where the marut-s wield sharp spears in formation while indra at their head fights with both his vajra and chakra. In the latter texts it is used primarily by विष्णु.

  -  ashas- A rare word in the ऋग्वेद coming only once again in a mantra of वामदेव gautama to agni the slayer of रक्षस् (daha ashaso रक्षसः pahy अस्मान्; RV 4.4.15c). सायण based on this suggests that the ashas here could be a रक्षस्.\
\*vadhas- The RV has a parallel neuter noun vadhar which means a weapon. For instance, in the famed first person सूक्त where indra speaks, he says: "[अहं शुष्णस्य श्नथिता वधर् यमं न यो रर आर्यं नाम दस्यवे ।]{style="color:#99cc00;"}" i.e. I am शुष्ण's slayer, \[by me was wielded] the yama-weapon \[i.e. death weapon]; I did not give up the name which of the Arya-s to the dasyu-s. But this represents the one rare instance where instead of the more frequent vadhar the parallel form vadhas is used.

Thus we may come up with the translation:\
O marut-s, O vasu-s, protect us from harm, the mortal enemy who has us placed among the wolves;\
Spin against him with the fiery discus; O rudra-s strike away the weapon from the hater (a रक्षस्).

What we have here is an early example of the second person imperative mantra that is common in the later Hindu traditions, especially in the तान्त्रिक मन्त्रशास्त्र. Some of the same imperative verbs are commonly used such as hana, रक्ष, daha, pAhi (latter two in RV 4.4.15). This represents of one of the elements of continuity between तान्त्रिक and vaidika mantra [which we had discussed before](https://manasataramgini.wordpress.com/2007/09/10/some-notes-on-the-evolution-of-the-mantra-shastra/).


