
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [अनन्तघनाः](https://manasataramgini.wordpress.com/2013/04/19/anantaghanah/){rel="bookmark"} {#अननतघन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 19, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/04/19/anantaghanah/ "7:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-dp7f0g2CSJY/UXDrWWwsh9I/AAAAAAAACpM/_xem04zS_ss/s640/Cubes.jpg){width="75%"}
```{=latex}
\end{center}
```



