
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Vaguely biological: The lemniscate echinodermomorph](https://manasataramgini.wordpress.com/2013/09/01/vaguely-biological-the-lemniscate-echinodermomorph/){rel="bookmark"} {#vaguely-biological-the-lemniscate-echinodermomorph .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/09/01/vaguely-biological-the-lemniscate-echinodermomorph/ "7:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-STy_KlkI5Ss/UiLpUVKqnAI/AAAAAAAACuk/GutsYqKALCU/s640/vaguebio05.jpg){width="75%"}
```{=latex}
\end{center}
```



