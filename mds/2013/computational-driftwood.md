
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Computational driftwood](https://manasataramgini.wordpress.com/2013/06/27/computational-driftwood/){rel="bookmark"} {#computational-driftwood .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 27, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/06/27/computational-driftwood/ "7:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-X6lJWTXMjR8/UcvgSLUSZCI/AAAAAAAACsY/wBrx9xfahPc/s800/falling_wood_small.jpg){width="75%"}
```{=latex}
\end{center}
```



The above object is a gnarled piece of driftwood. Only that it never floated in the sea and exists only in the virtual realm -- a pure mathematical abstraction -- a combination of three functions. Some would say it is an ultimate vindication of Platonism or the entities of shuddha-भुवनाध्वन्. In any case it raises the issue of whether natural form suggests that mathematical entities are merely discovered, i.e., a connection with that Platonic shuddha-शृष्टि, or are invented for describing nature, or is merely a reflection of a deep-rooted tendency for pareidolia.


