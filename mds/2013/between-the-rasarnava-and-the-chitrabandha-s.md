
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Between the रसार्णव and the chitrabandha-s](https://manasataramgini.wordpress.com/2013/01/06/between-the-rasarnava-and-the-chitrabandha-s/){rel="bookmark"} {#between-the-रसरणव-and-the-chitrabandha-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 6, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/01/06/between-the-rasarnava-and-the-chitrabandha-s/ "8:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the glorious summer we sought, through the secret practice, the enlightenment, which the elixir of viveka confers upon the lucky. We reclined in the holy land of bhArata, now sullied by the faded vigor of our peoples and the vileness of the म्लेच्छ and the तुरुष्क, in the shade of an ashoka tree. The ashoka reminded us of the stuff from legend, depicted in various forms of Indic art, the tree which symbolizes vernal delights, the epitome of the शृङ्गार rAsa (hence अङ्गनाप्रिय). Both kavI-s and shilpin-s of all hues have sought to introduce the ashoka in this capacity in their masterpieces.
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-d9AfhUHxxac/UOkxrIWZRjI/AAAAAAAACh0/fM33kjfdyPg/s800/a%257ENganApriya.jpg){width="75%"}
```{=latex}
\end{center}
```



*Depiction of an अङ्गना leaning on an ashoka from a derelict shaiva प्रासाद in the कलिङ्ग country*

Shielded by the ashoka's shadow in its native land we wondered as to when it became so central to the expression शृङ्गार by the Arya-s. It has no presence in the core Vedic layers of Arya texts. While some people have erroneously identified the Vedic शिंशप tree with the ashoka, it is clear that tree is actually the Indian Rosewood. It has a very limited presence in the महाभरत that still largely pertains to events in the original zone of the Arya-s in India. It is in the रामायण that we first find its repeated mention. Thereafter it becomes rather common place, with nAstika-s of both the ताथागत and jaina variety claiming association for their respective founders with the tree. Indeed, in the latter nAstika stream it takes a central place as the chaitya वृक्ष for the ford-makers. This suggested to us that the ashoka was not originally present in the उत्तरापथ that the Arya-s initially occupied but was rather a natural inhabitant of दक्षिणापथ (in particular the western ghats). Since the time the Arya-s came to know of it, we suspect they dispersed it as far north as the foothills of the hima-parvata-s. Indeed, the ashokavana of लङ्का probably had the southern species distinct from *Saraca asoca* of the upper peninsula.

Though it was a hot summer, we were in the spring of our life, and the embrace of our दूती सरोरुहा in the the shade of the ever-green ashoka, was much like it was hymned by कालिदास of yore. Indeed, he said:\
[नितम्ब-बिम्बैः स-दुकूल-मेखलैः स्तनैः स-हाराभरणैः स-छन्दनैः ।]{style="color:#0000ff;"}\
[शिरोरुहैः स्नान-कषाय-वासितैः स्त्रियो निदाघं शमयन्ति कामिनाम् ॥]{style="color:#0000ff;"}ऋतुसंहार 1.4

The commentator says: [दुकूलानि क्षौमाणि मेखला रसनाः ताभिः सहितैः स-दुकुल-मेखलैः । नितंब-बिंबईः नितंब मण्डलैः । हारा एवाभरणानि तैः सहितैः सहाराभरणैः । सचन्दनैः चन्दनानुलिप्तैः ।स्तनैः । स्नाने यः कषयो ऽङ्ग-रागः तेन वासितैः सुगन्धीकृतैः स्नान-कषाय-वासितैः । शिरसि रोहन्तीति शिरोरुहाः केशाः तैः शिरोरुहैः च । स्त्रियः कामिनां निदाघं ग्रीष्मोष्माणम् । शमयन्ति शान्तिं नयन्ति । निवारयन्तीति यावत् ।]{style="color:#0000ff;"}

Thus we might say: With well-rounded hips girdled with flaxen apparel, with breasts smeared with chandana and graced by garlands and ornaments, with hair tied up scented with the shampoo from the bath the women soothe their lovers' oppression by the summer's heat \[or keep off the summer's oppression from their lovers].

Coming back to the ashoka-s, cooling us with her stanau, सरोरुहा asked us: "We know and have seen the red and the yellow ashoka-s but O भृगु what is the blue ashoka (नीलाशोक) of वाल्मीकि?"\
We: "It might be that the bluish tinge of the new ashoka leaves have given that name."\
सरोरुहा: "What if there were a real plant of that type? Is there a blue flowered *Saraca*."\
We: "Not that I know of, but we should explore this more."\
सरोरुहा: "Remember that *Saraca* deviates from other caesalpinioids in that it has only sepals not petals. Is there another caesalpinioid like that?"\
We: "Indeed this gives us a clue -- we have *Dialium*, which among the caesalpinioids has no petals but only sepals."\
सरोरुहा: "The Dialium native to लङ्का has blue fruits, and hence it might have been termed the नीलाशोक. Perhaps वाल्मीकि purposely chose the Lankan plant to make his botany match the story line."\
We: "Indeed that seems like a plausible solution to the issue of the नीलाशोक".\
सरोरुहा: "How can we account for the convergent petal loss in these?"\
We: "Probably the expression of the B class genes are being pushed backwards?"\
सरोरुहा: "So does this have something with the emergence of extrafloral nectaries in *Saraca*? Look at them. They are feeding the ants. Is it that they are paying their body guards for protection?"\
We: "We wish we could find the answer for these questions?"\
सरोरुहा: "As also to the question if the great radiation of the legumes was due to the emergence of symbiosis with the rhizobia?"

We were to learn many important lessons of life from the rhizobia. Verily, the rhizobia first opened the door for one of the biggest insights of our very existence.

But as the cycle of the ऋतु-s turns, much as the six-spoked chakra of विष्णु, the pleasure of spring and the blaze of summer gave way the withering frigidity of the sunless months. In the depth of the freeze, when the only heat was from that of the funeral pyre सरोरुहा asked: What led to the crackling pyre on the break of the deathly chaitra chaturthi, when it seemed that the spring of life had been sucked by the trunk of a विनायक graha? Like virtue during the downward turn of the yuga-chakra was it all gone?"\
We: "We were right there. We were few steps away from the great curtain with the painting of कुमार, when all of it came to an end. It was like the dreadful noise and sparks at a fight between विनायक and skanda. Then there was the funeral; we were watching the fizz and snorts of the funeral pyre, where much was consumed by वैश्वानर who had become क्रव्याद, but not all. Even as we were in dismal retreat, we recalled the words of भारवि placed in the mouth of deva commander reproaching the retreating गण troops ."

In भारवी's great work the किरातार्जुनीय, skanda scolds the retreating गण-s thus:\
[देवाकानिनि कावादे वाहिकास्वस्वकाहि वा ।]{style="color:#0000ff;"}\
[काकारेभभरे काका निस्वभव्यव्यभस्वनि ॥ KA 15.25]{style="color:#99cc00;"}

It has two aspects to it: one is the set of constraints (bandha-s) pertaining its symmetry, which can be pictorially depicted (chitra) as what may be termed a sarvatobhadra मण्डल:
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-v0t8OZJbvdU/UOnXJBZm0ZI/AAAAAAAACiI/7DYXcAHmY6o/s400/भारवि_sarvatobhadra.jpg){width="75%"}
```{=latex}
\end{center}
```



It shows the symmetry of the square to both reflection and rotation -- hence the term sarvatobhadra. Hence, the Andhran commentator मल्लिनाथ सूरी from the Vijayanagaran court of देवराय states:[चतुष्-कोष्ठे चतुरङ्ग बन्ध क्रमेण अद्य पङ्क्ति चतुष्टये पाद-चतुष्टयं विलिख्यान् अन्तर्-पङ्क्ति-चतुष्टये ऽप्य् अधः क्रमेण पाद-चतुष्टय-लेखने प्रथमासु चतसृषु पङ्क्तिषु प्रथमः पादः सर्वतो वाच्यते, द्वितीयादिषु द्वितीय इत्यादि ॥]{style="color:#0000ff;"}

The second aspect to such bandha-s is that with such constraints, the entropy of the text reduces, i.e. it becomes low in complexity. Thus, the problem arises as to how a more complex meaning can be extracted from it. This has an interesting parallel to a problem in biology -- how is it that meaning is extracted from low complexity protein sequences. A globular protein typical has a high complexity of sequence and can be compared to a verse with fewer constraints that can easily express considerable nuance in terms of meaning (equivalent to higher specificity in biochemistry). One of the ways in which more complex information is extracted from low- complexity protein sequences in biology is by having enzymes that post-transcriptionally modify the low complexity sequences to allow for much greater nuance in "meaning". This is seen in the form of the modifications like phosphorylation, methylation, acetylation, ubiquitination and the like which can extract various "meanings" from the same low-complexity tails of histones or low complexity proteins like silaffins in formation of the exquisitely structured siliceous shells of diatoms. Thus, for our low-complexity sarvatobhadra verse we need the meta-knowledge of the काव्य-specialists to decode meaning from it. From the commentators मल्लिनाथ and ratneshvara we get:\
देवाकानिन् = deva + A-कायति (derived from the root kai meaning noise/sound) : possessed of the shouts of the deva-s OR देवाकानिन् = deva A-kan: inciting the deva-s; देवाकानिनि = masculine locative case

kA-वाde = confused cries; locative, hence: wherein there are confused shouts or कावादे= ka(head) + ava-A-dA (cut down): wherein heads are cut down.

वाहिका=movement of army OR van of the army

स्वस्वकाहि= sva (ones own) + svaka (own party) + A-hi( from root hi meaning to bear forward): bearing forward ones own party OR su (good) + asvaka (not ones own property, i.e. of the enemy) + A-hi: hastening with good property of the enemy

वा= and indeed

काकारेभभरे (again a locative) = ka (water, i.e. musth secretion) + A-कृ(from root कृ as in कीर्यते -- to pour out) + ibha (elephant) bhara (weight or temporal globe of elephants): where musth pours from the elephant's temple OR ka (splendor) + A-कृ(from root कृ as in कीर्यते -- to scatter, e.g. प्रकीर्ण) + ibha (elephant) bhara (multitude): where the splendor of the elephant multitude is scattered

काकाः (in vocative)= a contemptible ones OR greedy for plunder

nisva= ni-sva: selfless OR with no worth

bhavya = worthy OR successful result

vya= concealing OR the vya= squander

bhasvan = revealed OR bhasvan- reviling

Thus, we can have forward and backward interpretations:

Wherein (in the battle) there are shouts of the deva-s, wherein heads are cut down, wherein the movement of the army bears forward ones own party, wherein the splendor of the elephant multitude is scattered, O contemptible ones (i.e. the retreating गण-s)!, therein they revile those who squander the selfless and a successful outcome.

Wherein the worthy and the worthless under concealment are revealed, wherein the elephant's temporal globe flows with musth, O ones greedy \[for plunder], wherein the van of the army hastens with the wealth of the enemy, therein the deva-s are incited \[to war] in the midst of confused cries.

One might ask at this juncture -- are these really the meanings which भारवि originally intended for the verse? With मल्लिनाथ, ratneshvara and our own meditation we get different meanings from the verse -- so is it not so very subjective, with so many unusual words and constructs?

Regarding the first question it is clear that भारवि composed the verse with the symmetry of a square, the sarvatobhadra, so that meanings might be extracted forwards and backwards and they reveal themselves only after careful study if one were to allow for the least violence to grammar. This is not at all something which can be achieved easily -- hence, it is a high art form. Today this art is still kept alive in its pristine form by at least two extraordinary individuals शतावधानि Ganesh (whose अवधान performance displaying this art is a virtuoso display of Sanskritic tradition) and Shankar Rajaraman. Indeed, king bhoja-deva commenting on the structural feats of sarga 15 of the किरातार्जुनीयम् regards it the ornament of the whole work. The origins of such structural poetry might be traced back to [recombination of meters performed with the vaidika mantra-s during the shrauta rituals](https://manasataramgini.wordpress.com/2004/12/11/recombination-of-meters/).

Regarding the so called subjectivity the shruti holds: ["परोक्ष-प्रिया इवा हि देवा भवन्ति प्रत्यक्ष-द्विSअः ॥]{style="color:#0000ff;"} (gopatha ब्राह्मण of the atharvaveda 1.2.21)" Thus, it is in the nature of a kavi utilize such modes of expression which have the capacity to encode various alternative meanings, which can then be deconvoluted via the "epigenetic code" of परोक्ष. It is in this context the analogy with biochemistry is completed: If we take the histone tails they are low complexity when seen by themselves, much like base verse composed by भारवि or another such kavi. Then, various alternative "meanings" are put into these histone tails by the enzymes modifying them -- SET methylases, acetylases, arginine methylases, haspins, citrullination, ubiquitin ligases, the list goes on ... The action of these various enzymes is much like the action of the textual commentator who extracts the meanings from a low-complexity text. Finally, the meanings is read by the chromo-like, bromo, PHD and its derivatives, and some other domains. These are much like the consumers of the काव्य now realizing its meaning, or like a मन्त्रवादिन् deploying [such symmetric mantra-s](https://manasataramgini.wordpress.com/2006/07/23/the-samkshipta-bhairava-mantra/). Furthermore, just as different भाष्यकार-s emerge at different times after the original composition to discover different meanings in the text, so also at different times bacterial peptide-modifying enzymes have been transferred to eukaryotes allowing them generate new meanings in their histone tails. Just as the low-complexity sequences of the काव्य have to possess some internal robustness for such a multiplicity of semantics, likewise it is possible that the histone tails have comparable robustness due to their packing with the negatively charged DNA. This may also be true for comparable reasons with the silaffins in the diatom shells.

Then सरोरुहा asked two questions: 1) Do you think when those domains upon binding the modifications on the histones create a experience of "knowing" just as we experience when we realize the code of the काव्य? If yes who then has that experience? 2) I understand the pleasure we experience during संभोग, but why is it that we experience a comparable pleasure when we have understood the meaning behind an abstract structure?

As we pondered over these she wandered away... And we were reminded of भारवि again:

[निमीलद् आकेकर-लोच-चक्षुषां प्रियोपकण्ठं कृत-गात्र-वेपथुः ।]{style="color:#0000ff;"}\
[निमज्जतीनां श्वसितोद्धत-स्तनः श्रमो नु तासां मदनो नु पप्रथे ॥]{style="color:#0000ff;"} KA 8.53

निमीलद् closing; Akekara-partly closed; locha- glance; चक्षुषां- eyes; priya- lover; उपकण्ठ- in the vicinity; कृत makes; गात्र- limbs; vepathu- trembling; निमज्जतीनां -- women taking a plunge; shvasita- sighing; uddhata- lifting up; shrama- tiredness; nu- indeed; तासां- their; madana- love; paprathe- made known.

The women taking the plunge, closing and then glancing from the partly closed eyes, when their lovers are close, is it tiredness or indeed their loving being announced, that makes their limbs tremble and lifts up their breasts with a sigh.

Then hoping that the poisons of rudra might be pacified by the भेषज-s borne by शर्वानी we wandered away reciting:\
[अस्मिन्न् अगृह्यत पिनाक-भृता सलीलम् आबद्ध-वेपथुर् अधीर-विलोचनायाः ।]{style="color:#0000ff;"}\
[विन्यस्त मङ्गल महौषधिर् ईश्वरायाः स्रस्तोरग-प्रतिसरेण करेण पाणिः ॥]{style="color:#0000ff;"}

Here, the bearer of the पिनाक playfully took the hand of the supreme goddess, who had tied great healing remedies, with his hand bearing a slipping snake as an amulet, as she directed her tremulous, fluttering eyes \[to it].


