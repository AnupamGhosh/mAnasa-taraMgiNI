
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [रुद्राक्ष-s from the shuddha-भुवनाध्वन्-s or Platonic viruses](https://manasataramgini.wordpress.com/2013/04/22/rudraksha-s-from-the-shuddha-bhuvanadhvan-s-or-platonic-viruses/){rel="bookmark"} {#रदरकष-s-from-the-shuddha-भवनधवन-s-or-platonic-viruses .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 22, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/04/22/rudraksha-s-from-the-shuddha-bhuvanadhvan-s-or-platonic-viruses/ "5:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Based on the Icosahedron mated with the Mandelbulb of power 8
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-AFGzJKb2aKo/UXQ5DfzemtI/AAAAAAAACp0/r1w74ftdt_o/s800/mandel_bulb1_small.jpg){width="75%"}
```{=latex}
\end{center}
```



Its dual, the Dodecahedron, mated with the Mandelbulb of power 8
```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-BnCvyebAfac/UXTIStvI5bI/AAAAAAAACqE/9nFFiq_x7Oc/s800/mandel_bulb2_small.jpg){width="75%"}
```{=latex}
\end{center}
```



