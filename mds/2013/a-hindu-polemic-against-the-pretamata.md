
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A Hindu polemic against the pretamata](https://manasataramgini.wordpress.com/2013/04/30/a-hindu-polemic-against-the-pretamata/){rel="bookmark"} {#a-hindu-polemic-against-the-pretamata .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 30, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/04/30/a-hindu-polemic-against-the-pretamata/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

When the English gained ascendancy after the defeat of the marATha-s and the sikh-s they had gained military control over India but they knew well that their hold was still tenuous. Hence, in the period leading to the first war of independence in 1857 CE they realized that to control India they need to undermine the Hindu dharma and "storm the fortress of brahminism". To achieve this they deployed all manner of white missionaries to plant churches and bring in the harvests for the religion of love. They also saw this as a civilizing mission in which the barbarism of the Hindu dharma was to be purified by the shavamata. In particular, they sent European writers to denigrate the dharma in native languages of India. This attack from the pretamata could have well poisoned the wells of dharma had not :1) Hindu polemicists spear-headed by ब्राह्मण-s not launched a counter-attack on the pretamata. 2) The military mobilization taken place for the first war of independence. One of these polemics was composed by the by the proponent of the नवन्याय school tarka-पञ्चानन. Key to his approach, unlike that of several mahArAShTrI ब्राह्मण-s, was to engage in a direct counter-attack on the preta himself and expose the jejuneness of the primary belief of the pretamata. In this he was similar to कुमार-कार्त्तिकेय ayyar, another ब्राह्मण, who launched a similar counter-attack on the pretamata in लन्का. This approach was critical because otherwise a Hindu could be lost in being defensive, thereby conceding an advantage to the enemy right away. It also directly exposed the foundational principle of the shavamata to be flaky and thereby showed it as not meriting consideration as a serious cult. He also realized the real intention of the English प्रेताचरिन्-s, like John Muir who challenged the dharma, and explicitly warned people that he was a hindu-धर्मातिवैरिन्. It is clear that his arrow had hit its mark because the प्रेताचरिन्-s declared: "If the enemies we shall by and by have to contend in India are not more formidable than this brahman, our task will be an easy one" -- clearly a sense that they did not have an answer but only a boast. This smashing critique of the pretamata served as a model for later critiques by other ब्राह्मण-s in Hindi composed in response to the missionary attempts at वाराणसि.

tarka-पञ्चानन's devastating attack on the preta:

[मेरि-नाम्नी खृSटमाता बहुभिर् याजकैर् वृता ।]{style="color:#0000ff;"}\
[एकदेवालये ऽतिष्ठत् षोडषाब्द वयो ऽवधि ॥]{style="color:#0000ff;"}\
The mother of the preta, named मेरी, was loved by many worshipers when she lived in a certain temple till the age of sixteen.

[तत्र गर्भाह्विता भूत्वा जनयाम् आस संशयम् ।]{style="color:#0000ff;"}\
[स्वामिनो निश्चयं वापि ततः सो ऽcइन्तयत् सदा ॥]{style="color:#0000ff;"}\
Then having become pregnant, she gave rise to doubt in her husband, who, therefore, constantly bothered with the uncertainty of whether it was him or not.

[स्वप्ने चिन्ताकुलत्वेन तेन दृष्टह् समागतह् ।]{style="color:#0000ff;"}\
[देवदूतो जगादोछ्छैर् मा छिन्ता क्रियताम् इति ॥]{style="color:#0000ff;"}\
Unhinged with worry, he dreamed of a godly messenger coming to him from heaven and told him "do not worry".

[ईश्वरेणैव धर्मात्मा जनितो ऽस्ति स्त्रियाम् तव ।]{style="color:#0000ff;"}\
[न शङ्का व्यभिचारस्य तस्मात् कार्या त्वया नय ॥]{style="color:#0000ff;"}\
"By the lord, indeed, this holy one is conceived in your woman. Therefore, there should no suspicion of adultery in her actions by you.

[एवम् स्वप्नस्य वृत्तान्ते सर्वत्र प्रकटि कृते ।]{style="color:#0000ff;"}\
[स्वीय-दोषापहाराय याजकास् ते ऽपि संमताह् ॥]{style="color:#0000ff;"}\
Thus, when the rumor of the dream was made known to all, to conceal their own offense those same worshipers \[in the temple] colluded.

[विछारयन्तु विद्वांसो युवत्याः पुरुषैः सह ।]{style="color:#0000ff;"}\
[वासो गर्भस्य हेतुः स्यात् स्वप्नो वा स्यात् प्रमात्मकः ॥]{style="color:#0000ff;"}\
The knowledgeable should think whether sleeping with men could be the cause of the fetus or whether the dream could be believable.

[तया चेत् स्वीय-दोषस्य गोपनार्थं निवेदितम् ।]{style="color:#0000ff;"}\
[स्वप्ने दृष्टम् एवम् एवं तेन किं सत्यता भवेत् ॥]{style="color:#0000ff;"}\
Either she has announced \[this rumor] to conceal her offense, or he actually saw what he did in the dream. Who would not the reality?

[एवं स जार-जतो ।अपि याजकैर् अपि संस्तुतः ।]{style="color:#0000ff;"}\
[धर्मात्मा ऽहम् इति ज्ञात्वा धर्मं वक्तुं प्रचक्रमे ॥]{style="color:#0000ff;"}\
Thus, though he was a bastard, even the worshipers \[at the temple] praised him. He thinking "I am a holy soul" he began to preach a religion.

[कृत्व तद् वाचि विश्वासम् अविचार्य कथञ्चन ।]{style="color:#0000ff;"}\
[अल्प-बुद्धि जनास् तस्य जाताः सेवा-परायनाः ॥]{style="color:#0000ff;"}\
Placing belief in his words, hardly giving thought to anything, dim-witted people became his flock and servile followers.

[स्वीय-दोषाच्छादनार्थम् ऊचुर् अद्भूतचेष्टितम् ।]{style="color:#0000ff;"}\
[तथा तन् मायया ते ।अपि मोहिता हत-बुद्धयः ॥]{style="color:#0000ff;"}\
The worshipers \[from the temple] hide their own offense claimed miracles for him. Thus they too deluded by their own falsehoods became deprived of sense.

[ईश्वरो ऽयम् इति ग्रन्थे वर्णनाम् चक्रुर् अद्भुताम् ।]{style="color:#0000ff;"}\
[एवं क्रमेण धर्मस्य प्रछारो जनितो भुवि ॥]{style="color:#0000ff;"}\
In their book they made the tall claim: "This \[i.e. the preta] is the lord". Thus was this cult was publicized over the earth!

With the direct attack on the preta tarka-पञ्चानन had put back the ball in the court of the enemies and had exposed the chicanery at the heart of the preta cult.

Further tarka-पञ्चानन also launches a criticism of the way the प्रेतसाधक-s earn their converts:\
[केवलं धूर्त-पृष्तानाम् मायाजाले पतन् जनः ।]{style="color:#0000ff;"}\
[मद्य-माम्सादि-लोभेन पृष्ता-कन्यादिरूपतः ॥]{style="color:#0000ff;"}\
Only a person falling in the web of delusion woven by roguish प्रेताचरिन् priests, driven by the greed for liquor, meat or the beauty of the daughters of प्रेताचरिन् priests and the like,

[मोहितो भोग-लाभेच्छुर् अविचार्य स्वकं मतं।]{style="color:#0000ff;"}\
[खृSट-धर्मस्य दोषं वा नावेक्ष्य खृSटको भवेत् ॥]{style="color:#0000ff;"}\
One who is deluded by the urge for acquiring pleasures, who has not studied and contemplated his own religion nor looked at the flaws of Isaism, would become an Isaist.

These keen criticisms raised by तर्कपञ्चानन are strikingly consonant with the criticisms of the great yavana polemicist Celsus suggesting that the heathens have had a rather similar vision of the preta-mata over great distances in space and time. Many of तर्कपञ्चानन's criticisms of people falling to the moha of the pretamata are valid even today: how often one sees Hindus becoming शवसाधक-s for imbibing mada and consuming मांस in the संघ of the म्लेच्छ-s. Likewise, one also encounters Hindus falling to the delusions of the pretamata in order to consort fair-skinned स्त्री-s. Indeed these Hindus turn out to be those who have failed to study the dharma and look into the lunacy of the West Asian cult. If we had truly regained our freedom this work of तर्कपञ्चानन would be published in full and taught as part of our history of the struggle against the attacks we faced. Unfortunately, it is not easy at all to get a full copy of his work in bhArata today.


