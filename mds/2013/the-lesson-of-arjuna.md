
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The lesson of arjuna](https://manasataramgini.wordpress.com/2013/03/05/the-lesson-of-arjuna/){rel="bookmark"} {#the-lesson-of-arjuna .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 5, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/03/05/the-lesson-of-arjuna/ "7:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The third पण्डव, as an impetuous warrior, the embodiment of the great indra on earth gave a powerful lesson to his brother युधिष्ठिर when urged by his wife याज्ञसेनी. The pith of this lesson is truly one for the kali age, especially the situation in which the राष्ट्र that founded by the kuru and the पाञ्चाल finds itself in, ruled by दुष्ट-s, taskara-s and कुलुञ्च-s who are but frontmen of barbarous म्लेच्छ-s. Indeed, it is quite likely that a nation that forgets the essence of this lesson of the embodiment of indra, instead perpetually follows the false teachings of the मुण्डक who was proclaimed to be a महात्मन् by the uneducated, might fail to exist in the future.

We present the core of that teaching here:\
[नाच्छित्त्वा पर-मर्माणि नाकृत्वा कर्म दारुणम् ।]{style="color:#0000ff;"}\
[नाहत्वा मत्स्यघातीव प्राप्नोति महतीं श्रियम् ॥]{style="color:#0000ff;"}\
Without piercing the weak points \[Footnote 1] of enemies, without performing ruthless acts, without \[slaying foes] with the ruthlessness of a fisherman (slaying fish), no person can obtain great prosperity.

[नाघ्नतः कीर्तिर् अस्तीह न वित्तं न पुनः प्रजाः ।]{style="color:#0000ff;"}\
[इन्द्रो वृत्रवधेनैव महेन्द्रः समपद्यत ॥]{style="color:#0000ff;"}\
Without slaughter, no man has been able to achieve fame in this world or conquer wealth or subjects. Verily indra became the great indra (mahendra) by slaughtering वृत्र.

[य एव देवा हन्तारस् तांल् लोको ऽर्चयते भृशम् ।]{style="color:#0000ff;"}\
[हन्ता रुद्रस् तथा स्कन्दः शक्रो ऽग्निर् वरुणो यमः ॥]{style="color:#0000ff;"}\
Those amongst the gods who are slayers are more extensively worshiped in the world. The gods rudra, skanda, shakra, agni, वरुण and yama are all slayers.

[हन्ता कालस् तथा वायुर् मृत्युर् वैश्रवणो रविः ।]{style="color:#0000ff;"}\
[वसवो मरुतः साध्या विश्वेदेवाश् च भारत ॥]{style="color:#0000ff;"}\
काल, वायु, मृत्यु and kubera, ravi, the vasu-s, the marut-s, the साध्य-s, and the vishvedeva-s, O bhArata, are all slayers.

[एतान् देवान् नमस्यन्ति प्रताप-प्रणता जनाः ।]{style="color:#0000ff;"}\
[न ब्रह्माणं न धातारं न पूषाणं कथं चन ॥]{style="color:#0000ff;"}\
Laid low by the prowess of the above gods, all people pay obeisance to them, but not all time to brahmA or धातृ or पुषण्.

[मध्यस्थान् सर्वभूतेषु दान्ताञ् शमपरायणान् ।]{style="color:#0000ff;"}\
[यजन्ते मानवाः के चित् प्रशान्ताः सर्वकर्मसु ॥]{style="color:#0000ff;"}\
Only a few men that are pacific disposition worship in their rituals those gods that are equally disposed towards all creatures and that are pacific and peaceful.

[न हि पश्यामि जीवन्तं लोके कं चिद् अहिंसया ।]{style="color:#0000ff;"}\
[सत्त्वैः सत्त्वानि जीवन्ति दुर्बलैर् बलवत्तराः ॥]{style="color:#0000ff;"}\
I do not see an organism in this world that lives without doing any harm to others. Organisms live upon other organisms, the stronger upon the weaker \[Footnote 2].

[नकुलो मूषकान् अत्ति बिडालो नकुलं तथा ।]{style="color:#0000ff;"}\
[बिडालम् अत्ति श्वा राजञ् श्वानं व्यालमृगस् तथा ॥]{style="color:#0000ff;"}\
The mongoose eats the mouse; the cat the mongoose; the dog eats the cat; O king the dog is then \[consumed] by the cheetah .

[तान् अत्ति पुरुषः सर्वान् पश्य धर्मो यथागतः ।]{style="color:#0000ff;"}\
[प्राणस्यान्नम् इदं सर्वं जङ्गमं स्थावरं च यत् ॥]{style="color:#0000ff;"}\
All of these are eaten by man , behold \[this] dharma \[i.e. yama] which comes for all. All that is mobile and immobile are food for \[the continuation] of prANa \[Footnote 3].

[विधानं देव-विहितं तत्र विद्वान् न मुह्यति ।]{style="color:#0000ff;"}\
[यथा सृष्टो ऽसि राजेन्द्र तथा भवितुम् अर्हसि ॥]{style="color:#0000ff;"}\
This process is the way of the gods; hence, the learned man is never mystified by it. It is becoming you, O lord of the kings, to accept the very nature of your origins.

[विनीत क्रोध हर्षा हि मन्दा वनम् उपाश्रिताः ।]{style="color:#0000ff;"}\
[विना वधं न कुर्वन्ति तापसाः प्राणयापनम् ॥]{style="color:#0000ff;"}\
Giving up anger and pleasure \[it is] the slow-witted \[who] take refuge in the woods. \[Because] even the performers of austerities cannot support their lives without carrying out slaughter.

[उदके बहवः प्राणाः पृथिव्यां च फलेषु च ।]{style="color:#0000ff;"}\
[ na cha kash chin na tAn hanti kim anyat प्राणयापनात् ||]{style="color:#0000ff;"}\
In water, on earth, and in fruits, there are numerous living forms. It is not possible that one does not slaughter them how could one support one's life else?

[सूक्ष्म-योनीनि भूतानि तर्क-गम्यानि कानि चित् ।]{style="color:#0000ff;"}\
[पक्ष्मणो ऽपि निपातेन येषां स्यात् स्कन्धपर्ययः ॥]{style="color:#0000ff;"} Mbh 12.15.14-12.15.26\
There microscopic germ-like organisms whose existence can only be inferred by logic. At the wink of an eyelid whole multitude of such microscopic organisms are destroyed \[Footnote 4].

What arjuna does is to present this basic biological logical to this brother and explain that there can be no existence of one life form without injury to another. Hence, arguments for absolute ahiMsa are illogical. It is in this regard he taught a famous principle that was willfully distorted to emasculate the Hindus in modern times:

[लोकयात्रार्थम् एवेह धर्म-प्रवचनं कृतम् ।]{style="color:#0000ff;"}\
[अहिंसा साधुहिंसेति श्रेयान् धर्म-परिग्रहः ॥]{style="color:#0000ff;"}\
The dharma has been taught only for the appropriate maintenance of ways of the world. Between nonviolence (ahiMsa) and violence guided by proper motives (सधुहिंस), the superior action is that by which dharma is maintained.

[नात्यन्त गुणवान् कश् चिन् न चाप्य् अत्यन्त निर्गुणः ।]{style="color:#0000ff;"}\
[उभयं सर्व-कार्येषु दृश्यते साध्व् असाधु च ॥]{style="color:#0000ff;"} Mbh 12.15.49-50\
There is no act that is entirely meritorious, nor any that is entirely wrong. In all acts, something of both, right and wrong, is \[always] seen.

It is with this background arjuna urges the placing युधिष्ठिर to pursue the path of दण्डनीति that said to have been taught in the world of men by the भृगु उशना काव्य.

Footnote 1: In the Hindu martial tradition marman-s are special "junction" points where a warrior strikes to instantly disable or kill the opponent. The tradition of these marman-s and striking them survives today only the south Indian martial tradition from the Tamil and chera countries. What arjuna is talking about here is striking these marman points of enemies in battle.

Footnote 2: All students of biology know this to be a truism -- perhaps even an inherent to life, as the पाण्डु clarifies in this shloka.

Footnote 3: Here the पाण्डव is describing the food chain and the fact that flow of prANa is the consumption of food by all -- death is the dharma of life were every organism ends up as food for another.

Footnote 4: Here arjuna is encapsulating a very important facet of early Hindu knowledge i.e. that of microbial life. Even though they could not be seen, the early Hindus had inferred their occurrence by reasoning based on fermentation and postulated that the whole earth, water and plants are covered by these microbial life forms. We regard this a major scientific inference of our early tradition. This knowledge, which the nirgrantha-s inherited from their predecessors, combined with obsession for ahiMsa of their founder led to their numerous practices to avoid killing microscopic life.


