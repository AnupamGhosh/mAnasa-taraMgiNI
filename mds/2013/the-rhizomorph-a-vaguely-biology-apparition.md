
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rhizomorph: a vaguely biological apparition](https://manasataramgini.wordpress.com/2013/07/28/the-rhizomorph-a-vaguely-biology-apparition/){rel="bookmark"} {#the-rhizomorph-a-vaguely-biological-apparition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 28, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/07/28/the-rhizomorph-a-vaguely-biology-apparition/ "7:25 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-lzI_HdPQfp4/UfVvYcu0SUI/AAAAAAAACtg/QbBfoz7DRS8/s640/Rhizomorpha.jpg){width="75%"}
```{=latex}
\end{center}
```



