
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Time, world history, or the lack thereof](https://manasataramgini.wordpress.com/2013/06/13/time-world-history-or-the-lack-thereof/){rel="bookmark"} {#time-world-history-or-the-lack-thereof .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 13, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/06/13/time-world-history-or-the-lack-thereof/ "6:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many years ago in our old home we came across a book won by the भार्गव-trasadasyau in a contest that had the biographies of many a Euro-American figure. At that point we were impressed by certain threads in the German poetry of Rainer Maria Rilke and looked up that book for some biographical information on him. While doing so we stumbled upon another Germanic figure, Oswald Spengler, whose biography was contained in that volume. We became intrigued by this figure and read more of his work. We perused his "The decline of the West: form and actuality". It was a tedious effort for us; covering it English translation, we re-examined parts of it in the original German upon the inspiration of a German gentleman who was then visiting our city. The German said that Spengler was prescient in many ways and even when wrong his assertions pointed fertile thought-scapes. Hence, we pursued the effort of scaling Spengler -- something that was probably just a little less operose than the effort of that German to understand parts of the तैत्तिरीय ब्राह्मण describing a ritual to which he had no deeper connection. At the end we were not entirely sure of what to make of Spengler but we could sense why he was influential in the West.

Spengler made some peculiar statements; one right in the introduction to his tome that caught our eye (We are revisiting this due to a subliminal signal from a tweet on Twitter):

*"Amongst the Western peoples, it was the Germans who discovered the mechanical clock, the dread symbol of the flow of time, and the chimes of countless clock towers that echo day and night over West Europe are perhaps the most wonderful expression of which a historical world-feeling is capable. In the timeless countrysides and cities of the Classical world, we find nothing of the sort. Till the epoch of Pericles, the time of day was estimated merely by the length of shadow, and it was only from that of Aristotle that the word hora received the (Babylonian) significance of "hour"; prior to that there was no exact subdivision of the day. In Babylon and Egypt water-clocks and sun-dials were discovered in the very early stages, yet in Athens it was left to Plato to introduce a practically useful form of clepsydra, and this was merely a minor adjunct of everyday utility which could not have influenced the Classical life-feeling in the smallest degree. It remains still to mention the corresponding difference, which is very deep and has never yet been properly appreciated, between Classical and modern mathematics. The former conceived of things as they are, as magnitudes, timeless and purely present, and so it proceeded to Euclidean geometry and mathematical statics, rounding off its intellectual system with the theory of conic sections. We conceive things as they become and behave, as function, and this brought us to dynamics, analytical geometry and thence to the Differential Calculus. The modern theory of functions is the imposing marshaling of this whole mass of thought. It is a bizarre, but nevertheless psychologically exact, fact that the physics of the Greeks --- being statics and not dynamics --- neither knew the use nor felt the absence of the time-element, whereas we on the other hand work in thousandths of a second. The one and only evolution-idea that is timeless, ahistoric, is Aristotle's entelechy. This, then, is our task. We men of the Western Culture are, with our historical sense, an exception and not a rule. World-history is our world picture and not all mankind's. Indian and Classical man formed no image of a world in progress, and perhaps when in due course the civilization of the West is extinguished, there will never again be a Culture and a human type in which "world-history" is so potent a form of the waking consciousness."*

This assertion by Spengler arrested us at many in levels. As the German had said, even though it was clearly wrong, it did cast a stone in our mental pools setting off trains of thought waves. First, Spengler, unlike many other westerner thinkers closer to our time, did acknowledge the similarities between the Greco-Roman classical and Indian (i.e. Hindu) civilization in opposition to the "modern" western civilization in the Euro-American world with a Germanic epicenter. However, beyond this, he does not have much knowledge nor does he dwell much upon the Hindu civilization. As a result many of his sweeping generalizations are plain wrong or incomplete but still warrant further consideration and evaluation. After all even as a youth we had heard the famous western accusation: "Indians lack a sense of history". Many years later when we were blithely whiling away our time on internet fora we heard the occasionally insightful malla राजीव turn this around and make it a virtue: He declared that unlike the Abrahamists we are not "history-centric". Hence, apparently we are not caught up with the linear thinking and the need to adhere to a single messiah or rasool as the प्रेताचरिन्-s and मरून्मत्त-s respectively do. And then, while seeking to enjoy स्त्री-bhoga we instead found ourselves entangled in the notorious debate on the meaning of आनादित्वं of the shruti. Our interlocutor in addition to keeping us away from the yonirati declared that the thought process of the मुमुक्षु Hindu was really time independent because the brahman was eternal and unchanging -- time apparently truly belonged in the realm of avidyA which had to be discarded much as a worn out vesture. There was also that outrageous श्रीलन्कन् scientist, whom we heard in our youth, who declared that the Indian concept of time did not require one to care much for history because it was anyhow going to happen again -- whether one being reborn over and over or events on the large scale of the whole yuga cycle. Hearing all of this we wondered after all if Spengler was right and there was not much place for temporal dynamics leave alone "world history" in our thought. Perhaps our own interest in such things was merely a grafted fancy, a western influence, much as horoscopic astrology in the past and cricket in our times.

But clearly this was not the case -- after all the first, very contemporary-looking, world history to our knowledge was the [brainchild of the Mongols which was financed and executed by a multi-ethnic group of scholars employed by them](https://manasataramgini.wordpress.com/2011/05/20/some-notes-on-rashid-ad-din-bin-imad-ud-dawla-abul-khair-and-his-times/). At that point in time none in the European world even a had smidgen of idea to create such a history. This we see as a natural outgrowth of ideas such as the "histories" of self and neighboring clans which were seen among various Eurasian peoples, including formulations like the dynasties of the पञ्चजनाः and इक्ष्वाकु-s among the Arya-s.

But going right to the bottom of it, i.e., the idea of time itself, things are rather different from Spengler or what our own compatriots think it to be.In the तैत्तिरीय AraNyaka we have the mantra:\
[कला मुहूर्ताः काष्ठाश् चाहोत्राश् च सर्ववशः ।]{style="color: #0000ff"}\
[अर्धमासा मासा ऋतवस् संवत्सरश् च कल्पन्तां ॥]{style="color: #0000ff"}

Hindu tradition tells us that:\
[निमेषैः पञ्च-दशभिः काSठा त्रिंशत् तु ताः कलाः ।]{style="color: #0000ff"}\
[त्रिंशत् कलाः मुहूर्तस् तु त्रिंशता तैः मनीषिण अहो-रात्रम् इति प्राहुः ॥]{style="color: #0000ff"} हरिवंश 1.8.3+4

15*निमेष-s=काष्था; 30* काष्था=कला; 30*कला-s=मुहूर्त; 30*मुहूर्त-s= अहोरात्र (the civil day).

Thus, the early Hindus at least \~3500 years ago already had divided the day into units going down to intervals comparable to a second. How did they measure fine divisions of time? After all the white indologists and their Japanese imitators would not grant our ancestors any capabilities beyond approximate discernment of gross intervals. The answer is by using the water-clocks. By 1300 BCE we know that the Hindus were definitely using water-clocks (evidence from वेदाङ्ग ज्योतिष). In Egypt and Babylon the first evidence points to the period of around 1300-1400 BC. That the Hindus used one before that is suggested by the काल-सूक्त composed by our ancient clansmen:\
[पूर्णः कुम्भो ऽधि काल आहितस् तं वै पश्यामो बहुधा नु सन्तम् ।]{style="color: #0000ff"}

That काल was so important in the Weltanschauung of the early Hindus is evident from the two सूक्त composed by our ancient clansmen (AV-vulgate 19.53-54) wherein the above statement is found. The सूक्त goes on to say:\
[तेनेषितं तेन जातं तद् उ तस्मिन् प्रतिष्ठितम् ।]{style="color: #0000ff"}\
[कालो ह ब्रह्म भूत्वा बिभर्ति परमेष्ठिनम् ॥]{style="color: #0000ff"} (AV 19.53.9)\
In time the \[universe] was emitted, by time \[all that exists] came into being, and in time indeed \[the universe] is contained. Time having verily having become brahma, holds up the primal station of existence.

[सर्वांल् लोकान् अभिजित्य ब्रह्मणा कालः स ईयते परमो नु देवः ॥ ]{style="color: #99cc00"}(AV 19.54.5)\
Having conquered all the worlds by brahman, काल is indeed perceived as the primal deity.

Thus, far from being "timeless" the Hindu saw time as being supreme and equated it with his concept of the foundation of all existence, the brahman. This fascination of led to the Hindu creating many measures for both ends of its spectrum. While the Mesopotamian had has his smallest unit of time as 4 minutes (Akkadian गेष्), the Hindus proceeded to divide time to units much smaller than a second. On the other end they had the yuga, the kalpa and the day of prajApati among others, which went on at colossal scales of greater than 10^17 seconds. At the lower end in the physical models of सांख्य and वैशेषिक this led to the concept of the quantized nature of time -- that time too is composed of the fundamental quanta even as matter is composed of quanta they termed परमाणु-s. At the other end the Hindu conceived a dramatic vision of the end of the universe upon the conclusion of the day of brahmA happening after 8.64 billion years. कृष्ण-द्वैपायन narrates it to युधिष्ठिर thus (I do not attempt to translate it all literally at this instance and stop at the end of the physical universe; the सांख्यन् theory being more complex):

[ततः शत-सहस्रांशुर् अव्यक्तेनाभिचोदितः ।]{style="color: #0000ff"}\
[कृत्वा द्वादशधात्मानम् आदित्यो ज्वलद् अग्निवत् ॥]{style="color: #0000ff"}\
[चतुर्विधं प्रजाजालं निर्दहत्य् आशु तेजसा ।]{style="color: #0000ff"}\
[जराय्व् अण्ड-स्वेदजातम् उद्भिज्जं छ नराधिप ॥]{style="color: #0000ff"}\
[एतद् उन्मेष-मात्रेण विनिष्टं स्थाणु जङ्गमम् ।]{style="color: #0000ff"}\
[कूर्मपृष्ठ-समा भूमिर् भवत्य् अथ समन्ततः ॥]{style="color: #0000ff"}\
[जगद् दग्ध्वामित-बलः केवलं जगतीं ततः ।]{style="color: #0000ff"}\
[अम्भसा बलिना क्षिप्रम् आपूर्यत समन्ततः ॥]{style="color: #0000ff"}\
[ततः कालाग्निम् आसाद्य तद् अम्भो याति संक्षयम् ।]{style="color: #0000ff"}\
[विनष्टे ऽम्भसि राजेन्द्र जाज्वलीत्य् अनलो महान् ॥]{style="color: #0000ff"}\
[तम् अप्रमेयो ।अतिबलं ज्वलमानं विभावसुम् ।]{style="color: #0000ff"}\
[ऊष्माणं सर्वभूतानां सप्तार्चिषम् अथाञ्जसा ॥]{style="color: #0000ff"}\
[भक्षयाम् आस बलवान् वायुर् अष्टात्मको बली ।]{style="color: #0000ff"}\
[विछरन्न् अमित-प्राणस् तिर्यग् ऊर्ध्वम् अधस् तथा ॥]{style="color: #0000ff"}\
[तम् अप्रतिबलं भीमम् आकाशं ग्रसते ।आत्मना ।]{style="color: #0000ff"}(Mbh- "critical" 12.300.4-11)

The hundred-thousand rayed sun by the force of the unmanifest (the सांख्यन् avyakta) blazes forth with the combined luster of the 12-fold Aditya-s. The 4 types of living organisms on earth are incinerated in a निमेष and the surface of the earth is entirely baked taking the appearance of the shell of a tortoise. Then in the great world-encompassing blaze the earth melts down into a fluid. This fluid is ignited by the great blaze of the sun which burns consuming all the elements even as the fuel for the seven-fold agni. This blaze is then swallowed by the great 8-fold वायु who encompasses all space. This this great gas is absorbed into empty space -- this marks the end of the physical universe as per the model of सांख्य. Thus, the universe of the Hindu is hardly not one of the "world in progress". In fact as per his vision the world progresses serially through several cycles of yuga-s and is finally destroyed at the end of the night of brahmA as described above.

Differential calculus does not necessarily imply kinetics. One could conceive the derivative in an entirely static sense as the slope of the tangent to a curve much like in constructions that the yavana-s revered so much (of course one could argue that by its very definition the curve encompasses dynamics but this is a rather strained way of seeing things). But then the Hindu understanding of the infinitesimal time did lead to the derivative with respect to time well before it did in the West. In his analysis of planetary motion भास्कर clearly distinguishes the average speed (स्थूल-gati, i.e. s=∆x/∆t) from velocity (तात्कालिकी-gati, i.e. dx/dt the time derivative).

Thus, several of the concepts claimed by Spengler to be distinguishing the modern West from the "timeless" Indian might have arisen first among the Hindus. In the foundational epic of our nation the location of the temporal frame within the Hindu time cycles is important:

[त्रेता-द्वापरयोः संधौ रामः शस्त्र-भृतां वरः ।]{style="color: #0000ff"}\
[असकृत् पार्थिवं क्षत्रं जघान अमर्ष-चोदितः ॥]{style="color: #0000ff"}\
In the junction between the त्रेता and the द्वापर epochs रामो भार्गव, the foremost of the warriors, repeatedly slaughtered the warriors on the earth impelled by his indignation.

[अन्तरे छैव संप्राप्ते कलि-द्वापरयोर् अभूत् ।]{style="color: #0000ff"}\
[समन्त-पञ्चके युद्धं कुरु-पाण्डव-सेनयोः ॥]{style="color: #0000ff"}\
In the interval between the kali and the द्वापर epochs there took place at the five samanta lakes the war between the kuru and पाण्डव armies.

Thus, despite the cyclical nature of the Hindu time with a sense of history repeating itself -- the gory battles in the vicinity samanta पञ्चक waged by the भृगु-s and later the kuru-पाण्डाव clash at the two yuga junctions -- the events are not identical. There is a clear awareness of the "progress" inherent in history which is superimposed upon the large cycles of time where periodicity might be observed.

However, it was the Chingizid Mongols, with a rather simple temporal sense to start with (at least by Spenglerian standards), who were the first to create a "World history". Indeed their own great epic, Mongghol-un nighuca tobchiyan (The Secret History begins rather timelessly):

"*There was a blue wolf which was born with fate decreed by Köke Möngke Tengri above. His wife was a fallow doe (Dama dama). They came passing over the Chingiz (ocean). When they were encamped at the head of the Onan river \[at the foot of] Mount Burkhan Khaldun to them was born Batachi-Khan.*"\
Thus, it would seem that one does not start with a sophisticated conception of time to compose a "World history" as Spengler seems to suggest.

The Chingizid Mongols, like the modern West, had a relatively meteoric rise. This perhaps more than anything else predisposed them to be more conscious of the need to place themselves vis-a-vis the more established peoples of the world. Hence, at least in the west there was both an overt and subliminal urge to present their ascent within the framework of the "Force of history" which was inevitably driving their rise to the pinnacle of civilizational achievement on the earth (as our old ancestors would have said "वर्ष्मन् पृथिव्या"). But Spengler with a certain prescience saw this to be a temporary flash that was to be followed by the decline that evidently lies beyond any pinnacle. Thus, he thought that the very concept of world history might come to an end with them. Indeed, with the decline that followed the glorious elevation of the Chingizids the Mongolian world history was largely lost to humanity (Though we cannot rule out that their inspiration was not somehow transmitted to west via Mohammedan intermediaries who only half understood the concept).

Thus, the counter-examples falsify Spengler's assertions that the concept of world history is unique to the West and also that it is dependent upon a deep interest in or knowledge of time and its divisions. Rather it would seem that in the case of the Hindus the fascination with time has resulted in understanding that history can take a long time to unfold. Indeed, Hindus right from their earliest days have placed an emphasis on the continuity of existence over great temporal intervals. Hence, they term their tradition the सनातन dharma. The constancy of natural laws over deep time is an important facet of Hindu thought. Thus, the ancient vipra of the अङ्गिरस् clan vishvamanas vaiyashva in his mantra to mitra and वरुण describes the ऋत of मित्रावरुणा:\
[सनात् सुजाता तनया धृतव्रता ।]{style="color: #0000ff"}RV 8.25.2c\
The well-born sons \[of aditi] whose ancient laws still hold firm.

Again, nodhas of the clan of the gotama-s describes the celestial cycles as ancient laws that are set in place by indra (he uses the word सनात् many times in this सूक्त to emphasize the ancient nature of the celestial motions):\
[सनात् सनीळा अवनीर् अवाता व्रता रक्षन्ते अमृताः सहोभिः ।]{style="color: #0000ff"}\
\[The celestial cycles] emerging from a common source, seen as paths in the airless heavenly realm firmly watch the unviolated laws since the ancient times.

Similarly, we have विश्वामित्र say:\
[वैश्वानराय पृथुपाजसे विपो रत्ना विधन्त धरुणेषु गातवे ।]{style="color: #0000ff"}\
[अग्निर् हि देवां अमृतो दुवस्यत्य् अथा धर्माणि सनता न दूदुषत् ॥]{style="color: #0000ff"} RV 3.3.1\
For वैश्वानर of broad luminescence, \[our] incantations offer jewels to move within the foundations \[of the ritual].\
Indeed, the immortal agni makes worship to the gods; hence, none violated the ancient laws.

This emphasis on the ancient laws comes from the recognition of a much longer cycle of time than the day, night, month or year, namely the precessional cycle. Thus, the ancient ब्राह्मण-s state:\
[द्वे ते चक्रे सूर्ये ब्रह्माण ऋतुथा विदुः ।]{style="color: #0000ff"}\
[अथैकं छक्रं यद् गुहा तद् अद्धातय इद् विदुः ॥]{style="color: #0000ff"} (RV 10.085.16)\
O goddess sUryA two of your cycles are known to the ब्राह्मण-s well-versed in calenderical calculations; but that other one cycle which is secret is known only to those versed in the highest truths.

The first two cycles are the cycles of the month and year, but the 3rd secret cycle known only to the higher vipra-s is the precessional cycle. Thus, the knowledge of the great cycles comprising of thousands of years has made the Hindu prefer long-standing constants (hence conservativeness) over fleeting fads. He even holds that in course of the cycles the shruti might be lost it in its physical form but returns over and over again due it being a mark of the ancient (सनात्) laws that continue standing through all these cycles (धृतव्रत). However, the West driven by its belief in the force of history driving it to glory needs to desperately cling on to its place as the tide will eventually be past it. In this regard Spengler is probably correct. The end of of the west lies within its own constructs -- the need to place a patina on its adharma-ridden martial past spawned liberalism and "modernism". But these constructs are fatal to a civilization that adopts them -- this is consonant with things Spengler foresaw -- for instance, the consequences of demographic decline (e.g. just a few days ago the white birth rate dropped below its death rate in the US). However, as the Hindus realized, this is not going to happen instantly, rather it is something that will play out over a long temporal interval. Importantly, Hindus need to realize that by adopting the western constructs, chief among them being liberalism, and turning their back on what their own civilization emphasizes, the सनात्, they are eating the poisoned seeds of perdition. The same way the sun will eventually set on the west it will on the Hindus for good, unless they return to the ways of the सनातन dharma that persists through the cycles of the yuga-s. Hence, one may meditate on the mantra:

[अनु यत् पूर्वा अरुहत् सनाजुवो नि नव्यसीष्व् अवरासु धावते ॥]{style="color: #99cc00"} RV 1.141.5cd


