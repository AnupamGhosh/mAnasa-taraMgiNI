
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief rant on the Indian political system](https://manasataramgini.wordpress.com/2013/06/11/a-brief-rant-on-the-indian-political-system/){rel="bookmark"} {#a-brief-rant-on-the-indian-political-system .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 11, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/06/11/a-brief-rant-on-the-indian-political-system/ "6:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The coming elections in the secular, socialist, democratic republic of India certainly seem to be a rather important fork in the road. A member of the clan mentioned that an exuberant Hindu politician had declared that this election will mark the beginning of the great rise of the Hindus. This made us give some thought about whether things were really looking so rosy. Of course we can hardly lay claims to prophetic insights, which belong to the domain of the सूत-s of the भविष्यत पुराण; however, we do have some expectations based on the current situation that hardly match the estimates of that politician. The short-live collaboration between the Hindus and the Islamic Jihad during the First War of Independence in 1857 CE came to a catastrophic closure with the genocidal victory of the English. However, it had poisoned the wells of Hindu sustenance beyond what the English could inflict. It laid the seeds of the idea that somehow the मरून्मत्त-s were acceptable brethren of Hindus despite their being infected by the West Asian meme. Hence, rather than seeking to cure their infection, or quarantining, or removing the infected it was thought that it was alright to have them live side-by-side the uninfected. To add insult to injury the wonders of western education brought with it the side-effect of making the Hindus immunocompromised to the toxicity of मरून्माद and also lowered their group fitness. This was particularly terrible because the मरून्माद infection increased the group fitness of the मरून्मत्त-s. This meant that living beside their infected brethren was an eventual death sentence for the H. The events that played to close to independence only enhanced this. The मरून्मत्त-s seized large chunks of our land for themselves reminding Hindus that the battle with the English was only a passing sideshow and that the millennial Jihad was still unfinished but not unforgotten business. The immunocompromised and fitness-depressed H instead were saddled with a secular republic where they continued living beside the मरून्मत्त-s. In the midst of all this, they were further stabbed from within by the greed of various jati-s who exploited the reservation provision to ensure that residual bhArata does not put forth its best people to meet the challenges of the world. Rather it placed ill-trained and cognitively weaker members of society to face the harsh challenges of a real world, where even in the Asian backyard the threat from the the cognitively well-endowed प्राच्य-s presented itself. It was unable to channelize its cognitive elite who left it shores to serve the West (or in the least seek a home in the West for there was really nothing for them in bhArata).

The Hindu imitation of the English democratic system, by turning its back on the collective wisdom of manu, याज्ञ्वल्क्य, भीष्म, विष्णुगुप्त and a host of others resulted in a fundamental misunderstanding of numerous issues:

 1.  Democracy works best in relatively uniform populations with the same basic cultural bonds. It will start having problems as soon as diversity is introduced in the form of groups that are held by fundamentally different bonds from that of the majority. Experiments establishing this can be seen in the form the events in the past decade in France and Scandinavia among others. As this type diversity increases democracy will collapse.


 2.  Genuine democracy will lead to "systems failure" when there is great cognitive differential between the masses and elite. This has been diagnosed and correctly described by [Amy Chua](https://manasataramgini.wordpress.com/2004/10/03/watch-out-democracymarxism-is-for-all/) several years ago.


 3.  Democracy will have negative effects if the host population has a depressed group fitness relative to the two West Asian monotheisms which reside in its midst.


 4. The Aryans upon their entry into India actually gauged the nature of the social diversity pretty well and created systems that could create functional systems despite this diversity, which would have normally crippled formation of any mega-system. In this the democratic system was restricted to local governments. However, on the global scales the meritorious राजन्, and संराट् were the one who held sway. Thus, the democracy worked within relatively uniform local groups while they were held in place in the mega-system by the mobile elite comprised of the brahma-क्षत्र that offered a unifying large-scale ideology and law-enforcement for allowing the वणिज to carry out his trans-regional economic activity. The Indo-Aryans also weakened the transformation of diversity into separate identity by fostering their functional diversification and thereby creating dependencies between the elements within the overall diversity. This large-scale ideology was reinforced by the grand shrauta rituals. Hence, a Hindu modern nation can only survive if it shelves any notions of secularity and reinforces its identity through the performance of grand national rituals of the type of the राजसूय, वाजपेय and ashvamedha or the तान्त्रिक temple rituals that bring together all sections of the society.


