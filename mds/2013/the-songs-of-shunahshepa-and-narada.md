
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The songs of शुनःशेप and नारद](https://manasataramgini.wordpress.com/2013/06/26/the-songs-of-shunahshepa-and-narada/){rel="bookmark"} {#the-songs-of-शनशप-and-नरद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 26, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/06/26/the-songs-of-shunahshepa-and-narada/ "6:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the nocturnal ritual of the soma rite 27th and 28th सामवेदिc stuti-s are described below. These songs are sung sometime between 12.30-1.00 AM. The सामवेदिc mantra-s have the following musical pattern with respect to the three सामवेदिन् ritualists:\
The प्रस्ताव is sung by the प्रस्तोता, the उद्गीथ by the उद्गाता, the प्रतिहार by the प्रतिहर्ता, the upadrava by the उद्गाता, and the nidhana by all three.

[प्रस्ताव: योगे-योगे तवा हा वु स्तारां ।]{style="color:#99cc00;"}\
[उद्गीथा: ऒ जेवाजे हवामाहा ए हू वा औ हो वा ।]{style="color:#99cc00;"}\
[प्रतिहार: साखाय इन्द्रं ऊताया ए हू वा औ हो वा ।]{style="color:#99cc00;"}\
[उपद्रव: ऒ खाय या हू वा औ हो वा ।]{style="color:#99cc00;"}\
[निधन: द्रम् ऊतये ॥]{style="color:#99cc00;"}

[प्रस्ताव: आ घा गमद् यदी हा वु श्रावात् ।]{style="color:#99cc00;"}\
[उद्गीथा: ऒ हस्रिणीभिर् ऊता यि भा ए हू वा औ हो वा ।]{style="color:#99cc00;"}\
[प्रतिहार: वाजेभिर् ऊप नो हावाम् हू वा औ हो वा ।]{style="color:#99cc00;"}\
[ upadrava: O jebhir A hU वा au ho वा |]{style="color:#99cc00;"}\
[ nidhana: pa no हवां ||]{style="color:#99cc00;"}

[प्रस्ताव: अनु प्रत्नस्यौ हा वु कसः ।]{style="color:#99cc00;"}\
[उद्गीथा: ऒ वे तूवीप्रता यिन् नारां हू वा औ हो वा ।]{style="color:#99cc00;"}\
[प्रतिहार: यान् ते पूर्वं पिता हूवा ए हू वा औ हो वा ।]{style="color:#99cc00;"}\
[उपद्रव: ऒन् ते पूर्वं हू वा औ हो वा ।]{style="color:#99cc00;"}\
[निधन: पिता हूवे ॥]{style="color:#99cc00;"}

The lyrics are based on the following mantra-s of the शुनःशेप the son of the fallen भार्गव ajigarta composed after he was adopted by विश्वामित्र:\
[योगे-योगे तवस्तरं । वाजे-वाजे हवामहे । सखाय इन्द्रम् ऊतये ॥]{style="color:#99cc00;"}\
In every junction of life, in every booty-winning contest, like \[calling[]]{style="font-size:13px;line-height:19px;"}[ friends we call the mightiest indra for aid.]{style="font-size:13px;line-height:19px;"}

[आ घा गमद् यदि श्रवत् । सहस्रिणीभिर् ऊतिभिः । वाजेभिर् उप नो हवम् ॥]{style="color:#99cc00;"}\
If he shall hear our call, he shall come to our call with a 1000 aids, conquering booty.

[अनु प्रत्नस्यौकसः । हुवे तुविप्रतिं नरम् । यं ते पूर्वम् पिता हुवे ॥]{style="color:#99cc00;"}\
I invoke the irresistible warrior of the primal fortress, you whom father [विश्वामित्र] had formerly invoked.

[प्रस्ताव: इन्द्र सुतेषु सोमेषु ।]{style="color:#99cc00;"}\
[उद्गीथा: ऒ तूं पुनायिष उक्थियां विदे वार्द्धास्य दक्षस्य ।]{style="color:#99cc00;"}\
[प्रतिहार: महं हायि षः ।]{style="color:#99cc00;"}\
[ upadrava: O ham hi ShaH |]{style="color:#99cc00;"}\
[ nidhana: O yi DA ||]{style="color:#99cc00;"}

[प्रस्ताव: स प्रथमे वियॊमानी ।]{style="color:#99cc00;"}\
[उद्गीथा: ऒ वानां सादने वृधास् सुपारास् सूश्रवस्-तमः ।]{style="color:#99cc00;"}\
[प्रतिहार: समप्सूजीत् ।]{style="color:#99cc00;"}\
[उपद्रव: ऒ मप्सुजा यित् ।]{style="color:#99cc00;"}\
[ nidhana: O yi DA ||]{style="color:#99cc00;"}

[प्रस्ताव: तमु हूवे वाजसाताया यि ।]{style="color:#99cc00;"}\
[उद्गीथा: ऒन्द्रां भाराय शुष्मीणां भवा नास् सूम्ने अन्तमः ।]{style="color:#99cc00;"}\
[प्रतिहार: सखा वार्द्धा यि ।]{style="color:#99cc00;"}\
[उपद्रव: ऒ खा वृधा यि ।]{style="color:#99cc00;"}\
[ nidhana: O yi DA ||]{style="color:#99cc00;"}

The lyrics are based on the following mantra-s of the great vipra नारद of the clan of the काण्व-s

[इन्द्रः सुतेषु सोमेषु क्रतुम् पुनीत उक्थ्यम् ।]{style="color:#99cc00;"}\
[विदे वृधस्य दक्षसो महान् हि षः ॥]{style="color:#99cc00;"}\
indra, at the soma libations, makes himself clear to receive the chants; he is known to be of high skill because he is the great one!

[स प्रथमे व्योमनि देवानां सदने वृधः ।]{style="color:#99cc00;"}\
[सुपारः सुश्रवस्तमः सम् अप्सुजित् ॥]{style="color:#99cc00;"}\
He became mighty in the primal sky, the seat of the deva-s, all-transcending, of great fame, conquering all waters.

[तम् अह्वे वाजसातय इन्द्रम् भराय शुष्मिणम् ।]{style="color:#99cc00;"}\
[भवा नः सुम्ने अन्तमः सखा वृधे ॥]{style="color:#99cc00;"}\
Him I call for the conquest of booty, for the battle \[I call] the impetuous indra; be close to us in favorable disposition, a friend for our augmentation.

Note that in the first of the above songs the upadrava incorporates a modified version of the first pada of the प्रतिहार. In the second song the upadrava repeats of the whole प्रतिहार pada in a modified form. All these sAman-s are followed by the recitation of shastra-s by the ब्राह्मणाच्छंसिन्, which include the above ऋक्-s and several other ऋक्स् with the terminal triplications and the interspersion of the shOMsAvOM incantation. At the end of each shastra that follows the song a soma libation is made with the below mantra-s which begin with the syllable 'pro'. After the first of the above songs and the associated shastra:\
[प्रो द्रोणे हरयः कर्माग्मन् पुनानास ऋज्यन्तो अभूवन् ।]{style="color:#99cc00;"}\
[इन्द्रो नो अस्य पूर्व्यः पपीयाद् द्युक्षो मदस्य सोम्यस्य राजा ॥]{style="color:#99cc00;"}\
Forth to the droNa-kalasha the tawny stream flows for the ritual, and purified flows straight ahead. May indra, heavenly  king, as in the past, drink of this exhilarating soma.

After the second:\
[प्रोग्राम् पीतिं वृष्ण इयर्मि सत्याम् प्रयै सुतस्य हर्यश्व तुभ्यम् ।]{style="color:#99cc00;"}\
[इन्द्र धेनाभिर् इह मादयस्व धीभिर् विश्वाभिः शच्या गृणानः ॥]{style="color:#99cc00;"}\
To make you start \[for our ritual], I offer you a strong, true libation, the manly one,  whom the tawny horses carry to the \[soma] pressing. Here, O indra, be delighted in our milk-effused soma offerings, as we offer recitations skillfully crafted with all our intelligence.\
With this concludes the offerings to indra.

This is followed by the making of the special seat with bricks and cushions for the होता to the west of his fire altar. Then the सामवेदिन् ritualists sing the 9-fold rahasya गाण to agni, उष-s and the ashvinau. This followed by the recitation of 1000 mantra-s to the ashvin-s with the Ashvina offering ordained by our ancient ancestor च्यवान.


