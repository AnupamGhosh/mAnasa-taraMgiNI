
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief note on प्रह्लाद, हिरण्यकशिपु and an early नारसिंहाख्यान](https://manasataramgini.wordpress.com/2013/07/25/a-brief-note-on-prahlada-hiranyakashipu-and-an-early-narasimhakhyana/){rel="bookmark"} {#a-brief-note-on-परहलद-हरणयकशप-and-an-early-नरसहखयन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 25, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/07/25/a-brief-note-on-prahlada-hiranyakashipu-and-an-early-narasimhakhyana/ "6:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The daitya-s प्रह्राद/प्रह्लाद and virochana are known in the veda itself. The killing of प्रह्लाद and his clan by indra in a battle in the high heavens is alluded to in more than one [post-RV vaidika text](https://manasataramgini.wordpress.com/2006/12/19/knowing-indra/). This great battle between indra and प्रह्राद is also alluded to in the epics on multiple occasions:\
[जिगीषतोर् युधान्योन्यम् इन्द्र-प्रह्लादयोर् इव ।]{style="color: #0000ff"} (महाभरत 3.270.12cd)

प्राह्राद is among the daitya-s killed by indra in the epics consistent with the vaidika account:\
[रथेनानेन मघवा जितवाञ् शम्बरं युधि ।]{style="color: #0000ff"}\
[नमुचिं वल-वृत्रौ च प्रह्लाद-नरकाव् अपि ॥]{style="color: #0000ff"}\
[बहूनि छ सहस्राणि प्रयुतान्य् अर्बुदानि छ ।]{style="color: #0000ff"}\
[रथेनानेन दैत्यानां जितवान् मघवान् युधि ॥]{style="color: #0000ff"} महाभरत 3.165.18-19 ("critical")

Seated on this car indra conquered shambara in battle. Seated on this chariot मघवान् conquered namuchi, vala, वृत्र, प्रह्लाद and naraka along with thousands, millions and tens of millions of daitya-s.

In the तैत्तिरीय ब्राह्मण 1.5.9-10 the daitya प्रह्राद कायाधव is said to hide his son virochana in a crater (termed a pradara) in the ground so that the deva-s might not kill him. Hence, the ब्राह्मण in its characteristic style states that one must not drink water from a crater -- implying that it might be poisoned by the presence of the daitya virochana.

प्रह्राद was an enemy of the deva-s in all early ऐतिहासिक and पौराणिक narratives consistent with the vaidika account. One पौराणिक narrative that is aligned with vaidika allusions is the सृष्टिकाण्ड of the पद्मपुराण 13.186, which mentions indra slaying प्रह्लाद during the churning of the world ocean. The battle between the gods and प्रह्लाद is described in detail the archaic स्कन्दपुराण account of the same incident, but there विष्णु is said to defeat प्रह्लाद. In the kurma-पुराण he is described as fighting विष्णु during his attack on हिरण्यकशिपु and is defeated by a puruSha emanated by विष्णु. Later he attacks नृसिंह and flees to save himself after being struck by him. In the हरिवंश he attacks the deva-s along with his grandson bali and is eventually subjugated by विष्णु. In the shantiparvan the [arrogant प्रह्लाद is defeated by कार्त्तिकेय](https://manasataramgini.wordpress.com/2005/08/07/the-fall-of-prahlada/). Only later the zealous vaiShNava-s in their drive to elevate विष्णु over indra made him a positive figure and an ideal devotee of विष्णु. In fact, this is in contrast with the view of the early vaiShNava-s who mention his defeat along with that of other daitya-s and दानव-s as an act of विष्णु, evidently alluding to the famous दानव-conquering strides of विष्णु already known in the veda:

[आसुरीं श्रियम् आहृत्य त्रींल् लोकान् स जनार्दनः ।]{style="color: #0000ff"}\
[सपुत्रदारान् असुरान् पाताले विन्यपातयत् ॥]{style="color: #0000ff"}\
[नमुचिः शम्बरश् चैव प्रह्लादश् च महामनाः ।]{style="color: #0000ff"}\
[पाद-पाताभि-निर्धूताः पाताले विनिपातिताः ॥]{style="color: #0000ff"} (an old vaiShNAva AkhyAna found in certain recensions of the bhArata)

He जनार्दन having seized the wealth of the asura-s and the three worlds pushed the asura-s along with their sons and wives down to the netherworld. The mighty namuchi, shambhara and प्रह्लाद were trampled underfoot and pushed down into the netherworld \[by विष्णु].

However, the famous father of प्रह्राद, हिरण्यकशिपु, is not mentioned in the shruti. Nevertheless, there are hints that he existed in the ancient Indo-Aryan tradition:

 1.  The early वंश-s invariably mention him as a daitya the father of प्रह्राद. E.g. the ancient daitya-सृष्टि in the महाभारत states-

[एक एव दितेः पुत्रो हिरण्यकशिपुः स्मृतः ।]{style="color: #0000ff"}\
[नाम्ना ख्यातास् तु तस्येमे पुत्राः पञ्च महात्मनः ॥]{style="color: #0000ff"}\
[प्रह्रादः पूर्वजस् तेषां संह्रादस् तदनन्तरम् ।]{style="color: #0000ff"}\
[अनुह्रादस् तृतीयो ऽभूत् तस्माच् च शिबि-बाष्कलौ ॥]{style="color: #0000ff"}\
[प्रह्रादस्य त्रयः पुत्राः ख्याताः सर्वत्र भारत ।]{style="color: #0000ff"}\
[विरोचनश् च कुम्भश् च निकुम्भश् चेति विश्रुताः ॥]{style="color: #0000ff"}\
[विरोछनस्य पुत्रो ।अभूद् बलिर् एकः प्रतापवान् ।]{style="color: #0000ff"}\
[बलेश् च प्रथितः पुत्रो बाणो नाम महासुरः ॥]{style="color: #0000ff"} महाभरत 1.59.17-20 ("critical")

diti bore one son who was known as हिरण्यकशिपु. The names of his five sons are narrated thus: प्रह्राद was the first, followed by संह्राद thereafter; the third was अनुह्राद followed by shibhi and बाष्कल. प्रह्राद's three sons O bhArata are said to be virochana, kumbha and nikumbha. virochana's had one son the valiant bali. bali's son was known by the name of bANa, the great demon.

Thus, as in the veda the eldest son of प्रह्राद is described as virochana. Further the महाभरत has the AkhyAna of sunda and upasunda, the sons of nikumbha, who are mentioned as being born in the clan of हिरण्यकशिपु ([महासुरस्यान्ववाये हिरण्यकशिपोः पुरा ।]{style="color: #0000ff"}).


 2.  In the रामायण there is an allusion the tale of indra capturing the wife of हिरण्यकशिपु. The पौरानिक narratives (e.g., bhAgavata 4.18.12) indicate that she, कयाधु (consistent with the vaidika form of प्रह्राद's matronym) was the daughter of the दानव jambha who was killed by indra. Other पौराणिक narratives suggest that indra abducted कयाधु to preclude the birth of her dreadful and mighty son प्रह्राद, but subsequently released her (in some accounts upon नारद's suggestion).


 3.  In the bhArata there are multiple mentions of the slaying of हिरण्यकशिपु by विष्णु-\
The vanaparvan has an account of the killing of the दानव-s called the कालकेय-s with draining of the "ocean" by Canopus (agastya, i.e., Alpha Carinae; cognate of Iranian Satvas). Into which the early vaiShNava-s have inserted a stuti of विष्णु (the abhaya stuti), which describes his chief exploits as the lifting of the submerged "earth"; killing of the first of the daitya-s हिरण्यकशिपु as नृसिंह, the trampling of bali as वामन, and the felling of the यज्ञ-destroying archer jambha (it is not clear if विष्णु is regarded as having killed him; because this exploit is largely attributed to indra). It is clear it is an early stuti because the list stops at jambha and does not have the structure of the later अवतार lists.\
The वैष्णवाभय-stuti goes thus:\
[त्वं नः स्रष्टा च पाता च भर्ता च जगतः प्रभो ।]{style="color: #0000ff"}\
[त्वया सृष्टम् इदं सर्वं यच् चेङ्गं यच् च नेङ्गति ॥]{style="color: #0000ff"}\
O lord, you are the emitter, the sustainer, and the destroyer of ourselves and the rest of the universe. It is you who has emitted this universe with all that is living and nonliving.

[त्वया भूमिः पुरा नष्टा समुद्रात् पुष्करेक्षण ।]{style="color: #0000ff"}\
[वाराहं रूपम् आस्थाय जगद्-अर्थे समुद्धृता ॥]{style="color: #0000ff"}\
O lotus-eyed one, it was you who had formerly for the benefit of the world raised from the sea the lost earth, having assumed the form of a boar

[आदि-दैत्यो महावीर्यो हिरण्यकशिपुस् त्वया ।]{style="color: #0000ff"}\
[नारसिंहं वपुः कृत्वा सूदितः पुरुषोत्तम ॥]{style="color: #0000ff"}\
O foremost puruSha, assuming the form of the man-lion, you had slain हिरण्यकशिपु of mighty valor, the first of the daitya-s.

[अवध्यः सर्वभूतानां बलिश् चापि महासुरः ।]{style="color: #0000ff"}\
[वामनं वपुर् आश्रित्य त्रैलोक्याद् भ्रंशितस् त्वया ॥]{style="color: #0000ff"}\
The great asura bali was indeed incapable of being slain by any one. Taking the form of a dwarf, you banished him from the triple-world.

[असुरश् च महेष्वासो जम्भ इत्य् अभिविश्रुतः ।]{style="color: #0000ff"}\
[यज्ञक्षोभकरः क्रूरस् त्वयैव विनिपातितः ॥]{style="color: #0000ff"}\
The asura jambha, was renowned as a great archer. This fierce disturber of rituals was indeed felled by you.

[एवम् आदीनि कर्माणि येषां संख्या न विद्यते]{style="color: #0000ff"}\
[अस्माकं भय-भीतानां त्वं गतिर् मधुसूदन ॥]{style="color: #0000ff"} Mbh 3.100.18-23 ("Critical")\
Former achievements of yours such as these cannot be counted. O slayer of madhu, you are the refuge for us afflicted by fear.

In the droNa parvan, the पाञ्चाल hero दृष्टद्युम्न advancing against droNa is described thus:\
[तस्य रूपं बभौ राजन् भारद्वाजं जिघांसतः ।]{style="color: #0000ff"}\
[यथा रूपं परं विष्णोर् हिरण्यकशिपोर् वधे ॥]{style="color: #0000ff"} Mbh 7.164.146 ("Critical")\
The form, O king (ध्र्^इतराष्ट्र), of his ( दृष्टद्युम्न) when he sought to slay the भरद्वाज hero was like the ancient form of विष्णु during the killing of हिरण्यकशिपु.

Again दृष्टद्युम्न is compared to नृसिंह in course of a fiery speech to arjuna when the latter was expressing deep concern over fighting अश्वथामन्:\
[ततः पाञ्चालराजस्य पुत्रः पार्थम् अथाब्रवीत् ।]{style="color: #0000ff"}\
[संक्रुद्धम् इव नर्दन्तं हिरण्यकशिपुं हरिः ॥]{style="color: #0000ff"} Mbh 7.168.21 ("Critical")\
Then the son of the पाञ्चल king, said to arjuna the following, like the lion (i.e. विष्णु) to the furious and enraged हिरण्यकशिपु.

कृष्ण tells युधिष्ठिर that he should kill duryodhana with the appropriate plots or clever plans. As an example he says हिरण्याक्ष and हिरण्यकशिपु were both killed by plots or plans:\
[क्रियाभ्य् उपायैः पूर्वं हि हिरण्याक्षो महासुरः ।]{style="color: #0000ff"}\
[हिरण्यकशिपुश् चैव क्रिययैव निषूदितौ ॥]{style="color: #0000ff"} Mbh 9.30.9 ("Critical")\
The great asura हिरण्याक्ष, as also हिरण्यकशिपु, were both slain by plots and clever plans.

In the early पाञ्चरात्रिक account in the शान्तिपर्वन् we see:\
[नारसिंहं वपुः कृत्वा हिरण्यकशिपुं पुनः ।]{style="color: #0000ff"}\
[सुरकार्ये हनिष्यामि यज्ञघ्नं दिति-नन्दनम् ॥]{style="color: #0000ff"} Mbh 12.326.73 ("Critical")\
विष्णु said: Again having taken the form of the man-lion I will slay for the sake of the gods the यज्ञ-destroying हिरण्यकशिपु.

Further, in the शान्तिपर्वन्, in the account of skanda defeating प्रह्लाद he is clearly mentioned as हिरण्यकशिपु's son. Again, in the शान्तिपर्वन् there is a remarkable prose section composed by the early नारायणीय पाञ्चरात्रिक-s in a form resembling the vaidika ब्राह्मण texts with the express purpose of presenting विष्णु as supreme and down-sizing indra and rudra. This text mentions that हिरण्यकशिपु originally had वसिष्ठ as his होतृ. He dismissed वसिष्ठ and appointed his nephew trishiras त्वाष्ट्र as his होतृ. Furious वसिष्ठ cursed him:\
[हैरण्यगर्भाच् च वसिष्ठाद् धिरण्यकशिपुः शापं प्राप्तवान् । यस्मात् त्वयान्यो वृतो होता तस्माद् असमाप्त यज्ञस् त्वम् अपूर्वात् सत्त्वजाताद् वधं प्राप्स्यसीति । तच् छापदानाद् धिरण्यकशिपुः प्राप्तवान् वधम् ।]{style="color: #0000ff"} (Mbh 12,329.19-20)\
हिराण्यकशिपु received a curse from वसिष्ठ the son of हिरण्यगर्भ. Since another person was chose as the होतृ by you, your ritual will be incomplete, and you will attain death at the from being the like of which has never existed before. And by that apparition हिरण्यकशिपु attained death. \[Subsequently, indra kills trishiras त्वाष्ट्र who was taking away the soma from the deva-s as in the vaidika account.]

This early पाञ्चरात्रिक account is remarkable in more than one way: it seems like it was actually adopted from a real ब्राह्मण text and reworked to establish the primacy of विष्णु-नारायण. It contains an unusual form of the legend, which is seen nowhere else in the later texts, i.e., the connection of हिरण्यकशिपु with trishiras and him being killed as a result of a curse of वसिष्ठ who was formerly is purohita. His killing is however attributed to an unprecedented being. This being while not being explicitly named as नृसिंह or विष्णु, there is sufficient hint that it was none other than नृसिंह. This indeed appears to be one of the earliest versions of the नृसिंह legend.

In conclusion, we may infer the following: In the रामायण while the tale of नृसिंह is not mentioned, the daitya हिरण्यकशिपु is mentioned. In the महाभरत, there are multiple clear mentions of हिरण्यकशिपु and his killing by विष्णु in an unusual form namely that of a man-lion or lion. The leonine nature of विष्णु per say has ancient roots, but it is not unique to him [as several other deities are also attributed leonine forms in the veda](https://manasataramgini.wordpress.com/2012/02/02/notes-on-vaishnava-shaiva-and-kaumara-sectarian-competition-in-the-purana-s/). Thus, it appears that the daitya हिरण्यकशिपु had an early presence (predating the epics) as a major daitya, but his importance was relatively lower than that of other daitya-s like प्रह्लाद, virochana, and bali who despite being his successors are more important in the early layer. While the नृसिंह legend was connected to the killing of हिरण्यकशिपु from the beginning it appears that this legend grew in importance only later with the manifestations of विष्णु as वराह, वामन-trivikrama and हयग्रीव being more important in the early period.

In this context we may make note of the following. Hindu tradition records two kinds of demons, namely दानव-s and daitya-s, who were later collectively called asura-s (after the mutual Indo-Iranian polarization of the terms deva and asura). The दानव-s are the sons of दानु and daitya-s those of diti, as is emphasized in the पौराणिक वंश-s. In the oldest layer of Hindu tradition (i.e., the RV) only दानु and दानव-s are mentioned. Consistent with the old वंश preserved in the Mbh, we see that all the major antagonists of the deva-s who are killed by indra assisted by other deva-s in the RV are दानव-s and not daitya-s. Following are the major दानव-s mentioned in the RV:\
दानु: mother of the दानव-s killed by indra.\
वृत्र: killed by indra assisted by marut-s sons of rudra, वायु and विष्णु\
vala: killed by indra assisted by बृहस्पति\
namuchi: killed by indra assisted by ashvin-s and सरस्वती.\
shambara: killed by indra assisted by विष्णु and agni.\
शुष्ण: killed by indra assisted by marut-s sons of rudra.\
pipru, kuyava, और्णवाभ: killed by indra.\
To this the epics add:\
naraka: slain by indra.\
jambha: slain by indra probably assisted by विष्णु.\
viprachitti: killed by indra.\
तारक: killed by कुमार.\
क्रौञ्च: killed by कुमार.\
tripura-s (sons of तारक): killed by rudra (already mentioned in YV).

In contrast, diti is mentioned only 3 times in the RV and the term daitya is wholly missing. Similarly, diti is mentioned a few times in the AV vulgate and पैप्पलाद saMhitA-s but not term daitya. However, the daitya virochana is mentioned as the son of प्रह्राद in both AV saMhitA-s. Another daitya andhaka (or in the form ardhaka) is also mentioned in the AV (and a lost kaTha text; probably its ब्राह्मण). As mentioned earlier, the daitya-s receive little more mention in the ब्राह्मण texts. Importantly, among the daitya-s fewer appear to be slain by indra (primarily the ones mentioned in earlier text) and mostly appear in later texts:\
हिरण्याक्ष: killed by विष्णु as वराह.\
हिरण्यकशिपु: killed by विष्णु as नृसिंह.\
प्रह्राद/प्रह्लाद: killed by indra.\
virochana: killed by indra.\
balin: crushed by विष्णु as trivikrama.\
andhaka (son of हिरण्याक्स): killed by rudra.\
sunda and upasunda (grandsons of प्रह्लाद): deva-s tricked them with the help of apsaras तिलोत्तमा to kill themselves.\
महिष (grandson of diti via daughter): killed by कुमार.\
bANa (son of balin): killed by कुमार (later partially appropriated by the vaiShNava-s for कृष्ण vAsudeva).

Thus, it appears that the daitya cycle was a later one developed after the original दानव cycle and विष्णु had a place of prominence as the great deva warrior this cycle. It was within this context that his legend of नृसिंह arose.

While we have nothing else left of the enigmatic prose नारायणीय account mentioned above, do we have any other early account of the नृसिंह legend? Indeed, there appears to be one which is found in some recensions of the महाभरत that never made it into the vulgate or the "Critical i.e. the Pune neo-recension". This is a standalone section that appears in the vanaparvan and describes the exploits of विष्णु in a succinct manner. Its archaic nature is suggested by the fact that it recognizes only three manifestations of विष्णु namely वराह (incipiently present in the veda), नृसिंह and वामन-trivikrama (already present in the veda). It also sets those manifestations in a distinctive context from the usual accounts:

*It first describes the end of the universe after the completion of 1000 * 4 yuga cycles; it is first burnt by the संवर्ताग्नि and the stars and planets are said to be completely destroyed. Then all there is a still fluid which is now called the एकार्णव (the one ocean). In this ocean the thousand-headed, thousand-limbed नारायण sleeps on the coils of the great serpent sheSha. This is the night of the god when the manifest universe ceases to exist. The text then gives the etymology of नारायण; नारास् is said to be the body of fluid, i.e., that of the एकार्णव. This fluid was sent into motion (ayana) by him who is called नारायण for the origination of the universe. Thus, from his nave arose a lotus. From the lotus brahmA was emitted and from him all of existence. Then विष्णु assumed the form of a gigantic boar 100 yojana-s in length and brought up from the एकार्णव the land for the living beings to live. \[Note that in this context वराह is not described as fighting हिरण्याक्ष and is placed in the context of a primal origin myth in line with the vaidika allusions to this myth.] Then he is said to assume the of नृसिंह and slay हिरण्यकशिपु. Then assuming the form of a dwarf he tricks bali into giving him the three paces of land; Growing gigantic he covers the whole universe and conquers it on behalf of indra.*

The नृसिंह narrative of this text goes thus:\
[नरस्य कृत्वार्ध-तनुं सिंहस्यार्ध-तनुं प्रभुः ।]{style="color: #0000ff"}\
[दैत्येन्द्रस्य सभां गत्वा पाणिं संस्पृश्य पाणिना ॥]{style="color: #0000ff"}\
[दैत्यानाम् आदि-पुरुषः सुरारिर् दिति-नन्दनः ।]{style="color: #0000ff"}\
[दृष्ट्वा चापूर्व-वपुषं क्रोधात् संरक्त-लोचनः ॥]{style="color: #0000ff"}\
[शूलोद्यत-करः स्रग्वी हिरण्यकशिपुस् तदा ।]{style="color: #0000ff"}\
[मेघ-स्तनित-निर्घोषो नीलाभ्र-चय-संनिभः ॥]{style="color: #0000ff"}\
[देवारिर् दितिजो वीरो नृसिंहं समुपाद्रवत् ।]{style="color: #0000ff"}\
[समुपेत्य ततस् तीक्ष्णैर् मृगेन्द्रेण बलीयसा ॥]{style="color: #0000ff"}\
[नारसिंहेन वपुषा दारितः करजैर् भृशम् ।]{style="color: #0000ff"}\
[एवं निहत्य भगवान् दैत्येन्द्रं रिपु-घातिनम् ॥]{style="color: #0000ff"}

Having assumed a body of half man and half lion the lord [विष्णु] having gone to the assembly hall of the the lord of the daitya-s touched one palm with another. The foremost person among the daitya-s, the enemy of the gods, the son of diti, having seen this unprecedented being, who full of anger with blood-shot eyes, with a raised trident in the hand, wearing a garland, thundering like a rain-cloud, gloomy as a black cloud, the enemy of the gods the son of diti, the valiant हिरण्यकशिपु assailed नृसिंह. Having seized him with his powerful, sharp claws of the chief of the animals, with the body of a man-lion, he [विष्णु] tore him apart violently with his claws. Thus, the lord slew the chief of the daitya, who was the killer of his enemies.

While this account is rather short it still conveys the chief points of the myth: 1) The sudden appearance of नृसिंह in the सभा of the demon. 2) The gesture of touching his palms seems to indicate that he would not kill him with his palms, or a weapon held by his palms. 3) It is emphasized that the form was something unprecedented, suggesting that हिरण्यकशिपु had immunity against regular forms. 4) He is described as holding a trident in his hand -- even in this early version of the narrative the *[convergence between rudra and नृसिंह](https://manasataramgini.wordpress.com/2012/02/02/notes-on-vaishnava-shaiva-and-kaumara-sectarian-competition-in-the-purana-s/)* had already emerged.

[ॐ नारसिंह-वपुषे श्रीमते नमः ॥]{style="color: #0000ff"}


