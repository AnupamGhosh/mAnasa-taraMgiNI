
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The terror of the ergative and such thoughts](https://manasataramgini.wordpress.com/2013/08/27/the-terror-of-the-ergative-and-such-thoughts/){rel="bookmark"} {#the-terror-of-the-ergative-and-such-thoughts .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 27, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/08/27/the-terror-of-the-ergative-and-such-thoughts/ "3:43 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In our youth we were confronted with the prospect of learning two New Indo-Aryan languages, namely Hindi and Marathi. All around us were native speakers of the latter language. Several of them were multilingual and knew Hindi to different degrees and other languages such as English or Sanskrit. But except for the well-educated most had learned their languages instinctively. Not surprisingly, the teachers who were supposed to teach us these NIA languages felt that there was really no need for teaching grammar -- it was something you acquired naturally, they claimed, by just reading the pieces in the textbooks. The lessons started with the nAgarI script. We were king when this began because we already knew this script by way of the देवभाषा. Then there was vocabulary. This too we fathomed due to the generic भारतीय vocabulary being relatively similar across tongues. However, we soon realized that the going was not easy because, despite what the teachers claimed, we really had to understand the grammatical logic of these languages if we were to construct real sentences in them. So we began our own grammatical analysis of these languages, which was fraught with much tentativeness, in order to gain control over them. We soon realized that the verbs took endings that were determined by the gender, number and honorific state of the agent. It then dawned on us as to why our चाचाजी-admiring teacher had torn into our essay for writing sentences such as: [नेहरूजी बोला: हम् स्वतन्त्र हून् ।]{style="color:#008080;"} rather than: [नेहरूजी बोले: हम् स्वतन्त्र होन्गे!]{style="color:#008080;"} However, the going soon got worse rather than better as we hit the stone wall of the ergative in Hindi and Marathi. It was as though we were the Inca troops of Atahualpa being blasted out of existence by the म्लेच्छ-s in the battle of Cajamarca. We had pulled the plug badly enough that we were facing a premature existential crisis in our educational career. At this point our mother interceded and asked us to comparatively reanalyze the grammar of these species of NIA based on a book she gave us. The book had an interesting sounding term called the "ergative", which meant that the verb ending took the gender/number/honorific state of the object rather than the agent. Thus, we realized that: [नेहरूजी ने छिट्टी लिखे थे ।]{style="color:#008080;"} was wrong and it was[नेहरूजी ने छिट्टी लिखी थी ।]{style="color:#008080;"} Thus, we plodded along and soon excavated further complications such as the "ko" modifier, the use of "se" in the instrumental and the ablative senses and why Marathi formed sentences such as: [तु त्याला किति पैसे दिलेस् ।]{style="color:#008080;"} etc. Armed with these new realizations we eventually gained a nominal understanding of NIA; at least enough for us to pass in Hindi in what was termed the First Class. Ironically, we fancied ourselves as being about as English as the English themselves, but did worse in it in the final test than in Hindi! This left us feeling good about our understanding of NIA, but it was not without it being seared into our cerebral recesses so as to return periodically in the form of haunting dreams of sitting in the Hindi examination hall! We had wondered then as to why Hindi and Marathi had the ergative but did not have even an inkling of an answer. We asked कौण्डिन्या, who claimed Hindi to be like her mother-tongue, but she had no explanation either. But my exposition of the bizarreness of the ergative stuck somewhere in her head.

Many many years later, कौण्डिन्या told us that she was soaking herself in elegant Hindi prose and much enjoying it, as though it was the scented rice flavored with the pickle of Sanskrit. She asked us to read some of the pieces she was reading, some Rahula Samkrityayana or HP Dvivedi or some author I forgot on हरिसिंह नाल्वा who took the war against the मरून्मत्त-s right into Afghanistan. However, unlike her, we came off worse from the readings in Hindi because that very night we woke up in sweat from a dream of a dreadful Hindi examination in which we had regressed to the state prior to our realization of the ergative -- we did not sleep for the rest of the night. No more Hindi literature, we told ourselves. So when कौण्डिन्या asked us how we felt about her recommended readings we confessed our predicament. At this point she asked us: "How do you think the ergative came into existence when संस्कृत does not have it?" Suddenly, we had a flash of insight. We realized that it after all did evolve in a proximal sense from संस्कृत's instrumental-coupled past participle (कृदन्त formations).

In classical Sanskrit a common use of the past participle is as an adjective:\
[मया भुक्तं भोजनं रुछिरम् आसीत् ॥]{style="color:#0000ff;"}\
The food eaten by me was tasty.\
[मया दृष्टा लूतिका उत्तमाऽस्ति ॥]{style="color:#0000ff;"}\
The spider seen by me is splendid.

Note that in these constructions the agent is in the instrumental case, while the past participle is being used as an adjective for the object. Being an adjective in Sanskrit it takes the endings as per the gender, number and case of the noun it describes. This use is clearly in the passive sense as the so called object of the sentence is in the nominative case. One can also use some past participles in an active sense:\
[चषकं गृहीत्वा रामो ऽपि मैरेयम् पीतः* ॥]{style="color:#0000ff;"}\
Having taken the chalice rAma then drank the maireya.

\*some have questioned whether the kta कृदन्त of class 1 verb pA can be used as active here, as is allowed by default for verbs of motion or those with upasarga-s like [[अधि]]{style="color:#ff0000;"}ष्ठित

Thus, in this active form the past participle behaves has though it is an adjective for the agent (rAma in this sentence). The same sense is also conveyed in the manner of the earlier sentences if the participle is used in a passive sense. i.e.\
[रामेणापि मैरेयम् पीतं ॥]{style="color:#0000ff;"}\
Here again the past participle takes the form of an adjective for maireya, a neuter singular noun in the nominative. Further it can also be used to express future perfect formations like:

[रामेण मैरेयम् पीतम् भविष्यति ॥]{style="color:#0000ff;"}

Such past participle constructions form part of a well-known riddle:\
[हतो हनूमतारामः सीता हर्षम् उपागता ।]{style="color:#0000ff;"}\
[रुदन्ति राक्षसाः सर्वे हा हा रामो हतो हतः ॥]{style="color:#0000ff;"}

A naïve reader would be puzzled by this because it would read contrary to what is known:\
rAma was killed by hanumAn; sItA attained delight; all the rAkShasa-s are crying: Ha! Ha! rAma was killed, was killed!\
Note the use of the past participles here: हतः | रामः| हनूमता || Here हतः is used in the passive sense with rAma in the nominative and हनूमत् in the instrumental with हतः taking the ending after rAma. sItA | हर्षम् | उपागता || Here the past participle is उपागता used in the active sense hence it takes the ending after sItA, with हर्षम् (masculine singular) becoming accusative to denote its object-hood. In the final foot of the shloka we have: रामो हतः || (duplicated for effect). Here rAma is in the nominative with हतः taking the same ending but there is no agent specified. This type of usage where the agent has been omitted (being obvious) is similar to the equivalent passive construction in a language like English. Finally the answer to the puzzle lies in writing it as:

[हतो हनूमता।आरामः सीता हर्षम् उपागता ।]{style="color:#0000ff;"}\
[रुदन्ति राक्षसाः सर्वे हा हा ।आरामो हतो हतः ॥]{style="color:#0000ff;"}\
hanUman destroyed the grove (आरामः, masculine singular, i.e. ashokavana), sItA was delighted; All the rAkShasa-s are crying: Ha! Ha! The grove was destroyed! Was destroyed!

Now coming to NIA languages like Hindi we can construct a sentence like:\
[दार्विन् ने पत्रिका लिखी थी ।]{style="color:#008080;"}\
Darwin had written a paper.\
The equivalent in the देववानी would be:\
[दार्विणा पत्रिका लिखिता ॥]{style="color:#99cc00;"}\
A paper was written by Darwin.

It is the latter construct that gave birth to the ergative in several NIA languages such as Hindi, Marathi, and several others. The conservation of the "ne" particle across Hindi, Marathi, Punjabi, Marwadi and the like showed that it was in all likelihood derived from the instrumental ending of the agent coupled to the passive participle in Sanskrit, even though in NIA it is used in the active sense. This realization for us was like discovering a new homology among proteins, with the accompanying realization of the evolutionary path and uncovering of biological functions. Of course many others have done that before us, and reams have evidently been written on the IA ergative. However, our self-discovery of this was for us a triumph over the trauma of the childhood trauma of the ergative.

Now why this kind of formation has emerged in the first place? The Sanskrit construction is clearly logical and non-ergative passive but in the NIA languages its derivative is ergative. This is no longer passive and it does not correspond to the original active forms seen in Sanskrit which were described above. Rather via the ergative it imitates the Sanskrit use of the participle in an adjectival sense with respect to the agent's "object". The ergative is rare in Indo-European and unlikely to have been there in Proto-Indo-European (PIE). It is also absent to our knowledge in Dravidian which is the other major language family of India. So it was not acquired by contact with Dravidian. In IE the ergative is primarily seen in the more vulgar dialects of Indo-Aryan and Iranian (where it is much less common). However, it should be noted that it is present in several Caucasian languages like Georgian, Circassian and Chechen. It is also present in the mysterious isolate language Burushaski from the Northwest of जंदुद्वीप and apparently in several versions of Tibetan. We suspect that the original Indus languages also used the ergative. Thus, it was probably present among the languages close to the original Indo-Iranian homeland and also on the path of the Indo-Aryans and the Iranians (at least part of them) to their later lands of settlement. This trail of ergative users probably influenced the Indo-Aryan and Iranian languages on the way to their final destinations. In particular, in India the influence appears to have resulted in Sanskrit first evolving a passive past participle formation that could mirror the ergative without compromising the ancestral non-ergative IE formations. This constraint was possibly strong in the elite version of the language (i.e. Sanskrit; also Avestan on the Iranian side). This was probably less so in the more vulgar forms of Middle Indo-Aryan leading to ergativity creeping into Indo-Aryan in its full-fledged form albeit via formations inherited from Sanskrit. Interestingly, Sumerian and Hurrian also apparently display some ergativity, but its absence in PIE argues against the "Anatolian theory" of PIE, which in any case has several serious problems.


