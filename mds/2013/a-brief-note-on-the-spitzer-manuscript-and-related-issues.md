
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief note on the Spitzer manuscript and related issues](https://manasataramgini.wordpress.com/2013/05/01/a-brief-note-on-the-spitzer-manuscript-and-related-issues/){rel="bookmark"} {#a-brief-note-on-the-spitzer-manuscript-and-related-issues .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 1, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/05/01/a-brief-note-on-the-spitzer-manuscript-and-related-issues/ "6:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In his famed काव्य the buddhacharita, अश्वगोष states that when the तथागत was about to renounce the world he had an "अक्षय-dharma-जात-रागः", i.e., a "passion" of the indestructible dharma was born in him. When we encountered this term we realized that it was a nAstika's version of the term सनातन-dharma. Thus, अश्वगोष is continuing the trend in the bauddha-mata, seen right from its founder, of repositioning the realizations of the तथागत as the real सनातन-dharma as opposed to the shruti and its spirit transmitted by the इतिहास-s and the darshana-शास्त्र-s. Thus, despite all his urge to separate himself from the सनातन-dharma, the bauddha needed it to provide an essential background for the very existence of his mata. Even the तथागत, who seems to have seen his teachings as an absolute supersession of the सनातन-dharma, needed to place himself in the context of its darshana-s. Thus, the bypassing of the yoga master आराड कालाम and the shrauta ritualist उरुबिल्वा जटिल kAshyapa are important events in his narrative. Even अश्वगोष in his काव्य tries to subtly hint this supersession by stating that siddhArtha's father shuddhodhana was a great kShatriya soma ritualist in the manner of father manu the lawgiver, and pointing out that the buddha being raised in such a household was familiar with all that. Now the constant need for the Astika backdrop among the ताथागत-s is something that has been misunderstood by several modern students of history, in part due to the influence of the white indologists and their fellow travelers. The primary misunderstanding is to interpret this background of सनातन-dharma as being an integral bauddha innovation that has later been adopted by the Astika-s. For example, several have called अश्वघोष as the originator of "real" काव्य literature. Some (e.g. Winternitz) have gone even as far as to claim that अश्वघोष was the original exponent of काव्य and even वाल्मीकि borrowed from him! We also heard a claim that अश्वघोष preceded the महाभारत as we know it. Of course there is much to suggest that none of these claims are consistent with the available facts. First, there is ample evidence from अश्वघोष himself that he considered वाल्मीकि the founder of काव्य (i.e. काव्य in the later sense, as distinct from the ऋक् mantra-s of the earlier vaidika kavI-s and their Iranian counterparts). Second, the whole of the background of अश्वघोष's buddhacharita as well as saundara-nanda is an Astika one, transparently borrowed from Astika contexts including the इतिहास-s. He knows of the रामायण, the महाभारत and पौराणिक tales like the burning of काम by rudra and the birth of कुमार and uses them for similes to describe the buddha's life. It was in this context that Spitzer manuscript was brought up to present a more subtle argument that the महाभरत was not yet in its current parvan structure at the time of अश्वघोष. While the Spitzer manuscript is useless to support this claim, it is still of enormous interest in terms of understanding the state of the indosphere in the first two centuries of the common era and the also the general framework of the Astika background within which the nAstika tradition placed itself at that time.

The Spitzer manuscript is a highly fragmented Sanskrit manuscript that was discovered in Qizil (Eastern Turkestan; today Han-occupied Xinjiang) during the German expedition of 1906 (3rd of the Turfan expeditions). Its exact date is not known, but is believed to be between the 100-300 CE. It appears to have been written with a broad-nib copper pen in brAhmI script following a style similar to that seen in the time of the कुशान-s, which forms the basis for the above temporal window. A very difficult to find version has been published by Eli Franco and some additional material in the form of fragments known only as copies has been studied by Japanese researchers. The contents of the manuscript are a rather diverse mixture of Astika and nAstika issues that are not entirely clear due to the state of the manuscript. However, it can be said this manuscript is rather unique in that there is no parallel text that has been found to date. The account of the manuscript as edited by Franco is presented below with a few comments.

  -  avidyA-लक्षण; गोदान vastra-dAna, criticism of the घृहस्ताश्रम, ब्राह्मण-s. None of this is clear as the pages are rather fragmentary.

  -  An account of AjIvika theories such as dharma and adharma having no consequence.

  -  Some account of sukha, dukha, death, bandha and mokSha, etc.

  -  An account of the properties (लक्षण-s) of the primary substances teja, वायु, Apa etc. A detailed account of वैशेषिक theory of guNa-s, probably statement of a पूर्वपक्ष for a nAstika (?)

  -  The four Arya सत्यानि of the buddha and the concept of नैरात्म्य.

  -  Some account of principles of logical inference and argument.

  -  saMkSipta रामायण- a summary of वाल्मीकि's epic. A parvan summary of the महाभारत. It should be noted that this is fragmentary with the so claimed missing विराटपर्वन् being a lacuna in the manuscript with some name starting in 'a' or 'A', which might have read अज्ञातपर्वन् -- effectively the same as the विराटपर्वन्. The missing अनुशासनपर्वन् cannot be confirmed as being really missing or: 1) poor preservation; 2) some Mbh manuscripts outside India, like Indonesia, combine the शान्तिपर्वन् and the अनुशासनपर्वन्; simply accidental or ignorant omission by the author. In conclusion, the evidence is just tenuous to insist that this fragmentary parvan list from a unique manuscript from उत्तरापथ (Central Asia) represented the state of the Mbh as was known elsewhere in जम्बुद्वीप at that age. Franco also places a fragment of the text regarding the origin of daitya-s and दानव-s, a legal procedure, an account of the gandharva veda, the चतुः षष्टि कला-s, वेदाङ्ग-s, and the duties of each वर्ण in this part of the text.

  -  Brief account of उपनिषद्-s, mantra-s and ब्राह्मण injunctions. The concepts of adhidaiva and अध्यात्मन्.

  -  Brief account of taxonomy of living beings.

  -  The claim that the buddha knew all of the veda, the वेदाङ्ग-s, astronomy, dance and music. Arguments \[possibly of an Astika] as to why the buddha could not have been all-knowing.

  -  The buddha as an authoritative teacher, the merits of building stUpa-s, the evils of dishonest actions, destruction of desire by knowledge, a meditation on the bodily processes to end desire, mokSha, use of garlic vis-a-vis ब्राह्मण-s and shaka-s.

  -  Nature of saMsAra, a refutation of Ishvara concept, law of conservation of matter and the beginningless nature of saMsAra.

  -  An attack on the bauddha-mata \[Arguments of मीमांसक-s]: The buddha's teaching is not प्रमाण because he used प्राकृत, examples of vulgar प्राकृत

  -  Debate regarding whether compassion is dharma because it involves attachment to the object of compassion.

  -  sharabha and other animals.

  -  Existence of past and future dharma-s in addition to those of the present -- bauddha theory of सर्वास्तिवाद, which was popular in उत्तरापथ and among the chIna-s.

  -  Discourse on how the Arya सत्यानि of the buddha can be understood -- by a gradual process or in a sudden revelation. The text explains that it is a gradual process.

  -  An attack on the Astika theory of the "self-luminescent" consciousness.

  -  The तथागत's place in the संघ and the obscure question of whether making a donation to the संघ is a donation to the buddha.

  -  The concepts of samyag-buddhi and मिथ्या-buddhi -- correct and wrong cognition.

  -  An attack on the काश्यपीय theory of the action continuing to exist until it bears fruit.

  -  Lengthy philosophical considerations and debates between ताथागत-s and नैयायिक-s, मीमांसक-s and सांख्यवादिन्-s.

What the text illustrates is the degree to which the nAstika-s needed to place themselves in the context of the Astika-s. The darshana-bheda is well-know as it goes back to the tradition of the तथागत himself trying to refute the darshana-s of other contemporary thinkers. But there is also a second stream -- that of taking up and transmitting Astika knowledge regarding their texts and traditions. This is distinct from the parallel nAstika tendency (both among bauddha-s and jaina-s) of producing "fake" versions of the Astika traditions like the महाभारत, रामायण and पुराण-s in order to present their doctrines as being superior to those of the Astika-s. Rather, the tendency which we note in this text is the faithful acquisition of Astika knowledge, perhaps as part of illustrating the omniscience of the buddha and also thereby attempting to show all Astika knowledge as a mere subset of nAstika knowledge. In historical terms, this appears to have accompanied to conversion of various ब्राह्मण-s to the नास्तिकमत (like अश्वघोष himself). Thus, we should see काव्य as being one of these appropriations of the "कला-s" for the omniscient buddha, even as काव्य-composing ब्राह्मण-s started falling for the seductions of the ताथगत -mata. Thus, the urge to create a life of the buddha in काव्य is more naturally seen as an imitation of the preexisting काव्य-s that Astika-s had starting from वाल्मीकि whom अश्वघोष as a ब्राह्मण still salutes as the first kavI.

In fact, we see more of a link between the Spitzer manuscript and अश्वघोष: 1) There is evidence that copies of the buddha-charita, saundara-nanda and this bauddha play on the ब्राह्मण student of the buddha, shari-putra, were widely distributed in Central Asia. 2) Tocharian plays have been recovered from the lost Indic civilizational centers of the Central Asian oases like Kucha, Agni and the like, which were destroyed by the invasions of Han imperialists and their Turkic ally Arshina Shuoel. The plays are Tocharian versions of Sanskrit originals that point to the transmission of काव्य to central along with the bauddha-dharma. This, reinforces the idea that certain nAstika-s of this period like अश्वघोष saw it as important part of their tradition to create काव्य with bauddha themes but mirroring the astika originals, which is where a bulk of their metaphors and similes come from. 3) अश्वघोष shows a wide knowledge of the Astika इतिहास-पुराण and also strongly attacks संख्य, वैशेषिक and मीमंस and vaidika traditions. Moreover, he specifically goes into a refutation of ईश्वरवाद (all of these through the mouth of maitreya who will come as the buddha of a future age) and show sevidence for specific knowledge of the उपनिषद्-s. These points covered by अश्वघोष are key points in the Spitzer manuscript all together with an account of the इतिहास-s. Thus, we suspect that this manuscript represents some one belonging to the same tradition of अश्वघोष. There is much debate as to whether अश्वघोष was a सर्वस्तिवादिन् or belonged to one of the many proliferating schools of nAstika thought from that period. But we feel this does not detract from the idea of a link between his school and the manuscript as they share some basic similarities which go beyond the general पूर्वपक्ष of Astika sources.

Finally, the case of the Spitzer manuscript illustrates how many ancient texts of our larger tradition might have been lost. In some measure this is due to climatic conditions in much of the sub-continent not being very conducive for manuscript survival. The drier and cooler climes of Central Asia are simply better for survival of manuscripts. Hence, the spread of Indian traditions to central Asia along with the creation of the greater India and indosphere helped in the survival of at least some of these traditions. However, the case of Nepal and South India where texts have survived to a much greater extant bring home the bigger problem, which many like to deny, namely Islam. Destruction of Hindu textual traditions by the wave after wave of depredations by the army of Islam, left Hindus with only a part of what they originally possessed -- in fact over large swaths of northern India deeper Hindu traditions have simply become extinct. Even what survives would have gone had not certain Hindu rulers and officials in the courts of lapsed and active Islamic tyrants (often despite the destruction all around them unlike what white Indologists and secularists would tell us) had not taken steps to preserve what they could. In central Asia the destruction of traditions by the same army of Islam (the buddha-busters) brought a closure to the transmission of traditions to those regions. This points the importance of the need to spread the indosphere so that the traditions might survive even if bhArata falls.


