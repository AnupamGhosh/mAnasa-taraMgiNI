
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on the dispersion of the कौमार-शासन](https://manasataramgini.wordpress.com/2013/01/12/kumara-griha-s-of-the-madhyaugha-and-dispersion-of-the-kaumara-shasana/){rel="bookmark"} {#some-notes-on-the-dispersion-of-the-कमर-शसन .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 12, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/01/12/kumara-griha-s-of-the-madhyaugha-and-dispersion-of-the-kaumara-shasana/ "8:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the early 1900s the English archaeologist John Marshall began excavations at Bhita (pronounced: भीटा) near प्रयाग, modern Uttar Pradesh. He wrote an account of this excavation aided by the native assistants in the archaeological survey of India's annual report of 1911-12. In that account he describes and pictures a baked clay seal which was discovered during the excavation. It bears the writing in brAhmI:\
"[श्री विन्ध्या-वेध-महाराजस्य महेश्वर-महासेन अतिसृष्ठ-राज्यस्य वृषध्वजस्य गौतमीपुत्रस्य]{style="color:#99cc00;"}"

This is taken to mean: "\[Seal] belonging to the great विन्ध्या penetrating king, who donated his kingdom to the great god महासेन \[or whose kingdom was created by the great god महासेन], वृषध्वज the son of गौतमी"

Similar phrases are seen in [two inscriptions](https://manasataramgini.wordpress.com/2005/10/28/royal-kumara-worshippers/) very far away from प्रयाग. The first of these comes from the inscription of नाल arthapati भट्टारक found at Kesaribeda, Koraput district modern Orissa from the 7th year of the reign of the said ruler. The second, bearing the same phrase is from the inscription of the नाल mahArAja bhavadattavarman from Rithapur in Amaravati district of Vidarbha (modern महाराष्ट्र).: "[महेश्वर-महासेन अतिसृष्ठ-राज्य विभव...]{style="color:#99cc00;"}"

In his study of the Bhita seal Marshall noted that the characters were similar to those found far away from प्रयाग at जग्गय्यापेट (Krishna district, modern Andhra Pradesh) in the context of a ruined bauddha chaitya. Since the जग्गय्यापेट shrine is associated with 3 distinct flavors of the brAhmI script, belonging respectively to the mauryan, इक्ष्वाकु/kekaya and another from a latter dynasty. The characters Marshall is talking about are those from the इक्ष्वाकु/kekaya period which as we have seen before was a [high point of कौमार worship in the andhra country](https://manasataramgini.wordpress.com/2007/03/14/the-southern-kekaya-s/). Marshall was hence inclined to date the seal as being contemporaneous with इक्ष्वाकु-s, i.e. from the 200-300s of he common era. At the same time he noted that this seal, which "excels any object of this class which has yet been found in India" might be connected to the andhra-s because they bear matronymics similar to those of the rAjA to whom the seal belonged (e.g. गौतमीपुत्र श्री-शातकर्णि. In the same excavation at Bhita he also uncovered more seals that belong to kings of this potential dynasty. These include those bearing the names of गौतमीपुत्र श्री shivamegha (two copies) and वासिष्ठीपुत्र श्री bhimasena. Their andhran connection is strongly supported by the "pile of ball" or "chaitya" symbol along the with sun and moon signs, which are characteristic features of the andhra coins, on all these seals. The former andhra symbol is a motif retained since the days of the Indus valley. Another interesting set of finds from this excavation was a collection well-made stone balls ranging in diameter from 3.4-21.6 cm (which would approximately correspond to 1-6 अङ्गुल-s) to be used by ballistas of various sizes. We have some accounts that ballistas were used by the maurya-s against the Macedonian invaders (as per Greek accounts also supported by the अर्थशास्त्र). So these could belong that period or they could have be those of the andhra-s used in their invasion of the north. We suspect that this गौतमीपुत्र वृषध्वज penetrated the विन्ध्या-s from the south leading an Andhran conquest of the north, which is faintly remembered in the Indian folk history as शतवाहन's conquest of vikrama.This might also explain how the essentially south Indian dynasty of the andhra-s entered the pan-Indian dynastic tradition as a successor state of the काण्व-s with a northern center of influence.

In this context is of interest to note that in the terminal part of the andhra list in the पुराण-s we tend encounter theophorous names with "skanda": shiva-skanda शातकर्णि; skanda शातकर्णि; skanda-स्वाति. The reality of such theophorous names is supported by multiple lines of archaeological evidence: 1) Coins belonging to an andhra ruler वासिष्ठीपुत्र श्री skanda शातकर्णि have been found in the (i) वाटेगाओन् hoard, वाळ्वा tAlukA, Sangli, Maharashtra; (ii) the तर्हाळ hoard from मन्ग्रुळ् tAlukA, Akola, vidarbha region of Maharashtra; (iii) ब्रह्मपुरी hoard near Kolhapur, Maharashtra. This indicates that the पुराण-s were recording at least one real theophorous name with skanda among शातवाहन-s. Also some of these coins bear the same symbols as seen in the Bhita seals. 2) Among the late andhra inscriptions we encounter a ruler named as skanda nAga शातवाहन (kanheri near Mumbai) or बनवासे in Karnataka as shiva-skanda नागश्री. These observations suggest that the worship of कुमार was probably a direct link between the andhra-s and those northern rulers.

However, there are a few issues that suggest that the matter of the dynastic identity of these kings is not entirely simple. The exact names of these kings as seen on the Bhita seals do not figure in any traditional पुराण in the andhra list. So we are not sure if they were simply forgotten kings or clansmen of the southern andhra-s who founded their own dynasty after settling in the north (the पुराण-s mention a line known as आन्ध्रभृत्य who appear to have been some kind of branch of the andhra-s, which may have included rulers like skanda nAga शातवाहन of the Kanheri inscription). On the other hand, we also have some पुराण-s mention a dynasty called the megha that ruled a koshala (i.e. with ayodhya as center of power):\
[कोशलायां तु राजानो भविष्यन्ति महाबलाः ।]{style="color:#99cc00;"}\
[मेघा इति समाख्याताः बुद्धिमन्तो नवैव तु ॥]{style="color:#99cc00;"}

Some have suggested, based on the name shivamegha, that this dynasty might actually correspond to that of the 9 megha-s who ruled before the gupta-s. The relative temporal position of these rulers in not inconsistent with that of the above kings. However, there is nothing solid to support this view as the पुराण-s do not give the names of these rulers.

Despite the many lacunae in our understanding of Hindu history, what does emerge from these observations is that there were connections between the southern, northern and Orissan rulers encompassing the andhra, इक्षवाकु, नाल and the dynasty of गौतमीपुत्र वृषध्वज, in the form of the कौमार शाशन, particular brAhmI fonts, royal symbols on coins and seals and the form names. This web of connections linking geographically proximal and distant sites was central to the pre-gupta dispersion of the कौमार शासन over the subcontinent. This spread of the कौमार शासन, which was already underway by the time of the terminal शतवाहन kings continued through their successor states in the south and east (इक्षवाकु/kekaya and नाल) as also the gupta-s. This period was when all the ogha-s of the कौमार शासन corresponding to the sacred geography of bhArata were all simultaneously active at their greatest vigor. The ground work for this was probably already in place due to the movements of the वैखानस-associated ब्राह्मण-s who worshiped both विष्णु and कुमार.


