
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The crows and the parasitic koel: a kavi's take](https://manasataramgini.wordpress.com/2013/08/13/the-crows-and-the-parasitic-koel-a-kavis-take/){rel="bookmark"} {#the-crows-and-the-parasitic-koel-a-kavis-take .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 13, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/08/13/the-crows-and-the-parasitic-koel-a-kavis-take/ "7:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In संस्कृत literature we have come across multiple references to the brood parasitic behavior of the koel (Eudynamys). One good verse in this regard is that by the kavi वल्लण. We do not know much of him other than that he was in the court of the kalchuri king लक्ष्मी-कर्ण (1041-1072 CE) and was probably not very happy with him as a patron. What we know of his religious proclivities are interesting and reflective of a certain strain of Hindus in the final centuries of the ताथागत-mata in India. Given that he composed stuti-s to rudra, the buddha and the nAstika deity मञ्जुश्री as worshiped in the मञ्जुश्रीय मूलकल्प, he was clearly eclectic, reminiscent of the admixture witnessed in the late तान्त्रिक traditions such as the Astika श्रीविद्यार्णव and the nAstika कालचक्र. There is a subtle play of dhvani to achieve an epic ring in whatever survives of वल्लण. This particular piece is given by DD Kosambi from a collection preserved by a bauddha Acharya विद्याकार, which was taken to Tibet even as नालन्द was going up in flames during the attack of Ikhtyaruddin Bhaktiyaruddin Khalji:\
[yan नीड-prabhavo yad अञ्जन-ruchir yat khecharo yad dvijas]{style="color:#99cc00;"}\
[तेन त्वं स्वजनः किलेति करटैर् यत् तैर् उपब्रूयसे ।]{style="color:#99cc00;"}\
[तत्र-अति-इन्द्रिय-मोदिम-अंसल-रसोद्गारस् तवैष ध्वनिर्]{style="color:#99cc00;"}\
[दोषो ऽभूत् कलकण्ठ-नायक निजस् तेषां स्वभावो हि स ॥]{style="color:#99cc00;"}

As you emerged from their nest (निड), as you are of black color (अञ्जन-ruchi), as you fly in the sky (khechara), as you are twice-born (i.e. once from the mother once and then from the egg; द्विजः), you are said to be (उपब्रूयसे) one of their own (svajana) by the crows (करट). But then your sensual (अंसल), sentiment-emitting (रसोद्गार) voice (dhvani) which delights (modima) the senses beyond description (ati-indriya), they saw as a fault (दोष) as it was your own nature (स्वभाव), O male koel.

Only the black-colored male koel makes the melodious song alluded to here. The spotted females make a shrill, repeated call. This naturalistic characterization of the koel may be compared to similar sketches by other Hindu poets:\
1) [The verse of भवभूति](https://manasataramgini.wordpress.com/2011/11/02/bhavabhutis-avifauna-and-flora/)

 2.  The kingfisher by [वाक्पतिराज](https://manasataramgini.wordpress.com/2010/07/30/the-kingfisher/)


