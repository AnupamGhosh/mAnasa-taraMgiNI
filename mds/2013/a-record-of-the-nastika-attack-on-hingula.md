
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A record of the nAstika attack on हिङ्गुला](https://manasataramgini.wordpress.com/2013/05/13/a-record-of-the-nastika-attack-on-hingula/){rel="bookmark"} {#a-record-of-the-nastika-attack-on-हङगल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 13, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/05/13/a-record-of-the-nastika-attack-on-hingula/ "4:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Hindu historical tradition holds that bauddha-s of the sindhu had made common cause with the the Arab मरून्मत्त-s as they launched invasion after invasion into जम्बुद्वीप. This is not entirely surprising given that the bauddha-s (at least the स्थविरवादिन्-s) from the sindhu (along with those of श्रीलन्का) had show a notable iconoclastic streak before they themselves fell prey to the buddha-busting beards. We have accounts that they burned वज्रयाण tantra-s and demolished images of heruka-s such as hevajra at gaya. So it is hardly surprising that they made common cause with the मरून्मत्त-s blissfully unaware that the fate that they hoped for the Astika-s was to soon fall upon them, not just in the sindhu but throughout जंबुद्वीप. We also have some evidence for earlier iconoclastic activity on their part even though this is narrated in a perhaps anachronistic fashion in the bauddha chronicles such as the history of the lAma तारनाथ and chIna chronicles of the major संघकार-s.

Legend holds that upon the death of सती, rudra was disconsolate and wandered carrying her calcined body. To relieve rudra of his sorrow विष्णु hurled his chakra and cut the body of सती into fifty one pieces that fell all over भारतवर्ष defining its sacred geography by founding the sites of the shaktI पीठ-s. Her brain is said to have fallen to the western boundary of भारतवर्ष and founded the पीठ of हिङ्गुला. It lies in the Makran desert (today in the Balochistan province of TSP) and is associated with several mud volcanoes. The nAstika-s narrate an account of how this shrine was "subjugated" by a ताथागत:

*"The शाक्यसिंह had prophesied to his cousin Ananda that 300 years after his death (परिनिर्वाण) a great bauddha would emerge who would spread their शासन all over India. In a city called भृगुकच्छ in Gujarat there was a chieftain called darshana belonging to the kShatriya clan of the पाण्डु-s. He had a son called sudarshana who was richly endowed in wealth and had a retinue of beautiful women. One day while going to his garden with his ladies he saw an bauddha arihant known as शुखायन. He bowed to the arihant who offered him a lecture on the bauddha-mata. sudarshana underwent an instant conversion and sought to renounce the world and become an arihant himself. He then sought his father's permission who instead bound him in chains. Then sudarshana showed miracles like levitation, emitting light and the like and his father acceded to him becoming an arihant. Thereafter he also converted his father to the bauddha way. Eventually, he became the supreme leader of the bauddha संघ and acquired a great following among all the four वर्ण-s. In the sindhu lived a mighty यक्षिणी हिङ्गुलाची with great powers of mAyA. She caused epidemics in various lands and when people tried to flee those lands she assumed dreadful forms and blocked their paths. They used to offer her food drawn in a large cart by six oxen and also a man, a woman and horse. Then sudarshana decided to crush her. So he took alms of cooked food from a village in the sindhu and went to the temple of हिङ्गुलाची and began eating it. He then desecrated her place by washing his mouth there. Furious, she showered stones and weapons at sudarshana. But he being an arihant meditated in undisturbed on compassion as these showered on him and his followers. Then using his own miracle powers he set fire to the place. Burnt by it हिङ्गुलाची became afraid and sought refuge in the arihant. He preached the bauddha-mata to her and converted her to the the bauddha way. Since her conversion no flesh or blood has ever been offered to her. Realizing that after him there would be none to crush the यक्ष-s and nAga-s who did not follow the bauddha-mata he went on a mission of subduing 500 of such and established vihAra-s for the संघ to propagate the buddha's way. Then he went to the chIna-s and spread the bauddha-mata among them to an extant."*

What is interesting in this account is that the description of the हिङ्गुला पीठ is fairly specific: 1) It is described as being in the west of भारतवर्ष in the sindhu province. 2) The custom of offering a large amount of food is mention -- a practice which continued to a degree until recently despite the heavy hand of Islam over the region. Offerings were made in large amounts and carried to the mud volcano where they were offered directly into the geothermal vent. 3) The hurling of stones and weapons by हिङ्गुला is probably an allusion to the volcanic activity of the site. The event is supposed to have taken place at the time of ashoka's reign because sudarshana is supposed to have attained nirvANa just before ashoka's death. It is possible that the bauddha chronicles composed much later anachronistically placed this attack on हिङ्गुला in ashoka's time and attributed it to an important arihant who is supposed to have been greater than the subsequent arihants. The figures in these chronicles are primarily early स्थविरवादिन्-s whose history was supposedly recorded by a क्षेमेन्द्रभद्र, the source of तारनाथ. It is possible the स्थविरवादिन्स् attributed the "conquest" of हिङ्गुला back in time to sudarshana and this was taken up by the वज्रयाण chroniclers later.


