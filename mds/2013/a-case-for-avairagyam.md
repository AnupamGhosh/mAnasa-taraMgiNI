
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A case for अवैराग्यं?](https://manasataramgini.wordpress.com/2013/12/06/a-case-for-avairagyam/){rel="bookmark"} {#a-case-for-अवरगय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 6, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/12/06/a-case-for-avairagyam/ "7:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The teacher had renounced worldly life and become a saffron-robed yati. He was teaching the installation of the awful गणेश for the vara-chaturthi rite. In course of that he provided mantra-s that indicated that the qualities of वैराग्यं and अवैराग्यं are both to be worshiped. Then he added that one must merely use these to remind oneself that one withdraws from अवैराग्यं to वैराग्यं and then prepares for the पिण्ड-patana along with the simultaneous mokSha which ensues from the realization that triggered वैराग्य. He then narrated the tale of भर्तृहरि and emperor विक्रमादित्य. In course of that he mentioned how भर्तृहरि had whiled away his time in his days of अवैराग्यं composing delicate verses on शृङ्गार before finally realizing its utter futility and taking to the course of वैराग्यं. He told the assembled students to attend the संस्कृत class on the वैराग्य-शतकं to learn more and use it as stepping stone to graduate to the course on the teachings of अष्टावक्र. Thereafter as an appendix to the main teaching he asked the students to meditate on the following mantra:\
[कामो हि सर्वदुःखानां योनिः । कमो ऽकार्षीन् मन्युर् अकार्षीत् कामं शमय शमय मन्युं शमय शमय वैराग्यं देहि मे स्वाहा ॥]{style="color:#0000ff;"}\
While uttering स्वाहा he instructed the students to imagine all their desires and anger arising from unfulfilled desires being offered as an oblation into the fire and being utterly burnt up.

We were not to be seen in the class that expounded the वैराग्य-shatakam but watched the students spilling out of it. Even as they came out, they were accosted by another ocher-robed मुण्डक, who followed the way of shuddhodana-putra. He said: "वैराग्यं is indeed the path but how can you attain your goal if the अलातं within you does not undergo निर्वाणं? Is the clinging to the reality of your selves not a काम in itself by which you are feeding that ever-hungry अलातं?[बुद्धं शरणं गच्छत । धर्मं शरणं गच्छत । संघं शरणं गच्छत ।]{style="color:#0000ff;"}" Even as the students stood confused, we wondered: "though the buddha साख्यमुनि might have blown off his अलातं after a prolonged practice of वैराग्यं what about the cosmic buddha who "gave up the ghost" in the योनी of the splendid चित्रसेना and spat out the बुद्धकापल तन्त्रं postmortem from his desiccated cranium. This made us turn to the verses on शृङ्गर by भर्तृहरि, wondering after all there might be a teaching therein.

Therein we encountered this beauty:\
[मत्तेभ-कुम्भ-परिणाहिनि कुङ्कुमार्द्रे]{style="color:#0000ff;"}\
[कान्ता-पयोधरयुगे रति-खेद-खिन्नः ।]{style="color:#0000ff;"}\
[वक्षो निधाय भुज-पञ्जर-मध्यवर्ती]{style="color:#0000ff;"}\
[धन्यः क्षपाः क्षपयति क्षण-लब्ध-निद्रः ॥]{style="color:#0000ff;"}

Happy is he who whiles his nights away exhausted by sexual exertion,\
resting his chest upon his woman's twin breasts moist with vermillion,\
that swell like the frontal globes of an elephant in musth and\
\[therein] encaged in the midst of her arms may gain sleep in a moment!

That indeed sounded like a high attainment a man might desire to attain. To us it seemed as worthy an attainment as that of वैराग्यं, perhaps more natural.

The yati and his followers told us that we were in error, engaging in kutarka, even as our coethnic [उद्दण्ड of Kanchipuram had addressed his नंबूथिरि rivals as elephants of kutarka](https://manasataramgini.wordpress.com/2005/05/29/the-great-chera-magicians/ "The great chera magicians"). They explained that the blissful sleep alluded to in the above verse was an ephemeral one. Moreover, its perpetual continuation till the point of पिण्ड-patana, as in the case of the tantra-spitting बुद्धकपाल, was hardly a given. In reality the man seeking such pleasures could weaken his immunity and fall prey to disease or acquire a venereal affliction. Then from the great sukha of rati he could descend to the immense pains of roga. Not just that, such a state was unlikely in the real life -- either him, or his girl or both could lose their libido in due course and this sukha could come to naught. Or else one or both of them could lose their bodily charms to age or disease making the other's company unbearable. The woman might also not yield up such pleasures; she might merely use the promise of them to ensnare the unwitting man in something much worse unlike her pretty bhuja-पञ्जर and leave you in greater agony with nothing in return. Furthermore, to keep her yielding such pleasures it might turn out be an unpleasant game of constantly putting up false displays which could cancel any pleasures from the rati-लाभ at the end of it. Thus, they concluded it was just a Platonic ideal not existing in this world, which is chased only by men unable to make out difference between the two.

It was in that statement they fell into our trap of what they might call our kutarka-buddhi. After all, we argued all of this applied to वैराग्यं too: It is an ideal that a trifling minority have ever attained. In the path to वैराग्यं there are just as many pitfalls as on the path to rati-शिखरं described above by भर्तृहरि. One could lose focus even as one might lose libido -- the pangs of roga could be so profound that one might lose interest in any pursuit of वैराग्य; so whatever might be the benefits one experiences upon its attainment, they are of little use to the जिज्ञासु on that path, even as the rivals might argue regarding महारति-सुखं to its जिज्ञासु. Moreover, ultimate goal of मुमुक्षुत्वं is attained only on पिण्डपतनं; so one is not sure if it is true. If one can understand the principle of mokSha through विचार and one can confirm it is true via yoga then वैराग्यं becomes superfluous in this pursuit. Finally, we do not see organisms by natural behavior seek वैराग्यं but they do seek the ideal represented by भर्तृहरि's verse. In fact, in भारतवर्ष one encounters वैरागिन्-s who exhibit atypical behaviors, in a sense like those seen in the mathematically gifted with Asperger's spectrum of fitness-nullifying behaviors. Hence, we concluded that वैराग्यं is not a replacement for अवैराग्यं. Indeed, that is why in the विनायक-स्थापन we seek worship both of them.

The true क्षेत्रज्ञ is he who like the eponymous bhairava holds both in his hands using one as a balance for the other so that he indeed resides in the rati-शिखरं even as the bhairava with the fair-complexioned मालिनी. Since all this easier said than done one might relax and meditate on the Platonic ideal offered by भर्तृहरि.


