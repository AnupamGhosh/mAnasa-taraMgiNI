
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कुमार chakra with the twelve dreadful goddesses](https://manasataramgini.wordpress.com/2013/07/17/kumara-chakra-with-the-twelve-dreadful-goddesses/){rel="bookmark"} {#कमर-chakra-with-the-twelve-dreadful-goddesses .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 17, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/07/17/kumara-chakra-with-the-twelve-dreadful-goddesses/ "6:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The account of the mantra practice of these कौमार goddesses was originally intended as part of the great skanda vrata. Given that that might take a while to complete, and that these goddess might be worshiped in other rites in different contexts, it is separately described here.

The worship of कुमार in the circle of twelve dreadful goddesses might be done as part of pediatric pacifications or for victory in battles. The great कुमार is invoked in the middle of a circle of these goddess. First the oblations are made to कुमार, followed by those to the twelve goddesses.

He makes at least one oblation with the following offerings to कुमार:\
milk, curds, bean-rice, cakes, sesame seeds, ground mung beans, red kidney beans, sweet syrup, cooked goat/sheep meat, beer and fruit jam.

If he desires to perform the rite for medical reasons he uses the mantra:\
[व्याधिं हर महाबाहो महाभय-विनाशन ।]{style="color:#0000ff;"}\
[प्रसन्नो देव-सेनानि विकलं त्राहि रोगिणं हुं फट् स्वाहा ॥]{style="color:#0000ff;"}

If he desires victory against his foes he offers with:\
[ॐ शत्रून् जहि कुरु कुरु कुशलं! अघोराय महाघोराय नेजमेषाय नमो हुं खट् फट् मृत् स्वाहा ॥]{style="color:#0000ff;"}

He visualizes कुमार with 6 head and 12 arms holding the usual weapons. In the pediatric ritual he visualizes the one face as being that of a goat.

The skanda-मातृ-s of this circle are:

 1.  विमोचिनी; 2) मोहिनी; 3) सुनन्दा; 4) पूतना; 5) AsurI; 6)रेवती; 7)शकुनी; 8)पिशाचिका; 9) पाशिनी; 10) महामरी; 11) कालिका; 12) भामिनी

To these goddesses he offers oblations with the following mantra-s:

विमोचिनी:[ऒं नमो भक्तवत्सले विमोछिनी स्वाहा ॥]{style="color:#0000ff;"}\
As oblations he may offer cooked red rice , molasses and ghee. He visualizes her as red in color with flowing tresses and holding a saw-toothed cleaver and a severed head.

मोहिनी: [ॐ सर्वेश्वरी सर्वजन-प्रियंकरी त्राहि त्राहि जगत्सर्वम् ॐ नमो भगवत्यै स्वाहा ॥]{style="color:#0000ff;"}\
As oblations he may offer ghee. He visualizes her as young and beautiful, with firm breasts and a snake girdle. She holds an inflorescence of trumpet flowers in her hand. She wears a beautiful diadem and has flowing hair. Certain aspects of her visualization are only revealed in private by the ritualist to his student.

सुनन्दा:[ऒं नमः सुनन्दायै घोरायै हुं फट् स्वाहा ॥]{style="color:#0000ff;"}\
As oblations he may offer ghee, guggulu and curds. He visualizes her as saffron in color with a spear like that of कुमार, a bow, an arrow and a severed head. She wears her hair in the form of a special coiffure with a skull ornament decorating it.

पूतना; for fighting enemies he uses:[ॐ नमो मात्रे । पूतने! मम शत्रुनां रुधिरं पिब पिब स्वाहा ॥]{style="color:#0000ff;"}\
For pediatric purposes he uses:[ॐ पूतने मातः! आहुतिम् इमां सुशान्त्या स्वीकुरु । बालकं/बालिकां कृपया मुञ्च वेगेनाहुत्या सुहर्षिते ॥ + स्वाहा]{style="color:#0000ff;"}\
As oblations he may offer fish, goat/sheep meat, milk and ghee. He visualizes her as beautiful but with sharp long canines and wearing a black saree. She hold a trident, a cranial bowl, and a severed head.

AsurI: [सुकले सुभगे देवी सर्व-शत्रु-निवारिणी । कुरु शान्तिम् शिशोश् चात्र वह्निहुत्या महासुरि ॥ + स्वाहा]{style="color:#0000ff;"}\
As oblations he may offer white mustard and ghee. He visualizes her as cloudy dark in color, with her hair in an upward pointing coiffure and earrings with skulls. She holds a large mace.

रेवती; for fighting enemies he uses: [घोररूपी महाभागे जयं देहि शुभानने । क्षेमं कुरु जगत्य् अस्मिङ् चोभना भव रेवती ॥]{style="color:#0000ff;"}\
For pediatric purposes: [घोररूपी महाभागे बालम् मुञ्च शुभानने । क्षेमं कुरु जगत्य् अस्मिङ् चोभना भव रेवती ॥ + स्वाहा]{style="color:#0000ff;"}\
As oblations he may offer goat butter and पोलिका-s. He visualizes her as having a rich raiment studded with gems and wearing several gold ornaments and a crown on her head. She has and angry look and holds a spear like that of कुमार, a bunch of severed heads, a खट्वाङ्ग, a उत्क्रान्तिद (death-dealing rod) and an ashani (thunderbolt).

शकुनी; for fighting enemies he uses: [ॐ नमः पद्मपत्राक्षी कल्याण-वदने शिवे । इमाम् आहुतिम् संगृह्य जयं देहि सुशोभने ॥ +स्वाहा]{style="color:#0000ff;"}

For pediatric use: [ॐ नमः पद्मपत्राक्षी कल्याण-वदने शिवे । इमाम् आहुतिम् संगृह्य बालम् मुञ्च सुशोभने ॥ +स्वाहा]{style="color:#0000ff;"}\
As oblations he may offer guggulu, ghee and पोलिका-s. He visualizes her as slender-waisted with well-formed breasts, having large eyes. She holds a spear and a water pot.

पिशाचिका; for fighting enemies he uses: [ॐ नमः सर्वभूतेशी शोभने त्वं पिशाचिके । हुतिम् चैव पुरस्कृत्य त्वरितं भञ्ज दुष्कृतम् ॥]{style="color:#0000ff;"}\
For pediatric use: [ॐ नमः सर्वभूतेशी शोभने त्वं पिशाचिके । हुतिम् चैव पुरस्कृत्य त्वरितं मुञ्च बालकम् ॥]{style="color:#0000ff;"}\
As oblations he may offer ghee rice.He visualizes her as black in color, with a cleaver and a skull bowl.

पाशिनी: [नमो ऽस्तु ते पाशिनी रक्ष रक्ष प्रसन्ना भव स्कन्दप्रिये प्रियानने ॥ + स्वाहा]{style="color:#0000ff;"}\
As oblations he may offer cooked rice. He visualizes her as wearing a white saree, of fair complexion and holding a lasso and a club.

महामारी; for countering enemies: [ॐ नमो रक्तवर्णाक्षि रक्त-पद्म-निभानने । रक्तोष्ठे रक्त-दशने महामरि नमोऽस्तुते ॥+स्वाहा]{style="color:#0000ff;"}\
For pediatric use: [ॐ नमो रक्तवर्णाक्षि रक्त-पद्म-निभानने । व्याधिना पीडितं बालं रक्षेमं परमेश्वरी ॥+ स्वाहा]{style="color:#0000ff;"}\
As oblations he offers white mustard, rice and पोलिका-s. He visualizes her as fair in color, with a trident, sword, Damaru, and skull bowl, with sharp long canines.

कालिका; For destroying enemies: [ॐ नमो घोररुपायै विकटे घोरदर्शने ! त्राहि सर्वजगद्धात्रि जयं देहि रणप्रिये ॥+ स्वाहा]{style="color:#0000ff;"}\
For pediatric use:[ॐ नमो घोररुपायै विकटे घोरदर्शने ! त्राहि सर्वजगद्धात्रि बालं मुञ्च बलिप्रिये ॥ +स्वाहा]{style="color:#0000ff;"}\
As oblations he offers ghee mixed with garlic pods. He visualizes her as being blue in color with firm rising breasts. She wears a skirt of severed hands and a garland of severed heads. She stick her tongue out and has dense flowing tresses. She hold a cleaver, trident, skull bowl and a severed head. She has three eyes and a lunar crescent as avheaddress.

भामिनी; for countering enemies: [शत्रून् संहर संहर वैरिबलम् भञ्ज भञ्ज इमाम् आहुतिम् गृहीत्वा सुप्रसन्ना भव भामिनी ॥]{style="color:#0000ff;"}\
For pediatric use: [बालं इमं मुञ्च मुञ्च व्याधीन् संहर संहर षड्वक्त्र-प्रसादेन प्रसन्ना भव भामिनी ॥]{style="color:#0000ff;"}

He may offer cakes and milk as oblations. He conceives भामिनी with an angry mien and holding a large rod and an axe.

After these oblations he offers bali-s in baskets under a कदंब or fig tree saying:[द्वादश घोरा देव्यः बलीन् इमान् गृहीत्वा स्कन्देन सह मोदध्वम् । स्कन्दः सुप्रीतो सुप्रसन्नो वरदो भवतु ॥]{style="color:#0000ff;"}

Then he returns without looking back.


