
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The मयाभेद सूक्त: a discursion](https://manasataramgini.wordpress.com/2013/07/02/the-mayabheda-sukta/){rel="bookmark"} {#the-मयभद-सकत-a-discursion .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 2, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/07/02/the-mayabheda-sukta/ "6:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the great pravargya ritual the माहावीर pot containing the milk of a cow and a goat, i.e. the gharma offering, is intensely heated until it starts glowing. When the pot starts glowing the hotar looks at it starts reciting the सूक्त RV 10.177 (In some traditions in south India they only recite RV 10.177.1 & 3). This सूक्त is traditionally referred to as the मायाभेद सूक्त. Regarding this the सूक्त the शौनकीय ऋग्विधान redacted by the early vaiShNava विष्णुकुमार states:

[पतङ्गम् इति नित्यम् तु जपेद् अज्ञान भेदनम् ।]{style="color:#0000ff;"}\
[माया भेदनम् एतद्धि सर्व मायाः प्रबाधते ॥]{style="color:#0000ff;"}

He should constantly do japa of the \[mantra-s beginning with] पतङ्ग that destroys ignorance. It is breaks the \[spell of] mAyA and drives away all mAyA.

[शाम्बरीम् इन्द्रजालाम् वा मायाम् एतेन वारयेत् ।]{style="color:#0000ff;"}\
[अदृष्टानाम् च सत्त्वानाम् मायाम् एतेन बाधते ॥]{style="color:#0000ff;"} ऱ्‌Vध् 4.115-116

By this he should block mAyA be it of the shambara or the इन्द्रजाल variety; By this he repulses the mAyA of the unseen ones and of the consciousness.

This prayoga is interesting because it uses the term शाम्बरी in the sense of a magical prayoga, which is closer to its use in the sense we encounter it in the temporally later tantra texts. But of course the mAyA of shambara has a very old precedence, primarily in a negative sense, in the ऋग्वेद itself:

[त्वं दिवो बृहतः सानु कोपयो ऽव त्मना धृषता शम्बरम् भिनत् ।]{style="color:#0000ff;"}\
[यन् मायिनो व्रन्दिनो मन्दिना धृषच् छितां गभस्तिम् अशनिम् पृतन्यसि ॥]{style="color:#0000ff;"} RV 1.54.4\
You shook the pinnacle of the high heavens; with your own valor you rent apart shambara,\
when exhilarated with the flowing soma juice you battled with the sharp, radiating thunderbolt those wielding mAyA.

This might be compared to the mAyA of other दानव-s and dasyu-s like वृत्र:

[इन्द्रो महां सिन्धुम् आशयानम् मायाविनं वृत्रम् अस्फुरन् निः ।]{style="color:#0000ff;"}\
[अरेजेतां रोदसी भियाने कनिक्रदतो वृष्णो अस्य वज्रात् ॥]{style="color:#0000ff;"}\
indra threw down mAyA-bearing वृत्र who sprawled across the great sindhu: both the celestial hemispheres trembled in terror of the manly warrior's vajra when he roared.

On the other hand the mAyA of indra and other deities is clearly praised in the ऋग्वेद:

[महो महानि पनयन्त्य् अस्येन्द्रस्य कर्म सुकृता पुरूणि ।]{style="color:#0000ff;"}\
[वृजनेन वृजिनान् सम् पिपेष मायाभिर् दस्यूंर् अभिभूत्य् ओजाः ॥]{style="color:#0000ff;"}\
\[The ritualists] express wonder at the great deeds of this great one, the numerous glorious acts of indra; Through his might the surpassed the mighty, of unsurpassed might, with his mAyA powers he pounded the dasyus.

This mAyA of indra is likely to have been the precursor of the term इन्द्रजाल. However, in the विधान the सूक्त is being deployed against both the शाम्बरी and इन्द्रजाल suggesting that there was some semantic shift in the term mAyA --- or at least a possible negative connotation for both the daivi, i.e. इन्द्रजाल, and दानवी mAyA.

While witnessing the pravargya from the sadas over two decade ago, a local ब्राह्मण remarked that the meaning of the मायाभेद was to dispel mAyA in the sense of advaita vedAnta. This is an even further semantic shift. Indeed, the memory of both शांबरी and इन्द्रजाल in negative sense survives in later Hindu tradition in the yoga वासिष्ठ of the Kashmirian advaitin स्मार्त-s. Here in a very typically advaitic manner the world is portrayed as an illusory इन्द्रजाल. Further, evidence for this memory comes from the verse of the great Kashmirian kavi क्षेमेन्द्र:

[संसार-cइत्र-मायां शम्बर-मायां विष्णु-मायाम् च ।]{style="color:#0000ff;"}\
[यो जानाति जितात्मा सो ऽपि न जानाति योषितां मायां ॥]{style="color:#0000ff;"}\
Even the one who has realized the atman who understands the mAyA-s of saMsAra, shambara and विष्णु does not understand the mAyA of women.

Coming to the yoga वासिष्ठ, वसिष्ठ tell rAma:\
[अत्र ते शृणु वक्ष्यामि वृत्तान्तं इमम् उत्तमं ।]{style="color:#0000ff;"}\
[जागती हेन्द्रजाल श्रीश् चित्तायत्ता यथा स्थिता ॥]{style="color:#0000ff;"}मुमुक्षु-व्यवहार प्रकरणं 104.1

Now you hear me narrate this excellent story which shows the world to be like an इन्द्रजाल, i.e, illusory wonder resting on the consciousness.

The story might be retold thus:\
In the lands of the उत्तरपाण्डव-s ruled लवण the descendent of harishchandra. He was a brave warrior and a good administrator well assisted by meritorious officers. Once to his court arrived an इन्द्रजाल magician. With his sweet talk he convinced the राजन् that he would display a form of इन्द्रजाल known as the खरोलिका. He twirled his magic wand with peacock feathers and began to manifest many wonders even as the परमात्मन् creates numerous forms by mAyA. It emitted variegated particles even as the rainbow of indra. Immediately a chieftain from the saindhava country became manifest by magic followed by an extremely fast horse, which he said was like the उच्चैःश्रवस् of indra. He told लवण that he was bringing the horse as a gift from the राजन् of sindhudesha. The magician told the king that it befitted a great राजन् like him to ride that horse. According the king mounted the illusory horse and went into a hypnotic spell for two मुहूर्त-s during which he witnessed numerous hallucinations. He saw himself entering the realm of चण्डाळस् in forests of the vindhya-s and married a चण्डाली. Then he lives with her family and begets children via her. As he is living as a chaNDAla, their land is afflicted by drought and famine. Afflicted by hunger they start wandering. Finally he sees his younger son sorely afflicted by hunger and he offers his own flesh as his food. At this point he was about to kill himself when he break out from the hypnosis.

When his courtiers finally resurrected him from his hypnotic stupor, he, interestingly describes these hallucinations simultaneously as both शाम्बरि and इन्द्रजाल:\
[इदम् आश्चर्यम् आख्यानं श्रूय्तां रे सभासदः ।]{style="color:#0000ff;"}\
[मम शाम्बरिकेणेह यन् मुहूर्तं प्रदर्शितम् ॥]{style="color:#0000ff;"} मुमुक्षु-व्यवहार प्रकरणं 105.25

The gloss of bodhendra states:[शम्बरस्य माया शांबरी सा ऽस्या ऽस्ति इति शांबरीकस् तेन ।]{style="color:#0000ff;"}\
Thus the hypnosis which the king was placed under is understood as the mAyA (magic) of shambara. Thus we have: Arrey courtiers! Hear this astonishing account of the hallucinations I witnessed under the shambara spell in course of the मुहूर्त.

[दृश्टवान् अहम् एतस्मिन् बह्वीः कार्य-दशाश्-चलाः ।]{style="color:#0000ff;"}\
[मुहूर्तं प्रार्थितो ऽध्वस्त शक्र-सृष्टिर् इव अब्जजः ॥]{style="color:#0000ff;"}मुमुक्षु-व्यवहार प्रकरणं 105.26

I have seen under this spell numerous passing scenes of existences even as the lotus-born brahmA was assailed for a मुहूर्त by the illusions of indra's creation that he had destroyed.

bodhendra, explains the illusion of indra's creation in his gloss thus by providing an old narrative concerning the battle between the daitya bali and indra:

[अध्वस्त शक्र-सृष्टिर् इति च्छेधः । पुरा किल बलिः क्वचिद् असहायं शक्रम् आसाद्य बलेर् निगृह्याहर्तु-कामः शक्रेण मायया स्व-सैन्यं सृष्ट्वा बलिं मायाबन्धेन मोहयामास । तदा बलिना स्व-बन्धन-मोक्षणाय स्तुत्या प्रसादितो ब्रह्मा तत्र आगत्य शाक्रीं सृष्टिं ध्वंसयितुं प्रवृत्तः शक्र-प्रार्थनया अध्वस्त-शक्र-सृष्टिर् मुहूर्त-मात्रं तन् माया-कौतुकं यथा दृष्टवांस् तद्वद् इत्य् अर्थः ॥]{style="color:#0000ff;"}

The demon bali had succeeded in making indra somewhat ineffective against him. With the intention of defeating bali's coercion indra used his mAyA to generate a magical army and entrapped bali in the bonds of his mAyA. Then bali to free himself from this bondage of indra's magic praised brahmA, who pleased by his praise arrived there at battle field and proceeded to destroy indra's magical creation. However, at indra's request, brahmA had seen the wonders of indra's creation via his mAyA power for a मुहूर्त.

This tale gives a hint regarding the development of a negative connotation for इन्द्रजाल. This tale clearly belongs to the tradition of the primacy of prajApati that began in the late Vedic period. The rise of prajApati to primacy within the shrauta system happened at the expense of indra and this clearly indicated in this tale. Moreover, the designation of prajApati as an originator deity was also clearly in competition with indra's role in generating or structuring the universe. Thus, the tale has indra's own creation as being transient and destroyed by prajApati (brahmA). We know that this प्राजापत्य tradition with brahmA at the forefront was a major force in the period the composition of the रामायण and the महाभारत (in both texts the sub-current tension with the aindra-para tradition is seen). It was the same tradition that was a big force among the vaidika ब्राह्मण-s in the days of the तथागत and was vehemently attacked by him. The tradition eventually succumbed to the competition from within in the form of the shaiva, vaiShNava, कौमार traditions and attacks from without from the bauddha-s. But we hear from the आचार्य abhinavagupta in the तन्त्रालोक that there were प्राजापत्य-s still around in his days (\~1000 years ago) in कश्मीर country. Hence, it is not surprising that an account aligned with their worldview is found in the yoga-वासिष्ठ. In terms of relative chronology based on the language and content we can place the composition of the ऋग्विधान right in the core of the प्राजापत्य period. Hence, it is quite possible that ideas such as those expressed in the above tale of indra-सृष्टि contributed to the emergence of a negative connotation for इन्द्रजाल alongside शाम्बरि in this text. However, in the shaiva तान्त्रिक tradition these still retained a positive sense as in the celebrated योगिनी-जाल-shambara \[Footnote 1].

The yoga-वासिष्ठ also has a tale regarding shambara's magic that appears to be a a version of the legend alluded to in the ऋग्वेद where indra is said to have destroyed the मायाविन्'s of the shambara. However, as described above the tale has clearly been reworked by the प्राजापत्य-s to assert the primacy of brahmA. Indeed, the योगवासिष्ठ takes a rather pro-daitya stance in certain places and even praises the daitya shambara renowned for his magic as an exemplar of advaitic renunciation with a difficult understand word play:\
[शम्बरैक परो ऽप्य् अन्तः शम्बरैक तयोदितः ।]{style="color:#0000ff;"}\
[संसार शम्बरं राम शम्बरस् त्यक्तवान् इदम् ॥]{style="color:#0000ff;"}

[बोधेन्द्र स्ततेस्: शम्बरं माया तद् एक परो ऽप्यन्तः शम्बरं हार्द-चिद्-आकाशस् तद् एकतया उदित आविर्भूतः ॥]{style="color:#99cc00;"} upashama-प्रकरणं 75.9

Thus: shambara is the foremost मायाविन्, but his internal "heart space" is clear as water, (shambara) within which emerges the unity. Hence, O rAma, shambara had renounced the "shambara" (i.e. the mAyA) of saMsAra (a very advaitic teaching!).

The tale of shambara narrated in the sthiti-प्रकरणं 25 onwards goes thus:\
In the depths of पाताल lived the foremost lord of the daitya-s name shambara. This demon was a great मायाविन् and created a magic world. He terrorized the deva-s in svarga with his machine known as the tarku-yantra and a magical elephant. In retaliation when he was away from his city or sleeping, the deva-s attacked his realm and massacred his daitya army. The furious shambara invaded svarga and set off a long war. The deva-s fearing his powers left their abodes and continued fighting and killing all the forces he dispatched against them. Thus discomfited, he created three daitya-s known as dAma, व्याल and कट to lead his forces. They were in reality zombies or robots who lacked any kind of mental preconditioning -- thus they were free from fear or doubt.Being robotic zombies they lacked the sense of 'I'; hence, they were not afflicted by the possibility of "I will be harmed".

Commanded by shambara they rose out of the ocean and with their gigantic forms obscured the sun and absorbed all its light. The deva army of indra issued forth from caverns and the forests of the towering mountain meru like a great deluge and an awe-inspiring deva-दानव encounter with the use of numerous celestial missiles. These are described at length in the account of the great encounter between indra's forces and those of shambara. For example:

[भुषुण्डी-मण्डला-स्फोट स्फुटन् मेरु शिरः शतम् ।]{style="color:#0000ff;"}\
[शर-मारुत निर्लून दैत्य-देव-मुखाम्बुजम् ॥]{style="color:#0000ff;"} sthiti-प्रकरण 26.28

The explosions of arrays of भुषुण्डी-s \[fired by the daitya-s] burst the hundred peaks of meru; the wind of missiles pierced the lotus faces of the daitya and deva-s.

[हेत्य् उग्र वात निष्पिष्ट पतद् वाइमानिक-व्रजम् ।]{style="color:#0000ff;"}\
[अस्त्रोदिताब्धि वार्योघ प्लावित व्योम-पत्तनं ॥]{style="color:#0000ff;"}sthiti-प्रकरण 26.28

Blasted by the violent shock-waves of the missiles whole formations of airplanes fell; The flying stations were deluged by floods generated by water hurled by the astras.

[अस्त्र-निर्मित-दुर्वार-तमः कल्पान्त-रात्रिकम् ।]{style="color:#0000ff;"}\
[माया सूर्य-गणोद्योतैः पीतातनुतमः पटं ॥]{style="color:#0000ff;"} sthiti-प्रकरण 26.42

With foul clouds generated by their astra-s \[the daitya-s] created darkness like the end of the kalpa; \[The deva-s] using a cluster of mAyA suns lit up the darkness.

There is also an interesting shloka that has been interpreted in two ways:

[एवं शब्द-शतोन्-नाद-पाताल-तल-वारणं ।]{style="color:#0000ff;"}\
[विनायक-कराकृष्ट-दीर्घ-दानव-पर्वतं ॥]{style="color:#0000ff;"}sthiti-प्रकरण 26.56

Thus, \[in counter to the cries of the] mountain-like दानव-s seized by the kara (talons) of vi-नायक (= lord of the birds, garuDa) | seized by the kara (trunk) of विनायक, the elephants of the netherworld trumpeted a hundred times.

Thus, even after a prolonged fight the deva-s were unable to overcome dAma, व्याल and कट and retreated to brahmA. Then brahmA explained that their lack of I-ness allows them to be unlimited. He asked indra to foster a sense of egoistic avarice in them. Then they would lose their zombie-like nature conscious of their desires. Then the deva-s rested and refreshed themselves and challenged the daitya-s to war. The daitya-s immediately retaliated again with a remarkable array of celestial weapons, e.g.:

[मध्य-प्रवाहवहद्-उल्मुक-शूल-शैल-]{style="color:#0000ff;"}\
[प्रास असि-कुन्त-शर-तोमर-मुद्गरेण ।]{style="color:#0000ff;"}\
[गङ्गोपम अम्बु-वलित अमर-मन्दिरेण]{style="color:#0000ff;"}\
[सर्वासु दिक्ष्व् अशनि वर्ष निकर्षणेन ॥]{style="color:#0000ff;"} sthiti-प्रकरण 28.8

The deva strongholds were assailed by a deluge of meteor-like missiles, tridents, rocks, lances, swords, sharp spikes, arrows, poled-cleavers, and hammers, they were flooded by gushing water like that of the गङ्गा, and blazing lightning weapons from all directions.

[गरुड-गुड-गुडाकुलान्तरिक्ष-]{style="color:#0000ff;"}\
[प्रविसृत-हेति-हुताश-पर्वतौघैः ।]{style="color:#0000ff;"}\
[जगद् अभवद् असह्य कल्प-काल-]{style="color:#0000ff;"}\
[ज्वलित-सुरालय-भूतल अन्तरालम् ॥]{style="color:#0000ff;"} sthiti-प्रकरण 28.15

\[The deva-s] with clusters of garuDa bombs and daityas with shells; the streams of fire and rocks from launched missiles made it unbearable as the world at the end of the kalpa, even as deva strongholds burnt and fell to the earth.

Thus, undaunted the deva-s led by indra and the daitya-s by dAma, व्याल and कट fought on and the war raged for 35 years. Then after a break they fought on 5 years 8 month and 10 days. Fighting ceaseless over this prolonged period the robotic daitya-s felt a sense of "I" developing. This lead to the desire for complete victory over the deva-s but it also caused a sense doubt that they could be killed by the deva-s. Once this happened the deva-s got better of them and smashed the daitya-s slaughtering their army in a great battle on the slopes of mount meru. Their corpses are said to have showered all over जम्बुद्वीप.

With his mAyA thus broken, shambara called out to his three मायाविन्-s in despair but the three robotic daitya-s now endowed with Atman-s fled to naraka where they were married to ladies of the naraka realm. Once they showed disrespect to yama when he was touring the realms of naraka and furious he dispatched them to a great blazing cauldron where they were torture and died. Then they were reborn as किरात tribesmen. After that they were born as crows, then as vultures, then as boars in the trigarta country, then as red gorals in the hills of magadha, then as snakes. Then they reincarnated as a zebrafish and other types of fishes in काश्मीर. After many births as fishes they were crushed to death under the hoof of a buffalo which was wallowing in a Kashmirian lake. Then they were born as cranes. Finally, व्याल was born as sparrow, dAma as a mosquito and कट as a parrot. This happened when the great king yashaskara was ruling काश्मीर. They happened to hear his minister नरसिंह recite the narrative of the great war of the deva-s and the daitya-s. When they heard this narrative they "awoke" in the eternality of Atman and attained mokSha.

Even so at the सोमयाग as the great flames leapt from the महावीर pot when the goat and cow's milk were added to the superheated ghee, we awoke from the advaitin's discourse and started wondering about the real meaning of the original मायाभेद सूक्त deployed in the pravargya and it dawned on us that it was close to the ritual in which it was used.

मायाभेद सूक्तम् (RV 10.177):

[पतङ्गम् अक्तम् असुरस्य मायया हृदा पश्यन्ति मनसा विपश्चितः ।]{style="color:#0000ff;"}\
[समुद्रे अन्तः कवयो वि चक्षते मरीचीनाम् पदम् इच्छन्ति वेधसः ॥]{style="color:#0000ff;"}

विपष्चितः = seers, plural subject; मनसा= by their mind, instrumental; हृदा= by heart, instrumental of हृद्; पतङ्गम्= bird, singular object; aktam= anointed; asurasya=asura's, genitive singular; मायया= by mAyA; singular instrumental; pashyanti= see, 3rd person plural present, parasmai.

samudre= in ocean, locative; अन्तः= inside; कवयः= kavi-s, vocative plural; vi चक्षते= 3rd person singular present, atmane; मरीचीनाम्= rays, genitive plural; padam= station, accusative singular; वेधसः= ritualists; plural subject; इच्छन्ति= 3rd person plural present, parasmai.

Here the terms vipashchit, vedhas and kavi all refer to the ritualist-mantra composer-sages who are participating in the rite.

The seers see with their minds and with their heart the bird anointed with the mAyA of the asura; O kavi-s, from within the ocean he shines forth, the ritualists seek the station of \[his] rays.

Here the पतङ्ग, by vaidika metaphor, is the sun and he is "anointed" with rays by the mAyA of the asura who is none other than the deva सविता (Indeed हिरण्यस्तूप आङ्गिरस praises सविता thus: [वि सुपर्णो अन्तरिक्षाण्य् अख्यद् गभीरवेपा असुरः सुनीथः ।]{style="color:#0000ff;"} RV 1.35.7a. The golden eagle, lit up the mid-regions, the one from the wavy depths, the good asura). Even when not visible he shines from within the ocean. The meditating on him the ritualists seek the "station" of his rays, which could figuratively mean the illumination of his rays. This effectively is parallel to the image presented by the famed sAvitrI ऋक् of विश्वामित्र that used in the संध्या ritual, where in the rays of the deva सविता are sought to illuminate the mind of the meditator. This connects to the fact that one of the prime deities of the gharma offering is सविता (the deva-s offered the gharma are: ashvin-s, वायु, indra, सविता, बृहस्पति and yama).

..........\
[पतङ्गो वाचम् मनसा बिभर्ति तां गन्धर्वो ऽवदद् गर्भे अन्तः ।]{style="color:#0000ff;"}\
[तां द्योतमानां स्वर्यम् मनीषाम् ऋतस्य पदे कवयो नि पान्ति ॥]{style="color:#0000ff;"}

पतङ्गः= singular, subject; वाचम्= holy utterance/incantation, object; मनसा= singular instrumental; bibharti= bears, 3rd person singular present, parasmai; tAM= that, accusative feminine; गन्धर्वः=subject; avadat= uttered, 3rd person singular past, parasmai, garbhe= in the womb, locative singular, अन्तः= inside;

tAM= that, accusative feminine, द्योतमानां= radiant; svaryam= celestial; मनीषां= mental creation, i.e., incantation, object; ऋतस्य= of the ऋत, genitive; pade= in the station, locative; कवयः=kavi-s, nominative plural; ni पान्ति= take in \[literally drink in], 3rd person plural present, parasmai.

The bird holds the incantation (वाक्, the holy utterance) in the mind; the gandharva had uttered within the womb. That radiant, celestial incantation the kavi-s take in at the station of the ऋत (i.e. the universal laws).

Here the bird, i.e., the sun is said to hold the incantation, which is uttered by the gandharva within the womb, which represented by the world hemispheres. But for the ritualist the external sun is also homologized with the "light" of mental enlightenment; hence the synesthetic metaphor of the sun holding it in the mind. This enlightenment leads to the utterance of the mantra-s within, which is expressed by the metaphor of the gandharva \[For an explicit statement of this internal homology see below]. The gandharva here is the sun as indicated in other ऋग्वेद mantra-s such as:\
[ऊर्ध्वो गन्धर्वो अधि नाके अस्थाद् विश्वा रूपा प्रतिचक्षाणो अस्य ।]{style="color:#0000ff;"}\
[भानुः शुक्रेण शोचिषा व्य् अद्यौत् प्रारूरुचद् रोदसी मातरा शुचिः ॥]{style="color:#0000ff;"} RV 9.85.12

High to the zenith as the gandharva risen, beholding all these varied forms.\
His rays have shone widely with brightness: the pure one has lit both the worlds, the parents.

This gandharva also specifically relates to the gandharva-s invoked at the pravargya after the महावीर pot is finally disposed. Here the ritualists as a chorus utter a series of यजुष् incantations which include the formulae:\
[रन्तिर् नामासि दिव्यो गन्धर्व । तस्य ते पद्वद् धविर्धानं । अग्निर् अध्यक्षा । रुद्रोऽधिपतिः ॥]{style="color:#0000ff;"}\
you are ranti by name, the celestial gandharva, the हविर्धान is your foot messenger, agni your president, rudra is your overlord !

[विश्वावसुर् अभि तन् नो गृणातु । दिव्यो गन्धर्वो रजसो विमानः । यद् वा घा सत्यम् उत यन् न विद्म । धियो हिन्वानो धिय इन्नो अव्याद् । प्रासं गन्धर्वो अमृतानि वोचद् । (प्राना वा अमृताः ॥)]{style="color:#0000ff;"}

विश्वावसु, proclaim it to us, the celestial gandharva who measures out the heavens. Whether we know the truth already or not, he who has impelled our inspired intellect also have these thoughts inspired. The gandharva has proclaimed he immortal one. The prAna-s are immortal.

Thus, the mantra-s of the मायाभेद सूक्त are closely related to the inspiration that is received as result of the rite which is supposed to be proclaimed by the celestial gandharva. The gandharva in Vedic tradition has a mysterious nature and can take possession of individuals during which he can make revelations via the possessed medium. This relates to the earlier ऋक् where the sun(bird) is said to be covered by mAyA, thus making it fit to be described as a gandharva. Like the possessing gandharva-s, the solar gandharva-s ranti and विश्वावसु confer knowledge to the ritualists. This is what is alluded to when they are described as drinking the gandharva's proclamation at the seat of the universal laws. This seat of the ऋत is in essence the ritual arena -- the uttaravedi which is homologized with the universe. Indeed the incantation used in the pravargya when he sets down the महावीर pot at the conclusion of the ritualist utter the formula:

[चतुःस्रक्तिर् नाभिर् ऋतस्य । (इयम् वा ऋतम् । तस्या एष एव नाभिः ।)]{style="color:#0000ff;"}\
Quadrangular is the center of ऋत. This \[the uttaravedi] is the ऋत; the pravargya is its root.

..........\
[अपश्यं गोपाम् अनिपद्यमानम् आ च परा च पथिभिश् चरन्तम् ।]{style="color:#0000ff;"}\
[स सध्रीचीः स विषूचीर् वसान आ वरीवर्ति भुवनेष्व् अन्तः ॥]{style="color:#0000ff;"}

अपश्यं= saw; 1st person singular past, parasmai; गोपाम्= guardian, object; अनिपद्यमानम्=never resting; A cha parA cha= northern and southern (eastern and western); पथिभिः= paths, instrumental; charatam= moving;

sa= he; सध्रीचीः= approaching; विषूचीः= departing; वसानः= clothes [ or in this context taking on appearances], singular nominative; A वरीवर्ति= revolves, 3rd person singular, parasmai; भुवनेषु= in worlds, locative plural; अन्तः= inside.

I saw the never-resting guardian, moving along the northern and southern \[eastern and western] paths. He, in his approaching and departing apparitions (or: clothing the सध्रीची= अन्तरिक्ष and the विषुची= dyaus \[with his rays]), continually revolves in midst of the worlds.

The तैत्तिरीय AraNyaka explains:\
[अप्श्यं गोपां इत्य् आह । प्राणो वै गोपाः । प्राणम् एव प्रजासु वि यात्यति । "अपश्यं गोपां इत्य् आह । असौ वा अदित्यो गोपाः । स हीमाः प्रजा गोपायति । तम् एव प्रजानां गोप्तारं कुरुते ॥]{style="color:#0000ff;"}\
"I saw the guardian" so he recites. The guardian is the prANa. \[If he meditate thus] he has the prANa flowing into all reproducing life. "I saw the guardian" so he recites. The yonder Aditya is the guardian for he guards all this reproducing life. \[If he mediates thus] he makes a protector for all life forms.

This explanation establishes clearly the duality that is implied in these mantra-s i.e. homologizing of the external sun with the internal prANa which is also described as a bird (e.g. the हंसः). It also holds for the rest of the ऋक्: Both the prANa and the sun are seen as never-resting. The paths taken by the sun might be interpreted as the rising and the setting (i.e. eastern and western). But we prefer the explanation of the northern and southern paths because traditionally these are the two paths or ayana-s of the sun. The eastern and western one are part of the same path after all. The prANa is similarly seen as having the inhalation and exhalation as its two paths. The सध्रीचीः and विषूचीः in the case of the sun might be interpreted as the visible apparition during the day with the bright clothing or the invisible one at night with the dark clothing. However, it is has also been connected with water -- i.e. the one which draws water away from the earth and the one which supplies waters in the form of the rains. The inward prANa similarly is dry and the outward one is wet. Finally like the sun is seen as revolving within the world, the prANa is seen as constantly revolving within an organism.

..........

This shows that the Hindu tradition of meditative practices connecting the observation of the prANa with the celestial solar movement or penetration by solar light (as seen in the daily संध्योपासना) was also related to the mantra-s in the context of the pravargya ritual. Moreover it also became clear to us that the मायाभेद सूक्त is closely linked to the pravargya rite and was most probably composed precisely for that rite. However, the mAyA and asura in this context are meant in a largely positive sense as that of सविता. The mystery of this mAyA may be seen as being discerned by the ritualists who realize the homologies between the prANa and sUrya. It was only later in the विधान tradition that the mAyA acquired a negative connotation. Even latter the advaitin-s interpreted it in the sense they understood mAyA.

***Footnotes***

Footnote 1: While the majority of the surviving vaidika tradition tilts in favor of the प्राजापत्य interpretation, we favor the older aindra interpretation. We hold that the aindra interpretation is more robust and dynamic and consistent with the actual ritual process. We also believe this will be more suitable for the upholding and defense of the dharma via emulation of the impetuous vigor and dasyu-felling mAyA of indra. In this view prajApati remains in the background much like his सांख्यन् abstraction. This aindra-para view of ours is similar to that expressed by [arjuna in the bhArata](https://manasataramgini.wordpress.com/2013/03/05/the-lesson-of-arjuna/).


