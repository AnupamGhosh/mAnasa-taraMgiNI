
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [कुमार gAthA](https://manasataramgini.wordpress.com/2013/05/10/kumara-gatha/){rel="bookmark"} {#कमर-gatha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 10, 2013]{.entry-date}](https://manasataramgini.wordpress.com/2013/05/10/kumara-gatha/ "6:32 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[जय अतुल-शक्ति दीधिति पिञ्जर ।]{style="color:#99cc00;"}\
[भुज-दण्ड-छण्ड-रण-रभस ।]{style="color:#99cc00;"}\
[सुर-वदन कुमुद-कानन-विकासनेन्दो कुमार ।]{style="color:#99cc00;"}\
[जय दितिज-कुल-महोदधि-वडवानल ।]{style="color:#99cc00;"}\
[षण्मुख मधुर-रव-मयूर-रथ ।]{style="color:#99cc00;"}\
[सुर-मुकुट-कोटि-घट्टित-चरण-नखाङ्कुर महासन ।]{style="color:#99cc00;"}\
[जय ललित-छूड कलाप-नव-विमल-दल-कमल-कान्त ।]{style="color:#99cc00;"}\
[दैत्य-वंश-दुःसह-दावानल ।]{style="color:#99cc00;"}\
[जय विशाख विभो ।]{style="color:#99cc00;"}\
[जय सकल-लोक-तारक ।]{style="color:#99cc00;"}\
[जय देवसेना-नायक स्कन्द ।]{style="color:#99cc00;"}\
[जय गौरी-नन्दन घण्टाप्रिय ।]{style="color:#99cc00;"}\
[प्रिय विशाख विभो ।]{style="color:#99cc00;"}\
[धृत-पताक प्रकीर्ण-पटल ।]{style="color:#99cc00;"}\
[कनक-भूषण भासुर-दिनकरच्-छाय ।]{style="color:#99cc00;"}\
[जय जनित संभ्रम लीला-लूनाखिलाराते ।]{style="color:#99cc00;"}\
[जय सकल-लोक-तारक दितिजासुरवर-तारकान्तक स्कन्द ।]{style="color:#99cc00;"}\
[जय बाल-सप्तवासर ।]{style="color:#99cc00;"}\
[जय भुवनावलि-शोक-विनाशन ॥]{style="color:#99cc00;"}

Victory! him glowing golden from the radiance of his incomparable spear;\
the one impetuous in battle with terrible, staff-like arms;\
कुमार, who is the moon that causes the thicket of lotuses formed by the faces of the devas to bloom!\
Victory! One who is the equine fire to the ocean the clan of diti's descendents;\
the six-headed one, with a sweet-crying peacock for a chariot;\
One seated on the great throne with sprout-like toe-nails worshiped by the bowing crowned heads for crores of gods!\
Victory! One with a charming crest, one shining with a garland of fresh, blemishless lotus petals;\
the irresistible conflagration that burns the daitya clan!\
Victory! विशाख the opulent one!\
Victory! protector of the whole world!\
Victory! Lord of the deva army, skanda!\
Victory! Son of the fair goddess, one delighted in the clang of bells;\
dear, विशाख, the opulent one;\
holding a flag, with a diverse retinue;\
ornamented with gems, with the complexion of the glowing sun!\
Victory! One who has been born, the ever-active one, one who playfully lops off all enemies.\
Victory! The protector of all worlds; the killer of तारक the foremost of the diti-born asura-s, skanda !\
Victory! The seven day old child!\
Victory! The destroyer of the sorrows of the worlds!\
\[All epithets are in the singular vocative]

The कुमार-काण्ड of the matysa पुराण (159) preserves the above fairly ancient gAthA to कुमार.

This is also accompanied by the following stuti:

[नमः कुमाराय महाप्रभाय स्कन्दाय छ स्कन्दित-दानवाय ।]{style="color:#99cc00;"}\
[नवार्क-विद्युद्-द्युतये नमो ऽस्तु ते नमो ऽस्तु ते षण्मुख कामरूप ॥]{style="color:#99cc00;"}\
[पिनद्ध-नानाभरणाय भर्त्रे नमो रणे दानव-दारणाय ।]{style="color:#99cc00;"}\
[नमो ।अस्तु ते ।अर्क-प्रतिम-प्रभाय नमो ।अस्तु गुह्याय गुहाय तुभ्यम् ॥]{style="color:#99cc00;"}\
[नमो ऽस्तु त्रैलोक्य-भयापहाय नमो ऽस्तु ते बाल कृपापराय ।]{style="color:#99cc00;"}\
[नमो विशालामललोचनाय नमो विशाखाय महाव्रताय ॥]{style="color:#99cc00;"}\
[नमो नमस्ते ।अस्तु मनोहराय नमो नमस्ते ।अस्तु रणोत्कटाय ।]{style="color:#99cc00;"}\
[नमो मयूरोज्-ज्वल-वाहनाय नमो ।अस्तु केयूरधराय तुभ्यम् ॥]{style="color:#99cc00;"}\
[नमो धृतोदग्र-पताकिने नमस्ते नमः प्रभाव-प्रणताय ते ऽस्तु ।]{style="color:#99cc00;"}\
[नमस्ते नमस्ते वर-वीर्य-शालिने कृपापरो नो भव भव्यमूर्ते ॥]{style="color:#99cc00;"}


