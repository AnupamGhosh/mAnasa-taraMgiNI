
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Packing constants for polygonal fractal maps](https://manasataramgini.wordpress.com/2019/03/10/packing-constants-for-polygonal-fractal-maps/){rel="bookmark"} {#packing-constants-for-polygonal-fractal-maps .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 10, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/03/10/packing-constants-for-polygonal-fractal-maps/ "6:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Among the very first programs which we wrote in our childhood was one to generate the famous Sierpinski triangle as an attractor using the "Chaos Game" algorithm of Barnsley. A couple of years later we returned to it generalize it as a map in the complex plane. Consider the polynomial equation,

 $$z\^m+1=0$ , where integer  $m=3,4,5...$$ 

The roots of this equation,  $z_j: z_1, z_2... z_m$ , define the vertices of a  $m$ -sided polygon in the complex plan. For example, if  $m=3$ , we get an equilateral triangle defined by the roots  $z_1= \tfrac{1}{2}+\tfrac{\sqrt{3}i}{2}, z_2= -1, z_3= \tfrac{1}{2}-\tfrac{\sqrt{3}i}{2}$ .

With this in place the Chaos Game map is defined for a given  $m$  as:

 $z_{n+1}=r(z_n+z_j)$ ,

where  $z_j$  is one of the  $m$  roots chosen randomly with equal probability as the others in each iteration of the map and  $0< r\le 1$ . If  $r=1$  for any  $m$  we get a random-walk structure (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg06_1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


For other  $m, r$  we get chaotic maps and for particular values of  $m, r$  we get attractors with a fractal structure. Thus, the Sierpinski triangle is obtained with  $m=3, r=\tfrac{1}{2}$ . This fills the triangle defined by  $z_1, z_2, z_3$ 
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg03.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


For  $m=4, r=\tfrac{1}{1+\sqrt{2}}$  we get the fractal street block (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg04.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


We currently revisited this map because of a curious problem that emerges when we continue this operation as below for further polygons. For  $m=5, r=1-\tfrac{1}{\phi}$  (where  $\phi=\tfrac{1+\sqrt{5}}{2}$ , the Golden Ratio) we get the fractal pentagons surrounding the interior penta-flake (Figure 4).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg05.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


For  $m=6, r=\tfrac{1}{3}$  we get the fractal hexagons surrounding the interior Koch's snowflake (Figure 5).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg06.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


For  $m=7, r=\tfrac{1}{S}$  (where  $S=2+2\cos\left(\tfrac{2\pi}{7}\right)$ , the Silver constant. The [Silver constant](https://manasataramgini.wordpress.com/2017/06/01/constructing-a-regular-heptagon-with-hyperbola-and-parabola/) is an algebraic number which is the real root of the cubic  $x^3-5x^2+6x-1$ ) we get the fractal heptagon necklace surrounding the interior hepta-flake (Figure 6).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg07.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


For  $m=8, r=\tfrac{1}{2+\sqrt{2}}$  we get the fractal octagon necklace (Figure 7).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg08.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad
```{=latex}
\end{center}
```


For  $m=9, r=\tfrac{1}{2+2\cos\left(\pi/9\right)}$  we get the fractal nonagon necklace (Figure 8).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg09.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 8\quad
```{=latex}
\end{center}
```


For  $m=10, r=\tfrac{1}{1+2\phi}$  we get the fractal decagon necklace (Figure 9).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg10.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 9\quad
```{=latex}
\end{center}
```


For  $m=11, r\approx 0.2209$  we get the fractal hendecagon necklace (Figure 10).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg11.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 10\quad
```{=latex}
\end{center}
```


For  $m=12, r=\tfrac{1}{3+\sqrt{3}}$  we get the fractal dodecagon necklace (Figure 11).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg12.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 11\quad
```{=latex}
\end{center}
```


Given that these attractors are fractal, they have an infinite perimeter but occupy a finite area. Thus, one can define a common feature for the above attractors namely "tangency" of the fractal elements, i.e., each polygonal unit is distinct from its neighbor with the same scale-factor but at same time makes a contact with it like a tangent. While for the triangle and the square this definition is a bit murky, it is clearly visible from the pentagon onward. It can be contrasted with other fractal attractors obtained by this method where the elements overlap or are shared. For instance for  $m=12, r=\tfrac{1}{2+2\cos\left(\pi/6\right)}$ , we have the expected dodecad structure but two dodecad sub-elements are shared by each of the adjacent elements (Figure 12).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/chg_12_wheel_time.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 12\quad
```{=latex}
\end{center}
```


In the above examples (Figure 2-11) we have determined for each polygon the value of  $r$  which results in a tangent attractor:

 $$m=3, r=\tfrac{1}{2}; m=4, r=\tfrac{1}{1+\sqrt{2}}; m=5, r=1-\tfrac{1}{\phi}; m=6, r=\tfrac{1}{3};\\ m=7, r=\tfrac{1}{S}; m=8, r=\tfrac{1}{2+\sqrt{2}}; m=9, r=\tfrac{1}{2+2\cos\left(\pi/9\right)}; \\ m=10, r=\tfrac{1}{1+2\phi}, ; m=11, r\approx 0.2209; m=12, r=\tfrac{1}{3+\sqrt{3}}$$ 

For  $m=5..8$  it is the reciprocal of  $2+2\cos\left(\tfrac{2\pi}{m}\right)=4\cos^2\left(\tfrac{\pi}{m}\right)$ , which are known as Beraha constants (  $B_n$ ) that appear in graph-coloring theory. However, for  $m=9..12$  this principle clearly breaks down: e.g. for  $m=12$ , we see that  $\tfrac{1}{B_{12}}$  yields the overlapping attractor (Figure 12). We have been able to obtain closed forms for the  $r$  that yield tangent attractors for all  $m$  except 11, where we report an empirically determined approximate value. What is the closed form expression for it? This leads to the question of whether there is a general formula to obtain our  $r$  that results in tangency? To our knowledge these have not been answered.

