
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Newton's cows](https://manasataramgini.wordpress.com/2019/01/07/newtons-cows/){rel="bookmark"} {#newtons-cows .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 7, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/01/07/newtons-cows/ "12:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Cultures with an Indo-European background have had a long history of symbiosis with the bovine animal since they started herding on the steppes in the Black Sea-Caspian region. Indeed, the very emergence of the modern steppes of Eurasia is likely a result of human-animal action to foster a certain pattern and type of grass growth. Hence, not surprisingly, cows frequently appear even in their mathematical literature, like the Greek problem of cows attributed to Archimedes or नारायण's cow population problem. Even after the destruction of Indo-European tradition by Abrahamism in groups like the English such problems persisted and once such example is Newton's problem from his *Arithmetica universalis*. It goes thus:\
 $a_1$  cows graze  $b_1$  fields bare in  $c_1$  days,\
 $a_2$  cows graze  $b_2$  fields bare in  $c_2$  days,\
 $a_3$  cows graze  $b_3$  fields bare in  $c_3$  days,\
What relationship exists between the 9 quantities from  $a_1, a_2, a_3...c_3$ ?

To solve this we must make some assumptions that Newton indicates in his work: 1) On an average the cows and fields are equivalent. That would mean that we can take each cow to eat the same amount  $w$  daily and each field to have the same type and amount of grass  $x$ . Grass, ungulates and fungi are in a complex relationship. Grass "hire" fungal symbionts to produce toxins like the ergot alkaloids to deter ungulates. There is some empirical evidence that grazing by ungulates triggers grass growth. So we get the assumption: 2) The grass is not at standstill while being grazed daily but is growing back at a daily rate of  $y$ .

From the above, for the first set of cows and fields, we have  $b_1x$  as the total amount of grass at the start of the grazing. The amount of grass growing on the field in day 1 would be  $b_1\times 1 \times y$ . The total amount of grass eaten by the  $a_1$  cows at the end of the day would be  $a_1\times 1 \times w$ . Thus, we can calculate the amount of grass at the end of day 1 as:  $b_1x +1b_1y - 1a_1w$ \
At the end of day 2 we get:  $b_1x+2b_1y-2a_1w$  and so on.\
Hence, when the fields are grazed bare in  $c_1$  days we get:  $b_1x+c_1b_1y-c_1a_1w=0$ . By writing  $z=-w$  we get the equation:  $b_1x+b_1c_1y+a_1c_1z=0$ . Similarly, for the other two sets of cows and fields we get the equations:  $b_2x+b_2c_2y+a_2c_2z=0$  and  $b_3x+b_3c_3y+c_3a_3z=0$ . We thus have as set of 3 simultaneous equations in 3 variables:

 $$b_1x+b_1c_1y+a_1c_1z=0$$ 

 $$b_2x+b_2c_2y+a_2c_2z=0$$ 

 $$b_3x+b_3c_3y+c_3a_3z=0$$ 

We can hence eliminate  $x, y, z$  using the determinant of the system  $\det A= 0$ 

 $$\det A = \begin{vmatrix} b_1 & b_1c_1 & a_1c_1 \\ b_2 & b_2c_2 & a_2c_2 \\ b_3 & b_3c_3 & a_3c_3 \end{vmatrix} =0$$ 

If one wants to avoid  $\det A=0$  being obtained for solutions: 1) where the amount of grass eaten by a cow is  $w=0$ ; 2) negative values for  $y$  or  $w$  we have to set further constraints:

 $$a_1, a_2, a_3...c_3> 0$$ 

 $$c_2> c_1$$ 

 $$a_1b_2-a_2b_1> 0$$ 

 $$a_2b_1c_2-a_1b_2c_1> 0$$ 

In Newton's original numerical example the matrix of the 9 values is:

 $$\begin{bmatrix} a_1 & b_1 & c_1 \\ a_2 & b_2 & c_2 \\ a_3 & b_3 & c_3 \end{bmatrix} = \begin{bmatrix} 36 & 10 & 12 \\ 63 & 30 & 27 \\ 162 & 108 & 54 \end{bmatrix}$$ 

This yields:

 $$\det A = \begin{vmatrix} 10 & 120 & 432 \\ 30 & 810 & 1701 \\ 108 & 5832 &8748 \end{vmatrix} =0$$ 

There seems to have been a error in Newton's original copy of this problem which he corrected late in his life. A question that comes to us is: Is there some easy algorithm for generating such valid integer nonads and is there some pattern to them?

