
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion: the वायवीयं Kaubera-vratam](https://manasataramgini.wordpress.com/2019/07/09/matters-of-religion-the-vayaviya%e1%b9%83-kaubera-vratam/){rel="bookmark"} {#matters-of-religion-the-वयवय-kaubera-vratam .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 9, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/07/09/matters-of-religion-the-vayaviya%e1%b9%83-kaubera-vratam/ "4:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The bus came to halt at the underground stop. Vrishchika of pretty tresses got off the bus and walked up the stairs to reach the over-bridge. She reached the middle of the bridge and stood there looking out expectantly at the road. It was a remarkably clear day and she could see the yonder horizon where the azure heights of father Dyaus met his lover mother पृथिवी clad in the verdant vesture of the distant woods. Vrishchika passed into a reverie at that sight and remained oblivious to the throng of people who criss-crossed the bridge in either direction. Suddenly, an ugly man sidled up beside her and before she could react to his presence he said: "Ma'am do you visit the Hindu temple at शुक्रमार्ग?" Vrishchika: "On rare occasions." The man responded: "We need wealthy patrons like you to visit the temple more often and make donations. Please do so देवी". So saying he handed a pamphlet to her. Vrishchika wondered: "A soothsayer at the temple of the वायु-born वानर had once told my family that I would be the wealthiest of all my sisters. But I'm hardly in that state now. Yet, why does he even think I'm wealthy?" She snapped out of that train of thought and waved the ugly man away saying "शुभम् astu! It is far way from my place but I'll try."

She continued to look out at the road and not seeing what she wanted to see, she turned to the pamphlet. It was crowded with temple events. There was a Rudra-पूजा for the 13th प्रदोष and a विष्णु-पूजा for the एकादशी. There was another event for विनायक, for which there was a call for people to join in a mass-recitation of the गणेशोपनिषत्. Yet another corner mentioned a वटकिनि-mahotsava for the fierce ape. Then there was some announcements of secular events. A professor of Indian origin, who was popular with the people at the local chapter of the "right-wing debate club", with a penchant for peddling false etymologies and outrageous counter-factual theories was giving a lecture series covering issues such as the autochthonous origin of Aryans, reading Vedic mantra-s in the Harappan inscriptions, and situating the महाभारत at earlier than 5000 years in India. Vrishchika said to herself: "I'm sure the पुरुष would find this buffoon quite amusing where it not for the pernicious consequences it has on our people." Then there was a call for people to attend classes for language acquisition or learning the recitation of five सूक्त-s from the कृष्ण-yajurveda. She noticed that the latter was organized by a बाबा-crazed young lady who had been her patient.

Just as her eyes wandered to the next item on the pamphlet the corner of her vision lit up with the sight of a bus halting on the road below and Indrasena saunter out of it. Upon meeting midway at the bridge the two walked ahead to towards the station to board the train for a long ride back home. Vrishchika: "Dear आत्रेय, when I was waiting on the bridge, I received this temple pamphlet from a man who, if my vague recollection is correct, evidently supplements his income by playing a horoscopist and palm-reader at the temple on शुक्रमार्ग. What is interesting is that just as I caught sight of you I was reading this little announcement here." Saying so she handed it over to Indrasena. Even as he was looking at it she articulated it: "It looks as though this banker Atigupta साधु is commissioning the consecration of an image of the यक्षराट् in the northern niche of the temple on the एकदशी of next month. It reminded me that you have yet to impart to me the rahasya of the एकदशी-vrata that you had mentioned long ago in your hometown." Indrasena: "That's quite remarkable. There is actually more than what meets the eye here. Indeed, it was in my mind too that I convey the विद्या to you, O daughter of an अङ्गिरस्, for after all I'd need a दीक्षित-पत्नी when I impart it to the आचार्य who will be doing that स्थापना."

Vrishchika: "That's interesting. So you were aware of this?"\
Indrasena: "Yes Alini. There is a long story here that I've not had the chance to tell you."\
Vrishchika: "I'm now very curious to hear it all."\
Indrasena: "Atigupta साधु was the student of a charismatic मन्त्रवादिनी from the regions of महाकाल. He had received a magic square of राजराज from her and consequently his wealth greatly expanded as befits a V3. He wanted to install an image of Dhanada at the temple at शुक्रमार्ग. He employed none other than the initial teacher of mine the वङ्गसिंह, for whom there is no "ततः kim?" after "धनं meru-tulyam" and he was almost half an Uesugi Kenshin of the प्राच्य-s. But, as I've told you before, he only possessed incomplete विद्या-s. Thus, his स्थापना failed like that of Skanda by the मराठा chief रघुनाथराउ and the image was damaged when lightning struck the temple. The V3 was distraught and went to his teacher back in मालव. She happened have studied some शास्त्र-सिद्धान्त under Lootika. She then consulted your sister, who not wanting to give out my identity publicly told her that if वित्तेश were to be pleased with the said V3 he would find the right V1 who will solve his समस्या. The वणिज् went around for a while until he circuitously got introduced to me by Somakhya. I told the V3 that I could not be a स्थापकाचार्य at a temple but I could impart a key vrata to the वैष्णव स्थापक at the temple who would then achieve success with the endeavor upon performing the vrata. This, my pretty girl, is the whole story. The vrata is none other than the one of the एकादशी that I'll expound to you once we are home."

Back home, taking in the good night air and the dark canopy of the नक्षत्र-s, Indrasena expounded to his wife the rahasya-s of the वायवीयं Kaubera-vratam: "Dear गौतमी, first you must know why it is called the वायवीयं Kaubera-vratam and why the एकादशी is considered the day of the यक्ष lord. In this regard the following has been narrated by the ब्राह्मण महातपास् to king प्रजापाल in the वराहपुराण:

[महातपा उवाच:]{style="color:#000080;"}\
[शृणु चान्यां वसुपतेर् उत्पत्तिं पापनाशिनीम्।]{style="color:#000080;"}\
[यथा वायु-शरीरस्थो धनदः सम्बभूव ह ॥]{style="color:#000080;"}\
[आद्यं शरीरं यत् तस्मिन् वायुर् अन्तःस्थितो 'भवत् ।]{style="color:#000080;"}\
[प्रयोजनान् मूर्तिमत्त्वम् आतिष्ठत् क्षेत्र-देवता ॥]{style="color:#000080;"}\
[तत्र मूर्तस्य वायोस् तु उत्पत्तिः कीर्तिता मया ।]{style="color:#000080;"}\
[तां शृणुष्व महाभाग कथ्यमानां मया +अनघ ॥]{style="color:#000080;"}\
[ब्रह्मणः सृष्टि-कामस्य मुखाद् वायुर् विनिर्ययौ ।]{style="color:#000080;"}\
[प्रचण्ड-शर्करा-वर्षी तं ब्रह्मा प्रत्यषेधयत् ॥]{style="color:#000080;"}\
[मूर्तो भवस्व शान्तश् च तेनोक्तो मूर्तिमान्-भवत् ।]{style="color:#000080;"}\
[सर्वेषाम् eva देवानां yad वित्तं phalam eva ca |]{style="color:#000080;"}\
[तत् सर्वं पाहि येनोक्तं तस्माद् धनपतिर् भवान् ॥]{style="color:#000080;"}\
[तस्य ब्रह्मा ददौ तुष्टस् तिथिम् एकादशीं प्रभुः ।]{style="color:#000080;"}\
[तस्यां अनग्नि-पक्वाशी यो भवेन् नियतं शुचिः ।]{style="color:#000080;"}\
[तस्यापि धनदो देवस् तुष्तः सर्वं प्रयच्छति ॥]{style="color:#000080;"}\
[एषा धनपतेर् मूर्तिः सर्व-किल्बिष-नाशनी ।]{style="color:#000080;"}\
[य एतां शृणुयाद् भक्त्या पुरुषः पठते 'पि वा ।]{style="color:#000080;"}\
[सर्व-कामान् अवाप्नोति स्वर्ग-लोकञ् च गच्छति ॥]{style="color:#000080;"}

The sage महातपस् narrated this legend to king प्रजापाल:\
Now, listen to that other sin-destroying tale of the origin of the lord of wealth, regarding how the wealth-giver arose from the body of the god वायु. Within what was the primordial body, वायु came into being situated in its interior. For the purpose of assuming a discrete form the deity of the regions [वायु] emerged forth. Now, the origin of that form of वायु will be made known by me. O sinless and most fortunate you ought listen that narrated by me. Due to the god ब्रह्मा's creative desire वायु issued forth from his face. \[He emerged] with a tremendous shower of grit/gravel. ब्रह्मा restrained him by saying: "You should take on a pacific discrete form." He took the form as stated by ब्रह्मा. [ब्रह्मा said]: "all this wealth of the gods, whatever they produce, you must indeed protect it". Thus, due to what was said by ब्रह्मा he became Dhanapati. The pleased lord ब्रह्मा gave the एकादशी \[as his] lunar day. On that day, if one with discipline purifies himself and only eats food which is not cooked by fire, then the god Dhanada is pleased and gives him everything. This form of Dhanapati destroys all that is ill. If one verily listens to this narrative with devotion or the person recites it, then he attains all desires and goes to the heavenly realm.

Now, Vrishchika tell me what you think of this narrative?"

V: "I believe it had its roots in the ancient Indo-Iranian system of the supreme वायु, with a प्राजापत्य overlay atop that. Indeed Kubera and वायु simultaneously figure as mighty deities in the backdrop of the रामायण and to some extent also in the महाभारत. Hence, their connection does not seem unexpected. Moreover, along with the other deity of the same class, Rudra, the three of them adorn the northern face of the dik-मण्डल --- NE-N-NW. The primordial body referred to in this narrative is verily the पुरुष of the श्रुति. There indeed it has been said '[प्राणाद् वायुर् अजायत ।]{style="color:#000080;"}' and वायु was seen as the breath within the पुरुष. In this myth we see an allusion the the role of वायु in the creative process. It is amply clear from the श्रुति that in the old system focused on वायु he was conceived as playing a central role in this process. Indeed, in the mantra to वायु-वात we hear:\
['आत्मा देवानाम् भुवनस्य गर्भो]{style="color:#000080;"}\
[यथा-वशं चरति देव एषः ।]{style="color:#000080;"}\
[घोषा इद् अस्य शृण्विरे न रूपं]{style="color:#000080;"}\
[तस्मै वाताय हविषा विधेम ॥']{style="color:#000080;"}\
The breath of the gods, the germ of the universe,\
this god wanders as he wishes.\
Only his sounds are heard, not \[seen is] his form.\
For him, for वात, we would do honor with our oblation.

We see here that the deity is presented as the germ of the universe (reflective of the creative process) and his formlessness which is alluded to in the myth you narrated is also a central element of this mantra. Tellingly, the very first mantra of the सूक्त captures aspects of his storminess mentioned in this myth as he emerged from the face of ब्रह्मा with a shower of grit.:\
['वातस्य नु महिमानं रथस्य]{style="color:#000080;"}\
[रुजन्न् एति स्तनयन्न् अस्य घोषः ।]{style="color:#000080;"}\
[दिविस्पृग् यात्य् अरुणानि कृण्वन्न्]{style="color:#000080;"}\
[उतो एति पृथिव्या रेणुम् अस्यन् ॥']{style="color:#000080;"}\
Now for the greatness of वात and his chariot:\
smashing he proceeds; thundering is his noise.\
Touching heaven as he drives, making things red,\
also blowing up dust from the earth he goes.

Thus, I would conclude that at the foundation of the वराह-पुराण narrative are clear motifs coming from the old वायु-centric system such as those expressed in the सूक्त I just mentioned."

I.s: "That is good, Vrishichika. It touches on something which will come up again as I expound the ritual itself. One may also note the this idea of the वायु within the primordial body might have been behind another somewhat mysterious name of the god --- मातरिश्वन् --- he who grows within the mother or womb. Thus, it is likely that what was meant in this name was वायु filling up the interior of the 'primordial womb'. His emergence from the mouth of the protogonic deity is something that evidently had a deep history: While in the पुरुष-सूक्त it is Indra and Agni who emerge from the पुरुष's mouth, we have a bauddha text the अवलोकितेश्वर-गुण-करण्डव्यूह where the नास्तिक-s have appropriated the mythologem of the पुरुष for their invention, the अवलोकितेश्वर. In that text we hear of the various gods emerging from the different organs of the macranthropic अवलोकितेश्वर and वायु is specifically mentioned as emerging from his mouth. This suggests the existence of an old आस्तिक source with such an imagery from which this concept was borrowed by the bauddhas.

Now further, while in the वराह-पुराण myth the motifs acquired from the वायु-focused system may have been placed in a प्राजापत्य context typical of the earlier पौराणिक narratives, we must not miss the fact that right from the beginning वायु had an intimate connection with the protogonic deity with roots likely going back to the para-Vedic systems of the Indo-Iranian period with evolutes lasting until much latter on both the Indian and Iranian sides. This is already apparent in the प्राजापत्य portions of the late श्रुति. In the शतपथ-ब्राह्मण we hear: '[स एष वायुः प्रजपतिः ।]{style="color:#000080;"}' (This वायु is प्रजापति). It is further expounded in the कौशीतकि-ब्राह्मण that the manifest form of प्रजापति is वायु: '[प्रजापतेः प्रत्यक्सं रुपं यद् वायुर् इति।]{style="color:#000080;"}'. The शतपथ-ब्राह्मण goes on to even present प्रजापति as a composite of himself and वायु: '[अर्धं ह प्रजापतेर् वयुः । अर्धम् प्रजपतिः ।]{style="color:#000080;"} Regarding this intimate connection between the two deities, I could point to a rather late survival of this idea in the गरुड-पुराण (मोक्ष-काण्ड) in a rather pristine form albeit lodged within a larger, quite unwholesome वैष्णव section, which tries to establish the superiority of विष्णु over the other gods:\
['अन्नाभिमानं ब्रह्म चाहुर् मुरारिं]{style="color:#000080;"}\
[जीवाभिमानं वायुम् आहुर् महान्तः ।]{style="color:#000080;"}\
[न शक्तो 'सौ ब्रह्म-देवो विवस्तुं]{style="color:#000080;"}\
[वायुं विना संसृताव् एव नित्यम् ॥]{style="color:#000080;"}\
[न तं विना मातरिश्वा च वस्तुम्]{style="color:#000080;"}\
[अन्योन्यम् आप्तिः कालतो न्यूनता च ।]{style="color:#000080;"}\
[यदा महत्-तत्त्वनि यामको भूद्]{style="color:#000080;"}\
[ब्रह्माण्डान्त-स्थूल-सृष्टौ महात्मा ॥]{style="color:#000080;"}\
[तदा वायुर् नाशकद्वै महात्मा]{style="color:#000080;"}\
[बाह्ये सृष्टौ कालभेदेन चास्ति ।']{style="color:#000080;"}\
The great sages have declared that ब्रह्मा and मुरारि (विष्णु) are the nutritive principle, \[whereas] वायु is the metabolic principle. Verily, this god Brahma does not have ever the ability to emit the universe without वायु. Without him (ब्रह्मा) मातरिश्वन् (वायु) does not have place to dwell. (O great soul), their relative strengths and wants depends on the course of time. When [ब्रह्मा] restrained of the principles \[evolving from] mahat in the \[primordial] world-egg from manifesting as material creation वायु could not set it motion. The evolution of the manifest universe \[by वायु] happens only with passage of time.

Thus, Alini, in this system वायु and ब्रह्मा are close and almost equal partners in the manifestation of the material universe. Indeed, it appears that these ideas were preserved by a group of late-surviving वायु-focused thinkers who later converted to the वैष्णव-mata in southern India. This becomes apparent from a discussion occurring earlier in this section of the गरुड-पुराण wherein the votaries are asked to set up an image of वायु and worship it. However, it is then clarified that वायु is merely a symbol of विष्णु and that वायु should only be offered the उच्छिष्ट of विष्णु. Notably, it warns votaries who offer fresh flowers, sandalwood and incense to वायु and says that they would eternally suffer unless they meditate on विष्णु as an expiation. The last statement clinches their वायु-focality prior to their conversion. Their unconverted versions still exist, albeit in a degenerate form, in the लाट country --- a point we will revisit in the context of the परम्परा of our ritual."

V: "Ah! Having sat in on a discussion Lootika and Somakhya once had, I do see the parallels to the comparable Iranic systems. In the Zoroastrian stream of the Iranic tradition, despite all the demonization of the ancestral deva-s, वायु still retains his primacy. In the yasht of Rama Hvastra even Ahura मज़्दाह् worships वायु to aid him in creation by countering Angra Mainyu. In the para-Zoroastrian system of ज़ुर्वान् (Avestan: Zrva, cognate of Sanskrit sarva) he comes as an integral partner of the great time deity ज़ुर्वान् who has a fundamentally protogonic character. Thus, ज़ुर्वान्, associated with time and वायु associated with space are the heart of the genesis of this Iranic system. The integral association of ज़ुर्वान् and वायु is already seen in the Zoroastrian Avesta where they are invoked together and are supplied with the epithet 'deregho ख्वधात (दीर्घ-स्वधातृ in our tongue)'. i.e. those who have long-standing law of their own. That ज़ुर्वान् was an old deity with his own system is seen from the occurrence of his name close to the Avestan form in the Nuzi documents from Iraq dating to around 3200-3400 years ago where we find some of the earliest attestations of the Iranians in archaeology. This association is also recorded by Eudemus of Rhodes, the student of Aristotle, who notes that the Iranians regard Time (ज़ुर्वान्) and Space (वायु) as the foundational principles of the universe. That he was referring to the para-Zoroastrian ज़ुर्वान् worshipers becomes apparent from a comparable system presented in ज़ुर्वानिस्तिच् Iranic traditions preserved by the Zoroastrians in their Denkart, where Zurvan as time is the generative principle and वायु provides the space for the entities that come into being due to Zurvan. There, वायु is also the one who conveys the good things bestowed by ज़ुर्वान् to men --- this perhaps captures a bit of his nature as the messenger, which we see in his मातरिश्वन् form in our tradition. Thus, his association with the Iranic protogonic deities, be he Ahura मज़्दाह् or Zrva, in the generative process is indeed a parallel to his association with ब्रह्मा in our tradition."

I.s: "As we move from वायु, who was specifically hymned by your ancestor वामदेव, to Kubera, we again find these parallels between Indo-Aryan and Iranian tradition as illuminating their association. Clearly in our tradition वायु-वात has both a benign and malignant nature. In the mantra you mentioned he is described as `'rujan' i.e. smashing. This nature is clearly brought out in his earthly epic manifestations, the ape हनूमत् and the पाण्डव भीम. On the other hand, as you correctly mentioned, in the Iranic tradition वायु conveys the good things of Zurvan to men. This is his benign aspect which comes up in the second such वात-सूक्त in the RV:\
[वात आ वातु भेषजं शम्भु मयोभु नो हृदे ।]{style="color:#000080;"}\
[प्र ण आयूंषि तारिषत् ॥]{style="color:#000080;"}\
[उत वात पितासि न उत भ्रातोत नः सखा ।]{style="color:#000080;"}\
[स नो जीवातवे कृधि ॥]{style="color:#000080;"}\
[यद् अदो वात ते गृहे 'मृतस्य निधिर् हितः ।]{style="color:#000080;"}\
[ततो नो देहि जीवसे ॥']{style="color:#000080;"}\
May वात blow towards us a medicine, which is auspicious and conferring weal to our heart. May he lengthen our lives.\
O वात, you are father to us, and brother, and also a friend to us. Make us live long.\
O वात, what wealth of ambrosia has been placed yonder in your house, give us of that for us to live.\
Here, we see that the benign aspect of वायु is emphasized with him being a bearer of medicines (note a similar role played by हनूमत् in the रामायण), fellowship and the ambrosia. Importantly, note the [अमृतस्य निधिः]{style="color:#000080;"}, which becomes important for the connection with the महाराज. On the Iranian side वायु again is invoked for benign gifts such as protection from यातुधान-s, a long life, offspring, and by women for a good husband. On the other-hand, he is also the fierce 'pitiless वायु', the bone-breaker (अस्तोविधातु) who slays the man (the bone-crushing facet is well represented in the earthly emanations of our वायु, हनूमत् and भीम, who more generally also have a certain malignant side to them, which, O Gautami, your ancestors are said to have faced in the case of the former). Thus, in later Iranic tradition we hear of two वायु-s, the वाय् i veh the good वायु and the वाय् i vattar, the evil वायु, who in the Iranic text known as the greater Bundahishn is explicitly termed अस्तोविधातु who causes death.

Thus, even in our origin myth of Kubera, the two facets of वायु are clearly laid out. He originally emerges in what is evidently his malefic form showering gravel/grit and then assumes a benefic form which becomes Kubera. Notably, Kubera, like वायु in the RV mantra, also holds the nidhi of अमृत, which is described as the honey of Jambhala, known to the V1s who have mastered the Kaubera lore, in the महाभारत."

V: "Dear आत्रेय, may be in the regard I should add that this benign form of वायु, which becomes वैश्रवण might relate to the magical concealment power of the यक्ष-s, तिरोधा that is alluded to multiple times in the श्रुति. On one hand it connects Kubera with the primordial goddess विराज्. The पुरुष hymn, echoing the cyclic generation of the primordial god दाक्ष and goddess Aditi from each other, states that the goddess विराज् arose from the पुरुष and that conversely the पुरुष arose from विराज्. The अथर्वाङ्गिरस-श्रुति adds that Kubera was a calf of this goddess विराज् in the form of तिरोधा which conferred the concealment (तिरोधा) capacity on his folk (i.e. the यक्ष-s). This is again mentioned and worshipped in his ritual in the तैत्तिरीय-श्रुति. In the AV it is said that he who knows this thing about Kubera causes his पापमन् to vanish, whereas in the TA it makes his enemies disappear, like the प्राच्य Uesugi Kenshin experienced with his devotions. This again reflects the beneficent nature of this deity whose origin is from the primordial being. Perhaps the तिरोधा power, which the तैत्तिरीयक-s describe as coming from the waters represents, the benefic clouds brought by वायु as opposed to the destructive sand storm.

Finally, before we go to the ritual, O Atri, I would like to to ask you if you can think of any parallels to the connection between वायु and वैश्रवण which might exist in the Iranian world?"

I.s.: "Firstly, O आङ्गिरसी, it is pleasing to see that you have thoroughly acquired the श्रौत-सिद्धान्त of पुण्यजनेश. Regarding your question --- I am aware of no direct cognate of Kubera in the early Iranic world. However, we may note the following: interestingly, even in the Avesta the benign वायु is described as being associated with gold, a characteristic of the यक्षराट्. Subsequently, among the non-Zoroastrian and para-Zoroastrian Iranics of the कुषाण empire it appears that the personification of royal power, Avestan Khvarena (कुषाण: Pharro), came to identified with वैश्रवण. This is not surprising given both their association with royal power. This led to syncretic depictions in the कुषण age that appear to have eventually reached the eastern steppes whose new masters were the Turkic and Mongolic people. This greatly influenced the development of the Kubera-like deity Kut-Tengri among the Uighurs and other central Asian Turks."

V: "Now, O Atricandra, expound to me details of the ritual --- the observances, actions and the mantra-s."\
I.s: "Sure. The vrata, as has been specified, should be done on an एकादशी, either in the afternoon or in the evening. After having had a bath the ritualists should observe purity and abstain from sexual activity for the entire day. They should eat air-dried fruits and nuts. They should abstain from consuming milk products but they may drink fresh fruit juices. In old भारत the ideal spot for the ritual was said to be under an अश्वत्थ or a वट tree but these days such spots are difficult to come by.

First the ritualists should offer तर्पण to the key teachers of the lineage: the founder was a sage of the वैखानस clan. That knowledge then passed on to the queen वासवदत्ता. Her son the renowned नरवाहनदत्त was next. His minister Gomukha was next. These were the teachers of the lore at कौशाम्बी. Then the lore was taught at the holy city of मथुरा, where all the great dharma-s were expounded by the mantra-vit-s. As you know both Somakhya and I trace our lineages to घरांनाय-s of that holy city. There it was taught in the center known as the यक्ष-गुहा, which is today Maholi near the मथुरा city. Here the teachers were Gomitraka and Kunika. From there the tradition moved to Gopagiri (modern Gwalior), where it was taught by your old clansmen, the Gotama रुद्रदास and his son शिवत्रात. From them it was transmitted to शिवनेमि, शिवभद्र and Kubhaka." From there it was transmitted along the Candravati river (modern West Banas) to the V1 मणिभद्र who lived in वाय्वग्रहार (a little hamlet near modern Banaskantha) in the लाटानर्त country. He transmitted it to his students Kuberadatta and यक्षमित्र who lived at वायुवट (modern Vayad). All these V1s were special worshipers of वायु at his प्रासाद-s that were once prominent in the region and the rite we perform takes its form in their teachings.\
V: "It is indeed rather remarkable that even today we have nearly forgotten remnants of the old cult at these key cultic centers, like the वायु temples in लाट and the वित्तेश near a step-well in their vicinity, or the images of नलकूबर at Gopagiri and the fragment of the old यक्षराट्-प्रासाद at कौशाम्बी."

I.s: "If the यक्ष is good to us, may be some day we shall visit those cultic centers to revive our connection. To continue, the ritualist then salutes his teachers: '[ॐ गुरुभ्यो नमः ।]{style="color:#000080;"}'. The spot of the rite should be to the north of the trunk of the said tree for indeed the northern line is where वायु, वित्तेश and Bhava, all deities of the rite, are stationed. The ritualist begins by uttering the यजुष्:\
[इष्é त्वोर्ज्é त्वा वाय्áवः स्थोपाय्áवः स्थ देव्ó वः सविता́ प्रा́र्पयतु श्र्éष्ठतमाय क्áर्मणे ।]{style="color:#000080;"}\
For vigor you; for nourishment you; the वायु-s abide, the approachers abide. Let the god सवितृ set you in motion for the most excellent ritual.

Vrishchika, here you may note the multiplicity of the वायु-s reminiscent of the Aeolian plurality seen among our yavana cousins. Then he stands up and recites the following ऋक्-s:\
[स्त्óम-त्रयस्त्रिंशे भ्úवनस्य पत्नि व्íवस्वद् वाते अभ्í नो गृणाहि ।]{style="color:#000080;"}\
[घृत्áवती सवितर् आ́धिपत्यैः प्áयस्वती र्áन्तिर् आ́शा नो अस्तु ॥]{style="color:#000080;"}\
[ध्रुवा́ दिशा́ं विष्णुपत्न्य् áघोरास्य्éशाना स्áहसो या́ मन्óता ।]{style="color:#000080;"}\
[बृहस्प्áतिर् मातर्íश्वोत्á वाय्úः संधुवाना́ वा́ता अभ्í नो गृणन्तु ॥]{style="color:#000080;"}\
[विष्टम्भ्ó दिव्ó धर्úणः पृथिव्या́ अस्य्éशाना ज्áगतो व्íष्णुपत्नी ।]{style="color:#000080;"}\
[विश्व्áव्यचा इष्áयन्ती स्úभूतिः शिवा́ नो अस्त्व् áदितिर् उप्áस्थे ॥]{style="color:#000080;"}\
O you of the 33-fold stoma, lady of the world, with winds set in motion by विवास्वान्, may you be gracious to us.\
Rich in ghee, O सवितृ, through your overlordship, may the generous space be rich in milk for us.\
Firm among the quarters, wife of विष्णु, the benign \[lady], the strong queen of all, the desirable one;\
बृहस्पति, मतरिश्वन्, वायु, the वात-s blowing in unison, may all be gracious to us.\
Stabilizer of the sky, supporter of the earth, ruling this world, wife of विष्णु.\
Encompassing all space, invigorating all, prosperous, may the goddess Aditi be auspicious to us in her lap.

He then marks out a square area on the ground with the side equal to his full arm's length and sweeps it clean. Within it he marks out another square region with a side equal to his span. He does this with the sphya (the wooden sword) while chanting the formula:\
[áस्तभ्नाद् द्या́म् ऋषभ्ó अन्त्áरिक्षम् áमिमीत वरिमा́णम् पृथिव्या́ आ́सीदद् विश्वा भ्úवनानि सम्रा́ड् व्íश्वेत् ता́नि व्áरुणस्य व्रता́नि ॥]{style="color:#000080;"}\
The bull has stabilized the sky \[and] the atmosphere; he has measured the circumference of the earth; he has set him in all worlds as the emperor. All these are वरुण's laws.

He then digs out a cuboidal excavation corresponding to the central smaller square. While doing so he recites the यजुष्:\
[व्íष्णोः क्र्áमो 'सि व्íष्णोः क्रान्त्áम् असि व्íष्णोर् व्íक्रान्तम् असि । न्áमो मात्र्é पृथिव्य्áइ मा́ह्áन् मात्áरम् पृथिवी́ँ हिँसिषम् मा́ । मा́ं माता́ पृथिवी́ हिँसीत् ॥]{style="color:#000080;"}\
You are the stepping of विष्णु; you are the step of विष्णु; you are the stride of विष्णु. Salutation to mother Earth; may I not harm mother Earth; may mother Earth not harm me.

The he fills the sruc with ghee using the the sruva and pours out the ghee into excavation uttering the यजुष्:\
[वाय्áवे त्वा व्áरुणाय त्वा न्íरृत्यै त्वा रुद्रा́य त्वा स्वाहा । वा́यो वी́हि स्तोका́नाम् ॥]{style="color:#000080;"}\
For you वायु; for you वरुण; for you निरृति; for you Rudra, hail! O वायु, taste the drops!

He then fills the sruc again with milk pours it into the excavation uttering the incantation:\
[व्áसुर् व्áसुपतिर् ह्íकम् áसि। क्षत्रा́णां क्षत्र्áपतिर् असि । áति दिव्áस् पाहि । वैश्रवणाय स्वाहा॥]{style="color:#000080;"}\
You are indeed rich, the lord of riches. You are the the king of the kings. From the heavenly realm protect us. To वैश्रवण hail."

Indrasena then brought out a bronze image of a chameleon and showed it to Vrishchika and explained further: "The ritualist should have such a bronze image of a chameleon, which as you know is the special animal of the यक्षपति. Alternatively, he may have a metal plate with the image of a chameleon etched on it. He deposits it in the excavation for the rite and may reuse it over and over, each time he performs the rite. He takes up the image reciting the following incantation:\
[इयं वर्णगोधा वैश्रवण-रूपा बहुवर्णिका । तिरोधा 'सि । हिरण्यवक्त्रो 'सि वैश्रवणो 'सि । क्षत्रं क्षत्रं वैश्रवणः । महाराजाय स्वाहा ॥]{style="color:#000080;"}\
This chameleon of many colors is the form of वैश्रवण. You are the camouflaged one. You are the golden-cheeked one; you are वैश्रवण. Kingly power, kingly power \[is that of] वैश्रवण. Hail to the great king!

He then deposits the chameleon in the excavation and with the last mantra of the above he pours out ghee from the sruva on to image. He then covers up the image by filling the excavation. To the north of the square he sets up a platform with a silk cloth on it. On that he should place an ornate bronze jar with a lid. He should thereafter invoke वायु and his wife शिवा in the jar and offers them water for arghya and पाद्य and incense with the following incantation of मार्कण्डेय:\
[वायुम् आवाहयिष्यामि सर्वगं दीप्ततेजसम् ।]{style="color:#000080;"}\
[देव वायो त्वम् अभ्य् एहि सर्व-भूत-जगत्-प्रिय ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke वायु of blazing splendor, who can go anywhere. O वायु, dear to all beings and the world, you must come here. Please accept this arghya, पाद्य (water for washing the feet) and incense.

[शिवाम् आवाहयिष्यामि वायोः पत्नीं प्रभावतीम् ।]{style="color:#000080;"}\
[शिवे 'भ्य् एहि महाभागे वरदे कामरूपिणी ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke शिवा of great luster, the lady of वायु. O highly distinguished goddess शिवा, who can assume any form, the boon-giver, come here. Please accept...

He then places an image of Kubera inside the jar. The image should ideally be fashioned generally following the teaching in this regard of the भार्गव मार्कण्डेय to Vajra, the great-grandson of कृष्ण, the king of Indraprastha: The image of the wealth-giver Kubera should be painted dark green, i.e., the color of a lotus-leaf. He should be shown as being borne by a naras यक्ष. He should have all kinds of golden ornaments with necklaces resting on his belly. He should be shown wearing a beautiful cloak. He should have four hands and one of his eyes should be yellow. He must have two fangs in his mouth, and a mustache and beard on his face. His head is adorned with a jeweled crown. On his left lap, his wife, the beautiful, boon-giving goddess ऋद्धि should be comfortably seated. She should be shown with two hands. One of them should be shown hugging the महाराज and the other should display a jewel-pot. In one of his left hands he should be shown holding a jeweled vessel and a gold-spitting mongoose in the other. In his two right hands Kubera should be shown with a mace and a शक्ति missile. Beside him there should be his dhvaja with the lion-emblem. A palanquin should be shown near his feet and his treasure-chests should be shown beside him in the form of a conch and a lotus filled with treasures.

If he cannot obtain such an image, any other image that is properly made might be used. However, he must use the above teaching of मार्कण्डेय for the ध्यान of the great यक्ष because if he does so he obtains wealth. मार्कण्डेय gives the following symbolism for the image: The lord should be seen as the embodiment of all अर्थशास्त्र-s. It is only by the institutes of अर्थशास्त्र-s does any endeavor of a living-being exist, as was taught by चाणक्य who devised the overthrow of the barbarous Macedonians. Kubera's शक्ति weapon stands for royal power and his गदा represents दण्ड-नीति. Goddess ऋद्धि represents a full worldly life. The jeweled vessel in her hand is the receptacle of good qualities. The kingdom over which the यक्षपति rules is the body itself. The lotus and the conch are the reservoirs of all treasures in nature from which wealth perpetually flows. His lion flag is desire. Hence, he is known as कामेश्वर in the Yajurveda. His two fangs are the reward and the punishment that he metes out. He who mediates thus has Kubera dwell in him.

Having placed the image of the राजराज in the jar he invokes him and his retinue and offers them worship with the following incantations:\
[याक्षान् आवाहयिष्यामि धानाध्यक्ष-समन्वितान् ।]{style="color:#000080;"}\
[आयान्तु वरदा यक्षास् त्रैलोक्य-विदिता मम ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke the यक्ष-s together with the lord of wealth. Let the यक्ष-s renowned in the 3 worlds come to me giving boons. Please accept this arghya, पाद्य (water for washing the feet) and incense.

[भद्राम् आवाहयिष्यामि धनाधिपति-सुन्दरीं ।]{style="color:#000080;"}\
[एहि ऋद्धि महादेवि वरदे काम-रूपिणी ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke the lucky one, the beautiful lady of the lord of wealth. Come, O great boon-giving goddess ऋद्धि of much desired form. Please accept...

[अहम् आवाहयिष्यामि त्वाम् एव नलकूबरम् ।]{style="color:#000080;"}\
[समभ्य् एहि महाभाग धनदस्य+आत्मसंभव ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall verily invoke you O नलकूबर. May you, O fortunate one born of Dhanada, come here. Please accept...

[अहम् आवाहयिष्यामि शिबिकां परमायताम् ।]{style="color:#000080;"}\
[शिबिके त्वम् इहा 'भ्य् एहि सर्व-सत्त्व-सुखङ्करि ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke the wide palanquin of Kubera. O palanquin who generates all good and pleasures may you come here. Please accept...

[नरम् आवाहयिष्यामि नराधिपति-वाहनम् ।]{style="color:#000080;"}\
[नर शीघ्रं त्वम् 'भ्य् एहि तथा नृपतिनायक ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke the Naras यक्ष who is the vehicle of the king \[Kubera]. O Naras may you quickly come here, you have the king as your lord \[Kubera]. Please accept...

[शङ्खम् आवाहयिष्यामि निधि-प्रवरम् उत्तमम् ।]{style="color:#000080;"}\
[शीघ्रं शङ्ख त्वम् अभ्य् एहि धानाधिपति-वल्लभ ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke the conch, the foremost of excellent treasures. May you, O conch, quickly come here, the one dear to the lord of wealth. Please accept...

[पद्मम् आवाहयिष्यामि निधि-प्रवरम् उत्तमम् ।]{style="color:#000080;"}\
[पद्मं शीघ्रं त्वम् अभ्य् एहि महाविभव-कारक ।]{style="color:#000080;"}\
[इदम् अर्घ्यं च पाद्यं च धूपो'यं प्रतिगृह्यताम् ॥]{style="color:#000080;"}\
I shall invoke the lotus, the foremost of excellent treasures. May you, O lotus, quickly come here producing great prosperity. Please accept...

Then he offers flowers, honey and भक्षण-s to the image of Kubera with the famous incantation from the तैत्तिरीय-आरण्यक:\
[राजाधिराजाय प्रसह्यसाहिने । नमो वयम् वैश्रवणाय कुर्महे । स मे कामान् कामकामाय मह्यम् । कामेश्वरो वैश्रवणो ददातु ।]{style="color:#000080;"}\
[कुबेराय वैश्रवणाय । महाराजाय नमः ॥]{style="color:#000080;"}

He should thereafter make the triple-arghya offerings as ordained by the गायत्री-s of वैखानस-s:\
[राज-राजाय विद्महे धनाध्यक्षाय धीमहि । तन् नो राजा प्रचोदयात् ॥]{style="color:#000080;"}\
[रुद्र-सखाय विद्महे वैश्रवणाय धीमहि । तन् नः कुबेरः प्रचोदयात् ॥]{style="color:#000080;"}\
[यक्षेश्वराय विद्महे गदा-हस्ताय धीमहि । तन् नो यक्षः प्रचोदयात् ॥]{style="color:#000080;"}

Then he offers bali to धनेश and नलकूबर meditating upon them traversing the heavens in the पुष्पक space-station made by the god त्वष्तृ which is alluded to in the mantra. This is as per the अरुण-ketuka-vidhi with the incantations:\
[अद्भयस् तिरोधा'जायत । तव वैश्रवणस् सदा । तिरोधेहि सपत्नान् नः । ये अपो 'श्नन्ति केचन । त्वाष्ट्रीं मायां वैश्रवणः । रथँ सहस्र-वन्धुरम् । पुरुश्-चक्रँ सहस्राश्वम् । आस्थायायाहि नो बलिम् । यस्मै भूतानि बलिम् आवहन्ति । धनञ्-गावो-हस्ति-हिरण्यम्-अश्वान् । असाम सुमतौ यज्नियस्य । श्रियम् बिभ्रतो 'न्नमुखीं विराजम् । सुदर्शने च क्रौञ्चे च । मैनागे च]{style="color:#000080;"}[महागिरौ । शत-द्वाट्टारगमन्ता । सँहार्यन् नगरं तव । इति मन्त्राः । कल्पो'त ऊर्ध्वम् । यदि बलिँ हरेत् । हिरण्यनाभये वितुदये]{style="color:#000080;"}[कौबेरायायम् बलिः । सर्वभूतधिपतये नम इति । अथ बलिँ हृत्वोपतिष्ठेत । क्षत्रं क्षत्रं वैश्रवणः । ब्राह्मणा वयँ स्मः । नमस्ते अस्तु मा मा]{style="color:#000080;"}[हिँसीः ॥]{style="color:#000080;"}

After that he makes bali offerings to the रक्ष-s, यक्ष-s and यक्षी-s in the entourage of Kubera with:\
[मिश्र-वाससः कौबेरका रक्षो-राजेन प्रेषिताः ।]{style="color:#000080;"}\
[ग्रामँ स-जानयो गच्छन्तीछन्तो परिदाकृतान् ॥]{style="color:#000080;"}\
Wearing mixed clothes the agents of Kubera, directed by the king of the रक्ष-s together with the [यक्षी] consorts go as they please to the villages granting protection.

There after he sets up a स्थण्डिल in the square he has marked out. He strews darbha grass with the shoots facing north. He installs the fire therein from his औपासन or आहवनीय altar or kindles it afresh. He recites this ऋक्:\
[वैश्वानरं कवयो यज्ञियासो 'ग्निं देवा अजनयन्न् अजुर्यम् ।]{style="color:#000080;"}\
[नक्षत्रम् प्रत्नम् अमिनच् चरिष्णु यक्षस्याध्यक्षं तविषम् बृहन्तम् ॥]{style="color:#000080;"}\
The gods, the poets of the ritual, generated the unaging Agni वैश्वानर,\
the primordial star, wandering but not violating \[the natural law], the mighty and great lord of the यक्ष.

He makes an oblation of ghee with: [अग्नये वैश्वानराय स्वाहा । इदम् अग्नये वैश्वानराय न मम ।]{style="color:#000080;"}

If he is a purohita performing it on behalf of a यजमान other than himself, like at the temple it would be done with the V3, then the यजमान must recite the त्याग-mantra '[इदम्...न मम]{style="color:#000080;"}' as the oblation is offered. Then he recites:\
[धाता́ रात्íः सवित्éद्áं जुषन्ताम् प्रजा́पतिर् निधिप्áतिर् नो अग्न्íः ।]{style="color:#000080;"}\
[त्व्áष्टा व्íष्णुः प्रज्áया संरराण्ó य्áजमानाय द्र्áविणं दधातु ॥]{style="color:#000080;"}\
May धतृ the giver, may सवितृ, rejoice in this \[oblation], प्रजापति, the lord of treasures, and Agni, for us; may त्वष्तृ and विष्णु generously give wealth with offspring to the ritualist.

He makes an oblation of ghee with: [देवेभ्यः स्वाहा । इदम् देवेभ्यो न मम ।]{style="color:#000080;"}

He then prepares for the core oblations of the यज्ञ by deploying the nigada recitation as in the vaidika rite calling upon the god Agni who is of the ब्राह्मण-s and the भारत-s to bring the gods of the rite for receiving the oblations:\
[ॐ अग्ने महाँ असि ब्राह्मण भारत । देवेद्धो मन्विद्ध घृताहवनः प्रणीर् यज्ञानां रथीर् अध्वराणाम् अतूर्तो होता तूर्णिर् हव्यवाट् । त्वं परिभूर् अस्य् आ वह देवान् यजमानाय ॥ वायुं नियुत्वन्तम् आ वह । कुबेरं वैश्रवणम् आ वह । रुद्रम् पशुपतिम् आ वह । देवाँ आज्यपाँ आ वह ॥]{style="color:#000080;"}

He then offers in the northern side of the स्थण्डिल with the sruva:\
[प्रजापत्ये स्वाहा । इदम् प्रजापत्ये न मम ॥]{style="color:#000080;"}

He utters the call '[अस्तु श्रौ३षट् ।]{style="color:#000080;"}' and starts filling he sruc and as he does so he recites:\
[तव वायव् ऋतस्पते त्वष्टुर् जामातर् अद्भुत ।]{style="color:#000080;"}\
[अवांस्य् आ वृणीमहो३म् ॥]{style="color:#000080;"}\
वायु, lord of the natural law, of wondrous form, त्वष्टृ's son-in-law, your protection we choose.

Then he recites:\
[प्र वायुम् अच्छा बृहती मनीषा]{style="color:#000080;"}\
[बृहद्रयिं विश्ववारं रथप्राम् ।]{style="color:#000080;"}\
[द्युतद्यामा नियुतः पत्यमानः]{style="color:#000080;"}\
[कविः कविम् इयक्षसि प्रयज्यो३म् ॥]{style="color:#000080;"}\
[वौ३षट् । इदं वायवे नियुत्वते न मम ॥]{style="color:#000080;"}\
\[Our] great meditation (goes) forth to वायु; with great wealth and all that is excellent, he fills his chariot; on the brilliant path, master of horse-teams, you the poet seeks to reach the poet \[i.e. the hymn-composing vipra], you who are worshiped at the forefront of the ritual.

At the वौषट् call he makes the oblation and then utters the त्याग-mantra. He makes the oblation facing whichever direction the wind blows at the time he is reciting the this mantra. He utters the call '[अस्तु श्रौ३षट् ।]{style="color:#000080;"}' and starts filling he sruc and as he does so he recites:\
[रायस् पोषायायुषा त्वा निधीशो भ्रातृव्याणां महसां चाधिपत्यो३म् ।]{style="color:#000080;"}\
\[I invoke you] for riches and prosperity with a long life. The lord of wealth \[is worshiped] for taking over the glory of our rivals and for overlordship.

This peculiarly-formed mantra is that of the old Jambhala-vit वैखानस, the master of divine rituals. Then he recites:\
[दूरे पूर्णेन वसति दूर ऊनेन हीयते ।]{style="color:#000080;"}\
[महद् यक्षं भुवनस्य मध्ये]{style="color:#000080;"}\
[तस्मै बलिं राष्ट्रभृतो भरन्तो३म् ॥]{style="color:#000080;"}\
[वौ३षट् । इदं कुबेराय वैश्रवणाय न मम ॥]{style="color:#000080;"}\
In fullness he dwells in the distance, is left behind in distance on diminishing;\
A mighty यक्ष is in the center of the universe:\
to him the tributary rulers bring tribute.

At the वौषट् call he makes the oblation and then utters the त्याग-mantra. I should mention that this is the mysterious mantra is of the भृगु-s by knowing which one becomes the complete Jambhala-vit. It was taught to me by Somakhya on a dark new moon night as we were wandering in a beautiful mountainous region. This completed my learning of the tradition and conferred on me the ability to perform this ritual. The thing which dwells in distance in full and then diminishes is an allusion to the moon. This disappearance of the moon is in turn indicative of तिरोधा, the famed concealing power of the यक्ष. He then utters the call '[अस्तु श्रौ३षट् ।]{style="color:#000080;"}', starts filling he sruc and as he does so he recites:\
[कद् रुद्राय प्रचेतसे मीऌहुष्टमाय तव्यसे ।]{style="color:#000080;"}\
[वोचेम शन्तमं हृदो३म् ॥]{style="color:#000080;"}\
What might we say to Rudra, the wise, the most generous and mighty one; what might we say that is most pleasing to his heart?

[उप ते स्तोमान् पशुपा इवाकरं]{style="color:#000080;"}\
[रास्वा पितर् मरुतां सुम्नम् अस्मे ।]{style="color:#000080;"}\
[भद्रा हि ते सुमतिर् मृऌअयत्तमा]{style="color:#000080;"}\
['था vayam ava it te वृणीमहो३म् ||]{style="color:#000080;"}\
[वौ३षट् । इदं रुद्राय पशुपतये न मम ॥]{style="color:#000080;"}\
Like a cowherd, I have driven these praises close to you.\
Grant your favor to us, father of the Marut-s,\
for your benevolence is auspicious, most merciful.\
It is indeed your aid that we choose. \[translation after Geldner]

At the वौषट् call he makes the oblation, then utters the त्याग-mantra and then touches water.

Then with the sruva he utters the following mantra-s and makes an offering of ghee at each स्वाहा followed by the त्याग-mantra '[इदं न मम]{style="color:#000080;"}':\
[इन्द्रः स्वाहा । मरुतः स्वाहा । सर्व-निधिदो रत्न-धातुमान् स्वाहा । तिरोधा भूः स्वाहा । तिरोधा भुवः स्वाहा । तिरोधा स्वः स्वाहा । तिरोधा भूर्-भुवः-स्वः स्वाहा । कामेश्वराय स्वाहा । कुबेराय स्वाहा । धन्याय स्वाहा । वैश्रवणाय स्वाहा । यक्ष-राजाय स्वाहा ॥]{style="color:#000080;"}

He then initiates the expiatory स्विष्टकृत्-याग by reciting the mantra of my ancestor वसुश्रुत आत्रेय:\
[ॐ जुष्टो दमूना अतिथिर् दुरोण]{style="color:#000080;"}\
[इमं नो यज्ञम् उप याहि विद्वान् ।]{style="color:#000080;"}\
[विश्वा अग्ने अभियुजो विहत्या]{style="color:#000080;"}\
[शत्रूयताम् आ भरा भोजनानो३म् ॥]{style="color:#000080;"}\
As the satisfied lord of the house and the guest in the home,\
journey to this our यज्ञ, the learned god,\
Having demolished all assaults, O Agni,\
bring here the sustenance of our enemies.

He utters the call '[अस्तु श्रौ३षट् ।]{style="color:#000080;"}' and fills the sruc. He then recites:\
[अग्निं स्विष्टकृतम् अयाड् वायोर् नियुत्वतः प्रिया धामान्य् अयाट् कुबेरस्य वैश्रवणस्य प्रिया धामान्य् अयाड् रुद्रस्य पशुपतेः प्रिया धामान्य् अयाड् देवानाम् आज्यपानां प्रिया धामानि यक्षद् अग्नेर् होतुः प्रिया धामानि यक्षत् स्वं महिमानम् आयजतामेज्या इषः कृणोतु सो अध्वरा जातवेदा जुष्ताँ हविर् मार्जाल्यो मृज्यते स्वे दमूनाः कवि-प्रशस्तो अतिथिः शिवो नः । सहस्र-शृङ्गो वृषभस् तद् ओजा विश्वाँ अग्ने सहसा प्रास्य् अन्यान् वौ३षट् ॥]{style="color:#000080;"}

At the वौषट् call he makes the offering from the sruc and utters: '[इदम् अग्नये स्विष्टकृते न मम।]{style="color:#000080;"}' The terminal mantra of the स्विष्टकृत्-याग is also composed by my ancestors Budha आत्रेय and गविष्ठिर आत्रेय:\
Fit to be kindled, he is kindled in his own (house) as master of the\
house, praised by poets, our auspicious guest.\
A thousand-horned bull having its virility,\
O Agni, in might you excel all others.

Then he offers a homa to the विनायक-s, Skanda, and other अनुयायिन्-s of Rudra which include the यक्षपति himself so that the यजमान-s rite might be successful and he and his family not be harmed. He utters the त्याग-mantra '[इदं न मम]{style="color:#000080;"}' after each स्वाहा call:\
[ओँ शालकटङ्कटाय विनायकाय स्वाहा । कुष्माण्डराजपुत्राय विनायकाय स्वाहा । उस्मिताय विनायकाय स्वाहा । देवयजनाय विनायकाय स्वाहा । विमुखाय स्वाहा । श्येनाय स्वाहा । बकाय स्वाहा । यज्ञाय स्वाहा । कलहाय स्वाहा । भीरवे स्वाहा । यज्ञविक्षेपिणे स्वाहा । कुलङ्गापमाराय स्वाहा । यूपकेश्यै स्वाहा । सूकरक्रोड्यै स्वाहा । उमायै हैमवत्यै स्वाहा । जम्भकाय स्वाहा । वीरूपाक्षाय स्वाहा । लोहिताक्षाय स्वाहा । वैश्रवणाय स्वाहा । महाराजाय स्वाहा । महासेनाय स्वाहा । कुमाराय स्वाहा । विशाखाय स्वाहा । शाखाय स्वाहा । नेजमेषाय स्वाहा । षष्ठ्यै स्वाहा । रुद्राय महादेवाय स्वाहा ॥]{style="color:#000080;"}

He then touches water. Thereafter he takes up some grain and throws it in the south-west direction for the रक्ष-s uttering: '[रक्षसाम् भगो 'सि ।]{style="color:#000080;"}' Then he touches water again and takes up the darbha grass and casts it into the fire reciting:\
[ओँ स्áं बर्ह्íर् अङ्क्ताँ हव्íषा घृत्éन स्áम् आदित्य्áइर् व्áसुभिः स्áं मर्úद्भिः स्áम् íन्द्रो विश्व्áदेवेभिर् अङ्क्तां दिव्य्áं न्áभो गच्छतु य्áत् स्वा́हा ॥ इदं दिव्याय नभसे न मम । रुद्राय पशुपत्ये स्वाहा । इदं रुद्राय पशुपतये न मम॥]{style="color:#000080;"}\
Auspicious \[is] the grass smeared with offerings and ghee. Let Indra together with the आदित्य-s, the Vasu-s, Marut-s, and the विश्वेदेव-s go (having been honored). Let स्वाहा-offerings rise to the heavenly ether. Hail to Rudra पशुपति.

Then he recites the following to conclude the यज्ञ with the recitation of Agastya:\
[अग्ने नय सुपथा राये अस्मान्]{style="color:#000080;"}\
[विश्वानि देव वयुनानि विद्वान् ।]{style="color:#000080;"}\
[युयोध्य् अस्मज् जुहुराणम् एनो]{style="color:#000080;"}\
[भूयिष्ठां ते नम-उक्तिं विधेम ॥]{style="color:#000080;"}\
O Agni, lead us to wealth by an easy path:\
you know all the rituals, O god.\
Keep us from ritual transgression.\
May we offer you the greatest reverence.

He finally recites the शान्ति-mantra-s: '[तच् छम्योर्...]{style="color:#000080;"}' and '[नमो वाचे या चोदिता...]{style="color:#000080;"}' He then goes to the jar with the image of Kubera and utters the formulae: ['वायुः सुप्रीतः सुप्रसन्नो यथा स्थानं तिष्ठतु । वैश्रवणो राजा सुप्रीतः सुप्रसन्नो यथा स्थानं तिष्ठतु ।]{style="color:#000080;"}' He and his family drink some of the honey offered to the यक्ष and he distributes the भक्षण-s to the beholders of the ritual. He gives a toothbrush, sandals and umbrella along with a hefty ritual fee if he is a यजामान who has employed a purohita to do the rite for him."

V: "I must remark that the final homa has a set of rather remarkable deities. Perhaps we are seeing the worship of वाराही for the first time in सूकरक्रोडी. I also wonder if यूपकेशी is some early version of चामुण्डा. Beyond that I'm most inspired by this account of the rite and feel that I have become a Jambhala-ज्ञा"

