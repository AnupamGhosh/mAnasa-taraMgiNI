
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Chaos, eruptions and root-convergence in one-dimensional maps based on metallic-sequence generating functions](https://manasataramgini.wordpress.com/2019/08/27/chaos-eruptions-and-root-convergence-in-one-dimensional-maps-based-on-metallic-sequence-generating-functions/){rel="bookmark"} {#chaos-eruptions-and-root-convergence-in-one-dimensional-maps-based-on-metallic-sequence-generating-functions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 27, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/08/27/chaos-eruptions-and-root-convergence-in-one-dimensional-maps-based-on-metallic-sequence-generating-functions/ "5:15 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[bronze_bouncer](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/bronze_bouncer.pdf "bronze_bouncer")

Over the years we have observed or encountered certain natural phenomena that are characterized by rare, sudden eruptive behavior occurring against a background of very low amplitude fluctuations. We first encountered this in astronomy: most remarkably, in the constellation of Corona Borealis there are two stars that exhibit this kind of behavior albeit in opposite directions. There is T Coronae Borealis, which for most part remains fluctuating rather dimly in a narrow magnitude band between 9.9 and 10.6, well below naked eye visibility, and then once in a century or so (e.g. in 1866 and 1946) explosively blazes forth at magnitude of 2-3 (  $\approx 1000 \times$  brighter) changing the shape of the visible constellation. On the other side, we have the equally charismatic R Coronae Borealis, which for most part very mildly fluctuates around the magnitude of 6 at barely naked eye visibility and then suddenly once in several years suddenly drops to the magnitude of 14 or lower (  $\approx 1500 \times$  dimmer), beyond the reach of even a typical amateur telescope.

It has become increasingly clear that versions of such behaviors are observed across the domains of science. In biology, recent studies in foraging behavior have shown that diverse animals follow a pattern of foraging movements, which are characterized by routine small movements punctuated by the rare large movements. This kind of behavior allows the escaping of local resource limitations by episodic saltations to reach distant resource-rich regions. An unrelated phenomenon, earthquakes, also displays similar behavior, where small low-magnitude tremors are punctuated by rare episodes of major earthquakes with noticeable effects. This might also be seen in sociology/geopolitics where long periods of low intensity conflicts are interrupted by the rare cases of major warfare. The world wars could be seen as such manifestations against a background of low intensity conflict. This is relevant to the clash between the thinkers Taleb and Pinker regarding whether there is a real trend of the world having become more peaceful or not.

One of the great mathematicians of our age Benoît Mandelbrot provided a framework to understand these disparate phenomena under the rubric of random walks. He called a regular random walk, where the step sizes are normally distributed, as the Rayleigh flights. In contrast, if they instead show some distribution that has a tail with a slower than exponential decay then he defined them as Lévy flights after the mathematician Lévy. One example, of this is the so called Cauchy flight which results from the steps of the walk showing the famous Cauchy distribution (originally discovered by Poisson but attributed to Cauchy). Such random walks are characterized by typical steps that are smaller in magnitude more common than the typical steps under a normal distribution and the extreme steps are rarer than under a normal distribution but way more in magnitude than one would see under a normal distribution. Thus, they capture the eruptive behavior quite well.

We have been long interested in creating simple mathematical analogies for such behaviors observed in nature. Given that a random walk can be reduced to its simplest form, i.e. a one dimensional change in magnitude, we have been interested in one-dimensional maps that can display such behaviors. We describe below a class of one-dimensional maps, which we discovered, that show such a behavior. They are all related to quadratic roots known as metallic ratios.

**The metallic ratios and generating functions of the metallic sequences**\
Metallic ratios can be defined as irrationals which are produced by the following formula:

 $$m_n=\dfrac{\left(n+\sqrt{n^2+4}\right)}{2}; n =1, 2, 3...$$ 

They are called "metallic" after the smallest of them, the famous Golden ratio. As we have noted before the first few metallic ratios are "interesting" because they appear in various unrelated contexts but the larger ones do not seem to do so.  $m_n$  and its conjugate  $m_n'$  are root of a quadratic equation of the form:

 $$x^2 \pm nx -1=0$$ 

The two roots are opposite in sign but correspond to  $m_n, m_n'$ , which show the relationship:

 $$m_n'=\dfrac{1}{m_n}$$ 

Accordingly, they count among the the so-called Pisot-Vijayaraghavan numbers. We use  $m_n$  for the larger and  $m_n'$  for the smaller root in absolute magnitude. Below are the first few metallic ratios, which may assigned special symbols as below:

![bouncer_table1](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/bouncer_table1.png){width="75%"}

They are called ratios because they are the convergents of integer sequences which are specified by the rule:

 $$f\lbrack n\rbrack =k \cdot f\lbrack n-1\rbrack +f\lbrack n-2\rbrack ; k=1, 2, 3...; f\lbrack 1\rbrack =1, f\lbrack 2\rbrack =f\lbrack 1\rbrack +k$$ 

With  $k=1$  we get: **1, 2, 3, 5, 8, 13, 21...**\
With  $k=2$  we get: **1, 3, 7, 17, 41, 99...**\
With  $k=3$  we get: **1, 4, 13, 43, 142, 469...**\
With  $k=4$  we get: **1, 5, 21, 89, 377, 1597...**

Notably these sequences are related to the quadratics whose roots they are via certain generating functions. For a function  $y=f(x)$  the series expansion of  $f(x)$  at the value of  $x=a$  is given by:

 $$y=f(a)+\dfrac{f'(a)(x-a)}{1!}+\dfrac{f''(a)(x-a)^2}{2!}+\dfrac{f'''(a)(x-a)^3}{3!}....+\dfrac{f\^n{'}(a)(x-a)\^n}{n!}...$$ 

Now, for instance, consider the function:

 $$y=\dfrac{x}{x^2+x-1}$$ 

For  $x=0$  its series expansion becomes:

 $$y= -(x + x^2 + 2 x^3 + 3 x^4 + 5 x^5 + 8x^5...)$$ 

We notice that the coefficients of the expansion polynomial are  $1, 1, 2, 3, 5, 8...$ . Thus, this expansion becomes the generating function of the Golden sequence. Similarly, if we take:

 $$y=\dfrac{2x-1}{x^2+3x-1}$$ 

For  $x=0$  we get the series expansion:

 $$y=1 + x + 4x^2 + 13x^3 + 43x^4 + 142x^5...$$ 

Here, the coefficients of the expansion polynomial correspond to the Bronze sequence. The functions of the above type are tripartite curves with two parallel asymptotes at  $x=-m_n$  and  $x=m_n'$  (Figure 1). The central branch of the curve is bounded between these parallel asymptotes. The left and the right hyperbolic branches are bounded respectively on the right and left sides by the two asymptotes.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


**One dimensional maps based on the metallic sequence generating functions**\
The above type of functions specify our one-dimensional maps. The maps operate by using the well-known cobweb construction used in the study of chaotic maps (Figure 1). Figure 1 shows the process for  $y=f(x)=\tfrac{2x+1}{x^2+x-1}$ , which has the series expansion at  $x=0$  as  $y=-(1 + 3x + 4x^2 + 7 x^3 + 11 x^4 +18x^5+29x^6...)$ . The convergent of the coefficients of this expansion polynomial is the Golden ratio. We start with an initial point  $X_0$ . We then project the point on the the curve  $f(x)$  defined by one such metallic sequence generating function to obtain point  $Y_1$ . We then project  $Y_1$  on the line  $y=x$  to obtain point  $X_1$ . We then repeat the above procedure with  $X_1$ . The resulting projection segments are plotted as a cobweb diagram (Figure 1). In algebraic terms the map is expressed as  $x_{n+1}=f(x_n)$ . Our empirical examination revealed that not all maps of this type produce interesting behavior --- they simply converge to a single fixed attractor value. Moreover, we did not observe interesting behavior with other small quadratic Pisot-Vijayaraghavan roots, e.g.:  $x^2-3x+1=0; x= 1+\phi, 1-\phi'$  or  $x^2-2x-2; x=1\pm \sqrt{3}$ .

In our exploration we found the following maps to show interesting behaviors.

 1.  Golden-ratio-based:

 $$x_{n+1}=\dfrac{x_n}{x_n^2+x_n-1}$$ 

 $$x_{n+1}=\dfrac{x_n}{x_n^2-x_n-1}$$ 

 $$x_{n+1}=\dfrac{2x_n+1}{x_n^2+x_n-1}$$ 

 2.  Silver-ratio-based:

 $$x_{n+1}=\dfrac{x_n}{x_n^2+2x_n-1}$$ 

 $$x_{n+1}=\dfrac{2x_n+1}{x_n^2+2x_n-1}$$ 

 3.  Bronze-ratio-based:

 $$x_{n+1}=\dfrac{x_n}{x_n^2+3x_n-1}$$ 

 $$x_{n+1}=\dfrac{2x_n-1}{x_n^2+3x_n-1}$$ 

 $$x_{n+1}=\dfrac{2x_n+1}{x_n^2+3x_n-1}$$ 

 3.  Copper-ratio-based:

 $$x_{n+1}=\dfrac{x_n}{x_n^2+4x_n-1}$$ 

 $$x_{n+1}=\dfrac{2x_n-1}{x_n^2+4x_n-1}$$ 

One point to note regarding these maps is the high degree of numerical instability of most of them. Hence, we have to use high precision numbers to study the evolution of a starting value under the map. We found that one needs a precision of 1500 bits in order to obtain a proper evolutionary trajectory for 20000 iterations of the map. The investigations discussed below are with this precision and number of iterations. Evolution under these maps can be classified into 4 distinct types and we shall discuss examples of each of the distinct types of behaviors in greater detail below.

**Type-1: Chaotic and eruptive behavior**\
The archetypal member of this type is Golden-ratio-based map:\
 $x_{n+1}=\dfrac{2x_n+1}{x_n^2+x_n-1}$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer3_fig2-2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Figure 2 shows the evolution of  $x_0=\tfrac{1}{4}$  under this map. It is immediately apparent that it is characterized by predominantly low amplitude movements interrupted by rare episodes of extreme eruptions that can be several orders of magnitude of the typical values. A feel for this can be obtained via the below table.

\begin{tabular}{|l|c|} \hline Quantile & Value \\ \hline Minimum & -6114.33 \\ Octile 0.125 & -2.406 \\ Octile 0.25 & -0.995 \\ Octile 0.375 & -0.413 \\ Median & 0.0036 \\ Mean & 1.75 \\ Octile 0.625 & -0.418 \\ Octile 0.75 & 1.003 \\ Octile 0.875 & 2.418 \\ Maximum & 35903.36 \\ \hline \end{tabular}

We observe that  $75\%$  ot the values are in rather narrow band of  $\pm 2.41$ ; however, the extremes are roughly 2500-15000 times greater than that band. Thus, we see what is plainly eruptive behavior. It is also rather obvious from these values that the distribution of the values attained under this map are dramatically different from a normal distribution with comparable dispersion. The rarity and the magnitude of the extreme values results in the mean being greatly different from the median.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer3_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


To get a better look at the nature of the movements as  $x_0$  evolves under this map we plot the same on the  $\mathrm{arcsinh}(x)$  scale (Figure 3). The central blue line corresponds to  $0$ . It is flanked on either side by two red lines which correspond to  $\pm 0.2$ . In this case  $\approx 12.5\%$  of the  $x_n$  lie in this band. The green lines correspond to the Golden ratios  $\phi', -\phi$ . The cases where  $x_n \ge 100$ , which is  $0.64\%$  of the values, are marked with red points. We observe that:

 1.  The evolution of  $x_n$  follows a chaotic course.

 2.  The eruptions are triggered in  $x_{n+1}$  when  $x_n$  approaches  $\phi', -\phi$  (Blue points in figure 3). This is illustrated at greater magnification in Figure 4. The reason for this is rather obvious from the equation of the map --- if  $x_n=\phi', -\phi$  then  $x_{n+1}$  will explode to  $\infty$ : thus, closer  $x_n$  gets to  $\phi',-\phi$  greater is the eruption in  $x_{n+1}$ . If the value of  $x_n> \phi', -\phi$  when approaching it then the eruption in  $x_{n+1}$  will positive, if  $x_n< \phi',\phi$  the eruption will be negative.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer3_fig4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```



 3.  Because the map tends to mostly produce small absolute values  $\approx 50\%\; x_n \in \pm 1$  it is obvious that the smaller root  $\phi'$  triggers more eruptions than  $-\phi$ .

 4.  From Figure 4 one also observes a peculiar motif in the form of runs of relatively low amplitude fluctuations in the vicinity of  $\phi, \tfrac{-\phi'}{2}$  (marked as dark green horizontal lines).

Because of the rather dramatic dispersion of the values of  $x_n$ , we studied their distribution in the  $\mathrm{arcsinh(x)}$  scale (Figure 5).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer3_fig5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


We observe that the distribution is symmetric about 0, as we would expect from the above quantile distributions of  $x_n$ . The blue curve shows an attempt to fit a Cauchy distribution to the observed frequencies. We see that it does not capture the distribution too well. The more general form of such a distribution, the t-distribution, also does not perfectly capture the observed frequencies. However, taking inspiration from that we were able to derive a curve that fits the histogram better than any of these distributions. It is defined by a shape function with 4 parameters  $a, b, r, s$  of the form (red curve in Figure 5):

 $$y=\dfrac{1}{1+a|x|\^r+b|x|\^s}$$ 

Another map belonging to this type is similar one based on the Silver ratio (Figure 6):

 $$x_{n+1}=\dfrac{2x_n+1}{x_n^2+2x_n-1}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/silver_bouncer2_fig6.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


It displays the same type of chaotic movements as the above map with episodes of major eruptions.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/silver_bouncer2_fig7.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad
```{=latex}
\end{center}
```


The zoom in Figure 7 shows that the eruptions in  $x_{n+1}$  are in this triggered by  $x_n$  approaching the Silver ratio and its conjugate (light green lines). Like the above map, here too there are motifs comprised of short runs of low amplitude oscillations around two values:  $4-2\sqrt{2}, -1+\tfrac{1}{\sqrt{2}}$  (Figure 7).

**Type-2: low amplitude oscillations punctuated with destabilization and eruptions**\
The maps exhibiting the second type of behavior are:

 $x_{n+1}=\dfrac{x_n}{x_n^2+x_n-1}$ ;  $x_{n+1}=\dfrac{x_n}{x_n^2-x_n-1}$ ;  $x_{n+1}=\dfrac{x_n}{x_n^2+2x_n-1}$ ;

 $$x_{n+1}=\dfrac{x_n}{x_n^2+3x_n-1}$ ;  $x_{n+1}=\dfrac{2x_n+1}{x_n^2+3x_n-1}$ ;  $x_{n+1}=\dfrac{x_n}{x_n^2+4x_n-1}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/bouncers3_fig8.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 8\quad
```{=latex}
\end{center}
```


Figure 8 shows examples of the evolution of the same  $x_0$  under three of these maps based on the Golden, Silver and Bronze ratios. At first sight the eruptions are comparable to what we saw in the Type-1 maps --- they are rare episodes and huge in magnitude relative to the rest of the movements under the map. However, these maps are distinct, in that other than the larger magnitude instability just before major eruptions the background movements are not even registered. To get a better look at what is happening we plot one of the examples (the map based on the Golden ratio) in the  $\mathrm{arcsinh(x)}$  scale (Figure 9).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer2_fig9.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 9\quad
```{=latex}
\end{center}
```


Here we can see that, as in the Type-1 maps, the eruptions are triggered when  $x_n$  passes close to  $-\phi', \phi$  (blue points in Figure 9 prior to the red points marking the eruptions greater than 100). But quite strikingly, even in the  $\mathrm{arcsinh(x)}$  scale, the background movements under the map are barely visible. Hence, we zoom in on a particular region of the  $\mathrm{arcsinh(x)}$  scale plot (Figure 10).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer2_fig10.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 10\quad
```{=latex}
\end{center}
```


Only here we observe that the map is for most part characterized by a very quiet behavior, which, unlike the Type-1 maps, takes the form of regular low-amplitude oscillations that asymptotically build up till they near one of the Golden ratio roots at which point they show unstable behavior leading to an eruption. After a major eruption the evolution settles in a very low amplitude oscillation as seen in the right part of Figure 10. Accordingly we get a paradoxical distribution of  $x_n$  values on the  $\mathrm{arcsinh(x)}$  scale with a very sharp peak and a heavy tail Fig 11. This distribution might also be described by some form the shape equation specified in the above type.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/golden_bouncer3_fig11.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 11\quad
```{=latex}
\end{center}
```


These maps may also be used to describe a feature common to all these maps, i.e. extreme sensitivity to the initial conditions (Figure 12, 13). Even a difference of  $10^{-8}$  in the starting  $x_0$  results in completely different evolutionary trajectories. This is why we need to use extremely high precision numbers to get numerical stable evolution: e.g. 450-600 decimal digits.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/bronze_bouncer_fig12.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 12\quad
```{=latex}
\end{center}
```


An example based on the Bronze ratio.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/silver_bouncer_fig13.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 13\quad
```{=latex}
\end{center}
```


An example based on the Silver ratio. The second example illustrates how eruptive behavior can return after an extremely long phase of quiescence.

**Type-3: Convergent root-seeking behavior after eruptions and instability**\
This is an interesting behavior prototyped by the Bronze ratio-based map:

 $$x_{n+1}=\dfrac{2x_n-1}{x_n^2+3x_n-1}$$ 

Here, the map tends to show oscillatory behavior with occasional eruptions as in the above type. However, after 1 or a few eruptions associated with some chaotic instability the map settles to a fixed behavior where it cycles between a small set of values that gradually converge to roots of a certain polynomial equation (Figure 14, 15).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/rootseeker_r1_fig14.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 14\quad
```{=latex}
\end{center}
```


In this case after 2 large eruptions within the first 400 iterations it eventually converges to cycling between the three roots of the equation:

 $$P_1: y=x^3-3x+1; \; \overbrace{x_1=2\sin\left(\dfrac{\pi}{18}\right) \rightarrow x_2=-2\cos\left(\dfrac{\pi}{9}\right) \rightarrow x_3=2\cos\left(\dfrac{2\pi}{9}\right)}$$ 

Here too, we notice that the eruption occur in  $x_{n+1}$  (red dots) when  $x_n \approx \beta', -\beta$  (blue dots close to blue lines), i.e. the bronze ratios. The map makes multiple attempts to settle into cycling between the roots of  $P_1$  (brown horizontal lines) but each time slips away nears either  $-\beta$  or  $\beta'$  and then erupts. Between 600 and 700 iterations the map settles into what seems a permanent cycling between the roots of  $P_1$  and by 800 iterations approaches within  $10^-6$  of those roots. Iteration of the map for an additional 100000 iterations showed no further eruptive behavior suggesting final convergence to the roots; however, we have not been able to prove this is indeed case.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/rootseeker_r2_fig15.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 15\quad
```{=latex}
\end{center}
```


In this case the map makes multiple attempts to cycle between the roots of  $P_1$  and the roots of a second polynomial  $P_2$  before finally converging to cycling between the roots of the latter (brown horizontal lines):

 $$P_2: y= x^4+7x^3-6x^2-2x+1\lbrack 8pt\rbrack \left.\begin{aligned} x_1 = \dfrac{-7 + 3\sqrt{5} + \sqrt{150 - 66\sqrt{5}}}{4}\lbrack 8pt\rbrack x_2 = \dfrac{-7 - 3\sqrt{5} - \sqrt{150 + 66\sqrt{5}}}{4}\lbrack 8pt\rbrack x_3 = \dfrac{-7 + 3\sqrt{5} - \sqrt{150 - 66\sqrt{5}}}{4}\lbrack 8pt\rbrack x_4 = \dfrac{-7 - 3\sqrt{5} + \sqrt{150 + 66\sqrt{5}}}{4} \end{aligned}\right\}$$ 

There is one obvious static  $x_0$  which remains unchanged under this map, namely the real root of the equation:  $x^3+3x^2-3x+1=0$ :

 $$x_s = -1-2^{1/3}-2^{2/3}$$ 

If  $x_0$  is exactly this  $x_s$  then it will return itself under the map. Even a point very close to  $x_s$  is unstable and will not remain there for too long, eventually converging to  $P_1$  or  $P_2$ . For example, a  $x_0$  that was identical to  $x_s$  till 600 places after the decimal point slid away from  $x_s$  by iteration 705 of map and within the next 20 iterations was on its way to converge to the roots of  $P_1$ . Thus, all other points under this map eventually converge to either the roots of  $P_1$  or of  $P_2$  (Figure 16).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/bouncer_covergence._figure16.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 16\quad
```{=latex}
\end{center}
```


The  $x_0$  are sampled in the interval  $(-15,15)$  in steps of .025. The red  $x_0$  converge to roots of  $P_1$ , while the green  $x_0$  converge to roots of  $P_2$ . The roots of  $P_1$  are marked by blue vertical lines and those of  $P_2$  by orange lines. Notably, it seems that they converge to  $P_2$  less often than  $P_1$  such the ratio  $P_2:P_1$  appears to converge to  $\beta'$  (is there a way to formally test this conjecture?). Interestingly, immediately below  $P_2: x_2$  there is a region where every  $x_0$  converges to  $P_1$ . There is a pseudo-symmetric region above  $P_2: -x_2$  where they all converge to  $P_1$ . Overall, this convergence plot shows a strange pseudo-symmetry in terms of the iterations to convergence required by  $x_0$  (Figure 16).

These roots show some interesting properties:

 $$P_1: x_1+x_2+x_3=0; \; \dfrac{1}{x_1}+x_2=1; \; \dfrac{1}{x_2}+x_3=1;\; x_1 \cdot x_2 \cdot x_3=-1$$ 

Further, the roots of  $P_1$  are connected to an interesting three-seeded नारायण-like sequence with a subtraction rather than a sum:

 $$f\lbrack n\rbrack =3f\lbrack n-1\rbrack -f\lbrack n-3\rbrack ;\; f\lbrack 1\rbrack =0, f\lbrack 2\rbrack =1, f\lbrack 3\rbrack =3$$ 

This yields: **0, 1, 3, 9, 26, 75, 216, 622, 1791, 5157, 14849, 42756, 123111, 354484, 1020696, 2938977...**

The convergent of this sequence is  $1+2\cos\left(\tfrac{\pi}{9}\right)=1-x_2 \approx 2.8793852$ . In principle, this could be used to construct approximations of a regular nonagon. The terms of this series are provided by a generating function which is reciprocal of  $P_1$ . Its series expansion at 0 is:

 $$\dfrac{1}{x^3-3x+1}=1 + 3x + 9x^2 + 26x^3 + 75x^4 + 216x^5 + 622x^6 + 1791x^7 + 5157x^8 + 14849x^9 +...$$ 

For the second polynomial we have:

 $$P_2: x_1+x_2+x_3+x_4=-7;\; \dfrac{1}{x_1}+\dfrac{1}{x_3}=1; \; \dfrac{1}{x_2}+\dfrac{1}{x_4}=1; \; x_1\cdot x_2\cdot x_3 \cdot x_4=1$$ 

These roots are similarly related to 2 sequences. The first is:

 $$f\lbrack n\rbrack =7f\lbrack n-1\rbrack +6f\lbrack n-2\rbrack -2f\lbrack n-3\rbrack -f\lbrack n-4\rbrack ;\; f\lbrack 1\rbrack =-1, f\lbrack 2\rbrack =0, f\lbrack 3\rbrack =1, f\lbrack 4\rbrack =2$$ 

This yields: **-1, 0, 1, 2, 21, 157, 1220, 9438, 73051, 565388, 4375926, 33868270, 262129619, 2028799713, 15702263239, 121530513443...**

The convergent of this sequence is  $P_2: -x_2 \approx 7.739681318$ .

The second sequence is:

 $$\displaystyle f\lbrack n\rbrack =7f\lbrack n-2\rbrack +f\lbrack \rbrack n-1\rbrack -\sum_{j=1}^{n-4} f\lbrack j\rbrack ); \; f\lbrack 1\rbrack =-1, f\lbrack 2\rbrack =0, f\lbrack 3\rbrack =1, f\lbrack 4\rbrack =2$$ 

This yields: **-1, 0, 1, 2, 10, 25, 95, 268, 921, 2760, 9075, 27995, 90199, 282083, 900320, 2833750, 9004640...**

The convergent of this series is  $P_2: \dfrac{1}{x_1} \approx 3.165352$ 

Notably, the terms of this sequence can be produced using the reciprocal of  $P_2$  as the generating function. Its series expansion at 0 is:

 $$\dfrac{1}{x^4+7x^3-6x^2-2x+1}=1 + 2 x + 10 x^2 + 25 x^3 + 95 x^4 + 268 x^5 + 921 x^6 + 2760 x^7 + 9075 x^8 + 27995 x^9 +...$$ 

Thus, this map function has a peculiar property with respect to the roots of the polynomials  $P_1$  and  $P_2$  in that applying it one root yields another in cyclic fashion. Given that for both these polynomials there is a root close to  $\beta'$ , these can "capture" the  $x_n$  approaching it and drive them into a cycle. Thus, these might be seen as stable examples of the motifs encountered in the Type-1 behavior.

**Type-4: Convergence to bounded bands after initial eruptions and instability**\
This type of behavior is exhibited by the Copper ratio-based map:

 $$x_{n+1}=\dfrac{2x_n-1}{x_n^2+4x_n-1}$$ 

Evolution under this map has parallels to the Type-3 behavior. Like in Type-3, after initial instability, which might include some eruptions and chaotic fluctuations, the evolution under the map settles to cycling between specific bounded bands. Within those bands it wanders quasi-chaotically, i.e. with some discernible patterns, but never leaves those bands. Number iterations taken by different  $x_0$  to converge to cycling within those bands can vary greatly (Figure 17).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/bouncer_covergence_fig17.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 17\quad
```{=latex}
\end{center}
```


Here,  $x_0$  are sampled in the interval  $(-40,40)$  in steps of .01. The plot shows a pattern with zones of rapid convergence interspersed with clusters of much longer convergence times. When we zoom in on a region with a cluster of slowly converging  $x_0$  we see that the number of iterations to converge to the band-cycle shows a fractal pattern (Figure 18).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/bouncer_covergence_fig18.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 18\quad
```{=latex}
\end{center}
```


Here, the  $x_0$  are sampled in the interval  $(-4,4)$  at steps of .001. In both these cases a pseudo-symmetry similar to the iterations-to-convergence plot for the Type-3 behavior (Figure 16) is observed. Here too there is a static point  $x_s \approx -4.68577952...$ , which is the real root of  $x^3+4x^2-3x+1=0$ ; it returns itself under the map. Unless one is exactly at  $x_s$  which has a very complicated closed form, any other  $x_0$  in the vicinity continues to converge to the band-cycle.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/band_convergence_fig19.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 19\quad
```{=latex}
\end{center}
```


To examine this type of behavior more closely we consider the evolution of  $x_0=4.37$  (Figure 19), which is particularly slowly converging in Figure 17. We find that there is a massive eruption little after  $n=150$  (eruptions  $> 20$  are marked with red points), which is lodged in the midst of generally chaotic behavior which lasts till close to  $n=260$  (see lower panel in  $\mathrm{arcsinh}(x)$  scale). Not surprisingly, within this region, we observe that, as in the previous types of behavior, the eruptions occur in  $x_{n+1}$  when  $x_n$  (marked with blue points) approaches  $\kappa', -\kappa$  (marked with blue horizontal lines). However, the striking feature of this type is that sometime after this region the map settles into a more regular cycling behavior and never leaves it to degree we have tested these maps  $(n=100000)$ . We examine this cyclic behavior more closely in Figure 20.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/band_convergence_points_fig20.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 20\quad
```{=latex}
\end{center}
```


Here, we plot the evolution of  $x_0=4.37$  for  $n=300..3000$ . We find that the  $x_n$  falls in very specific bands:

 1.  A very narrow middle band centered at  $\frac{2}{5+\sqrt{5}}\approx 0.276393...$  (Figure 20, red points).


 2.  A lower band bounded by  $\left(-2 -\frac{\sqrt{5}}{2}\approx -3.118033, -2 -\frac{1}{\sqrt{5}} \approx -2.447213 \right)$  (Figure 20, violet points).


 3.  An upper band bounded by  $\left(\sqrt{5}-1 \approx 1.236067, \frac{20+4\sqrt{5}}{15} \approx 1.929618\right)$  (Figure 20, blue points).

In the state of convergence the map cycles from the middle band to the upper to the lower band over 3 successive  $x_n$ . Beyond these bands, there are two lines:

 1.  in the lower band corresponding to  $\frac{2577-1221 \sqrt{5}}{58} \approx -2.6420517$ 


 2.  in the upper band corresponding to  $\frac{5-\sqrt{5}}{2}$ 

It is notable that the above band-bounds and lines are all related to  $\sqrt{5}$  which is the surd in  $\kappa, \kappa'$ . They are indicated by horizontal lines in the plot. From the two lines within the bands the points appear to alternately wander towards the upper and lower bounds of the upper and lower bands. As the map evolves, once they reach close the bounds the process repeats again starting from those lines (Figure 20). Each round of wandering is largely symmetric between the upper and lower bands but each round is different from the previous one. In this, type as in Type-3 it appears that  $x_n$  is captured into the band cycle as it approaches  $\kappa'$  which is close to the central band defined by  $\frac{2}{5+\sqrt{5}}$ .

**Tailpiece**\
There are several open questions (for us) regarding these investigations. Is there a formal way of deriving which roots a map may converge to if it shows a Type-3 behavior? Is there a way to formally establish the bands of convergence for Type-4 behavior? Is there way to say whether a map would go the way of Type-1 or Type-2 from its equation?

Whatever the case, the maps discussed here are most remarkable because they can produce complex behavior similar to that observed in natural systems with very simple underlying equations. They illustrate cases of long quiescence (Type-1 and Type-2) with eruptive behavior. These behavior suggests that prolonged low amplitude oscillations or secular changes do not guarantee the absence of sudden eruptions. This is important in realizing that situations like prolonged peace do not mean that there would no major catastrophic conflict. This suddenly emerges as the system moves towards a superficially unremarkable low magnitude state that in reality is something like the metallic ratio trigger seen in these maps. Then we have the reverse behavior where after an initial chaotic phase the map settles into a more regular behavior from which it never emerges due to being captured by certain similarly superficially unremarkable values (Type-3 and Type-4). In history we see that the dramatic movements that occur early in a civilization are never reproduced in the later stages from which it might be unable to break out.

