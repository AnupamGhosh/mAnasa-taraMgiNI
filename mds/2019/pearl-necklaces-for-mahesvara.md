
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Pearl necklaces for महेश्वर](https://manasataramgini.wordpress.com/2019/08/04/pearl-necklaces-for-mahesvara/){rel="bookmark"} {#pearl-necklaces-for-महशवर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 4, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/08/04/pearl-necklaces-for-mahesvara/ "6:40 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**श्रीपति's pearl necklace for महेश्वर**\
The ब्राह्मण श्रीपति of the काश्यप clan was a soothsayer from रोहिणीखण्ड, which is in the modern Buldhana district of Maharashtra state. Somewhere between 1030 to 1050 CE he composed several works on mathematics, astronomy and divination, one of which is the गणित-tilaka on basic arithmetic and algebra which has come down to us through incomplete manuscripts. In that he posses the following problem (The solution is provided by his Jaina commentator Siṃhatilaka सूरि in 1275 CE):

[विश्व-ख-सप्त-भुजङ्ग-नवार्काः शैल-तुरङ्ग-समाहत-देहाः ।]{style="color: #0000ff"}\
[स्यात् स्फुट-तारक-वर्तुल-मुक्ता-भूषणम् अत्र महेश्वर-कण्ठे ॥]{style="color: #0000ff"}\
The bodies of all (13), the space (0), the seven (7), the snakes (8), the nine (9) and the suns (12) struck together (means multiplied in mathematical terminology) with the mountains (7) and the horses (7) may now be the clear, sparkling and globular pearls forming an ornament for the neck of महेश्वर.

The purpose of the problem is two-fold: (1) to make the student familiar with using the Hindu numerical code (the भूत-संख्य) and (2) make the student do some elementary large number multiplication. Thus, the problem is actually a simple multiplication  $12987013 \times 77$  whose answer is  $1000000001$ . Interestingly, श्रीपति offers a clue for the answer in the problem itself: he says the multiplication of the 2 numbers results in a necklace of clear, sparkling, spherical pearls for महेश्वर. This is clearly an allusion to the palindromic structure of the product, with the 0s forming the pearls, since they were written historically as circles and the two flanking 1s form the bindings of the necklace. Some numerical savants are known to exhibit a synesthesia with respect numbers --- perhaps such was indeed the situation with श्रीपति for a palindromic number such as this could simultaneously produce in ones mind the vivid image of a pearl necklace.

Moreover, this is not just any pearl necklace but one for Rudra's neck. It is in this regard we believe he encoded more into that number. As we can see from the above, one of the factors of 1000000001 is 11, which is the characteristic number of the Rudra-s. At the same time, the Rudra-s are also said to be 1000s upon 1000s (Yajurveda: [सहस्राणि सहस्रशो ये रुद्रा अधि भूम्याम् ।]{style="color: #0000ff"}). Thus, this product captures both those aspects. Further, the numbers that yield the product are described in भूत-संख्य (itself eminently amenable synesthetic experience of numbers) as including विश्वा (= all); kha (= space); 7, which symbolizes the heavenly realms or व्याहृति-s, the 8 directional earth-bearing snakes, the 9, which symbolizes the planets, the 12 आदित्य-s on one hand and the 7 continental mountain ranges and 7 solar horses on the other. Thus, the two numbers are described by the entities of entire universe pervaded by the 1000s of Rudra-s with their 11-fold essence and their product is seen yielding a necklace for महेश्वर. In this regard, the use of विश्व for 13 is curious. In the भूत-संख्य system, विश्व represents the विश्वेदेव-s (all gods). In the गणपाठ database from which पाणिनि constructed his grammar we find विश्वदेव as entry 13 in the मनोज्ञादि-गण (GP 177.13), thus lending विश्व to encode 13.

**महेश्वर's necklace sequence and its factors**\
Taking the cue from श्रीपति, we can define a general integer sequence  $f\lbrack n\rbrack$  of महेश्वर's necklaces thusly:  $f\lbrack n\rbrack =10\^n+1$ , where  $n$  is an integer and  $f\lbrack n\rbrack \^m$ , where  $m=1,2,3,4$ , is also palindromic. This implies that  $n=1, 2, 3...$  Thus, the first few terms of our sequence are:\
**11, 101, 1001, 10001, 100001, 1000001, 10000001, 100000001, 1000000001, 10000000001...**

We can next ask what are the factors of each  $f\lbrack n\rbrack$ . The first few  $f\lbrack n\rbrack$  are factorized and tabulated below:

Table 1[![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png){.aligncenter .size-full .wp-image-11877 attachment-id="11877" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="maheshvara_table1" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png" orig-size="690,567" permalink="https://manasataramgini.wordpress.com/2019/08/04/pearl-necklaces-for-mahesvara/maheshvara_table1/" height="526" sizes="(max-width: 640px) 100vw, 640px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png 640w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png 690w" width="640"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table1.png)

From the above one may notice a few obvious things. The first 2 terms of the sequence are primes, 11 and 101, but all others are composites. It is also obvious that 3 can never be a factor of  $f\lbrack n\rbrack$  because the sum of the digits of  $f\lbrack n\rbrack$  will never be 3. We also observe that many of the  $f\lbrack n\rbrack$  have a tendency to have a mix of small factors with a very large one. श्रीपति's original example  $f\lbrack 9\rbrack$  is one such:  $f\lbrack 9\rbrack =7\times 11\times 13\times 19\times 52579$ . We can then ask questions such as: 1) which primes will divide a given  $f\lbrack n\rbrack$ ; 2) For which  $n$  will a given prime  $p$  be a factor of  $f\lbrack n\rbrack$ . 3) Knowing these, we can ask questions, such as, at what further  $n$  will we get a  $f\lbrack n\rbrack$  that will be again divisible by 52579, i.e. they will be further महेश्वर's necklaces of the type specified by श्रीपति.

We notice right away that 11 divides every other term, i.e. whenever  $n$  is odd. We also notice that 101 is a factor of  $f\lbrack 2\rbrack$  and every 4th term thereafter. Thus, we can formally write that 11 divides every  $f\lbrack n\rbrack$  when  $n=2k+1$ , where  $k=0,1,2,3...$ . Similarly, 101 divides every  $f\lbrack n\rbrack$  when  $n=4k+2$ . Thus, it also becomes obvious that 11 and 101 will never be co-factors of the same  $f\lbrack n\rbrack$ . With closer observation we can see that every prime  $p$  that divides a subset of  $f\lbrack n\rbrack$  does so at some  $n=m\cdot k+\tfrac{m}{2}$ , where  $m=2,4,6,8...$ : the reason for writing it this way will become clear below. The first few  $p$  that divide  $f\lbrack n\rbrack$  for some  $n$  are tabulated in the order of how often they do so along with the form  $n$  takes when  $f\lbrack n\rbrack$  is divisible by that  $p$ :

Table 2[![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png){.aligncenter .size-full .wp-image-11876 attachment-id="11876" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="maheshvara_table2" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png" orig-size="591,1077" permalink="https://manasataramgini.wordpress.com/2019/08/04/pearl-necklaces-for-mahesvara/maheshvara_table2/" sizes="(max-width: 591px) 100vw, 591px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png 591w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png 82w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png 165w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_table2.png)

At first site the order in which the primes which divide  $f\lbrack n\rbrack$  appear beyond 11 and 101 is puzzling --- they wildly differ in magnitude and form. However, a closer examination reveals a striking pattern behind this: a prime  $p$  appears in the above list as per the [*multiplicative order*](https://manasataramgini.wordpress.com/2018/11/03/fermats-little-theorem-and-the-periods-of-the-reciprocals-of-primes/) of 10 modulo  $p$ : consider  $10\^j \mod p$  where  $j=1, 2, 3...$ ; when for the first time  $10\^j \mod p = 1$ , that  $j$  is  $m$  the multiplicative order of  $10 \mod p$ . Carl Gauss had famously shown in his *Disquisitiones Arithmeticae* that  $m$  is the length of the repeat pattern of the decimal expansion of  $\tfrac{1}{p}$ . Thus, it is also clear that  $p$  divides  $10\^j-1$  for the first time when  $j=m$ . For example, for 7 we get  $m=6$  because 7 divides  $10\^j-1$  for the first time when  $j=6$  to give  $\tfrac{999999}{7}=142857$ . Likewise, the repeat pattern in the decimal expansion of  $\tfrac{1}{7}=0.\overline{142857}$  which is of length 6. From the above we can easily see why a  $p$  will divide  $f\lbrack n\rbrack$  first time when  $n=\tfrac{m}{2}$ . Thus, the sequence of  $p$  that divide  $f\lbrack n\rbrack$  for the first time will be arranged as per the multiplicative order of  $10 \mod p$ :

 $$10\^m-1= 10^{(m/2)^2}-1=(10^{m/2}-1)(10^{m/2}+1)$$ 

Now  $p$  divides  $10\^j-1$  for the first time when  $j=m$ . Hence, it will not divide  $10^{m/2}-1$ . However, because  $p$  divides  $10\^m-1$ , it therefore divides  $10^{m/2}+1$ 

Now what if  $p$  divides some factor of  $10^{m/2}+1$  which takes the form  $10\^j+1; j=1, 2, 3...$ ? We can see from polynomial factorization that a polynomial of the form  $x\^j+1$  can frequently have two factors of the form  $x\^j+1; j=1, 2, 3...$ , namely  $x+1$  or  $x^2+1$ . For example,  $x^3+1=(x+1)(x^2-x+1)$  and  $x^6+1=(x^2+1)(x^4-x^2+1)$ . Now, in our case  $x+1 \equiv 11$  and  $x^2+1 \equiv 101$ . Those are primes and the first two terms of  $f\lbrack n\rbrack$ ; hence, they will not be divided by any other  $p$ . Now, less frequently, other numbers of the form  $10\^j+1$  are divisors of another such number for a larger  $j$ . For example, 10001 is a divisor of 1000000000001. So, let us assume for a moment that  $p$  divides some  $10\^l+1$  which is a factor of  $10^{m/2}+1$ , then  $l< \tfrac{m}{2}$ . If  $p$  does divides  $10\^l+1$ , then it also divides  $10^{2l}-1$ . But  $2l-1< m$ ; hence,  $p$  cannot divide it because it will only divide a number of of the form  $10\^j-1$  when  $j=m$ , i.e. the multiplicative order of  $10 \mod p$ . Thus,  $p$  cannot divide any other  $10\^l+1$  where  $l< \tfrac{m}{2}$ .

The महेश्वर's necklaces  $f\lbrack n\rbrack$  are the sequence  $10\^j+1$ ; therefore they would be divided by a given  $p$  for the first time when  $n=\tfrac{m}{2}$ , which is half the multiplicative order of  $10 \mod p$ 

By the procedure we followed above we can see that, after  $n=\tfrac{m}{2}$ ,  $p$  would divide every  $f\lbrack n\rbrack$  where  $n=m\cdot k+\tfrac{m}{2}; k=1, 2, 3...$  A corollary to this is only  $p$  with even  $m$  can be factors of  $f\lbrack n\rbrack$  for only then  $\tfrac{m}{2}$  would be an integer. Hence, those primes with odd  $m$  such as  $3, m=1$ ;  $37, m=3$  etc will never be factors of any  $f\lbrack n\rbrack$ .

Armed with the above, we can also tell which will be the next  $f\lbrack n\rbrack$  that will have 52579 as a factor as the original example of श्रीपति. For 52579,  $m=18 \; \therefore p | f\lbrack n\rbrack \iff n=18k+9; \; k=0,1,2...$  Hence, next term would be:

 $$f\lbrack 27\rbrack =1000000000000000000000000001$$ 

We can also see that some  $p$  will always come together as factors of  $f\lbrack n\rbrack$  because they have the same  $m$ . Thus, 7 and 13 with  $m=6$  or 19 and 52579 with  $m=18$  will always co-occur. Further, if a certain  $n$  satisfies the relationship  $n=m \cdot k +\tfrac{m}{2}$  for a certain  $p$  and  $p$  also divides that  $n$  then  $p$  will occur again as a factor of  $f\lbrack n\rbrack$ . For example, consider  $n=6 \times 3+3=21$ . Now,  $m=6$  here; hence  $f\lbrack 21\rbrack$  will be divisible by both 7 and 13 as they have  $m=6$ . However, 7 divides 21. Hence, 7 will occur again as the factor of  $f\lbrack 21\rbrack$ . Thus, we have:  $f\lbrack 21\rbrack = \underline{7} \times \underline{7} \times 11 \times 13 \times 127 \times 2689 \times 909091 \times 459691$ . Likewise,  $n=6 \times 6 +3=39$  will correspond to a  $f\lbrack n\rbrack$  divisible by both 7 and 13. However, as 39 is divisible by 13, we will have 13 occur again as a factor of  $f\lbrack 39\rbrack$ . Thus,  $f\lbrack 39\rbrack =7 \times 11 \times \underline{13} \times \underline{13} \times 157 \times 859 \times 6397 \times 216451 \times 1058313049 \times 388847808493$ . All other  $f\lbrack n\rbrack$  would be square-free.

**The families of महेश्वर's necklaces**
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


We can represent any given  $f\lbrack n\rbrack$  as a clique of its factors. For example, Figure 1 shows the dodecagonal clique formed by the factors of  $f\lbrack 45\rbrack$ , which the most composite  $f\lbrack n\rbrack$  for  $n=1..50$ . We then merge all cliques sharing common nodes for  $f\lbrack n\rbrack , n=1..50$ . The edges and the nodes are then scaled as per their frequency of occurrence across all 50 cliques. The result is a factor graph for  $f\lbrack n\rbrack$  which is shown in Figure 2.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad (click on figure to magnify)
```{=latex}
\end{center}
```


We can see from Figure 2 that there are totally 6 families of  $f\lbrack n\rbrack$  in this range. These families can be described according to their founder member which is then the divisor of the remaining  $f\lbrack n\rbrack$  of that family. The founder member of each family can be described as the  $f\lbrack n\rbrack$  of the form  $10^{2\^l}+1, l=0,1,2...$ , where  $2\^l$  corresponds to a particular  $\tfrac{m}{2}$ :

  -  When  $l=0, \tfrac{m}{2}=1$ , we get the 11 family. 11, as we saw above, divides every  $f\lbrack n\rbrack$  corresponding to  $n=2k + 1$ . Thus, every  $f\lbrack n\rbrack$  corresponding to an odd  $\tfrac{m}{2}$  is drawn into this family, there making it the largest of them.

  -  When  $l=1, \tfrac{m}{2}=2$ , we get the 101 family. 101 draws all  $f\lbrack n\rbrack$  corresponding to  $n=4k+2$ . Thus it becomes the largest of the even  $\tfrac{m}{2}$  families.

  -  When  $l=2, \tfrac{m}{2}=4$ , we get the 1001 family. 1001 being composite is centered on its two factors 73 and 137 and corresponds to the terms where  $n=8k+4$ .

  -  When  $l=3, \tfrac{m}{2}=8$ , we get the 100000001 family. This number being composite is centered on its factors 17 and 5882353 and corresponds to the terms where  $n=16k+8$ .

  -  When  $l=4, \tfrac{m}{2}=16$ , we get the 10000000000000001 family centered on its factors 353, 449, 641, 1409 and 69857. This encompasses the terms corresponding to  $n=32k+16$ .

  -  When  $l=5, \tfrac{m}{2}=32$ , we get the 100000000000000000000000000000001 family centered on its factors 976193, 19841, 6187457 and 834427406578561. This includes the terms corresponding to  $n=64k+32$ .

Thus, we find that the even  $\tfrac{m}{2}$  terms are split up among the various families that appear as per the powers of 2.

**The largest factor of महेश्वर's necklace**\
Given the above information, we can cut down the time in which we factorize महेश्वर's necklaces and gather the set of factors for the first 300 terms of  $f\lbrack n\rbrack$ . We can then ask which is the largest prime  $p_m\lbrack n\rbrack$  which divides the corresponding  $f\lbrack n\rbrack$ . Figure 3 shows the plot of  $\log_{10}p_m\lbrack n\rbrack$  against  $\log_{10}(f\lbrack n\rbrack )$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


We see that the general increase of  $\log_{10}(p_m)$  appears to be linear with  $\log_{10}(f\lbrack n\rbrack )$ . It is bounded between lines  $y=a_ux, y=a_lx$ , where  $a_u=1$  and  $a_l \approx 0.1831$ . The upper bounding slope  $a_u$  is easy to understand: as we observed above, some  $f\lbrack n\rbrack$  tend to have factors widely differing in magnitude; thus the large one is closer in magnitude to  $f\lbrack n\rbrack$ . Trivially, first two terms are primes. There after we get  $f\lbrack n\rbrack$  that are minimally composite. These tend to be of a particular form, e.g.:

 $$f\lbrack 19\rbrack =11 \times 909090909090909091$$ 

 $$f\lbrack 31\rbrack = 11 \times 909090909090909090909090909091$$ 

 $$f\lbrack 53\rbrack = 11 \times 9090909090909090909090909090909090909090909090909091$$ 

Thus, these have factors that approach the upper bounding line.

The median value of ratio of  $p_m\lbrack n\rbrack$  to  $f\lbrack n\rbrack$  is approximately 0.5006. This indicates an even distribution with half the number of  $p_m\lbrack n\rbrack$  being greater than the  $\sqrt{f\lbrack n\rbrack }$  and the other half being lesser than  $\sqrt{f\lbrack n\rbrack }$ .

We understand the lower bound is less clearly. Is there a way to derive it from theory alone? One can see that for  $n$  corresponding to multiples of 15 there is an increased propensity to be close to the lower bound. This is in part expected from the factorization of polynomial of the form  $x\^n+1$  where  $n$  is a multiple of 15. For example, we can see that:

 $$x^{13}+1=(x + 1) (x^{12} - x^{11} + x^{10} - x^9 + x^8 - x^7 + x^6 - x^5 + x^4 - x^3 + x^2 - x + 1) \lbrack 5pt\rbrack x^{14}+1=(x^2 + 1) (x^{12} - x^{10} + x^8 - x^6 + x^4 - x^2 + 1) \\ \lbrack 5pt\rbrack x^{15}+1=(x + 1) (x^2 - x + 1) (x^4 - x^3 + x^2 - x + 1) (x^8 + x^7 - x^5 - x^4 - x^3 + x + 1) \\ \lbrack 5pt\rbrack x^{16}+1=x^{16} + 1 \\ \lbrack 5pt\rbrack x^{17}+1=(x + 1) (x^{16} - x^{15} + x^{14} - x^{13} + x^{12} - x^{11} + x^{10} - x^9 + x^8 - x^7 + x^6 - x^5 + x^4 - x^3 + x^2 - x + 1)$$ 

As one can see from the above example the polynomial where  $n=15$  tends to have more factors than other polynomial adjacent to it. Thus, these tend to be among highly composite  $f\lbrack n\rbrack$ ; hence, they are more likely to have smaller  $p_m\lbrack n\rbrack$  than their neighbors.

**Tailpiece**\
Finally, let us look at some actual historical pearl ornaments of महेश्वर. The below were likely आत्मलिङ्ग-s of a medieval South Indian ruler or a royal शैव officiant.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_fig4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


In this example we see the first term of महेश्वर necklace sequence. 11 is of course the characteristic number of the Rudra-s. If we wish to add the bottom two pearls which belong to a different register we get 13 which is the next most frequent factor of  $f\lbrack n\rbrack$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/08/maheshvara_fig5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


In this example we get two sets of pearl ornamentation one with 17 and another with 7. Both of these are factors of  $f\lbrack n\rbrack$ , with 7 being the next most frequent and 17 the founder of a distinct family. Of course there are other numbers with other symbolisms in these ornaments.

For background also see:\
[Fermat's little theorem and the periods of the reciprocals of primes](https://manasataramgini.wordpress.com/2018/11/03/fermats-little-theorem-and-the-periods-of-the-reciprocals-of-primes/)\
[Visualizing the Hindu divisibility test](https://manasataramgini.wordpress.com/2018/11/11/visualizing-the-hindu-divisibility-test/)

