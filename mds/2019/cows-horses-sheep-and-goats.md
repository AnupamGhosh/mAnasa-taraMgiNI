
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Cows, horses, sheep and goats](https://manasataramgini.wordpress.com/2019/03/08/cows-horses-sheep-and-goats/){rel="bookmark"} {#cows-horses-sheep-and-goats .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 8, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/03/08/cows-horses-sheep-and-goats/ "7:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It goes without saying that humans are what they are today because of cows, horses, sheep and goats. Hindu civilization in particular is in the very least the product for the first two, while remaining two contributed to it from "behind the scenes". The major civilizational transitions in human history were built atop the bones of these four animals: the Neolithic developments in West Asia, the urban Nilotic civilization in Egypt, the rise of the Indo-Europeans on the Eurasian steppes, the rise of the Turco-Mongolic peoples on the same steppes but at their eastern end, and the expansions of the African pastoralists. We were curious if the present distribution of these animals has any notable features or relationship to their history. Accordingly, we used the UN Food and Agriculture Organization data on these animals to visualize their distribution across different countries and continents. Table 1 shows their total population (here the term "cows" stands for what is provided in the FAOSTAT database as cattle).

Table 1. The latest UN FAOSTAT population data\
\begin{tabular}{lr} \hline Animal & Population \\ \hline Cows & 1491687240 \\ Sheep & 1202430935 \\ Goats & 1034406504 \\ Horses & 60566601 \\ \hline \end{tabular}

We first plot the populations of each pair of these four animals for all countries that possess both of them using the  $\log_{10}$  scale (Figure 1) and tabulate the correlations between them in Table 2.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/livestock_correlations.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Table 2. The Pearson correlations between life stock on the  $\log_{10}$  scale\
\begin{tabular}{rrrrr} \hline & Cows & Horses & Goats & Sheep \\ \hline Cows & 1.00 & & & \\ Horses & 0.85 & 1.00 & & \\ Goats & 0.81 & 0.66 & 1.00 & \\ Sheep & 0.80 & 0.72 & 0.83 & 1.00 \\ \hline \end{tabular}

The populations of the horses and cows are the most correlated pairs. The sheep and goats are the next most correlated. Thus, animals of comparable size tend to be more correlated in their populations. The horse-cow pair is also the primary pair of the old Indo-European pastoralism. It is possible there is a historical echo of the Indo-European conquests that made this pair a widely adopted unit.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/livestock_ratios.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


We then looked at the distributions of the ratios for these six pairs (Figure 2). In each plot we mark the following: the mean(red), India (blue), Mongolia (green), Kazakhstan (gray), Kyrgyzstan (cyan). We can see that the plot for the cow:horse plot is the least skewed and is approximately normal. India presents a large country that is heavily skewed in the favor of the cow over the horse. The horse evidently did not do very well over larger swaths of India unlike the cow. On one hand this had its consequences for the Indians in course of their conflict with horse-borne invaders. On the other it made India the land of cows. In contrast, Mongolia, Kazakhstan and Kyrgyzstan show the reverse trend, being horse-heavy pastoralist nations. Kazakhstan was one of the first sites of horse-domestication in horse-centric pastoralist Botai culture. This form of pastoralism persisted on the eastern steppes among the Altaic type of peoples long after the Botai people became extinct after colliding with our Indo-Iranian ancestors . Mongolia still retains this state in its most drastic form.

Mongolia is also unusual in favoring both goats and sheep over cows. Notably it also has a comparable number of sheep and goats. The sheep:goat ratio shows multimodality with some countries investing heavily in favor of sheep over goats. This is where the two steppe nations Kazakhstan, Kyrgyzstan differ from Mongolia. India in contrast weighted in favor of goats over sheep, again reflecting the environmental conditions favoring goats. Further, goat pastoralism might have come early to India along with the Iranian farmers who played a major role in the foundation of the Harappan civilization. Nevertheless, sheep pastoralism eventually spread deep into peninsular India with the use of sheep milk and a memory of it is preserved in the [origin myths of the रेड्डि-s of Andhra](https://manasataramgini.wordpress.com/2003/09/01/masi-reddi/). The goat:horse ratio is also notably multimodal suggesting that originally goats and horses defined very different pastoralist niches. The effects of this ancient niche distinct appears to persist to this date.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/livestock_pop_change.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad India (blue), Mongolia (green), world (black)
```{=latex}
\end{center}
```


The above are based on the latest population statistics but one can also ask about their dynamics. In this regard the FAOSTAT database has data for many countries and the world for about 57 years. One simple way to look at population change in this period is fraction of population change with respect to the start and end of this period. This can be assessed using the formula:

 $$f_{pc}=\dfrac{2(p_f-p_i)}{p_f+p_i}$$ 

Here  $f_{pc}$  is the fraction of population change,  $p_i$  the initial population and  $p_f$  the final population. The distribution of  $f_{pc}$  for the four animals is plotted in Figure 3. Except for horses which have shown a slight decline, the populations of the other animals have shown growth world-wide. One sees that, except for sheep, the  $f_{pc}$  for India is lower than for the world for the remaining animals. In contrast, for Mongolia it is higher than for the world define a truly pastoralist nation. The Indian horse populations have shown a notable decline in this period.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/03/livestock_dynamics.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad  $y$ -axis in  $\log_{10}$  scale.
```{=latex}
\end{center}
```


This measure only gives a coarse view of the population change. Hence, we looked at the absolute population change for 3 nations (Mongolia, India, USA) in Figure 4 over a 57 year period for which data exists. The dynamics of the precipitous decline of the horse in India is apparent. As noted above, much of India is not suited for the horse and in the absence of the old military and royal-display derived pressure to keep the horse numbers high it has mostly had a free fall. USA shows almost the reverse trend with respect horses and cows. We are not sure if this trend for cows relates to the decline in beef consumption among the Americans. The India cattle situation two booms followed by busts. The turning points for these busts seem to correspond to El निञो-related droughts and it needs to be see if they were indeed the triggering factors for the declines in Indian cattle. In Mongolia there seem to be generally similar trends for both cattle and horses. The marked rise in productivity after the fall of the Soviet Empire suggests the release from Soviet collectivism allowed the Mongols to recover their traditional pastoralist lifestyle. The Mongolian situation also shows the strongest evidence for climate effects, given that after the Soviet collapse the Mongolian cattle and horse populations have shown similar busts and booms. These seem to correspond to the aftermath of the severe Mongolian winters known as the jud-s that take a heavy toll on the animals and the pastoralists. However, the warming in northern latitudes might be allowing a rapid bounce back from the juds.

