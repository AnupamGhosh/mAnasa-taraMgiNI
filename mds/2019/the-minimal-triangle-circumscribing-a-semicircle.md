
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The minimal triangle circumscribing a semicircle](https://manasataramgini.wordpress.com/2019/10/06/the-minimal-triangle-circumscribing-a-semicircle/){rel="bookmark"} {#the-minimal-triangle-circumscribing-a-semicircle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 6, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/10/06/the-minimal-triangle-circumscribing-a-semicircle/ "7:25 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Consider a fixed semicircle with center at  $O$  and radius  $r$ . Let  $\triangle ABC$  be the isosceles triangle which circumscribes it (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/10/triangle_circumscribing_semicircle.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


What will be the characteristics of the minimal form of the said triangle, i.e. triangle with minimum perimeter, which will circumscribe the said semicircle? Let the base angles of the isosceles triangle be  $\angle x$ . In order to determine the equation for the perimeter with respect to this base angle this we perform the following construction (Figure 2):

 1.  Join point  $O$  to one of the points of tangency of the circumscribing triangle ABC and the semi-circle (Point  $I_1, I_2$ ).

 2.  This gives us two right triangles  $\triangle OI_2C$  and  $\triangle AI_2O$ .

 3.  Using their sides and  $\angle x$  we can determine the lengths of the segments  $\overline{OC}, \overline{I_2C}, \overline{I_2A}$  (Figure 2). Twice their sum will yield the equation of the perimeter of the triangle. It is:

 $$P=2r\left (\tan(x)+ \dfrac{1}{\tan(x)}+\dfrac{1}{\sin(x)}\right)$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/10/triangle_circumscribing_semicircle2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Thus, we will get the minimal triangle when the function  $y=\left (\tan(x)+ \tfrac{1}{\tan(x)}+\tfrac{1}{\sin(x)}\right)$  attains a minimum (Figure 3).

![semicircle_triangle_minim](https://manasataramgini.wordpress.com/wp-content/uploads/2019/10/semicircle_triangle_minim.png){width="75%"}Figure 3

The derivative of this function is (Figure 3):

 $$y'=\left (\dfrac{1}{\cos^2(x)}- \dfrac{1}{\sin^2(x)}-\dfrac{\cos(x)}{\sin^2(x)}\right)$$ 

We get our minimum when  $y'=0$ . Thus at minimum,

 $$\dfrac{1}{\cos^2(x)}=\dfrac{1}{\sin^2(x)}+\dfrac{\cos(x)}{\sin^2(x)}\lbrack 7pt\rbrack \sin^2(x)=\cos^3(x)+\cos^2(x)\lbrack 7pt\rbrack 1-\cos^2(x)=\cos^3(x)+\cos^2(x)\lbrack 7pt\rbrack \cos^3(x)+2\cos^2(x)-1=0$$ 

Putting  $y=\cos(x)$  we get the cubic equation  $y^3+2y-1=0$ . It can be factorized as  $(y + 1) (y^2 + y - 1) = 0$ . From which we get the roots as  $(-\phi, -1, \phi')$ , where  $\phi=\tfrac{1+\sqrt{5}}{2}$  is the Golden ratio and  $\phi'=\tfrac{\sqrt{5}-1}{2}=\tfrac{1}{\phi}$ .  $x=\arccos(y)$ . Since  $x$  is the base angle of an isosceles triangle,  $y=\phi'$  is the only valid value for which the  $\arccos(y)$  may be taken. Thus we get,

 $$x=\arccos(\phi') \approx 0.9045569$$ 

Thus, when the base angles of the isosceles  $\triangle ABC$  are  $\arccos(\phi')$  then we have the minimal circumscribing triangle of a given semicircle (Figure 4).

![triangle_circumscribing_semicircle_shrI](https://manasataramgini.wordpress.com/wp-content/uploads/2019/10/triangle_circumscribing_semicircle_shri.png){width="75%"}Figure 4

The special features of this triangle can be seen in Figure 4, where the radius of the fixed semicircle is taken to be a unit: It is made up 2 right triangles whose sides specify the equation of  $\phi$ , i.e.  $\phi^2-\phi-1=0$ . The various powers of  $\phi$  are captured by it. The perimeter of the minimal circumscribing triangle is thus  $2r \phi^{5/2}$ . The 2 points where the triangle touches the circular arc of the semicircle are specified by the angle  $\arcsin(\phi')$ .

This triangle has a special place in the great geometric manifestation of the Bhairava and the भैरवी, known as the श्री-yantra, versions of which are worshiped in the पश्चिमांनाय (कुब्जिका; Western lodge) and दक्षिणांनाय (श्री-kula; Southern lodge) of the kaula tradition. This yantra is described in a key text of these traditions, the सौन्दर्यलहरी, thusly:\
[चतुर्भिः श्रीकण्ठैः शिवयुवतिभिः पञ्चभिर् अपि]{style="color:#0000ff;"}\
[प्रभिन्नाभिः शंभोर् नवभिर् अपि मूलप्रकृतिभिः ।]{style="color:#0000ff;"}\
[त्र्यश्-चत्वारिंशद् वसु-दल-कलाश्र-त्रिवलय]{style="color:#0000ff;"}\
[त्रि-रेखाभिः सार्धं तव शरण-कोणाः परिणताः ॥]{style="color:#0000ff;"}

![shrI_yantra](https://manasataramgini.wordpress.com/wp-content/uploads/2019/10/shri_yantra.png){width="75%"}Figure 5

The above verse from the सौन्दर्यलहरी may be rendered as:\
The (tri)angles in what constitute your dwelling, which is built of 9 मूल-प्रकृति-s, i.e. the 4 श्रीकण्ठ-s and the 5 शिव-yuvati-s, all \[stationed] apart from शम्भु, along with the lotuses with Vasu- (8) and lunar-digit- (16) petals, the 3 circles and the 3 lines, are 43 in number. (modified version of the translation of Subrahmanya Shastri and Shrinivasa Ayyangar)

This verse precisely captures the essence of the structure of the श्री-yantra. The 9 मूल-प्रकृति-s are the 9 basic triangles that constitute to the yantra. They represent the 9-fold form of the Bhairava of the पश्चिमांनाय, namely नवात्मन्-bhairava. They are in turn divided into: 4 upward-facing or लिङ्ग triangles, which are termed श्रीकण्ठ-s. These are the manifest forms of the Bhairava. 5 downward-facing or yoni triangles, which are termed शिव-yuvati-s. These are the manifest forms the भैरवी. The central point, the bindu, lies apart from from them and represents the Bhairava in his state as the unmanifest singularity, i.e. अकुलवीर. In contrast, the rest of the yantra with is manifold geometric structures can be seen as his शक्ति, कौलिनी, who manifests diversely as the kula-s of योगिनी-s. These योगिनी-s inhabit all elements of the structure: For example, the 8 मातृका-s (चण्डिका, ब्राह्मी, रुद्राणी, कौमारी, वैष्णवी, वाराही, इन्द्राणी, चामुण्डा) inhabit the 8-petalled lotus (This is as per the मूल-प्रक्रीया of the आनन्द-bhairava tradition. Other परम्परा-s place the काम-योगिनी-s from अनङ्गपुष्पा to अनङ्गमालिनी in these petals). The 16 श्री-नित्या-s inhabit the 16-petalled lotus.

The 43 triangles are reckoned as the 42 outward-pointing triangles and the inner-most triangle formed by the intersection of the 9 basic triangles. The outermost of these has 14 triangles known as the सौभाग्यदायक in which dwell the Sarva-सौभाग्यदायक-योगिनी-s. The second rung with 10 triangles is known as the सर्वार्थसाधक with eponymous योगिनी-s. The third rung again with 10 triangles is the Sarva-रक्षाकर with the 10 योगिनी-s of that kula. The 4th rung with 8 triangles is the Sarva-rogahara with the 8 वाग्देवी-s as its resident योगिनी-s. The 5th rung with the central triangle around the bindu has कामेश्वरी as its योगिनी.

So, where does our above minimal triangle figure in all this? It defines the श्रीकन्ठ-1 which is the first triangle to be drawn while constructing the श्री-yantra. Also, notably, the first शिव-yuvati is an isosceles triangle formed by welding two 3-4-5 right triangles along the side of length 4. As we have seen before, this 3-4-5 triangle, the most primitive of the integral right triangles, emerges naturally as the maximal triangle defined by [the characteristic ellipse of a right triangle](https://manasataramgini.wordpress.com/2019/07/18/discovering-bronze-in-the-characteristic-ellipse-of-right-triangles/). Thus, the two primary triangles of the श्री-cakra natural feature as the ultima of certain problems of triangle geometry.

