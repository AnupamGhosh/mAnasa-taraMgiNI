
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [From Plato to Euler and back](https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/){rel="bookmark"} {#from-plato-to-euler-and-back .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 15, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/ "5:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This is primarily meant as an educational handout on some very basic theorems of geometry that one might have studied in school. Some educated adults whom we asked about these had either forgotten them or claimed to have never studied them. Hence, we provide these here for those who might be interested. In the below discussion all angles are in radians unless explicitly specified otherwise.

**There are only 5 Platonic solids\
**The first question is at least as old as the times of Plato: Why are they only 5 Platonic solids? Platonic solids are regular polyhedra, i.e. they have all their faces as the same regular polygon. The simplest regular polygon is an equilateral triangle. Its angle is  $\tfrac{\pi}{3}$ . In order to fold a solid the sum of the angles of the polygonal faces must be less than  $2 \pi$  radians because  $2 \pi$  is the sum of the angles about a point on a plane in Euclidean space. Further, to make a solid we need at least 3 polygonal faces meet at a vertex. Hence, with an equilateral triangle we can make solids with 3 or 4 or 5 faces meeting at a vertex,  $3 \times \tfrac{\pi}{3}; 4 \times \tfrac{\pi}{3}; 5\times \tfrac{\pi}{3}< 2\pi$ . These respectively yield the tetrahedron, the octahedron and the icosahedron (Figure 1). With the next regular polygon, a square, we can get 3 squares to meet at a vertex, i.e.  $3 \times \tfrac{\pi}{2}$ . This yields us a cube. Next, with a regular pentagon we can get three pentagonal faces at vertex, i.e.  $3 \times \tfrac{3\pi}{5}$ . This gives us the dodecahedron. Thus, we can have only 5 Platonic solids in 3-dimensional space  $_\blacksquare$ .

In principle, one can join the centers of adjacent faces of regular polyhedron to get another regular polyhedron due to their inherent symmetry (Figure 1). Performing this operation one can also see that the Platonic solids have a duality with another Platonic solid: (cube-octahedron) -- (tetrahedron-tetrahedron) -- (dodecahedron-icosahedron). In each dual pair there is one of the three formed by equilateral triangle faces. Given that the duality operation on the tetrahedron yields a congruent tetrahedron, you again will have only 5 Platonic solids. It made a such a profound impression on me when I first discovered this for myself as a kid that I could entirely appreciate the profound impact the discovery of these solids (apocryphally by Pythagoras and Theaetetus) had on Plato of yore. It tells you some very basic thing about Euclidean space.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/platonic_solids.png){width="75%"}
```{=latex}
\end{center}
```

**Figure 1. The 5 Platonic solids and their duals.**

**The sum of the angles of a convex polygon**\
A polygon is a closed path with  $B$  vertices where adjacent vertices are connected by a single edge. Hence, it follows that the polygon has  $B$  edges or sides as well (Figure 2). This gives our next question: what is the sum of the angles of a polygon? The simplest polygon, the triangle, has angles summing to  $\pi$  radians in Euclidean space. Every other planar convex polygon of  $B$  vertices can be constructed from  $B-2$  triangles all of which share a common vertex (Figure 2). From Figure 2 it is apparent that the sum of angles of the polygon is equal to the sum of the angles of the constituent triangles. Thus, the sum of the angles of a convex polygon is  $(B-2)\pi$  radians. This result was first derived by [Proclus](https://manasataramgini.wordpress.com/2013/02/10/the-end-of-the-heathens/).

**[![polygon_triangulation](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png){.size-full .wp-image-11730 .aligncenter attachment-id="11730" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="polygon_triangulation" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png" orig-size="1170,702" permalink="https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/polygon_triangulation/" sizes="(max-width: 1170px) 100vw, 1170px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png 1170w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_triangulation.png)Figure 2. The sum of the angles of a convex polygon.**

**The triangulation theorem**\
Let a convex polygon have  $B$  vertices and  $I$  internal points (Figure 3). We triangulate it by connecting the internal points to the vertices of the bounding polygon or each other without any edge crossing over such that the whole polygon is dissected into  $F$  triangles (Figure 3). We then ask: Is there are relationship between  $B$ ,  $I$ ,  $F$ . The answer is the triangulation theorem, which states that  $F=2I+B-2$ .

**[![triangulation_theorem](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png){.size-full .wp-image-11732 .aligncenter attachment-id="11732" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="triangulation_theorem" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png" orig-size="1135,654" permalink="https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/triangulation_theorem/" sizes="(max-width: 1135px) 100vw, 1135px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png 1135w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_theorem.png)Figure 3. Triangulation of a polygon.**

*Proof:*

 1.  The sum of the angles of the bounding polygon (as shown above) is  $(B-2)\pi$ .

 2.  The sum of all the angles on a plane sharing common vertex is  $2\pi$  radians. Hence, the sum of the angles at all internal points is  $2I\pi$ .

 3.  Since the sum of the angles of a triangle is  $\pi$ , the sum of the angles of all the  $F$  constituent triangles of the triangulation of the polygon is  $F\pi$ . From Figure 3 it can also be seen that it is  $(B-2)\pi+2I\pi$ .\
4)  $F\pi=(B-2)\pi+2I\pi; \; \therefore F=2I+B-2\;\; _\blacksquare$ 

**Euler's theorem**

**[![polygon_Euler](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png){.size-full .wp-image-11729 .aligncenter attachment-id="11729" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="polygon_Euler" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png" orig-size="1557,799" permalink="https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/polygon_euler/" loading="lazy" sizes="(max-width: 1557px) 100vw, 1557px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png 1557w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler.png)Figure 4. Two polyhedra and the polygonal tilings derived from them.**

The above result regarding the triangulation of polygons in 2D space Euclidean space and Plato's discovery that only 5 regular polyhedra exist in 3D space are both related to the sum of all the angles on plane about a vertex being  $2\pi$ . This in turn leads to the celebrated theorem of Euler regarding polyhedra, which while deceptively simple had to wait for 2 millennia since Plato's days. Consider the two polyhedra in Figure 4. The first is a pentagonal pyramid while the second is a hexagonal prism. Let  $F$  be the number of faces,  $V$  the number of vertices and  $E$  the number of edges of the polyhedron. For the pyramid in question we have  $F=6; V=6; E=10$ . Thus,  $6+6=10+2$ . For the prism in question we have  $F=8; V=12; E=18\; \therefore 8+12=18+2$ . You can try this out for other polyhedra like those in Figure 1. We can as the question: Is there a general relationship here? We observe that always:  $F+V=E+2$ . This is Euler's theorem and and we prove it below.

*Proof:*

 1.  We first reduce the 3D polyhedron to a 2D graph (Figure 4). The way we do this is by choosing one face of the polyhedron as the bounding polygon. We then flatten all other faces inside that bounding polygon while still maintaining the topology of the face. This means that a triangular face remains a triangle , a quadrilateral remains a quadrilateral, and a  $n$ -gon remains a  $n$ -gon in the flattened 2D graph. Thus, the 2D graph is a polygonal tiling of the selected face polygon of the starting polyhedron. This is easy to conceive for pyramids and prisms (Figure 4). It is more complicated for some of the Platonic polyhedra (Figure 5).

**[![polygon_Euler_reduction](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png){.size-full .wp-image-11728 .aligncenter attachment-id="11728" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="polygon_Euler_reduction" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png" orig-size="1763,557" permalink="https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/polygon_euler_reduction/" loading="lazy" sizes="(max-width: 1763px) 100vw, 1763px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png 1763w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/polygon_euler_reduction.png)Figure 5. The polygonal tilings derived from the Platonic polyhedra.**

Thus, we observe that the 2D graph retains all edges and vertices of the starting polyhedron. However, since it is constructed by flattening all other faces onto one face we lose that face and the resulting number of tiling polygons is 1 less than the number of polyhedral faces. Thus, the 3D equation  $F+V=E+2$  becomes  $F+V=E+1$  for a 2D polygonal tilings. Proving the latter in 2D effectively results in proving the former.


 2.  We define the number  $P=F+V-E$  for a given polygon tiling. Our objective is to prove that always  $P=1$ . We next triangulate all non-triangular polygons in a given polygon tiling into triangles. Let us define  $T=F_t+V_t-E_t$  as the number we get from the faces, vertices and edges after triangulation. From Figure 6 it is apparent that for each new edge we add during triangulation we get a new face. Thus,  $P=T$ . Hence, proving  $T=1$  will prove  $P=1$ .

**[![triangulation_conservation](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png){.size-full .wp-image-11727 .aligncenter attachment-id="11727" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="triangulation_conservation" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png" orig-size="893,666" permalink="https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/triangulation_conservation/" loading="lazy" sizes="(max-width: 893px) 100vw, 893px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png 893w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png 768w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/triangulation_conservation.png)Figure 6. Conservation of  $F+V-E$  upon polygon triangulation.**


 3.  Now that we have triangulated the polygonal tiling, we can apply the triangulation theorem on the resulting graph to show  $T=1$ . The faces  $F$  in the triangulation theorem is the same as the  $F_t$  in our triangulated graph; hence we can write  $F_t=2I+B-2$ . Further, the number of vertices  $V_t$  of the triangulated graph is the sum of the number of boundary vertices of the bounding polygon and the internal points. Thus, we get  $V_t=I+B$ . Let  $s$  be the sum of the number of sides of all triangles in the triangulation redundantly counting all shared edges for each triangle. Since there are  $F_t$  triangles we have  $s=३F_त्$ . Now  $s$  can be expressed in another way.  $B$  will be the number of all the sides of triangles making up the boundary polygon. The  $E_t-B$  will be the remaining edges. Now they will fall on the side of 2 triangles each (e.g. see Figure 6) so to make up  $s$  we have count them twice. Thus, we can write  $s=B+2(E_t-B)$ .

Thus, we get:

 $$B+2(E_t-B)=३F_त्; \; \therefore B=2E_t-३F_त्$$ 

From the triangulation theorem and  $I=V_t-B$  we can write:  $F_t=२V_त्-2B+B-2; \therefore F_t=२V_त्-B-2$ \
Plugging the above value of  $B$  into this we get:  $F_t=२V_त्-2E_t+३F_त्-2; \; \therefore F_t+V_t-E_t=T=1$ \
Since  $P=T$ , it follows that  $F+V=E+1$  for a polygonal tiling. From this it follows that:

 $$F+V=E+2 \;\; _\blacksquare$$ 

**Back to the Platonic solids**\
We may next ask: Given Euler's theorem can we prove that there are only 5 Platonic solids? To answer this we need to prove two further relationships first. For this we take into account that a Platonic polyhedron has regular polygons as its faces and that each vertex must necessarily belong to the same number of polygonal faces. Let  $n$  be the number of edges of the regular polygonal face of a Platonic solid. Let  $m$  be the number of edges of meeting at any given vertex of a Platonic solid. Let  $s$  be the sum of the number of sides of all polygonal faces of a Platonic solid counting redundantly. Given that we have  $F$  faces in the solid, we get  $s=न्F$ . Given that we have  $V$  vertices with  $m$  edges meeting at each of them we get  $s=म्V$ . Finally, we note that each edge of the polyhedron simultaneously belongs to 2 of its polygonal faces; hence, we get  $s=2E$ . From this we get the relationship for any Platonic solid:  $2E=न्F=म्V$ . For example, in an octahedron we have  $E=12, n=3, m=4$ ; thus,  $2\times 12=3 \times 8 = 4 \times 6=24$ .

From the above we have for a Platonic solid:  $F=\tfrac{2E}{n}, V=\tfrac{2E}{m}$ 

Next, we plug the above into Euler's formula  $F+V=E+2$  to get:

 $$\tfrac{2E}{n}+\tfrac{2E}{m}-E=2\lbrack 7pt\rbrack \tfrac{2}{n}+\tfrac{2}{m}-1=\tfrac{2}{E}\lbrack 7pt\rbrack \therefore \tfrac{2}{n}+\tfrac{2}{m}-1> 0\lbrack 7pt\rbrack \therefore \tfrac{2}{n}+\tfrac{2}{m}> 1$$ 

By multiplying both sides by  $nm$  we get the Eulerian inequality for a Platonic solid:  $2m+2n> nm$ 

We can now use this to prove that there can be only 5 Platonic solids. For this we should first keep in mind that to have a solid at least 3 faces should meet at a vertex; hence, minimally  $m=3$ . We then begin with a triangular face  $n=3$  and plug it into the Eulerian inequality:

 $$2m+6> 3m;\; \therefore m< 6$$ 

Thus, for a triangle,  $m=3, 4, 5$ : these yield the tetrahedron, octahedron and icosahedron.

Next we take a square face  $n=4$ 

 $$2m+8> 4m;\; \therefore m< 4$$ 

Thus, we can only have  $m=3$  for a square face and we get the cube.

Next we take a pentagonal face  $n=5$ 

 $$2m+10> 5m;\; \therefore m< \tfrac{10}{3}$$ 

Thus, we can only have  $m=3$  for a pentagonal face and we get the dodecahedron.

Next we take a hexagonal face  $n=6$ 

 $$2m+12> 6m;\; \therefore m< 3$$ 

Thus, we cannot get any polyhedron because  $m< 3$ . The same applies for any  $n> 6$ . Thus there can only be 5 Platonic solids  $_\blacksquare$ 

**Tailpiece**\
While we can have infinite regular polygons, only the first 3 of them yield a total of just 5 Platonic solids. This is a strong constraint in 3D space; thus, one would expect these Platonic ideals to occur as reflections throughout nature. Indeed, that is the case and we may note the following:

 1.  The 4 bonds formed by the  $sp^3$  hybridized orbitals of carbon define a tetrahedron. Similarly, phosphates  $Pऒ_४^{3-}$ , such as those forming the backbone of DNA and RNA also assume a tetrahedral geometry.  $Fe_3O_4$  assumes tetrahedral crystals. Thus, the tetrahedron is a fundamental structure of nature and life.

 2.  The cube is a ubiquitous habit of crystals:  $णCल्$  is a good example of such.

 3.  The octahedron is the habit of diamond and alum  $ख़ाल्(SO_4)_2 \cdot 12H_2O$ .

 4.  The dodecahedron is the habit of the quasi-crystal of the holmium--magnesium--zinc alloy. The RNA of certain nodaviruses is packed in a dodecahedral form within their capsids.

 5.  The icosahedron is famously the form of the capsids of numerous viruses. It is also seen in some non-viral protein assemblies like those which form the propanediol utilizing and ethanolamine utilizing bacterial microcompartments.

In the human world, a deliberately made cube used as a gaming die and a fixed weight was seen among the Harappan people of bronze age India. Harappans also made regular tetrahedral weights, such as those found in Mohenjo daro. Some believe that the occurrence of pyrites which are found in certain places in Italy and naturally assume the forms of cubes, dodecahedra and icosahedra might have inspired the yavana-s to think about these solids. Among the yavana-s, we are informed by Proclus that Pythagoras was the first to discover the Platonic solids. However, there is no evidence that he actually knew all 5 of them and that there were only 5. Plato mentions them in his Timaeus and by then it was known that there are 5 of them. It is believed that Plato obtained that knowledge from Theaetetus, his mathematical interlocutor, who is believed to have first recognized all 5 of them. However, the evidence for this comes from an apocryphal commentary on Euclid. Plato thought that the tetrahedron, octahedron, cube and icosahedron corresponded to the 4 "elements" of Greek tradition: fire, air, earth and water. He then added that a god used the dodecahedron "for embroidering the constellations on the whole heaven." This is perhaps an allusion to the 12 Zodiacal constellations of the Greeks. It is also held that Theaetetus' explorations inspired the *Elements*, where Euclid presumably following his successors gives these 5 Platonic polyhedra and indicates that there are only 5 of them. Material evidence for their recognition in the Classical world comes from the discovery of the icosahedral dice from Ptolemaic Egypt. Further, the Romans made metal dodecahedra and icosahedra (mostly the former), whose function remains unknown to date as far as we know.

In Indian mathematics we are unaware of a specific mention of Platonic solids as a group. However, we have evidence for a "ghost-lineage" of Hindu mathematical knowledge regarding Platonic solids in the form of beads shaped as those solids from at least the Gupta age. There is a persistent belief among white indologists and their imitators that Hindu mathematical tradition is a poor derivative of the Mesopotamian and Greek traditions. However, there is absolutely no evidence for transmission of Greek texts with knowledge of Platonic solids to Hindus prior to the 1700s of the common era. Thus, there is no evidence that the Indian production of Platonic solids was inspired by the Greek textual tradition. One may point out that their emergence in Indian art is approximately contemporaneous with their emergence in material depictions in the Classical world. However, the Indian versions are not used as dice as in Ptolemaic Egypt or as made like or of the large size of their Roman counterparts. They are part of a distinctly Indian use in jewelry. Indeed, in this regard one may point out that already in the सामञ्ञफल-sutta-84 the ताथागत mentions such a well-polished octahedral bead. One may also note that some ताथागत atomic theories in India saw substances as being formed from their constituent molecules by an octahedral packing. It is also notable that this style of polyhedral beads was transmitted to Myanmar, Thailand and Vietnam along with early gold-working traditions and at least dodecahedral and octahedral beads have been found in those regions.

