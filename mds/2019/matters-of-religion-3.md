
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion-3](https://manasataramgini.wordpress.com/2019/12/30/matters-of-religion-3/){rel="bookmark"} {#matters-of-religion-3 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 30, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/12/30/matters-of-religion-3/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Lootika seeing Somakhya contemplating something remarked: "That great clash of men is upon us, where, as the श्रुति says, our flag will be mingled with that of the enemy in close combat."\
Somakhya: "It's almost as if your mind is reflecting mine!"\
Lootika: "Indeed, though born V1s our life is like that of V2s in a sense like as this is the downward turn of the yuga-cakra. Hence, may वज्रबाहु sink the dasyu to his left and bring light to the आर्य on the right."\
Somakhya: "The founders of my clan or your शरद्वान् clansman never shied from yuddha. It is with that in mind O गौतमी we have to invoke the fierce spells of our ancestors as enjoined by father Manu and as Harihara and Bukka had done in the southern country before embarking on the destruction of the makkha-राक्षस-s. Before we make those oblations we have protect and purify ourselves with the शान्त्युदक as was done by the young brahman विचारिन्, the knower of the अथर्वाङ्गिरस-श्रुति."

Early in the day Lootika placed the leaves of the secret plants known as citi, tumburu, शिग्रु, देवदारु, पीलु, kapu, विपर्वा, रोदाका, वृक्कवती, नाडा and निर्दहन्ती in the शान्त्युदक pitcher. As she did so Somakhya recited the following incantation of his ancient ancestors:

[सहस्व यातुधानान्]{style="color:#0000ff;"}\
[सहस्व यातुधान्यः ।]{style="color:#0000ff;"}\
[सहस्व सर्वा रक्षांसि]{style="color:#0000ff;"}\
[सहमानास्य् ओषधे ॥]{style="color:#0000ff;"}

Conquer the the यातुधान-s, conquer the यातुधानी-s, conquer all the रक्ष-s. You are conquering, O herb!

L: "O आर्य, tradition holds that a subset of these plants belong to the Atharvan-s or the भृगु-s and the other subset to the आङ्गिरस-s. How do we place them in each of these categories."\
S: "That is indeed so वरारोहा. Those from citi to पीलु are those of my ancestors while those from kapu to निर्दहन्ती belong to yours. Such is the lore passed down by the knowers of the अथर्वण-श्रुति."

Then, as Lootika poured water from her कमण्डलु into the pitcher Somakhya standing up held his palms in the अञ्जलिक-मुद्रा and recited the following incantations of his ancient ancestors:

[विश्वाद् रिप्रान् मुञ्चत सिन्धवो नो]{style="color:#0000ff;"}\
[यान्य् एनांसि चकृमा तनूभिः ।]{style="color:#0000ff;"}\
[इन्द्र-प्रशिष्टा वरुण प्रसूता]{style="color:#0000ff;"}\
[आ सिङ्चतापो मध्व आ समुद्रे ॥]{style="color:#0000ff;"}

O rivers, free us from all the impurity and from whatever sins we have committed with our bodies. Directed by Indra, impelled by वरुण, O waters pour honey into the sea.

[यूयं मित्रस्य वरुणस्य योनिर्]{style="color:#0000ff;"}\
[यूयं सोमस्य धेनवो मधिष्ठाः ।]{style="color:#0000ff;"}\
[युष्मान् देवीर् देव आ क्षियन्तीन्दुर्]{style="color:#0000ff;"}\
[यूयं जिन्वत ब्रह्म-क्षत्रम् आपः ॥]{style="color:#0000ff;"}

You are the seat of Mitra and वरुण, you are the sweetest cows of Soma. In you, O goddesses, dwells the godly Moon. You, O waters, must impel the the brahma and the क्षत्र.

[पुनाना आपो बहुधा स्रवन्ति]{style="color:#0000ff;"}\
[इमांश् च लोकान् प्रदिशश् च सर्वाः ।]{style="color:#0000ff;"}\
[पुनन्त्व् अस्मान् दुरिताद् अवद्यान्]{style="color:#0000ff;"}\
[मुङ्चन्तु मृत्योर् निरृतेर् उपस्थात् ॥]{style="color:#0000ff;"}

The waters, becoming pure, stream in many directions, throughout these worlds and throughout all the quarters of space. Let them purify us from evil, from disgrace; let them release us from death, from the lap of the goddess निरृति.

Then he muttered the सावित्री and recited the त्रिषप्तीय hymn.

L: "Should the goddesses of the waters be correctly understood as Apsaras-es with उर्वशी, सहजन्या, घृताची and मेनका at their head? Their being called upon to called upon to impel the brahma and the क्षत्र, who lead the society of the आर्य-s, is similar to their being invoked to protect the brahma-क्षत्र in the यजुष् incantations known as the राष्ट्रभृत्-s."\
S: "One may understand it that way from the etymology of Apsaras-es, which is a extension of the more basic form आपः. Indeed, the राष्ट्रभृत्-s further the equation with the Apsaras-es. It has also been explicitly indicated by my ancient clansman Kavi, the son of भृगु:

[समुद्रिया अप्सरसो मनीषिणम्]{style="color:#0000ff;"}\
[आसीना अन्तर् अभि सोमम् अक्षरन् ।]{style="color:#0000ff;"}

The marine Apsaras-es, sitting within, have streamed toward Soma of inspired mantra-thought.

Here the word समुद्रिया indicates their belonging to the watery realm. Further, the sense in which they are used in the ऋक् of Kavi relates to them being termed "somasya dhenavo मधिष्ठाः" in the mantra-s we just deployed. This identity of the Apsaras-es as the goddesses of the waters is reinforced in the famous ritual of the apsaras-es and the gandharva of the ocean, which we shall perform at a different time. There we shall encounter another dangerous class of 80 Apsaras-es who are known as the उलुङ्गुलुका-s, coming in groups of 31, 4, 10, 10, 25, associated with the asura उलुङ्गुल. We shall see in a later rite how to ward off danger from them by invoking Indra and obtain benefits instead."

L: "Ah, I recall that while studying that rite I saw parallels between the अथर्वणिच् divisions of the Apsaras-es and the Greek division of the water-nymphs into Naiads, Nereids, Oceanids and the like. This brings up another point for discussion. The waters are described as streaming through the worlds. The earthly realm is easily understood: the oceans and rivers and the like. The atmospheric realm too is obvious. But what about the heavenly realm? Even the moon is said to be contained in them. This seems to be reiterated in the तैत्तिरीय-श्रुति where waters are described thus in the context of 'watery' नक्षत्र of पूर्वाषाढा:

[या दिव्या आपः पयसा संबभूवुः ।]{style="color:#0000ff;"}\
[या अन्तरिक्ष उत पार्थिवीर्याः ।]{style="color:#0000ff;"}

Here, the enumeration of the waters begins with the divine waters followed by the more familiar ones. This suggests that our ancestors saw the 'waters' not just in the earthly regions but extending even to the heavenly realm. What might this mean?"

S: "That used seem puzzling to me too. But the clue comes from that very ऋक् regarding पूर्वाषाढा that you cited. I slips out of my mouth involuntarily whenever I see the said constellation on a clear night with Milky Way. It is telling that its states: '[या दिव्या आपः पयसा संबभूवुः ।]{style="color:#0000ff;"}' Which heavenly waters had their being by milk. We hold that the milk mentioned here is none other than the Milky Way --- an analogy which is rather clear to anyone who has beheld the core of our galaxy on a good night. Hence, it is possible that the heavenly waters were an allusion to the Milky Way. This might also apply to the term दिव्यं nabhas to which we offer the बर्हिष् at the end of the ritual."

L: "The idea of दिव्या आपः is also philosophically satisfying in a current sense. After all where did water on the earth come from? Most likely from the asteroids, the comets, and the objects of the dwarf planet belt. Thus, we may say all the earthly waters are indeed from the दिव्या आपः."

Later that evening knotting the yoktra girdle around Lootika's waist Somakhya indicated to her to cover the mouth of the pitcher with her palms. Then he uttered the incantations:

[शं नो मित्रः शं वरुणः]{style="color:#0000ff;"}\
[शं विष्णुः शं प्रजापतिः ।]{style="color:#0000ff;"}\
[शं न इन्द्रो बृहस्पतिः]{style="color:#0000ff;"}\
[शं नो भवत्व् अर्यमा ॥]{style="color:#0000ff;"}

Weal to us Mitra, weal to us वरुण, weal to us विष्णु, weal to us प्रजापति, weal to us Indra, weal to us बृहस्पति. May Aryaman be beneficent to us.

[शं नो ग्रहाश् चान्द्रमसाः]{style="color:#0000ff;"}\
[शम् आदित्यश् च राहुणा ।]{style="color:#0000ff;"}\
[शं नो मृत्युर् धूमकेतुः]{style="color:#0000ff;"}\
[शं रुद्रास् तिग्मतेजसः ॥]{style="color:#0000ff;"}

Weal to us the planets and the Moon, weal to us the Sun and the eclipses, weal to us death and the comets, weal the Rudra-s of sharp luster.

[शं रुद्राः शं वसवः]{style="color:#0000ff;"}\
[शम् आदित्याः शम् अग्नयः ।]{style="color:#0000ff;"}\
[शं नो महर्षयो देवाः]{style="color:#0000ff;"}\
[शं देव्याः शं बृहस्पतिः ॥]{style="color:#0000ff;"}

Weal the Rudra-s, weal the Vasu-s, weal the आदित्य-s, weal the Agni-s, weal to us the great seers and gods, weal to us the goddesses and बृहस्पति.

Then Somakhya steeped a fistful of darbha in the water and uttered the following incantation:

[पृथिवी शान्तिर् अन्तरिक्षं शान्तिर् द्यौः शान्तिर्]{style="color:#0000ff;"}\
[आपः शान्तिर् ओषधयः शान्तिर् वनस्पतयः शान्तिर्]{style="color:#0000ff;"}\
[विश्वे मे देवाः शान्तिः सर्वे मे देवाः शान्तिः]{style="color:#0000ff;"}\
[शान्तिः शान्तिः शान्तिभिः ।]{style="color:#0000ff;"}\
[यद् इह घोरं यद् इह क्रूरं]{style="color:#0000ff;"}\
[यद् इह पापं तच् छान्तं]{style="color:#0000ff;"}\
[तच् छिवं सर्वम् एव शम् अस्तु नः ॥]{style="color:#0000ff;"}

The Earth peaceful, the atmosphere peaceful, the heaven peaceful, the waters peaceful, the plants peaceful, the trees peaceful, all gods my peace, the collection of gods my peace, peace be peaceful. By peace, I render all that is terrible here, all that is cruel here, all that is sinful here, peaceful and auspicious. Let all this be weal to us.

Then he sprinkled the water on himself, her and around the vedi with the below incantations:

[अग्निर् आयुष्मान् स वनस्पतिभिर् आयुष्मान् ।]{style="color:#0000ff;"}\
[स मायुष्मान् आयुस्मन्तं कृणोतु ॥]{style="color:#0000ff;"}

Agni is full of life: with the trees he is full of life. Full of life let him make me full of life.

[वायुर् अयुष्मान् सो 'न्तरिक्षेणायुष्मान् ।]{style="color:#0000ff;"}\
[स मायुष्मान् आयुस्मन्तं कृणोतु ॥]{style="color:#0000ff;"}

वायु is full of life: with the atmosphere he is full of life. Full of life let him make me full of life.

[इन्द्र आयुष्मान् स विर्येणायुष्मान् ।]{style="color:#0000ff;"}\
[स मायुष्मान् आयुष्मन्तं कृणोतु ॥]{style="color:#0000ff;"}

Indra is full of life: with manliness he is full of life. Full of life let him make me full of life.

[देवा आयुष्मन्तस् ते 'मृतेनायुष्मन्तः ।]{style="color:#0000ff;"}\
[ते मायुष्मन्त आयुष्मन्तं कृण्वन्तु ॥]{style="color:#0000ff;"}

The gods are full of life: with ambrosia they are full of life. Full of life let them make me full of life.

They seated themselves on the reddish brown bull skin to commence the ritual. Somakhya then placed a fire-stick of the विकङ्कत plum in the वेदी dipped in ghee: "गौतमी, it has been stated in the श्रुति: [वज्रो वै विकङ्कतः ।]{style="color:#0000ff;"}; hence, for such a ritual we use this samidh. It is an ancient medicinal plant, perhaps used an Indian substitute for the willow." He then muttered the वारवन्तीय incantation to the god Agni of his ancient clansman who had transferred to the clan of the वैश्वामित्र-s:

[अश्वं न त्वा वारवन्तं]{style="color:#0000ff;"}\
[वन्दध्या अग्निं नमोभिः ।]{style="color:#0000ff;"}\
[सम्राजन्तम् अध्वराणाम् ॥]{style="color:#0000ff;"}

This is to praise you, Agni, with salutations, the one like tail of the horse | the one of excellent things, presiding like the emperor over the ritual.

The great god वैश्वानर, who takes the oblations to the deva-s, blazed forth illuminating their fire-room with a purple hue in the dim twilight even as the last rays of the sun withdrew from the sky. Lootika looking at the ever-moving flames was deeply moved by this thought: It is indeed for this reason our ancient आर्य ancestors called him वारवन्त् --- his sinuous movements are verily like the prancing tail of a horse dashing across the grassy steppe. His changing from form to form, like the vibhakti-s of the substantives, he truly embodies the ever-moving aspect of the ऋत. Hence, my ancestor Nodhas had stated thus when he performed a sacrifice for the king शातवनेय:

[मूर्धा दिवो नाभिर् अग्निः पृथिव्या]{style="color:#0000ff;"}\
[अथाभवद् अरती रोदस्योः ।]{style="color:#0000ff;"}\
[तं त्वा देवासो 'जनयन्त देवं]{style="color:#0000ff;"}\
[वैश्वानर ज्योतिर् इद् आर्याय ॥]{style="color:#0000ff;"}

The head of heaven and the navel of the earth is Agni. Then he became the spoked wheel of the world-hemispheres. The gods generated you, that god वैश्वानर, verily as light for the आर्य-s.

Indeed, that wheel in the midst of the world-hemispheres indicates his ever-moving moving form as the celestial cycles. She snapped out her reveries as she had to fill the sruk and hand it over to Somakhya to pour out the great attacking oblations.

[yo no दिप्साद् adipsato]{style="color:#0000ff;"}\
[दिप्सतो यश् च दिप्सति ।]{style="color:#0000ff;"}\
[वैश्वानरस्य दंष्ट्रयोर्]{style="color:#0000ff;"}\
[अग्नेर् अपि दधामि तम् ॥ + वौ३षट्]{style="color:#0000ff;"}

Him who, though unharmed, would harm us, and him who, harmed, would harm us,\
I verily place between the two fangs of Agni वैश्वानर.

Then Somakhya made the sruva offering to the foe-killing वायु:

[सगराय शत्रुहणे स्वाहा । शर्णिलाय शत्रुहणे स्वाहा । समुद्राय शत्रुहणे स्वाहा ।]{style="color:#0000ff;"}\
[सान्धसाय शत्रुहणे स्वाहा । इषिराय शत्रुहणे स्वाहा । अवस्यवे शत्रुहणे स्वाहा ।]{style="color:#0000ff;"}\
[वायवे शत्रुहणे स्वाहा । वाताय शत्रुहणे स्वाहा । मातरिश्वने शत्रुहणे स्वाहा । पवमानाय शत्रुहणे स्वाहा ॥]{style="color:#0000ff;"}

L: "These 10, sometimes obscure, names of वायु bring to mind some यजुष् oblations that I have heard my father making from the तैत्तिरीय-श्रुति."\
S: "There are two sets of such oblations among the तैत्तिरीयक-s. The first of those are related to the 'offering of the seven winds' or the वात-नामानि:

[समुद्राय त्वा वाताय स्वाहा । सलिलाय त्वा वाताय स्वाहा । अनाधृष्याय त्वा वाताय स्वाहा । अप्रतिधृष्याय त्वा वाताय स्वाहा । अवस्यवे त्वा वाताय स्वाहा । दुवस्वते त्वा वाताय स्वाहा । शिमिद्वते त्वा वाताय स्वाहा ॥]{style="color:#0000ff;"}

The second of those are rather remarkable in associating वायु with the the Marut-s, a development which completely enveloped the latter in the epic period, as in the रामायण:

[समुद्रो 'सि नभस्वान् आर्द्रदानुः शम्भूर् मयोभूर् अभि मा वाहि स्वाहा ।]{style="color:#0000ff;"}\
[मारुतो 'सि मरुतां गनः शम्भूर् मयोभूर् अभि मा वाहि स्वाहा ।]{style="color:#0000ff;"}\
[अवस्युर् असि दुवस्वाञ् शम्भूर् मयोभूर् अभि मा वाहि स्वाहा ॥"]{style="color:#0000ff;"}

L: "It is notable that while these are पौष्टिक, our AV prayoga is अभिचारिक. This is rather reminiscent of the Iranian tradition where a long series of वातनामानि are prescribed to be deployed in the sacrifice to be performed when an Iranian is under attack or during war. However, what is consistent across such वायु invocations in our vaidika traditions is the association of वायु with moisture and the waters: salila, sagara, samudra, शर्णिल and आर्द्र-दानुः(!)."\
S: "In fact that watery connection is seen in the very same Iranian collection of incantations you alluded to, suggesting that this is an ancient feature from the Indo-Iranian period referring to वायु as the bearer of moisture and the stirrer of water-bodies. The Avestan incantations states:

[ýazâi apem ca bakhem ca]{style="color:#008000;"}\
[ýazâi âxshtîm hãm-वैञ्त्îम् ca]{style="color:#008000;"}\
[सुय्ãम् च कतरेम्चित्]{style="color:#008000;"}\
[tem vayêmcit ýazamaide]{style="color:#008000;"}\
[tem vayêmcit zbayamahi]{style="color:#008000;"}

I will sacrifice to the waters and to him who divides them. I will sacrifice to the peaceful one, whose breath is beneficent, and to weal, both of them. To this Vayu do we sacrifice, this Vayu do we invoke."

Next they prepared the sruk for offering to Indra:

[इन्द्रस्य बाहू स्थविरौ वृषाणौ]{style="color:#0000ff;"}\
[चित्रा इमा वृषभौ पारयिष्णू ।]{style="color:#0000ff;"}\
[तौ योक्षे प्रथमो योग आगते]{style="color:#0000ff;"}\
[याभ्यां जितम् असुराणां स्वर् यत् ॥+ वौ३षट्]{style="color:#0000ff;"}

Indra's two arms, firm and manly, these two are wondrous as stud bulls. The \[time of] yoking has arrived, I first yoke these two, by which the heaven of the Asura-s was conquered.

S: "This is the famous Atharvanic apratiratha-ऋक्. The arms of Indra are compared to fierce bulls, something our ancestors would seen in a rather unadulterated form on the steppes from even before the age of the श्रुति. But do you suspect a wordplay here?"\
L: "Seems so. While the reference is to the two bulls, the time of yoking I believe actually refers to horses being yoked to the ratha, that key war-conveyance of the आर्य-s. The time of yoking is verily that of the warrior yoking the horses for war. But what the mantra conveys is that even before the horses we first yoke the Vajra- and the arrow-bearing arms of Maghavan that bring victory."\
S: "Good."

They prepared the sruk for the next oblation with the below incantation:\
[अतिसृत्यातिसरा]{style="color:#0000ff;"}\
[इन्द्रस्यौजसा हत ।]{style="color:#0000ff;"}\
[अविं वृक इव मथ्नीत]{style="color:#0000ff;"}\
[ततो वो जीवन् मा मोचि]{style="color:#0000ff;"}\
[प्राणम् अस्यापि नह्यत ॥ + वौ३षट्]{style="color:#0000ff;"}

Having overtaken him the spells slay \[him] with the might of Indra, snatch him like a wolf a sheep. Then let him not get away from you alive, indeed block his breath.

L: "The atisara which is dispatched reminds one of the missile dispatched in the hair-raising combat between Arjuna and कर्ण in the national epic, which was much like that of शतमन्यु and वृत्र. There when कर्ण deployed the missile of राम भार्गव it is said to be of अथर्वणिच् nature:

[रामाद् उपात्तेन महामहिम्ना]{style="color:#800000;"}\
[आथर्वणेन+अरिविनाशनेन ।]{style="color:#800000;"}\
[तद् अर्जुनास्त्रं व्यधमद् दहन्तं]{style="color:#800000;"}\
[पार्थं च बाणैर् निशितैर् निजघ्ने ॥]{style="color:#800000;"}

With the \[missile] of great might obtained from राम, the अथर्वणिच् foe-destroyer, [कर्ण] smashed that fiery weapon of Arjuna and he pierced पार्थ with sharp arrows.

Then again when finally Arjuna killed कर्ण with a gigantic missile it is said:

[कृत्याम् अथर्वाङ्गिरसीम् इवोग्रां]{style="color:#800000;"}\
[दीप्ताम् असह्यां युधि मृत्युनापि ।]{style="color:#800000;"}\
[ब्रुवन् किरीटी तम् अतिप्रहृष्टो]{style="color:#800000;"}\
[अयं शरो मे विजयावहो 'स्तु ॥]{style="color:#800000;"}

Having fired the blazing weapon fierce as an अथर्वाङ्गिरस कृत्या incapable of being endured by Death herself in battle, the crowned one (Arjuna) said with great joy: "Let this arrow be victory-bearing to me."

S: "For a V2 in battle the atisara might indeed be his actual weapon, while a V1 performing such a prayoga might visualize it as striking his foe like a wolf seizing a sheep. In the national epic there is another such account of the deployment of a शक्ति made by त्वष्टृ for Rudra by युधिष्ठिर which was be worshiped with such mantra-s. Let us read it out in full for it is most inspiring:

[ततस् तु शक्तिं रुचिरोग्र-दण्डां]{style="color:#800000;"}\
[मणि-प्रवालोज्ज्वलितां प्रदीप्ताम् ।]{style="color:#800000;"}\
[चिक्षेप वेगात् सुभृशं महात्मा]{style="color:#800000;"}\
[मद्राधिपाय प्रवरः कुरूणाम् ॥]{style="color:#800000;"}

The great soul, the foremost of the Kuru-s [युधिष्ठिर] then hurled with great force at the king of the Madra-s that blazing शक्ति with a well-made and fierce handle blazing \[like] with the shine of gems and corals.

[दीप्ताम् अथैनां महता बलेन]{style="color:#800000;"}\
[सविस्फुलिङ्गां सहसा पतन्तीम् ।]{style="color:#800000;"}\
[प्रैक्षन्त सर्वे कुरवः समेता]{style="color:#800000;"}\
[यथा युगान्ते महतीम् इवोल्काम् ॥]{style="color:#800000;"}

All the Kuru-s together saw that blazing dart emitting sparks as it powerfully flew hurled with great might like a meteorite \[falling] at the end of the yuga.

[तां कालरात्रीम् इव पाश-हस्तां]{style="color:#800000;"}\
[यमस्य धात्रीम् इव चोग्ररूपाम् ।]{style="color:#800000;"}\
[सब्रह्म-दण्ड-प्रतिमाम् अमोघां]{style="color:#800000;"}\
[ससर्ज यत्तो युधि धर्मराजः ॥]{style="color:#800000;"}

Guiding it, the धर्मराज hurled that \[missile], which was of dreadful form like the goddess of the night of dissolution holding a noose, the midwife who birthed Yama, and infallible as the rod of Brahman.

[गन्ध-स्रग् अर्ग्यासन-पानभोजनैर्]{style="color:#800000;"}\
[अभ्यर्चितां पाण्डुसुतैः प्रयत्नात् ।]{style="color:#800000;"}\
[संवर्तकाग्नि-प्रतिमां ज्वलन्तीं]{style="color:#800000;"}\
[कृत्याम् अथर्वाङ्गिरसीम् इवोग्राम् ॥]{style="color:#800000;"}

With much effort the sons of Pandu had worshiped \[that missile] with perfumes and garlands, water pourings, a seat, drinks and food. It blazed like the fire of destruction and was fierce as an अथर्वाङ्गिरस कृत्या.

[ईशान-हेतोः प्रतिनिर्मितां तां]{style="color:#800000;"}\
[त्वष्ट्रा रिपूणाम् असु-देह-भक्षाम् ।]{style="color:#800000;"}\
[भूम्य्-अन्तरिक्षादि-जलाशयानि]{style="color:#800000;"}\
[प्रसह्य भूतानि निहन्तुम् ईशाम् ॥]{style="color:#800000;"}

For the sake of ईशान this missile was engineered by त्वष्टृ; it consumes the breath and bodies of foes. By its force it can violently destroy the Earth, the atmosphere, water-bodies and living beings.

[बल-प्रयत्नाद् अधिरूढ-वेगां]{style="color:#800000;"}\
[मन्त्रैश् च घोरैर् अभिमन्त्रयित्वा ।]{style="color:#800000;"}\
[ससर्ज मार्गेण च तां परेण]{style="color:#800000;"}\
[वधाय मद्राधिपतेस् तदानीम् ॥]{style="color:#800000;"}

Thus, with a forceful effort, having inspired it with fierce mantra-s, [युधिष्ठिर] released it with utmost momentum along the best trajectory for the destruction of the lord of the Madra-s.

[हतो 'स्य् असाव् इत्य् अभिगर्जमानो]{style="color:#800000;"}\
[रुद्रो 'न्धकायान्तकरं यथेषुम् ।]{style="color:#800000;"}\
[प्रसार्य बाहुं सुदृढं सुपाणिं]{style="color:#800000;"}\
[क्रोधेन नृत्यन्न् इव धर्मराजः ॥]{style="color:#800000;"}

"There, you are killed", roaring thus the धर्मराज \[hurled the missile], having stretched his arm with a firm good hand, as if dancing in wrath, even as Rudra had shot his arrow for bringing the end of Andhaka.

As you would have noted, the weapon is said to be like the अथर्वाङ्गिरस कृत्या and specifically it was inspired by mantra-s. Thus, indeed a V2 might inspire his weapon as an atisara before its deployment."

L: "But what about the deployment of the कृत्या-prayoga related to the famous सूक्त starting with यां kalpayanti...?"\
S: "That is used primarily in the deflecting mode. But this is primarily in the attacking mode in the vaidika tradition."

Then they continued with the next oblation:

[अथैनम् इन्द्र वृत्रहन्न्]{style="color:#0000ff;"}\
[उग्रो मर्मणि विध्य ।]{style="color:#0000ff;"}\
[अत्रैवैनम् अभि तिष्ठ]{style="color:#0000ff;"}\
[शक्र मेद्य् अहं तव ।]{style="color:#0000ff;"}\
[अनु त्वेन्द्रा रभामहे]{style="color:#0000ff;"}\
[स्याम सुमतौ तव ॥ + वौ३षट्]{style="color:#0000ff;"}

Now here O fierce Indra, the वृत्र-slayer, pierce him in his marman. Trample him right here. O शक्र, I am your votary. We take hold of you, O Indra. May we be in your favor.

L: "It is indeed clear from this mantra that the आर्य chooses Indra, whereas the शत्रु is anindra as has been clearly stated in the श्रुति."\
S: "Next comes the oblation to delude the mind of the foe."

[यद् वेद राजा वरुणो]{style="color:#0000ff;"}\
[वेद देवो बृहस्पतिः ।]{style="color:#0000ff;"}\
[indro yad वृत्रहा veda]{style="color:#0000ff;"}\
[तत् सत्यम् चित्तमोहनम् ॥ + वौ३षट्]{style="color:#0000ff;"}

That which King वरुण knows, which the god बृहस्पति knows, which Indra the वृत्र-slayer knows, is the truth which bewilders the mind \[of the foe].

[शर्वेण नीलशिखण्डेन]{style="color:#0000ff;"}\
[भवेन मरुतां पित्रा ।]{style="color:#0000ff;"}\
[विरूपाक्षेण बभ्रूणा]{style="color:#0000ff;"}\
[वाचं वदिष्यतो हतः ॥ + वौ३षट्]{style="color:#0000ff;"}

By शर्व, but the blue-crested one, by Bhava, by the father of the Marut-s, by the odd-eyed one and by the brown one the voice of he who verbally \[attacks] us is smitten.

[शर्व नीलशिखण्ड]{style="color:#0000ff;"}\
[वीर कर्मणि-कर्मणि ।]{style="color:#0000ff;"}\
[इमाम् अस्य प्राशं जहि]{style="color:#0000ff;"}\
[येनेदं विभजामहे ॥ + वौ३षट्]{style="color:#0000ff;"}

O शर्व, the blue-crested one, O hero, in every rite smite the food of this one with whom we portion this out.

L: This triad strikes me as being one of the vaidika-prayoga-s comparable to the तान्त्रिक- prayoga-s like that of बगलामुल्खी or वराही. बगला is used identically for the वाच-stambhana-प्रक्रिया while the sow-headed संकेतयोगिनी may be deployed for mohanam like the first ऋक्. The last mantra appears a little obscure to me.\
S: "One who is has उभयवीर्य in both the तान्त्रिक and the vaidika prayoga-s is truly an accomplished मन्त्रवादिन्, like that one from the कलिङ्ग country. Each path has its own rigor and strictures; hence, in the least it is good to be educated in both. But for a V1 the priority is the vaidika rite. As for the last mantra, I believe that it indicates a भ्रातृव्य with whom we are competing for space or glory."

Finally, they made the mysterious oblation to Rudra and his son.

[त्वं देवानाम् असि रुद्र श्रेष्ठ]{style="color:#0000ff;"}\
[तवस् तवसाम् उग्रबाहो ।]{style="color:#0000ff;"}\
[हृणीयसा मनसा मोदमान]{style="color:#0000ff;"}\
[आ बभूविथ रुद्रस्य सूनो ॥ + वौ३षट्]{style="color:#0000ff;"}

You are the chief of the gods O Rudra, the mightiest of the mighty, O one of formidable arms. With fury, delighting in your mind, you had come into being, O son of Rudra.

L: "That is quite remarkable! The son of Rudra clearly in singular here! It seems to indicate the emergence of a proto-Skanda. In my readings I have only encountered rudrasya सुनुः in singular in that glorious सूक्त to the Marut-s of my ancestor Nodhas."\
S: "That सूक्त of Nodhas (RV 1.64) is truly an awe-inspiring one to the Marut-s who are entirely spoken of in plural as is usual. But then as you say we mysteriously get this singular occurrence of the singular form in it (RV 1.64.12):

[घृषुम् पावकं वनिनं विचर्षणिं]{style="color:#0000ff;"}\
[रुद्रस्य सूनुं हवसा गृणीमसि ।]{style="color:#0000ff;"}

The agile, pure, winning, all-seeing son of Rudra we extol with an invocation.

Everywhere else in the RV the term is always in plural --- rudrasya सुनवः except for the rather notable similar singular usage by the other आङ्गिरस clan, the भरद्वाज-s, in their parallel सूक्त to the Marut-s (RV 6. 66):

[तं वृधन्तम् मारुतम् भ्राजद्-ऋष्टिं]{style="color:#0000ff;"}\
[रुद्रस्य सूनुं हवसा विवासे ।]{style="color:#0000ff;"}

This growing मारुत, with a blazing spear, the son of Rudra I bring with with an invocation.

Hence, it does seem that these rare singular usages for the Marut-s in the श्रुति point to the latent tendency for consolidation of the Marut-s into a single form. This process was indeed likely on the pathway to the emergence of Skanda in the para-Vedic Indo-Iranian borderlands. I could also point to a more circumstantial connection via the peculiar word हृणीयसा in the AV mantra. Its root is of early Indo-European vintage and can be related to the fury of the gods such as Rudra and वरुण. Hence, it is notable that in the late अथर्वणिच् tradition the hymns to fury, the Manyu-सूक्त-s are associated with a hapax ऋषि named ब्रह्मास्कन्द, which is none other than the name of our patron deity. Further, those Manyu hymns are used in the Skanda ritual of the late AV tradition."

[अयज्वनः साक्षि विश्वस्मिन् भरो३म् ॥]{style="color:#0000ff;"}

