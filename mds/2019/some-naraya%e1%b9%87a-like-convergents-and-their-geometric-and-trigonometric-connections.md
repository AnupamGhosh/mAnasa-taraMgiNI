
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some नारायण-like convergents and their geometric and trigonometric connections](https://manasataramgini.wordpress.com/2019/12/08/some-naraya%e1%b9%87a-like-convergents-and-their-geometric-and-trigonometric-connections/){rel="bookmark"} {#some-नरयण-like-convergents-and-their-geometric-and-trigonometric-connections .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 8, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/12/08/some-naraya%e1%b9%87a-like-convergents-and-their-geometric-and-trigonometric-connections/ "10:13 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While playing with an iterative geometric construction in our youth we discovered for ourselves a particular right triangle whose sides are in the proportion  $1: \sqrt{\phi} : \phi$ , where  $\phi= \tfrac{1+\sqrt{5}}{2}$  is the Golden Ratio. This triangle is of course famous as being the basis of the largest triangles of the श्रीयन्त्र and the त्रिपुरशेखर- (बाला-) yantra of the [श्रीकुल tradition](https://manasataramgini.wordpress.com/2019/10/06/the-minimal-triangle-circumscribing-a-semicircle/) and also the famous [Great Pyramid](https://manasataramgini.wordpress.com/2016/08/23/some-meanderings-among-golden-stuff/) of the ancient Egyptians. Revisiting it, we were able to provide the trigonometric proof for our construction, which in turn led several sequences of interest. The construction goes thus (Figure 1):

*[![root_Golden_convergence](https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png){.size-full .wp-image-12093 .aligncenter attachment-id="12093" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="root_Golden_convergence" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png" orig-size="1498,807" permalink="https://manasataramgini.wordpress.com/2019/12/08/some-naraya%e1%b9%87a-like-convergents-and-their-geometric-and-trigonometric-connections/root_golden_convergence/" sizes="(max-width: 1498px) 100vw, 1498px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png 1498w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png 1024w"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/root_golden_convergence.png)Figure 1*


 1.  Start with an isosceles right triangle  $\triangle AOB$  or  $\triangle A'OB$ , whose equal sides are 1 each and hypotenuse is  $\sqrt{2}$ .

 2.  Slide the hypotenuse down to point  $O$  create a segment equal to it in length  $\overline{OC_1}$ . Join point  $C_1$  to  $B$ .

 3.  Draw a perpendicular to  $\overline{C_1B}$  to meet the base line  $\overleftrightarrow{AC_1}$  to meet it at  $C_1'$ . Joint  $C_1'$  to  $B$  to create a right triangle  $\triangle C_1'OB$ .

 4.  Draw a segment equal to the hypotenuse  $\overline{C_1'B}$  of this new triangle along the baseline  $\overleftrightarrow{AC_1}$  to get a point at the distance equal to  $\overline{C_1'B}$  from  $O$ .

 5.  Iterate the above steps with this point.

What will happen if you keep iterating thus? We learned to our surprise in our youth that it converges to a right triangle  $\triangle K_1BK_2$  (shaded yellow in Figure 1), whose sides will be  $(\sqrt{\phi}, \phi, \phi^{3/2})$ . Further the altitude  $\overline{OB}$  will divide the hypotenuse into segments  $\overline{OK_1}=\sqrt{\tfrac{1}{\phi}}$  and  $\overline{OK_1}=\sqrt{\phi}$ .

The trigonometric proof we obtained for this is rather simple. It essential comprises of finding the attractor of the following map:

 $$x_{n+1}=\cos\left(\arctan(x_n)\right)$ ; where  $x_0=1$$ 

 $$\cos\left(\arctan(x)\right) = \dfrac{1}{\sqrt{1+x^2}}$$ 

 $$\therefore x_1=\sqrt{\dfrac{1}{2}}; \; x_2= \sqrt{\dfrac{2}{3}}; \; x_3= \sqrt{\dfrac{3}{5}}; \; x_4=\sqrt{\dfrac{5}{8}}...$$ 

Given the मात्रा-meru-पङ्क्ति:  $f\lbrack 1\rbrack =1, f\lbrack 2\rbrack =2, f\lbrack n\rbrack =f\lbrack n-1\rbrack +f\lbrack n-2\rbrack ; x_n=\sqrt{\dfrac{f\lbrack n\rbrack }{f\lbrack n+1\rbrack }}$ 

 $$\displaystyle \therefore \lim_{n \to \infty} x_n= \sqrt{\dfrac{1}{\phi}}$$ 

One can see that the  $x_n$  of this map corresponds to the bases of the right-side triangles in successive steps (Figure 1), starting with  $\overline{OA}, \overline{OC_1'}...\overline{OK_1}$ . Thus,  $\overline{OK_1}=\tfrac{1}{\sqrt{\phi}} \;\;\; _{...\blacksquare}$ 

Now, we specify a similar map of the form:

 $$y_{n+1}=\sin\left(\arctan(y_n)\right)$ ; where  $y_1=1$$ 

Given that  $\sin\left(\arctan(y)\right) =\tfrac{y}{\sqrt{1+y^2}}$ , we get the sequence:

 $$y_2=\dfrac{1}{\sqrt{2}}; \; y_3=\dfrac{1}{\sqrt{3}}; \; y_4=\dfrac{1}{\sqrt{4}}... \; y_n=\dfrac{1}{\sqrt{n}}$$ 

Thus,  $\displaystyle \lim_{n \to \infty} x_n=0$ 

Inspired by the above, we define the below intertwined maps that generate interesting sequences and convergents:

**Map 1**

 $$x_{n+1}=\cos\left(\arctan\left( x_n\right) \right); \; x_0=1$$ 

 $$y_{n+1}=\sin\left(\arctan\left( x_{n+1}\right)\right)$$ 

Here, as  $n \to \infty\;\; x_{n} \to \dfrac{1}{\sqrt{\phi}}, \; y_{n} \to \dfrac{1}{\phi}$ 

We see that  $x_n \rightarrow \sqrt{\dfrac{1}{2}}; \sqrt{\dfrac{2}{3}}; \sqrt{\dfrac{3}{5}}; \sqrt{\dfrac{5}{8}}...$  and  $y_n \rightarrow \sqrt{\dfrac{1}{3}}; \sqrt{\dfrac{2}{5}}; \sqrt{\dfrac{3}{8}}; \sqrt{\dfrac{5}{13}}...$ 

It is apparent that these sequences are constructed from consecutive or every second term of the मात्रा-meru-पङ्क्ति.

**Map 2**

 $$x_{n+1}=\cos\left(\arctan\left( y_n\right) \right); \; y_0=1$$ 

 $$y_{n+1}=\sin\left(\arctan\left( x_{n+1}\right)\right)$$ 

Here, as  $n \to \infty\;\; x_{n} \to \dfrac{1}{\sqrt\lbrack 4\rbrack {2}}, \; y_{n} \to \dfrac{1}{\sqrt{1+\sqrt{2}}}$ 

We see that  $x_n \rightarrow \sqrt{\dfrac{1}{2}}, \sqrt{\dfrac{3}{4}}, \sqrt{\dfrac{7}{10}}, \sqrt{\dfrac{17}{24}}...$ 

This sequence can be derived from the sequence  $\textbf{1, 2, 3, 4, 7, 10, 17, 24...}$  which has  $f\lbrack 1\rbrack =1, f\lbrack 2\rbrack =2$  and alternating terms of the form  $f\lbrack n+1\rbrack =f\lbrack n\rbrack +f\lbrack n-1\rbrack , f\lbrack n+1\rbrack =f\lbrack n\rbrack +f\lbrack n-2\rbrack$ 

Further, we have  $y_n \rightarrow \sqrt{\dfrac{1}{3}}, \sqrt{\dfrac{3}{7}}, \sqrt{\dfrac{7}{17}}....$ 

One notices that it is constructed from the above integer sequence by taking the square root of the ratio of ever second term.

**Map 3**

 $$x_{n+1}=\cos\left(\arctan\left(y_n\right) \right); \; y_0=1$$ 

 $$y_{n+1}=\sin\left(\arctan\left(\frac{x_{n+1}}{y_n}\right)\right)$$ 

Here, as  $n \to \infty\;\; x_{n} \to \dfrac{1}{\sqrt\lbrack 6\rbrack {2\tau}}, \; y_{n} \to \dfrac{1}{\sqrt{\tau}}$ 

where  $\tau$  is the convergent of [नारायण's सामासिक-पङ्क्ति](https://manasataramgini.wordpress.com/2019/07/26/naraya%e1%b9%87as-sequence-madhavas-series-and-pi/) (known in the Occident as the tribonacci constant)

We see that  $x_n \rightarrow 1, \sqrt{\dfrac{1}{2}}, \sqrt{\dfrac{3}{4}}, \sqrt{\dfrac{13}{22}}, \sqrt{\dfrac{367}{536}}, \sqrt{\dfrac{225273}{359962}}...$ 

This sequence defines an oscillatory convergence to  $\tfrac{1}{\sqrt\lbrack 6\rbrack {2\tau}}$  and can be derived from integer sequence:  $\textbf{1, 1, 1, 2, 3, 4, 13, 22, 367, 536, 225273, 359962...}$ . Here,  $f\lbrack 1\rbrack = 1, f\lbrack 2\rbrack =1, f\lbrack 3\rbrack =1$  and every alternate term is defined by the formulae:

 $$f\lbrack n\rbrack =f\lbrack n-1\rbrack + f\lbrack n-3\rbrack ^2$$ 

 $$f\lbrack n\rbrack =f\lbrack n-1\rbrack \cdot f\lbrack n-2\rbrack +f\lbrack n-4\rbrack ^4$$ 

Further, we see that  $y_n \rightarrow \dfrac{1}{\sqrt{3}}, \dfrac{3}{\sqrt{13}}, \dfrac{13}{\sqrt{367}}...$ 

This sequence can again be derived from the above integer sequence by taking every other term, 1, 3, 13, 367 and results in an oscillatory convergence to  $\tfrac{1}{\sqrt{\tau}}$ 

**Map 4**

 $$x_{n+1}=\cos\left(\arctan\left(x_n^2\right) \right); \; x_0=1$$ 

 $$y_{n+1}=\sin\left(\arctan\left(x_{n+1}^2\right)\right)$$ 

To understand the convergent of this map consider the unit circle  $x^2+y^2=1$  and the cubic parabola  $y=x^3$  (Figure 2).

*[![cubic_circle_inter](https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png){.wp-image-12092 .aligncenter attachment-id="12092" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="cubic_circle_inter" large-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png" medium-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png" orig-file="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png" orig-size="1499,1230" permalink="https://manasataramgini.wordpress.com/2019/12/08/some-naraya%e1%b9%87a-like-convergents-and-their-geometric-and-trigonometric-connections/cubic_circle_inter/" height="473" sizes="(max-width: 576px) 100vw, 576px" srcset="https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png 576w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png 1152w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png 150w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png 300w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png 768w, https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png 1024w" width="576"}](https://manasataramgini.wordpress.com/wp-content/uploads/2019/12/cubic_circle_inter.png)Figure 2*

Here,  $\lambda_1$  is the positive intersect of the two curves with coordinates  $(\lambda_x, \lambda_y)$ . Further,  $r=\tfrac{\lambda_y}{\lambda_x}$  is the only real root of the cubic equation  $x^3+x-1=0$ . Thus,  $\lambda_x=\cos(\arctan(r)), \lambda_y=\sin(\arctan(r))$ 

Then, as  $n \to \infty\;\; x_{n} \to \lambda_x, \; y_{n} \to \lambda_y$ 

We see that  $x_n \rightarrow 1, \dfrac{1}{\sqrt{2}}, \dfrac{2}{\sqrt{5}}, \dfrac{5}{\sqrt{41}}, \dfrac{41}{\sqrt{2306}}...$ 

and  $y_n \rightarrow \dfrac{1}{\sqrt{5}}, \dfrac{4}{\sqrt{41}}, \dfrac{25}{\sqrt{2306}}, \dfrac{1681}{\sqrt{8143397}}...$ 

These sequences are related to the integer sequence:  $\textbf{1, 2, 5, 41, 2306, 8143397...}$  which can be described by the formula:  $f\lbrack n\rbrack =f\lbrack n-1\rbrack ^2+f\lbrack n-2\rbrack ^4$  with  $f\lbrack 1\rbrack =1, f\lbrack 2\rbrack =2$ . Then  $x_n$  is derived from successive terms with the square root of the large term.  $y_n$  is derived similarly from every other term with the square of the first and the square root of the next.

**Map 5**

 $$x_{n+1}=\cos\left(\arctan\left( \frac{y_n}{x_n}\right) \right); \; x_0=1, y_0=1$$ 

 $$y_{n+1}=\sin\left(\arctan\left( \frac{x_{n+1}}{y_n}\right)\right); \; y_0=1$$ 

Here, as  $n \to \infty\;\; x_{n}, \; y_{n} \to \dfrac{1}{\sqrt{2}}$ 

The  $x_n$  is interesting in that it begins with  $\tfrac{1}{\sqrt{2}}$  and converges back to it in an oscillatory fashion. This convergence takes the form:

 $$x_n \rightarrow \dfrac{1}{\sqrt{2}}, \dfrac{2}{\sqrt{7}}, \dfrac{7}{\sqrt{101}}, \dfrac{101}{\sqrt{19609}}, \dfrac{19609}{\sqrt{782362082}}...$$ 

The convergence of  $y_n$  to  $\tfrac{1}{\sqrt{2}}$  takes the form:

 $$y_n \rightarrow 1, \sqrt{\dfrac{2}{3}}, \sqrt{\dfrac{7}{13}}, \sqrt{\dfrac{101}{192}}, \sqrt{\dfrac{19609}{39001}}...$$ 

These sequences can be derived from the integer sequence  $\textbf{1, 1, 2 ,3, 7, 13, 101, 192, 19609, 39001...}$ . Given  $f\lbrack 1\rbrack =1, f\lbrack 2\rbrack =1, f\lbrack 3\rbrack =2$ , the subsequent alternate terms of this sequence are defined the formulae

 $$f\lbrack n\rbrack =f\lbrack n-1\rbrack +f\lbrack n-2\rbrack \cdot f\lbrack n-3\rbrack$$ 

 $$f\lbrack n\rbrack =f\lbrack n-2\rbrack ^2+f\lbrack n-1\rbrack \cdot f\lbrack n-4\rbrack ^2$$ 

More generally the convergents obtained for any of the above maps will be reached from any other starting  $(x_0,y_0)$  but using  $x_0=y_0=1$  gives us a convergence via easy to understand explicit sequences.

