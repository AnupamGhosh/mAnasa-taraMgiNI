
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [लिङ्ग-कामादि-सूत्राणि](https://manasataramgini.wordpress.com/2019/02/03/li%e1%b9%85ga-kamadi-sutra%e1%b9%87i/){rel="bookmark"} {#लङग-कमद-सतरण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 3, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/02/03/li%e1%b9%85ga-kamadi-sutra%e1%b9%87i/ "9:48 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Devanagari PDF version](https://manasataramgini.wordpress.com/wp-content/uploads/2019/02/lingakama-1.pdf)

**.. सूत्रपाठः ..\
**\
atha लिङ्ग-कामादि-सूत्राणि व्याख्यास्यामः . जीवसूत्राणुनाम् अनुक्रमेषु परिमेया विकारा जीवा-परम्पराया+अवश्यम् . जन्त्वोः संग्रामस् tasya परमं कारणम् . तस्माद् अजायत जीवसूत्राणुनां व्यूढीकरणम् . ऱेcआ-नाम जीवकार्याणु-कुलं जीवसूत्राणुनां व्यूढीकरणं karoti . मुख्यशो 'नग्निजीवसूत्राणुनाम् . अनग्निजीवसूत्राणु-mithunayor मैथुनात् . idam eva जन्तूनां maithunasya rahasyam ..

अनाभिकानाम् प्रनाभिकानां ca+ अनग्निजीवसूत्राणुनाम् मैथुनं नैमित्तिका प्रक्रिया . नाभिकानाम् मैथुनं निरूपिता प्रक्रिया . व्यूढीकरण-द्विर्भावात् . तस्माद् अजायन्त लिङ्गानि . नाभिकेषु बहुशो लिङ्गे dve . kecid बहुलिङ्गानि प्रदर्शयन्ति . क्याकूनि yukta-कामरूपिण रोमकोष्ठकाश् cety उदाहरणानि . एतेषु लिङ्ग-कोष्ठानाम् परिमाण-bhedo बहुशो नास्ति . द्विलिङ्गस्थितौ लिङ्ग-कोष्ठ्योर् परिमाण-भेदः sadaivodeti (sadaiva udeti) . mahattaro लिङ्ग-कोष्ठः स्त्रीति (स्त्री+iti) . सा बहुशस् तिष्ठति . कनीयः पुमान् iti . sa बहुशो gacchati . लिङ्गकोष्ठानां निर्माणस्य दत्तांश-भेदाज् जायते लिङ्गयोः संग्रामः . किं tu परम्परा-santatyai विपरीतयोर् लिङ्ग-कोष्ठयोर् आकर्षणः संगमनं संयोगश् ca+अवश्यम् . etaddhi मूल-कारणं कामस्य . दत्तांश-संग्रामो 'कर्षणश् ca द्वयोः प्रतिध्राज्योः सम्मेलनाद् dvayor एकः पूर्णं विजयं नापनोति . तस्माज् जायते 'नन्ता स्पर्धा लिङ्गयोः ..

बहुकोष्ठ-जन्तुषु लिङ्गकोष्ठा अन्येभ्यः कोष्ठेभ्यो भिन्नाः संवृताः . तस्माद् विविक्ता उपस्थाः . उदाहरणान्य् ओषधीषु पशुषु ca . तेषु विपरीतानां लिङ्गकोष्ठानां सम्मेलनाय विविधा upastha-लक्षणान्य् avartanta . शेपो योनिः पुष्पञ् cety उदाहरणानि . पशुषु त्रिविधा maithuna-व्यवस्था . धृष्ट-vratam बहुपत्नी-vratam एकपत्नी-vratam वा . बहुपत्नी-vratam dvividham . क्रमेण बहुपत्नयः sadyo bahupatnayo वा . पृष्ठदण्ड-पशुषु प्रायेण 25 जीवसूचनाः prabhavanty एकपत्नी-vratam . Gibbons, orangutans, gorillas, chimpanzees and bonobos te sarve नृबन्धवः . alpa-नृबन्धुष्व् एकपत्नीव्रतम् बहुशः प्रकृतिमत् . अन्येषु नृबन्धुषु बहुपत्नी-व्रतं ca धृष्ट-व्रतं ca सामान्यम् . मानवेषु ca . प्रायेणेयं नृबन्धुनाम् मूल-स्थितिः . स्त्री-बलात्कारो rakta-नृबन्धुनाम् eko मैथुनोपायः . rakta-नृबन्धुनां वृष-जातिर् द्विविधा . नेमिवन्तश् ca+अनेमिवन्तः . nemivanta उग्रा धुनिमन्तश् ca परस्परं yudhyante ca . अनेमिवन्तः शान्ताः pracchanam मैथुनं kurvanti ca . भूरिरेताः पुमान् nityam बहून् मैथुनावकाशान् मृगयते . स्वाण्डानां निषेकाय स्त्री su-जीवसूचना-धारिणम् पुरुषम् प्रतीच्छत्य् अन्यान् निराकरोति ca . तस्माद् बहुविधाः स्पर्धाश् ca प्रदर्शनानि ca ..

मनुष्याणाम् प्रायेण सहजा व्यवस्था धृष्ट-व्रतं ca क्रमेण बहुपत्नी-व्रतं ca sadyo बहुपत्नी-व्रतं ca . lubdhaka-वनगोचरावस्थायां te sarve 'vartanta . किं tu मनुष्यानाम् उपवासितायां स्थितेः प्रभूत्यास् te sarve वैघ्नका abhavan . कास्मत्? स्त्रीभ्यः parasparam पुरुषाणां नैष्ठिकात् संग्रामात् . इदं कारणं एकपत्नी-vratasya+आवश्यकता ध्रुवायोपवासित-जीवनाय . किं tu मानवानां नेतृत्वम् प्रतिभा साहसं ca नृद्रव्याद् udiyanti . अतः स्त्रीनियान-bhedo नृणां स्वाभाविकः . tasya विरोधात् upadravam प्रवर्तितुं शक्नोति . परं tu शान्त्यै ध्रुवायोपवासित-जीवनाय ca sarvebhyo पुरुषेभ्यो न्यूनातिन्यूनम् eka-पत्न्या saha विवाहम् अवश्यम् . स्त्रीषु व्याभिचारिणी-vratam भ्रूणहत्या +अधिपुरुषानुधावनं चेत्यादि प्रवृत्तीनां चोदनात् पुरुष-विषादो'pi vardhate . आधुनिकता नूतन-कृत्रिम-मानव-समाजस्य निर्माणं वा तानि सर्वाणि codanti . अतः प्रायेण + आधुनिकताः paura-संस्कृतेः प्रतिष्ठाम् pratirundhate . यदीदम् तत्वं tarhi मनुष्याणाम् आधुनिक-paura-संस्कृतिश् चिरायुर् नास्ति ..

नमः सोमारुद्राभ्यां नमः प्रजापतये ..

**.. निघण्ठुः ..**\
जीवसूत्राणुः : nucleic acid molecule\
अनग्निजीवसूत्राणुः : DNA\
जीवकार्याणुः : protein\
व्यूढीकरणम् : recombination\
व्यूढीकरण-द्विर्भाव : meiosis\
अनाभिकः : bacteria\
प्रनाभिकः : archaea\
नाभिकः : eukaryotes\
क्याकु : fungus\
yukta-कामरूपिन् : plasmodial slime mold\
कामरूपिन् : amoebozoan\
रोमकोष्ठकः : ciliate\
लिङ्गकोष्ठः : gamete\
दत्ताम्षः : investment\
bahu-कोष्ठ-जन्तुः : multicellular organism\
जीवसूचना : gene\
नृबन्धुः : ape\
alpa-नृबन्धुः : gibbon\
rakta-नृबन्धुः : orangutan\
नृद्रव्यम् : testosterone

