
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Platonic culmination of Euclid and the pentagon-hexagon-decagon identity](https://manasataramgini.wordpress.com/2019/05/05/the-platonic-culmination-of-euclid-and-the-pentagon-hexagon-decagon-identity/){rel="bookmark"} {#the-platonic-culmination-of-euclid-and-the-pentagon-hexagon-decagon-identity .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 5, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/05/05/the-platonic-culmination-of-euclid-and-the-pentagon-hexagon-decagon-identity/ "5:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Why did great sage पाणिनि compose the अष्टाध्यायी? There were probably multiple reasons but often you hear people say that he wanted to give a complete description of the Sanskrit language. That was probably one of his reasons but was it the central reason or the most important driver of his endeavor. From his own language and the evolution of the language to his times we can see that the Vedic register of Sanskrit was well known but already largely a thing of history in terms of being a spoke language. Yet, पाणिनि devotes an enormous amount of effort in describing it accurately and it still forms the foundation of our effort to understand the earliest words of our आर्य ancestors. Thus, understanding the Vedic language so that we can properly understand the religion of our आर्य ancestors and perform rituals as enjoined in the श्रुति was the major reason for पाणिनि-s effort. This indeed is acknowledged by his great commentator पतञ्जलि. One who is well-acquainted with the श्रुति also realizes that the आर्य-s conceived this linguistic framework for understanding the श्रुति and performing the rituals enjoined by it as having a deeper significance --- it offered an means of apprehending the nature of the deva-s and understanding the universe. Hence, the great अङ्गिरस् Dirghatamas औच्चाथ्य states in the श्रुति:

[ऋचो अक्षरे परमे व्योमन्]{style="color:#000080;"}\
[यस्मिन् देवा अधि विश्वे निषेदुः ।]{style="color:#000080;"}\
[yas tan na veda kim ऋचा करिष्यति]{style="color:#000080;"}\
[ya it tad vidus ta ime sam आसते ||]{style="color:#000080;"} RV1.164.39

In the syllable of the ऋच् set in the highest world,\
therein all the gods have taken residence,\
he who doesn't know that what can he do with the ऋच्?\
verily only they who know can sit together \[in this ritual session].

It was with such considerations in mind that the sages पाणिनि and पतञ्जलि composed their works. Thus, their works, which aimed at developing a certain comprehensiveness of the framework from as small as set of foundational axioms and with as much internal consistency as possible, never lost focus on its relationship with the "root", i.e. the श्रुति.

Just as पाणिनि is for the आर्य-s, Euclid is to their yavana cousins. His geometric system was in many ways parallel to the व्याकरण of पाणिनि. Like the work of पाणिनि it served as the framework of knowledge at large among the yavana-s, but what was the central reason for its composition? In the modern Occident which considers Euclid as one of the elements of their intellectual undergirding this is either completely ignored or wrongly stated as being the need to compose a comprehensive secular geometric text. However, we hear from Euclid's great commentator, a sage in his own right, Proclus, that the reason was to ultimately develop a geometric framework to understand the "roots" (rhizomata) of existence. These were first expounded by Empedocles in his verse:

And first the fourfold root of all things hear!--\
White gleaming Zeus, life-bringing Hera, Aidoneus,\
And Nestis whose tears bedew mortality.

Empedocles identified the "roots" with the four deities Zeus (the fiery element= Skt: agni), Hera (the gaseous element= Skt:वायु), Aidoneus (the solid element= Skt: पृथिवी) and Nestis (the liquid element= Skt:ap). One may compare the initial part of the Empedoclean system with the proto-सांख्य system described in the उपनिषत् of the तैत्तिरीय-s.

Plato, who came after Empedocles, incorporated these "roots" into his theory of knowledge as "stoicheia", which literally mean letters of the alphabet. This hints a parallel to the आर्य usage that might lurk behind Plato's stoicheia because indeed the अक्षर-s, the Sanskrit equivalent of stoicheia, are at the foundation of the Hindu system as encapsulated by पानिनि in his माहेश्वर-सूत्राणि. However, the sense in which Plato constructed his stoicheia was not by using linguistic analogs but by using geometric ones. He chose for this purpose the only regular solids that can be inscribed in a sphere --- i.e [the 5 Platonic solids](https://manasataramgini.wordpress.com/2019/04/15/from-plato-to-euler-and-back/). Plato is said to have learned of these from his predecessors like Pythagoras and his mathematical interlocutor Theaetetus. He incorporated them into an elaborate geometric "atomism" wherein the the fiery element was the tetrahedron, the solid element was the cube, the gaseous element was the octahedron, the stuff of the universe as a whole was the dodecahedron and the liquid element was the icosahedron. However, as his polyhedral system had 5 solids his system ultimately had to break away from the 4 element system of the old yavana-s and converge towards the 5 element system of the आर्य-s.

Plato's polyhedra may be seen as "molecules" because beneath them there lay his true atomic entities namely the  $30-60-90$  and the  $45-45-90$  right triangles (the 3 angles of this triangle are given in degrees). Two of these combined to give the equilateral triangle and the square which in turn constituted the four elemental polyhedra (sort of like the द्वयाणु theory of अक्षपाद Gautama). So in a process like the boiling of water into the gaseous state under the influence of the fire tetrahedron Plato saw the water icosahedron break down into equilateral triangles and reconstitute gaseous octahedra. In his system the solid cube had to keep out of these of inter-conversions. Aristotle was uncomfortable with this but the later Platonists including Proclus saw this a genuine feature of the solid element. Proclus explained it by stating that the solid element might be subdivided in interaction with the other two but does not ultimately transform.

In any case, for Proclus the foundation of understanding was based in mathematics. Indeed, he says that it "purifies and elevates the soul even as the goddess Athena dispersed mist obscuring the intellectual light of understanding." Hence, it was critical to have a complete theory from few starting axioms to construct the 5 Platonic solids that form the basis of existence. This, he says, is the ultimate objective of Euclid --- a complete theory from axioms to construct the Platonic solids. In support of this understanding one may note that the key constructions of the Platonic solids come in the last book of the *Elements* of Euclid. As per Proclus, Euclid was a Platonist who had studied with associates and students of Plato such as Philippus and Theaetetus before composing his treatise. Now the tetrahedron and cube are easy to construct from equilateral triangles and squares. The octahedron is slightly more involved by is also quite easily achieved as a equilateral bipyramid on a square base. However, the dodecahedron and the icosahedron are much more of a challenge. It is this context we encounter the "culmination of Euclid" in the form of the famous pentagon-hexagon-decagon identity of Euclidean geometry. Below we shall journey through this identity, and note its proof and how it specifies the icosahedron. Once, we have an icosahedron we can construct the dodecahedron as its dual.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/05/penta_hexa_deca.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Consider a regular pentagon, hexagon and decagon inscribed within the same circle (Figure 1). Let their sides be  $P, H, D$ . Then, what is the relationship between their sides? The answer to this is provide by Euclid in his *Elements*, Book 13, Proposition 10:

**The pentagon-hexagon-decagon identity(  $P.H.D$ )**: *If an equilateral pentagon is inscribed in a circle, then the square on the side of the pentagon equals the sum of the squares on the sides of the hexagon and the decagon inscribed in the same circle.*
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/05/penta_hexa_deca_bkh.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Alternatively, if the sides of a regular pentagon, hexagon and decagon respectively form the hypotenuse, greater and smaller legs of a right triangle then their circumcircles are congruent (Figure 2).

Now, the the proof for this as given by Euclid is not entirely obvious and, at least to us, it is unclear if he even gave a complete proof for it. Nevertheless, we can prove it using all the material he has described up to that point and the commentary of Proclus. For the proof one needs to first define that special ratio known as the Golden ratio  $\phi$ :

 $$\phi^2=\phi+1$$ 

Since this is a quadratic equation it has two roots. We call the larger one of these  $\phi \approx 1.618...$  and by definition the smaller one is  $\tfrac{1}{\phi}=\phi-1$ . Proclus tells us that it was revealed to the Pythagoreans by the Muse goddess who came to them. He then says that Plato recognized its importance and made conjectures on them which his mathematician student Eudoxus then proved using geometry. It is evidently this material of Eudoxus that is presented by Euclid --- for example, he provides a means to construct  $\phi$  without calling it that in Book 2 of his *Elements*. Now this ratio has a special relationship to the isosceles triangle with angles  $\tfrac{2\pi}{5}, \tfrac{2\pi}{5}, \tfrac{\pi}{5}$ : in such a triangle the ratio of either of the congruent sides to its base is  $\phi$ . This Golden ratio triangle is central to the proof of the  $P.H.D$  identity.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/05/penta_hexa_deca.proof_.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Figure 3 depicts the construction required for our proof. We observe that the said pentagon of our identity is in green, the hexagon in blue dotted segments and the decagon in red. The radius of their circumcircle is the same as the side of the hexagon  $H$ . From the construction we see two instances of the above-mentioned Golden ratio triangles: One with sides  $H, H, D$  and another with sides  $D, D, 2x$ . From those triangles we can write the below:

 $$\dfrac{P^2}{4}=D^2-x^2\lbrack 10 pt\rbrack \dfrac{P^2}{4}=H^2-(H-x)^2=२ःक्ष्-x^2\lbrack 10 pt\rbrack \dfrac{P^2}{2}=D^2+२ःक्ष्-2x^2\lbrack 10 pt\rbrack \dfrac{H}{D}=\dfrac{D}{2x}=\phi \lbrack 10 pt\rbrack x=\dfrac{D^2}{2H}\lbrack 10 pt\rbrack P^2=२ड्^२+४ःक्ष्-4x^2 = ४ड्^२-\dfrac{D^4}{H^2}\lbrack 10 pt\rbrack P^2=D^2(4-\dfrac{1}{\phi^2})=D^2\left(4-(\phi-1)^2\right)\lbrack 10 pt\rbrack P^2=D^2\left(4-\phi^2+2\phi-1 \right)=D^2\left(3-1-\phi+2\phi \right)\lbrack 10 pt\rbrack P^2=D^2\left(2+\phi \right)=D^2\left(1+1+\phi \right)=D^2\left(1+\phi^2 \right)\lbrack 10 pt\rbrack P^2=D^2+\फि^२ड्^२=D^2+H^2 \quad _{\blacksquare}$$ 

If one were to assume the theorem of Brahmagupta or Ptolemaios regarding the diagonals and sides of a cyclic quadrilaterals one can prove it by an alternative path. We leave this for the geometrically inclined reader to work out.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/05/icosahedron_p_h_d.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


The remarkable discovery of the yavana-s was the relationship of this  $P.H.D$  identity to the icosahedron. Consider the icosahedron in Figure 4. By definition all edges of the icosahedron are equal and all faces are equilateral triangles. Now, the edges of the icosahedron are congruent to the sides of the pentagon  $P$  in the  $P.H.D$  identity. Thus, if one constructs the circle as in Figure 4 we get the circumcircle as in Figure 3. With this circle we can construct the sides of the decagon  $D$  as above, e.g.  $\overline{PQ}=D$  in Figure 4. Then, the relationship it has to the icosahedron is that  $\overline{AB}=\overline{PQ}=D$ . The 5 faces of an icosahedron each of which share a common edge constitute a pentagonal pyramid. The height of this pyramid is  $D$  (Figure 4). Now, by construction the radius of the circumcircle of the pentagonal bases of such pyramids in the icosahedron is of length  $H$ , i.e. the side of the hexagon in the  $P.H.D$  identity (Figure 4;  $\overline{BC}=H$ ). Interestingly, if we visualize the icosahedron as being made up of two pentagonal pyramids stuck to a central band of 10 facets then the distance between the pentagonal bases of the two pyramids is  $H$  (Figure 4;  $\overline{QR}=H$ ). Thus, the planar  $P.H.D$  identity is able to describe the icosahedron in 3D space and we can use it to construct this non-obvious Platonic solid. In order to prove this relationship between the  $P.H.D$  identity and the above landmarks of the icosahedron we have to effectively prove that  $\triangle{ABC} \cong \triangle{PQR}$  (Figure 4). They are right triangles and their hypotenuse and legs are respectively  $P, H, D$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/05/penta_hexa_deca.grd_icosa-1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


In order to prove that we have first construct rectangles inside the icosahedron like the one shown in Figure 5. From the above Golden triangle (Figure 3) and its relationship to the pentagon (used in the compass and straight-edge construction of the pentagon; [see this note](https://manasataramgini.wordpress.com/2016/08/23/some-meanderings-among-golden-stuff/)) we can see that such an internal rectangle of the icosahedron will be a Golden rectangle, i.e. ratio of its non-equal sides would be  $\phi$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/05/penta_hexa_deca_icosahedro.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


We then use this rectangle to make an orthographic projection of the icosahedral construction in Figure 4 on to a plane (Figure 6). In the right panel of Figure 6 we see how this Golden rectangle is placed with respect to key landmarks of the icosahedron. In the left panel we see the actual projection of icosahedral construction in Figure 4. This preserves the mutual relationships of  $\triangle{ABC}, \triangle{PQR}$  between 3D construction and the planar projection. We construct  $\overline{XY}$ . By examining this figure we observe  $\overline{XY} || \overline{AC}$ . Thus,  $\angle{ACB} \cong \angle{XYP}=\theta$ . From this, one can show that  $\angle{PRQ}=\theta$ . By the nature of the projection we preserve the equivalence of the edges of the icosahedron such that  $\overline{AC} \cong \overline{PR}$ . Thus, by Side-Angle-Angle test we can show  $\triangle{ABC} \cong \triangle{PQR} \quad _\blacksquare$ 

Thus, we complete our journey through one of the profound aspects of Euclidean space --- a thread passing through its defining feature the भुजा-कोटि-कर्ण-न्याय, the three regular polygons and the regular polyhedron, the icosahedron. When we were young, on account of our fascination for viral capsid structures, we closely studied the geometry of polyhedra by practical means, i.e. making them out of paper. It was in course of this we learnt of the  $P.H.D$  identity. We were able to practically confirm that for ourselves based on our models. However, it took us some time before we actually apprehended the proof.

