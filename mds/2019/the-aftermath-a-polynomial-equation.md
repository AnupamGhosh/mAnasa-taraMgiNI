
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The aftermath: A polynomial equation](https://manasataramgini.wordpress.com/2019/11/16/the-aftermath-a-polynomial-equation/){rel="bookmark"} {#the-aftermath-a-polynomial-equation .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 16, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/11/16/the-aftermath-a-polynomial-equation/ "6:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This is merely the tailpiece to the last tale of the strange hauntings. A reader may wonder why expend so many words on a high school problem. While the ball could have fitted into the socket, it rolled away.

  - -----------------------------------------------------------------------

As Jhilleeka was playing with her nephew upon Vidrum and Kalakausha's departure with the blue bottle she remarked to Somakhya: "Do you know I had a peculiar little encounter with Hemling as a delayed aftermath to his sliming by the कौणप?"\
S: "Lootika was once pretty angry and rough with him when we were in college --- he apparently came after you --- does it relate to that? I must say though that while Hemling had his quirks, I hardly think he was a guy of harmful intent. He had genuine fascination for cracking mathematical questions."\
Jhilli: "Indeed. But I think he also had a strange competitive streak though he likely meant no malice. At the face of it, it seemed he just wanted to test me with an interesting problem as he remained in disbelief for years that I solved the one he lobbed at our house in the talent show."\
Lootika: "May be he might have not intended anything inappropriate but at that time I thought he was going out of the way to court Jhilli's attention and that's what prompted my response."\
Somakhya: "What exactly happened and what was the interesting problem?"\
J: "It was some problem on a series sum or something --- I have it recorded in my collection of interesting results and will pull it out. It happened when you all were in the university."\
Lootika: "Yes, Hemling would sit in some common courses with us."

J: "Varoli was in college too and it was the year when I used ride home alone from school. One day while returning home from school I stopped my bike to let a herd of milch-buffaloes rampaging like a procession of यमदूत-s to clear the road. As I was waiting, Hemling came up to me suddenly out of nowhere and asked me whether I really solved his problem by myself at the talent competition years ago. The suddenness with which he came upon me startled me enough that I whipped out the pepper from my bag and might have sprayed him had my reaction been quicker. But I recognized that he was your classmate and former schoolmate Hemling. He seemed scared but seeing me pause, he stated without any further explanation that it would have been impossible for a girl that age to have known calculus. I told him off saying something like what I know or not know was none of his business. He continued unfazed saying that he wanted to test me with a problem and if I solved it he would be 'full of admiration' for me. I again told him off that I did not need his admiration but would take his problem nevertheless."\
Lootika: "No doubt he was mathematically gifted, but it was a bit too much to just accost a much younger girl that way."

Jhilleeka then dug out the problem from her collection: "Here it is! He asked me this; given the polynomial equation,

 $$1+2x+3x^2+...nx^{n-1}=n^2$$ 

Show that one of its roots will always be real and rational. Provide a closed expression for that root.

Somakhya took the problem from Jhilli and looked at it: "Indeed it amounts to evaluation of a sum of a series with both geometric and arithmetic features."\
J: "I told him I could not solve it right away but will take it home and work on it. He was bothered that I will ask Lootika for the answer and cheat. A touch irritated I told him something like I was too conscious of my own abilities to ask her. Anyhow, here is my solution that I arrived at after spending sometime on it that evening.

We can evaluate the sum in the  $L.H.S$  of the given equation thus:

 $$\displaystyle S=1+2x+3x^2...(n-1)x^{n-2}+nx^{n-1}\lbrack 10pt\rbrack क्ष्S= x+2x^2+3x^3...(n-1)x^{n-1}+nx^{n}\lbrack 10pt\rbrack क्ष्S-S=nx^{n}-1-x-x^2-x^3...-x^{n-1}\lbrack 10pt\rbrack S(x-1)=nx^{n}-\sum_{k=1}^{n}x^{k-1} = nx\^n- \dfrac{x\^n-1}{x-1}= \dfrac{nx^{n+1}-nx\^n-x\^n+1}{x-1}\lbrack 10pt\rbrack \therefore S= \dfrac{nx^{n+1}-nx\^n-x\^n+1}{(x-1)^2}= \dfrac{(nx-n-1)x\^n+1}{(x-1)^2}$$ 

We see that,\
if  $n=2; 1+2x=4$  we get a rational root  $x=1\tfrac{1}{2}$ \
if  $n=3; 1+2x+3x^2=9$  get a rational root  $x=1\tfrac{1}{3}$ \
if  $n=4; 1+2x+3x^2+4x^3=16$  get a rational root  $x=1\tfrac{1}{4}$ 

This tells us that the rational root might take the form  $1\tfrac{1}{n}$ . Substituting  $x=1\tfrac{1}{n}$  in the sum  $S$  we see that is indeed the case  $_{...\blacksquare}$ 

Thus,  $n^2$  is the only power of  $n$  for which that sum will yield a polynomial equation from such a sum with a rational root for every  $n$ . Thus, we get the sum:

 $$\displaystyle \sum_{k=1}^{n} k \left( 1+\dfrac{1}{n}\right)^{k-1}=n^2$$ 

Hemling asked अग्रजा the next day if I had asked her help for a math problem and whether she had taught me calculus. Lootika did not know what he was talking about but was primed that he was up to something. Evidently, he wanted to catch me that evening and see if I had solved it but I reached home before he could intercept me. It seems he trailed Lootika and Vrishchika and figured out where we lived. He evidently hung around for sometime and made the foolish mistake of asking them if I was around when the two went out to play. Bristling like cats which have fluffed up their fur for an attack my sisters yelled at Hemling threatened him of deep trouble if he ever asked about me again. Being only a गणितशूर and otherwise quite a skittish guy, Hemling fled never to come our way again.

Lootika: "While I think he really had no business tracking you that way, your sister Vrishchika felt we had been too aggressive and apologized to him."\
J: But now comes the strange part. Just after you'll left home Varoli and I one day pried out a folded piece of paper that was inserted between two stones in the compound wall of our house. On it was a solution to the same problem in green ink. I suspect it was left there by Hemling because it had solution using calculus --- I sometimes wonder if that's how he believed it could be solved. Here is a picture of it:

 $$\displaystyle S=1+2x+3x^2...(n-1)x^{n-2}+nx^{n-1} \lbrack 10pt\rbrack \int Sdx = \int \left(1+2x+3x^2+...(n-1)x^{x-2}+nx^{n-1}\right) dx\lbrack 10pt\rbrack = x+x^2+x^3+x^{n-1}+x\^n=\dfrac{x^{n+1}+x}{x-1}\lbrack 10pt\rbrack \therefore S= \dfrac{d}{dx} \left( \dfrac{x^{n+1}+x}{x-1} \right) = \dfrac{(nx-n-1)x\^n+1}{(x-1)^2}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/11/polyroots.png){width="75%"}
```{=latex}
\end{center}
```

\
Roots of the above polynomial equation

