
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नारायण's sequence, माधव's series and pi](https://manasataramgini.wordpress.com/2019/07/26/naraya%e1%b9%87as-sequence-madhavas-series-and-pi/){rel="bookmark"} {#नरयणs-sequence-मधवs-series-and-pi .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 26, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/07/26/naraya%e1%b9%87as-sequence-madhavas-series-and-pi/ "3:53 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**The coin-toss problem and नारायण's sequence**\
If you toss a fair coin  $n$  times how many of the possible result-sequences of tosses will not have a successive run of 3 or more Heads? The same can be phrased as given  $n$  tosses of a fair coin, what is the probability of not getting 3 or more successive Heads in the result-sequence. For a single toss  $(n=1)$  we have two result-sequences  $H,T$ ; so we have 2 result-sequences with no run of 3 or more continuous Heads. Let  $n$  be the number of tosses,  $f\lbrack n\rbrack$  the number of result-sequences satisfying the condition and  $p$  the probability of its occurrence. This can be tabulated for the first few coin tosses as below:

\begin{tabular}{|l|p{0.5\linewidth}|l|l|} \hline n & All result-sequences & f\lbrack n\rbrack & p\\ \hline 1 & H,T & 2 & 1\\ 2 & HH, HT, TT, TH & 4 & 1\\ 3 & HHH, HHT, HTH, HTT, TTT, TTH, THH, THT & 7 & 0.875\\ 4 & HHHH, HHHT, HHTH, HTHH, HHTT, HTHT, HTTH, HTTT, TTTT, TTTH, TTHT, THTT, TTHH, THHT, THTH, THHH & 13 & 0.8125\\ \hline \end{tabular}

Thus we get the sequence as  $f\lbrack n\rbrack =2,4,7,13...$  from which we can compute the probability as  $p=\tfrac{f\lbrack n\rbrack }{2\^n}$ . The question then arises at to whether there is a general formula for  $f\lbrack n\rbrack$ . The answer to this comes from a class of sequences described by the great Hindu scientist नारायण पण्डित in 1356 CE. In his गणित-कौमुदी he provides the following (first brought to the wider public attention in modern times by Paramand Singh in his famous article on such sequences):

[एकाङ्कौ विन्यस्य प्रथमं]{style="color:#000080;"}\
[तत् संयुतिं पुरो विलिखेत् ।]{style="color:#000080;"}\
[उत्क्रमतो 'न्तिम-तुल्य-]{style="color:#000080;"}\
[स्थानाङ्क-युतिम् पुरो विलिखेत् ॥]{style="color:#000080;"}\
First placing 1 twice  $f\lbrack 1\rbrack =1; f\lbrack 2\rbrack =1$ , write their sum ahead  $f\lbrack 3\rbrack =f\lbrack 1\rbrack +f\lbrack 2\rbrack =2$ . Write ahead of that, write the sum of numbers in reverse order \[and in] positions equal to the "terminal"  $(q)$ .

[उत्क्रमतो 'न्तिम-तुल्य-]{style="color:#000080;"}\
[स्थान-युतिं तत् पुरस्ताच् च ।]{style="color:#000080;"}\
[अन्तिम-तुल्य स्थानाभवे]{style="color:#000080;"}\
[तत् संयुतिं पुरस्ताच् च ॥]{style="color:#000080;"}\
[एवं सैक-समास-स्थाना-सामासिकीयं स्यात् ।]{style="color:#000080;"}\
Write ahead of that, write the sum of numbers in reverse order \[and in] positions equal to the "terminal" (continue process). (This means: If  $3 \le n \le q$  then  $f\lbrack n\rbrack =f\lbrack n-1\rbrack +f\lbrack n-2\rbrack ...f\lbrack 2\rbrack +f\lbrack 1\rbrack$ ) In the absence of \[numbers in] positions equal to the terminal write in the front the sum of those \[in available places] (This means if  $q< n$  then  $f\lbrack n\rbrack =f\lbrack n-1\rbrack +f\lbrack n-2\rbrack ...f\lbrack n-q\rbrack$  ). Thus, the numbers till the position one more than \[the prior] may be known as the additive sequence  $(f\lbrack 1\rbrack , f\lbrack 2\rbrack ...f\lbrack n-q\rbrack , f\lbrack n-q+1\rbrack ...f\lbrack n\rbrack )$ .

One will note that if one takes  $q=2$  then we get the famous मात्रा-meru sequence  $f\lbrack n\rbrack =f\lbrack n-1\rbrack +f\lbrack n-2\rbrack$  (known in the Occident as the Fibonacci sequence after Leonardo of Pisa). नारायण then goes on to provide a numerical example for this class of additive sequences:\
[उदाहरणम् --]{style="color:#000080;"}\
[समासे यत्र सप्त स्युर्]{style="color:#000080;"}\
[अन्तिमस् त्रिमितः सखे ।]{style="color:#000080;"}\
[कीदृशी तत्र कथय]{style="color:#000080;"}\
[पङ्क्तिः सामासिकी द्रुतम् ॥]{style="color:#000080;"}\
Now an example: Friend, if we have 7 in the sequence and 3 as the "terminal"  $(q=3)$  then quickly say what will be the additive sequence under consideration.

The sequence in consideration in modern form starting from 0 will be  $f\lbrack n\rbrack =f\lbrack n-1\rbrack +f\lbrack n+2\rbrack +f\lbrack n-3\rbrack$ , with the first three terms being 0,1,1 (in नारायण's reckoning they will be 1,1,2): **0, 1, 1, 2, 4, 7, 13, 24, 44, 81, 149, 274, 504, 927, 1705...** Thus, नारायण's numerical example from  $f\lbrack 3\rbrack$  onwards gives us the solution to the coin toss problem. In modern times this sequence has been given the name "Tribonacci". With this, one can see that the probability of the getting a result-sequence without 3 or more continuous heads decays with  $2\^n$  asymptotically towards 0.

**The convergent of नारायण's sequence and problem of the triple proportional partition of a segment**\
Consider the geometric problem: Partition a line segment  $\overline{AD}$  of length  $t_4$  in 3 parts  $t_1, t_2, t_3$  such that  $\tfrac{t_2}{t_1} = \tfrac{t_3}{t_2}=\tfrac{t_4}{t_3}=\tfrac{t_1+t_2+t_3}{t_3}$  (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/narayana_tribonacci_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


We can see that the above geometric process is equivalent to the formulation of the above tryantima-सामासिका-पङ्क्ति of नारायण (Figure 1). This indicates that, at its limit, the convergent of the above sequence of नारायण,  $\tau$ , will yield us the ratio in which the partition of the segment should be performed. Thus, we have:

 $$\tau= \displaystyle \lim_{n \to \infty}\dfrac{f\lbrack n\rbrack }{f\lbrack n-1\rbrack } \approx 1.8392867552141611325518525646532...$$ 

This number, unlike the Golden ratio  $\phi$ , the convergent of the मात्रा-meru-पङ्क्ति, cannot be constructed using a straight edge and a compass. However, as we shall see below, it can be easily constructed using two simply specified conics which can in turn be constructed using the geometric mean theorem or other standard methods (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/narayana_tribonacci_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```



 1.  Draw the parabola specified by  $y=x^2$  with its vertex  $C$  as the origin.

 2.  Draw a unit rectangular hyperbola with center  $C_h$  at  $(1,1)$ . Therefore, its equation will be  $xy-x-y=1$ .

 3.  The two conics will intersect at  $P$ . Drop a perpendicular from  $P$  to the  $x$ -axis to cut it at  $B$ .  $t_1=\overline{CB}=\tau$ .

 4.  Extend  $\overline{CB}$  by a unit to get point  $A$ .  $\overline{BA}=1$ .

 5.  Draw a circle with  $C$  as center and  $\overline{CB}$  as radius to cut the  $x$ -axis at  $X$ .

 6.  Mark point  $Q$  along  $\overline{CB}$  at a unit distance from  $C$ . Join  $Q$  to  $X$ .

 7.  Draw a perpendicular to  $\overline{QX}$  at  $X$ . It will cut the  $x$ -Axis at point  $D$ . Join  $C$  to  $D$ . By the geometric mean theorem  $\overline{CD}=\tau^2$ .

This gives us the required partition of  $\overline{AD}$  into 3 segments each proportional to the other as  $\tau$ , and  $\overline{AD}$  proportional to the largest partition  $\overline{CD}$  again as  $\tau$ . Thus, we have:

 $$\tau=\dfrac{\tau^2+\tau+1}{\tau^2}$$ 

Hence,  $\tau$  is the root of the cubic equation  $\tau^3-\tau^2-\tau-1=0$ . Thus, we see that the above construction is achieved by solving this cubic via the intersection of the said parabola and hyperbola. This cubic equation has only one real root which is  $\tau$ . We can take the help of computer algebra to obtain the exact form of this root as:

 $$\tau=\dfrac{1}{3}\left(1+\left(19-3\sqrt{33}\right)^{1/3}+\left(19+3\sqrt{33}\right)^{1/3}\right)$$ 

The other two roots are complex conjugates:

 $$\tau'=\dfrac{1}{3} - \dfrac{(19 - 3 \sqrt{33})^{1/3}- (19 + 3 \sqrt{33})^{1/3}}{6} + i\dfrac{ \sqrt{3}(19 - 3 \sqrt{33})^{1/3}-\sqrt{3}(19 + 3 \sqrt{33})^{1/3}}{6}\lbrack 7pt\rbrack \overline{\tau'}=\dfrac{1}{3} - \dfrac{(19 - 3 \sqrt{33})^{1/3}- (19 + 3 \sqrt{33})^{1/3}}{6} - i\dfrac{ \sqrt{3}(19 - 3 \sqrt{33})^{1/3}-\sqrt{3}(19 + 3 \sqrt{33})^{1/3}}{6}$$ 

Comparable to the situation with  $\phi$  and its conjugate, these roots have a relationship of the form:

 $$\tau=\dfrac{1}{\tau' \overline{\tau'}}$$ 

There are also some other curious identities satisfied by  $\tau$  like:

 $$\dfrac{(1+\tau)^2}{1+\tau^2}=\tau$$ 

**The convergent of नारायण's sequence and माधव's  $\arctan(x)$  and  $\pi$  series**\
The triple partitioning of a segment leads us to a geometric construction that yields the relationship between  $\pi$  and  $\tau$  (Figure 3):

 $$\pi=4\left(\arctan\left(\dfrac{1}{\tau}\right)+\arctan\left(\dfrac{1}{\tau^2}\right)\right)$$ 

Figure 3 provides a proof for this of a type the old Hindus termed the upapatti or what in today's mathematics is a proof without words (to our knowledge never presented before). Nevertheless, for the geometrically less-inclined we add a few words below to clarify this.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/narayana_tribonacci_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


In Figure 3, one can see how  $\angle{\alpha}=\arctan\left(\tfrac{1}{\tau^2}\right)$ . It emerges once from the starting triply partitioned segment as  $\angle{\alpha}= \arctan\left (\tfrac{\tau}{\tau^3} \right)$ . The construction creates segments  $t_1, t_2, t_3$  in the proportion of  $1:\tau:\tau^2$ . Thus, we get the second occurrence of  $\angle{\alpha}=\arctan\left (\tfrac{t_1}{t_3} \right)$ . That in turn implies the occurrence of a vertical segment of size  $\tau^2$ . From the construction we also get  $\angle{\beta}=\arctan\left (\tfrac{t_3}{t_1+t_2+t_3}\right)=\arctan\left (\tfrac{1}{\tau}\right)$ . Thus,  $\angle{\alpha}+\angle{\beta}$  add up to form the congruent base angles of an isosceles right triangle with congruent sides measuring  $\tau+\tau^2$ . This implies that:

 $$\arctan\left(\dfrac{1}{\tau^2}\right)+\arctan\left (\dfrac{1}{\tau}\right)= \arctan(1)=\dfrac{\pi}{4}\lbrack 7pt\rbrack \therefore \pi=4\left(\arctan\left(\dfrac{1}{\tau}\right)+\arctan\left(\dfrac{1}{\tau^2}\right)\right) \; \; \; _{...\blacksquare}$$ 

Likewise we can also see that:

 $$\pi=\dfrac{4}{3}\left(\arctan(\tau)+\arctan\left(\tau^2\right)\right)$$ 

Approximately contemporaneously with नारायण's work, apparently unbeknownst to him, माधव, the great mathematician and astronomer from Cerapada, presented his celebrated infinite series for the  $\arctan(x)$  function:

 $$\arctan(x)=\dfrac{x}{1}-\dfrac{x^3}{3}+\dfrac{x^5}{5}-\dfrac{x^7}{7}...$$ 

We can use the first of the above relationships between  $\pi$  and  $\tau$  to obtain an infinite series for the former based on the latter:

 $$\pi=4\left(\dfrac{1}{\tau}-\dfrac{1}{3\tau^3}+\dfrac{1}{5\tau^5}-\dfrac{1}{7\tau^7}+\dfrac{1}{9\tau^9}-\dfrac{1}{11\tau^{11}}+\dfrac{1}{13\tau^{13}}...+\dfrac{1}{\tau^2}-\dfrac{1}{3\tau^6}+\dfrac{1}{5\tau^{10}}-\dfrac{1}{7\tau^{14}}...\right)$$ 

Gathering terms in order of their exponents we get:

 $$\pi=4\left(\dfrac{1}{\tau}+\dfrac{1}{\tau^2}-\dfrac{1}{3\tau^3}+\dfrac{1}{5\tau^5}-\dfrac{1}{3\tau^6}-\dfrac{1}{7\tau^7}+\dfrac{1}{9\tau^9}+\dfrac{1}{5\tau^{10}}-\dfrac{1}{11\tau^{11}}+\dfrac{1}{13\tau^{13}}-\dfrac{1}{7\tau^{14}}...\right)$$ 

One notices that all except the fourth powers are represented. One can compactly express this as:

 $$\pi=\displaystyle 4\sum_{n=1}^{\infty} \dfrac{a\lbrack n\rbrack }{n\tau\^n}$$ 

Here the cyclic sequence  $a\lbrack n\rbrack$  is defined thus:

 $$a\lbrack n\rbrack =1, a\lbrack n+1\rbrack =2\cdot (-1)^{m-1}, a\lbrack n+2\rbrack =-1, a\lbrack n+3\rbrack =0; n=4(m-1)+1; m=1,2,3... a\lbrack n\rbrack =1, 2, -1, 0, 1, -2, -1, 0, 1, 2, -1, 0...$$ 

Using this series to calculate  $\pi$  results in reasonably fast convergence with a nearly linear increase in the correct digits after the decimal point with every 4 terms. Thus, with 200 terms we get  $\pi$  correct to 53 places after the decimal point (Figure 4). However, we should keep in mind that  $n$  actually includes a null term which removes every 4th powers; hence, the real number of terms is lower by  $\tfrac{n}{4}$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/narayana_tribo_pi.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


Using just the first 3 terms we get an approximation of  $\pi$  that works as well as  $\tfrac{22}{7}$  as:

 $$\pi \approx 4 \left(\dfrac{3\tau^2+3\tau-1}{3\tau^2+3\tau+3}\right)$$ 

We can further compare this to the famous single angle  $\arctan$  infinite series for  $\pi$  provided by माधव using भूत-संख्य (the Hindu numerical code):

[व्यासे वारिधि-निहते रूप-हृते व्यास-सागराभिहते ।]{style="color:#000080;"}\
[त्रि-शरादि-विषम-संख्या-भक्तं ऋणं स्वं पृथक् क्रमात् कुर्यात् ॥]{style="color:#000080;"}\
In the diameter multiplied by the oceans (4) and divided by the form (1), subtraction and addition \[of terms] should be repeatedly done of the diameter multiplied by the oceans (4) and divided respectively by 3, the arrows (5) and so on of odd numbers.

In modern notation that would be:

 $$\pi=\displaystyle 4\sum_{n=1}^{\infty} \dfrac{1}{2n-1}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/narayana_pi_madhava.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


This series converges very slowly and in an oscillatory fashion (Figure 5) reaching just 2 correct digits after the decimal point after computing 200 terms. The oscillatory convergence features an alternation of better and worse approximations, with the latter showing a curious feature. For example, with 25 terms we encounter 3.1[8]{.underline}15 which is "correct" up to 4 places after the point (3.1415) except for the wrong 8 in the second place. With 50 terms we get 3.1[2]{.underline}159465, which is correct to 8 places (3.14159465) after the point except for the wrong 2 at the second place. More such instances can be found as we go along the expansion. For example at 500 terms we get:\
3.141592653589793238\
3.1[39]{.underline}59265[5]{.underline}5897[8]{.underline}3238

This is correct to 18 places except for 4 wrong places. Late J. Borwein and colleagues have reported an occurrence of this phenomenon even in a calculation of  $5 \times 10^6$  terms of this series.

Thus, the double angle series based on  $\tau$  fares way better than the basic single angle माधव series for  $\pi$ . Of course, माधव was well aware that it converged very slowly. Hence, he and others in his school like the नम्पूतिरि-s, the great निलकण्ठ सोमयाजिन्, ज्येष्टदेव and चित्रभानु, devised some terminal correction terms to derive alternative series to speed up convergence and obtained approximations of  $\pi$  that had good accuracy for those times. Two of their series which were mediocre in convergence speed are in modern notation:

 $$\pi=\displaystyle 4\left(\dfrac{3}{4}+\sum_{n=1}^\infty \dfrac{-1^{n+1}}{(2n+1)^3-(2n+1)}\right)$$ 

This sequence produces  $\pi \approx 3.1415926$  after 200 terms.

 $$\pi=\displaystyle\sum_{n=1}^\infty \dfrac{-1^{n+1} \cdot 16}{(2n-1)^5+4(2n-1)}$$ 

This one works better than the above and produces  $\pi \approx 3.141592653589$  after 200 terms

The third uses  $\tan\left(\tfrac{\pi}{3}\right)$  as a multiplicand:

 $$\pi=\displaystyle\sum_{n=1}^\infty \dfrac{-1^{x-1}\cdot 2\sqrt{3}}{3^{n-1}(2n-1)}$$ 

This series fares much better and produces 97 correct digits after the decimal point with 200 terms. This is quite impressive because it outdoes the above double angle series based on  $\tau, \tau^2$ . It is quite likely that माधव and चित्रभानु used this series for around 20-25 terms to obtain approximations such as the below (expressed in भूत-संख्य):

[विबुध-नेत्र-गजाहि-हुताशन-त्रिगुण-वेद-भ-वारण-बाहवः ।]{style="color:#000080;"}\
[नव-निखर्व-मिते वृत्ति-विस्तरे परिधिमानम् इदं जगदुर् बुधाः ॥]{style="color:#000080;"}\
The gods (33), eyes (2), elephants (8), snakes (8), the fires thrice (333), the veda-s (4), the asterisms (27), the elephants (8), the hands (2) is the measure of the circumference of a circle with diameter of  $9 \times 10^{11}$ , so had stated the mathematicians:

 $$\pi \approx \dfrac{2827433388233}{900000000000} \approx 3.14159265359$$ 

In addition to approximations of  $\pi$  derived from studies on  $\arctan(x)$  series we also see that माधव's successors, if not himself, were also using a sequence of continued fraction convergents of  $\pi$ . These were probably inspired by the ability to initially calculate good approximations using series derived from the  $\arctan(x)$  series such as the above. Of these a large one is explicitly stated by शन्कर-वारियर् and नारायण नम्पूतिरि in their work the क्रियाक्रमकरी:

[वृत्त-व्यासे हते नाग-वेद-वह्न्य्-अब्धि-खेन्दुभिः ।]{style="color:#000080;"}\
[तिथ्य्-अश्वि-विबुधैर् भक्ते सुसूक्ष्मः परिधिर् भवेत् ॥]{style="color:#000080;"}\
Multiplying the diameter of a circle by snakes(8), veda-s(4), the fires (3), the oceans (4), the space (0), the moon (1) and dividing it by the tithi-s (15), अश्विन्-s (2), gods (33) one may get the circumference to good accuracy.

 $$\pi \approx \dfrac{104348}{33215} \approx 3.141592653$$ 

The  $\arctan(x)$  sequence remained a workhorse for calculating  $\pi$  long after the heroics of माधव's school. A particularly famous double angle formula was obtained by Euler with a [simple geometric proof](https://manasataramgini.wordpress.com/2017/04/20/eulers-squares/):

 $$\arctan\left(\dfrac{1}{2}\right)+\arctan\left(\dfrac{1}{3}\right)=\dfrac{\pi}{4}$$ 

Using this formula we get we get a rather good convergence and reach 122 correct places after the decimal point with 200 terms.

**Tailpiece: From  $\phi$  to  $\pi$  via  $\arcsin(x)$ **\
We may conclude by noting that the while  $\tau$  relates to  $\pi$  via the  $\arctan(x)$  function, the Golden ratio  $\phi$  relates to  $\pi$  via the  $\arcsin(x)$  function. This stems from the special relationship between  $\phi$  and the sines of the angles  $\tfrac{3\pi}{10}$  and  $\tfrac{\pi}{10}$ . In the Jyotpatti appendix of his Siddhānta-शिरोमणि's भास्कर-II specifically presents the values of the sines of these angles as common knowledge of the पूर्वाचार्य-s. We reproduce below his account of the angles, the closed forms of whose sines were know to them (Note that old Hindus used Rsine instead of modern  $\sin(x)$ ; hence the technical term "त्रिज्या" for  $R$ . Originally, आर्यभट had set  $R=\tfrac{60\times 180}{\pi} \approx 3438'$ , i.e. corresponding approximately to a radian measure in minutes. Below we take it to be 1 to correspond to our modern  $\sin(x)$ ):

[त्रिज्यार्धं राशिज्या तत् कोटिज्या च षष्टि-भागानाम् ।]{style="color:#000080;"}\
[त्रिज्या-वर्गार्ध-पदं शरवेदांश-ज्यका भवति ॥]{style="color:#000080;"}\
Half the  $R$  is the zodiacal sine (i.e.  $\tfrac{360\^o}{12}=30\^o$ ). Its cosine (i.e. of  $\cos(30\^o)$ ) will be the sine of  $60\^o$ . The square root of half the square of the  $R$  becomes the sine of arrows (5) and veda-s (4) degrees ( i.e.  $\sin(45\^o)=\tfrac{1}{\sqrt{2}}$ )

[त्रिज्या-कृति+इषुघातात् त्रिज्या कृति-वर्ग-पङ्चघातस्य ।]{style="color:#000080;"}\
[मूलोनाद् अष्ट-हृतान् मूलं षट्-त्रिंशद्-अंशज्या ॥]{style="color:#000080;"}\
From arrow (5) times the square of the  $R$  subtract the square root of 5 times the  $R$  to the power of 4. Divide what remains from above by 8; the square root of that gives  $\sin(36\^o)$  (i.e.  $\sin(36\^o)=\sqrt{\tfrac{5-\sqrt{5}}{8}}$ )

भास्कर then goes on to give an approximation for it as fraction:

[गज-हय-गजेषु निघ्नी त्रिभजीवा वा 'युतेन संभक्ता ।]{style="color:#000080;"}\
[षट्-त्रिंशद्-अंश-जीवा तत् कोटिज्या कृतेषुणाम् ॥]{style="color:#000080;"}\
The  $R$  multiplied by elephants (8), horses (7), elephants (8), arrows (5) and divided by  $10^4$  gives the sine of  $36\^o$ . Its cosine is the sine of 4 and arrows (5) (i.e.  $\sin(54\^o)$ ).

With this approximation we get  $\sin(54\^o) \approx 0.80901$  correct to 5 places after the decimal point (one would not it is  $\tfrac{\phi}{2}$ ) and implies that भास्कर was using an approximation of  $\pi$  correct to 4 places after the decimal point.

[त्रिज्या-कृति+इषु-घातान् मूलं त्रिज्योनितं चतुर्भक्तं ।]{style="color:#000080;"}\
[अष्टदश-भागानां जीवा स्पष्टा भवत्य् एवम् ॥]{style="color:#000080;"}\
From the square root of the product of the square of  $R$  and the arrows (5) subtract  $R$  and divide what is left by 4. This indeed becomes the exact sine of  $18\^o$  (i.e.  $\sin(18\^o)=\tfrac{\sqrt{5}-1}{4}$ ).

These basic sines emerge from the first 3 constructible regular polygons: the equilateral triangle yielding  $\sin(30\^o), \sin(60\^o)$ ; the square yielding  $\sin(45\^o)$  and finally the pentagon yields  $\sin(18\^o)$  and its multiples (Figure 6).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/narayana_tribonacci_fig6.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


Thus, from the geometry of the regular pentagon (the proof is obvious in Figure 6) it is seen that these values are rather easily obtained and can be expressed in terms of the Golden ratio  $\phi$ , which emerges in the diagonal to side ratio (Figure 6). Thus, we have:

 $$\sin\left(\dfrac{\pi}{10}\right)=\dfrac{1}{2\phi}$$ 

 $$\sin\left(\dfrac{3\pi}{10}\right)=\dfrac{\phi}{2}$$ 

Thus, we can use the first angle in the infinite series for  $\arcsin(x)$  to obtain the series for  $\pi$  in terms of  $\phi$  as:

 $$\pi= 10 \displaystyle \sum_{n=0}^\infty \dfrac{(2n)!}{(2n+1) \cdot 2^{4n+1}(n!)^2\phi^{2n+1}}$$ 

This series fares excellently in computing  $\pi$  --- with the same 200 terms as used in the above experiments we get the value correct 208 places after the decimal point.

Now, instead of  $\phi$  if we resort to the angle  $\tfrac{\pi}{6}$  from the geometry of the equilateral triangle, we get the below infinite series:

 $$\pi=6 \displaystyle \sum_{n=0}^\infty \dfrac{(2n)!}{(2n+1)\cdot 2^{4n+1}(n!)^2}$$ 

This is obviously worse than the above series with  $\phi$  and yields 124 correct places after the point with 200 terms. Thus, it is only marginally better than the Eulerian double angle  $\arctan$  series in its performance.

