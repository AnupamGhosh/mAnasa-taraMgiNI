
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [शिव-गण-s and अन्धकासुर-vadha in the वामन-पुराण](https://manasataramgini.wordpress.com/2019/04/06/siva-ga%e1%b9%87a-s-and-andhakasura-vadha-in-the-vamana-pura%e1%b9%87a/){rel="bookmark"} {#शव-गण-s-and-अनधकसर-vadha-in-the-वमन-परण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 6, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/04/06/siva-ga%e1%b9%87a-s-and-andhakasura-vadha-in-the-vamana-pura%e1%b9%87a/ "7:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

On Twitter, one of our acquaintances going by the name \@ग़्होरान्गिरस had a discussion on the significance of the number 66 in शैव tradition. That reminded us of an unfinished article where we had noted this number in the context of a शैव पौराणिक narrative. This lengthy discursive article looks at some additional features of the ancient tale of the [killing of Andhaka by Rudra](https://manasataramgini.wordpress.com/2012/01/21/a-saiddhantika-adaptation-of-the-vastupurusha-narrative/) beyond what we had discussed earlier in a note on the evolution of the "fertilizing sweat" motif. Accordingly, we took this piece out of the shelf and worked it up so that it could be minimally presentable for publication. It provided as a pdf file because of the length and the number of illustrations in it.

[शिव-गण-s and अन्धकासुर-vadha in the वामन-पुराण ](https://manasataramgini.wordpress.com/wp-content/uploads/2019/04/shiva_gana-s-1.pdf)

