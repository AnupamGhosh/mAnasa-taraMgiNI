
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Notes on the Bhadra-सूक्त, a hymn for felicity to the Vedic pantheon](https://manasataramgini.wordpress.com/2019/02/10/notes-on-the-bhadra-sukta-a-hymn-for-felicity-to-the-vedic-pantheon/){rel="bookmark"} {#notes-on-the-bhadra-सकत-a-hymn-for-felicity-to-the-vedic-pantheon .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 10, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/02/10/notes-on-the-bhadra-sukta-a-hymn-for-felicity-to-the-vedic-pantheon/ "7:34 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For better type-setting and Devanagari rendering use [PDF version](https://manasataramgini.wordpress.com/wp-content/uploads/2019/02/bhadrasuktam.pdf).\
**Introduction**\
Several sub-schools of the तैत्तिरीय school of the कृष्णयजुर्वेद possess their own collections of mantra-s distinct from their संहिता-s known as the mantra-पाठ-s. These include mantra-s that are often found in other traditions but not in their own संहिता or ब्राह्मण. Additionally, they also include some mantra-s which are unique to these mantra-पाठ-s. For example the famous Yajurvedic version of the श्रीसूक्त is found in the mantra-पाठ of बोधायन. However, most practitioners in South India do not correctly use the accents of this सूक्त and seem to be unaware that the KYV version of this सूक्त occurs in this text. The mantra-पठ-s of the आपस्तम्ब, बोधायन, वैखानस and हिरण्यकेशिन् sub-schools have come down to us. The mantra-s in them are typically deployed in गृह्य rituals directed by instructions from their गृह्यसूत्र-s. However, the वैखानस-mantra-पाठ is distinctive in having a late terminal part that is used in the iconic worship of विष्णु by the वैखानस-s. The हिरण्यकेशी-mantra-पाठ has a ऋक्-संहिता as part of it and is used by the हिरण्यकेशीन्-s of Maharashtra and Tamil Nadu in their rituals to this date. In someways this is reminiscent of the hautra-परिशिष्ठ of the आपस्तम्ब-s that is used by Yajurvedin-s to supplement the role normally performed by the ऋग्वेदिन् होतृ.

Other than these mantra-पाठ-s of the KYV, we also have comparable supplementary mantra-collections among the सामवेदिन्-s in their मन्त्रब्राह्मण and the famous khila of the RV. Further, beyond the तैत्तिरीय school, the कठ school, which was once widespread in the northern parts of the subcontinent like Kashmir and the Panjab, had it own mantra-पठ that went along with their गृह्यसूत्र, namely that of लौगाक्षि. While the original form of this mantra-पठ does not survive to my knowledge, a version of it with accretions of तान्त्रिक and पौरानिक material used in स्मार्त practice by the ब्राह्मण-s of Kashmir and some of their counterparts in Himachal Pradesh has come down to us. This text was published by the Kashmirian ब्राह्मण-s केशव भट्ट and काशीनाथ-शर्मन् in the first half of the 1900s. I had earlier examined a defective version of this text but thanks to the massive text-scanning effort of the एग़न्गोत्रि Trust of the texts at the Kashmir Research Institute, Srinagar we can now examine a better version of this text.

The melding of तान्त्रिक and vaidika tradition found in this Kashmirian text has a long history in Hindu tradition. Indeed, as we have pointed out before\[footnote 1], a small mantra-संहिता comparable to the mantra-पाठ-s is found preserved in the शाक्त-पुराण, the देवी-पुराण, which might preserve a distinct vaidika tradition. Similarly, the आङ्गिरसकल्प of the पैप्पलाद school of the Atharvaveda preserves a combined mantra-deployment of पैप्पलाद AV mantra-s along with तान्त्रिक-विद्या-s. With regard to the AV tradition one may also point to the त्रिपुरार्णव-tantra, an authoritative मूल-tantra of the श्रीकुल tradition. The  $20^{th}$  तरंग of this text preserves a combined तान्त्रिक-vaidika mantra-deployment for the Indramahotsva (the great festival of Indra) which associates itself the AV. This association is likely genuine for the AV is the one vaidika tradition that has clear injunctions for the Indramahotsva in its परिशिष्ठ-s. This section of the त्रिपुरार्णव-tantra specifies several vaidika mantra-s that are to be used in the worship of Indra and other deva-s, which are combined with the worship of the Bhairava of the श्रीकुल tradition under the तान्त्रिक scheme.

Our Kashmirian text associated with the कठ school, like other mantra-पठ-s, has some unique Vedic material. One such is the भद्रसूक्त which is the topic of this note. This सूक्त of 22 ऋक्-s is to our knowledge not found in any other संहिता. It is mostly comprised of regular jagati-s (12-12-12-12), with the last ऋक् being a त्रिष्टुभ् (11-11-11-11). There may be some hypermetrical verses like ऋक्-17 (12-12-12-13). The सूक्त has a form rather similar to the RV7.35 of वसिष्ठ मैत्रावरुणि. Like that one it is a वैश्वदेव-सूक्त, which invokes the entire pantheon for luck or felicity. In RV7.35 the word for luck or felicity is the indeclinable शम्. In our सूक्त the word bhadra is used instead. It is used as an adjective that declines as the deity being invoked with the dative pronoun नः ("for us") being used just like in RV7.35. Hence, in this सूक्त we translate bhadra as auspicious (or can be taken in the sense of the deity being luck-granting). This word bhadra is also found in multiple ऋक्-s of another famous वैश्वदेव-सूक्त, RV1.89 of Gotama राहूगण, in an equivalent sense ([आ नो भद्राः; देवानाम् भद्रा सुमतिर् ऋजूयतां; भद्रं कर्णेभिः शृणुयाम देवा भद्रम् पश्येमाक्षभिर् यजत्राः]{style="color:#0000ff;"}). More generally, the pattern of the repetitions of bhadra is seen on multiple occasions in the RV albeit not in वैश्वदेव-सूक्त-s (e.g. RV8.62 of प्रगाथ काण्व) and also in the AV संहिता-s. A ऋक् of Sobhari काण्व (RV8.19.19) also uses the word bhadra repeated in a sense similar to this सूक्त:\
[भद्रो नो अग्निर् आहुतो भद्रा रातिः सुभग भद्रो अध्वरः । भद्रा उत प्रशस्तयः ॥]{style="color:#0000ff;"}\
For us auspicious Agni when he is made an offering, the auspicious gift, the auspicious ritual, you the giver of good luck, \[for us] auspicious hymns of praise.\
Another comparable word is svasti ("well-being") used in a similar sense by the Atri-s in their वैश्वदेव-सूक्त, RV5.51.11-15 and also by Gotama राहूगण in RV1.89. Indeed, in the Kashmirian tradition the भद्रसूक्त is used on conjunction with RV1.89 and RV5.51.11-15. This style continues into the epic period where we observe कौसल्या confer a blessing on राम using a comparable incantation with svasti ( in R2.25).

The pantheon of the भद्रसूक्त is entirely Vedic with no पौराणिक features. This squarely places the सूक्त within the classic vaidika tradition and it was perhaps even originally attached to some now lost संहिता. However, in ऋक्-2 we encounter the god प्रजापति. He is not found in the comparable RV7.35 or other core RV वैश्वदेव-सूक्त-s. He appears to have entered the Vedic tradition relatively late from a para-Vedic tradition \[footnote 2]. His position in the सूक्त suggests that he has not superseded the old aindra system as it happened in the even later Vedic layers. In this regard his position is comparable to that found the camaka-प्रश्न of the Yajurveda tradition. This suggests that the सूक्त indeed belongs to a comparable relative temporal layer and was a relatively late composition with the Vedic tradition, perhaps consciously mirroring the RV7.35 and RV1.89. The final ऋक् has the refrain: "[तन्नो मित्रो वरुणो मा महन्ताम् अदितिः सिन्धुः पृथिवी उत द्यौः]{style="color:#0000ff;"}" (Mitra and वरुण, Aditi, the river, the Earth and also Heaven should grant this to us), which is characteristic of the Kutsa-s of the RV (e.g. RV1.94). Kutsa also has a certain predilection for composing low complexity सूक्त-s, which is also seen rather plainly in this one. Importantly, his two वैश्वदेव-सूक्त-s, RV1.105 and RV1.106, have characteristic low-complexity style with repetition. Notably, his सूक्त to the Sun (RV1.115) uses the word bhadra repeatedly as in this सूक्त. Together, these indicate that the composer of the भद्रसूक्त was a member of the Kautsa clan.

Some notable features of the भद्रसूक्त are:

1. Venas is implored to be ever-desirous (उशन्...सदा) of the worshiper. This furnishes a link between Venas and the later name of Venus in Sanskrit tradition, उशनस्. Thus, it further strengthens the identification of Venas with Venus and suggests an early IE origin for this planetary name.

2. मातरिश्वन् is explicitly identified with वायु in this सूक्त. In the RV मातरिश्वन् is often mentioned as bringing Agni to the भार्गव-s and humans at large (evidently from Vivasvat). In RV3.29.11 विश्वामित्र clarifies this identification by stating: "[मातरिश्वा यद् अमिमीत मातरि वातस्य सर्गो अभवत् सरीमणि ॥]{style="color:#0000ff;"}": \[He is called] मातरिश्वन् when he measures out \[the space] in his mother; he became the rush of the wind in flowing out. Thus, we translate मातरिश्वन् as "he who grows in his mother" meaning "he who grows in the world-womb.

3. In ऋक्-10 we seen an invocation of various physiological processes. This is unique for a वैश्वदेव-सूक्त and not seen in RV सूक्त-s of this type. In this regard it has a flavor more typical of the AV.

4. In ऋक्-s 12-14 we encounter a great diversity of देवता-dvandva-s, which is unprecedented in any other वैश्वदेव-सूक्त elsewhere in the श्रुति.

The emended text of the सूक्त is presented below with an approximate translation.

**भद्रसूक्त: text**\
[भद्रो नो अग्निः सुहवो विभावसुर् भद्र इन्द्रः पुरुहूतः पुरुष्टुतः ।]{style="color:#0000ff;"}\
[भद्रः सूर्य उरुचक्षा उरुव्यचा भद्रश् चन्द्रमाः समिथेषु जागृविः ॥१॥]{style="color:#0000ff;"}\
For us the auspicious Agni, well-invoked and abounding in light, the auspicious Indra much-invoked and much-hymned; the auspicious Sun, wide-seeing and wide-ranging, the auspicious Moon keeping an eye \[on us] in the battle.

[भद्रः प्रजा अजनयन्नः प्रजापतिर् भद्रः सोमः पवमनो वृषा हरिः ।]{style="color:#0000ff;"}\
[भद्रस् त्वष्टा विदधद् रूपाण्य् अद्भुतो भद्रो नो धाता वरिवस्यतु प्रजाः ॥२॥]{style="color:#0000ff;"}\
For us the auspicious प्रजापति \[who] progeny-generated, the auspicious Soma, the purified one \[Footnote 3] and the manly yellow one; the auspicious त्वस्टृ giving wondrous forms \[to things]. May the auspicious धातृ show favor to \[our] progeny.

[भद्रस् तार्क्ष्यः सुप्रजस्त्वाय नो महाँ अरिष्टनेमिः पृतना युधा जयन् ।]{style="color:#0000ff;"}\
[भद्रो वायुर् मातरिश्वा नियुत्पतिर् वेनो गयस्फान उशन् सदा 'स्तु नः ॥३॥]{style="color:#0000ff;"}\
For us the auspicious तार्क्ष्य अरिष्टनेमि for the sake of good progeny and for conquering the hostile army by means of battle; the auspicious वायु, expanding within the world-womb \[footnote 4], the lord of the team of horses. May Venus, the wealth-increaser, be always desirous of us.

[भद्रो मित्रो वरुणो रुद्र इद् वृधा भद्रो 'हिर्बुध्न्यो भुवनस्य रक्षिता ।]{style="color:#0000ff;"}\
[भद्रो नो वास्तोष्पतिर् अस्त्व् अमीवहा भद्रः क्षेत्रस्य पतिर् विचर्षणिः ॥४॥]{style="color:#0000ff;"}\
For us the auspicious Mitra and वरुण, and Rudra verily with augmentation, and Ahirbudhnya the protector of the universe; the auspicious guardian of the homestead: may he be the destroyer of illness and the auspicious guardian of the field, ever-full of activity.

[भद्रो विभुर् विश्वकर्मा बृहस्पतिर् भद्रो द्विषस्तपनो ब्रह्मणस्पतिः ।]{style="color:#0000ff;"}\
[भद्रः सुपर्णो अरुणो मरुत्-सखा भद्रो नो वातो अभिवातु भेषजी ॥५॥]{style="color:#0000ff;"}\
For us the mighty, all-maker बृहस्पति, the auspicious foe-scorcher and lord of the ritual; the auspicious falcon, reddish-brown and the friend of the Marut-s \[footnote 5]. May the auspicious वात \[footnote 6] blow medicines towards us.

[भद्रो दधिक्रा वृषभः कनिक्रदद् भद्रः पर्जन्यो बहुधा विराजति ।]{style="color:#0000ff;"}\
[भद्रा सरस्वाँ उत नः सरस्वती भद्रो वशी भद्र इन्द्रः पुरूरवः ॥६॥]{style="color:#0000ff;"}\
For us the auspicious \[horse] Dadhikra, the neighing stallion, the auspicious Parjanya \[who] manifoldly shines forth; the auspicious Sarasvat and also सरस्वती, the auspicious cow and the auspicious Indra, the loud-roarer.

[भद्रो नः पूषा सविता यमो भगो भद्रो 'ग्रज एकपाद् अर्यमा मनुः ।]{style="color:#0000ff;"}\
[भद्रो विष्णुर् उरुगायो वृषा हरिर् भद्रो विवस्वाँ अभिवातु नस्त्मना ॥७॥]{style="color:#0000ff;"}\
For us the auspicious पूषण्, सवितृ, Yama and Bhaga, and the auspicious first-born एकपात्, Aryaman and Manu; the auspicious विष्णु, the wide-strider and the manly lion. May indeed the auspicious Vivasvat blow towards us.

[भद्रा गायत्री ककुब् उष्णिहा विराड् भद्रानुष्टुप् बृहती पङ्क्तिर् अस्तु नः ।]{style="color:#0000ff;"}\
[भद्रा नस् त्रिष्टुब् जगती पुरुप्रिया भद्रातिच्छान्दा बहुधा विभूवरी ॥८॥]{style="color:#0000ff;"}\
For us the auspicious गायत्री, Kakubh, उष्णिहा \[Footnote 7] and विराट्. May अनुष्टुभ्, बृहती, पङ्क्ति each be auspicious to us. For us the auspicious त्रिष्टुभ्, the much-loved Jagati \[footnote 8] and the auspicious long meters manifold and of many treasures.

[भद्रा नो राकानुमतिः कुहूः सुहृद् भद्रा सिनीवाल्य् अदितिर् मही ध्रुवा ।]{style="color:#0000ff;"}\
[भद्रा नो द्यौर् अन्तरिक्षं मयस्करं भद्रो 'श्वो दक्षस्तनयाय नस् तुजे ॥९॥]{style="color:#0000ff;"}\
For us the the auspicious राका, Anumati and friendly कुहू, auspicious Sinivālī, Aditi, and the firm Earth goddess. For us the auspicious Heaven goddess, the atmosphere giving pleasure, the auspicious horse, and दक्ष for extending for us \[our] lineage.

[भद्रो नः प्राणः सुमनाः सुवागसद् भद्रो अपानः सतनुः सहात्मना ।]{style="color:#0000ff;"}\
[भद्रं चक्षुर् भद्रम् इच्छोत्रम् अस्तु नो भद्रं न आयुः शरदो असच्छताम् ॥१०॥]{style="color:#0000ff;"}\
For us the auspicious life-process with a good mind and good speech unmanifest, the auspicious excretory process with the body and the consciousness; indeed may the vision be auspicious and hearing be auspicious for us. For us the auspicious life with autumns, a 100 yet to manifest.

[भद्रेन्द्राग्नी नो भवताम् ऋतावृधा भद्रा नो मित्रावरुणा धृतव्रता ।]{style="color:#0000ff;"}\
[भद्राश्विना नो भवतां नवेदसा भद्रा द्यावा-पृथिवी विश्व-शंभुवा ॥११॥]{style="color:#0000ff;"}\
For us the auspicious Indra and Agni fostering the Law; for us the auspicious Mitra and वरुण maintaining the Laws. May the two auspicious अश्विन्-s be cognizant \[of us]. For us the auspicious Heaven and Earth benevolent to all.

[भद्रा न इन्द्रावरुणा रिशादसा भद्रा न इन्द्रा भवतां बृहस्पती ।]{style="color:#0000ff;"}\
[भद्रेन्द्राविष्णू सवनेषु यावृधा भद्रेन्द्रासोमा युधि दस्यु-हन्तमा ॥१२॥]{style="color:#0000ff;"}\
For us the auspicious Indra and वरुण, devourers of foes. May Indra and बृहस्पति be auspicious to us. \[For us] auspicious Indra and विष्णु who augment \[us] during the soma libations. May the auspicious Indra and Soma slay the dasyu in battle.

[भद्राग्नाविष्णू विदधस्य प्रसाधना भद्रा नो 'ग्नीन्द्रा वृषभा-दिवस्पती ।]{style="color:#0000ff;"}\
[भद्रा नो अग्नीवरुणा प्रचेतसा भद्राग्नीषोमा भवतां नवेदसा ॥१३॥]{style="color:#0000ff;"}\
For us the auspicious Agni and विष्णु, the ornaments of the gift-distribution. For us the auspicious Agni and Indra, the bulls, the lords of heaven. For us the auspicious Agni and वरुण, the ever-mindful ones. May the auspicious Agni and Soma be cognizant of us.

[भद्रा सूर्या-चन्द्रमसा कविक्रतू भद्रा सोमा भवतां पूषणा नः ।]{style="color:#0000ff;"}\
[भद्रेन्द्रावायू पृतनासु-सासही भद्रा सूर्याग्नी अजिता धनञ्जया ॥१४॥]{style="color:#0000ff;"}\
For us the auspicious Sun and Moon, the two full of insight. May Soma and पूषण् be auspicious. \[For us] the auspicious Indra and Vayu conquering in battle and the auspicious सूर्य and Agni unconquered and winning wealth.

[भद्रा नः सन्तु वसवो वसुप्रजा भद्रा रुद्रा वृत्रहणा पुरन्धरा ।]{style="color:#0000ff;"}\
[भद्रा आदित्याः सुपसः सुनीतयो भद्रा राजानो मरुतो विरप्शिनः ॥१५॥]{style="color:#0000ff;"}\
May the auspicious Vasu-s be wealth and progeny \[giving]. For us the auspicious Rudra-s who slay वृत्र and smash the \[hostile] forts and the auspicious आदित्य-s, well-seeing and well-guiding, and the auspicious kings, the Marut-s, the exuberant ones \[footnote 9].

[भद्रा न ऊमा सुहवाः शतश्रियो विश्वेदेवा मनवश् चर्षणीधृतः ।]{style="color:#0000ff;"}\
[भद्राः साध्या अभिभवः सूरचक्षसो भद्रा नः सन्त्व् ऋभवो रत्न-धातमाः ॥१६॥]{style="color:#0000ff;"}\
For us the auspicious helper-\[gods], well-invoked and with a 100 riches, all the gods and the Manu-s, supporters of the folks. For us the auspicious साध्य-s, the overpowerers, radiant as the Sun. May the auspicious ऋभु-s be gem-givers for us.

[भद्राः सर्वे वाजिनो वाजसातयो भद्रा ऋषयः पितरो गभस्तयः ।]{style="color:#0000ff;"}\
[भद्रा भृगवो 'ङ्गिरसः सुदानवो भद्रा गन्धर्वाप्सरसः सुदंससः ॥१७॥]{style="color:#0000ff;"}\
Auspicious the racers, winners of booty; auspicious the sages, \[our] ancestors, the sun-beams. Auspicious the भृगु-s and अङ्गिरस्-es, the liberal givers; auspicious the Gandharva-s and Apsaras-es, the powerful ones\[footnote 10].

[भद्रा आपः शुचयो विश्वभृत्तमा भद्राः शिवा यक्ष्मनुदो न ओषधीः ।]{style="color:#0000ff;"}\
[भद्रा गावः सुरभयो वयोवृधो भद्रा योषा उशतीर् देवपत्न्यः ॥१८॥]{style="color:#0000ff;"}\
For us the auspicious waters, pure and the foremost supporters of all, the auspicious, benign, disease-repulsing herbs; the auspicious cows, charming and invigorating, the auspicious nymphs and loving wives of the gods.

[भद्राणि सामानि सदा भवन्तु नो भद्रा अथर्वाण ऋचो यजूंषि नः ।]{style="color:#0000ff;"}\
[भद्रा नक्षत्राणि शिवानि विश्वा भद्रा आशा अह्रुताः सन्तु नो हृदि ॥१९॥]{style="color:#0000ff;"}\
May the Saman-s forever be auspicious to us. For us the अथर्वण् spells, the ऋक्-s and the यजुष्-es. May the auspicious asterisms \[be] all benign and \[may the] directions, the coordinate lines be auspicious at their conjunction.

[संवत्सरा न ऋतवो मयोभुवो यो वा आयुवाः सुसराण्य् उत क्षपाः ।]{style="color:#0000ff;"}\
[मुहूर्ताः काष्टाः प्रदिशो दिशश् च सदा भद्रा सन्तु द्विपदे शं चतुष्पदे ॥२०॥]{style="color:#0000ff;"}\
The years \[of the 5 year cycle] and the seasons be gladdening to us, be they productive, easy-going or drought-ridden. May the मुहूर्त (=48 minutes)-s and कष्ट (=3.2 seconds)-s, the directions and the inter-directions be ever-auspicious and \[may there be] welfare for the bipeds and quadrupeds.

[भद्रं पश्येम प्रचरेम भद्रं भद्रं वदेम शृणुयाम भद्रम् ।]{style="color:#0000ff;"}\
[तन्नो मित्रो वरुणो मा महन्ताम् अदितिः सिन्धुः पृथिवी उत द्यौः ॥२१॥]{style="color:#0000ff;"}\
May we see auspiciousness. May we perform auspiciousness. May we speak auspiciousness. May we hear auspiciousness. Mitra and वरुण, Aditi, the river, the Earth and also Heaven should grant this to us.

  - -----------------------------------------------------------------------

Footnotes\
1. <https://manasataramgini.wordpress.com/2010/06/25/the-mantra-samhita-of-the-devi-purana/>\
2. Note the presence of a comparable deity among the Greeks in the form of Phanes or Protogonos\
3. Emended पावमान to पवमान\
4. मातरिश्वन्: literally growing within the mother: the mother implies the world-womb or the world-hemisphere\
5. Later tradition clarifies him to be the charioteer of the Sun\
6. The wind deity\
7. Another form of उष्णिह् meter\
8. The composer seems to indicate his love for the Jagati, the meter in which he has composed most of the सूक्त\
9. Emended virapsin to विरप्शिन् keeping with the form found in the RV\
10. Emended सुदम्शस् to sudamsas

