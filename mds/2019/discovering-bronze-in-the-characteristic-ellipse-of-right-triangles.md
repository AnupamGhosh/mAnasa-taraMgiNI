
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Discovering bronze in the characteristic ellipse of right triangles](https://manasataramgini.wordpress.com/2019/07/18/discovering-bronze-in-the-characteristic-ellipse-of-right-triangles/){rel="bookmark"} {#discovering-bronze-in-the-characteristic-ellipse-of-right-triangles .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 18, 2019]{.entry-date}](https://manasataramgini.wordpress.com/2019/07/18/discovering-bronze-in-the-characteristic-ellipse-of-right-triangles/ "5:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**The arithmetic mean square of a right triangle**\
An entire family of right triangles that includes all the different forms of right triangles defined in terms of the proportion of their legs can be obtained by setting their altitude to a constant  $a$  and letting their base  $x$  vary. The right-angle vertex of these triangles, i.e. that which contains the right angle is common to both legs and constant across the family. By the above definition the altitude vertex, i.e. that defined by the terminus of the altitude, is also constant. The base vertex, i.e. that defined by the terminus of the base can be seen as continuously varying as it moves along the line perpendicular to the altitude.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/arithmetic_mean_sqr_righttriangle_fig1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Consider Figure 1:\
For a given right triangle (  $\triangle{ABC}$ ) the half-hypotenuse square is defined as the square erected on half of its hypotenuse (  $\square{BDEF}$ ). Then we have the following:

  -  The feet of the perpendiculars dropped from the vertex of the half-hypotenuse  $(E)$  square diagonally opposite to the base vertex to the base and the altitude of the given right triangle, together with its right-angle vertex defines a square  $(\square{CMEN})$ .

  -  The area of this square is the sum of the areas of the given right triangle and the half-hypotenuse square.

  -  The side of this square is the arithmetic mean of the two legs  $(\overline{CA}=a, \overline{CB}=x)$  of the given right triangle.

Hence, the above-defined square  $(\square{CMEN})$  is termed the arithmetic mean square of a give right triangle.

By examining Figure 1, registering all the self-evident congruences and performing the dissections indicated by the colored and marked areas it is possible to obtain "wordless" proofs of statements 1 and 2  $_{...\blacksquare}$ 

By भुजा-कोटि-कर्ण-न्याय the area of the half-hypotenuse square is  $\tfrac{a^2+x^2}{4}$ . Thus,

 $$A(\square{CMEN})=A(\square{BDEF}) + A(\triangle{ABC}) = \tfrac{a^2+x^2}{4} +\tfrac{ax}{2}=\left(\tfrac{a+x}{2}\right)^2$$ 

Thus, the side of  $\square{CMEN}$  is the arithmetic mean of the legs of the given right triangle  $A(\triangle{ABC})$   $_{...\blacksquare}$ . Thus we term this square the arithmetic mean square.

**The characteristic ellipse of right triangles**\
We also note that the shared vertex of the half-hypotenuse square and the arithmetic mean square of a right triangle (point  $E$ ) is the vertex of the semicircle erected on the hypotenuse of the right triangle (Figure 1). Thus, the segment  $\overline{EF}$  will be the diameter of the maximal circle that can be inscribed in the above semicircle (Figure 2). Now, if we draw a perpendicular to the base of the right triangle at the base vertex  $B$  then the perpendicular will cut the inscribed maximal circle at two points  $G, H$  (Figure 2). What will be the locus of  $G, H$  as the base of the triangle  $\overline{CB}=x$  grows in size? In what range of  $x$  in terms of the altitude  $a$  will this intersection will happen (i.e. for what range of  $x$  in  $a$  units will the locus exist)?
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/triangle_ellipse_problem_fig2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


To answer the above let us place point  $C$  at origin. Then, given the above-described arithmetic mean square of a right triangle we have the coordinates of its vertex and that of the semicircle erected on the hypotenuse to be  $E=(\tfrac{a+x}{2},\tfrac{a+x}{2})$ . This is also one end of a diameter of the maximal circle inscribed in the semicircle. The second end of this diameter is the midpoint of the hypotenuse  $F$ . Therefore, the coordinates of  $F=(\tfrac{x}{2}, \tfrac{a}{2})$ . Thus, we can get the midpoint of the the segment  $\overline{EF}$  to be  $I=(\tfrac{2x+a}{4},\tfrac{x+2a}{4})$ . The coordinates of point  $G$  (or of point  $F$ ), which lies on the desired locus will be  $G=(x,y)$ . From circle  $\circ I$  using the equivalence of its two radii we can write the following:

 $$\overline{FI}^2=\overline{GI}^2$$ 

But  $\overline{FI}^2=\tfrac{1}{4}\overline{AB}^2=\tfrac{x^2+a^2}{4}$ 

 $$\therefore (\tfrac{a+2x}{4}-x)^2+(\tfrac{x+2a}{4}-y)^2=\tfrac{x^2+a^2}{4}\lbrack 7pt\rbrack (\tfrac{a+2x-4x}{4})^2+(\tfrac{x+2a-4y}{4})^2=\tfrac{x^2+a^2}{4}\lbrack 7pt\rbrack (a-2x)^2+(x+2a-4y)^2=x^2+a^2\lbrack 7pt\rbrack x^2-2xy+4y^2-4ay+a^2=0$$ 

The above equation of the locus is a quadratic equation in two variables. Hence, it is a conic. Now given a general conic  $Ax^2+Bxy+Cy^2+Dx+Ey+F=0$ , its shape is determined by the conic discriminant:

 $$\delta_c=-4\Delta=-4 \begin{vmatrix} A & B \big /2\\ B \big /2& C \end{vmatrix} =B^2-4AC$$ 

If  $\delta_c< 0$  then we have an ellipse. In the special case of  $A=C, B=0$  it reduces to a circle. If  $\delta_c=0$  we have a parabola. If  $\delta_c> 0$  we have a hyperbola. In our case  $\delta_c=4-4 \times 1 \times 4=-12$ . Thus, the desired locus is an ellipse (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/triangle_ellipse_problem_fig3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


We can now rewrite the equation of the ellipse explicitly as:

 $$\dfrac{x+2a \pm \sqrt{4ax-3x^2}}{4}$$ 

The discriminant of this equation is:

 $$\Delta=\sqrt{4ax-3x^2}$ ; it will be real only when:  $0 \le x \le \tfrac{4a}{3}$$ 

From this we can see that the ellipse will exist only in the interval:

 $$\left(0,\tfrac{a}{2}\right)..\left(\tfrac{4a}{3}, \tfrac{5a}{6}\right)$$ 

Thus, at those two points  $G, H$  become coincident. Thus, the altitude of the right triangles is a tangent to this ellipse at  $\left(0,\tfrac{a}{2}\right)$  (Figure 3). Further, since we have the coordinates of point  $I$ , the center of the maximal circle inscribed in the hypotenuse semicircle, we can show that as  $x$  increases  $I$  moves along the line  $y=\tfrac{1}{2}x+\tfrac{3a}{8}$ . Thus, outside the above interval we see that the right triangle's base moves beyond the maximal inscribed circle and the perpendicular to the base at  $B$  will never touch this circle.

A shape of a triangle is characterized by the relative proportion of its 3 sides. For a right triangle we have further constraint; hence its shape is defined by the relative proportion of its legs. Thus, a right triangle with legs in proportion  $a_1:b_1$  is the same as that with  $b_1:a_1$  --- just that they are rotated with respect to each other. Using this concept we, can see that various landmarks of the characteristic ellipse correspond to notable underlying right triangles (Figure 4). The extreme left bound of the ellipse corresponds to the degenerate case where one leg is 0 and the hypotenuse and the other leg coincide. As we determined above, the right bound of the ellipse corresponds to  $x=\tfrac{4a}{3}$ . Thus, this corresponds to the famous  $3,4,5$  triangle. It is remarkable how  $3,4,5$  triangle, which represents the most basic भुजा-कोटि-कर्ण triplet, comes up as a natural feature associated with the characteristic ellipse.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/special_triangles_fig4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


Now that we have its equation we can also see that the ellipse will have a maximum at the point  $(a,a)$  and a minimum at the point  $\left(\tfrac{a}{3}, \tfrac{a}{3}\right)$ . The maximum corresponds to isosceles right triangle (Figure 4). The minimum of the ellipse corresponds the  $1,3,\sqrt{10}$  triangle (Figure 4). At both these positions the separation between the ellipse-generating points  $G, H$  is  $\tfrac{a}{2}$ .

When  $x=\tfrac{2a}{3}$ , the ellipse-generating line passes through the center of the ellipse, point  $C_e=\left(\tfrac{2a}{3}, \tfrac{2a}{3}\right)$  (Figure 4). This results in the maximal separation attained between the ellipse-generating points  $G, H$  of  $\tfrac{a}{\sqrt{3}}$ . This corresponds to the  $2,3,\sqrt{13}$  triangle. In this triangle the ratio of the sum of the long and middle sides to the short side is the Bronze ratio, which we shall encounter again discuss in greater detail below.

When  $x=\tfrac{a}{2}$  we have the underlying  $1,2,\sqrt{5}$  triangle (Figure 4). As is obvious, the ratio of the sum of the short and the long side of this triangle to the middle side is the Golden ratio  $(\phi)$ . Thus, not surprisingly, the separation between the ellipse-generating points at this value of  $x$  is  $\tfrac{2\phi-1}{4}$ . Another notable feature of this case is that only in this configuration the lines drawn parallel to the base of the triangle at the ellipse-generating points  $G,H$  are tangents to the ellipse-generating circle.

Between  $\tfrac{3a}{4} \le x \le \tfrac{4a}{3}$  (colored section of ellipse in Figure 4), we get similar triangles on either side of  $x=a$ . However, to the left of that region every triangle has a unique shape.

**The bronze ratio and the characteristic ellipse**\
Given the general conic equation  $Ax^2+Bxy+Cy^2+Dx+Ey+F=0$  one can calculate the eccentricity of the corresponding ellipse as:

 $$e_e=\sqrt{\dfrac{2\sqrt{(A-C)^2+B^2}}{A+C+\sqrt{(A-C)^2+B^2}}}$$ 

By substituting the requisite values, we get

 $$e_e=\sqrt{\dfrac{5\sqrt{13}-13}{6}} \approx 0.9154012$$ 

Now, we consider the Bronze ratio  $\beta=\tfrac{3+\sqrt{13}}{2} \approx 3.302776$ . We observe that  $\beta'=\tfrac{1}{\beta}=\beta-3 \approx 0.302776$ . Thus,  $\beta, -\beta'$  are conjugate roots of the quadratic equation  $x^2-3x-1=0$ . One immediately notes the parallel to the Golden ratio  $\phi$ :  $\phi' =\tfrac{1}{\phi}=\phi-1$  and likewise  $\phi, -\phi'$  are the roots of the equation  $x^2-x-1=0$  (see appendix for more). Thus, we can write the eccentricity of the characteristic ellipse as:

 $$\sqrt{\dfrac{5\beta'+1}{3}}$$ 

With the eccentricity we can determine the axes of the ellipse; we see that:\
 $a_{smin}=\dfrac{a\sqrt{1+\beta}}{3}$  ...semi-major axis

 $a_{smaj}=\dfrac{a}{\sqrt{3(1+\beta)}}$  ...semi-minor axis

As an aside the area of the characteristic ellipse is therefore  $A=\tfrac{\pi a^2}{3\sqrt{3}}$ . We also get the aspect ratio of the ellipse to be:

 $$AR=\dfrac{a_{smaj}}{a_{smin}}=\dfrac{1+\beta}{\sqrt{3}}$$ 

Interestingly, the connection of the characteristic ellipse to the bronze ratio does not end with the parameters of the ellipse itself. Its inclination, i.e. the slope of the line joining the two foci of the ellipse (i.e. the tangent of the angle made by this focal line with the base of the right triangles) is  $\beta'$ . Thus, the equation of the local line of the characteristic ellipse is:

 $$y=\beta'x+\dfrac{2a}{1+\beta}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/triangle_ellipse_problem_fig5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


Interestingly, when  $x=\beta'$ , it is only time the diameter of ellipse-generating circle perpendicular to the hypotenuse of the right triangle (i.e.  $\overline{EF}$ ) becomes parallel to the focal line of the ellipse and acquires the equation:\
 $y=\beta'(x+\dfrac{3a}{2})$  (Figure 5).

Thus, by doing some algebra we can see that the difference in the intercepts is  $\tfrac{a}{26\beta+8}$ 

This is one of few rare occurrences of the Bronze ratio as persistent feature in geometry and to our knowledge is described here for the first time.

**Appendix: Bronze ratio**\
A simple geometric interpretation of the Golden ratio  $\phi$  goes thus: If we construct a rectangle such that upon removing a square whose side is equal to the smaller side of the said rectangle we are left with a remainder rectangle whose aspect ratio is the same as the original rectangle then the aspect ratio of these rectangles is  $\phi$  (Figure 6). Likewise, if we get a similarly-shaped remainder rectangle upon removing two equal squares whose sides are equal to the shorter side of the starting rectangle then their aspect ratio is the Silver ratio  $\sigma=1+\sqrt{2}$  (Figure 6). These two ratios are also associated with diagonals of a pentagon and an octagon respectively. If upon removing 3 squares we have similarly proportioned remainder rectangle as the original one then their aspect ratio is the Bronze ratio  $\beta$ . This is not associated with any polygon.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/gold_silv_bron_fig6.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


All these "metallic" ratios, their reciprocals, and negative conjugates have a common feature in that they can be described by the general double hyperbolic equation with asymptotes as  $x$ -axis and the lines  $y=x; y=-x$  (Figure 7),

 $$y^4-x^2y^2-2y^2+1=0$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2019/07/metallic_ratio_curve_fig7.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad
```{=latex}
\end{center}
```


They emerge as the solutions  $m_n$  of the biquadratic equation in  $y$  for the integer values of  $x$ :

 $$m_n=\dfrac{\pm n \pm \sqrt{n^2+4}}{2}$$ 

Thus,  $n=1 \rightarrow \phi; n=2 \rightarrow \sigma; n=3 \rightarrow \beta ...$ . This also shows that they all follow the conjugate principle with respect to their conjugates, i.e.  $m_n-n=\tfrac{1}{m_n}; m_n^2=1+nm_n$ . As the double hyperbola converges to its asymptotes the fractional parts of the ratios converge to 0. Thus, it only the first few metallic ratios are likely to be "interesting". Indeed the Golden ratio is a common encountered in all manner of mathematical situations. The Silver ratio is seen to a lesser extant and the Bronze ratio is even rarer in it occurrence (conversely to the metals after which they are named). Beyond the Bronze ratio these ratios are mostly "uninteresting" in that they are rarely found in mathematical entities.

The above geometric operation of rectangle dissection is equivalent to the abstract string substitution operation under the rules  $1\rightarrow 1\^k0, 0 \rightarrow 1$ . Here the  $1\^k$  means  $k$  repetitions of 1. For  $k=1$  a string initiated with 1 grows thus:

 $$1\rightarrow 10\rightarrow 101\rightarrow 10110\rightarrow 10110101\rightarrow ...$$ 

One observes that ratio of 1s:0s in these strings converges to  $\phi$ . Likewise, the ratio of the length of the string  $n$  to that of string  $n-1$  also converges to  $\phi$ .

For  $k=2$  the string grows thus:

 $$1\rightarrow 110\rightarrow 1101101\rightarrow 11011011101101110\rightarrow 11011011101101110110110111011011101101101\rightarrow ...$$ 

Here the ratio of 1s:0s and the ratio of the length of the string  $n$  length of string  $n-1$  converge to the Silver ratio  $\sigma$ .

For  $k=3$  the string grows thus:

 $$1\rightarrow 1110\rightarrow 1110111011101\rightarrow\\ 1110111011101111011101110111101110111011110\rightarrow\\ 11101110111011110111011101111011101110111101110111011101111011101110111101110\\ 11101111011101110111011110111011101111011101110111101110111011101\rightarrow ...$$ 

The corresponding convergents for this case is the Bronze ratio  $\beta$ .

As we have noted before these all these strings are aperiodic but have an intricate fractal structure. The growth of the length of these strings can be expressed by the two-seeded sequence  $f\lbrack n\rbrack =kf\lbrack n-1\rbrack +f\lbrack n-2\rbrack$  whose starting elements are  $f\lbrack 1\rbrack =1, f\lbrack 2\rbrack =f\lbrack 1\rbrack +k$ . When  $k=1$  we get the famous Meru sequence: 1, 2, 3, 5, 8, 13, 21, 34, 55, 89... with the  $\phi$  as its convergent. When  $k=2$  we get: 1, 3, 7, 17, 41, 99, 239, 577, 1393, 3363... with  $\sigma$  as its convergent. When  $k=3$  we get:1, 4, 13, 43, 142, 469, 1549, 5116, 16897, 55807... with  $\beta$  as its convergent. This last sequence and related sequences with  $\beta$  as their convergent has an intimate connection to the function  $y=\tfrac{2x-1}{x^2+3x-1}$ , which is at the root of some remarkable behavior which we shall explore in the next article.

