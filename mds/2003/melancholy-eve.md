
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Melancholy Eve](https://manasataramgini.wordpress.com/2003/12/08/melancholy-eve/){rel="bookmark"} {#melancholy-eve .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 8, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/12/08/melancholy-eve/ "11:14 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The yellow autumn had slipped by silently. The white cover brought with it the short days of diablerie, and the old curse of another शिवरात्रि passing by was apparent. A coin fell without any apparent reason, but the omen was hardly understandable. Four roads crossed at a certain point in the southern boundary of the kingdom of the Assakas of yore. On side of one of those roads was a buffalo shed and on the other the shrine of the tribal buffalo lord, Mahsoba. At the crossing of the four roads was the shrine of The deva. With respect to The deva one is not sure if one has commited a mistake. In any case the periodic curse of the gods fell on the victim. The curse drew the power of the victim in the tenth battle after two. In the 15th battle too it blunted the strength of the victim.

In the current episode a facimile of the fate of राधा's son appeared to loom. "When the moment dawns, even though you are a great kshatriya, your knowledge of the deadly astras that you plied with ease will depart you. You may even fail to control your chariot despite having a great charioteer. On the melancholy eve your head would be cut off".


