
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Bow-making in Hindu lore](https://manasataramgini.wordpress.com/2003/08/20/bow-making-in-hindu-lore/){rel="bookmark"} {#bow-making-in-hindu-lore .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 20, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/08/20/bow-making-in-hindu-lore/ "10:20 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The making of the bow is discussed in dhanurveda.\
\
It states that a bow should have 3 material: iron, horn and wood and should be 4 cubits in length. The Bow string is supposed to be made of bamboo fiber, hide or bark fiber. The middle part is recommended to have grip for the hand. The shape should be that of the "eye brows of a beautiful damsel" . A good bow is supposed to be made of a composite of horn, metal and wood. The good horns for a bow were said to be those of the buffalo, the rohisha deer and the recommended trees for the wood are the sAl, chandana, cane, dhavala or kakubha.. The tips of the bow termed Artni were made from horn reinforced with iron. The body was made with a combination of wood and horn or iron and horn. Additionally, Hindus also made an all iron or all bronze bow. From the hymns of the atharva veda one may infer that a composite bow was being refered to. So it is likely that the composite bow goes back to the early Aryan period. Thus the composite bow is unlikely to have been introduced to India by the हूनस्, चीनस्, mlecchas or पहालवन्स्. The all metal bow could be perpetually kept strung and used a ready weapon for assault like a pistol. The Hindus also appear to have wielded the bow with a thumb ring and used a leather guard to protect agains the bow string abrading the hand.

As many other Hindu traditions the traditional practice of archery had its last hold out in Thanjavur. The Maharatta king, Thulajendra Raja Bhonsle Chatrapathy ex-Raja of Thanjavur, a descendent of Chatrapati Shivaji, is said to have been trained well in the use of the bow by hindu traditions.


