
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [R's words](https://manasataramgini.wordpress.com/2003/11/04/rs-words/){rel="bookmark"} {#rs-words .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 4, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/11/04/rs-words/ "10:46 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some years ago R and me were lying on the grass following our quarry, a goodly specimen of [Menemerus]{style="font-style:italic;"}, that kept saltating in search of its quarry. Then our eyes caught sight of a [Salticus ]{style="font-style:italic;"}specimen that was capturing a small earwig and we kept following it to observe the process. R then moved on to a [Marpissa mucosa]{style="font-style:italic;"}, while I drifted away towards a gorgeous Icius. At the that point for some reason we paused. R pulled my collar and pointed to a [Calotes ]{style="font-style:italic;"}soulfully seated on a bough. I was a bit surprised and asked: So what. She then said: Did you think over my words last night.\
\
I said: Yes, but could only feel the fragile bliss shattered. We are definitely in some delusion. Sometimes I cannot believe that I could be that good.\
\
She said: Grab it when it comes. The bliss already passed when we set at the outdoor table in the restaurant looking at the terrapin.\
\
I said: Give me an year.\
\
She said: Look at the lizard there. A day will come when you will feel like it. It may be worse, you could feel like that [Argiope ]{style="font-style:italic;"}in the dark corner.\
\
I now realize that those words were prophetic.


