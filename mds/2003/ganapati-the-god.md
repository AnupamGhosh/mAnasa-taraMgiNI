
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [gaNapati-The god](https://manasataramgini.wordpress.com/2003/12/09/ganapati-the-god/){rel="bookmark"} {#ganapati-the-god .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 9, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/12/09/ganapati-the-god/ "11:13 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

gaNapati while being one of the most widely worshiped deities of the Hindu pantheon is one of the most poorly understood ones in terms of origins and history. The sectarian worship of gaNapati and कुमार unlike the vibrant and well-preserved shaiva, shakta and vaiShNava systems are largely extinct or facing extinction. The literature of the गणपत्यस् is also largely neglected or unknown to most Hindus (A consequence of this is their inability to properly circumvent weak western productions like that of Courtright).

The ancient origins of gaNapati as can be gleaned from the manu's गृह्य सूत्रस् and याग़्ण्यवल्क्य's स्मृतिस्. With my old home situated just some distance from the greatest गणपत्य kshetras I have always been fascinated with the evolution of the गणपत्य sect and its hidden mysteries. The one of the main Acharyas of the गणपत्य sect was गिरिजासुत who propounded the tantrik worship of महागणपति. Here gaNapati is concieved as being embraced by his Shakti with one tusk and an elephant face. This महागणपति gives rise to emanations namely, brahmA, विष्णु and rudra. From these emanantions the other devas headed by indra then emerge. The meditation of this gaNapati with the esoteric ulka gAyatri is supposed to lead to the path of moksha for these गणपत्यस्.

The next great गणपत्य Acharya was गणपतिकुमार. He founded the cult of हरिद्रागणपति. In its most basic form this gaNapati is worshiped in a lump of turmeric. The मूलमन्त्र of this sect is that of गृतसम्द shunahotra shaunaka भार्गव which begins as गणानान्त्व गणपतिं havamahe... They worship gaNapati in a yellow form with yellow silken clothing, yellow यग़्ण्योपवीत and having 4 arms, 3 eyes and a club, a pAsha and an ankusha. They also use the emanation imagery where the other devas directly emerge from gaNapati and then form the universe. The followers of this cult brand themselves with an image of gaNapati, just as the वैष्णवस् with the discus and conch of विष्णु.

The next great Acharya of the sect was herambasuta, who founded the heterodox वामाचर cult of gaNapati worship. They worship the form of gaNapati termed उcचिष्ट gaNapati. He is mediated in a form, which may be viewed by many uninitiated modern Hindus and others as being very obscene. The votaries wear red clothes, smear a red tilak on their forehead and are not bound by norms like marriage. They activate the great गणनाथ in the मूलाधर after stilling themselves with wine, eating modakas with their left hand and then performing sexual intercourse to kick start the upward ascent of गणनाथ's shakti.

The three Acharyas नवनीत, स्वर्ण and saMtAna founded other sects of gaNapati that are poorly studied. They follow a pantheistic view with the whole world being concieved as the parts of the great गणनाथ.They use mantras termed the gaNapati atharvashiras to perform their rites. They also perform the great rite of the gaNapati kalpa as laid out in the dharma शास्त्रस्. They also use vedic hymns to the maruts and rudra in their rites.


