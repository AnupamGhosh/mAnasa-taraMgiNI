
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Kasar Gavlis](https://manasataramgini.wordpress.com/2003/08/30/kasar-gavlis/){rel="bookmark"} {#kasar-gavlis .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 30, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/08/30/kasar-gavlis/ "10:18 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Near Baramati lived a band of Kasar Gavlis, who sold their curds and milk in brass pots. One day they were visited by विषूचिका. They all died as a result. The vacuum was filled by a roving band of Maharatta tribals called the Bhosles. The Bhosles also took to breeding the gavlis' cattle and trading milk and curd. They then were called the Gavlis. The original gavlis worshiped a horned buffalo deity called म्हसोबा. The incoming Bhosles too started worshiping म्हसोबा there after. Dotting the whole Pune district and its surrounding are buffalo breeding settlements and associated म्हसोबा shrines. Also seen are shrines of वाघोबा, खन्दोबा, धुलोबा, ज्योतिबा, म्हस्कोबा, बिरोबा and various आयीस् or female deities.


