
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Jinnah and Huntington](https://manasataramgini.wordpress.com/2003/09/11/jinnah-and-huntington/){rel="bookmark"} {#jinnah-and-huntington .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 11, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/09/11/jinnah-and-huntington/ "10:13 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Often, certain Hindus labor under the belief that they have a superior understanding of Christianism and Mohammedanism than the Christians and Mohammedans themselves. This was never more clear than in the case of Jinnah the founder of the Terrorist State of Pakistan. Eventhough Jinnah was pretty lucid in explaining how the Two nation theory worked, the Hindus completely failed to understand it. Even today many Hindus, both secularvaadi and otherwise, believe that the two nation theory is incorrect. What has been shown to be incorrect is the ridiculous "martial races" theory that was mainly an European invention. The crux of the issue is that, in the two nation theory, the person who who proposes it, is the one controls the dialog. So when Mr. Jinnah told Gandhi and Chachaji that there were two nations: Dar-ul-Momeen and Dar-ul-Harb, he was in control of the dialog right from the begining. So irrespective of whatever our Chacha or the Mahatma thought there were going to be two nations. The response of the Nehru-Gandhi clique was of course no different from the manner in which Hindus deal with contentious issues amidst themselves. This made no sense in the context of Jinnah because he was a part of Dar-ul-Momeen and idiots from Dar-ul-Harb have no say in the matter.

Several years later came Huntington, who, in his now legendary slender tome, provided a more generalized solution termed the clash of civilizations. He is of course correct in his explanations regarding civilizations based on Islam, Judaeo-Christianism, Marxism, and east Asian imperialisms. However, when it comes to Hindu civilization he is a little off mark because he is unable to grasp the fundamental structure of Hindu thought. This Hindu thought is unable to grasp the civilizational clash within its own constructs. In a sense it is like a spherical geometry, unable to reproduce the world of the hyperbolic geometry. The result of this construction of the Hindu world results in a peculiar duality. On the positive side it results in Hindus soaring to tremendous hights of intellectual achievement: you have the वसिष्ठ, the पाणिनि, the आर्यभट, the निलकण्ठ सोमयाजि and many others. You have the legendary Hindu tenacity to survive vicious life and death struggle against the deadly force of Islam. On the other hand you have the legendary apathy of India, an abundance of traitors willing to sell the Hindu cause for money or even without rewards. This latter phenomenon manifests as secularism in India. Another facet of this Hindu existence is the profound inability of many modern Hindus to understand why the Aryan invasion of India was a reality. They preserve the veda, but are unable construct their own history in a satisfactory way.

It is time Hindus pay more careful attention to the existence of alternative geometries where the relationships which are true in their world may not exist. The late Ram Swarup had initiated this re-thinking but his deep thoughts are yet to permeate through the Hindu masses.


