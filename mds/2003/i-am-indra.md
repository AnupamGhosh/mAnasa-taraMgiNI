
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [I am indra](https://manasataramgini.wordpress.com/2003/11/27/i-am-indra/){rel="bookmark"} {#i-am-indra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 27, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/11/27/i-am-indra/ "11:25 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I am indra,\
I made दिवोदास triumph over his foes.\
I handed over the वीतहव्यस् to pratardana,\
I gave sumitra a 1000 rods to slay दिर्घजिह्वी of a 1000 orifices.\
I am indra,\
the drinker of soma, without limit.\
In the sumada of soma I slew namuchi with my vajra concealed in foam.\
I am Indra,\
the brahman known by ऋशिस्, the entire universe.\
I slew कालकञ्जस्, I crushed the paulomas and expelled them from existence.\
I extirpated प्रह्लाद's brood and had my brother विष्णु smash virochana.\
I am indra,\
praised by the भृगुस् and the अञ्गिरस् who know the right path.\
The might of my vajra no one has borne, when the mountains flew I brought them down.\
The arurmaghas opposed me and I annihilated them without exception;\
the yatis who derided my worship I gave away to hyaenas.\
I am indra,\
who cut off the 3 heads of trishira त्वाश्ट्र, the ritualist of the दासस्.\
For the descendents of manu I slew the the arrogant dasyus and gave the world to the Aryas.\
When the Aryas, arNa and chitraratha, bloated with pride opposed my worship I brought an end to them, on the banks of the sarayu.\
I am indra,\
who drunk 3 lakes of soma and through my might trasadasyu, the great son of purukutsa, conquered the forts of the दासस् of slurred speech.\
My fatal arrows rained on the दानवस् from my infallible bow of variegated colors.\
Thus did pipru, kuyava, सुष्ण and varchin meet their end when they opposed me in battle.\
I am indra,\
by whose might the valiant सुदास triumphed over his foes on the banks of the the परुष्णि and through my fury the unfettered विपाशा flowed, unstoppable.\
sarasvati, विपाशा, परुष्णि, sindhu, asikni, कूभा, वितस्ता, सुशोमा, गङ्गा and यमुना swelled by me, raced, like mares on the race course, to the the ocean.\
I am Indra,\
who animating विष्णु and the maruts put an end to वृत्र with my spear and vajra.\
When रौहिण tried to scale upwards towards heaven, I struck him with my dart and sent him to the wells of perdition.\
When shambhara stood in the path of the Aryas, I picked him by his feet and hurled with force off a mountain top.\
I am Indra,\
who shattered the 99 metal forts that stood on my path, and when animated in battle my roar is even heard by the deaf.\
I have borne away the head of makha and sent dhuni and chumuri to nether regions.

indra is greater than all!


