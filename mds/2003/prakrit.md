
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Prakrit](https://manasataramgini.wordpress.com/2003/08/05/prakrit/){rel="bookmark"} {#prakrit .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 5, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/08/05/prakrit/ "10:30 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

hattha=hand\
bahira=outside\
vahu=son's wife\
kOra=horse\
tObI=washer\
kANI=story\
pai=brother\
lihai=write\
suha=happiness\
sAhA=branch\
maueda=crown\
kayala=plantain\
koha=anger\
meha=cloud\
dIva=island\
vihalai=split\
subha=good\
makkaDa=monkey\
Naha=nail

The Dravidian language Tamil retains a large number of old loans from Maharashtri Prakrit, even though its descendent language Marathi does not. This suggests that the prakritic base of other IA languages were overlayed by a secondary layer derived from a later degeneration of Sanskrit. This secondary layer is actually a Pan-Indian phenomenon and could represent the point of normalization of the Pan-Indian Sanskritic elite with respect to the Prakritic masses.


