
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Dream interrupted](https://manasataramgini.wordpress.com/2003/11/06/dream-interrupted/){rel="bookmark"} {#dream-interrupted .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 6, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/11/06/dream-interrupted/ "10:45 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We moved up the hill rapidly (remember the hill which came in the dreams!). It was the brief pleasure sent by the gods. It was like that, which had visited the four kings, namely दिवोदास, उषिनार, विश्वामित्र etc in the form of ययाति's daughter माधवि. But it was the shorter and wholly empty unlike that briefly enjoyed by those great kshatriyas of yore. Coursing freely in a rapid ratha we reached the paravata. Before dismounting from the ratha, I acted in the most atypical manner deviating significantly from the norms that exist. Yet nothing happened! I took it as sign of victory. On reaching the parvata we freely moved in the manner of विश्वामित्र on the slopes of the हिमपार्वत, brushing aside the dark निशादस् and दासस् who infested the mountain strongholds. Having reached the upper reaches we beheld the yonder territory, like videgha माथव beholding the expanses of mithila ripe for conquest. I did not realize at that time that I lacked the equivalent of the son of राहूगण beside me. Then we descended. The solitude allowed some images to hang motionlessly. This was a sign that indicated a total victory. After all it was ordained that those pleasures were even destined for me in the very kshetra, where I had observed the vratas. Then having moved amidst the settlements of the nishadas and dasyus we returned to my place. The black bitch signalled that the noose of karma could not be escaped, even as the great विष्णु could not escape his 10 incarnations. I returned like ययाति after a stroll in shakra's garden. Then, just as ययाति having exhausted his stay in svargo loka fell head-long back to the midst of mortals, my karma having been exhausted I descended to the lower worlds. When my descent was complete and my feet hit the ground, I looked around. Unfortunately, unlike ययाति, there was no pratardana, वसुमना, अष्टक or shivi to revive me and remained trapped in the dark lower loka!


