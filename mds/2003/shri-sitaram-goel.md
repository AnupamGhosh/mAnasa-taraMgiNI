
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Shri Sitaram Goel](https://manasataramgini.wordpress.com/2003/12/03/shri-sitaram-goel/){rel="bookmark"} {#shri-sitaram-goel .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 3, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/12/03/shri-sitaram-goel/ "11:16 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The doyen of the Hindu revival, the battler of secularists, communists and other allied fiends and the exposer of India's middle period history has become a guest of the great Vaivasvata. He has been the corrective influence on many a Hindu and been at the vanguard of the fight to preserve the Dharma.

He has lived a full life and protected his small publishing houses the Voice of India and Aditya Prakashan. Along with Ram Swarup will remembered long for his attempt at awakening the slumbering Hindu consciousness that had been subverted by the disasterous English rule.

A Hindu is first a Hindu and only then an Indian, or Carribean or whatever. India assumes its importance for Indians because it has been the home for the Hindu Dharma for more than 3 millenia.


