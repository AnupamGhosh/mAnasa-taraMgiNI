
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Indians:a bunch of idiots?](https://manasataramgini.wordpress.com/2003/09/28/indiansa-bunch-of-idiots/){rel="bookmark"} {#indiansa-bunch-of-idiots .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 28, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/09/28/indiansa-bunch-of-idiots/ "10:08 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A gentleman called Pingree and his lackey called Plofker would like us to believe that the Indians were a bunch of idiots till the benign rule of the white man brought them to light. In essence what they tell us by means of writing several prolix papers and using sophisticated jargon is the same as what Mr. Macaulay briskly delivered in his minute. "The vast accumulations of Indian literature is worth less than a shelf of books in a good European library."\
Why is this the case? The learned Pingree tells us that till Alexander's invasion circa 300-400 BC the Indians did not even recognize the planets in the sky. The around the 200 AD the Indians with liberal help from the Greeks, Babylonians and assorted Middle Easterners came up with their first model of the universe where the earth was a vast square flat piece of existence above which were situated various planets in concentric spheres. This whole phantasm was supported by a menagerie of oversized turtles, serpents and elephants. The Indians believed that this was शास्त्र and could not give up these terrible concepts till the 19th century. The piercing intellect of the Moslems showed the Indians that they were talking bullshit, but they insisted on sticking to their dogmas. The benign rule of the Moslem tyrants only aided them to pursue these dogmas without change. They wrote texts called virodhas that tried to prove that the earth was flat and a new sun shone every month of the year. Finally the sight of the awe-inspiring European gadgets made the old fashioned pandits give up their stupidity and accept the truth.

It is a necessary and important aspect of sub-altern studies that one study the stupidity of Indians with sympathy and write reams about it rather than flippantly tossing it into the trashcan as Ol' sir Blabberington had done more than a century ago.


