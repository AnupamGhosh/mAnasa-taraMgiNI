
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief summary of the state of early Indian history](https://manasataramgini.wordpress.com/2003/07/20/a-brief-summary-of-the-state-of-early-indian-history/){rel="bookmark"} {#a-brief-summary-of-the-state-of-early-indian-history .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 20, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/07/20/a-brief-summary-of-the-state-of-early-indian-history/ "3:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For a moment let us set archaeology aside and examine what textual evidence is available for the dates in early Hindu history.

Generally people accept the following relative chronology:

  - Early/middle layers of the ऋग्वेद, parts of the atharva veda.

  - Latest ऋग्वेद, core atharva veda, core yajur veda

  - Events of the रामायण

  - vedAnga ज्योतिष, core events of महाभारत, late vedic material

  - systematization of vedic संहितस्, shrauta rituals, janamejaya's conquest of takshashila

  - first पुराणस् composed, end of vedic composition

  - brahminical sutra literature begins

  - पाणिनि, कणाद

  - buddha\
Other assumptions that generally may be accepted:

  - based on their content it appears that the ब्राह्मणस्/उपनिषत्स् were composed starting from the circum-रामायण period right down to the sutra period.

  - many hindu texts are layered and mix compositions of different ages.

  - Most of the records are pUru-centric, followed by इक्श्वाकु, yadu, videha/kanyukubja. There were significant areas of Indo-Aryans who did not belong to this basic extended core. These included, for example haihaya yadus, some anu kingdoms, बृहद्रथस्, काशिस्, प्रग्ज्योतिषस्.

Main inference that may be drawn regarding dates:

  - In my examination both the epics while having several astronomical allusions represent mixed tradition. Hence it is obvious that one gets widely conflicting dates.

  - However, the इतिहास and पुराण tradition have memory of a very long standing series of monarchs amidst many Indo-Aryan dynasties: 80-95 generations from the start to the bhArata war. These lists are largely consistent across various पुराणस् and epics, hence the lists of this size could not have been simply concocted. Hence, even with a conservative estimate about 1700 years had elapsed since the bhArata war to the start of Indian memory.

  - In my opinion the best way of obtaining historical dates is by using astronomical data. We get one such anchor point in the form a series of hymns from the yajurvedic and and atharva vedic corpus in the form of the कृत्तिक at 0 hr on Spring equinox. This constellation is very closely linked to some very basic vedic rituals and hence it is very contrived to imagine it as being a reminiscence. Also by the time of the vedAnga ज्योतिष a change had occured in the 0hr and this was recognized. There are allusions to an earlier equinotical point in the रोहिणि nakshatra. These facts have been known for more than 100 years now and its plain foolishness on the part of detractors to wish them away. If one were to believe them the Indo-Aryans only reminisced of the past but never composed anything regarding the present.

  - From these observations one can calculate the sheet anchor कृत्तिक date as being \~२२००BC, that is the core yajur/atharvan period. The core yajur ब्राह्मण portions were clearly after this period and contain mentions of kings corresponding to the bhArata war era, like विचित्रवीर्य, धृतराश्ट्र and janamejaya. So the bhArata war was well after 2200BC. And based on the earlier astronomical allusions in the ऋग् and brahminical genealogies the early phase of the ऋग् can be placed around 3100 BC. Considering that some of the early kings in the purANic king lists such as मन्धाता, trasadasyu, purukutsa, mudgala, भृम्यश्व and विश्वामित्र span a part of the early ऋग्वेदिc period one may fix the bhArata war era as definitely occurring after 2000BC and probably even after 1500 BC. The most plausible dates being between 1500-1000 BC. This also matches the पौराणिc testimony of the bhArata war relative to the nanda regime.

  - If the Indus valley civilization had shown clear Hindu (Indo-Aryan) characteristics then one may have safely assumed that all this history played out entirely in Greater India, with a pre-ऋग्वेदिc, pre-IVC Indo-European invasion of India. However, this is not exactly the case and hence one is left with the invasion and Aryanization during the IVC as the best option.

  - IVC archaeology is not really in an advanced state and hence not much can be made of the details from the existing information.

  - So after all New Ayodhya, New Kanyakubja may not be far fetched.


