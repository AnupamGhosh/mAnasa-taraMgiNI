
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Frontline warriors](https://manasataramgini.wordpress.com/2003/09/06/frontline-warriors/){rel="bookmark"} {#frontline-warriors .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 6, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/09/06/frontline-warriors/ "10:14 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The charge of the Indian brigade has been consistently plagued with the lack of proper frontline troops who can bear the cannon fire of uncouth, wild-eyed, gutter-mouthed barbarians like Brett Lee, Mcgrath and Pollock. In contrast, when India has to return the fire with the occassional exception of the over-used field gun, Shrinath, the rest of the guns typically fail to fire, or deliver noisy, but empty thunderbolds like Agarkar. The result is the now familiar rout and dispersal of Indian troops in any encounter with an average or better opposition on battle fields outside the subcontinent. The two factors are clearly linked: The inability to produce high velocity guns results in the inability to produce warriors who can fight when high velocity guns are being fired. As result the Indian army always teeters on the slow lane and panics when the heavy guns go off on the maidan-i-jangs.

This brings us to one great mystery: Why are those frontline warriors showing potential, often forsaken for third rate replacements who are as such destined to shatter when confronted with the barbarian gunnery. Take the brave warriors of yore, like Sadagopan Ramesh, Wookeri Raman, etc all of whom had potential, but were replaced with flimsy defenders like Rathore, Gandhi, Bangar and their ilk.

With no big guns in sight, and one more war with the Barbarians of Southern Saxony coming up, one can only conjure up ghosts of the 92 and 99 . They have been seared into the Indian mind like the encounters on the Panipat plain.


