
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [महाकालि](https://manasataramgini.wordpress.com/2003/09/30/mahakali/){rel="bookmark"} {#महकल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 30, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/09/30/mahakali/ "10:07 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The भार्गव sage मार्कण्देय narrated the 3 tales of the fierce goddess and the battles they fought.

The first tale was that of महाकालि. On account of her mAya, the great god विष्णु lay in slumber when the plane was submerged in the waters at the end of the kalpa. There arose from the wax in his ears two terrible daityas, madhu and कैटभ. They rapidly advanced to slay brahmA when महाकालि in order to awaken विष्णु arose from his forehead and departed. विष्णु having awoken fought the daityas in a relentless battle for 5000 years. At the end of it, under महाकालि's delusion the daityas sought a boon from विश्णु. As the whole plane lay submerged in the waters, the daityas asked the boon that विष्णु could only kill them in the spot where there was no water. विष्णु granted it to them. He immediately raised the daityas to his thighs and beheaded them with the chakra.


