
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Orang-utan behavior](https://manasataramgini.wordpress.com/2003/08/09/orang-utan-behavior/){rel="bookmark"} {#orang-utan-behavior .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 9, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/08/09/orang-utan-behavior/ "10:28 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great ape of East , the Orang was until recently thought to be rather limited in its behavioral complexity. The lineage of the Orangutan is an ancient one, including Sivapithecus, which inhabited the subcontinent under 16 Million years. Given its time of divergence from the rest of the anthropoid apes the matter of its behavioral complexity has been one of great interest. In captivity the orang shows much dexterity and capability of tool use, much like the chimp or Bonobo which are closer to Homo. The summation of several years of work has shown that the Orang has many of the behavioral traits that are commonly associated with [Homo ]{style="font-style:italic;"}and [Pan]{style="font-style:italic;"}. One striking example is how orangs use organized hunting to kill the slow loris for food. This is very reminiscent of the chimps hunting the colobus monkeys. Orangs also make noises using leaf whistles and sexually stimulate themselves with sticks (reminiscent of bonobos). So their behavioral complexity in captivity is indeed mirrored in nature. Thus, most of the basic habits of human behavior were already in place before 16 Million years in the common ancestor of the [Sivapithecus]{style="font-style:italic;"}-- [Ouranopithecus ]{style="font-style:italic;"}clade.

Amidst the apes, I see human behavior as being close to the primitive behavioral state of the ancestral anthropoid ape. This primitive state is exaggerated in some apes while muted in the others. Thus for example social interactions are somewhat muted in the orangs, while aggression is exaggerated in the chimps, while sexuality is exaggerated in the bonobos. The increase in brain size in apes of the human lineage (Homo, Paranthropus, Australopithecus) does not appear to have interfered with the basic behavioral repertoire. The persistance of these ancient behavioral characters in the orang mean that we are hardly going to lose them anytime soon, even though some people may want to erase them.


