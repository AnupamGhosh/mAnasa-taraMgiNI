
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Netaji Palkar](https://manasataramgini.wordpress.com/2003/08/15/netaji-palkar/){rel="bookmark"} {#netaji-palkar .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 15, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/08/15/netaji-palkar/ "10:24 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In 1667, the great Maharatta general Netaji was arrested by the lowly Hindu traitor Mirza Raje Jaisingh under the orders of the Padishah Awrangzeb and placed in the custody of the Qazi of Delhi. The Maharatta was give given three days to chose between the options of losing his head or his foreskin. Netaji decided to forego the latter and was duely made Muhammad Quli. His wife and children were also captured and converted to the fold of Allah. He was then sent to Afghanistan to quell the rebellions of the Afghans and battle the Iranians.

In June 1676 he escaped with his family from the clutches of the Mohammedans and retreated back to Maharashtra. Here he was purified on Shivaji's orders and returned back to the Hindu fold.

This was rare case when a Hindu had returned from the abyss of the Mohammedan fold.


