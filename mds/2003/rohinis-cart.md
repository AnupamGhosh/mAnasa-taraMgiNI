
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [रोहिणि's cart](https://manasataramgini.wordpress.com/2003/08/17/rohinis-cart/){rel="bookmark"} {#रहणs-cart .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 17, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/08/17/rohinis-cart/ "10:32 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In Greco-Roman star lore the constellation Auriga is the charioteer. Traditionally, in India too the constellation is refered to as the सारथि, which means the same thing. The mainstream Western scholarship has believed that this is a recent acquisition of the constellation name from the Greco-Roman world.

If Auriga is the charioteer, where is his car? India star-lore records an unsual name for the Hyades cluster in constellation of Taurus: रोहीणि शकट: The cart of रोःइणि (alpha Tauri) \[sUrya siddhAnta, 8.13]. Not surprisingly, the constellation is also called the शकट of prajApati or brahmA [ पञ्च siddhAnta 238-241]. Traditionally, Hindu astronomy records the star Capella (alpha-Aurigae) as brahma-हृदय (the heart of brahmA). This suggests that the Hyades, which indeed look like a simple cart was the cart of रोहिणि or prajApati, thus linking the cart to the charioteer. Hence, while lateral Greco-Roman influence cannot be ruled out entirely, it is likely that the basic concepts concerning this constellation go back to the misty Indo-European past when Greeks and Indians shared a common ancestor.


