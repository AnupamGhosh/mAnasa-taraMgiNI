
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Masi Reddi](https://manasataramgini.wordpress.com/2003/09/01/masi-reddi/){rel="bookmark"} {#masi-reddi .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/09/01/masi-reddi/ "10:17 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Masi Reddi and his wife Nilamama lived in the eastern ghats selling wood with their 6 sons. They invoked महादेव, who appeared in their house in the form of a jangama. He gave Nilamma some ashes and said they would become rich when another son was born. This son was Undala Padmanna. Soon they became prosperous by acquiring agriculture. However Padmanna never did any work. So his brothers decided to kill him. They asked him to burn a termite hill hoping that a cobra that lived in it would emerge and bite him. He did so an instead a herd of black bleating sheep emerged. He was frightened by them, but rudra appeared and asked him to rear them and live by their milk. rudra taught how to milk the sheep and boil their milk. He then sent him to a distant place where राक्शस् lived and obtain fire from there. The राक्शस् had captured and chained a ravishingly beautiful fair-skinned ब्राह्मण girl. Though of collyrium complexion Undala Padmanna seduced the ब्राह्मण girl and managed to release her. As they were running away the राक्शस् started chasing them. The brahmin girl asked the Reddi to run ahead to attract the राक्शस् attention. As they chased him, she made her escape and used a Sanskrit spell to turn the Reddi into a *Varanus* lizard. The राक्शस् were confused and returned. She then made him back into a man and they returned to their flock with the fire. He married her giving her a woollen bracelet. Then he married a tribal woman of his own ethnicity and gave her a cotton bracelet. The descendents of the brahmin woman founded a higher semi-sanskritized caste amidst the herders.


