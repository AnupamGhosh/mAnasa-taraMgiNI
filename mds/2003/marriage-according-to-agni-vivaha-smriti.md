
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Marriage according to agni-विवाह स्मृति](https://manasataramgini.wordpress.com/2003/07/29/marriage-according-to-agni-vivaha-smriti/){rel="bookmark"} {#marriage-according-to-agni-ववह-समत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 29, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/07/29/marriage-according-to-agni-vivaha-smriti/ "10:31 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While there are reams of writings on the yeses and nos of Hindu marriage there is a short sanskrit tract called the agni-विवाह स्मृति that provides its somewhat atypical account of the same. The text is attributed to a certain पुष्कर and contains 19 verses. The tract may be dated to approximately to the period of the first 6 centuries of the common era. I examined it because it throws interesting light on old Indian customs. Below are some excerpts:\


 1. A Brahmin is allowed a maximum of 4 wives and the number decreases for each subsequent वर्ण.\


 2. The kshatriya is asked to hold an arrow during marriage, while the vaishya a bag having a toad in it, and the shudra a piece of cloth.\


 3. One who abducts a woman is recommended the death punishment or payment of a heavy fine.\


 4. The wife may choose a new husband under the following conditions:\
\
a) the male is sterile b) the male indulges in promiscuous dalliance or visits public women c) the male has no interest in married life and takes संन्यास d)the male dies e)the male vanishes during a journey or voyage. Ideally if the male dies, the female must take the brother of the male.\


 5. Same gotra marriage should not happen \[If it doesthe male and female should cancel their marriage and live as a brother and sister avoiding intercourse]. The male and the female should not share a common ancestor for 7 generations on the paternal side and 5 on the maternal side.\


 6. An image of the great goddess इन्द्राणि was made of pot clay on the marriage day and worshipped by the woman beside a pond, then the bride is lead to the house to the accompaniment of music.\


 7.  There was the आर्ष marriage where the marriage was accompanied by the gift of wealth, and there was the asura marriage where a hefty purchase money was payed to obtain the woman from her clan.


 8.  And then as in these days there was some astrology.

The traditions of Hindu marriage have considerably changed since those time


