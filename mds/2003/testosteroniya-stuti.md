
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Testosteroniya stuti](https://manasataramgini.wordpress.com/2003/09/18/testosteroniya-stuti/){rel="bookmark"} {#testosteroniya-stuti .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 18, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/09/18/testosteroniya-stuti/ "10:09 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Males get to enjoy this wonderful steroid. The poets of the long past days may have called it loosener of काम's shara-s. It creates mathematical prowess where none existed previously and quickens activity like no other secretion. The mind starts conceiving what was previously unthinkable. It is on account of this that males get more violent, more innovative, and at times downright brilliant.

When it was not there I could not multiply two matrices,\
when it surged forth from its fount I saw the inverse of a matrix.\
When it flowed not, I saw in wonder my fellow farers being ruined by सुजाता,\
but when it filled the channels I conquered सुजाता and her handmaids.\
The Hamiltonian of H left me dazed: had it not coursed through my tubes the pretty orbitals I would have never seen.\
The alphas left me shuddering in terror, but due to its grace I drew them to my lair.\
They felt confidently triumphant in theirs, but then it had joined for me new wires which were not there.\
Beefy they were, as I was lean, but I left them stewing in their own blood.\
May it keep us welling with might for long.


