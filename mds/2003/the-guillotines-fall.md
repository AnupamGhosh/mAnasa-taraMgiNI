
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The guillotine's fall](https://manasataramgini.wordpress.com/2003/10/30/the-guillotines-fall/){rel="bookmark"} {#the-guillotines-fall .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 30, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/10/30/the-guillotines-fall/ "10:49 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Are standing in front of the guillotine?\
Or did you walk through the guillotine before the blade fell?\
Or is your head on the block waiting the fall of the blade?\
One verily does not know..\
Sometimes you do things you would have never done; why did you do them?\
I really cannot tell.


