
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Gory Tamil poems](https://manasataramgini.wordpress.com/2003/10/01/gory-tamil-poems/){rel="bookmark"} {#gory-tamil-poems .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 1, 2003]{.entry-date}](https://manasataramgini.wordpress.com/2003/10/01/gory-tamil-poems/ "10:05 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The old woman's shoulders\
were dry, unfleshed,\
with outstanding veins;\
her low belly\
was like a lily pad.

When people said\
her son had taken fright,\
had turned his back on battle\
and died,

she raged\
and shouted,\
" If he really broke down\
in the thick of strife,\
I'll slash these breasts\
that gave him suck,"

and went there\
sword in hand.

Turning over body after fallen body,\
she rummaged through the blood-red field\
till she found her son,\
cut, in four pieces,

and she rejoiced\
more than on the day\
she gave him birth.

नccएळ्ळैयार् काक्कैपाडिनियार् vide Ramanujam

Old tamil society was a heroic and gory one, full of cattle raids leading to skirmishes and full fledged battles. They were fought with spears, arrows and swords with no ritual show, but plain ferocity.

Poetess औवैयार् sings of the fall of her patron, a tamil war lord अतियमान् नेडुमान् अङ्चि:\
"Pointing at the enemy\
the tongues of lances,\
new-forged and whetted,\
urging soldiers forward\
with himself at the head,\
in a skirmish of arrows and spears,\
cleaving though an oncoming wave of foes,\
forcing a clearing,\
he had fallen\
in that space\
between the armies,\
his body hacked to pieces."

औवैयार् singing on her dead warlord अदियमान् नेडुमान् Anchi

"He gave us\
all the flesh\
on the bones.\
where is he now?

wherever spear and arrow flew,\
he was there.\
where is he now?

with his hands scented\
with lemon grass,\
he caressed my hair\
smelling of meat.\
where is he now?\
The vEl that pierced his chest\
pierced at once\
the wide eating bowls\
of great and famous minstrels,

and, dimming the images in the eyes\
of men he sheltered,\
it went right through the subtle tongues\
of poets\
skilled in the search\
for good words.

Where is he now?


