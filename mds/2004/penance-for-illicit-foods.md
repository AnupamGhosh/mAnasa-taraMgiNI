
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [penance for illicit foods](https://manasataramgini.wordpress.com/2004/09/05/penance-for-illicit-foods/){rel="bookmark"} {#penance-for-illicit-foods .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 5, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/05/penance-for-illicit-foods/ "5:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

1.1) drinking वारुणि beer inadvertantly: Has to have to undergo a new उपनयनं to be ritually fit.\
\
1.2) drinking वारुणि beer intentionally: There is no penance- the death is the only reliever, and one looses that status of a dvija\


 2.  If he touches, receives or gives a beer or a wine, he should drink water boiled with kusha grass for 3 days.\


 3.  If he accidently drinks urine he must undergo a re-उपनयनं.\


 4.  If inadvertantly eats foul-smelling food he is ritually impure until it is entirely excreted.\


 5. If he eats forbidden meats, mushrooms, or meat from a slaughterhouse he must under go the चन्द्रायन penance.\


 6. For meat derived from fowls he must undergo the arduous सान्तपन penance.\


 7. If he eats food tainted by the lick of a cat, dog, rat, mongoose, crow or insects, he should consume a decoction of the brahmasuvarchala plant that will save from their disasterous effects. He can become ritually pure if he vomits those foods.


