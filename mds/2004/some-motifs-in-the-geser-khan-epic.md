
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some motifs in the Geser Khan epic](https://manasataramgini.wordpress.com/2004/08/10/some-motifs-in-the-geser-khan-epic/){rel="bookmark"} {#some-motifs-in-the-geser-khan-epic .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 10, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/10/some-motifs-in-the-geser-khan-epic/ "5:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Geser Khan Epic of the Turko-Mongols is one of the last frontiers of comparative mythology that remains unexplored in terms of its potential to provide insights into the archaic structure of Eurasian mythology. It is the only surviving Eurasian epic that in its size and scope compares with the महाभारत of the Indo-Aryans.

The epic* beings with the lines:[\
]{style="color:rgb(102,0,204);"}\
*[When Khan Hormazda Tengri was a swaddled child, ]{style="color:rgb(255,0,0);"}*\
*[In the very beginning, when the thousand white gods]{style="color:rgb(255,0,0);"}*\
*[were being rocked in the warmth of their cradles,]{style="color:rgb(255,0,0);"}*

  - [Ten thousand white tenger were raised by Mother Ekhe Yuuren Ibii.\
When fifty-five tengri were born from the Milky Way,]{style="color:rgb(255,0,0);"}*\
*[When many rich tenger began in the evening time,]{style="color:rgb(255,0,0);"}*\
*[A thousand gods and protector spirits and ten thousand tenger were supported by]{style="color:rgb(255,0,0);"}*\
*[Grandmother Manzan Gurme Toodei with her silver cup.]{style="color:rgb(255,0,0);"}*\
[[]{style="color:rgb(0,0,0);"}]{style="color:rgb(255,0,0);"}\
The point of interest to be noted here is the principal protagonist of the epic is the great deity Hormazada. He is clearly a derivative of the Iranian deity Ahura Mazda, the cognate of the Indo-Aryan वरुण (asura medhira). This suggests that development of the main epic structure occured after the first contact of the Altaic tribes of Central Asia with the Iranian branch of Indo-Europeans. While there is definitely older purely, Altaic and Eurasiatic material in the Epic there its development in current form did have the Iranian influences. Linguistic evidence strongly suggests that the early Altaic words for pastoralism are of Indo-Iranian origin, suggesting that these cultural infusions may have had a common source. The Iranians of the proto-Kushana and Shaka lineages were amongst the earliest to interact with the archaic Huns during the meteoric rise of the first Hun Kha'Khan of all Mongolia, Motun Tegin. The history of Motun Tegin documents the role of the Iranian tribes in introducing the Mongols to effective horse-borne pastoralism. This was soon followed by the crushing defeat of the Shakas and the Kushans on the steppes and their eventual eviction by the archaic Huns.

While the possibility of even earlier interaction between the Altaic tribes and the Indo-Aryans is suggested by the transfer of the Altaic goddess Umai to the Indo-Aryans (as उमा, the wife of rudra, earlier known as अम्बिका and पृश्णि), the exact contributions of this interaction cannot be confidently assessed.

\*The translations cited here are those made from Mongolian by Sarangerel Odigan


