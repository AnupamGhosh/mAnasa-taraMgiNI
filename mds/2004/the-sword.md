
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Sword](https://manasataramgini.wordpress.com/2004/09/16/the-sword/){rel="bookmark"} {#the-sword .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 16, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/16/the-sword/ "5:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

nakula the 4th पाण्डव asked the kuru grandsire on his arrowy death bed about the origin of the sword. nakula said that he believed that the sword was a superior weapon and even if one lost his bow or horse or chariot one could still defend himself against mace and spear wielders if one were a good swordsman.

भीश्म replied that he was excited beyond words by that question and began to utter a narrative.

"In the beginning there there was no sky, nor light nor motion. Extending over it all was utter silence. In his own proper time prajApati manifested as हिरण्यगर्भ. From prajApati emerged the sky, the stars, the earth and divisions of time. From him emerged the deva rudra and devas: the maruts, Adityas, ashvins, and vasus. brahmA then manifested as various living forms which included the ऋषिस्. prajApati then promulgated the सनातन dharma. The ऋषि-s saw this dharma in the form of the vedas and lived in obedience to it. However in the darkness of night the दानवस् began transgressing the dharma. They were हिरण्यकशिपु, हिरण्याक्ष, virochana, shambhara, viprachitti, प्रह्लाद, namuchi, bali and many others, who delighted in evil deeds. The persecuted and afflicted all beings by thrashing them with their rods of chastisement.

Then prajApati collected objects to perform a grand sacrifice with the foremost of the devas. I have heard from the ऋशिस् that something awful happened in that sacrifice. A creature sprang from the midst of the sacrificial fire scattering flames all around him. It was as though a moon had arisen in the midst of the stars. He was colored like a deep-blue lotus. His teeth were sharp, stomach lean and stature tall. He was of exceeding energy and winds were howling all around, trees were being torn apart and meteor blazed through the skies. Then the great prajApati declared: This being whom I have conceived is known as asi, and he shall effect the destruction of the enemies of the gods. Then that being assumed the form of a blazing, sharp-edged sword glowing like the flame at the end of the kalpa. brahmA then gave that sword to the blue-necked rudra with the bull-banner and asked him to put down pApa and adharma.

Then that rudra praised by the great ऋशि-s took up that sword and assumed a terrible form. He put forth four arms and filled the space between heaven and earth. Flames gushed out of his mouth and he assumed diverse colors of blue, white and red. He wore an upper garment of black deerskin studded with stars of gold and he bore an eye on his forehead that resembled the sun in splendor. His other two eyes were black and tawny. The awful महादेव also picked up huge shield with 3 studs resembling a black mass of clouds with flashes of lightning. Then the great deva uttered terrible roars and laughing awfully, began to whirl the sword in the sky desiring a showdown with the demons.

The दानव began attacking rudra with diverse razor sharp and fiery weapons. But rudra though single handed moved so rapidly with his sword that the demons thought that they were facing a thousand rudras. Tearing, piercing, lopping off, mincing and smashing down the great deva moved with great celerity amongst the daityas, like a forest conflagration consuming dry wood. Arms severed, heads lopped off, chests pierced the दानवस् expired, while other fled penetrating the depths of the earth or the oceans. The earth became miry with flesh and blood of the दानवस् by the acts of rudra. Drenched in gore the earth looked like a fair-complexioned maid intoxicated with alcohol and attired in crimson robes in abandon. Having thus extirpated the demons and re-established dharma rudra cast of his awful form and assumed the benign shape, shiva.

rudra gave this sword dyed with the blood of the daityas to the deva विष्णु. विष्णु gave it to the deva indra. indra then gave it to the other devas. They presented that mighty sword to manu. Giving it to him they said: O manu protect the world with this sword having dharma in its womb. Duly deliver punishments to those who transgress dharma. Never miss use it according to caprice. Some should be punished by wordy rebukes, fines and confiscations. Mutilations and death punishments should never be inflicted for small transgressions. This all these punishments are various shapes of this sword, O manu with which the law in maintained and creatures protected. manu then passed the sword of the god to his son क्षूप. From क्षुप it passed to manu's other son इक्ष्वाकु.

From him the sword of dharma passed to pururavas born of iLA. From him it passed to AyU. From him it passed to नहुष. From him it passed ययाति. From him it passed to pUru. From him it was wrested by अमूर्तरायस् of the clan of the अमावसुस्. From him it went to bhumishaya. From him it went to bharata daushyanti. From him it went Ailavila, the upholder of dharma. From him it went to कुवलाश्व the ऐक्ष्वाकव. From him it went to kambhoja. From him it went to muchukunda. From him it went to marutta. From him it went to raivata. From him to युवनाश्व. From him it went to raghu, the great conqueror. From him it went to हरिणाश्व. From him it went to shunaka. From him it went to उशिनार. From him it went to the bhojas and yadavas. From the yadus to shivi. From him it went to the partardana-s of kAshi. Then it was taken by विश्वामित्रस् of the अष्टक lineage. Then it was taken by the पञ्चल पृशदश्व. From him it went to the brahmins of the भरद्वाज lineage. The last of that lineage was droNa. He gave to कृप. He in turn gave it to the पान्डुस्.

कृत्तिक is the नक्षत्र of the sword. रोहिणि the gotra, agni the deity, and rudra the maharshi. It is verily धर्मपाल the upholder of dharma.

It is praised by the names:

[**"असिर् विशसनः खड्गस् तीक्ष्ण-वर्त्मा दुरासदः ।\
श्रीगर्भो विजयश् चैव धर्मपालस् तथैव च ॥"**]{style="color:#0000ff;"}

After narrating the above AkhyAna, भीष्म tersely notes:

["अग्र्यः प्रहरणानां छ खड्गो माद्रवतीसुत"]{style="color:#0000ff;"}

The sword is the foremost of the striking weapons, O son of माद्रवति.

The above इतिहास is narrated from महाभारत (vulgate text, chapter 167/ "critical edition" chapter 161 of शान्ति parvan).


