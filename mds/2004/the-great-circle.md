
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Great Circle](https://manasataramgini.wordpress.com/2004/04/18/the-great-circle/){rel="bookmark"} {#the-great-circle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 18, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/04/18/the-great-circle/ "2:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Berossus, the priest of Marduk of the great god of the Babylonians declared in the 3rd century BC that 432000 solar years was the time that had elapsed from the origin of divine kingship to the end of the world. He believed that when 432000 years elapsed from the begining of divine kingship, the planets would conjunct in Capricorn and mark the begining of a world destroying fire. Many western scholars have assumed that this number entered India and was taken up by the पौराणिc bards for describing the महायुग. The period of the महायुग comprising of कृत, treta, dvapara and kali is 4320000 (10*432000), with each of the 4 yugas respectively having 4, 3, 2 and 1 units of 432000 solar years.\
\
The number 432000 is not restricted to the पुराणस् as the माध्यन्दिन shatapatha ब्राह्मण mentions it as the mystic number of the aksharas in the veda. It also mentions 10800 (432000/40) as the number of the bricks in the grand platform of the soma sacrifice or the number of mantras in the ऋग्वेद. The ब्राह्मण also mentions the number of stars in dyaus to be 25*432000. The number was also known to the Greeks, as Ptolemy mentions the number 432000 as the totality of the great circle on which he calculated his musical frequencies. Finally it appears much farther afield in the Germanic world in the tale of the end of the world in the Ragnarok. In the final battle between the fiery demons of Surt and the gods lead by Odin, 800 warriors are said to have issued forth from the 540 gates of Odin's palace. Again, the same number (540*800=432000) is seen in the context of the fiery end of the world. The distribution of this motif, like the macranthropic motif suggests that this number emerged in the Indo-European world and was adopted by the Babylonians relatively late rather than what is perceived by the western scholars.

This number obviously comes from the precessional year or the time taken for one cycle of precession to complete= 25920 years. 60 degrees of this precessional circle is covered in 4320 years (*100 gives the महायुग of the hindus). Thus the combination of decimal and hexagismal measure is seen only in the Indo-European world while the Mesopotamians are purely hexagismal (As seen in दीर्घतमस् hymn). Importantly the ब्राह्मण mentions that the sacrificial platform for the great soma sacrifices (महायग्न्यस्) has 10800 लोकम्पृण bricks surrounded by 360 stones symbolizing this link between the solar year and the precessional year.


