
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Enemy strategy on front #1](https://manasataramgini.wordpress.com/2004/12/04/enemy-strategy-on-front-1/){rel="bookmark"} {#enemy-strategy-on-front-1 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 4, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/04/enemy-strategy-on-front-1/ "2:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The enemy forces did not strike on front #1 as we had expected. Our spies inform us that they have decided to instead pause futher hostilities on front #1 and instead resort to the Trojan horse strategy. In our "Gaugamela", which was in a month of blazing heat we had fallen prey to that strategy and lost terribly. We, however, learnt our lesson, even if it came at a high cost. We get the temptation to attack but given our inability to hit the capital we have decided to wait silently.


