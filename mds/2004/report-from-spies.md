
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Report from spies](https://manasataramgini.wordpress.com/2004/12/11/report-from-spies/){rel="bookmark"} {#report-from-spies .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 11, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/11/report-from-spies/ "2:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our सचीव had proposed a long and intricate escape plan that had promising aspects to it. It needed a particular maneuver that was difficult but could be achieved with the help of my greatest ally. I had started considering it seriously as an option in the event we face a defeat on the front #3. However, I suddenly realized that this escape route could very well turn out the route for exit from this earth. I sent my able spy who knew all happenings of those regions to check out. Not un-expectedly, I learnt from his reliable investigations that a scorpion's journey down a solifugid's lair awaited us. The well-know sapatnas, whom we had thrashed in the past, rudhira-dhvaja प्राच्य and tiryagnetra could easily burn us if we took that route. "na mama" I said and scrapped the plan. I issued orders to the सचीव to drop preparations and consider alternatives.


