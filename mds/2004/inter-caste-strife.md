
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Inter-Caste strife](https://manasataramgini.wordpress.com/2004/09/01/inter-caste-strife/){rel="bookmark"} {#inter-caste-strife .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/01/inter-caste-strife/ "5:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The British, at the pinnacle of their rule over the sub-continent succeeded in raising a wide section of Indic public opinion against brahmins. This resulted in several anti-brahmin movements that blamed the brahmins for the ills of various socio-ethnic sections of Indic society. They cited as proof for their grievance the dharma शास्त्रस्, chief amongst which was the मानव dharma शास्त्र. These anti-brahminists claimed that these texts sanctioned the brahmins to ill-treat and oppress their people. These movements were orchestrated and their perceptions abetted by the British, and later by the collective Anglosphere, because they saw it as a potent tool to blunt the traditional Brahminical leadership of Hindu society against foreign invaders. This framework for the interpretation of the ancient Hindu texts was further built up by Anglospheric India-agents ("Indological scholars") and their Marxist fellow travellers who had been planted in India just before freedom. So great has been the influence of these constructs that many Brahmins and other upper caste Indians in a defensive knee-jerk response try to deny the primacy of manu's great work on dharma or concoct peculiar claims about castes and their nature. However, in the midst of all the sound and light created by the internalization of Anglospheric frameworks by Indians for the analysis of Hindu texts, the actual historical analysis of the texts and the nature of conflicts alluded therein , both perceived and real, have been largely ignored. I shall briefly discuss some of these points. But I must state upfront that these are a discussion of hypothesis and I am not necessarily accepting them as the final picture, which would require more investigation.

The first point, which many Hindus like to deny these days, but must be discussed without any mincing of words, is the ethnic structure of Indic society.


 1.  The ब्राह्मणस् (brahmins) and the क्षत्रियस् are in essense the descendents of the aristocracy or elite families of the invading Aryans. Overtime the kShatriya fold adopted other Indo-Aryan tribes outside of the पञ्चजन-इक्ष्वाकु orthodoxy, and various "honorary" क्षत्रियस् of diverse ethnic backgrounds. These newcomers displaced or supplemented the original क्षत्रियस् to a significant degree. However, in the case of the brahmin the displacement was less significant, though there was a noticeable ethnic diversification through the process of anuloma marriage and the 7 generation rule of ascent to brahmin-hood. Nevertheless the brahmins and kshatriyas do seem to maintain a significant component of their ancestral Indo-Aryan ethnicity as supported by recent genetic studies.


 2.  The vaishyas- these were in chief the descendents of the Indo-Aryan laity. However, they eventually included a diverse group of individuals.


 3.  शूद्रस्- these seem to represent a mixture of individuals, namely: 1) those of Indo-Aryan ethnic and linguistic origins, but outside of the pale of the पञ्चजन-इक्ष्वाकु orthodoxy 2) Non-Indo-Aryan peoples of various origins. We shall discuss their case in greater detail below.

An examination of the Hindu texts reveal direct or indirect evidence for some major inter-वर्ण conflicts either in the period of their composition or within their historical memories.


 1.  In the core of the ऋग्वेद the principal conflict is between the Aryas and the dasyus/dasas. We shall not discuss this conflict directly here. The terms dasyu/दास appears to represent the technical term for the foes of the principle protagonists of the ऋग्वेद- the Aryas.


 2.  The post ऋग्वेदिc texts allude to the Arya vs शूद्र conflict and the brahma+क्षत्र vs शूद्र conflicts.


 3.  The मानव dharmaशास्त्र (manu स्मृति) and allied material alludes to the same 2 above-mentioned conflicts.


 4.  The भार्गव mythological and genealogical literature often embedded in the इतिहासस् and to some extant the पुराणस् refer to ब्राह्मण vs kShatriya conflicts. Some versions of this mention the ब्राह्मणस् forming an alliance with vaishyas and शूद्रस् against the क्षत्रियस् in this conflict. It is typically centered around the भृगु epic of their struggle with the haihayas.

We shall first look into 2 and 3 and briefly at 4.

The common understanding amongst modern Indians, and definitely amongst almost all Western India-watchers, is that the term शूद्र in these ancient texts refered to the "low castes". While there is not much doubt that this term was applied at some point, typically, to the unskilled laborers, servants and slaves, there is more to it that is often missed in emotional analysis of the texts.

Firstly, the main "scholarly paradigm (with or without an explicit display of Marxism; eg. DD Kosambhi -- explicit Marxist; M Witzel- not an explicit Marxist but using the same artifices )" is that the शूद्रस् are the lowest caste of society being continually opressed by the ब्राह्मण-s or the brahma-क्षत्र alliance.

Yet it is fairly plain from the manu स्मृति that the शूद्र is feared as a danger to ब्राह्मण-s. The same is true of the vaguer vedic allusions, where they are a threat to the Aryas or the brahma-क्षत्र aristocracy. It should stated here that this clearly contrasts the situation with the vaishyas. They are after all the "vish (people)" or the laity who function as traders and cultivators and should have the resources to for the brahma-क्षत्र or brahmin overlord to exploit. Further, given their resources they could actually mobilize a threat to the dominion of the aristocrats. Yet, there is no sense of hostile interactions vis-a-vis the vaishyas in the texts under discussion. They are discussed, variously, as Arya insiders or miscellaneous dvijas using a neutral or common-place terminology. This presents a major contradiction, for why should the brahma-क्षत्र, who were believed to be in power in Hindu society, fear the lowest group rather than the potential more dangerous vaishyas whom they are believed exploit. This is also in contrast with the generally neutral or positive treatment of the "low caste" trades which may be at the border of vaishya and shudra functions. The texts both vedic and the MDS, consider these trades and their practitioners a necessary feature of society that needs protection. So the sense of threat from the hostile शूद्र suggests that term 'शूद्र' was not merely applied to the lowest caste but actually extended to enemy of the brahmin or the brahma-क्षत्र alliance.

To provide some detail on this from extent text of the मानव dharma शास्त्र:

MDS 10.129 warns that शूद्रस् on acquiring wealth could harass brahmins. Importantly, this is never stated of vaishyas who have the potential to do so.

MDS 10.43-45 explains that the क्षत्रियस् who have sunk to the state of शूद्रस् due to negligence of rites are चीनस्, द्रविडस्, yavanas, shakas, pahlavas and the like. They are also described as being dasyus, irrespective of whether speak Arya or foreign languages. So clearly several foreign tribes, typically hostile, were being identified with शूद्रस् and they were also described with the older term of the foes of the Aryas- the dasyus. MDS 8.22 warns that a kingdom teeming with शूद्र-run courts rapidly degenerates as though over-run by enemies of dvijas. MDS4.60-61 again mentions that the ब्राह्मण should avoid residence in a शूद्र kingdom, which is described as being over-run with inimical people and having practitioners of fake religions.

Thus, the term शूद्र appeared to be used in the context of an hostile individual or individual who actually possessed power to harm the ब्राह्मण or the Arya-s. So who were these hostile शूद्रस्?

Obviously one subset of them included the named foreign tribes, many of whom were known for their invasion of India and specific anti-brahminical activities: for e.g the yavana Alexander the Macedonian's execution of ब्राह्मण during his invasion of the Punjab. The regions occupied by these invaders could be viewed as being ruled by the शूद्र kings who were destructive to the ways of the Aryas. The Persians before them, with their anti-Daiva fanaticism also seem to be candidates for the other inimical class of rulers- the म्लेच्छस्.

P. Olivelle has suggested that the mauryas were the शूद्र rulers who threatened the ब्राह्मण-s of manu. This suggestions seems unlikely due to many grounds. 1) The Greek visitors during the mauryan period, clearly mention that the ब्राह्मण was highly respected in the Indian social setting. 2) There is no evidence from the mauryan inscriptions that they were particularly anti-Brahminical, for after all the support for ब्राह्मण is explicity mentioned in the ashokan inscriptions. 3) Hindu tradition ascribes the rise of mauryans to ब्राह्मण advisers and what ever information we have suggests that the administration was dominated by ब्राह्मण officials.

The same thing cannot be said of the nanda-s. The evidence suggests that they were indeed malignant to ब्राह्मण (caricatured in the चाणक्य tale). This probably lead to the brahmin sponsored mauryan coup against the nandas. The ब्राह्मण rulers of the भरद्वाज family, the शुङ्गस् arose by overthrowing the mauryans when the Greek invasions threatened to penetrate deep into India. But the शुङ्गस्, stemmed this tide by routing the Greek armies and driving them beyond the sindhu. This incident too seems to have left a clear impression of potential shudra and mleccha threats to the Aryavarta-based nationalism of ब्राह्मण (something they had inherited from their old predcessor of the kuru-पञ्चल state).

The redaction of the ancient law-books, which were in their core kuru-पञ्चल documents (as supported by their definition of Aryavarta=the kuru-पञ्चल realm; the territory of the भारतस् or sometime their feudatories and allies, the yadus and matsyas), seems to have occurred during the maurya, शुङ्ग and काण्व periods. These redactions were characterized by inclusion of negative references with respect to shudra, a term which specifically subsumed the recent hostile elements that the brahma-क्षत्र alliance had faced. However, the vedic references suggest that the redactors were not being innovative in this regard. It is clear that there was an ancient hostile sub-text to the शूद्र term, as enemies of the Aryas. This parallels the term दास, that in ancient sanskrit literature was typically an individual or group hostile to the Aryans, but later also came to mean slave. These terminologies definitely have a link to the subjugation of hostile enemies and their eventual assimilation at the lower social rungs.

So in conclusion, I would state that the entire enterprise of searching for a doctrine of oppression in negative statements regarding the शूद्रस् in the MDS and allied texts is a rather misplaced venture. These texts are a product of a certain historical conflicts, where the brahma-क्षत्र alliance is merely seen to be defending its interests against competitors and threats. Whether, their texts or their interpretations based on misunderstood terminologies were basis of social inequities, which are very natural in primate societies, is highly questionable. A dispassionate understanding of these texts as historical documents will definitely benefit the Indians, but it may be too late now.

Finally, I shall briefly consider the brahmin-kShatriya conflict of the भृगु mythological cycles. In their most drastic form the भृगु hero rAma जामद्ग्न्य is described as exterminating 21 generations of क्षत्रियस्. The reason being, the क्षत्रियस् under haihaya, and his successor like कार्तवीर्य were extremely oppressive to brahmins in particular and the world in general. One version of the tale narrated in the महाभारत suggests that the ब्राह्मण-s and the शूद्रस् and vaishyas formed an alliance to defeat the क्षत्रियस् after a long struggle. The references in the atharva veda and the older cycles of the भार्गव myth in the महभारत, suggest that the core of these events concerned a historical struggle between the भृगुस् and the haihaya rulers of माहिष्मति. However, the later पौरणिc redactions of the myth, and late reinsertion in the महाभारत seem to have given it the color of a general struggle between the क्षत्रियस् and brahmins+rest of castes. I believe that this generalization was actually inspired again by a similar constellation of much later historical events that inspired the manu स्मृति's reaction to the shudra threat. These events were most probably the oppressive rule of the nanda-s, colored to certain degree by the atrocities of the foreign rulers (described as shudras by the manu स्मृति) around the same time.

The victory of the ब्राह्मण ruler पुष्यमित्र शुङ्ग, probably inspired a mythological reworking, in which his clan's glorious victories were compared to the historical struggle of परशुराम, and that tale romanticized to its extant mythological form. It is not a matter of coincidence that the politically pre-eminant brahmin clan, the भार्गवस् were involved, both in the generalization of their family epic (the परशुराम epic) and the redaction of the manu स्मृति into its extant form.


