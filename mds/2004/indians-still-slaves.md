
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Indians still slaves?](https://manasataramgini.wordpress.com/2004/06/01/indians-still-slaves/){rel="bookmark"} {#indians-still-slaves .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 1, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/06/01/indians-still-slaves/ "3:45 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many many moons ago the good old Padishaw of Dilli, Akbar, was locked in a mortal combat with the vaishya fighter Hemachandra (Hemu) who was trying to restore Hindu rule over the famed capital of India. Sadly for India, Hemu was wounded in the fight and fell into the hands of Akbar. The young Mohammedan tyrant gleefully stepped forward and slit the the fallen Hindu leader's throat and acquired the title Ghazi- the wager of Jihad on the infidels, in the midst of his admiring henchmen.

Fast forward many years: apparently we are no longer ruled by Islamic tyrants from Dilli. Is this true?

We recently heard in the news that a bunch of Ghazis ran amok in Khobar, Saudi Arabia claiming a number of Dhimmis and Kaffrs in their rampage. Most Western news outlets told us that a few whitemen had been sent to their graves by the Ghazis- of course Kaffrs are Kaffrs even for other Dhimmis. We later heard from Indian sources that the bulk of the victims of were Kaffrs from Hindustan.\
So what does our government say in response:

**"The Indian casualties were victims of the circumstances rather than pre-meditated targets for attack."**

Wonderful- circumstance indeed. Did these idiots not know that it is fair game for a Mohammedan to attain Ghuzzwat by slitting the throats of Kaffrs? It shows how much the Hindu still fears the Mohammedan and does not even dare to call a spade a spade. A terrorist is a terrorist- and a Mohammedan adhering closely to his book is very likely to be one. You do not talk of terrorism in terms of "circumstance". When you see it you call it that. After all who is there to mourn the Hindu victims of Mohammedan terror. The White Dhimmi will mourn his people, and will not even acknowledge the existance of other Kaffrs. The Indian government will say it was "circumstance".

It makes us ask the simple question : "Has Mohammedan rule on the Indians really ended?"


