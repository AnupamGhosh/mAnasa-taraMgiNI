
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The scooter circuit](https://manasataramgini.wordpress.com/2004/12/25/the-scooter-circuit/){rel="bookmark"} {#the-scooter-circuit .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 25, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/25/the-scooter-circuit/ "7:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We performed our peculiar scooter circuit that year. Earlier it was the cycle circuit. We were like the पाशुपत, distanced from the plebian world. We we passed in the midst of the plebians, we heard their chatter, we saw them indulge in the pleasures of the world and suffer from the pains of the world. They reflected in us, like a scenery in a mirror. We moved right in their middle but they stuck not to us like water on a oiled surface. There were fumes around us, but we did not breathe them. Some performing हुड्डुकार, other अट्टहास, and yet others jumbaka नृत्य, came to us but we remained entirely silent. We existed on crags of the "far away land". There we roamed like an all-knowing gandharva in the constant embrace of our dazzling अप्सरा. We spoke endlessly with our अप्सरा on the Topics. We only stopped thrice a day for food and drink. Rest of the time it was the non-stop discourse. We slept in the highest realm of pleasure. We knew that we knew all. A stone hit the lake surface. Ripples permeated through it. Our knowing slipped through the troughs in the waves.

Our strange friend (should we call him that? may be he is actually an enemy?) धर्मदृढ वातव्यादि keeps wanting to talk to us. Why does he want to do that? We get no pleasure from talking to him; we send him away. I do not see the ripples in his lake any more. They were once there but are not seen anymore. Not being a gandharva, but firm in the middle course, he is always in the embrace of a मानवी. Still he is perpetually restless, always asking something from us, and always peddling his half-baked thoughts with a compulsive appetite. May be there is a lesson to be learnt from the strange case of dharma दृढ वातव्यादि?


