
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Rules for ritual eligibility](https://manasataramgini.wordpress.com/2004/12/11/rules-for-ritual-eligibility/){rel="bookmark"} {#rules-for-ritual-eligibility .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 11, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/11/rules-for-ritual-eligibility/ "7:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There is considerable debate about who is eligible for vedic rites. The shrauta सूत्रस्, which are the authorities on the topic are quite clear about the matter, barring a few debatable points. कात्यायन expounds these matters most clearly:\
\
1.1.1 अथातो.अधिकारः\
\
Now we shall explain the eligibility for performing vedic rites.\
\
1.1.2 falyuktAni कर्माणि\
\
The rites are connected with specific results.\
\
1.1.3 सर्वेषांविशेषात्\
\
Prima facie everyone is entitled vedic rites as no specification has been made in this regard.\
\
1.1.4 मनुष्याणां वा .अरम्भसामर्थ्यात्\
\
By this it is meant that only humans who are capable of such actions are eligible.\
\
1.1.5 अङ्गहीना .ashrotriya-षण्ड-shudra-वर्जं\
\
Individuals with defective bodies, untrained in the vedas, hermaphrodites and shudras are excluded. \[For shudra exceptions see below]\
\
1.1.6 ब्राह्मण-राजन्य-वैश्यानां श्रुतेः\
\
The brahmins, क्षत्रियस् and vaishyas are known to be eligible.\
\
1.1.7 स्त्री chA .अविशेषात्\
\
As there is no specification otherwise women are also eligible.\
\
1.1.8 दर्शनाच्च\
\
The above is true as it is seen in actual practice. The commentator cites TS 6.1.3 as the authority for this clause: "मेखलया यजमानं दीक्षयति योक्त्रेण पत्नीं"\
\
1.1.9 रथकारस्या .अधाने\
\
A person of the chariot maker class may set up the sacrificial fires for rituals [अग्नेयधान]\
\
1.1.10 नियतं cha\
\
His [रथकार's] eligibility also extends to obligatory vedic rites\
\
1.1.11 nA .अभावादिति वात्स्यः\
\
The \[shukla yajurvedic authority] वात्स्य is of the opinion that his eligibility does not extend to other vedic rites.\
\
1.1.12 निषादस्थपतिर्-गावेधुके .अधिकृतः\
\
The niShada chieftain is eligible to a vedic ritual with the offerings of गावेधुक [[Coix barbata]{style="font-style:italic;"}] (usually to the deity rudra).\
\
1.1.13 वाव्.अकीर्णिनो गर्दभेज्या\
\
The optional ass sacrifice may be offered by the fallen student who has had sex.\
\
1.1.14 laukike\
\
The niShada sthapati and the fallen student set up their sacrificial fires in an ordinary way (that is non-cermonial)\
\
The other sacrificers use 3 cermonially set up shrauta fires and the औपासन fire for domestic worship.

Two shrauta sutras proscribe anyone other than a brahmin from being the ऋत्विक् at a sacrifice:\
\
कात्यायन 1.2.8: ब्राह्मणा ऋत्विजो भक्षप्रतिषेधादितरयोः /\
\
\[Of the 3 allowed वर्णस् only] the brahmins can act as officiating priests as the other two are forbidden from eating the sacrificial \[remnants]. The commentators cite the authority of the shatapatha ब्राह्मण of the shukla yajurveda to support this point.\
\
SB 2.3.1.39: tad vai na अब्राह्मणः pibet (non brahmins do not drink \[the remnants of sacrificial beverages])\
\
Apastamba 24.1.21: ब्राह्मणानां आर्त्विज्यं\
\
The ऋत्विक्स् duty is that of the brahmins.

However other sutras do not have any prohibitory rules.


