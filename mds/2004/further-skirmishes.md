
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Further skirmishes](https://manasataramgini.wordpress.com/2004/11/27/further-skirmishes/){rel="bookmark"} {#further-skirmishes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 27, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/27/further-skirmishes/ "5:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We tested out a few maneuvers directly in enemy territory and we found things held pretty well. We also spied a long but interesting attack route that would carry us straight to the ground of the initial encounter. We realized that this attack route would save us a lot on investing in the baggage train. While there is one outstanding issue, we were fairly satisfied with it and will investigate it further. We also tested out some other attack routes and have a reasonable picture of how the battle may go. We still are bothered by our main weakness, but there is not much we can do about it.


