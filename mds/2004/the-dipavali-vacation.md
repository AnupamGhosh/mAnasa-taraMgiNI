
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The दीपावलि vacation](https://manasataramgini.wordpress.com/2004/02/02/the-dipavali-vacation/){rel="bookmark"} {#the-दपवल-vacation .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 2, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/02/02/the-dipavali-vacation/ "1:05 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Never before had I descended to such a low, especially given all the show I had made. It was like Tendulkar scoring two zeroes in a row. All around us were atirathis and महारथिस् (mark that word for it is indeed a pun) but still I was a महारथि too, who had taken on the intense heat of battle. Pricked quick by the defeats I had faced, but confident about my abilities, I made a charge, even as Prataprao had done in the fateful battle of Ahmednagar, and blew away our rival महारथिस् in a battle that knew no bounds in its fury. After launching myself into this fiery battle and emerging victorious, I suddenly felt refreshed at the prospect of enjoying a long दीपावळी vacation. We met in shady glade at the appointed afternoon hour and proceeded to rove the sylvan slopes. First there were the 3 bubbles of 65 million year old lava. There the sacrificial cock had been butchered and its feathers billowed around. Then came the arm chair of the demons; it was wrought in solid basalt. Then came flat plateau where the motion of वात surrounded his precocious son. We then descended on the other side where the tower arose beside the great basaltic depression of the snake. We saw some rival dasyus, I defensively shielded my companion and showed them a sign to get lost. We knew we were in control. Then we traversed the extremely long path to its end. We peered down the second basaltic depression. We saw the turtle again. She said "Batagur baska- We beheld it twice; you will experience it at least once more in life. Mark those words and think of me when they return to your mind many years later. We shall make a profound telepathic connection long after the basaltic depressions would have dried. Remember the Oviraptor prancing through the Gobi wilderness. "The flash comes home"


