
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [indivara and वारवन्तिका](https://manasataramgini.wordpress.com/2004/08/25/indivara-and-varavantika/){rel="bookmark"} {#indivara-and-वरवनतक .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 25, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/25/indivara-and-varavantika/ "6:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**"Those who want to know the real story will know it."**

Beyond the world of the Falcon, stationed in eternal time, were the great atharvans. udvanta आथर्वण amongst them held a soma cup. He descended to the sattra of the 3 seers. He asked प्रेणिन् the son of सोमाहित, madhucchandas the वैश्वामित्र and asita the kAshyapa to make their wishes. प्रेणिन् wished that he should have the best goat and that which ever woman he calls unto him should desire him.

....\
\
प्रेणिन् सौमाहित got his wish but could not look at the soma cup.\
\
....

प्रेणिन्'s spell that issued from him fell in the village girt by the high mountains. Amongst the mountains was a resting place where वणिजस् and ग्रामण्यस् rested and drank surA, राघखान्डव, maireya and other durmadas. इन्दीवर one day, after a long journey, in course of which he was nearly eaten by a tiger, arrived at the rest house. He scrupulously avoided the mada, the matsya and the मांस that was being plied and fed himself strictly on food that was fit for a ब्राह्मण. When प्रेणिन् had died the deadly spell, charged with the might of the आथर्वण's boon, had issued from him and entered a stone placed on his स्मशान. इन्दीवर taking his food, sat on a tripod to consume it, some distance from the स्मशान. वरावन्तिका, the great grand-daughter of उपमा was passing by. She looked just like her great-grandmother in her prime. She happened to step on the स्मशान, which was concealed under moss. A gandharva emerged from it an entered her, even as the gandharva that entered the daughter पतञ्जल काप्य in the land of the mAdras. She called out to इन्दीवर as she proceded to obtain a leather bladder filled with the maireya brew. वारवन्तिका stated that she had not forgotten इन्दीवर for the rites he had performed for her and thanked him for that. इन्दीवर asked her if need any further rites to be done. She claimed that she was capable of everything with her own rituals. But at some point she said that she was not sure if they helped her to cross the great mystery depicted by the code: Da kshaa Da ha ha Ta 654123 इन्दीवर excitedly said he had the means to cross that. Just then kaumbhya and shammad arrived and asked वारवन्तिका to share her bladder of durmada with them. इन्दीवर had earlier cast a deadly spell on the false-mouthed shammad, which had burned down his house and cattle-shed. So इन्दीवर withdrew wanting to avoid any further run in shammad. वारवनित्का too knowing of shammad's odd ways avoided him and moved away with kaumbhya and they were soon joined by gara and गान्दम with whom they savored the durmada from the bladder, until late at night. इन्दीवर proceeded with his wanderings and after visiting the groves of The God returned to rest-house the village. After finishing his lunch there he was about to retire for some rest before going to examine the booty that gara and गान्दम had brought in their car. Just then, he caught sight of the beautiful वारवन्तिका pass by. For some he asked her if she would like to know the secret of Da kshaa Da ha ha Ta 654123.\
\
वारवन्तिका: "There are two [shiro-veSTa](http://www.uni-koeln.de/cgi-bin/SFgate?language=english&verbose=0&listenv=DL&application=%2fphil-fak%2findologie%2ftamil%2fmwd&convert=Tabelle&waisurl=%2Fvol%2Finfo%2Fwais%2Fdb%2Ftamil2%2Fmwd/HTML/92/1=local;2=/vol/info/wais/db/tamil2/mwd;3=20237782%2020237874%20/vol/info/wais/data/.wais-data/tamil2/mwd.txt;4=local;5=/vol/info/wais/db/tamil2/mwd;6=20237782%2020237874%20/vol/info/wais/data/.wais-data/tamil2/mwd.txt;7=%00;)s; one is placed after Ta, after 1, after that comes a ka; the second is placed before the last Da Da क्षा क्षा Ta."\
\
इन्दीवर: "I know how one can trace the thorny path to the shiro-veSTas and the and the anya vastras to see the great rahasya of वारवन्तिका's अस्त्यआणु-bhuk.\
\
वारवन्तिका: "इन्दीवर, I verily know that you are the one who saw the rahasya of these अस्त्यआणु-bhuks when I was pounding grain for gara and गान्दम. It was because of this rahasya that I got back the wealth of उपमा and regained my regal status."\
\
इन्दीवर: "I can take you to the deepest rahasyas of this world of the अस्त्यआणु-bhuks."\
\
वारवन्तिका: "Meet me tonight at the room near the madhu शाला. Let us talk about this"\
\
इन्दीवर: "Sure"

इन्दीवर then went to look at the booty of gara and गान्दम. इन्दीवर found their collection ordinary, because he had seen and possessed all that before. But two kept claiming that they were the only ones who possessed them and that the king of the gandharas was always in praise of them for their wonderful possessions. इन्दीवर rued the fact that the chief of स्थूलार्म hardly bothered about the same things that he used to show him from time to time. After chatting with gara and गान्दम, इन्दीवर proceeded to have dinner and then walked silent to the room near the madhu शाला to meet with वारवन्तिका. After a while वारवन्तिका arrived and kaumbhya, gara, गान्दम, gojihva, उक्षमुख, बब्लुजङ्घ, कपोतग्रीव, अजदाढिक and बीडालस्मश्रु followed her thickly trying to get a glimpse or a word of attention from her. She fooled them by saying that she would shortly join them with a bladder of kautsasya surA and slipped into the nearby room.


