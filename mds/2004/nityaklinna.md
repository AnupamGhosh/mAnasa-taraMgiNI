
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [nityaklinna](https://manasataramgini.wordpress.com/2004/07/31/nityaklinna/){rel="bookmark"} {#nityaklinna .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 31, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/31/nityaklinna/ "6:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[**"Burg Niedeck ist im Elsass der Sage wohlbekannt. Die Höhe, wo vorzeiten die Burg der Riesen stand."-Adelbert von Chamisso**]{style="color:#ff0000;"}\
\
I stood before the great vimalashambhu. The अमृत flowed from the heaven cup of soma and drenched the great नित्यक्लिन्ना on the 4th point of the yoni triangle. Raimented in the red, her limbs were smeared with red unguent, she held a skull bowl filled with red wine. Her face was full of smiles and restless with desire. The power of nityaklinna briefly filled me and I coursed over the रसार्णव like boat gliding over water. It was that mixture of all the emotions that welled up in me at once. The sensations were of every possible description. And at the end of it I had transcended them.\
\
I stood apart watching swirling mass of रसार्णव, which was fading away as the giants from the German mind.


