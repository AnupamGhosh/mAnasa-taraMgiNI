
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Islamic vandalism in Gujarat](https://manasataramgini.wordpress.com/2004/07/11/islamic-vandalism-in-gujarat/){rel="bookmark"} {#islamic-vandalism-in-gujarat .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 11, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/11/islamic-vandalism-in-gujarat/ "6:29 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Rudramahalaya temple complex in Siddhapura in Gujarat was one of the greatest temples ever built in India north of Maharashtra. Its is supposed to have housed 11 separate temples of rudra and many more of other gods. One of these housed a sixteen-armed, 5 headed image of shiva with his right foot upraised and placed on a lotus. A गण with a sword is shown below the lotus. Another is supposed to have contained a 4-armed naTarAja. Another had a लिञ्ग while and yet another had a उमा-maheshvara on a bull. The complex also contained shrines of: इन्द्रनी with 4 arms in seated ललितासन; गणpati with 4 arms; kubera with his wife, holding a purse; 8 armed चामुण्डा;  4 armed कुमार, temples of वायु, वरुण and yama, each depicted with 4 arms; and one of विष्णु near a river.

Clearly this complex was one of the most magnificant pieces of चालुक्य architecture and built by सिद्धराज जयसिंह and कुमारपाल. It was considered one of the holiest shrines of Hindu pilgrimage and by all descriptions it appears to have been a marvel of Indian art. Importantly it shows that even in Northern India there were shrines with depicitions similar to the southern ones (eg. naTarAja, लिञ्ग with nandi, कुमार and the like, were not exclusive to the south). Its glory is borne out in a testimony of a twelth century poem:

[*Fourteen storeys rise above the earth and seven thousand pillars,  In row after row, while eighteen hundred statues studded with emeralds adorn it.* ]{style="color:#00ffff;"}\
[*It is endowed with thirty thousand flagstaffs with stems carved and leaves of gold.* ]{style="color:#00ffff;"}\
[*Seven thousand sculptured elephants and horses stand in attendance on Rudra.* ]{style="color:#00ffff;"}\
[*Seeing it all, Gods and men get struck with wonder and are greatly charmed, ज़यसिंह has built a temple which excites the envy of emperors.* ]{style="color:#00ffff;"}\
[*The sculpted elephants and lions trumpet and roar, all around, again and again, the golden kalashas glitter on the मण्डप upheld by numerous pillars.* ]{style="color:#00ffff;"}\
[*The icons sing and dance and roll their eyes, So that even the Gods jump with joy and blow their conches.* ]{style="color:#00ffff;"}\
[*The ecstatic dance of Gods is watched by Gods and men who crowd around,That is why the Bull, O Sidha! O King of Kings! is feeling awestruck.* ]{style="color:#00ffff;"}

Even if it was hyperbole, clearly the glory was imprinted on the onlookers.\
What happened to this great shrine? How did it vanish from the Hindu mind?

Alla-ud-din Khalji sent Ulugh Khan to sack Gujarat. Siddhapura was raided. His successor Ahmed Shaw destroyed the temple complex entirely as is narrated in a poem composed in his honor by Sikandar ibn-i- Muhammad:

*[Ahmed marched under divine inspiration, ]{style="color:#ff6600;"}*\
*[For the destruction of temples at Saiyidpur, ]{style="color:#ff6600;"}*\
*[Which was a home of the infidels, ]{style="color:#ff6600;"}*\
*[And the native place of accursed fire-worshippers.]{style="color:#ff6600;"}*\
*[There they dwelt, day and night, ]{style="color:#ff6600;"}*\
*[The thread-wearing idolaters.]{style="color:#ff6600;"}*\
*[It had always remained a place for idols and idol-worshippers, ]{style="color:#ff6600;"}*\
*[It had received no injury whatsoever from any quarter. ]{style="color:#ff6600;"}*\
*[It was a populous place, well-known in the world, ]{style="color:#ff6600;"}*\
*[This native place of the accursed infidels. ]{style="color:#ff6600;"}*\
*[Its foundations were laid firmly in stone, ]{style="color:#ff6600;"}*\
*[It was decorated with designs as if drawn from high heaven. ]{style="color:#ff6600;"}*\
*[It had doors made of sandal and scented wood. ]{style="color:#ff6600;"}*\
*[It was studded with rings of gold, ]{style="color:#ff6600;"}*\
*[Its floors were laid with marble, ]{style="color:#ff6600;"}*\
*[Which shone like mirrors. incense was burnt in it like fuel, ]{style="color:#ff6600;"}*\
*[Candles of camphor in large numbers were lighted in it. ]{style="color:#ff6600;"}*\
*[It had arches in every corner, ]{style="color:#ff6600;"}*\
*[And every arch had golden chandeliers hanging in it. ]{style="color:#ff6600;"}*\
*[There were idols of silver set up inside, ]{style="color:#ff6600;"}*\
*[Which put to shame the idols of China and Khotan. ]{style="color:#ff6600;"}*\
*[Such was this famous ancient temple, ]{style="color:#ff6600;"}*\
*[It was famous all over the world. ]{style="color:#ff6600;"}*\
*[By the effort of Ahmad, it was freed from the idols, ]{style="color:#ff6600;"}*\
*[The hearts of idol-worshippers were shattered with grief. ]{style="color:#ff6600;"}*\
*[He got mosques constructed, and minars placed in them, ]{style="color:#ff6600;"}*\
*[From where the Law of Shariah came into force. ]{style="color:#ff6600;"}*\
*[In place of idols, idol-makers and idol-worshippers, ]{style="color:#ff6600;"}*\
*[Imaams and callers to prayers and khateebs were appointed. ]{style="color:#ff6600;"}*\
*[Ahmad's good grace rendered such help, ]{style="color:#ff6600;"}*

*[That an idol-house became an abode of Allah. ]{style="color:#ff6600;"}*

The description of its opulence, even by the worst enemies of the Hindu fold, suggests that Rudramahalaya was truely one of the greatest Indian temples whose existence has been well forgotten, and with it the religion has also been denuded entirely in those regions. Gujarat is still to come out of that eclipse. This is just one instance in the history of Bharatavarsha that the secularists would like to hide from its people's vision.


