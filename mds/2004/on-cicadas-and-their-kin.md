
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [On cicadas and their kin](https://manasataramgini.wordpress.com/2004/05/22/on-cicadas-and-their-kin/){rel="bookmark"} {#on-cicadas-and-their-kin .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 22, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/05/22/on-cicadas-and-their-kin/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Those were days when I was the local expert on hoppers and to lesser extant on Hemipterans in general. They are all around and we had our favorite haunts to pursue these insects. The lake-side-haunt and the hill-of-ill-luck were the best locale to get these insects.\
Some of the more unusual ones were the rare Dictyopharids: *Dictyophara* had a long nose and was usually found on grass the other was *Orthopagus lunulifer* with a small and pudgy nose. The classic cicadellid hoppers have a row of spine running down the powerful hindleg that is used in jumping. Then there were the cercopidae which have a rim of spine around the end of tibial segment of the hindleg. Then there are the membracids with a sharp backward pointed notum that shields a part of the abdomen.

The two really pretty ones that we liked a lot were the cicadellids *Tylozygus bifidus* which has a bright striped pattern and *Erythroneura vitis* which has a brightly colored wings. *Mileewa dorsimaculata* is a beauty with blue wings and a transparent patch on the forewing. We first saw it in a peculiar patch of compositan plants in full bloom. *Cicadella viridis* is the good old green guy who always visits the lamps in the evenings: some believe that might have been the famed indragopaka of the old Aryans. This was the first of the cicadellids I noticed in my life and got hooked to them. I conducted an anatomical study of the same with my first microscope. Two years later I introduced R to it and got her hooked to them too. It is a sight that always delights me when in Bharata. More beautiful but less frequent was the cicadellid *Bothrogonia ferruginea* with yellow wings having a black patch at their extremities. The completely yellow *Nirvana pallida* was also an occasional vistor at the household lamps.

The really dirty guys were the spittle bugs (cercopids): For long, when R and I used to explore insects in our earliest forays we actually mistook them for human spit. One day we were puzzled by its occurrence in considerable abundance in a rather isolated spot rarely visited by humans. We knew it had to be of non-human origin and began exploring it rather carefully, and soon rediscovered the spittle bug, which is the cercopid larva. The most common spittle bug in our regions was the brownish *Eoscarta assimilis*.

Of the membracids the most fascinating was the two horned *Butragulus flavipes* that was abundant on the Zizyphus trees, where ants farmed them like cows for their exudate. Similar to Butragulus but darker and with more prominent horns was *Centrotus nitobei*. Then there was the pudgy *Gargara katoi* which was like a brown dumpling on the same tree. During the adventure of Naga Nayaka's skull we saw the dramatic flatid: *Mimophantia maritima* of creamish white color and a fluffy coating.\
The ricanids tend to imitate small moths. Once, R called me over to see what she described as "a small fly imitating a moth". On careful examination I realized that it was my first sighting of a ricanid of the Fulguromorpha lineage of Hemipterans: the steel grey *Pochazia albomaculata*.

It was during one such exploration that R told me about the long-lived giants of the cicadamorph world- the classic cicadas. I had longed to see them since then. I first got see the regular cicads *Tibcen* and *Oncotympana* that I duly dissected. But after all these days in mleccha-द्वीप I finally got to see the periodic *Magicicada septendecim* and *Magicicada cassini* the great periodic cicada. They burst like tidal wave at the appointed time and reached the peak even as the day of yama dawned. The whole area was reverberating with their songs. The song of cassini has a remarkable shrill and strident rasp while that of septendecim comprises of a hollower whoop. Both sing the chorus as well as the specific oscillating courting call as they approach their females. They are immense in numbers and when walks into their midst the orchestra of is awe-inspiring. Some are being infected by the deadly fungus *Massospora cicadina* that sterilizes them. The emergence of the periodic cicadas has been a truly dramatic event.


