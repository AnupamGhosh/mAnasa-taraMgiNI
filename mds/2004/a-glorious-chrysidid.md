
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A glorious chrysidid](https://manasataramgini.wordpress.com/2004/07/17/a-glorious-chrysidid/){rel="bookmark"} {#a-glorious-chrysidid .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 17, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/17/a-glorious-chrysidid/ "5:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Chrysidid wasp Chrysura refulgens on a grass shoot\
\
 [![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="absMiddle" border="0" style="border-right:0;border-top:0;background:none transparent scroll repeat 0 0;border-left:0;border-bottom:0;padding:0;"}](http://www.hello.com/){target="ext"}\
```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/320/chrysidid.jpg){width="75%"}
```{=latex}
\end{center}
```



