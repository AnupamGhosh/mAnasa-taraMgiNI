
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Shambhaji's inaugural campaign](https://manasataramgini.wordpress.com/2004/10/22/shambhajis-inaugural-campaign/){rel="bookmark"} {#shambhajis-inaugural-campaign .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 22, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/10/22/shambhajis-inaugural-campaign/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

After Shambhaji had crowned himself Chatrapati, in Oct 1680 on थेVइजय Dashmi day decided to inaugurate his campaigns with strike against the Mogols. He set off with 3 divisions of the Maharatta cavalry: One division moved to attack the Mogol garrison at Surat via the parallel to coast northwards along the Konkans. The second division of Maharatta horse invaded Khandesh to strike a Mogol outpost there. Simultaneously a 3rd division moved to attempt a liberation of the Mogol outpost of Aurangabad in Maharashtra. Awrangzeeb dispatched Khan-i-Jahan Bahadhur Khan with the central Mogol army to counter the Maharastras. Shambhaji led his division as though to attack Berar and then suddenly swerved and attacked Burhanpur in Khandesh through a very rapid forced march. The Maharattas destroyed the Mogol garrisons and captured the vasty booty of the Mogol camps in Burhanpur and Hasanpur. The Maharattas demolished the Masjids and in both the places and put the terrible Mussalmans invaders to sword. Khan-i-Jahan tried to retrieve the cities but he was beaten by the Maharatta army and they fell back to defend from the fort of Salhir. Raj Singh of Chittor at that time destroyed a Mogol army in the battle of Aravalli (a very less-described battle, that needs investigation!). Akbar II the son of Awarangzeeb went over to the Rajputs. But Awrangzeeb through intrigue had him chased by his main army from Delhi. He fled and took refuge with Shambhaji. This immediately shook the Padishaw and he declared that he would lead the Jihad in Deccan personally.


