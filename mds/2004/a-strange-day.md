
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A strange day](https://manasataramgini.wordpress.com/2004/08/02/a-strange-day/){rel="bookmark"} {#a-strange-day .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 2, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/02/a-strange-day/ "6:13 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It had been pouring heavily. Normally on such a day I would have not liked to step out of the warmth of the house and drive through the slush and the dampness of the Hindustani monsoon. But that was a strange day. There was this sense of adventure that seized me and I got on to my aging black "horse" and drove on till I reached the bike stand. Having parked and not minding the oppression caused by indra's bounty I head straight to the classroom. R was tensely looking out from the window and even as I got in, I was followed by the droning botany lecturer. This damped all the built up excitment and we had to put up with an hour of the botany buzz with a straight face. It often puzzled me how my studious deluded classmates so thoroughly enjoyed these interminable sessions with the relish similar to what one might get while enjoying a ripe mango or a jackfruit.

In any case we slipped out as soon as the droning ended and decided to make way before the organic chemistry and other lectures of the afternoon broke out upon us. R and I ran to the bike stand jumped onto our "horses" and rapidly drove away towards the K dam. Having reached the K dam we took a look at the great river being swelled by the torrents of maghavan. All of a sudden the showers stopped and the solar rays appeared out of the clearing. We started ascending the steep slopes with our horses' engines droning like the botany lecturer. I had never had the courage to take on these slopes in the monsoon. But somehow R had this wierd courage that probably arose from ignorance. I was forced to look manly in situations where my courage would otherwise failed me. One slip and we may have become guests to विवस्वान्'s son. But the beauty of the view was breath-taking. Many years later recently I was in similar situation all for a breath-taking 360 degree view but had thrown caution to winds. But this time I was all by myself with no human in sight for miles on end. At the moment of that view one feels the oneness with [पिता द्यौः]{style="color:#ffcccc;"}above and [माता पृथिवि]{style="color:#ffcccc;"} below, standing upon the pinnacle in the mid-region. And the wilderness with the stalking pashupati is all around you.

Both those days were strange days because I also saw what it meant to come close to vaivasvata.


