
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Towards the battle of काल-निर्णय](https://manasataramgini.wordpress.com/2004/12/08/towards-the-battle-of-kala-nirnaya/){rel="bookmark"} {#towards-the-battle-of-कल-नरणय .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 8, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/08/towards-the-battle-of-kala-nirnaya/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Our spies provided us with speculative intelligence that the hostile forces may be preparing a trap for us. However, we were not able to confirm it based on other reports. The [princess of Hayastan]{style="color:rgb(51, 204, 0);font-weight:bold;"}, with calves like the quivers of काम, gave us one of those Mona Lisa smiles and drove away on her ratha. As ever, if one meets her the right way one is transported to the shores of the ocean of bliss. She expressed deep Schadenfreude that R had been plunged into her current condition. She laughed like चामुण्डा as she said: "Imagine if R were not reduced to certifying pesticides, we might have had nothing left to do." But she added: " I would be happy if my daughter turned out be half as her." I told she was not the first or even the second who had wished secretly or even openly that R plunge into shit, very literally, like the धर्मशास्त्रस् pronounce for certain unfortunate souls.

#Flashback#

She then seemed to console me over my current state. Our primary allies asked to hold a major sabha. Several suggestions flew, though none seemed meaningful. Finally, I told the sabha where exactly we stood as per my suggestion. There were some expressions of despair and anger. One of our primary courtiers spoke the great humiliations our allies had faced at the hands of our foes. Why were the gods not favoring us? Had [indra ]{style="font-weight:bold;color:rgb(255, 102, 102);"}gone over to the druhyus forsaking the पूरुस्? This courtier suggested a complicated plan for a battle. I rapidly thought through the plan and saw that it was infeasible, though if events went badly in the immediate future we (especially, if the speculative intelligence were true) a version of it could be used for the worst case scenario. Our अमात्य then spoke and said we need to act decisively in a matter of days, which ever way the battle went. Our अमात्य proposed a plan that seemed reasonable. We decided to consider it seriously.

I then lead a solo expedition and sighted my old foe, Pande of the broken bottle, and near him my accquaintence the trustworthy kAyastha from लक्ष्मणपुर. Both were now omens. I realized that there was a glimmer of hope. Both smiled and put on airs of friendship- one false and the other real. But they also reminded me of the mistakes that had weakened me on front #2.

The hostile force suddenly made a peculiar move- They rapidly projected a wedge of troops on front #1. The picket was too small to really compete against our firepower. We decided not respond and remained firmly behind fort walls. But we saw certain signs, a decisive encouter is on the cards. Our ever-optimistic अमात्य agreed.


