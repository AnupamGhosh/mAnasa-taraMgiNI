
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Recombination of meters](https://manasataramgini.wordpress.com/2004/12/11/recombination-of-meters/){rel="bookmark"} {#recombination-of-meters .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 11, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/11/recombination-of-meters/ "7:17 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A technique of Chandas transformation is the circular permutation, which is used in the [षोडशिन्]{style="font-weight:bold;color:#ff9966;"} soma pouring. Here offerings of soma are made to indra with 5 specialized अनुष्टुभ् mantras called indra जुषस्व. They are derived from ऋक्स् originally in the atharva veda in the rare and peculiar स्वराज् meter (shaunaka AV 2.5.1-3). The स्वराज् has a total of 34 syllables in a mantra. The total of 5 स्वराज् mantras are derived by repeating the first ऋक् 3 time, as is typical of a shastra recitation. Thus we get 34*5=170 syllables.

[१) इन्द्र जुषस्व प्र वहा याहि शूर हरिभ्यां / पिबा सुतस्य मतेर् इह मधोश् चकानश् चारुर् मदाय //]{style="color:#0000ff;"}

15:19 (*3 times)

[२) इन्द्र जटरं नव्यो न पृणस्व मधोर् दिवो न / अस्य सुतस्य स्[उ]वर्ण्[अ][उ]ओप त्वा मदाः सुवाचो अगुः //]{style="color:#0000ff;"}

16:18

[३) इन्द्रस् तुराषाण् मित्रो वृत्रं यो जघान यतिर् न / बिभेद वलं भृगुर् न ससहे शत्रून् मदे सोमस्य //]{style="color:#0000ff;"}

16:18

From these 5*32=160 syllables are taken to make 5 अनुष्टुभ्स्

It is done in the following manner: the स्वराज्स् of 34 syllables are

split up as below

32+2

30+4

28+6

26+8

24+10

then the following combinations are made: to make the new अनुष्टुभ्

mantras:

32 : 2+30 : 4 +28 : 6+26 : 8+24 : --->10

That residue of 10 syllables is then used up as preceeding chant in the second part of the षोडशि pouring.

The recombination of gAyatri and pankti is described in the aitareya ब्राह्मण 4.1.3 as a part of the chant in the part 2 of the [षोडशिन्]{style="font-weight:bold;color:#ff6666;"}rite, generally performed before the अतिरात्र. The first part contains the indra जुषस्व permutation (the स्वराज् mantras of च्यवानो भार्गव) that I described earlier. The below technique is the celebrated [विहरणं]{style="font-weight:bold;color:#66ff99;"}or the intertwining of meters.

The gayatri of मेधातिथि ख़ाण्व RV 1.16.1 :

[आ त्वा वहन्तु हरयो । वृषणं सोमपीतये । / इन्द्र त्वा सूरcअक्षसः । //]{style="color:#0000ff;"}

24= 3*8 as 8+8 / 8 // (padas marked as .) mantra=1

The pankti of gotamo राहूगण 1.84.10 :

[स्वादोर् इत्था विषूवतो । मध्वः पिबन्ति गौर्यः । /]{style="color:#0000ff;"}\
[या इन्द्रेण सयावरीर् । वृष्णा मदन्ति शोभसे । वस्वीर् अनु स्वराज्यम् । //]{style="color:#0000ff;"}

40=5*8 as 8+8 / 8+8+8 // (padas marked as .) mantra=2

pankti+gayatri=64=2*32= 2*अनुष्टुभ् as 2* ( 8+8 / 8+8 //)

simple व्युढ generation of 2 अनुष्टुभ्स् from above:

[आ त्वा वहन्तु हरयस् (१) । स्वादोर् इत्था विषूवतः (२) । /]{style="color:#0000ff;"}\
[वृषणं सोमपीतये (१) । मध्वः पिबन्ति गौर्यॊ३(2) . //]{style="color:#ffff33;"}

(new अनुष्टुभ् 1)

[इन्द्र त्वा सूरcअक्षसो (१) । या इन्द्रेण सयावरीर् (२) । /]{style="color:#0000ff;"}\
[वृष्णा मदन्ति शोभसे । वस्वीर् अनु स्वराज्यॊ३ । (२)//]{style="color:#ffff66;"}

(new अनुष्टुभ् 2)

Note the endings of the new verses in the shastra:

gauryO3 ; स्वराज्yO3 are sung out with an "ooo" in a musical tone for

the purpose of the शास्त्र.


