
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Vedic Beer](https://manasataramgini.wordpress.com/2004/03/22/vedic-beer/){rel="bookmark"} {#vedic-beer .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 22, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/03/22/vedic-beer/ "3:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Whereas beer and other alcoholic beverages were not consumed by the ब्राह्मण-s (and ascetics) unlike rest of Arya society, its production was still a part of ब्राह्मण lore. The hindus of yore had an elaborate ritual of beer making connected with the exhalted rite called the सौत्रामणी. The adhvaryu priest bought the following items through barter: grass blades in exchange for a piece of zinc. Barley grains for goat wool, parched rice in exchange for yarn. Each bargain the priest uttered the formula "सुरासोमविक्रयिन् krayyaste सुरासोम". Then these materials are carried to the प्रचीन वंश altar and prepares fungal powders known as नग्नहू. He then boils the श्यामाक beans and rice and collects the supernatants to which he adds the नग्नहू powder and then mixes the two and keeps them away for a while. This preparation is called मासर. Then he mixes the मासर with the boiled grains of rice and श्यामाकस् in a large vessel called the सुरपात्र chanting the mantras स्वाद्वीं... He deposits this vessel in a pit at the नैऋत corner of the प्राचिन वंश altar for 3 nights. Then a cow is freshly milked with the mantra अशिविभ्यां अपाकरोमि and the milk is poured into the ferment with the mantra परीतो सिञ्चता... He then shreds the rice leaves into fine pieces and adds them to the ferment and lets is stay for a night. The next morning he milks two cows with the mantra 'sarasvatyai अपाकरोमि...' and adds the milk to the ferment. He also adds the powder of fresh barley grains and lets it stand for a night. The next day he adds the milk of 3 cows into the ferment and adds the flour of parched grains into it. This is let to ferment for 4 days.

He then goes to the southern altar and pours the ferment through a ox hide funnel through a hoof-sieve with the mantra "सिञ्चति pari सिञ्चति.." to get a preliminary liquor. This liquor is further purified by straining through ox and horse hair filters in a pot made of पलास wood by reciting the mantra "पुनाति te परिस्रुतं..". This liquor is termed the surA. At this point if a यजमान or his patni are morbidly reacting to the soma drink by vomitting or flux they are given the surA as a remedy. indra, ashvins and sarasvati are offered several cups of the beer, milk and soma in the sacrifice that follows. The ब्राह्मणस् performing the rite reverse the direction of their yaGNYOpavItas and drink what is left behing in one of the cups or they merely smell the liquor, collect the remnants and offer it to a kshatriya or a vaishya to drink along with a gift. If they do not get a drinker, they may smell it and then burn all the liquor in charcoal pyres outside the ritual area with mantras to offer it to the ancestors.


