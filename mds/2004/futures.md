
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Futures](https://manasataramgini.wordpress.com/2004/02/14/futures/){rel="bookmark"} {#futures .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 14, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/02/14/futures/ "1:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We wandered on the path by day and decided that we should wander in the pitch darkness of night. We set out slowly wending our way through the paths. We agreed that either of us would not have done on our own. midway there was the four-cross. We sat on the stones there in the middle. The black figure of Dollolas suddenly emerged from the thickets. I see the past and future with my disc, said the foremost of the निशादस्. Why are you here? we asked. The followers of The God shall show me the future and the past, right here he said. We asked what does the mist of the future hold for us. Ask separately he said. I asked first.\
\
He said:\
\
The day will come.\
\
What is a dream is a dream.\
\
It does not intersect with the world of men\
\
and will not in your case.\
\
"Is that true?" I asked.\
\
He said:\
\
Truth to be told it shall intersect,\
\
but then you would have transcended some of those desires by the time it intersects.\
\
"How does it feel" I asked.\
\
She said:\
\
When I ask what do you want, you will say "Ice cream"\
\
When you will have a sea of it, it will not mean much.\
\
Thus, when you want me you shall not have me.\
\
She asked: What shall be my fate.\
\
He said:\
\
Five are the arrows of The God.\
\
You shall be hit by one of them.\
\
It would look as though you would die.\
\
Then you shall attain the keys of immortality.\
\
But if he dies, you shall die too and those keys will be useless.

"Would I ever find the way out?", I asked\
\
He said:\
\
In the ब्रह्माण्ड is said that there are 3 प्रळयस्. The काम प्रळय shall over take you. If the कल्पान्त strikes you before the काम प्रळय ends then you shall be destroyed.

We parted ways and kept walking.

मृत्यवे स्वाहा | मृत्यवे स्वाहा ||


