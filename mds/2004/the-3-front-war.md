
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The 3 front war](https://manasataramgini.wordpress.com/2004/12/01/the-3-front-war/){rel="bookmark"} {#the-3-front-war .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 1, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/01/the-3-front-war/ "4:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We completed the fortifications on various crags and they stood reasonably. Shortly after we had done so the hostile forces opened a massive assault on front #3. Our fortifications fended the attack, but were severely tested. We then strengthened some further outposts, and called off all diplomats. Then, tired as never before, we retired to rest. After that we went to inspect one of the fortifications, when a massive hostile thrust occurred on front #1. A gaping hole was seen in the fortification that simply could not be repaired. We had always known our weakness on front #1 and yet again it was starkly haunting us. We felt like Maharana Pratap facing the Mogol artillery. Even as we were taking stock of the situation, a massive thrust was launched on front #3. We have now decided to take the extreme step of flooding the moat in an attempt to save front #3. Off to the front.


