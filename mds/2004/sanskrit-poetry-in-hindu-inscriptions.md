
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Sanskrit poetry in Hindu inscriptions](https://manasataramgini.wordpress.com/2004/05/23/sanskrit-poetry-in-hindu-inscriptions/){rel="bookmark"} {#sanskrit-poetry-in-hindu-inscriptions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 23, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/05/23/sanskrit-poetry-in-hindu-inscriptions/ "6:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Dr. BC Chabra provides several examples of exemplary Sanskrit poetry in Hindu inscriptions. I shall provide a few of these as illustrations:

[श्री चन्द्रगुप्तस्य महेन्द्र कल्पः कुमारगुप्तस्तनयः समग्रां ।]{style="color:#99cc00;"}\
[ररक्ष साध्वीम् इव धर्मपत्नीं वीर्याग्रहस्तैर् उपगुह्य भूमिं ॥]{style="color:#99cc00;"}

[श्री चन्द्रगुप्तस्य महेन्द्र कल्पः कुमारगुप्तस्तनयः समग्राम् |]{style="color:#99cc00;"}\
[ररक्ष साध्वीम् इव धर्मपत्नीं वीर्याग्रहस्तैर् उपगुह्य भूमिं ||]{style="color:#99cc00;"}

The verse is in the त्रिष्टुभ् meter with 44 syllables 4*11.

"कुमारगुप्त the son of the illustrious chandragupta, endowed like the great indra, protected the land even as his wedded wife embraced in his valiant hands."

upaguhya -- embracing (could also mean hiding/covering/spanning)

[कुर्वन्तु कीर्तन शतानि रणाङ्गणेषु मथ्नन्तु वैरिनिकरं धनम् उत्सृजन्तु ।]{style="color:#99cc00;"}\
[कालान्तरे तद् अखिलं प्रबलान्धकार नृत्योपमं कविजनैर् अनिबध्यमानम् ॥]{style="color:#99cc00;"}

[कुर्वन्तु कीर्तन शतानि रणाङ्गणेषु मथ्नन्तु वैरिनिकरं धनम् उत्सृजन्तु ।\
कालान्तरे तद् अखिलं प्रबलान्धकार नृत्योपमं कविजनैर् अनिबध्यमानम् ॥]{style="color:#99cc00;"}

They may perform hundreds of famous acts, on the battle-fields they may churn the enemy masses, they may donate wealth,\
but with the passage of time all these deeds will be like a dance performed in overpowering darkness;\
unless they are limitlessly praised by the poet-folks.

This is from the Koni (near Khutaghat Dam, Ratanpur, Chhattisgarh) stone inscription from circa 1148-49 CE during the reign of king पृथिविदेव of the ratnapura kalachuri clan. The author काशल was the quintessence of the erudite kShatriya before the horror years of Indian history. He declares himself as being a warrior, poet, veterinary physician of elephants and philosopher. Ironically nothing is known of him but for this inscription- in a sense he was a poet immortalizing himself.

The meter here is an unusual meter of classical Sanskrit called the vasantatilaka meter, which is typified by 14+14+14+14 syllabic pattern in the verse.

रणाञ्गणेषु=on the battle fields; mathnantu= to churn \[imperative 3rd person plural]; vairi-निकरं=enemy masses. This is a common usage sanskrit accounts of battle, and is also encountered in tamil. It is good example of how a pan-Indian phraseology that spread to local languages in the classical period.

dhanam उत्सृजन्तु: This phrase may have a double-meaning on purpose- 1) meaning those who become famous by donating wealth or 2) those who become famous by renouncing their wealth. उत्सृजति may be interpreted as renouncing wealth.


