
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [भास्कर and differential calculus](https://manasataramgini.wordpress.com/2004/09/05/bhaskara-and-differential-calculus/){rel="bookmark"} {#भसकर-and-differential-calculus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 5, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/05/bhaskara-and-differential-calculus/ "6:22 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is fair to state that भास्कर provides a clear description of the concept of the differential. This can be seen in his treatise siddhAnta शिरोमणि (chapter गणिताध्याय; section गतिस्फुटिप्रकरण). He clearly states that this is a formalization of the approximate formulae given by the earlier आचार्य brahmagupta.

He defines two quantities: 1) sthulagati (roughly speed) that is defines as ratio of the distance travelled by a object to the time taken for that.\


 2.  सुक्ष्मगति or तत्कालिकी gati. This the one used to define velocity of an object. It defined as the value obtained by taking the time duration to be an infinitesmal, which is given the technical term प्रतिक्शणं. This is evident from the formula he gives for the calculation of the sine tables because he is assuming the तत्कालिकी gati of sin(x) to be cos(x)


