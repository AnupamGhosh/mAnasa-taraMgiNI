
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Enemies and their ilk](https://manasataramgini.wordpress.com/2004/08/12/enemies-and-their-ilk/){rel="bookmark"} {#enemies-and-their-ilk .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 12, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/12/enemies-and-their-ilk/ "5:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

संस्कृत terms for enemies have some interesting social connotations. Below we discuss some of these angles

sapatna- a rival or an enemy. In a strict usage related to rivals for the same woman or competing for lordship over the same object. The adjectival derivative सापत्न could also mean children sharing only one common parent and thereby potential rivals due to lower gene-sharing that real sibs.

भ्रातृव्य- a rival. Bears a connection with the word भ्रात, meaning brother. Members of the same brotherhood may have intense competition for lebensraum. Eg. duryodhana and युद्धिष्ठिर

ari- a rival. Could also mean, in-laws or the exogamous community with which one maintains marital ties. They are the "in-group" rivals. Thus the collection of ari-s into which one marries are the Arya (the ethnonym Arya).

पृतना- a hostile agent or army. It could also mean a body of men. Such a body of men could be potential threat to others.

<div>

</div>

<div>

dasa/dasyu- An out group enemy- those who are not Arya. dasa may also mean slave ( the fate of the defeated foe) or in some Indo-European languages men/people.

</div>

amitra- Not a friend.

shatru- enemy, plain and simple.

vairi- enemy.

द्विष- a hater.


