
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The obscene formulae in the ashvamedha](https://manasataramgini.wordpress.com/2004/11/21/the-obscene-formulae-in-the-ashvamedha/){rel="bookmark"} {#the-obscene-formulae-in-the-ashvamedha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 21, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/21/the-obscene-formulae-in-the-ashvamedha/ "7:06 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The male participants (the priests and the bard) say:\
[अम्बे अम्बाल्यम्बिके न मा यभति कश्चन । स सस्त्यश्वकः । हये च हये असौ ॥]{style="color:#99cc00;"}

Ladies (ambA, अम्बालि and अम्बिका) no one seems to have sex with me. The bad horse is sleeping. That is the horse and you are the mare princess.

The princess says the following अनुष्टुभ् (one of the dialogs ):\
[इयं यका शकुन्तिका ऽहलमिति सर्पति । आहतं गभे पसु नि जल्गुलीति धाणिका ॥]{style="color:#99cc00;"}

Who is this female chick who moves in un-ploughed \[Footnote 1]? The phallus penetrates the vulva and her pudendum muliebre jostles about !

\~ओ^ऊ\~

Footnote 1: un-ploughed is a euphemism for a virgin, i.e. one who has not yet been copulated with. For example the name अहल्या mean one who was not ploughed until indra gives her the experience of sexual pleasure. Hence she is listed among the कन्या-s rather than among the sati-s (i.e. those who have only one male).


