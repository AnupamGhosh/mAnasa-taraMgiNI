
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The ghost again](https://manasataramgini.wordpress.com/2004/12/29/the-ghost-again/){rel="bookmark"} {#the-ghost-again .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 29, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/29/the-ghost-again/ "7:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It was disastrous day. Unfazed by the disasters we were returning. We paused on our path for we distinctly saw some strange dark object like a coat lying on our a path a few feet away. I reached the object somehow drawn by curiousity to examine it. Just before I reached it I was distracted for a moment as I looked to the side due to a scrapping noise. I turned back and the black object was gone! Where it lay I say some ungainly stains like smeared flesh or blood of a dead animal. I heard the scrapping noise again and there was just no sign of the black object that I had clearly seen.

ST had wished for me : "You will go up the mountain. On your way back as you are rushing back you will get stuck and will not be able to come down."


