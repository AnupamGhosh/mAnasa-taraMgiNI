
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Wasps by the iron fort](https://manasataramgini.wordpress.com/2004/07/15/wasps-by-the-iron-fort/){rel="bookmark"} {#wasps-by-the-iron-fort .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 15, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/15/wasps-by-the-iron-fort/ "5:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the early 90s we decided to scale to the great cliff of Lohagad where the founder of the Maharatta nation had hidden his loot after his ferocious raid on Surat. The fort had remained largely impregnable till 1818 when the Britons of cursed memory took it by bombing it from the adjacent citadel of Visapur. The old fortications on the Lohagad spur appear to have been built by the Andhras with subsequent embellishements by the Maharattas forming the most prominent part. At its foot we saw a derilict Shiva temple that had been devasted by the Moslem hordes during the great 26 year Jihad in the Deccan. The settings were at their verdant best on the day close to the solistice when we visited the site.

Our main interest was to observe and document two remarkable groups of wasps- the cuckoo wasps or the Chrysidids and the Cynipids or the Gall wasps. Relatively frequent were the brilliant blue wasps of the genus Chrysis coerulans. These were kleptoparasites that works on the hornet wasp. R brought my attention to a common wasp of the hornet variety flying in with a sedated caterpiller from somewhere. It then stashed the caterpiller along with others in its nest and a flew of to get another. The hornet lays its egg in the caterpiller and its larva eats the sedated caterpiller as it matures. I then saw the chrysidid enter the nest of the hornet and kill the larva of the hornet and replace it with its own, much like the cuckoo parasitizing nests of other birds.

The gall wasp was commonly seen on the [Zizyphus ]{style="font-style:italic;"}and some other plants. The wasp's salivary secretions cause the plant to undergo uncontrolled proliferation forming a gall in which the eggs of the wasp are layed. The gall provides shelter and nutrition for the larve. We observed that on the same tree the leaves and stem had two different kinds of wasps. It was here that we realized they were actually the same wasp that went through 2 alternating cycles with different anatomies and different type of galls. The exact significance of this alternating cycle of two different morphologies in these gall wasps is somewhat unclear, though it may be a seasonal adaptation.

Finally, we also saw the Pompilid wasps in action. The black pompilid walks restless on the ground tapping around with its antennae. As soon as it encounters a spider it pounces on it and stings it, sedating it right away. The it stuffs the spider in a nest and lays an egg on it. Its larva eats its away through the spider as it matures. The most common victims are the Thomisidae spiders. We rarely saw Salticid spiders being taken.


