
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The bleak winter evening](https://manasataramgini.wordpress.com/2004/12/04/the-bleak-winter-evening/){rel="bookmark"} {#the-bleak-winter-evening .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 4, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/04/the-bleak-winter-evening/ "10:07 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I was standing alone in the railway station in the bleak winter air. There was not a single sound and the air hung eerily as though weighed down by the cold. Suddenly the air quivered with the fluttering of a pigeon's wings as it made its roostward retreat and then there was silence again- the shakuna! The sun sunk on the pashchima mayukha, leaving behind the gloaming and the azure depths of dyaus turned inky dark. There was a sharp cry of the wind, reminiscent of the howl of The god (sa देवः). The shadows of the stones in the cemetery beyond grew rapidly even as the the last beams of the dusk retreated. They seemed to transform into the agents of the lord of the animals.[ "oM yo vai रुद्रः sa भगवान् yashcha मृत्युस् tasmai vai namo नमः"]{style="color:rgb(255, 102, 0);font-weight:bold;"}[ ]{style="color:rgb(255, 102, 0);"}

The roar of a ratha broke my reverie. Hayastanika called out to me to board the ratha. The irony that surrounds every encounter with the राष्ट्री suddenly struck me. "That flow of karma cannot be countered. Even the fierce विष्णु had to assume the दशाकृतिः." Hayastanika of fervid energy and deed, declared that she needed to imbibe a चषक of माद्वि पान before doing anything further. I dreaded her driving the ratha after the वारुणि got to her, but acquiesed silently and proceeded. As ever she was unfazed by the pot of वारुणि and instead seemed as though she drunk the soma that her very ancient ancestors may have once imbibed on the steppes of Azerbaijan. Soon we were speeding to the dwellings of R and Marc with the great bull and the mothers of skanda rising beyond the पूर्व मयूख. There there was throng of people in festive celebration. A lot of clatter of vessels and tongues filled the air in contrast to the dim silence of the station. For a while we returned to the favorite object of our chatter: each of our exploits in chemistry lab. In this respect we seemed have had remarkable coincidences at the same times in our lives- throwing that block of sodium into water on the rainy day; making picric acid or silver acetylides etc. With the kids engrossed in their play, and the more prying adults captivated by the discussion on the vicissitudes of their fiduciary games, we retreated to the basement.

Hayastanika suggested that we ply the planchette. So we were at it shortly thereafter. I asked a particularly appropriate question. The coin seemed to stop after spelling out l-o-m. The left eyelid throbbed. I wonder what that means?


