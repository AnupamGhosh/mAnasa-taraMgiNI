
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Divergence in the constraining motifs of the gAyatri: काण्वस् and others](https://manasataramgini.wordpress.com/2004/09/26/divergence-in-the-constraining-motifs-of-the-gayatri-kanvas-and-others/){rel="bookmark"} {#divergence-in-the-constraining-motifs-of-the-gayatri-कणवस-and-others .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 26, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/26/divergence-in-the-constraining-motifs-of-the-gayatri-kanvas-and-others/ "5:11 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vedic gAyatrI meter is constructed as per the following rules:\
\
#1 It shall ideally have 24 syllables in two hemistiches\
\
#2 hemistich 1 shall have 16 syllables and hemistich 2 shall have 8 syllables.\
\
#3 Thus there shall be 3 पादस् each of 8 syllables divded into 4 syllable units on which the further rules shall act.

The following are defined as light or short syllables denoted by U\
\
a; i; u; ऋ\
\
these vowels make a syllable short.

The following are defined as heavy or long syllables denoted by --\
\
A; I; U; e; ai; o; au; ॠ\
\
vowels make a syllable heavy or long.

The combination with a consonant (eg. ag in agni) makes a light vowel heavy or long.\
\
A position which is either short or long is denote by .

#4 The ideal structure of each पाद of the gayatrI is then given as (syllables 1 -8; each 4 syllable unit divided by '/' ):\
\
[.][-\>U][.][-\>U]/ \[U][-\>U]\[U][.]\


  - \>U means long is preferred over short or vice-versa.\
\
#5 If in the open set of 4 syllables of the second syllable is chosen as the less-preferred U then the 3rd syllable is preferred to be long.

Another frequent but not universal feature is that the 4 syllable halves of each पाद split a word in the 2nd and 3rd पाद of the gAyatrI.

To illustrate these rules in action:\
\
agnim ईळे / पुरोहितं / : यग़्ण्य़ास्य de/vam ऋत्विजम्/ :: होतारं rat/नधातमं ::\


  - U--/U-UU = 4+4 =पाद 1\


  - -U-/U-UU=पाद 2\


  - ---/U-UU=पाद 3

अग्निः पूर्वे/भिः ऋषिभिः/ : ईडियो nU/तनैः uta / :: sa देवां e/ha वक्षति/ ::\


  - U--/UUUU\


  - U--/U-UU\
\
U---/U-UU

agnim दूतं/ वृणीमहे /: होतारं vish/ववेदसं :: asya यग़्ण्य़स्/ya सुक्रतुं ::\


  - U-U/U-U-\


  - -U-/U-UU\


  - UU-/U-U-

This is the most frequent gAyatri used by the vipras of most clans. However, the काण्वस् tend to have an additional consistent usage from time to time giving a peculiar gAyatrI structure of their own.

This is given by the rule (for each 8- syllabled पाद):\
\
[.][-\>U][.][-] : [-\>U]\[U>-][-][.]

Note the difference in the second 4-syllabled unit. This is illustrated by:\
\
उक्थं cana/ शस्यमानं /: agor arír/ A ciketa/ :: na गायत्रं/ गीयमानं/ ::\


  - UUU/-U-U-\


  - --/-U-UU\


  - -U/-U-U

वयं u त्वा/ तदिदर्थाः /: indra त्वायन्/तः सखायः/:: कण्वा ukthe/bhir jarante/::\
\
UU--/UU--\


  - ---/UU-U\


  - ---/-U--

This variant of the gAyatri interestingly occurs elsewhere in the 3*8 syllabled meter of the **avesta**. While this suggests a potential connection between the काण्वस् and the Iranians of the avesta it is not immediately clear as to which form was the primitive gAyatri. The more frequent classical gAyatrI form is likely to be primitive as it is also seen in the underlying motif of syllables used in the non-sholka type अनुष्टुभ्स् of the ऋग्वेद. Thus, it is possible that the काण्व-type gAyatri that follows the pattern of the European trochaic meterical foot (as against the equivalence to the iambic foot of the classic gAyatri) in the closing 4 syllables is a derivation that arose in the milieu shared with the Iranians. These forms may also have relationship with the construction of specialized गायत्र गानंस् which are alluded to by the काण्वस् in their hymns (गायत्रं गीयमानं).


