
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rape of अरजा](https://manasataramgini.wordpress.com/2004/04/01/the-rape-of-araja/){rel="bookmark"} {#the-rape-of-अरज .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 1, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/04/01/the-rape-of-araja/ "1:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the वम्शावलिस् of the founding fathers of my many branched clan there are many tales of confrontation with the राजन्यस्. While the conflict between कार्तवीर्यार्जुन and rAma is well known, the much earlier showdown in the early career of उशना काव्य is hardly well known. The king daNDa of the line of the इक्श्वाकुस् was a powerful king who ruled over the Aryan realms based in his capital, madhumanta. He was a contemporary of the great luminary of the भार्गवस्, उशना काव्य, the descendent of भृगु. काव्य's first daughter was the very beautiful अरजा. In the intoxicating month of Chaitra, the king daNDa paid a visit to the Ashrama of the भार्गवस्. There he saw अरजा ranging the forest with carefree abandon. Her sight greatly aroused the king daNDa and with palpitating heart he ran to her. He asked her " O beauty with wonderous hips, breasts and face, who are you? I am afflicted severely by काम's darts and desire you desperately." She courteously replied: "I am अरजा भार्गवि, the daughter of the mighty atharvan, उशना काव्य. Do not make the mistake of trying to force your self on a girl of the atharvan lineage. If you come on me against my wishes, remember that frightful consequences would envelop you without much doubt. The fury of the भार्गव can be immense." Holding his hands in a namaskar over his head, daNDa said: "Oh अरजा of beautiful breasts and loins I am just unable to control myself; my breathing is being arrested in excitement. Please come to me, I just need you." अरजा said: "king your uncontrollable passion cannot be vented, I have already warned of the consequences." danDa said: "I am a mighty kshatriya and my sons and army stands behind me, what can your father, a mere ऋशि do to me." So saying rushed forward at and अरजा pinning her down and disrobing , raped her. Having enjoyed himself thoroughly in the process he returned to madhumanta.

अरजा crying and shaking in fright and dishonor waited for her father to return. Hungry after a long journey the great physician returned home and saw his daughter in a pitiable state, stained with smears of semen. Having figured out what had happened, without pausing to eat he prepared for a deadly Atharvanic rite. He prepared samids of the कण्टक and kaTuKa wood five finger lengths in size and dipped them in castor oil. He then cast these in to the गार्हपत्य fire with the formula beginning thus:

"1) Ours is what is conquered, ours what has shot up, ours the law, ours the brilliancy, ours the brahman, ours the skies, ours progeny, ours heroes. From that we exclude him yonder: daNDa of the इक्श्वाकु lineage, son of the kosalya, who is yonder; let him not be released from the fatal fetter of the atharvans. Of him I take away the splendor, the brilliancy, breath and life; now I place him in the lap of perdition.


 2.  Having pierced him thrice and thrice over, and having crushed his head and having pierced his heart thrice over I place on the lap of perdition.


 3.  I invoke the प्रैष of the devas against him, his sons and his supporters. I place him on the tusks of the god वैश्वानर. To that वैश्वानर Hail. Oh my great god indra, you who had borne my grandmother aid, when she borne away by the rakshas, shower the rain of burning particles for a 100 yojanas all around. To that indra Hail "

The great indra, who succours his praisers, duely sent down that hail of fiery particles on the kingdom of daNDa for seven continuous days. daNDa who was making merry in his park found his skin burnt away in this blazing shower; his palace, his chariots, and sons and henchmen were all buried in the smouldering ruins known as the वैलस्थान.


