
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [maitreya's atomism](https://manasataramgini.wordpress.com/2004/09/18/maitreyas-atomism/){rel="bookmark"} {#maitreyas-atomism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 18, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/18/maitreyas-atomism/ "5:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The bhagavata पुराण preserves the 5 अनुष्टुभ्स् composed by ग्लाव maitreya, the उद्गाता of the kurus, when he was asked about the atomic doctrine by vidura वैचित्रवीर्य. It contains a clear exposition of the atomic doctrine. It is interesting that these openly वैशेषिक mantras are preserved along side the more theistic formulation of typical of the सांख्य associated with पाञ्चरत्र. It may also suggest the source for the elements of atomism in vaiShNava tantras like the लक्ष्मी तन्त्रं.

मैत्रेयोवाच :\
cअरमः sad-विशेषाणाम् aneko .असंयुतः सदा |\
परमाणुः sa विज्ञेयो नृणाम् aikya bhramo यतः || 3.11.1

pada:\
मैत्रेयः | उवाच |\
चरमः | sat | विशेषाणाम् | अनेकः | असंयुतः | सदा | parama-अणुः | saH |\
विज्ञेयः | नृणाम् | aikya | भ्रमः | यतः

maitreya said:\
The ultimate reality is know to be made of numerous particles, which are always elemental, called परमाणु and it is an error of men to consider it seen \[existance] as continuous

sata eva पदार्थस्य स्वरूपावस्थितस्य yat |\
कैवल्यं parama-महान् अविशेषो निरन्तरः | |3.11.2

pada:\
सतः | eva | pada-arthasya | स्वरूप| avasthitasya | yat | kaivalyam | parama | महान् | अविशेषः | निरन्तरः |

These particles indeed comprise all substances, but retain their structure, even if the substance transforms, these are the eternally unchanging, original, supreme, indivisible \[existence].

एवं kAlo .apy अनुमितः सौक्ष्म्ये sthaulye ca sattama |\
संस्थान-भुक्त्या भगवान् avyakto vyakta-bhug विभुः || 3.11.3

pada:\
evam | कालः | api | अनुमितः | सौक्ष्म्ये | sthaulye | ca | sattama | संस्थान | भुक्त्या | भगवान् | अव्यक्तः | vyakta-bhuk | विभुः

O great \[vidura], in the same way time also has its discrete divisions, which comprise its gross form and this can be measured by the movement and combination of particles; विष्णु is that which is unmanifest, existing in movement and in potential.

sa कालः परमाणुर् vai yo भुञ्क्ते परमाणुताम् |\
sato .अविशेष-bhug yas tu sa कालः paramo महान् || 3.11.4

pada:\
saH | कालः | parama-अणुः | vai | yaH | भुञ्क्ते | parama-अणुताम् | सतः | अविशेष-bhuk | yaH | tu | saH | कालः | परमः | महान्

Those discrete units of time, which are verily further indivisible, correspond to the time required by a परमाणु to cover the space equivalent to a परमाणु; this is verily the primal, supreme time.

pada:\
अणुर् dvau परमाणू स्यात् त्रसरेणुस् त्रयः स्म्ऱ्‌तः |\
जालार्क-rashmy-अवगतः kham एवानुपतन् अगात् || 3.11.5

अणुः | dvau | parama-अणु | स्यात् | त्रसरेणुः | त्रयः | स्म्ऱ्‌तः | जाल | arka | rashmi | अवगतः | kham | eva | anupatan | अगात्

Two परमाणुस् are combine to form an अणु, and 3 अणुs combine to form a त्रसरेणु*; the rays of light emerging from a mesh can make these [त्रसरेणुस्] move up in empty space.

\*This idea follows the nyAya pattern of अक्शपाद as opposed to that provided by कणाद.


