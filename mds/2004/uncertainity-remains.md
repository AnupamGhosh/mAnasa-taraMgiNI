
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Uncertainity remains](https://manasataramgini.wordpress.com/2004/12/06/uncertainity-remains/){rel="bookmark"} {#uncertainity-remains .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 6, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/06/uncertainity-remains/ "6:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Seeing that our foes had not fired the expected salvos, we were growing curious like the legendary cat. There was a heated discussion amongst our staff whether to counter-attack or lie and wait. While we had gained an advantage on front #3, a counter-attack could prove costly, as our resources had run out. Towards night I could not bear the wait and ordered a cautious counter-attack. The hostile forces behaved unusually. At first they put up a strong fight and then retreated into inaccessible terrain. We stopped pursuing at the point, for even a victory could prove empty and costly. It is possible they are hiding something and have a plan to strike us unexpectedly. Today again we sent a reconnaisance force to probe their actions. They retreated quickly after a brief encounter. We have now decided to wait and watch though we are not certain of their ploys in battle.


