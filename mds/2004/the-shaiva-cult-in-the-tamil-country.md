
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The shaiva cult in the tamil country](https://manasataramgini.wordpress.com/2004/05/19/the-shaiva-cult-in-the-tamil-country/){rel="bookmark"} {#the-shaiva-cult-in-the-tamil-country .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 19, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/05/19/the-shaiva-cult-in-the-tamil-country/ "9:30 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The rise of shaiva thought in the draviDa country was a milestone in the incorporation of motifs of Sankrit thought from North India. The Dravidian works of shaiva literature closely parallel their development in North Indian Sanskrit literature pointing to a pan-Indian explosion of Hindu thought that gradually displaced the competing heterodox ideas of the Jaina and Bauddha stream from the collective folk conciousness. Unfortunately, the Tamil shaiva and vaishNava literature is not studied as an integral companion to their Sanskrit precursors and counterparts because of the political perversion of Dravidian nationalism and Tamil chauvinism in the past century. The importance of the Dravidian literature is underscored by the fact that it represents one of the few ancient traditions in a regional langugage that has scope and bulk comparable to the Sanskrit tradition.\
\
The classical tamil shaiva canon was made by nambi आण्डार् nambi in the 900s of the common era. The earliest work in the tamil shaiva canon is that of कार्रैक्काल् ammaiar from the 500s of the common era. Ammaiar, a woman poet, had a vision of the great dance of महादेव in तिरुवालन्गादु composed poems in the honor of rudra. Here poems mark the earliest specimens of the prabandha style in tamil. The next great shaiva poet was the tamil war lord ऐयडिगळ् kADavarkOn, who handed his holdings to his son and lived the life of a shaiva ascetic. He wrote the work क्शेत्रत्तिरुवेण्बा, an अन्तादि, where the last syllable of a verse begins the next one. He describes 21 great shaiva shrines such as महाकालेश्वर of ujjain. Then came the era of appar with his 307 padigams or songs to rudra and the brahmin ज्ञान sambandhar who composed 384 hymns. sambandhar and appar were great critics of jaina and bauddha and played an active role in the repulsion of these sects.

Then comes the heart of the tamil shaiva canon in the form of the tirumantiram of 3000 verses-A tamil rendering of the mantra शास्त्र of tantric shiva worship of the sanskrit canon. It draws its authority from many sanskrit tantras later surviving only in the Kashmirian school. It is said that its author तिरुमूलर् was a tantric adept from the Himalayas who migrated to Tamil Nadu to see agastya. At तिरुवाडुतुरै he saw a dead shepherd and his sheep in disarray. So he entered the body of the shepherd and having revived him restored his herd to his family. Then the hyperbolic tamil legend informs us that he meditated under a tree for 3000 years creating the text at one verse per year.

Then we have the last three great shaiva composers headed by सुन्दरमूर्ति, the brahmin. He composed the तिरुत्तोण्डत्तोगै at तिरुवालूर्, in which he lists the 60 great tamil devotees of shiva, the नायनार्स्. Including the parents of सुन्दरमूर्ति and himself the number is traditionally claimed by the tamils to be 63. A friend of सुन्दरमूर्ति was the chera chief, पेरुमाल्. When he and सुन्दरमूर्ति had gone to a pilgrimage to Manasarovar, the चेरमान् invented a novel poetic style starting with the poem on तिरुकैलास ज्ञान ulA inspired by the sight of mount Kailasa. Last in this group was the famed brahmin माणिक्कवाशगर्, whose tirukkovai and तिरुवाशगम् and highly rated by the द्रविडस् and brahmins.


