
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Skaldskaparmal](https://manasataramgini.wordpress.com/2004/06/22/skaldskaparmal/){rel="bookmark"} {#skaldskaparmal .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 22, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/06/22/skaldskaparmal/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

How shall the great one be referred to?\
\
He is father of the gods Magni and Modi and of goddess Thrud. He is the wielder of the Miollnir and Sif's man and Iord's son.

Eilif said:\
\
Roskva's brother[0] stood enraged, Magni's father struck a victorious blow. Neither Thor nor Thialfi's powerstone[1] shakes with terror.

Eistein Valdason said:\
\
Thrud's father looked with piercing eyes on the steep-way's ring [2], until the red-fish's dwelling [3] surged over the boat.

Bragi chanted:\
\
Well you have, split apart the Thrivaldi's 9 heads, held back your steeds with with the notorious giant-feast drinking Thrym [4]

Vetrlidi chanted:\
\
You broke Leikn's bones,\
\
you pounded Thrivaldi,\
\
you cast down Starkad,\
\
You stood over the dead Gialp.

... to that great one we pour an offering of mead

Thorbjorn Disarskald chanted:\
\
Your hammer clang on Keila's crown,\
\
you demolished Kiallandi completely,\
\
before that you slaughtered Lut and Leidi,\
\
you made Buseyra bleed,\
\
you halted Hengiankiapta,\
\
Hyrrokkin died previously at your hands,\
\
yet was the dark Svivor's life taken earlier [5].

... to that great one we pour an offering of mead

...........\
\
0-The demon Thialfi\
\
1-heart\
\
2-The midgard snake.\
\
3-sea\
\
4-the thunder bolt\
\
5- Names of demons and demonesses who fell to Thor.


