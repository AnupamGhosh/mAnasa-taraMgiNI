
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The construction of सामन्स्](https://manasataramgini.wordpress.com/2004/08/06/the-construction-of-samans/){rel="bookmark"} {#the-construction-of-समनस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 6, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/06/the-construction-of-samans/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[The gAnaM rules are thus constructed. First we define elements corresponding to the padas of the ऋक्, here a,b,c,d : 4*11 in the त्रिष्टुभ् example we are considering.]{style="font-family:courier new;"}

a=brahma जज्ञानं प्रथमं पुरस्ताद्\
\
b=vi सीमतः surucho vena आवः\
\
c=sa बुध्निया उपमा asya विष्ठाः\
\
d=satash cha योनिं asatash cha विवः

Then the musical स्तोभास् are defined:\
\
A= huve hA yi\
\
B= हेषाया\
\
C= au ho वा\
\
D= e ऋतं अमृतं\
\
W=A+A+B

Then the gAnaM is specified as :\
\
W+a [ b c d+W+C D+D+D\
\
1 2 3 4 5\
\
The parts of the gAnaM labeled 1-5 are refered to in the ब्राह्मणस् of the साम veda by the technical terms:\
\
1 प्रस्ताव\
\
2 उद्गीथ\
\
3 प्रतिहार\
\
4 upadrava\
\
5 nidhana\
\
This is one of the simplest gAnaM constructs.]{style="font-family:courier new;"}

This general rule may be construct other त्रैष्टुभ गानंस्, though different stobhas may be defined.

The सत्यं class of chants- the song of the inviolable satya upheld by deva mitra is defined thus:\
\
a=satya\
\
E= ho yi\
\
F= hA A vu वा\
\
G= e suvar ज्योतिः\
\
p=the प्रणव, oM\
\
Ganam:\
\
a.p a+E+a+E+a+F G\
\
a.p represent the insertion of the प्रणव into satya to give सत्यों.

satya may be replaced by puruSha, agna, वाय, sUrya, chandra, नाका, shukra, to give rise through the same above formula to the 8 "space filling chants". These represent the equivalence between the elements of space and the sacrificial altar. The adhvaryu lays the bricks with the yajur brick chants. Even as he does so the उद्गाता establishes with these सामन्स् the equivalence with the inviolable satya, the puruSha that pervades, heat (agna), atmosphere (वाय), the sun (sUrya), moon (chandra), नाका (the night sky) , shukra (Venus). Thus, completing the identification of the sacrificial platform with these elements of space.

The सामn adopts the operator notation to represent cases where the nidhana element may differ in the space filling charms.\
\
Thus, exactly like the Hamiltonian of mechanics, the 'space filling chant operator', H is defined as :\
\
H(x)=x.oM x + ho yi+ x + ho yi+x+hA A vu वा e\
\
Then the new सामन्स् are indicated as:\
\
H(satya)+C1\
\
H(puruSha)+C1\
\
H(agna)+C2\
\
H(वाय)+C3\
\
H(sUryA)+C4\
\
H(chandra)+C5\
\
H(नाक)+C6\
\
H(shukra)+C7\
\
C1=suvar ज्योतिः; C2= ज्योतिः; C3=rAjA; C4=भ्राजा; C5=A भ्राजा; C6= पृष्ठं; C7= भ्राळा भ्राजा


