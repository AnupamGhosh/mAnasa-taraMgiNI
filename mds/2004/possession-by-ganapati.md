
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Possession by gaNapati](https://manasataramgini.wordpress.com/2004/01/04/possession-by-ganapati/){rel="bookmark"} {#possession-by-ganapati .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 4, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/01/04/possession-by-ganapati/ "11:10 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The सूत asked "O great kshatriya narrate to me the effects of displeasing the the fierce god gaNapati."

The descendent of the great manu, the law giver, said " The displeasure of विनायक is of terrible consequence. The 6 ferocious विनायकस्, namely शाल, कटञ्कट, कूष्माण्ड, राजपुत्र, sammita and devayajana can seize a person at anytime. The person immediately shows a variety of strange behaviors- he or she may start pounding clumps of soil with the hand, cutting grass, writing on ones own body and dreaming of waters, men with shaved heads, camels, pigs and asses. They may feel that they are floating in air and feel the constant sensation of being pursued by someone while walking. A women, even if of great beauty will simply not be looked at by any man, when possessed by the विनायकस्. Men even if rich or handsome will simply fail to get a woman. Married women will go sterile and children will die. A kshatriya will lose his post or kingdom even if he has good qualifications. A good teacher will fail to get any students and students will repeatedly pull the plug in their studies. vaishyas' fields will lie fallow and they will sink in their trade. Death may strike the victims without warning and their crops or vehicles may be crushed by rampaging elephants. Rats will devastate their granaries and snakes may kill them or their children. They will tormented by bites of insects, spiders and scorpions"

The सूत asked "pray tell me, Oh mighty राजन्य, of some examples of such a curse."

The मानव said "On the banks of the मूलक lived the brahmin woman gomati. She failed to immerse an idol of the fierce गणनाथ after the festival in the month of अशाढ. She failed to make due offerings to the idol which had been consecrated during the festival after that. An year later she died from the possession by the agents of rudra.

Likewise, किरण kAshyapa on the banks of the ताम्रलिप्त had consecrated an idol of gaNapati in a little shrine. After having done that he and his friends observed unclean practices in the shrine. They were all struck by terrible possessions by the agents of rudra that tormented them for an year till they purified themselves with the vighna शान्ति rite"

"we invoke rudra, vighna, skanda and their awful agents and humbly beseech them to keep their missiles and agents away from us instead send them over to those who hate us or we hate."


