
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The lull before the storm](https://manasataramgini.wordpress.com/2004/11/24/the-lull-before-the-storm/){rel="bookmark"} {#the-lull-before-the-storm .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 24, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/24/the-lull-before-the-storm/ "7:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We were waiting quietly on the undergrowth beside the banks of the River महासन्कट. There was a tense silence in the air. We had performed our karma to the best we could, but were indeed limited by our terrible short comings. Would we tide over it? Time would tell, but would we be around to receive the answer? There was no going back now. The war had been declared. The previous encounters and their results flashed in our minds eye. We felt supersitious, but that hardly helped. The disastrous showing on the hillock of the shudra bard, came to our minds. We invoked सरस्वती, even as दिवोदास and वध्र्याश्व did on the great day of the show down with the पणिस्. We now could feel how it was for sahadeva, somaka and भयमान as they readied themselves to face the dasyus and the shimyus. It was clear that it needed a great effort like that of सुदास if we were to come out victorious on the other side. The weather is supposed to be cold, we have no rathas, and we were battling on alien territory....

We now realize that we have to build fortifications at home when the counter-attack is pressed on us.

The princess of Hayastan, with calves like palmyra trees, and ever energetic, destroyer of many will be there ! Is it through design or chance? The beauty of the princess always had an tremendously ironic aspect to it. Will she prove to be a charm or luck or otherwise? She has both those faces in us.

Zarathushtra had asked Ahura Mazda with outstreached arms for aid. As atharvan had, as शुनःशेप, as वसिष्ठ had, we seek thy aid, O राजन् वरुण, in this great संग्राम.\
\
[]{style="font-weight:bold;"}


