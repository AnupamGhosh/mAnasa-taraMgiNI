
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Categories of Hindus](https://manasataramgini.wordpress.com/2004/09/23/categories-of-hindus/){rel="bookmark"} {#categories-of-hindus .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 23, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/23/categories-of-hindus/ "5:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

She said would only marry रन्गनाथ. In a dream the triple-strider appeared to her father and asked him to send her decked in wedding attire to his shrine. Even as she neared the shrirangam her father was filled with that inexplicable mixture of joy and sorrow, but she was experiencing unrestrained excitement. As the party reached the temple she jumped out of her palanquin and rushed to the गर्भगृह and merged with the 4-handed one in a flash of light.

He appeared in the form of a brahmin and asked the saint iyarpakai नायनार् (saint Hostile-Nature) to give his bride to him for conjugal pleasure. He seeing The god (rudra) within the brahmin and filled with peity offered up his bride to him. As he did so his relatives furious over his deed attacked the brahmin to disrupt the act. But he drew a sword and cut up his own relatives and allowed the brahmin proceed un-interrupted. Please with his devotion the 3-eyed one gave saint Hostile-Nature a vision of his physical form and then restored his wife and dead relatives.

These acts are still remembered by many Hindus with their hair standing on its ends, tears of devotion streaming from their eyes and an inner excitement filling their body.

Much before this the hoary seer of विश्वामित्रस् had chanted:\
"Come here \[o indra], you who are always going around, from here or from the heavenly realm of light. These chants propitiate you around."

Or my ancient ancestor, the भार्गव, recited:\
"indra enjoy- drive on, come, hero- with your two golden steeds, drink the soma, like a seer, loving the sweet brew, pleased with its exhilaration."

At the face of it some may see the same force that drove the later day saints in these vipras of yore. But a careful look at the sacrificial ritual shows that the Hindus come in at least two fundamentally distinct categories. The nature of this distinction is suggested by the tradition of मीमामस in sutra 9.1.9:\
["अपि वा शब्द-पूर्वत्वाद्यज्ञन-कर्म प्रधानं स्याद्-गुणत्वे देवता-श्रुतिः"]{style="color:#0000ff;"}

The great ritualist-scholar shabara svamin explains this sUtra:\
*"The deity and the offering are both accomplished entities, while the act of ritual is what is to be accomplished. The deity, therefore, cannot be the prompter...The view of the opponent makes it necessary to admit of deities having material bodies and actually eating and drinking the substances offered; and this idea is utterly repugnant to the श्रुती, which does not lend support to any such idea regarding deities.*

*The text quoted by the opponent regarding the right hand of the great god indra("we have taken hold of what is indra's right hand") does not mean that indra possesses a right hand. Moreover, even if he did, it would be impossible for any human to 'take hold' of it... The texts that speak of the "arms" of indra being hairy or his "eyes" as tawny-all the are purely eulogistic. Nor is there actual feedng or eating at rituals; in fact, the deity never eats ..."*\
\[this is based on the Sanskrit text, translation and summary of shabara svamin's मीमाम्स भाष्य by Ganganatha Jha]

Furthermore, the मीमाम्सकस्, who are in continuity with the tradition of the ब्राह्मण texts clarify that the mantras and गान-s recited in the vedic ritual are themselves the embodiment of the deity. This remarkable concept, not easily grasped by outsiders to the mantra tradition, is actually also at the heart of the tantric rites. These despite their superficial differences from the vedic rite are based on the same principle of the mantra embodying the deity. Thus, rites stress what Frits Staal termed, orthopraxy- practice approximating an ideal process with increasing accuracy as against orthodoxy- opinion approximating an ideal ideology.

The vedic and tantric rite stress the deity lying embedded in the mantra and this manifests in the form of structures that the mantras possess. Thus the vedic seer composing his hymn or a sAman song and the tantric revealing a mantra with its बीजस् was not like the later day saint merely composing a devotional outpouring. The deity was manifesting in the chant with a deep link to is structure: a striking example of this the organization of the सामिधेनी chant with its structure bearing an equivalence to the year with the 365+x elements. At the junction of the veda and the tantra the hymn being an embodiment of the deity spawn new deities by virtue of its structure:\
[ॐ हों / इशान सर्वविद्यानाम् [शशिन्यै] /उर्ध्वमूर्ध्नि /ॐ हों /इश्वरः सर्वभूतानां [अञ्गदायै] पूर्वमूर्ध्नि / ॐ हों / ब्रह्माधिपतिर् ब्रह्मणोऽधिपतिर् ब्रह्मा [इश्टायै] / दक्षिणमूर्ध्नि/ॐ हों /शिवो मे अस्तु [मरीच्यै] उत्तरमूर्ध्नि /ओम् हों / सदाशिवों / [ज्वालिन्यै नमः] / पस्चिममूर्ध्नि /]{style="color:#0000ff;"}\
\[from the shiva न्यास compiled by rAja bhoja]\
In this way from the expanded expression of great rudra embodied in the chant from the taittiriya ब्राह्मण emerged the five goddess of the head: शशिनी, अञ्गदा, इष्टा, मरीची and ज्वालिनी.

Thus the hindus are of two types :1) those who follow the path of ritual, for whom the deity manifests in the mantra. 2) those who follow the path of devotion, the deity is anthropomorphic and really "something else" from what the ritualist perceives. The fundamental division is not between the tantra and veda. We term the former the path of the ब्राह्मण. The latter is the path of the शूद्र (not exactly identified with the वर्ण-s going by those names).  But the interesting part is that both have co-existed since the earliest reconstructions of the Indo-European society.


