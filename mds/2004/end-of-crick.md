
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [End of Crick](https://manasataramgini.wordpress.com/2004/07/30/end-of-crick/){rel="bookmark"} {#end-of-crick .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 30, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/30/end-of-crick/ "5:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the men who solved the crystal structure of DNA is dead. Francis Crick was a great scientist- man full of great ideas. His partner James Watson was more of gas and show. Amongst Crick's other great contributions was the idea of the selfish DNA, panspermia and musings on the neural cognates of consciousness. The last of these topics was built up by Kristhof Koch, though it is far from clear if these insights help in dealing with what Chalmers calls the "Hard problem of consciousness".

Most of all I have been influenced by the idea of panspermia. While I would not call myself a total "believer" of panspermia, I do feel sympathetic towards it. I think the evidence definitely is not inconsistent with it, though one cannot entirely rule out the alternative with the current data. In general I believe the age of oldest remanant of life of on Earth support some form of panspermia by an indirect extrapolation. The farthest frontier of biology and its general ramifications lie in the direction of understanding panspermia. For now we have to wait like pre-spectroscope stellar observers, puzzling over the nature of the stars.

If it happened, what was the nature of the original life seeded on the Earth. Was it whole cells? or was it prebiotic molecules that then spawned life. If it were the former it would be most remarkable for we may share a kinship with life elsewhere in the universe. But it seem rather inconcievable to the naieve mind as to how life might have survived such cosmic journeys.


