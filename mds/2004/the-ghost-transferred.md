
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The ghost transferred](https://manasataramgini.wordpress.com/2004/07/16/the-ghost-transferred/){rel="bookmark"} {#the-ghost-transferred .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 16, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/16/the-ghost-transferred/ "6:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I received a call from him. He sounded much the same rustic fellow as I hand last enountered him over 8 years ago. His accent was the same rasping one, which sounded like a metal plate rubbing on another. I asked what had befallen him. He replied that he had been to the house beside the curve on the northward street and was quite sick after his stay there. I was shocked, that this could happen. I remembered the old words that the place might not be alright. My mind turned to the tale of Dildrum and Doldrum.Once a man walked through an alley of cats. Suddenly a cat yelled "tell Dildrum that Doldrum is dead". All the cats dispersed meowing away incomprehensibly. The man came home and told his wife. I heard a cat state that Doldrum is dead. Immediately his cat yelled: "I shall soon be king of the cats" and climbed up the wall and departed.

Thus, when he said "mujhe lagta hai ki, udar koi bhoot rahta hai", I realized that the sprite will be transferred to him.  As we had passed through काकोदकपुर we had heard the words of the epitome of काकत्व. We grasped those not then, but now we understand.


