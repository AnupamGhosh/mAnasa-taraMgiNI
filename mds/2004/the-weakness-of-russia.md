
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The weakness of Russia](https://manasataramgini.wordpress.com/2004/09/07/the-weakness-of-russia/){rel="bookmark"} {#the-weakness-of-russia .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 7, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/07/the-weakness-of-russia/ "3:31 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The recent string of Islamic attacks and the spineless Russian response have shown that Russia has indeed lost its edge. In the case of India one can easily see how it is really a weak country and not really capable of standing up to the Americans. So when the Americans and the West ask India to show restrain even when punched by the Mohammedans, India obeys with nothing more than a mild protest. However, Russia has nuclear weapons that can indeed seriously threaten the US, and it is unlikely that the US can do much to Russia directly. Russia has the resources to carry itself and it has proven itself in battle against the superior and ruthless army of the Nazis. Despite this Russia has thus far shown hardly any signs of acting decisively. To the contrary Putin admitted that they were weak.


