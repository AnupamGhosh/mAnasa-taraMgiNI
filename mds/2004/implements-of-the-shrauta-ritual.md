
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Implements of the shrauta ritual](https://manasataramgini.wordpress.com/2004/08/18/implements-of-the-shrauta-ritual/){rel="bookmark"} {#implements-of-the-shrauta-ritual .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 18, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/18/implements-of-the-shrauta-ritual/ "3:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ratha chakra: The spoked chariot. Typically made of audumbara wood. It is used in the आधान rite (deposition of the 3 fires) where the brahmA priest rolls it along from the गार्हपत्य to the दक्षिण and आहवनीय. In the वाजपेय rite (a 17 spoked-wheel) its pegged to the ground and rotated 3 times to the accompaniment of the वजिनां sAman.

अरणी: Two blocks of wood from an ashvattha tree that serves as the surface for the generation of fire. 4X12X16 angulas typically.

The fire drill: It has 3 parts: 1) pramantha- a peg of wood cut out from the upper अरणि that is drilled into the lower अरणि to make fire. 2)The chatra the spindle with grooves around which the rope passes while drilling and holds the pramantha. 3) The ओविली- the T shaped handle fitted on top of the chatra and used for pressing into the block for friction.

rajju: the rope used to churn the fire drill against the lower अरणि.

agnihotra स्थाली: A wide circular bowl with straight edges. The cow is milked into the स्थाली in the agnihotra. The rice or the gaveduka charu may be cooked in the स्थाली. It is only made by an Aryan by hand.

कूर्च: a fish shaped flat wooden bar an arm's length. All ritual ladles are placed on the कूर्च when not deployed. If the कूर्च is not available they may be placed on darbha grass.

asida: an iron sickle used for cutting darbha grass.

sruch: a ladle with a long handle (about a bahu long) with a terminal cup and a down-ward directed beaker-like lip. A groove passes from the terminal cup to the tip of the lip. It has a stand at the rear end. Different kinds of sruch-s with different designs are used in different ritual applications. These are the जुहू, dhruva, उपभृत and agnihotra हवणी. It is made of gold, silver or wood.

sruva: a special ladle with a terminal cup with diameter equal to the tip of a thumb. Liquid flammable oblations are offered with the sruva. It may be made of adumbara, पर्ण or khadira wood, gold, silver or the alloy काम्स्य for अभिचार. Some sruvas have bi-septate terminal cup and are called the dvibila sruva-s.

darvi: A ladle about a bahu in length with a shallow circular cup at the tip. It is usually used for the स्वाहा oblations in the गृह्य cycle and the साकमेध offerings in the गार्हपत्य fire. It is made of khadira typically.

आकर्ष phalaka: A special ladle whose handle is shaped like a composite bow. The cup is shaped like a cobra's hood. It is used to offer tilas in the उपाकर्म rite. वारण or udumbara wood.

प्रणीता: a rectangular wooden vessel with a short handle about 4 angulas in length. The dimensions of the vessel are 3X4X6 angulas. Used to carry the ritual water. Made of nyagrodha wood or clay for अभिचार rites.

सक्षीर प्रणीता: A प्रणीता like vessel with two compartments like the above vessel. The fore compartment is half the volume of the hind compartment. The front compartment is filled with milk and the hind with water in the चातुर्मास्य charu offering to the sons of deva rudra.

प्रोक्षणी पात्रं: a vessel shaped like a lotus bud with a large groove leading to its narrow terminal lip. It is used to contain water for sprinkling in purificatory incantations.

sfya: a wooden sword of khadira wood. It is a बाहु in length and is used to score lines for geometric constructions on the sacrificial site. The adhvaryu holds it while reciting a प्रैष or the अप्रतिरथं. It is also held up as a sword in the stamba yajur हरण rite.

शम्या: peg made of khadira wood shaped like the eponymous quarter pin of the chariot wheel. Is thrown to mark the next sacrificial spot in the सारस्वत and yat satras. It is used for measurements in the construction of the altar. In the shukla yajurvedic tradition it is shaped like a flat oblong bar with a central slit. It is also struck on the दृषद् or the grinding stone to signify the devas sounding their weapons to declare war on the asuras.

धृष्टि or उपवेष is a wooden staff a प्रादेश in lenght, ending in a hand-shaped head. The hand-shaped head of the staff is used to push aside coals from the baking of the पुरोडाश or stirring fires.

मेक्षण: a flat baking stick similar to the baking stick used to turn dosais. Used for stirring the odana when it is cooked and to take up odana for the offering to the ancestors.


