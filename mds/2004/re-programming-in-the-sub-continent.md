
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Re-programming in the sub-continent](https://manasataramgini.wordpress.com/2004/07/16/re-programming-in-the-sub-continent/){rel="bookmark"} {#re-programming-in-the-sub-continent .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 16, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/16/re-programming-in-the-sub-continent/ "5:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-2TsWiCk6IxU/TwvtX99OY8I/AAAAAAAACVE/4xqbMnChMZw/s400/reprog.jpg){width="75%"}
```{=latex}
\end{center}
```



An elegant illustration of the process by which some well-known ideologies have perverted the Hindu mind in the subcontinent.

