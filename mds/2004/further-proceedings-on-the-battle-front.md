
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Further proceedings on the battle front](https://manasataramgini.wordpress.com/2004/11/28/further-proceedings-on-the-battle-front/){rel="bookmark"} {#further-proceedings-on-the-battle-front .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 28, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/28/further-proceedings-on-the-battle-front/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We invested today in some heavy duty diplomatic negotiations in the hope that we may strengthen our hand for the impending battle. The negotiations went off well but we cannot be lulled into security because the enemy too is possibly using this as a ploy to obtain a dependable escape route if they are pressed hard. Funnily the situation is somewhat like Iraq (them) and the USA (us). On the occassions we smashed them, we gained little, but their constant assaults have a tremendous nuisance value. We felt some omens but refrained from consulting the shakuna शास्त्र to see whether they good or bad, because omens are only tradesmen and not brave क्षत्रियस्.


