
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [French wine: practical symbolism?](https://manasataramgini.wordpress.com/2004/03/18/french-wine-practical-symbolism/){rel="bookmark"} {#french-wine-practical-symbolism .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 18, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/03/18/french-wine-practical-symbolism/ "7:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Malhotra had once mentioned in his blog that French wine may be a major cultural symbol. This reminded me of a peculiar incident that indeed does support this contention in a twisted way- the tale of how French wines mediated in a civilizational clash. ...Raghuji Bhosle at the head of a major Maharatta force had routed Chand Sahib, a freebooter in Tamil Nad, and besieged Pondicherry and asked the French to capitulate. The French declared that they were friends of the Maharattas and sent Bhosle 10 bottles of some vintage French brew with the innocent name "Nantes Cordials". Not knowing it to be a forbidden beverage Bhosle passed the bottles over to his wife who took a profound liking for it and asked Bhosle to make peace with the Francisi in exchange for more bottles. The French happily obliged a good stock of **[णन्तेस् Cओर्दिअल्स्]{style="color:rgb(153,255,153);"}** and Raghuji got to confirm his wife's opinion on the brew and promptly withdrew the Maharatta army!...Thus, the it is claimed that the vineyards of France was a savior in adversity. I would not be surprised if this token of decadent French tastes serves as a potent symbol of their culture to those who can appreciate it....There have also been claims by other regarding the role of French wine in staving of a Mongol invasion at the height of Mangku's power. But this incident remains unconfirmed, unlike the reliable evidence favoring the Raghuji incident


