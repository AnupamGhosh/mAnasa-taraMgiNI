
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A brief history of the Vijayan rule of Khotan](https://manasataramgini.wordpress.com/2004/09/17/a-brief-history-of-the-vijayan-rule-of-khotan/){rel="bookmark"} {#a-brief-history-of-the-vijayan-rule-of-khotan .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 17, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/17/a-brief-history-of-the-vijayan-rule-of-khotan/ "5:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Nilakantha Shastri brings attention to the Indian rulers of Khotan and has attempted to reconstruct their chronology from various sources.

[Map of Khotan](http://idp.nlc.gov.cn/chapters/topics/buddhism/khotanese/images/khotan_map.jpg)

In the period that followed the death of the great emperor ललितादित्य, and the confusion during the reign of संग्रामपीड, the Chinese generals Yao Yao and Wei-ch'ih-Sheng devised a daring plan to take Khotan. With the help of the internal Sinitic population they engineered a revolt and suddenly appeared at the head of a massive Chinese army of 80,000 to beseige and conqueror the kingdom. First Sheng and there after Yao declared themselves rulers of Khotan. Alarmed at this advance the descendent of ललितादित्य, जयादित्य, son of संग्रामपीड, who ascended the throne in 751 CE, assigned general vijaya vikrama a descendent of the original Hindu royal family of Khotan to wrest it back from the चीनस्, हूनस् and "mlecchas". With a mobile calvary force he cut off the supply line of the चीनस् and then luring them into an ambush in narrow escape route he destroyed the chIna army completely.\
In late 756 vijaya vikrama was crowned rAja of Khotan. His dynasty as currently reconstructed:

vijaya नक्षत्र early 800's\
vijaya कीर्ति around 820-40's\
vijaya vikrama II mid-800's\
vijaya संग्राम late-800's\
vijaya sambhava the great 912-966

Till vijaya संभव lived he stood like a pillar of the dharma and protector of the ब्राह्मणस् in the midst of the turushkas. His kingdom was considered the geographic ओड्डियान पीठ on earth (जालन्धर in Punjab, पुर्ण giri being in Orissa, कामकूट in Assam, मूल in मूलस्थान/Multan). He placed a wall of steel in the path of the three Qarakhanid Sultans: Abd al Karim, Musa Bughra Khan and Suleyman Arslan Khan and defeated each one of them on multiple occasions in fierce cavalry engagements, even though he was entirely cut off from receiving aid from the Hindu rulers of Gandhara and Kashmir. He also routed the Moslem army from Bokhara, which was led by the Arab chief Ahmed ibn Ismail. Subsequently he defeated another Arab army of Abd al-Malik-I from Bokhara. As a victorious ruler and builder of many temples (of which fragmentary ruins still survive) he was like the last flash of Indic glory in Central Asia.

vijaya surapati 967-977\
vijaya dharma 978-982

vijaya dharma faced invasion from Sultan Noah ibn Mansur from Bokhara and the Qara Khan turks from Kashgar. vijaya dharma fought off the attack and repulsed the Sultan but died shortly there after. He was succeeded by vijaya कुमार who immediately resumed the struggle against the Moslem hordes. Noah ibn Mansur and the other Turkic and Arab armies formed a pact for surrounding the Kaffr kingdom. The Hindu king was give the choice between his foreskin and his head. Though having no Hindu allies anywhere near him, he and his people decided to uphold the dharma or die in an attempt. He gathered all his troops and staved of the Moslem hordes which were pouring ceaselessly from all directions. He single-handed kept the terrible war raging despite the numerically superior foes and better horsepower of the enemy.

The Moslems erected huge Majiqs (mangonels) and sarakels (trebuchets) all around Khotan and bombarded it. But the heavy fortification still held on. After 10 years of continuous fighting the Hindu kingdom was entirely shorn of its resources, reduced in man and horse power, and denuded in defences. Finally, the fortifications were breached and the Mohammedan armies poured in. The king and his army died fighting the invaders. The city was looted and the idols of gold with gems studded in them were melted down. The male population was massacred and the women taken. Thus ended last Hindu kingdom in Central Asia in its final blaze.


