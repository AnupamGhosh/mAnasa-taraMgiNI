
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [rudra- his agents](https://manasataramgini.wordpress.com/2004/05/19/rudra-his-agents/){rel="bookmark"} {#rudra--his-agents .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 19, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/05/19/rudra-his-agents/ "9:33 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A major strand of shaiva thought holds that rudra himself does not directly perform the actions himself but does so via his agents. In philosophical terms he may be seen as the consciousness that witnesses and sanctions the actions.

**वीरभद्र**: He emerged from a fragment of the matted locks of rudra for the killing of prajApati दक्ष. He wrought havoc in the midsts of the deva armies and destroyed the demons created by भृगु before going on to behead दक्ष. He then revived दक्ष with a goat's head.

**काल bhairava:** Created by rudra for the beheading of the 5th head of brahma which was mocking rudra. For this he acquired the pApa of ब्रह्महत्या and had to perform the कापालिक vrata. He moved on to slay vishvaksena, the chief गण of विष्णु and was finally purified of brahma हत्या after drinking the blood of विष्णु.

**nandikeshvara**: The bull-headed or ape-headed chief गण of rudra. A great warrior and leader of the armies of rudra in many battles. In the battle against the दक्ष he was felled by a blow from the vajra of indra. In the battle against अन्धकासुर he captured the भार्गव shukra and handed him over to rudra.

**क्षेत्रपाल**: A fierce agent of rudra who emits a fiery meteor from his mouth to counter missiles hurled by the asuras when they do battle against rudra.

**कीर्तिमुख**: A fierce गण created by rudra for the chastisement of राहु. In his insatiable hunger he ate his own body parts and was reduced to a roving head that devoured asuras.

**वीरक**: A terrible गण created by shiva to guard पार्वति when he was performing the महापाशुपत vrata to regain his energies after the dalliance with उमा. He fought the army of andhaka and wrought havoc in its midst. He might be an ectype of वीरभद्र

**कुमार**: The elder son of rudra, the six-headed god. He was first born for the slaying of demons like महिष and तारक. He led the deva armies in these battles and subsequently was one of the major commanders of rudra's armies. Has numerous गणस् of his own.

**गणpati**: The younger son of rudra, the elephant headed god. He was created by पार्वति as her personal गण, but was beheaded in a battle with rudra and the gods and restored with an elephant head. He became one of the major commanders of the गणस् and slew asuras like analasura and सिन्धूर.

**भद्रकालि**: The wife of वीरभद्र. She led the shakti सेना in the दक्शा battle and destroyed the दक्ष army.

**महामारि**: The principal female commander of rudra's armies. She lead the pramatha army of shiva along with कुमार in the battle against the demon शङ्कचूड and the two of them slaughtered the commanders of शन्कचुड's army.

**काली**: The wife of rudra. She lead the forces of the shaktis in the battle against shumbha and nishumbha. In the battle against शङ्कचूड, कुमार was struck down by the brahmashakti hurled by शङ्कचूड. She rescued कुमार from the battle lines and brought him back to shiva. Thereafter, when वीरभद्र fell unconscious struck by he missile of शङ्कचूड she took over the command of the गण armies and marched against the asuras. She decimated the asura army and in a fierce battle with शङ्कचूड struck him down with the माहेश्वर missile.


