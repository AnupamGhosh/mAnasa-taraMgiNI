
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [vachaspati on atomic coordinates](https://manasataramgini.wordpress.com/2004/09/07/vachaspati-on-atomic-coordinates/){rel="bookmark"} {#vachaspati-on-atomic-coordinates .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 7, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/07/vachaspati-on-atomic-coordinates/ "5:29 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great ब्राह्मण वाचस्पति mishra, who was described as "sarva\
\
tantra svatantra" provides a model that is equivalent to the 3D\
\
cartesian system in his work nyAya vartika तात्पर्य टीका (4.2.25) to\
\
describe molecular/atomic contacs. The 3 axes were termed\
\
पूर्व-pashchima, uttara-दक्षिण and urdhva-adhara. The position of\
\
any point is given by measuring the the distance along these\
\
direction. The position of contacts between atoms can then be given as\
\
numerical series of these coordinates.

ekatve.api दिशः Adityodayadesha प्रत्यासनदेश संयुक्तो yaH sa\
\
इतरस्माद् विप्रकृष्ट pradesha संयोगात् परमाणोः purva\
\
एवमादित्यास्त mayadesha प्रत्यासनदेश संयुक्तो yaH sa इतरस्माद्\
\
विप्रकृष्ट desha संयोगात् परमाणोः पश्चिमः tau cha\
\
पूर्वपश्स्चिमौ परमाणू अपेक्ष्य yaH sUryOdayAstamayadesha\
\
विप्रकृष्ट desha sa मध्यवतीं\
\
evam etayoryau tiryagdesha संबन्धिनौ madhyasya Arjavena\
\
vyavasthitau पार्श्ववर्तिनौ tau दक्षिणोत्तरौ परमाणू\
\
एवं madhyandina वर्तिसूर्यसनिकर्ष विप्रकर्षौ अपेक्ष\
\
उपर्यधोभावो द्रष्टव्यः\
\
samyukta संयोगाल्पत्व भूयस्तवे cha सनिकर्ष विप्रकर्षौ\
\
पूर्वसम्ख्यावcचिन्न्त्वं va अल्पत्वं परसम्ख्यावcचिन्नत्वं ca\
\
भूयस्त्वं


