
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The magic formula of the भृगुस्](https://manasataramgini.wordpress.com/2004/12/05/the-magic-formula-of-the-bhrigus/){rel="bookmark"} {#the-magic-formula-of-the-भगस .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 5, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/05/the-magic-formula-of-the-bhrigus/ "7:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[प्र सुवानः सोम ऋतयुश् चिकेतेन्द्राय ब्रह्ंअ जमदग्निर् अर्चन् /]{style="color:#0000ff;font-weight:bold;"}\
[वृषा यन्ता ऽसि शवसस् तुरस्यान्तर्यच्छ गृणते धर्तं दृग्ंह //]{style="color:#0000ff;font-weight:bold;"}

[soma]{style="color:#ff9966;"}, endowed with the [ऋत]{style="color:#ff9966;"} becomes visible when extracted,\
[जमदग्नि]{style="color:#ff9966;"}chants the hymn to [[indra]{style="font-weight:bold;"}]{style="font-style:italic;color:#ff0000;font-size:130%;"};\
you are the mighty restrainer of impetuous force;\
Ward it off and strengthen the scaffold for the sacrificer.


