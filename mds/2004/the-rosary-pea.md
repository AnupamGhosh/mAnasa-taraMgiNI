
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rosary pea](https://manasataramgini.wordpress.com/2004/12/21/the-rosary-pea/){rel="bookmark"} {#the-rosary-pea .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 21, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/21/the-rosary-pea/ "6:44 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Many seeds greatly fascinated me when I was young (actually they continue to do so) . One of these was the [rosary pea (]{style="color:rgb(204,102,204);"}[Abrus precatorius]{style="font-style:italic;color:rgb(204,102,204);"}[)]{style="color:rgb(204,102,204);"}. The other was the castor seed. When we had beheld the all-bewitching Kappa Hayastanika, like most other mortals, we had uttered the words:

[](http://srimadbhagavatam.com/a/aho)

"aho रूपं aho धाम aho अस्या नवं वयः / कङ्ज-पलाशाक्षि वामोरु मथ्नतीव मनांसि नः /त्वं asi शरीरिणां sarvendriya-मनः-प्रीतिं // "

[](http://srimadbhagavatam.com/n/nah)

Kappa told me in reply that the rosary pea is a very instructive lesson in life. "With the masura seeds we might survive, though the mAshA would be a better alternative for life..."


