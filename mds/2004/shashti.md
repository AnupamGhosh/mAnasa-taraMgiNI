
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [षष्टि](https://manasataramgini.wordpress.com/2004/06/06/shashti/){rel="bookmark"} {#षषट .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 6, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/06/06/shashti/ "6:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**षष्टि** is the wife of the terrible 6-headed lord of the भूतस्. She is the daughter of yama or indra. When grandson of manu was still-born he was revived by the great षष्टि. She is also known as देवसेना or सेना and is worshipped on the shri पञ्चमी, the 5th day after दीपावलि. A तर्पणं performed to her and कुमार with the mantras from the बोधायन dharmasUtra. Usually the तर्पणं is performed under a large ashvatta tree or some other tree. A cat's face may be draw on a cloth or a leaf and left there as to invoke her. Then a cat is offered milk as it is the वाहन of षष्टि and she is supposed to accept offerings that form. This day marks her marriage with the great कुमार. She is also invoked on the 6th day after the birth of a child to keep away the awful agents of her husband from obliterating the child. She is accordingly invoked in the Ayushya homa performed for safe-guarding the lives of kids. She is worshipped by thieves and knaves on the 6th day of the waning moon for success in thievery. Another important rite of षष्टि is the आरण्य षष्टि that is performed around May-June during the first 6th digit of the waxing moon. The rite is usually done under a kadamba tree in a forest, where an idol of षष्टि may be installed. Then oblations are offered in the fire with the आरण्य सूक्तं of the ऋग् veda and then a तर्पणं for कुमार and देवसेना is done. An oblation is also offered to the मात्रिक लोहितायनि, a female गण of कुंआर. Then food is left for a fishing cat or a lynx and women are given a thread to wear around their wrist.

षष्टि is invoked as residing in the bindu at the heart of the षट्कोण yantra perpetually conjoined with कुमार. She is shining blue in color and is held in the tight embrace of the red कुमार, with his hands around her saffron-smeared full breasts, even as he draws her glorious face closer to this own central face. Seated on his lap she plays with the opposite face of the kumara which is in the form of a goat.

षष्टि and कुमार are depicted frequently on the coins of the yaudheya janapada.


