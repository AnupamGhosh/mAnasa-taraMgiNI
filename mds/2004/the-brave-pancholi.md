
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Brave Pancholi](https://manasataramgini.wordpress.com/2004/03/09/the-brave-pancholi/){rel="bookmark"} {#the-brave-pancholi .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 9, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/03/09/the-brave-pancholi/ "3:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Ajayamerupura (known today as Ajmer) had always been a key center for the dominance of Hindustan. It was the home of one of the most famous Indian Vedic universities in north India during the reign of the Chahamanas. It was also the center of the famous temple of Brahma near the Pushkara Tirtha. After the death of Prithiviraja Chahamana at the hands of Shihabuddin Ghori, Ajayamerupura was attacked by the Mohammedans, and finally wrested by Qutub-ud-din. Having demolished the Vedic university and a series of Hindu temples he erected a grotesque structure termed the Adai Din Ka Jhopda- the shed constructed in 2 1/2 days. This was used as a mosque for the namaz of the Moslem conquerors. Since then it remained under Mohammedan control.

The brief liberation of Ajmer was due to the valiant Pancholi, Kshema Simha. The Rathod ruler Rao Chanda had been slain by Salim Shah, the Sultan of Multan, in a Jihad at Nagor. Salim had then gone on a pilgrimage to the Mazaar at Ajmer. Rao Chanda' s son Rao Ran Mal conferred with Pancholi Kshema Simha to take punitive action. Kshema Simha thought out a brilliant plan to take the fort by commando action. He chose a band of around couple of hundred elite Rathod warriors who were extremely well-trained in the kshatriya lore of taking on a large number of attackers single-handed. He then came up with the excuse of conveying a daughter for marriage to a Rajput noble of Ajmer. Kshema Simha and his Rathod commandos entered in the guise of being a marriage party and then at night took aim at the Mohammedan garrison manning the walls and shot them down in a concerted shower of arrows. Then the Rajputs got into the inner defenses by escalade and butchered their entire Moslem army after firing their quarters. Salim Shah tried to hide in the Dargah and escape back to Multan with the help of some Mullahs. But the Rajputs caught sight of him and cut his head off. Thus Kshema Simha liberated Ajmer from Islamic rule. As reward for this brave deed the Rao Ran Mal rewarded the Pancholi with Khatoo, that he had just wrested from the Qara'unas Turk Khaim Khan. The Hindus allowed the Mohammedans to remain behind in Ajmer but forbid any public displays of Islamic practices.


