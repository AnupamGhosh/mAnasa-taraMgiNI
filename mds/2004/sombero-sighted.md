
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Sombero sighted](https://manasataramgini.wordpress.com/2004/04/20/sombero-sighted/){rel="bookmark"} {#sombero-sighted .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 20, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/04/20/sombero-sighted/ "11:55 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We we celebrated the true arrival of spring by a remarkable wandering on the moutain paths. We felt as though the Atman had ceased to be bound and made its universal identification. As the azure heights of the celestial dome darkened with the setting eye of mitra and वरुण, we role the telescope out. One by one we ticked off each of the galaxies that lay within our reach in Leo and Virgo. That vast swath of galactic richness starting from NGC3115 in Sextans to M95 and M96 in Leo to the Sombrero Hat on the head of the Crow to the faint beauty of NGC4038 on the flank of the Crow.\
10 years had passed between the sightings!


