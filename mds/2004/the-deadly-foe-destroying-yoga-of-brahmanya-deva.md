
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The deadly foe-destroying yoga of ब्रह्मण्य deva](https://manasataramgini.wordpress.com/2004/09/12/the-deadly-foe-destroying-yoga-of-brahmanya-deva/){rel="bookmark"} {#the-deadly-foe-destroying-yoga-of-बरहमणय-deva .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 12, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/12/the-deadly-foe-destroying-yoga-of-brahmanya-deva/ "6:12 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**"We pacify the six-headed being, of fiery birth, who is lost in the embrace of the beautiful daughter of the god of the gods indra. May his lethal darts not harm us and instead strike our foes."**

At the heart of the kaumara tantric rite lies the great secret yoga which destroys ones foes. The स्कन्दार्चन kalpadruma and other obscure कौमार texts refer to this secret yoga, the configuration of the incantation for which is normally revealed only by oral tradition.

The basic incantation is known as the ब्रह्मण्यदेव trishati comprised of the 300 names of the great god कुमार combined with the षडक्षरि mantra of कुमार. Many performers of the कौमार tAntric worship use this incantation in their basic regular archana. This practice is commonly seen in several temples in India and has recently even been transplanted to the foreign shores. Only those who have received the esoteric षडक्षरि mantras of कुमार may deploy this incantation even in its basic archana form.

However its deployment in the specialized form requires a more ardous procedure.

  - women are forbidden from performing the procedure as they may not be able to sustain some of its effects.-Firstly an idol of कुमार, ideally of the 6-headed form is procured; alternatively a कुमार yantra may be used.-the votary then abstains from eating all forbidden foods (including onions, garlics and leeks), meats and liquors.

  - for 3 days he observes total celibacy, not spilling his semen, and avoiding mentally entertaining sexual thoughts.

  - then he prepares sweet-smelling flavored milk.

  - then he prepares for the rite early in the morning or in the evening.

  - He first offers the twilight offerings to the gods mitra or वरुण.

  - He then mutters 1000 incantations to the god सविता holding the knot of his यग़्ण्योपवीत.

  - He then allows the thoughts coursing in his mind like a raging ocean to still themselves and concentrates on his चित्रिणि नाडि. At the level of the genitalia on the track of the चित्रिणि नाडि within the spinal cord he senses the स्वादिष्ठान chakra. He then percieves the deva वरुण in the chakra in form of a blazing white god with pAsha and didyu weapons in the cerebro-spinal aqueduct. He meditates on वरुण with the वं bIja. As his concentration deepens, within the god वरुण, the deva विष्णु emerges in dark blue form with four hands holding the usual weapons. He meditates on विष्णु in the bindu of the वं bIja. Beside him is विष्णु is his aquiline गण, garuDa, whom the votary also worships. As his concentration increases he sees in a flash filling the स्वादिष्ठान chakra the goddess रकिणी. She is of the color of a blue water-lily, exceedingly beautiful and delightful in sight but having sharp canines, is wrapped in a white raiment with a large red vermillion mark on her fore head, having doe-like eyes which are gracefully painted with collyrium and holding a deadly sword and shield in her hands. Once he perceives her, he utters the 6-fold bIja mantra. He senses the six fold structure of the chakra and six flames burst forth in a hexagonal form. At this point he sees the great god कुमार ब्रह्मण्यदेव filling the chakra. He is of red color with six heads and 12 arms, one of which is like a goat, and embraces the goddess षष्ठि who is seated on his lap. He needs to now merge his consciousness into the मूल mantra of कुमार (as percieved by learned तान्त्रिcस् like लक्ष्मण deshikendra) and utter it 600,000 times for best effect.

  - Then he begins making 300 fire oblations to the god कुमार with the trishati. It becomes the shatru सम्हार trishati that destroys all foes.

  - At the end he offers 3 libations of the flavored milk before the idol of कुमार with appropriate gAyatri incantation from the मैत्रायणि text.

  - The next day he makes a special very sweet dish with 5 sweet materials and covers the idol with it uttering the quenching incantation of the बोधायन text to कुमार and his wife षष्ठि.

  - The votary may then distribute this sweet paste as an offering.

The votary may experience a throbbing and intense internal sense of burning or rise in body temperature if he is a novice at the rite. The complete yogic control of the meditation of the स्वादिष्ठन is however a must for proper performance of the rite.


