
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The roguish Karttikeya and the wives of the gods](https://manasataramgini.wordpress.com/2004/08/02/the-roguish-karttikeya-and-the-wives-of-the-gods/){rel="bookmark"} {#the-roguish-karttikeya-and-the-wives-of-the-gods .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 2, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/02/the-roguish-karttikeya-and-the-wives-of-the-gods/ "4:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**[We worship the roguish deity कुमार with offerings as ordained by the school of the great sage gopatha.]{style="color:rgb(255,0,0);"}**

Due to the efficiency of the law enforcement and strictures of the dharma शास्त्र the rogues and thieves of the land were no longer able achieving their aims. So the rogues and thieves decided to worship the great spear-wielding deity who is known to some as son of agni, to others as the first born son of the fierce rudra and to yet other as the son of the कृत्तिका-s. The chief of the taskara-s accordingly set up a well decorated मण्डप and invoked कुमार with the great formulae ending in the refrain "**[धुर्तं आवाहयाम्यहं]{style="color:rgb(153,153,255);"}**". Pleased by the worship the six-headed god promised the rogues that their trouble will come to an end.

Accordingly the fowl-bannered deity incarnated as the sage kanaka-shakti-hastin. kanaka-shakti-hastin became the great sage of the rogues and robbers and composed the text on taskaravidya for their benefit. Armed with this the thieves regained their ways and accordingly offered worship to the great षण्मुख.

After slaughtering तारक, the lord of the दानवस्, the commander of the deva army, who is endowed with good looks and great prowess, took to amorous and erotic sports. On account of his looks he started flirting with all the goddesses in svarga and engaged in dalliance with the wives of other great deities. On account of his charms the goddesses were unable to keep away from the six-headed god and constantly milled around him and were lost in his embraces and passionate plays. In great consternation, the deva-s rushed to the great daughter of the mountains and asked उमा to save their wives from her son. उमा called her fiery son and upbraided him about his ways. But the great deity ignored her and continued dallying with the goddesses. Finally to save the devas from this situation, उमा decided to manifest in every goddess. Thus, to which ever wife of the god कुमार went, he only saw his mother उमा. Utterly disgusted he renounced his amorous sports and vanished to the seclusion of कुमार parvata.


**[ॐ वचद्भुवे नमः]{style="color:rgb(255,102,0);"}**



