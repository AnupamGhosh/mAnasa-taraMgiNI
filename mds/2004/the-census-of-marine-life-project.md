
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Census of Marine Life project](https://manasataramgini.wordpress.com/2004/11/23/the-census-of-marine-life-project/){rel="bookmark"} {#the-census-of-marine-life-project .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 23, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/23/the-census-of-marine-life-project/ "4:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[A new-to-science goby fish from Guam ]{style="font-weight:bold;"}

[](http://photos1.blogger.com/img/133/1300/640/new_gobyfish.jpg)

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/new_gobyfish.jpg){width="75%"}
```{=latex}
\end{center}
```



[The ]{style="font-weight:bold;"}[[ Census of Marine Life ]{style="font-weight:bold;"}project has uncovered an interesting goby fish in Guam that forms a symbiosis with a snapping shrimp. The shrimp excavates a burrow, and the fish guards the borrow and uses it as a dwelling. The level of coordination between the two is unclear, but would definitely be of interest in terms of understanding macro-symbiosis.\
\
]{.regblk}

[Scorpaenopsis eschmeyeri]{style="font-style:italic;"} : [A new scorpionfish ]{style="font-weight:bold;"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/scorpionfish.jpg){width="75%"}
```{=latex}
\end{center}
```



Scorpion fishes are a family of poisonous fishes. For more pictures of these remarkable fishes see [here.](http://www.hkdivephoto.com/stephenwong/lionfish/)

My all time favorite amongst these fishes is the awful venomous [[Rhinopias eschmeyeri]{style="font-style:italic;"}.](http://www.edge-of-reef.com/foto/gallery/SCO03.jpg) Not far behind is the drab, but deadly [[Uranoscopus bicinctus](http://www.edge-of-reef.com/foto/gallery/URA01.jpg). ]{style="font-style:italic;"}The scorpion fish family is Scorpaenidae which has many subdivisions:\


 1.  Scorpaeninae: Scorpionfishes and Lionfishes. 2) Synanceinae: Stonefishes 3) Choridactylinae: Stingfishes and Devilfishes and 4) The Leaf fish (Taenianotus). They are characterized by their odd shapes and spines with venom on their backs and fins.

[ ]{style="color:rgb(0,128,0);"}

Lionfishes have been observed to show cooperative hunting behavior, something worth investigating further.

[](http://www.hkdivephoto.com/stephenwong/lionfish/)

[Pycnopodia helianthoides]{style="font-style:italic;"}[: the new sea-star ]{style="font-weight:bold;"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/starfish.jpg){width="75%"}
```{=latex}
\end{center}
```




