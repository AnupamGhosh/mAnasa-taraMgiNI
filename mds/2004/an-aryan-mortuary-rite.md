
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An Aryan mortuary rite](https://manasataramgini.wordpress.com/2004/08/05/an-aryan-mortuary-rite/){rel="bookmark"} {#an-aryan-mortuary-rite .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 5, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/05/an-aryan-mortuary-rite/ "6:13 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The पितृमेध is a relatively obscure Aryan mortuary rite done an year or later after the death of an ancestor. It used to be performed in the height of summer, a new moon day or in the month of magha, usually choosing a नक्षत्र with a single star. The bones of the ancestor were brought in an urn and placed on a couch. The urn was wrapped in an unwashed cloth and the living descendents and their women folk go around the urn 3 times as the vINA and other musical instruments are played. They fan the urn with their upper garments as they go around. These circumambulations are performed in the initial, middle and last part of the night. The day the ritual is done also includes a **[feast]{style="color:#ff6666;"}** and distribution of food along with a [**provision for dancing, singing and and playing on musical instruments.**]{style="color:#ff6666;"} Some food is also dedicated to the urn of the dead ancestor.

Final depositing of the bones occurs when the sun is just rising. By dawn the adhvaryu, the यजमान and his clansmen start walking towards the south. The site for interment should be far away from a place of habitation and roads. They should also be away from banyan, pipal विभीदक and other fig trees. The place should be beautiful and should have reeds. An isoceles trapezoidal श्मशान is measured out a man's length in height and the four corners are marked with pegs of palasha wood (East), shami wood, वरण wood and stone. The stone peg should have an image of वृत्र on it and be placed in the southern quarter. A rope is held taut around he marked area by the pegs. The place is swept with a palasha twig with the formula 'apeto yantu...'. The twig is then thrown to the south and the यजमान arranges the enclosing stones around the श्मशान. With oxen he plows along the rope with the formula 'वायुः पुनातु ..." from north to west. Then the bullocks are released and the plough placed at the south with the formula "विमुच्यन्तां ...". He then sows all kinds of seeds in the furrow with the formula ashvatthe वः. Then the bones are deposited with the formula with 'सविता te..'. An assistant of the brahmin breaks the urn to the south while performing a प्राणायाम. When he returns the adhvaryu utters the formula 'param मृत्यो..' The bones are then arranged in [**anatomically correct**]{style="color:#ff0000;"} articulation as a stretched out man, with the formula 'shaM वाता..' and a brick is placed in the middle. At each side of the trapezium 3 bricks are placed, going east to west. Soil is then spread over the bricks. If the dead one was not a great fire sacrificer then the grave is covered with gravel. If the dead one was brahmin then the head part of the स्मशान is made tall. If it were a kshatriya then the chest region is made high, if it were a vaishya the thighs are made high. If it were a woman of any caste then the region corresponding to the pelvis is made high. For a shudra the knee section is made high. Then the श्मशान is covered with moss and kusha grass.

If the land was plowed nearby to get soil for the mound then barley is sowed into the furrows. Then they dig two trenches in the south of the श्मशान and 7 to the north from west to east. The two to the south are filled with water and milk and those to the north with water. The ritualists then throw 3 stones into each of the 9 trenches they then return to their habitation after crossing over the trenches with अश्म्नवतीः forumula. The यजमान purifies himself by spraying an extract of the अपामार्ग plant on himself with the formula 'apA अघं..' and ritually bathes. Between the zone of habitation and the श्मशान the यजमान places a clod of earth with the formula imam जीवेभ्याः...

The adhvaryu then gives the यजमान ointments and perfumes to apply on himself and spread grass around the household औपासन fire. Then वरण wood is placed on the fire and offer ghee with the chant of agna आयुगुंषि... He then recites the protective formulae begining with pari ime gaM अनेषत... The adhvaryu then throws away the औपासन fire through an outlet other than the house door with the formula क्रव्यादं... and concludes with the formula iha eva ayam...

The fees for the rite is a chair with a cushion and a jowar sack or an old ox.

It may have helped in the development of the knowledge of skeletal anatomy in the early sacrificers.


