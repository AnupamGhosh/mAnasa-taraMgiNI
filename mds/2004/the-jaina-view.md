
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The jaina view](https://manasataramgini.wordpress.com/2004/09/24/the-jaina-view/){rel="bookmark"} {#the-jaina-view .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 24, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/24/the-jaina-view/ "6:09 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The jainas possess some interesting divergent variants of the canonical Indo-Aryan myths. The हरिवंश of jinasena and the त्रिषष्ठिसलकपुरुष charita of hemachandra is a rather rich source for such mythic material. The jaina view postulated the existance of 63 सलकपुरुषस् or notable figures who are like spokes in the wheel of time. Not unexpectedly the 63 includes the 24 tirthankaras. Other than these there are 12 chakravartins who start with the great bhArata. Then there are the 9 वासुदेवस् or the 9 नारायणस्, 9 balabhadras and 9 प्रतिवासुदेवस्. In each epoch of the time wheel is born a vAsudeva or a नारायण with an elder brother termed the balabhadra. A woman called a कृत्य is also born in each epoch to aid the vAsudeva in his acts. The evil in the world is spread by the प्रतिवासुदेव, who is finally killed in the show-down with the vAsudeva. Below are these entities:

**vAsudeva balabhadra प्रतिवासुदेव\
\
**त्रिपृश्ट vijaya अश्वग्रीव or हयग्रीव\
\
द्विपृश्ट achala तारक\
\
स्वायम्भुव dharmaprabha naraka\
\
पुरुषोत्तम suprabha nishumbha\
\
पुरुषसिंह sudarsana madhu and कैटभ\
\
पुण्डरीक Ananda प्रह्लाद\
\
दत्तात्रेय nandimitra bali\
\
लक्ष्मण रामचन्द्र रावन\
\
कृष्ण सम्कर्षण जरासन्ध

In Jain temple 49 of Vimalavasahi (Mt. Abu) there is a remarkable idol of the 16 armed vAsudeva पुरुषसिंह killing a प्रतिवासुदेव. The jaina version of the रामायण obviously has लक्ष्मण kill रावण the प्रतिवासुदेव and कृष्ण kill जरासन्ध rather than bhIma.

From the list of वासुदेवस् or नारायणस् and प्रतिवासुदेवस् it is clear that the jaina epicists had diverged from the Hindu mainstream early on and were drawing in an ad hoc fashion from a body of historical and mythological elements they had inherited from the mainstream. These were used to rebuild works that paralleled the mainstream सूत literature. The core elements of the original inheritance include the concept of periodic नारायणस् from the incarnations of विष्णु and their number, names of some of these incarnations and the names of demons. The concept of the vAsudeva and the balabhadra appear to have been acquired from the early form of the bhAgavata religion of the सात्वतस्, which later developed into the full-fledged पाञ्चरात्र system where they are the primary manifestation of the puruSha नारायण. This appears to have been further systematized to include all the incarnations of विष्णु. That the elements of the original inheritance were reordered in an ad hoc fashion is suggested by the several anachronisms such as the insertion of madhu and कैटभ in to the prati-vAsudeva list when they were actually killed by the un-incarnated विष्णु. Other demons like तारक who was killed by कुमार and naraka much later by कृष्ण were made earlier प्रतिवासुदेवस्.

Such conflations are also apparent in the bauddha घतजातक. It talks of the tale of vAsudeva and baladeva the sons of उपसागर and देवगब्भा the sister of कंस. In this tale vAsudeva and baladeva are brought up by a certain अन्धकवेण्हु and his wife नन्दगोपा who was an attendant of देवगब्भा. Here the name अन्धकवेण्हु is a corruption of the compound of andhaka-वृष्णि, the main clans of yadus from which कृष्ण hailed. One can see that the conflation of names coming from the ancestral mainstream source following their isolation of the bauddhas was similar to the case of the jainas. The chakravartins amongst the 64 jaina सलकपुर्षस् also seems to be a concept derived from the mainstream सूत histories which enumerate chakarvatins and the षोडषराजिक. But the jaina list a of chakravartins is a strange motley including mainstream rulers like bhArata, sagara and brahmadatta woven into jaina-centric legends and polemics.

The jaina concept of prati-vAsudeva may not be very original either. The भागवतं/ हरिवंश describe the showdown between कृश्ण and the false-vAsudeva (or the प्रतिवासुदेव), the पौन्ड्रक in which the latter's head is cut off. Thus, there may have existed the concept even in the mainstream that in the age when vAsudeva emerge there is competition for that status and the winner is the vAsudeva, while the loser the prati-vAsudeva. The jaina theory present the balabhadra as a more peaceful individual compared to the vAsudeva who slays the hostile elements of his epoch. The baladeva is often portrayed as the promulgator of peaceful jaina concepts. While the historical baladeva of the mainstream literature was hardly in a jaina tirthankara mould he did devote time to peace efforts even in the account of the हरिवंश. During the marriage of साम्भ he prevented a major showdown between the yadus and the kurus by his peace effort and a minimal threat of diverting a river to flood Hastinapura. He also refused to participate in the great bhArata war. After baladeva murdered the सूत लोमहर्षण and रुक्मिणी's brother rukma, he decided to retire from all military activities and he and his wives devoted themselves to shrauta practices. These may have provided the jainas with the inspiration for their idea of the baladeva.


