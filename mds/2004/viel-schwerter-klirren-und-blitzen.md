
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [viel Schwerter klirren und blitzen](https://manasataramgini.wordpress.com/2004/12/09/viel-schwerter-klirren-und-blitzen/){rel="bookmark"} {#viel-schwerter-klirren-und-blitzen .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 9, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/09/viel-schwerter-klirren-und-blitzen/ "4:51 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

So will ich liegen und horchen still,\
Wie eine Schildwach, im Grabe,\
Bis einst ich höre Kanonengebrüll\
Und wiehernder Rosse Getrabe.

Dann reitet mein Kaiser wohl über mein Grab,\
Viel Schwerter klirren und blitzen;\
Dann steig ich gewaffnet hervor aus dem Grab,\
Den Kaiser, den Kaiser zu schützen.

Around midday we hear a huge explosion on the western ramparts. An enormous hostile horde was trying to make its way through the western wall. We retaliated with rocket fire and advanced a wing of our wall-guards to kill the enemy sappers and fusiliers. While the rockets broke down and blew away many of the hostile climbers quite a few made it to the lower point in the fort via the gapping irrepairable hole we had talked about. A fierce hand to hand fight was fought there. Our men retreated under the intense assault and the foes started aiding a second sapping party. We were suffering from fever like Lachit Barphukan on the day of that historic encounter on the Brahmaputra against the army of Awrangzeb. We despatched our sachiva with a force counter the thrust at the breach. 15 minutes later I noticed the tide turning and our men were driving them out. We rolled in some rockets to hold the breach. The enemy had erected a 200 feet tower with platforms across the lower battlement and was preparing to launch rockets. There was an immense din on the walls of the lower fort as the storming parties were clashing swords with our men. Heads, arms, bodies were being hacked and streams of blood were flowing. We ordered an attack with trebuchets on the tower. The tower was hit twice but they quickly raised two new ones for the attack.

Our forces had repulsed the attack on the lower battlement. We were distracted momentarily by this success. Just then the foes began a bombardment from one of the towers and under the covering fire from the rockets on the fort walls their sappers moved foward. I suddenly realized the magnitude of the enemy armies, which we had vastly underestimated. The whole atmosphere was reverberating with the explosions of the cannonade. We saw their sappers trying to mine under our fort. I needed to send a party out destroy the miners.

....\
I remembered that eerie statement: "The one mantra that completes the sattra successfully may be missing"


