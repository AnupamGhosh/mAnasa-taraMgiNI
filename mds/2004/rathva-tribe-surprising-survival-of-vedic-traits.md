
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Rathva tribe: surprising survival of Vedic traits](https://manasataramgini.wordpress.com/2004/07/16/rathva-tribe-surprising-survival-of-vedic-traits/){rel="bookmark"} {#rathva-tribe-surprising-survival-of-vedic-traits .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 16, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/07/16/rathva-tribe-surprising-survival-of-vedic-traits/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Rathvas are a peculiar tribe in Gujarat whose art was studied by Jyotirindra Jain. Most tribes of India preserve certain aboriginal traits that have are often interlaced with Indo-Aryan tendencies. Most commonly the rudra cult of the the Indo-Aryans and its various ramifications has been used as a conduit for the assimilation of aboriginal tribes. Hence, the most frequent Indo-Aryan elements are associated with the worship of rudra and his entourage, and syncretization of local goddess into the pan-Indo-Aryan transfunctional goddess cult. This could have in the early stages involved the व्रात्यस् and para-Vedic Indo-Aryans, who often existed at the periphery of the orthodox Vedic Indo-Aryan world and often had special rites to the god rudra mahadeva.  We see very few  pristine vedic elements in these assimilated tribes of India.

However, the Rathva tribes of Gujarat appear to have been directly assimilated into the core of the vedic  religion. Importantly, their native religion and the religious paintings give a central place to the **[great god indra]{style="color:#33ff33;"}**. indra is worshiped by their shaman, the [बद्व]{style="color:#ff0000;"}, as the lord of heaven. The pictures of indra are painted on the wall and worshiped with offerings of grains and occassionally animal sacrifices. The main animal of the Indo-Aryans, the horse, figures prominently in their paintings and their epithets of Indra is remniscent of the vedic worship of Indra. Importantly, though the Rathvas have [गणपति]{style="color:#6666cc;"} and [राणी काजल्]{style="color:#3366ff;"}, a potential trans-functional goddess, they do not have any memory of the other prominent deities of classical Hinduism.  Thus, it would appear that the Rathvas were Aryanized by the mainstream vedic cult, and after that largely isolated until modern times from the mainstream. The acquisition of the late पौराणिc deity gaNapati could be explained as a lateral transfer from the rudra peripheryof the classical Indo-Aryan world.

This would require that the Rathvas were Aryanized very early on, perhaps around 500 BC, or earlier, and then they diverged away rapidly. A plausible hypothesis is that they represent a late survival of an early branch of Vedic Indo-Aryans (most probably a kshatriya band) who penetrated deep into the Bhilala territory and merged with them, thereby giving rise to the Rathvas. Thus, they may have incorporated elements of the archaic vedic religion into these tribes and also introduced agriculture in their midst.  The possibility of the archaic Aryanization is suggested by the finding of Rathva style paintings in the Navadatoli pottery specimens.


