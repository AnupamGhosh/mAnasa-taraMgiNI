
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [लक्ष्मी तन्त्रं](https://manasataramgini.wordpress.com/2004/02/09/lakshmi-tantram/){rel="bookmark"} {#लकषम-तनतर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 9, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/02/09/lakshmi-tantram/ "12:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the most lucid expositions of the foundational philosophy of पाञ्चरत्र is seen in the lecture given by लक्ष्मी to indra in the लक्ष्मी-तन्त्रं (chapter 4):


 1.  Originally लक्ष्मी and विष्णु are conjoined in an undifferentiated form termed the "pure space". This combination of विष्णु and श्री is termed the homogeneous state which cannot be discerned and with out any fluctuations in it.


 2.  From time to time a minute particle comprising of a 10^-18 विष्णु-श्री continuum state differentiates into action. Such emergent particles are endowed with a set of rules (सिसृक्ष) that can cause emergence of universes from them.


 3.  लक्ष्मी as that particle of her own substance instantaneously develops into the primary emanation- the विशुद्धाध्वा. This is filled by the activity of लक्ष्मी that manifests in the form of rays filling it, vibrating with energy, and driving its evolution. Thus, this primary emergent comes out of that still fluctuation free विष्णु-लक्ष्मी continuum.


 4.  6 attributes are seen together in equal proportion in लक्ष्मी associated with the primary particle that stirs out of the primary विष्णु-श्री continuum: ज्ञान, shakti, bala, aishvarya, वीर्य and tejas. There is no fluctuation of these attributes in this initial state of लक्ष्मि and the results in a manifestion called vAsudeva. When only  ज्ञान and bala become manifest emerges the form termed संकर्षण. When only वीर्य and aishvarya are manifest the form is pradyumna. When only shakti and tejas are manifest the form is aniruddha. In all these forms the remain tattvas of the 6 are present but unmanifest.


 5.  These are the 4 व्यूहस् that further divide themselves into 3 resulting in 12 व्यूहान्तरस्. The vAsudeva व्यूहान्तरस् form the basis of existance, the pradyumna the origin of activities, the aniruddha consequences of the activities and everything is destroyed or merges into संकर्षण.


 6.  The above is considered the "non-phenomenal" or an ideal (in the sense of Plato) emanation . Then there is the phenomenal emanation made of 3 गुणस्. The  ज्ञान gives rise to sattva, aishvarya to rajas and shakti to tamas. लक्श्मी drives the phenomenal universe's emanation by initiating a disequilibrium of the perfectly equilibriated 3 गुणस्. At first the rajas manifests and drives the emanation of the universe. This rajas is worshipped as महालक्ष्मी also known as महाश्री, चण्ड, chaNDi and चण्डिका.

To fill this 'empty' universe that has emerged as result of लक्ष्मी's activity the तमोगुण comes to the forth. This form is worshiped as तामसी. The goddess तामसी has mighty arms holding a sword, a pot of beer, a severed head and a shield. She wears a garland of severed heads and a diadem of snakes.

She is called महाकाली, महामाया, महामारी, क्षुधा, तृशा, निद्रा, तृश्ण, एकवीरा, कालरात्री and दुरत्यया.

Then the universe is filled by the emanation of sattva. This is worshiped as a goddess termed महाविद्या holding in her mighty arms a rosary, goad, a vINA and a book. She is also called महावाणी, भारती, वाक्, सरस्वती, AryA, brAhmI, महाधेनु, गीह्, दीह् and वेदगर्भा.


