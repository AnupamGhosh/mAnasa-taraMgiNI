
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Mythologists](https://manasataramgini.wordpress.com/2004/12/24/mythologists/){rel="bookmark"} {#mythologists .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 24, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/24/mythologists/ "7:18 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

So impressed was Jung by the mental patients that he believed the origins of myth lay in mental disease. With this conclusion Jung set rolling many a modern western critique of myth. The first sterile offshoot was that of Freud, whose own 'diseased mind' produced certain unfalsifiable constructs that he used to deconstruct myth. Freud's many successors live in a certain dark world where the light of scientific practice has still not shone. There was Frazer who noticed the omnipresence of several motifs in the mythosphere thus formulating the raw material for studying the raw materials for the origins and evolution of myth. Campbell who while furthering these motifs fell to the falacies of Freud in attempting explanations- as in his eerie book: the hero with a thousand faces. Dumezil saw the importance of the evolutionary principle: Basically myths evolve through duplication, divergence and recombination from ancestral motifs. Von Dechend and Santillana saw the importance of the underlying cosmological models that are contained within the language of myth. They defined a few great seeds that inspired some of the most persistant motifs of the mythosphere. Staal and his successors restored neo-Jungism (though avoiding any direct association with the old Germanic- after all he was too tainted in his associations and his modern imitators need to sound modern). Mental disease was indeed the well-springs of the archetype of myth.\
\
Once I mused in the midst of my despicable plebeian band:\
\
"The cytoskeletal kinase ळिम्ख़्१, which is downstream of the protein neuregulin is mutated and you have Williams syndrome. These unlucky souls see everything, even highly mundane objects, in a highly [anthropomorphic ]{style="font-weight:bold;color:rgb(51,255,51);"}form and exhibit hypersocial behavior!"

Neo-Jungians believe here in lies the impetus for the mythic archetype: The primitive myth-making human as a borderline case of mental disease (Then verily the Hindus were in this state of mental disease until pretty recently they might slyly add).

Nevertheless, what the case of Williams syndrome illustrates is that there is a mental template for the form myths may assume. For this we discard certain recent pronouncements on the topic and seek the following for the grand synthesis of the understanding of myth:\


 1.  The evolutionary aspect: divergence and recombination from an ancestor through replication.\


 2.  Do convergent myths arise repeatedly due to constraints of neural architecture.\


 3.  What is the role in external inspiration on myth:1) was it a repeated seed. 2) was it a repeated reinforcement of the ancestral myths diverging away.\


 4.  What is the role of internal inspiration: the neural architecture (including the aspect of brain disease) was a mere reinforcement or a repeated generator.\
\
We need to synthesize these points into an explanative model.


