
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Encounter continues](https://manasataramgini.wordpress.com/2004/12/02/encounter-continues/){rel="bookmark"} {#encounter-continues .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 2, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/02/encounter-continues/ "5:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As we have a long day ahead of us tommorow we shall be curt. We did surprisingly well on front #3 and we hope the gods favor us again in a similar encounter tommorow. We also obtained some clue regarding our ranking in the midst of the warring states. We believe we were evenly matched with our adversary. But as every one knows battles can be lost on small points. Certain unusual delays that our rivals were subject to favored us on the front. More mysteriously the same omen continues unabated, but we are not bothering to check it means good or bad according to the shakuna शास्त्र.


