
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Battle of हिमप्रारम्भ](https://manasataramgini.wordpress.com/2004/12/03/battle-of-himaprarambha/){rel="bookmark"} {#battle-of-हमपररमभ .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 3, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/03/battle-of-himaprarambha/ "4:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We saw that front #3 was a grand success in yesterday's hostilities. We first blunted the attack by raising the water in the moat and causing the enemy troops to retreat. Then we used the naphta trick that we discovered after a terrible defeat a few winters ago. Invoking deva सविता and taking our time to launch counter-offensive we went heavy on naptha again today. The hostile forces suffered heavy losses and retreated in disarray. We pursued them relentlessly and destroyed the pockets of resistance. Thus, we had contained the enemy name "kosha". On front #2 we were pressed hard but out fortifications held fine and the baggage train was protected even as the enemy made several attempts to bang it. Thus, the enemy named "gandharva ratha" could not make significant progress against us and retreated. On front #1 we were the weakest. We kept shielding it from the enemy vision with smoke screens. The enemy could launch a strong attack on this front to retaliate agains the defeat in front #3. We cannot do much but the simply wait and take the attack, because the gaping hole was just too much to repair. Over the next few days fighting may intensify on this front. The first salvo could come as early as in an hour from now. But I will be retiring to rest after heavy fighting and will leave it to the staff to tackle.

However 2 things are coming in focus. 1) If we are badly beaten on front #1, still it will not be a complete defeat for us. The battle would have been a draw and the boundaries of the kingdoms will not change much. 2) If we win front #1 by some chance we will still not have a great victory- at best we would be able to capture some desert territory and some "Siachins" from the foes.

The downside is that we are unlikely to be able to hit the enemy capital anymore and we may need to settle for holding "strategic territory". We also see the possibility of the captured territory becoming our undoing-swallowing poison. But we are indeed between the rock and the hard place. If we do not capture territory we could stil be destroyed in the future due our inability to real do anything on front #1.

Front #1 reminds one of the maxim: "There are things that lie beyond the best made human plans". Conan Doyle said something like that.


