
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Max Mueller: good, bad or a Malhotran U-turner?](https://manasataramgini.wordpress.com/2004/04/12/max-mueller-good-bad-or-a-malhotran-u-turner/){rel="bookmark"} {#max-mueller-good-bad-or-a-malhotran-u-turner .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 12, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/04/12/max-mueller-good-bad-or-a-malhotran-u-turner/ "2:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

It is common these days to find many Indians stating that Max Mueller was an India-hater and a hand-maid of Macaulay. Max Mueller was the son of a well-known German poet and influenced greatly by the German scholastic community of his era. His ideas should, hence, be viewed as a product of that continuum and era. It is true that he never visited India; hence he definitely had a poor grasp of the first-hand subtilties of India. His information was also being filtered through the English Raj, which was obviously a barrier to objective information on the Hindu tradition. But does not automatically disqualify him --- after all one of the founding fathers of the modern Indo-European Urheimat theory, the Jewish Indo-Europeanist Theodor Benfey (who taught himself Sanskrit in four weeks) never visited India. Closer to our times, the prodigious Sanskritist Jan Gonda also never visited India but deeply penetrated Hindu thought (much more than the average Hindu Sanskritist).

That said, let us see what some contemporary Hindus thinkers had to say about Max Mueller. Swami Vivekananda, a great Indian reformer and patriot said about him: "Max Mueller is a Vedantist of Vedantists. He has, indeed, cought\[sic] the real soul of the melody of the Vedanta." This is a peculiar statement regarding someone who is these days commonly regarded as detrimetnal to Indian interests.  It is interesting to note that multiple Indian freedom fighters followed Lokamanya Tilak in terming him mokSha मूल भट्ट of go-तीर्थ (Oxford)! In 1897, when Tilak was charged of sedition by the English court he tabled a representation to the British government along with the Iranic Dadabhai Nowroji to discharge Tilak. This move shortened his imprisonment by 6 months. If one reads his book '*India: What it can teach us*' it is in large part sympathetic towards Indians (especially if Annie Besant, who supported the Jawlianwalla massacre can be considered sympathetic, as our school history books told us). Ironically his efforts to translate the vedic texts were critical in allowing many Hindus to get some idea of the vedic texts -- we should realize that understanding the vedic language is not a joke, and only a small section of highly educated ब्राह्मणस्, who were facing extinction, could really fathom its complexities. While it is good to study vedic in its original, it is not easy at all. Given this background MMs efforts ironically helped many Indians themselves to get a glimpse of the veda and start their new round of interpretations (including Vivekananda and Aurobindo).

At the face of it, there is no evidence that MM was a British agent trying to subvert Indian identity. This did happen but we have to look elsewhere. So what really happened?

If we look at the early stage of MMs career, then we see that he was sympathetic towards India. In the later phases of his life, he suddenly found himself making a U-turn (vide Rajiv Malhotra): In the 1860s he found himself actually refused professorship by the British because in part of his pro-Indian and Indian-influenced world views. He was even accused of  a "crusade against Jesus and Christianity" (Bishop Munro). We posit that this had it is effect on him and placed pressure on him to conform to the European norms. In 1880s he started translating Kant, here he started rediscovering his Western identity, which had been smudged by his dabbling with the "Orient". In the process he started making statements like: "If I live for any purpose it is for this, that I will preach the union of Eastern and Western Theism, the reconciliation of Europe and Asia. The idea may seem absurd to many in the present age. It may provoke ridicule and angry reviling. But posterity will prove a better judge."

The synthetic phase, where he started becoming obsessed with his own ideas, set in soon there after. This gradually completed the U-turn as he started placing things in his own occidental categories. Yet, he did not completely give up his attachment to India, and perhaps to the end of his life he even returned to it: It  showed up in works like Ramakrishna \[Paramahamsa] (which he wrote in his later life; compare with Kripal's lurid depiction of RK as a homoerotic tormented soul) and "*Six systems of Indian philosophy*" that he wrote just before his death. So I think we have to look elsewhere to find the real subversionists of Indian identity and

To follow his U-turn let us look at the following quotes of his:

"*The Hindus were a nation of philosophers. Taken as a whole, history supplies no second instance, where the inward life of the soul has completely absorbed all the practical faculties of the whole people, and in fact almost destroyed those faculties by which a nation gains its place in history. It might therefore be justly said that India has no place in the political history of the world*".

He cites Schopenhauer:\
*"In the whole world there is no study so beneficial and so elevating as that of the Upanishads. It has been the solace of my life -- it will be the solace of my death".*

His speech to the brown sahibs of the ICS:\
*"If I were asked under what sky the human mind has most fully developed some of its choicest gifts, has most deeply pondered over the greatest problems of life, and has found solutions of some of them which well deserve the attention of those who have studied Plato and Kant -- I should point to India. And if I were to ask myself from what literature we here in Europe, we who have been nurtured almost exclusively on the thoughts of Greeks and Romans and of one Semitic race, the Jewish, may draw the corrective which is most wanted in order to make our inner life more perfect, more comprehensive, more universal, in fact more truly human a life, not for this life only, but a transfigured and eternal life -- again I should point to India".*

Thus we can present the following course:


 1.  He reads the Sanskrit literature and sees the mark of a great civilization in it authors --- in fact he recommends it as corrective for his own civilization.


 2.  We have him espouse the view that India had no history of its own because it was nation of philosophers. This continued to be the western view for a long time. But at least he credited us with having a refined philosophical faculty (more on this below). He could not reconcile the glory of Hindu philosophical thought with the decadent state of India he saw through the British portrayal: a defeated nation that had been conquered by a hand-full of white men. A nation, which prior to the British conquests (according he British histories), had conquered by the Mohammedans. So he came up with his interpretation of India espoused in the first quote. Given the background, I would tend to interpret him as being taken in by the \*British* propaganda of the "Force of history" where it was the natural destiny of a nation like India with no historical sense to fall into the hands of the Christian white men.

Finally, note the elements of the newest round of Euro-American propaganda:


 1.  At least MM thought we had philosophy, but recently many Western scholars have been peddling the view that उपनिषद्स् never mattered to the Indians: it was only after the West (MM, AS etc.) and Moslems like Dara Sukoh recognized it, did the elite Indians reading the English translations of the उपनिषद्स् consider them philosophical high points.


 2.  Modern Western scholars work hard to deny the civilizing role of India on the far East and Central Asia that earlier Western scholars noted. Instead they give this credit to China. Note Jared Diamond's "*Guns, Germs etc*" where he entirely ignores the history of billion people and the sub-cultures influenced by them.


 3.  Several modern Western scholars also present the view that उपनिषद्स् and Hindu philosophy have nothing to teach other than obscure hocus-pocus.


 4.  Having scrapped India of its philosophical achievements that their old predecessors were willing to concede, the modern Western scholars paint a new picture of India: "evil" of caste, poverty, lawlessness, "evil" of recognizing one's Hindu identity and rootless, ill-mannered techie slaves with a taste for positive western influences.

These are often done by brown sahib sepoys for their overt or covert Abrahamist गौराङ्गनाथस्. Thus, I would posit that the actual process of demonization has increased, though it has shifted to more subtle directions in certain quarters of academia (couching "Hindus as idiots" in all kind of verbiage passing off as nuance) and this is what we need to concentrate on.


