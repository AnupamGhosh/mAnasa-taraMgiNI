
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Sadness](https://manasataramgini.wordpress.com/2004/08/17/sadness/){rel="bookmark"} {#sadness .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 17, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/17/sadness/ "3:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The smart and ever-scheming GS had been my student some years ago. He came from a साम vedic family of भरद्वाजस्. His academic record had been a long string of successes, like the victories of Alexander of Macedon. I had lead him to a few glorious victories and he had paid me suitable [दक्षिण]{style="color:#ff0000;"} for that. He put to good use what he learned from me as he landed one great job after another, making enormous fiduciary killings on the way. He was always the typical Indian sincere kind, working unbelievably hard at anything he did. His energy was not unlike that of the**[ princess of Hayastan]{style="color:#33ff33;"}**, and he could go on at one meal a day for long. He never really cared if his food tasted good or bad, he never cared for entertainment nor those long-winding chatter sessions with our chosen ones that the rest of us blithely gave ourselves to.

In his personal life too he was well endowed- he had furthered his genes severally, owned large houses and horses, was in possession of an adequately endowed woman. In short he compared very well to the "[एको मानुष आनन्दः]{style="color:#ff0000;"}" that the vedic texts have defined. I was happy that my student had made such exemplary progress and made good use of his lesson and adopted their purport well. While some other disciples of mine became my real friends, GS and me never really became friends, though kept in good contact with each other after parting. We even occasionally collaborated and speak to each other in positive and pleasant ways. So when GS called me one fine day, I spoke to him at some length. All of a sudden, I heard from this individual who may be considered by others as the "**[एको मानुष आनन्दः]{style="color:#33ccff;"}**" that he was profoundly sad. On asking why this was so, he answered that he felt dejected that he was not living up to his ambitions. He wanted to be greater, he wanted to be better known, and he wanted to be better appreciated.

Ambitions I realized are a bad things, and cravings for attention even worse. The lord of the heavens, the great god indra, is said to give things in balance. You may have one good thing from him, but everything else may be missing. While those very things may belong to someone else! As my progenitor would say, the devas gave the ब्राह्मणस् many things, but he made most of their women ugly. It may be better to live to survive than to live for ones ambitions, I realized. But there is a second part to this state that leads to true success in some.


