
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Indus iconography and its parallels](https://manasataramgini.wordpress.com/2004/11/22/indus-iconography-and-its-parallels/){rel="bookmark"} {#indus-iconography-and-its-parallels .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 22, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/22/indus-iconography-and-its-parallels/ "6:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As Thomas McEvilley pointed out there are numerous connections between Indus Valley (IVC) Iconography and those in the Middle East. The real significance of these clear connections have hardly been understood.


 1.  The Central strongman figure wrestling lions on either side symmetrically.\
This is seen in period 2000-3000 BC in the Sumerian seals. There are two IVC seals with the same image, one with the Indus script and one without from around the same time. Much later around 500 BC it is seen amongst the Achaemenid Iranians.


 2.  The bull-head geared male and female figures. These figures commonly known as the "Pashupati" figures are seen on numerous Indus seals and pottery. There is one seal with the bull-headed male and female figures and a tree with 7 subsidiary figures with sprout headgears and plaited locks. A fairly reasonable parallel of these figures, including both the sprout heads and the male and female figures is seen amongst the Sumerian seals. The bull-headed female figure is also seen in an Akkadian seal from 2000-3000 BC. Standing male bull-headed figure seen in the Indus plaques is very similar to an ivory standing bull-headed figure amongst the Sumerians.


 3. The 3 headed bull. This figure is seen in at least two distinct Indus seals with different Indus "script" markings on it. A similar 3-head bull figure is seen amongst the Elamites.


 4.  The svastika figure formed by 4 entwined human or animal figures are seen in both IVC and a Syrian seal from around 1700 BC.


 5.  The two goats prancing symmetrically towards a central plant figure. Seen in both a Mohenjo Daro seal and one from the ancient Uruk period of Sumer.

These no doubt suggests an interaction between Sumeria and the IVC. But more than that it suggests a potential cultural congruence is certain respects between these two civilizations. But the significance of it remains very puzzling.

A western commentator who noted these similarities remarks: "Indeed, so receptive to this material was the IVC that it seems to have taken the position of the culture that feels less developed and hence borrows from the one that seems more developed(Mespotamia)..."

However we see absolutely no reason to postulate such a one way direction.

And what does all this have to do with the Aryans?


