
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The rite of the solsticial day](https://manasataramgini.wordpress.com/2004/12/17/the-rite-of-the-solsticial-day/){rel="bookmark"} {#the-rite-of-the-solsticial-day .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 17, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/17/the-rite-of-the-solsticial-day/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The old solsticial rite of the Indo-Aryans is performed on the second last-last day of the year (महाव्रत). The description of the oldest version of the rite is seen in the पञ्चविम्श, aitareya and शाङ्खायन ब्राह्मण. The clearest explanations of the ritual actions are found in the शाङ्खायन shruti. It appears to preserve some of the archaisms of the Proto-Indoeuropean form of the rite. It was literally round the clock and one requires considerable energy to perform it successfully.


 1.  It begins in the morning with a बहिष्पवमान soma rite. The hotar chants the Ajya (the chants composed by the काण्व with one ऋक् for every month of the year:12 ; then the chants composed by वसिष्ठ with one ऋक् to agni for every moon cycle in the year + 1 ऋक् (head of prajapati) for the remaining days that add up to the solar year: 24+1=25 ) and prayoga shastras (chants to indra, वायु, mitra, वरुण, ashvins and sarasvati) and the प्रशास्त, ब्राह्मणच्छम्सिन्, potar, नेष्टर्, अग्नीध्र and अच्छावाक (the hotraka priests) chants the 3 Ajya शास्त्रस्.


 2.  In the midday pouring the माध्यन्दिनपवमान and the पृSठ पवमान stotras and the मरुत्वतीय (the nivids are recited in ekashruti between the vasukra aindra chants in this शास्त्र) and निष्केवल्य shastras (respectively to indra with maruts and indra) are chanted


 3.  In the evening the soma pouring is made with the आर्भवपवमान stotra, यज्ञ्नायज्ञ्नीय सामn and the vaishvadeva and आग्निमारुत shastras.


 5.  Then the performers rest for the night session as the sacrificial place is swept and the various accessories are brought and set up.

  - A swing of udumbara fig wood is made for the hotar to sit on.

  - A seat for the उद्गातर् with a darbha grass cushion is made.

  - The adhvaryu stands on cushion of kusha grass while chanting the yajuses.

  - The brahmA and other priests get reed mats.

  - A वाण with 100 strings, with the body made of udumbara and handle of पलाश woods, covered with a brown ox hide is readied for the उद्गातर् to accompany the साम gAnaM. A reed bow decorated with leaves is used to play it.

  - Following musical instruments to accompany the साम गनंस्: अवघटरिका (a pot instrument), अलाबुवीणा (a vINA), घाटकर्करि (the pot-cymbals?), गोधावीणाख़ा (vINA), काण्डवीणा (vINA) and पिcचोरा (flute) are readied.

  - 6 मृदन्ग drums are placed all around the saman singers

  - behind the आग्नीध्र altar a hole is dug up in a hemispherical form and covered with the hide of a sacrificed bull. This is the earth drum that is played to accompany the साम gAnaM.

  - a round piece of hide, held up by two posts is set up as an archery target to the left of the आग्नीध्र fire.

  - a chariot with a single horse with a bow and 3 arrows are readied for the kShatriya to use.

  - pots filled with water are readied for the dancing maidens


 6.  At night the प्रातरनुवाक chant comprising of 1000 ऋक्स् to indra from the ऋग् and atharvan is chanted by the priests. Then an animal or a flour model of an animal is offered to indra and agni. The sacrificial implements are then washed carefully outside the sacrificial area and brought to the sacrificial hall, the sadas, where the guests and witnesses are seated.


 7.  Then the माहेन्द्र cups are filled and soma offerings made with the yajur vedic mantras to indra.


 8.  Then the adhvaryu goes before the hotar's fire altar. The hotar utters the प्रैष "adhvaryu, now stop". The hotar then leaves the sadas and goes around the आग्नीध्र's altar and bending his right knee, takes with a sruk made of udumbara wood, and makes 8 offerings of ghee to the devas. He then puts down the sruk and stands in front of the sadas, north of the shruti altars, and faces east and in a low tone chants the परिमाद् formulae.

  - The उद्गातर् sings the आञ्गिरस, भुतेछद्, krosha, anukrosha, payas, arka and अर्कपुष्प सामन्स्, with hotar in a low tone muttering the परिमाद्स्.

  - He then invokes सविता in the sacrificial hut and returns to the sadas and holds the swing from behind. He then instructs the adhvaryu as to how to proceed and the prastotar regarding the stotriyas.


 9.  The adhvaryu then initiates the महाव्रत सामn. The उद्गातर् mounts his udumbara stool and the other ब्राह्मणस् sit on their mats. The उद्गातर् picks up the 100-stringed वाण and begins playing it, singing the महाव्रत सामn. The wives start playing on the other instruments that were brought in, while the drummers start beating the मृदन्गस् and the earth drum. Maidens water pitchers on their heads dance thrice to the left round the मार्जालीय altar singing the madhu gAnaM. Then they dance thrice to the right in silence. The horse is then yoked to the chariot and the kshatriya mounts it and taking the bow and three arrows encircles the vedi and shoots the target even as he is riding (these are stick to the target marking the 3 stars of Orion).


 10.  The prastotar signals to the hotar with the formula 'A velA'. The hotar then pulls the swing towards him and performs प्राणायांअ thrice and chants some गाथस्. He then holds the swing stretches his feet forward and performs प्राणायांअ once. He then sits on the swing and with his right hand touches the back of the swing and chants the prathama प्रतिहार and concludes with a प्राणायांअ. The end of the chant all the drums stop beating and the maidens put down their jars on the मार्जालीय़.

  - The prastotar then utters the formula 'एषा'. The hotar then begins chanting the निष्केवल्य शास्त्र to indra.

  - The chants corresponding to different set of bones of the body are chanted

  - The 9 sets of 80 ऋक्स् to different devas corresponding to the 720 days and nights of the normalized year are chanted.

  - Then he chants a 100 त्रिष्टुब् ऋक्स् to indra, followed by the हिराण्यस्तूप chant and the यातऊतीय chant. Then the procession of the nakshatras in the sky is enacted by the combination of the सजनीय chant (RV2.12) and RV2.14 which together constitute 27 verses, one for each nakshatra. The adhvaryu chants the nakshatra hymn for the 27 nakshatras. This is followed by the सूक्त of of विश्वामित्र begining with A याह्य् अर्वाङ्...

  - finally the उदुब्रह्मीय hymn with विहरण चन्दसं permutation is chanted.


