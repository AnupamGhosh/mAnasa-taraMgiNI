
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some works of नारायण भट्टथिरि](https://manasataramgini.wordpress.com/2004/08/21/some-works-of-narayana-bhattathiri/){rel="bookmark"} {#some-works-of-नरयण-भटटथर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 21, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/21/some-works-of-narayana-bhattathiri/ "6:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

ंएल्पत्तूर् नारायण भट्टथिरि, was a great nambuthiri sanskrit writer who apparently lived for a 106 years (born 1560) after escaping death due to syphilis earlier in his life. He wrote a remarkable vaishNava tract, the नारायणीयं, which is a glorious work of sanskrit poetry despite its extremist stance. प्रक्रिया sarvasva was his famous treatise on संस्कृत् grammar that took the place of the siddhAnta कौमुदी of भट्टोजि दीक्षित in the midst of the nambuthiris. He also revolted against the पाणिनिअन् grammatic tradition by composing the आपाणिनीय प्रामाण्य साधान where he built upon his teacher achyuta पिशारडि's work of non-पाणिनिअन् approach to grammar.

He wrote one half of the मीमांस text मानमेयोदय. Also well known is his निरनुनासिक prabandha a work without nasals describing शूर्पणखा lament to रावण after लक्ष्मण had cut off her nose. He also composed many works in the champu style.

In the नारायणियं he shows his link to the great nambuthiri astronomers, by giving the date of his composition in the katapayadi system as आयुरारोगयसौख्यं.


