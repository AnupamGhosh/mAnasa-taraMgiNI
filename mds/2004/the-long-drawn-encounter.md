
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The long drawn encounter](https://manasataramgini.wordpress.com/2004/12/19/the-long-drawn-encounter/){rel="bookmark"} {#the-long-drawn-encounter .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 19, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/19/the-long-drawn-encounter/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We repulsed another attack on our position by the brigade from dundubhidurga. Seeing that dundudbhidurga was strategically placed to fire rockets at us, we decided to use the confusion of our foe to take the fort. I set out with a force of commandos and attacked dundubhidurga. We detonated a part of the wall an got in. We face some opposition but as our men had taken the commanding heights we were able to kill the defenders. We destroyed much of the defending party and a few survivors go away. We are however in a quandry as to whether to garrison the fort or simply blow it up. We still have a few days to decide, but the news reached us that our ally in सुवर्णपुर was facing a grim assault. If he is overthrown we may face a dire situation and may need to withdraw any garrison placed for this fort. If we blow up the fort we simply may have no leverage on the enemy territory. The enemy had cleverly moved all their supplies and resources from dundubhidurga- so in a sense it was an empty conquest.


