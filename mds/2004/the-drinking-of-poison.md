
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The drinking of poison](https://manasataramgini.wordpress.com/2004/12/23/the-drinking-of-poison/){rel="bookmark"} {#the-drinking-of-poison .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 23, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/23/the-drinking-of-poison/ "7:14 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While the भागवतं is considered a vaiShNava text, it contains an highly magnified stotra to rudra. The stotra contains yet another example of the classical macrantropic motif that is so pervasive in Hindu thought since the puruSha सूक्तं and the ashvamedha ब्राह्मणं. It is full-fledged development was in India and not in Mesopotamia or Egypt. I provide a translation of the stotra given that the available ones are unsatisfactory.

[श्री-प्रजापतय ऊचुः :/]{style="color:#ff0000;"}

The प्रजापतिस् said:

[देव-देव महा-देव भूतात्मन् भूत-भावन/]{style="color:#ff0000;"}\
[त्राहि नः शरणापन्नांस् त्रैलोक्य-दहनाद् विषात्//]{style="color:#ff0000;"}

O god of the gods, the great god, the basis of the elements and origin of the elements, we surrender at your lotus feet. Save us from this poison, which is burning the three worlds.

[त्वम् एकः सर्व-जगत ईश्वरो बन्ध-मोक्षयोः/]{style="color:#ff0000;"}\
[तं त्वाम् अर्चन्ति कुशलाः प्रपन्नार्ति-हरं गुरुम्//]{style="color:#ff0000;"}

You are the one lord of all worlds; isvarah, cause of bondage and liberation. We worship you seeking good fortune, who is the mitigator all the distresses of the votary, and the preceptor.

[गुण-मय्या स्व-शक्त्यास्य सर्ग-स्थित्य्-अप्ययान् विभो/]{style="color:#ff0000;"}

[धत्से यदा स्व-द्ऱिग् भूमन् ब्रह्म-विष्णु-शिवाभिधाम्//]{style="color:#ff0000;"}

By your own energy you set the गुणस् in action; oh god you cause the emergence, maintenance and annihilation of this world when you oh great one manifest as brahmA, विष्णु and shiva.

[त्वं ब्रह्म परमं गुह्यं सद्-असद्-भाव-भावनम्/]{style="color:#ff0000;"}\
[नाना-शक्तिभिर् आभातस् त्वम् आत्मा जगद्-ईश्वरः//]{style="color:#ff0000;"}

you are the brahman, the primal secret cause of the manifest and the unmanifest \[states of the universe] as well as cause and effect. With diverse forms of energy you manifest; you are the consciousness {atman}, the lord of the universe.

[त्वं शब्द-योनिर् जगद्-आदिर् आत्मा प्राणेन्द्रिय-द्रव्य-गुणः स्वभावः/]{style="color:#ff0000;"}\
[कालः क्रतुः सत्यम् ऱितं च धर्मस् त्वय्य् अक्षरं यत् त्रि-व्ऱिद्-आमनन्ति//]{style="color:#ff0000;"}

You are the source of sound, origin of the universe, consciouness, living matter, sensory substances, the atoms, the गुणस्, propertied of matter, time, action, and natural law (ऋत) comprised of satya (inviolable laws) and dharma (the enforced laws). They say the syllable, which comprises of the 3-fold form (a-u-m) is for you.

[अग्निर् मुखं ते ऽखिल-देवतात्मा क्षितिं विदुर् लोक-भवाङ्घ्रि-पन्कजं /]{style="color:#ff0000;"}\
[कालं गतिं ते ऽखिल-देवतात्मानो दिशश् cअ कर्णौ रसनं जलेशं //]{style="color:#ff0000;"}

Fire is your mouth, the breath of all gods; the earth is known to be your lotus feet the source of the worlds; time is your change of state, the sum-total of the gods your and the directions are your ears and water is you taste.

[नाभिर् नभस् ते श्वसनं नभस्वान् सूर्यश् च चक्षूंषि जलं स्म रेतः/]{style="color:#ff0000;"}\
[परावरात्माश्रयणं तवात्मा सोमो मनो द्यौर् भगवन् शिरस् ते//]{style="color:#ff0000;"}

The coulds are your navel, the atmosphere your breathing, the sun your eyes and the \[celestial] waters; verily your semen. Your self is the shelter of all beings, primary and secondary, the moon your mind and the heavens your head.

[कुक्षिः समुद्रा गिरयो ऽस्थि-सङ्घा रोमाणि सर्वौषधि-वीरुधस् ते/]{style="color:#ff0000;"}\
[छन्दांसि साक्षात् तव सप्त धातवस् त्रयी-मयात्मन् ह्ऱिदयं सर्व-धर्मः//]{style="color:#ff0000;"}

The oceans are your abdomen, the mountains your skeleton, and the plants and trees are your hairs. The \[Vedic] Meters are verily the seven constituents of your body the three-fold vedic chants your metabolism, and the laws your heart.

[मुखानि पञ्चोपनिषदस् तवेश यैस् त्रिंशद्-अष्टोत्तर-मन्त्र-वर्गः/]{style="color:#ff0000;"}\
[यत् तच् छिवाख्यं परमात्म-तत्त्वं देव स्वयं-ज्योतिर् अवस्थितिस् ते//]{style="color:#ff0000;"}

Your \[five] faces are the five mantras of the [महानारायणोपनिषत्], oh lord, by which thirty-eight fold mantras* have merged. You are the one renowned as shiva, the self luminous one, by whom the state of knowledge of the परमात्मन् is attained.

[छाया त्व् अधर्मोर्मिषु यैर् विसर्गो नेत्र-त्रयं सत्त्व-रजस्-तमांसि/]{style="color:#ff0000;"}\
[साङ्ख्यात्मनः शास्त्र-क्ऱितस् तवेक्षा छन्दोमयो देव ऱिषिः पुराणः//]{style="color:#ff0000;"}

Your shadow is the wave of adharma, by which emerge \[many of its manifestations]; your three eyes are \[the three गुणस्],sattva, rajas and tamas, you are the foundation of सांख्य thought, the shastras with metrical verses are made by your glance. You are the ancient god and seer.

[न ते गिरि-त्राखिल-लोक-पाल-विरिञ्च-वैकुण्ठ-सुरेन्द्र-गम्यम्/]{style="color:#ff0000;"}\
[ज्योतिः paraM yatra rajas tamash cha सत्त्वं na yad brahma nirasta-bhedam//]{style="color:#ff0000;"}

Oh mountain-dweller, neither the lords of the world, nor brahmA, nor विष्णु, nor the lord of the of the gods (indra) can perceive, you primal brilliance, which is the brahman, in which sattva, rajas and tamas lose their distinction.

[कामाध्वर-त्रिपुर-कालगराद्य्-अनेक-भूत-द्रुहः क्षपयतः स्तुतये न तत् ते/]{style="color:#ff0000;"}\
[यस् त्व् अन्त-काल इदम् आत्म-क्ऱितं स्व-नेत्र-वह्नि-स्फुलिङ्ग-शिखया भसितं न वेद//]{style="color:#ff0000;"}

काम, the sacrifice \[of दक्ष], the tripuras, the poison of time and many other molestors of beings have been slain by you. We do not praise those acts, because do we not know \[that] at the end of time, by your acts the \[whole universe itself] is burned by the flashes of fire emerging from your eye.

[ये त्व् आत्म-राम-गुरुभिर् ह्ऱिदि चिन्तिताङ्घ्रि-द्वन्द्वं चरन्तम् उमया तपसाभितप्तम्/]{style="color:#ff0000;"}\
[कत्थन्त उग्र-परुषं निरतं श्मशाने ते नूनम् ऊतिम् अविदंस् तव हाट-लज्जाः//]{style="color:#ff0000;"}

Those gurus, who verily meditate on your two lotus feet within their heart become self-realized; but those seeing you prancing with उमा and performing severe penances and deride you as being perpetually in the graveyard, as being fierce and violent verily do not grasp your \[apparently] shameless ways.

[तत् तस्य ते सद्-असतोः परतः परस्य नाञ्जः स्वरूप-गमने प्रभवन्ति भूम्नः/]{style="color:#ff0000;"}\
[ब्रह्मादयः किम् उत संस्तवने वयं तु तत्-सर्ग-सर्ग-विषया अपि शक्ति-मात्रम्//]{style="color:#ff0000;"}

There your primal form, beyond the manifest and unmanifest, is very difficult \[to perceive] nor is it possible to for even brahmA and others to perceive your primal form; what to speak of others? Thus we can offer worship to source of all existence only to best of our limitations.

[etat paraM प्रपश्यामो na paraM te maheshvara/]{style="color:#ff0000;"}\
[मृइडनाय हि लोकस्य व्यक्तिस् ते ।अव्यक्त-कर्मणः//]{style="color:#ff0000;"}

The primal state of yours that is beyond all this, oh great god, we cannot see. For the mercy of the world you have manifested, you whose \[all] activities cannot be perceived by anyone.


