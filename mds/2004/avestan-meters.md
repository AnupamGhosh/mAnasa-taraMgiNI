
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [avestan meters](https://manasataramgini.wordpress.com/2004/09/27/avestan-meters/){rel="bookmark"} {#avestan-meters .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 27, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/27/avestan-meters/ "11:49 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Avestan \[saMhita पाठ]

ýazata pouru-hvarenanghô ýazata pouru- baêshazô : cithra vô buyâresh masânå.\
\
O gods of abundant divine luster, O gods of abundant medicines; let your excellence become manifest!'

meter in vedic terminology: पिपीलिकमध्या उष्निक् \[Avestan उष्निक् variant]\
\
\[pada पाठ]\
\
ýazata pouru-huvarenanghô / ýazata pouru- baêshazô / : cithra vô buya aresh masânå.\
\
1 2 3 4 5 6 7 8 9 10 1 2 3 4 5 6 7 8 1 2 3 4 5 6 7 8 9 10

An avestan chant fragmemt displaying the avestan equivalent of the vedic jagati (12*4). These jagatis that are approximately 12*4 पादस्. A terminal yajur like formula is added at the end of chant

aurvat-aspem bavâhi ýatha hvare,\
\
रओचिनवञ्तेम् bavâhi ýatha månghem,\
\
सओचिनवञ्तेम् bavâhi ýatha âtarem,\
\
तिज़्हिनवञ्तेम् bavâhi ýatha mithrem\
\
May you be swift-horsed, like the Hvar;\
\
May you be lustrous, like the moon!\
\
May you be blazing, like Atar!\
\
May you sharp-rayed, like Mithra!

huraodhem verethrajanem bavâhi, ýatha sraoshem ashî\
\
May you be tall and Verethra-smiting, like the \[Asha*-filled] Sraosha!

\*Asha= ऋत in vedic.


