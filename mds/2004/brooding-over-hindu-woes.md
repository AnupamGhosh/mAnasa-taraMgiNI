
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Brooding over Hindu woes](https://manasataramgini.wordpress.com/2004/03/02/brooding-over-hindu-woes/){rel="bookmark"} {#brooding-over-hindu-woes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 2, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/03/02/brooding-over-hindu-woes/ "7:17 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Though I have no personal liking for Malhotra nor agree with him in entirity, I appreciate his efforts in the recent efforts to raise the awareness of the Hindus regarding the ongoing attempts by the Anglosphere to subvert and demonize the Hindus. Sankrant Sanu, Narayanan and Ramesh Rao's efforts in this direction are also commendable. Some present well-reasoned critiques, while others humorous satires of these Anglospheric attempts. Hindus are angry and that is a good sign. It is unlikely they will take things lying down. But the question repeatedly raises itself as to whether the Hindus will successfully counter the newest assaults. Many Hindus take history for granted. They say we defeated the British, we defeated the Moslems, so we will also over come this most recent assault. Yes it is good to be optimistic but we need to see if the case is really that water-tight.\
\
As I see it the Hindus do have a serious problem- their knowledge of their own texts and culture is pretty shallow. The number of dvijas learning the vedas these days has dwindled. They have to depend on the same Indologist, about whom they have been warned, to understand the veda. The knowledge of the पुराणस् and इतिहासस् is likewise abyssmal. The knowledge of the tantra is likewise dismal. Thus the Hindus while alerted and angry have still not overcome the effects of Macaulayism. This makes it easier for a Courtright or Doniger to make up fables about the ऋग्वेद or the जैमिनीय ब्राह्मण or the कामसूत्र, and the Hindus may not be able to pick right from wrong. For example Malhotra had to depend on Witzel, a well-known detractor of Hindus, rather than his own knowledge of the ऋग्वेद or जैमिनीय ब्राह्मण to pick out the flaws of Doniger. This phenomenon put the Hindus in a relatively compromised situation.\
\
The other problem is after Macaulayitis what?I feel that Macaulayitis is not really reversible in full, so it has to be factored into any future consideration. A corollary to this is that ःईन्दुस् should have translations of Sanskrit texts and expositions of the Sanskrit material in English. Have Indian scholars contributed positively to this? One heroic attempt comes to mind: the पुराण translation effort. But why are the vedic संहितस् like मैत्रायणि, काठक etc lying unattended? Why are the ongoing efforts by Hindu scholars in translation not being popularized. Instead we see proliferation of the highly distorted translations of the Arya Samaj group.\
\
100 years ago Tilak rued the fact that Hindus had very poor access to information. His words uttered then have full force even now. I believe the Hindu upheaval has to go hand in hand with improved Hindu education. \
\
 


