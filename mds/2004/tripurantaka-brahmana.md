
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [त्रिपुरान्तक ब्राह्मण](https://manasataramgini.wordpress.com/2004/12/11/tripurantaka-brahmana/){rel="bookmark"} {#तरपरनतक-बरहमण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 11, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/11/tripurantaka-brahmana/ "9:09 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[ओ३ं तेषाम्सुराणां तिस्रः पुर आसन्नयस्मय्यवमाऽथ रजथाऽथ हरिणी ता देवा जेतुं नाशक्नुवन् ता उप्सदैवाजिगीषन् तस्मादाहुर्यश्cऐवं वेद यस्च नोपसदा वै महापुरं जयनीति त इषुगं समस्कुर्वताग्निमनीकगं सोमगं शल्यं विष्णुम् तेजनं ते ब्रुवन् क इमामसिष्यतीति रुद्र इत्यब्रुवन् रुद्रो वै क्रुरः सोऽस्यत्विति सोऽब्रवीद्वरं वृणा अहमेव पशूनामदिपतिरसानीति तस्माद्रुद्राः पशूनामदिपतिस्तागं रुद्रोवासृजत् स तिस्रः पुरो भित्त्वैभ्यो लोकेभ्योऽसुरन् प्राणुदते ॥]{style="font-weight:bold;color:#99cc00;"}

The Asuras had three citadels; the lowest was of iron, then there was one of silver, then one of gold. The gods could not overcome them; so they sought to destroy them by siege; therefore they say--both those who know thus and those who do not--'By siege they conquer great citadels.' They made ready an arrow, with Agni forming the point, Soma forming the blades, and Visnu as the wings. They said, 'Who shall shoot it?' 'Rudra', they said, 'Rudra is a fierce (archer), let him shoot it.' He said, 'Let me choose a wish; let me be overlord of animals.' Therefore is Rudra overlord of animals. Rudra shot the missile; it cleft the three citadels and extirpated the Asuras away from these worlds.


