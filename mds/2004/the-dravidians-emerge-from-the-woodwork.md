
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Dravidians emerge from the woodwork](https://manasataramgini.wordpress.com/2004/11/17/the-dravidians-emerge-from-the-woodwork/){rel="bookmark"} {#the-dravidians-emerge-from-the-woodwork .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 17, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/17/the-dravidians-emerge-from-the-woodwork/ "7:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The recent incidents in India have resulted in the Dravidians emerging like worms from the woodwork. In practically every Indian list one can now see them floating their anti-Brahmin rhetoric. A few hopeful ब्राह्मणस् had told me just a few months ago that the Dravidianists were on the wane. I always doubted this. The Tamils are too useful a tool for foreign subversionists to use against the metaphorical head of the puruSha. I had also earlier pointed out that the ब्राह्मणस् had cashed their cheques and are currently a rather spent force.

One can only dream that the ब्राह्मणस् unite and mobilize a mass movement similar to one that destroyed that despicable structure in Ayodhya that stood for 5 centuries of slavery. Lessons need to be taken from the great चाणक्य and विष्णुशर्मन् in destroying the foes.


