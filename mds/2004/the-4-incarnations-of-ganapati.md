
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The 4 incarnations of gaNapati](https://manasataramgini.wordpress.com/2004/09/19/the-4-incarnations-of-ganapati/){rel="bookmark"} {#the-4-incarnations-of-ganapati .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 19, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/19/the-4-incarnations-of-ganapati/ "5:16 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The गणेश पुराण has the 4 fold incarnations of gaNapati. They are:\


 1.  विनायक in the कृतयुग. 3 demons धूम्राक्ष, नरान्तक and देवान्तक assume great power and spread terror. So gaNapati is born of kAshyapa and his wife. He has a 10 handed form with lion वाहन and slays these asuras in a great battle at kAshi.\


 2. mayureshvara in the tretayuga. A demon called sindhu swallowed a pot of अमृत he had obtained from rudra and became invincible. gaNapati assumed a form with 6 hands and captured मयूर a mighty son of vinAta and rode him to battle and slew sindhu after making him disgorge the अमृत. It said that he then gave his peacock to his brother कुमार.\


 3. lambodara in the द्वापरयुग. This was for the killing of the demon sindhura who arose from the yawn of brahma. brahma graced him with many boons and armed with these he spread terror. gaNapati was born of व्यास the वासिष्त्ःअ and captured a terrible rat that was wreaking havoc in his house. Then riding that mouse and emerging in a 4 handed form he slew sindhura in a deadly encounter.\


 4. धूम्रकेतु is the incarnation of the future in the kaliyuga. Here gaNapati is said to assume a 2 handed form riding a horse for the destruction of the mlecchas and dasyus who would be ruining the world and restoring it to the Aryas.

The incarnations of gaNapati in the G.P are a suggestive of a relatively late work, as they clearly have borrowed from the older layer of Pauranic myths. So the GP is clearly a second order purana resting on the older motifs of the primary पुराणस्. The पुण्यक्षेत्रस् of some of these myths are associated with Maharashtra and Gujarat supporting a later regional character of this stream of the गाणपत्य sect. This is similar to the pattern observed in the populist kaumara sect in south. Also note the methodlogical parallels of Pauranic reworking in the kaumara tract "tamil skanda पुराणं" and the G.P.


