
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [datta, the atri](https://manasataramgini.wordpress.com/2004/09/26/datta-the-atri/){rel="bookmark"} {#datta-the-atri .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 26, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/26/datta-the-atri/ "1:47 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

datta, the atri, was endowed with lasting youthfulness and was the embodiment of all the 3 गुणस्. In his early life was a great warrior and was the prime minister and priest of haihaya emperor, कार्तवीर्य arjuna. He led the haihaya troops to many great victories and participated in the rise of the emperor. The haihaya king was un-paralleled in the use of arms and having conquered the entire known world was crowned as संराट् by दत्तात्रेय. The haihaya, however, had a deep enemity with the भृगुस्. He stole the cow of the भार्गव ऋशि jamadagni. In due course arjuna was challenged by rAma the son of jamadgni and axed to death by him. dattatreya was furious and carried out many अभिचार rites to destroy the भार्गवस्. Aided by his spells, the वितीहोत्रस् or the sons of arjuna slew the भार्गव ऋशि jamadgni. However, रामो भार्गव over came the spells with their own deadly incantations of the अथर्वण shruti and prepared for war with the वीतहव्यस्. In the battle that followed they were protected by the brahman of दत्तात्रेय and the भार्गव could not over come them. However, finally with the atri-destroying charm of the अथर्वण shruti rAma overcame the brahman of datta. After that he pierced the vitahavyas with his deadly astras and slew each of them. With the वीतिहोत्रस् dead, datta renounced the world and practiced the sattva guNa. He lead the life a celibate ascetic for long, teaching the principles of renunciation. In the final stage of he became and embodiment of the तमोगुण and picked a beautiful woman as his companion and wandered around with her in carefree indulgence, bearing a trident and a bow and guarded by 4 dogs. The were continually lost in dalliance, loud laughter, merriment and imbibition of वारुणि beer, when रामो भार्गव chanced upon them. In this state the atri enlightened him on the tantric lores.


