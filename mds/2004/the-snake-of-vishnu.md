
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The snake of विष्णु](https://manasataramgini.wordpress.com/2004/12/06/the-snake-of-vishnu/){rel="bookmark"} {#the-snake-of-वषण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 6, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/06/the-snake-of-vishnu/ "5:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Upon the great snake shesha sleeps the great god विष्णु, even as the the kalpa wears out. He stirs only when the waters rise and threaten to sink the world plane. This shesha is time and hence he is called ananta. Thus, when विष्णु sleeps on the coils of the great world snake he is experiencing the cyclic passage of time.

Once the great god वायु and the world snake ananta had a contest of strength. shesha wound around the mount meru around which the world revolves and वायु was supposed to dislodge the snake from the great mountain. The efforts of मातरिश्वन् to dislodge the great snake proved futile for a long time eventhough the world shook with his terrible gusts. Then in fury he uprooted the mount meru snake and all and hurled the mountain with great ferocity. Under the impact of वायु's blow, meru shattered and ananta lost his grip and surrendered. With meru shattered the world shook and had no axis. The deva विष्णु then stabilized the shattered meru it with his foot.

Versions of this tale are persistantly narrated in the local पुरणस् associated with विष्णु shrines and also the garuDa पुराण. This is remarkable example of the fact of precession being preserved in the language of myth. विष्णु is a predominantly precessional deity. All his five early incarnations contain the precessional motifs that were uncovered by Santillana and von Dechend in [The Hamlet's Mill]{style="font-style:italic;color:#ff9966;"}.


 1.  matsya. The deluge represents the sinking of the old equinoctical constellations below the equatorial plane- that is sinking of the world plane below water. It is revived by the विष्णु as the fish which provides the new world axis in the form of its immense horn.


 2.  kurma. The meru is used as a churning rod by the devas and the asuras and the same snake ananta or वासुकि as the churning rope. The rod meru, is the classic world axis of Hindu myth and the snake is the cyclic time. The great rod slips, much to the consternation of the devas, when विष्णु assuming the form of a turtle stabilizes the meru mountain and allows the cosmic churn to continue. This again represents the slipping of the axis by precession.


 3.  वराह. The world plane sinks again below water (that is the equinoctical plane sinking due to precession) and is restored by विष्णु as वराह. The boar brings back a new equinoctical plane.


 4.  नृसिंह. The world axis is called the skambha, as praised in the great hymn of the atharvans. नृसिंह emerges from the shattered pillar at the end of the kalpa. It occurs at the twilight zone (neither day nor night) between the inner and outer zones of the palace of हिरण्यकशिपु (neither in nor out). This again represents the loss of the world axis (the pillar) and the transitional zone between two equinoctical constellation during precession.


 5.  वामन. Here विष्णु with the 3 steps secures the 3 scaffolds with which the ancients concieved the world axis: 1) the zenith segment 2) the nadir segment and 3) the zone formed by the tilt in the axis- the celestial tropics.

Thus, the snake shesha coiled around meru represents the path of cyclic time around the around the world axis, and the shaking of the axis by वायु the precessional shifts of the axis. Likewise the Hindu mythic image of वासुकि supporting the earth is a symbolic representation of the cyclic time around the world axis.

The language of myth has indeed preserved one of the most primal astronomical discoveries of the ancient humans and Hindus verily retain the most vivid memories of it. It is, hence, not surprising that the Vedic astronomers were able to carry out successful calendrical reform stretching from punarvasu in the hazy past to भरणि-ashvayuja in the late vedic period. In face of this it is certain that those Indologists who doubt the antiquity of the Vedic texts are afflicted by an incurable mental ailment.


