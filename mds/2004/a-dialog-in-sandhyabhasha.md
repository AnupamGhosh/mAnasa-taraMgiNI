
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A dialog in सन्ध्याभाष](https://manasataramgini.wordpress.com/2004/11/02/a-dialog-in-sandhyabhasha/){rel="bookmark"} {#a-dialog-in-सनधयभष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 2, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/02/a-dialog-in-sandhyabhasha/ "7:22 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

R called me to describe her views on the ललितार्चनचन्द्रिक. This lead to a discussion of the सन्ध्याभाष used in the text. So we had this conversation in म्लेच्छित-manipravala SB.\
\
I: "I feel close to jaya but there is a strange bhaya due to precedence"\
\
R: "Are you really sure that you are close to jaya ?"\
\
I: "That it is good point, I was close to grabbing the Hvarena- same time different cycle. Like Frangrasyan (though not is his physical state :-)) I tried to grab it but failed. The failure almost happened as thought from the blue."\
\
R: "Indeed that seems like a shakuna. But the inner नालिक seems to be functional."\
\
I: "Ananda- will the root of that work"


