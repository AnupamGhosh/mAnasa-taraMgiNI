
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Why did you lose?](https://manasataramgini.wordpress.com/2004/09/30/why-did-you-lose/){rel="bookmark"} {#why-did-you-lose .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 30, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/30/why-did-you-lose/ "4:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

What is it that lies hidden in you they spot?\
Why is it that they call you Devang Gandhi when you were VVS Laxman?\
Why is it that you are a hero of the Ranji pitch but deliver no more than an egg at the real Test grounds?\
Why is it that you smash a Karthik outside the ground, but get out on the first ball to Warnie?\
Why is it that they run behind you to give you the Ranji Trophy but are not invited to disbursions occuring after the real Tests?\
It seems that you held the bat just a Dravid did but it never seem to help?\
You even equalled Lara in some domestic match but they did not look at you...

The say "Kambli never learnt to drive...He never became a man" was that the cause?

iti परिसमाप्तं


