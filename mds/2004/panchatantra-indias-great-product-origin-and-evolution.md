
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Panchatantra- India's great product: origin and evolution](https://manasataramgini.wordpress.com/2004/06/05/panchatantra-indias-great-product-origin-and-evolution/){rel="bookmark"} {#panchatantra--indias-great-product-origin-and-evolution .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 5, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/06/05/panchatantra-indias-great-product-origin-and-evolution/ "6:04 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

What were the origins of the पञ्चतन्त्र (PT) of the great ब्राह्मण विष्णुशर्मन्?\
It is said that he composed to these tales to teach नीति to the errant sons of the king amarashakti of the southern janapada of महिलारोप्य \[meaning city of ascendant women]. In his opening verse he salutes the great teachers of politics and declares the पञ्च्तन्त्र to be an epitome of the अर्थशास्त्र:\
[**मनवे वाचस्पतये शुक्राय पराशराय स-सुताय ।\
cआणक्याय cअ विदुषे नमो ऽस्तु नय-शास्त्र-कर्तृभ्यः ॥**]{style="color:#99cc00;"}

[सकलार्थ-शास्त्र-सारं जगति समालोक्य विष्णुषर्मेदम् ।]{style="color:#99cc00;"}\
[तन्त्रैः पञ्cअभिर् एतc cअकार सुमनोहरं शास्त्रं ॥]{style="color:#99cc00;"}\
To manu, to बृहस्पति, shukra, पराशर and his son and चाणक्य the learned, we salute; these great makers of शास्त्रस्. Having examined the essence of all शास्त्रस्, विष्णुशर्मन् too, has composed these five volumes, a delightful text.

Thus, the great विष्णुशर्मन् considers himself to belong to the line of notable scholars of नीति and artha that include व्यास and पराशर. Here we shall try to have an overview of the history of this notable text and trace the origins of its devices back to the महाभारत (Mbh).

The age of विष्णुशर्मन् may be bracketed by two major signposts. An Iranian version of the PT in Pahlavan from the Sassanian period is rather confidently dated as being from around 550 CE. This provides the upper limit. The PT in addition to the salutation of चाणक्य also quotes directly from his अर्थसास्त्र. Further, in the list of predecessor चाणक्य is mentioned as the last authority prior to विष्णुशर्मन्. Given that the core अर्थशास्त्र is a mauryan text of around 300 BC, get the lower bound for the PT. On the tentative ground of the janapadas and पाटलिपुत्र being mentioned along with petty rulers we may place the text closer to declining the mauryan period when there was brief revival of the janapadas. The PT is also known by other names such as तन्त्राख्यायिक (the Kashmirian variant), the पञ्चाख्यानक or the तन्त्रोपाख्यान, and as suggested by some works the most likely original name was नीति पञ्च तन्त्राख्यायिक.

A phylogenetic reconstruction suggests that there are two major divisions of the PT texts: 1) the तन्त्राख्यायिक branch is associated with Nothern India, principally represented by the Kashmirian recension. From it were derived through rather drastic divergence the पञ्चाख्यानक of the Jainas. The पञ्चाख्यानक recombining with older versions of the PT seems to have given rise to पूर्णभद्र variant, in the hands of the eponymous Jaina अचार्य. A divergent variant of this appears to have been inserted within the ancestral northern बृहत्कथ and is preserved in the two descendent variants prepared by the Kashmirian ब्राह्मणस् क्षेमेन्द्र [बृहत्कथ मञ्जरि] and somadeva [कथसरित्सागर]

 2.  The southern पञ्चतन्त्र is the shortest version of the text and its primary descendent was that of the south Indian ब्राह्मण वसुभाग, who recombined it with material from the southern recension of the बृहत्कथ to make his text. The version of वसुभाग was transported to Thailand and Indonesia and gave rise to the local versions there. Another variant of the Southern PT, showing an inversion of volume 1 and 2 was prepared by the medieval स्मार्त ब्राह्मणस् of Tamil Nad from the version of वसुभाग. This version was carried by these ब्राह्मणस् during their northward migration to the region of Pashupatinath in Nepal and degenerated into the Nepali version. A version of this also found its way to Bengal where it was recast by नारायण to constitute the hitopadesha.

The tales also traveled west via the Iranians of the Sassanian empire and was eventually incorporated into many Arabic and European folk tales. Thus, based on the investigations of the early western पञ्चतन्त्र scholars Edgerton and Hertel it may be remarked that it was the most widely circulated piece of literature throughout the ancient world and provides a remarkable material for the evolutionary studies on textual memetics.

When we attempt to pierce the veil and look into the Indic precursors of it we find ourselves drawn to that great fountain of all Indic lore the इतिहास, particularly the महाभारत. In a very general sense the PT inherits from the Mbh the great Indo-Aryan innovation of nested structure of stories. Thus these stories resemble certain other structure in existence such as: 1) the modular computer programs- with different FOR, WHILE, IF etc loops closed by some delimiter such as a '{}' pair. 2) The structure of chromosomes, with mobile elements inserted one within another. 3) The ancient multi-domain protein which show nesting of domains one within the other. In a more direct sense precursors of the PT can be seen in that great interminable death-bed lecture of भीश्म to युद्धिSठिर. Here the kuru grandsire narrates several animal stories related to नीति. These include the maxims of dharma and artha provided by the following tales: 1) long-necked camel killed by the jackal couple. 2) tale of the wise mouse palita and his rivals the cat lomasha, the owl chandraka and mongoose harita. 3) the जीवाजिवक bird pujani that blinded the पञ्चल prince for killing her son. 4) The pativrata pigeon etc. An examination of these tales makes it clear that it was such a base that inspired that great work of विष्णुशर्मन् and it is not without reason that he acknowledges the great scion of पराशर, the compiler of the great bhArata epic. Not surprisingly it also inspired stream of Buddhist tales, the जातकस्. Thus one of the greatest exports of the Hindus to the world may be ultimately traced back to the literary innovations when of the first great Indic unification under the kuru and the पञ्चाल.

[**"Be it a horse, a science, or a sword,\
A lute, a voice, a woman or a man;\
Whether they become useful or not\
Depends on the competence of the man\
To whom they belong"**]{style="color:#ff9900;"}\
So says विष्णुशर्मन्\
[1.44; critical edition of Edgerton]


