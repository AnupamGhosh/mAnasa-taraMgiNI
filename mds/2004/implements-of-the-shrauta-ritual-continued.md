
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Implements of the shrauta ritual ... continued](https://manasataramgini.wordpress.com/2004/08/20/implements-of-the-shrauta-ritual-continued/){rel="bookmark"} {#implements-of-the-shrauta-ritual-continued .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 20, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/08/20/implements-of-the-shrauta-ritual-continued/ "4:48 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**उलूखल-musala**: The mortar and pestle used for pounding grains to separate husk. They are made of khadira wood. The उलूखल may also use palasha wood, and be about knee heigth. The musala may have a metal base.

**दृशद्-upala**: The grinding stone for grinding the grains into the flour for various preparations such as the पुरोडाश. They are fashioned from stone.

**कपाल**: The circular potsherd on which the पुरोडाश cakes are baked and offered. They may be by default the width of 2 angulas or 1/4th of the palm. Larger ones are scaled according the default but should be no bigger than the palm. The अपूपस् may also be baked on a पुरोडाश in certain rites to indra.

**रौहिण** **कपाल**: These are specialized कपालस् always used as a pair. They are circular in shape the diameter of a palm, and mounted on ceramic horses. They are specifically used in baking the specialized pravargya offerings to the ashvins.

**रौहिण हवणि**: There are pair of specialized sruch like ladles with flat terminals (that is lacking a cup, but a flat plate of the same outline instead) with a downward drooping protuberance. They are used for offering the रौहिण cakes in the pravargya.

**iDA पात्री**: The shape of the outline should be ovoid, broad at the ends and constricted in the middle like a non-intersecting "8" figure or an hour-glass. It is one अरट्नि in length, depth of 3 angulas and has a clyindrical handle of 4 angulas. The various oblations may be mixed in it before offering. Made from bronze or वारण wood.

**पुरोडाश पात्रि**: A square outline vessel with each side a pradesha and a depth of upto 6 angulas. A handle of 4 angulas. Used to make offerings of whole पुरोडाशस्. Material is same as above.

**पिष्ट पात्रि**: The length is 20 angulas, depth 6 angulas and breadth 6 1/2 angulas. वैखानसस् alone specify their vessel to have a circular rim at the end away from the handle. Same type of handle as above. May be made from वारण or clay. Used to offer पिष्ट or mix the flour with water to make the पुरोडाश dough.

**षड्वत्त पात्रं**: The outline is shaped like two peepal leaves fused together at the base. On each broad leaf shaped part there is one cavity each. In each cavity goat cheese or goat butter is placed. Then the part of the पुरोडाश to be offered is cut into two equal parts and placed in each cavity on the cheese/butter. On top of the पुरोडाश a second layer of cheese or butter is added. Thus the offering is six fold is prepared when ever indicated. The अग्नीध्र priest may eat these offerings if so directed in the ritual.

**prashitra हरण**: It is a special set of spoons of the brahmA priest. They are made shaped as a cow's ear, a peepal leaf, a lotus bud and a square. The पुरोडाशस् offered to agni and soma are cut to remove a small piece that is placed on these spoons and brought to the brahmA, covered by a wooden lid. He eats from these spoons.

**dogdhre patre**: They are a pair of vessels which are shaped like a sruch without a handle but have beaks shaped like the tip of an elephants trunk. The two are used to mix the milk of the ewe and the cow in the pravargya ritual.

**वसोर्धारा**: special device with a large receptacle at one end into which ghee is poured. The receptacle drains out through a groove passing though a staff a व्यायाम in length and ends in a beak. The offering in constantly streamed into the fire through this device. Made of audumbara wood.

<div>

</div>

<div>

**अन्तर्धान कट**: semicircular wooden plate twelve angulas in length. It may have a handle of 3 angulas on either side at the diameter or along a 60 degree radius. The plate has to shield the दीक्षितानि or the patni from the sight of the आहवनीय when the offerings to the goddesses are made.

</div>

<div>

<div>

</div>

<div>

**vighana**: A wooden hammer with one head flat and the other head conical and ending a ball like protuberance. It is used to break earth clods at the sacrificial ground and ramming the earth for the site of the vedi.

</div>

<div>

</div>

<div>

**राजासन्दि**: A high four legged stool made of audumbara wood. It is covered with the कृश्णजिन or the black-buck skin and the base of the seat is woven from the munja grass cords. The seat should be the height of the navel of the दीक्षित and leg bars should extend an अरट्नि beyond the seat. The soma plant is placed on it

</div>

<div>

</div>

<div>

**संराडासन्दि**: it is a seat similar to the राजासन्दि but is higher than it at the seat level. The महावीर pots are placed on top of it.

</div>

<div>

</div>

<div>

**अधिषवण**: These are the soma pressing boards. Audumbara or palasha wood is used for them. They are covered with antelope leather and their front parts are semi-circular. The soma is crushed on them with 5 stones.

<div>

</div>

<div>

**droNa kalasha**: A reservoir for the soma juice. वारण wood is preffered for it. It is outline is like a peepal leaf, or a boat or square. It is deep to hold enough soma and has a handle of 4 angulas.

</div>

<div>

**dasha pavitra**: The soma filter. It has a cloth rim and a central filter section woven from the wool of a white sheep by the दीक्षितानि. It is shaped as to fit over the droNa kalasha.

<div>

<div>

</div>

<div>

**pariplu**: a small wooden vessel made from the विकङ्ती wood. It is like a sruch without a handle. It is used to draw soma from the droNa kalasha for आघार pourings.

</div>

</div>

</div>

</div>

</div>


