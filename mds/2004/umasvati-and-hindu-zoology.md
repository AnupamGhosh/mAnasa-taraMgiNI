
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [उमास्वति and Hindu zoology](https://manasataramgini.wordpress.com/2004/05/17/umasvati-and-hindu-zoology/){rel="bookmark"} {#उमसवत-and-hindu-zoology .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 17, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/05/17/umasvati-and-hindu-zoology/ "2:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**उमास्वति**, the great jaina Acharya produced a remarkable work termed the तत्त्वार्थाधिगम around 40 AD which contains one the great early productions of Hindu zoology. उमास्वति believed that all animals could be classified into the following groups:

**Group I invertebrates:**\
अपादिक = nematodes\
नूपुरक = annelids\
गण्डूपद = cnidaria\
shankha,shutika,samubuka= molluscs\
जलूका=leeches

**Group II ("lower arthropods")**\
पिपीलिका\
रोहिणिका\
उपचिका, kunthu, tuburaka=bugs\
trapusavija, करपासास्थिका=beetles\
shatapadi, utpataka=myriapods\
त्रिणपत्र (plant hoppers)\
काष्ठ हारक (termites and cockroaches)

**Group III ("higher arthropods")**\
bhramara, वरट, सारन्ग=hymenopterans\
मक्षिक, पुत्तिका, दंष, mashaka=Diptera\
वृश्चिक, नन्द्यावर्त=scorpions and spiders\
कीट=lepidoptera\
पतञ्ग=orthoptera (locusts and hoppers)

**Group VI**\
The vertebrates know as tiryagyonis\
matsya=fishes\
uraga/nakra, godha, कूर्म=crocodiles, lizards and turtles\
bhujanga=snakes\
pakshi=birds\
जारयुजास्=mammals

This shows that the ancient Hindus had a working knowledge of animal diversity and a natural complexity based on biological organization. While it may not be the most accurate classification of Animalia it shows that the hindu world clearly encouraged the study of biology, something which was sorely lacking in later day India.

The great ancient ब्रह्माण zoologist **लाद्यायन** of a lost atharvan shAkha presents a similar picture and made a particular study of arthropods and "reptiles". His entire text does not survive but fragments preserved by dalvana present a clear picture of his meticulous approach. लाद्यायन states :

[कटुबिः बिन्दु लेखाभिः पक्शैः पादैः मुकैः नकैः शूकैः कण्टक-लान्गलैः सम्श्रिष्टैः पक्श-रोमभिः ।]{style="color:#0000ff;"}\
[स्वनैः प्रमाणैः संस्थानैः लिञ्गैश्वापि शरीरगैः विष-वीर्यैश् च कीटानां रुपग्यानं विभावयते ॥]{style="color:#0000ff;"}

In order to obtain knowledge of the classification of arthropods one must carefully examine: 1) veins, spots and markings 2) wings, 3) legs, 4) oral appendages 5) mandibles, 6) antennae, 7) claws and spines 8) abdominal stings 9) bristles on body and wings 10) sounds 11) structure of organs, 12) genitalia 13) toxins and their actions.


