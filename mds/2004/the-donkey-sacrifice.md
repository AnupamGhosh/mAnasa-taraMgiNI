
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The donkey sacrifice](https://manasataramgini.wordpress.com/2004/12/12/the-donkey-sacrifice/){rel="bookmark"} {#the-donkey-sacrifice .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 12, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/12/the-donkey-sacrifice/ "10:47 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The shrauta sUtra of कात्यायन and the पारस्कर गृह्य sUtra mention a peculiar rite namely the gardhabhejya or the donkey sacrifice. The context suggests that it is performed by a brahmachAri who has fallen to passion and had sex against the norms of a vedic student. The offerings were made into the unconsecrated fire with the पुरोडाश cakes being cooked on the bare ground. The heart and the tongue and other parts of the donkey were offered into water. The prashitra offering made to iDA by the brahmA priest was the phallus of the donkey. This primitive rite does not seem to have any echoes elsewhere in the Indo-Aryan world, but by its characteristics may be a late surviving archaism from the time when the early Indo-Aryans roved the central Asiatic steppes.


