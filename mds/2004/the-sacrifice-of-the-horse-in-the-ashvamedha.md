
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The sacrifice of the Horse in the ashvamedha](https://manasataramgini.wordpress.com/2004/11/21/the-sacrifice-of-the-horse-in-the-ashvamedha/){rel="bookmark"} {#the-sacrifice-of-the-horse-in-the-ashvamedha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 21, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/11/21/the-sacrifice-of-the-horse-in-the-ashvamedha/ "5:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}


 1.  The adhvaryu takes the last drink of water to the horse and offers it with the formulae अपां पेरुः and अग्निः पशुः.

 2.  Then the yajur priests take fire around the 13 animal starting from the कपिञ्जल that are tied along with the stallion and then release them.

 3.  The horse is lead to a large piece of cloth spread on the ground on which a piece of gold is placed. The the horse is strangulated by another large piece of cloth bound around its muzzle by the shamitar.

 4.  The adhvaryu offers the परिपशवया oblations as the horse is being killed and as it dies 3 oblations of ghee are made with प्रानाय ; अपानाय; व्यानाय + स्वाहा.

 5.  At the direction of the नेष्टा priest the royal ladies chant the mantras beginning with namaste amba.

 6.  Then royal ladies move towards the horse and go around it 3 times from left to right and recite the mantra गणानान्त्व गणपतिं. Then the go 3 times from right to left and chant प्रियानाम् tva. Then they go 3 times from left to right reciting निधीनां त्वा.

 7.  Then the emperor, the chief queen the महिषि and the adhvaryu utter the purificatory incantation for the dead horse. Then the महिषि recites Aham अजानि and lies besides the dead horse.

 8.  The adhvaryu then covers the two with the large sheet of cloth with the formula svarge loke.

 9.  Then then phallus of the horse is excised and the महिषि takes it on her lap reciting the formula वृषा वाजी.

 10.  The emperor looks at the horse and recites the formula उत्सक्थ्या.

 11.  Then the adhvaryu, the brahmA, the उद्गाता, the होता and the सूता bard of the emperor, each, recite one of the five sexually explicit formulae beginning with "yakA asakau.." directed at the royal ladies and their ब्राह्मण female companions. At the end of each hymn and they recite the formula "haye cha haye asau -----" and utter the name of the ladies. The kshatriya women and their companions reply by reciting the sexually explict hymns paired with with the "yakA asakau.." series.

 12.  Then the महिषी stands up and the priests in unison recite the surabhimati hymns to purify their mouths.


 13.  3 of the royal wives then mark the track of the knife cut on the body of the horse with needles with copper, silver and gold bead heads, reciting six mantras beginning with gAyatrI त्रिष्टुप् as they do so.

 14. The knife for dissecting the horse has a gold handle.

 15.  The adhvaryu then starts dissecting the horse along the track laid by the ladies while reciting 7 mantras starting with कस्त्वा आच्छयति.

 16.  The fat is then scrapped off the visceral cavity of the horse. The blood of the horse is collected in a separate vessel.

 17.  The animals slaughtered to prajapati, a bull, a male goat and a and ram are also dissected similarly and their omentum is fried into dumplings and havis cakes are also prepared to the north of the आहवनीय fire.

 18.  adhvaryu and the होता then engage in a dialog in vedic in the sadas shed. Then the brahmA and the उद्गात do the same. Then the former pair of priests move to the west of the uttaravedi and do the same.

 19.  Then the adhvaryu picks up the silver महिमान् cup and filling it with soma offers it entirely to indra with the yaste ahan chant.

 20.  Then the adhvaryu immolates the omenta of the animals with a recitation of 20 mantras particular to these offerings to various devas beginning with the combined indra and agni offering, other devas ending with विष्णु and then prajApati. Then there are offerings to the gotra founders of the emperor and rudra.

 21.  Then the adhvaryu offers the second महिमान् cup soma pouring to indra with the yaste रात्रौ chant.

 22.  The rest of the meat of the horse is fried as dumplings in an iron cauldron usually in the northern area.

 23.  Then he makes an oblation to the deity of the trees.

 24.  Then he offers each organ of the horse roasted as a dumpling to the god to which it is designated along with the requisite yajur incantation.

 25.  Then he recites the mA no mitra chant of two yajur आनुवाकस् and offers 16 ghee offerings. The last of these oblations is made to dyaus and पृथिवि.

 26.  Then the स्विष्तकृत् offering is made.

 27.  Then the offerings to the wives of the gods called the पत्नीसम्याज rite is done. During this the womenfolk are shielded from the glimpse of the sacrificial fire using the अन्तर्धान plates.

 28.  Then comes the rite of the blood offerings, the rakta याज.


 29.  In the rakta याज the blood of the horse is taken in a copper pot and offered into the fire by streaming it through the dried trachaea of a nilgai and by using the hoof of a horse as a juhu ladle. The formula used is: अग्निभ्यः स्विष्टकृद्भ्यः स्वाहा.


