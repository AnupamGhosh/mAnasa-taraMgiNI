
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Khusroo waxing eloquent](https://manasataramgini.wordpress.com/2004/04/20/khusroo-waxing-eloquent/){rel="bookmark"} {#khusroo-waxing-eloquent .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 20, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/04/20/khusroo-waxing-eloquent/ "5:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The excerpt from Amir Khusroo's florid prose of the savage attack on the Hindus by Maliq Kafr, the general of Alla-ad-din Khalji (Tarikh-i-Allai). Facts that are denied by many Indians themselves

The tongue of the sword of the Khalifat of the era, which is the tongue of the flame of Islam, has imparted light to the entire pagan darkness of Hindustan by the illumination of its guidance. On one side an iron wall of royal swords has been raised before the Kafirs of Mongolia, the Magog-like Tatars, so that all of the Mongolian tribe, deserted by Allah, drew their feet within their skirts amongst the hills of Ghazni, and even their advance-arrows had not strength enough to reach into Sind which was conquered by the sword of Islam. On the other side so, much dust arose from the battered temple of Somnath that even the sea was not able to settle it. On the right hand and on the left hand the army of Islam has conquered Hindustan from sea to sea, and several capitals of the rulers of the Hindus, in which the worship of the fire and their devilish gods and had prevailed since the time of the Jinns, have all been demolished. All these impurities of infidelity have been cleansed by the Sultan Alla-ad-din's destruction of idols and temples, beginning with his first Jihad against Devgir (Devagiri in Maharashtra). Now the flames of the light of the Shariat and the righteous Fatwa illuminate all these unholy Hindu lands, and places for the callers to Namaz occupy the high places and the and Namaz is read in mosques. Allah be praised!

Maliq Kafr heard that in Brahmastpuri (Near Rameshwaram) there was a golden idol, round which many elephants were stabled. The Maliq started on a night expedition against this place, and in the morning seized no less than two hundred and fifty elephants. He then determined on razing the beautiful temple to the ground. That temple was beauteous as the Paradise of Shaddad, the golden temple of Ram (Rameshwaram) where the hellish Hindus worshiped their gods. The roof was covered with rubies and emeralds,-in short, it was the holy place of the Hindus. The Maliq dug up the temple from its foundations without leaving anything, and made the heads of the Brahmans and other idol worshiper dance from their necks and fall to the ground at their feet. And blood of Hindus flowed in torrents. The stone idols called Ling Mahadeo, which had been a long time established at that place. They were copulating sex organs worshiped by the Kafir Hindus. Up to this time, the kick of the horse of Islam had not yet broken them. The Musalmans destroyed all the lings, and idols of Deo Narain (विष्णु), and the other gods. Had these idols been able to move they would have jumped to the fort of Lanka. Had those sex organs had legs they would have run at fright of the Moslems. There was much fierce fighting with the Hindu Nayakas defending the land who were finally butchered. Much gold and valuable jewels fell into the hands of the Musalmans, who returned to the royal canopy, after executing their project of the holy Jihad on the 13th of Zi-l ka'da, 710 H. (April, 1311 A.D.). They destroyed all the temples at Birdhul (Near Madhurai), and plundered the public treasury.


