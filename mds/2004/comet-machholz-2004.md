
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Comet Machholz 2004](https://manasataramgini.wordpress.com/2004/12/16/comet-machholz-2004/){rel="bookmark"} {#comet-machholz-2004 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 16, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/12/16/comet-machholz-2004/ "7:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Comet Machholz 2004 ]{style="font-weight:bold;"}[![Posted by Hello](https://i0.wp.com/photos1.blogger.com/pbh.gif){align="middle" border="0" style="border:0 none;background:transparent none repeat scroll 0 50%;padding:0;"}](http://www.hello.com/){target="ext"}

```{=latex}
\begin{center}
```

![](https://i0.wp.com/photos1.blogger.com/img/133/1300/400/machholz2004.jpg){width="75%"}
```{=latex}
\end{center}
```



We finally saw comet Machholz 2004 today with my 20X75 binoculars. It shone as fuzz ball in the southern sky, in the constellation of Eridanus (The southern river; the celestial यमुना of the Hindus which leads to the path of death). It was a little above the level of Sirius, the head of shabala praised at the morning shuna shairya offering of the पञ्चविम्श ब्राह्मण. I estimate its magnitude as around 4.5 and was able to perceive a slight tail with the binoculars. It is supposed to rise higher beyond Orion. I uttered the incantation of the dogs.

Marc took the above photo.


