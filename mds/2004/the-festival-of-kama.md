
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The festival of काम](https://manasataramgini.wordpress.com/2004/09/01/the-festival-of-kama/){rel="bookmark"} {#the-festival-of-कम .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 1, 2004]{.entry-date}](https://manasataramgini.wordpress.com/2004/09/01/the-festival-of-kama/ "4:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

From a somewhat corrupt, but legible copy of the padma पुराण, I have been able to piece together the most complete version of the damanaka utasava vidhi for काम and rati. I provide it here as a part of my ongoing project of creating critical vidhis for ancient Hindu festivals ( for after all the learned have said the शास्त्र is more authoritative than prayoga).

Source: padma पुराण, uttara काण्ड, 84.\
Day: chitra; shukla पक्ष; द्वादशि.\
Rite:In a sylvan grove, idols of rati and काम are installed with the mantra:

**[कामदेव नमस्तेऽस्तु विश्वमोहन कारक]{style="color:#66ffff;"}**\
**[विष्णोररथे विचेश्यामि कृपां कुरु ममोपरि]{style="color:#66ffff;"}**

Then the idols are brought home to with accompanying music and singing.The idols are installed in the indra disha or the eastern quarter after sunset. The whole worship is conducted after sunset.An auspicious circle is drawn around the idols and the idols are clothed in white cloth.They are decorated with the shoots of the damanaka plant.The deities are worshipped with the formulae:

**[ॐ क्लीं कामदेवाय नमः]{style="color:#99ffff;"}**\
**[ॐ ह्रीं रत्यै नमः]{style="color:#99ffff;"}**\
The the deities are devotely worshiped with sandal paste, flowers, incense and lamp-waving with the following formulae:\
**[ॐ मदनाय नमः (एअस्त्)]{style="color:#99ffff;"}**\
**[मन्मथाय नमः (षोउथेअस्त्)]{style="color:#99ffff;"}**\
**[कन्दर्पाय नमः (सोउथ्)]{style="color:#99ffff;"}**\
**[अनञ्गाय नमः (south-west)]{style="color:#99ffff;"}**\
**[बहसम सहरिरय नमह (वॆसत)]{style="color:#99ffff;"}**\
**[स्मराय नमः (णोर्थ्-ऎअस्त्)]{style="color:#99ffff;"}**

**[ईश्वराय नमः (नोर्थ्)]{style="color:#99ffff;"}**

**[पुष्प-बाणाय नमः (नोर्थेअस्त्)]{style="color:#99ffff;"}**

Then they offer turmeric stained rice, festive eatables (भक्षणस्), and ताम्बूलस् (paan)Then they meditate of काम by performing the काम gAyatri japa 108 times:\
**[तत् पुरुषाय विध्महे कामदेवाय धिमहि । तन्नोऽनञगः प्रचोदयात् ॥]{style="color:#99ffff;"}**

A pitcher of water is placed before the idols Then the following formulae are recited:\
**[नमोऽस्तु पुष्पबाणय जगदाह्लादकारिणे मन्मथाय जगन्नेत्र रति प्रीतिकराय च]{style="color:#99ff99;"}**\
**devadeva namaste.astu shri vishvesha namo.astute\
ratipate namaste.astu namaste.astu विश्वमण्डन\
**\
**namaste.astu jagannatha सर्वबीज namo.astute\
**

feet of the idols are washed with water from the pitcher.Then there is a great celebration with song, music, dance and a Holi-styled splashing-sports.


