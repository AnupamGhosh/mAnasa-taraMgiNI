
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some reminiscences of our study of chaotic maps-1](https://manasataramgini.wordpress.com/2016/12/18/some-reminiscences-of-our-study-of-chaotic-maps-1/){rel="bookmark"} {#some-reminiscences-of-our-study-of-chaotic-maps-1 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 18, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/12/18/some-reminiscences-of-our-study-of-chaotic-maps-1/ "6:40 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Starting in our teens, we began our exploration of chaotic (strange) attractors emerging from simple iterative maps, numerical solution of ordinary differential equations and other fractals inspired by the work of Benoit Mandelbrot. It led us in two directions. First, it provided us analogies and inspiration to understand how "complex" and "intricate" form emerges from simple mechanisms in nature. This is encountered in the context of the behavior of systems, organization and development of living organisms, in folding of biopolymers like proteins and in the context of mineral processes like formation of dendritic structures in rocks, coast-lines and topography. It gave rise to the worldview of natural systems are computers in themselves. Second, these maps produced an extraordinary and unparalleled aesthetic experience.

The first maps we explored were the logistic map and the tent map. We studied these even before we accessed a computer using a hand-held calculator. The very first time we managed to access a computer we started studying 2D maps starting with two of the simplest. The first of these maps go back to the work of Henon even before we were born. The Henon map itself is a simplified presentation of the analysis of the strange attractor emerging from the solution of three differential equations that constitute the system discovered by Lorenz in his description of complex or chaotic behavior of the atmosphere using a simple model. This in turn emerged from earlier studies with more complex equation systems like those of Subrahmanyam Chandrasekhar on convection fluids heated from below. The Henon map:

 $$x_{n+1}=1-ax_n^2+y_n\\ y_{n+1}=bx_n$$ 

where  $a$  and  $b$  are constants.\
When one studies the evolution of zero i.e.  $(x_0,y_0)=(0,0)$  under this map it produces a strange attractor of a parabolic form at for example  $a=1.4$  and  $b = 0.3$ . This is the most commonly illustrated and studied Henon attractor. One can see in the figure below that at the 4th iteration of the map zero has evolved into a point on the attractor on which it bounces chaotically thereafter.

![henon_parabolic1](https://manasataramgini.files.wordpress.com/2016/12/henon_parabolic1.png){width="75%"}Figure 1

We found another such attractor at  $a=1.04; \; b = .51933$  where zero converges on to the attractor by the 6th iteration. Sprott reported a very similar attractor at  $a=1; \; b = 0.5427$ .

![henon_parabolic2](https://manasataramgini.files.wordpress.com/2016/12/henon_parabolic2.png){width="75%"}Figure 2

These strange attractors are not the most aesthetic nor very "rich" in structure. However, there are some other aesthetically more appealing behaviors seen in this map. For values of slightly greater than  $b=-1$  e.g. ( $a=.8; \; b=-.9999$ ) zero evolves very gradually towards a single point attractor. In the process we get an interesting map where the evolving points revolve in the triangular region between the three points  $(0,0);(1,0);(0,-1)$  in smaller and smaller cycles. The map shows a banded appearance due to greater or lesser permissive regions. At  $a=.8; \; b=-1$  the attractor becomes a closed curve forming the limit cycle of the earlier earlier attractor (in blue). The lower point of intersection of the parabola  $x=1-ax^2+y$  and the line  $y=bx$  which are related to the two generating equations of the map for the  $(a,b)$  corresponding to the above limit cycle curve is the attractor point. The line  $y=bx$  is the axis of symmetry of the map and the limit attractor curve.

![henon_one_point_trigonal](https://manasataramgini.files.wordpress.com/2016/12/henon_one_point_trigonal.png){width="75%"}Figure 3

Another case of the evolution of zero to a single point attractor via a visually interesting map is generated in when  $a=-.5$  and values slightly greater than  $b=-1$ , e.g. (  $b=-.99995$ ). Here the map assumes two oppositely directed pentagonal regions with a prominent five-lobed region of exclusion. This attractor point is specified by the higher point of intersection of the parabola  $x=1-ax^2+y$  and the line  $y=bx$  for the  $(a,b)$  corresponding to the limit cycle curve formed when  $b=-1$  (in blue). This pattern reminiscent of structures emerging in the Kolmogorov-Arnold-Mosser maps.

![henon_one_point_pentagonal](https://manasataramgini.files.wordpress.com/2016/12/henon_one_point_pentagonal.png){width="75%"}Figure 4

Evolution of zero to a two point attractor is seen with parameters close to  $a=.1; \; b=.9999$ . At  $a=.8; \; b=1$  the attractor becomes two closed curves which form the outer limit of the earlier attractor (in blue).

![henon_two_point](https://manasataramgini.files.wordpress.com/2016/12/henon_two_point.png){width="75%"}Figure 5

Here again the two points of convergence are specified by the points of intersection of the parabola  $-x=1-ax^2+y$  and the line  $-y=bx$  for  $(a,b)$  corresponding to the limit curve, with the latter line being axis of symmetry of the map.

![henon_map](https://manasataramgini.files.wordpress.com/2016/12/henon_map.png){width="75%"}Figure 6

Zero also evolves to multi-point attractors, like a 6-point attractor at  $a=-.65; \; b=-.999199$  and a 13 point attractor at  $a=-.65; \; b=-.9994142$  (both shown together). The limit curve in this case is broken up into 13 separate closed lobes.

![henon_6_13_point](https://manasataramgini.files.wordpress.com/2016/12/henon_6_13_point.png){width="75%"}Figure 7

Given a pair of  $(a,b)$  values, we can also study the number of iterations it takes for points on the x,y plane to escape to infinity under the Henon map. Those points which get trapped in and attractor are colored black while the rest are color-coded according to the number of iterations they take to escape. This produces interesting fractal structures with intricate boundaries (the Henon equivalent of a Julia set). Examining these set we see that in each case the successively diverging regions are a series of nested parabolas that give way at their heart to a chaotic region that looks like a pigment mixing in a fluid.

In the case of  $(a,b)$  values that spawn the strange attractor, the attractor is tightly bounded by a parabolic entrapment basin.

![henon_julia_02](https://manasataramgini.files.wordpress.com/2016/12/henon_julia_02.png){width="75%"}Figure 8

In the case of  $(a,b)$  values that result in a single point attractor with gradual convergence (e.g.  $(-.5,-.99995)$ ), the evolution of a point like zero lies at the center of the basin of entrapment, which generally has an axis of symmetry same as that of the evolution of the point.

![henon_julia_01](https://manasataramgini.files.wordpress.com/2016/12/henon_julia_01.png){width="75%"}Figure 9

In the case of parameters like  $a=.2; \; b=1.015$  where the point  $(0,0)$  escapes to infinity we get two distinct major basins of relative entrapment with a very complex boundary and zone around them.

![henon_julia_04-2](https://manasataramgini.files.wordpress.com/2016/12/henon_julia_04-2.png){width="75%"}Figure 10

Analogous to the Mandelbrot set we can also compute the escape iterations for  $(0,0)$  to diverge towards infinity along the  $a-b$  parameter plane. This results in a peculiar set with a basin of entrapment bearing resemblance to the side view of a mammal's head with a pointed snout and vibrissae. Around it is a zone of complex enfolding of regions with different escape values. Notably, we can see that two of the values spawning strange attractors lie close to the upper edge of the snout.

![henon_mandelbrot_escape](https://manasataramgini.files.wordpress.com/2016/12/henon_mandelbrot_escape.png){width="75%"}Figure 11

[proceed to part-2](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/)

