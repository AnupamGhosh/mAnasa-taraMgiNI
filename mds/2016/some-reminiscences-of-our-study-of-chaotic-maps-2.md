
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some reminiscences of our study of chaotic maps-2](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/){rel="bookmark"} {#some-reminiscences-of-our-study-of-chaotic-maps-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 26, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/ "2:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Continued from part-1](https://manasataramgini.wordpress.com/2016/12/18/some-reminiscences-of-our-study-of-chaotic-maps-1/)

The second two dimensional map we studied in our early days was that of Lozi:

 $$x_{n+1}=1-a|x_n|+by_n\\ y_{n+1}=x_n$$ 

where  $a$  and  $b$  are constants.

It becomes immediately evident that this map is conceptually similar to the Henon map, using the absolute value operator instead of the squaring operator. Both generate a positive signed value causing the observed change in direction of the curve. However, the squaring operator produces a smooth parabola, while the absolute value operator produces a sharp inflection. In 1 dimensional chaotic maps these correspond to the situation seen with the logistic map and the tent map: the former corresponding to the Henon map and the latter to the Lozi map.

At  $a=1.7;\;b=.5; \; (x_0,y_0)=(0,0)$  this map produces a "tent" equivalent of the parabola-like Henon strange attractor.

![lozi_tent](https://manasataramgini.files.wordpress.com/2016/12/lozi_tent.png){width="75%"}Figure 1

However, this map produces richer behavior than the Henon attractor in terms of interesting forms of attractors. One such attractor is seen at  $a=1.5$  and  $b=1$ ,  $(x_0,y_0)=(0,0)$ , where the Lozi map produces the striking "isosceles triangle" attractor with vertices at  $(0,-2); (2,0); (-2,2)$ . Thus the base angles of the triangle are  $\arctan(3)$  and the two sides form angles of  $\arctan(2)$  and  $\arctan(1)=\frac{\pi}{4}$  with the x-axis --- a configuration which results in the famous  $\arctan(1)+\arctan(2)+\arctan(3)=\pi$  expression.

![Lozi_1.5.1.png](https://manasataramgini.files.wordpress.com/2016/12/lozi_1-5-1.png){width="75%"}Figure 2

Incidentally, this triangle and its bounding square also presents the creases for an origami base, which can be used to prove the above identity in a self-evident way ([Also see our earlier note for trigonometric proof of same](https://manasataramgini.wordpress.com/2014/01/29/idiosyncratic-synesthetic-experiences-in-some-trivial-trigonometric-identities/)). It was used by a man from Japan to make a simple grebe that even a beginner can make. Inside this isosceles triangle the points are chaotically distributed except for fractal exclusion zones. The two biggest exclusion zones are ellipses placed orthogonal to each other and along the vertical axis of the isosceles triangle; they have a major axis to minor axis ratio of 3 which indicates that they are defined by two triangles fused at their bases, similar to the outer isosceles triangle. Also along the axis of the main isosceles triangle are small, nearly regular, pentagonal zones of exclusion --- perhaps a reflection of the characteristic angle  $\arctan(3)$  being close to the pentagonal angle  $\frac{2\pi}{5}=72\^o$ . As one moves from the axis of the main triangle to the sides one gets further and further distorted elliptical and pentagonal zones of exclusion.

![lozi_analysis](https://manasataramgini.files.wordpress.com/2016/12/lozi_analysis.png){width="75%"}Figure 3

When  $a=-1$  and  $b=-1$ ,  $(x_0,y_0)=(4,-2.1)$  the Lozi map produces the famous "Gingerbread man" attractor.

![gingerbread](https://manasataramgini.files.wordpress.com/2016/12/gingerbread.png){width="75%"}Figure 4

Giving at little "nudge" to the parameters  $(a=-1.0001;b=-.9999)$  for the same  $(x_0,y_0)$  results in the map wandering all over the Gingerbread man before eventually (somewhere between  $5 \times 10^4..10^5$  iterations) to start spirally converging to five stable points:(  $(-1,-1);\; (3,-1);\; (5,1);\;(3,5);\;(-1,1)$ ).

![gingerbread_5convergence](https://manasataramgini.files.wordpress.com/2016/12/gingerbread_5convergence.png){width="75%"}Figure 5

When one increases  $a$  in the Gingerbread man configuration slightly, e.g.  $a=-1.001;\;b=-1$ , we get a Gingerbread man with a somewhat smooth "aura".

![gingerbread_aura](https://manasataramgini.files.wordpress.com/2016/12/gingerbread_aura.png){width="75%"}Figure 6

Further increases in the  $a$  result in larger and larger strange attractors where the Gingerbread man acquires increasing number of "legs" and a multi-spiked aura.

![lozi_big_ginger](https://manasataramgini.files.wordpress.com/2016/12/lozi_big_ginger.png){width="75%"}Figure 7: The attractor for  $a=-1.48, b=-1$  with a tripodal Gingerbread man.

![Lozi_big_ginger2.png](https://manasataramgini.files.wordpress.com/2016/12/lozi_big_ginger2.png){width="75%"}Figure 8: The attractor for  $a=-1.7523, b=-1$  with a pentapodal Gingerbread man.

Just as with the Henon attractor, we also explored the escape plot of the Lozi attractors where we plot regions in a different color depending on the number of iterations in which they escape towards  $\infty$ . When we do this for  $a=-1, b=-1$ , i.e. the standard Gingerbread man parameters we get a interesting fractal region of entrapment (for the number of iterations we used to test escape  $n=100$ ) at the middle of which lies the Gingerbread man attractor we plotted above.

![Lozi_Julia.png](https://manasataramgini.files.wordpress.com/2016/12/lozi_julia.png){width="75%"}Figure 9

Outside of this basin of entrapment is a collage-like pattern of different escape zones. These are bounded from the outermost escape zone by a cardiomorph boundary. We wondered as to why this boundary arises and what might be the form of the function which specifies it? The answer to lies in the generating equations of the Lozi map itself. In order to compute the potential for escape to infinity we use the radius from the center of the plot region. This, with the Lozi map leads to the equation:

 $$\left(1-a\left|x\right|+by\right)^2+x^2=1$$ 

[https://www.desmos.com/calculator/psvopnbtq7](https://www.desmos.com/calculator/psvopnbtq7){rel="nofollow"}

Indeed the shape of the cardiomorph depends on the  $(a,b)$  values as can be see from the below version of the escape plot for  $a=.2, b=1.02)$  where the attractors are two points in the two entrapment regions or they escape to  $\infty$  along the central line between them.

![lozi_julia_02](https://manasataramgini.files.wordpress.com/2016/12/lozi_julia_02.png){width="75%"}Figure 10

This study in our youth of the Lozi map then lead us to discover another related attractor, which we arrived at by a rather simple modification of the Lozi map:

 $$x_{n+1}=1-a \sqrt{|x_n|}+by_n\\ y_{n+1}=x_n$$ 

where  $a$  and  $b$  are constants.

At positive  $a \approx 1.25 \;..\; 1.9$  and  $b \approx -.1567 \;..\; .1567$  it leads to an incomplete deltoid-like attractor that is at border of chaos and regular convergence. When  $b=-1$  this map yields a reasonably rich and aesthetically fairly pleasing set of strange attractors. We term these the butterfly attractors because most them have a vaguely four-winged form with a bilateral symmetry along the  $x=y$  line. They appear when  $b=-1$ .

![lozi_sqrt_attractors](https://manasataramgini.files.wordpress.com/2016/12/lozi_sqrt_attractors.png){width="75%"}Figure 11

They are fairly stable over a large range of  $a$  values with the attractor increasing in size with increasing  $a$ .

![lozi_sqrt_attractors03](https://manasataramgini.files.wordpress.com/2016/12/lozi_sqrt_attractors03.png){width="75%"}Figure 12

