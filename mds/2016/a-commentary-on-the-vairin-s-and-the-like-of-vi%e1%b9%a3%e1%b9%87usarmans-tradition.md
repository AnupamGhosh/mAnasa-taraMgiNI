
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A commentary on the vairin-s and the like of विष्णुशर्मन्'s tradition](https://manasataramgini.wordpress.com/2016/06/28/a-commentary-on-the-vairin-s-and-the-like-of-vi%e1%b9%a3%e1%b9%87usarmans-tradition/){rel="bookmark"} {#a-commentary-on-the-vairin-s-and-the-like-of-वषणशरमनs-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 28, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/06/28/a-commentary-on-the-vairin-s-and-the-like-of-vi%e1%b9%a3%e1%b9%87usarmans-tradition/ "6:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In one sense विष्णुशर्मन्'s political presentation was nothing short of revolutionary. One may rightly ask: why so? One could say after all he was merely encapsulating in tales the principles already laid out by the ancient आर्य-s and thoroughly presented by विष्णुगुप्त चाणक्य before him. To counter it one may say the proof is in the pudding it -- brilliant as it was, the अर्थशास्त्र was forgotten even by the Hindus (although much to their detriment). But the tantra-s of विष्णुशर्मन् were widely appreciated as a serious educational text, be it by the Zoroastrian Iranians, the Syrians, or even our enemies, the मरून्मत्त-s of Arabia and Turkey. For example, it was part of the "curriculum" of Sultan Suleyman the Osman Amir al-Momin who carried मरून्माद deep into the lands conquered earlier by प्रेतोन्माद.

Yet the primary reason why we stress the revolutionary nature of विष्णुशर्मन्'s teaching is because of this:

[इश्वराणाम् इदं तन्त्रं प्रायेणौत्सुक्यम् आवहेत् ।]{style="color:#99cc00;"}\
[यतस् तिरश्चां चरितैर् नीतिमार्गः प्रदर्श्यते ॥]{style="color:#99cc00;"}\
This text of the gods might appear puzzling due to its teachings;\
However, it intends illustrating by actions of animals the path of right politics.

Human politics is ultimately biological in origin; hence, other examples from the biological world, examples from fellow animals as विष्णुशर्मन् used, should be taken seriously. [As we have briefly pointed out before विष्णुशर्मन् was a keen observer of biological conflict in nature](https://manasataramgini.wordpress.com/2014/04/13/wise-vishnusharmans-vignette-on-biological-warfare/). It is with this lesson in mind one can understand [this story](https://manasataramgini.wordpress.com/2015/03/31/the-dream-motif/) fully. Likewise, an example is also expounded in [this story](https://manasataramgini.wordpress.com/2015/01/13/the-caves/). Human conflict is just another animal conflict and should be analyzed with the same lens rather than creating constructs that have no foundations in the world of the living.

So what विष्णुशर्मन् was doing was to put the nature of politics back into its primal context. He was not the first to do this among the आर्य-s though he was perhaps one of the most effective in presenting this. The history of this tradition goes back to our national epic the महाभारत, where one encounters the tale of the long-necked camel's death at the jaws of the jackal couple. This is a lesson in optimization seen in nature: the camel by acquiring a very long neck greatly increased the ease with which it procured food; however, this made it clumsy and vulnerable to predation, which ultimately brought its end. Indeed, such teachings were provided even earlier by king of the gods Indra in the ऋग्वेद itself albeit in a very cryptic form to Vasukra; e.g.:\
[इदं सु मे जरितर् आ चिकिद्धि]{style="color:#99cc00;"}\
[प्रतीपं शापं नद्यो वहन्ति ।]{style="color:#99cc00;"}\
[लोपाशः सिंहम् प्रत्यञ्चम् अत्साः]{style="color:#99cc00;"}\
[क्रोष्टा वराहं निर् अतक्त कक्षात् ॥]{style="color:#99cc00;"} RV 10.28.4

Know well this \[teaching] of mine O chanter \[of mantra-s] (i.e. Vasukra),\
the resisting debris is borne away by the rivers,\
the fox stealthily (अत्सार् Vedic लुङ् of the latterly rare verb tsarati) proved a match for the lion,\
the jackal charged at the boar from its \[secret] hiding place.\
Here we merely present this to indicate the presence of such tales in the teachings of the gods in the RV itself (i.e. इश्वराणाम् इदं तन्त्रं) but shall not get into the cryptic intricacies of the RV text.

Returning to विष्णुशर्मन् we shall now look into an important teaching regarding conflict in the second tantra. Now any student of the पञ्चतन्त्र should realize that in learning lessons from it one needs to keep dharma in mind -- that it has many textures and contextual wrinkles that are important for its correct application, else one may look like a "[वसुधैव कुटुंबकम् ।]{style="color:#99cc00;"}" mouthing Indian politician or [the बाबाजी of this story](https://manasataramgini.wordpress.com/2014/08/03/the-fourth-story/). The point we are going to talk about is presented in the conversation between the mouse हिरण्यक and the crow Laghupatanaka.

[हिरण्यक आह: अहो त्वं भोक्ता । अहं ते भोज्य-भूतः । तत् का त्वया सह मम मैत्री ? तद् गम्यताम् । मैत्री विरोध-भावात् कथम् ? उक्तं च-]{style="color:#99cc00;"}

The mouse हिरण्यक said: Well, you are the eater. I am among those who become food. Thus, how can my friendship be with you. You ought to leave. What friendship can exist between opposite natures. It has been said \[in the texts]:

[ययोर् एव समं वित्तं ययोर् एव समं कुलम् ।]{style="color:#99cc00;"}\
[तयोर् मैत्री विवाहश् च न तु पुष्ट-विपुष्टयोः ॥]{style="color:#99cc00;"} (1)

Only the two whose wealth is similar; only the two whose clans are of similar \[standing],\
can have friendship and marriage; indeed, it cannot be between the well-endowed and unendowed.

[तथा ca-]{style="color:#99cc00;"}\
[यो मित्रं कुरुते मूढ आत्मनो 'सदृशं कुधीः ।]{style="color:#99cc00;"}\
[हीनं वाप्य् अधिकं वापि हास्यतां यात्य् असौ जनः ॥]{style="color:#99cc00;"} (2)\
[तद् गम्यताम् इति ।]{style="color:#99cc00;"}

Moreover: The dim-witted idiot who makes friends with those unlike himself,\
with a lesser or a greater one, such a person ends up as a laughing-stock.\
You ought to go.

[वायस आह: भो हिरण्यक ! एषो 'हं तव दुर्ग-द्वार उपविष्टः । यदि त्वं मैत्री न करोषि ततो 'हं प्राण-मोक्षणं तवाग्रे करिष्यामि । अथवा प्रायोपवेशनं मे स्यात् इति ।]{style="color:#99cc00;"}

The bird said: Hey हिरण्यक! I have \[just] alighted at your fort-door. If you do not make friendship then I will give up my life in front of you or else I may starve to death.

[हिरण्यक आह: भोः ! त्वया वैरिणा सह कथं मैत्रीं करोमि ? उक्तं च-]{style="color:#99cc00;"}

हिरण्यक said: Hey! What friendship can I make with you an enemy. It has been said \[in the texts]

[वैरिणा न हि सन्दध्यात् सुश्लिष्टेनापि सन्धिना ।]{style="color:#99cc00;"}\
[सुतप्तम् अपि पानीयं शमयत्य् एव पावकम् ॥ (३)]{style="color:#99cc00;"}

Never enter an entente with with an enemy however strongly tied he may be by the alliance\
\[for] water even if well heated puts off fire.

[वायस आह: भोः ! त्वया सह दर्शनम् अपि नास्ति । कुतो वैरम् ? तत् किम् अनुचितं वदसि ?]{style="color:#99cc00;"}

The bird said: Hey! I have not even had a glimpse of you. What enmity? Why are you uttering such unfitting \[words]?

[हिरण्यक आह: द्विविधं वैरं भवति । सहजं कृत्रिमं च । तत् सहज-वैरी त्वम् अस्माकम् । उक्तं च-]{style="color:#99cc00;"}

हिरण्यक said: There are two types of enmity: natural and incidental. Now, you and us are natural enemies. It has been said \[in the texts]:

[कृत्रिमं नाशम् अभ्येति वैरं द्राक् कृत्रिमैर् गुणैः ।]{style="color:#99cc00;"}\
[प्राण-दानं विना वैरं सहजं याति न क्षयम् ॥]{style="color:#99cc00;"} (4)\
Incidental enmity comes to an end swiftly with specifically made reparations,\
\[however,] natural enmity does not come to an end without the death \[of the foes]

[वायस आह: भोः ! द्विविधस्य वैरस्य लक्षणं श्रोतुम् इच्छामि । तत् कथ्यताम् ।]{style="color:#99cc00;"}

The bird said: Alright! I wish to hear the characteristics of the two types of enmity; sir, may they be described.

[हिरण्यक आह: भोः ! कारणेन निर्वृतं कृत्रिमम् । तत्-तद्-अर्होपकार-करणाद् गच्छति । स्वाभाविकं पुनः कथम् अपि न गच्छति । तद् यथा नकुल-सर्पाणां, शष्पभुङ्-नखायुधानां, जल-वह्न्योः, देव-दैत्यानां, सारमेय-मार्जराणां, ईश्वर-दरिद्राणां, सपत्नीनां, सिंह-गजानां, लुब्धक-हरिणानां, श्रोत्रिय-भ्रष्ट-क्रियाणां, मूर्ख-पण्डितानां, पतिव्रता-कुलटानां, सज्जन-दुर्जनानाम् । न कश्चित् केनापि व्यापादितः। तथापि प्राणान् सन्तापयन्ति ।]{style="color:#99cc00;"}

हिरण्यक said: Yes! Incidental \[enemity] is produced by particular causes. Thus, it goes away when a suitable act reparation is made. Again, natural enmity never ever goes away. Thus, it is like that between: mongooses and snakes, herbivores and carnivores (literally armed with claws), fire and water, deva-s and daitya-s, dogs and cats, rich and poor, between competing wives, lions and elephants, hunters and deer, Vedic ritualists and rite-breakers, fools and scholars, chaste wives and sluts, good and evil people. Though no one of these has harmed the other \[in the pair] with a particular \[cause] they still fight to take the others life (What is being implied is their conflict does not arise due a particular redressable affront (as in the कृत्रिम enmity) but it is natural, i.e. by their very existence they are enemies of each other).

[तच् च स्वाभाविकं वैरं द्विविधं भवति । एकाङ्ग-वैरम् उभय-वैरं च ॥]{style="color:#99cc00;"}

And then natural enmity is of two types: unilateral enmity and bilateral enmity.

[वायस आह: कस् तयोर् विशेषः?]{style="color:#99cc00;"}

The bird said: What are the special features of the two?

[सो 'ब्रवीत्: यो विहन्यात् परस्परम् । अन्यो'न्येन भक्ष्यते । परस्परापकारात् तद् उभयवैरम् । यथा सिंह-गजानां । यः पूर्वम् एव हत्वा भक्षयति न च+असौ तस्य+अपकरोति न हिनस्ति न भक्षयति । तद् एकाङ्गवैरम् अकस्मात् यथा अश्व-महिषाणां मार्जार-मूषकानाम् अहि-नकुलानाम् । किम् अश्वो महिषस्य सर्पो वा बभ्रो मुषको वा मार्जारस्य+अपकरोति । तत् सर्वथा किम् अशक्येन समयकारणेन ?]{style="color:#99cc00;"}

He (हिरण्यक) said: Bilateral \[arises] enmity from the killing of each other, when the two eat each other, from the harm each causes the other, like that between lions and elephants (i.e. they attack each other). When one kills and eats the other, though the other does not harm it or injure it or eat it then it is unilateral enmity without \[reciprocal] reason, like that between horses and buffaloes, cats and mice, snakes and mongooses. Why does a horse harm a buffalo or a snake a mongoose, or a mouse a cat? Then why \[use] all means to create an impossible alliance?

**Commentary**\
• The first major white scholar of the पञ्चतन्त्र Hertel produced critical editions of several पञ्चतन्त्र recensions. In white indological enterprise he was followed by Edgerton who attempted to reconstruct the original पञ्चतन्त्र of विष्णुशर्मन्, whom he without any strong reason thought to be fictitious. While Edgerton's judgment that the original was a more coherent piece than many of the later recensions was correct, we do not consider his reconstruction to be perfect. In this particular case we have not followed his reconstruction but provide a more extended narrative including parts Edgerton has left out from the reconstructed ancestor (the verses are numbered serially). We have taken this material from Ramchandra Jha and DD Kosambi's editions (the latter being quite a good effort despite the author's ideological leanings). We believe it is difficult to be sure of the exact form of the original for this narrative.

• A naturalist might see conflicts forming the frame of विष्णुशर्मन्'s narratives in real life. For example, in our youth as we were stumbling out into the then well-wooded parking lot of our university campus we saw a white laboratory mouse, which had escaped and was scurrying away towards the undergrowth. It was targeted by a crow-couple, which was actively cooperating in the hunt. One, swooping down, created a distraction with its wings and caused the mouse to run in the opposite direction into an ambush laid by the other crow, which with one strike of its beak disemboweled the mouse. Then two crows tore into their little meal. The mouse in a very general sense represents a baseline mammalian body-plan, which has been present since the near contemporaneous rise of the dinosaurs and mammals in the Mesozoic. Indeed, small fossil mammals have been found the fossilized gut contents of more than one dinosaur. Thus, such a conflict between a mammal of such a form and a dinosaur have been now playing out for over 225 million years: a true स्वाभाविक-vairam as हिरण्यक terms it.

• विष्णुशर्मन् also mentions the bilateral conflict between lions and elephants, which may appear unfamiliar to the modern Hindu. While lions are today nearly gone in India, in his days such conflicts must have been known in the wild. We do have examples of such today in Africa as can be seen from this video. \[http://www.arkive.org/lion/panthera-leo/video-11b.html] We do not know much of the buffalo-horse conflict in real life though we have, much to our terror, experienced more than once the sudden violent behavior of domesticated Indian buffaloes -- something which could also easily spook horses. Moreover, in विष्णुशर्मन्'s days wild buffalo was common in India and must have been quite an aggressive animal like the African buffalo, which has been reported to be a threat to horses.

• Other observations by विष्नुशर्मन् elsewhere in the PT also point to his keen eye for conflict in nature. For instance he says:\
[अरितो 'भ्यगतो भृत्यः शत्रु-संवास्-तत्परः ।]{style="color:#99cc00;"}\
[सर्प-संवास-धर्मित्वान् नित्योद्वेगेन दूषितः ॥]{style="color:#99cc00;"}

A follower defecting from the enemy-side intent on living with his \[former] foemen,\
has the problem of causing constant worry \[like] having to cohabit with a serpent.

[प्लक्ष-न्यग्रोध-बीजाशात् कपोताद् इव शाल्मलेः ।]{style="color:#99cc00;"}\
[मूलोत्खातकरो दोषः पश्चाद् अपि भयंकरः ॥]{style="color:#99cc00;"}

Like the \[danger] of the silk-cotton tree from a dove\
which has eaten seeds of a प्लक्ष or banyan tree,\
of being strangled to death by their roots\
this indeed possess a future threat.

Here the possibility of a defecting enemy (coming in peace like a dove) turning against one is compared to the danger to a silk-cotton tree of accepting a dove which has feed on the figs of a Ficus virens or Ficus benghalensis tree and defecates on it. This illustrates विष्नुशर्मन्'s knowledge of strangler figs. In practical terms if Hindus had kept this in mind they would have perhaps avoided the debacle at Talikota caused from accepting the foes into their camp.

• The concept of the natural enmity expounded by विष्नुशर्मन् is something very apparent to any student of biological conflicts. There need not be a proximal reason for the conflict: it comes from something very deep or encoded in the biology, like in conflicts associated with interactions of prey-predator, host-parasite, or resource competitors. By their very existence the two organisms are vairin-s. Indeed, here extinction of one of the players can be only outcome and the constant arms race can continue over millions of years. The great biologist Leigh van Valen's Law of Extinction is a manifestation of this: the probability of extinction for clade is constant and is independent of how long a species or clade might have existed. Unless a species constantly develops new weaponry to survive conflict it becomes extinct. But then what ever its adaptations it might encounter a predator or a parasite or a competitor that it lacks of the defenses against and can go extinct.

• This concept has bearing even in human conflict within with a memetic component. Modern leftism-liberalism, almost as thought to conceal its Abrahamistic roots, tries to paint all conflict in terms of proximal causes or causes stemming from nurture, as though they were कृत्रिम-vairam. Thus, it hides and actively provide cover for the स्वाभाविक-vairam that is intrinsic to Abrahamism. In this context, it is notable that the पञ्चतन्त्र sees the ईश्वर-दरिद्राणां vairam as belonging to the स्वाभाविक type. This is rather distinct from Marxian view of it where the class-struggle results from the particular cause of the bourgeoisie appropriating the surplus value of the wealth generated by the proletariat without participating in the labor. The view of the पञ्चतन्त्र puts it back in the more primal category of the natural conflict, which exists in primate societies between individuals of different rank with fitness consequences. Thus, lower ranked individuals might try to overthrow the higher ranked ones to acquire their rank and the corresponding fitness benefits. This is what we see when Marxian doctrines are actually practiced rather than their flattened non-biological utopia.

• Again विष्नुशर्मन् classifies the natural enmity into two types -- bilateral and unilateral. One might see this as the "hawk-hawk" versus the "hawk-dove" conflict scenarios. However, this also has implications for human conflicts. When two heathen groups fight it might be from a particular cause (कृत्रिम-vairam) or स्वाभाविक-vairam. When the two heathen groups vastly differ in their social, racial or technological backgrounds the outcome is typically a स्वाभाविक-vairam. Thus, the Moriori being eaten by the Maori is a typical prey-predator interaction type of स्वाभाविक-vairam. Such interactions might have played out many times in the pre-modern and even modern tribal conflicts, like the Paleo-Eskimos probably being destroyed by the Neo-Eskimos and in recent times the pygmies being eaten by large-bodied tribal groups in Africa. Indeed, this illustrates the wisdom of the advice provided in the first of the verses in the above-quoted section.

• But where an Abrahamistic power is involved in a conflict it almost always needs to be analyzed as a स्वाभाविक-vairam. This is a natural consequence of the Mosaic distinction (vide Assmann) element of the Abrahamistic meme-complex. Indeed, such a type of enmity had already been foreseen by the wise विष्णुशर्मन् when he presents श्रोत्रिय-भ्रष्ट-क्रियाणां vairam in the स्वाभाविक category. This enmity between the Vedic ritualist and those of who have given up ritual mirrors that arising from the militant "New Atheists", liberal rationalists, and other variants of today. Moreover, when the conflict involves two Abrahamistic powers, say USA and the Moslems or Judaists and Moslems, it should be analyzed as the bilateral type of the स्वाभाविक-vairam. On the other hand when it involves an Abrahamistic power and a heathen power (e.g. India) it should be analyzed primarily as the unilateral type of the स्वाभाविक-vairam. These details are exactly, what the leftist-liberalist authors (e.g. Jared Diamond) try to obfuscate and keep out of sight.

• Unfortunately, modern Hindus have lost the wisdom of their wise forebears like विष्णुशर्मन्. Hence, they were unable to correctly discriminate स्वाभाविक-vairam from कृत्रिम-vairam. A stark e.g. is presented by events concern the partition of जम्बुद्वीप. Mohammad Ali Jinnah was very clearly presenting his case, which Hindus should have realized as being from स्वाभाविक-vairam. Instead, addled with secularism they thought it was something proximal about people who wanted to go to Pakistan or the territorial separateness of Pakistan relating to political aspirations of मरून्मत्त-s. Furthermore, by treating the problem with the Abrahamists as one of कृत्रिम-vairam the Hindus are hiding from themselves one of the most dreadful truths of स्वाभाविक-vairam i.e. "[प्राण-दानं विना वैरं सहजं याति न क्षयम् ।]{style="color:#99cc00;"}"

• One can ask if there are extensions to the old Hindu discourse on vairam from biology or modern geopolitics. One well-known conflict is that between the myxoma virus and rabbits in Australia. Here, after starting with a bang and decimating rabbits by the millions the myxoma virus settled to a much milder form. This was because the highly virulent forms were selected against as they exhausted their hosts too quickly to sustain further transmission. Thus, the milder forms eventually won out. Some might ask could this not happen to प्रेतोन्माद and मरून्माद. Being parasites of the mind such hopes are limited because they, these days मरून्माद in particular, can be bloody within and without. So even if there is a civilizational decline upon exhaustion of the heathen prey (e.g. Afghanistan, Pakistan, Bangladesh and the European Dark Age) there are no major fitness consequences for the meme, thus making attenuation of virulence towards heathens unlikely. One can compare this scenario to the arrival of Austronesians on certain Pacific islands where they quickly drove the native fauna to extinction (e.g. the many birds of New Zealand) without any attenuation of predatory practices. After they had exhausted such prey cannibalism seems to have been prevalent -- an analogy to the "bloody within and without (vide Huntington)" state.

• One geopolitical example one can consider is Japan a heathen nation following its defeat by white Christians and Marxians. Did their enmity not largely end some might ask. We would say that the enmity has not really ended -- it is only a period of quiescence -- like a pride of lions fleeing from an irate herd of elephants attacking them for the cost of pursuing the conflict at that point is to high for one side. It is not as if the US did not try to impose प्रेतोन्माद on Japan. It tried and continues to try, as major प्रेतभाणक outfits continue to call for volunteers to go east and achieve what they did in Korea. On the other hand, some have also felt that Japan has retaliated by means such as the automobile industry against their conquerors. So because of the power differential we say that the battle-ground has shifted for now but the स्वाभाविक-vairam has not ended.

• Finally, one wonders why, as Monier-Williams had remarked, the Hindus have failed to put their ancestors' teachings into practice. Perhaps, a part of the answer lies in विष्णुशर्मन्'s words themselves. Among the स्वाभाविक-वैराणि he lists मूर्ख-पण्डितानां vairam. It is notable that this is termed स्वाभाविक because a modern might reason that when a मूर्ख receives appropriate education he would become a पण्डित thus putting this enmity in the कृत्रिम category. However, from our observations on fellow Hindus we suspect that विष्णुशर्मन् was correct. As we have remarked before on more than one occasion (also see [this आख्यान](https://manasataramgini.wordpress.com/2015/12/23/lutika-somakhyo%E1%B8%A5-pravada%E1%B8%A5/)) there are several H who are well-educated and seemingly even intelligent (as in possessing markers of above-average IQ) but exhibit a very natural animosity towards any meaningful पाण्डित्यम्. Moreover, the Hindu mass tends to gravitate with great celerity towards the inane and the erroneous while ignoring the perspicacious -- this after all might be a sign of that deep स्वाभाविक divide between the मूर्ख and the पण्डित which the text talks about. Moreover, as civilizations reach old age the मूर्खता perhaps tends to win in this मूर्ख-पण्डितानां vairam, but then that is a story for some other time.


