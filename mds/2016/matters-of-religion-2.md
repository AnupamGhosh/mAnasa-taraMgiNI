
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion-2](https://manasataramgini.wordpress.com/2016/06/29/matters-of-religion-2/){rel="bookmark"} {#matters-of-religion-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 29, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/06/29/matters-of-religion-2/ "7:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Indrasena and Vrishchika were visiting the house of Somakhya and Lootika. They were seated in a corner of the fire-room, where the latter had installed an image of the god कुमार in the midst of a षडर-cakra. Around the image of the god was a circle of images of the twelve dreadful goddesses and a row of images of the seven मातृका-s.\
Somakhya: "Good to see you all back safe from the हिमालय."\
Lootika: "I have been dying to hear the highlights of the trip."\
Vrishchika: "I must say we learned that you two might have fooled us!"\
Lootika looked puzzled and smiled: "Why so अनुजा?"\
Vrishchika: "You might have imparted to us the incomplete कौमार-विद्या."\
Somakhya: "Why don't you tell us the whole story?"\
Vrishchika turned to look at Indrasena.

Indrasena: "Alright, here it is in short. Having reached the land once ruled by the great पृथिवीनारायण we visited the shrine of Jaya-वागीश्वरी and then went to the cave of the old पण्डिता गुह्यसोमा to whose lineage our wives belong. Then we went the yajurvedin whom you had mentioned and paid our respects. He in turn directed us to a somewhat isolated slightly elevated place on the over-lying incline. After trekking out there we visited the man we were supposed meet; Dev Rudrapal was his name, evidently of a local विश्वकर्मन् or तक्षक जाति. He was a young man making a profusion of idols of various deities with extraordinary dexterity. For a while we quietly watched. By then I realized he must be a capable मन्त्रवादिन्. He looked up and asked us to come along. He then opened a wooden case showed us the idols of the seven मातृका-s. This is what you have come for. We responded in the affirmative with the surprise clearly writ on our faces. Having obtained those we then began to ask him about the idols of the कौमार-cakra. Before we could complete the sentence he opened another box and showed us those idols. But as we completed the transaction of giving him the दक्षिण he remarked: "Those कौमार idols will be 'अवीर्य'". We asked why that was so. He brusquely said: "Now go to your teachers and ask them why not me. But let me tell you the निर्माल्य देवता does not favor you.". Now my wife here of pretty tresses, as you know, is not exactly the one to wait for niceties and has come to the issue as curtly as our विश्वकर्मन् in the हिमप्रदेश."

Vrishchika: "When you completed the instruction to Indrasena and me did you all not say: "with this you have all that is needed to attain the व्यूह of the six-headed god.""\
Lootika: "Dear अनुजा how can you be sure that your विश्वकर्मन् told you something true?"\
Vrishchika: "Indrasena and me know enough of mantra-सिद्धान्त to know what he said is the truth. Even those who know the कृतागमाः कौमार-विद्याः are merely उपासक-s not on the path of being siddha-s in the कौमार-शासन. After all only one who knows the निर्माल्य-देवता as per the उदीच्यो घरांनायः, as मन्वर्णवनाथ and सर्वमङ्गला did in the लाटदेश during the days of emperor हर्षवर्धन, can be on that path. Moreover, one who knows it by the navya-द्रंइड-रीति only attains the lower path."

Somakhya: "Vrishchika, as a मन्त्रवादिनी whom I have seen in action from when you were a kid I am sure that you know that some instructions are not directly revealed."\
Indrasena: "That's what I told her. Since the पूर्वतन्त्रम् is not accessible to anyone except the knowers of the complete सिद्धान्त I knew we would need the direct signal from the देवता. I believe we have got it that's why we approach you for a confirmation so that we may now receive the complete सिद्धान्त."\
Somakhya: "When I revealed the विद्या to Lootika, I did not give her the निर्माल्य-देवता too. The test to see whether she was fit for साधन was whether she got it on her own seeing my actions -- since you know well that we have no idol of the निर्माल्य-देवता unlike for Indra or Rudra. I would let her tell you how she arrived at it without mentioning their names and then you can see if you obtained the right signal."

Lootika: "Indeed Somakhya did not impart me the rahasya of the निर्माल्य-देवता upon imparting the कल्पोक्ता विद्या-s to me when we were young. Having studied the ritual I wondered if the Skanda ever had a निर्माल्य-देवता. Sometime during the summer festival of कार्त्तिकेय our family and that of Somakhya's were visiting the hilltop shrine of the god. There during the public service I closely observed the देशिक the temple had called to perform the rite. He offered the निर्माल्य to the गण वीरबाहु. Indeed, that's not a wrong procedure. In the old days among the द्रमिड peoples ruled a powerful line of kings well-versed in rituals known as the पाण्ड्य-s with their capital at दक्षिण-mathura. On the outskirts of the old town of these द्रमिड-s was a giant temple of Guha. There the देशिक-s of the पाण्ड्य-राजन् performed the ritual as specified in the दक्षिण-घरांनाय taught by Madhurai मार्कण्डेय the वैखानस. In that the proper vidhi with the निर्माल्य-देवता as वीरबाहु was specified. But the दक्षिण-घरांनाय-s kalpa was completely lost though the कौमार-शासन continued to flourish in that part of the country. Only allusions of that ancient tradition survive in old द्रमिड songs. So the निर्माल्य offering has no appropriate mantra. After that the families got to do their own rituals. Somakhya's father asked him to perform it on all our behalf. I observed closely and saw that he placed a पिण्ड of haridra prepared with the juice of the Calotropis gigantea plant. Before invoking the निर्माल्य-देवता I saw him sever it vertically into two halves with the darbha grass. Immediately I knew the नीर्माल्य देवता as per the northern घरांनाय as was expounded by अचलनाथ and his दूती मयूरशिखा at the अचलक्षेत्र in the पाञ्चनद."

Indrasena: "So indeed the देवता-s are M...". Somakhya extended his hand to stop Indrasena from uttering their names aloud. Vrishchika seeing that remarked: "Then that must be it!"\
Somakhya: "Before commencing with the instruction of their mantra first worship the लिङ्ग of Rudra with the mantra-s starting with "[निधनपतये नमः ।]{style="color:#0000ff;"} ...". Then utter the following incantations and offer arka flowers to each of the deities of those mantra-s:\
[ॐ य र ल व श ष स हों सोहं हंसः सोहं हंसः लोहितायन्यै नमः ।]{style="color:#0000ff;"}\
[ॐ य र ल व श ष स हों सोहं हंसः सोहं हंसः वृक्षिकाभ्यो नमः ।]{style="color:#0000ff;"}\
[ॐ य र ल व श ष स हों सोहं हंसः सोहं हंसः भुवोद्भूभ्यो नमः ।]{style="color:#0000ff;"}\
[ॐ य र ल व श ष स हों सोहं हंसः सोहं हंसः अर्करश्मिकाभ्यो नमः ।]{style="color:#0000ff;"}\
[ॐ स्कन्द-पार्षदीं तर्पयामि । ॐ षष्ठी-पार्षदीं तर्पयामि । सर्वेभ्यो रुद्र-रेतस उद्भवेभ्यो गणेभ्यो नमः ॥]{style="color:#0000ff;"}

Thereafter Somakhya whispered mantra of the देवता-s in the ear of Indrasena and Lootika did the same Vrishchika.

Somakhya: "Now we shall proceed with that old rahasya of the कौमारशासन, the prayoga of भाल्लवि. The भाल्लवि-ब्राह्मण states:\
[हरः कुमार-रुपेण ब्रुवंस् ताम् अभयभाषत ।]{style="color:#0000ff;"}\
[विज्योतिषेति चोक्तायां सहसाग्निर् उद्ज्वलत् ।]{style="color:#0000ff;"}\
[सहमानः समायान्तं प्रकाशं च प्रकाश्यन् ।]{style="color:#0000ff;"}\
[पिषाचीम् अधतात् तां स यत्र चोपविवेश सा ॥]{style="color:#0000ff;"}

This refers to the tale of वृश the badger-like purohita of the इक्ष्वाकु lord त्र्यरुण of the lineage of the great आर्य conqueror Trasadasyu. वृश the master of Atharvan charms noted that a पिशाची in the form a woman had taken over त्र्यरुण and sucked away all the energy from his ritual fires. He seated himself next to her addressed the fiery energy with the mantra "spreading with light". That energy in the form of कुमार emerged forth conquering all those who came at him lit up even what was already illuminated. He then burned down the पिशाची right where she was seated. Thus, being born suddenly he is known as Sadyoj[आ]{style="display:inline !important;float:none;background-color:transparent;color:#3d596d;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:19.2px;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}ta as per the Atharvan-s and as वचद्भू (he who manifests as the incantation is being recited) and यज्ञभू (he who manifests as the ritual is being performed) by the तैत्तिरीयक-s."

Somakhya continued: "Indrasena, now of course I know you will identify the सूक्त being referred to. But we must test your wife. So उपगौतमी can you identified the सूक्त being indicated for the prayoga?"

Vrishchika: "Well, while the badger-like वृश was the purohita of the इक्षवाकु-s of the lineage of emperor Trasadasyu, they were patrons of ब्राह्मण-s of the Atri lineage. It was these Atri-s who composed a सूक्त to Agni as कुमार, which वृश used. Thus, it is the mysterious सूक्त beginning with[कुमारम् माता युवतिः समुब्धं]{style="color:#0000ff;"}.

Somakhya: "That's good उपगौतमी."

Then Somakhya kindled the fire on the स्थण्डिल placing a samidh on it using the mantra:\
[मित्रो अग्निर् भवति यत् समिद्धो मित्रो होता वरुणो जातवेदाः ।]{style="color:#0000ff;"}\
[मित्रो अध्वर्युर् इषिरो दमूना मित्रः सिन्धूनाम् उत पर्वतानाम् ॥]{style="color:#0000ff;"}

Agni becomes Mitra when kindled, the होतृ is Mitra and वरुण is Jatavedas.\
Mitra is the agile Adhvaryu, friend of the household, \[he is] the Mitra who is in the rivers and the mountains.

Somakhya: "A knower of the कौमारशासन should realize why it is important to use the mantra of the old vipra विश्वामित्र to kindle the fire for he was the "discoverer" of कुमार."

Then Lootika filled the sruk to the brim with ghee using the sruva and handed it over to him. As she touched Somakhya with a tuft of darbha grass, holding the sruk aloft with the flange Somakhya uttered the following six mantra-s and after each of the first five mantra-s made an offering in the fire with स्वाहा ([इदं कुमाराय आग्नेयाय न मम]{style="color:#0000ff;"}):

1)[कुमारम् माता युवतिः समुब्धम् ...]{style="color:#0000ff;"}\
2)[कम् एतं त्वं युवते कुमारम् ...]{style="color:#0000ff;"}\
3) [हिरण्यदन्तं शुचिवर्णम् आरात् ...]{style="color:#0000ff;"}\
4)[क्षेत्राद् अपश्यं सनुतश् चरन्तं ...]{style="color:#0000ff;"}\
5) [वि ज्योतिषा बृहता भात्य् अग्निः ...]{style="color:#0000ff;"}\
6) [उत स्वानासो दिवि षन्त्व् अग्नेः ...]{style="color:#0000ff;"}

After the 6th mantra he made an offering with वषट्.

Then Lootika filled the sruk again and handed it over to Somakhya as he uttered the following mantra and made a offering pouring out the ghee entirely with स्वाहा ([इदं सप्तमातापरिवृताय न मम]{style="color:#0000ff;"}) :\
[जज्ञानं सप्त मातरो वेधाम् अशासत श्रिये ।]{style="color:#0000ff;"}\
[अयं ध्रुवो रयीणां चिकेत यत् ॥ +स्वाहा ॥]{style="color:#0000ff;"}

At his birth for glory, the seven mothers instructed him as the wise one, by knowing which he is firm of opulence.

Somakhya: "These are the vedokta oblations as promulgated by the भाल्लवि-s. They have an important role in अभिचारिक function. However, if a further offering is made with the following mantra taught by sage बोधायन it also serves a pediatric function:\
[ॐ अघोराय महाघोराय नेजमेषाय नमो नमः ॥]{style="color:#0000ff;"}

It was the mantra that was deployed by the क्षत्रिय-s for the birth of the nagna, the founder of the नास्तिक-mata of the nirgrantha-s."

Vrishchika: "That is interesting. In this context our mother would tell a mysterious tale in this regarding the births of us four sisters. Each time she was carrying one of us in her womb she would have a dohada to go to the west coast. There, even as she felt one us to be moving particularly actively in her womb, she would see a dream wherein would appear a terrifying goddess who would say: '*Behold me. I am none other than पूतना. The भागवत-s might have fed you the fiction that देवकीपुत्र slew me but remember all that is a brazen lie much like their myth that he defied the great Indra by lifting a mountain. You may run to Indra or to विष्णु but none of them can save your daughter from my grip unless the god कुमार so wishes.*' Suddenly, whichever of us she would be bearing in her womb would cease to move. Filled with fear, she would get up and at once go to a small, ancient shrine of Skanda which had been installed originally by षष्ठीशनाथ. Having seen the image of the god through the railing she would come back very tired and lie down again. Then she would have another dream. Therein she would see four youthful gods and a youthful goddess, an ape-headed deity carrying a trident and a goddess wearing a mask. The masked-goddess would then say: '*If you name your daughter with a poisonous name by which lay people would be repelled then she would live past her sixth day and attain great glory*.' That's how we came to be named. But after Jhilleeka's birth she witnessed three goddess in her dream. One was six-headed like the youthful goddess she saw before, the other was dolphin-headed and the third had a single beautiful head. On seeing them she forgot whatever mantra-s of those deities which the masked goddess had revealed to her."

Lootika: "Indeed, it is said कुमार is a dangerous deity. While conferring vara-s that bestow extraordinary abilities his agents can also cause possession or harm at the same time of conferring those vara-s. Hence, knowing and appropriately propitiating that pantheon of deities is important. The four youthful deities who appeared in our mother's dream were Skanda, शाख, विशाख, and नेजमेष. The youthful goddess was षष्ठी; the ape-headed deity was नन्दिकेश्वर and the masked goddess was मुखमण्डिका. The goddesses who took away the mantra-s from her were in addition चतुष्पथनिकेता and शिशुमारमुखी. A key to the complete सिद्धान्त is श्रेधिविद्या and the corresponding yantra."

continued...


