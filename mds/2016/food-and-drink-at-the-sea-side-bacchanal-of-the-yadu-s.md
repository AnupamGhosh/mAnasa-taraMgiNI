
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Food and drink at the sea-side bacchanal of the yadu-s](https://manasataramgini.wordpress.com/2016/05/16/food-and-drink-at-the-sea-side-bacchanal-of-the-yadu-s/){rel="bookmark"} {#food-and-drink-at-the-sea-side-bacchanal-of-the-yadu-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 16, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/05/16/food-and-drink-at-the-sea-side-bacchanal-of-the-yadu-s/ "7:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Sections 2.88-89 of the हरिवंश (विष्णुपर्वन्) gives a graphic description of the bacchanal of the yadu-s at the sea-side or their celebration of the samudrotsava. It has a beautiful ring to it and gives a feel for the festive culinary excesses of the आर्य-s, or at least their yadu branch, after they had settled in the subcontinent. We hence present it below, especially given that a notable section of Hindus who still read such texts tend to be rather coy about sections such as these. The two great and dramatic images from the epic -- the sea-side bacchanal and their final end in the sea-side drunken brawl are a stark illustration of how an exuberant existence like that of the yadu-s is not for ever.

[उपेन्द्रम् उत्तीर्णम् अथाशु दृष्ट्वा]{style="color:#0000ff;"}\
[ भैमा hi te tatyajur eva toyam ।]{style="color:#0000ff;"}\
[विविक्त-गात्रास्त्व् अथ पान-भूमिं]{style="color:#0000ff;"}\
[कृष्णाज्ञया ते ययुर् अप्रमेयाः ॥]{style="color:#0000ff;"} 2-89-55

Having seen the brother of Indra (विष्णु as कृष्ण) swiftly get out the Bhaima-s (Yadu-s) also abandoned the water. Now with their bodies cleansed they went to the drinking-arena.

[यथा+अनुपूर्व्या च यथा वयश्च]{style="color:#0000ff;"}\
[यत् सन्नियोगाश् च तदोपविष्टाः ।]{style="color:#0000ff;"}\
[अन्नानि वीरा बुभुजुः प्रतीताः]{style="color:#0000ff;"}\
[पपुश् च पेयानि यथा+अनुकूलम् ॥]{style="color:#0000ff;"} 2-89-56

By the order of their rank and their relationships they seated themselves in there. The heroes having made themselves comfortable ate the foods and drank the drinks of their liking.

[मांसानि पक्वानि फलाम्लकानि]{style="color:#0000ff;"}\
[चुक्रोत्तरेण+अथ च दाडिमेन ।]{style="color:#0000ff;"}\
[निष्टप्त-शूलाञ् छकलान् पशूंश्च]{style="color:#0000ff;"}\
[तत्रोपजह्रुः शुचयो'थ सूदाः ॥]{style="color:#0000ff;"} 2-89-57

There (in the feasting arena) hygienic cooks then served them meats marinaded in acidic fruit juices, excellent vinegar and pomegranates, and slices of animals roasted on stakes.

[सुस्विन्न-शूल्यान् महिषांश् च बालाञ्]{style="color:#0000ff;"}\
[छूल्यन्-सुनिष्टप्त घृतावसिक्तान् ।]{style="color:#0000ff;"}\
[वृक्षाम्ल-सौवर्चल-चुक्रपूर्णा]{style="color:#0000ff;"}\
[पौरोगवोक्त्या उपजह्रुर् एषाम् ॥]{style="color:#0000ff;"} 2-89-58

Instructed by the chief chef \[the cooks then] served them well-boiled and roasted buffalo and lamb \[meat] impaled on spikes, sprinkled with ghee and well-marinaded with sour-vegetable juice, black salt and vinegar.

[पौरोगवोक्त्या विधिना मृगाणां]{style="color:#0000ff;"}\
[मांसानि सिद्धानि च पीवराणि ।]{style="color:#0000ff;"}\
[नाना-प्रकाराण्य् उपजह्रुर् एषां]{style="color:#0000ff;"}\
[मृष्टानि पक्वानि च चुक्र-चूतैः ॥]{style="color:#0000ff;"} 2-89-59

Instructed by the chef with due procedure \[the cooks] served them dressed and cooked flesh and fat of various animals which were glazed with \[a sauce] of vinegar and mangoes.

[पार्श्वानि चान्ये शकलानि तत्र]{style="color:#0000ff;"}\
[ददुः पशूनां घृत-मृक्षितानि ।]{style="color:#0000ff;"}\
[सामुद्र-चूर्णैर्-अवचूर्णितानि]{style="color:#0000ff;"}\
[चूर्णेन मृष्टेन समारिचेन ॥]{style="color:#0000ff;"} 2-89-60

Thereafter they gave \[the yadu-s] ribs and other slices of animals smeared with ghee mixed with powdered sea-salt and ground black pepper.

[समूलकैर्-दाडिम-मातुलिङ्गैः]{style="color:#0000ff;"}\
[पर्णास-हिङ्ग्वार्द्रक-भूस्तृणैश् च ।]{style="color:#0000ff;"}\
[तदोप-दंशैः सुमुखोत्तरैस्ते]{style="color:#0000ff;"}\
[पानानि हृष्टाः पपुर् अप्रमेयाः ॥]{style="color:#0000ff;"} 2-89-61

\[They served] radishes, pomegranates, sweet limes, mahagony fruits spiced with asa foetida, ginger, and broad-leafed turpentine grass. \[Then] the incomparable heroes joyously drank drinks from rhytons with beautiful faces.

[कट्वाङ्क शूलैरपि पक्षिभिश् च]{style="color:#0000ff;"}\
[घृताम्ल-सौवर्चल-तैलसिक्तैः ।]{style="color:#0000ff;"}\
[मैरेय-माध्वीक-सुरासवांस्ते]{style="color:#0000ff;"}\
[पपुः प्रियाभिः परिवार्यमाणाः ॥]{style="color:#0000ff;"} 2-89-62

Along with bird-flesh impaled and \[fried] in ghee and oil, marinaded with black salt, vinegar and bitter-gourd paste they drank wine, mead, beer and rum along with their dear female companions and retinue.

[श्वेतेन युक्ता नृप शोणितेन]{style="color:#0000ff;"}\
[भक्ष्यान्-सुगन्धांल् लवणान्वितांश् च ।]{style="color:#0000ff;"}\
[आर्द्रान्-किलाटान्-घृत-पूर्णकांश् च]{style="color:#0000ff;"}\
[नाना-प्रकारान् अपि खण्ड-खाद्यान् ॥]{style="color:#0000ff;"} 2-89-63

O king \[Then] they ate \[desert] which was white and red in color, great-smelling and salty, and confections of various types which were soft and filled with ghee and made from congealed milk.

[अपानपाश् चोद्धव-भोजमिश्राः]{style="color:#0000ff;"}\
[शाकैश् च सूपैश् च बहु-प्रकारैः ।]{style="color:#0000ff;"}\
[पेयैश् च दध्ना पयसा च वीराः]{style="color:#0000ff;"}\
[स्वन्नानि राजन् बुभुजुः प्रहृष्टाः ॥]{style="color:#0000ff;"} 2-89-64

O king Uddhava and the Bhoja मिश्र ब्राह्मण-s who did not drink liquors \[consumed] vegetarian \[dishes] and soups of various kinds. Joyously these valiant ones drank curds and पायस and ate good food.

[तथारनालांश् च बहु-प्रकारान्]{style="color:#0000ff;"}\
[पपुः सुगन्धान् अपि पालवीषु ।]{style="color:#0000ff;"}\
[शृतं पयः शर्करया च युक्तं]{style="color:#0000ff;"}\
[फल-प्रकारांश् च बहूंश् च खादन् ॥]{style="color:#0000ff;"} 2-89-65

Then they drank well-flavored sour rice gruel of various kinds and boiled milk sweetened with sugar from tumblers and ate various types of fruits.

[तृप्ताः प्रवृत्ताः पुनरेव वीरा-]{style="color:#0000ff;"}\
[स्ते भैममुख्या वनितासहायाः ।]{style="color:#0000ff;"}\
[गीतानि रम्याणि जगुः प्रहृष्टाः]{style="color:#0000ff;"}\
[कान्ताभि-नीतानि मनोहराणि ॥]{style="color:#0000ff;"} 2-89-66

Again getting up satisfied \[with the feasting] the leaders of the bhaima (yadu-s) along with the ladies joyfully sang beautiful songs charmingly prompted by their wives.


Certain points are notable here: To any modern Hindu it would be obvious that the menu of the yadu-s was rather different from that of modern Hindus from the same region. In being heavy on meat, ghee and milk products it is certainly close to that of the ancestral Indo-Aryans on the Eurasian steppes. The flavoring, which is repeatedly emphasized in the account, is relatively elaborate and clearly represents, at least in part, the Indianization of the Indo-Aryan menu. It primarily comprises of sea salt, black salt, vinegar, asa foetida, ginger, scented grass, black pepper, bitter gourd, mango, pomegranate, and probably the acidic juices of gooseberry, Garcinia indica (चुक्राम्ल) and tamarind. The fruits from which the acidic juices were obtained are not explicitly named, but any or all of them could be what is implied by फलाम्लकानि and वृक्षाम्ल. However, many of the well-known later day Indian spices are not named here. Whether it is merely a textual omission or whether it really reflects the early Indo-Aryan dietary preference in what is today Gujarat is still unclear. Of these the हिङ्गु (Asa foetida; latex of Ferula) was probably the first the आर्य-s adopted even as they entered the Bactria-Margiana region (where it was likely native) and it is even mentioned in the Atharvaveda. In addition, the term anna while translated generally as food could also specifically mean rice in the above account.

Likewise, the fare of fruits/vegetables is relatively simple: radish (turnip), mango, pomegranate, sweetlime and mahogany fruits. The last of these is no longer popular in India and some fruits which are known to have been used in the Indus valley civilization, like the kadali, do not figure in this account. They are not common until much later in Sanskrit tradition. In this context the word दाडिम raises a question: We usually take it to mean pomegranate. However, Sanskritists have told us that here it might mean the cardamom. If this were true then it would give us one more spice. However, the pomegranate was probably well-known to the Indo-Iranians via trade even as they neared the Bactria-Margiana region so we see no reason for the less-obvious translation. Thus, based on this and from comparable evidence from the महाभारत, it is possible that the Indo-Aryans or at least their elite did not adopt all existing Indian dietary choices by default. However, we cannot entirely rule out poetic omission, especially because they were still conservatively following older conventions even in the culinary account.

Of the drinks, mead and beer were likely prepared by the Indo-Europeans on the steppe, especially given that the Vedic beer faithfully uses the old IE grain, barley. The rum was likely acquired from the pre-Aryan Indians. While I translated maireya as wine above, it is unlikely to mean wine in the Indian context. The classical Hindu bhojana specialists like अरुणदत्त state that maireya was prepared by fermenting either the kodrava (millet) grain or खर्जूर (date), whereas कौटिल्य holds that it was made from fermenting of certain plant extracts followed by a complex flavoring process. The flavored soured rice-gruel was a drink that probably proceeded through a primarily acidic fermentation pathway rather than being primarily alcoholic. The origins of this drink and the flavors used with it remain unclear. Finally, we have rendered the sumukhottara following a traditional हरिवंश exponent as being a rhyton. While rhytons are rare in India, the few early examples which have been found show a similar form to those from Mycenaean Greek and Iranian sites. Hence, this interpretation might not be out of place.

These, culinary features might be compared to the glimpse we are offered by the poetess औवैयार् of the feasts sponsored by her patron the great Tamil war-lord अतियमान् नेटुमान् अञ्चि. Therein we see a similar emphasis on meat, and alcoholic drinks, and at least one common flavoring agent, i.e. the scented grass. However, the liquor favored by the अतियमान् is toddy which we do not seem to encounter in the Yadu bacchanal unless we take the maireya to be some such. A fare similar to the हरिवंश is also encountered in a somewhat later द्रमिऴ work, the पोरुनराट्ट्रुप्पडै. There the चोऴा करिकाल offers his proteges a meal rich in meat prepared similar to this account and rice. Finally, this brings us to the ब्राह्मण-s who seem to explicitly avoid the meat and the liquors. Now, someone could argue that the statement regarding the ब्राह्मण-s like Uddhava is a later interpolation by Brahminical authors who wished to make their dietary strictures very clear. In support of this one could cite the use of the term bhoja-मिश्र for the Yadu ब्राह्मण-s as being indicative of the use of later Brahminical title of मिश्र. However, we cannot be sure of such an interpretation because otherwise the rest of this section does not show any notable signs of "tampering".


