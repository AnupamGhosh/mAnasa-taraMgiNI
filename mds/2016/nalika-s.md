
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [नालिक-s](https://manasataramgini.wordpress.com/2016/07/28/nalika-s/){rel="bookmark"} {#नलक-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 28, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/07/28/nalika-s/ "7:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The first week of college had ended. Vidrum was returning from an eatery with his new friend Manjukeshi after an early supper. The two of them were rather surprised by the ferocious competitiveness of their classmates. Some refused to discuss the solutions to a difficult problem in trigonometry, while others refused to talk about synthesis using Grignard's reagents. Just then he saw his old friends from school Sharvamanyu, Somakhya and Lootika. He heaved a sigh of relief at their familiar faces. He thought to himself: "I hope Somakhya and Lootika remain the same and help us out a bit with these difficult problems which seem much stiffer than what we had in school." He told Manjukeshi: "Hey, let me introduce you to my old friends from school. They may also be able to help us with these problems our classmates are refusing to discuss with us." After the introductions he quickly came to the issue: "You guys seem rather unaffected by new stuff in the classroom. I noticed that you cut most of the classes this week! I hope we can ask you some questions."

Sharvamanyu pointed to Somakhya and Lootika and said: "Hey Vidrum, as I have always told you, whatever the quirks of these two dear friends of ours, we must put up with them. Do you think I found all that stuff easy. I am looking calm because I have signed up to get some tutoring from them this weekend in all the subjects." Manjukeshi thought they must be studious students and added: "Trust me we have been spending the evening brushing up on all the books but the new stuff seems really difficult!"\
Lootika: "Easy or difficult depends on the vantage point. We can show you a viewing point -- if that works for you then great!"

Somakhya turned to Vidrum: "You are of course welcome to join us for the lessons the next two days. They will be in my home lab starting 2.00 PM. As for Manjukeshi, you too are welcome to try out a lesson or two to first see if it works for you because as Sharva politely put it, Lootika and I can be bad people."\
Vidrum heaved a sigh of relief that things were settling into a more familiar routine: "So what are you all up to now?"

Lootika: "We were going to have some exercise trekking up the rocky outcrop of गणेशशिला and then Sharva was going to show us some stuff."\
Sharvamanyu: "Would you like to join us?"\
Vidrum and Manjukeshi: "Why not?"\
At the end of their trek they were seated on basaltic eminences under a hidden rocky overhang. Vidrum: "Sharva, what do you have to show?"\
Sharvamanyu: "New आयुध-s ?"\
Vidrum: "New gravity knives?"\
Sharvamanyu: "No, those are for kids. नालिक-s." Saying so he pulled out a couple of हस्तनालिक-s and passed them around saying: "They are not loaded."\
Somakhya examined one closely and asked: "So they are forging these शूलपुरुषीय-नालिक-s now? Is this one like the one you have got for me?"\
Sharvamanyu: "Yes, will deliver it tomorrow. When the मरून्मत्त-s whacked the शूलपुरुष-s they got hold of the stuff and smuggled the design into the देश् for mass production. One of our पक्ष in Uttar Pradesh then got hold of the designs in course of the demolition of a राक्षसालय during the riots and it has become accessible to us."\
Vidrum: "Wow!"\
Manjukeshi was shocked by what she saw and the discussion of her companions. Her face turned pale as she looked at Vidrum with the expression of utter disapproval and fear. Vidrum sensing this pacified her: "Don't worry our friends here are not terrorists. This is just for self-defense for they have strong reasons to suspect that the मरून्मत्त-s could cause mayhem in our regions too as they doing along the Ganges."\
Manjukeshi: "But is this not illegal."\
Sharvamanyu: "When the chips are down and the beards are out to saw your neck off, legal or illegal is not going to matter much."\
Manjukeshi: "But by this attitude of yours you are contributing to a violent society yourself. Why do we have the constitution, the law and police? I bet you did not take civics seriously in school."\
Sharvamanyu: "Sure I did not. But how will all of those, the मामु-s included, help when push will come to shove? You must have seen the news in the शर्मण्य-देश and Phiranga-देश. Did their well-armed मामु-s help in any way? What to say of our weak मामु-s in Uttar Pradesh? It was our counter-rioters who saved the day. In contrast, in trivarga-देश the मरून्मत्त-s were easily put in place. Why, because most people have a good नालिक with them!"\
M: "OK I see your point about Moslems but most Moslems are not terrorists and our Moslems are citizens just like us, not migrants. But then just imagine if everyone takes your advice. Ours is a violent society as it is. People would be settling scores with नालिक-s and the streets would be lined with corpses! You know how it is in the Land of the Free."\
Sharvamanyu: "On what basis do you say our society, excluding the मरून्मत्त-s, are a more violent society than that in the Land of the Free? I would say the Land of the Free is going to be difficult for the मरून्मत्त-s to ultimately conquer because of their love for नालिक-s."

M: "Common on. Everyday when I take the bus or train to college I see angry altercations. Now imagine if everyone had a नालिक like you and Somakhya: These altercations are nasty as they are but will now turn bloody and fatal! Then there is road rage it will devolve from fistfights to gunfights. We don't need the मरून्मत्त-s to do it. It will be like in the Land of the Free!"\
Sharvamanyu: "Have you even studied the violence from नालिक-s in the Land of the Free? Do you think they are shooting each other on trains and buses or on the road?"\
M: "Of course they are! Do you not see the news. The saving grace is they have better police and law than us. Which gives the appearance of keeping things under some control."\
Sharvamanyu: "What you said sounds paradoxical. Just think about it and what you said before."\
M: "What paradox: you just seem to be unaware of the news. Extending the comparison from the Land of the Free we have an even more divided society riddled with caste. It will be the upper castes which gets all the नालिक-s and use it to oppress the Dalits and tribals. So the Moslems are only an excuse boy, it is going to be the socially deprived and Dalits who bear brunt of your trigger-happy types."\
Sharvamanyu: "While I might have not studied my civics, it appears that you have not studied you history. Did you know that till the British disarmed them the deprived groups you mention were well-armed. As of today there is no magical mechanism by which the upper caste is somehow preventing them from being armed. Knowing guys from those जाति-s, I can tell you they are no less armed than me and fear the मरून्मत्त rather than upper castes guys like Somakhya and Lootika. In fact their numbers are much greater than the upper castes; hence, if they wanted they could have killed all of them anytime in the past 3000 years or so for their oppression."\
M: "I must repeat, I wish you guys showed more interest in the constitution, the law and social reform than lionizing violence and whitewashing historical injustices. Lootika, as an educated girl are you sitting and approving this ridiculous stuff of your friends?"\
Vidrum: "Please cool it everyone. Manjukeshi, they have killed nobody and are quiet law-abiding citizens."

Lootika: "Never mind. People say I am good with my hands but for whatever reason it has taken me some effort to shoot accurately. So all I can say for now is that I am not sure my marksmanship will see me through a tough spot. Instead, I keep my body fit so that I can at least flee swiftly if needed."\
M: "Lootika. Think about it; you don't have to do these useless things just to impress your violent friends. You are really giving in to their displays of machismo and fantasy, rather than fighting for gender equality, which is what we should be fighting for first in this day and age rather than the rare Moslem who takes to terror."\
L: "Just to let you know Manjukeshi -- I am a real biologist in that I use the evolutionary theory for my work; hence, equality is not exactly the stuff I am operating off."\
Somakhya: "Alright everyone. It is rather dark now and even with the safety of our नालिक it might not be a great idea to linger on here. Manjukeshi, as true Hindus we have enjoyed the debate and hope you did so too and are not feeling as though you want to use a नालिक on us, in case you knew to use one."


