
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Mysterious fractal polyhedra](https://manasataramgini.wordpress.com/2016/08/20/mysterious-fractal-polyhedra/){rel="bookmark"} {#mysterious-fractal-polyhedra .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 20, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/08/20/mysterious-fractal-polyhedra/ "7:13 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![hedral_s](https://manasataramgini.files.wordpress.com/2016/08/hedral_s.jpg){width="75%"}

