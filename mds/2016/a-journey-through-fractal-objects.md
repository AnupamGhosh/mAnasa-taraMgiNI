
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A journey through fractal objects](https://manasataramgini.wordpress.com/2016/09/12/a-journey-through-fractal-objects/){rel="bookmark"} {#a-journey-through-fractal-objects .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 12, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/09/12/a-journey-through-fractal-objects/ "12:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[](https://player.vimeo.com/video/182331261)

Play

![Vimeo](https://f.vimeocdn.com/p/images/crawler_logo.png){width="75%"}

