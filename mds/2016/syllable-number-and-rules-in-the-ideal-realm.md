
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Syllable, number and rules in the ideal realm](https://manasataramgini.wordpress.com/2016/10/23/syllable-number-and-rules-in-the-ideal-realm/){rel="bookmark"} {#syllable-number-and-rules-in-the-ideal-realm .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 23, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/10/23/syllable-number-and-rules-in-the-ideal-realm/ "6:16 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note is neither meant to be complete exposition of this matter nor a complete view of all what we have realized in this regard. Nor can it be completely understood by those who are not insiders of the tradition.

***Syllable in the primal realm***\
In below verse, from what is perhaps the most famous सूक्त of the ऋग्वेद, the अङ्गिर Dirghatamas Auchathya, the founder of the Gotama clan says:

[ऋचो अक्षरे परमे व्योमन्]{style="color:#0000ff;"}\
[यस्मिन् देवा अधि विश्वे निषेदुः ।]{style="color:#0000ff;"}\
[ yas tan na veda kim ऋचा करिष्यति]{style="color:#0000ff;"}\
[ ya it tad vidus ta ime sam आसते ||]{style="color:#0000ff;"} RV 1.164.39

In the syllable of the ऋच् set in the highest world,\
therein all the deva-s have taken residence,\
he who doesn't know that what can he do with the ऋच्?\
verily only they who know can sit together \[in this ritual session].

He continues:\
[गौरीर् मिमाय सलिलानि तक्षत्य्]{style="color:#0000ff;"}\
[एकपदी द्विपदी सा चतुष्पदी ।]{style="color:#0000ff;"}\
[अष्टापदी नवपदी बभूवुषी]{style="color:#0000ff;"}\
[सहस्राक्षरा परमे व्योमन् ॥]{style="color:#0000ff;"} RV 1.164.41

The female gaur \[Bos gaurus] having measured out fashions the waters,\
she is one footed, two-footed, four-footed,\
becoming eight-footed and nine-footed,\
she is a thousand syllables in the highest realm.

In the अथर्वाङ्गिरस-श्रुति Kutsa आङ्गिरस says:\
[एकचक्रं वर्तत एकनेमि]{style="color:#0000ff;"}\
[सहस्राक्षरं प्र पुरो नि पश्चा ।]{style="color:#0000ff;"}\
[अर्धेन विश्वं भुवनं जजान]{style="color:#0000ff;"}\
[यद् अस्यार्धं क्व तद् बभूव ॥]{style="color:#0000ff;"}AV-vulgate 10.8.7c

One-wheeled and one-rimmed it spins,\
\[with a] thousand syllables north, east, south and west,\
With half it gave rise to the all the universe,\
That which is the other half, what has become of it?

The Atharvaveda has several more mysteries pertain to this primal realm of syllables but we shall only touch upon some here. In the long Rohita recitation we hear:

[एकपदी द्विपदी सा चतुष्पद्य्]{style="color:#0000ff;"}\
[अष्टापदी नवपदी बभूवुषी ।]{style="color:#0000ff;"}\
[सहस्राक्षरा भुवनस्य पङ्क्तिस्]{style="color:#0000ff;"}\
[तस्याः समुद्रा अधि वि क्षरन्ति ॥]{style="color:#0000ff;"} AV-vulgate 13.1.42

She is one footed, two-footed, four-footed,\
becoming eight-footed and nine-footed,\
she of thousand syllables \[emits] the series of worlds;\
from her the oceans flow forth.

The above ऋच्-s have much in them that can be expounded but what we wish to stress here is the repeated allusion to the syllables, "अक्षर-s" which are:

 1.  constituents of the spinning wheel from which the universe originated.

 2.  residents of the "parame vyoman" which can be understood not just as the highest world in some physical sense but also as the highest in terms of "ideals" and also in terms of time as the primordial world from which all has sprung.

 3.  constituents or the very form of highest realm that is the seat or the dwelling of the gods.

 4.  associated with counting or numbers in a sequence.

Indeed, this understanding of the syllables or the primal syllable is the secret behind the Vaidika ritualist receiving the teaching of the 4th foot "[परो रजसे 'सावदों]{style="color:#0000ff;"}" upon having completed his basic initiation into the सावित्री.

Now moving many centuries down in time we come across the rudra-यामल tantra, which teaches:\
[अ-मूला तत् क्रमा ज्ञेया क्षान्ता सृष्टिर् उदाहृता ।]{style="color:#0000ff;"}\
[सर्वेषां चैव मन्त्राणां विद्यानां च यशस्विनि ।]{style="color:#0000ff;"}\
[इयं योनिः समाख्याता सर्वतन्त्रेषु सर्वदा ॥]{style="color:#0000ff;"}\
The process of emission \[of existence], whose root is 'a' \[and] in order known to have 'क्ष' as the end has been expounded. O glorious one, this is declared in all tantra-s as the source of everything, of mantra-s and विद्या-s, the giver of all.

This persistent idea of the syllables being the root of the universe, even in the physical sense, and the very manifestation of the gods of the Veda or the tantra might seem strange to one who has not grasped its secret. The way it might be understood is by the conception of the Sanskrit language by its tradition of grammarians who were intimately linked to the apprehension and transmission of Vedic knowledge. पाणिनि and his brilliant successors like कात्यायन and पतञ्जलि see the starting premise as the Sanskrit syllabary which might be arranged in different ways the: the regular periodic-table like structure or as the माहेश्वर-सूत्राणि. On this their rules operate to emit the whole language. Now we hold that पाणिनि did not invent this out of thin air. He was merely following upon a very old tradition that went back to more than 2000 years before him that conceived this process of rules operating on syllables to generate intricate form as manifest by the Vedic incantations. That was the art of the kavi or the vipra by which he crafted his mantra-s -- it relates to the old Indo-European tradition of Chandas with a fixed syllable-count. Now these kavi-s extended this logic to the universe. There in the "parame vyoman" there exist these "thousand syllables" from which the universe was emitted by a series of rules ("vrata-s") even as the Sanskrit language is emitted from its syllabary. This idea persisted with the तान्त्रिक mantra theory which succeeded the Vaidika prototype.

This vision of generating complex structure from a simple syllabary, reflected in the work of पाणिनि, was increasingly important in the evolution of Sanskrit, where rules allowed for unambiguous (or purposely ambiguous) sense while still allowing for enormous structural complexity. This came at what might be called a "marker cost" i.e. marking the elements precisely: nominals precisely inflect -- right in the first सूक्त of the RV we see in succession inflections of the name of the great god (agnim, अग्निः, अग्निना, agne) as though to announce the importance of unambiguous markers of sense. Likewise, the verb assumes an enormous range of forms to convey very precise temporal and modal textures. For this very reason, the traditional grammarians split hairs and go into great depths with समास-vigraha or dissection of compounds, precisely because the markers are mostly lost in समास. For instance, you are left with the puzzle of whether अश्वशिरस् is the horse's head or he who has a head like a horse or whether a lokanatha is the lord of the world or one who is lorded over by the whole world. While it makes Sanskrit a difficult language to master and might generate undue pride upon its operational mastery (caricatured by the advaitin-s as the ineffectuality of पाणिनि's डुकृञ्), it has had a profound effect on Hindu thought, the modern implications of which have not entirely been fathomed. Curiously, one facet of it which is central to our current discursion is the fact that in Hindu tradition the number is often subservient to the syllable. There are many ways in which numbers might be linguistically represented: the syllabic contraption of the great आर्यभट or the #कतपयादि system or the system of using markers like: 1= candramas/Indra; 2=पक्षौ; 3=वःनयः; 4=वेदाः; 5=बाणाः; 6=ऋतवः/स्कन्दः; 7=मुन्यः/अब्धयः; 8=नागाः/वसवः; 9=ग्रहाः; 10=dik.

***"The extraordinary effectiveness of mathematics"***\
In our times we see a great thinker of the age Roger Penrose talk of three worlds: 1) The "real world" made of particles and energy including forms of matter-energy we do not yet understand. 2) The world of consciousness or first person experience, what in the western philosophical language would be called qualia. 3) The "Platonic world" or ideal mathematical entities both geometric shapes and numbers and the relationships which they contain (what I think mathematicians call theorems). Penrose points out that a great mystery is the fact these objects of the 3rd or the "Platonic" world are the ones that are used to govern the making and the working of the real world. Penrose believes that ultimately there is only one world but it appears three-fold because we do not understand its mysterious unity. That apart, the mathematical world of Penrose is thus comparable to the world of अक्षर-s and the vrata-s which operate on them like पाणिनि's rules in our tradition, which is placed at the root of the world.

Now the existence of such a pure mathematical world is rather easily perceived by anyone who has played with some mathematics, irrespective of whether he really understands its rigorous formulations or not. Its actual manifestation is what Eugene Wigner called the "extraordinary effectiveness of mathematics" or more recently extensively discussed by astronomer Mario Livio as "Is god (in singular emphasizing the certainty of Abrahamistic credo) a mathematician?" (In a sense following a similar Greek statement by Karl Gauss). One can fill a whole volume and more with examples of this mystery of mathematics and we have narrated some of our personal journeys through such on these pages; yet, we shall offer a few here for it is always worth savoring:

 1.  A trivial case is the Hindu predilection for huge numbers. At the time the Hindus named them people had little reason to count that much. But today we know the various measures at the extremities of the universe fall in the orders magnitude of many of those big numbers of our ancestors. Thus, long after the fact of their discovery in the "mathematical world" we see them as very real entities in the real world. For instance, Avogadro's number would be approximately षष्ठि-वृन्दानि.


 2.  This one is more personal: may be a couple of years after we had learned to construct a hyperbola we learned that this curve is describes how enzyme reaction rate changes with respect to substrate concentration. It was the first time we had the personal experience of the mystery of how a curve discovered in a purely mathematical realm in relation to the Delian problem by the yavana-s appeared in the real world of biochemistry.


 3.  Another personal example: around the 14th year of our life we constructed a curve known as the witch (of Agnesi), which is rather trivial:![witch](https://manasataramgini.files.wordpress.com/2016/10/witch2.png){.alignnone .size-full .wp-image-8921 attachment-id="8921" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="witch" large-file="https://manasataramgini.files.wordpress.com/2016/10/witch2.png" medium-file="https://manasataramgini.files.wordpress.com/2016/10/witch2.png" orig-file="https://manasataramgini.files.wordpress.com/2016/10/witch2.png" orig-size="998,307" permalink="https://manasataramgini.wordpress.com/2016/10/23/syllable-number-and-rules-in-the-ideal-realm/witch/" sizes="(max-width: 998px) 100vw, 998px" srcset="https://manasataramgini.files.wordpress.com/2016/10/witch2.png 998w, https://manasataramgini.files.wordpress.com/2016/10/witch2.png 150w, https://manasataramgini.files.wordpress.com/2016/10/witch2.png 300w, https://manasataramgini.files.wordpress.com/2016/10/witch2.png 768w"}

  -  Draw the generating circle which is tangent to the x-axis at origin O and radius  $a$ .

  -  Draw the diameter along the y-axis  $\overline{OA}$  and a tangent to it at point A.

  -  Let B be a point moving on the generating circle. Draw  $\overleftrightarrow{OB}$  which cuts the above tangent at point C.

  -  Draw a line perpendicular  $\overleftrightarrow{AC}$  at point C and a line parallel to  $\overleftrightarrow{AC}$  through point B. These two lines intersect at point D.

  -  The locus of D as a B moves on the generating circle is the witch which has the equation:  $y=\dfrac{8a^3}{4a^2+x^2}$ .

Looking at the witch in those days of our youth we thought that it looked like a good function for a statistical distribution and wondered if anything in nature might be thus distributed. Only several years later we learned that the great mathematician Augustin-Louis Cauchy or the statistician Poisson had discovered a probability distribution of this shape which has the interesting feature of being "fat-tailed". Closer to our times it emerged mysteriously in the real world of physics as the shape of broadening of certain spectral lines with molecular collisions. When Pierre de Fermat discovered this curve in the 1600s in the purely mathematical world he certainly could not have had an inkling that it would appear in the real world of physics centuries later, just like the conics of the yavana-s. Thus, over and over again this objects discovered in that "Platonic" realm were seen to govern the working of the real world.

This triumph of mathematics in the structure of the laws of physics gives them the feel of being true because mathematical truths are quite unlike scientific models -- while much of the science of ancients has been quite superseded by new science their mathematics still stands firms and it was their mathematical teachings that allowed all the new science to emerge. The planetary models of the ancients might have died but their भूजा-कोटि-कर्ण-न्याय remains as true today as when, to their wonder, they first discovered it. This mathematical foundation perhaps gives a sense of inviolability to the laws or in the least it gives them the sense of being enforced (see more on that below). Indeed, the Hindus of yore did record this sense of inviolability in the laws in connection of the gods seated in that same "parame vyoman". For instance the अर्चनानस् आत्रेय states:

[ऋतस्य गोपाव् अधि तिष्ठथो रथं सत्यधर्माणा परमे व्योमनि ।]{style="color:#0000ff;"}\
The two upholders (मित्रा-वरुणा) of the natural law (ऋत), you two of inviolable laws (dharman) stand in your chariot in the primal realm.

***An ideal realm with a syllabary?***\
As noted above this mysterious intrusion of mathematics from its ideal realm into the real world is rather palpable even for someone with relatively limited knowledge but can the same be said for world of syllables postulated by the Hindus? At the face of it, to most it is less apparent -- indeed, to some it even seems to bring up images of the well-known mleccha indological trope of "Hindus as idiots".

When we look at physics we find the mathematics directly present in the structure of its laws -- it is very apparent, even if entirely mysterious. Now, when we look at biology we find that same geometry acting as an enforcer that channels all variety by eliminating what falls outside the allowed geometries. This is what is termed natural selection. This is the foundation of the commonly observed phenomenon of convergent evolution -- over and over again synapsid carnivores evolved the same saber-toothed morphologies, archosaur carnivores evolved generally similar skulls with serrated teeth (ziphodont morphology), tetrapods returning to the water assumed fish-like morphs like those of their ancestors. One part of this channeling is from the geometric structure resident in the underlying physics -- the shapes that are best suited to fly in the sky or swim within the waters. But a part of it stems from deeper phenomenon that becomes apparent only when we descend down to the molecular level. At the microscopic level the three dimensional geometries of the molecules of life are ultimately encoded in one dimensional strings. The information in these strings is read in many ways, the operationally most important aspect of which is the reading of genetic code. That world is where the encodings occurs as letters quite literally अक्षर-s and these can specify many different types of words and phrases by a slew of different rules that run the system from which emerges the 3D geometry. These "linguistic" entities are not just the genetic code but also seen in architectures of domains of proteins. Our studies over decades have shown that these have distinct syntaxes, with these syntaxes being the enforcers of what is seen as natural selection. Bad grammar is relentlessly purged like king Bhoja dismissing the bad grammar of his rivals. At the same time "creativity" of domain architectures within the allowed grammar, including the use of rare domains that conform to rules like a Sanskrit versifier's use of unusual words, is rampant. Different cellular molecular ecologies have allowed for different styles -- great creativity in the bacteria, robust simplicity in the archaea, and unexpected reuse of phrases and words in new contexts like a prolix hack in eukarya. Thus, the biological world offers us a glimpse of an ideal realm which contains as the ideals a set of अक्षर-s and rules which operate on them -- it is indeed quite a menagerie of rules, like the अष्टाध्यायी.

Are these "अक्षर-s" in addition to or in place of the mathematics present in the ideal realm? We have an intuition that it is more fundamental than the mathematics and the mathematics emerges as a limb of it. However, being of limited intelligence and knowledge, we by ourselves currently have no way of establishing this to be the case. However, it appears to us that the view of one of the eminent scientist-mathematicians of our age Stephen Wolfram bears a relationship to such an idea. While it is not clear if he postulates a ideal realm with computer programs, he does propose the existence of simple computational mechanisms comprised of simple rules acting on a limited set of characters as a powerful alternative to purely mathematical mechanisms in generating enormous complexity.

***Measuring the real realm with numbers***\
While we have spoken above of the privileging of the syllable over the mathematical abstraction of number and geometry in the primal realm in Hindu thought, this should not be construed as an absence of importance for these. Indeed, the main constituents of ordinary modern mathematics can be seen as numbers and geometry, which respectively stand on that bed-rock of mathematical tradition provided by the two great branches of the Indo-European world, the आर्य-s and yavana-s. Any discerning student realizes that the ordinary secular mathematics of today is a successor of an ancient tradition of the Indo-European world, which emerged in the context of ritual constructions going back to at least the ancestral Greco-Aryan period. Such a role for mathematics in the religious process might be seen in many other civilizations -- with close parallels in Egypt or much later in time in the temple geometry of Nippon. Ultimately all of this might have a direct connection going back to our shared ancestry with other apes like the chimpanzee. The piling of bricks to construct the altar (literally citi or piled in Aryan parlance) may be compared to recently reported ritual of piling of stones in tree hollows by the troglodyte -- a operation with allows for counting in the least.

Geometry of the ritual altars was a key feature that can be traced back to the ancestral Greco-Aryan tradition. The cubical altar, which is at the center of the famous yavana Delian problem, can be seen as the cognate of the near cubical शामित्र altar of the animal sacrifice of the आर्य-s. The Delian problem itself, which involves doubling of the cubical altar, might be compared to its planar equivalent found in the Yajurvaidika tradition of the eka-शतविधा ritual where a square altar of Agni is increased from a single unit to 101 units through successive increments without ever changing the square shape. It was this ancestral tradition that divergently evolved to give rise to the Pythagorean and then Platonic world view among the yavana-s which privileged Euclidean geometry and the discovery of conics and other plane curves. In the midst of the आर्य-s it evolved in a more numerical and algebraic form which privileged measurement as the primary feature. This feature emerges early in the tradition of our ancestors: one of the primeval preserved memories of our clan is that of our hoary ancestor च्यवान भार्गव measuring out the ritual altar with a depth-measuring device: "[च्यवानः सूदैर् अमिमीत वेदिम् ।]{style="color:#0000ff;"}". Right in the ऋग्वेद we hear the great astronomer ऋषि Atri Bhauma, who saved our ancestor ऋचीक Aurva, say this as he praises the great Asura:

[इमाम् ऊ ष्व् आसुरस्य श्रुतस्य]{style="color:#0000ff;"}\
[महीम् मायां वरुणस्य प्र वोचम् ।]{style="color:#0000ff;"}\
[मानेनेव तस्थिवां अन्तरिक्षे]{style="color:#0000ff;"}\
[वि यो ममे पृथिवीं सूर्येण ॥]{style="color:#0000ff;"} (RV 5.085.05)

I proclaim this great माया\
of the famed Asura वरुण,\
who, standing in the atmosphere as if with a ruler,\
measured out the earth with the sun.

Thus, the physical action of that great Asura वरुण who upholds the natural laws (ऋत) is carried out by measurement.

The अङ्गिर sage भरद्वाज बार्हस्पत्य elaborates on this theme in that सूक्त which can only be apprehended by those who see the rahasya-s of the heavenly Agni वैश्वानर and not one who mistakes the ritual fire in front of him to be just a fire.

[वैश्वानरस्य विमितानि चक्षसा]{style="color:#0000ff;"}\
[सानूनि दिवो अमृतस्य केतुना ।]{style="color:#0000ff;"}\
[तस्येद् उ विश्वा भुवनाधि मूर्धनि]{style="color:#0000ff;"}\
[वया इव रुरुहुः सप्त विस्रुहः ॥]{style="color:#0000ff;"} RV 6.7.6

By the eye of वैश्वानर heaven's heights have been measured out,\
by the ray of the immortal one.\
Indeed on his head are all the worlds;\
like branches his seven tongues have grown.

[वि यो रजांस्य् अमिमीत सुक्रतुर्]{style="color:#0000ff;"}\
[वैश्वानरो वि दिवो रोचना कविः ।]{style="color:#0000ff;"}\
[परि यो विश्वा भुवनानि पप्रथे]{style="color:#0000ff;"}\
['दब्धो गोपा अमृतस्य रक्षिता ॥]{style="color:#0000ff;"} RV 6.7.7

The skillful one, who measured out the atmospheric realms,\
is the sage वैश्वानर who \[measured] out the starry heaven\
who spread around all the worlds\
the irresistible guardian, the protector of immortality.

Notably, that Gauri who was described in the RV and AV (see above) as the embodiment of the syllables in the primal world also needs to be measured out and this can be done using a god as the measuring rod. This is described Kurusuti काण्व:

[वाचम् अष्टापदीम् अहं]{style="color:#0000ff;"}\
[नवस्रक्तिम् ऋतस्पृशम् ।]{style="color:#0000ff;"}\
[इन्द्रात् परि तन्वम् ममे ॥]{style="color:#0000ff;"} RV 8.76.12

The syllabary, with eight-feet and nine vertices\
is embedded in the natural law, (literally in contact with the natural law)\
I have measured out its body by means of Indra.

The same Gauri is also embodied in ritual as a cow who represents the measure of heaven. In तैत्तिरीय संहिता 7.1 we have:

[सह्áस्र-सम्मितः सुवर्ग्ó लोक्áः।...]{style="color:#99cc00;"}

The celestial world is measured in thousands...

While the holy cow stands in front of him the ritualist offers with this incantation:

[उभा́ जिग्यथुर् न्á प्áरा जयेथे]{style="color:#0000ff;"}\
[न्á प्áरा जिग्ये कतर्áश् चन्áइनोः ।]{style="color:#0000ff;"}\
[íन्द्रश् च विष्णो य्áद् áपस्पृधेथां]{style="color:#0000ff;"}\
[त्रेधा́ सह्áस्रं व्í त्áद् ऐरयेथाम् íति ॥]{style="color:#0000ff;"}

You two have conquered, you two are not conquered;\
Neither of the two of them have been defeated;\
Indra and विष्णु when you two contested,\
you had divided the thousand into three. Thus, \[he recites].

[त्रेधा-विभक्त्áं व्áइ त्रिरात्र्é सह्áस्रम् । साहस्रीम् एव्áइनां करोति । सह्áस्रस्यैव्áइनाम् मात्रां करोति ।]{style="color:#0000ff;"}

Thousand is indeed divided into three parts at the three-night \[ritual]; he makes her \[the cow] a \[symbol] of thousand. He thus makes her the measure of a thousand.

[रूपाणि जुहोति । रूप्áइर् एव्áइनां स्áम् अर्धयति । त्áस्या उपोत्थाय क्áर्णम् आ́ जपेत् ।]{style="color:#0000ff;"}

He makes oblations to her forms. He thus furnishes her with her forms. Rising up he mutters in her ear:

[íडे र्áन्त्é 'दिते स्áरस्वति प्र्íये प्र्éयसि म्áहि व्íश्रुत्य् एता́नि ते अघ्निये नामानि ।]{style="color:#0000ff;"}

"इडे, Ranti, Aditi, Sarasvati, प्रिया, Preyasi, Mahi, विश्रुति", these, O unassailable one, are your names (Thus, the ritual cow is identified with the goddesses among others embodying the syllabary of the primal word).

[सुकृतम् मा देव्éषु ब्रूताद् íति । देव्éभ्य एव्áइनम् आ́ वेदयति । áन्व् एनं देवा́ बुध्यन्ते ॥]{style="color:#0000ff;"}

Declare me as a doer of good among the gods. She indeed lets \[this] to be known to the gods. The gods take note of this.

The world of the heavens is much vaster than this one hence it is symbolic represented as measuring in a bigger unit, the thousand. For the यजमान to let the gods know that he is a doer of good, by which he can conquer that celestial realm, he needs a measuring unit of the thousand. For that he makes the ritual cow a representative of the goddesses who embody that thousand. Thus, for the आर्य in ritual other than geometry the number played an important role. Indeed, he encompassed the base number of all gods of the श्रुति numbering 33 (12 आदित्य-s, 11 Rudra-s, 8 Vasu-s and 2 अश्विन्स्) by measuring out the altar in the form of successive squares formed of square bricks from 1 to 289. This embodies the relationship:

 $$\displaystyle \sum_{n=0}^{16} (2n+1) = (n+1)^2= 289$$ 

Thus the above sequence goes from  $1,3,5..33$  (spelled out the camaka प्रश्न of the Yajurveda) while the sum of sequence goes from  $1^2..17^2$ . Thus, the first square is one brick. Adding 3 bricks the next in the sequence on its 3 sides give you  $2^2$ . Adding 5 bricks to sides of this new square gives you  $3^2$  so on till you get the square of 17. Thus, geometry for the आर्य was closely linked with number, and measurement with a cord and ruler. It was the philosophical consequence of this that marked a subtle point of departure of the Hindus from the Platonic realm of the yavana-s: one hears of Plato disapproving of the measured constructions -- feature that dominated yavana geometry thereafter. Thus, while geometry and other Pythagorean mathematical traditions moved into the realm of the ideals as absolute-measure-free entities among the yavana-s, they were firmly as part of real world in आर्य-s emulating the measurements of the universe performed by the great gods. Based on what the काण्व says even the primal syllabary has to be measured out for the construction of the real world.

***A different way of looking at numbers***\
We first encountered a new class of systems, the iterative systems, with the 3/2 floor map. This we originally explored, to our great wonder, in the old way with a hand-held calculator and graph paper. The 3/2 floor map works thus:

 $$x_{n+1}=\dfrac{3}{2}x_n-\left \lfloor \dfrac{3}{2}x_n \right \rfloor$$ 

where the starting value  $0< x_0< 1$ 

![3by2map](https://manasataramgini.files.wordpress.com/2016/10/3by2map.png){width="75%"}

*The 3/2 floor map for two close starting values for 300 iterations*

The remarkable feature about it is how we get chaotic behavior from this very simple system. We can see spiking and bursts of spikes as has been observed with the membrane potential of neurons without any differential equations but just a simple iterative system. We realized that the while the behavior is chaotic it is not entirely random in a statistical sense. For different starting  $x_0$  values we get a generally similar frequency distribution of the values visited by  $x_n$  over  $n$  iterations though there might be very subtle differences for different starting values (shown in the figure for 50,000 iterations for six different starting values). Over all the values closest to 0 are 3 times more frequent than those closest to 1. Moreover, there are two clear steps in the distribution at 0.5 and 0.75 which are line with how the 3/2 floor map operates.

![3by2](https://manasataramgini.files.wordpress.com/2016/10/3by2.png){width="75%"}

*The distribution of values of  $x_n$  visited by the 3/2 floor map for different starting values*

Strikingly, even though the bulk distribution is generally similar for all starting values the actual behavior of  $x_n$  is very sensitive to the starting value. For example in the figure we illustrate the evolution of 0.5 versus 0.50001 over 300 iterations. One can notice that beyond the 20th iteration the two sequences completely diverge that they have nothing in common despite similar bulk statistics.

This opened our eyes to the fact that numbers can be looked at in way different from how they behave in the system of classical calculus. In the method of calculus, the key aspect is that numbers close to each other behave similarly so that we can smoothly transition from one to another in a continuous sequence of ever close numbers. In iterative systems even a small change in the number can result in a very different end result -- something which goes against the classic operation of calculus. Moreover, whether such behavior is produced or not depends on nature of iteration rule or the "map".

The importance of another such kind of behavior was further driven home to us by the floor-difference sequence. This sequence is generated thus:

 $$x_n=\left \lfloor (n+1)h \right \rfloor -\left \lfloor nh \right \rfloor$$ 

where  $n=1,2,3...\infty$  and  $h$  is a constant fractional number like say  $\pi$  or  $\sqrt{2}$ .

![floor_difference_spectrum](https://manasataramgini.files.wordpress.com/2016/10/floor_difference_spectrum.png){width="75%"}

*The floor-difference sequence as step graph for various numbers*

The floor-difference sequence which results from the above rule can be represented as shown in the figure -- as a step graph -- a sequence of up and down values. This set of up and down values can hence be also represented as a binary string. When  $h$  is a rational fraction like 3/2 then we get a regularly repetitive graph which translates into a regular binary string like  $10101...$ . Now if  $h$  is instead an irrational number then we do not get a repetitive graph. Instead, there are more complex patterns which have their own structure. For example, Stephen Wolfram, the pioneer in the study of such systems, has shown that if  $h$  is a root of a quadratic equation then the pattern of the sequence is obtained by an automaton with simple substitution rules. Thus, he has shown the following substitution rules:

 $$\sqrt{2}:\; 0 \rightarrow 0,1; \; 1 \rightarrow 0,1,0\\ \sqrt{3}:\; 0 \rightarrow 1,1,0; \; 1 \rightarrow 1,1,0,1\\ \phi: 0 \rightarrow 1; \; 1 \rightarrow 1,0$$ 

These can be easily verified with the figure.

However, for other irrational numbers the rulers are more complex or subtle. The case of  $h= e$  or  $h= \pi$  is rather notable. For  $e$  we see that the step graph looks quasi-regular: it can represented as repeats of the binary string  $1101110$ . This goes fine till we reach position 32 at which place a we get an unexpected string  $11101110$ . Then it resumes again as usual with the repeat of the earlier string till position 71 where we again get  $11101110$ . From this we see that first deviation occurs at 32 and second at 39 from the point of the first deviation. From then on this pattern repeats with successive insertions of the string  $11101110$  occurring alternately at 32 and 39 positions from the previous one. Thus, we can predict and see the string  $11101110$  at: 32, 71, 103, 142, 174, 213, 245, 284, 316, 355, 387, 426, 458, 497=458+39. But at that point instead of seeing the next occurrence of the string at 529 (497+32) we instead see it at 536 (497+39). Thereafter, from 536 the sequence of the points of insertion of the string are as usual in the 32, 39 alternate pattern: 536, 568, 607, 639, 678, 710, 749, 781, 820, 852, 891, 923, 962, 994, 1033=994+39. But then instead of getting 1065 (1033+32) next we get 1072=1033+39. Thereafter, from 1072 the sequence of appearance of the string continues as usual with the alternating 32, 39 as: 1072, 1104, 1143, 1175 ...

To understand the secret behind this interesting pattern we have to turn to the continued fraction representation of  $e$ , which was developed by the great Leonhard Euler. By working out the fractions to successive terms we get increasingly better approximations of  $e$ , which are listed below starting from the 0th :

 $$(0) \; 2 \; (1) \; 3\; (2) \;\dfrac{8}{3}\; (3) \;\dfrac{11}{4} (4) \;\dfrac{19}{7}\; (5) \; \dfrac{87}{32}\; (6) \;\dfrac{106}{39}\; (7) \;\dfrac{193}{71}\lbrack 10pt\rbrack (8) \;\dfrac{1264}{465}\; (9) \;\dfrac{1457}{536}\; (10) \;\dfrac{2721}{1001}\; (11)\; \;\dfrac{23225}{8544}$$ 

Now the floor-difference sequence for the first fractional approximation  $\dfrac{8}{3}$  produces a repeat of the string  $110$ ; the next approximation  $\dfrac{11}{4}$  yields the repeats of string  $1110$ ; the next approximation  $\dfrac{19}{7}$  yields the string  $1101110$ . Thus, the first three approximations set up the basic pattern which forms the floor-difference sequence of  $e$ . We then note that the appearance of the deviant pattern  $11101110$ , corresponding to positions alternately separated by 32 and 39 (71=32+39), is related to the denominators of the next three approximations. Strikingly, the further points where we observed break of the preexisting pattern occur at or upon crossing of 536 and 1001 which define the denominators of the 9th and 10th cycles of approximation.

Now when we look at the step graph for the floor-difference sequence of  $\pi$  we see that it shows a strikingly regular pattern which can represented as repeats of the string  $0000001$ . We see 15 such repeats till we reach the position 106 where we see a deviant string  $00000001$ . Then it resumes with the usual repeat until position 219 where we again see the deviant string as above. This deviant string then reappears regularly at 332, 445, 558, 671, 784, 897, 1010, 1123... i.e. every 113th position. Now the secret of this pattern becomes clear when we look at the continued fraction derived approximations of  $\pi$  starting from the 0th cycle:

 $$(0) \; 3 \; (1) \;\dfrac{22}{7}\;(2) \;\dfrac{333}{106}\;(3) \; \dfrac{355}{113}\lbrack 10pt\rbrack (4) \; \dfrac{103993}{33102}\; (5) \; \dfrac{104348}{33215}$$ 

The 0th approximation 3 is the ancient one which is already alluded to as \*approximation* of  $\pi$  even the RV. The 1st approximation  $\dfrac{22}{7}$  is what Archimedes arrived at. Its floor-difference step graph corresponds to a sequence starting with the string  $000001$  followed by never-ending repeats of  $0000001$ . This later string sets up the basic pattern of the  $\pi$  sequence. Then we see that as with  $e$  the denominator of the 2nd approximation yields the point where the first deviant string appears (106). Then the next copy of that deviant string appears 113 positions away from that point, 113 being the denominator of the next approximation. The next irregularity is would appear in the vicinity of the denominator of the next approximation, which is way beyond the number of iterations to which we have calculated the sequence. That explains why  $\dfrac{355}{113}$  serves as great approximation for  $\pi$  and was so remarkably captured by Srinivasa Ramanujan in his first construction to approximately square the circle.

Thus, here we see that the application of a procedure (the floor-difference operator) converts an irrational number with uniformly distributed set of digits after the decimal point into an orderly structure -- a structure which can be generated by relatively simple rules similar to those of पाणिनि operating on a syllabic string.

As the final example in this section we shall consider the remarkable curlicue curves which were first discovered and described by the mathematician Mendes France. Different types of these curves may be generated using two related algorithms and in addition to a deep aesthetic value have rich implications for various natural systems including optics and the folding of polymers. However, for this discussion we shall restrict ourselves to first of these algorithms which generates these curves thus:

 1.  Let the seed  $s$  be a fractional or irrational number like  $\pi, \sqrt(2), \phi$ .

 2.  Start with an initial point  $x_0$  say  $(0,0)$ .

 3.  Then for n iterations do the following:

 $$dt_{n+1}=dt_n+2\pi \times s\\ t_{n+1}=t_n+dt_{n+1}\\ x_{n+1}=x_n+\cos(t_{n+1})\\ y_{n+1}=y_n+\sin(t_{n+1})$$ 

 4.  Join successive points  $(x_n,y_n)$  to get the desired curlicue. The figure shows curlicues generated using various seeds run for 2000 iterations. Top row from left: 1)  $\pi$ ; 2)  $\sqrt{10}$ ; 3)  $\dfrac{355}{113}$ ; 4)  $\phi$  Bottom row from left: 5)  $e$ ; 6)  $\log(2)$ ; 7)  $\pi \times \phi$ .

![curlicue](https://manasataramgini.files.wordpress.com/2016/10/curlicue.png){width="75%"}

*Curlicue curves with various numbers*

First, one can see several aesthetically pleasing interesting patterns:  $\phi$  curve produces fir-tree like patterns. The  $\log(2)$  curve produces several 'S' shaped patterns. The  $\pi \times \phi$  produces a basic pattern of highly folded "beads" connected by unfolded linkers. Together they are organized into the path of multiple spiraling curves that resemble Johann Bernoulli's clothoid which emerges in optics (where it is called a Cornu spirals). When a beam from a distant source of light encounters a slit of size comparable to its wavelength we get a typical diffraction pattern with a central peak of high intensity followed by increasingly damped peaks of much lower intensity on its flanks. Now, if the light source is close to the slit then the analysis of the diffraction patterns becomes very complicated. It is in such a case, where a nearby linear light source produces a cylindrical wave front, that this spiral curve can be used to predict the diffraction pattern.

Second, nearness of numbers does not necessarily translate into the similar pattern. We know that  $\sqrt{10}$  was used often in early Hindu mathematics as an approximation for  $\pi$  correct to one decimal place. However, this number produces a wandering curve that is not at all similar to that of  $\pi$ . Then we see the famous approximation used by Ramanujan  $\dfrac{355}{113}$  correct to six decimal places. It being a rational number does not produce a fractal pattern but a linear sequence of highly folded "beads" connected by a linker element. This resembles the unfolded nucleosome-DNA complex in archaea and eukaryotes. The real  $\pi$  in contrast has a highly folded bead similar to the former but the linker gradually curls making curve take an overall spiral form.

In conclusion, here we see numbers playing a different role: different from their role as the backdrop of the smooth continuity of curves emerging as solutions to differential equations but as abstractions similar to syllabic strings, which can be acted upon by simple rules to generate complexity. Here, the actual strings of the digits matter. Seeming uniformly distributed sequences of digits can produce complex patterns that are far from random when sent through the appropriate rules. It would almost seem as though they hide something within them which is not at all apparent until they are made to reveal them by procedures like the floor-difference operation or the curlicue algorithm. This complexity can capture the forms and behaviors we see in nature and produce grammar-like features -- some of which also emerges from more complex mathematical descriptions of natural systems.

***The possibility of a unified vision***\
Finally, we intuit the possibility of a unified vision -- by no means a complete one or rigorously hammered out. First, we do see the presence of the an ideal realm, one might call it "Platonic". In our view this is how the "parame vyoman" and "शुद्ध-भुवनाध्वन्-s" of the tantra-s should be understood. We also hold that the correct way of understanding the मीमांसक nityatvam of the word is in this sense rather than the physical श्रुति composed by our आर्य ancestors starting from the early Indo-European period. In this realm reside the numbers or syllables, which are comparable entities in that they are operands of a collection of operations or rules, which in themselves might be simple. However, they are capable of generating great all the complexity we see in the world and mathematics. When this ideal world "shapes" the world of matter-energy we see its expression as a different type of number, the number of measurement. It is this number that appears directly in the sciences. The attempt to apprehend the structure residing in these numbers leads to the discovery of mathematics.

In a biographical sense all this unfolded gradually over time -- a story which we might narrate at some other point with different illustrations and more detailed acknowledgments of our influences. The first phase of our life was marked by our experiments in Euclidean geometry and plane curves which culminated around our 14th year in the penetration of calculus. It was the high-point of our personal mathematical attainment in that direction which allowed us to advance into the quantum theory and relativity in physics to the extent we could. As we were fighting our way through Dirac's book on bra and ket vectors in the summer of our 14th year, on one hand we felt great pleasure with what we had managed to understand but on the other the insurmountable mathematical complexities that underpinned an even deeper conquest of physics put us in place. But our experience was one which nevertheless resounded with the triumph of "ordinary" mathematics -- a culmination of the mathematical traditions of the आर्य-s and yavana-s in one sense. The existence of the ideal realm came into our focus then and it was a very mathematical one filled with curves. It was around this time we came across an article on the work of Benoit Mandelbrot in a science magazine. This immediately opened the doors to a new world, marking the second phase of our life. However, not having easy or continuous access to a computer this exploration began slowly. But it advanced over the subsequent years with the completion of our transition from the hand-held calculator to the computer and the ideas of Turing and Wolfram become a प्रत्यक्ष. By then we saw a new picture, one converging what has been articulated here.

