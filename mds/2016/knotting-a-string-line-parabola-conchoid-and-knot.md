
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Knotting a string: line, parabola, conchoid and knot](https://manasataramgini.wordpress.com/2016/10/01/knotting-a-string-line-parabola-conchoid-and-knot/){rel="bookmark"} {#knotting-a-string-line-parabola-conchoid-and-knot .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 1, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/10/01/knotting-a-string-line-parabola-conchoid-and-knot/ "11:18 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

**The basic construction**\
In course of studying various methods of constructing conics we stumbled upon a means of using the relationship between uniform circular motion (UCM) and simple harmonic motion (SHM) to construct four distinct loci with common procedure. They seem to describe the progression in the process of knotting a string --- in a sense the "Platonic" ideal of the making of a knot. The basic construction goes thus:

![parabola_knot_conchoid_line](https://manasataramgini.files.wordpress.com/2016/10/parabola_knot_conchoid_line.png){width="75%"}


 1.  Draw a circle with origin O as center and radius  $r$ . This is our directing circle.

 2.  Let A be a point on the circle and  $\overline{BC}$  its diameter along the x-axis.

 3.  Drop perpendicular from A onto  $\overline{BC}$  so that it cuts it at point D.

 4.  As point A performs UCM on the directing circle the point D performs SHM on the diameter  $\overline{BC}$ . We can locate point D on  $\overline{BC}$  with respect the midpoint and and extremity of its oscillation using two segments, namely  $\overline{OD}$  and  $\overline{DC}$ .

 5.  We thus get two reciprocal ratios:  $\dfrac{OD}{DC}$  and  $\dfrac{DC}{OD}$ .

 6.  We draw the radial line which passes through points O and A.

 7.  Along this radial line, we locate two points symmetrically about point A, namely points E and F such that:  $\dfrac{AF}{r}=\dfrac{AE}{r}=\dfrac{OD}{DC}$ .

 8.  The loci of points E and F as A performs UCM on the directing circle are a parabola and a knot curve.

 9.  Likewise, we locate two points symmetrically about point A, namely points G and H such that:

 $$\dfrac{AH}{r}=\dfrac{AG}{r}=\dfrac{DC}{OD}$$ 

 10.  The loci of points G and H as A performs UCM on the directing circle are a straight line perpendicular to the x-axis at r and a conchoid of Nicomedes.

When we derive their equations formally we get a pair of each of the four loci, as will be illustrated below.

**Formal derivation of the equation of the parabolas**\
![parabola_knot_conchoid_line_parabolas](https://manasataramgini.files.wordpress.com/2016/10/parabola_knot_conchoid_line_parabolas.png){width="75%"}

Let us first consider the case in the above construction as typified point F:

 1.  The coordinates of point F are  $(x,y)$ , which are the coordinates of the locus whose equation we are seeking to derive.

 2.  The coordinates of point A on the directing circle are  $(x_1=r\cos(t), y_1=r\sin(t))$ .

 3.  From the construction we obtain the following relationships:

 $$OD=x_1=r\cos(t)\\ DC=r-OD=r-r\cos(t)\\ \dfrac{AF}{r}=\dfrac{OD}{DC}\lbrack 15pt\rbrack \therefore AF=\dfrac{OD}{DC}r=r\dfrac{r\cos(t)}{r-r\cos(t)}=\dfrac{r\cos(t)}{1-\cos(t)}\lbrack 15pt\rbrack x^2+y^2=(r+AF)^2=\left(r+\dfrac{r\cos(t)}{1-\cos(t)}\right)^2=\dfrac{r^2}{(1-\cos(t))^2}\lbrack 15pt\rbrack \dfrac{y}{x}=\tan(t)=\dfrac{\sqrt{1-\cos^2(t)}}{\cos(t)}\lbrack 15pt\rbrack \therefore \dfrac{y^2}{x^2}=\dfrac{1-\cos^2(t)}{\cos(t)}\lbrack 15pt\rbrack \therefore \cos^2(t)= \dfrac{x^2}{x^2+y^2}\lbrack 15pt\rbrack \therefore \cos(t)= \pm \dfrac{x}{\sqrt{x^2+y^2}}$$ 

With these relationships in place we can eliminate  $\cos(t)$  to get:

 $$x^2+y^2=\dfrac{r^2}{\left( 1\pm \dfrac{x}{\sqrt{x^2+y^2}} \right)^2}=\dfrac{r^2(x^2+y^2)}{(\sqrt{x^2+y^2} \pm x)^2}\lbrack 15pt\rbrack \therefore (\sqrt{x^2+y^2} \pm x)^2 =r^2\\ \therefore x^2+y^2+x^2 \pm 2x\sqrt{x^2+y^2} =r^2\\ \therefore (2x^2+y^2-r^2)^2=4x^2(x^2+y^2)\\ \therefore 4x^4+y^4+r^4-4r^2x^2-2r^2y^2+4x^2y^2=4x^4+4x^2y^2\\ \therefore y^4-2r^2y^2+r^4=4r^2x^2\\ \therefore (y^2-r^2)^2=(2rx)^2$$ 

The above equation defines a double parabola. By resolving it we get a dyad of parabolas:

 $$y^2-r^2=2rx$  and  $y^2-r^2=-2rx$$ 

In the above construction we considered only one intersection between the radial line and the directing circle; in reality there are two. Hence, if we consider the intersection happens diametrically opposite to point A then we would get a second mirror image curve. This is why in the formal derivation we get a dyad of opposite facing parabolas.

The two parabolas share the x-axis as their common axis and expand in opposite directions. They intersect the x-axis at  $\pm .5r$  and the y-axis and the directing circle at  $\pm 1$ .

**Formal derivation of the equations of the knot curves**\
![parabola_knot_conchoid_line_knot](https://manasataramgini.files.wordpress.com/2016/10/parabola_knot_conchoid_line_knot.png){width="75%"}

We next perform the same operation as above on the case typified by point E. Its coordinates are  $(x,y)$ , which are the coordinates of the locus whose equation we are seeking to derive.The following relationships remain the same for point E as above:

 1.  The coordinates of point A on the directing circle are  $(x_1=r\cos(t), y_1=r\sin(t))$ .\
2) $\cos(t)= \pm \dfrac{x}{\sqrt{x^2+y^2}}$ \
3) $OD=x_1=r\cos(t) \; and \; DC=r-OD=r-r\cos(t)$ \
But here we have to consider everything in terms of point E; hence we have:

 $$\dfrac{AE}{r}=\dfrac{OD}{DC}\lbrack 15pt\rbrack \therefore AE=\dfrac{OD}{DC}r=r\dfrac{r\cos(t)}{r-r\cos(t)}=\dfrac{r\cos(t)}{1-\cos(t)}\lbrack 15pt\rbrack x^2+y^2=(r-AE)^2=\left(r-\dfrac{r\cos(t)}{1-\cos(t)}\right)^2\lbrack 15pt\rbrack =\left(\dfrac{r-2r\cos(t)}{1-\cos(t)}\right)^2=\left(\dfrac{r-\dfrac{2rx}{\sqrt{x^2+y^2}}}{1-\dfrac{x}{\sqrt{x^2+y^2}}}\right)^2=\left(\dfrac{r\sqrt{x^2+y^2}-2rx}{\sqrt{x^2+y^2}-x}\right)^2\lbrack 15pt\rbrack x^2+y^2=\dfrac{r^2(x^2+y^2)+4r^2x^2-4r^2x\sqrt{x^2+y^2}}{2x^2+y^2-2x\sqrt{x^2+y^2}}\lbrack 15pt\rbrack 2x^4+y^4+3x^2y^2-5r^2x^2-r^2y^2=\\ 2x^3\sqrt{x^2+y^2}+2xy^2\sqrt{x^2+y^2}-4r^2x\sqrt{x^2+y^2}\lbrack 10pt\rbrack 2x^4+y^4+3x^2y^2-5r^2x^2-r^2y^2=\\ (2x^3+2xy^2-4r^2x)\sqrt{x^2+y^2}\lbrack 10pt\rbrack \left(2x^4+y^4+3x^2y^2-5r^2x^2-r^2y^2\right)^2=\\ \left(2x^3+2xy^2-4r^2x\right)^2\left(x^2+y^2\right)\lbrack 10pt\rbrack 9r^4x^4-6r^4x^2y^2+r^4y^4-4r^2x^6-2r^2x^4y^2\\ -2r^2y^6+x^4y^4+2x^2y^6+y^8=0\lbrack 10pt\rbrack (3r^2x^2-r^2y^2-2rx^3-2rxy^2+x^2y^2+y^4)\\ (3r^2x^2-r^2y^2+2rx^3+2rxy^2+x^2y^2+y^4)=0$$ 

The above equation defines a double knot curve. By resolving it we get a dyad of quartic knots:

 $$3r^2x^2-2rx^3-r^2y^2-2rxy^2+x^2y^2+y^4=0$$ 

and

 $$3r^2x^2+2rx^3-r^2y^2+2rxy^2+x^2y^2+y^4=0$$ 

Each of the knots has the x-axis as their symmetry axis and respectively cut the x-axis at  $\pm 1.5r$  which marks the maximum horizontal extent of the loop of the knot on the side opposite to the knot point. For both knots the points of inflection and knotting is at  $(0,0)$ . The free ends of the knot with the left-facing inflection cross over the loop of the knot respectively at  $(r, \pm r)$ . The free ends of knot with the right-facing inflection cross over the loop of the knot respectively at  $(-r,\pm r)$ . Thereafter the free ends run below the above pair of parabolas and converge towards them as  $x \rightarrow \infty$ . The maximum horizontal extent of the humps of the right-facing knot on the side of the knot/inflection point is given by the points  $((3-2\sqrt{2})r, \pm (\sqrt{8\sqrt{2}-11})r)$ . The same for the left-facing knot is  $((2\sqrt{2}-3)r,\pm (\sqrt{8\sqrt{2}-11})r$ . For the right-facing knot the maximum vertical extant is given by the points  $(2\sqrt{3}-3)r, \pm (\sqrt{6\sqrt{3}-9})r)$ . For the left-facing knot it is given by the points  $((3-2\sqrt{3})r, \pm (\sqrt{6\sqrt{3}-9})r)$ .

**Formal derivation of the equations of the straight lines**\
![parabola_knot_conchoid_line_lines](https://manasataramgini.files.wordpress.com/2016/10/parabola_knot_conchoid_line_lines.png){width="75%"}

Now we shall consider the cases with the reciprocal ratio. First we shall tackle the case represented by point H in the construction. Its coordinates are  $(x,y)$ , the coordinates of our desired locus.\
We have:  $\dfrac{AH}{r}=\dfrac{CD}{OD}; \; \therefore AH=\dfrac{CD}{OD}r$ .\
As above point A performing UCM on our directing circle has coordinates  $(x_1=r\cos(t),y_1=r\sin(t))$  we get:  $OD=r\cos(t); CD=r-r\cos(t)$ .\
From this we get  $AH=\dfrac{1-\cos(t)}{cos(t)}r$ \
Now as above:  $\cos(t)=\dfrac{x}{\sqrt{x^2+y^2}}\lbrack 15pt\rbrack \therefore AH=\dfrac{\left(1-\dfrac{x}{\sqrt{x^2+y^2}}\right)r}{\dfrac{x}{\sqrt{x^2+y^2}}}=\dfrac{(\sqrt{x^2+y^2}-x)r}{x}$ \
Now for the point on the locus we have:

 $$x^2+y^2=(r+AH)^2=(r+\dfrac{(\sqrt{x^2+y^2}-x)r}{x})^2\lbrack 10pt\rbrack x^2(x^2+y^2)=(r\sqrt{x^2+y^2}-rx+rx)^2\\ x^2=r^2$$ 

The above equation defines a dyad of lines which can be resolved as:\
 $x=r$  and  $x=-r$ ; These are a pair of vertical lines that cut the x-axis at  $(\pm r)$ .

**Formal derivation of the equations of conchoid**\
![parabola_knot_conchoid_line_conchoids](https://manasataramgini.files.wordpress.com/2016/10/parabola_knot_conchoid_line_conchoids.png){width="75%"}

Finally, we shall consider the point G  $(x,y)$  in the construction to derive locus it specifies. We have  $AG=AH$ ; hence from above we get  $AG=\dfrac{(\sqrt{x^2+y^2}-x)r}{x}$ . However, for point G we have:

 $$x^2+y^2=(r-AE)^2=\left(\dfrac{rx+rx-r\sqrt{x^2+y^2}}{x}\right)^2\lbrack 15pt\rbrack \therefore x^2(x^2+y^2)=(2rx-r\sqrt{x^2+y^2})^2\\ x^4+x^2y^2=5r^2x^2+r^2y^2-4r^2x\sqrt{x^2+y^2}\\ (x^4+x^2y^2-5r^2x^2-r^2y^2)^2=16r^4x^2(x^2+y^2)\lbrack 10pt\rbrack (3r^2x^2-r^2y^2-2rx^3-2rxy^2-x^4-x^2y^2)\\ (3r^2x^2-r^2y^2+2rx^3+2rxy^2-x^4-x^2y^2)=0$$ 

The above equation defines a double conchoid and by resolving it we get the two conchoids:

 $$x^2(r-x)(x+3r)=y^2(x+r)^2$  and  $x^2(x+r)(3r-x)=y^2(x-r)^2$$ 

The two conchoids have the respective lines  $x=\pm r$  as the asymptotes of their two branches. The bow-like branches of the two conchoids intersect the x-axis at points  $\pm 3r$ . The loops of the two conchoids cut the x-axis at  $\pm r$  and the point of looping is at origin coincident with the point of knotting of the knots. Thus the two asymptotic lines are the tangents to the loops of the opposite conchoid at  $x= \pm r$ . The loop of the right-looped conchoid bounded by the points  $(x \approx 0.587401r,y \approx \pm 0.450196r)$  which mark its maximum vertical extent. For the loop of the left-looped conchoid the corresponding bounds are the points  $(x \approx -0.587401r,y \approx \pm 0.450196r)$ .

**Polar equations of the above curves**\
From the construction and the above derivations we can also easily derive the polar equations of the four curves of the original construction thus:

 1.  Right diverging parabola:

 $$\rho=r\left(1+\dfrac{\cos \left(\theta \right)}{1-\cos \left(\theta \right)}\right)=\dfrac{r}{1-\cos \left(\theta \right)}$$ 

 2.  Right facing knot:

 $$\rho=r\left(1-\dfrac{\cos \left(\theta \right)}{1-\cos \left(\theta \right)}\right)=\dfrac{r\left(1-2\cos \left(\theta \right)\right)}{1-\cos \left(\theta \right)}$$ 

 3.  Right straight line:

 $$\rho=r\left(1+\dfrac{1-\cos \left(\theta \right)}{\cos \left(\theta \right)}\right)=\dfrac{r}{\cos \left(\theta \right)}$$ 

 4.  Right-looped conchoid:

 $$\rho=r\left(1-\dfrac{1-\cos \left(\theta \right)}{\cos \left(\theta \right)}\right)=\dfrac{r\left(2\cos \left(\theta \right)-1\right)}{\cos \left(\theta \right)}$$ 

![knotting](https://manasataramgini.files.wordpress.com/2016/10/knotting.jpg){width="75%"}

In addition to having a nice symmetry to them these equations also represent an interesting progression in the process of knotting a linear entity like a rope or a polymeric molecule like the DNA double-helix: the straight line represents the starting state of the linear entity. The parabola represents the next with a bending appearing in it. Third it undergoes more extensive bending and looping as represented by the conchoid. Finally, it undergoes knotting in the form of the knot curve. Alternating, between the loci specified by the two reciprocal ratios we get this progression. Thus, the combination of UCM and SHM can be seen as mechanism to specify a knot through serial transformation of a linear entity. Another notable point is that the pair of loci produced by the same ratio are mutually asymptotic at their free ends.

**Points of intersection**\
![parabola_knot_conchoid_line_intersections](https://manasataramgini.files.wordpress.com/2016/10/parabola_knot_conchoid_line_intersections.png){width="75%"}

Dynamic version:\
```{=latex}
\begin{center}
```

![](https://s3.amazonaws.com/calc_thumbs/production/rqovdfmmxm.png){width="75%"}
```{=latex}
\end{center}
```



Finally, we shall consider the non-obvious points of intersection between the above loci which have not been mentioned earlier. There are 4 sets of them in each of the 4 quadrants which are symmetric when we have all 4 pairs of loci. Hence, we provide those only for the first quadrant with those for the remaining quadrants being the same except for sign changes.

Generating circle-conchoid:  $\left(\dfrac{1}{3}r,\dfrac{2\sqrt{2}}{3}r\right)$ 

Generating circle-knot:  $\left(\dfrac{2}{3}r,\dfrac{\sqrt{5}}{3}r\right)$ 

Knot-conchoid inner branch:  $\left(\left(1-\dfrac{1}{\phi }\right)r,\left(\sqrt{2-\dfrac{1}{\phi }}\right)r\right)$  where  $\phi=1.61803398875$  is the Golden ratio. This intersection produces an angle of  $\dfrac{2\pi }{5}=72\^o$  with respect to the x-axis thereby defining a regular pentagon

Knot-conchoid outer branch:  $\left(\left(1+\phi \right)r,\left(\sqrt{2+\phi }\right)r\right)$ 

Left-facing parabola-conchoid inner branch:  $\left(\left(2-\sqrt{3}\right)r,\ \left(\sqrt{2\sqrt{3}-3}\right)r\right)$ 

Left-facing parabola-conchoid lobe:  $\left(\left(\sqrt{2}-1\right)r,\left(\sqrt{2}-1\right)r\right)$ 

Left-facing parabola-knot:  $\left(\left(1-\dfrac{1}{\phi }\right)r,\left(\sqrt{2\phi -3}\right)r\right)$ 

Right-facing parabola-conchoid inner branch:  $\left(\left(\sqrt{2}-1\right)r,\left(\sqrt{2\sqrt{2}-1}\right)r\right)$ 

Right-facing parabola-conchoid outer branch:  $\left(\left(1+\sqrt{2}\right)r,\left(1+\sqrt{2}\right)r\right)$ 

