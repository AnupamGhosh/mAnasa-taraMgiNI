
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Ramanujan's second construction for the approximate squaring of a circle](https://manasataramgini.wordpress.com/2016/10/19/ramanujans-second-construction-for-the-approximate-squaring-of-a-circle/){rel="bookmark"} {#ramanujans-second-construction-for-the-approximate-squaring-of-a-circle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 19, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/10/19/ramanujans-second-construction-for-the-approximate-squaring-of-a-circle/ "5:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

To experience the greatness of great men one has to relive or redo some acts of theirs to the best of ones ability. In ones youth such enactments might inspire one to make a bid for greatness. Whether this happens or not is mostly up to your genetics. Nevertheless, through the enactments one can at least savor the experience of what it takes to get there. If there was one man in our midst who could have lived up to be a Gauss or an Euler it was Srinivasa Ramanujan.

By redoing some of his acts that are within the grasp of our limited intellect we experienced the monument that he was. He gave two constructions for the approximate squaring of a circle using a compass and a straight-edge. [We had earlier described the first and more widely known of those](https://manasataramgini.wordpress.com/2011/12/21/squaring-of-the-circle-by-srinivasa-ramanujan/). The second appears in his paper titled "*Modular equations and approximations to*  $\pi$ ". In this paper in addition to remarkable approximations for the perimeter of the ellipse, [which we had also alluded to before](https://manasataramgini.wordpress.com/2012/10/23/a-personal-discursion-on-conic-sections/), he gives several series for  $\pi=3.141592653589793...$ . One of these series with just the first term leads to the below approximation:\
 $\pi \approx \dfrac{99^2}{2*\sqrt{2}*1103} = 3.141592730013305$  which is correct down to 6 decimal places. It is this kind of accuracy he captures in his first construction for the quadrature of the circle. In the midst of the dizzying series he conjures in a very Hindu style of mathematics, he says that he came up with an empirical approximation which leads to the below construction for the approximate squaring of the circle:

![ramanujan_squaring_circle2](https://manasataramgini.files.wordpress.com/2016/10/ramanujan_squaring_circle2.png){width="75%"}


 1.  Draw circle to be squared with center O.

 2.  Draw its diameter  $\overline{AB}$ .

 3.  Trisect its radius  $\overline{AO}$  to get a third of it as  $\overline{AF}$ .

 4.  Bisect the semicircle AB to get point C.

 5.  Draw  $\overline{BC}$ .

 6.  On  $\overline{BC}$  mark  $\overline{CG}= \overline{GH}=\overline{AF}$ .

 7.  Join point A to point H to get  $\overline{AH}$  and to point G to get  $\overline{AG}$ 

 8.  With radius as  $\overline{AG}$  cut  $\overline{AH}$  to get point I.

 9.  Draw a line parallel to  $\overline{GH}$  through point I to cut  $\overline{AG}$  at point J.

 10.  Join points O and J to get  $\overline{OJ}$ .

 11.  Draw a line parallel to  $\overline{OJ}$  through point F to cut  $\overline{AG}$  at point K.

 12.  Draw the tangent to the circle at point A and cut it with radius as  $\overline{AK}$  to get point L.

 13.  Draw  $\overrightarrow{OL}$  to cut circle at point M.

 14.  Draw semicircle LM and perpendicular from point O to cut this semicircle at point N.

 15.  Triplicate  $\overline{ON}$  to get  $\overline{OQ}=3 \times \overline{ON}$ .

 16.  Produce  $\overline{OQ}$  in the opposite direction to cut circle at point R.

 17.  Draw semicircle RQ and a perpendicular from point O to cut it at point S.

 18.  Thus, we have  $\overline{OS}$  as the side of the square OSTU which has approximately the same area the starting circle.

Ramanujan tells us that his earlier construction gave an "ordinary" value  $\pi \approx \dfrac{355}{113}=3.141592920353982$ , which is correct to six decimal places. This one, however, gives us the value:

 $$\pi \approx \left (9^2+\dfrac{19^2}{22}\right)^{\frac{1}{4}}= 3.141592652582646$$ 

This is correct to a whopping eight decimal places keeping with the Hindu love for big numbers.

