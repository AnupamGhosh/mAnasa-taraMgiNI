
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [शूलजन-काव्य: Das Riesenspielzeug](https://manasataramgini.wordpress.com/2016/01/11/sulajana-kavya-das-riesenspielzeug/){rel="bookmark"} {#शलजन-कवय-das-riesenspielzeug .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 11, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/01/11/sulajana-kavya-das-riesenspielzeug/ "7:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Somakhya's and Lootika's families were returning from a ritual at the less-known temple of the goddess भुवनेश्वरी. They were there to worship a mysterious yantra, which is one of the rare examples of the depiction of a curve known as the astroid in Hindu tradition. On the way back Somakhya's family was invited for lunch at Lootika's house. Lootika wanted Somakhya to show her how to construct the astroid used in the yantra without using one circle rolling inside another -- the method used by the शैव तान्त्रिक-s of yore who composed the भुवनेश्वरी lore. But before they could begin Somakhya was distracted by the lines Lootika was humming: "Hey, you seem to be humming a शूलजन-काव्य. That too a poem I was reading last night."\
Lootika: "What? I was reading the same last night too ... talk of the Über-geist."\
Somakhya: "Why that poem?"

Lootika: "It is an interesting reason why I picked it up. I had gone to the university yesterday to do some experiments. There a professor asked me if I might be interested in studying some strange basal chordates, salps, which he had obtained from the sea. These salps with an interesting alternation of sexual and asexual cycles were described long ago by Adelbert von Chamisso in course of his explorations -- one could after all call him a lesser von Humboldt. I recalled that there was a poem of his in the collection of शूलजन-काव्य that we have for reading. So on returning home I read it and was captivated by it. If there is a non-Aryan काव्य that has dhvani then this is one exemplar from the शूलजन-s."

Somakhya: "dhvani ... yes, that's exactly what I feel with that padya. Our resonance reinforces that. The opening itself has a special quality to it. Read out the first verse गौतमी."

Lootika picked up her book and read it out:\
"[बुरग निॆदॆcक िसत िम ॆलसß दॆर सगॆ वॊहलबॆकननत,]{style="color:#33cccc;"}\
[ die Höhe, wo vorzeiten die Burg der Riesen stand;]{style="color:#33cccc;"}\
[ sie selbst ist nun verfallen, die Stätte wüst und leer,]{style="color:#33cccc;"}\
[दु फ़्रगेस्त् नछ् देन् ऋइएसेन्, दु फ़िन्देस्त् सिए निछ्त् मेह्र्।]{style="color:#33cccc;"}"\
\[The castle Niedeck is in Alsace, its saga is well-known,\
On its heights, aforetime the castle of the giants stood,\
it is now all ruined, the place itself is empty and bare,\
if you ask regarding the giants, you find them no more.]

Lootika then continued: "Somakhya, this looks like a memory filtering down from the time before pan-Germania fell to the प्रेतोन्माद. The Riese seems like a remnant of the lore of the giants that was at the base of the northern Germanic genesis. In a sense the passing of the time of the giants is in built into the Germanic genesis with \*Wodin, Wili and We, the first of the Æsir gods slaying the ancient giant Ymir and fashioning the world from his various parts even as in our tradition the world is fashioned from the parts of the पुरुष sacrificed by the deva-s or the slain अश्वमेध horse. But what is the link between the jotunn of northern Germanic and the word Riese?

Somakhya: "Indeed, this is a memory from prior to the coming of the Abe' disease to these peoples. Jotunn is an interesting word. It appeared in early Niederdeutsch as Etennine meaning a giantess. It appears in the वाक् of the कृशजन-s as ettin all descending from proto-Germanic \*etunaz. While the ettins of pan-Germania resemble the Titans of the yavana-s or the post-RV asura-s of our tradition, very few know that their actual etymological cognate in our tradition are the attrin-s. These demons are only known to those who know the spells of our ancestors, the भृगु-s and अङ्गिरस्-es. The Deutsch Riese and risi of their northern cousins thus appear to be a specific type of Jotunn associated with the mountains and the sea. One type of northern Germanic risi was said to live on the high hills like in von Chamisso's verse. He was associated with the troll girls Fenya and Menya. The people of the frigid northern Norway were said to have descended from the risi via a Viking warrior chief Halogi. The other risi, the sjórisar (sea risi-s), were Ægir the god of the ocean, Ran the fierce goddess, and their nine daughters. When a viking drowned he was supposed become an offering for Ran. "

Lootika: "I suspect that the daughter of the Riese, the central protagonist in von Chamisso's poem, is a reflection of the old the motif of the several daughters of the risi in old legends of the शूलजन-s, be it the mill-turning Fenya and Menya or the nine daughters of Ægir and Ran. Thus, the Riese surviving down to the days of von Chamisso in the 1800s probably includes within a faint memory of these prototypes from heathen Germania."

Somakhya: "As you know, motifs can survive over great distance of time and space, much like domains in the the protein-universe. Perhaps their survival in the universe of legend is more of an evolutionary challenge than what is seen in the protein-universe. In fact Fenya and Menya as endless turneresses of the mill represent a motif going back to early Indo-European times. They appear in the great भारत as the two mysterious girls धाता and विधाता who were seen endless plying the loom by उतङ्क. This in turn is stems from the old brahmodaya of the भृगु-s and अङ्गिरस्-es:\
[तन्त्रम् एके युवती विरूपे अभ्याक्रामं वयतः षण्-मयूखम् ।]{style="color:#99cc00;"}\
[प्रान्या तन्तूंस् तिरते धत्ते अन्या नाप वृञ्जाते न गमातो अन्तम् ॥]{style="color:#99cc00;"}\
\[Singly the two girls of different forms approach the six-pegged loom to weave.\
One stretches the threads, the other places them; they do not break them, and go on without an end]\
The key point is [न गमातो अन्तम् ।]{style="color:#99cc00;"} -- like Fenya and Menya they have no rest."

Lootika: "Talking of old motifs turning up long at a place far from their time and place of origin I was reminded of a very curious local reflex of the motif of the end of the time of the giants with the cleaving of Ymir I heard when my family had gone to काशी. While it is detour from the शूलपुरुष's काव्य I have been wanting to tell you this one if you don't mind."\
Somakhya: "Pray continue."

Lootika: "When we were in काशी beside a घाट् possibly renovated by the शिन्दे we espied a yati from Tobago giving fiery speech on the cult of Kabir. The tale he narrated went thus: 'The original self-existent deity was the Sat-पुरुष. He created six brahminical sons ओंकार, इच्छा, Soham, Acintya and अक्षर. They were not productive. He generated a vast realm of water in which he placed अक्षर in a deep sleep. Then अक्षर saw a great egg floating on the water. He meditated with single focus on this egg. The egg exploded with a loud clap and from it emerged the ferocious deity निरञ्जन who was also known as the काल-पुरुष. After doing tapasya for long निरञ्जन obtained a boon from Sat-पुरुष for the genesis of the triloka. But he did not have material for this creation. The Sat-पुरुष told निरञ्जन that the gigantic polycephalous primal tortoise possessed the material and asked निरञ्जन to request the beast to give him the same. निरञ्जन instead tried to seize it from the tortoise leading to a fight between them. In course of that निरञ्जन cut off 16 heads of the giant tortoise and from those severed heads emerged the material that formed the sun, the moon, the earth and other aspects of existence. The great tortoise complained to the Sat-पुरुष of the violence of निरञ्जन. Then the Sat-पुरुष cursed निरञ्जन that he should never enter his Platonic sat-loka. While the genesis of much of the universe had proceed निरञ्जन failed to make man. So he ate up three of the severed chelonian heads and meditated upon Sat-पुरुष asking him for a क्षेत्र. The Sat-पुरुष emanated a woman called माया. She was first unable to get her eyes of the Sat-पुरुष but gradually she was attracted towards निरञ्जन and copulated with him. The three chelonian heads he had ingurgitated now allowed him to father three sons on माया known as Brahman, विष्णु and Rudra. At the same time he emanated the Veda-s, and in the process entirely vanished. माया in turn spawned three daughters but at that point the Veda-s and the three females sunk into the great world-ocean that surrounded them. At that point Brahman, विष्णु and Rudra began churning the world ocean with a churning rod \[Does this this not remind you of Fenya and Menya's mill which was thrown in the ocean to become the churning maelstrom?]. As result the three females and the Veda-s came up as the butter. The first was Sarasvati into whom the Veda-s merged and she became Brahman's wife. Then came लक्ष्मी who became विष्णु's wife and finally उमा who became that of Rudra. Then Brahman spawned the world of men. The men first worshiped the Veda-s and Sarasvati. They then worshiped the trinity but ignored माया so she created three further women who gave men song and dance and turned them towards the worship of the transfunctional देवी who is none other than माया.'

Thus, the winding Kabirian narrative of the Caribbean yati went on with a culmination in a Kabirian Fimbulvetr and Ragnarök followed by a re-genesis on the back of the giant tortoise. Of course, the persistence of many an old motif in this rather recent genesis narrative should be rather obvious to you."

Somakhya: "Lootika, undoubtedly a remarkable legend -- who could miss the ancient, colossal tortoise, almost as though inspired by Archelon of the Mesozoic or Stupendemys of the Miocene -- verily, an ancient motif appearing in the ऋग्वेद as a manifestation of Indra as अकूपार, the कश्यप. It is indeed this primordial ocean bearing that colosal turtle, which is invoked by the भृगु-s, who alone are ordained in the श्रुति to serve as the होतृ-s in the ritual crowning of the victorious आर्य monarch at the राजसूय:\
[हिरण्यवर्णाः शुचयः पावका]{style="color:#99cc00;"}\
[यासु जातः कश्यपो यास्व् इन्द्रः ।]{style="color:#99cc00;"}\
[या अग्निं गर्भं दधिरे सुवर्णास्]{style="color:#99cc00;"}\
[ता न आपो राजसूय अवन्तु ॥]{style="color:#99cc00;"}

Golden-tinted, pure and shining,\
wherein was born the primal turtle, wherein indra,\
who had borne the golden embryonic agni,\
may those waters \[of \]the राजसूय \[ritual] confer protection for us.

And then there is the churning, that archaic motif of axial precession, which appears in so many legends throughout the human mythosphere rather notably resurfacing here along with the allusion to the loss of the previous axial position in the form of the veda-s and the three females.

That said continue reading the rest of the काव्य, Lootika."

[ऎइन्स्त् कम् दस् ऋइएसेन्फ़्र्äउलेइन् औस् जेनेर् Bउर्ग् हेर्वोर्,]{style="color:#33cccc;"}\
[एर्गिन्ग् सिछ् सोन्देर् Wअर्तुन्ग् उन्द् स्पिएलेन्द् वोर् देम् टोर्]{style="color:#33cccc;"}\
[ und stieg hinab den Abhang bis in das Tal hinein,]{style="color:#33cccc;"}\
[ neugierig zu erkunden, wie's unten möchte sein.]{style="color:#33cccc;"}\
\[Once, the giant's daughter came out from that castle,\
attending to herself with special care and playing above the valley,\
and \[she] went down the slope right into the valley,\
curious to explore how it would be below her \[place].]

[Mit wen'gen raschen Schritten durchkreuzte sie den Wald,]{style="color:#33cccc;"}\
[एर्रेइछ्ते गेगेन् ःअस्लछ् दस् ळन्द् देर् ंएन्स्छेन् बल्द्,]{style="color:#33cccc;"}\
[उन्द् ष्त्äद्ते दोर्त् उन्द् ड्öर्फ़ेर् उन्द् दस् बेस्तेल्ल्ते Fएल्द्]{style="color:#33cccc;"}\
[ erschienen ihren Augen gar eine fremde Welt.]{style="color:#33cccc;"}\
\[With a few quick steps she crossed through the forest,\
soon \[she] arrived beside Haslach, the land of men,\
and there cities and villages and the plowed field\
became visible to her eyes, verily an alien world.]

[Wie jetzt zu ihren Füßen sie spähend niederschaut,]{style="color:#33cccc;"}\
[बेमेर्क्त् सिए एइनेन् Bऔएर्, देर् सेइनेन् आच्केर् बौत्;]{style="color:#33cccc;"}\
[एस् क्रिएछ्त् दस् क्लेइने Wएसेन् एइन्हेर् सो सोन्देर्बर्,]{style="color:#33cccc;"}\
[ es glitzert in der Sonne der Pflug so blank und klar.]{style="color:#33cccc;"}\
\[How now peeking she looks down to her feet,\
\[and] she notices a farmer who works his field;\
\[the thing] accompanying the little being crawls, it's so curious,\
The plow, it glitters so brightly and clearly in the sun.]

["ऎइ! अर्तिग् ष्पिएल्दिन्ग्!" रुफ़्त् सिए, "दस् नेह्म्' इछ् मित् नछ् ःऔस्!"]{style="color:#33cccc;"}\
[ Sie knieet nieder, spreitet behend ihr Tüchlein aus]{style="color:#33cccc;"}\
[ुनद फ़ॆगॆत मित दॆन हäनदॆन, वस सिcह द aललॆस रॆगत,]{style="color:#33cccc;"}\
[ zu Haufen in das Tüchlein, das sie zusammenschlägt,]{style="color:#33cccc;"}\
["Hey, it's like a toy"she cries,"I am taking it home with \[me]"\
She kneels down, nimbly spreads her handkerchief out,\
and sweeps with her hands, all that is moving\
in a pile together into the cloth, which she bundles up]

[ुनद ॆिलत मित फ़रॆुद'गॆन सपरüनगॆन, मन वॆिß, विॆ किनदॆर सिनद,]{style="color:#33cccc;"}\
[ zur Burg hinan und suchet den Vater auf geschwind:]{style="color:#33cccc;"}\
["ॆि वतॆर, लिॆबॆर वतॆर, ॆिन सपिॆलदिनग वुनदॆरसcहöन!]{style="color:#33cccc;"}\
[षो आल्लेर्लिएब्स्तेस् सह् इछ् नोछ् निए औफ़् उन्सेर्न् ःöह्'न्।"]{style="color:#33cccc;"}\
\[and with joyous leaps she scurries, you know how children are,\
to ascend the castle and seek her father in a hurry:\
"Oh Father, dear father, a beautiful toy!\
I have not yet seen so lovely \[a thing] in our high realm"]

[डेर् आल्ते सß अम् टिस्छे उन्द् त्रन्क् देन् क्üह्लेन् Wएइन्,]{style="color:#33cccc;"}\
[ er schaut sie an behaglich, er fragt das Töchterlein:]{style="color:#33cccc;"}\
["Wअस् ऴप्पेलिगेस् ब्रिन्ग्स्त् दु इन् देइनेम् टुछ् हेर्बेइ?]{style="color:#33cccc;"}\
[दु हüपफ़ॆसत ज वॊर फ़रॆुदॆन; लß सॆहॆन, वस ॆस सॆि।"]{style="color:#33cccc;"}\
\[The elder sat at the table and drank the cool wine,\
at ease he looks at her, he asks the little daughter:\
"What wriggling thing you bring in your towel?\
Indeed, you are leaping about in joy, Let's see what it is."]

[Sie spreitet aus das Tüchlein und fängt behutsam an,]{style="color:#33cccc;"}\
[ den Bauer aufzustellen, den Pflug und das Gespann;]{style="color:#33cccc;"}\
[ wie alles auf dem Tische sie zierlich aufgebaut,]{style="color:#33cccc;"}\
[सो क्लत्स्छ्त् सिए इन् दिए ःäन्दे उन्द् स्प्रिन्ग्त् उन्द् जुबेल्त् लौत्।]{style="color:#33cccc;"}\
\[She spreads out her handkerchief and gently begins\
to place the farmer, the plow and his team;\
like everything else placed on the table they are made small,\
she claps her hands and jumps and cheers loudly.]

[दॆर aलतॆ विरद गर ॆरनसतहफ़त ुनद विॆगत सॆिन हुपत ुनद सपरिcहत:]{style="color:#33cccc;"}\
[ "Was hast du angerichtet? Das ist kein Spielzeug nicht!]{style="color:#33cccc;"}\
[ Wo du es hergenommen, da trag es wieder hin,]{style="color:#33cccc;"}\
[ der Bauer ist kein Spielzeug, was kommt dir in den Sinn?]{style="color:#33cccc;"}\
\[The elder becomes really serious and shakes his head and says:\
"What have you done? That ain't any toy!\
wherever you have brought it from place it back there again.\
the farmer is not a toy, whatever got into you head?"]

[षोल्ल्स्त् ग्लेइछ् उन्द् ओह्ने ंउर्रेन् एर्फ़्üल्लेन् मेइन् ङेबोत्;]{style="color:#33cccc;"}\
[दॆनन वäरॆ निcहत दॆर बुॆर, सॊ हäततॆसत दु कॆिन बरॊत;]{style="color:#33cccc;"}\
[एस् स्प्रिएßत् देर् ष्तम्म् देर् ऋइएसेन् औस् Bऔएर्न्मर्क् हेर्वोर्,]{style="color:#33cccc;"}\
[ der Bauer ist kein Spielzeug, da sei uns Gott davor.]{style="color:#33cccc;"}\
\[Right now, without grumbling fulfill my command;\
because without the farmer, you will not have any bread;\
the race of giants springs from the farmer stock,\
the farmer is not a toy, god-forbid.]

[बुरग निदॆcक िसत िम ॆलसß दॆर सगॆ वॊहल बॆकननत,]{style="color:#33cccc;"}\
[ die Höhe, wo vor Zeiten die Burg der Riesen stand;]{style="color:#33cccc;"}\
[ sie selbst ist nun verfallen, die Stätte wüst und leer,]{style="color:#33cccc;"}\
[उन्द् फ़्रग्स्त् डु नछ् देन् ऋइएसेन्, दु फ़िन्देस्त् सिए निछ्त् मेह्र्।]{style="color:#33cccc;"}"\
\[The castle Niedeck is in Alsace, its saga is well-known,\
On its heights, aforetime the castle of the giants stood,\
it is now all ruined, the place itself is empty and bare,\
if you ask regarding the giants, you find them no more.]

Lootika then added: "This tale seems to have been rather popular or widespread among the शूल-jana-s. I have read of at least 6 retellings of it with several illustrations of the giant's daughter, with the first apparently being being collected by the Grimms, those notable folklorists among the शूल-jana-s. Why this love for farmers among the Riese? Was it a persistent expression of the farmer's protest against the feudal lord or angst for displacement by him who was euphemized as the giant?"

Somakhya: "Indeed, some interpreters have taken that line of reasoning, including those who have sought to interpret this poem of von Chamisso -- a plea to the state, symbolized by the giant's daughter, against displacing farmers and seizing agricultural land for other purposes. The idea that giants depend on the farmer for their food is seen by them as a strong argument in favor of protecting the agrarian economy, which according to some was being threatened by the developments beginning around that time. However, expression of such dependencies have old histories. For instance, in our own tradition there is the discussion in the श्रुति regarding the elite, namely we ब्राह्मण-s and our क्षत्रिय partners, vis-a-vis the विश्. The ब्राह्मण-s and क्षत्रिय-s are constantly moving around (a reflection of our steppe-zone ancestry) and hence unstable. But the विश् stays put and is stable. Hence, the brahma-क्षत्र elite depend on the presence of a stable base of विश् for their sustenance -- something similar to the Riese-Bauer dependency alluded in this cycle of legends..."

Lootika: "But then there could also be something else deeper. For folklorists have collected evidence for a similar specific link between the farmers and these giants not only in Burg Nideck in Elsaß but also spread out over greater Germania, including legends from the Danish region of Zealand and Swedish regions of Västergötland and Dalsland. There the giants are said to get ale for their revelry and hay for their livestock from the farmers and return much more than they got. The giants are also said to help them during crop failure. In particular the giants are said to get ale from the farmers during the winter solstice feast in December. All this suggest a link going back to the old Germanic tradition before the onslaught of Isa's warriors."

Somakhya: "I agree there is a deeper motif from the old Germanic tradition that is speaking to us here from beyond the veil cast by the evils of Isaism. These giants who were favorable to the विश् were probably a part of the old religion, like the यक्ष-s led by वैश्रवण the brothers of the रक्षस्-es, who are worshiped in our midst. Looking at such legends today in ruined cultures we cannot escape the feeling of having come a full cycle. The discovery of Indo-European led to the idea of evolving languages in which these Grimm brothers were pioneers. Their work appears to have made its entry into the land of our former conquerors the कृशजन-s in the 1830s and deeply influenced Darwin's cousin/brother-in-law Wedgwood. These influences appear to have provided a strand of inspiration of the development of his evolutionary theory -- after all phylogenetic tree diagrams were first depicted by this school Indo-Europeanists for languages. The Grimms are more known for their tales today -- in their later editions it is said: 'the Grimms..."vaccinated" or censored them with their sentimental Christianity and puritanical ideology\[J. Zipes]'. But the originals of these tales too have naturally evolved, much like the languages that bore them. Even as we have been doing with biological sequences we might endeavor to discover the function of faint motifs unearthered in their midst."


