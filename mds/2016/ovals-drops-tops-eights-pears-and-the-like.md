
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Ovals, drops, tops, eights, pears and the like](https://manasataramgini.wordpress.com/2016/06/06/ovals-drops-tops-eights-pears-and-the-like/){rel="bookmark"} {#ovals-drops-tops-eights-pears-and-the-like .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 6, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/06/06/ovals-drops-tops-eights-pears-and-the-like/ "6:06 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Ovals, drops, tops, eights, pears and the like](https://manasataramgini.files.wordpress.com/2016/06/ovals_etc.pdf)

This piece may be seen as a [continuation of the earlier one on our journey through the world of ovals](https://manasataramgini.files.wordpress.com/2016/06/ovals_etc-2.pdf). As it needed a lot of figures and some mathematical notation it is being provided as a PDF file.

![oval_evolute](https://manasataramgini.files.wordpress.com/2016/06/oval_evolute.jpg){width="75%"}

