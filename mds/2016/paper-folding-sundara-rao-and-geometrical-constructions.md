
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Paper folding, Sundara Rao and geometrical constructions](https://manasataramgini.wordpress.com/2016/11/26/paper-folding-sundara-rao-and-geometrical-constructions/){rel="bookmark"} {#paper-folding-sundara-rao-and-geometrical-constructions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 26, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/11/26/paper-folding-sundara-rao-and-geometrical-constructions/ "7:31 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Paper folding, which we shall hereinafter refer to as origami without further discussion on the correctness of the usage, is believed to have had a long history in Japan. Some believe that some primitive form of origami might have been transmitted by the bauddha-s from the 600s of the common era but we have no strong evidence for such. Real evidence for methodical folds only comes from the charms known as noshi used by the samurai from the last millennium. In this regard some have speculated that the word 'kami' in origami might also be homophonic play on 'kami' as a god, with paper having an association with offerings to the gods in the shrines. The first clear references to folds that are part of modern origami only come from the period between the 1600-1700s. In Saikaku Ihara's story from the late 1600s he talks of his hero distinguishing himself at the age of seven by folding butterflies and flowers with great dexterity. The butterflies and the flowers would imply the presence of the famous waterbomb base assuming they were similar to what has been provided by Yoshizawa Akira, the father of origami in the modern era. In Akisato Rito's "Sembazuru Orikata" from the late 1700s we encounter the famous bird base for folding a crane.

Our own introduction to origami came from some folds that were known to my late grandfather (see below). Then our parents gave us a book of Robert Harbin. His work which was influenced by Yoshizawa Akira's made us aware of several simple folds but many of his models were frustratingly unclear. We then ran into a man who had spent some time in Japan and learned folding from a successor of Yoshizawa Akira. He showed the real complexities that can be achieved by folding. We folded models of that level of complexity only when we encountered the diagrams of the virtuoso mathematician-folder John Montroll. Somewhere in Montroll prodigious output lay the highest level of origami we ever reached. Montroll and the physicist Robert Lang can be considered the extant masters of "scientific origami", which emerges from a combination of a deep understanding of the underlying mathematics with aesthetics. Their models display extraordinary complexity and naturalism.

The encounter with origami quickly kindled our interest in using it as means to study Euclidean geometry. In our youth we discovered for ourselves some trivial constructions for the octagon, the square pyramid and the point of intersection of two lines using such techniques. It was only thereafter that we discovered the pioneering work of a remarkable Hindu mathematician named T. Sundara Rao, who should be regarded the father of the geometry of paper folding.

![sundara_rao](https://manasataramgini.files.wordpress.com/2016/11/sundara_rao.png){width="75%"}

*Figure 1: T. Sundara Rao*

Since he has been largely forgotten we shall here reiterate his biography. Sundara Rao was the son of Gopala Rao and grandson of Bava Pandita. The latter came from a family of महाराष्ट्री देशस्थ ब्राह्मण-s who had settled in the Thanjavur in the days of Shivaji's conquest of the south and had been appointed as advisers to the मराठा राजन् of Thanjavur. Gopala Rao was born at the गणपति अग्रहार on the banks of the Kaveri in 1832 CE. At home he with the rest of his family was conversant in Sanskrit but he largely taught himself English and rose to be a brilliant educator at Kumbhaghona. Even Englishmen were supposed to attend his classes for the special insights he provided in his classes on mathematics, history and English. He was also a Marathi poet but I have little knowledge of his literary achievements in that direction. In 1853 CE his first son Sundara Rao was born. At the age of 21 Sundara was awarded a BA degree standing second in mathematics in the whole Madras Presidency and was also said to be well-versed in Sanskrit. To earn a better wage he joined the civil service and became a deputy collector serving at Tiruchirappalli. He was an inventive man and had made the following: 1) Boat for his own use. 2) An ink pen which he used throughout his life. 3) An ink which was widely used for cyclostyling documents. 4) A version of the Hitchcock lamp that had a much longer life. Throughout his life he kept doing his researches in mathematics and published two books on the subject. Of these the one on paper folding termed "Geometrical Exercises in paper folding" survives to date. This was mainly because it caught the attention of his famous contemporary, German mathematician Felix Klein, who referred to it in his work. As a result two American mathematicians, Wooster Beman from University of Michigan and David Smith from Columbia University made an American edition of his book, resulting in the survival of this remarkable work. Sundara was also a good horseman and photographer and appears to have died sometime after 1923. In many ways his fate was like that of other early modern Hindu scientists and mathematicians --- a bright meteor in the intellectual firmament with no successors continuing the school. Indeed, there was nothing at all done in India to build on Rao's work in geometry by folding, and most of the people who were able to appreciate and continue his work were either from Europe or USA. This was despite the fact that there were several roughly contemporaneous luminaries in India like the great Srinivasa Ramanujan who came from the same town as Sundara Rao, which is not far from the hamlet of one strand of our ancestors.

Rao's work was inspired Friedrich Fröbel who introduced paper folding as a part of Kindergarten education in the west. But as Rao notes there was also an indigenous tradition of paper folding in India whose history has been much neglected. This tradition produced folds like the boat, catamaran, ink-bottle, briefs, flying boat, lotus bud and the like. These folds were known to my late grandfather who had no contact with western folding. The lotus bud which appears to have a relationship with the Japanese flower and the waterbomb might imply that these designs using the waterbomb base came to India from Japan. However, the other folds noted above are unique and likely to have developed independently of the Japanese and western origami traditions by the 1800s in India. Rao also states that folding was used to maintain register when writing Sanskrit or Marathi on paper.

Rao's constructions span the entire range from the trivial to the fairly complex but throughout they display a deep familiarity with geometry and opened a new type of understanding for old problems.

![equilateral_triangle2](https://manasataramgini.files.wordpress.com/2016/11/equilateral_triangle2.jpg){width="75%"}*Figure 2: Equilateral triangle*

One of the simplest of his constructions is the folding of an equilateral triangle from a square sheet of paper:

 1.  Fold the square in half by getting corners A to B and D to C to produce crease  $\overline{EF}$ .

 2.  Fold through corner A to make corner B lie on  $\overline{EF}$ . Similarly fold through corner B to make A lie on  $\overline{EF}$ . In each case crease along the folded  $\overline{EF}$  to get creases  $\overline{BI}$  and  $\overline{AJ}$ .

 3.  The point of intersection of above two creases ( $B\'$ ) defines the equilateral  $\triangle ABB\'$ .

 4.  Once you have an equilateral triangle we can determine its center and use that to fold a hexagon (all described by Rao).

![origami_golden_section_pentagon](https://manasataramgini.files.wordpress.com/2016/11/origami_golden_section_pentagon.png){width="75%"}

*Figure 3: Golden Ratio and folding a pentagon*

A notable construction of Rao is to obtain the Golden ratio by folding a square piece of paper and use this to fold a regular pentagon:

 1.  Bisect the square as above to produce crease  $EF$ . This in turn produces rectangle  $EFCD$ .

 2.  Fold through corners of this rectangle E and C to obtain  $\overline{EC}$ . This crease is the diagonal of the said rectangle.

 3.  Fold through E to bring corner D to lie on  $\overline{EC}$  at point G. As a result  $\overline{DH}$  will now lie along  $\overline{HG}$ 

 4.  Fold along the edge of the paper  $\overline{DH}$  to produce crease HI.

 5.  Fold through corner C to bring side  $DC$  of the paper to lie along crease  $EC$ .

 6.  Keeping the fold mentioned in the above step use crease  $HI$  and again fold along it. This will produce crease  $KG\'$ . Extend this crease vertically folding through it to produce crease  $G\'L$ .

 7.  This will Golden section  $\overline{DC}$ .  $\therefore \overline{G\'C}:\overline{G\'D}=\phi$ .

As an exercise one can use this construction to mark out two linked squares whose sides are in Golden ratio. This can be used fold a Golden version Akisato Rito's linked cranes from each of the squares, perhaps in the memory of the tragic tale of Sadako Sasaki a victim of the mass murder by the Americans at Hiroshima.

To construct a regular pentagon from this square piece of paper do the following:

 1.  Bisect  $\overline{AL}$  to get crease  $MM\'$ .

 2.  One will note that  $\overline{AM}=\overline{BI}$ .

 3.  Fold through point M so that point I comes to lie on edge  $AD$  of the paper. Similarly fold through point I so that point M comes to lie of side  $BC$  of the paper. This will determine points N and O. Fold along  $\overline{MN}$  and  $\overline{IO}$  to get the first three sides of the pentagon.

 4.  Fold through point N so that point M comes to lie on the vertical bisector of the square and crease the paper along  $\overline{MN}$ . Do the same with point O so that point I lies on the vertical bisector of the square. This operation will establish point P. Folding along  $\overline{NP}$  and  $\overline{OP}$  will give us the required pentagon.

 5.  One will note that the pentagon has sides of length  $\dfrac{1}{\phi}$  and diagonals of unit length, where the unit is the side of the starting square paper.

Nearly a century after Rao's folding of the pentagon using the Golden ratio, non-compass and straight-edge polygons like the heptagon have been folded by Robert Geretschläger using pure origami. These folds reminds a Hindu of the noshi carried as charms by the samurai. The polygonal folds could be similarly adopted as yantra-s: e.g. the pentagram in the yantra of the योगिनी श्यामल

![regular_polygons](https://manasataramgini.files.wordpress.com/2016/11/regular_polygons.jpg){width="75%"}*Figure 4: Various polygons folded from square paper and a couple adapted as yantra-s*

Rao boldly advanced folding as a method to construct conic sections. Below we shall describe his construction of the parabola:

![origami_parabola](https://manasataramgini.files.wordpress.com/2016/11/origami_parabola.png){width="75%"}*Figure 5: Parabola*


 1.  Take a square or rectangular piece of paper. One side of it  $AB$  shall be the directrix of the parabola.

 2.  Fold a vertically to crease rectangle ABIE. Use this rectangle to crease rectangle  $IGHE$  such that  $\overline{BI}=\overline{IG}$ .

 3.  Fold the paper horizontally in half to get bisecting crease. Where this crease intersects crease  $GH$  will be the focus of the parabola  $F$ .

 4.  Fold the top half of the paper into  $n$  horizontal parallel creases.

 5.  Fold the point of intersection of each of these parallel creases with the directrix edge of the paper such that it comes to lie on point  $F$ .

 6.  This will create the creases shown in green. Repeat the same for the bottom half.

 7.  These creases are tangents to the parabola and their envelop with mark out the parabola on the paper. One can see that this simulates via folding the construction of the parabola [we have described earlier](https://manasataramgini.wordpress.com/2008/03/28/fancies-of-the-parabola-and-hyperbola/).

This construction and our other experiments with envelop constructions inspired us to fold a square paper into successive mountain and valley folds which creates a 3D paraboloid surface. One can see that it is a curve of the form:  $z=xy$  -- a hyperbolic paraboloid. We later realized that such origami is being used by Erik Demaine to make a variety of more complex shapes. A similar technique can also be used to fold a double helix structure like a cartoon of double-stranded nucleic acid.

[](https://manasataramgini.wordpress.com/2016/11/26/paper-folding-sundara-rao-and-geometrical-constructions/paraboloid1/){border="0" itemprop="url"}

![paraboloid1](https://manasataramgini.files.wordpress.com/2016/11/paraboloid1.jpg&resize=367%2C364&h=364#038;h=364 "paraboloid1"){width="75%"}

[](https://manasataramgini.wordpress.com/2016/11/26/paper-folding-sundara-rao-and-geometrical-constructions/paraboloid2/){border="0" itemprop="url"}

![paraboloid2](https://manasataramgini.files.wordpress.com/2016/11/paraboloid2.jpg&resize=265%2C364&h=364#038;h=364 "paraboloid2"){width="75%"}

*Figure 6: Hyperbolic paraboloid*

Most of Rao's constructions were origami equivalents of constructions that can be done using a compass and a straight edge. For example, he was unable to double the cube using a square piece of paper and origami. However, with subsequent developments in this field several problems beyond the reach of classic compass-straight edge methods have fallen to origami. We had seen above that the heptagon can be folded. Below we shall illustrate two of the classic problems of the old yavana-s, namely doubling the cube (the Delian problem) and trisecting any angle.

**Doubling the cube**\
This involves folding  $\sqrt\lbrack 3\rbrack {2}$  which was first achieved by Margherita Beloch 1936 CE. We first illustrate how this can be done and the proof of it using elementary geometry.

![origami_doubling_cube](https://manasataramgini.files.wordpress.com/2016/11/origami_doubling_cube.png){width="75%"}*Figure 7: Doubling the cube*

In the below protocol fold and unfold so as to create creases. The first process involves dividing the square into 3 equal rectangles:

 1.  Fold along diagonal  $\overline{f}$ 

 2.  Bisect the square by folding along  $\overline{g}$  by bringing D to A and C to B.

 3.  Fold along  $\overline{h}$ , which is the diagonal of the rectangle AEFB that was formed by the previous crease.

 4.  Fold through point G where creases  $f$  and  $h$  intersect creating crease  $j$  parallel to the sides of the square.

 5.  Fold along the segment where  $\overline{AB}$  lies as a result of the above fold to create crease  $a\'$ . This trisects the square into 3 equal rectangles.

One can see that if corner A is at origin then  $\overline{f}$  has equation  $y=-x+l$  where  $l$  is the side of the square. Similarly  $\overline{h}$  has equation  $y=\dfrac{1}{2}x$ . Their intersection would hence be  $\left (\dfrac{2}{3},\dfrac{1}{3} \right)$ . This is the rationale for the trisection of the square.

Next we double the cube:

 1.  Fold the paper such that the end  $I$  of the first trisecting crease  $j$  comes to lie on the second trisecting crease  $a\'$  created above. This is point  $I\'$ .

 2.  The corner  $B$  of the paper should lie along edge  $AD$  so that it marks point  $B_2$  on it.

 3.  Then point  $B_2$  partitions  $\overline{AD}$  in the ratio  $1:\sqrt\lbrack 3\rbrack {2}$ .

 4.  Once we have the starting square's side partitioned in the ratio  $1:\sqrt\lbrack 3\rbrack {2}$  we can then fold two squares with sides equal to each partition. Each square is then separated and folded into a cube using the Japanese waterbomb method. This will result in cubes whose volumes are in the ratio  $1:2$ .

![doubling_cube](https://manasataramgini.files.wordpress.com/2016/11/doubling_cube.jpg){width="75%"}*Figure 8: Cubes doubled*

We need to show that  $\dfrac{\overline{DB_2}}{\overline{AB_2}}=\sqrt\lbrack 3\rbrack {2}$ . Hence for ease of calculation we take  $\overline{DB_2}=x$  and  $\overline{AB_2}=1$ . Then we have:  $\overline{A\'B_2}=x-\left(\dfrac{x+1}{3}\right)=\dfrac{2x-1}{3}$ ;  $\overline{I\'B_2}=\dfrac{x+1}{3}$ .\
In  $\triangle AJB2$  let  $\overline{AJ}=y$ .  $\overline{JB2}=x+1-y$ . Thus, by the भुजा-कोटि-कर्ण-न्याय we have:  $1+y^2=(x+1-y)^2 \; \therefore y=\dfrac{x^2+2x}{2x+2}$ .

We can see that  $\triangle AJB_2 \sim \triangle A\'I\'B_2$ .\
Thus,  $\overline{AJ} \equiv \overline{A\'B_2}$  and  $\overline{I\'B_2} \equiv \overline{JB_2}$ .

 $$\therefore \dfrac{\dfrac{2x-1}{3}}{\dfrac{x+1}{3}}=\dfrac{\dfrac{x^2+2x}{2x+2}}{\dfrac{(2x+2)(x+1)-(x^2+2x)}{2x+2}}\lbrack 10pt\rbrack \dfrac{2x-1}{x+1}=\dfrac{x^2+2x}{x^2+2x+2}\lbrack 10pt\rbrack (2x-1)(x^2+2x+2)=x^3+3x^2+2x\\ 2x^3+3x^2+2x-2=x^3+3x^2+2x\\ x^3=2 \; \therefore x=\sqrt\lbrack 3\rbrack {2}$$ 

**Trisecting any angle**

![origami_angle_trisection](https://manasataramgini.files.wordpress.com/2016/11/origami_angle_trisection.png){width="75%"}*Figure 9: trisection of any angle*

This classical yavana construction can be achieved by folding using the above diagram. It is detailed in the below video.

[](https://player.vimeo.com/video/192993873)

Play

![Vimeo](https://f.vimeocdn.com/p/images/crawler_logo.png){width="75%"}

[trisection movie](https://vimeo.com/192993873) from [मानसतरंगिणी](https://vimeo.com/user56539721) on [Vimeo](https://vimeo.com).

