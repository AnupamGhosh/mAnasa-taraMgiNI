
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some meanderings among golden stuff-2](https://manasataramgini.wordpress.com/2016/10/04/some-meanderings-among-golden-stuff-2/){rel="bookmark"} {#some-meanderings-among-golden-stuff-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 4, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/10/04/some-meanderings-among-golden-stuff-2/ "5:57 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Related stuff:\
[Golden Ratio-0](https://manasataramgini.wordpress.com/2016/08/17/a-golden-construction/)\
[Golden Ratio-1](https://manasataramgini.wordpress.com/2016/08/23/some-meanderings-among-golden-stuff/)

If the golden ratio can fascinate erudite men of high IQ then what to say of simpletons like us. Hence, we shall here talk about some more trivia in this regard. The golden ratio is associated with a sequence, the method for generating which was provided in our tradition first by पिङ्गल (the Meru-prastha) and was spelt out in full by विरहाङ्क in his वृत्त-जाति-samuccaya. Hence, we may call it the Meru-श्रेधी (  $M$  with elements  $M(n)$ ;  $n= -\infty ...-2,-1,0,1,2,3... \infty$ ; among the mleccha-s it is commonly known as Fibonacci's sequence).\
 $\lim_{n\to\infty} \dfrac{M(n+1)}{M(n)}=\phi \approx 1.61803398875$ , which provides the relationship to the golden ratio.

![golden_function](https://manasataramgini.files.wordpress.com/2016/10/golden_function.png){width="75%"}Figure 1

In modern parlance this sequence can be seen as integer values emerging from the function (Figure 1):

 $$y=\dfrac{1}{2\phi-1}\left(\phi\^x-\dfrac{\cos(\pi x)}{\phi\^x}\right)$$ 

This function oscillates with ever-increasing amplitude for negative  $x$ , hits  $0$  at  $x=0$ , remains stable between  $x=1..2$ , then almost linearly climbs to  $2$  at  $x=3$  and then explodes nearly exponentially.

The sequence can be obtained from this function thusly:\
 $M(n)=\lfloor y \rfloor$  for  $x=n=-\infty...-2,-1,0,1,2,3...\infty$ .

 $$M \rightarrow ...34,-21, 13,-8,5,-3,2,-1,1,0,1,1,2,3,5,8,13,21,34...$$ 

Thus having obtained the sequence, a well-known property of it, apparently discovered by Kepler, becomes apparent:\
 $M(n)^2=M(n-1) \times M(n+1)- (-1)\^n$ .

![golden_section_rectangle](https://manasataramgini.files.wordpress.com/2016/10/golden_section_rectangle.png){width="75%"}Figure 2

Now this relationship might be linked to an interesting geometrical procedure of dissection of a square which can performed only using the golden ratio (Figure 2). The procedure goes thus:

 1.  Let there be a square of side  $a$ .

 2.  The square is first partitioned into two rectangles one of sides  $a$  and  $\dfrac{a}{x}$  and other of sides  $a$  and  $a-\dfrac{a}{x}$ .

 3.  Partition the first rectangle into two congruent right-angled trapezia with  $base=\dfrac{a}{x}, height=\dfrac{a}{x}, top=a-\dfrac{a}{x}$ . Partition the second rectangle along its diagonal into two right triangles with  $base=a, height=a-\dfrac{a}{x}$ .

 4.  Rearrange the the four pieces of the square thus obtained such that they reconstitute a rectangle of equal area as the starting square. This is done by taking two opposites sides of the rectangle to be the sides of the two trapezia  $\dfrac{a}{x}$  and the other two opposite sides made by laying the shorter edge of the right triangle against the top of the trapezium. Thus, this side is  $a+\dfrac{a}{x}$ .

 5.  Thus, we have:

 $$a^2=\dfrac{a}{x}\left (a+\dfrac{a}{x}\right)\lbrack 10 pt\rbrack \therefore 1= \dfrac{1}{x}+\dfrac{1}{x^2}\\ \lbrack 10 pt\rbrack \therefore x^2-x-1=0$$ 

The positive root of above is  $x=\phi$ . Thus, only by using the golden ratio can we convert a square into a rectangle as described above. For any other ratio you will have a parallelogram area in the middle which is either in excess or less than the area of the reconstituted rectangle.

![golden_near_square](https://manasataramgini.files.wordpress.com/2016/10/golden_near_square.png){width="75%"}Figure 3

Now instead of the golden sectioning of the square let us use a square of side  $M(n)$  and divide it as above using  $M(n-1)$  and  $M(n-2)$  to make the four pieces (Figure 3; e.g.  $8, 5, 3$ ). If we then reconstitute them as a rectangle we will get its sides as  $M(n-1)$  and  $M(n+1)$  (e.g.  $5, 13$ ). Then by the above relationship regarding these elements of series  $M$  this reconstituted figure will have a long thin parallelogram of excess or insufficient area relative to the reconstituted rectangle by 1 square unit. Thus, if we make a cardboard cutout of the above one could "hide away" that parallelogram worth of area difference along a roughly cut diagonal of the reconstituted rectangle. This can produce to paradox of square of side  $8$  yielding a rectangle of sides  $5,13$ . Many years ago we had seen precisely such a "puzzle", may be made of wood or cardboard. We must confess that then it took a little while before we realized that it stemmed from the golden dissection of the square. We recently learned via Mario Livio that this puzzle was invented by a guy called Loyd.

