
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Apollonian parabola](https://manasataramgini.wordpress.com/2016/09/25/the-apollonian-parabola/){rel="bookmark"} {#the-apollonian-parabola .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 25, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/09/25/the-apollonian-parabola/ "6:39 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Some say that Archimedes and Apollonius of Perga (modern Murtina in Turkey; the center of the great yavana temple of the goddess Artemis in the days of Apollonius) were the two great yavana-s who might have rivaled Karl Gauss or Leonhard Euler in their in intellectual achievements. Beyond doubt a whole lot of modern knowledge rests on the foundation of Apollonius and in the west one could perhaps say that a Kepler or a Newton may have never shone forth had that Apollonian foundation not existed. Indeed, even today the study of Apollonius is a profound experience for the man who can do so. As with yavana religion much of their knowledge including the books of Apollonius were lost due to the ravages of प्रेतोन्माद in the west. Not surprisingly even at a later age the Lord Protector of England the duke of Somerset was burning books with yavana geometrical diagrams at the Oxford university.

Ironically in the world of its younger sister, the मरून्माद, Apollonius held a great fascination among those influenced by (Neo)-Platonism. Among the मरून्मत्त-s, their intellectual ibn Sina from Persia was perhaps the most fascinated by Apollonius. This allowed the preservation of much more of his works than in the west. Not surprisingly later मरून्मत्त-s referring to his work said that while his Qoranic studies were fine, he should be accused of kaffr-hood, and "innovation" derived from yavana and Hindu knowledge. Indeed, some mullahs suggested that he should have been cut pieces and feed to raptors. Nevertheless, his studies on yavana geometry and Hindu trigonometry has resulted in some interesting preservations which would have otherwise been lost. One such we shall discuss here is an Apollonian construction of a parabola using the geometric mean theorem. It goes thus in modern terminology:

![appollonian_parabola](https://manasataramgini.files.wordpress.com/2016/09/appollonian_parabola1.png){width="75%"}


 1.  Let point O be the origin. Take a point A on the x-axis at a given negative coordinate  $(-a,0)$ .

 2.  Let B be a point moving on the x-axis from the origin in the positive direction.

 3.  Draw a circle with diameter as  $\overline{AB}$ .

 4.  Draw the tangent to this circle at point B. It will be perpendicular to the x-axis.

 5.  The above circle cuts the y-axis at points C and D. Draw lines through these points which are parallel to the x-axis.

 6.  The above pair of lines will intersect the tangent to the circle at point B at points E and F.

 7.  The locus of points E and F as B moves along the x-axis is the desired parabola.

From the construction we see:\
 $\overline{OA}=a$ ; The x-coordinate of the parabola is  $\overline{OB}=x$ ; The y-coordinate of the parabola is  $\overline{OC}=\overline{OD}=y$ . Given the circle used for the construction we notice that  $\overline{OC}=\overline{OD}$  is the geometric mean of  $\overline{OA}$  and  $\overline{OB}$  because  $\overline{AB}$  is the diameter of the circle (geometric mean theorem). Thus we get:

 $$y=\sqrt{ax}$$ 

 $$\therefore y^2=ax$$ 

which is the equation of our desired parabola.

