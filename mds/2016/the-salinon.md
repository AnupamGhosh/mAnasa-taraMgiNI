
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The salinon](https://manasataramgini.wordpress.com/2016/06/14/the-salinon/){rel="bookmark"} {#the-salinon .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 14, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/06/14/the-salinon/ "6:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The yavana Archimedes or some later commentator of his among the Neo-Platonists of Harran described a figure they called the *salinon*, which was supposed to mean a "salt-cellar". This material was acquired by the Mohammedans from those Neo-Platonists from whom it has in turn become known to us. Evidently yavana-s had such a vessel from which they perhaps sprinkled their लवण. This figure went hand in hand with that other remarkable figure the arbelos, which we have spoken of before on these pages. It was constructed thus:

 1.  Draw a downward semicircle with radius  $r_1$ .

 2.  On either side of it draw two upward semicircles each with radius  $r_2$ .

 3.  Then enclose all three semicircles with an upward semicircle. The complete close figure formed by these four semi-circles is the salinon (colored brownish-red in the figure).

![salinon](https://manasataramgini.files.wordpress.com/2016/06/salinon.jpg){width="75%"}

The yavana-s showed that the area of the salinon is the same a the circle which has its diameter stretching from the topmost to the bottommost points of the salinon. This is of course rather obvious but seemed rather interesting to us. The area of the salinon can be calculated by summing the areas of the two axial semicircles and subtracting that of the area of the lateral semicircles:

 $$A_s=\pi\left(\dfrac{1}{2}r_1^2+\dfrac{1}{2}\left(r_1+2r_2\right)^2-r_2^2\right)=\pi\left(r_1^2+2r_1r_2+r_2^2\right)=\pi\left(r_1+r_2\right)^2\\$$ 

Now from the construction it is clear that the circle in blue will have radius *r* which is the average of the two axial circles:

 $$r= \dfrac{r_1+2r_2+r_1}{2}=r_1+r_2\lbrack 5pt\rbrack \therefore A_c=\pi\left(r_1+r_2\right)^2=A_s$$ 

To us the salinon and the arbelos look like axe-heads.

