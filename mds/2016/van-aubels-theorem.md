
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [van Aubel's theorem](https://manasataramgini.wordpress.com/2016/06/12/van-aubels-theorem/){rel="bookmark"} {#van-aubels-theorem .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 12, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/06/12/van-aubels-theorem/ "7:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The van Aubel's theorem is a simple theorem which is comparable to the theorem attributed to the French conqueror [Napoleon Bonaparte](https://manasataramgini.wordpress.com/2014/12/28/some-trivia-on-equilateral-triangles-and-the-like-2/) regarding triangles. It is easy to prove once you know the उपाय, even as the yogin-s would say आनन्द is easily achieved once you know the right उपाय. The उपाय here is the kind of vector algebra problem one used to see on ones college entrance exam. One may ask why we need to present such trivialities here. There are certain things that fascinate you when you are young. When you grow old you think about those often with a nostalgia and you think of re-experiencing it. For us this happens with certain elementary explorations in geometry among various other things and we just feel the urge to put it out there.

![vanAubel](https://manasataramgini.files.wordpress.com/2016/06/vanaubel.jpg){width="75%"}

A dynamic version: [http://www.geogebra.org/m/Sण्S९तक़्६](http://www.geogebra.org/m/Sण्S९तक़्६){rel="nofollow"}

As for the theorem itself draw any quadrilateral ABCD. On each of the sides AB, BC, CB, DA erect a square. The segments joining the centers of the opposite squares are equal in length and at right angles to each other irrespective of what form the quadrilateral might take. Now the quadrilateral can be convex or concave; it can be simple or complex (self-intersecting); yet, this theorem holds good. To us this seemed like a philosophical analogy: it seemed to be some kind of conservation law (constancy of the angle of intersection and for each case equivalence of the length of the two inter-centric segments) that was written into the structure of space.

The proof goes thus:

 1.  The quadrilateral ABCD can be represented as a complex vector system which originates at point A. Thus,  $\overrightarrow{AB}+\overrightarrow{BC}+\overrightarrow{CD}+\overrightarrow{DA}=0$ .

 2.  Now the centers of the squares erected on sides AB, BC, CB, DA are respectively points P, Q, R, S which allows us to define the vectors  $\overrightarrow{AP}$ ,  $\overrightarrow{AQ}$ ,  $\overrightarrow{AR}$ ,  $\overrightarrow{AS}$ .

 3.  From the construction it is clear that to reach point P we travel half way along  $\overrightarrow{AB}$  and then take a turn at at right angle and travel the distance. This means we reach point P by rotating vector  $\dfrac{1}{2}\overrightarrow{AB}$  by  $\dfrac{\pi}{2}$  radians. Since we are using the complex plane we can show this as:

 $$e^{\frac{\pi}{2}i}.\dfrac{1}{2}\overrightarrow{AB}=\left(\cos\left(\dfrac{\pi}{2}\right)+i\sin\left(\dfrac{\pi}{2}\right)\right).\dfrac{1}{2}\overrightarrow{AB}=\dfrac{1}{2}i\overrightarrow{AB}\lbrack 7pt\rbrack \therefore \overrightarrow{AP}=\dfrac{1}{2}\overrightarrow{AB}+\dfrac{1}{2}i\overrightarrow{AB}$$ 

Similarly we can now write:

 $$\overrightarrow{AQ}=\overrightarrow{AB}+\dfrac{1}{2}\overrightarrow{BC}+\dfrac{1}{2}i\overrightarrow{BC}\lbrack 5pt\rbrack \overrightarrow{AR}=\overrightarrow{AB}+\overrightarrow{BC}+\dfrac{1}{2}\overrightarrow{CD}+\dfrac{1}{2}i\overrightarrow{CD}\lbrack 5pt\rbrack \overrightarrow{AS}=\overrightarrow{AB}+\overrightarrow{BC}+\overrightarrow{CD}+\dfrac{1}{2}\overrightarrow{DA}+\dfrac{1}{2}i\overrightarrow{DA}$$ 

From the construction it is clear that:

 $$\overrightarrow{AP}+\overrightarrow{PR}=\overrightarrow{AR}\lbrack 5pt\rbrack \therefore \overrightarrow{PR}=\overrightarrow{AR}-\overrightarrow{AP}$$ 

Substituting from above for  $\overrightarrow{AR}$  and  $\overrightarrow{AP}$  we get:

 $$\overrightarrow{PR}=\dfrac{1}{2}\overrightarrow{AB}+\overrightarrow{BC}+\dfrac{1}{2}\overrightarrow{CD}+\dfrac{1}{2}i\overrightarrow{CD}-\dfrac{1}{2}i\overrightarrow{AB}$$ 

Similarly

 $$\overrightarrow{AQ}+\overrightarrow{QS}=\overrightarrow{AS}\lbrack 5pt\rbrack \therefore \overrightarrow{QS}=\overrightarrow{AS}-\overrightarrow{AQ}=\dfrac{1}{2}\overrightarrow{BC}+\overrightarrow{CD}+\dfrac{1}{2}\overrightarrow{DA}+\dfrac{1}{2}i\overrightarrow{DA}-\dfrac{1}{2}i\overrightarrow{BC}$$ 

Multiplying  $\overrightarrow{QS}$  by  $i$  we get:

 $$i\overrightarrow{QS}=\dfrac{1}{2}i\overrightarrow{BC}+i\overrightarrow{CD}+\dfrac{1}{2}i\overrightarrow{DA}-\dfrac{1}{2}\overrightarrow{DA}+\dfrac{1}{2}\overrightarrow{BC}\lbrack 5pt\rbrack \therefore \overrightarrow{PR}-i\overrightarrow{QS}=\dfrac{1}{2}\overrightarrow{AB}+\overrightarrow{BC}+\dfrac{1}{2}\overrightarrow{CD}+\dfrac{1}{2}i\overrightarrow{CD}-\dfrac{1}{2}i\overrightarrow{AB}-\dfrac{1}{2}i\overrightarrow{BC}-i\overrightarrow{CD}\\ -\dfrac{1}{2}i\overrightarrow{DA}+\dfrac{1}{2}\overrightarrow{DA}-\dfrac{1}{2}\overrightarrow{BC}\lbrack 5pt\rbrack =\dfrac{1}{2}\left(\overrightarrow{AB}+\overrightarrow{BC}+\overrightarrow{CD}+\overrightarrow{DA}\right)-\dfrac{1}{2}i\left(\overrightarrow{AB}+\overrightarrow{BC}+\overrightarrow{CD}+\overrightarrow{DA}\right)=0\lbrack 5pt\rbrack \therefore \overrightarrow{PR}=i\overrightarrow{QS}$$ 

This means vectors  $\overrightarrow{PR}$  and  $\overrightarrow{QS}$  are equal in magnitude and at right angles to each other.

