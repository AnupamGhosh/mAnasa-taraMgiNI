
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The first three squareable Lunes](https://manasataramgini.wordpress.com/2016/06/25/the-first-three-squareable-lunes/){rel="bookmark"} {#the-first-three-squareable-lunes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 25, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/06/25/the-first-three-squareable-lunes/ "7:12 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

For the sake of some readers we shall first define a lune: A lune is a concave closed region bounded by two circular arcs respectively with radii  $r_1$  and  $r_2$  and distance between their centers as  $d$ , where  $r_1 \geq r_2$ . This region looks like a digit of the moon (hence lune or what Hindus call the इन्दुकला) or the illuminated fraction of a star or planetary body eclipsed by another. The formula for the area of the lune is complicated but has a nice symmetry to it:

 $$A_l=r_2^2 \cos^{-1}\left(\dfrac{r_1^2-r_2^2-d^2}{2dr_2}\right)-r_1^2\cos^{-1}\left(\dfrac{r_1^2-r_2^2+d^2}{2dr_1}\right)+\lbrack 5pt\rbrack \dfrac{1}{2}\sqrt{\left(d+r_1+r_2\right)\left(d-r_1+r_2\right)\left(d+r_1-r_2\right)\left(-d+r_1+r_2\right)}$$ 

One notices from the formula that the first two terms with inverse cosine functions can give rise to transcendental numbers, like fractions of  $\pi$ . Due to the subtraction at certain angles these can potentially cancel each other out leaving the 3rd term which is in principle geometrically constructable. This suggests that the quadrature of at least some lunes should be possible in principle.

At some point during our school years, shortly after acquiring some elementary geometrical insights, we learned of this problem of the squaring of lunes -- we quickly performed for ourselves the construction of the lunes specified by a rectangle and  $\dfrac{1}{4}$ th square lune and proved their quadrature (simple problems). A few years later we successfully conquered the trapezium lune and proved its quadrature for ourselves as part of a study of some Hindu mathematics. The third squareable lune is conceptually similar but its conventional compass and straight-edge construction needs one to construct a segment of length  $\dfrac{\sqrt{11}+\sqrt{3}}{2}$ . While relatively easy, this was practically cumbersome given the usual paper-size we used those days for our physical compass/ruler constructions. It was then that we read of Proclus mentioning yavana-s using a "cheat" for this purpose. But a comparable "cheat" is rather natural to Hindus due their geometry being dependent on the रश्मि; hence we use here.

The remaining two squareable lunes were apparently first proved to be so by Euler (though it is also attributed to some others). To construct them exactly with a compass and straight-edge would need one to construct lengths which are expressed as ratios of grisly multiple nested radicals and we have never attempted to do so nor figured out a way using other curves. Likewise, the deep reasons for the very existence of only these five and no more squareable lunes remained beyond us. Baffled by all this any fancies we might have had of pursuing mathematics as a mode of living came to an end. It showed us that there are kinds of insight and understanding pertaining to mathematical expression that are not easy or even possible for a human with a limited mental capacity to acquire as a part of his regular course of existence. Aristotle informs us regarding the yavana Hippocrates, who achieved the initial lune quadratures, that he was so immersed in his geometry that he was pretty much lost in his regular life: a stark reminder of the blurring of the boundaries between mathematical virtuosity and dysfunctionality. This was something we could appreciate despite lacking that virtuosity.

Yet, what we did learn regarding the lunes was rather fascinating and reminded us of experiments with other curves whose area becomes independent of  $\pi$ , like a type of lemniscate we described earlier. The construction of the classic  $\dfrac{1}{4}$ th square lune is part of some yantra-s such as the भूतविजय and the Hindu re-appropriation of मञ्जुघोष. We have also seen an intriguing figure termed the शृष्टि-yantra contemplated upon by साधक-s in the school of the kaula practitioner हंसमिट्ठु from Gujarat, which includes the first two and possibly the third squareable lune. We have not seen any textual precedence for this. We place here the construction of the first three squareable lunes.

**The four lunes specified by a rectangle and the  $\dfrac{1}{4}$ th square lune**

![lune_tetrad](https://manasataramgini.files.wordpress.com/2016/06/lune_tetrad.png){width="75%"}\
Figure 1


 1.  Draw a rectangle ABCD inside a circle (Figure 1). Every rectangle by definition is a cyclic quadrilateral; hence it will be circumscribed in a circle.

 2.  Draw semicircles on each side of the rectangle. Four lunes are defined by the region between the circumcircle of ABCD and the four semicircles on its sides (Figure 1).

 3.  The sum of the area of the 4 lunes is equal to that of the rectangle ABCD used to define them.

 4.  While we could use the above area formula to prove this, it is visually much easier to use the construction: let  $\overline{AB}=a$ ;  $\overline{BC}=b$ ; let radius of the circumcircle of the rectangle ABCD be  $r$ .

 5.  The area a given lune ( $A_l$ ) in the construction can be visualized as the difference of the area of the semicircle with diameter as one of the sides of the rectangle ABCD ( $A_s$ ) and the area of the circular segment or cap ( $A_{cp}$ ) of the circumcircle of rectangle ABCD delimited by the cord which is one of the sides of the said rectangle. Thus  $A_l=A_s-A_{cp}$ .

 6.  For the 4 lunes we get two pairs of semicircles, each pair corresponding to one of the pairs of congruent sides of the rectangle. Thus, the total area ( $A_{ts}$ ) bounded by the four semicircles forming the 4 lunes is the sum of the areas of the circles having the two adjacent sides of the rectangle as their diameters:  $A_{ts}=\pi \left(\left(\dfrac{a}{2}\right)^2+\left(\dfrac{b}{2}\right)^2\right)$ .

 7.  Now a right angle inscribed in a circle defines a semicircle. Therefore the diagonals of the rectangle are diameters of its circumcircle;  $\overline{AC}=\overline{BD}=2r$ . By the भुजा-कोटि-कर्ण-न्याय  $a^2+b^2=4r^2$ 

8\)  $\therefore A_{ts}=\pi \left(\dfrac{a^2+b^2}{4}\right)=\pi r^2$ 

 9.  The total area of the four caps of the circumcircle ( $A_{tcp}$ ) defined by the rectangle ABCD can be seen from the construction as being:  $A_{tcp}=A(circumcircle)-A(rectangle ABCD)=\pi r^2-ab$ .

 10.  Thus we can get the total area of the four lunes ( $A_{tl}$ ) to complete the proof:  $A_{tl}=A_{ts}-A_{tcp}=\pi r^2-(\pi r^2-ab)=ab$ 

![lune1by4](https://manasataramgini.files.wordpress.com/2016/06/lune1by4.png){width="75%"}\
Figure 2

From the above construction (Figure 1) it is also clear that for any right triangle inscribed in a circle the two lunes formed by the circumcircle and semicircles on the two orthogonal sides of the triangle have a total area equal to that of the right triangle. A special case of the above construction leads to the first squareable single lune (Figure 2): If the two adjacent sides of the rectangle ABCD were equal then it becomes a square (Figure 2). Then the 4 lunes will have equal area and the area of each of these lunes will  $\dfrac{1}{4}$  that of square (from Figure 2):

 $$\therefore A_l=\dfrac{1}{4}A(ABCD)=A(\triangle OBC)$$ 

As can be seen from Figure 2 this lune can be easily constructed by the intersection of the circumcircle of square ABCD and semicircle erected on the hypotenuse of the quarter square  $\triangle OBC$ .

**The squareable trapezium lune**

![lune_trapezium](https://manasataramgini.files.wordpress.com/2016/06/lune_trapezium.png){width="75%"}\
Figure 3


 1.  Take point A at origin (Figure 3). Draw a point  $Y_1$  at distance 1 unit above it along the y-axis. Draw point  $Y_2$  at distance 3 units below it along the y-axis.

 2.  Obtain the midpoint of  $Y_1$  and  $Y_2$  (the arithmetic mean operation), namely  $Y_3$ . Draw a semicircle with  $\overline{Y_1Y_2}$  as diameter and  $Y_3$  as center. Draw a perpendicular to  $\overline{Y_1Y_2}$  at point A to cut the semicircle at point B. By geometric mean theorem  $\overline{AB}=\sqrt{3}$ 

 3.  Bisect  $\overline{AB}$  to obtain point P. With P as center and radius .5 cut arcs on either side on  $\overline{AB}$  to obtain points Q and R. draw perpendiculars to  $\overline{AB}$  at points Q and R.

 4.  With points A and B as centers and radius 1 cut the above perpendiculars drawn at points Q and R to obtain points C and D. Join points A, B, C, D to obtain isoceles trapezium ABCD with sides  $\overline{AB}=\sqrt{3}, \overline{BC}=\overline{CD}=\overline{DA}=1$ .

 5.  By definition every isoceles trapezium is a cyclic quadrilateral. Obtain its circumcenter by drawing perpendicular bisectors any two sides and finding their point of concurrency, point E. With point E as center draw the circumcircle of ABCD. This will be the smaller circle bounding the desired lune.

 6.  Draw a line perpendicular to  $\overline{BC}$  at point B. With radius  $\overline{BE}$ , i.e. same as circumcircle of ABCD, and center as B cut the the above perpendicular line to obtain point F (thus this perpendicular line with henceforth be  $\overleftrightarrow{BF}$ );  $\therefore \overline{BF}=\overline{BE}$ . Join points C and F to obtain  $\overleftrightarrow{CF}$ . Thus,  $\triangle CBF$  is a right triangle. Extend  $\overline{BC}$  as  $\overleftrightarrow{BC}$ . With B as center and radius  $\overline{AB}=\sqrt{3}$  cut  $\overleftrightarrow{BC}$  to get point H. Through point H draw a line parallel to  $\overleftrightarrow{CF}$  to cut  $\overleftrightarrow{BF}$  at point I.

 7.  Using  $\overline{AB}$  as the base and radius  $\overline{BI}$  construct isoceles  $\triangle ABJ$ . With point J as center draw a circle passing through points A and B. This will be the larger circle of the lune; thus we now have the second squareable lune (Figure 3).

Here again rather than use the area formula we can use the construction and some simpler formulae to establish its quadrature. From the construction (Figure 3) we can establish the following:

 1.  Area of isoceles trapezium ABCD is  $A(ABCD)$ .

 2.  The area of cap of the bigger circle on side AB is ( $A_{cp1}$ )

 3.  The three caps ( $A_{cp2}$ ) of the smaller circle on sides BC, CD and DA are equal because by construction they subtend congruent angles.

 4.  The area of the lune is:  $A(ABCD)+3A_{cp2}-A_{cp1}$ 

 5.  The area of a cap of a circle with radius  $r$ , where the cord defining the cap subtends an angle  $\theta$  at the center is  $\dfrac{r^2}{2}\left(\theta-\sin \left(\theta \right)\right)$ .

 $$\therefore A_l=A(ABCD)+3\dfrac{r_2^2}{2}\left(\theta_2-\sin \left(\theta_2 \right)\right)-\dfrac{r_1^2}{2}\left(\theta_1-\sin \left(\theta_1 \right)\right)$$ 

 6.  From our construction  $\triangle BEC \cong \triangle CED \cong \triangle DEA \sim \triangle AJB$ . Thus  $\angle AJB = \theta_1 \cong \angle BEC \cong \angle CED \cong \angle DEA =\theta_2$ . We call this angle  $\theta$ . Likewise, given that  $\overline{BF} \cong \overline{BE}$  we have  $\dfrac{\overline{BI}}{\overline{BE}}=\dfrac{r_1}{r_2}=\sqrt{3}$ 

7\)  $\therefore A_l=A(ABCD)+3\dfrac{r_2^2}{2}\left(\theta-\sin \left(\theta \right)\right)-\left(\sqrt{3} \right)^2\dfrac{r_2^2}{2}\left(\theta-\sin \left(\theta \right)\right)\\ =A(ABCD)$ \
Thus, the area of our lune is the same as the isoceles trapezium used in its construction.

Now what is the area of this trapezium. Since it is a cyclic quadrilateral, we can use Brahmagupta's theorem to find its area:

 $$A(ABCD)=\lbrack 5pt\rbrack \dfrac{1}{4}\sqrt{(-a+b+c+d)(a-b+c+d)(a+b-c+d)(a+b+c-d)}\lbrack 5pt\rbrack a=\sqrt{3};\; b=c=d=1\lbrack 5pt\rbrack \therefore A(ABCD)=A_l=\dfrac{(1+\sqrt{3})^{3/2}\sqrt{3-\sqrt{3}}}{4} \approx 1.27$$ 

Interestingly, the third term in the area of the lune formula provided in the beginning of the article has resemblance to the Brahmagupta formula. As the two 'curved' terms in the area formula cancel out the third term essentially becomes equal to the area of of the trapezium.

Now we can get the radius of the smaller circle  $r_2$  of the lune because it is the circumcircle of trapezium ABCD using परमेश्वर's theorem:

 $$r_2=\dfrac{\sqrt{(ab+cd)(ac+bd)(ad+bc)}}{4A(ABCD)}\lbrack 5pt\rbrack =\dfrac{(1+\sqrt{3})^{3/2}}{4} \times \dfrac{4}{(1+\sqrt{3})^{3/2}\sqrt{3-\sqrt{3}}}=\dfrac{1}{\sqrt{3-\sqrt{3}}}$$ 

Now the angle subtended by a cord of length  $a$  at the center of a circle with radius  $r$  is:

 $$\theta= 2\sin^{-1}\left(\dfrac{a}{2r}\right)$$ 

Taking cord  $\overline{BC}$  and radius  $r_2$  which has been calculated above we get:

 $$\theta=2\sin^{-1}\left(\dfrac{\sqrt{3-\sqrt{3}}}{2}\right)\approx 1.1960618940\^c \approx 68.52929856756\^o$$ 

From the construction this is the inner angle  $\angle AJB$  of the lune. Thus, the outer angle of the lune  $\angle AEB=3\times \angle AJB \approx 205.587895702680\^o$ 

**The squareable trapezium-triangle difference lune**

![lune3_concave](https://manasataramgini.files.wordpress.com/2016/06/lune3_concave.png){width="75%"}\
Figure 4


 1.  Take a point A and draw point  $Y_1$  1 unit above it along the y-axis (Figure 4). Draw point  $Y_2$  2 units below it along the y-axis. Find their midpoint  $Y_3$  (arithmetic mean operation) and draw a semicircle with  $\overline{Y_1Y_2}$  as its diameter. Draw a perpendicular at point A to  $\overleftrightarrow{Y_1Y_2}$  which cuts the semicircle at point B. Thus by the geometric mean theorem  $\overline{AB}=\sqrt{2}$ .

 2.  Draw the perpendicular bisector of  $\overline{AB}$ , which we will call  $\overleftrightarrow{g}$ . Here is where the special trick comes in which we shall describe in terms of using a rajju and शङ्कु. Mark off a length of  $\sqrt{3}$  from a शङ्कु tied on the rajju. This length can be easily constructed as described above using the geometric mean theorem. Move that शङ्कु along  $\overleftrightarrow{g}$  keeping the opposite end of the rajju to one on which the  $\sqrt{3}$  has been marked taut. When it passes through point A, peg the rajju to point A and also to  $\overleftrightarrow{g}$ . This point on  $\overleftrightarrow{g}$  is D.

 3.  Using the marked end of the rajju draw a circle of radius  $\sqrt{3}$  with point D as center. With point B as center and radius  $\overline{AB}$  draw an arc to cut this circle. Thus we get point C.

 4.  Reflect point C about  $\overleftrightarrow{g}$  to get point E. Thus we get an isoceles trapezium ABCE. Draw the perpendicular bisectors of two of the sides of ABCE and obtain their point of concurrency J, the circumcenter of ABCE. With J as center draw the circumcircle of ABCE which will be the outer arc of the desired lune.

 5.  Similarly, find point K the circumcenter of  $\triangle CDE$  and draw its circumcircle. This gives us the inner arc thereby completing the construction of the desired lune.

From the construction (Figure 4) we can reconstitute the area of the lune thus:

 $$A_l=A(ABCE)+A_{cp}(AE)+A_{cp}(AB)+A_{cp}(BC)\\ -(A(CDE)+A_{cp}(CD)+A_{cp}(DE))$$ 

Now from the construction (Figure 4) we can see that:

 $$\triangle ABJ \cong \triangle BCJ \cong \triangle AEJ \sim \triangle CDK \cong \triangle DEK \\ \therefore A_{cp}(AE)=A_{cp}(AB)=A_{cp}(BC)$$ 

and

 $$A_{cp}(CD)=A_{cp}(DE)$$ 

If the length of the cord defining the cap is a and the angle subtended by it is  $\theta$  then we get the area of the cap to be:

 $$A_{cp}=\dfrac{a^2}{8}\dfrac{(\theta-\sin(\theta))}{\sin^2(\theta/2)}\lbrack 5pt\rbrack \therefore A_l=A(ABCE) + 3 \times \dfrac{(\sqrt{2})^2}{8}\dfrac{(\theta-\sin(\theta))}{\sin^2(\theta/2)} -A(CDE) \\ - 2 \times \dfrac{(\sqrt{3})^2}{8}\dfrac{(\theta-\sin(\theta))}{\sin^2(\theta/2)} =A(ABCE)-A(CDE)$$ 

Now from the construction it clear that:  $A(ABCE)-A(CDE)=A(ABCDE)$ , which is the area of the concave pentagon ABCDE.

To find the actual area of this polygon and lune we need to do some more geometry. From the construction it is apparent that:

 $$\triangle ABC \sim \triangle ABD \therefore \dfrac{\overline{AB}}{\overline{AC}}=\dfrac{\overline{AD}}{\overline{BC}}$$ 

Let  $\overline{AD}=x \; \therefore \overline{AC}=\sqrt{3}+x\lbrack 5pt\rbrack \therefore \dfrac{\sqrt{2}}{\sqrt{3}+x}=\dfrac{x}{\sqrt{2}} \; \therefore x^2+\sqrt{3}x-2=0 \; \; \therefore x = \dfrac{\sqrt{11}-\sqrt{3}}{2}$ \
Thus  $\overline{AD}=\dfrac{\sqrt{11}-\sqrt{3}}{2}; \;\; \overline{AC}=\dfrac{\sqrt{11}+\sqrt{3}}{2}$ 

Again from the construction we get:

 $$\dfrac{\overline{AB}}{\overline{CE}}=\dfrac{\overline{AD}}{\overline{CD}} \;\; \therefore \dfrac{\sqrt{2}}{\overline{CE}}=\dfrac{\sqrt{11}-\sqrt{3}}{2\sqrt{3}}\;\; \therefore \overline{CE}= \dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}} \approx 3.091$$ 

Now that we have every side of the trapezium ABCE and we can calculate its area using Brahmagupta's theorem:

 $$a=b=c=\sqrt{2}; \; d=\dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}}\lbrack 10pt\rbrack \therefore A(ABCE)=\dfrac{1}{4}\left(\sqrt{2}+\dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}}\right)^{3/2}\sqrt{3\sqrt{2}-\dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}}}\lbrack 10pt\rbrack =\dfrac{1}{8}\sqrt{\dfrac{423+73\sqrt{33}}{2}}$$ 

Similarly for  $\triangle$  CDE we can use Brahmagupta's theorem thus:

 $$a=b=\sqrt{3}; \; c=\dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}}\lbrack 10pt\rbrack \therefore A(CDE)=\dfrac{1}{4}\left(\dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}}\right) \sqrt{2\sqrt{3}+\dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}}}\sqrt{2\sqrt{3}-\dfrac{2\sqrt{6}}{\sqrt{11}-\sqrt{3}}}\\ =\dfrac{3}{8}\sqrt{\dfrac{15+\sqrt{33}}{2}}$$ 

The difference of the above areas is the area of the concave pentagon ABCDE, which is the same as that of the lune, and shows the lune to be squareable. Thus:

 $$A_l= A(ABCDE)=\dfrac{1}{2}\sqrt{\dfrac{9+\sqrt{33}}{2}} \approx 1.3576$$ 

One can also do the tedious calculation of the circumcircles using परमेश्वर's theorem to get the outer angle of the lune as:  $161.12\^o$  and the inner angle as  $107.72\^o$ . These lunes briefly teased the ancients with the hope of squaring the circle but it was merely a mirage.

