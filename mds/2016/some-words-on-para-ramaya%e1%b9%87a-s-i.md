
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some words on "para-रामायाण-s"-I](https://manasataramgini.wordpress.com/2016/08/12/some-words-on-para-ramaya%e1%b9%87a-s-i/){rel="bookmark"} {#some-words-on-para-रमयण-s-i .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 12, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/08/12/some-words-on-para-ramaya%e1%b9%87a-s-i/ "7:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I use the hybrid Greek-Sanskrit word para-रामायाण for all राम-कथा-s other than that of वाल्मीकि. These span a great diversity of literature going back to relatively early times in Indo-Aryan tradition. Lots of people have said a lot of things on this matter and we are hardly going to review any of that here. The रामायण itself is enigmatic in some ways. There is no allusion to the main characters of the epic in the Vedic corpus just like the central पाण्डु heroes of the महाभारत. Yet the late Vedic tradition belonging to the पैप्पलाद branch of the Atharvaveda remembers king हिरण्यनाभ कौशल्य a descendant of रामचन्द्र ऐक्ष्वाकव. On the other side a remote dynastic predecessor of राम emperor Trasadasyu is one of the greatest heroes of the ऋग्वेद. Another mighty dynastic predecessor who is remembered in the ऋग्वेद is emperor Bhajeratha ऐक्ष्वाकव who is known by his more widely used name as भगीरथ from the time of the Samaveda ब्राह्मण-s. Another great hero and dynastic predecessor of राम who is remembered in the RV is emperor मन्धातृ. The founder of this dynastic throne इक्ष्वाकु appears as a राजर्षि in the RV. However, of the later occupants of the famed इक्ष्वाकु throne we know little from Vedic literature, which often talks of the occupants of the thrones of the Kuru and the पञ्चाल. This, we suspect, reflects the dynamics of the invasions and the subsequent conquest of the sub-continent by the Indo-Aryans warriors coming in from their ultimate homeland in the steppes -- इक्षवाकु group branched away from the पञ्चजन group and moved eastwards without major participation in the later Vedic consolidation of the श्रौत ritual. However, their hero रामचन्द्र was to have a major impact of Hindu tradition from his para-Vedic sphere.

The first of the para-रामायण-s is the रामोपाख्यान of मार्कण्डेय from the महाभरत. Many have suggested that this mini-रामायण was the precursor of वाल्मीकि's text. We however feel this is rather incorrect. The रामोपाख्यान has several text fragments in common with वाल्मीकि while being just over 700 verses in length in its most basic form reconstructed by Sukthankar. Thus, we posit that it simply represents a para-रामायण tradition that might have branched off by contraction from an earlier "Ur-वाल्मीकि-रामायण from which the extant वाल्मीकि text also evolved subsequently as part of the epic system (by epic system I mean the phase when the two national epics were similarly handled by a common set of transmitters, probably सूत-s, as evidenced by a certain similarity in phrase usages between them). It also appears that the रामायण itself was orally transmitted longer than the रामोपाख्यान itself. The रामोपाख्यान has feel of of being a "refresher" for people who knew the general रामायण story and we suspect this prior knowledge was due to the existence of an earlier वाल्मीकि text that was widely known at that time. It is important to note that the महाभारत repeatedly uses रामायण as a source of metaphors and allusions but not vice versa. Hence, there is no reason to give into a white indological assertion that the रामायण is a post-महाभारत tradition. However, we do suspect that the old वाल्मीकि that was around at the time of the महाभारत's old composition was distinct from the current version.

The रामोपाख्यान has several distinct features from वाल्मीकि-s version; hence, we suspect that it had already branched off and preserved as a separate tradition again providing indirect evidence for the antiquity of the original वाल्मीकि. Its notable features includes:

  - The god त्वष्टृ is said to have made सीता specially as a wife for राम. This is a reflection of the Vaidika[त्वष्टा रूपाणि पिंशतु; त्वष्टा रूपेव तक्ष्या; त्वष्टा रूपाणि हि प्रभुः...]{style="color:#0000ff;"}

  - No mention of any पुत्रकामेष्टि for the birth of the four इक्ष्वाकु princes.

  - विश्रवस् has 3 wives: पुष्पोत्कटा who bears रावण and कुम्भकर्ण; मालिनी who bears विभिषण; राका bears Khara and शूर्पन्खा as twins.

  - While well-versed in the श्रुति, with the exception of विभिषण, they were paradoxically anti-ब्राह्मण.

  - विभिषण joined the यक्षराट् Kubera who made him the lord of the रक्षस्-es who were under him (they are alluded to in the Vaidika ritual to Kubera specified in the तैत्तिरीय-श्रुति )\
-The remaining पिशाच-s and man-eating रक्षस्-es elected रावण as their overlord. He is said to have 1.4*10^8 पिशाच-s and 2.8*10^8 रक्षस् under his command.

  - ब्रह्मा sent the gandharva woman Dundubhi to be born as the hunchback to set the रामायण in motion.

  - रावण meets मारीच at गोकर्ण, a Rudra-तीर्थ to recruit him for his cause.

  - रामा pursuing the golden deer is described with the simile of him looking like Rudra pursuing the stellar deer with his bow.

  - राम kills वालिन् with a huge bow with a mechanical device (yantra; a cross-bow?).

  - सीता informs the ape हनूमत् of an old respected रक्षस् known as Avindhya who had told her of the emperor of the apes and his councillers.

  - No indications that विभीषण was in लन्का with रावण. He simply arrives with his four ministers and joins hands with राम against his brothers.

  - Many apes suggest crossing the sea by means of boats but राम goes for the causeway strategy arguing that they did not possess sufficient boats for landing a huge army in लन्का.

  - राम places his ape and bear force in the midst of a forest in लन्का so that they would have ample supplies.

  - A simile of अञ्गद smashing four रक्षस्-es like a tiger in conflict with four hawks.

  - The bears under their king जाम्बवन्त् are explicitly described as being sloth bears.

  - Several imp-like पिषाच-s launched the first wave of attack named: Parvana, पूतन, Jambha, Khara, क्रोधवश, Hari, Praruja, Aruja and Praghasa who were invisible to the apes and bears. But विभीषण broke their invisibility and they were killed by the वानर-s.

  - रावण is said to have arrayed his troops in the battle formation invented by the great भार्गव of yore उशनस् काव्य. राम arrayed his troops using the method of the god बृहस्पति.

  - विभीषण kills Prahasta (in VR the great ape नील kills him). Battle between धूम्राक्ष and हनूमत् is described as being like that between Indra and प्रह्लाद.

  - कुम्भकर्ण leads the assault accompanied by the brothers of दूषण named Pramathin and Vajravega. He captures सुग्रीव. लक्ष्मण pursues him and after a fierce fight kill him with ब्रह्मास्त्र. Then हनूमत् and Nala kill the other two रक्षस्-es.

  - After the fall of कुम्भकर्ण, मेगन्नाद enters the field and after a fierce fight became invisible in the sky and struck down राम and लक्ष्मण with his missiles. Then he tied them with his magical arrow net. विभीषण having successfully accomplished victory against the रक्षस् on another front came to the place and seeing the fallen heroes revived them by using the प्रज्ञास्त्र. Then सुग्रीव treated them using the विशल्य herb and mantra-s. The coming of गरुड is not found in the original रामोपाख्यान. It was only inserted into some recensions -- it is clearly an interpolation without basis because the preceding text of the रामोपाख्यान does not talk of the नागपाश.

  - Then विभिषण informed राम that Kubera has sent a guhyaka with magic water that will allow them to see invisible objects. Upon applying the water to their eyes the इक्ष्वाकु brothers and the apes and bears were able to see the invisible मेघनाद. Under the guidance of विभीषण, लक्ष्मण then attacked मेघनाद and after a fierce fight fired three आग्नेय weapons that respectively cut his bow-wielding hand, the other hand which was holding a naraca missile and finally his head.

  - रावण then wanted to kill सीता but Avindhya persuaded him from doing so and urged him to go and fight राम directly. After a some fierce fighting with the army of सुग्रीव, रावण started emitting numerous warriors from his body who resembled राम and लक्ष्मण. राम saw through this illusion and killed the impostor राम-s while directing लक्ष्मण to kill those who looked like him.

  - Then Indra sent down his own chariot with his charioteer मातलि. राम this time thought it to be an illusion of रावण and did not want to board the car. However, विभ्शण informed him that it was no illusion and really Indra's chariot.

  - In course of the great battle that followed the वानर-s scattered in terror from the fury of रावण but राम finally deploying the mighty ब्रह्मास्त्र "turned रावण to plasma" for the lack of a better usage. The Sanskrit goes thus:

[स तेन राक्षस-श्रेष्ठः सरथः साश्व-सारथिः ।]{style="color:#0000ff;"}\
[प्रजज्वाल महाज्वालेनाग्निनाभि-परिष्कृतः ॥]{style="color:#0000ff;"}

He the lord the राक्षस-s with his car, horse and charioteer were set ablaze by \[the brahma missile] and surrounded with a great fiery conflagration.

[ततः प्रहृष्टास् त्रिदशाः सगन्धर्वाः सचारणाः ।]{style="color:#0000ff;"}\
[निहतं रावणं दृष्ट्वा रामेणाक्लिष्ट-कर्मणा ॥]{style="color:#0000ff;"}

Thus, seeing रावण slain by राम of unperturbed deeds, the gods with the gandharva-s and चारण-s were gladdened.

[तत्यजुस् तं महाभागं पञ्च भूतानि रावणम् ।]{style="color:#0000ff;"}\
[भ्रंशितः सर्वलोकेषु स हि ब्रह्मास्त्रतेजसा ॥]{style="color:#0000ff;"}

By the energy of the brahma missile the five elements abandoned him [रावण] of great opulence and he was deprived of all the worlds.

[शरीर-धातवो ह्य् अस्य मांसं रुधिरम् एव च ।]{style="color:#0000ff;"}\
[नेशुर् ब्रह्मास्त्र-निर्दग्धा न च भस्माप्य् अदृश्यत ॥]{style="color:#0000ff;"}

All his bodily substances, indeed his flesh and blood, were burnt by the brahma missile such that not even ash was seen.

  - राम then conferred लन्का on विभीषण. He along with Avindhya came out bringing सीता.

  - राम then tells सीता that she is free and may go wherever she would like. He says that he has done his duty of freeing her but irrespective of whether she was chaste or not he was not going to consort with her again.

  - The gods then arrived in person and सीता swore by the elements constituting her body that she was pure. This was confirmed by the gods वायु, Agni and वरुण. Then the god ब्रह्मा explained to राम that सीता had been protected from being raped upon abduction by रावण due the curse of नलकूबर the son of Kubera.

  - Then the ghost of दशरथ appeared and congratulating राम asks him to return to अयोध्या with सीता. There is no अग्निपरीक्ष of सीता in the original रामोपाख्यान. It has been inserted into a regional variant and is clearly an insertion for it simply does not fit with the rest of the coherent narrative of this part.

  - राम then bows to all the deva-s and Kubera and confers boons on the demon Avindhya and the demoness त्रिजटा for being good to सीता.

  - The gods offered राम boons: He asks for firm adherence to dharma, invincibility in battle and restoration of the lives of the apes killed in the battle. They also offered the yellow-eyed ape हनूमत् the boon of life as long as the रामायण is known, and access to divine food and drink.

  - राम and his retinue then crossed the ocean by the same bridge by which the came to लन्का and on reaching the other shore he rewarded the apes richly for their services.

  - The with the पुष्पक air-ship he returned to Kishkindha with just सुग्रीव and विभीषण to show it to सीता and install अङ्गद as the crown-prince.

  - Upon being united with Bharata, राम was crowned the king by वामदेव and वसिष्ठ. After which he respectfully returned the पुष्पक airship to Kubera.

In conclusion, the उपाख्यान clearly represents a distinct tradition but generally recapitulates all the key elements of the extant VR. Notably, in the this version विभीषण is a major positive figure -- he does not appear a like traitor, having joined Kubera early in his life he plays a major role in the war. लक्ष्मण-s profile is also higher in this version. No special role is allotted to हनूमत् such as bringing the medicinal mountain from the हिमालय or in the war to the exclusion of the rest. Yet, he is recognized as a key figure for his reconnaissance leap to लन्का.


