
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [An astronomical interpretation of the अनड्वान् सूक्त](https://manasataramgini.wordpress.com/2016/05/29/an-astronomical-interpretation-of-the-ana%e1%b8%8dvan-sukta/){rel="bookmark"} {#an-astronomical-interpretation-of-the-अनडवन-सकत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 29, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/05/29/an-astronomical-interpretation-of-the-ana%e1%b8%8dvan-sukta/ "6:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This may be seen as a continuation of this note: [Anatomy and heavens in the boomorphic universe.](https://manasataramgini.wordpress.com/2013/11/08/anatomy-and-heavens-in-the-boomorphic-universe/)

The अनड्वान् सूक्त is an enigmatic सूक्त from the Atharvaveda which falls in the same class as other सूक्त-s which describe a "boomorphic" world -- a very natural motif for the cattle-rearing Indo-European peoples. In the vulgate text it is 4.11 whereas in the पैप्पलाद संहिता it appears as 3.25. The सूक्त-s in the two संहिता-s are similar but the order of mantra-s is changed and there are few further पाठ-bheda-s in the individual mantras, which however do not greatly alter the meaning. The exact ritual in which this सूक्त was originally deployed remains a bit unclear. From the internal references it is clear that the rite was of 12 nights and was related to cattle. From the internal description it might be related to the rite known as the शबलीहोम described in the सामवेद tradition and might have been the Atharvan equivalent thereof (although with some unique developments). The Samavedic ritual (पञ्चविंश ब्राह्मण 21.3) goes thus:

  - The ritualist shaves his hair and beard and wears a new woven garment.

  - He keeps the ritual fire burning in his house.

  - He goes with a friend to a hill and for 12 nights lies there on the ground on an uncovered spot.

  - While there he only drinks hot milk (probably equivalent to gharma).

  - For these 12 nights he must avoid talking as far as possible and not ejaculate his semen.

  - As the 12th night is drawing to a close with the शबली mantra he makes an offering in the fire of honey and curds. The शबली mantra goes thus:

[शबलि समुद्रो 'सि विश्वव्यचा ब्रह्म देवानां प्रथमजा ऋतस्यान्नम् असि शुक्रम् असि तेजो 'स्य् अम्रृतम् असि तां त्वा विद्म शबलि दीद्यानां तस्यास्ते पृथिवी पादो 'न्तरिक्षं पादो द्यौः पादः समुद्रः पाद एषासि शबलि तां त्वा विद्म सा न इषम् ऊर्जं धुक्ष्व वसोर्धारां शबलि प्रजानां शचिष्ठा व्रतम् अनुगेषं स्वाहा ॥]{style="color: #99cc00"}

O शबली you are the all-expansive ocean, the brahman of the gods, the first-born of the ऋत; you are food, you are brightness, you are splendor, you are immortality. We know you O शबली as the shining one. Her one foot is the earth, her one foot is the atmosphere, her one foot is the sky, her one foot is the ocean. You are swift O शबली. We know you. Milk out for us strength, good, the stream of wealth O शबली. May I among my people attain the mightiest realm.

  - He then goes to a forested region away from the village where he can hear none of the domesticated animals.

  - He takes up a bunch of darbha grass and yells three times: "शबली". If he hears an animal call, other than that of a dog or an ass, as though answering back then his ritual will be successful.

  - If it does not work he repeats its two more times, each time after an year from the original performance. If he does not hear any animal by his 3rd attempt or in any attempt hears a dog or an ass then his ritual will not succeed.

The following parallels might be cited as links between the शबली ritual and the अनड्वान् सूक्त:

 1.  The twelve night duration of the rite.

 2.  The gharma (tapta) is extolled in the सूक्त and is also the hot milk drink of the observer of the शबली-vrata.

 3.  The gharma is said to be four-footed just as the शबली.

 4.  The honeyed offering and the कीलाल.

 5.  The पञ्चविंश ब्राह्मण 21.3 states that the शबली is a milk-giving bovine, just as the bovine extolled in this सूक्त. Both are said to yield strength on milking.

 6.  The observer practices austerities in the शबली vrata, which is parallel to the austerities alluded to in our सूक्त (tapas and श्रम).

 7.  The use of the form गेषं/गेष्म. In the जैमिनीय version the ritualist attains an आवृतम्. Thus the end objective of this rite might imply a loka.

Regardless of this inference, in the classical tradition of the Atharvan-s the सूक्त itself is specified for one of a series of bovine sava rituals. The particular sava for which this recitation is used is known as अनड्वाह sava. While these Atharvan sava-s are distinctive they might have had elements in common with the old Gosava. Some discussion on this aspect might be found in the writings of a gentleman who has been an interlocutor on Twitter: [https://aryanthought.wordpress.com/](https://aryanthought.wordpress.com/){rel="nofollow"}

Additionally, this सूक्त has some astronomical significance comparable to the other boomorphic constructs seen in the Atharvaveda, [which we had discussed before](https://manasataramgini.wordpress.com/2013/11/08/anatomy-and-heavens-in-the-boomorphic-universe/). This is what we intend to discuss here. Here we must mention that the above-stated gentleman who was in touch with us on Twitter arrived at the possibility of astronomical implications for the allusions in this सूक्त independently of us. This astronomical material offers a clear date for when such observations might have been made. However, like many other such Vedic astronomical allusions it also raises an important conflict between Vedic philology and archaeology, which barring a small number of Hindus, few even recognize:

The heart of the paradox is that the astronomical dates, which can be recovered with considerable confidence and consistency from the oldest Vedic layers available to us, i.e. parts of the ऋग्वेद and the Atharvaveda, are entirely inconsistent with what archaeology tells us about key markers of Indo-Iranian culture: the oldest dates emerging from the Vedic corpus are considerably (500-1000 years) earlier than the first attested remains of spoked-wheel chariots -- that notable marker of Indo-Iranians. A related issue is that the oldest dates mean that the Indo-Aryans or their precursors at the time of those dates were likely on the Eurasian steppes not India. Thus, the incorporation of mantra-s into rituals developed in the Indic phase of their history was necessarily much later their original conception.

Among the white indologists of the Abrahamosphere and their imitators this paradox is not recognized because they simply refuse to acknowledge the existence of precisely dateable astronomical references in the Veda. In its most extreme form they would take recourse to the "Hindus as idiots" hypothesis by means of which they would argue that early Indo-Aryans were simply incapable of any accurate astronomical observations along the lines of their Afro-Asiatic counterparts from West Asia. Among the Hindus, especially those who adhere to untenable time lines or the equally untenable "out of India theory", this paradox has likewise been mostly brushed aside because: 1) either archaeological anchors do not matter at all; e.g. they are completely comfortable with a महाभारत or रामायण in the neolithic, leave alone the early Vedic corpus; 2) or they seek to situate their archaeology in early Harappan India and again explain away the lack of any archaeological correlates.

While, we had recognized this paradox for decades, we were earlier less concerned for there was always some wriggle-room due to the incompleteness of the archaeological record, just like the fossil record. However, in the past 2 years the genomic archaeology of steppe cultures of Eurasia is tightening the constraints greatly and making the wriggle-room smaller. Nevertheless, our intention here is not to delve at any depth on this paradox -- we are simply acknowledging that it exists, that we lack a completely clear explanation and that we will hopefully have a clearer picture of how to resolve it in the next 5-10 years. In particular, it will concern the key question of whether the Sintashta-Andronovo continuum represents the Indo-Iranians enroute to their final lands in outer Asia or whether the branch that came to India was distinct from those archaeological cultures.

But before we go into the astronomical motifs we shall begin with a relative straight-forward translation of the सूक्त.

[अनड्वान् दाधार पृथिवीम् उत द्याम् अनड्वान् दाधारोर्व् अन्तरिक्षम् ।]{style="color: #0000ff"}\
[अनड्वान् दाधार प्रदिशः षड् उर्वीर् अनड्वान् विश्वं भुवनम् आ विवेश ॥१॥]{style="color: #0000ff"}

The bovine holds up the earth and heaven, the bovine holds up the wide atmosphere. The bovine holds up the six (four + zenith and nadir) wide directions, the bovine has entered the entire universe.

[अनड्वान् इन्द्रः स पशुभ्यो वि चष्टे त्रयां छक्रो वि मिमीते अध्वनः ।]{style="color: #0000ff"}\
[भूतं भविष्यद् भुवना दुहानः सर्वा देवानाम् चरति व्रतानि ॥२॥]{style="color: #0000ff"}

The bovine is Indra, he looks out for cattle, शक्र measures out the triple circles (or circular paths); extracting (literally milking out) the past, the future and the present, he undertakes all the laws of the gods. (त्रयां chakro instead of त्रयाञ् chakro is a संधि shared with पैप्पलाद)

[इन्द्रो जातो मनुष्येष्व् अन्तर् घर्मस् तप्तश् चरति शोशुचानः ।]{style="color: #0000ff"}\
[सुप्रजाः सन्त् स उद् आरे न सर्षद् यो नाश्नीयाद् अनडुहो विजानन् ॥३॥]{style="color: #0000ff"}

Indra-born inside men, the heated fluid, he moves around shining brightly. He being of the good people (praja, as in the people rather than his children), indeed not creeping into the cavity, who knowing \[this] shall not eat the bovine.

[अनड्वान् दुहे सुकृतस्य लोक ऐनं प्याययति पवमानः पुरस्तात् ।]{style="color: #0000ff"}\
[पर्जन्यो धारा मरुत ऊधो अस्य यज्ञः पयो दक्षिणा दोहो अस्य ॥४॥]{style="color: #0000ff"}

The bovine gives milk (note the peculiar present tense form 'duhe' of गण-2 verb 'duh') in the world of the performers of meritorious deeds. पवमान (Soma) causes it to swell with milk from the front. Parjanya is the stream, the Marut-s the udder, the ritual is its milk, the ritual gift is the milking.

[यस्य नेशे यज्ञपतिर् न यज्ञो नास्य दातेशे न प्रतिग्रहीता ।]{style="color: #0000ff"}\
[यो विश्वजिद् विश्वभृद् विश्वकर्मा घर्मं नो ब्रूत यतमश् चतुष्पात् ॥५॥]{style="color: #0000ff"}

Of whom neither the lord of the ritual, nor the ritual are master, nor the donor nor the receiver are master; who is all-winning, all-bearing \[and] all the actions -- tell us which is the four-footed gharma (hot milk; this is brahmodya question which can be related to the "answer" found in the शबली incantation).

[येन देवाः स्वर् आरुरुहुर् हित्वा शरीरम् अमृतस्य नाभिम् ।]{style="color: #0000ff"}\
[तेन गेष्म सुकृतस्य लोकं घर्मस्य व्रतेन तपसा यशस्यवः ॥६॥]{style="color: #0000ff"}

By which the gods ascended to heaven; having left the body, to the nave of immortality, may we by that go (गेष्म: a Vedic form of जेष्म= jayema) to the world of the meritorious, by the ritual of the gharma (hot milk)) and austerity, desiring glory.

[इन्द्रो रूपेणाग्निर् वहेन प्रजापतिः परमेष्ठी विराट् ।]{style="color: #0000ff"}\
[विश्वानरे अक्रमत वैश्वानरे अक्रमतानदुह्य् अक्रमत ।]{style="color: #0000ff"}\
[सो 'दृंहयत सो 'धारयत ॥७॥]{style="color: #0000ff"}

Of the form of Indra, with Agni in between the head and the hump(of the bovine), \[who is] प्रजापति, परमेष्ठिन् \[and] विराज्. In विश्वानर he strode, in वैश्वानर he strode, in the bovine he strode. He stabilized, he bore \[it] up.

[मध्यम् एतद् अनडुहो यत्रैष वह आहितः ।]{style="color: #0000ff"}\
[एतावद् अस्य प्राचीनं यावान् प्रत्यङ् समाहितः ॥८॥]{style="color: #0000ff"}

The midpoint of this bovine, where the yoke is placed -- as much of it is placed in east of it as much as is west of it.

[यो वेदानदुहो दोहान् सप्तानुपदस्वतः ।]{style="color: #0000ff"}\
[प्रजां च लोकं चाप्नोति तथा सप्तऋषयो विदुः ॥९॥]{style="color: #0000ff"}

Whoever knows the milkings of the bovine, the seven which are inexhaustible, he attains progeny and the world thereafter -- the seven sages know this.

[पद्भिः सेदिम् अवक्रामन्न् इरां जङ्घाभिर् उत्खिदन् ।]{style="color: #0000ff"}\
[स्रमेणानड्वान् कीलालं कीनाशश् चाभि गछतः ॥१०॥]{style="color: #0000ff"}

Trampling down exhaustion with the feet, drawing refreshment by the thighs, the tiller and the bovine by toil go unto the sweet drink.

[द्वादश वा एता रात्रीर् व्रत्या आहुः प्रजापतेः ।]{style="color: #0000ff"}\
[ tatropa brahma yo veda tad वा अनडुहो vratam ||11||]{style="color: #0000ff"}

They say: "twelve indeed are these nights of प्रजापति's rite". Whoever knows the brahman in them -- that indeed is the bovine rite.

[दुहे सायं दुहे प्रातर् दुहे मध्यंदिनं परि ।]{style="color: #0000ff"}\
[दोहा ये अस्य संयन्ति तान् विद्मानुपदस्वतः ॥१२॥]{style="color: #0000ff"}

It gives milk in the evening, it gives milk in the morning, it gives milk around the midday; its milkings which come together; those which are inexhaustible we know.

In passing, we may also compare this सूक्त with another enigmatic ऋक् from the ऋग्वेद which contains a यजुष् mantra within it:\
[स्वावृग् देवस्यामृतं यदी गोर्]{style="color: #0000ff"}\
[अतो जातासो धारयन्त उर्वी ।]{style="color: #0000ff"}\
[ विश्वे देवा anu tat te yajur gur]{style="color: #0000ff"}\
[दुहे यद् एनी दिव्यं घृतं वाः ॥]{style="color: #0000ff"} RV 10.12.3

The god (Agni)'s own possession (literally: what he has taken), the ambrosia which comes from the cow; "Those born thereof uphold the two wide ones \[heaven and earth]";\
this यजुष् of yours all the gods go along with, when the variegated cow milks out heavenly ghee and water.

The astronomical allegory in the सूक्त under discussion is not obvious but reveals itself once we trace the homologization of the earthly bovine with the heavenly one. For a more detailed workout of the identification of the heavenly bovine with the constellation of Taurus one may refer to [this earlier note](https://manasataramgini.wordpress.com/2013/11/08/anatomy-and-heavens-in-the-boomorphic-universe/) as we shall not be repeating much of what has been said there here. Indeed, this identification of the bovine and the constellation of Taurus is of great antiquity as suggested by the depiction of the Pleiades (M45) and the bull together in the famous Lascaux paintings (\~17,300-15,000 years before present (YBP); e.g. see M Rappenglück's work on the same).![Lascaux_Taurus](https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg){.wp-image-8166 .aligncenter attachment-id="8166" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"0\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="Lascaux_Taurus" large-file="https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg" medium-file="https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg" orig-file="https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg" orig-size="538,600" permalink="https://manasataramgini.wordpress.com/2016/05/29/an-astronomical-interpretation-of-the-ana%e1%b8%8dvan-sukta/lascaux_taurus/" height="348" sizes="(max-width: 312px) 100vw, 312px" srcset="https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg 312w, https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg 135w, https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg 269w, https://manasataramgini.files.wordpress.com/2016/05/lascaux_taurus.jpg 538w" width="312"}

The following points are notable:

 1.  The first mantra, which states that the bovine is the prop of the world, potentially links the bovine to the constellation of Taurus, which has been allegorically termed the same in other Vedic references.


 2.  This is further strengthened by the identification of the bovine with प्रजापति whose constellation is explicitly identified in Vedic and post-Vedic tradition as Taurus. The bovine is also identified as Indra. Indra's identification with the constellation of Taurus seems to have been parallel to that of प्रजापति's and is encountered in the RV thus:

[अयं विदच् चित्र-दृशीकम् अर्णः]{style="color: #0000ff"}\
[शुक्र-सद्मनाम् उषसाम् अनीके ।]{style="color: #0000ff"}\
[अयम् महान् महता स्कम्भनेनोद्]{style="color: #0000ff"}\
[द्याम् अस्तभ्नाद् वृषभो मरुत्वान् ॥]{style="color: #0000ff"} RV 6.47.5

This wave[-like one] found \[that] with appearance of Spica in the forefront of the dawns with their brilliant seats; this mighty one, with a mighty pillar propped up heaven, the bull with the Marut-s.

Here Indra is explicitly identified with the bull as he usually is. However, his being identified as the bull which is the prop of heaven (i.e. the skambha), and an allusion to Citra (Spica) rising before the dawns (i.e. a star on the opposite side of the ecliptic) suggests that as in our सूक्त Indra here is identified with Taurus.


 3.  Further support for this conjecture comes from the explicit mention in our सूक्त of Soma पवमान filling the bovine with milk from the front. Now, tradition records the constellation of Soma as being that of मृगशिरस्, i.e. the part of Orion "in front" of Taurus. Thus, this allusion is entirely consistent with the identification of the bovine as Taurus.


 4.  The सूक्त states that the bovine measures out the three adhvan-s, i.e. the triple circles of the sky coordinates -- the encoding of these in several myths talking of the "measuring out" of the sky was pointed out by von Dechend and de Santillana. These are the ecliptic, the longitudinal and the latitudinal coordinates. Since the bovine is said to measure them out it is a likely allusion to Taurus being at the vernal equinox.


 5.  This contention becomes much firmer as the सूक्त clearly states that bovine is associated with a mid-point where there is as much to the east as to the west -- a clear allusion to its position on the ecliptic.


 6.  The bovine bears Agni on at place where the yoke is placed. [As we have seen before](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/) (also see von Dechend and de Santillana) the equinoctial colure is associated with Agni (see also this discussion: [[The crossing of अश्मन्वती](https://manasataramgini.wordpress.com/2006/11/26/the-crossing-of-ashmanvati/)]{style="color: #99cc00"}). Indeed, the सूक्त explicitly states that the midpoint is where the yoke is placed (vaha) i.e. between the head and the hump. Now, the constellation of Taurus is widely understood as having the Hyades cluster as the bovine head with the orange star α Tauri as the eye of the bovine. Thus, the vaha alluded to in this सूक्त would mean that the equinoctial colure was situated in the interval east beyond γ Tauri and west of λ Tauri. This location of the equinoctial colure is most compatible with a period \~4850-4650 YBP.

![Taurus1](https://manasataramgini.files.wordpress.com/2016/05/taurus1.jpg){width="75%"}

This dating is of considerable significance because it relates to other observations that have suggested the presence of the equinoctial colure in the constellation of Taurus during the Vedic age. Indeed, a position close to the कृत्तिका-s (Pleiades) has been argued from number Vedic sources since Bal Gangadhar Tilak. We have suggested such a possibility base on the identification of [शकधूम with the Pleiades](https://manasataramgini.wordpress.com/2015/02/15/sakadhuma-possible-parallels-in-a-meteorological-tradition-from-india-and-south-america/). This approximately corresponds to period around 4400-4200 YBP. But the position in this सूक्त seems to be of a slightly earlier epoch. Indeed, it is temporally close to the epoch proposed by the astronomer SB Dikshit and Sanskritist Kameshvara Aiyar when the Pleiades were close to the celestial equator based on their interpretation of ब्राह्मन statements from the Yajurvedic corpus (e.g.,[ एता ha vai प्राच्यै दिशो na cyavante |]{style="color: #0000ff"} ) This supports the idea that it was not an isolated observation but went along with other observations from a comparable period. Notably, around the same time as our सूक्त, the opposite asterism of the bull would have been ज्येष्ठा (α Scorpii), which corresponded with the autumnal equinox. This might explain why this asterism was chosen as that of Indra -- when the sun was in Taurus, originally associated with Indra, ज्येष्ठा would have been seen in the opposite side and likewise in autumn the bull would appear rising as though propping up the heaven. Perhaps, with the bull being granted to प्रजापति by the प्राजापत्य-s the opposite constellation was left with Indra.


 7.  This सूक्त also appears to allude to the knowledge of precession of the equinoctial point into the constellation of the bovine. It was previously argued (also see de Santillana and von Dechend) [the previous Agni-s or stations of Agni are allusions to the prior positions of the equinoctial point](https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/). Indeed, such stations are alluded here as विश्वानर and वैश्वानर which precede the bovine station (mantra 7). Striding through these Indra or Agni are said to evidently stabilize and bear up the heavens.


 8.  Finally, this mantra might contain yet another relevant astronomical encoding. The seven sages are alluded to along with the associated seven infallible milkings. Elsewhere in the Atharaveda we hear of the seven sages sitting on the heavenly ladle with aperture cross ways (i.e. the constellation of the Big Dipper; AV-vulgate 10.8.9). At the proposed epoch of our सूक्त the head of the Big Dipper would have pointed towards the equinoctial point. This perhaps is the implication of the seven sages knowing the \[secret of the] rite.

![Taurus2](https://manasataramgini.files.wordpress.com/2016/05/taurus2.jpg){width="75%"}

**Further reading:**\
<https://manasataramgini.wordpress.com/2006/11/21/the-path-of-fire/>\
<https://manasataramgini.wordpress.com/2006/11/26/the-crossing-of-ashmanvati/>\
<https://manasataramgini.wordpress.com/2013/11/08/anatomy-and-heavens-in-the-boomorphic-universe/>\
<https://manasataramgini.wordpress.com/2015/02/15/sakadhuma-possible-parallels-in-a-meteorological-tradition-from-india-and-south-america/>


