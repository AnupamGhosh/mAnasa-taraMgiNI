
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion: discussion on वायव्य offerings](https://manasataramgini.wordpress.com/2016/12/11/matters-of-religion-discussion-on-vayavya-offerings/){rel="bookmark"} {#matters-of-religion-discussion-on-वयवय-offerings .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 11, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/12/11/matters-of-religion-discussion-on-vayavya-offerings/ "2:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Somakhya and Lootika had revived the private one day rite in the manner of their ancestors as had been ordained by the youthful sage शुनःशेप आजिगर्ति. They were resting at the corner of their fire-room in the interval between the sessions after having offered honey at a pipal tree to Indra, वायु and the Marut-s. Lootika wiped the sooty particles off her glasses and donning them again read out the mantra-text they had used in the offerings that had been made in the morning: "Dear Somakhya, thus is the recitation for the offering to the great god वायु in prince Parucchepa's characteristic अत्यष्टि chandas:

[तुभ्यम् उषासः शुचयः परावति भद्रा वस्त्रा]{style="color:#0000ff;"}\
[तन्वते दंसु रश्मिषु चित्रा नव्येषु रश्मिषु ।]{style="color:#0000ff;"}\
[तुभ्यं धेनुः सबर्दुघा विश्वा वसूनि दोहते ।]{style="color:#0000ff;"}\
[अजनयो मरुतो वक्षणाभ्यो दिव आ वक्षणाभ्यः ॥]{style="color:#0000ff;"} RV 1.134.4

For you, the radiant उषा-s, the auspicious clothing in the distant realm\
stretch-out, into the \[celestial] houses, into bright rays, into the new rays.\
For you the cow, yielding milk, milks out all wealth,\
You generated the Maruts from the womb-center, from womb-center of heaven.

In the late sections of the श्रुति वायु is placed together with the Marut-s (i.e. Rudra-s) in the same atmospheric domain by प्रजापति: '[उपादधाद् वायुं रुद्रास् तान् अन्तरिक्षे]{style="color:#0000ff;"}(in शतपत ब्राह्मण 6.1.2.10)'. However, this once again brings to mind how वाल्मीकि closely follows the Veda by fitting the earthly heroes of his इतिहास to the daiva-prototype of the Veda -- something I have paid careful attention to ever since you brought it to my attention when we were kids. What struck me here is the राजर्षि's allusion to the Marut-s being generated by वायु in the heavenly womb! Here, वायु is taking the usual role of the god Rudra even as he does in the रामायण in the engendering of मारुति. I believe this heavenly womb is none other than that of पृष्णि. The cognate in वाल्मीकि's text is अञ्जना, who in the key narrative on the birth of Hanumat can be seen as none other than पृष्णि in an earthly garb. वाल्मीकि says:

[अचरत् पर्वतस्याग्रे प्रावृड्-अम्बुद-संनिभे ।]{style="color:#0000ff;"}\
[विचित्र-माल्याभरणा महार्ह-क्षौमवासिनी ॥]{style="color:#0000ff;"} R 4.65.11

She [अञ्जना] wandered on the mountain top appearing like a rain-bearing cloud,\
She was adorned with beautiful garlands and clothed in expensive silk.

This otherwise peculiar simile of the rain cloud makes total sense when viewed in light of the goddess पृश्णि, the mother of the Marut-s in the Veda, manifesting as a rain cloud. Indeed, the very name अञ्जना should be taken as stemming from the sense of a dark rain-bearing cloud, like a manifestation of the mother of the Marut-s.

Somakhya: "Beloved, that is a good observation indeed. The link between the Marut-s and Hanumat is rather obvious. But the reader of the इतिहास would notice the extent to which this sambandha goes, including in connection to the common link shared between Skanda, the son of Rudra in the recombined Indo-Iranian tradition, and the Maruts. Note this narrative, O bright-eyed गौतमी, from the uttara-काण्ड regarding the birth of the mighty ape:

[शालिशूक-समाभासं प्रासूतेमं तदाञ्जना ।]{style="color:#0000ff;"}\
[फलान्य् आहर्तु-कामा वै निष्क्रान्ता गहने वरा ॥]{style="color:#0000ff;"}

अञ्जना then gave birth to him \[Hanumat] who was of the hue of rice husks.\
Desiring to get fruits \[for him] the excellent \[she-ape] entered into the forest.

[एष मातुर् वियोगाच् च क्षुधया च भृशार्दितः]{style="color:#0000ff;"}\
[रुरोद शिशुर् अत्यर्थं शिशुः शरवणे यथा ॥]{style="color:#0000ff;"} R 7.35.21-22c

He was afflicted by the separation from his mother and by hunger,\
\[Hence] the baby \[ape] howled exceedingly like the child \[the god कुमार] in the arrow-reed-forest.

Now the connection to the birth of कुमार is quite transparently made. More, specifically an allusion is made to the loud crying or yelling of the infant Hanumat, which is a key feature of Skanda in his earliest birth narratives too. Importantly, this links them to the Marut-s whose yelling or roaring is alluded to from the earliest श्रुति. Even in later folk etymology their name is explained as मा-रुदः (do not cry) from the same root rud as used in the perfect tense (लिट्) in this narrative.

Lootika: "Interestingly, मारुति retains the character of the Marut-s, whom the vipra-s fear even in the श्रुति. The ape's Marut-like ferocity is described as causing much fear among the ब्राह्मण-s until our mighty ancestors curbed it with their imprecation. O भार्गव let me read out the uttara narrative in this regard:

[बलेनापूर्यमाणो हि एष वानर-पुंगवः ।]{style="color:#0000ff;"}\
[आश्रमेषु महर्षीणाम् अपराध्यति निर्भयः ॥]{style="color:#0000ff;"}

Indeed, overflowing with might this bull among the apes fearlessly committed offences in the आश्रम of the great sages.

[स्रुग्-भाण्डान् अग्निहोत्रं च वल्कलानां च संचयान् ।]{style="color:#0000ff;"}\
[भग्न-विच्छिन्न-विध्वस्तान् सुशान्तानां करोत्य् अयम् ॥]{style="color:#0000ff;"}

He broke the sruk-s, smashed the utensils of agnihotra, and tore up the heaps of bark of the peaceful (sages).

[सर्वेषां ब्रह्म-दण्डानाम् अवध्यं ब्रह्मणा कृतम् ।]{style="color:#0000ff;"}\
[जानन्त ऋषयस् तं वै क्षमन्ते तस्य नित्यशः ॥]{style="color:#0000ff;"}

Knowing that he was made immune by ब्रह्मा to all the spells of ब्राह्मण-s the sages kept forgiving his \[affronts].

[यदा केसरिणा त्व् एष वायुना सो ।अञ्जनीसुतः ।]{style="color:#0000ff;"}\
[प्रतिषिद्धो 'पि मर्यादां लङ्घयत्य् एव वानरः ॥]{style="color:#0000ff;"}

Though अञ्जना's son was admonished by Kesari \[his earthly ape-father] and वायु, the ape kept breaking the laws.

[ततो महर्षयः क्रुद्धा भृग्व्-अङ्गिरस-वंशजाः ।]{style="color:#0000ff;"}\
[शेपुर् एनं रघु-श्रेष्ठ नाति-क्रुद्धाति-मन्यवः ॥]{style="color:#0000ff;"}

This angered the great sages of the clans of भृगु and अङ्गिरस, and, O chief of the Raghu-s, enraged they cursed him, though not with entirely ill intention.

[बाधसे यत् समाश्रित्य बलम् अस्मान् प्लवंगम ।]{style="color:#0000ff;"}\
[तद् दीर्घ-कालं वेत्तासि नास्माकं शापमोहितः ॥]{style="color:#0000ff;"}

O leaping ape, spellbound by our imprecation you will for a long time become non-cognizant of your strength, depending on which you harass us.

[ततस् तु हृत-तेजौजा महर्षि-वचनौजसा ।]{style="color:#0000ff;"}\
[एषो "श्रमाणि तन्य् एव मृदु-भाव-गतश् चरन् ॥]{style="color:#0000ff;"} R 7.36.28-34

Then shorn of his might and ebullience by the power of the great sages' spell he wandered around in their आश्रम-s with a soft temperament."

Somakhya: "Yes, आङ्गिरसी in this destructive aspect the great वानर is not very different from the रक्षस्-es who attacked the ब्राह्मण-s. Yet, the fact that our ancestors only mildly cursed him indicates that he belonged to the daiva side. This fury of the ape is verily comparable to the imploring calls of the vipra-s to the mighty Marut-s as they are invoked at the यज्ञ. The Atri-s in the RV say:

['अद्वेषो नो मरुतो गातुम् एतन]{style="color:#0000ff;"}\
[श्रोता हवं जरितुर् एवयामरुत् ।]{style="color:#0000ff;"}\
[विष्णोर् महः समन्यवो युयोतन स्मद्]{style="color:#0000ff;"}\
[रथ्यो न दंसनाप द्वेषांसि सनुतः ॥]{style="color:#0000ff;"} RV 5.87.8

Without hate O Maruts, come here on your march to us.\
Hear the invocation of the chanter: Marut-speed!\
O you of equal nature, together with the mighty विष्णु,\
like skilled car-warriors keep the hatreds faraway !

Even more tellingly they are explicitly called upon to be un-रक्षस्-like:\
[गन्ता नो यज्ञं यज्ञियाः सुशमि]{style="color:#0000ff;"}\
[श्रोता हवम् अरक्ष एवयामरुत् ।]{style="color:#0000ff;"} RV 5.87.9ab

March to our ritual with diligence, O recipients of the ritual.\
Hear the invocation without रक्षस्-nature: Marut-speed.

So even in that the रामायण captures a subtlety found in the Vaidika प्रतिमा in it is earthly mirror-character Hanumat."

Lootika: "Ah dear! that reminds me of a twenty verse para-रामायण tale from the body of legends preserved by my ancestors in the ब्रह्मपुराण (chapter 84), which links Hanumat to this रक्षस्-hood:

Two beautiful apsaras-es due to a curse of Indra were born as she-apes, अञ्जना and अद्रिका, a cat-like primate, and were the mates of the famous ape-lord Kesari, who reigned on the mountain slopes on the southern banks of the गोदावरि river. When Kesari had gone to the southern ocean, his realm was visited by the mighty sage Agastya. Kesari's wives took good care of him and pleased with them he offered them a boon. They asked for a mighty and powerful son each who would be worshiped in the world. Having blessed the she-apes, the ब्राह्मण went his way. In due course अञ्जना and अद्रिका were sporting on the mountain top, dancing, singing and laughing. They caught the attention the mighty god वायु and the रक्षस् नैरृत the regent of the South-west direction. The two divinities engaged in coitus with अञ्जना and अद्रिका respectively, and fathered on them Hanumat and Adri पिशाचराट्. Then the divinities revealed to the she-apes that they had become apes due to Indra's curse and would be restored by their sons to their original forms. The ape Hanumat and the king of the पिशाच-s grew to be great friends and brothers and eventually released their respective mothers from their curses by taking them to fords on the गोदावरि where they took a dip. These fords are known as अञ्जना and मार्जारिका after अद्रिका.

Here, the वानर Hanumat is seen as the brother of the पिशाच lord, as though to hint at his dark connections to रक्षस्-es. It is also possible that this brother is an ectype of the foremost simian गण of Rudra, नन्दिकेश्वर. This is also is keeping with the para-रामायण connections with raudra-class of deities which Hanumat is endowed with. Not surprisingly, Hanumat in the broader Hindu tradition tends to be increasingly seen as a son of Rudra, almost like restoring the parallel and more dominant vision of the parentage of the Marut-s in the Veda. Thus moving him away from the beneficent वायु of the Veda."

Somakhya: "That's an interesting narrative dear. But we must keep in mind that the old वायु, while situated with the benign "elemental" Vasu-s, tends to have one-foot in the Rudra class from an early stage of Indo-European history. Interestingly, in the शतपथ ब्राह्मण we see वायु becoming the leader of the animals, thereby encroaching on the domain of Rudra as पशुपति: '[वायु प्रणेत्रा वै पशवः । प्राणो वै वायुः । प्राणेन हि पशवश् चरन्ति ।]{style="color:#0000ff;"} (in 4.4.1.15)'. वायु is verily the leader of the animals. Breath is verily वायु. By breath indeed the animals move.

The real raudra side of वायु is particularly emphasized in the Zoroastrian, para-Zoroastrian, and non-Zoroastrian Iranian traditions. It also had a survival in Slavic folklore. In the यष्त् of वायु the Iranians clearly recognize him as a fierce and mighty deity with Rudra-like features, in particular holding a long piercing spear (cognate of Skt ऋष्टि of the Marut-s). He is depicted as revealing his names to Zarathushtra thus:

[tizhyarshte nãma ahmi tizhyarshtish nãma ahmi]{style="color:#008000;"}\
[पेरेथ्वरेश्ते न्ãम अह्मि पेरेथ्वरेश्तिश् न्ãम अह्मि।]{style="color:#008000;"}\
[ vaêzhyarshte nãma ahmi vaêzhyarshtish nãma ahmi]{style="color:#008000;"}\
[हवरॆनå नãम aहमि aिवि-हवरॆनå नãम aहमि ||]{style="color:#008000;"}

My name is sharpness of the spear, my name is he of the sharp spear,\
My name is length of the spear; my name is he of the long spear.\
My name is piercingness of the spear; my name is he of the piercing spear.\
My name is the fiery splendor; my name is the most fiery splendor.

Again in the Aogemadaeca we hear of the "pitiless वायु (vayaosh anamarezhdikahe). In the same text he is seen along with another deity Asto-vidotu (the bone-smasher) who is said to bring death to the mortal. In the material preserved in the middle Persian Bundahisn it is clarified that this Asto-vidotu is none other than wआय्-i vatar or the "evil वायु" who carries away the life breath of a mortal. Iranian oral tradition holds that he cast the lasso of death on Gayo Maretan the first mortal. All these are elements of the raudra class. Now, among the non-Zoroastrian Iranians in the कुषाण realm, Rudra and वायु were totally identified and essentially the latter was worshiped with the iconography of the former. In Iranian tradition these aspects of वायु are combined with the beneficent elements common with the वायु and the pacified Rudra as शिव of the Veda. The combination of these two aspects are important in the para-Zoroastrian religion of Zurwan. Moreover, the syncretic Rudra-वायु in Sogdhiana suggests that in certain streams of Iranic, वायु ruled supreme, a position acknowledged in certain aspects of the श्रुति."

Lootika: "That primacy of वायु also seems to have echoes in the इतिहास. Let me read what वाल्मीकि says describing the aroused Hanumat:\
[अरुजन् पर्वताग्राणि हुताशन-सखो 'निलः ।]{style="color:#0000ff;"}\
[बलवान् अप्रमेयश् च वायुर् आकाश-गोचरः ॥]{style="color:#0000ff;"} R 4.66.6

The wind-god rending apart mountain peaks is a friend of the god Agni. The god वायु is mighty beyond compare and with the atmosphere as the domain of his action.

While his primacy is explicitly mentioned, in describing him as the smasher of mountain peaks, we may again note that the रामायण confers features of the Marut-s in the श्रुति on him. After all we just deployed the mantra to the Marut-s which said: '[चिद् बिभिदुर् वि पर्वतम् ।]{style="color:#0000ff;"}'.

Perhaps, it is this old memory of an acknowledgment of his primacy which makes him the first recipient of the soma, which is stressed in the the next two mantra-s of the तृच we just deployed:

[तुभ्यं शुक्रासः शुचयस् तुरण्यवो मदेषूग्रा]{style="color:#0000ff;"}\
[इषणन्त भुर्वण्य् अपाम् इषन्त भुर्वणि ।]{style="color:#0000ff;"}\
[त्वां त्सारी दसमानो भगम् ईट्टे तक्ववीये ।]{style="color:#0000ff;"}\
[त्वं विश्वस्माद् भुवनात् पासि धर्मणासुर्यात् पासि धर्मणा ॥]{style="color:#0000ff;"}

For you the shining, pure, swift, formidable \[soma] libations,\
flowing as ripples \[even as] the waves rippling on water .\
You (accusative) the stealthy, tiring warrior invokes for luck in his rapid chase\
You drink before every being \[ordained] by the dharma of your asura-hood, you drink \[as ordained] by the dharma.

Here we may take this dharman, which is described as stemming from वायु's asura-hood, as a signal of his primacy. Moreover, his refreshing the stealthy warrior \[perhaps as a zephyr] in his chase reminds one of the Hanumat refreshing the crestfallen वानर-s in their pursuit when reminded by the old bear जाम्बवन्त् of his वायु-hood. Also note the word तक्ववीये derived from takva-वी \[falcon], literally meaning the falcon-chase suggesting that the Aryan-s hunted with falcons like other steppe people after them.

[त्वं नो वायव् एषाम् अपूर्व्यः सोमानाम्]{style="color:#0000ff;"}\
[प्रथमः पीतिम् अर्हसि सुतानाम् पीतिम् अर्हसि ।]{style="color:#0000ff;"}\
[उतो विहुत्मतीनां विशां ववर्जुषीणाम् ।]{style="color:#0000ff;"}\
[विश्वा इत् ते धेनवो दुह्र आशिरं घृतं दुह्रत आशिरम् ॥]{style="color:#0000ff;"} RV 1.134.4-6

You, O वायु, with none before you, have, for these soma libations of ours,\
the first right to drink, you have the right to drink these \[soma] pressings.\
Indeed, \[of] the peoples who do not make offerings and have shunned \[ritual] (the vihutamati-s = ayajvan-s ),\
all their cows milk the milk-soma drink, now they milk ghee and milk-soma drink \[for you] (even the milking of the ayajvan-s is turned towards वायु by the brahman of the आर्य ritualist).

O Somakhya, what do you think is the origin of the offering of the first soma offering with the वषट्-call to वायु."

Somakhya: "We may note that the in the grand soma rituals, in addition to the first soma-libation, वायु also receives the preliminary animal sacrifice known as the वायवम् पशु. This indeed might suggest a certain recognition of his primacy; a need to be pacified first. Indeed, in the उपनिषत् of the singers in the teaching of the ब्रह्मछारिन् to अभिप्रतारिन् काक्षसेनि and शौनक कापेय the primacy of वायु is explicitly taught thus:

[महात्मनश् चतुरो देव एकः]{style="color:#0000ff;"}\
[कः स जगार भुवनस्य गोपाः ।]{style="color:#0000ff;"}\
[तं कापेय नाभिपश्यन्ति मर्त्या]{style="color:#0000ff;"}\
[अभिप्रतारिन् बहुधा वसन्तम् ॥]{style="color:#0000ff;"}

The one god, four mighty ones\
he who has swallowed, the protectors of the universe,\
O कापेय, mortals do not see him\
\[though] O अभिप्रतारिन्, he is present in manifold forms!

[आत्मा देवानां जनिता प्रजानां]{style="color:#0000ff;"}\
[हिरङ्य-दंष्ट्रो बभसो 'नसूरिः ।]{style="color:#0000ff;"}\
[महान्तम् अस्य महिमानम् आहुर्]{style="color:#0000ff;"}\
[अनद्यमानो यद् अनन्नम् अत्ति ॥]{style="color:#0000ff;"} in छान्दोग्य उपनिषत् 4.3

The breath of the gods, the progenitor of the beings,\
The golden-fanged, wise devourer \[of all],\
They say his might is \[truly] great,\
who eats \[even] what cannot be eaten without being eaten!

Hence O ऊर्णायी, we use the यजुष्: [ॐ तद् ब्रह्म । ॐ तद् वायुः । ॐ तद् आत्मा । ॐ तत् सत्यम् । ॐ तत् सर्वम् । ॐ तत् पुरोर् नमः ॥]{style="color:#0000ff;"}

My ancient clansman the भार्गव Nema alludes to how वायु attained co-primacy with Indra due to his services in the smashing of forts:

[अयं त एमि तन्वा पुरस्ताद्]{style="color:#0000ff;"}\
[विश्वे देवा अभि मा यन्ति पश्चात् ।]{style="color:#0000ff;"}\
[यदा मह्यं दीधरो भागम्]{style="color:#0000ff;"}\
[इन्द्राद् इन् मया कृणवो वीर्याणि ॥]{style="color:#0000ff;"}RV 8.100.1

वायु said: '\[See] this, I go forth with my body before \[all].\
All the gods proceed towards me from behind.\
When you have secured \[this] share \[of the offering] for me,\
O Indra, only then you perform valorous acts together with me.'

[दधामि ते मधुनो भक्षम् अग्रे]{style="color:#0000ff;"}\
[हितस् ते भागः सुतो अस्तु सोमः ।]{style="color:#0000ff;"}\
[असश् च त्वं दक्षिणतः सखा मे]{style="color:#0000ff;"}\
['धा वृत्राणि जङ्घनाव भूरि ॥]{style="color:#0000ff;"}RV 8.100.2

Indra said: 'I put in place an offering of honey for you at the start \[of the ritual].\
Let the pressed out soma be established as your portion,\
and you will be the friend to my right.\
Now, we two shall demolish numerous forts!"

As an aside in, addition to the Marut-s who accompany Indra in the battle, he has two friends who are alluded to in this सूक्त of Nema. At the beginning Indra is assisted by वायु for whom he establishes the first offering. At the end Indra is joined by the mighty god विष्णु with whom also he shares an offering, as we shall offer in the rite that shall happen at midday:

[सखे विष्णो वितरं वि क्रमस्व]{style="color:#0000ff;"}\
[द्यौर् देहि लोकं वज्राय विष्कभे ।]{style="color:#0000ff;"}\
[हनाव वृत्रं रिणचाव सिन्धून्]{style="color:#0000ff;"}\
[इन्द्रस्य यन्तु प्रसवे विसृष्टाः ॥]{style="color:#0000ff;"} RV_8,100.12c

Indra said: 'O friend विष्णु, widely stride out.\
O Dyaus, grant space to hold the vajra aloft.\
We two shall slay वृत्र and release the rivers.'\
Nema's concluding quarter: 'Let them, released, gush forth by Indra's impetus'.\
(Note adverb vitara here is related to the name of the Germanic cognate of विष्णु: Vidarr).

In the शतपथ ब्राह्मण the first soma-libation with the वषट् being offered to वायु is used in an attempt to prop up the secondary प्राजापत्य religion over the primary Aindra religion by down-grading Indra: 'When दानव वृत्र was struck by Indra's vajra, Indra and other the gods did not know if he was really dead or not and remained in fear. वायु agreed to be their agent-messenger who would find out if he was really dead and in return asked for him becoming the recipient of the first libation. Indra however sought a share in that and the two went to the god प्रजापति for arbitration. He duly established the first share for वायु followed by the combined one for Indra and वायु.'

Keeping aside the unwholesome aspects of the प्राजापत्य religion aside for now, what we see from all this is that there might have been a para-Vedic system of the primacy of वायु among the early Indo-Iranian-s. This was probably similar to the centered cults in the Indo-European world, like those of the Mitra-वरुण, Rudra and विष्णु class of deities. Given the prominence of such cultic deities like वायु and विष्णु in the early I-Ir world, they were acknowledged within the framework of the standard model IE religion, the aindra religion, by the first offering to वायु and the combined one to Indra and विष्णु, for which Nema provides the legendary rationale. Notably, the इतिहास too acknowledges the role of the वषट् offering to वायु. When वायु retreated into a cave with Hanuman who was struck down by Mahendra we hear:

[निःस्वाध्याय-वषट्कारं निष्क्रियं धर्म-वर्जितम् ।]{style="color:#0000ff;"}\
[वायु-प्रकोपात् त्रैलोक्यं निरयस्थम् इवाभवत् ॥]{style="color:#0000ff;"} R 7.35.51c

Without Veda-study, without the offering with the वषाट्-call, without rituals and shorn of dharma, from the wrath of वायु the three worlds became as though sunk to the nether-region."

Lootika: "O सचाभू, we can see some further reflections in the इतिहास. While incorporated in the tale of the unwholesome प्राजापत्य religion, the ब्राह्मण narrative brings out one key point, namely that of वायु as the agent-messenger of the gods. This is exactly the role conferred on Hanumat, as the agent-messenger of the earthly Indra, राम. He goes in front of him as वायु in the smashing of the hostile forts of लन्का, while the earthly reflection of विष्णु, लक्ष्मण, stays close to राम, the Indra in their quest and battle. Only when coupled with Hanumat is राम able to perform his Indra-like acts.

This leads me to one last thing, the peculiar allusion in the uttara text to Hanumat as chasing राहु the planetary shadow in the sky even as he rose to seek the Sun as a fruit on a new moon day. It is said that राहु protested to Indra that his job of eclipsing the Sun was being appropriated by someone else. When Indra asked him not to fear, राहु was chased by Hanumat who took him too to be a fruit. It was then that the foremost of the gods proceeded to investigate and struck down Hanumat with his कुलिश even as he tried to swallow ऐरावत as a fruit. While this narrative contains an old motif also seen among the yavana-s as Zeus striking down Phaeton even as he tried to drive the chariot of Helios, is there a further significance to this?"

Somakhya: "While its exact significance remains a bit murky, it is indeed peculiar in raising the son of वायु from his usual atmospheric domain into the celestial domain. But the श्रुति clarifies that while वायु is the atmosphere, he is not merely restricted to the wind. He has a celestial dimension too where he is manifest as gravitation. The शतपथ ब्राह्मण states in the context of the ritual of the horse smelling the bricks for the first layer of the altar for the soma ritual and the laying of the विकर्णी brick and the stone with a natural hole:\
[तद् असाव् आदित्य इमां लोकान्त् सूत्रे समावयते ।]{style="color:#0000ff;"}

Now, the yonder Sun tethers these worlds to himself with a string.

[तद्यत् तत् सूत्रं वायुः सः ।]{style="color:#0000ff;"}

Now that string is the same as वायु.

[स यः स वायुर् एषा सा विकर्णी ।]{style="color:#0000ff;"}

That वायु is the same as this विकर्णी brick.

[तद्यद् एताम् उपदधात्य् असाव् एव तद् आदित्य इमां लोकान्त् सूत्रे समावयते ॥]{style="color:#0000ff;"}

When the lays down that \[brick he represents] the Sun tethering these worlds to itself by the string.

Thus, when Hanumat animates राहु he takes on a bit of that celestial role of his father in causing the movement of the celestial bodies "tethered" to the Sun."

Lootika: "This ब्रह्मवाद and इतिहास-पर्यालोचनं helps me catch a glimpse of the great god to whom we have made the offering having ensconced ourselves from the ayajvan-s and अविद्वान्-s. Restlessly churning the atmosphere, animating all life through respiration as "the friend of Agni", and swinging the heavenly bodies through their celestial paths, is the Niyutvant whom we praise. While the deva-dharma has declined among our people, a sliver of its spirit is indeed kept alive by the इतिहास which has become their dharma."

Somakhya: "That is verily true. The wobble caused by the प्राजापत्य religion, which culminated in the तथागत's virodha of the श्रुति resulted in mediocre मीमांसक-s who, while preserving the श्रुति like a donkey carrying sandalwood, lacked proper apprehension of the दैवी-मीमांसा. Some fragments of the spirit of the श्रुति remained alive in the इतिहास. Hence, it is not surprising that the नास्तिक बुद्धघोष proscribed people from attending lectures of "worthless stories" like the रामायण and महाभारत. With time our people having lost their martial ardor, which is signaled in the Veda by the aindra-भाव, succumbed to the bearded unmatta-s and their younger cousins. When the Veda and Dharma almost seemed lost, the dormant spirit of वायु in the form of Hanumat in the इतिहास sprang forth in the peninsula to impel our people to slay the makkha-प्रेतोन्मत्तादि, even as the वृत्र-s shattered by Indra and वायु, to at least give us a new lease of life.

In any case कान्ता that was a good ब्रह्मवाद , now let us proceed with the next karman."


