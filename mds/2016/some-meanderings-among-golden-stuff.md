
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some meanderings among golden stuff](https://manasataramgini.wordpress.com/2016/08/23/some-meanderings-among-golden-stuff/){rel="bookmark"} {#some-meanderings-among-golden-stuff .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 23, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/08/23/some-meanderings-among-golden-stuff/ "5:46 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There are some angles that we often encounter in the construction of the golden ratio and its use in religious art. The first is the most obvious is the angle  $\dfrac{2\pi}{5}=72\^o$  which is the angle made by the diagonals connecting a side of the regular pentagon to a vertex. Thus, for a unit pentagon  $\phi$  is the length of its diagonals. This captures the quintessence, literally the five-ness of  $\phi$ . This angle was used in Hindu tradition as can be seen in the figure below. Here the angle and  $\phi$  are deployed in one of the most remarkable religious works from the वङ्ग country (region of modern Balurghat) --- a लिङ्ग on the four sides of which are the four kula-योगिनी-s of the directional आम्नाय-s. This is keeping with the deployment of the golden ratio in other figures of the kaula tradition like the कुब्जिका and श्री yantra-s.

![golden_yoginIs](https://manasataramgini.files.wordpress.com/2016/08/golden_yoginis.jpg){width="75%"}

In the previous note on finding the translation vectors for the golden construction we encountered the famous angle  $\theta=\sin^{-1}\left(\dfrac{2}{2\phi-1}\right)=\cos^{-1}\left(\dfrac{1}{2\phi-1}\right)=\tan^{-1}(2) \approx63.43\^o$ . This angle is the supplementary angle of the dihedral angle of a dodecahedron that quintessentially quintessent object. This angle comes up often in the construction of golden ratio with the भुजा-कोटि-कर्ण-न्याय, which we place here along with other miscellany for the record. Indeed, Posamentier and Lehmann point out in their wonderful monograph on the golden ratio that Johannes Kepler had said: "*Geometry harbors two great treasures: One is the Pythagorean theorem, and the other is the golden ratio. The first we can compare with a heap of gold, and the second we simply call a priceless jewel.*" While these are rather elementary constructions, we are just putting down a few of them for own record and the instruction of those in need.

**Construction 1:**\
![golden_2squares](https://manasataramgini.files.wordpress.com/2016/08/golden_2squares.png){width="75%"}


 1.  Draw two unit squares sharing a common side  $\overline{DB}$ .

 2.  Bisect  $\overline{DB}$  to get midpoint H. With H as center draw a circle of radius 0.5

 3.  Draw  $\overline{AF}$ .  $\overline{AF}$  cuts the circle at points I and J. Now  $\overline{AJ}=\phi$ . Also  $\angle BHA=\tan^{-1}(2)$ \
From the construction we have:

 $$\overline{AJ}=\overline{HJ}+\sqrt{\overline{AB}^2+\overline{BH}^2}=\dfrac{1}{2}+\sqrt{1+\dfrac{1}{4}}=\phi$$ 

**Construction 2:**\
![golden_3squares](https://manasataramgini.files.wordpress.com/2016/08/golden_3squares.png){width="75%"}


 1.  Draw three unit squares with each adjacent one sharing a common side.

 2.  Draw  $\overline{ED}$ . Note  $\angle BED=\tan^{-1}(2)$ . Bisect  $\angle BED$  to get point K.  $\overline{AK}=\phi$ \
From construction we have:

 $$\overline{ED}=\sqrt{\overline{BE}^2+\overline{BD}^2}=\sqrt{1+4}=\sqrt{5}$$ 

Using the angle bisector theorem we have:

 $$\dfrac{\overline{BK}}{\overline{DK}}=\dfrac{\overline{BE}}{\overline{DE}}$ . Let  $\overline{BK}=x\; \therefore \dfrac{x}{2-x}=\dfrac{1}{\sqrt{5}}$$ 

 $$\therefore x=\dfrac{2}{1+\sqrt{5}}=\dfrac{1}{\phi}\therefore \overline{AK}=1+\dfrac{1}{\phi}=\phi$$ 

**Construction 3**\
![golden_square_triangle](https://manasataramgini.files.wordpress.com/2016/08/golden_square_triangle.png){width="75%"}


 1.  Draw a square ABCD with sides of length 2 units.

 2.  Obtain midpoint E of  $\overline{CD}$  and join it point A and B to obtain  $\triangle ABE$ . Note  $\angle ABE=\tan^{-1}(2)$ 

 3.  Draw incircle of  $\triangle ABE$  having determined incenter I. It touches  $\overline{AB}$  at F.

 4.  Using  $\overline{FI}$  as radius and F as center draw another circle to obtain point J.  $\overline{AJ}=\phi$ .\
From construction we have:

 $$\overline{AE}=\overline{BE}=\sqrt{5}\; \therefore perimeter(\triangle ABE)=2+2\sqrt{5}$$ 

Radius of incircle  $r_i=\sqrt{\dfrac{(1+\sqrt{5}-\sqrt{5})(1+\sqrt{5}-\sqrt{5})(\sqrt{5}-1)}{1+\sqrt{5}}}=\dfrac{1}{\phi}$ 

 $$\therefore \; \overline{AJ}=\phi$$ 

A construction such as these can be also used to easily construct a model of the great pyramid of king Khufu of Egypt. We illustrate this below using construction 2 with 3 unit squares:\
![golden_pyramid_khufu](https://manasataramgini.files.wordpress.com/2016/08/golden_pyramid_khufu.jpg){width="75%"}


 1.  The first of these is used to construct a square of side 2 units BEDC, which forms the base of the pyramid.

 2.  We then use the above construction to obtain  $\overline{OF}=\phi$ .

 3.  We then deploy the geometric mean theorem in the vertical plane on  $\overline{OG}$  and  $\overline{OF}$  to obtain  $\overline{OA}=\sqrt{\phi}$ , which will be the height of the pyramid.

 4.  We then join the vertices of the base square BEDC to form the model pyramid of Khufu.

![shrIchakra](https://manasataramgini.files.wordpress.com/2016/08/shrichakra.jpg){width="75%"}$  and the isoceles sides of the cross-section of through the middle of the square base being  $\phi$ . Thus, some form of expressing the golden ratio, perhaps in the form of a construction like the above was known to the Egyptians.

This makes the great pyramid at \~2560 BCE perhaps the first monument in world history to encode this famed ratio. It is simultaneously a stark reminder of a great ancient civilization and how it can vanish despite it achievements. But due to the encoding of this information on a monumental scale a glimpse of ancient Egypt's knowledge has come down to us. Indeed the scale and the brilliant engineering of the pyramids has helped them survive the attempts to erase them: Al-Malik al-Aziz Uthman the son of the counter-crusader Salah ad-Din who wanted clean Giza of its जाहिलीयह् devoted a large amount of labor to demolish the pyramids but only having succeeded in damaging the smallest of the big three eventually gave up. Then the Mamluqs of Nasir-ad-Din al-Hasan carved out the casing stones of the great pyramid to build masjids but could not get down the colossus. However, there is no guarantee that in the coming years a Mohammedan upheaval in Egypt does what their predecessors wished but failed to do.

This brings us to whether the Hindus and yavana-s obtained the idea of using  $\phi$  from the Egyptians. This is plausible since they are seen using a construction that embodies this ratio much before the other two. The yavana-s show a clear record of the ratio and specific constructions for the first time in the work of Euclid. Given Euclid was recording earlier geometric knowledge it might have been known sometime before him as suggested by  $\phi$  used by Phidias in several architectural features of the Parthenon. This was around the same time Herodotus records relationship concerning the great pyramid. Hence, we at least have evidence for Greek-Egyptian contact where a construction involving this ratio is recorded supporting the possibility that the yavana-s obtained the concept from the Egyptians before it was mathematically formalized in Euclid.

The Indian situation is less clear but offers certain interesting clues. The as yet mysterious Harappan civilization was undoubtedly in contact with Egypt at a time reasonably close to when the great pyramid was constructed. Yet, I have so far not found any evidence for the use of golden ratio-related constructions in the Harappan artefacts that I have seen. The Indo-Aryan tradition abounds in extensive geometric constructions related the श्रौत ritual. There is no evidence for the use of golden ratio-related constructions in the early Indo-Aryan श्रौत tradition among the bricks, altars or ritual halls. Since this tradition was something derived from the common ancestral tradition that also seeded Greek geometry, it is likely that in old Indo-European tradition the golden ratio did not play any major role. However, in the late Atharvavedic tradition we find the construction of a regular pentagonal altar for वायु where the ratio could have been involved ("[वर्तुलं पञ्चकोणं...वायव्यां पञ्चकोणं तु वायव्येष्व् अपि कर्मन् ॥]{style="color:#000080;"}").

On the other hand we see a completely distinct mathematical approach which yields a series whose limiting ratio is  $\phi$ : the Meru of पिङ्गल. This at its latest would have been close to Phidias in antiquity but there is no evidence that the numbers of the Meru were used in a geometric construction. With the rise of recognizable Indian iconography, i.e. the iconographic tradition that has remained rather conservative since then, suddenly the golden ratio appears in certain examples. Below is one of its early appearances in a ताथागत context from Sarnath.

![golden_buddha](https://manasataramgini.files.wordpress.com/2016/08/golden_buddha.jpg){width="75%"}

I am aware of another case of a terracotta image of Rudra and Uma from Rangmahal, Rajasthan belonging to the first few centuries of the common era present in a Golden rectangle. However, I need to examine a clearer image of this icon to confirm this. While iconographic examples such as these might be disputed, the श्रीचक्र is a rather clear illustration. Any direct connection to yavana tradition for these Hindu examples can hardly be established. Moreover, the ratio does not play any role to our knowledge in the classic Hindu successor mathematical tradition of the श्रौत constructions. Thus, it appears to have been a para-mathematical tradition that primarily survived in a religious context illustrating that classical Hindu mathematical tradition does not encompass all of Hindu mathematical knowledge. Other examples of such include the Platonic solids in bead manufacture in ancient India or the ellipse in temple architecture.

