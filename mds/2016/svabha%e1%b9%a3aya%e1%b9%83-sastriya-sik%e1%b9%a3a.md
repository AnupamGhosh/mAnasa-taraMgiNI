
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [स्वभाषायां शास्त्रीया शिक्षा](https://manasataramgini.wordpress.com/2016/05/21/svabha%e1%b9%a3aya%e1%b9%83-sastriya-sik%e1%b9%a3a/){rel="bookmark"} {#सवभषय-शसतरय-शकष .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 21, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/05/21/svabha%e1%b9%a3aya%e1%b9%83-sastriya-sik%e1%b9%a3a/ "7:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[As PDF](https://app.box.com/s/yyz2db90d89efxnfnu3hvvaoqi742g6s)

navame वर्षे लूतिका तस्याः पञ्च-वर्षीयायै अनुजायै varolyai द्रव्यशास्त्रम् अशिक्षयत् | लूतिका 'vadat : "priye varoli ध्यानेन कार्णार्पणं kuru | atha वक्ष्याम्य् amla-क्षार-सिद्धान्तं ca लवणीकरणस्य प्रयोगं ca | purve काले स्वस्त्यात्रेयेण+अस्मदीयेन द्रव्यविदा+amla-क्षार-सिद्धान्तः प्रोक्तः | काले 'smin amla-क्षार-सिद्धान्तो arrheniyas ca lauri ca ब्र्योन्स्तेड् ca luyis ca नामभिर् म्लेच्छानां dravyavidbhir परिस्फुटः | lauri-ब्र्योन्स्तेड्-सिद्धान्ते 'mlam ity उदजनायनस्य दाता ca क्षार ity उदजनायनस्य गृहीता ca | dravyasyo\[u]दजनायन-दानस्य balam iti प्ः परिमाणम् | etasmin प्ः परिमाणे 0 iti तीव्राम्लञ् ca 14 iti तीव्रक्षारश् ca | udakasya प्ः 7 iti | मध्यस्थं प्ः | luyis-सिद्धान्ते 'mlam iti विद्युदणु-dvayasya गृहीता ca क्षार iti विद्युदणु-dvayasya दाता ca (विद्युदणु = electron)||

अस्मदीयाय प्रयोगाय+atra lauri-ब्र्योन्स्तेड्-सिद्धान्तः prayujyate | उदनीरिकाम्लम् ( $ःCल्$ ) एकं तीव्रम् अम्लञ् ca विक्षारोदजारेयम् ( $णऒः$ ) ekas तीव्रः क्षारश् ca | मषिज-द्रव्याम्लानि (organic acids), उदाहरणं चुक्रं, बहुशो नास्ति तीव्राणि | tadvan मषिज-द्रव्यक्षाराः (organic bases), उदाहरणं विनीलियं (aniline), बहुशो 'बलाः | atyanta-तीव्राण्य् अम्लान्य्, उदाहरणं fluorosulfuric acid ( $HSO_3F$ ) ca antimony pentafluoride ( $Sब्F_५$ ) cai\[e\]kam-ekam-मानेन मिश्रणं, महाम्लानि (magic acids) kathyante | महाम्लानि मषिज-dravyebhyo 'py उदजनायनानि dadhate | महाम्लेषु विषय etat प्ः परिमाणम् अनुचितं bhavati | परं tv atra महाम्लानि na वर्णयिष्यामि ||

nu लवणीकरणस्य प्रयोगं प्रवक्ष्यामि | सामान्योर् amla-क्षारयोः संगमेन sambhavato लवणम् उदकञ् ca |  $H^+X^- + Y^+ऒः^- \rightleftharpoons Y^+X^- + H_2O$  iti सूत्रम् | tato 'mla-क्षारयोः संमितेन मिश्रेण vartate लवणीकरणम् (acid-base titration) | ःCल् ity ekabalam अम्लञ् ca णऒः ity एकबलः क्षारः | ta ekam उदजनायनस्य दानं वा ग्रहणं वा शक्नुवन्ति | परं tu  $H_2SO_4$  iti dvi-balam अम्लञ् ca  $Ca(OH)_2$  iti द्विबलः क्षारः कारणं एताभ्यां  $2 \times H^+$  वा  $2 \times ऒः^-$  वा mucyete | tena 0.5 mol  $H_2SO_4$  tulayati 1 mol  $णऒः$  | ततः  $H_2SO_4$  asya normality (N) द्विगुणः | ततः  $H_3PO_4$  asya normality त्रिगुणः |  $N_1 \times V_1=N_2 \times V_2$  लवणीकरणस्य सूत्रम् iti | atha प्रयोगः ||"

लूतिका स्वसासहिता गार्ह-prayoga-शालायां प्राविशत् | सा 'ब्रवीत्: "anuje nu hastapau गृह्णीष्व | इदानीं द्रवमिं (burette) गृहीत्वा प्रणाल्या सावधानं द्रवम्यां 20 ml 1M णऒः क्षारम् अनुपृणीयाः | atha ध्यानेन सरित्नालेन (pipette) 10 ml 1M उदनीरिकाम्लं ( $ःCल्$ ) palighe उद्वपेः | palighe phenolphthalein वर्ण-देष्टारं उपदद्याः | Phenolphthalein एकं मषिज-dravyam asti | tad amle 'वर्णञ् ca क्षारे पाटलवर्णञ् ca धारयति | atra द्रवम्याः शनैः शनैः क्षारम् amla-paligha आपातयेः | कालेन कालं पलिघं मन्दं मन्दं कम्पयेथाः | yadi palighe dravo भूयते मृदुः पाटलस् tarhi क्षारस्याधिकं निर्दिशति | क्षारस्य द्रवम्याः प्रवाहं ततः क्षणात् nirunddhi | द्रवम्याः क्षारस्य स्तरं विलक्षय | tatra सृज्यते लवणम् iti लवणीकरणस्य प्रयोगः || "

evam एषः prayogo 'grajayo\[u]पदिष्टो वरोल्या निदानेन कृतः | क्षारस्य 10 ml samupayuktam iti varoly ऐक्षत ||

◊◊◊◊◊

लूतिकया + अनुजाभ्यो नक्षत्रविद्यायाः प्रथमः पाठः ||\
बालावस्थायां tamisre nakte लूतिका 'ब्रवीत् : "सूर्येति सामान्यं नक्षत्रम् | सर्वाणि नक्षत्राण्य् उदजनमयानीति | dravyasyo\[u]न्मानम् ekam अणु-संख्येति | yadi सर्वेषाम् अणूनां संख्यां गणयति tarhi अणुसंख्येति उन्मानं labhyate | 1 gram परमाणवीयो\[u\]dajana-madhye  $6.022 \times 10^{23}$  udajana-परमाणवः santi | iti 1 mole परमाण्वीयोदजनम् | 12 grams मषि-madhya evameva  $6.022 \times 10^{23}$  मषि-परमाणवः santi | iti 1 mole मषिः | atha 16 grams परमाणवीयाग्निजन-madhye  $6.022 \times 10^{23}$  agnijana-परमाणवः santi | iti 1 mole परमाणवीयाग्निजनम् | evameva dravya-भारस्य+अणु-भारेण विभागेन ca  $6.022 \times 10^{23}$  अभ्यासेन ca+अणुसंख्या labhyate | नक्षत्रेष्व् उदजनाणु-संख्या 92% iti | स्तृवातेर् अणुसंख्या 7.5% iti (स्तृवाति = helium) | अन्यानां मूलकानाम् अणुसंख्या 0.5% iti (मूलकम् iti element) | अनुजाः पश्यत पश्यत+adbhutam idam ! सर्वाः sthavara-जङ्गमाः 0.5% anena निर्मिताः ||

kena राजन्ति नक्षत्राणि? श्रुणत अनुजा विषयम् इमं महत्वपूर्णं | परमाणुर् नाभाणुना ca विद्युदणुना ca निर्मितः (नाभाणु= nucleus) | udajana-परमाणोर् नाभाणौ kevala एकः स्निग्धाणुर् vartate (स्निग्धाणु= proton) | अन्येषु परमाणुषु नाभाणुषु vartante स्निग्धाणवश् ca समाणवश् ca (समाणु= neutron) | नक्षत्रेषु+उदजनं विद्युदणुना विरहितं केवलं स्निग्धाणु-रूपम् asti | gurutva-ध्राज्या नक्षत्रेष्व् अन्तः स्निग्धाणवः संपीडिताः santi | tena एकः स्निग्धाणुर् anyena स्निग्धाणुना संस्करोति | अस्मात् समासात् sambhavanti द्वितीयोदजनस्य नाभाणुश् ca निर्वेदाणुश् ca प्रतिविद्युदणुश् ca (निर्वेदाणु= neutrino; प्रतिविद्युदणु= positron; द्वितीयोदजन= deuterium) | द्वितीयोदजनस्य नाभाणौ स्तः स्निग्धाणु-समाणू | तदनन्तरं tata uparacitasya द्वितीयोदजनस्य नाभाणुर् anyena स्निग्धाणुना संस्करोति | तस्मात् जायत ekona-स्तृवातेर् नाभाणुः ( $^३ःए$ ) | api चैतस्यां प्रक्रियायां आरोहत्य् eko tivras तेजाणुः (तेजाणु=photon) | एषस् तेजाणुर् नक्षत्रस्य शक्त्याः कारणम् iti | तदनन्तरं dve ekona-स्तृवाति-नाभाणऊ eka ekena संस्कुरुतः | तस्याः प्रक्रियायाः sambhavanti एकः सामान्य-स्तृवाति-नाभाणुश् ca dve स्निग्धाणू ca | etau स्निग्धाणू पुनः प्रथमां प्रक्रियां आरभेते | इयं स्रज्प्रक्रिया |

api ca : प्रतिविद्युदणुर् iti pratidravyam । dravyasya प्रतिद्रव्येण संमेलनात् जायते तीव्रस् तेजाणुः । अस्यां प्रक्रियायां विद्युदणु-प्रतिविद्युदाणू विनष्टौ भवतः । tato वितरं mucyate शक्तिः ।

 $$^१ः + \, ^१ः \rightarrow \, ^२ः + \nu + e^+$$ 

 $$^२ः \lbrack n+p\rbrack + \, ^१ः \rightarrow \, ^३ःए \lbrack n+2p\rbrack + \gamma$$ 

 $$2 \times \, ^३ःए \rightarrow \, ^४ःए \lbrack 2n+2p\rbrack + 2 \times \, ^१ः$$ 

 $$e^+ + e^- \rightarrow \gamma$$ 

iti सूत्राणि |\
नक्षत्राणां शक्त्या मूल-कारणम् | अनया rocante नक्षत्राणि ||"

