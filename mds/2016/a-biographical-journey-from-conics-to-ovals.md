
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A biographical journey from conics to ovals](https://manasataramgini.wordpress.com/2016/04/10/a-biographical-journey-from-conics-to-ovals/){rel="bookmark"} {#a-biographical-journey-from-conics-to-ovals .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 10, 2016]{.entry-date}](https://manasataramgini.wordpress.com/2016/04/10/a-biographical-journey-from-conics-to-ovals/ "7:21 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As this article needed a lot of figures with some mathematical notation it is being presented as a PDF file: [A biographical journey from conics to ovals](https://manasataramgini.files.wordpress.com/2016/06/ovals_etc-2.pdf)

This may be read a continuation of earlier notes such as:

[Ovals, drops, tops and the like](https://manasataramgini.files.wordpress.com/2016/06/ovals_etc.pdf)\
[Fancies of the parabola and hyperbola](https://manasataramgini.wordpress.com/2008/03/28/fancies-of-the-parabola-and-hyperbola/)\
[A personal discursion on conic sections](https://manasataramgini.wordpress.com/2012/10/23/a-personal-discursion-on-conic-sections/)\
[Iamblichus, quadratures, trisections and the lacuna of the cycloid](https://manasataramgini.wordpress.com/2016/03/23/iamblichus-quadratures-trisections-and-the-lacuna-of-the-cycloid/)\
[The cardioid and the arbelos: the scimitar and the axe](https://manasataramgini.wordpress.com/2016/03/19/the-cardioid-and-the-arbelos-the-scimitar-and-the-axe/)\
[The Ara and the Arbelos](https://manasataramgini.wordpress.com/2008/01/06/the-ara-and-the-arbelos/)\
[Some trivia on equilateral triangles and the like](https://manasataramgini.wordpress.com/2014/12/28/some-trivia-on-equilateral-triangles-and-the-like-2/)


