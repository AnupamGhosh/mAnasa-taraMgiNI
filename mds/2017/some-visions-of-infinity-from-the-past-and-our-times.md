
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some visions of infinity from the past and our times](https://manasataramgini.wordpress.com/2017/03/26/some-visions-of-infinity-from-the-past-and-our-times/){rel="bookmark"} {#some-visions-of-infinity-from-the-past-and-our-times .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 26, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/03/26/some-visions-of-infinity-from-the-past-and-our-times/ "5:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The great Hindu mathematician and astronomer भास्कर-II's work preserves a high-point of Hindu knowledge. His work contains ideas that are often seen as characterizing "modern" scientific understanding i.e. what in the west would seen as starting with Leibniz and Newton going down to our times. About 40 years from the time of भास्कर's death around 1185 CE we see the first irruptions of the monstrous Mohammedans in south India, initially spear-headed by the घाज़ि sufi-s and then by then by the main army of the Sultanate. The first of these invasions were successfully repulsed by the यादव monarchs. We hear in their inscriptions from around that time titular statements such as:

[तुरुष्क-कोप-प्रलय-महार्णव-मग्न-मेदिनी-समुद्धरण-महा-वराह ।]{style="color:#0000ff;"}\
The great boar \[the यादव monarch] who lifted up the world sunk by the deluge of the Islamic irruption.

[गम्भीराभीर-प्रचण्ड ... प्रराज्य-राज्य-तुरुष्कोपप्लव-मेदिनी-समुद्धरण-[कर्तृ] ।]{style="color:#0000ff;"}\
The mighty fierce warrior of the cowherd race who lifted up the kingdom, frontiers, and the world from the Islamic irruption.

Despite their initial successes it appears that the यादव monarchs did not entirely understand the extent of the threat they were facing. This conspired with internal fissures in their polity and about 50 years from that point in time their kingdom came to a cataclysmic end at the hands of the Army of Islam. This marked the drastic cessation of creativity in many of the deeper aspects of Hindu knowledge. Thus, the high-point reached by भास्कर-II remained that. Though भास्कर continued to be studied and commented upon the creative expansion of the foundations laid by him mostly ceased in Hindudom other than in the heroics of the नम्बूतिरि-s and their students in the south. This was mainly because of the Vijayanagaran empire reviving Hindudom in the south after overthrowing Mohammedanism in a series of brilliant campaigns. While this provided a refugium for the survival of Hindu knowledge, the spread of ideas and participation across the subcontinent which drove earlier Hindu creativity was no longer possible due to the truncation caused by the horrors of Mohammedanism.

In भास्कर's thought we see facets of something which fascinated us since childhood, though we never really understood it in completeness: the idea of infinities. Yet we talk about it for after all life is full of things that we can see and visualize but perhaps hardly "understand". As the old Hindus would say we experience their प्रत्यक्ष-s but may not realize their परोक्ष. Hence, we use भास्कर's work as the starting point of some musings on this matter. In भास्कर's work we explicitly or implicitly encounter at least two forms of infinity that a typical student of the sciences grapples with by the time he enters the second decade of his life. One of these is the infinity associated with calculus and its counterpart the infinitesimal. The development of the apparatus of calculus had a long history in Hindu mathematics that is not yet fully known. We already see use of what we today call derivatives in the calculation of the motion of the Moon by the great astronomer मञ्जुल the भरद्वाज in his surviving work the लघुमानस written around 932 CE. The rationale for this is presented by भास्कर-II along with other mathematical operations that imply the knowledge of infinity of calculus and limits. This comes out clearly in his commentators like मुनीश्वर and the नम्बूतिरि-s and their students. A second infinity is an algebraic infinity that भास्कर lays out in his बीज-गणित on the six-fold operations of zero.

[वध-आदौ वियत् खस्य खम् खेन घाते ख-हारो भवेत् खेन भक्तश् च राशिः । २।१८]{style="color:#0000ff;"}\
In multiplication etc of a number by zero the result is zero, in the multiplication by zero of a number the result is zero \[the transitivity of zero multiplication]. The division of a number by zero results in \[an infinity called] khahara.

[द्वि-घ्नं त्रि-हृत् खं ख-हृतं त्रयं च शून्यस्य वर्गं वद मे पदं च । २।१९]{style="color:#0000ff;"}\
 $0 \times 2; \frac{0}{3}; \frac{3}{0}; 0^2; \sqrt{0}$  tell me \[the answers]

[अस्मिन् विकारः ख-हरे न राशाव् अपि प्रविष्टेष्व् अपि निःसृतेषु ।]{style="color:#0000ff;"}\
[बहुष्व् अपि स्याल् लय-सृष्टि-काले अनन्ते अच्युते भूत-गणेषु यद्-वत् ॥ २।२०]{style="color:#0000ff;"}

In this number khahara there is no change from addition to or subtraction from it of many quantities even as at the time of destruction and emission there is no change in Ananta (संकर्षण) or Acyuta (वासुदेव) \[by the absorption or emission] of material beings.

There are two aspects that are striking in this account of the khahara. The first of these relates to how it is handled in algebra. We encounter this in a simple equation which भास्कर provides to test the student for this concept:

[कः खेन विहृतो राशिः कोट्या युक्तो (अ)थवो(ऊ)नितः ।]{style="color:#0000ff;"}\
[वर्गितः स्वपदेनाढ्यः खगुणो नवतिर् भवेत् ॥]{style="color:#0000ff;"}\
Which number divided by zero, then increased or reduced by crore, then squared, then increased by its own square root, then multiplied by zero becomes 90.\
Now let us unpack this using  $\circledcirc$  for kha and  $\bowtie$  for khahara of भास्कर:

 $$x\\ \frac{x}{\circledcirc}=x\cdot \bowtie \\ x\cdot \bowtie \pm 10^8 = x\cdot \bowtie\\ \left (x\cdot \bowtie \right)^2=x^2 \cdot \bowtie \\ x^2 \cdot \bowtie + \sqrt{x^2 \cdot \bowtie} = (x^2+x) \cdot \bowtie\\ (x^2+x) \cdot \bowtie \cdot \circledcirc = (x^2+x) \cdot 1 = 90\\ x^2+x-90=0\\ \therefore x=9; x=-10$$ 

The positive root is  $x=9$  is the preferred answer of the Hindus as राशि is taken to mean positive quantity. If one observes this operation carefully, the reduction to an ordinary quadratic would not happen if one were to treat the kha and the khahara as the zero and infinity of limits as in calculus. It would blow up to infinity and not leave behind a solvable quadratic residue. This happens only if one strictly adheres to भास्कर's prescription regarding khahara. A further discussion of such problems can be found in Avinash Sathaye's work on Hindu infinities.

This brings us to the second striking aspect of the account of khahara: Its philosophical implication(s), especially given that भास्कर goes out of his way to use पाञ्चरत्रिक वैष्णव terminology to explain khahara. पाञ्चरत्रिक tradition uses the concept of large numbers to illustrate the idea of the emission of the universe from the लक्ष्मी-नारायण continuum. For instance the लक्ष्मी-tantra postulates that the whole universe has emerged from a particle that is merely  $10^{-18}$  the quantity of the लक्ष्मी-नारायण continuum. Thus, it tries to illustrate that emission of something as huge as the universe does not make much of a change in the continuum. भास्कर too uses such huge numbers but he sees them like other good Hindu astronomers of the age as merely large finite numbers. For example he states a hypothesis that the diameter of the universe is  $4.69947571\times 10^{16}$  km. He is not sure if the hypothesis is correct and adds that it might merely be the distance that light from the sun has travelled rather than the actual size of the universe (while this is a large number it is still short of the modern estimate of the estimate of the diameter of the visible universe at  $8.8 \times 10^{23}$  km). भास्कर's concept of khahara is not merely a huge number but a special kind of infinity where operations like addition, subtraction, squaring etc have completely eroded. Thus, it may be visualized as the entity that restores the balance caused by the introduction of zero --- a principle of conservation to the number system which is shattered by the "black hole" of zero i.e. kha, viyat, शून्य. This conservation principle almost has a paradox in it because after all the example he uses is of things being emitted and absorbed by विष्णु without making any difference to the god. But once one sees the other pole of this system as the abyss of kha it becomes consistent. Moreover, भास्कर was not the only scientist to see philosophical implications for zero and infinity. Such an allusion is also made by the mathematician नारायण पण्डित as pointed out by सूर्यदास:

[एवम् एतत् प्रसंगेन साहित्यो(उ)क्त्या नारायणो'अपि स्वकृत-बीजे निरूपयां चकार यथा ।]{style="color:#0000ff;"}\
In this way नारायण had also defined \[note use of periphrastic perfect] this in his own algebra by means of a poetic utterance:

[शून्याभ्यास वशात्-खताम् उपगुप्तो राशिः पुनः स्वद्धृतो]{style="color:#0000ff;"}\
['अप्य् आवृत्तिं पुनर् एव तन्मयतया न प्राक्तनीं गच्छति ।]{style="color:#0000ff;"}\
[आत्माभ्यास-वशाद् अनन्यम् अमलं चिद्-रूपम् आनन्दं]{style="color:#0000ff;"}\
[प्राप्य ब्रह्म-पदं न संसृतिपथं योगी गरीयान् इवे(अ)(इ)ति ।]{style="color:#0000ff;"}

Under the operation of multiplication by zero the number vanishes (literally: is concealed by) into zeroness. But again when divided by zero it gets absorbed in that (khahara) and does not return to its previous (finite) state, even as a respectable yogin through the practice of self-realization having attained the unique bliss of pure-consciousness, free from impurities, does not return to the path of union (with matter).

नारायण's analogy is one of yoga. We posit that implicit in this again is a philosophical conservation principle: The destruction of finite number by zero is offset by the its absorption into the khahara. In the sense of a yogin the reduction of this material "mala" to zero (state of amala) is offset by his becoming infinite by oneness with the universal consciousness.

The philosophical interpretations of the algebraic infinity in भास्कर and नारायण may be linked comparable concepts from earlier Hindu tradition. In the context of भास्कर's khahara his commentator viloma-kavi सूर्यदास cites the verse of भीष्म from the महाभारत regarding the वासुदेव the supreme god of the पाञ्चरत्रिक-s:\
[यतः सर्वाणि भूतानि भवन्त्य् आदि युगागमे ।]{style="color:#0000ff;"}\
[यस्मिंश् च प्रलयं यान्ति पुनर् एव युग-क्षये ॥]{style="color:#0000ff;"}\
From whom all entities come into being at the begining of the first yuga and also into whom all go to dissolution at the end of the yuga-cycle.

Further one may also point to a much earlier, famous mantra from the शुक्ल-yajurveda that contains a comparable concept:\
[ॐ पूर्णम् अदः पूर्णम् इदं पूर्णात् पूर्णम् उदच्यते ।]{style="color:#0000ff;"}\
[पूर्णस्य पूर्णम् आदाय पूर्णम् एवावशिष्यते ॥]{style="color:#0000ff;"}\
That is the whole; this is whole; from the whole comes out a whole.\
When the whole is removed from the whole what is left is also the whole.

We had learned this mantra as the concluding chant of the only उपनिषत् of the शुक्ल-Yajurvedin-s whose recitation our teacher taught. Perhaps we were subliminally influenced by what we had just learned as we wandered to ironically attend a class on partial derivatives in our college. The attendance of that mathematics class was always sparse but that particular teacher, a good Hindu nationalist, was one of the few who had a soft-corner for us; hence, we saw it as an obligation to return the favor by sitting in his class. As we waited for the class to begin we stared out from terrace of the mathematics department into an open patch of untamed wild life that still existed in those days of our youth. That patch often filled us with a remarkable visions of natural selection action. But that day had begun early for us and we lapsed into a hypnogogia: We witnessed two visions. One was somewhat tame: We found ourselves on an endless staircase, with a serially increasing integer on each step. We kept walking up that staircase much like the escalator at the train station next to our workplace. We kept walking and walking but the stairs and the numbers on them never seemed to end. If we looked back we could see the stairs endless leading down to a great dark abyss whose bottom could not be seen. If we looked up the stairs endlessly led to the realm of skyblue light that never seemed closer how much ever we walked. Our legs seemed to hurt but with manly intent we labored on and on with no respite. It was then that we realized that this is how the ordinary infinity of arithmetic manifests --- the endless realm of numbers with neither a beginning nor an end. If the old ज्ञानात्मन् भास्कर compared it to विष्णु, the vision that came to us was that of the शैव-s --- of manifestation of their prime god Rudra as the लिङ्गोद्भव-मूर्ति. Suddenly we snapped out of that vision. A new vision filled our eyes. We saw a stairway in the distance with an oddly distributed size-pattern for the stairs it contained. We looked at it more closely and each stair seemed to be made up of smaller stairs arranged like the whole stairway itself. Now when we looked at each stair of those miniature stairways on a single stair of the stairway we saw more miniature stairways in them. Our vision kept zooming and there was a whole in every whole and whole within it. The physicality of the vision faded and it looked more and more like a plot of a peculiar 2D mathematical function. We raked our minds to think what might be the equation of such a function but it evaded us. Since, after all a mathematics class lay before us, we asked the teacher after it was over if he might have some suggestions. He lent us a Russian book and asked us to see if might help us in our quest.

With aid from that volume we arrived at the function known as the Devil's staircase. Our versions of it were slightly different from the traditionally defined one which we learnt off. Let us define various  $\textrm{Dscf}(x)$  thus:

 $$\textrm{Dscfa}(x)=\displaystyle\sum_{n=1}^\infty\dfrac{\lfloor nx\rfloor}{a\^n}$ ; where  $a> 1$$ 

The classic  $\textrm{Dscf}(x)$  which we encountered in books is based on this version where  $a=2$ ,

 $$\textrm{Dscf2}(x)=\displaystyle\sum_{n=1}^\infty\dfrac{\lfloor nx\rfloor}{2\^n}$$ 

We also defined other versions for  $a=\phi$  where  $\phi$  is the Golden ratio. Finally, we also define a variant version of the function  $\textrm{Dscr}(x)$  where we used the round function  $\lfloor x \rceil$  which rounds to the nearest whole number instead of the floor function in the above definition. This was what we used in our original definitions because the  $\lfloor x \rceil$  was the intuitive way of producing the infinite staircase:

 $$\textrm{Dscfr}(x)=\displaystyle\sum_{n=1}^\infty\dfrac{\lfloor nx\rceil}{a\^n}$ ; where  $a> 1$$ 

![Devil_Dscf2](https://manasataramgini.files.wordpress.com/2017/03/devil_dscf2.png){width="75%"}

In figure 1 we illustrate some of these functions which clearly showing the part as a whole concept inherent in these functions. A remarkable result pertaining to one of these functions is,

 $$\textrm{Dscf2}(\phi)=2+\cfrac{1}{2^{F_0}+\cfrac{1}{2^{F_1}+\cfrac{1}{2^{F_2}+\cdots}}}$$ 

where  $F_{0:n}= 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55...$ , i.e. the meru-prastha series.

The exploration of  $\textrm{Dscf}(x)/\textrm{Dscr}(x)$  led us to another remarkable function we learnt of in an obscure pamphlet of mathematical illustrations (sadly whose name I forget by noted Indian mathematician and politician: Badri Nath Prasad). This function was discovered by the famous mathematician Hermann Minkowski, the teacher of Albert Einstein. Minkowski was among those advocating, what we as heathens would term, a return to "Pythagorean" thinking by emphasizing mathematical beauty as a guiding principle for discovery of new physics. Fittingly, in his study on the geometry of numbers he presented the discovery of the question mark function,  $\textrm{?}(x)$ , which maps quadratic surds on to rational numbers! Let the number  $x$  be defined as a continued fraction,\
 $x=a_0+\cfrac{1}{a_1+\cfrac{1}{a_2+\cfrac{1}{a_3+\cdots}}}$  then,

 $\textrm{?}(x)=a_0+2\displaystyle\sum_{j=1}\^n\dfrac{(-1)^{j+1}}{2^{a_1+a_2+a_3...+a_j}}$  where  $n$  is a finite number if  $x$  is rational and  $n=\infty$  if  $x$  is irrational.

![question_mark_function](https://manasataramgini.files.wordpress.com/2017/03/question_mark_function.png){width="75%"}(x)$  has a structure similar to Devil's staircases.

When our code plotted out Devil's staircases and  $\textrm{?}(x)$  successfully for the first time our vision had become a प्रत्यक्ष and the apprehension of third type of infinity infinity was under way. This infinity, distinct from that of calculus and algebra, was one which embodied each part of the whole being a whole in itself. Thus, it seemed to be a mathematical reflex of the above mantra from the शुक्ल-yajurveda. While these mysterious functions were now प्रत्यक्ष on our computer screen, thereby bringing into the apprehension of a lesser mortal like us what was only previously accessible to the mathematician, the question that confronted us was whether such an infinity had a place in the real world.

It immediately caught our attention that we could convert these monotonically increasing functions to periodic or quasi-periodic functions (Such a thing had been independently done by others for  $\textrm{?}(x)$ ). For the Devil's staircases we centered the functions on 2, which resulted in a saw-tooth function. Thus we get:

 $$\textrm{Ddiff2}(x)=2\textrm{Dscf2}(x)-4x+1$$ 

 $$\textrm{Ddifr2}(x)=2\textrm{Dscr2}(x)-4x+1$$ 

We can likewise define attenuated functions like,\
 $\textrm{Ddiff2},\phi(x)=2\textrm{Dscf}\phi(x)-4x+1$ , etc\
For Minkowski's function we can define the periodic version simply as:

 $$\textrm{?dif}(x)=\textrm{?}(x)-x$$ 

Now when we use these functions to define a polar curve e.g.  $r=a\cdot\textrm{Ddiff2}(\theta)$  then we get the below fractal forms of great interest:

![devdiff_floor.png](https://manasataramgini.files.wordpress.com/2017/03/devdiff_floor.png){width="75%"}(\theta); 0 \le \theta \le 3\pi$ 

 

![Devdiff_round](https://manasataramgini.files.wordpress.com/2017/03/devdiff_round.png){width="75%"}(\theta); 0 \le \theta \le 2\pi$ 

![qndiff_minkowski](https://manasataramgini.files.wordpress.com/2017/03/qndiff_minkowski1.png){width="75%"}(\theta); 0 \le \theta \le 7$ 

![devattenuated_phifloor](https://manasataramgini.files.wordpress.com/2017/03/devattenuated_phifloor.png){width="75%"},\phi(\theta); 0 \le \theta \le 3\pi$ 

One notes that these forms bear resemblance to those that appear in nature in jagged leaves and petals (e.g. *Dianthus*), in inflorescences and petals. Thus, it appears that the biological processes that generate these natural forms are in some way trying to capture the geometry predicated by the nesting of wholes within a whole. However, as on the computer screen the biological forms are by no means totally scale-free facsimiles of the mathematical objects they resemble. The nesting has a certain lower bound beyond which the structure changes. Thus, in a sense they are approximate real world reflections of the Platonic forms of the third type of infinity --- that of infinite nesting. While the actual biological process that result in the form might differ from case to case and the mechanistic process might not involve constructing and plotting a  $\textrm{Ddiff2}(x)$  or a  $\textrm{?dif}(x)$ , the forms nevertheless converge to those dictated by such functions. Thus, this type of infinity, even more so than others, might impinge upon the real world. Indeed, perhaps, याज्ञवल्क्य wished to capture the Platonic ideal behind such forms when he recited the above mantra of the वाजसनेयिन्-s. The realization of this infinity has since then had a deep influence on our way of looking at the world and experiencing beauty.

