
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Sine rugs](https://manasataramgini.wordpress.com/2017/05/27/sine-rugs/){rel="bookmark"} {#sine-rugs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 27, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/05/27/sine-rugs/ "9:15 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Consider a square lattice with uniform vertical and horizontal spacing of a quantum  $q$ . This can be represented as an array of complex numbers of the form:  $z=n\cdot q+n\cdot q\cdot i; \; i=\sqrt{-1}, n \in \mathbb{Z}$ . For our purposes we chose  $n=-10:10$ . Thus the lattice comprises of all the numbers of the above form in the square on the complex plane defined by the left bottom corner  $-10q -10q \cdot i$  and right top corner  $10q+10q \cdot i$ . Now to each of these points we apply the following operation:  $c=\sin(z\cdot \overline{z})$ . Cosine may also be used. We convert  $c$  into a color value based on a predefined palette and assign it to the corresponding point on the lattice. We then remap this lattice on to the unit square. This last step normalizes all lattices created using different quanta  $q$  to a square of constant size. The result are "rugs" colored based on the palette we choose. The rugs show a 4-fold mirror symmetry. One may notice the forms produced here in several cases resemble the floor plans with pillar positions of Hindu temples

![3_rug](https://manasataramgini.files.wordpress.com/2017/05/3_rug.png){width="75%"}A rug based on quanta 3 to 3.75

![.5_rug](https://manasataramgini.files.wordpress.com/2017/05/5_rug.png){width="75%"}

A rug based on quanta from .5 to 3.5 with a different color scheme

![golden_Convergents](https://manasataramgini.files.wordpress.com/2017/05/golden_convergents.png){width="75%"}

A rug based on 4 successive convergents of the Golden ratio: 8/5, 13/8, 21/13, 34/21

![e_convergents](https://manasataramgini.files.wordpress.com/2017/05/e_convergents.png){width="75%"}

A rug based on 4 successive convergents of e

