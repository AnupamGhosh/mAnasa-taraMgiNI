
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [चित्रभानु's cubes](https://manasataramgini.wordpress.com/2017/09/22/citrabhanus-cubes/){rel="bookmark"} {#चतरभनs-cubes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 22, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/09/22/citrabhanus-cubes/ "7:14 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Hindus unlike their yavana cousins preferred algebra to geometry. Yet on occasions they could indulge in geometric games for demostrating proofs of algebraic relations. We see a bit of this in the आर्यभट school and the great भास्कर-II, but one of the masters of the self-evident geometric demonstration was चित्रभानु the नम्बूतिरि ब्राह्मण poet, mathematician and astronomer from the cera country (1500s). His student (also that of one of India's greatest pre-modern scientists नीलकण्ठ सोमयाजिन्) was the ritual assistant शंकर वारियार्. In his क्रियक्रमकारी he records some of these self-evident proofs of his teacher. Now one such question चित्रभानु poses is to show without any algebra that:

 $$\dfrac{a^3-b^3-(a-b)^3}{3(a-b)}=ab$$ 

Of course, this is simple 7th class algebra in our old school system. My father probably taught it to me while I was in 4th class. Yet it is some fun to show without using any algebra at all.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/chitrabhanu.png){width="75%"}
```{=latex}
\end{center}
```



चित्रभानु has several such 'wordless' proofs that delve into figurate numbers. Of course this was not a late medieval innovation suddenly dawning on चित्रभानु. It has a long Hindu tradition going back to the आर्यभट school and from there to figurate numbers of the Vedic ritual altar. Thus, it is another example of the continuity of Hindu science with the Vedic tradition of which चित्रभानु, like his coethnics, was a major practitioner. Finally, we may note that चित्रभानु seems to have had a particularly fertile abstract conception of higher dimensional space in the context of figurate numbers and what we show above is only the simplest of algebra.

