
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [loka-नीति-चर्चा](https://manasataramgini.wordpress.com/2017/01/22/loka-niti-carca/){rel="bookmark"} {#loka-नत-चरच .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 22, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/01/22/loka-niti-carca/ "5:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

pdf in देवनागरि ([loka-नीतिचर्चा](https://manasataramgini.files.wordpress.com/2017/01/lokaniticharcha1.pdf "lokaniticharcha"))

vijaya-नाम महा-म्लेच्छानाम् बहुप्रजावान् बहुपत्नीवांश् ca व्यापारी गृह-क्रयाच् छैलूष-प्रदर्शनाच् ca महाधन्य् abhavat । sa मरून्मत्तैर् अभिभृतां पूर्वतन-mleccha-राज-पत्नीम् अतिक्रान्त्वा राज-निर्वाचनम् ajayata । so 'bhavat म्लेच्छाधिपतिः । विरोधकास् tasya+अनेकाः । tasya vijayasya ca पट्टाभिषेकस्यानन्तरं वृष्ट्य्-ante पुत्तिका ivo(u)tplavante बहवः संक्षोभकाः । संक्षोभकेषु गणेषु santi शैलूष-पुंश्चल्यः क्लीबाः स्त्रीवादिनी-striya आपण-भञ्जका नियमोल्लङ्घ-vadino 'pasavya-मार्गाध्यापका वार्त्ता-विक्रयिकाश् ca (preto राक्षस-putrasya+अनुषङ्गिनः सदृशा आसन्) । te sarve sva-तन्त्रेण alpa-विवेकिनः । tatas te कैश् codayanti ca विधीयन्ते ce(i)ti प्रश्नः । kecid vadanti so'sti soro-नाम-महाधनी+iti । sa bejho-नाम+antar-जाल-महाव्यापारी+ity anye vadanti । एतान्य् उत्तराणि प्रयेण समुचितानि किं tu गूढं विषयं na स्पृशन्ति ॥

iyam asya विषयस्य मीमांसा । विजयोपनाम-व्यापारी केषां प्रतिनिधिः ? sva-dhana-वर्धनञ् ca sva-सामर्थ्य-भोगश् ca tasya प्रधाने लक्ष्ये । परं tu प्रधाना अनुयायिनस् tasya santi श्वेत-जात्य्-आधिपत्यवादिनश् ca व्यतिरोपिताः श्वेत-कर्मकराश् ca विशिष्टाः प्रेतसाधकाश् ca । कस्मात् कारणात् te sarve विजयं व्यापारिणं pratisevante? म्लेच्छानां madhye vartate ekaika-राक्षस-नाम-संप्रदायः । प्रथमा अनुयायिनस् tasya+आङ्ग्ल-देशस्य होलदेशस्य मुलैकराक्षसवादिनश् ca सामर्थ्य-साधकाः । महाम्लेच्छवर्षस्य विस्तारानन्तरं यूरोपात् tasmin देशे जग्मुः । te jana-भावेभ्यो राष्ट्र-सीमाभ्यो देशानुसरेभ्यश् ca na cintayanti व्यवसायिनो 'र्थसाधकाः । तेषाम् एकराक्षस्वत्वं मतविहीन-वेषं वा (आङ्ग्लानुसारेण "secularism" iti) apasavya-मार्ग-वेषं वा गृहीत्वा जनानां madhye प्रविशति । etasya matasya भाणकानां कर्मभ्यः साधारणा जनाः kupyanti । te dhana-क्षयं कुटुम्ब-क्षतिं समाज-भङ्गं मूल्य-नाशम् anubhavanti । तस्मात् प्रधानात् एकैकराक्षसत्व-विशिष्टैकराक्षसत्वयो रणम् अजायत (विशिष्टैकराक्षसत्वेषु पुराणा भेदास् santi) । tatas te prati-योद्धारं मृग्यन्ति । vijayo व्यापारिणो bhavena janasya कोपं sulabham अभिज्ञात्वा naya-योजनां प्रायुङ्क्त । अनयञ् जना विजयं जयाय ॥

सनातन-deva-साधकेभ्यो किं mahattvam एष-विषयस्य ? ekaika-राक्षसत्वम् अन्यान्य् एकराक्षसानि+iva deva-dharmasya रिपुः । "Secularism" iti मार्गेण dharmasya नाशम् अन्विषति । sa सर्वोन्माद-samyukta-मतविहीन-वेष-भृत्-भूतः । एष hi मार्गो dharma-विरोधाय mleccha-मरून्मत्ताभि-सन्धेः ॥

पश्य पश्य सर्वोन्माद-समायोग-पुरुषः । tasya प्रथमोन्मादैव शिरः । bilmy अनब्रह्मणा ca अमहामदेन ca गुप्तः । tatra vartate अब्रह्मा-नामोन्मत्त-janakasya दुष्टः सिद्धान्तः । प्रेतोन्माद+आदधाति tasya निगुढं dharma-निषूद्यमानं kalevaram । राक्षसोन्मादस् tasya बाहू दारुणव् asi-धारिणौ । महामदस्य वाक्यानि bhavanti tasya हस्तघ्नाव् अङ्गुलित्रे ca । रुधिरोन्मादो दृढः kavaco दात्र-मुद्गराभ्याम् भूषितः । अधोभागस् tasya जनानां मन्दता । upahata-jana-वृद्धिस् tasya महामुष्कौ । tasmin rakte vahati मूष-नामोन्मत्तस्य kutarka-पूर्णो दुरागमः । sa piparti नवीकरोति ca सर्वोन्मादांश् ca तेषान् त्विषश् ca । ayam eva deva-dharmasya ripur महान् । yady एतञ् जानाति tarhi सहस्राणां वर्षाणाम् अज्ञानं layati ॥


