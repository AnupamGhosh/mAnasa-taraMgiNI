
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The two squares theorem](https://manasataramgini.wordpress.com/2017/06/05/the-two-squares-theorem/){rel="bookmark"} {#the-two-squares-theorem .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 5, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/06/05/the-two-squares-theorem/ "5:53 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

I do not know who might have discovered this simple relationship first. I stumbled upon it while drawing figures in the notebook during a seminar.

Take any two squares such that they are joined at one side and the two sides of the respective squares perpendicular to the junction side lie along the same straight line in opposite directions. Join their corners as below. Then irrespective of the size of the squares points A, B, C and D are collinear.

![2_squares_theorem_proof](https://manasataramgini.files.wordpress.com/2017/06/2_squares_theorem_proof.jpg){width="75%"}

Figure 1

This can be easily proved as below by placing the origin of the x-y coordinate axis at the lower left corner of the left square. Let  $a$  be the side of the left square and  $b$  the side of the right square.

![2_squares_theorem_proof](https://manasataramgini.files.wordpress.com/2017/06/2_squares_theorem_proof.png){width="75%"}Figure 2

  - Then we can use the coordinates of the corners of the squares to obtain the equations of the 5 lines as shown in the figure.

  - Solve  $y=x$  and  $y=-x+a+b$  to obtain the coordinates of point B as,

 $\left(\dfrac{a+b}{2},\dfrac{a+b}{2}\right)$ .

  - Solve  $y=\frac{b}{a}x$  and  $y=-\frac{a}{b}x+\frac{a^2}{b}+a$  to obtained the coordinates of point C as

 $\left(\dfrac{a^2(a+b)}{a^2+b^2},\dfrac{ab(a+b)}{a^2+b^2}\right)$ .

  - These points satisfy  $y=\frac{b-a}{a+b}x+a$ . Hence, A, B, C, D are collinear.

  - The angle between two lines with slope  $m_1, m_2$  is,

 $$\theta= \arctan\left(\dfrac{m_1-m_2}{1+m_1m_2}\right)$$ 

Thus, the two intersections shown in the figure are always at right angles because  $\frac{\pi}{2}=\arctan(\infty)$ .

  - Hence, by the cyclic quadrilateral rule the top corners of the two squares on the junction sides define the diameter  $d=a-b$  of a circle on which points B and C lie.

Now we see that the height (and width) of point B is the arithmetic mean of the sides of the two squares. But is there any significance to point C? From above it can be seen that the ratio of its coordinates is  $\frac{a}{b}$ . As can be seen in the figure below it turns out that the coordinates of C are respectively ratio of the volume of two cuboids with sides  $(a,b,a+b)$  and  $(a,a,a+b)$  to the sum of the areas of the two squares. The ratio of the volumes of the cuboids is  $\frac{a}{b}$ , i.e. ratio of the coordinates of C.

![2_squares_theorem_vol.1](https://manasataramgini.files.wordpress.com/2017/06/2_squares_theorem_vol-1.jpg){width="75%"}

