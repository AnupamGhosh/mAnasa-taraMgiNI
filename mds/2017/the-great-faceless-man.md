
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The great faceless man](https://manasataramgini.wordpress.com/2017/01/29/the-great-faceless-man/){rel="bookmark"} {#the-great-faceless-man .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 29, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/01/29/the-great-faceless-man/ "6:54 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the late Yajurvaidika उपनिषत्, the श्वेताश्वतर, which is the foundational text of the शैव-शासन, the god Rudra is described thus:

[न तस्य प्रतिमा अस्ति यस्य नाम महद् यशः।]{style="color:#0000ff;"}\
There is no one who his equal, whose name \[itself] is great fame.

This sentence has also been taken to organically imply something else among Hindus too: Statues are not made of the great people -- their name itself is great fame. Keeping with this we mostly do not have statues of many of the great figures of Hindu tradition. For instance, we do not know how बोधायन or आपस्तम्ब or आश्वलायन or पैप्पलाद looked, though we take their names on a daily basis. So also with great men even closer to our times, like say वाचस्पति मिश्रा. Now, speaking of other heroes, like राम ऐक्ष्वाकव, whose name might be almost taken daily in some of our households, we have some kind of a description in the opening of the रामायण:

[इक्ष्वाकु-वंश-प्रभवो रामो नाम जनैः श्रुतः ।]{style="color:#0000ff;"}\
[नियतात्मा महावीर्यो द्युतिमान् धृतिमान् वशी ॥]{style="color:#0000ff;"}

Born in the इक्ष्वाकु clan, he is known among men by the name of राम. He is self-controlled, of great manliness, radiant, resolute, and has his senses under control.

[बुद्धिमान् नीतिमान् वाग्मी श्रीमाञ् शत्रु-निबर्हणः ।]{style="color:#0000ff;"}\
[विपुलांसो महाबाहुः कम्बुग्रीवो महाहनुः ॥]{style="color:#0000ff;"}

He is intelligent, politically astute, eloquent, opulent, and an extirpator of foes. He is broad-shouldered, of mighty arms, with a conch-like neck, and strong-jawed.

[महोरस्को महेष्वासो गूढजत्रुर् अरिंदमः ।]{style="color:#0000ff;"}\
[आजानुबाहुः सुशिराः सुललाटः सुविक्रमः ॥]{style="color:#0000ff;"}

His chest is broad, he is a great archer, his collar-bones are well-concealed, and is a suppressor of foes. With arms reaching up to his knees, with a good head, shapely forehead and good gait.

[समः सम-विभक्ताङ्गः स्निग्ध-वर्णः प्रतापवान् ।]{style="color:#0000ff;"}\
[पीनवक्षा विशालाक्षो लक्ष्मीवाञ् शुभ-लक्षणः ॥]{style="color:#0000ff;"} 1.1.8-11

His body is well-proportioned, he is of smooth complexion and mighty. His chest is rounded, his eyes large, he is prosperous and with auspicious marks.

Similar accounts might be found elsewhere in the रामायण too. One thing which comes out of this account is that it is fairly generic for a mighty क्षत्रिय except for one specific, unusual feature namely "आजानुबाहुः" -- i.e. that his arms reached down to his knees, which might have been a peculiar characteristic of the man himself. Thus, while one can build a generic image of emperor राम as a mighty क्षत्रिय, we can still say we do not know how he \*exactly* looked. Now, this is in part keeping with a the broader issue we have discussed earlier, [namely the iconic depictions of deities among the early Hindus](https://manasataramgini.wordpress.com/2008/02/23/early-temples-and-iconic-worship/). As we argued before such existed but were not prominent and were perhaps "primitive" keeping with the archaeological evidence from several early Indian sites. In this sense the Indian iconography mirrored the primitivism of the early Greek iconography.

This is in stark contrast to Egypt where their great Pharaohs are known more from their portraits rather than epic narratives. When we see the images of the lordly Pharaohs, while stylized, there is clearly an element of individuality behind them. Over the ages of its heathen existence, in addition to statues, Egypt developed an even more realistic portraiture in other media. It is conceivable that this Egyptian tradition of portraiture spread through West Asia and then Europe influencing other cultures, first Semitic and then Indo-European. Thus, we see it emerge first among the Hittites to some extent and then eventually among the yavana-s (here collectively Greek and Macedonian) and रोमाक-s. Thus, by the time of the Macedonian invasion of India we we see a vigorous tradition of realistic royal portraiture on their coins, medallions and mosaic work that has moved far away from their ancestral primitivism. Early Indian coinage was abundant in symbolism and even primitive iconography of Hindu deities but not royal portraits. However, in the years following the Macedonian attack we see an emergent tradition of such portraiture both on coins and in the form of larger icons as seen in the case of अशोक, the mighty Maurya. This trend would suggest that the impulse for portraiture eventually reached the Indian world only via the Macedonians. Likewise, there have also been plausible suggestions that the movement of yavana-s eastwards along with contacts with the now fully portrait-using Hindus sparked the emergence realistic portraiture among the चीन-s along side the unification by the Chin. This manifest burst upon the scene on a truly चीन scale in the funerary statuary of the Chin conquerors.

But is this the whole story? The mysterious Harappan civilization of India produced rare but striking pieces of realistic human portraiture. The most iconic of these is the famous bearded man with a trefoil-patterned garment. There are also several terracottas but these generally retain the primitivism typical of the Indo-European productions. Thus, while portraiture existed in the Harappan world, it was certainly rare relative to the coeval linked civilizations of West Asia and Egypt. So in this sense the Harappan civilization was rather in line with the Indo-European world (though we do not know right now if they had any Indo-European component or influence at all). What about the Indo-Aryan literature? There is nothing indicating a tradition of realistic human iconography in श्रुति. The इतिथास-s too leave us with at best with slim pickings in this regard -- e.g. we hear of the iron image of भीमसेन used by Duryodhana for his mace-practice but this need not have been realistic. However, in the पौराणिक tradition as it has come down to us today we see that portraiture had a vigorous and full blown expression. In between we have पतञ्जलि who explains a सूत्र in the अष्टाध्यायि by stating that statues of the sage काश्यप were made. Keeping with this, the early surviving वास्तु text, the मानसार describes how the statues of the the seven great brahminical sages were made. We have evidence from the Skanda-सद्भाव tantra that indeed such statues were made an installed in ancient कौमार temples. We even hear of a statue of पानिनि that was there in home town, which has evidently been destroyed by the Mohammedans sometime after Xuan Zang recorded seeing it in the 600s of CE. Importantly, despite the potential role of a yavana impetus, the Hindu style remained conservative and distinct, even in places of closest proximity to the yavana in the Northwest of the subcontinent. Hence, on the whole we have some evidence that Hindus had an independent early tradition of portraiture though much of that has not survived and/or they did not emphasize it in any serious way.

However, it does remain a fact that the explosive spread of \*extant* portraiture mostly post-dates the Macedonian invasion, which could imply a causal link between the two. Of this most traces have been erased outside the peninsular tip by the evil hand of the Army of Islam. In Nepal however some early specimens survive such as the image of king Jayavarma-deva from शक 105= 185 CE. In a 3 century time window from that point we also see traces portraiture among the शुङ्ग-s, Andhra-s, and Iranic rulers (कुषाण, पहालव, शक) in India. In discovering and describing the image of king Jayavarman of Nepal, Tara-ananda Mishra has given an excellent account of the evolution of portraiture including several points we have independently arrived at. Other than royal portraiture there appears to have been a vigorous tradition of the imagery of persons endowing religious images and constructions. Moreover, शैव teachers of both the mantra-मार्ग and अतिमार्ग, extraordinary शैव and वैष्णव devotees, and siddha-s were also prominent objects of whole-body portraits. Statues of such were once common throughout India but have been mostly damaged or demolished in the north. Thus, in south India we can still see statues of emperor कृश्णदेव or Govinda दीक्षित but we do not have the original image of the great Bhoja-deva or ललितादित्य.

Yet, despite all of this, on the whole the the majority of our great figures have not survived in portraiture, perhaps indicating a real Hindu tendency for the statement: [न तस्य प्रतिमा अस्ति यस्य नाम महद् यशः।]{style="color:#0000ff;"}. We too seem to be resonant with this Hindu tendency and take it in a broad sense as was possibly understood by many of our greats of the past. In large part we believe that a man's visual image or for that matter several details of his life should not matter at all. All that should matter are the words he leaves behind -- do you find something in them or not -- that's all. Indeed, this is how it is for आर्यभट-I or भास्कर or नीलकण्ठ सोमयाजिन्. They are their words not their portraits or even biographies. Now one may ask: "Have you not said there is great value in studying the biographies of past intellectuals?" I still hold that view but have always held that not all of their biography really matters. Indeed most Hindu notables have left behind colophonic biographies that only stress the needful in the best case scenario. For instance, they are not shy of revealing their youthful genius, like भट्ट Jayanta recording his advanced grammatical research as a kid or mathematician-astronomer गणेश recording his discoveries like the hyperbolic approximation of trigonometric functions in his teens or वटेश्वर his discovery of mathematical recipes for astronomy in his twenties.

This contrasts what we observed among the mleccha-s today and its back-flow to the Hindus. The person, his appearance and certain incidental aspects of his biography (e.g. ones sexual escapades or sexual orientation) seem to matter a lot. Indeed, the appearance of a person has a serious correlation to his/her intellectual influence especially in a लोकाभिमुख sense. There is a drive to get people who look acceptable in a mleccha sense as the face of even an intellectual matter, like physics. I was amazed by how certain भारत-s known to me converged to a similar style of appearance after hitting the talking circuit in the mleccha world. More than a person's intellectual substance what matters is that they lie at one extreme of the bell-curve in terms of the mleccha-defined (partly universal) measure of appearance, appeal, etc. Almost as though out of guilt, the mleccha-s would then balance it with a few chosen tokens from the other extreme of the normal distribution (e.g. the diseased, the handicapped and the rare ethnicities) and pepper it with elements of what matters in the mleccha facade: e.g., aberrant sexuality. Finally, in the biographic sense, often what the person holds on matters well-beyond his/her expertise is of great importance in his/her projection as an icon: s/he better mouth liberal platitudes along the lines of how they are out to save the world, save the disadvantaged, and other feel-good messages quite removed from reality and sometimes even biology all while looking 'cool' at the same time. This would have even been half-understandable where it not for the concomitant insistence of the mleccha system being "equal opportunity".

In face of this facade we think great father Manu had a point -- the ब्राह्मण intellectuals are best off being low-key in such dimensions -- the faceless intellectual whose biographic peculiarities matter little. In conclusion, perhaps keeping with the Hindu tepidity towards portraiture, we subscribe to the view that it is better for a man's words than his portraits to linger on after Vaivasvata takes him away -- indeed what use is it to be like the mysterious ब्राह्मण from what is today Afghanistan, the fragment of whose portrait is placed for auction in a mleccha market.


