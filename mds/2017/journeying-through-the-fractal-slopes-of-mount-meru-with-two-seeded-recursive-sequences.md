
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Journeying through the fractal slopes of mount Meru with two-seeded recursive sequences](https://manasataramgini.wordpress.com/2017/06/26/journeying-through-the-fractal-slopes-of-mount-meru-with-two-seeded-recursive-sequences/){rel="bookmark"} {#journeying-through-the-fractal-slopes-of-mount-meru-with-two-seeded-recursive-sequences .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 26, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/06/26/journeying-through-the-fractal-slopes-of-mount-meru-with-two-seeded-recursive-sequences/ "6:42 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The Hindus have been fascinated by sequences and series from the beginning of their civilizational memory recorded in the Veda. This continues down to the medieval mathematician नारायण पण्डित, who discovered a general formula (सामासिक पङ्क्ति) that can be to obtain the 'Meru-श्रेधी' (known in the west as Fibonacci's sequence). He uses it as a model to explain the population dynamics of cows. In modern terms the formula goes thus:\
Let  $f\lbrack 1\rbrack =f\lbrack 2\rbrack =1$ . These are two seeds of the sequence. Then,\
 $f\lbrack n\rbrack =f\lbrack n-1\rbrack +f\lbrack n-2\rbrack$ , where  $n=3,4,5...$ .\
One sees that this yields the famous sequence: 1, 1, 2, 3, 5, 8, 13, 21, 34, 55... i.e. a version of नारायण's Dhenu-संख्य or cow-numbers. We may denote this special sequence as  $M\lbrack n\rbrack$ . It is well-known that,\
 $\displaystyle \lim_{n \to \infty} \dfrac{M\lbrack n+1\rbrack }{M\lbrack n\rbrack }=\phi$ , where  $\phi$  is the Golden ratio.\
[Thus, as we have illustrated before,](https://manasataramgini.wordpress.com/2016/10/04/some-meanderings-among-golden-stuff-2/) the Dhenu-संख्य can be obtained as integer values of a function based on  $\phi$ .

Closer to our times, following the discovery of [a everywhere continuous but nowhere differentiable function by Bernhard Riemann](https://manasataramgini.wordpress.com/2017/03/26/some-visions-of-infinity-from-the-past-and-our-times/), the Japanese mathematician Teiji Takagi discovered another remarkable curve of this type. Let function  $s(x)$  be defined as,

 $$s\left(x\right)=\min \left(\left(x-\lfloor x \rfloor \right),\textrm{abs}\left(x-\lceil x \rceil \right)\right)$$ 

Then the Takagi function is defined as,\
 $f\left(x\right)=\displaystyle \sum _{n=0}^\infty w\^n \cdot s\left(2\^n x \right)$  Parameter  $w=0.5$  gives an aesthetically pleasing curve. As can be seen from the figure the curve as has fractal form keeping with its undifferentiable nature
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure1_takagi_curve.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad The Takagi curve (red)
```{=latex}
\end{center}
```


A lay person may wonder why we are mentioning these two seemingly disparate pieces of mathematics together. The connection between them becomes apparent via the remarkable sequences discovered by the scientist-philosopher Douglas Hofstadter. He first presented these ideas in his curious book 'Gödel, Escher, Bach: An Eternal Golden Braid'.

Our own journey through these began in the days of our youth when we chanced upon Hofstadter's book in a book-store. Not having the cash to procure it we spent sometime taking in its braided ideas right there. While that encounter was not enough to take in all the sequences he discussed in the book we got enough of the basic idea to experiment by ourselves. The basic idea behind the Hofstadter class of sequences is a generalization of the procedure behind the Meru-श्रेधी. This can be illustrated with the sequence with which we first experimented:

 $$f\lbrack n\rbrack =n-f\lbrack f\lbrack n-1\rbrack \rbrack$ , where  $f\lbrack 1\rbrack =f\lbrack 2\rbrack =1$  and  $n=3,4...$$ 

Like  $M\lbrack n\rbrack$  it is also initiated with two seed values but the definition of the subsequent elements involves a nested definition. The definition itself looks simple and yields the following sequence:\
1, 1, 2, 3, 3, 4, 4, 5, 6, 6, 7, 8, 8, 9, 9, 10, 11, 11, 12, 12, 13, 14...

One notices that unlike the Dhenu-संख्य-s this sequence covers all the positive integers. However, some are repeated multiple times. Interestingly, we observe that  $f\lbrack 3\rbrack =2, f\lbrack 5\rbrack =3, f\lbrack 8\rbrack =5, f\lbrack 13\rbrack =8, f\lbrack 21\rbrack =13$ . Thus more generally  $f\left \lbrack M\lbrack n\rbrack \right \rbrack =M\lbrack n-1\rbrack$ . Moreover, once we have this sequence we can also write out pairs of the form  $(f\lbrack n\rbrack ,n)$ . The set of such pairs can be represented as edges of a graph (i.e.  $f\lbrack n\rbrack \rightarrow n$ ), which turns out to be a tree.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure2_meru_tree.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad Graph of  $(f\lbrack n\rbrack ,n=1:50)$ 
```{=latex}
\end{center}
```


The tree displays an interesting structure which resembles an evolutionary process of descent through modification. In this we can recognize a "primitive" lineage which is the line of descent comprised of  $M\lbrack n\rbrack$ . The remain lines of descent branching off from it also display a Meru-श्रेधी process albeit each at a different level. For e.g. we have the line of descent: 3, 4, 7, 11, 18, 29, 47...

Our next advance in understanding these sequences came from reading a paper by Clifford Pickover, which presented a means of graphically visualizing the structure of these sequences. Inspired by this we used a slightly different graphical representation to study the structure specified by these sequences. One of the notable Hofstadter sequences is,

 $$f\lbrack n\rbrack =f\lbrack f\lbrack n-1\rbrack \rbrack +f\lbrack n-f\lbrack n-1\rbrack \rbrack$$ 

The technique we used to visualize the sequence is to subtract a certain factor proportional to  $n$  from the  $f\lbrack n\rbrack$  in order to render the values along the x-axis rather than as along the curve which the increasing  $f\lbrack n\rbrack$  follows. We call this rectification. Thus we rectify the above sequence by plotting  $(n, f\lbrack n\rbrack -\tfrac{n}{2})$ 
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure3_hofstadter_batrachion.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad  $(n, f\lbrack n\rbrack -\tfrac{n}{2})$  for  $f\lbrack n\rbrack =f\lbrack f\lbrack n-1\rbrack \rbrack +f\lbrack n-f\lbrack n-1\rbrack \rbrack$ 
```{=latex}
\end{center}
```


Remarkably we get ever larger copies of the Takagi curve, with each loop progressing in width as 2, 4, 8, 16, ... i.e. the powers of 2 (Figure 3). Thus, the curve captures the binary base representation of integers up to a give integer and the fractal has a predictable form. The successive loops of the structure also represent a journey into the fractal structure of the Takagi curve.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure4_mallows.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad  $f\lbrack n\rbrack =f\lbrack f\lbrack n-2\rbrack \rbrack +f\lbrack n-f\lbrack n-2\rbrack \rbrack$  rectified as  $f\lbrack n\rbrack -.68n$ 
```{=latex}
\end{center}
```


Another such sequence was discovered by Mallows (Figure 4), which has an overall similar behavior as the above sequence of Hofstadter. However, rather than the regular symmetric Takagi curve generated by the sequence it has a more jagged fractal curve with a distinct pattern of two peaks followed by three peaks.

Returning to the original sequence we studied,  $f\lbrack n\rbrack =n-f\lbrack f\lbrack n-1\rbrack \rbrack$ , we can rectify it using  $f\lbrack n\rbrack -\tfrac{n}{\phi}$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure5_hofstader_one_by_phi_doubly_recursive.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad  $f\lbrack n\rbrack =n-f\lbrack f\lbrack n-1\rbrack \rbrack$ 
```{=latex}
\end{center}
```


It has a very different structure from the above sequences. Instead of ever-increasing loops this is characterized by rapid but fixed bandwidth of oscillation with basic reoccurring units incorporated into higher order reoccurring units, reflective of its tree structure discussed above.

One would notice that the generative formulae for these sequences, unlike that of the Meru-श्रेधी, involve doubly nested specifications. Such specifications mostly lead to sterile sequences because the index  $n$  for a given  $f\lbrack n\rbrack$  can drop below 1 or above the current value thus killing the sequence. Nevertheless, there are several additional productive formulae beyond the above that lead to a range of interesting behavior. These are illustrated before.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure6_hofstadter_pseudo_repetitive.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad  $f\lbrack n\rbrack =f\lbrack n-f\lbrack n-1\rbrack \rbrack +f\lbrack n-f\lbrack n-2\rbrack -1\rbrack$  rectified using  $f\lbrack n\rbrack -\tfrac{n}{2}$ .
```{=latex}
\end{center}
```


Somewhat similar theme to the above case with similar looking motifs nested at multiple levels giving rise to a pseudo-repetitive appearance.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure7_hofstadter_pulsations.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad  $f\lbrack n\rbrack =f\lbrack n-f\lbrack n-1\rbrack \rbrack +f\lbrack n-f\lbrack n-2\rbrack \rbrack$  rectified using  $f\lbrack n\rbrack -\tfrac{n}{2}$ 
```{=latex}
\end{center}
```


This is one of Hofstadter's discoveries which has ever larger pulses of high intensity fluctuations of similar form separated by regions of low intensity fluctuations. The repetition of the same basic form albeit with variation at larger and larger scales resembles the formula generating the Takagi curve.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure8_hofstadter_sinusoid.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 8\quad  $f\lbrack n\rbrack =f\lbrack f\lbrack n-1\rbrack \rbrack +f\lbrack n-f\lbrack n-2\rbrack -1\rbrack$  rectified using  $f\lbrack n\rbrack -\tfrac{n}{2}$ 
```{=latex}
\end{center}
```


Another Hofstadter sequence similar to the above but the pulses themselves are arranged on waves of ever-increasing wave-length giving it the form of ever-larger sigmoids.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure9_hofstadter_wolfram_wavy.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 9\quad  $f\lbrack n\rbrack =f\lbrack f\lbrack n-1\rbrack \rbrack +f\lbrack n-2\cdot f\lbrack n-1\rbrack +1\rbrack$ .
```{=latex}
\end{center}
```


This is non-linear in terms of its central growth curve and cannot be perfectly rectified. Hence, the factor  $f\lbrack n\rbrack -.42n^{.818}$  is used. This was discovered by Stephen Wolfram in an excellent introduction to these sequences for a lay reader although he does little to acknowledge his predecessors' work. It shows a higher order wavy pattern of increasing wavelength but within it the pulsations show much more irregularity.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure10_hofstadter_random.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 10\quad  $f\lbrack n\rbrack =f\lbrack n-f\lbrack n-1\rbrack -1\rbrack +f\lbrack n-f\lbrack n-2\rbrack -1\rbrack$  rectified using  $f\lbrack n\rbrack -\tfrac{n}{2}$ 
```{=latex}
\end{center}
```


Unlike the above this shows neither regions of reduced intensity pulsation nor a higher order wavy pattern. Instead is simply shows a chaotic pattern of fluctuations which grow in magnitude with  $n$ .

If the above sequences were generated using doubly nested specifications we also find complex behavior emerging from triply nested formulations. We discovered for ourselves some such sequences which are illustrated below.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure11_triply_nested_-6823.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 11\quad  $f\lbrack n\rbrack =n-f\lbrack f\lbrack f\lbrack n-2\rbrack \rbrack \rbrack$  rectified using  $f\lbrack n\rbrack -0.6823n$ 
```{=latex}
\end{center}
```


This is a triply nested relative of the generative formula shown in Figure 5. Like it is shows a fixed bandwidth along with some basic motifs reoccurring at various scales.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure12_triply_nested_seahorse.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 12\quad  $f\lbrack n\rbrack =f\lbrack f\lbrack f\lbrack n-1\rbrack \rbrack \rbrack +f\lbrack n-f\lbrack f\lbrack n-2\rbrack \rbrack -1\rbrack$  rectified using  $f\lbrack n\rbrack -0.45n$ 
```{=latex}
\end{center}
```


This formula resembles the doubly nested version seen in Figure 8 and like it shows a higher order wave-like structure with increasing wavelength. Each wave module is a "sea-horse"-like structure, which develops an increasingly complex pattern of fluctuations as it grows larger in size.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure13_mt_meru.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 13\quad  $f\lbrack n\rbrack =f\lbrack f\lbrack f\lbrack n-1\rbrack \rbrack \rbrack +f\lbrack n-f\lbrack f\lbrack n-1\rbrack \rbrack \rbrack$  rectified using  $f\lbrack n\rbrack -\tfrac{n}{\phi}$ 
```{=latex}
\end{center}
```


This triply nested formula shows a link to the Meru-श्रेधी and  $\phi$  similar to the one explored in Figure 5. The rectified form shows cycles of increasing size in the sequence 2, 5, 13, 34 ... i.e.  $M\lbrack 2k+1\rbrack ,\; k=1,2,3,4...$ . These cycles are a journey up and down a fractal mountain, which we term the journey through the slopes mount Meru. The mountain has some fractal faces and others which are sheer cliffs. Further, the set of edges  $f\lbrack n\rbrack \rightarrow n$  constitute a tree graph as seen with the above-described doubly nested version. Remarkably, there is one primitive lineage from which all other lineages branch off which is the Meru-श्रेधी with the Dhenu-संख्य-s (Figure 14). Now the branches also show a curious relationship to the Dhenu-संख्य-s: at any section through a given level in the tree we have that many branches as the numbers that would count up to the corresponding Dhenu-संख्य in that level. Thus at the level of first branch the numbers are 4, 5 (2 branches); at the next level the numbers on the branches are 6,7,8 (3 branches); at the next level 9,10,11,12,13 (5 branches) and so on. Thus on each branch there is an inherent relationship to the underlying Dhenu-संख्य, which can be seen in the figure. Whereas the graph in Figure 2 shows a strict bifurcation this graph shows a pattern for n-furcation related to the Dhenu-संख्य: if a non-primitive branch emerges from a number the  $M\lbrack n\rbrack$  then it will display  $n-3$ -furcation (Figure 14). Thus, the branch from  $M\lbrack 4\rbrack$ =3 will show 1-furcation; the branch from  $M\lbrack 5\rbrack =5$  will show 2-furcation; the branch from  $M\lbrack 6\rbrack =8$  with show 3-furcation and so on.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/06/figure14_meru_tree_triple_recursive.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 14\quad The branching graph for  $f\lbrack n\rbrack =f\lbrack f\lbrack f\lbrack n-1\rbrack \rbrack \rbrack +f\lbrack n-f\lbrack f\lbrack n-1\rbrack \rbrack \rbrack$ 
```{=latex}
\end{center}
```


One could explore other interesting features of these sequences but we stop here with the philosophical insight the imparted to us. Simple two-seeded sequences like the Meru-श्रेधी represent a process that is directly dependent on the state of the system at two former time points. Thus they evolve directly as a function of time like the unconstrained growth of an organism under ideal conditions, which नारायण tried to model. However, the doubly and triply nested generative formulae, while starting from the same seeds, do not develop directly dependent on time but rather as a second or third order consequence of states at former time points. Thus, while the simple case might be seen as a direct reaction of two former reactants leading to a current product, the nested specifications can be seen as reactants at former time leading to further reactants which in turn specify the current product. What we observer is that by the simple act of including this kind of higher order specification we get several different kinds of complexity: Some forms of complexity like the Takagi curve while intricate have a regular pattern to them. Other forms show different degrees of higher order pattern but are much more irregular in their immediate behavior. Finally there are those that are very irregular and not obviously predictable beyond some general statistical features. Actions on simple strings can generate complex forms --- hence, these sequences could serve as an analogy for how higher order dependencies naturally generates complex structure in nature. Such processes could also be behind the patterns of other systems like human history. A question that leads to a deep philosophical puzzle is whether such processes might be active in biological systems.

