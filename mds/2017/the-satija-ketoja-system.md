
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Satija-Ketoja system](https://manasataramgini.wordpress.com/2017/09/29/the-satija-ketoja-system/){rel="bookmark"} {#the-satija-ketoja-system .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 29, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/09/29/the-satija-ketoja-system/ "6:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Satija and Ketoja discovered an interesting dynamical system in course of the study of the Schrödinger equation for one electron in a two dimensional periodic lattice on a uniform magnetic ﬁeld. While this equation and its variants have several uses in physics, that is not our concern here. Rather, we shall purely focus on the neglected mathematical aspects of the Satija-Ketoja system in a form completely removed from its connections to quantum mechanics. To our knowledge, some of this has never been explicitly discussed. The Satija-Ketoja map is two dimensional map with a strikingly simple definition:

 $$x_{n+1}=\dfrac{1}{x_n-E+2\sigma\cos(2\pi\theta_n)}$$ 

 $$\theta_{n+1}=(\theta_n+\omega) \mod 1$$ 

Unless stated otherwise for all the experiments described here we recursively perform the above mapping for 100,000 iterations. Here, the parameter  $E$  stems from the eigenvalue of the underlying Schrödinger equation. It may be taken as 0 or for the purposes of the experiments shown here as 0.1 -- it is not of much consequence for our purpose. For the first experiment (Figure 1) we set the parameter  $\omega=e$ , i.e. 2.718281828.  $\theta_n$  the angle variable of the map changes by increments of size  $\omega$ . However, since we are taking modulo 1 of the value, our  $\theta_n$  axis is bounded between 0 and 1 and effectively projects the map on the surface of a cylinder of unit circumference. The parameter  $\sigma$  is then made to vary between .1 to 2. We find that as  $\sigma$  changes the map generates an attractor, whose "core" evolves from a simple closed curve (on the said cylinder; .1, .25); to a curve with one break (.5), to one with break and a separate branch (.75), to a strange attractor at  $\sigma=1$ . For  $\sigma> 1$  attractor becomes a many branched curve with trails of points (1.25, 1.5, 1.75, 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/satija_01.jpeg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


In our next experiment (Figure 2) with the S-K system we keep  $\sigma=1$  and set  $\omega$  to the following values:  $\phi$  (Golden Ratio),  $\sqrt{2}$ ,  $\sqrt{3}$ ,  $\sqrt\lbrack 3\rbrack {2}$ ,  $e$ ,  $\pi$ ,  $\sqrt{10}$ ,  $\tfrac{22}{7}$ ,  $\tfrac{355}{113}$ . The first 7 are irrational while the last two are rational and the well-known 1st and 3rd convergents of the continuous fraction expressions for  $\pi$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/satija_02.jpeg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Another way of visualizing this attractor is in a  $\theta_n$ -independent manner by plotting  $x_n$  and  $x_{n+1}$  (Figure 3) for the same values of  $\sigma$  as above. This reveals the strange attractor structure in a rectangular hyperbolic framework. This latter framework comes from form of the definition of  $x_{n+1}$  which involves a reciprocal relation with  $x_n$ 
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/satija_04.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


We notice that for all irrational values both these representations define complicated attractors. In the case of the  $x_n-\theta_n$  plots they are either of the form of a many branched curve with point trails or a strange attractor similar to that seen with  $e$  in our first experiment. However, the two rational  $\omega$  values produce "sparse" attractors of the form of a small number of attracting points. The closer the rational number is to the irrational number it approximates the denser is its attractor and the degree to which it recapitulates the structure of the irrational attractor (compare  $\tfrac{22}{7}$  to  $\tfrac{355}{113}$  and both to  $\pi$ ). However, two relatively close irrationals (e.g.  $\pi$  and  $\sqrt{10}$ ) have no particular similarity in their attractors.

While the modulo operator confines  $\theta_{n+1}$  to a cylinder of unit circumference, the hyperbolic framework for  $x_{n+1}$  in principle makes it unbounded allowing to take a value between  $-\infty :\infty$ . In the above plots we used all  $x_n$  within the range -4:4. But what is the entire range covered by  $x_n$ ? To see that simply we plot all  $x_n$  values obtained for each  $\omega$  in the above experiment (Figure 4).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/satija_03.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


This plot reveals a striking feature: For irrational  $\omega$  values  $x_n$  can explore a very wide range with rare values that are huge in magnitude. However, for rational values  $x_n$  remains bounded in a relatively small range. For example, using  $\omega=e$ , in our next experiment  $92.7\%$  fall within the range -4:4. It has a mean of 0.00551 and a median of 0.0326 but a large standard deviation of 102.38 and can explore values ranging from -14899.84 to 13963.58. Figure 5 shows part of this peculiar distribution.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/satija_06.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad the blue curve is the below Cauchy distribution
```{=latex}
\end{center}
```


This distribution is approximately symmetrical about 0 but has a paucity of values in the range from -0.5 : .5. Instead, it shows two peaks on either side of that zone of suppression. If we discount that central zone of suppression it shows an informal fit to a Poisson-Cauchy distribution (today simply called Cauchy) with a location of 0 and a scale-factor  $\gamma \approx .6$ . Thus, such a Cauchy distribution would be:

 $$f(x)=\dfrac{\gamma}{\pi}\left(\dfrac{1}{\gamma^2+x^2}\right)$$ 

Indeed, certain properties of our distribution are captured quite well by this Cauchy distribution. For example, for the above run of the S-K system we compare probabilities for different values being reached with Cauchy distribution:\
S-K system                             Cauchy distribution

 $$p(|x_n|> 100)=0.00253$       $p(|x|> 100)=0.00381$$ 

 $$p(|x_n|> 500)=0.00058$       $p(|x|> 500)=0.000763$$ 

 $$p(|x_n|> 1000)=0.00031$     $p(|x|> 1000)=0.000381$$ 

This suggests that the S-K system  $x_n$  values appear to have a "pathological" or "fat-tailed" distribution that violates the central limit theorem.

This resemblance of the Cauchy distribution has a connection to a hidden fractal structure in the S-K system. For this experiment we continue with the above run of the S-K system with  $w=e$ . In order unveil the fractal we have to perform the following Fourier transform. We treat the successive  $x_n$  values as a time series. Thus,  $x_1, x_2, x_3...x_T$  are values happening at time  $t=1,2,3...T$ . We then define a frequency  $k=\tfrac{1}{12}$ . Then for Tth  $x_T$  we can define the Fourier transform as:

 $$s(k,T)=\displaystyle \sum_{n=1}^ट् x_n e^{2\pi.k.n.i}$ , where  $i=\sqrt{-1}$$ 

Thus, for each  $x_n$  we get a complex number  $x+iy$  which can then be plotted to visualize the "walk" of the S-K system. In figure 6 we successively do it for 169, 625, 2500 and 4900 successive  $x_n$  values.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/satija_051.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


We observe that each of these numbers of steps captures a different scale. However, the walk has a qualitatively similar form across all scales establishing its fractality. We observe that in each case the walk has a large number of minuscule steps and a small number of huge saltations that are orders of magnitude larger than the minuscule steps and dominate the image. This pattern is indeed similar to what Benoit Mandelbrot defined as Cauchy flights, i.e. random walks where the steps follow a Cauchy distribution. This pattern is very different from what one would get from a Gaussian distribution of the steps. In the S-K system walks, if one discounts the suppression close to 0, the central events are more common and the infrequent events are extreme like in the classic Cauchy distribution.

Such distributions are what N.N. Taleb emphasizes in the context of the behaviors of financial markets. We have been attracted to them as potential analogical models (and perhaps eventually a real models) for various biological phenomena: 1) The movement of nucleic acid-binding proteins on DNA or long RNAs when measured with respect to their position along the nucleic acid polymer could display such saltations when they are transferred to distant sites by polymer looping. 2) We also believe that such saltations might capture evolutionary explorations of a certain morphospace. The morphological diversification usually is around certain fixed "types" or central tendencies but there are rare events, which are more extreme that allow entry into a highly disparate part of the morphospace. This might give the pattern biological diversity we observe: many related species with small differences but a rather large gap between one such cluster and another such cluster of related species. At the higher level this is the reason why taxonomic units like phyla of animals or families of plants look meaningful. 3) Finally, it might capture extreme consequences of certain rare events such as the origin of certain highly destructive species like Homo. This last pattern is also an analogy to understand the consequences of rare but extreme historical events.

For another such unusual distribution one look at [our earlier article on the Hindu square root](https://manasataramgini.wordpress.com/2016/10/21/chaos-in-the-iterative-hindu-square-root-method-of-the-ga%e1%b9%87aka-raja/).

