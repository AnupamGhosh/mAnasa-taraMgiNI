
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A note on the asterisms forming the नक्षत्र-s](https://manasataramgini.wordpress.com/2017/07/01/a-note-on-the-asterisms-forming-the-nak%e1%b9%a3atra-s/){rel="bookmark"} {#a-note-on-the-asterisms-forming-the-नकषतर-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 1, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/07/01/a-note-on-the-asterisms-forming-the-nak%e1%b9%a3atra-s/ "7:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In Hindu tradition the ecliptic is divided into 27 parts of  $13\tfrac{1}{3}\^o$  which correspond to 27 asterisms known as the नक्षत्र-s. In the earliest extant layers of our tradition this number is 28 implying division into sectors of  $12\tfrac{6}{7}\^o$  or insertion of a नक्षत्र with adjacent compressed sectors. The earliest complete नक्षत्र lists are found in the तैत्तिरीय-श्रुति and the Atharvaveda where they are recited as part of the नक्षत्रेष्टि ritual which places the deities of pantheon in these celestial compartments. Indeed, ever since it has been common practice in Hindu astronomy to use the names of the deities corresponding to a given compartment alternatively for the नक्षत्र itself. The old नक्षत्र-lists in the TS, लौगाक्षि's सूत्र-s and the Atharvaveda begin with कृत्तिका i.e., the Pleiades, suggesting that the system as described in these texts was put in place sometime in the interval of 4500-4000 years before present. However, we hold that the नक्षत्र system did not begin with these texts. These texts are mostly predated by the ऋग्वेद, where we find stray references to specific नक्षत्र-s and also the term itself in its generic sense. Thus, we believe a form of the system was already in place even in the days of the ऋग्वेद. Further, there are some homologies between नक्षत्र-s and Iranian asterisms suggesting that some form of the system might have existed even in the Indo-Iranian past on the Eurasian steppes.

Now, one question which is often asked is whether there is a way of knowing precisely how नक्षत्र-s were defined in the Vedic period. In classical Indian astronomy each नक्षत्र is defined by giving the latitude and longitude of a star called the योगतारा, which was defined by the scientist Brahmagupta in 665 CE as the brightest star in the asterism. This makes the नक्षत्र quite unambiguous but then there are नक्षत्र-s, which are far away from the ecliptic in the Vedic reckoning raising the question if the later definitions differ from the Vedic ones. The best way to start answering this is by using the earliest surviving list in tradition that gives the number of stars in each नक्षत्र from the नक्षत्र-kalpa of the Atharvan tradition:

[षट् कृत्तिका एका रोहिणी तिस्रो मृगशिर एकार्द्रा द्वे पुनर्वसू एकः पुष्यः षड् आश्लेषाः षण् मघाः चतस्रः फल्गुन्यः पञ्च हस्त एका चित्रा एका स्वातिर् द्वे विशाखे चतस्रो 'नुराधा एका ज्येष्ठा सप्त मूलम् अष्टाव् अषाढा एको 'भिजित् तिस्रः श्रवणः पञ्च श्रविष्ठा एका शतभिषा चतस्रः प्रोष्ठपदौ एका रेवती द्वे अश्वयुजौ तिस्रो भरण्यः । इति संख्या परिमितं ब्रह्म ॥]{style="color:#0000ff;"} NK2

कृत्तिका -s: 6. This clearly coincides with the traditional understanding of the 6 Pleiades being this asterism. However, an older text the तैत्तिरीय संहिता names seven of them explicitly:\
[अम्बा दुला नितत्निर् अभ्रयन्ती मेघयन्ती वर्षयन्ती चुपुणिका नामासि]{style="color:#0000ff;"}

Likewise in Greek tradition there was an older record of 7 with Aratus claiming that one of them had faded away. This is generally believed to be अम्बा (Greek Pleione). This has led to the debate that originally the 7 were of comparable brightness with 28 Tauri fading away later. The parallel between the आर्य and yavana records suggests that indeed such a fading might have happened.

रोहिणी: 1. Hindu tradition has always identified रोहिणी with  $\alpha$  Tauri (Aldebaran); hence, the single star assigned to रोहिणी should be taken as this one. The name is also indicative of the redness of he star.

मृगशिरस्: 3. Some take this to be the 3 stars on the head of Orion ( $\phi 1, \phi 2, \lambda$  Orionis), which is how they are denoted in classical astronomy. This remains a possibility even in the Vedic reckoning. In Vaidika reckoning the core of मृग was Orion with the arrow of Rudra shot through it (See below). The arrow is identified with the three stars of the belt of Orion ( $\zeta , \epsilon , \delta$  Orionis), known in the Veda as the Invaka-s. Hence, we could entertain the possibility that originally the 3 could have meant the Invaka-s. In support of this proposal we note that the तैत्तिरीय ब्राह्मण 1.5.1.1 states:

[सोमस्येन्वका विततानि ।]{style="color:#0000ff;"}\
Soma's are the Invaka-s \[which are] drawn \[from the bow to be fired].

This explicitly identifies मृगशिरस् with the Invaka-s.

आर्द्रा: 1. The तैत्तिरीय, कठ and अथर्वण-श्रुति-s are all consistent in identifying आर्द्रा with the god Rudra. Going by classical astronomy the coordinates of the योगतारा of आर्द्रा would indicate  $\gamma$  Geminorum. There is another commonly held view that  $\alpha$  Orionis is आर्द्रा. The Vedic text says there is a single star associated with it and the evidence within the Veda suggests that it was not  $\gamma$  Gem. First the तैत्तिरीयस्-श्रुति is unambiguous is stating:\
[आर्द्रया रुद्रः प्रथमान एति ।]{style="color:#0000ff;"}\
With आर्द्रा, Rudra goes forth luminescent.

This means that he original आर्द्रा was likely seen as a bright star. Now, while both  $\alpha$  Ori and  $\alpha$  Canis Majoris are bright stars,  $\alpha$  Ori is too close to मृगशिरस् making  $\alpha$  Can Ma more likely, and is also closer to the position of the later योगतार when projected on to the ecliptic. In the ब्राह्मण on the नक्षत्र ritual in TB 1.5.1.1 the name आर्द्रा is replaced by मृगयु which is always understood as Sirius:

[रुद्रस्य बाहू मृगयवः]{style="color:#0000ff;"}\
The two arms of Rudra are the मृगयु-s (Stars in Can Ma).

Further, this is supported by the evidence of the Aitareya ब्राह्मण on the famed आग्निमारुत-शस्त्र recitation, which we provide in full:

[प्रजापतिर् वै स्वां दुहितरम् अभ्यध्यायद् ।]{style="color:#0000ff;"}\
प्रजापति desired his own daughter.

[दिवम् इत्य् अन्य आहुर् उषसम् इत्य् अन्ये ।]{style="color:#0000ff;"}\
The sky some say and others the उषस्.

[ताम् ऋश्यो भूत्वा रोहितम् भूताम् अबुयैत् ।]{style="color:#0000ff;"}\
Having become a stag he approached her as a red deer.

[तं देवा अपश्यन्न्: आकृतं वै प्रजापतिः करोतीति ।]{style="color:#0000ff;"}\
The gods saw him: "प्रजापति is doing something that is not done".

[te tam aichan ya enam आरिष्यत्य् etam anyonyasmin नाविन्दंस्]{style="color:#0000ff;"}\
They wished to punish him. They did not find find him among one another.

[तेषां या एव घोरतमास् तन्व आसंस्, ता एकधा समभरंस्]{style="color:#0000ff;"}\
Whatever most terrible forms exist they brought together in one place.

[ताः सम्भृता एष देवो 'भवत्, तद् अस्यैतद् भूतवन् नाम ।]{style="color:#0000ff;"}\
Brought together they became this god; hence, his name has the word "भूत" (भूतपति)

[भवति वै स यो 'स्यैतद् एवं नाम वेद ॥]{style="color:#0000ff;"}\
He who knows his name thus prospers.

[तं देवा अब्रूवन्न्: अयं वै प्रजापतिर् आकृतम् अकर्, इमं विध्येति ।]{style="color:#0000ff;"}\
The gods told him: "this प्रजापति has verily done a deed that is not done; pierce him."

[स तथेत्य् अब्रवीत्, स वै वो वरं वृणा इति । वृणीष्वेति ।]{style="color:#0000ff;"}\
He said: "So be it" He also said: "let me choose a boon from you." They said: "Choose".

[स एतम् एव वरम् अवृणीत: पशूनाम् आधिपत्यं ।]{style="color:#0000ff;"}\
He chose this boon: "The overlordship of animals".

[तद् अस्यैतत् पशुमन् नाम पशुमान् भवति यो 'स्यैतद् एवं नाम वेद ।]{style="color:#0000ff;"}\
Hence, his name contains the word animal (पशुपति). He who knows this name thus becomes rich in cattle.

[तम् अभ्यायत्याविध्यत्, स विद्ध ऊर्ध्व उदप्रवत ।]{style="color:#0000ff;"}\
He took aim and pierced him [प्रजापति]. Pierce thus he flew upwards.

[तम् एतम् मृग इत्य् आचक्षते ।]{style="color:#0000ff;"}\
They know him as the \[constellation of the] deer.

[ya u eva मृगव्याधः sa u eva sa, या rohit सा रोहिणी,]{style="color:#0000ff;"}\
He who is the piercer of the deer \[is the asterism] known as that, she who is red is \[is the star] रोहिणी.

[यो एवेषुस् त्रिकाण्डा सो एवेषुस् त्रिकाण्डा ।]{style="color:#0000ff;"}\
That which is the 3-pointed arrow is the \[asterism] of the three-pointed arrow.

[तद् वा इदम् प्रजापते रेतः सिक्तम् अधावत्, तत् सरो 'भवत् ।]{style="color:#0000ff;"}\
The semen of प्रजापति spilled out and ran; it became a lake \[the Milky Way].

[ते देवा अब्रुवन्: मेदम् प्रजापते रेतो दुषद् इति ।]{style="color:#0000ff;"}\
The gods said let this semen of प्रजापति not get ruined.

[यद् अब्रुवन्: मेदम् प्रजापते रेतो दुषद् इति, तन् मादुषम् अभवत् ।]{style="color:#0000ff;"}\
As they said: "let the semen of प्रजापति not be spoiled" it became "not spoiled"

[तन् मादुषस्य मादुषत्वम् ।]{style="color:#0000ff;"}\
The state of not being spoiled is of not spoiled.

[मादुषं ह वै नामैतद् यन् मानुषं ।]{style="color:#0000ff;"}\
From "not spoiled" is the name which is "man".

[तन् मादुषं सन् मानुषम् इत्य् आचक्षते परोक्षेण,]{style="color:#0000ff;"}\
That which is "not spoiled" they know by metaphorical meaning to be linked to man.

[परोक्ष-प्रिया इव हि देवाः]{style="color:#0000ff;"}\
For it is as if the gods like the mysterious.

This narrative clearly identifies Rudra with the killer of प्रजापति. प्रजापति is unambiguously identified with the constellation of Orion and positioned with respect to रोहिणी. Further, he is described as "flying above" his hunter when pierced. Together these identify the constellation of Rudra his hunter with Can Ma. Hence, we may conclude that originally  $\alpha$  Can Ma was आर्द्रा. Further, the name आर्द्रा means moist indicating a link with the wet season. The Iranian equivalent of Sirius, Tishtrya is also associated with rain suggesting that आर्द्रा inherits this ancestral association. This identification is retained in medieval Indian astrolabes where [ $\alpha$  Can Ma is labeled as आर्द्रा-Lubdhaka.]{style="display:inline !important;float:none;background-color:#ffffff;color:#2c3338;cursor:text;font-family:'Noto Serif', Georgia, 'Times New Roman', Times, serif;font-size:16px;font-style:normal;font-variant:normal;font-weight:400;letter-spacing:normal;line-height:1.2em;orphans:2;text-align:left;text-decoration:none;text-indent:0;text-transform:none;white-space:normal;word-spacing:0;"}

पुनर्वसू: 2. There is a fairly uniform agreement that the two stars of पुनर्वसू are  $\alpha , \beta$  Geminorum (Castor and Pollux). The simile of these two stars is frequently encountered. In the रामायण (1.29.25; Gita Press edition):

[प्रविशन् नाश्रम-पदम् व्यरोचत महामुहिः ।]{style="color:#0000ff;"}\
[शशीव गत-नीहारः पुनर्वसु-समन्वितः ॥]{style="color:#0000ff;"}\
The refulgent great sage (विश्वामित्र) entered the path of the आश्रम, even as the moon free from fog accompanied by the two stars of पुनर्वसू (i.e. राम and लक्ष्मण.

Vedic tradition associates पुनर्वसू with the goddess Aditi. Under this association the simile of the moon in पुनर्वसू is again seen in the रामायण (6.67.161):

[कुम्भकर्ण-शिरो भाति कुण्डलालङ्कृतं महत् ।]{style="color:#0000ff;"}\
[आदित्ये'भ्युदिते रात्रौ मध्यस्थ इव चन्द्रमाः ॥]{style="color:#0000ff;"}\
The giant head of कुम्भकर्ण adorned with earrings shone forth even as the moon having arisen at night in the midst of the constellation of Aditi (i.e. between Castor and Pollux).

The तैत्तिरीय संहिता has an incantation in the Soma procurement ritual (in 1.2.4): [अदितिर् अस्य् उभ्यतः शीर्ष्णी सा नः सुप्राची सुप्रतीची सं भव ।]{style="color:#0000ff;"}\
You are Aditi, she is two-headed, be good to us together eastward and westward.

शतपथ ब्राह्मण (3.2.4.16) states:

[अदितिर् अस्य् उभयतः शीर्ष्णीति स यदेनया समानं सद्विपर्यासं वदति यदपरं तत्पूर्वं करोति यत्पूर्वं]{style="color:#0000ff;"}[तदपरं तेनोभयतः शीर्ष्णी तस्माद् आहादितिर् अस्य् उभयतःशीर्ष्णीति ॥]{style="color:#0000ff;"}

You are Aditi, the two-headed. Because he speaks the right in an inverted manner and makes what comes comes first last, and what comes last first by her, therefore she is double-headed. That is why he says: "You are Aditi, the double-headed".

These allusions indicate that the two-headed nature of the constellation of Gemini was transposed on to the presiding deity Aditi and the inversion associated with the two heads along with the eastward and westward paths might indicate an old memory of the start of the ecliptic at Aditi in prehistoric times (>7000 years BP).

पुष्य: 1. This single star is generally taken to be  $\delta$  Cancri which was close to the ecliptic. However, we have evidence from the ऋग्वेद that originally it meant the Praesepe open cluster (M44). The great ऋषि श्यावाश्व आत्रेय:

[युष्मादत्तस्य मरुतो विचेतसो रायः\
स्याम रथ्यो वयस्वतः ।\
[न यो युच्चति तिष्यो यथा\
दिवो 'स्मे रारन्त मरुतः सहस्रिणम् ॥]{style="color:#0000ff;"}5.054.13c]{style="color:#0000ff;"}

May we bear as charioteers of the great wealth\
given by you, discriminating Maruts.\
That which does not depart, even as तिष्य does not from the sky,\
to us in thousands, Marut-s rejoice.

The comparison of great riches, in thousands, is indicative of the great mass of stars in the open cluster supporting the identification of the old तिष्य with M44.

आश्लेषा: 6. This constellation is associated with the snakes in Vedic tradition. It corresponds to the head of the Greek constellation of Hydra, suggesting that the link to a snake goes back to early Indo-European times. आश्लेषा is specifically associated with the head of the snake. Hence, the 6 stars should correspond to  $\theta , \zeta , \epsilon , \delta , \sigma , \eta$  Hydrae. The वेदाञ्ग ज्योतिष states that the summer solstice began in the middle of this constellation suggesting that it was composed around \~3350 YBP.

मघाः: 6. While today Magha is associated with  $\alpha$  Leonis, the Vedic tradition indicates 6 stars for this asterism. This would mean it included the entire sickle of Leo:  $\epsilon , \mu , \zeta , \gamma 1 , \eta , \alpha$  Leonis. The Atharvaveda नक्षत्र सूक्त states that the summer solstice happened in this asterism pointing to a period of \~4400 YBP.

फल्गुन्यः: 4. There are 2 Phalguni-s पूर्व and uttara together with 4 stars. These can be identified with  $\theta , \delta$  Leonis (पूर्व) and  $\beta$ , 93 Leonis (uttara).

Hasta: 5. Tradition unequivocally identifies Hasta with Corvus. Hence the 5 principal stars of Corvus are the 5 listed for Hasta:  $\alpha , \beta , \gamma , \delta , \epsilon$  Corvi.

चित्रा: 1. Spica in Virgo. The star itself is one the नक्षत्र-s mentioned in the RV (according to us contra white indological opinion). In the TB 1.5.1.3 चित्रा is described as an additional star of the god Indra.

स्वाति: 1. Arcturus in Bootes. Also known as निष्ट्या in the Yajurveda.

विशाख: 2.  $\alpha , \beta$  Librae. The constellation of the इक्ष्वाकु-s according to the रामायण.

अनुराधा: 4.  $\beta , \delta , \eta , \rho$  Scorpii

ज्येष्ठा: 1. Antares. The TB describes this star as a second रोहिणी keeping with the red color of the star.

मूल: 7. These seven stars are in the tail of Scorpio. Which stars exactly were identified with the asterism is hard to say but most likely were:  $\zeta , \eta , \theta , \iota , \kappa , \upsilon , \lambda$ . This is the constellation of the goddess of the nether regions निरृत्ति in the Veda. In the रामायण it is associated with राक्षस-s who are supposed to have emanated from निरृत्ति.

अषाढा-s: 8. These eight stars are in the two अषाढा-s: The पूर्व group may be identified with the 4 stars associated with the spout of the teapot of Sagittarius:  $\delta , \gamma , \epsilon , \eta$  Sagittarii. The uttara group may be identified with the handle of the teapot:  $\phi , \sigma , \tau , \zeta$  Sagittarii. The तैत्तिरीय ब्राह्मण's नक्षत्र-सूक्त identifies the पूर्व group with divine waters ([या दिव्या आपः पयसा सम्बभूवुः ।]{style="color:#0000ff;"}) and all other waters as emerging from it. We take this identification as an allusion to the bright center of the Milky Way just next to the पूर्व group.

Abhijit: 1, Vega. This star is way off the ecliptic and is omitted in latter lists. However, its name meaning the all conquering is equivalent to the Iranian name for the same star Vanant. This suggests that it might have been an ancient association. The Aitareya ब्राह्मण indicates that it was used to mark the day just before the स्वरसामन् days during the annual sattra. Tilak we believe rightly realized this was the reason why Abhijit was important in the early period to mark this ritual day which in turn is critical for marking the days leading up to the विशुवान् day.

The महाभरत preserves a curious tale regarding Abhijit's fall and also involves other asterisms pointing to a precessional legend. The great god Indra tells the god Skanda:

[अभिजित् स्पर्धमाना तु रोहिण्या कन्यसी स्वसा ।]{style="color:#0000ff;"}\
[इच्छन्ती ज्येष्ठतां देवी तपस् तप्तुं वनं गता ॥]{style="color:#0000ff;"}

Abhijit, the younger sister of रोहिणि, contested with her desiring seniority. She went woods to perform austerities.

[तत्र मूढो 'स्मि भद्रं ते नक्षत्रं गगनाच् च्युतम् ।]{style="color:#0000ff;"}\
[कालं त्व् इमं परं स्कन्द ब्रह्मणा सह चिन्तय ॥]{style="color:#0000ff;"}

I am dumbstruck by the fall of that auspicious star from the sky.\
O Skanda you with ब्रह्मा should think about this important \[issue] regarding time \[i.e. vacant zone].

[धनिष्ठादिस् तदा कालो ब्रह्मणा परिनिर्मितः ।]{style="color:#0000ff;"}\
[रोहिण्याद्यो 'भवत् पूर्वम् एवं संख्या समाभवत् ॥]{style="color:#0000ff;"}\
Time was specified by ब्रह्मा starting with धनिष्ठा around \[the ecliptic]. Formerly, they started from रोहिणि and thus their number was complete around \[the ecliptic].

[एवम् उक्ते तु शक्रेण त्रिदिवं कृत्तिका गताः ।]{style="color:#0000ff;"}\
[नक्षत्रं शकटाकारं भाति तद् वह्नि-दैवतम् ॥]{style="color:#0000ff;"} 3-219.8-12 ("Critical edition")\
Thus told by Indra, कृत्तिका-s went to the third heavenly realm.\
There they shone forth in the shape of a cart presided by the god Agni.

While several authors have attempted to decode this legend it remains rather obscure. The only clear parts are the memory of a transition from the रोहिणि period to the कृत्तिका period and an allusion to the loss of Abhijit from the नक्षत्र reckoning. This might relate to Abhijit having lost its utility as a marker of important rituals close to the solstices due to precession.

श्रवण/श्रोण: 3. These are quite unambiguously identified as  $\alpha , \beta , \gamma$  Aquilae. It is possible that it was associated with the celestial footprint of विष्णु in his three strides.

श्रविष्ठा/धनिष्ठा: 5. While the नक्षत्र-kalpa gives 5 stars for this asterism the older तैत्तिरीय-श्रुति seems to indicate that there were 4. In any case this group is unambiguously identified with Delphinus. The older reckoning likely took 4 of the brightest stars,  $\alpha , \beta , \gamma , \delta$ . The NK included one further the star.

शतभिषा: 1 This is today take to be  $\lambda$  Aquarii. But was it the star meant in the Vedic texts is unclear. There is an asterism of Iranians known as Satavaēsa, which we hold to be the equivalent of the Vedic one. The Iranian asterism was associated with the sea while the Vedic one with वरुण. The possibility of Fomalhaut ( $\alpha$  Pisces Austrinisis) being this star is not implausible.

प्रोष्ठपद-s: 4. The two प्रोष्ठपद-s are given 2 each. Identifying each pair with the two vertically adjacent stars of the 4 stars comprising the square of Pegasus seems the most likely for these.

रेवती: 1. Classical astronomy identifies it with  $\zeta$  Piscium. This is a really undistinguished star. So we cannot be sure if that is what was originally meant or a higher up star like  $\beta$  Andromedae was used. Narahari Achar holds that the goddess पथ्या रेवती mentioned in the स्वस्तिसूक्त of the Atri-s implied this asterism. While this is not impossible we are not entirely sure of that especially given the undistinguished nature of the star identified with it.

अश्वयुजौ: 2.  $\alpha , \beta$  Arietis. Concerning this asterism there is a problematic issues concerning the the Yajurvaidika incantation known as the उत्तरनारायण. This text describing the cosmic विष्णु now bearing a special name नारायण states:

[अहो रात्रे पार्ष्वे । नक्शत्राणि रूपम् । अश्विनौ व्यात्तम् ।]{style="color:#0000ff;"}\
His sides are the day and the night. His form \[is comprised of] the asterisms. The two अश्विन्-s his jaws.

Here नारायण is identified with the constellations even as प्रजापति was earlier identified with them in the ritual of the नक्षत्र-रूपिन् प्रजापति specified in the Yajurveda (तैत्तिरीय ब्राह्मण 1.5.2.2). This identification continues through later वैष्णव tradition. The ritual itself has further continuity going down to the Gupta age where it is described by the naturalist वराहमिहिर who states that by performing it a man becomes attractive to women and women attain beauty. Now the question is whether the account of the अश्वयुजौ at the mouth of नारायण have some significance of the date of this text. The text is clearly a late one clinging to the edge of the Vaidika productions but when exactly was it composed. If one takes अश्वयुजौ to imply the start of the नक्षत्र cycle having shifted to this asterism it would yield a date of around 2300 YBP. This date resonates with the white Indologists who ascribe late dates to all Vedic production. However, we do not think the mouth should be taken as the beginning of the नक्षत्र cycle. Rather, that position is usually reserved for the top of the head. Hence, the mouth likely implies the नक्षत्र after it which might imply the equinoctial coelure passing before अश्वयुजौ suggesting a date of around 3300-3000 YBP. One also wonders if the tale of प्रजापति being fitted with a goat's head after his beheading by Rudra's agent वीरभद्र alludes to this period, with the goat's had representing Aries.

भरणी: 3. This triad is uniformly understood to be the compact triangle formed by 41, 39, 35 Arietis.

Brahmagupta says in खण्डखाध्यक (1.9.1-2):\
[मूल+अज+अहिर्बुध्न्य+अश्वयुग्+अदिति+इन्द्राग्नी+फल्गुनी द्वितयम् ।]{style="color:#0000ff;"}\
[त्वाष्ट्र-गुरु-वारुण+आर्द्र+अनिल-पौष्णान्य् एक ताराणि ॥]{style="color:#0000ff;"}

[ब्रह्म+इन्द्र-यम-हरि+इन्दु-त्रितयम् षड्-वह्नि-भुजग-पित्र्याणि ।]{style="color:#0000ff;"}\
[मैत्राषाड-चतुष्कम् वसु-रवि-रोहिण्य इति पञ्च ॥]{style="color:#0000ff;"}

Clearly by the early medieval period Hindu asterism-reckoning had changed to a degree from the Vedic period. Now the number of stars in each asterism was specified as:\
कृत्तिका: 6; रोहिणी: 5 (likely whole Hyades+Aldebaran); मृगशिरस्: 3; आर्द्रा: 1; पुनर्वसू: 2; पुष्य: 1; आश्लेषा: 6; मघा: 5; Phalguni-s: 2 each; Hasta: 5; चित्रा: 1; स्वाति: 1; विशाख: 2; अनुराधा: 4; ज्येष्ठा: 3; मूल: 1; अषाढा-s: 4 each; Abhijit: 3; श्रवण: 3; श्रविष्ठा: 5; शतभिषा: 1; प्रोष्ठपद-s: 2 each; रेवती: 1; अश्वयुजौ: 2; भरणि: 3.

