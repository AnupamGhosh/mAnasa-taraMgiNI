
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Euler and Ramanujan: primes, near integers and चक्रवाल](https://manasataramgini.wordpress.com/2017/03/18/euler-and-ramanujan-primes-near-integers-and-cakravala/){rel="bookmark"} {#euler-and-ramanujan-primes-near-integers-and-चकरवल .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 18, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/03/18/euler-and-ramanujan-primes-near-integers-and-cakravala/ "12:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Mathematician Watson who worked on the famed notebooks said regarding some of Srinivasa Ramanujan's equations: "*a thrill which is indistinguishable from the thrill which I feel when I enter the Sagrestia Nuovo of the Capella Medici and see before me the austere beauty of the four statues representing "Day," "Night," "Evening," and "Dawn" which Michelangelo has set over the tomb of Giuliano de'Medici and Lorenzo de 'Medici.*" \[Quoted by computer scientists Borwein, Borwein and Bailey in their great paper on Ramanujan and  $\pi$ ]. This very thrill is something that a mere mortal like us can also experience when gazing at the monuments of the great men like Euler, Gauss and Ramanujan. This indeed is the basis of this note.

In 1772 the blind Euler discovered a remarkable parabola:  $y=x^2+x+41$ . For  $x=0,1,2...39$  the value of  $y$  is a prime number. Thus we see 41-1 primes consecutively generated by it:\
41, 43, 47, 53, 61, 71, 83, 97, 113, 131, 151, 173, 197, 223, 251, 281, 313, 347, 383, 421, 461, 503, 547, 593, 641, 691, 743, 797, 853, 911, 971, 1033, 1097, 1163, 1231, 1301, 1373, 1447, 1523, 1601

Interestingly this parabola is one of a set of special parabolas with deeper connections elsewhere. One notices that it can be written as:

 $$y=(x+\frac{1}{2})^2+\frac{163}{4}$$ 

Now the following other parabolas of such a form exist:

 $$y=(x+\frac{1}{2})^2+\frac{67}{4}$$ 

 $$y=(x+\frac{1}{2})^2+\frac{43}{4}$$ 

 $$y=(x+\frac{1}{2})^2+\frac{19}{4}$$ 

 $$y=(x+\frac{1}{2})^2+\frac{11}{4}$$ 

All these parabolas can also be written as:

 $$y=x^2+x+n; n \Rightarrow 41, 17, 11, 5, 3$$ 

They all generate  $n-1=40, 16, 10, 4, 2$  primes consecutively for  $x=0:n-2$ . Moreover they are increasingly productive in terms of prime-production. For each value of  $n=3...41$  we tabulate below  $4n-1$  and the number of primes produced by these equations for  $x=0:99$ 

![Euler_primes_table1](https://manasataramgini.files.wordpress.com/2017/03/euler_primes_table1.jpg){width="75%"}

One notices that the set culminates in Euler's original parabola, which is rather remarkably rich in its prime-generative capacity. The  $4n-1$  numbers also have several remarkable properties. Consider the following cubic equations whose non-cube coefficients relate to the number -2:

 $$x^3-2x^2+2x-2 = 0; \; x=1.543689012$$ 

 $$x^3-2x-2 = 0; \; x=1.769292354$$ 

 $$x^3-2x^2-2 = 0; \; x=2.359304085$$ 

 $$x^3-2x^2-2x-2 = 0; \; x=2.91963956$$ 

 $$x^3-6x^2+4x-2 = 0; \; x=5.318628217$$ 

Their roots form a peculiar figure on the complex plane (Figure 1) with the real roots (shown next to each equation) falling successively on the positive x-axis.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/03/ramanuja_cubic_roots.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Now for each of these roots if we compute:  $y=x^{24}-24$  successively we interestingly get  $y \approx e^{\pi\sqrt{k}}$  where  $k=4n-1$  from the above Euler parabolas. Even more strikingly these values of  $e^{\pi\sqrt{k}}$  are approximations of  $b=a^3+744$  where  $a$  is a multiple of 32. These striking relationships are tabulated below. Notably, the largest  $k$  exhibits the relationship  $\frac{163}{\log(163)}=31.999998$  which is almost the integer 32.

![Ramanujan_163](https://manasataramgini.files.wordpress.com/2017/03/ramanujan_163.jpg){width="75%"}

Further as  $k$  gets bigger the values of  $e^{\pi\sqrt{k}}$  and either approximation get closer and strikingly the values become almost integers. The last expression for  $k=163$  also leads to the below expression which is a very close near integer:

 $$\left(\dfrac{\log\left(640320^3+744\right)}{\pi}\right)^2=163.0000000000000000000000000000232$$ 

This is also the basis of the famous and good approximation of  $\pi$ :

 $$\pi \approx \dfrac{\log(640320^3+744)}{\sqrt{163}}$$ 

It is correct to 30 decimal places and is greater  $\pi$  by  $2.23\times 10^{-31}$ . Mysteriously, the largest  $k=163$  is associated with another near integer relationships involving  $\pi$  and  $e$ :

 $$163*(\pi-e)=68.99966$$ 

The near integer values of  $e^{\pi\sqrt{k}}$  for the larger of these  $k$  brings us to the remarkable discovery of several such near integers for other  $e^{\pi\sqrt{n}}$  where  $n$  is a positive integer by the 26 year old Srinivasa Ramanujan. To look closer at these we computed the values of  $e^{\pi\sqrt{n}}$  for  $n=1:1000$  and looked for those values. Figure 2 shows the  $\log(|e^{\pi\sqrt{n}}-nint|)$ , where  $nint$  is the nearest integer. A table of these values along with the difference from  $nint$  and also the  $a^3+744$  =  $b$  values and differences (last two columns) [are provided](https://manasataramgini.files.wordpress.com/2017/03/ramanujan.pdf).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/03/ramanujan_epi_diff.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Figure 3 shows the distribution of the  $e^{\pi\sqrt{n}}-nint$ . We notice a striking peak of 27 at  $-.005< e^{\pi\sqrt{n}}-nint< .005$ . Otherwise with a mean of 10 and standard deviation of 3.52 the distribution of differences is fairly uniform. Hence, this production of near integers by  $y=e^{\pi\sqrt{n}}$  does not seem to be by chance alone ( $p=6.93 \times 10^{-7}$ ).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/03/ramanujan_epi_difference_dist.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


In 16 of these cases (tabulated below)  $e^{\pi\sqrt{n}}-\left \lceil e^{\pi\sqrt{n}} \right \rceil< .005$  similar to the near integers of the 43, 67, 163 system we saw above.

![Ramanujan_near_integer_table3](https://manasataramgini.files.wordpress.com/2017/03/ramanujan_near_integer_table3.jpg){width="75%"}

Of these three (43, 67, 163) belong to the above system. A further  $n=326$  is the double of 163 but it does not show the  $a^3+744$  =  $b$  approximation. Three numbers are multiples of 29:  $58=2\times29$ ;  $232=2^3\times29$ ;  $522=2\times3^2\times 29$ .  $386= 2\times 193; 772=2^2\times 193$  feature 193. Then we have 37 and its double 74. Of these the Ramanujan set of  $n=22,37,58$  show a relationship similar to those seen from the above set with 744:  $a^2 \pm 104=b$  where  $b$  is the integer approximation of  $e^{\pi\sqrt{n}}$ :

 $$e^{\pi\sqrt{22}} \approx 1584^2 - 104=2508952$$ 

 $$e^{\pi\sqrt{37}} \approx 14112^2 + 104=199148648$$ 

 $$e^{\pi\sqrt{58}} \approx 156816^2 - 104=24591257752$$ 

But beyond these I am not aware of any other such simple formulae that produce the remaining approximating integer values. The link to 29 however lies at the heart of a class of remarkable approximations of  $\pi$  discovered by Ramanujan of the type:

 $$\pi \approx \dfrac{12 \log(5 + \sqrt{29}) - \log(64)}{\sqrt{58}}$$ 

There is some connection between this and our [previous article](https://manasataramgini.wordpress.com/2017/02/28/early-hindu-mathematics-and-the-exploration-of-some-second-degree-indeterminate-equations/). The explosive values emerging from the solution of चक्रवाल equation are analogous by to the "special" near integer approaches of  $y=e^{\pi\sqrt{n}}$ . Indeed, deeper links beyond the superficial comparison exist between the चक्रवाल-type equations and these systems, which were discovered by Ramanujan through the approximations of  $\pi$  like the one provided above. One such link works thus: Consider the चक्रवाल indeterminate equation,

 $$x^2=ay^2+1$$ 

Consider a second indeterminate equation in  $(p,q)$  for same  $a$ ,

 $$aq^2=p^2+4$$ 

Now if  $a\mod 8 \equiv 5$  then the following holds,

 $$\left(\dfrac{p+q\sqrt{a}}{2}\right)^6=x+y\sqrt{a}$$ 

We see that  $a=29=3\times 8+5$  fulfills the above criterion. Hence,\
 $(p=5, q=1)$ ; by चक्रवाल we get  $(x=9801, y=1820)$ , thus:

 $$\left(\dfrac{5+1\cdot\sqrt{29}}{2}\right)^6=9801+1820\sqrt{29}$$ 

Thus, we can write an approximation of  $\pi$  in terms of the चक्रवाल result for 29 as:

 $$\pi \approx \dfrac{\log\left(64\left(9801+1820\sqrt{29}\right)^2\right)}{\sqrt{58}}$$ 

Finally, while not a near integer there is one involving the Golden ratio  $\phi$  in the same genre:\
 $e^{\pi\sqrt{190}} \approx \left(\sqrt{2}\phi^3\left(3+\sqrt{10}\right)\right)^{12}+24$ ; thus, we get.

 $$\pi \approx \dfrac{\left(6\log(2)+36\log(\phi)+12\log\left(3+\sqrt{10}\right)\right)}{\sqrt{190}}$$ 

Other than the famous  $B^3$  paper mentioned above while writing this we came across formulae of the Ramanujan type discovered by Tito Piezas with new चक्रवाल connections. One may also study the related formulae of the Chudnovsky brothers. All of these show that what's discussed here is merely the tip of the iceberg. Much of the deeper connections regarding these systems lie outside our very meager mathematical knowledge, hence we stop here. Briefly, the  $k=4n-1$  values of the above Euler parabolas are precisely the numbers which Gauss first connected to some deep mathematics. Thus, the mysterious connections presented here are not coincidental. It stems from some very deep modern results which started with Carl Gauss' discovery of a function of the complex variable known as the j-function. It has wended its way via Gauss's student Dedekind's  $\eta$  function and through Ramanujan's discovery of the above-discussed near integers and other things from his "lost notebook". Finally in our times it has led to something called the *Monstrous Moonshine* which lies outside the perimeter of our mathematical education. We are simply recording some of what we have explored for ourselves (well-known to mathematicians) for their astounding nature. Fittingly, some physicists hold that these might have deep connections to the very nature of existence.

