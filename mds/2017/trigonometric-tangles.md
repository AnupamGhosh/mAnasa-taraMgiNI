
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Trigonometric tangles](https://manasataramgini.wordpress.com/2017/04/02/trigonometric-tangles/){rel="bookmark"} {#trigonometric-tangles .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 2, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/04/02/trigonometric-tangles/ "2:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Let us define a define the trigonometric tangle as the following parametric function:

 $$x=\cos(t/k)-a\cos(b\cdot t)\sin(t)$$ 

 $$y=\sin(t/k)-a\cos(b\cdot t)\cos(t)$$ 

where  $k$  can be a rational number  $\frac{p}{q}$  or an irrational number.  $a$  and  $b$  are any real number. If  $k$  is a rational number and  $|b|=1$  then we get a tangle  $c$  petals defined thus:

 $$k=\dfrac{p}{q}$$ 

 $\dfrac{c}{d}=\dfrac{2p+q}{q}$ , such that  $c,d$  are relatively prime.

When  $|b|\gg 1$  then the envelop of the tangle converges to a figure with  $e$ -fold symmetry defined thus:\
 $\dfrac{e}{f}=\dfrac{2p+2q}{q}$  such that  $e,f$  are relatively prime.

For irrational  $k$  for the above conditions we get figures coming close those of  $k=\frac{p}{q}$  corresponding to the continued fraction approximations of the irrationals. This provides us an interesting way of visualizing the irrationals. Interestingly, visual distinction can be made between algebraic and transcendental irrationals with these curves.

![trignots4](https://manasataramgini.files.wordpress.com/2017/04/trignots41.png){width="75%"}Figure 1. Convergence from  $c$ -lobed initial to  $e$ -fold symmetry

![trignots5](https://manasataramgini.files.wordpress.com/2017/04/trignots5.png){width="75%"}Figure 2. Curves for small and large  $b$  for integral  $k$ 

![trignots6](https://manasataramgini.files.wordpress.com/2017/04/trignots6.png){width="75%"}Figure 3. Curves for small and large  $b$  for fractional rational  $k$ 

![trignots7.1](https://manasataramgini.files.wordpress.com/2017/04/trignots7-1.png){width="75%"}$ 

