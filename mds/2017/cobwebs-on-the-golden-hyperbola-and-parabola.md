
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Cobwebs on the golden hyperbola and parabola](https://manasataramgini.wordpress.com/2017/06/17/cobwebs-on-the-golden-hyperbola-and-parabola/){rel="bookmark"} {#cobwebs-on-the-golden-hyperbola-and-parabola .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 17, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/06/17/cobwebs-on-the-golden-hyperbola-and-parabola/ "4:19 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The material presented here is rather trivial to those who have spent even a small time looking at chaotic systems. Nevertheless, we found it instructive when we first discovered it for ourselves while studying conics. Hence, as part of recording such little tidbits of trivia that over the years have caught our eyes we are putting it down here.

Consider the iterative map,

 $$x_{n+1}=1+\dfrac{1}{x_n}$$ 

Notably, irrespective of what starting number  $x_n$  you take (with one exception) it converges to the Golden ratio  $\phi=1.61803...$  (Figure 1).

![golden_ratio_convergence](https://manasataramgini.files.wordpress.com/2017/06/golden_ratio_convergence.png){width="75%"}

Figure 1

The one exception is when  $x_0=-\tfrac{1}{\phi}$ . In this case it is rather obvious why it remains fixed at  $-\tfrac{1}{\phi}$ . However, notably, say you are very close to  $x_0=-\tfrac{1}{\phi}$ , e.g. -.61804, then you hover close to  $-\tfrac{1}{\phi}$  for around 7 iterations and then drift away rapidly to converge to  $\phi$  quite rapidly. The rate of convergence measured as the number of iterations you take to converge within a certain range of  $\phi$  (say  $\pm 10^{-7}$  in below figure) shows an interesting pattern: for values closer and closer to  $-\tfrac{1}{\phi}$  you take longer and longer to converge to  $\phi$  whereas values closer to  $\phi$ , not surprisingly, converge faster. Thus, while  $\phi$  serves as the near universal attractor and  $-\tfrac{1}{\phi}$  as the near universal repellor for this map, the repellor is actually weaker at repelling values that lie closer to it (Figure 2).

![Golden_Ratio_convergence3](https://manasataramgini.files.wordpress.com/2017/06/golden_ratio_convergence3.png){width="75%"}\
Figure 2

Rather dramatic jumps but still convergent behavior can be obtained at certain points: When  $x_0=0, -\tfrac{1}{2}, -\tfrac{3}{5}, -1$  we jump between 0 and  $\infty$  before moving towards convergence. This behavior can be studied in terms of the maximum value reached by  $x_n$  before convergence towards  $\phi$ . The distribution of these values for various starting  $x_0$  shows a peculiar fractal structure with peaks of decreasing heights as one moves towards the repellor  $-\tfrac{1}{\phi}$  from either direction (Figure 3).

![golden_ratio_convergence2](https://manasataramgini.files.wordpress.com/2017/06/golden_ratio_convergence2.png){width="75%"}\
Figure 3

So what it is the connection to conics alluded to above? One can see right away that the above map corresponds to the hyperbola,

 $$y=1+\dfrac{1}{x}$$ 

The iterations of the map can be rendered geometrically as the famous cobweb diagram used by students of chaotic dynamics: Start with a curve and a point representing  $x_0$  on the x-y plane. Move along the vertical direction from that point till you hit the nearest point on the curve. Mark that segment. Then proceed in the horizontal direction till you hit a point on the line  $y=x$ . Mark that segment. Then move vertically again to hit the curve and so on. Repeat this procedure till you converge. When you do this procedure for the above hyperbola (Figure 4) then every point except one with x-coordinate  $-\tfrac{1}{\phi}$  converges to the point  $(\phi, \phi)$ , which is the attractor. The repellor is  $(-\tfrac{1}{\phi},-\tfrac{1}{\phi})$ . One can see that these fixed points are intersects of the said hyperbola and the line  $y=x$ 

![Cobwebs01](https://manasataramgini.files.wordpress.com/2017/06/cobwebs01.png){width="75%"}\
Figure 4

Now there exists a parabola with the same fixed points as the above hyperbola:

 $$y=x^2-1$$ 

For this parabola, if a starting point for the cobweb diagram lies in the band delimited by the lines  $y=\pm \phi$  then it moves towards  $-\tfrac{1}{\phi}$ . But it does not converge to that point. Instead it is eventually trapped in a four-point orbit:  $(0,0); (-1,0); (-1,-1); (0,-1)$  (Figure 5). If the starting point lies outside the above band then it races away to  $\infty$ . But there are a some points where it actually converges to one of the fixed points. Any point whose x-coordinate is  $\pm \phi$  converges to  $(\phi, \phi)$ . Any point whose x-coordinate is  $\pm \sqrt{\phi}$  or  $\pm \tfrac{1}{\phi}$  converges to  $(-\tfrac{1}{\phi},-\tfrac{1}{\phi})$ . The closer point's x-coordinate is to one of the above values the greater the number of iterations it requires to be placed in the final four-point orbit. Thus, the parabola with the same fixed points as the said hyperbola displays a very different behavior --- the outcomes are convergence to a fixed point, divergence to  $\infty$  or eventually settling into a fixed orbit. However, for certain parabolas such as the logistic parabola,  $y=k x(1-x)$  we see the famous chaotic behavior for certain values of  $k$ .

![Cobwebs02](https://manasataramgini.files.wordpress.com/2017/06/cobwebs02.png){width="75%"}\
Figure 5

