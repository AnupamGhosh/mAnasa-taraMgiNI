
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Trigonometric tangles-2](https://manasataramgini.wordpress.com/2017/04/15/trigonometric-tangles-2/){rel="bookmark"} {#trigonometric-tangles-2 .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 15, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/04/15/trigonometric-tangles-2/ "11:50 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We had earlier described our exploration of the spirograph, hypocycloids, epicycloids and related curves. In course of our study of the शैव tantra-s of the kaula tradition we started thinking about a remarkable piece of imagery mentioned in them. Tantra-s like the विरूपाक्ष-पञ्चाक्शिक and नित्याषोडषिकार्णव talk of the waves and spinning cakra-s of शक्ति-s. The विरूपाक्ष-पञ्चाक्शिक describes these innumerable शक्ति-waves and wheels of शक्ति-s as emanating the universe:

[स्वाञ्गे चिद्-गगन+आत्मनि दुग्धोदधि-निभः स्व-शक्ति-लहरीणाम् ।]{style="color:#99cc00;"}\
[सम्भेद-विभेदाभ्यां सृजति ध्वंसयति चै(ए)ष जगत् ॥]{style="color:#99cc00;"} 2.13

In his (शिव's) own body, in the void of the conscious-self, resembling the milk-ocean, through the constructive and destructive interference of the waves of his own शक्ति-s this universe is generated and destroyed.

[रूपादि-पञ्च-विषयात्मनि भोग्य-हृषीक-भोक्तृ-रूपे 'अस्मिन् ।]{style="color:#99cc00;"}\
[जगति प्रसरद् अनन्त-स्व-शक्ति-चक्रा चितिर्भाव्या ॥]{style="color:#99cc00;"}2.14

In the contents of the five sense-streams starting with form, in the objects being sensed, in the sense organs and the first-person experiencer, in the world generated by the innumerable wheels of his शक्ति-s consciousness should be conceived.

The नित्याषोडषिकार्णव states:\
[प्रोच्छलन्-मद-कल्लोल-प्रचलज्-जघनस्थलाम् ।]{style="color:#99cc00;"}\
[शक्ति-चक्रोच्छल-च्छक्तिवलना-कवलीकृताम् ।]{style="color:#99cc00;"} 2.24-25

The goddess त्रिपुरा is worshipped as: The gushing intoxicant waves setting in vibration the vulval receptacle, setting in motion the शक्ति wheels, spinning the शक्ति wheels and devouring all.

This account of the waves of शक्ति-s and their wheels led us to conceive the imagery as related to the more general epicycloid problem: Imagine a point on the circumference of a wheel rotating at some speed either clockwise or anticlockwise. That point is the center of another wheel, which is likewise rotating at a distinct speed. On the circumference of the second wheel is a point which in turn is the center of yet another wheel rotating at yet another speed and so on. Now what curve would a point on the rim of the terminal wheel of this system would trace out? \[See link for [animation](https://app.box.com/s/xplwovnkox5nz6wspt0mua4lufeu6ktm)] 

More than 20 years ago mathematician Frank Farris, who has a great eye for symmetry and beauty, showed that this can be described rather simply by a function in the complex plane thus:

 $$z(t)=a_1e^{k_1it}+a_2e^{k_2it}...+a_ne^{k_nit} ; \; z(t) \in \mathbb{C}$$ 

 $$a_j=a_1,a_2...a_n$  represent the relative radii and  $k_j=k_1, k_2...k_n$  represent the relative speeds of rotation of the  $n$  wheels in the system. If the wheel is rotating clockwise then  $k_j< 0$  and  $a_j=i\cdot a_j$ ; where  $i=\sqrt{-1}$$ 

![spirographic_6tangle1](https://manasataramgini.files.wordpress.com/2017/04/spirographic_6tangle1.png){width="75%"}Figure 1: The curve generated by a system of 9 wheels of relative radii: 1.0000000, 0.6180340, 0.3197333, 0.1227690, 0.2256262, 0.2233212, 0.4724658 and rotation speeds: -1, 5, 11, 17, 23, 29, 35.

Thus more generally we can define a complex function which would generate "trigonometric tangles" related to those generated by a n-wheel system:

 $$z(t)=\displaystyle \sum _{j=1}^{n}a_je^{k_jit}$$ 

where  $k_j$  is real number and  $a_j$  might be real or imaginary. Now these functions are symmetric and closed as  $t$  takes all values  $\lbrack 0,2\pi\rbrack$  when the following conditions are satisfied:

 $$k_j=l\times n+m; \; l \in \mathbb{Z}; \; n,m \in \mathbb{N}_1\; and \; \textrm{gcd}(n,m)=1$$ 

i.e if  $l$  is any integer and  $n,m$  are relatively prime, positive integers then the resultant curve is closed and has n-fold symmetry. Thus, keeping to symmetric closed curves this equation can literally produce nava-nava-चमत्कार.

![spirographic_8tangles2](https://manasataramgini.files.wordpress.com/2017/04/spirographic_8tangles2.png){width="75%"}Figure 2: A 12 term system with 8-fold symmetry allowing only real  $a_j$ 

![spirographic_4tangles1](https://manasataramgini.files.wordpress.com/2017/04/spirographic_4tangles1.png){width="75%"}Figure 3: The svastika-system with 4-fold symmetry, random number of terms up to 10, and allowing both real and imaginary  $a_j$ 

![spirographic_24guilloche](https://manasataramgini.files.wordpress.com/2017/04/spirographic_24guilloche.png){width="75%"}Figure 4: The 17 term system with 24-fold symmetry allowing both real and imaginary  $a_j$ . It somewhat resembles the guilloche security patterns used on bank notes.

