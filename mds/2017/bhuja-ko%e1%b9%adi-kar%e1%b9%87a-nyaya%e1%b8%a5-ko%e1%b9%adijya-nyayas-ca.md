
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [भुजा-कोटि-कर्ण-न्यायः कोटिज्या-न्यायश् ca](https://manasataramgini.wordpress.com/2017/06/16/bhuja-ko%e1%b9%adi-kar%e1%b9%87a-nyaya%e1%b8%a5-ko%e1%b9%adijya-nyayas-ca/){rel="bookmark"} {#भज-कट-करण-नयय-कटजय-नययश-ca .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 16, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/06/16/bhuja-ko%e1%b9%adi-kar%e1%b9%87a-nyaya%e1%b8%a5-ko%e1%b9%adijya-nyayas-ca/ "5:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![bhujA_koTi_karNa_nyAya](https://manasataramgini.files.wordpress.com/2017/06/bhuja_koti_karna_nyaya.png){width="75%"}

भुजा-कोटि-कर्ण-न्यायः

![cosrule](https://manasataramgini.files.wordpress.com/2017/06/cosrule.png){width="75%"}\
कोटिज्या-न्यायः

