
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Frustrations and ramblings ensuing from Cretaceous amber](https://manasataramgini.wordpress.com/2017/12/18/frustrations-and-ramblings-ensuing-from-cretaceous-amber/){rel="bookmark"} {#frustrations-and-ramblings-ensuing-from-cretaceous-amber .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 18, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/12/18/frustrations-and-ramblings-ensuing-from-cretaceous-amber/ "7:37 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Time and again I have been frustrated by the inability of Hindus to make the most of the riches that are available in their own land or right next to them. One such case is that of Cretaceous amber from Myanmar, which has recently become the focus of an enormous scientific exploration. It is telling us about arthropod natural history like never before: Many insect lineages were already close to their modern form by the time window of 98.7-108 million years ago to which this amber is dated. For example we see ants and termites were already on the rise and had established their caste structure. This gives a firm evidence for the enormous antiquity and resilience of the caste-structure in social insects, thereby supporting its adaptive value. It is giving us the first picture of the ticks that sucked dinosaur blood as well as dermestid beetle larvae, which were probably nest symbionts that fought these ticks with their specialized structures called hastisetae. We are also getting the rare dinosaur chicks, which were unfortunate enough to get sloshed in this resin and be preserved for us to understand their anatomy in some detail. How is all this being studied? The researchers appear to simply go to Myanmar and buy amber and study its inclusions. However, I do not see one Hindu involved in this process despite India having close ties to Myanmar and knowing of this resource for centuries. This is research where the Hindus could have beaten the चीन-s and mleccha-s by a stretch but failed to even notice the wealth in their neighborhood.

This set off a train of thoughts that go a long way back in time. Pliny the elder, was one of those charismatic figures in history with few parallels -- a general in both the Roman navy and land army and a naturalist, who apparently died during the eruption of mount Vesuvius. Of him his nephew writes:

"For my part I deem those blessed to whom, by favor of the gods, it has been granted either to do what is worth writing of, or to write what is worth reading; above measure blessed those on whom both gifts have been conferred. In the latter number will be my uncle, by virtue of his own and of your \[Tacitus of Germania fame] composition."

The composition his nephew is talking about is the Naturalis Historia. This interesting work has a whole chapter on amber, and mentions the Indian amber on three occasions. He mentions that the yavana Nicias had noted that amber was used in place of guggulu or aguru as a high-end incense. He also mentions that the yavana physician Ktesias, employed by the Iranian emperor, noted that the Hindus obtained amber from a river called Hypobarus that flowed from the north to the eastern ocean. The river flows by a mountain where trees called "siptachorae" produce amber and discharge it into the river. Then Pliny states that Archaelaus, a chief of Cappadocia, brought amber from India with bark still adhering to it and processed it to get the shiny polish. It was said to contain ants, gnats and lizards in it by means of which Pliny inferred that amber must have once been liquid.

These ancient Greek accounts would suggest a pre-Mauryan knowledge of amber in India. Importantly, the river flowing into the eastern ocean brings to mind the tributaries of the इरावति river in Myanmar in whose valleys amber is found. However, it does appear that the accounts might have in part conflated real amber, i.e. the fossil resin, with more recent resin. Nevertheless, that real amber was used as jewelry by the Hindus is confirmed by the चीन-s, who during the great Tibet-Tang-Hindu drama mention that the राजा of Nepal had jewels of amber.

We have often thought that the great Hindu naturalist वराहमिहिर was a kindred soul of Pliny. In his natural history, the बृहत्-संहित, he has a few chapters on gemstones. Turning to those we find a curiously named gem called saugandhika, which literally means good-smelling. This is consistent with Nicias account of Indian amber being used for incense. Hence, we believe this was an old name for amber. वराहमिहिर classifies this gemstone in the पद्मराग category. He says the gems of this class are of three types in the beginning of chapter 81:

[सौगन्धिक-कुरुविन्द-स्फटिकेभ्यः पद्मराग-सम्भूतिः ।]{style="color:#0000ff;"}\
The पद्मराग-s are formed from saugandhika (resin), corundum and crystals

[सौगन्धिकजा भ्रमर-अञ्जन-अब्ज-जम्बूरस-द्युतयः ॥]{style="color:#0000ff;"}\
That originating from resin have the shimmer of bees, collyrium, clouds and the rose-apple juice.

[कुरुविन्द-भवाः शबला मन्द-द्युतयश् च धातुभिर् विद्धाः ।]{style="color:#0000ff;"}\
Those originating from corundum are dappled, with a dim glow and permeated by mineral inclusions.

[स्फटिक-भवा द्युतिमन्तो नाना-वर्णा विशुद्धाश् च ॥]{style="color:#0000ff;"}\
Those originating from crystals are lustrous, with multi-colored \[sparkle] and clear.

There are few points of note here: One may wonder why वराहमिहिर groups these together as पद्मराग-s. First, several samples of Burmese amber can have a reddish tint (he specifically mentions जम्बूरस) placing them together with the other reddish gems he describes in this category. Second, Burma also produces rubies and corundums, which might have led to a geographic association between these stones; in any case वराहमिहिर does recognize their different "geological" origins. Finally, one may wonder what the सौगन्धिकजा's bhramara-dyuti would mean. Most simply take it to be bee-colored i.e. yellow and black. However, we are tempted to see it as an acknowledgment of the insect inclusions in amber. वराहमिहिर tells us of a नास्तिक author बुद्धभट ("Buddha's soldier") who had composed treatise on gems to which he refers the readers. May be we would have gotten more information from that text had it come down to us. Perhaps, due to declining scientific interests of the bauddha-s over time or the Mohammedan book-burning this text has not survived.

The Greeks and Romans called amber elektron after its yellowish luster. This word has give rise to the modern words like electricity and electron. This is due to the static electricity generated in amber, i.e. triboelectric charging, when rubbed with a woolen cloth. This property has given rise to the second name for it in Sanskrit तृणमणि or तृणग्रह. This literally means the straw-gem or the straw-seizer. This property of amber is used by the great Kashmirian kavi कल्हण in a beautiful simile, where he describes the acts of a warrior रिल्हण:

[तत् खड्गस्य घ्नतः खड्गाञ् जीवैर् जालच् छलाद् ध्रुवम् ।]{style="color:#0000ff;"}\
[उत्थाय लग्नं शत्रूणां तृणैस् तृणमणेर् इव ॥]{style="color:#0000ff;"}

As his[रिल्हण's] sword's blows struck down their swords,\
the enemies' lives \[were stuck firmly] as a web \[to his sword],\
even like straw-blades rise up to stick to amber.

This clearly indicates that not just amber but its propensity for static electricity was well-known even in the Hindu world. Indeed, another Kashmirian kavi भल्लट also talks of the same property but his verse indicates that by the beginning of the last century of the first millennium of the common era amber's value was declining in the Indian mind. He says:

[चिन्तामणेस् तृणमणेश् च कृतं विधात्रा]{style="color:#0000ff;"}\
[केनोभयोर् अपि मणित्वम् अदः समानम् ।]{style="color:#0000ff;"}\
[नैको'र्थितानि ददद् अर्थिजनाय खिन्नो]{style="color:#0000ff;"}\
[गृह्णञ् जरत्-तृणलवं तु न लज्जते'न्यः ॥]{style="color:#0000ff;"}

The चिन्तामणि and amber, both made by विधातृ,\
now then why should the gem-hood of both \[be considered] equal?\
One has undesired \[qualities], giving sadness to one desiring it,\
attracting dry pieces of straw, but is not ashamed by the other.

भल्लट's anthology is filled with verses with a suggestive satire. Here, he compares amber to the fabulous gem चिन्तामणि and says that while both are called gems one (i.e. amber) is in reality rather worthless compared to the other member in the same category. This verse reminded me of the category "biologist". While many scientists are placed in that category the difference between actual exemplars is like that between the चिन्तामणि and the तृण्मणि of भल्लट. While they sit in high seats and publish papers in magazines considered prestigious their knowledge and understanding of the science is shockingly abysmal. Yet the mleccha system allows this charade to continue much in the manner of things lumped in the gemstone category of भल्लट.

However, we would say this decline of amber in the Hindu mind was not a good thing, bringing us back to where we started this note. For a paleontologist the Cretaceous amber, the तृण्मणि of yore is indeed nothing short of a चिन्तामणि: परिणामवदिभ्यस् तृणमणिश् चिन्तामणिर् eva |


