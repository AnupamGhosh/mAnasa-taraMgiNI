
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Constructing a regular heptagon with hyperbola and parabola](https://manasataramgini.wordpress.com/2017/06/01/constructing-a-regular-heptagon-with-hyperbola-and-parabola/){rel="bookmark"} {#constructing-a-regular-heptagon-with-hyperbola-and-parabola .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 1, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/06/01/constructing-a-regular-heptagon-with-hyperbola-and-parabola/ "4:16 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

There is little doubt that Archimedes was one of the greatest yavana intellectuals. He would also figure in any list of the greatest mathematician-scientists of all times. His work on the construction of a regular heptagon has not survived the destruction of yavana works by the प्रेतसाधक-s. However, the Platonist heathen of Harran, [Thabit ibn Kurra](https://manasataramgini.wordpress.com/2006/08/28/the-makings-of-islamic-science/), preserves Archimedes conclusion that to construct a heptagon one needs to trisect an angle. This can be achieved using yavana techniques by say the conchoid of Nicomedes, [as we had shown previously](https://manasataramgini.wordpress.com/2016/03/23/iamblichus-quadratures-trisections-and-the-lacuna-of-the-cycloid/).

In 1798 CE, long after the yavana-s were gone, the 21-year old Carl Gauss wrote his famed work the Disquisitiones arithmeticae in Latin, which showed that geometric constructions way beyond what the yavana-s of even the order of Archimedes and Apollonius had achieved existed. The famous construction of the 17-gon of with just a compass and straight-edge is one of those Gaussian conquests which shows why there is Gauss and there are the rest. Additionally, Gauss provides the general principles which can be used for conic constructions of other polygons. We present below the construction of the regular heptagon using these Gaussian principles in lay terms. The constructions assumes you can draw conics given certain necessary and sufficient prerequisites as we have earlier demonstrated on these pages.

![Gaussian_Heptagon](https://manasataramgini.files.wordpress.com/2017/06/gaussian_heptagon.png){width="75%"}


 1.  Draw a circle with center O in side which the heptagon will be drawn.

 2.  Draw the horizontal and vertical diameters. The former meets the circle at points A and B.

 3.  Bisect the angle formed by the two diameters to get points C and D on the circle.

 4.  Mark points H and I at distance half the radius of the circle from its center O.

 5.  Draw perpendiculars at H and I to the horizontal and vertical diameters respectively so that they meet at point J.

 6.  Using point J as the vertex and points C and D as foci draw a rectangular hyperbola (colored brown).

 7.  Mark point K at distant half the radius of the circle from center O and draw a line parallel to the horizontal diameter through K. Draw tangent to the circle at point B. The two meet at point E.

 8.  Using point E as center mark points F and G on  $\overleftrightarrow{EK}$  using a circle with radius  $\frac{1}{8}$ th of the starting circle.

 9.  Draw a vertical line through G. Use it as the directrix and F as the focus to draw a parabola with vertex at point E.

 9.  Mark the three points of intersection of the hyperbola and parabola which have been thus constructed.

 10.  Draw vertical lines through these three points to cut the starting circle at 6 points. These points together with A are the vertices of the desired heptagon.

The Gaussian principle behind this can be derived as below. Consider the following,\
The unit circle:  $x^2+y^2=1$ 

The rectangular hyperbola:  $xy=\frac{1}{4}$ 

The parabola:  $x=2y^2+2y-\frac{1}{2}$ 

Solve the equations of the hyperbola and parabola simultaneously. Since the substitution results in a cubic with 3 real roots we get 3 points:\
(-0.2225209,-1.1234898); (0.6234898; 0.4009689); (-0.9009689, -0.2774791)\
The vertical line passing through these 3 points will cut the above unit circle at 6 points which correspond to the 6 complex roots of the equation  $z^7=1$ . The real root will be point (1,0), i.e. corresponding to point A in our construction.

The "seveness" of the three points obtained by the intersection of the parabola and hyperbola is seen in the coordinates of the points which can be represented as:

 $$(\frac{1}{2(1-S)}, \frac{1-S}{2})$$ 

 $$(\frac{S-2}{2}, \frac{1}{2(S-2)})$$ 

 $$(-\frac{\sqrt{S}}{2},-\frac{1}{2\sqrt{S}})$$ 

Where the silver constant is  $S=2+2\cos\left(\frac{2\pi}{7}\right)$ ; note  $\frac{2\pi}{7}$  is the heptagon angle.

I recently learned of an interesting expression for the silver constant apparently discovered by T.Piezas of the form,

 $$k=\sqrt\lbrack 3\rbrack {7+7\sqrt\lbrack 3\rbrack {7+7\sqrt\lbrack 3\rbrack {7+...}}}$$ 

 $$S=2+\frac{k+2}{k+1}$$ 

It also relates to a peculiar number discovered by Srinivasa Ramanujan that brings three of these heptagon angles together:

 $$\sqrt\lbrack 3\rbrack {\cos\left(\frac{2\pi}{7}\right)}+\sqrt\lbrack 3\rbrack {\cos\left(\frac{4\pi}{7}\right)}+\sqrt\lbrack 3\rbrack {\cos\left(\frac{6\pi}{7}\right)}=\sqrt\lbrack 3\rbrack {\frac{5-3\sqrt\lbrack 3\rbrack {7}}{2}}$$ 

More trivially,

 $$\cos\left(\frac{2\pi}{7}\right)+\cos\left(\frac{4\pi}{7}\right)+\cos\left(\frac{6\pi}{7}\right)=-\frac{1}{2}$$ 

and,

 $$\cos\left(\frac{2\pi}{7}\right)\cdot\cos\left(\frac{4\pi}{7}\right)\cdot\cos\left(\frac{6\pi}{7}\right)=\frac{1}{8}$$ 


