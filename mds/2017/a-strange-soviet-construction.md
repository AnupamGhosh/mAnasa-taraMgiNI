
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A strange Soviet construction](https://manasataramgini.wordpress.com/2017/01/14/a-strange-soviet-construction/){rel="bookmark"} {#a-strange-soviet-construction .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 14, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/01/14/a-strange-soviet-construction/ "10:34 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

in our college days we used to visit the लाल्-pustak-भण्डार् in our city where Soviet books on science and mathematics were sold at a low price (alongside Marxian literature). They were a great resource that enormously contributed to our intellectual development. Among the books were some which contained nasty problems in mathematics and physics that were used for boasting rights in our circle. Some of the more curricularly oriented students in my class apparently used those books for entrance exams to certain Indian undergraduate institutes that we had no interest in joining. We were reminded of  the Soviet penchant for such problems recently by an acquaintance. She brought to my attention a Euclidean problem which was allegedly part of a set of difficult mathematical questions which were used by the Soviet authorities to prevent Jews and other "undesirables" from entering the Moscow State University (She learned of those from a paper containing a whole set of such problems and their solutions which was posted several years ago by Tanya Khovanova and Alexey Radul and was recently highlighted by Pickover on his well-known site). I am simply recording it here for I found it interesting and spent some time on it. I took a while to solve it but once achieved it looked so trivial that I felt like a fool. The yavana-s of yore and other ancients might have liked it.

Problem: "Construct with ruler and compass a square given one point from each side."

![soviet_square](https://manasataramgini.files.wordpress.com/2017/01/soviet_square2.png){width="75%"}


 1.  Let A, B,C,D be the 4 points each from one side of the square.

 2.  Construct parallelogram ACDE using 3 of the points (A, C, D). Thus  $\overleftrightarrow{DE}$  would be parallel to AC.

 3.  Drop a perpendicular from point D to the side of the parallelogram containing the other two points  $\overline{AC}$ .

 4.  Draw circle with center D and radius  $\overline{DE}$  to cut above perpendicular at point F.

 5.  Draw  $\overleftrightarrow{BF}$  and drop a perpendicular to  $\overleftrightarrow{BF}$  from A to cut it at point J.

 6.  Drop a perpendicular to  $\overleftrightarrow{AJ}$  from point D to cut it at point G.

 7.  Draw  $\overleftrightarrow{GD}$  and drop a perpendicular to from point C to cut it at point H.

 8.  Draw  $\overleftrightarrow{HC}$  and complete the desired square GHIJ via obtaining the intersection I between the above line and  $\overleftrightarrow{BF}$ .

A square is formed by intersection of two orthogonal, equidistant pairs of parallel lines. Thus, a segment formed by points lying on two opposite sides of a square if rotated by  $90\^o$  using one of the remaining two points as the pivot would define two points on the other pair of opposite sides. This is the principle enacted by the above construction to get the desired square.

We adduce below several other Soviet problems with the respective solutions without any detailed explanation.

\*Given two intersecting lines on a plane find locus of points D such that the sum of the distances from D to each line is equal to a given number.

![soviet_lines](https://manasataramgini.files.wordpress.com/2017/01/soviet_lines.png){width="75%"}

\*Can you put six points on a plane, so that the distance between any two of them is an integer, and no three are colinear?

![soviet_integral_hexagon](https://manasataramgini.files.wordpress.com/2017/01/soviet_integral_hexagon.png){width="75%"}

\*Given two parallel segments (AB, CD) divide one of them (AB) into six equal parts using just a straight edge.

![soviet_segment_6parts](https://manasataramgini.files.wordpress.com/2017/01/soviet_segment_6parts.png){width="75%"}

\*Given a triangle ABC, construct, using a straight edge and compass, a point M\
on AC and a point K on BC, such that

 $$\overline{AM}$  =  $\overline{MK}$  =  $\overline{BK}$$ 

![soviet_triangle_segments](https://manasataramgini.files.wordpress.com/2017/01/soviet_triangle_segments.png){width="75%"}

