
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Deliberations on richness and beauty: discovery of some multi-parameter iterative maps](https://manasataramgini.wordpress.com/2017/01/17/deliberations-on-richness-and-beauty-discovery-of-some-multi-parameter-iterative-maps/){rel="bookmark"} {#deliberations-on-richness-and-beauty-discovery-of-some-multi-parameter-iterative-maps .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 17, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/01/17/deliberations-on-richness-and-beauty-discovery-of-some-multi-parameter-iterative-maps/ "12:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

As we have explained in the earlier notes ([1](https://manasataramgini.wordpress.com/2016/12/18/some-reminiscences-of-our-study-of-chaotic-maps-1/), [2](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/), [3](https://manasataramgini.wordpress.com/2016/12/25/some-elementary-lessons-from-iterative-fractal-maps/)), the second major factor in our exploration of 2D strange attractors maps, IFS and other fractals was the aesthetic experience they produced. Around that time we came across a curious statement of Plato in the Timaeus: "Now, the one which we maintain to be the most beautiful of all the many figures of triangles (and we need not speak of the others) is that of which the double forms a third, the equilateral triangle."(translation by Jowett via Birkhoff). Thus, for Plato the most beautiful of triangles is the so-called  $\frac{\pi}{6}-\frac{\pi}{3}-\frac{\pi}{2}$  (a.k.a.  $30-60-90$ ) triangle. As we meditated upon this statement we realized that this triangle can be recursively used to make many aesthetic polygons beyond the equilateral triangle, like a rectangle, parallelogram, kite (all  $2 \times$ ), square, rhombus (both  $4 \times$ ), hexagon (  $6 \times$ ). In doing so we realized what Birkhoff had noticed in the 1930s. To us it also illustrated the second of the two basic principles behind aesthetic experience that had dawned on us namely, symmetry and recursion.

Starting with our study of the [Henon](https://manasataramgini.wordpress.com/2016/12/18/some-reminiscences-of-our-study-of-chaotic-maps-1/) and [Lozi](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/) maps, we soon realized that there is more to this: All fractal objects produce a profound aesthetic experience in some people. I happen to be one of them, though I am cognizant of the fact that this is not universal --- many people are neither wonder-struck nor aesthetically moved when I show them such objects. Nevertheless, the fascination for such objects is widespread across human cultures: In Hindu tradition, temple architecture increasingly converged towards fractional dimensions before its expression was terminated by the coming of the Meccan demons. We had briefly alluded to this earlier, pointing to a relationship between the floor plan of the central spire and the boundary curve produced by an IFS fractal using simple rotations. Other forms of fractal structures were also depicted in Hindu temple art, such as "vegetal motifs". Similarly, Wolfram has documented examples of fractal objects in medieval Western art. Tendencies towards fractality might also be noted in Japanese Ukiyo-e, like in the famous "The Great Wave off Kanagawa". All this suggested to us that complexity might have an important role in aesthetics.

An attempt to understand the role of complexity in aesthetic experience was made by in the 1930s by Birkhoff. For an aesthetic object he defined two values, "order"  $(O)$  and "complexity"  $(C)$ , which led to the aesthetic measure,  $M=\frac{O}{C}$ . However, his  $M$  measures for various polygons did not have a strong correlation with aesthetic experience they produced in us. This made us suspicious of the value of Birkhoff's measure. The pioneer in the study of fractals, Mandelbrot, suggested that fractality of an object might be related to the aesthetic experience it produces. Exploration of fractal maps for aesthetics emerged from pioneering productions of physicists/mathematicians like Mira, Gumowski, Sprott, Pickover and Abraham among others. Sprott and Abraham carried out analyses of the aesthetic experience from fractal objects attempting to relate it to their fractal dimension. Based on their experiments they suggested that a fractal dimension in the middle of the range  $1..2$  was probably a sweet spot for the best aesthetic experience in 2D maps. This generally corresponds to our own aesthetic evaluation of fractal objects.

Thus aesthetic experience was a major driver for us in the exploration of new maps for fractal objects. Thus, we studied the work of Sprott and Pickover among others, reproducing and exploring many of the strange attractors they had discovered using iterative maps. Our own experiments led us to the discovery of the map that produces the "[butterfly attractor](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/)", which we had described earlier. After obtaining it we realized that a certain Martin had also discovered a comparable class of maps which produced an attractor with a considerable richness of structure and aesthetic variety (also called hopalong or chip maps).

Martin's maps are surprisingly simple but produces remarkable beautiful and complex structures. The simplest of these maps the "hopalong" is:

 $$x_{n+1} = y_n - sign(x_n) \sqrt{|b\*x_n-c|}$$ 

 $$y_{n+1} = a - x_n$$ 

Where  $a$ ,  $b$ ,  $c$  are three constants. An examples are shown in Figure 1 and the map is robust over a wide parameter range

![figure1_small](https://manasataramgini.files.wordpress.com/2017/01/figure1_small.png){width="75%"}Figure 1

We modified versions of the Martin map into a more complex set of maps. Map-1 works thus. The first mapping is similar to the Martin process:

 $$x_{n+1} = y_n - sign(x_n) \cos \left( \dfrac{\sqrt{|b x_n - c|} d}{2 \pi} \right) \arctan \left ( \left (log_l \left (|c x_n - b| \right ) \right)^2 \right)$$ 

 $$y_{n+1} = a - x_n$$ 

In place of  $\cos()$  in the first x-mapping we could also have  $\sin()$ .\
Then we subject  $(x_{n+1},y_{n+1})$  to a further "affine" rotation before plotting it:

 $$x_{n+1}=x_{n+1} \cos(k) - y_{n+1} \sin(k)$$ 

 $$y_{n+1}=x_{n+1} \sin(k) + y_{n+1} \cos(k)$$ 

Here we thus have 6 constants:  $a$ ,  $b$ ,  $c$ ,  $d$ ,  $l$ ,  $k$ 

Map-2 works thus:

 $$x_{n+1}=y_n-sign(x_n) \cos \left ( \dfrac {log_{l1}(|b x_n-c|) d} {2 \pi} \right) \arctan \left(\left(log_{l2}\left(|c x_n-b|\right)\right)^2\right)$$ 

 $$y_{n+1}=a-x_n$$ 

In place of  $\cos()$  in the first x-mapping we could also have  $\sin()$ .\
Here again we subject the map to an "affine rotation" before plotting:

 $$x_{n+1}=x_{n+1} \cos(k) - y_{n+1} \sin(k)$$ 

 $$y_{n+1}=x_{n+1} \sin(k) + y_{n+1} \cos(k)$$ 

The constants here are similar to those in the above map but we have one extra one because we have two distinct logarithm terms. Finally in both these maps replacing  $\arctan()$  by the hyperbolic function  $\tanh()$  also produces interesting maps. Figures 2 and 3 respectively show examples of each of the above maps.

![figure2_small](https://manasataramgini.files.wordpress.com/2017/01/figure2_small.png){width="75%"}Figure 2 (First two with  $\cos()$  and second two with  $\sin()$ 

![figure3_small](https://manasataramgini.files.wordpress.com/2017/01/figure3_small.png){width="75%"}Figure 3 (all maps use  $\tanh()$  in place of  $\arctan()$  in Map-2; the third example uses  $\sin()$ .

These maps produce new forms of great beauty, diversity and richness of structure with considerable robustness --- like what the old Hindus would term "nava-nava-चमत्कार" or "muhur-muhur-आश्चर्याय कारणम्". Indeed the beauty in these maps relates to features beyond simple symmetry. They have a degree of rotational symmetry but it is not perfect. What is striking in however the fractional dimension and the tendency for doublings or multiplications (an element see in the evolution of the Hindu temple too). These features deeply touch the heart of beauty. Perhaps the order within chaos is the most beautiful of all.

The maps displayed here along with some additional ones can be see in this [PDF file.](https://app.box.com/s/uxpa68pir47bmq8hd8r5lcv1ym3t593g)

