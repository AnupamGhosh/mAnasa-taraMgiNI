
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The magic of the deva-ogdoad](https://manasataramgini.wordpress.com/2017/05/14/the-magic-of-the-deva-ogdoad/){rel="bookmark"} {#the-magic-of-the-deva-ogdoad .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 14, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/05/14/the-magic-of-the-deva-ogdoad/ "8:04 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Classical Hindu tradition holds that the ogdoad of deva-s corresponding to their directions is:\
Indra: East; Agni: Southeast; Yama: South; निरृति: Southwest; वरुण: West; वायु: Northwest; Kubera: North; ईशान: Northeast.\
The central position might be occupied in certain traditions by प्रजापति, विष्णु, सवितृ or the पुरुष-प्रकृति continuum from which the gods differentiated.

Now, a famous verse, quoted by the great scientist-mathematician भास्कर in his लीलावती, says:

[इन्द्रो वायुर् यमश् चैव नैरृतो मध्यमस् तथा ।]{style="color:#99cc00;"}\
[ईशानश् च कुबेरश् च अग्निर् वरुण एव च ॥]{style="color:#99cc00;"}

Now using the positions of the gods as in the classical ogdoad we get the below matrix:

 $$M=\begin{bmatrix} वायु & kubera & इशान\\ वरुण & madhyama & indra\\ निरृति & yama & agni\\ \end{bmatrix}$$ 

We then plug values for each of the elements of the matrix based on the positions of the gods in the above verse. Indra= 1; वायु= 2; Yama= 3; निरृति= 4; madhyama, the middle or mean= 5; ईशान=6; Kubera= 7; Agni= 8; वरुण= 9\
When we do so we get the famed magic square (sarvatobhadra) of Hindu tradition known as the ubhaya-पञ्चादशक

 $$M=\begin{bmatrix} 2 & 7 & 6\\ 9 & 5 & 1\\ 4 & 3 & 8\\ \end{bmatrix}$$ 

This magic square is found widely in Hindu tradition. It is one of the magic squares that is worshiped by the physician while deploying भेषज-s pertaining to child-birth along with a mantra to Agni, वायु, सूर्य and Indra. There it is specified using partial भूतसंख्य notation as:

[वसु-गुणाब्ध्य्-एक-बाण-नव-षट्-सप्त-युगैः क्रमात् ।]{style="color:#99cc00;"}\
[यन्त्रं तु पञ्चदशकं लिख्यते नव-कोष्ठके ॥]{style="color:#99cc00;"}

8, 3, 4; 1, 5, 9; 6, 7, 2 in order are to be written in the 9 cells for the yantra with sum of 15.

It is also recorded in शैव traditions as a yantra. Indeed tradition holds that the mathematics of magic squares (भद्रगणित) was taught by the god Rudra to the यक्षराट् Kubera or his ectype मणिभद्र (e.g. see नरायण पण्डित's chapter on भद्रगणित).

There is literally much mathematical magic in the bhadra square:

 1.  If you add the rows, columns and diagonals you get 15, which is what gives it name. Thus the magic constant  $\sum(M)=15$ 


 2.  If you reflect the square about any of the sides or transpose the matrix i.e. make rows into columns then the resultant square is also magic.


 3.  It contains the first 9 positive integers and is a  $3 \times 3$  magic square; hence, its order  $n=3$  and its elements are  $1:n^2$ . For such a magic square (known as a normal or fundamental magic square) the  $\sum(M)=\frac{n^3+n}{2}=\frac{27+3}{2}=15$ 


 4.  If you put any sequence of consecutive positive integers serially in the same positions as the occurrence of 1:9 in the square then you get another magic square. Thus you can generate infinite magic squares. Tradition holds that the ancient astronomer Garga described this additive derivation of a sequence of magic squares. He states that sun is represented by the above deva square; by adding 1 successively the sequence of squares associated with other planetary bodies. Adding 19 to each element generates the Kaubera-sarvatobhadra:

 $$M_k=\begin{bmatrix} 21 & 26 & 25 \\ 28 & 24 & 20 \\ 23 & 22 & 27 \\ \end{bmatrix}$$ 

Its magic constant is  $\sum(M_k)=72$ 


 5.  For the deva (fundamental) magic square and all magic squares sharing the general form with it (see below for general form) the arithmetic mean of the elements of the matrix equals the central element or the madhyama as it is termed in Hindu tradition:  $\mu(M)=M\lbrack 2,2\rbrack$ . It is both central positionally and in the sense of being the mean. Further, for the deva square and all its derivatives obtained as above  $\sum(M)=\mu(M)\times n$ , where  $n=3$  the order of the matrix. Thus for the deva square it is  $3\times 5=15$  and for the Kaubera square it is  $3\times 24=72$ 

More generally we have the सिद्धान्त attributed to नरायण पण्डित which generates these magic squares using the madhyama ( $a$ ) and 2 other numbers  $b,c$  thus:

 $$M_g=\begin{bmatrix} a+c & a-b-c & a+b \\ a+b-c & a & a-b+c \\ a-b & a+b+c & a-c \\ \end{bmatrix}$$ 

Thus we get the magic constant as  $\sum(M)=3 \times a$ . We get the deva square by plugging  $a = 5, b = 1, c = -3$ . This general form of the square can be used to prove other results as those presented below pertaining to such squares. By plugging  $a=10, b=-2, c=6$  we get the उभयत्रिंशक seen in शैव and medical works:

 $$M_{ut}=\begin{bmatrix} 16 & 6 & 8\\ 2 & 10 & 18\\ 12 & 14 & 4\\ \end{bmatrix}$$ 

Its magic constant is  $\sum(M_{ut})=30$  the idealized number of days in the lunar month and its dot-product constant (see below) is 364 which is 1 short of the idealized number of days in the year.


 6.  Whereas the off-diagonals or the wrapped diagonals do not add up to  $\sum(M)$  their sums  $\sum(M_{od})$  do show a regular relationship:  $\sum(M_{od})=n \times Cr(M)$  where  $Cr(M)\Rightarrow M\lbrack 1,1\rbrack , M\lbrack 1,3\rbrack , M\lbrack 3,1\rbrack , M\lbrack 3,3\rbrack$  i.e. the corner elements. Thus for the four off-diagonals of the deva-square we have:

 $$2+1+3=3 \times 2=6\\ 4+1+7=3 \times 4=12\\ 6+9+3=3 \times 6=18\\ 8+9+7=3 \times 8=24$$ 

One immediately notices that the mean of the sum of these off-diagonals is:  $\frac{6+12+18+24}{4}=15$ . This property is a general feature that holds good for all the magic squares derived from the same general form as the deva square such as the Kaubera square.


 7.  Remarkably if we write out each row, column and diagonals (including the off-diagonals) as 3 numbers forward and reverse then the following relationships hold:

 $$276+951+438=672+159+834=\\ 294+753+618=492+357+816=\\ 258+714+693=852+396+417=\\ 654+798+213=456+312+897=1665$$ 

Now if one does the same with any other additively derived magic square we get the same. But we need to keep in mind the places thus for the Kaubera square we get:

 $$212625+282420+232227=252621+202428+272223=\\ 212823+262422+252027=232821+222426+272025=\\ 212427+262023+252822=272421+232026+222825=\\ 252423+262827+212022=232425+272826+222021=727272$$ 

Thus this summation property is a general one for such magic squares. Further if we denote this sum as  $S$  then its ratio to the magic constant  $\frac{S}{\sum(M)}$  is a mirror number of the form 111, 10101, 1001001 etc. For the deva square made up of single digit numbers it its 111. For magic squares with double digit numbers it is 10101. For magic squares with triple digit numbers it is 1001001, so on.


 8.  Notably, as above if we write out each row, column and diagonals (including the off-diagonals) as 3 numbers forward and reverse then the following relationship holds for their squares:

 $$276^2+951^2+438^2=672^2+159^2+834^2$$ 

 $$294^2+753^2+618^2=492^2+357^2+816^2$$ 

 $$258^2+714^2+693^2=852^2+396^2+417^2$$ 

 $$654^2+798^2+213^2=456^2+312^2+897^2$$ 

This is also generally true for generalizations of the deva square.


 9.  The dot product rules: Let  $R_j, C_j$  be the rows and columns of such magic squares. For our 3rd order squares they define three element vectors. The dot product of these vectors would be a scalar e.g.:  $R_1 \cdot R_2=M_{11}M_{21}+M_{12}M_{22}+M_{13}M_{23}$ . Then for the deva-square and its generalization we have:\
 $R_j\cdot C_j=K$ ; where  $K$  is the dot product constant of the magic square.

 $$R_1 \cdot R_2= R_2 \cdot R_3$  and  $C_1 \cdot C_2=C_2 \cdot C_3$$ 

 10.  We can create a square matrix  $D$  by the process of cyclic difference of rows or columns:  $D_R=\lbrack R_1-R_2, R_2-R_3, R_3-R_1\rbrack$  or  $D_C=\lbrack C_1-C_2, C_2-C_3, C_3-C_1\rbrack$ .  $D_R, D_C$  are zero-sum semi-magic squares in that the rows and columns add up to zero but the diagonals do not. This applies for the deva square and its generalization. Thus for the deva square we have:

 $$D_R=\begin{bmatrix} -7 & 2 & 5\\ 5 & 2 & -7\\ 2 & -4 & 2\\ \end{bmatrix}$$ 

 $$D_C=\begin{bmatrix} -5 & 4 & 1\\ 1 & 4 & -5\\ 4 & -8 & 4\\ \end{bmatrix}$$ 

 11.  For the deva square and its additive derivatives we can define the eigenvalues  $\lambda$  thus:\
 $(M-\lambda I)\overrightarrow{v}=0$ ; where  $\overrightarrow{v}$  is the eigenvector and I is the identity matrix:

 $$M_k=\begin{bmatrix} 1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \\ \end{bmatrix}$$ 

Then for the deva-square  $\lambda$  is obtained by solving the cubic equation:

 $$|M-\lambda I|= (\lambda-15)(\lambda^2-24)=0$$ 

Strikingly, the eigenvalues are  $15,-2\sqrt{6},2\sqrt{6}$ . Thus, the largest eigenvalue square is its magic constant. More generally for all additive derivatives of the deva square the eigenvalues are:  $\lambda= \sum(M),-2\sqrt{6},2\sqrt{6}$ 

For the deva square the Eigenvectors are:\
 $\overrightarrow{v_1}= \begin{bmatrix} -\frac{1}{\sqrt{3}} \\ -\frac{1}{\sqrt{3}} \\ -\frac{1}{\sqrt{3}} \\ \end{bmatrix}$ ;\
 $\overrightarrow{v_2}= \begin{bmatrix} -\frac{1}{3}-\frac{1}{\sqrt{6}} \\ \frac{2}{3} \\ -\frac{1}{3}+\frac{1}{\sqrt{6}} \\ \end{bmatrix}$ ;

 $$\overrightarrow{v_3}= \begin{bmatrix} \frac{1}{3}-\frac{1}{\sqrt{6}} \\ -\frac{2}{3} \\ \frac{1}{3}+\frac{1}{\sqrt{6}}\\ \end{bmatrix}$$ 

Notably for all additive derivatives of the deva squares the eigenvectors take the above forms though the signs of the values might be reversed.


 12.  For such 3rd order magic squares each row, column, diagonal and off-diagonal can be seen as a triple of numbers that can specify a point in space as its x, y and z coordinates. We have a total of 3 rows + 3 columns + 2 diagonals + 4 off-diagonals= 12 triples. By permuting the order in which each number in the triple is taken we can specify 6 points from each triple. Thus we have a total of  $6 \times 12 = 72$  points. Of these  $x+y+z=\sum(M)$  for those points derived from the columns, rows and diagonals. Thus, 48 out of the 72 points lie on the plane  $x+y+z=\sum(M)$ . While those from the off-diagonals lie on 4 different parallel planes on either side of the main plane. From their equations we know\
that these planes have a vector of the form  $\langle 1,1,1 \rangle$  and will make with the horizontal plane (  $z=0; \langle 0,0,1 \rangle$ ) the angle:  $\arccos\left(\frac{1}{\sqrt{3}}\right)=\arcsin\left(\sqrt{\frac{2}{3}}\right)= \arctan\left(\sqrt{2}\right) \approx 54.7356\^o$ 

Thus, these points define a regular hexagonal bipyramidal frustum, with the 48 points magic constant points on the principal plane forming a nice pattern of 4 concentric hexagons.

![magic_hexagon](https://manasataramgini.files.wordpress.com/2017/05/magic_hexagon.png){width="75%"}

Figure 1: the principal plane with 48 points from the deva square

The 12 points from two of the off-diagonals form 2 hexagons congruent to the inner-most hexagon on either side of the principal plane and lie farthest from it. The remaining 12 points from the other two off-diagonals form 2 hexagons on either side of the principal plane and are congruent to the last but 1 of the concentric hexagons counting from inside.

![magic_frustum](https://manasataramgini.files.wordpress.com/2017/05/magic_frustum.png){width="75%"}Figure 2: The hexagonal bipyramidal frustum of the deva square

For the deva square the four concentric hexagons have side-lengths of  $\sqrt{2}, 2\sqrt{2}, 3\sqrt{2}, 4\sqrt{2}$ , with the first and the last being also the sides of the hexagons forming the base and the height of bipyramidal frustum. The pyramidal edge of the frustum is  $3\sqrt{5}$  and the remaining off diagonal hexagons cut the pyramidal side into segments in the proportion  $1:2$ . It may be considered the मण्डल of the deva-s.


 13.  Such magic squares can also be converted to the Hindu magic triangles using नरायण पण्डित's simple algorithm. Below is illustrated the 4-fold magic triangle, the पञ्चविंशति-yoni derived from the deva square.

![panchaviMshati_yoni](https://manasataramgini.files.wordpress.com/2017/05/panchavimshati_yoni1.png){width="75%"}Figure 3: the पञ्चविंशति-yoni

While magic squares might seem to the deracinated modern as a quaint superstition of the Hindu and some other old civilizations like the Chinese and Japanese, they might have some deeper significance. For example there is the deceptively simple looking but yet unsolved mystery: does a order 3 magic square exist where all the elements are perfect squares, i.e. are they squares on sides of a right triangle with integer sides? Here we have looked only at what the Hindus termed nava-कोष्ठक-विषंअगर्भ-bhadra-s. But the several other magic figurate numbers described in Hindu tradition might have other interesting mathematics lurking in them. Notably, the great Srinivasa Ramanujan also (re)discovered several संअगर्भ-bhadra-s similar to traditional Hindu versions (like the ones found on चन्द्रात्रेय temples, including one demolished by the Moslem tyrant Baboor at Gwalior) with interesting properties. Additional also gives the above formula for the general nava-कोष्ठक-विषंअगर्भ-bhadra-s

More generally they might be seen as a numerical representative of a conservation law and the structures that emerge naturally once such a law is laid down. One can also use it as an analogical aid for understanding structures emerging under constraints. One could conceive an imaginary model for a protein where the amino acids (standing for numbers in the square) are under interaction constraints with others to attain some total constant stabilizing energy. Under such a scenario the magic square plane as derived above would represent a  $\beta$ -sheet.

