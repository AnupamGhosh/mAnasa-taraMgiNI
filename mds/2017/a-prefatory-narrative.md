
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A prefatory narrative](https://manasataramgini.wordpress.com/2017/04/08/a-prefatory-narrative/){rel="bookmark"} {#a-prefatory-narrative .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 8, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/04/08/a-prefatory-narrative/ "6:03 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Jhilleeka and Prachetas were visiting Lootika and Somakhya. Prithika, the daughter of the latter two was much excited as she resembled and was to resemble her youngest aunt in more than one way -- they seemed to have a some special connection. Thus, she clung to Jhilleeka until she was put in bed. Then the adults went up to the terrace of Somakhya and Lootika's house, where Jhilleeka started narrating the special matters pertain to their visit even as the pleasant प्रथमी moon shone above them: "A while back when you and Vrishchika were away Varoli and I had conspired with Prachetas and Mitrayu to do our summer internships at labs in Turushkarajanagara. One evening after dinner we went up to the terrace of the house in which were staying and started plying the planchette with the hope of snaring a वेताल. I must confess that I wished to test Prachetas' mettle to see if he was really up to my level in prayoga matters. अग्रजा I wonder if you did something like that with Somakhya? Lootika merely smiled and said: "I don't want to break your narrative with our exploits; pray continue."

Jhilleeka: "After a while a वेताल did seize our भूतचक्र. He announced his name पृथुरोमन्. I immediately, deployed the mantra known as हरहुंकारम् and made the वेताल seize Prachetas. He seemed to be totally taken over by it. Varoli and Mitrayu wanted to intervene right away but I bound both them with the सरस्वती-bandha and they were unable to intervene. Only late that night Varoli broke free and greatly upbraided me. She even wanted to call Vrishchika and tell her what I had done but I convinced her that I would ensure that no real harm comes to Prachetas in the event he is unable to defend himself. He will now tell you what happened next."

Prachetas: "Thus, attacked by surprise by my future पत्नी I succumbed to the seizure by the वेताल. I took a bus in that state to seizure and went to what in those days was a little hamlet at the outskirts of Turushkarajanagara. There, I walked at the dead of night to the boundary of a cemetery. Despite being a just a hamlet there seemed to be a brisk flow of corpses which were being fed to क्रव्याद. I wandering under the possession sneaked in through a hole in fence of the cemetery to enter the grounds. Evidently the वेताल was guiding me there for some reason. But whatever the reason, I suddenly realized what had happened to me. I realized that it was my future पत्नी who had caused this possession -- but at that moment I was livid with anger and wanted to perform a powerful prayoga to strike back at her. But first I had to get myself free of the वेताल. My eyes had by then adjusted themselves to the dark and I saw a corpse of a young man lying in the vicinity. He seemed to have a noble bearing. I wondered who had left the corpse there without it being cremated. With some effort I deployed the gandharva mantra and performed स्वावेश with a gandharva, who then drove out the वेताल. Intended to dispatch the gandharva then at Jhilli so that she might seized by him like विश्वावसु seizing a young woman. The वेताल entered the corpse that I had just seen. The corpse underwent a strange transmogrification thereafter. The young man aged rapidly and his hair fell off and what remained whitened. Then the corpse seated itself cross-legged and began to speak: 'That girl Jhilleeka who is your friend is going to be your wife in the not so distant future so don't let any harm reach her due to your प्रतिक्रिया. I realized that when the वेताल speaks we get a prognostication. To make him speak more I deployed the सरस्वती-प्रवाह.

He pranced about a bit and then grabbing one hand of his with another he said sat down and scrapped some ashes and applied it on himself. I observed him more closely and he looked strangely familiar giving me a fright for the first time. He then said: "I was a ब्राह्मण like you. I descended from the clan of a भार्गव or an अङ्गिरस. I performed many vaidika rituals and drank soma. One day when I traveled to Turushakarajanagara to visit the #\$*& institute. \[Prachetas: strangely that happened to be the institute next to where Jhilli and I worked]. As I was talking to my host there suddenly a centrifuge spun out of control in the floor below us and it smashed through our floor and struck me. Thus, I expired. As I had no close relatives, my corpse was dumped by the cops after their investigation at the cremation ground for the general cremation which would take place at 4:00 AM. I glad you have heard my story. In my pocket lies a letter for a girl who lives in the Bhatahata sector the city. If you post it so that it might reach her then good would come to you. But I tell you a death not different from mine could be yours and your wife's too unless you become the master of तीक्ष्णतुण्ड. He then handed me the letter in his pocket and shrieking loudly ran into the darkness and dropped down about 25 meters away from me. I snapped out of the आवेश and withdrew the gandharva I had intended to seize pretty Jhilli. I took the first bus I could get in the morning and returned to our rental after posting the letter of the animated corpse. I was brooding over the prognosis of our deaths and wondering what तीक्ष्णतुण्ड was as I entered our dwelling. I and Mitrayu lived on the ground floor while Jhilli and Varoli lived on the first floor. Even as I came in Mitrayu had freed himself from the bandha and had joined Varoli in screaming at Jhilli. I calmed them all down and even as I did so I forgot about the strange prognosis.

Jhilleeka: "Last night I had a terrifying dream in which I saw a spinning object come flying through the floor and kill a young man. I then saw a young woman jump off a building and die. I narrated it to Prachetas who told me it was a very deadly sign and mentioned that only if we figured out what तीक्ष्णतुण्ड was we could be saved. You अग्रजा and Somakhya are our elders and guides hence we place ourselves at your feet so that you may help us cross the noose of Vaivasvata that the young ब्राह्मण in Turushkarajanagara could not.

Somakhya: "By तीक्ष्णतुण्ड what is meant is the mantra-vidhi of शरभेशान that we shall we reveal to you. It is hard to master even by a good ब्राह्मण. Many have tried and failed but he who does so shall be saved like मार्कण्देय of the race of the भृगु-s when confronted by Antaka at the dead of night when the powers of the दानव-s are exalted."


