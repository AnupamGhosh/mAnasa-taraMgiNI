
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Doubling the cube with ellipses](https://manasataramgini.wordpress.com/2017/05/23/doubling-the-cube-with-ellipses/){rel="bookmark"} {#doubling-the-cube-with-ellipses .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 23, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/05/23/doubling-the-cube-with-ellipses/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The problem of doubling of the cube which emerged in the context of the doubling of the cubical altar of the great god Apollo cannot be solved using just a straight-edge and a compass. It needs one to construct a segment of length of  $\sqrt\lbrack 3\rbrack {2}$ . The yavana sages of yore solved the problem using various special curves: Menaechmus and Dinostratus using [parabolas and hyperbolas](https://manasataramgini.wordpress.com/2016/04/10/a-biographical-journey-from-conics-to-ovals/); Eudoxus using the kampyle; Nicomedes using the conchoid; Diocles using the cissoid. But to my knowledge no yavana used the other conic, ellipse, to solve this problem. Who did it first whether it was the French savant Descartes or some other modern mathematician it is not entirely clear. However, it can be done using segments which can be constructed just using a compass and straight-edge i.e. rational fractions and square roots (which can be drawn using the geometric mean theorem), and ellipses drawn using those lengths. To draw an ellipse we need to know its semimajor and semiminor axes. Additionally, we need to know its center to locate it based on a particular point and the direction of the axes. Below is the procedure to do the same:
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/05/doubling_cube_ellipses_calc2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```



 1.  Let  $\overline{OA}$  be the side of the cube to be doubled. All lengths/distances from now on will be expressed in  $\overline{OA}$  units.

 2.  Draw a line by extending  $\overline{OA}$ . Draw two lines perpendicular to it at points J and K at distances  $\frac{1}{3}$  and  $\frac{1}{2}$  respectively from point O.

 3.  On the line through J mark point B such that  $\overline{JB} = \frac{1}{2} + \sqrt{2}$ . On the line through K mark point C such that  $\overline{KC} = \sqrt{2}$ 

 4.  With B as a center to draw an ellipse with semimajor and semiminor axes  $a_1=\frac{1}{2}\cdot\sqrt{\frac{31}{3}}; \; b_1=\frac{1}{2}\cdot\frac{\sqrt{31}}{3}$  and major axis along  $\overline{JB}$ .

 5.  With C as a center to draw an ellipse with semimajor and semiminor axes  $a_2=\frac{1}{2}\cdot\sqrt{6}; \; b_2=\frac{1}{2}\cdot\sqrt{3}$  and major axis along  $\overline{KC}$ .

 6.  Obtain the two points of intersection of the ellipses: D and E. Drop a perpendicular from D to  $\overleftrightarrow{OA}$  to cut it at point F.

 7.  Draw  $\overline{OF}=\sqrt\lbrack 3\rbrack {2}\cdot \overline{OA}$ .

The secret behind this rather simple-looking method needs some algebra to simultaneously solve the equations of the two ellipses choosing coordinates and axes appropriately which in the pre-computer era could have been difficult for the lay person (Remember mathematicians like Descartes handled such calculations long before computers though). Below are the double cubes as an offering for Apollo or should we say our equivalent deity Rudra.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/05/doubling_cube_ellipses.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


