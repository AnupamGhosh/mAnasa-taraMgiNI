
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The रामायण and a para-रामायण in numbers-I: epic as religion](https://manasataramgini.wordpress.com/2017/02/12/the-ramaya%e1%b9%87a-and-a-para-ramaya%e1%b9%87a-in-numbers-i-epic-as-religion/){rel="bookmark"} {#the-रमयण-and-a-para-रमयण-in-numbers-i-epic-as-religion .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 12, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/02/12/the-ramaya%e1%b9%87a-and-a-para-ramaya%e1%b9%87a-in-numbers-i-epic-as-religion/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note may be read as part of our studies on the रामायण and para-रामायण-s of which [an earlier part is presented here](https://manasataramgini.wordpress.com/2016/08/12/some-words-on-para-ramaya%e1%b9%87a-s-i/).

A study of the epic in Indo-European tradition suggests that there were two registers of the old Indo-European religion. While today both of them survive together with any vigor only among the Hindus, until not too long ago these registers showed some survival even among their Iranian cousins. From these it is apparent the first register is the "high religion" which manifests as श्रौत and स्मार्त performance. Among the आर्य-s this further evolved into other manifestations as seen in the tantra-s of the sectarian traditions. Nevertheless, the Vedic base remained the model for most of these later developments. On the other hand the lay manifestation of religion was by the medium of the epic or इतिहास-s in India. Their religious value elsewhere in the Indo-European world was apparent in Greece. Indeed, in the classical Greco-Roman confluence the last attempt of reviving the religion by emperor Julian, which was being swept away by the "Typhonic" evil of the preta-moha, involved a focus on the religious facet of the Homeric epics.

In both India and Greece there are two epics, which have numerous parallels in their motifs, and resonate even in their overall themes. However, in India each has a distinct character. The रामायण is what might be termed "the universal epic of ideals." The महाभारत is on the other hand our national epic, the epic of the first आर्य nation in India, the foundation on which the modern Hindu nation rests. The Iranians have a comparable national epic in the form of the क्शथाय-नामग् and its precursors but apparently lack the universal epic. Among the Greeks to an extent the Iliad probably played a national role but tended towards the universal in the later phase. It was the universal epic, the रामायण, which was the vehicle of the आर्य-dharma beyond boundaries of जम्बुद्वीप. In its role as the foundation of the "lay religion" it was remarkably tenacious and withstood the assault of the other Abrahamistic evil in the form the मरून्माद in Indonesia. It also served as a means of preserving the आर्य-dharma in both India and in the east against the assault of the Aryan counter-religions promulgated by the naked-one and the ground-toucher. Indeed, in India the powerful force of the इतिहास-s was realized by successors of both these heterodox promulgators, who either attacked the इतिहास-s or tried to have people not attend their exposition.

The remainder of this note we shall look at the रामायण via numbers, which was part of my self-discovery of its key religious facet. Most importantly, it reveals something about the deep layers of the आर्य-dharma and its evolution over time. Before we get started, a few caveats should be stated upfront: The texts I am using are the so-called "critical editions" of the रामायण and महाभारत for the first para-रामायण, the रामोपाख्यन of मार्कण्डेय. These critical editions have their faults but are available in electronic form and are thus amenable to semi-automatic text analysis by regular expression searches. Almost all of these analysis were performed by means of such. The Heidelberg system has a very sophisticated text-parsing mechanism for several Sanskrit works but I did not use it except for one word search (inspired by an [interlocutor](https://shreevatsa.wordpress.com/) on Twitter), which will be discussed as part of another note, as it was not quite compatible with my command line pipeline. So my system could have some deficiencies but manual checking of the results shows that it is largely correct and the magnitudes should be taken as genuinely representative. In general for this activity you need to have a good knowledge of the various names of the gods, characters and weapons used in the text. Although not a पण्डित, being a ब्राह्मण, I believe that I have a level of command of this as a reasonable representative of my वर्ण should, so the results might be taken as generally reliable. Finally, I am aware that in white indological circles some work in this direction has been done by the likes of Brockington. However, I did not consult his papers as I wanted to have my own unbiased experience of the data and conclusions from it. More generally, wherever there is tractable data I believe that an educated man should analyze it himself rather than wholly relying on hearsay of others.

First we shall look at the gross features of the रामायण (Figure 1):

![ramayana_stats](https://manasataramgini.files.wordpress.com/2017/02/ramayana_stats.png){width="75%"}Figure 1

  - The text has seven काण्ड-s, which are composed of multiple sarga-s, which in turn are composed of श्लोक-s. The अयोध्या, Yuddha and Uttara have much more than median number of sarga-s and श्लोक-s.

  - However, it is notable that except for Yuddha the other काण्ड-s have a nearly constant median श्लोक count per sarga (\~24-27). This was the likely count maintained by Pracetas and his son वाल्मीकि the original composers of the रामायण for a typical काण्ड, probably aiming to be around 25 श्लोक-s. The Yuddha in contrast is longer both in terms of number of sarga-s and also the number of श्लोक-s per sarga. Clearly, this is a distinct composition suggesting that a different style was adopted on purpose for the military narratives typical of Indo-European epics. Unlike the median, the mean श्लोक count per sarga is higher with anomalies for both the Yuddha and Sundara. We shall take a closer look at this in Figure 2

![sarga_length_ramayana](https://manasataramgini.files.wordpress.com/2017/02/sarga_length_ramayana.png){width="75%"}

Figure 2.

  - Here we see the actual frequency distribution of the sarga length across the रामायण and per काण्ड in श्लोक-s: Here the differences are more apparent.

  - The first three काण्ड-s are "tighter" in distribution with modal sarga length close to the median length. The किष्किन्धा shows some divergence in the form of a fat tail with several sarga-s in of great length (40-70 श्लोक-s).

  - The Sundara is most unusual in having a bimodal distribution with short sarga-s peaking less than 20 in length and longer ones peaking around 35. This pattern suggests a deliberate compositional shift perhaps reflecting the peculiar nature of the Sundara as an avenue for display of poetic beauty.

  - The Yuddha is clearly distinct with the general peak and median length being shifted to being between 30 and 40. There is also a sizable fraction of very long sarga-s above 40 going all the way to well over 80. This again emphasizes the distinctness of the battle narratives where the long recitations perhaps appealed to the war-like ancient आर्य audience who might have been in similar battles in their own lives.

  - Finally, Uttara shows a typical median distribution of sarga length with a major fraction of sarga-s distributed around this value. However, it is distinct in showing a bimodality with two peaks one with length between 10-20 श्लोक-s and another with length between 40-45 श्लोक-s. This suggests a certain composite character with the shorter sarga-s probably representing the several short narratives included in it and the long ones relating to battle-sequences comparable to the Yuddha.

Now coming to the core issue of religion we shall look at the frequency of occurrence of the gods in the रामायण (Figure 3)

![deva_ramayana](https://manasataramgini.files.wordpress.com/2017/02/deva_ramayana.png){width="75%"}Figure 3

  - It is apparent that Indra is literally the leader of the gods. He occurs nearly twice as frequently as the next contender प्रजापति or ब्रह्मा. He is the standard for all comparisons and the hero of the रामायण is frequently likened to him. Indeed, there is a the tacit understanding that Indra used his माया to take the form of a man in order to slay रावण. This is suggested by मन्दोदरी's lament upon her husband's death:

[अथ वा राम-रूपेण वासवः स्वयम् आगतः ।]{style="color:#0000ff;"}\
[मायां तव विनाशाय विधायाप्रतितर्किताम् ॥]{style="color:#0000ff;"}R 6.99.10

Or indeed Indra himself appeared in the form of राम,\
for ruining and slaying you using impenetrable illusion.

Thus, it is hinted that Indra, who right in the ऋग्वेद is famous for his माया, uses it to kill the रक्षस्.

Now again, though the core काण्ड narrative itself mentions राम taking the weapons of विष्णु from Agastya, in the preamble it is mentioned that they were the weapons of Indra himself.

[अगस्त्यवचनाच् चैव जग्राहैन्द्रं शरासनम् ।]{style="color:#0000ff;"}\
[खड्गं च परमप्रीतस् तूणी चाक्षय-सायकौ ॥ ]{style="color:#99cc00;"}R 1.1.34c

At Agastya's words राम verily took up Indra's bow,\
sword and the excellent inexhaustible quiver.

Of course the grand finale of the युद्धकाण्ड has राम ride the chariot of Indra steered by मातलि himself and using Indra-s weapons:

[सहस्राक्षेण काकुत्स्थ रथो 'यं विजयाय ते ।]{style="color:#0000ff;"}\
[दत्तस् तव महासत्त्व श्रीमाञ् शत्रुनिबर्हणः ॥]{style="color:#0000ff;"}

O descendant of Kakutstha, the slayer of foes, one of great strength and opulence, the thousand-eyed Indra has given for your victory this chariot.

[इदम् ऐन्द्रं महच्चापं कवचं चाग्नि-संनिभम् ।]{style="color:#0000ff;"}\
[शराश् चादित्यसंकाशाः शक्तिश् च विमला शिताः ॥]{style="color:#0000ff;"}R 6.90.9-6.90.10

\[He has also given] this great bow of Indra and his armor which glow like fire,\
as also these arrows blazing like the sun and this bright sharp spear.

Finally, to slay रावण he is said to use the missile made by ब्रह्मा. But even here it is a mighty missile made by ब्रह्मा in the manner of त्वष्टृ in the Veda for Indra to conquer the three worlds:

[ब्रह्मणा निर्मितं पूर्वम् इन्द्रार्थम् अमितौजसा ।]{style="color:#0000ff;"}\
[दत्तं सुरपतेः पूर्वं त्रिलोक-जयकाङ्क्षिणः ॥]{style="color:#0000ff;"} R 6.97.5c

\[The missile] was formerly made by the god ब्रह्मा of immeasurable might for the sake of Indra. It was given to the lord of the gods \[Indra] when he formerly sought to conquer the three worlds.

The missile itself has characteristics that are clearly suggestive of the vajra of Indra:\
"[रथ-नागाश्व-वृन्दानां भेदनं क्षिप्रकारिणम् ।]{style="color:#0000ff;"}R 6.97.8c"\
The swift acting \[missile] was the smasher of \[entire] troops of chariots, elephants and horses.\
"[द्वाराणां परिघाणां च गिरीणाम् अपि भेदनम् ।]{style="color:#0000ff;"}R 6.97.9"\
It was capable of smashing its way through through bar-reinforced doors and also mountains"

Tellingly it is described as "वज्रसारम्" (imbued with the essence of the vajra), and "yama-रूपम्" (of the form of Yama). The latter epithet directly recalls the the first person statement of Indra in the 10th मण्डल of the ऋग्वेद where he says that he wields a missile that is like Yama himself.

The deployment of this missile by राम on रावण is again thus described thus:

[स वज्र इव दुर्धर्षो वज्रबाहु-विसर्जितः ।]{style="color:#0000ff;"}\
[कृतान्त इव चावार्यो न्यपतद् रावणोरसि ॥]{style="color:#0000ff;"}

The missile, difficult to defend against like the vajra hurled by the arm of Indra, unstoppable like the causer of death (Yama), hit रावण on his chest.

Thus struck रावण fell:

[गतासुर् भीमवेगस् तु नैरृतेन्द्रो महाद्युतिः ।]{style="color:#0000ff;"}\
[पपात स्यन्दनाद् भूमौ वृत्रो वज्रहतो यथा ॥]{style="color:#0000ff;"} R 6.97.021

His life-breath having departed the lord of the नैरृत-s of fierce speed and great luster fell from his battle-car to the ground like वृत्र struck by the vajra.

Thus, to the ancient आर्य audience this recitation would have immediately evoked the imagery of the ऋग्वेद, where Indra's heroic deeds in battle are praised in the ritual.\
In conclusion, this makes it is clear that the original रामायण was composed in a setting where the aindra flavor of the आर्य-dharma was the still the main expression of the religion. It is indeed likely that that it was tacitly implied that राम was a manifestation of Indra in human form to kill रावण.

Now what about the rest of the Vaidika pantheon. Was it like the late Vedic age or the संहिता-s themselves?

  - We see considerable prominence for सूर्य, वायु, विष्णु, Yama, Rudra in addition the प्रजापति/ब्रह्म्मा. However, the अश्विन्-s, the Marut-s, the distinct आदित्य-s are not prominent. Agni has a moderate presence although primarily in the sense of poetic similes. This suggests that period of composition while still marked by Aindra dominance was one which was probably positionally distinct and temporally much later than the संहिता period. Of the prominent deities the indistinct solar deity suggests the rise of the [new Indic solar cult](https://manasataramgini.wordpress.com/2015/08/12/a-note-on-the-pantheon-of-the-indian-saura-tradition/) with links to the older आदित्य system but certainly very distinct in its manifestation with parallels to those seen in the Iranian world.

  - -[The prominence of वायु](https://manasataramgini.wordpress.com/2016/12/11/matters-of-religion-discussion-on-vayavya-offerings/) is related to his association with Indra in battle against the दानव-s, a feature which was prominent in both the Veda and the para-Vedic tradition. The latter is partly reflected in the रामायण and also relates to the importance of his son, Hanumat in the epic. We should mention here that in counting वायु we have almost entirely avoided including the incidental occurrence of his name as a epithet of Hanumat. A similar situation accounts in part of the prominence of विष्णु; however, his story has more which will be further discussed below. If Indra is identified with राम, and the role of वायु is taken by Hanumat, then it is rather obvious that the place of विष्णु is taken by लक्ष्मण. The रामायण makes this obvious in the statement:

[विक्रमिष्यति रक्षःसु भर्ता ते सह-लक्ष्मणः।]{style="color:#0000ff;"}\
[यथा शत्रुषु शत्रुघ्नो विष्णुना सह वासवः ॥]{style="color:#0000ff;"}6.024.029c

Your husband [राम] with invade the रक्षस् with his brother लक्ष्मण even as the foe-killing Indra against his foes along with विष्णु.\
or:

[स ददर्श ततो रामं तिष्ठन्तम् अपराजितम् ।]{style="color:#0000ff;"}\
[लक्ष्मणेन सह भ्रात्रा विष्णुना वासवं यथा ॥]{style="color:#0000ff;"}R 6.87.9

He then saw the undefeated राम standing with his brother लक्ष्मण like Indra with विष्णु.

Like in the Veda the most frequently referred act of विष्णु are three world-conquering strides suggesting that this old motif was still of great importance in the age of the रामायण rather than later elements like his incarnations or battles with certain demons. His weapon, the cakra is frequently mentioned, unlike in the Veda, where other gods are described as wielding it but not विष्णु. This suggests that the रामायण marks a stage after the संहिता period where the cakra became established as the favored weapon of विष्णु. However, it does preserve the memory of Indra's cakra mentioned in the श्रुति in R 1.26.5. Notably, विष्णु is mentioned as killing the demon Naraka in a conflict which was perhaps coupled with Indra's battle with शम्बर:\
[शम्बरो देवराजेन नरको विष्णुना यथा ।]{style="color:#0000ff;"} R 6.57.7

Thus is appears possible that this exploit of विष्णु was transferred to his अवतार कृष्ण in a later retelling of the legend. Indeed, the whole कार्ष्णी retelling has विष्णु only thinly veiled by the Yadu hero.

  - Of the other gods, गरुड and Kubera despite having a presence in the Veda are not prominent there beyond specific rituals. Nevertheless, even there, there is an under-current that they had a role of some note in household rituals. Their importance clearly comes out in the रामायण. In particular it is clear that the whole epic has a frame that tries to highlight the might of रावण as the expense of Kubera, implying that he was an important deity of the time. He is named as one of the great regal gods along with kings वरुण and Yama and his greatness is repeatedly mentioned. This importance of Kubera, as we have seen before has a strong para-रामायण tradition too as laid out in the रामोपाख्यान. Notably, in that relatively short text he is 3rd most frequently mentioned deity (Figure 4) suggesting that his importance was visible throughout the whole early phase of the रामायण tradition.

![deva_ramopakhyana](https://manasataramgini.files.wordpress.com/2017/02/deva_ramopakhyana.png){width="75%"}Figure 4

His importance is also implied by his airplane the पुष्पक playing a notable role in the epic. His son नलकूबर is also seen as cursing रावण resulting in the protection of Sitā's chastity upon her abduction. Kubera is also described as providing a secret missile to लक्ष्मण in his dream that allowed him to counter the Yama weapon of मेघनाद in their final encounter.

[लक्ष्मणो 'प्य् आददे बाणम् अन्यं भीम-पराक्रमः ।]{style="color:#0000ff;"}\
[कुबेरेण स्वयं स्वप्ने यद् दत्तम् अमितात्मना ॥]{style="color:#0000ff;"}

लक्ष्मण of fierce valor also deployed another missile, which given \[to him] by the incomparable Kubera himself in a dream.

When the two missiles collided a great explosion is said to have taken place with a fire breaking out as they neutralized each other -- in a sense implying that Kubera is no less than the god of death in his might.

  - Yama in the ऋग्वेद is strictly associated with the context of the funerary and ancestor rituals. However, there is again the under-current in the other संहिता-s that he was an important deity in regular existence as the god of death. This role of his in the रामायण is rather prominent and both in terms of numbers and the way he is referred to as a great king suggests that he was an important god in the आर्य-dharma of the time. The death-dealing rod of Yama and entering his abode are common similes.

  - प्रजापति: This deity is hardly present in the core clan-specific works of ऋग्वेद -- he is mentioned only twice outside of मण्डल-10. But in मण्डल-10 he has already risen to being the supreme deity in certain सूक्त-s. He is conceived as both the overlord deity as well as the protogonic "golden-egg". Now this would suggest that he was a late-emerging deity, probably specifically in the Indic setting after the आर्य-s had left their ancestral steppe regions. However, we do not think this is the case. Comparisons with protogonic deities in the Greek realm suggest that such a deity predated the Greco-Aryan split. Rather we posit that he was not a key protogonic deity of the normative Indo-European pantheonic system but was the focus of one of several Indo-European cults outside the standard polytheism. Some deities who were part of the standard polytheism were also foci of such extra-normative cults but others like प्रजापति were solely cultic to start with. In both India and Greece the proponents of such protogonic deities started acquiring great prestige and religious centrality. In India this is reflected in the late ऋग्वेद of the मण्डल-10 and the ब्राह्मण-s where we witness the meteoric rise of प्रजापति. In the process of his rise he began to eat into the dominance of Indra, the head deity of the standard IE model.

In the इतिहास-s his ectype Brahman is likewise prominent as the head of the pantheon, though he is already beginning to face competition from the radiations from the cultic foci around Skanda, Rudra and विष्णु. What we see in the रामायण is that he is without any close competitor the second most frequently mentioned deity (Figure 3). His prominence in this इतिहास seems to be similar to what we see in the ब्राह्मण-s: As a deity at the head of the pantheon Brahman shares the position with Indra, but his prominence is clearly eating into that of Indra. This suggests two possible scenarios: 1) He was already a prominent figure from the very beginning of the रामायण tradition and his "power-sharing" with Indra is reflective of the parallel scenario in the ब्राह्मण-s were he had already risen to the highest rank. Thus this would imply that both aindra and प्राजापत्य memes were already active as the epic was being composed. 2) The रामायण as proposed above was primarily an aindra epic and Brahman secondarily encroached on Indra's share in an independent replay of what happened in the ब्राह्मण-s.

On historical grounds we favor the second scenario. A comparison of the नास्तिक productions of the ground-toucher and the naked-one's cults clearly indicate that at their time the प्राजापत्य strand of the religion was primarily among ब्राह्मण "intellectuals". This intellectual link continued to later times when we see mathematical and scientific authors like आर्यभट and Brahmagupta invoke Brahman as their deity (contrast with older scientific tradition in the Caraka-संहिता where Indra is dominant). The rest of the people in large part seem to have still followed the aindra religion until pretty late in Indian history with some competition from the other cultic foci mentioned above. This is indicated by the fact that the two नास्तिक teachers accepted this aindra mainstream as their background and mention the प्राजापत्य tradition primarily in the context of their ब्राह्मण rivals. Notably, in the first of the many रामायण of the jaina-s, the पौमचरियं, विमलसूरि explicit calls out the stupidity of the आस्तिक versions on grounds of their denigration of the great god Indra. This historical background would imply that the प्राजापत्य-s first rose as a dominant force inside the Vedic intellectual circles. The mark of this rise was first seen in the ब्राह्मण texts. Then as the प्राजापत्य-s "conquered" the intellectual landscape they extended their influence to more "secular" intellectual activities such as the इतिहास-s and mathematics/science. This was when Brahman came to prominence in the रामायण tradition. However, by the time the पुराण-s started taking shape in their extant form, the other cultic sectarian foci had radiated enough to catch up and supersede Brahman. Of the old cultic foci, Skanda after an initial rise faded away. In contrast, विष्णु and Rudra came up to Brahman and soon overtook him to the point that despite the three of them being acknowledge as a trinity Brahman sunk to the "junior" position of the trinity. In part the tale of him having no temples might reflect the inability of the intellectual-centered प्राजापत्य system to capitalize on the rising आगम-dharma, despite an early attempt hinted by the Atharvaveda परिशिष्ट-s.

So what do the numbers from the text tell us? First looking at the रामोपाख्यान we find that Brahman/प्रजापति has gone ahead of Indra (Figure 4). It was created by an author(s) who were clearly प्राजापत्य and did not see any need to emphasize or maintain the position of Indra beyond what was absolutely unavoidable. What this tells us is that the रामायण tradition passed through a distinct phase after its original composition where प्रजापति had become important in it and it was in this phase that the fork leading to the रामोपाख्यान was created. More tellingly, this proposal is supported when we look at the by काण्ड counts of key deities (Figure 5: shown as percentage of verses featuring particular deva). Here we see that Brahman has a peculiar distribution that is distinct from that of Indra and वायु. While the latter two show clear काण्ड-specific differences, they are more uniformly distributed across the काण्ड-s. In contrast the occurrences of Brahman show a significantly higher occurrence in the बाल and Uttara काण्ड-s while being greatly under-represented in the rest. We know that both these काण्ड-s were clearly subject to reshaping after the core epic was composed because they try to explain things which were not clear elsewhere in the epic (e.g. the origin of the heroes and villains of the text). This together with the above observation clinches the case for the second of the above proposals: after the original epic in an aindra form was composed the प्राजापत्य-s refashioned it by primarily redacting the first and last काण्ड-s.

![deva_kanda](https://manasataramgini.files.wordpress.com/2017/02/deva_kanda.png){width="75%"}Figure 5

  - विष्णु again: Two other major deities show a similar of काण्ड-wise pattern of distribution as Brahman: विष्णु and Rudra. Importantly, they are minor in their presence in the रामोपाख्यन (Figure 4). This suggests that the वैष्णव and शैव redaction occurred later than the forking of the रामोपाख्यान and acted in manner very similar to the प्राजापत्य action before them. That they were also directly in conflict with each other is suggested by the fight between Rudra and विष्णु which is inserted into the बाल-काण्ड. Another key point is that the वैष्णव material show no strong hints of the अवतार doctrine nor the early पाङ्चरात्रिक tradition which is strong in the महाभारत. This suggests that the वैष्णव redaction comes from an early stream of the sect that underwent further evolution by time of the redaction of the भारत.

  - Rudra: In the रामायण has his characteristic features of being dark-throated, three-eyed, with braided locks (Kapardin), having a bull as his banner/vehicle, holding a great bow, having उमा for this wife and displaying great ferocity. His destruction of the Tripura-s is frequently mentioned. Additionally, his slaying of Andhaka gets multiple references. These references frequently come in काण्ड-s 2-6 suggesting that they are indeed the ancient similes involving the deeds of Rudra. E.g.

[स पपात खरो भूमौ दह्यमानः शराग्निना ।]{style="color:#0000ff;"}\
[रुद्रेणैव विनिर्दग्धः श्वेतारण्ये यथान्धकः ॥]{style="color:#0000ff;"} R 3.29.27

He, Khara, fell to the ground being burnt by the fire of the missile even as Andhaka \[fell] burnt down by Rudra in the White Forest.

Most of these features have direct or indirect reference in the Veda, often going back to the oldest layers. However, we do not hear of his exploits made prominent in the पुराण-s like the killing of Jalandhara or शङ्खचूद. Thus Rudra in the रामायण has not changed in any notable way from his Vedic form.

  - Finally one may note that in this काण्ड-wise distribution Kubera is mostly uniform across काण्ड except for the uttara -- paralleling वायु to an extent. This we believe suggests his ancient and intrinsic importance to the text with the Uttara merely serving as a receptacle for lore relating to him and वायु.

In conclusion, we can say with some confidence that the great रामायण of sage वाल्मीकि was originally an epic encapsulating the popular register of the Indo-European religion as manifest among the Indians -- the आर्य-dharma. Its heroes were set in the mold of the great deities Indra (राम and वालिन्), वायु (Hanumat), विष्णु (लक्ष्मण), Kubera (perhaps some of the Kuberian element transferred to विभीषण), the opaque popular आदित्य (सुग्रीव), with simile-linkages to Rudra and the Maruts (encompassed in Hanumat). Despite the later sectarian redactions starting from the प्राजापत्य-s casting it in different light, it retained this ancient religious spirit of the आर्य-dharma. It was this that erupted forth like the great ape Hanumat to animate the Hindus in their life and death struggle against the unadulterated evil of Mohammedanism when they seemed all but lost. That is why a memorial to the epic should be built at अयोध्या after destruction of all मरून्मत्त elements in the holy city.


