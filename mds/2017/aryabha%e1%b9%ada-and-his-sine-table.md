
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [आर्यभट and his sine table](https://manasataramgini.wordpress.com/2017/03/19/aryabha%e1%b9%ada-and-his-sine-table/){rel="bookmark"} {#आरयभट-and-his-sine-table .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 19, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/03/19/aryabha%e1%b9%ada-and-his-sine-table/ "4:56 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Everyone and his son have written about आर्यभट and his sine table. Yet we too do this because sometimes the situation arises where you have to explain things clearly to a layman who might have some education but is unfamiliar with the intricacies of, or in some cases lacks the correct perspective on, Hindu tradition.

Regarding him Hindu tradition states:\
[सिद्धान्त-पञ्चक-विधाव् अपि दृग् विरुद्धम्]{style="color:#333399;"}\
[औढ्योपराग-मुख-खेचर-चार-कॢप्तौ ।]{style="color:#333399;"}\
[सूर्यः स्वयं कुसुमपुर्य् अभवत् कलौ तु]{style="color:#333399;"}\
[भूगोलवित् कुलप आर्यभटाभिधानः ॥]{style="color:#333399;"}\
When the predictions of the five सिद्धान्त-s and observations of conjunctions, occultations and setting times of planets began to conflict the solar deity himself incarnated in Kusumapuri in the Kali age in the form of the geographer and head professor आर्यभट.

The great astronomer and mathematician आर्यभट-I's, who was seen by some Hindus as the incarnation of the solar deity, was born in 476 CE in the अश्मक country (close to modern Maharashtra-Telangana border). He was active at पाटलिपुत्र in the golden age of Hindu power during the reign of emperor Budhagupta. He is known to have composed at least two works the आर्यभटीय, which was an update of the old स्वायम्भुव-सिद्धान्त tradition, and the आर्यभट-सिद्धान्त which was modeled after the old सूर्य सिद्धान्त. We are informed by भास्कर-I that in his आर्यभटीय आर्यभट was following in the footsteps of the great astronomer of antiquity पाराशर्य, who was likely one of the early promulgators of the स्वायम्भुव tradition. आर्यभट as head professor is said to have had the following notable students: लाटदेव, निशङ्कु, पाण्डुरङ्गस्वमिन् and प्रभाकर. Sadly their works have been lost. Of them लाटदेव is recorded in old Hindu scientific tradition as having written several works including on Hellenistic astronomy and likely succeeded आर्यभट as the आचार्य of his school.

Thus, आर्यभट's work is the earliest surviving record of one of the most important Hindu scientific traditions, namely that of स्वायम्भुव-s. In the manner of the scientists of old Hindu naturalistic tradition आर्यभट presents the acquisition of scientific knowledge as the attainment of ब्रह्मविद्या:\
[दश-गीतिक-सूत्रम् इदम् भू-ग्रह-चरितम् भ-पञ्जरे ज्ञात्वा ।]{style="color:#333399;"}\
[ग्रह-भ-गण-परिभ्रमणं स याति भित्त्वा परम् ब्रह्म ॥]{style="color:#333399;"}\
Having known these सूत्र-s in the ten verses composed in the गीतिक meter providing the motions of the Earth and the planets within the celestial sphere [पञ्जर: marked by the coordinate grid], and having penetrated the orbits of the planets and the stars he attains the supreme brahman.

His clear mention of the movement of the Earth in the celestial sphere along with the other planets in the celestial sphere has been taken as आर्यभट's discovery of heliocentricity. However, here we are not going into this issue and the real nature of his unique planetary model which sets the old Hindu स्वायम्भुव planetary model apart from those of the Greeks. Nevertheless, as a testimony of his astronomical achievements we will merely state the period of sidereal day of the Earth as determined by आर्यभट in modern units:  $23\^h 56\^m 4^{s.}1$ , which is practically the modern value.

One of the important features of आर्यभट's work was his presentation of the old Hindu sine difference table. आर्यभट gives the table using his syllable-numeral equivalence which goes as:\
[मखि भकि फखि धखि णखि ञखि ङखि हस्झ स्ककि किष्ग श्घकि किघ्व ।]{style="color:#333399;"}\
[घ्लकि किग्र हक्य धकि किच स्ग झश ङ्व क्ल प्त फ छ कला अर्ध-ज्याह् ॥]{style="color:#333399;"}\
225, 224, 222, 219, 215, 210, 205, 199, 191, 183, 174, 164, 154, 143, 131, 119, 106, 93, 79, 65, 51, 37, 22, 7 are the  $R\sin(\theta)$  \[differences].

So how do we understand this? As per Hindu terminology if the arc is equivalent to the bow then the chord is equivalent to the bowstring (ज्या). Hence, the sine can be seen as half a ज्या (अर्धज्या) as आर्यभट terms it in the above सूत्र. For simplicity call आर्यभट's अर्धज्या a function named ज्या and represent it thus in modern notation:\
Let the function ज्या  $(\theta)$  be defined as,\
ज्या  $(\theta)=R\sin(\theta)$ , where  $R=3438$ .\
Why the number 3438? आर्यभट conceives a circle whose circumference is divided into  $360 \times 60=21600$  parts. Now the radius of this circle given आर्यभट's  $\pi \approx 3.1416$  will be,

 $$\dfrac{21600}{3.1416}=3437.73 \approx 3438 \Rightarrow 1\;radian$$ 

Thus, आर्यभट's R value is for the first time a radian-like concept was used in trigonometry. Now, आर्यभट divides his quadrant into 24 parts; thus, his minimal angle is  $\theta_1=\frac{\pi}{48}$ . To see why he chose this value of  $\theta_1$  note the following:

 $$\dfrac{\pi}{48} \times 3438 \approx 225$$ 

ज्या  $\left(\dfrac{\pi}{48}\right)=3438\sin\left(\dfrac{\pi}{48}\right)=3438\times0.06540 \approx 225$ 

 $\therefore$  ज्या  $\left(\dfrac{\pi}{48}\right)\Big/R\cdot\dfrac{\pi}{48} \approx 1$ ; actual value 0.99982

Thus, आर्यभट's value of  $\theta_1$  is chosen such that the angle and its ज्या are practically the same (Figure 1). This value as a proxy for  $\displaystyle \lim_{\theta \to 0}\frac{\sin(\theta)}{\theta}=1$  continued to be used in the subsequent development of Hindu calculus. The  $\theta$  and ज्या  $(\theta)$  being nearly the same at this value allows linear interpolation for intermediate values.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/03/jyathetaratio.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Now the rest of his table is in the form of differences. So to get a ज्या  $(\theta_n)$ 

 

