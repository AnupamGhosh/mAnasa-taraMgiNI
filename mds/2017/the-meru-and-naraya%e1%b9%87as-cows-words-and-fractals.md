
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Meru and नारायण's cows: Words and fractals](https://manasataramgini.wordpress.com/2017/08/20/the-meru-and-naraya%e1%b9%87as-cows-words-and-fractals/){rel="bookmark"} {#the-meru-and-नरयणs-cows-words-and-fractals .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 20, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/08/20/the-meru-and-naraya%e1%b9%87as-cows-words-and-fractals/ "7:08 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The fractals described herein are based on and inspired by the work of the mathematicians Rauzy, Mendes-France, Monnerot and Knuth. Some their works, especially the first of them, are dense with formalism. Here we present in simple terms the means of generating and visualizing these remarkable objects which anyone with high-school mathematics and computer skills can reproduce and enjoy.

The "prehistory" of these objects goes back to their discovery among the Hindus. Old Indo-Aryan hieratic and epic poetry primarily utilized meters which were based on the count of the syllables. Thus, the famous गायत्री meter had 8 syllables in each of its three feet amounting to a total of 24. Alongside these meters which conserved syllable count there were other meters which conserved total syllable length or duration in temporal terms. These appear to have originally been primarily used in secular poetry and maxims though later are witnessed more widely in different types of texts. Syllables come in two lengths or morae --- short (laghu) and long (guru). Thus, in these meters their count of morae had to add up to a constant. Let us denote laghu by 0 (=1 mora) and guru by 1 (=2 morae). If we had a meter/metrical unit of just one mora then it had to be just 0; for two morae units we can have: 1 or 00; for three morae units we have: 10, 01, 000; for four morae units we have: 11, 100, 010, 001, 0000. If we write it the traditional Hindu way we get:

 $$\begin{matrix} & & & & 1 & & & & &\\ & & & 1 & & 00 & & & &\\ & & 10& & 01 & 000 & & & &\\ & & 11&100 & 010& 001 & 0000 & & &\\ 110&011&101&1000 & &0100 & 0010 & 0001 & 00000 &\\ \end{matrix}$$ 

This arrangement of the combinations for each mora-length was seen as resembling a mountain and duly termed Meru by the ancient Hindus. They also noted that the total number of permutations for each mora-length forms a sequence (1, 2, 3, 5, 8...) which is the famous Meru-श्रेधि.

This idea can be extended to generate Meru words. One way of doing so is using the following substitution rules:  $0 \rightarrow 01; 1\rightarrow 0$ . To make the word we start with the initial word 1 and recursively apply the above substitution rules. Thus we get:\
1\
0\
01\
010\
01001\
01001010\
0100101001001\
010010100100101001010

We notice that length of the Meru word grows as the Meru-श्रेधि towards infinity. If we observe the pattern within each word we notice that it is not periodic yet there is a structure to it. Importantly,

 $$\displaystyle \lim_{n \to \infty}\dfrac{N(0)}{N(1)}=\phi$$ 

The ratio of 0s to 1s in the Meru word converges to the Golden ratio. For example for the 22nd Meru word we have 10946 0s and 6765 1s which approximates the  $\phi$  correctly to 8 decimal places. But to get a better picture of the structure of the word we perform the following operation:


 1.  Take a Meru word. Start of by drawing a segment in the horizontal direction.

 2.  If you encounter a laghu syllable in the word, i.e. a 0 then draw another segment in the same direction as the current one.

 3.  If instead you encounter a guru syllable in the word i.e. a 1 then check if the syllable is at an even or odd position.

 4.  If 1 is at an even position then turn counter-clockwise by some angle, say  $\tfrac{\pi}{2}$ , and draw the segment.

 5.  If 1 is at an odd position then turn clockwise by the same angle as above and draw the segment.

 6.  Continue thus till the end of the word.

Figure 1 shows the result of this operation on the 22nd Meru word of length 17711 with angle of rotation as  $\pm \tfrac{\pi}{2}$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_simple_rule.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


The result is a striking fractal curve that is tightly folded but never crosses itself. The aligment of the folds give the impression of "lines" forming kites and arrowheads with angles of  $\tfrac{\pi}{4}, \tfrac{3\pi}{4}, \tfrac{\pi}{2}$  passing through the fractal. We can also visualize this as the process of folding a "polymeric" string made up of segment monomers of the length of a Meru number of segments. The monomers either continue in the same direction or turn by a given angle in one direction or the opposite as per the sequence. Instead of turning  $90\^o$  we can turn by other angles too.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_simple_hexagon.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad Turning by  $\pm 60\^o$ 
```{=latex}
\end{center}
```


This results in fractal that is superficially reminiscent of the famous von Koch curve but is clearly different from it. It has a core "hexagonal" symmetry which can be simulated by drawing hexagons at the vertices of a core hexagon and repeating it.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_simple_pentago.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad Turning by  $\pm 72\^o$ 
```{=latex}
\end{center}
```


This operation is similar to the above one but by virtue of its angle generates a "pentagonal" symmetry. These examples establish the innate fractal structure of the Meru words.

We had seen earlier that given a quadratic surd  $q$  we can perform the following operation:

 $$s=\left \lfloor{(n+1)q}\right \rfloor - \left \lfloor{nq}\right \rfloor, \; n \in \mathbb{N}$$ 

It generates a sequence which can be represented as a word in a two letter alphabet (i.e in 0 and 1). For a quadratic surd but not transcendental number we can even figure out a substitution rule which generates that pattern. For the Golden ratio the pattern is the same as what we get by the above substitution operation to generate the Meru word. The words for  $\sqrt{2}$  or  $\sqrt{3}$  when subject to the above drawing mechanism, unlike that for the Golden ratio do not generate a fractal pattern. However, we discovered words generated in a 3-letter alphabet that encode  $\sqrt{3}$  or  $\sin(\tfrac{\pi}{3})$  which can result in fractal curves (see below). In any case, sticking to the 2-letter alphabet for now, a substitution rule which generates words where the ratio of 0s to 1s converges to  $1+\sqrt{2}$ , also called the Silver ratio, generates a fractal curve with an underlying bifurcating pattern symmetry (Figure 4).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_root2plus1_word.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad Rule:  $0 \rightarrow 001;\; 1\rightarrow 0$ , Silver ratio.
```{=latex}
\end{center}
```


Notably, a rule which generates 0s and 1s in 1:1 ratio also generates a fractal, which is equivalent to that generated by iterative removal of a rectangle from a half-square isoceles triangle (Figure 5).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_1isto1_rule.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad Rule:  $0 \rightarrow 011;\; 1 \rightarrow 010$ 
```{=latex}
\end{center}
```


We can generate Meru words by yet another method: Start with  $w_1=0,\; w_2=1$ . Then concatenate the current word with the previous one to get the next word:  $w_{n+1}=w_{n}w_{n-1}$ . This generates the words (  $w_1:w_5$  shown):\
0\
1\
01\
101\
01101

One will notice a relationship of these words with a particular mora-count of the Hindu metrical system (0 and 1 inverted). Meru words generated by this mechanism will have the ratio of 1s to 0s converge to the Golden ratio. Applying the above drawing procedure results in a non-crossing fractal related to, but distinct from, the Meru curve obtained by the above procedure (Figure 6).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_concat.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad Concatenated Meru curve, rule:  $w_1=0,\; w_2=1$ 
```{=latex}
\end{center}
```


If we start with  $w_1=0,\; w_2=001$  and apply the same procedure then we get words which have ratio of 0s to 1s converging to the square of the Golden ratio  $\phi^2$ .

Now instead of a 2-letter alphabet we can next try a 3-letter alphabet (0,1,2). Here, instead of the even-odd evaluation for turning we can instead use 0 as an injunction to continue in the same direction, 1 to turn counter-clockwise by a given angle and 2 to turn clockwise by the same angle. We discovered that a series of rules which generate words where the ratio of 0s to 1s and 2s converges to  $\sqrt{3}$  generate a wide range of distinct fractal curves (angle  $\pm \tfrac{\pi}{2}$ ):
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_3letter-1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad Rule  $0 \rightarrow 210; \; 1 \rightarrow 020; \; 2 \rightarrow 10$ 
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_3letter-2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 8\quad Rule  $0 \rightarrow 120; 1\rightarrow 020; \; 2\rightarrow 10$ 
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_3letter-3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 9\quad  $0 \rightarrow 120; 1\rightarrow 020; \; 2\rightarrow 01$ 
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_3letter-4.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 10\quad  $0 \rightarrow 012; 1\rightarrow 200; \; 2\rightarrow 10$ 
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_3letter-5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 11\quad  $0 \rightarrow 210; 1\rightarrow 020; \; 2\rightarrow 10$ 
```{=latex}
\end{center}
```


There are more of these curves which potentially deserve a more systematic study. We have merely provided some of the visually most interesting examples. The first four of these curves are non-crossing curves.

Such curves might also be generated using an analogy drawn from biochemistry. The sequences nucleic acids are in a 4-letter alphabet. These (specifically that of RNA) is translated by the ribosome into proteins which are in a 20-letter alphabet. To encode a 20-letter alphabet with just a 4 letter alphabet you have to assign a mapping to strings of length greater than 2 in the 4-letter nucleic acid alphabet to the letters in the 20-letter alphabet of proteins. This mapping is the genetic code. We can thus map the Meru word in a 2-letter alphabet on to a 3-letter alphabet of 0,1,2 by making 2-letter strings in the Meru word encode letters in the 3-letter space. Thus, we get translated words. As in the above 3-letter alphabet case while folding the translated word we simply interpret the 0 as a directive to draw a segment in the same direction, 1 as a counter-clockwise turn by a given angle and 2 as a clockwise turn by the same angle. By applying this folding rule we can now generate curves with different translation rules:
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_dense-1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 12\quad Rule  $00 \rightarrow 0; \; 01 \rightarrow 1; \; 10 \rightarrow 2$ . This simple translation produces an armless svastika-like curve.
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_dense-2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 13\quad Rule  $00 \rightarrow 12; \; 01 \rightarrow 1; \; 10 \rightarrow 2$  This translation produces a crossing curve with some resemblance to the loops in the sand/flour अलंकार patterns.
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_dense-3.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 14\quad Rule  $00 \rightarrow 21; \; 01 \rightarrow 02; \; 10 \rightarrow 10$  This a svastika-like non-crossing pattern.
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/meru_dense-5.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 15\quad Rule  $00 \rightarrow 210; \; 01 \rightarrow 010; \; 10 \rightarrow 20$  This a stepped variant of the basic Meru curve.
```{=latex}
\end{center}
```


Now the question arises as to whether we can generate words corresponding to नारायण's classic dhenu sequence just as we did with the Meru sequence. It turns out that we can generate a set of simple substitution rules in a 3-letter alphabet (we use 1,2,3 here simply for some plotting conveniences) along the lines of the Meru sequence:  $1 \rightarrow 12; \; 2 \rightarrow 3; \; 3 \rightarrow 1$ . Application of these rules on  $w_0=1$  gives us the following set of dhenu words (up to  $w_6$ ):\
1\
12\
123\
1231\
123112\
123112123

We note that the length of these words is the classic dhenu sequence, 1, 2, 3, 4, 6, 9... Further, in these words the ratio of 1s to 2s and 2s to 3s converges to नारायण's convergent  $N_c=1.46557123$ . The ratio of 1s to 3s converges to  $N_c^2$ .

In the previous article we noted (see Figure 2) that the Hofstadter sequence, which is related to the dhenu sequence, has a fixed bandwidth when rectified. Moreover, the individual oscillations have a range of fixed amplitudes but the pattern of oscillations is not periodic despite showing some pattern. Indeed, the pattern of 1, 2, and 3 in the dhenu words is again not periodic but shows a peculiar pattern which can be captured by making circles of diameter 1, 2 and 3 and plotting them based on the dhenu words (shown below for  $w_{12}; \; l=88$ ):
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/narayana_circles.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 16\quad
```{=latex}
\end{center}
```


Lurking within this pattern is a deep fractal structure that can be unpacked by using Rauzy's analysis of such words. The first step for this involves construction of a matrix which captures the dhenu word generator. The matrix is a  $3\times 3$  matrix because we have a 3-letter alphabet with 3 substitution rules --- the rows are for the substitutions and the columns for the alphabets.

  - The first rule  $1 \rightarrow 12$  puts one element in the first and one in the second alphabet column.

  - the second rule  $2 \rightarrow 3$  puts one element in the third alphabet column.

  - the third rule  $3 \rightarrow 1$  puts one element in the first alphabet column. Thus we can write out the matrix:

 $$M= \begin{bmatrix} 1 & 1 & 0\\ 0 & 0 & 1\\ 1 & 0 & 0\\ \end{bmatrix}$$ 

The eigenvalues of our matrix are: 1.465571, -0.23278+0.79255i, -0.23278-0.79255i, the solutions of  $x^3-x^2-1=0$  which we encountered in the previous article in the context of the dhenu sequence. The one real value is  $N_c$ . The other two values indicate that  $N_c$  is a Pisot-Vijayaraghavan number.

In the next step we find the eigenvectors of the matrix. Recall that if  $A$  is a  $n \times n$  matrix then  $(A-\lambda)\vec{v}=0$ , where  $\lambda$  is an eigenvalue of the matrix and  $\vec{v}$  is the corresponding eigenvector. For our above matrix we get the three eigenvectors as:

 $$\vec{v_1}=\langle -0.77098, -0.35894, -0.52606\rangle$$ 

 $$\vec{v_2} =\langle -0.39162-0.25177i, 0.68232, -0.15883+0.54078i \rangle$$ 

 $$\vec{v_3}=\langle -0.39162+0.25177i, 0.68232, -0.15883-0.54078i \rangle$$ 

The eigenvector  $\vec{v_1}$  has all real-valued components; hence, we drop it. The next two eigenvectors have one real-valued component which is equal to the rectification value of the Hofstader H sequence which we encountered in the previous article. The remaining two values are complex and also conjugates of each other between the two eigenvectors. Hence, we may use either vector for the next step.

The next step involves first taking a long dhenu word  $w_j$  (e.g.  $w_{28}; \; l=39865$ ). We define a sub-word of a  $w_{j,k}$ , which means the first  $k$  letters of word  $w_j$ . We start with the first sub-word which includes all 3 letters of the alphabet. That would be  $w_{j,3}=123$ . From there we increment  $k$  by 1 until we reach the end of the word  $w_j$ . Now for each such sub-word  $w_{j,k}$  we calculate the frequency of 1s, 2s and 3s; respectively denoted as  $f_1,f_2,f_3$ . Then we multiple these frequencies by the corresponding component of the eigenvector (in our case  $\vec{v_2}$  or  $\vec{v_3}$  and sum up the three values:

 $$\displaystyle S_k=\sum_{n=1}^3 f_n\cdot v_{2,n} = f_1\cdot v_{2,1}+f_2\cdot v_{2,2}+f_3\cdot v_{2,3}$$ 

The same may be done with  $\vec{v_3}$  too. The result of each sum will be a complex number and as we traverse the word  $w_j$  we will get a set of  $l(w_j)-3$  complex numbers in our case. If the  $k^{th}$  letter is a 1 then we assign then we assign color red for the set of complex numbers derived at that letter; if it is a 2 we assign color blue; if it is a 3 we assign color green. The sizes of these three color sets will be in the proportion of  $N_c$  and  $N_c^2$  to each other. Remarkably, when we plot each of these three sets we find that they neatly segregate into three similar fractals. Even more remarkably, the three distinctly colored copies define a fractal tiling of the complex plane (Figure 17).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/08/narayana_word_rauzy.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 17\quad
```{=latex}
\end{center}
```


Seeing this fractal tiling manifest from the dhenu words we were reminded of the ideas of our ancestors on words manifesting form.

