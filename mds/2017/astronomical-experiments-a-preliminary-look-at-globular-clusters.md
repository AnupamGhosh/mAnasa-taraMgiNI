
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Astronomical experiments: a preliminary look at globular clusters](https://manasataramgini.wordpress.com/2017/12/21/astronomical-experiments-a-preliminary-look-at-globular-clusters/){rel="bookmark"} {#astronomical-experiments-a-preliminary-look-at-globular-clusters .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 21, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/12/21/astronomical-experiments-a-preliminary-look-at-globular-clusters/ "7:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

While Alexander von Humboldt was exploring South America he observed a fuzzy object in the constellation of Centaurus and thought that it was a new comet. However, it turned out that it was the globular cluster  $\omega$  Centauri. It had already been noticed long before his times by Edmond Halley of comet fame with his primitive telescope as a fuzzy object which was not a star. Long before Halley, Ptolemaios the yavana astronomer observing from Egypt thought it was a star in Centaurus. Halley soon discovered a second such object in the constellation of Hercules, which was to be later known as M13. Charles Messier following up on these observations discovered that there were several more such objects and obtained the first hints that they were made up of stars. This was confirmed finally by Wilhelm Herschel, who was a prolific discoverer of globular clusters (GCs).

![Globular_figure1](https://manasataramgini.files.wordpress.com/2017/12/globular_figure1.jpg){width="75%"}Figure 1  $\omega$  Centauri

We obtained one of the clearest views of  $\omega$  Centauri from a fort of the erstwhile मराठ admiral Angre on an exceptionally clear night --- it was truly one of those sights which cause a रोमहर्षण in the beholder. We have had several fruitful nights of GC sighting using various instruments from a 20×75 binoculars to a 12 inch telescope. Indeed, GCs remain objects of great fascination to everyone from the casual observer to the astronomers at the edge of their science. They have ramifications for everything from the age of the universe, the origin of galaxies, the detection of dark matter to gas physics.

A GC is unmistakable for anyone who has seen one -- a dense globular conglomeration of stars with up to a several million stars packed in a close oblate spheroid of few parsecs (1 parsec  $\approx$  3.26 light years). We had the desire of illustrating some basic features of them using the data from 147 GCs of the Milky Way. This number is not far from the total number of GCs in our Galaxy. The brightest in our set is of course  $\omega$  Centauri with integrated visual magnitude,  $m_V=3.68$  and the faintest is UKS 1 with  $m_V=17.29$ , which is beyond any telescope we have personally operated.  $\omega$  Centauri is also the brightest in absolute visual magnitude  $M_V=-10.24$ , whereas the absolutely faintest GC in our set is AM 4 with  $M_V=-1.55$ .

![Globular_Figure2](https://manasataramgini.files.wordpress.com/2017/12/globular_figure2.png){width="75%"}Figure 2

One of the first things an observer notices is that GCs are not uniformly distributed in the sky. Even the brightest GCs, those from the Messier catalog, make this apparent --- there are 7 each in Ophiuchus and Sagittarius; thus, they seem to be clustered close to the center of the Milky Way which lies in Sagittarius. If we take all the GCs in our data and convert their coordinates to galactic coordinates using some spherical trigonometry we can plot them with respect to the galactic equator. Figure 2 shows such a plot using the Eisenlohr projection with the Milky Way outline in green. The GCs are colored red for higher than average metallicity an blue for lower than average metallicity (see below for discussion on metallicity). One can clearly see that they are clustered in just one part of the sky with the galactic center as the focal point. This observation establishes their presence around the core of the Galaxy in the halo.

![Globular_Figure3](https://manasataramgini.files.wordpress.com/2017/12/globular_figure3.png){width="75%"}Figure 3

We can also visualize their distribution by directly plotting their inferred distance from the galactic center (Figure 3). Their density rapidly falls off with distance and  $\tfrac{2}{3}$  of the total number of GCs in the Milky Way lie within 10 kiloparsecs(Kpc) of the galactic center.

![Globular_Figure4](https://manasataramgini.files.wordpress.com/2017/12/globular_figure4.png){width="75%"}Figure 4

We next examine the distribution of the absolute magnitudes  $M_V$  of the GCs (Figure 4). We find a clear central tendency;  $\mu=-7.022$  (vertical line),  $\sigma=1.54631$ . The observed distribution has a sharp peak close to the mean. Thus, the GCs seem to come with a clearly preferred  $M_V$ . This has been exploited to use the magnitude of GCs in other galaxies as a standard candle to measure their distances.

![Globular_Figure5](https://manasataramgini.files.wordpress.com/2017/12/globular_figure5.png){width="75%"}Figure 5

The radii of GCs is measured in two ways. First, it has been observed that luminosity of a globular drops off when one moves from the center of the GC toward the periphery. The radius at which luminosity drops by half of what it is in the center is defined as the core radius  $R_c$ . Another measure of radius is based on gravity: The distance from the center of the GC beyond which the gravity of the galaxy has a greater influence than that of the GC itself is termed the tidal radius  $R_t$ . Based on these radii the central concentration index is defined as  $c=log_10(\tfrac{R_t}{R_c})$ . The distribution of  $c$  is shown in Figure 5. Its most interesting feature is the situation above  $c=2.0$  --- we see a significant over-representation at the very end of the range  $c\approx2.5$ , which is greater than even that at the mean. What does this signify? As one moves towards the center of the GC in a typical GC they luminosity rises and then plateaus. However, about 20 percent of the GCs which show  $c=2.5$  behave differently. In these high  $c$  GCs the brightness just keeps rising right to the center. Hence, they are described as having undergone core collapse wherein stars clump more closely at the center than what is seen in typical GCs. This is believed to happen due to the dynamics of the cluster wherein sorting of stars due to close encounters reduces the central kinetic energy and favors the closer clumping of stars as gravity dominates.

![Globular_Figure6](https://manasataramgini.files.wordpress.com/2017/12/globular_figure6.png){width="75%"}Figure 6

We next examine a key property of GCs, their metallicity (Figure 6). The early universe was almost solely made up of Hydrogen and Helium, which still remain the dominant elements. But since then stellar nucleosynthesis and supernovae have generated other heavier elements. The fraction of elements other than H and He is absolute metallicity of an astronomical object. In practice it is measured using spectroscopy as the ratio of Iron to Hydrogen. Typically, the metallicity of an astronomical object as relative to the sun:

 $$\left\lbrack \dfrac{Fe}{H}\right\rbrack =\log\left\lbrack \dfrac{N_{Fe}}{N_{H}}\right\rbrack -\log\left\lbrack \dfrac{N_{Fe}}{N_{H}}\right\rbrack _{Sun}$$ 

Where N is the number of atoms of Iron or Hydrogen inferred from the spectrum. Thus, stars with  $\lbrack Fe/H\rbrack > 0$  are more metallic than the sun and those with  $\lbrack Fe/H\rbrack < 0$  are less metallic than the sun. The stars with greater metallicity tend to be redder and those with lower metallicity are bluer, all other things being equal. All GCs in the Milky Way, barring Liller 1 (  $\lbrack Fe/H\rbrack =0.22$ ), have lower metallicity than the sun. The distribution of GC metallicities however interestingly shows a bimodal pattern (Figure 6). One peak, the dominant one, is seen at  $\lbrack Fe/H\rbrack =-1.5$  and another at  $\lbrack Fe/H\rbrack =-0.7$ . Notably, in the past 2 decades studies on several other galaxies have recapitulated this bimodal distribution we see in the Milky Way. The dominant peak in this distribution points to a prevalence of low metallicity GCs, which in turns implies that they are predominated by population-II stars or those that formed early in the Galaxy's history. The second peak could represent the formation of later clusters during galactic merger/cannabalism events. Indeed, the Hubble space telescope observations suggest that large-scale gas clumping in galactic collisions might allow formation of new GCs. A differential history of the GCs with different metallicities is supported by their spatial distribution (Figure 1). One observes that clusters with lower than average metallicity are more widely distributed and dominant at higher galactic latitudes. This suggests that they are indeed likely to be "fossils" from the early era of star formation in the galactic halo and contain some of the earliest surviving stars in our galaxy. Consistent with this, more recent studies show that galaxies with bigger central bulges have more GCs.

![Globular_Figure7](https://manasataramgini.files.wordpress.com/2017/12/globular_figure7.png){width="75%"}Figure 7

Now we can look at some correlations between the between the above discussed variables. First, we look at the correlation between metallicity and distance from the galactic center (Figure 7) as a follow up to the above discussion on Figure 1. Each GC in Figure 7 is color-coded according to its metallicity as in Figure 6 and the distribution of the metallicity on the y-axis is indicated by the rug. We observe a trend of decreasing metallicity at greater distances from the center of the galaxy. Importantly, there are hardly any GCs with  $\lbrack Fe/H\rbrack > -1$  at distances greater than 8 kpc from the galatic center.

![Globular_Figure8](https://manasataramgini.files.wordpress.com/2017/12/globular_figure8.png){width="75%"}Figure 8

If we discount the GCs which have undergone core collapse, we also observe a positive correlation between central concentration  $c$  of the GC and its absolute magnitude  $M_V$  (Figure 8). This suggests that as long as a GC does not undergo core collapse its brightness increases with the central concentration. Notably, this plot also shows that there are few GCs, which have undergone core collapse which are brighter than  $M_V=-7.5$ . Astronomers propose that this might mean that the time required for the most luminous GCs to undergo core collapse is greater than the current age of the universe. Notably, we also see that in a plot of  $c$  versus distance from the galactic center (Figure 9), nearly all GCs, which have undergone core collapse, occur closer to the center of the Galaxy (  $< 8 kpc$ ).

![Globular_Figure9](https://manasataramgini.files.wordpress.com/2017/12/globular_figure9.png){width="75%"}Figure 9

This preliminary glance of at GCs is just to obtain a flavor of the mysteries they hold regarding the origin and evolution of the universe. Recent studies (e.g. that of WE Harris published last year) have shown a particular relationship between the luminosity of the host galaxy and number of GCs they have. While the Milky Way has close to the 147 GCs we have explored here, giant galaxies like the great Virgo galaxy of M87 have over 10,000 GCs. Over the years astronomers have also obtained evidenced for another interesting relationship. Let  $M_h=M_b+M_d$  (where  $M_h$  is the total mass of a galaxy, which is the sum of the total mass in baryonic matter  $M_b$  and the total dark matter  $M_d$ ), then  $\eta=\frac{N_{GC}}{M_h}$  (where  $N_{GC}$  is the total number of GCs associated with the galaxy). This  $\eta$  has been found to be nearly constant for galaxies ranging from dwarfs to giants. Why is this the case remains a mystery.

Recent studies with the Hubble space telescope have show other types of associations of GCs. In the great galactic assemblage Abell 1689, there is swarm of approximately 160,000 GCs a sphere of diameter of 2.4 million light-years close to center of this galaxy cluster. These are apparently not GCs associated with one galaxy but seem to be a common pool of GCs from the entire galactic cluster that have aggregated close to its center, which is rich in the mysterious dark matter. It such indirect clues regarding dark matter that give us faint hints regarding one the greatest mysteries of the universe. What exactly is the link between GCs formation and dark matter and how did they form in the first place. These are questions which show that as ever astronomy leads the way to where the frontiers of physics lie.

