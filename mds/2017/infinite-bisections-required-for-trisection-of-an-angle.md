
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Infinite bisections required for trisection of an angle](https://manasataramgini.wordpress.com/2017/05/29/infinite-bisections-required-for-trisection-of-an-angle/){rel="bookmark"} {#infinite-bisections-required-for-trisection-of-an-angle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 29, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/05/29/infinite-bisections-required-for-trisection-of-an-angle/ "6:56 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

![sum_series_1by2_equal_1by3](https://manasataramgini.files.wordpress.com/2017/05/sum_series_1by2_equal_1by3.png){width="75%"}

Figure 1: Self-evident demonstration of  $\frac{1}{3}=\frac{1}{2}-\frac{1}{4}+\frac{1}{8}-\frac{1}{16}...$ 

![serial_trisection](https://manasataramgini.files.wordpress.com/2017/05/serial_trisection.png){width="75%"}

Figure 2: Application of the same as serial bisections to trisect the angle. In the example chosen here we have  $\theta=102\^o; \; \frac{\theta}{3}=34\^o$ . In ten steps we get to  $33.97\^o$  which is a pretty close, though in principle it shows that with a compass and straight-edge we would need infinite steps.

