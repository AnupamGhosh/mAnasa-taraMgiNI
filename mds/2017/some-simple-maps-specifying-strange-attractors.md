
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some simple maps specifying strange attractors](https://manasataramgini.wordpress.com/2017/09/26/some-simple-maps-specifying-strange-attractors/){rel="bookmark"} {#some-simple-maps-specifying-strange-attractors .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 26, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/09/26/some-simple-maps-specifying-strange-attractors/ "6:40 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note may be read a continuation of: [Some reminiscences of our study of chaotic maps-2](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/)

While the story of the chaotic 2D attractors began with the simple-looking maps of Henon and Lozi, by the early 1990s the high-point was reached with multi-parameter maps of Sprott and some further additions of Pickover. In the process a bunch of simple maps were mostly ignored. Still we believe they are of some interest because they are excellent as simple devices to introduce a student to the concept of strange chaotic attractors. Hence, as part of recording our studies on such we present some of these. The first of these is a simple map which we discovered inspired by the basic Lozi map and has two parameters like it.

 $$x_{n+1}=1-a|x_n|+b(y_n-x_n)\\ y_{n+1}=1-a|y_n|+b(x_n-y_n)$$ 

The evolution of this attractor for  $a=1.1$  and  $b= .63 : .91$  is shown below in Figure 1. Keeping with the symmetric form of the definition the map is symmetric along the  $x=y$  line.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/double_abs_attractor.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


From such a map we can develop a 2-parameter quadratic map where the square operator takes the place of the absolute value operator.

 $$x_{n+1}=1-ax_n^2+b(y_n-x_n)\\ y_{n+1}=1-ay_n^2+b(x_n-y_n)$$ 

An example of the attractor specified by this map (in black) for  $a=0.78; b=0.406$  is shown along with the basin of attraction in Figure 2. The colors represent the number of iterations needed to escape to infinity while the basin where the iterates remained trapped in the attractor is is white. We see that the attractor sits rather snugly within the basin of attraction which itself has a rather simple form.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/double_quadratic_basin.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


We next study its evolution as  $a=.78$  remains unchanged while  $b$  varies from  $0.25:0.406$  (Figure 3). In this parameter range the we find the attractor evolving from a two-lobed bounded curve, to a 10 point convergence, to a multi-point convergence presaging the attractors, to finally a series of strange attractors.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/09/double_quadratic.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


