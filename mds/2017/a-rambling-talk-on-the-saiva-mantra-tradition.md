
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A rambling talk on the शैव mantra tradition](https://manasataramgini.wordpress.com/2017/09/24/a-rambling-talk-on-the-saiva-mantra-tradition/){rel="bookmark"} {#a-rambling-talk-on-the-शव-mantra-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 24, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/09/24/a-rambling-talk-on-the-saiva-mantra-tradition/ "7:30 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

A rambling talk we had given on the शैव tradition. Talking is easier but less precise than writing. So please be aware of the insufficiency that goes with the domain of any talk while perusing this material.

**Part 1**; Introduction to mantra-s and पाशुपत-s as a MP3 file: [https://app.box.com/s/3wx313rfm8wijnpiv42f1q8r8z9zg2vu](https://app.box.com/s/3wx313rfm8wijnpiv42f1q8r8z9zg2vu){rel="nofollow"}

**Part 1, 1st addendum**; early legends and iconography of Rudra: [https://app.box.com/s/iqt7g7vye7c9z9tvb314swljh3um9eem](https://app.box.com/s/iqt7g7vye7c9z9tvb314swljh3um9eem){rel="nofollow"}

**Part 1, 2nd addendum**; Deployment of Rudra mantra-s in vaidika rituals; give a brief account of राजगवी-homa the animal sacrifice to rudra in the सोमयाग, the सोमारुद्रा ritual, the अभिचार ritual invoking rudra against enemy's cattle, apotropaic rituals for protection again rudra's attack (रुद्रः पशून्/प्रजाः शमायेत), अथर्ववेदीय मृगारेष्टि, and पैप्पलाद तुंबुरु-याग: [https://app.box.com/s/zg2v7zr3b36ckps4xo1i6fzavmzow8qb](https://app.box.com/s/zg2v7zr3b36ckps4xo1i6fzavmzow8qb){rel="nofollow"}

**Part 2**; The emergence of a new mantra-शास्त्र: the beginnings of the तान्त्रिक lore. Touches upon 3 key features that contributed to the emergence of the तान्त्रिक mantra-शात्र -- 1) the sectarian traditions; 2) the origins of iconic and मण्डल worship; 3) The emergence of a new mantra languages, i.e. mantra-s without semantic value, prose mantra-s with onomatopoeic elements, and mantra-s emphasizing repetitive action:\
[https://app.box.com/s/5l2tcpmrr07ohzh2worg2es4ml3xhmpu](https://app.box.com/s/5l2tcpmrr07ohzh2worg2es4ml3xhmpu){rel="nofollow"}

**Part 3**; The शैव तान्त्रिक tradition; the 3 categories of शैव तान्त्रिक साधकस्: 1) the siddha-s and विद्याधर-s; 2) the साधक-s and आचार्य-s; 3) the नीर्बीजदीक्षित-s and lay followers; the classification of knowledge in the शैव tradition; the division of the शैव शास्त्र into the five स्रोतांसि: गरुड, भूत, वाम, bhairava and सिद्धान्त; the गारुड tradition: गरुड, शरभ and नीलकण्ठ; the भूत tradition: खड्गरावण and his परिवार, चण्डासिधर, हयग्रीव, अर्धनारीश्वर and his ध्यान, क्रोधेश्वर and ज्वरेश्वर; the वाम tradition: Tumburu and the catur-भगिनी; the ध्यान of Tumburu in the चन्द्रदुर्दण्द-मण्डल; the transmission of वाम tantra-s and their history; special ध्यान-s of the four sisters; वाम mantra-s and yoga:\
[https://app.box.com/s/vd6xak6jqyuxoacv9nbesigm690de876](https://app.box.com/s/vd6xak6jqyuxoacv9nbesigm690de876){rel="nofollow"}

**A very basic introduction to aspects of शैव iconography with an emphasis on the icons depicted in सैद्धान्तिक स्थापन tradition.**\
[https://app.box.com/s/byb5yqght94gtqq9fnk867xbg6ca881q](https://app.box.com/s/byb5yqght94gtqq9fnk867xbg6ca881q){rel="nofollow"}

continued...


