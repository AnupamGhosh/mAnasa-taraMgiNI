
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Pattern formation in coupled map lattices with the circle map, tanh map, and Chebyshev map](https://manasataramgini.wordpress.com/2017/11/26/pattern-formation-in-coupled-map-lattices-with-the-circle-map-tanh-map-and-chebyshev-map/){rel="bookmark"} {#pattern-formation-in-coupled-map-lattices-with-the-circle-map-tanh-map-and-chebyshev-map .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 26, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/11/26/pattern-formation-in-coupled-map-lattices-with-the-circle-map-tanh-map-and-chebyshev-map/ "3:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The coupled map lattices (CMLs), first defined by Kunihiko Kaneko around the same time Wolfram was beginning to explore cellular automata, combine features of cellular automata with chaotic maps. The simplest CMLs are defined on a one dimensional lattice with  $n$  cells. The value of the  $j^{th}$  cell in the lattice generated by a combination of the action of a chaotic mapping function  $f(x)$  and coupling of that value with the values of adjacent cells. Imagine a CML where the  $j^{th}$  cell is coupled with the two cells on either side  $j-1$  and  $j+1$  with a coupling fraction of  $\epsilon \in \lbrack 0,1\rbrack$ . Then, the value of this cell in the next generation  $(n+1)$  (indicated as a superscript) of the CML is given by:

 $$x_j^{n+1} = (1-\epsilon)f\left(x_j\^n\right)+\dfrac{\epsilon}{2}\left(f\left(x_{j-1}\^n\right)+f\left(x_{j+1}\^n\right)\right)$$ 

Thus, the CML adds a further level of complexity coming from the chaotic behavior of the map defined by  $f(x)$  to [the basic one dimensional cellular automaton principle](https://manasataramgini.wordpress.com/2016/11/06/some-lessons-we-learned-from-3-color-totalistic-cellular-automata/).

One map of interest that can be played on a CML is the circle map discovered by the famous Russian mathematician Vladimir Arnold, which he proposed a simple model for oscillations such as the beating of the heart. It essentially performs the operation of mapping a circle onto itself:

 $$x_{n+1}=x_n+\Omega-\dfrac{K}{2\pi}\sin(2\pi \cdot x_n)$$ 

Figure 1 shows the the iterates of  $x_0=\tfrac{1}{3}$  for  $K=.9$  and  $\Omega \in \lbrack 0,1\rbrack$ . We observe that there are several regions where the circle map generates chaotic behavior and other bands where it is mostly non-chaotic. Also visible more subtly are regions of less-preferred values.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/11/circle_map.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


We then play the circle map on a CML of 101 cells by keeping  $K=0.9$  and varying  $\Omega$  to take multiple values. We take the coupling fraction  $\epsilon=0.5$ , which we found experimentally to give interesting results. We initialize the CML by setting the value of cell 51 to 0.5, and setting the 50 flanking cells on either side of it to the value of 0 in generation  $n=1$ . We then let it evolve such that if the cell on either edge of the lattice are neighbors of each other --- thus the CML here is in reality plays out on a cylinder. Each value of  $\Omega$  results in a different kind of behavior of the circle map (The left panels in Figure 2). The corresponding evolution of the CML is shown in Figure 2, for 500 generations going from left to right.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/11/kaneko_circle_1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```



 1.  In the first case one can see that the map converges to a single value after a brief initial fluctuation. Correspondingly, when played on the CML it results in the seed anisotropy quickly dying off and the CML settles into a constant state.

 2.  In the second case the circle map shows an oscillation with a gradual concave rise and a sharp fall. While the oscillations are roughly similar in shape they are not identical. This results in the CML rapidly evolving into a complex pattern. The triangular elements seen in the pattern are reminiscent of those which emerge in cellular automata.

 3.  In this case the circle map generates sharp approximately regular pattern of oscillations, with rapid, abrupt changes in values. The corresponding CML evolves into a basic pattern of waves. Central seed sets up a pattern that develops into a fairly fixed width pattern the keeps propagating independent of the background waves.

 4.  Here, the circle map generates oscillations similar to above but slightly less-abrupt and has a more convex descending branch. This results in a more complex pattern developing from the central cell that stands out more clearly from the background waves. It gradually grows in width and shows a central band and flanking elements.

 5.  In this case the circle map generates oscillations with an abrupt rise and gradual, convex fall. This again, like case 2, rapidly generates a complex pattern.

To investigate the effect of other types of chaotic oscillations applied to the CML, we next considered the tanh map which is based in the hyperbolic tangent function. It is defined thus:

If  $x_n< 0$ ,  $x_{n+1}=\dfrac{2}{\tanh(r)}\tanh(r(x_n+1))-1$ ,

else,  $x_{n+1}=\dfrac{2}{\tanh(r)}\tanh(-r(x_n-1))-1$ 

This maps  $x_n \in \lbrack -1,1\rbrack \rightarrow \lbrack -1,1\rbrack$ . Figure 3 shows a plot of iterations of  $x_n$  for the parameter  $r \in (0,10\rbrack$ . For  $r< 1$  the distribution of  $x_n$  is all over the place. For  $r> 1$  the distribution of  $x_n$  becomes more and more U-shaped with a preference for values closer to 1 or -1.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/11/tanh_chaos.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


We then play the above tanh map on the CML with 101 cells initialized with a central cell (51)  $x_{51}^1=.5$  for 500 generations. The coupling fraction is chosen as  $\epsilon=0.1$ . The experiment is run for different values of the parameter  $r$ . These results are shown in figure 4.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/11/kaneko_tanh2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


The first two values of  $r$  are in that part of the parameter space of the tanh map that produces highly chaotic oscillations. This results in the CML quickly evolving into nearly random continuous variation.

In the next three cases the effects of the U-shaped distribution of the iterates kicks in and we have predominantly abrupt up-down oscillations of the tanh map. However, the subtle difference in the oscillations causes clearly distinct results, albeit with some common features. In the third example, surprisingly, the CML quickly converges and freezes into several tracks of distinct periodic patterns. In the fourth and fifth case, we see tracks with patterns similar to those seen in the above case. However, they do not freeze, at least in the 500 generations we ran them. Rather, the tracks persist for different number of generations and then become extinct or evolve into other patterns after persisting for even 100 or more generations. These more regular patterns play out in a more irregular rapidly changing background.

In the last experiment presented here we consider the effect of the coupling fraction  $\epsilon$  on long-term dynamics. For this purpose we use the Chebyshev map, which is related to the eponymous polynomials of that famous Russian mathematician.

 $$x_{n+1}=\cos(a \cdot \arccos(x_n))$$ 

This surprisingly simple map produces extreme chaos with a distribution similar to the tanh map for values of the parameter  $a> 1.5$ . Values of  $a=1.5:2$  produce interesting behavior in CMLs. In our experiment the we keep the Chebyshev map itself the same for all runs with  $a=1.8$ . Figure 5 shows the chaotic pulsations produced by this Chebyshev map.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/11/chebyshev_map.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


In this case the CML was run for 5000 generations and every 10th generation was plotted. It was initialized with the central cell  $x_{51}^1=0.1$  and each of the flanking 50 cells on either side were set to  $x_j^1=-0.75$ . Here, the  $\epsilon$  value is varied to establish the effects of coupling on the behavior of the CML. This is shown in figure 6
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/11/kaneko_chebyshev.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad
```{=latex}
\end{center}
```


The behavior is rather interesting:

 1.  At  $\epsilon=0.05$  we observe that randomness permeates the entire evolution of the CML.

 2.  At  $\epsilon=0.075$  there is a fall in randomness with repeated emergence of lines of persistence, lasting for several generations before going extinct. Some times they reappear several 100s of generations later. Within, each line, while it lasts, we see some fluctuations in intensity.

 3.  At  $\epsilon=0.085$ , the randomness mostly dies out by one fourth of the total number of generations of the CML's evolution. Thereafter, barring the fringes, the lines of persistence alone remain over the rest of the evolution, albeit with some fluctuations of intensity within each line.

 4.  Interestingly, at  $\epsilon=0.095$  we observe the emergence of "wandering" chaotic lines that emerge from old or spawn new lines of persistence.

 5.  At  $\epsilon=0.105, 0.115$  we observe that most of the CML rapidly settles down in to strong unchanging lines of persistence.

 6.  The  $\epsilon=0.13$  shows similar behavior to the above, except that certain lines of persistence display a periodic variation within them like a regular wave.

 6.  At  $\epsilon=0.16$ , we interesting see the return of chaos with repeated episodes of chaotic behavior breaking up old lines of persistence followed by emergence of new lines of persistence.

 7.  Finally, in the last two runs we see a return to the predominantly random pattern. However, this is qualitatively different from the first case in that it shows some short lines of persistence, which establish small domains of local structure.

Thus, the degree of coupling between the cells of the lattice affect the long term evolution of the system for same initial conditions and driving chaotic oscillator. In the range of  $\epsilon$  explored above we see an optimal point for freezing of persistent patterns with randomness dominating in the extremes of the range. However, within the more "orderly" zone we may see outbreaks of mixed chaos and pattern-persistence.

Simple CMLs are computational elementary and conceptually easy-to-understand as simple cellular automata. In some ways they captures natural situations more closely than cellular automata. But on the other hand extracting interesting behavior from them appears to be more difficult. Importantly, they are unique in providing a tractable model for how the local chaotic oscillations couples with other such oscillators. This is seen in many biological systems --- networks of neurons, interacting bacterial cells in a colony, colonial amoebozoans and heteroloboseans --- all are good natural systems for real-life CMLs to play out. We see chaotic oscillatory patterns in individual cells, which if coupled appropriately, can result in regularized patterns after some generations or rounds of interactions. Both nature of the underlying chaotic oscillator and the degree of coupling will determine whether randomness, frozen patterns, or dynamic but not entirely random patterns dominate. This gives an opening for an important force, namely natural selection, that is often neglected in such dynamical systems-based approaches. Selection is required for setting up the oscillator and its parameters as also the coupling fraction. Further, like CAs, CMLs also have potential as historical models, where local oscillations in populations and their interactions could be captured by the coupling of the chaotic oscillators.

