
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion: "he becomes नरवाहनदत्त"](https://manasataramgini.wordpress.com/2017/01/02/matters-of-religion-he-becomes-naravahanadatta/){rel="bookmark"} {#matters-of-religion-he-becomes-नरवहनदतत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 2, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/01/02/matters-of-religion-he-becomes-naravahanadatta/ "8:31 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Somakhya's mother (SM) and Lootika's mother (LM) ran into each other during their visit to the shrine of Rudra beside the river on a Monday evening. They sat at the platform below the vast अश्वत्थ tree beside the subsidiary विष्णु shrine to chat for some time.

LM: "How is it going with all the quiet at your place now that Somakhya has left?"\
SM: "Well, I've returned to teaching the महाभारत and have been contemplating on the features of early Indo-Aryan. By the way I saw your brilliant daughter last evening at the clothes shop."\
LM: "Ah! You mean Vrishchika; have been worrying about her."\
SM: "Why? I heard that she has been invited to give a talk at some famous human genomics meeting regarding the paper she has just published. She seems to be following her stellar elder sister with this paper while just in the fourth year of med-school."\
LM: "That's exactly the matter of worry. The conference is at क्षयद्राजनगर, a big bad city, where I am sure you heard that just a week ago there was a major attack at the train station by the मरुपिशाच-s in which several were killed. Vrishchika has set her mind on going and you know when that happens there's no easy way to change it."

SM: "You should let her go. After it will bring her यशस्. It would seem she is on course to out doing even Lootika. Moreover, you let Lootika go on multiple trips, which included that really scary day of the Uniform Civil Code riots. You also let Varoli go to the dreadful Visphotaka, which is an even worse city than Kshayadrajanagara."\
LM: "O dear, you over-estimate Vrishchika. Knowing my daughters well I can say that that Lootika is to the rest of them like Maghavan among the gods. Thus, even on that day, while I feared for her greatly, I knew deep within she'll be back home: लूतिका बहुयुतिका वा."\
SM: "But some day Vrishchika will be on her own. You can't just keep her protected at home and unexposed to the big world. My brother lives in Kshayadrajanagara. I could give you his number. May be his daughter Saumanasa or son Mandara could pick Vrishchika up at the station and make sure she's alright."

LM: "That's very kind of you. I will certainly take their number, it will be useful in more than one way. However, I understand that Somakhya's friend, that kid Indrasena, who is her co-author and co-speaker in that conference, also lives there. I know she has gotten very pally with him and he would pick her up at the station. But at the same time I fear the two of them might be up to some mischief. Vrishchika let it slip that after their talk they were going to skip part of the meeting to go roaming in the city and make an excursion to Devaparvata."\
SM: "Ah, if Indrasena is there I'm sure he'll ensure she's OK. As for the mischief, I could give you his parents' number. Call them and express your concerns; they would make sure that the two don't do something too extravagant. But Vrishchika has to find her way in life: educate her but let her have her fun within limits. Let her meet my niece Saumanasa too: I am sure Saumanasa would benefit a lot from advice from Vrishchika."

Just then they saw a mongoose scampering away into overgrowth adjacent to the northern wall of the temple. It paused for while giving them a good "दर्शन" and then vanished. LM : "That looks almost like an adbhuta to me. What might it mean?"\
SM: "Well, since you saw it first possibly you are going to be favored by the mighty कामेश्वर."\
LM : "Hope both of us are..."

◊◊◊◊◊

Vrishchika's father had boarded her onto the train to Kshayadrajanagara, which soon got moving. Vrishchika felt tremendous happiness as the train started coursing on its way towards its destination -- for the first time she was travelling alone, unlike her sister Lootika, and sensed that she had become an adult. She also felt nice to be in solitude -- she remembered Lootika's words on the importance of solitude: "When you have to spend all your time in close contact with mundane people you start thinking like them and soon lose your ability to see the परोक्ष by which that which mystifies or even kills them can be apprehended. Hence, it is useful to have some time off in silent contemplation." After a while she started preparing for her presentation. Here again she followed her sister's method of carefully preparing her talk: laying out the time for each slide, planning what she was going say and when, and anticipate various questions and keep answers for them ready. At that point she wondered how their friend Somakhya had this uncanny knack of giving rather disparate talks back to back without preparing at all. She thought to herself: "Perhaps, that is तत्त्वावेश". But then Lootika's words came back to her that a good experimentalist must be methodical and well-planned in their approach to anything.

After a while, even as she was preparing to sleep, a woman passed by Vrishchika to her seat and as she did so dropped notes amounting to a few thousand रूपक-s and her passport on the floor. Vrishchika called out to her and but she had her ears plugged with a headset and walked on oblivious of her lost property. Vrishchika gingerly gathered the stuff and walked up and gave it to her. She was very happy and gave Vrishchika a note as a reward. Vrishchika smiled saying that she did not need it as: "धनेश्वर keeps me well!" The woman responded: "whoever that धनेश्वर is may he continue to be good to you", even as Vrishchika walked back to her seat.

◊◊◊◊◊

Indrasena and Vrishchika were rather disappointed that the former's parents had poured water on their plan of roaming through certain parts of the city that night after the meeting was over. As they were dropping them off at the conference center they said: "Indra, if it were just you we would not even ask where you have been. But we have promised this charming young lady's parents to keep her out of all trouble. While we have some confidence in your fighting skills, and even if you were to take your pistol with you, it is a wholly different matter of defending oneself and defending a young lady by your side. You know well that महामद's prowlers and other assorted dasyu-s are constantly looking out for girls in places you want to go to." Instead, they proposed that would pick them up at the conference center and bring them home for dinner.

As they were walking in Indrasena said somewhat sharply: "गौतमी, you should have been more careful with keeping secrets. If you want to have adventure you cannot announce everything to your parents."\
Vrishchika simpered and said: "I know. I just accidentally let it slip under pointed interrogation. But after all we might get to talk about the rahasya-s that I have been long wanting to know more about."\
Indrasena: "OK. Indeed, there are many exciting rahasya-s to talk about but remember that with such rahasya-s it is even more imperative to shut ones trap with the general public."\
Vrishchika: "I know, I know. I can be quite a Kunti when it comes to such matters but you know how it is when you are interrogated."

Later that evening Indrasena's parents picked them up and took them home for dinner. After dinner Indrasena took Vrishchika to his room. He offered Vrishchika a cushion as a seat and as she looked around his room for the first time taking in the sights she felt a strange sense of déjà vu. She saw a large painting of नरवाहन on the wall with his three wives श्रिया, ऋद्धि and भद्रा hugging him and his son नलकूबर seated beside him. The mantra "[नमो धनदाय च धनस्वामिने च ॥]{style="color:#0000ff;"}" was inscribed around the painting. Below it hung a Japanese painting with several figures of which she could only recognize one as being a यक्ष. Below that on a separate stool was a well-bound copy of the बृहत्कथा.

Vrishchika: "O आत्रेय, I see them all, the signs of यक्षराट् everywhere. This is the indeed the sign of knower of rahasya-s. Who else today can even contemplate being on the path of नरवाहन-datta the son of Udayana. How was that most powerful path revealed to you? Most only sadly wish for it as losers do."\
Indrasena: "O गौतमी it is a long story but short of it goes thus. In my childhood as I was learning the तैत्तिरीय-श्रुति I had dreams on many continuous nights of the great peak of Kailasa-parvata. Above it I saw hovering a great airplane, the पुष्पक. By myself I began uttering the mantra-s starting with [तिरोधा-भूः स्वाहा।]{style="color:#0000ff;"} Other mysterious mantra-s of वैश्रवण started coming to my mind as the old Hindus would say as if from a past birth. But they were incomplete and I had wait till I could apprehend most of them due to the teachings of the वञ्ग-सिंह."

Vrishchika: "Who is this teacher -- is it not remarkable that he obtained the कौबेरी विद्या that lies concealed?"\
Indrasena: "My father has a friend. He was known as the वङ्ग-सिंहो राजराज-mata कण्ठीरवः: a vipra who was a lion among the वङ्ग-s. He could be truly described as 'धनं meru-tulyam' and possessed of many kinds of विद्या-s. People looked at him and wondered 'how could he be endowed with all of this wealth, health, beauty, intelligence, wife and children? He never seemed to age!' After all it is almost a truism that: "[इन्द्रेण सर्वानि शुभान्य् एकस्मिन् पुरुषे संयुक्तं न स्थापितानि ।]{style="color:#0000ff;"}' Seeing him people got the hint that after all the advaitin-s were simply being losers when they asked 'ततः kim?' when confronted with धनं meru-tulyam| As though guided by राजराज, with youthful impropriety, I once jousted with him on a scientific topic very familiar to the two of us, as also your अग्रजा and Somakhya of quick discernment. I will tell you about that at a different point in time. While he did not openly admit it, despite being a widely acknowledged professor, he felt he had been defeated by a mere youth like me in that debate. It was then that it slipped out of his mouth: 'Perhaps, this why the आचार्य had said the totality of Dhanada-siddhi is not on me. It must be due to the mantra of the भृगु-s I don't have.' I queried him on the धनेश्वर-siddhi and he agreed to help me apprehend the Kaubera-शासन whose mantra-s had briefly flashed before me saying if one mastered it 'he becomes नरवाहनदत्त."

Vrishchika: "Most interesting, I see signs of a genuine Jambhala-विद्वान् in you. Indeed, why talk about the imaginary '[एको ब्रह्मण आनन्दः]{style="color:#0000ff;"}' when you cannot achieve '[एको मानुष आनन्दः]{style="color:#0000ff;"}' in this world. It is more like wishful thinking coming from not applying oneself vigorously to the path of धनेश्वर. But since there is no gain without pain I have long suspected that this path must have be arduous. What is the significance of that Nipponic painting below that of the राजराज-परिवार? It reminds me of the incident I have told you from childhood when I captured as a खार्खोड the भूत of a प्राच्य from those regions."\
Indrasena: "Ah that one is important to me as a reminder that, even though arduous, one who applies himself to the वित्तेश-सिद्धान्त can be like a नरवाहनदत्त even if he might be from the far-off land of the पीतवर्ण-s"\
Vrishchika: "How did you get it? Who are those figures depicted in it?"\
Indrasena: "My father obtained it when visiting the प्राच्य-s on matters relating to his work. He managed to with some difficulty befriend the phlegmatic प्राच्य "देशिक'' of the temple on mount Shigi who eventually gave him this painting on mulberry paper. The figure you see to the left is a Nipponic mantra-वादिन् known as Myoren with immense Kaubera-siddhi. The figure to the right is Daigo the emperor of Japan. He was afflicted by an incurable disease. Myoren performed a वित्तेश-साधना and as result a guhyaka appeared -- he is the central figure with the sword -- the यक्ष then cured the emperor."\
Vrishchika: "Remarkable!"\
Indrasena then walked up to the painting and turned it around: "Here on the back you can see the images of the great वैश्रवण along with his wife श्रिया and son नलकूबर. This a painting of their idols installed on Mt Shigi."

Vrishchika: "O Atri, that painting of a sword with the siddham script also seems to be Nipponic. It too seems to be connected with श्री नरवाहन. It looks like the sword of a विद्याधर upon attaining siddhi. Could you please tell me more about it?"\
Indrasena: "गौतमी that is the painting of the sword received by Sakanoue no Tamuramaro from यक्षराट् before going to war with the Ainus. There is a long back-story here. It is said that when Xuanzong, the Tang emperor of the चीन-s, was in deep fear of his enemies he was aided by वैश्रवण and नलकूबर due to their mantra-s deployed by Amoghavajra. A little later when the Silla Koreans backed by the चीन-s threatened the Nipponians the latter similarly invoked वैश्रवण through an elaborate ritual that gave them total immunity from those mainland cousins of theirs. At that time a Nipponic मन्त्रवादिन् had a dream wherein a horse with a jeweled saddle led him to a holy site, Kuramadera, to the north of Kyoto, where he found a स्वायम्भुव idol of Kubera and installed it in a great temple there. Thereafter in the late 700s of the Common Era, the warrior Sakanoue no Tamuramaro worshiped Dhanada at that temple, attained siddhi much like our नरवाहनदत्त, and obtained a sword from the god. He used this sword in his campaign against the Ainus and conquered their land. There he built a copy of the temple at Kuramadera depicting यक्षराट् as being borne by guhyaka-s known as nara-s exactly as prescribed in our tradition."\
Vrishchika: "Thank you for the most interesting narrative. What is written in the siddham script on that painting?"\
Indrasena: "It is the mantra Sakanoue used: [ॐ वैश्रवण सहपरिवारेण समाज जः हुं वं होः ॐ वे स्वाहा ॥]{style="color:#0000ff;"}"

Vrishchika: "Indra, I have so many things to talk to you about. But since time is short right now I would like to ask you regarding the पुष्पक-विमान yantra which is mentioned in the श्रुति of the तैत्तिरीयक-s as being engineered by the god त्वष्टृ for Kubera. Could you please give me a दर्शन of the पुष्पक-विमान-yantra you worship and lead me to the पुष्पक-विमान-साधना?"\
Indrasena: "अलिनी. That's a rahasya-prayoga. You would need to engage in much Kaubera-योगाभ्यास for any degree of success in that direction. Nevertheless let me show the पुष्पक to you." Indrasena led her to a closet in his room and opened the door to reveal the image of the विमान in the center of which were the seated images of राजराज, ऋद्धि and नलकूबर. His अन्तर्धानास्त्र was placed in front of him. In front of the विमान was the image of यक्ष मङ्कनक the gatekeeper of Kubera. There was also an image of the cow सर्वकामदुघा. There was a pot of the five-metal alloy with water and a glass pot with honey in it on the विमान-प्रतिमा. Indrasena: "अलिनिका utter the following mantra-s after me gazing single-mindedly at the पुष्पक-विमान:\
[ॐ हं जम्भलाय वैश्रवणाय मणिभद्राय पाङ्चिकाय पूर्णभद्राय नलकूबरय यक्षेभ्यो यक्षाणां पत्ये नमः ।]{style="color:#0000ff;"}"

Then he gave her some honey with a silver spoon and asked her to eat it with the mantra:\
"[इदं जम्भलस्य मधु मघोनं मधुना प्रजावती पयस्वती धनवती धीमती अयुष्मती भूयासम् ।]{style="color:#0000ff;"}"

Then with another spoon he gave Vrishchika the water with a small amount of cedar nut oil: "Apply this on your eyelids with the following mantra:\
"[कुबेरस्योदकम् इदम् तेनादृष्टं दृष्टम् भवति हुं नकुलहस्ताय स्वाहा ।]{style="color:#0000ff;"}"

Vrishchika: "Ah this is the प्रतिकृति of the water sent by वितेश to help राम and लक्ष्मण penetrate the माया of मेघनाद!"

Indrasena: "Indeed. Then you shall close your eyes, visualize the great cave in the Himalayan heights where Kubera has placed the pot of golden kauberaka honey and meditate upon it doing japa of the mantra:\
[ॐ ह्रीं वैश्रवण! धनं पुष्टिं देहि मे स्वाहा ।]{style="color:#0000ff;"}\
When you have become sthiramati emerge from your meditation uttering [ॐ वैश्रवण अर्थतमोम्।]{style="color:#0000ff;"} 3 X [हुं फट्।]{style="color:#0000ff;"}"

After Vrishchika did so she emerged from her ध्यान uttering: "[अपश्यं त्वा यक्षम् उग्रं त्वाष्ट्रीं विमानरूढं पुरुश्चक्राणि सहस्र-वन्धुराणि । स वैश्रवणो राजराजा महतो महीयान् यस्य प्रतिग्रहेणाप्स्यामि धनं मेरु-तुल्यम् । कामम् प्रतिगच्छामि । धनम् प्रतिगच्छामि । प्रजाः प्रतिगच्छमि । ईश्वरेण महदो३ ओ३ ॐ ॥]{style="color:#0000ff;"}"

Indrasena: "That's good it appears you are on the path of ईश्वर-siddhi. I know you already have accomplishments as a मन्त्रवादिनी but before proceeding I need to know if you have studied your earlier सिद्धान्त-s well. Utter and expound the द्वादश-नामानि."\
Vrishchika: "I do know them from my सर्वाधिकार-दीक्ष:\
[धनदश् च यक्षपतिर् वित्तेशो निधिपालकः ।]{style="color:#0000ff;"}\
[राक्षसाधिपतिश् चैव पिङ्गलाक्षो विमानगः ॥]{style="color:#0000ff;"}\
[रुद्रसखा कुबेरश् च गुह्यकानां पतिस् तथा ।]{style="color:#0000ff;"}\
[वैश्रवणेश्वरश् चैव यक्षेन्द्रः परिकीर्तितः ॥]{style="color:#0000ff;"}

Dhanada: the giver of wealth; यक्षपति: the lord of the यक्ष-s; वित्तेश: the lord of wealth; निधिपालक: the guardian of wealth; राक्षसाधिपति: the lord of the रक्ष-s, this has been already stated in the यजुष् and the अथर्वण-श्रुति-s; पिङ्गलाक्ष: he whose eyes are of a golden tint; विमानग: one who can go anywhere on his पुष्पक airplane; रुद्रसखा: friend of Rudra, this has been explained in our national epic; Rudra with his family often resides in the space station of Kubera; Kubera: One who has a frightful form; गुह्यकानां pati: lord of the hidden यक्ष-s who in the श्रुति are known as तिरोधा; वैश्रवण: the lord of the northern garden-land known as विश्रवस्; ईश्वर: the great lord; यक्षेन्द्र: who is like Indra among the यक्ष-s."

Indrasena: "Great. At some future point when we are united after passing beyond the place known as म्लेच्छदिग्द्वारवृत्ति we shall perform together the great याग as enjoined in the तैत्तिरीय-श्रुति concluding with the mantra [राजाधिराजाय]{style="color:#0000ff;"}... By that time I would have mastered the full rahasya-s of this very mysterious mantra of the भृगु-s from the Atharvaveda:

[यद् इन्द्रादो दाशराज्ञे]{style="color:#0000ff;"}\
[मानुषं वि गाहथाः ।]{style="color:#0000ff;"}\
[विरूपः सर्वस्मा आसीत्]{style="color:#0000ff;"}\
[सह यक्षाय कल्पते ॥]{style="color:#0000ff;"}

But for now you may do पुरश्चरण of the अन्तर्धानास्त्र-mantra. It will provide us with the fury needed withstand the assault of the एकराक्षसवादिन्-s who would seek to place in the museum in that great clash in the future."

Vrishchika: "That sounds frightening. Tell me more of how one approaches this rahasya?"\
Indrasena: "The full vidhi goes thus: One observes fast on the द्वितीय. Then on तृतीय one performs the rite having broken ones fast. One places an image of the गदा in a golden or silver vessel with ghee on which has been inscribed the 12 names of the यक्ष. One invokes Kubera with the following mantra enjoined by the वैखानस-s:\
[रायस्-पोषाय आयुषे प्रजायै नीधीशम् आवाहयमि ।]{style="color:#0000ff;"}\
(For increasing prosperity, life and offspring I invoke the lord of wealth)

Then he does the visualization of Kubera thus:\
[अथ ध्यानम्:]{style="color:#0000ff;"}\
[यक्ष-राक्षस-सैन्येन गुह्यकानां गणैर् अपि ।]{style="color:#0000ff;"}\
[युक्तश् च शङ्ख-पद्माभ्यां निधीनाम् अधिपः प्रभुः ॥]{style="color:#0000ff;"}\
[राजराजेश्वरः श्रीमान् गदापाणिर् धनेश्वरः ।]{style="color:#0000ff;"}\
[विमान-योधी धनदो विमाने पुष्पके स्थितः ॥]{style="color:#0000ff;"}\
[स राजराजः सुशुभे युद्धार्थी नरवाहनः ।]{style="color:#0000ff;"}\
[प्रेक्षमाणः शिवसखः साक्षाद् इव शिवः स्वयम् ॥]{style="color:#0000ff;"}

(The lord of wealth, the overlord, is united with यक्ष, रक्ष-s and guhyaka hosts, as also शङ्ख and पद्मा. The lord of the king of kings, the wealthy one, the lord of riches holds a mace in his hand. The airplane-warrior, the giver of wealth is stationed in his पुष्पक airplane. He the king of kings, residing in great auspiciousness, eager in combat, is borne by यक्ष-s known as Nara-s. Watching on, the friend of शिव is himself like a second शिव)

Then as ordained by the वैखानस-s one offers the पूर्वार्घ्य of water in a receptacle with the गायत्री:\
[राज-राजाय विद्महे धनाध्यक्ष्याय धीमहि।]{style="color:#0000ff;"}\
[तन् नो यक्षः प्रचोदयात् ॥]{style="color:#0000ff;"}

Then one performs japa and/or homa of the mantra:\
[अस्य मन्त्रस्य वदन्य ऋषिः । विराट् छन्दः । अन्तर्धान-धारिन्-उग्र-कुबेरो देवता ।]{style="color:#0000ff;"}\
[ॐ छण्डोग्रयक्षाय हुं फट् तिरोधेहि सपत्नान् नः स्वाहा ॥]{style="color:#0000ff;"}

There after one concludes with the incantation of the शान्खायन-s:

[नैनं रक्षो न पिशाचो हिनस्ति न जम्भको नाप्य् असुरो न यक्षः ॥]{style="color:#0000ff;"}\
(Neither रक्ष-s nor पिशाच-s, nor jambhaka-s, i.e. Kubera's agents, nor asura-s nor यक्ष-s harm him)

Then one offers the मध्यमार्घ्य as above with the mantra:\
[रुद्र-सखाय विद्महे वैश्रवणाय धीमहि ।]{style="color:#0000ff;"}\
[तन् नः कुबेरः प्रचोदयात् ॥]{style="color:#0000ff;"}

Then one recites the stuti:\
[धनस्य कामस्य प्रणायकस् त्वं ।]{style="color:#0000ff;"}\
[सुखस्य लाभस्य प्रदायकस् त्वं ॥]{style="color:#0000ff;"}\
[विमानगस् त्वं लोकेश्वरस् त्वं ।]{style="color:#0000ff;"}\
[नामामि जिष्णुं चण्डोग्र-यक्षं ॥]{style="color:#0000ff;"}\
(You are the leader in wealth and desire;\
You are the giver of happiness and profit;\
You are the airplane-rider the lord of the world\
I salute the conquering fierce formidable यक्ष)

Then one does तर्पण with the 12 names in the accusative case.\
Additionally he also offers तर्पण to the परिवार:\
[ऋद्धिं तर्पयामि ।]{style="color:#0000ff;"}\
[श्रियां तर्पयामि ।]{style="color:#0000ff;"}\
[भद्रां तर्पयामि ।]{style="color:#0000ff;"}\
[नलकूबरं तर्पयामि ।]{style="color:#0000ff;"}\
[मणिभद्रं तर्पयामि ।]{style="color:#0000ff;"}\
[शङ्खं तर्पयामि ।]{style="color:#0000ff;"}\
[पद्मं तर्पयामि ।]{style="color:#0000ff;"}\
[मङ्कनकं तर्पयामि ।]{style="color:#0000ff;"}\
[यक्ष-पार्षदांस् तर्पयामि ।]{style="color:#0000ff;"}\
[यक्ष-पार्षदींस् तर्पयामि ॥]{style="color:#0000ff;"}

One then offers the प्रसन्नार्घ्य with the mantra:\
[यक्षेश्वराय विद्महे गदा-हस्ताय धीमहि ।]{style="color:#0000ff;"}\
[तन् नो यक्षः प्रचोदयात् ॥]{style="color:#0000ff;"}

One then makes offering of pure cooked food as bali with mantra ordained by the वैखानस-s:\
[राज-राजो धनाध्यक्षः कुबेरो विश्रवस्-पतिः ।]{style="color:#0000ff;"}\
[प्रीयतां निधि-संयुक्त ईश्वरस्य सखा प्रभुः ॥]{style="color:#0000ff;"}\
(The king of kings, the administrator of wealth, Kubera, the lord of the northern land of विश्रवस्, possessed of wealth, the friend of Rudra, may the lord be pleased)

Then he does उपस्थान:\
[परिवारेण सह कुबेरो नकुलहस्तः सुप्रीतो सुप्रसन्नो वरदो यथा स्थानं तिष्ठतु ॥]{style="color:#0000ff;"}\
(With his retinue may Kubera, holding a mongoose, pleased and happy, boon-giving remain in his own region)"

Indrasena then continued: "One performs this vrata for an year observing restraints of not consuming alcohol and eating pure food and maintaining one's body with discipline."\
Vrishchika: "I express my profuse gratitude to you for revealing the vidhi. I am hoping to perform the vidhi even as you have described it. But the only issue is how I might be able to obtain the गदा?"\
Indrasena: "I will be duly sending you one. Again most only attain partial ईश्वरसिद्धि. Only those born of good families for multiple generations attain that complete siddhi. In गोमूत्र-nagara lived a man who saw all these विद्या-s but his discipline was incomplete so his लाभ was also incomplete. Likewise with the वङ्गसिंह. But fearlessly perform the पुरश्चरण. Someday all will come together."\
Vrishchika: "We will not be able see each other from some time in the coming future as our paths diverge but in the future we shall be united again and shall perform the great याग."


