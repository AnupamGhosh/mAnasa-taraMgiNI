
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Early Hindu mathematics and the exploration of some second degree indeterminate equations](https://manasataramgini.wordpress.com/2017/02/28/early-hindu-mathematics-and-the-exploration-of-some-second-degree-indeterminate-equations/){rel="bookmark"} {#early-hindu-mathematics-and-the-exploration-of-some-second-degree-indeterminate-equations .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 28, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/02/28/early-hindu-mathematics-and-the-exploration-of-some-second-degree-indeterminate-equations/ "11:53 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The following is merely a record of our exploration as a non-mathematician/non-computer scientist of a remarkable (at least to us) class of numerical relationships.

An equation like  $x^2-x-1=0$  can be solved to obtain specific solutions as:  $x=\frac{1+\sqrt{5}}{2},\;\frac{1-\sqrt{5}}{2}$ . However, if we have an equation like then  $437x+592y=298374$  it is termed indeterminate as it has no unique set of solutions. Rather the values of the variables satisfying it define a curve in a space with as many dimensions as the number of variables, which in the above case is a straight line on a plane. However, if we constrain the solutions to being integers we get specific, though sometimes infinite solutions. This constraint essentially reduces the problem to finding the points on the curve that lie on a fixed lattice. Further, if we add the constraint that the solutions should be positive then we can come down to a limited set of solutions, like for the above equation of the line we have  $(x=126, y=411)$  as lying on the integer lattice in the positive quadrant. Such integral solutions to indeterminate equations are referred to in western and thereby in modern mathematics as Diophantine equations after the yavana mathematician Diophantos.

However, Hindu tradition has one of the deepest histories of the analysis of such indeterminate equations. The earliest studies go back to the ritual वेदी-s of the Yajurveda and they find mention in the problems of the शुल्बसूत्र-s. This suggests that the Hindu interest in them arose in the context of ritual. Similarly, Proclus, Diophantos and Archimedes before him were likely building on a Pythagorean base, suggesting that such problems might have existed in the ancestral ritual tradition of both the yavana-s and आर्य-s. Already in the Yajurveda tradition we encounter such indeterminate equations of the linear form and also those resulting in the भुजा-कोटि-कर्ण (known in the west as Pythagorean) triples (see below). It is also likely that the adhvaryu-s used an equation of the form  $x^2=2y^2+1$  to obtain the solutions  $(x=577, y=408)$  to obtain the value of  $\sqrt{2} \approx 1+\frac{1}{3}+\frac{1}{3}\times\frac{1}{4}-\frac{1}{3}\times \frac{1}{4} \times \frac{1}{34} = \frac{577}{408}$  correct to 5 decimal places. That this was the case is supported by the commentary of the great नीलकण्ठ सोमयाजिन् who notes that the factor 17 in the denominator of the शुल्बसूत्र value comes from  $17^2=2 \times 12^2+1$ . In the post-Vedic period the analysis of indeterminate equations was advanced further. We learn in the teachings of: 1) आर्यभट of the कुट्टक algorithm for linear indeterminate equations. 2) Brahmagupta and his successors Jayadeva and भास्कर-II of the algorithms to solve certain degree 2 indeterminate equations. 3) नारायण a general summary of these equations and their applications (his text the गणित-kaumudi).

Of the degree 2 indeterminate equations, integral solutions of those of the form  $x^2=ay^2+1$  where  $a$  is an integer were of greatest interest to the Hindus. As we saw above such an equation with  $a=2$  might have been used by seers of the Yajurveda to approximate  $\sqrt{2}$ . Looking at this equation some things become immediately clear. For  $(x=1, y=0)$  we get a trivial solution which is true for all  $a$ . If  $a$  is a perfect square then  $ay^2+1$  will never be a perfect square so we have no solutions beyond the trivial one. If  $a$  is not a perfect square we could potentially have infinite solutions because at some point  $ay^2+1$  is free to become a perfect square. This can be easily understood geometrically: The equation  $x^2-ay^2=1$  defines a family of hyperbolas (Figure 1). Integral solutions will occur where ever the hyperbola passes through the integer lattice. From the equation we can deduce that one of the two asymptotes of the hyperbola is  $y=\frac{x}{\sqrt{a}}$ . If  $a$  is a perfect square then the asymptote will pass through points on the integer lattice but beyond the trivial case the hyperbola will never pass through the points on the lattice. However, if  $a$  is not a perfect square then  $\sqrt{a}$  will be irrational. Hence, as the hyperbola converges towards it asymptote, each time it passes through a lattice point  $\frac{x}{y}$  will be a closer and closer approximation of  $\sqrt{a}$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/02/chakravala_hyperbola.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


For the particular case  $x^2=2y^2+1$  there is a special significance that links arithmetic to geometry. The sum of integers is given by the formula presented by sage शाकपूणि in Vedic antiquity:

 $$1+2+3...n \Rightarrow \sum \limits_1\^n i =\dfrac{n(n+1)}{2}$$ 

By laying the integers as equidistant points on successive rows we get an equilateral triangle lattice with the sum up to the nth integer completing the next equilateral triangle (Figure 2). Hence these sums are known as triangular numbers.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/02/triangular_numbers.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


Now consider the sum of odd integers:

 $$1+3+5+7...(2n-1) \Rightarrow \sum \limits_1\^n (2i-1)=n^2$$ 

Laying out the nth odd number  $2n-1$ , with each new addition bracketing the previous one we get a square lattice (Figure 3). Hence, their sum is  $n^2$ . Thus these sums are known as square numbers.
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/02/square_numbers.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad
```{=latex}
\end{center}
```


Now a question which arises is whether there are any triangular numbers that are also square numbers. Such a question might be germane to a neo-ritualist who wishes to make a triangular वेदी with the same number of lattice points as a square वेदी. This is not entirely out of place because square numbers arose first in the vaidika system of square altars and the triangular numbers had a significance for the Pythagoreans (also see [earlier note](https://manasataramgini.wordpress.com/2016/10/23/syllable-number-and-rules-in-the-ideal-realm/)). In any case, the answer to this question is obtained thus: Let  $r$  be the number to which we desire to sum ( i.e.  $1+2+3...+r$ ) to get the triangular number. Let  $s$  be the number which we desire to be side of the square number (i.e  $1+3+5+...(2s-1)=s^2$ ). Then we have:

 $$\dfrac{r(r+1)}{2}=s^2\\ r^2+r=2s^2\\ \therefore 4r^2+4r=2(4s^2)\\ \therefore 4r^2+4r+1=2(4s^2)+1\\ \therefore (2r+1)^2=2(2s)^2+1\\ let \; x=2r+1 \; and \; y=2s \\ \therefore x^2=2y^2+1\\ \therefore r=\dfrac{x-1}{2} \; and \; s=\dfrac{y}{2}$$ 

Thus the integral solutions of the above equation give us the triangular numbers which are also square numbers. As one can notice  $2y^2+1$  will always be an odd number; hence, for the solutions  $x$  will always be an odd number and  $y$  will always be even \[an odd square will be  $(2n-1)^2= 4n^2-4n+1$ ; Now  $2y^2+1=4n^2-4n+1$ ; thus we have  $y^2=2n^2-2n$ ; hence  $y$  is always even]. Thus, for each solution of the above indeterminate equation we get the equivalent triangular and square numbers. Interestingly, each successive approximation of  $\sqrt{2}$  yields the point where triangular and square numbers are equivalent. Illustrated in Figures 2 and 3 are the cases where the triangular and square numbers become equal till  $r=49, s=35)$ . Below is a table of the first 11 such values of  $(x,y)$  and corresponding  $(r,s)$ .![x2_2y2_1](https://manasataramgini.files.wordpress.com/2017/02/x2_2y2_11.jpg){.alignnone .size-full .wp-image-9355 attachment-id="9355" comments-opened="0" image-caption="" image-description="" image-meta="{\"aperture\":\"0\",\"credit\":\"aravind\",\"camera\":\"\",\"caption\":\"\",\"created_timestamp\":\"1488240781\",\"copyright\":\"\",\"focal_length\":\"0\",\"iso\":\"0\",\"shutter_speed\":\"0\",\"title\":\"\",\"orientation\":\"0\"}" image-title="x2_2y2_1" large-file="https://manasataramgini.files.wordpress.com/2017/02/x2_2y2_11.jpg" medium-file="https://manasataramgini.files.wordpress.com/2017/02/x2_2y2_11.jpg" orig-file="https://manasataramgini.files.wordpress.com/2017/02/x2_2y2_11.jpg" orig-size="614,435" permalink="https://manasataramgini.wordpress.com/2017/02/28/early-hindu-mathematics-and-the-exploration-of-some-second-degree-indeterminate-equations/x2_2y2_1/" loading="lazy" sizes="(max-width: 614px) 100vw, 614px" srcset="https://manasataramgini.files.wordpress.com/2017/02/x2_2y2_11.jpg 614w, https://manasataramgini.files.wordpress.com/2017/02/x2_2y2_11.jpg 150w, https://manasataramgini.files.wordpress.com/2017/02/x2_2y2_11.jpg 300w"}

As can be seen above these numbers can quickly large. A further question is can we get such solutions more generally for the above equation where  $a$  takes values other than 2. The answer to this lies in one of the greatest discoveries of the Hindu mind. For the rest of this note we shall restrict ourselves to the first  $(x,y)$  that satisfies the equation, though the method discussed can be used to get further values too.

Our encounter with this equation goes back to our youth when our father had taken us to a bookshop. There a book on Hindu mathematics caught our sight and in it we encountered this equation  $x^2=ay^2+1$  for the first time. We also noted in that the book that Brahmagupta had mentioned that one could be considered as having some mettle in mathematics if one solved the equation for  $a=83, a=92$ . Interestingly, when these equations were first being explored in the West (1600s) the Frenchman Fermat too posed one of them as a major challenge to rival English mathematicians and others. Not being of extensive means we were not in position of buying many books those days and had to leave it in the shop and return home. Nevertheless the problem stuck in our mind and we started exploring it with paper and pen. We were able obtain the first solutions for numbers  $\leq10$  but soon realized that 83 and 92 were beyond our means, plainly showing us that we had no mathematical talents. However, the book usefully informed us that there was an algorithm developed by the Hindu mathematicians/scientists from Brahmagupta onwards culminating in Jayadeva, which was known as the चक्रवाल that allowed one to solve any such equation. Commonly one finds it attributed to भास्कर-II but it is already mentioned in the commentary called सुन्दरी of उदयदिवाकर from 1073 CE where he says it was achieved by Jayadeva before him. Hence, it was already in place long before भास्कर-II, who when providing it merely says its is known or has been recited as the चक्रवाल ([चक्रवालम् इदं जगुः ।]{style="color:#99cc00;"}).

However, the time was too brief at the time of our first encounter for us to understand the चक्रवाल. Hence, a little later when we had access to a computer for the first time we wrote a simple program to carry out a brute force search for all the first solutions to the equation till  $a=100$ . We thus found the answers for  $a=83: (x=82, y=9)$  and  $a=92: (x=1151, y=120)$ . However, since our search was carried out only to a maximum of  $10^5$  (1 लक्ष) we had several holes. This indicated a surprising fact that for some values of  $a$   $(x,y)$  could suddenly take large values among otherwise pedestrian values. This meant we had to have a better way of finding them. Hence, we got back to study the चक्रवाल algorithm and implement it having found a book describing it with a richer relative. In doing so we realized that the way it was presented in the sources we used was not the best for implementation on a computer. Hence, we had to implement it in the form of a systematic search which catches all corner cases that cannot be handled by the "simple" presentation we usually see for this algorithm. Below is a presentation of such an implementation of the function  $cv(a)=(x,y)$  which would output the first non-trivial solutions of the equation (Hindus termed the solutions  $(x,y)$  as antya and आदि: Brahmagupta or as ज्येष्ट and hrasva: Jayadeva and भास्कर-II)

Do until  $val=(x,y)$  is obtained\
 $p_n=\lfloor \sqrt{a} \rfloor \Rightarrow \lfloor x \rfloor$  is the floor function\
if  $p_n==\sqrt{a}$  then  $val=(x=1,y=0)$ ; end execution\
write array  $j=(0,1,-1,2,-2,...(p_n-1),(-pn+1))$ 

 $$n=1$$ 

Do until  $val=(x,y)$  is obtained

 $$p_n=\lfloor \sqrt{a} \rfloor$$ 

 $$p_n=p_n+j\lbrack n\rbrack$$ 

 $$q_n=1$$ 

 $$m_n=p_n^2-a$$ 

 $$x_n=-p_n\; mod\; |m_n|$$ 

if  $x_n==0$  then  $x_n= \lfloor\sqrt{a}\rfloor+j\lbrack n\rbrack$ \
Do until  $val=(x,y)$  is obtained

 $$p_{n+1}=\dfrac{p_n x_n+a q_n}{|m_n|}$$ 

 $$q_{n+1}=\dfrac{p_n+x_n q_n}{|m_n|}$$ 

 $$m_{n+1}=\dfrac{x_n^2-a}{m_n}$$ 

 $$x_n= -x_n\; mod\; |m_{n+1}|$$ 

if  $x_n==0$  then  $x_n= \lfloor\sqrt{a}\rfloor+j\lbrack n\rbrack$ 

 $$p_n=p_{n+1}$$ 

 $$q_n=q_{n+1}$$ 

 $$m_n=m_{n+1}$$ 

if  $q_n-\lfloor q_n\rfloor> 0$  then break this loop;  $n=n+1$ ; restart from outer loop with next value in array j  $(j\lbrack n+1\rbrack )$ \
if  $m_n==1$  then  $val=(x=p_n, y=q_n)$ ; end execution\
return(val)

If one instructs a computer to do the above procedure in any language it understands one can unerringly get the first non-trivial solutions of the equation under consideration. The implementation has three nested loops. The outer loop catches the cases where  $a$  is a perfect square and has only trivial solutions. The innermost loop is the core चक्रवाल procedure where the search iteratively proceeds until  $m_n == 1$ . As the first approach it starts with an initial  $p_n$ , i.e.  $p_0=\lfloor \sqrt{a} \rfloor$ . This is what you typically see in examples illustrating the procedure. However, one quickly realizes that with  $a=14$  that does not work even though it has a relatively simple solution  $(x=15, y=4)$ . Here, we need to start with  $p_n=4$  instead of  $p_n=3$  because the latter results in fractional  $q_n$  (note it has to always be integer for procedure). भास्कर-II seems indicates that one might need take an alternative starting point with the 'अथवा':[गुणवर्गे प्रकृत्य +ऊने ।अथवा +अल्पं शेषकं यथा ।]{style="color:#99cc00;"} (गुण-varga  $\Rightarrow p_0^2$  i.e. square of starting  $p_n$ ; प्रकृत्य  $\Rightarrow a$ ). But when we go to  $a=21$  we see that both  $p_0=4,5$  fail. Hence, we need to go to  $p_0=3$  to get the solution without hitting a fractional  $q_n$ . It is for this issue that we need to incorporate the second loop where we search for the appropriate  $p_0$  systematically starting from  $\lfloor\sqrt{a} \rfloor$  and moving up and down from it by 1, 2 so on. Thus, we have a simple implementation of the चक्रवाल on a computer that anyone with even limited mathematical capacity like us can achieve. Since by this procedure we always reach the solution, i.e. the algorithm always halts, we have not implemented Brahmagupta and भास्कर-II's shortcuts.

Thus, armed with the चक्रवाल in a second or so we obtained the values of the first  $(x,y)$  for all  $a=1:100$ . In the process we blazed through भास्कर-II's famous illustration of  $a=61$  which yields big values:  $(x=1766319049, y=226153980)$ . While big, to put it in perspective the ज्येष्ट is still only about  $\frac{1}{3}$  number of people alive on earth. As we tried to go past the  $a=100$  barrier towards  $a=200$  we hit a snag as our numbers ran beyond our language's standard numbers kept as double-precision floating-point (64 bits \~ 15:17 digits). So we had re-implement the algorithm using multiple precision floating-point numbers. This slowed it down a bit but was not too bad given the चक्रवाल's inherent speed. Now we could obtain a table of all first  $(x, y)$  all the way to  $a=1000$ . Now, we are not talking just big but truly large numbers that are a delight to the Hindu eye. In this range we reach the maximum  $(x=16421658242965910275055840472270471049,\\ y=638728478116949861246791167518480580)$  for  $a=661$  interestingly 600 ahead of भास्कर-II's  $a=61$ . To put is perspective that is close to the diameter of the universe measured in Hydrogen atom diameters. At  $a=421$  we get  $(x=3879474045914926879468217167061449,\\ y=189073995951839020880499780706260)$  which is roughly the diameter of an apple (6.27 cm) in Planck's lengths. We provide this [table](https://manasataramgini.files.wordpress.com/2017/02/chakravala_padded.pdf "chakravala_padded") for the reader's enjoyment as such information does not seem to be easily available. The first two columns give the ज्येष्ट and hrasva. The next two columns  $(n,m)$  give the number of iterations of the second and total number of iterations of the third loop that were needed until the solution was obtained. The first non-trivial  $x$  values are also plotted for  $a=1:1000$  (Figure 4).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/02/chakravala1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


Is there any pattern to these values? As noted above  $a=14,21$  need us to search beyond the  $p_0=\lfloor \sqrt{a} \rfloor$ . Indeed, multiples of 7 seem have a higher propensity for this problem because we encounter a similar issues with 14, 21, 28, 70, 77, 98, 112, 126, 133, 140, 154, 161, 189, 238, 252, 259, 266, 273...: Out of the 142 multiples of 7 that  $a$  assumes between 1 and 1000, we need multiple rounds of exploration  $(n)$  for 79 of them. Indeed the highest values of  $n=21$  are attained for  $a= 966=7 \times 138$  and  $a=987= 7 \times 141$ .

For the actual values of  $(x,y)$  there are some patterns that can be easily proved: When  $a=k^2-1$ , i.e.  $a$  is one less than a perfect square the solution is  $(x=k, y=1)$ . For  $a=k^2+1$  we can easily show that  $(x=2k^2+1, y=2k)$ . When  $a=k^2 \pm 2$  it is obvious that  $(x=k^2 \pm 1, y=k)$ . Similarly, when  $a=4k^2 \pm 4$  then  $(x=2k^2 \pm 1, y=k)$ . Slightly more subtle ones are: when  $a=(3k)^2 \pm 3$  then  $(x=6k^2 \pm 1, y=2k)$ ; when  $a=9k^2 \pm 6$  then  $(x=3k^2 \pm 1, y=k)$ ; when  $a=25k^2 \pm 10$  then  $(x=5k^2 \pm 1, y=k)$ . Such may be termed regular categories of  $(a:x,y)$ 

It also becomes apparent (Figure 4, [Table](https://manasataramgini.files.wordpress.com/2017/02/chakravala_padded.pdf "chakravala_padded")) that for several prime values we get explosive eruptions of the  $(x,y)$  values. The famous explosive values like  $a=61, 109, 181, 397, 409, 421, 661, 673, 769, 937$  are all primes. Now if a prime happens to fall in one of the above categories where  $a$  can be regularly defined in terms of a perfect square then we get get pedestrian values despite it being a prime (e.g.  $a=47 \Rightarrow k^2-2$  category or  $a=101 \Rightarrow k^2+1$  category). However, double their value despite not being a prime has an explosive value if it escapes the above categories; e.g. for  $a=94=47 \times 2$ ,  $(x=2143295, y=221064)$  and for  $a= 202 = 101 \times 2$ ,  $(x=19731763, y=1388322)$ . Similarly, when take the case of 17 we see that  $17 \Rightarrow k^2+1$ ;  $17 \times 2 =34 \Rightarrow k^2-2$ ;  $17 \times 3 =51 \Rightarrow k^2+2$ ;  $17 \times 4 =68 \Rightarrow 4k^2+4$ . Thus, the prime 17 is suppressed over 4 successive multiples before it explodes finally at  $a=85$  with  $(x=285769, y=30996)$ . Thus, more generally we see explosions whenever a prime or its multiples escapes the regular categories. Among the primes, a famous set of primes (studied intensely from Fermat, Lagrange, Gauss to this date) that are of the form:  $p=r^2+ns^2$  (typically written as  $p=x^2+ny^2$ , where  $n=1,3$ ) tend to be particularly explosive if they are not suppressed by belonging one of the above regular categories. For example the above listed famous explosive values (e.g 61, 109, 181 etc) can be expressed as both  $p=r^2+s^2$  and  $p=r^2+3s^2$ . Thus by Fermat's theorems these primes are simultaneously:  $p\;mod\;4 \equiv 1$  and  $p \;mod\; 3 \equiv 1$ .
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/02/chakravala2.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 5\quad
```{=latex}
\end{center}
```


We can now understand what we see when we look closely at the log-scale plot of  $x$  against  $a$  (Figure 5). We see "cycloid" like arcs between successive  $a$  which are perfect squares. These peaks of these arcs are more pronounced for such even  $a$ . These arcs arise from the regular categories. The "central" part of these arcs are interrupted by eruptions arising from unsuppressed primes and their multiples. As  $a$  increases the explosions get larger and larger, and as the distance between successive perfect squares increases there are more unsuppressed values which erupt. The distribution of values of  $x$  show an interesting statistical property. Though the median value (lower dotted line in Figure 4, 5) is rather pedestrian the mean value is huge (upper dotted line). This illustrates how the minority explosive values have a much greater influence on the cumulative value of the system --- a pattern often seen in natural systems where a few "hubs" dominate a distribution. This remarkable system made easy by the चक्रवाल illustrates the beauty of this method of old Hindu mathematics. In the past many have said that the Hindus were unlikely to have known of a proof for the चक्रवाल. We do not think so; rather it is likely they had a proof of some kind for why it always works and with it had some grasp of its consequences for the theory of numbers.

In the final part of this note we shall consider another second degree indeterminate equation, the भुजा-कोटि-कर्ण न्याय:  $x^2+y^2=z^2$ . Its integral solutions where  $(x,y,z \neq 0)$  are the famous b-k-k triples (Pythagorean triples in the west), which were studied by the Hindus since the days of the Veda. This likely has a deep history among the आर्य-s and other IE people, which the white indological hot air is keen to ascribe to borrowing from the Mesopotamians (a discerning Hindu will see through the foundations of Mesopotamian boosterism that is common in the Abrahamosphere). In any case, the properties of its solutions are well-know to lay people with even the slightest mathematical interest but we are reiterating them here as another illustration of how even a simple indeterminate equations can produce complex patterns. whose. We encounter its solutions explicitly in the शुल्बसूत्र-s and they continue to be discussed in subsequent Hindu mathematics. For instance the great sage बौधायन says in his शुल्बसूत्र:\
[दीर्घ-चतुर्-अश्रस्य+अक्ष्ण्या-रज्जुः पार्श्व-मानी तिर्यञ्-मानी च यत् पृथग् भूते कुरुतस् तद् उभयं करोति ।]{style="color:#99cc00;"}\
The hypotenuse (अक्ष्ण्या-rajju) i.e. diagonal of a rectangle produces\
\[the area] that is equal to both the areas \[produced] separately by the horizontal and vertical sides.\
[तासां त्रिक-चतुष्कयोर् द्वादशिक-पञ्चिकयोः पञ्चदशिक+अष्टिकयोः सप्तिक-चतुर्विंशिकयोर् द्वादशिक-पञ्चत्रिंशिकयोः पञ्चदशिक-शट्त्रिंशिकयोर् इत्य् एतासु+उपलब्धिः ।]{style="color:#99cc00;"}\
Among the \[sides showing above relationship] are those obtained in this form: (3,4,[5]); (5,12,[13]); (8,15,[17]); (7,24,25); (12,35,[37]); (15,36,[39]).

The first of the triples is the most primitive of triples and sets the properties for all the other solutions: 1)  $xy\; mod \; 12 \equiv 0$ : the product of the two legs of the triple is always divisible by 12. 2)  $xyz \; mod \; 60 \equiv 0$ : the product of all three of them is always divisible by 60. Now of the forms listed in the सूत्र, the first 5 are illustrations of primitives i.e. are integers with greatest common divisor 1 (i.e. they are coprime). The 6th is merely a illustration of how further trivial triples can be obtained by merely scaling a primitive in this case  $(5,12,13) \times 3$ . However, triples of सूत्र are likely chosen to illustrate another point:

 $$z^2-y^2=(z-y)(z+y)=x^2$$ 

Three of them are of the form  $z-y=1$ , two are  $z-y=2$  and the odd one out is  $z-y=3$ . Since the adhvaryu uses difference of squares formula  $z^2-y^2=(z-y)(z+y)$  in the layout of the citi (3 sutra-s later in बौधायन) this was likely used to obtain the illustrated triples by setting  $(z-y)=1,2,3$ . With  $z-y=1$  we get  $x^2=z+y$  which easily yields the most primitive triple  $(5+4=3^2)$  and further  $(12+13=5^2), (24+25=7^2)$  etc.

More generally all primitive triples can be obtained using the formula in the complex plane:\
 $x+iy=(m+in)^2$  where  $(m,n)$  are coprime and of opposite parity (i.e. one is even and the other odd).\
Then the hypotenuse is  $z=Mod\left(\left(m+in\right)^2\right)=m^2+n^2$ . Thus one can see that in the triples  $z$  and one of the legs would be odd and the other even. If we compute the primitive triples and plot them on the xy plane (Figure 6) we see that they present a complex pattern.

![triples_primitive](https://manasataramgini.files.wordpress.com/2017/02/triples_primitive.png){width="75%"}Figure 6

There are two aspects to the pattern: A relatively trivial aspect is the arrangement of the points on parabolic arcs which stems from the parabolic form of the generative formulae. Indeed, if one plots the points as  $(x,y,z)$  in 3D, one can see that they lie on the parabolic sections cutting the surface of the generative cone of the conic (Figure 7).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/02/bkk_cone.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 7\quad
```{=latex}
\end{center}
```


The intricacy of their actual pattern arises from the interplay of this more obvious feature with the distribution of coprimes with opposite parity.  One specific aspect of this arrangement is the presence of certain  $(x,y)$  pairs that differ by 1, e.g. (3,4), i.e. triples with legs differing by 1. Rather interestingly these pairs are related to a specific case of the above considered indeterminate equations:  $x^2=8y^2+1$ . The successive solutions of this equation are shown in the below table:

![x2_8y2_1](https://manasataramgini.files.wordpress.com/2017/02/x2_8y2_1.jpg){width="75%"}

From the  $y_n$  as above values (i.e. the y column) we get:

 $$p_n=y_n+y_{n+1}\\ a_n=\left \lfloor\dfrac{p_n}{2} \right \rfloor \\ b_n=p_n-a_n\\ c_n=y_{n+1}-y_n$$ 

Now  $(a_n,b_n,c_n)$  are the triples whose legs differ by 1

A further striking relationship captures a more general aspect of the distribution of triples: We first order all triples by increasing magnitude of their hypotenuse.  $z_i$  will be the ith hypotenuse and  $N(z)$  will be the number of triples with  $z< z_i$ . Then we see that:

 $$\lim \limits_{z_i \to \infty} \dfrac{N(z)}{z_i} =\dfrac{1}{2\pi}$$ 

This convergence is illustrated in Figure 8.  Thus, remarkably, as is typical of mathematics, we start with the elementary geometry of right triangles which are part of the rectilinear figures of the vaidika ritual, pass on to conics, intersect with the object of the चक्रवाल, and end up with a nice appearance of that quintessential number  $\pi$ .  Thus, one appreciates Gauss's famous statement: "ἀεὶ ὁ θεὸς ἀριθμετἰζεῖ (the god always arithmeticizes)".

![triples_2pi_convergence](https://manasataramgini.files.wordpress.com/2017/02/triples_2pi_convergence.png){width="75%"}Figure 8

