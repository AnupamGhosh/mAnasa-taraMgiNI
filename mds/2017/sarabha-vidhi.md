
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [शरभ vidhi](https://manasataramgini.wordpress.com/2017/04/09/sarabha-vidhi/){rel="bookmark"} {#शरभ-vidhi .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 9, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/04/09/sarabha-vidhi/ "2:30 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The account of this rite continues from the [prefatory narrative](https://manasataramgini.wordpress.com/2017/04/08/a-prefatory-narrative/).

Lootika: "There are several ways in which Rudra is worshipped as शरभ conceived as the great dinosaur. We shall follow the way which is appropriate for us ब्राह्मण-s who observe the श्रुति as our supreme religious authority rather than the आगम-s, even though we accept the latter as specific authorities. The purely आगमिक methods are practiced by शैव-s, शिवभक्त-s and लिङ्गिन्-s."

![sharabha2](https://manasataramgini.files.wordpress.com/2017/04/sharabha2.jpg){width="75%"}Somakhya then rolled out a canvas and showed Jhilleeka and Prachetas a picture of शरभ: "One shall accordingly visualizing शरभेशान as having five dinosaur-like heads each with with 3 eyes and a heavy, sharp beak. He has 12 arms two of which will be wings. I shall talk of the weapons and other objects held in the arms later. Enclosed in his two wings are the goddesses प्रतिक्रिया-शूलिनी and प्रत्यङ्गिरा. In his heart we visualize the fierce Bhairava of the दक्षिणस्रोतस्, Svacchanda, associated with the pada ghora-घोरत्रेभ्यः of the बहुरूपिन्. In his belly we visualize the seven-flamed Agni वडवानल. In this two dinosaur-like feet we visualize ज्वरेश्वर and the ferocious lion-headed Haribhadra, even as they were emitted by Rudra to slay दक्ष.

One begins by invoking Rudra the lord of animals thus:\
[रुद्रः पशूनाम् अध्यक्षः]{style="color:#99cc00;"}\
[स मावत्व् अस्मिन् ब्रह्मण्य्]{style="color:#99cc00;"}\
[अस्मिन् कर्मण्य् अस्यां पुरोधायाम्]{style="color:#99cc00;"}\
[अस्यां देवहूत्याम् अस्याम् आकूत्याम्]{style="color:#99cc00;"}\
[अस्याम् आशिषि स्वाहा ॥]{style="color:#99cc00;"}

Make an offering in the औपासन fire at the स्वाहा.

Having visualized शरभ as stated earlier you shall first perform the worship the five faces conjoining the Brahma-mantra-s thus:

[ॐ हौं तत्पुरुषाय पक्षिराजाय नमः । तत्पुरुषय विद्महे...॥]{style="color:#99cc00;"}\
[ॐ हौं वामदेवाय गौर्यै गान्धार्यै गरुड्यै नमः । वामदेवाय नमो... ॥"]{style="color:#99cc00;"}\
[ॐ हौं सद्योजाताय शाल्वाय नमः । सद्योजातं प्रपद्यामि... ॥]{style="color:#99cc00;"}\
[ॐ हौं अघोराय आकाशभैरवाय नमः । अघोरेभ्यो'थ... ॥]{style="color:#99cc00;"}\
[ॐ हौं ईशानाय शरभेशानाय नमः । ईशानः सर्व-भूतानाम्... ॥]{style="color:#99cc00;"}

Then you shall practice the japa of the मूलमन्त्र:\
[वासुदेव ऋषिः । त्रिष्टुभ् छन्दः । कालाग्निरुद्रः शरभेशानो देवता ॥]{style="color:#99cc00;"}\
[खं बीजम् । खं शक्तिः । खं कीलकम् ॥]{style="color:#99cc00;"}\
[खं शरभेशानाय खं शाल्वाय खं पक्षिराजाय हुम् फण् नमः ॥]{style="color:#99cc00;"}

Then you pacifies the deity with:\
[ॐ शरभं तर्पयामि ।]{style="color:#99cc00;"}\
[ॐ शाल्वं तर्पयामि ।]{style="color:#99cc00;"}\
[ॐ पक्षिराजं तर्पयामि ॥]{style="color:#99cc00;"}

Thereafter one worships the अङ्ग-देवता with their mantra-s. Lootika will give you those mantra-s of the शक्ति-s. Hearing it from her mouth you stand the chance of instantaneous success."

Lootika: First one should worship प्रतिक्रिया-शूलिनी with the mighty वैरोचनी-ऋक्: [ताम् अग्निवर्नां तपसा...]{style="color:#99cc00;"} closing one's eyes. If the image of a दुर्गा riding a lion holding a trident in her hand appears while uttering the mantra then it is a sign of success. She has six-arms and it colored like a flame with a bluish tinge. You pacify her with the mantra:\
[ॐ महेशानीं महाशरभशूलिनीं तर्पयामि ॥]{style="color:#99cc00;"}

Then you worship अथर्वणभद्रकाली-प्रत्यङ्गिरा with the mantra:\
[ॐ ह्रीं प्रत्यङ्गिरे धाम धाम ज्योतिर् ज्योतिर् भूर्-भुवस्-सुवर्-महर्-वृधत् करद् रुहन् महत् तच् छम् ॐ लक्ष्म्यै नमो वः ॥]{style="color:#99cc00;"} She is fourteen-armed, black-colored, with dense free-flowing hair, wearing bright osseous ornaments and evoking erotic sentiments. She is लक्ष्मी because she is Siddhilakṣmī. The special व्याहृति-s of the mantra are from the Gopatha-ब्राह्मण of the veda of our ancestors the भृगु-s and the अञ्गिरस-es. If when uttering the mantra with closed eyes she appears before you with a hallow like an eclipsed moon around her head then it is a sign that you might be on the path of success. You must see her upraised sword. If erotic sentiments arise in you at that point it is a good sign. But you must not get lost in them. Instead you should take in a breath with the [ॐ ह्रीं]{style="color:#99cc00;"} and retaining it even as you slowly utter the rest of the mantra till the व्याहृति-s you must locate your मूलाधार-cakra and perform the जालन्धर and मूल-bandha. You pacify her with the mantra:\
[ॐ प्रत्यङ्गिरां विश्वलक्श्मीं सोमगुप्तां महाशरभीं तर्पयामी ॥]{style="color:#99cc00;"}

Now Somakhya would impart to you the remaining male देवता-s."

Somakhya: "Having worshiped the शक्ति-s you shall invoke in the heart of शरभेशान the fierce Bhairava with the following mantra:\
[ऐं क्षां क्षीं क्षूं क्षः क्ष्म्ल्व्र्यूं स्वच्छन्दभैरवाय स्फुरतात्मने हुं फण् नमः ॥]{style="color:#99cc00;"}\
He appears as five-headed and ten-armed riding a white bull.

Then you worships the fierce Agni वडवानल with his seven tongues which correspond to the heptagon of the शरभ-yantra with this mantra:\
[ॐ रं हिरण्यायै कनकायै रक्तायै कृष्णायै सुप्रभायै अतिरक्तायै बहुरूपायै नमः कृणुश्व पाजः प्रसितिं...॥]{style="color:#99cc00;"}\
The terminal of the mantra is the रक्षोहा-ऋक् of वामदेव Gautama.

Then you worship ज्वरेश or the ज्वरास्त्र of triadic form, which Rudra hurled at कृष्ण देवकीपुत्र with this mantra:\
[ॐ नमो भगवते रुद्राय ज्वररूपाय त्रिकायरूपाय वो नमः ॥]{style="color:#99cc00;"}

You then worship Haribhadra accompanied by a hundred Rudra-s of blue-hued necks as has been described in the श्रुति with the mantra: "[निलग्रीवाः शितिकण्ठाः...]{style="color:#99cc00;"}". He is worshiped by the mantra combining the utterance from the Skandam and Acintya-विश्व-सादाख्यम्:\
[ॐ जय जय रुद्र महारौद्र भद्रावतार ॐ श्रीं ह्रीं मृगेन्द्राय हरिभद्राय नमः । ॐ जूं सः ॐ ॥]{style="color:#99cc00;"}\
Now Lootika shall impart to you the worship of the zoocephalous आवरण-योगिनी-s."

Lootika: "First in the four directions having completed the worship of शरभेशान and his अङ्ग-s you shall worship the following योगिनी-s thus starting east in प्रदक्षिण order:\
[ॐ गजाननां पूजयामि तर्पयामि नमः ।]{style="color:#99cc00;"} elephant-headed.\
[ॐ सिंहमुखीं पूजयामि तर्पयामि नमः ।]{style="color:#99cc00;"} lion-headed\
[ॐ गृध्रास्यां पूजयामि तर्पयामि नमः ।]{style="color:#99cc00;"} vulture-headed\
[ॐ काकतुण्डीं पूजयामि तर्पयामि नमः ।]{style="color:#99cc00;"} crow-headed.

Then you shall worship the mighty मण्डल-योगिनी with the following mantra:\
[नमामि महायोगिनीं सर्वशत्रुविदारणीं ।]{style="color:#99cc00;"}\
[अष्टवक्त्रीं कोटराक्षीं वक्रां विकटलोचनाम् ।]{style="color:#99cc00;"}\
[उष्ट्रग्रीवां हयग्रीवां वाराहीं शरभाननाम् ।]{style="color:#99cc00;"}\
[उलूकीं च शिवारावां मयूरीं विकटाननाम् ॥]{style="color:#99cc00;"}

She bears the following 8 heads: 1) camel, 2) horse, 3) sow, 4) dinosaur, 5) owl, 6) jackal, 7) peacock, 8) lion. She is the guardian of the शरभ-मण्डल.

Then you pacify her:\
[अष्टवक्त्रीं तर्पयामि पुजयामि नमः ।]{style="color:#99cc00;"}

Now Somakhya would take you through the concluding pacifications."

Somakhya: "The concluding part of the worship is conducted with the mantra of the भृगु-s of yore:\
[तुभ्यम् आरण्याः पशवो]{style="color:#99cc00;"}\
[मृत्या वने हितास् तुभ्यं]{style="color:#99cc00;"}\
[वयांसि शकुनाः पतत्रिणः ।]{style="color:#99cc00;"}\
[तव यक्षं पशुपते 'प्स्व् अन्तस्]{style="color:#99cc00;"}\
[तुभ्यं क्षरन्ति दिव्या आपो वृधे ॥]{style="color:#99cc00;"}

You tie a रक्ष with the mantra:\
[शुने क्रोष्ट्रे मा शरीराणि]{style="color:#99cc00;"}\
[कर्तम् अरिक्लवेभ्यो गृद्ध्रेभ्यो]{style="color:#99cc00;"}\
[ये च कृष्णा अविष्यवः ।]{style="color:#99cc00;"}\
[मक्षिकास् ते पशुपते वयांसि]{style="color:#99cc00;"}\
[ते विघसे मा विदन्त ॥]{style="color:#99cc00;"}

Then worship his weapons:\
[त्रिशुलं तर्पयामि ।]{style="color:#99cc00;"}\
[दण्डं तर्पयामि ।]{style="color:#99cc00;"}\
[पिनाकं तर्पयामि ।]{style="color:#99cc00;"}\
[पाषुपतं तर्पयामि ।]{style="color:#99cc00;"}\
[कुलिशं तर्पयामि ।]{style="color:#99cc00;"}\
[चक्रं तर्पयामि ।]{style="color:#99cc00;"}\
[मुसलं तर्पयामि ।]{style="color:#99cc00;"}\
[खड्गं तर्पयामि ।]{style="color:#99cc00;"}\
[खट्वाङ्गं तर्पयामि ।]{style="color:#99cc00;"}\
[परशुं तर्पयामि ॥]{style="color:#99cc00;"}

i.e. the trident, rod, पिनाक bow, पाशुपत missile, thunderbolt, discus, pestle, sword, skull-brand and axe.

Then he concludes by reciting the mantra-s:\
[तस्मै प्राचाया दिशि अन्तर्देशाद् भवम् इष्वासम् अनुष्ठातारम् अकुर्वन् ।...य एवं वेद॥]{style="color:#99cc00;"}

Thus the rite is concluded."


