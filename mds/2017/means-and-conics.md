
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Means and conics](https://manasataramgini.wordpress.com/2017/07/22/means-and-conics/){rel="bookmark"} {#means-and-conics .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 22, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/07/22/means-and-conics/ "9:34 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

By the time one reaches high school one learns that: (i) there are four means that one might find some use of in life (I know there are more though they are hardly used) -- the arithmetic mean which is most widely used; the geometric mean; the harmonic mean; root mean squared (RMS), which most people would have encountered while studying Maxwell's brilliant derivation of the ideal gas velocities from theory. (ii) By then one also might have learned the geometric construction of the arithmetic mean and the geometric mean. The later is merely the midpoint of the length of two collinear segments and the latter arises from the famous geometric mean theorem. But fewer learn of or discover a simple geometric construction that gives all these four means (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/07/means_sized.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 1

The arithmetic and geometric means are obvious in Figure 1. If one does some elementary calculations using parallel-lines, similarity of triangles and भुजा-कोटि-कर्ण-न्याय one realizes why the other two lengths are respectively the harmonic mean and RMS. Moreover, it gives an immediate proof of the famous statement one might have also studied in high-school:\
 $HM \le GM \le AM \le RMS$ latex\
One also then sees that:

 $$GM(HM,AM)=GM$  and  $RMS(GM,RMS)=AM$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/07/means_open_conics.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 2

Now perform the following construction (Figure 2):

 1.  takes  $a=1$  as specified by the point A.  $b=x$  varies as point B moves along the horizontal line.

 2.  Construct the means of  $(a,b)=(1,x)$  as shown in Figure 1.

 3.  Draw a perpendicular at point B to  $\overleftrightarrow{AB}$ . Mark off segments of length equal to the means as constructed in the above step (e.g. point F, I).  $\overline{BF}$  is the segment corresponding to the harmonic mean and  $\overline{BI}$  is that corresponding to the RMS.

 4.  Determine the locus of these points as point B moves.

One notices that the HM and the RMS give two distinct hyperbolas (Figure 2). The HM hyperbola has asymptotes  $y=2, \; x=-1$ . The RMS hyperbola has asymptotes  $y=-\sqrt{2}x, \; y=\sqrt{2}x$ . The arithmetic mean by the same construction specifies a straight line and the geometric mean specifies a parabola which corresponds to the [lost construction of Apollonius](https://manasataramgini.wordpress.com/2016/09/25/the-apollonian-parabola/). All four conics are tangent to each other a  $(1,1)$  and illustrate how the four means diverge from each other.

