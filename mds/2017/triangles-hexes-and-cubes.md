
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Triangles, Hexes and Cubes](https://manasataramgini.wordpress.com/2017/10/23/triangles-hexes-and-cubes/){rel="bookmark"} {#triangles-hexes-and-cubes .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 23, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/10/23/triangles-hexes-and-cubes/ "5:02 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One philosophical question which we have often ponder about is: Are numbers "real"? One way to approach this question is via figurate numbers, where numbers directly manifest as very tangible geometry. This idea has deep roots in our tradition: as we have noted before, [the square numbers and their link to odd numbers](https://manasataramgini.wordpress.com/2016/10/23/syllable-number-and-rules-in-the-ideal-realm/) is directly represented in the square vedi-s of the श्रौत ritual. Another basic type of figurate numbers, the triangular numbers, are presented in the [early yavana philosophical tradition of the Pythagoreans and play an important role in Platonic thought](https://manasataramgini.wordpress.com/2005/09/26/hayastanika-samkhya-plato-etc/). Thus, we suspect that contemplation on figurate numbers played an important role in the ancestral philosophical tradition on the आर्य-s and the yavana-s. Here we will illustrate some well-known and basic features of figurate numbers to show how geometric conceptualization of them allows one to easily understand and derive certain properties of theirs.

![triangular_numbers](https://manasataramgini.files.wordpress.com/2017/10/triangular_numbers.png){width="75%"}Figure 1

Figure 1 depicts triangular numbers  $T_n$ . We see that they simply emerge from appropriately arranging rows of counters amounting to successive Natural numbers i.e. the set  $\mathbb{N}$ . Thus, the geometric figure, the triangle, is directly implied by the existence  $\mathbb{N}$ . From Figure 1 we also see that the nth  $T_n$  itself represents the sum of natural numbers from 1:n. This is the famous sum of the basic arithmetic series which is first indicated in [Vedic tradition as derived by the ancient sage शाकपूणि](https://manasataramgini.wordpress.com/2006/03/11/jatavedase-and-shredhi-vyavahara/). It was subsequently expounded by आर्यभट-I in his आर्यभटियम्. The medieval scientist नारायण पण्डित gives an old सूत्र for it thus:\
[सैक-पद-घ्न-पदार्धं संकलितम् ।]{style="color:#0000ff;"}\
sa+eka+pada  $\rightarrow (n+1)$ ; ghna: multiply; पदार्धं  $\rightarrow \tfrac{n}{2}$ ; yields संकलितम् (sum of series). Thus,

 $$T_n=\dfrac{n(n+1)}{2} \rightarrow 1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105, 120, 136, 153, 171, 190, 210 ...$$ 

![sum_of_series](https://manasataramgini.files.wordpress.com/2017/10/sum_of_series.jpg){width="75%"}Figure 2. This demonstration is given by the great scientist-mathematician नीलकण्ठ सोमयाजिन् and others like the मुनीश्वर विश्वरूप.

By looking at figure 2 the proof for the above formula becomes self-evident. By looking at figure 1 and working our way along successive numbers we notice that we can reach any natural number by means of a sum of at most 3 triangular numbers, like the 3 steps of विष्णु spanning the universe. This result was formally proved by Carl Gauss at age 19 following up on his auto-discovery of the sum of the simple arithmetic series as a child. Related to this, is the fractal structure that emerges when we represent triangular numbers in binary (base 2) following a device of Stephen Wolfram (Figure 3).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/10/triangular_binary.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 3\quad The triangular numbers up to  $T_{511}=130816$ 
```{=latex}
\end{center}
```


We had earlier seen how the problem of determining which triangular number is also a square number led to the famous indeterminate equation, which was generally tackled by the old Hindus using the चक्रवाल algorithm and how it yields approximations for  $\sqrt{2}$ . Other relationships between triangular numbers  $T_n$  and square numbers  $S_m$  can be easily found using the above construction of नीलकण्ठ (Figure 4).

![triangle_square](https://manasataramgini.files.wordpress.com/2017/10/triangle_square.png){width="75%"}Figure 4

From Figure 4 it is self-evident that,

 $$T_n+T_{n+1}=S_{n+1}$$ 

 $$८ट्न्+1=S_{2n+1}$$ 

Triangular numbers and related figurate numbers also relate to arithmetic problems which captured the attention of the Hindu mathematicians over the ages. For example, if we pile triangles formed by the triangular numbers one atop the next then we get a 3D figurate number, the tetrahedral number (figure 5). The tetrahedron thus formed had [significance in Platonic thought](https://manasataramgini.wordpress.com/2005/09/26/hayastanika-samkhya-plato-etc/).

![tetrahedral_numbers](https://manasataramgini.files.wordpress.com/2017/10/tetrahedral_numbers.png){width="75%"}Figure 5

From the above figure it is apparent that the tetrahedral numbers ( $Te_n$ ) are nth sums of triangular numbers. आर्यभट-I gives the formula for the nth sum of triangular numbers by the सूत्र:\
[षड्भक्तः स चितिघनः सैकपदघनो विमूलः ।]{style="color:#0000ff;"}\
sa चितिघनः: Sum of sum of natural numbers is called citighana; षड्भक्तः  $\rightarrow \div 6$ ; sa+eka+pada+घनः  $\rightarrow (n+1)^3$ ; विमूलः  $\rightarrow -(n+1)$  (literally subtract cube root of previous term). Thus we have,

 $$\displaystyle \sum_{j=1}\^n T_n=Te_n=\dfrac{(n+1)^3-(n+1)}{6}=\dfrac{n(n+1)(n+2)}{6}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/10/triangular_number_sum.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 6\quad Sum of triangular number demonstrated by नीलकण्ठ सोमयाजिन्.
```{=latex}
\end{center}
```


नीलकण्ठ सोमयाजिन् illustrates the geometric proof for this in his commentary on आर्यभट (Figure 6). The term citighana (literally stacked solid) implies that आर्यभट had 3D sense of it similar to constructions with bricks performed during the piling of citi-s or ritual altars in the श्रौत ritual. The earliest Hindu temples, known as caitya also stem from the same root implying the piling of bricks. The relationship of these terms to the piling of citi-s was first pointed out by TA Sarasvati Amma but mostly ignored by others.

The ghanacitighana laid out by आर्यभट-I and subsequently commented upon by many mathematicians is computation of the nth sum of cubes of natural numbers. आर्यभट's सूत्र is:\
[चिति-वर्गो घनचितिघनः ।]{style="color:#0000ff;"}\
citi  $\rightarrow \sum_{j=1}\^n j$ ; वर्गः: squared; yields sum of cubes of natural numbers (घनचितिघनः). We can use triangular numbers to easily derive this formula (Figure 7)

![cube_cutting](https://manasataramgini.files.wordpress.com/2017/10/cube_cutting.png){width="75%"}Figure 7. The sum of cubes of natural numbers.

We construct a square slab of unit cubes with each side of length equal to a triangular number. Thus, the total number of cubes in the slab is  $T_n^2$ . We then dissect the slab such that we separate the pieces shown in different colors first. Then we dissect each color piece further along the grey slicers. These divide the pieces into either square slabs or half-square slabs of unit cubes. We then assemble these slabs into cubes of the same color. This demonstrates that,

 $$\displaystyle \sum_{j=1}\^n j^3 =T_n^2=\dfrac{n^2(n+1)^2}{4}$$ 

Thus, the nth sum of cube numbers is the square of the nth triangular number.

We may next consider the Hex numbers  $H_n$  which are depicted in Figure 8. Such a figure plays an important role in the positioning of deities in मण्डल-s in the आगमिक tradition. We also encounter it as a packing principle in nature.

![Hex_numbers](https://manasataramgini.files.wordpress.com/2017/10/hex_numbers.png){width="75%"}Figure 8

We see that they are centered hexagons and based on the figure we can infer their general formula and relationship to triangular numbers:

 $$H_n=3n^2-3n+1$ ;  $n= 1,2,3,4...$$ 

 $$H_n=६ट्क्+1$ ;  $k=0,1,2,3...$$ 

The latter relationship can be seen by dissecting the hexagon into six triangular sectors of dots making the triangular number  $T_k$  after leaving out the central dot. Further, we see that a hex number is always of the form  $3m+1$  where  $m$  is 0 or an integer. The hex numbers are the 3D figurate equivalent odd numbers: [In the old श्रौत altar construction successive odd numbers of bricks](https://manasataramgini.wordpress.com/2016/10/23/syllable-number-and-rules-in-the-ideal-realm/) form right-angled "shells" that nest into the next such shell to constitute larger and larger squares. We may likewise ask if this can be extended to 3D, such that we have shells which nest into each other to constitute a cube (Figure 9).

![Hex_cubes](https://manasataramgini.files.wordpress.com/2017/10/hex_cubes.png){width="75%"}Figure 9

From figure 9 we observe that based on the geometry of the hex number we can arrange in 3D to constitute such a cubic shell. By nesting successive shells of hex numbers we constitute a cube. Thus we get,

 $$\displaystyle \sum_{j=1}\^n H_j =n^3$$ 

This tells us that just as the difference between successive square numbers is an odd number the difference between successive cube numbers is a hex number.

In conclusion, the study of figurate numbers might be seen as having their origin in old Greco-Aryan religious tradition. In Greece they took an important place in philosophy whereas in India, beyond the ritual tradition, their more general study assumed a form somewhat closer to their modern counterparts. While Hindus were generally less-inclined towards geometry than their Greek counterparts, this is one area where the Hindus developed a unique unbroken tradition of "geometric algebra" that clearly stretches from the Vedic tradition via आर्यभट-I to the later savants. That older root of it is evident from the fact that even in the period of regionalization, the tradition was similarly but independently continued in disparate parts of the Hindu nation. We see demonstrations of geometric algebra in the south in the famous school of नीलकण्ठ सोमयाजिन् and his successors like [चित्रभानु](https://manasataramgini.wordpress.com/2017/09/22/citrabhanus-cubes/) and शंकर वारिआर्; in Maharashtra by गणेश दैवज्ञ and his clansmen; in North India even under Islamic tyranny by the रङ्गनाथ-मुनीश्वर school.

The study of figurate numbers continues into modern mathematics. We noted above the discovery of Gauss on triangular numbers. Before him the Leonhard Euler generalized the concept of the root to figurate numbers. The square root of a number  $y=\sqrt{x}$  is a generalization coming from the  $n$  as the root of the square number  $n^2$ . Euler showed that similarly  $y$ , a general triangular root of  $x$ , can be defined as the solution of the quadratic equation  $y^2+y-2x=0$ ,

 $$y=\dfrac{\sqrt{8x+1}-1}{2}$$ 

We get  $y$  to be an integer only when  $x=T_n$ ; then  $y=n$ . Thus, only if  $\sqrt{x}$  and its triangular root are simultaneously integers it is both a square and a triangular number. In the below table we show the first few numbers which are both triangular and square along with their square root and triangular root.

Table 1.

     sqrt   troot   Sn/Tm

  - ----  ------  ------
        1       1       1
        6       8      36
       35      49    1225
      204     288   41616

From these numbers we can see that we can compose simultaneously square and triangular numbers by the two seeded series:

 $$S\lbrack n\rbrack =34\cdot S\lbrack n-1\rbrack -S\lbrack n-2\rbrack +2$ ; where  $S\lbrack 0\rbrack =0, S\lbrack 1\rbrack =1$$ 

Similarly, the solution  $y$  for the equation  $3y^2-3y+1-x=0$  provides the hex root of a number,

 $$\dfrac{3+\sqrt{9-12(1-x)}}{6}$$ 

Table 2. First few numbers which are both triangular and hex numbers with their triangular and hex roots.

    troot   hroot   Tn/Hm

  - -----  ------  ------
         1       1       1
        13       6      91
       133      55    8911

Table 3. First few numbers which are both square and hex numbers and their square and hex roots.

     sqrt   hroot   Sn/Hm

  - ----  ------  ------
        1       1       1
       13       8     169
      181     105   32761

Whereas a square number is never prime and  $T_2=3$  is the only prime triangular number, the general formula for hex numbers is a fairly rich prime-generating\
quadratic. For instance there are 58  $H_n| n< 10000$  of which 28 are prime:  $pf=0.4827586$ . For comparison we draw 58 numbers 10000 taking into account the following: 1) hex numbers are always odd; 2) they are not evenly distributed: the first 100 has 6 hex number while the next 100 has only 2. Thus, we make the clustering pattern of our draws identical to that of hex numbers in windows of 100. Doing a simulation with these constraints we get a probability of the prime fraction in draws of 58 numbers being  $pf=0.4827586$  or higher to be of the order of  $\approx 0.0002$  (Figure 10).
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2017/10/hex_primes.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 10\quad
```{=latex}
\end{center}
```


 

