
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Euler's squares](https://manasataramgini.wordpress.com/2017/04/20/eulers-squares/){rel="bookmark"} {#eulers-squares .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 20, 2017]{.entry-date}](https://manasataramgini.wordpress.com/2017/04/20/eulers-squares/ "6:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

On account of our fascination with the geometry of origami (albeit not well-endowed in mathematical capacity) we discovered for ourselves shortly after our father had taught us trigonometry that,

 $$\arctan(1)+\arctan(2)+\arctan(3)=\pi$$ 

We had earlier shown the [origami proof for that](https://manasataramgini.wordpress.com/2016/12/26/some-reminiscences-of-our-study-of-chaotic-maps-2/). But it was only a little later while drifting away from one of those trigonometric identities that you routinely faced in those annoying college exams we stumbled upon a beautiful relationship that was apparently first discovered by the great Leonhard Euler. This is a relationship parallel to the above one:

 $$\arctan\left(\frac{1}{2}\right)+\arctan\left(\frac{1}{3}\right)=\arctan\left(1\right)=\frac{\pi}{4}$$ 

The proof for this, like the origami proof for the above, can be achieved from a self-evident construction of Euler --- what the Hindus of yore would have called an upapatti or mathematicians today term "wordless" proof. It is illustrated below but I add several words for the benefit of the non-geometrically oriented reader.

![Euler_squares](https://manasataramgini.files.wordpress.com/2017/04/euler_squares.png){width="75%"}


 1.  Draw square ABCD and triplicate it so that the three squares share a side.

 2.  Draw diagonals AC and CG of first two squares and use them to draw square ACGF and duplicate it.

 3.  Draw  $\overline{AE}$ : from the construction it is apparent that  $\angle GAE=\arctan\left(\frac{1}{3}\right)$ 

 4.  From the construction it is clear that  $\angle EAC=\arctan\left(\frac{1}{2}\right)$ 

 5.  We thus see:  $\angle GAE+ \angle EAC = \angle BAC= \frac{\pi}{4}= \arctan(1)=\arctan\left(\frac{1}{2}\right)+\arctan\left(\frac{1}{3}\right)$ 

This relationship is one of a class of strange trigonometric relationships that interestingly bring in the meru-श्रेधी (called in western literature as Fibonacci sequence):

 $$\arctan\left(\frac{1}{M_{2n}}\right)=\arctan\left(\frac{1}{M_{2n+1}}\right)+\arctan\left(\frac{1}{M_{2n+2}}\right)$$ 

 $M=1,1,2,3,5,8,13,21...$ , the meru-श्रेधी; thus for  $n=1$ , we get  $M_2=1; M_3=2; M_4=3$ .\
This leads us to a formula for  $\pi$  based on the odd terms of meru-श्रेधी starting from  $M_3$ :

 $$\pi=4\displaystyle \sum_{n=1}^\infty \arctan \left(\frac{1}{M_{2n+1}}\right)$$ 

Shown below is the convergence of the above series to  $\pi$ : We reach an accuracy of 6 decimal places for  $n=18$ .

![pi_meru](https://manasataramgini.files.wordpress.com/2017/04/pi_meru.png){width="75%"}

