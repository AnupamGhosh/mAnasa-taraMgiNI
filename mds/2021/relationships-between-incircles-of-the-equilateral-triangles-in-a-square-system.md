
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Relationships between incircles of the "equilateral triangles in a square" system](https://manasataramgini.wordpress.com/2021/12/20/relationships-between-incircles-of-the-equilateral-triangles-in-a-square-system/){rel="bookmark"} {#relationships-between-incircles-of-the-equilateral-triangles-in-a-square-system .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 20, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/12/20/relationships-between-incircles-of-the-equilateral-triangles-in-a-square-system/ "1:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note relates to geometric relationships that may be likened to the Japanese temple-tablet problems. The inspiration for discovering and exploring it came from an origami construction presented by the pioneer in that field, Sundara Rao of कुम्भघोण, in the late 1800s. Given a square piece of paper, how does one fold it into an equilateral triangle? The construction which Rao gave was that of an equilateral triangle with sides equal to that of the starting square and sharing one side with it (Figure 1). Based on Figure 1, it is easy to see how that might be achieved. When we first folded this in our youth, we realized that it is not the largest equilateral triangle that can be placed inside a square. However, examining the origami construction for the above, we realized that it also gave us an easy origami construction for the largest equilateral triangle that can be inscribed in a square (the blue triangle in Figure 1). That construction should be self-evident once the first equilateral triangle sharing a side with the square is in place). These two equilateral triangles and the square result in a configuration of 7 other triangles (Figure 1).

The below study concerns the relationship between the incircles of these 7 triangles  $(c_1, c_2, \cdots, c_7)$  and two additional incircles, namely that of the starting square  $c_s$  and the Raoian triangle constructed from it  $c_t$ . Their radii, which the relationships connect, will be respectively,  $r_1, r_2, \cdots, r_7$  and  $r_s, r_t$ . We outline the proof rather than present all the tedious trigonometry and radical algebra. If you like to do that with paper and pencil and are good at that, you can try the same. However, we cut through the tedium of the at times complicated algebra using a combination of recognizing key patterns and the open-source computer algebra system from GeoGebra that seamlessly interfaces with its geometric constructions. Nevertheless, we will show a few obvious ones to lay the background.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/12/equilateral_in_square.png){width="75%"}
```{=latex}
\end{center}
```



**Figure 1.**

• First, with is straightforward trigonometry to show that

 $$\dfrac{r_s}{r_t}=\sqrt{3}$$ 

• The proportion of the radii of the incircles is equal to the proportion of the equivalent sides of their triangles.

• Let the side of the largest inscribed equilateral triangle (blue) be  $t_1$  and the smaller one (sharing the side with the square) be  $t_2$ . We can use the half-angle formula to show that  $\cos\left(15^\circ\right) = \tfrac{\sqrt{2}(1+\sqrt{3})}{4}$ . In turn, that means  $\tfrac{t_1}{t_2}= \tfrac{1}{\cos\left(15^\circ\right)} = \sqrt{2}\left(\sqrt{3}-1\right)$ 

• We can see that triangles with incircles  $c_2$ ,  $c_4$ ,  $c_5$  and  $c_6$  are similar  $45^\circ-60^\circ-75^\circ$  triangles. Using the Sine Law we can show that the sides of such a triangle are in the ratio  $1: \sqrt{\tfrac{3}{2}}: \tfrac{1+\sqrt{3}}{2}$ . With this and the above we can get the proportionality of these triangles. For example, we can show that the sides of the triangles with  $c_2$  and  $c_4$  are in the proportion:  $\tfrac{\sqrt{2}\left(1+\sqrt{3}\right)}{2}$ . :

 $$\dfrac{r_2}{r_4}=\dfrac{\sqrt{2}\left(1+\sqrt{3}\right)}{2}$$ 

 $$\dfrac{r_6}{r_2}= \sqrt{2}\left(\sqrt{3}-1\right)$$ 

 $$\dfrac{r_6}{r_4}=2$$ 

 $$\dfrac{r_6}{r_5}=\sqrt{2}$$ 

 $$\dfrac{r_5}{r_4}=\sqrt{2}$$ 

 $$\dfrac{r_5}{r_2}=\sqrt{3}-1$$ 

• Next, we use Brahmagupta's formula for the incircle of a triangle,  $r=\tfrac{A(\triangle)}{s}$ , where  $s$  is the semiperimeter (half the perimeter) of the triangle. From the proportions of the triangle sides, we can show that the ratio of the areas and the perimeters of the triangles whose incircles are  $c_3$  and  $c_4$  are both  $1+\tfrac{2\sqrt{3}}{3}$ 

 $$\therefore r_3=r_4$$ 

• Thus, from above we have:  $r_6=2r_4$  and  $r_5^2 = r_4 r_6 = r_3 r_6$ , i.e., a geometric mean relationship.

• Similarly, we use Brahmagupta's formula to obtain the incircle radii of  $c_1$  and  $c_7$ . With that, we get the following relationships:

 $$\dfrac{r_1}{r_2}=1+\sqrt{2}-\sqrt{3}$$ 

 $$\dfrac{r_1}{r_4}= \dfrac{r_1}{r_3} = 1-\sqrt{2}+\sqrt{3}$$ 

 $\therefore r_1 = \dfrac{2 r_2 r_3}{r_2+r_3} = \dfrac{2 r_2 r_4}{r_2+r_4}$ , i.e., a harmonic mean relationship.

 $$\dfrac{r_6}{r_7}=2-\dfrac{\sqrt{2}(\sqrt{3}-1)}{2}$$ 

 $$\dfrac{r_6}{r_2}= \sqrt{2}\left(\sqrt{3}-1\right)$$ 

 $\therefore r_6 = \dfrac{4 r_2 r_7}{2 r_2+r_7}$ , i.e.,  $r_6$  is the harmonic mean of  $(2 r_2, r_7)$ .

• One can also get relationships connecting the radii of all the 7 incircles  $c_1 \cdots c_7$  and also all 9 incircles in the configuration (Figure 1):

 $$r_2 r_5 r_6 - r_3 r_5 r_7= 2 r_1 r_4 r_7$$ 

 $$\dfrac{r_2}{r_3} \dfrac{r_6}{r_7}-2 \dfrac{r_1}{r_5}=\dfrac{r_s}{r_t}-\dfrac{r_5}{r_2}$$ 

Thus, one can see the "2-ness" of the square in the form of  $\sqrt{2}$  and the "3-ness" of the equilateral triangle in the form of  $\sqrt{3}$  pervades the system.

