
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Twin आदित्य-s, twin Rudra-s](https://manasataramgini.wordpress.com/2021/08/15/twin-aditya-s-twin-rudra-s/){rel="bookmark"} {#twin-आदतय-s-twin-rudra-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 15, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/08/15/twin-aditya-s-twin-rudra-s/ "5:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note originated as an intended appendix to the [article on Rudra and the अश्विन्-s](https://manasataramgini.wordpress.com/2020/01/12/the-asvin-s-and-rudra/) we published earlier. The first offshoot from that work, which we published separately, explored the links between [Rudra, विष्णु and the अश्विन्-s in the श्रौत ritual](https://manasataramgini.wordpress.com/2020/05/25/vi%e1%b9%a3%e1%b9%87u-the-marut-s-and-rudra/). We finally found the time to fully write down the intended appendix and present it as a separate note. To rehash, we noted an intimate connection between the primary Rudra-class deity (typically in his manifestation as the great heavenly Asura, the father of the worlds) and the twin deities (the अश्विन्-class) of the ancestral Indo-European religion. This is preserved in multiple descendants of our ancestral religion, such as in the श्रुति, the para-Vedic material in the ऐतिहासिक-पौराणिक corpus, in the Roman religion relating to Castor and Pollux, and probably the non-Zoroastrian strains of the Iranian religion. It was definitely there in at least some branches of the Germanic religion, but its destruction by the West Asian mental disease has only left us with the euhemeristic figment of Horsa and Hengist as the descendants of Woden. Likewise, we hear explicitly of the destruction by the Christians of the temple dedicated to the Western Slavic deity Rugiaevit and his twin sons Porevit and Porenut. It is pretty likely that Rugiaevit's name is derived from the same root ru- as that in the name of Rudra, and these twin sons are the equivalents of the अश्विन्-s.

In the श्रुति, this old motif manifests as the अश्विन्-s being the sons of Rudra who follow on his track as he rides his heavenly chariot. As the physicians of the gods, they inherit the medical and pharmacological virtuosity of their father. In a parallel Vaidika tradition, which entered the श्रुति fold from a group of Aryans distinct from the ऋषि-s who composed the RV, the twin sons of Rudra are [Bhava and शर्व](https://manasataramgini.wordpress.com/2020/01/12/the-asvin-s-and-rudra/), who accompany their father, like the Dioskouroi of the Greco-Roman worlds. Their worship is prominent in the Atharvaveda and some texts preserved in later Vedic collections; however, in the आध्वर्यव tradition, they were absorbed as names of Rudra or those of the multitude of Rudra-s. In the para-Vedic material preserved in the इतिहास-s and पुराण-s we see them as the twin ectype of Skanda, i.e., as Skanda-विशाखौ, the sons of Rudra.

With this background, we shall consider the enigmatic सूक्त of Urucakri आत्रेय (RV 5.70), which on the surface is a simple 4-ऋक् one in the गायत्री meter. The अनुक्रमणि specifies its deities at the twin आदित्य-s, Mitra and वरुण. Indeed, the first ऋक् of the सूक्त is directed to these gods and it is embedded amid the long series of सूक्त-s to Mitra and वरुण by different आत्रेय-s:\
[पुरूरुणा चिद् ध्य् अस्त्य् अवो नूनं वां वरुण । मित्र वंसि वां सुमतिम् ॥]{style="color:#0000ff;"}\
Indeed, now, in full breadth is the aid from you two, O वरुण! I have gained the benevolence of you two, O Mitra!

After beginning with an acknowledgment of the help gained from Mitra and वरुण, the next ऋक् suddenly changes the focal deities:

[ता वां सम्यग् अद्रुह्वाणेषम् अश्याम धायसे । वयं ते रुद्रा स्याम ॥]{style="color:#0000ff;"}\
O you two, may we attain you two together, in your benign state (literally: without intention to harm) for our stability. May we be so, o you two Rudra-s.

[पातं नो रुद्रा पायुभिर् उत त्रायेथां सुत्रात्रा । तुर्याम दस्यून् तनूभिः ॥]{style="color:#0000ff;"}\
Protect us, two Rudra-s, by your defenses; also save us, since you two are good rescuers. May we overpower the dasyu-s with our bodies.

Notably, the deities remain dual in the above two ऋक्-s, but they are explicitly identified as twin Rudra-s. While some students of the Veda have taken this use of Rudra to be merely an appellation transferred to the deities of the first ऋक्, there is no support for that. Mitra and वरुण are unanimously categorized in the आदित्य class, as its leading exemplars, and never placed in the Rudra class. Hence, we have to understand the twin Rudra-s of the above two ऋक्-s differently. First, their raudra nature is explicitly indicated in the entreaty to be benign (अद्रुह्वाणेषम्). Second, they are described as सुत्रात्रा, good rescuers, which immediately brings to mind the अश्विन्-s who are frequently invoked in such a capacity. In the RV, Rudra in singular denotes the god, in his unitary form, and as the father of his class. Rudra-s in the plural refer to the entire class or the Marut-s. The dual form of Rudra applies only to the अश्विन्-s everywhere else in the RV. In particular, the Atri-s repeatedly refer to them as such: e.g., RV 5.73.8, 5.75.3 and in RV 5.41.3 they are invoked together with Rudra as Asuro दिवः. Thus, we posit that in RV 5.70.2-3, Urucakri आत्रेय implies the अश्विन्-s by the dual form of Rudra and not the twin आदित्य-s.

The last ऋक् of the सूक्त goes thus:\
[मा कस्याद्भुतक्रतू यक्षम् भुजेमा तनूभिः । मा शेषसा मा तनसा ॥]{style="color:#0000ff;"}\
May you two of wondrous deeds not make us experience some phantom with our bodies. Neither with the rest \[of our people] nor with our descendants.

The word यक्ष (neuter) could be taken to mean a ghostly apparition or phantom --- perhaps one which causes a disease --- a यक्ष्म. The imploration is to avoid the possession of the ritualist's own body or that of this people or descendants by such a phantom. On the one hand, this is rather reminiscent of the supplications to Rudra for similar protection, often with the negative particle मा. On the other, it is reminiscent of the supplication to वरुण to be relieved from his हेऌअस् (="fury"; also, a feature of Rudra) for the sins that he unerringly notices. For instance, we have in the श्रुति the imploration of शुनःशेप:

[अव ते हेऌओ वरुण नमोभिर्]{style="color:#0000ff;"}\
[अव यज्ञेभिर् ईमहे हविर्भिः ।]{style="color:#0000ff;"}\
[क्षयन्न् अस्मभ्यम् असुर प्रचेता]{style="color:#0000ff;"}\
[राजन्न् एनांसि शिश्रथः कृतानि ॥]{style="color:#0000ff;"} RV 1.24.14\
We avert your fury (हेऌअस्) O वरुण with obeisances,\
we implore to avert it by rituals and oblations;\
ruling, for us, O all-seeing Asura,\
you will give release from the sins that were done.

Thus, both वरुण and Rudra share not only the हेऌअस् but are also known as Asura-s (the latter being emphasized for the cognate of वरुण in the Iranic branch of the religion, and remembered for Odin (see below) in the Northern Germanic religion). Thus, even though वरुण or Mitra are never called Rudra-s, they have a certain overlap of category, particularly in the actions of वरुण, and perhaps, to a degree, in the Iranic world in the cognate of Mitra. We believe that in the सूक्त under consideration, Urucakri आत्रेय plays on this overlap in the final ऋक् by not naming any deity but simply using the dual epithet अद्भुतक्रतू. Thus, we suggest that he is purposefully ambivalent to cover both sets of twin deities referred to in the सूक्त --- the आदित्य dyad or the twin Rudra-s, i.e., the अश्विन्-s. The epithet अद्भुतक्रतू will transparently apply to the अश्विन्-s as they are frequently described as wonderworkers in the श्रुति. The ऋक् could also apply to Mitra and वरुण in the sense of supplication to avoid their हेऌअस्. The invocation of the हेऌअस् of Mitra, वरुण, and the Marut-s, representing the intersection of their respective functional categories can be seen in RV 1.94.12 composed by Kutsa आङ्गिरस:

[अयम् मित्रस्य वरुणस्य धायसे]{style="color:#0000ff;"}\
['वयाताम् मरुतां हेऌओ अद्भुतः ।]{style="color:#0000ff;"}\
[मृऌआ सु नो भूत्व् एषाम् मनः पुनर्]{style="color:#0000ff;"}\
[अग्ने सख्ये मा रिषामा वयं तव ॥]{style="color:#0000ff;"}\
This one is to be fed ghee \[literally suckled],\
as the wondrous pacifier of the fury of Mitra and वरुण, and of the Maruts.\
Have mercy on us! May the mind of these (the above deva-s) be good again\
O Agni, in your friendship, may we not be harmed.

This overlap in category has confused some Indo-Europeanists of the Dumezilianist strain. They have split hairs and gone into contortions about whether the Germanic Odin represents a cognate of वरुण or Rudra. This ancient functional intersection has meant that one or the other class of deities could have served as a locus for absorption of traits of the other.

