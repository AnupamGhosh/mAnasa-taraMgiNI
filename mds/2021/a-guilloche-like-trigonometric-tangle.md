
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A guilloche-like trigonometric tangle](https://manasataramgini.wordpress.com/2021/02/21/a-guilloche-like-trigonometric-tangle/){rel="bookmark"} {#a-guilloche-like-trigonometric-tangle .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 21, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/02/21/a-guilloche-like-trigonometric-tangle/ "1:26 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Coprimality, i.e., the situation where the GCD of 2 integers is 1 is one of the fundamental expressions of complexity. In that situation, two numbers can never contain the other within themselves or in multiples of them by numbers smaller than the other. In other words, their LCM is the product of the 2 numbers. There are numerous geometric expressions of this complexity inherent in coprime numbers. One way to illustrate it is by the below class of parametric curves defined by trigonometric functions:

 $$x=a_1\cos(c_1t+k_1)+a_2\cos(c_2t+k_2)\lbrack 5pt\rbrack y=b_1\sin(c_3t+k_3)+b_2\sin(c_4t+k_4)$$ 

The human mind perceives symmetry and certain optimal complexity as the hallmarks of aesthetics. Hence, we adopt the following conditions:\
1)  $a_1, a_2, b_1, b_2$  are in the range  $\tfrac{3}{14}$ ..1 for purely aesthetic considerations.\
2)  $k_1, k_2, k_3, k_4$  are orthogonal rotation angles that are in the range  $\lbrack 0, 2\pi\rbrack$ \
3)  $c_1$ , for aesthetic purposes relating to optimal complexity, is an integer in the range  $\lbrack 5,60\rbrack$ \
4)  $c_2$  captures the role of coprimality in complexity. It coprime with  $c_1$  and is in the range  $\lbrack 40,141\rbrack$ \
5)  $c_3 = |c_1-c_2|$ .\
6)  $c_4=c_1+c_2-c_3$ \
The last two conditions are for making the curve bilaterally symmetric --- an important aesthetic consideration.

The result is curves with a guilloche-like form. For the actual rendering, they are run thrice with different colors and slightly different scales to give a reasonable aesthetic. Our program randomly samples through the above conditions and plots the corresponding curves. Below are 25 of them.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/tritangs02.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Here is another run of the same.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/tritangs04.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad
```{=latex}
\end{center}
```


