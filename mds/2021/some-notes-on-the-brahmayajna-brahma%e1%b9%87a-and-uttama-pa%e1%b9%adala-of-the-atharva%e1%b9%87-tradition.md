
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some notes on the ब्रह्मयज्ञ ब्राह्मण and Uttama-पटल of the अथर्वण् tradition](https://manasataramgini.wordpress.com/2021/01/29/some-notes-on-the-brahmayajna-brahma%e1%b9%87a-and-uttama-pa%e1%b9%adala-of-the-atharva%e1%b9%87-tradition/){rel="bookmark"} {#some-notes-on-the-बरहमयजञ-बरहमण-and-uttama-पटल-of-the-अथरवण-tradition .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 29, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/01/29/some-notes-on-the-brahmayajna-brahma%e1%b9%87a-and-uttama-pa%e1%b9%adala-of-the-atharva%e1%b9%87-tradition/ "6:07 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[Download](https://manasataramgini.wordpress.com/wp-content/uploads/2021/01/brahmayajnaskt.pdf){.wp-block-file__button download=""}

[PDF version](https://manasataramgini.wordpress.com/wp-content/uploads/2021/01/brahmayajnaskt-1.pdf) 

The ब्रह्मयज्ञ ब्राह्मण (1.1.29 of the Gopatha-ब्राह्मण) of the Atharvaveda provides a glimpse of the Vedic संहिता canon as known to the ब्राह्मण authors of the AV tradition. The ब्रह्मयज्ञ might be done as part of the basic rite as done by dvija-s of other शाखा-s or as part of the more elaborate AV tradition of the annual Veda-vrata. The annual vrata-s of the अथर्वण् ब्राह्मण-s include the सावित्री-vrata, Veda-vrata, Kalpa-vrata, Mitra-vrata, Yama-vrata and मृगार-vrata. The क्षत्रिय-s and वैश्य-s should do at least 3 and 2 of them respectively, with the first 2 being obligatory. During these vrata-s the ritualist follows certain strictures like not consuming butter milk nor eating kidney beans, common millets, or the masura lentils at the evening meal, bathing thrice a day and wearing woolen clothing. Before performing ब्रह्मयज्ञ, he performs the आचमन as per the vidhi which states:\
[स आचमनं करोति ।]{style="color:#0000ff;"}\
He performs the ritual sipping of water.

This calls for the special अथर्वणिच् आचमन described in the final section of the आचमन-ब्राह्मण of the AV tradition (GB 1.1.39):\
[तद् अप्य् एतद् ऋचोक्तम् ---]{style="color:#0000ff;"}\
It has also been thus stated in the ऋक्:

["आपो भृग्वङ्गिरो रूपम् आपो भृग्वङ्गिरोमयं ।]{style="color:#0000ff;"}\
[सर्वम् आपोमयं भूतं सर्वं भृग्वङ्गिरोमयम् ॥"]{style="color:#0000ff;"}\
The waters are of the form भृगु-अङ्गिरस् incantations. The waters are imbued with the भृगु-अङ्गिरस् incantations.\
All being is imbued with the waters; \[thus,] all \[being] is imbued by the भृगु-अङ्गिरस् incantations.

•The अथर्वण्-s justify the above ऋक् is by noting that the पैप्पलाद Atharvaveda begins with the ऋक् "शं no देवीः..." to the waters (see below).

[अन्तरैते त्रयो वेदा भृगून् अङ्गिरसो 'नुगाः ॥]{style="color:#0000ff;"}\
Within these \[waters] the three \[other] Veda-s follow the भृगु-अङ्गिरस् incantations.

["अपां पुष्पं मूर्तिर् आकाशं पवित्रम् उत्तमम्" इति आचम्याभ्युक्ष्य् आत्मानम् अनुमन्त्रयत । [सूर्य जीव देवा जीवा जीव्यासम् अहम् ।]{style="color:#0000ff;"}\
[सर्वम् आयुर् जीव्यासम् ॥]]{style="color:#0000ff;"}\
"The flower is the form of the waters, the empty space \[and] that which the most pure". Thus, he sips the water and having sprinkled water (practically मार्जनम्) he recites the incantation indra जीव etc: Enliven, o Indra; Enliven o सूर्य. Enliven, o gods. May I live. May I complete my term of life.

•The flower of the waters in the above incantation is an allusion to the ऋक् describing the the ancient action of the अथर्वण्-s in kindling the fire in waters \[from a lotus]: `[`त्वाम् अग्ने पुष्कराद् अध्य् अथर्वा निर् अमन्थत ।"]{style="color:#0000ff;"}\
•He does the आचमन by taking three sips each with two successive words from the mantra [अपाम् पुष्पम्...]{style="color:#0000ff;"}

[इति ब्राह्मणम् ॥]{style="color:#0000ff;"}\
Thus is the ब्राह्मण.

Now for the ब्रह्मयज्ञ:\
[किं देवतम् इति ? ऋचाम् अग्निर् देवतम् । तद् एव ज्योतिः । गायत्रं छन्दः । पृथिवी स्थानम् ।]{style="color:#0000ff;"}\
["अग्निम् ईऌए पुरोहितं यज्ञस्य देवम् ऋत्विजं । होतारं रत्नधातमम् ॥"]{style="color:#0000ff;"}\
[इत्य् एवम् आदिं कृत्वा ऋग्वेदम् अधीयते ॥]{style="color:#0000ff;"}\
Who is the deity? Agni is the deity of the ऋक्-s. That is indeed light. गायत्री is its meter. The earth is its station.\
"I praise Agni, the officiant of the ritual, the god and ritualist; the होतृ and the foremost giver of gems."\
Thus, having placed it at the beginning the ऋग्वेद is studied.

[यजुषां वायुर् देवतम् । तद् एव ज्योतिस् त्रैष्टुभं छन्दः । अन्तरिक्षं स्थानम् ।]{style="color:#0000ff;"}\
[इषे त्वोर्जे त्वा वायव स्थ देवो वः सविता प्रार्पयतु श्रेष्ठतमाय कर्मणे ॥]{style="color:#0000ff;"}\
[इत्य् एवम् आदिं कृत्वा यजुर्वेदम् अधीयते ॥]{style="color:#0000ff;"}\
वायु is the deity of the यजुष्-es. That is verily light; त्रिष्टुभ् is its meter. The atmosphere is its station.\
"To you for nourishment, to you for strength. You are the वायु-s. May सवितृ impel you the most excellent ritual."\
Thus, having placed it at the beginning the Yajurveda is studied.

[साम्नाम् आदित्यो देवतम् । तद् एव ज्योतिः । जागतं छन्दः । द्यौः स्थानम् ।]{style="color:#0000ff;"}\
["अग्न आ याहि वीतये गृणानो हव्यदातये । नि होता सत्सि बर्हिषि ॥"]{style="color:#0000ff;"}\
[इत्य् एवम् आदिं कृत्वा समावेदम् अधीयते ॥]{style="color:#0000ff;"}\
The आदित्य is the deity of the सामन्-s. That is indeed light. Jagati is its meter. The heaven is its station.\
O Agni, come to the oblations, praised with songs to the ritual offering. Sit as the होतृ on the ritual grass.\
Thus, having placed it at the beginning the सामवेद is studied.

[अथर्वणां चन्द्रमा देवतम् । तद् एव ज्योतिः । सर्वाणि छन्दांसि । आपः स्थानम् । <शं नो देवीर् अभिष्टय> इत्य् एवम् आदिं कृत्वाथर्ववेदम् अधीयते ॥]{style="color:#0000ff;"}\
The moon is the deity of the अथर्वण् incantations. That is indeed light. All are its meters. The waters are its station. "May the divine \[waters] be auspicious for us..." Thus, having placed it at the beginning the Atharvaveda is studied.

[अद्भ्यः स्थावर-जङ्गमो भूत-ग्रामः संभवति । तस्मात् सर्वम् आपोमयं भूतं सर्वं भृग्वङ्गिरोमयम् । अन्तरैते त्रयो वेदा भृगून् अङ्गिरसः श्रिता इत्य् अब् इति प्रकृतिर् अपाम् ओंकारेण च । एतस्माद् व्यासः पुरोवाच:]{style="color:#0000ff;"}\
["भृग्वङ्गिरोविदा संस्कृतो 'न्यान् वेदान् अधीयीत ।]{style="color:#0000ff;"}\
[नान्यत्र संस्कृतो भृग्वङ्गिरसो 'धीयीत ॥"]{style="color:#0000ff;"}\
From the waters the families of immotile and motile organisms have come into being. Hence, all being is imbued with water; \[thus] all is imbued with the भृगु-अङ्गिरस् incantations. The three other Veda-s are situated within these भृगु-अङ्गिरस् incantations. Therefore, indeed it is water and the origin of water is by the ओंकार. In this regard व्यास had formerly said:\
"He who is sanctified by the भृगु-अङ्गिरस् incantations may study the other Veda-s.\
The one sanctified elsewhere should not study the Veda of the भृगु-अङ्गिरस्-es."

•Regarding the origin of all beings from water: this is articulated early on in the ऋक्: [यो अप्स्व् आ शुचिना दैव्येन...]{style="color:#0000ff;"} (RV 2.35.8) of गृत्समद शौनहोत्र.

[सामवेदे 'tha खिलश्रुतिर् ब्रह्मचर्येण चैतस्माद् अथर्वाङ्गिरसो ha yo veda sa veda sarvam |]{style="color:#0000ff;"}\
[इति ब्राह्मणम् ॥]{style="color:#0000ff;"}\
Now there is also the khila of the सामवेद: "Therefore, he who as a celibate student knows the Veda of अथर्वाङ्गिरस्-es knows all this."

Thus is the ब्राह्मण.

•The statement from the सामवेद-khila is also taken to justify the punarupanayana that is performed in order for those of other traditions to study the Atharvaveda.

Notes\
Several notable points are raised by the ब्रह्मयज्ञ ब्राह्मण of the AV, not just regarding the AV tradition but also regarding its interaction with the other Vedic schools and their own evolution. It is quite obvious that the ब्रह्मयज्ञ ब्राह्मण represents a relatively late ब्राह्मण composition with a specific aim of justifying the primacy of the AV, probably in the context of the intra-brahminical competition for the position of the brahman in the श्रौत ritual. This is explicitly supported by the fact that it cites व्यास [पाराशर्य] who appears in late Vedic texts and is remembered by tradition as the redactor of the 4 fold form of the श्रुति. In a similar vein, the citation of the सामवेद-khila suggests that it was composed after the terminal sections of the सामवेदिच् tradition had been completed.

The opening ऋक् of the RV is compatible with any of the शख-s of the ऋग्वेद. The Yajurveda that it refers to is clearly the वाजसनेयि संहिता (either माध्यंदिन or the काण्व शाखा-s). The Samaveda could again be any of the Samavedic संहिता-s. The Atharvaveda is probably the पैप्पलाद संहिता because the vulgate and the शौनकीय begin with ["ye त्रिशप्ताः..."]{style="color:#0000ff;"} However, we must note that we do not know the beginning of the lost AV शाखा-s.

Why is this notable? The AV-परिशिष्ट 46 (Uttama-पटल) gives the beginning and end verses of the four Veda संहिता-s along with several AV verses to be used in the annual Veda-vrata. Notably, these are partly different from those of the ब्रह्मयज्ञ ब्राह्मण. Interestingly, according to the Uttama-पटल, the RV ends with the famous ऋक्: ["तच् छम्योर् आवृणिमहे..."]{style="color:#0000ff;"}. This is not present in the शाकल-पाठ which instead ends with the short संज्ञा-सूक्त. The former ऋक् was claimed by Michael Witzel to be the last ऋक् of the बाष्कल RV. However, as Vishal Agrawal correctly noted its is stated to be the last ऋक् by even the शाञ्खायन and कौशीतकि traditions. Thus, the Uttama-पटल is referring to some RV शाखा other than शाकल, though we cannot be sure of its identity.

The Uttama-पटल gives the सामवेद's first verse as ["अग्न आ याहि..."]{style="color:#0000ff;"}, which is known to be the first ऋक् of all surviving शाखा-s of the SV. However, the last ऋक् is given as:\
["एष स्य ते धारया सुतो 'व्यो वारेभिर् हवने मदितव्यम् । क्रीडन् रश्मिर् अपार्थिवः ॥"]{style="color:#0000ff;"}\
This is different from the ऋक् ["स्वस्ति न इन्द्रो वृद्धश्रवाः..."]{style="color:#0000ff;"} with which the surviving SV शाखा-s conclude. It is a divergent variant of the ऋक् RV 9.108.5 not attested elsewhere. In fact the extant SV संहिता-s contain a version that follows the RV cognate. Thus, evidently the Uttama-पटल is referring to a now lost SV शाखा.

The situation with the YV is the most interesting. The cited starting mantra goes thus:\
["इषे त्वोर्जे त्वा वायव स्थोपायव स्थ देवो वः सविता प्रार्पयतु श्रेष्ठतमाय कर्मण आप्यायध्वम् अघ्न्या इन्द्राय भागम् ऊर्जस्वतीः पयस्वतीः प्रजावतीर् अनमीवा अयक्ष्मा मा व स्तेन ईशत माघशंसो रुद्रस्य हेतिः परि वो वृणक्तु ध्रुवा अस्मिन् गोपतौ स्यात बह्वीर् यजमानस्य पशून् पाहि ॥"]{style="color:#0000ff;"}

Remarkably, this mantra is not found in any of the extant YV संहिता-s. However, the ["इन्द्राय भागम्"]{style="color:#0000ff;"} is reminiscent of the ["इन्द्राय देव-भागम्"]{style="color:#0000ff;"} found in the आपस्तम्ब-श्रौतसूत्र and the भारद्वाज-श्रौतसूत्र or the ["देवेभ्य इन्द्राय"]{style="color:#0000ff;"} found in the मैत्रायणीय संहिता. Moreover, the last mantra of the Yajurveda is given as ["दधिक्राव्णो अकारिषम्..."]{style="color:#0000ff;"}. In other YV संहिता-s, this mantra occurs in the अश्वमेध section and is used among other thing by the ritualists to purify their mouths after the obscene sexual dialog. However, it is not the last mantra of the अश्वमेध section in any of the extant संहिता-s. This indicates two things: first, the Uttama-पटल is recording a now lost YV शाख of the कृष्णयजुर्वेद (KYV). Second, while today आपस्तम्ब and भारद्वाज are attached to the तैत्तिरीय-शाखा, they were once the सूत्र-s of a lost KYV शाखा. This loss likely happened relatively early. It was probably associated with the southward movement of the आपस्तम्ब-s and भारद्वाज-s, who then shifted to the related तैत्तिरीय-संहिता (TS). The text of the बौधायन-श्रौतसूत्र precisely follows the TS; hence, it was definitely one of the original सूत्र-s of the तैत्तिरीय-शाखा.

Finally, the AV of the Uttama-पटल begins with ["ye त्रिशप्ताः...]{style="color:#0000ff;"}" indicating that it was recording the original शाखा behind the vulgate or the शौनकीय.

Thus, we see a striking difference between the two AV traditions of the Gopatha-ब्राह्मण and the Uttama-पटल. While the tendency is to see the AV-परिशिष्ट-s as late and post-dating the ब्राह्मण, we have to be more cautious. First, the AV-परिशिष्ट-s are a rather composite mass recording a range of traditions that with a wide temporal span. Some material like the नक्षत्र-kalpa-सूक्त could closer to the late ब्राःमण material in age whereas, at the other end, the tortoise-soothsaying (कुर्मविभाग) is likely a late text. We posit that the Uttama-पटल belongs to the an early layer of the AV-परिशिष्ट-s --- this provides a reasonable hypothesis for the divergence between it and the ब्राह्मण. First, it should be noted that the AV-पैप्पलाद-AV-शौनकीय/vulgate divergence is rather deep --- mirroring the deep divergence of the कृष्ण and शुक्ल branches of the Yajurveda. This split might have gone along with some geographical separation in the initial phase of their divergence. This geographical separation model would suggest that the ब्रह्मयज्ञ ब्राह्मण tradition was associated with the AV-पैप्पलाद or a related lost AV school that was in the vicinity of the old वाजसनेयिन्-s. This is also supported by certain parallels seen between the Gopatha-ब्राह्मण and the शतपथ-ब्राह्मण in the श्रौत sections. In contrast, the Uttama-पटल as associated with the शौनकीय or a related school that developed in the vicinity of a lost KYV शाखा.

We have evidence that the interactions between the KYV and AV traditions might go back even deeper in time: for example, this is clearly supported by the AV-related भवा-शर्वा-सूक्त of the कठ-s that was likely present in the lost कठ-ब्रह्मण and the various shared सूक्त-s and उपनिषत् material between the तैत्तिरीय and the AV. Finally, we have evidence from what is today Gujarat that at a later period there was a certain equilibriation of the AV schools with the combination of the पैप्पलाद and शौनकीय material. This parallels a similar acquisition of some कठ material by the तैत्तिरीय. Thus, there appears to have been a relatively complex web of fission and fusion interactions between the शाख-s over a protracted period.

