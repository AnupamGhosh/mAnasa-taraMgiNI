
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Sneha, snowstorms, the sun and the moon in enigmatic ऋक्-s](https://manasataramgini.wordpress.com/2021/12/06/sneha-snowstorms-the-sun-and-the-moon-in-enigmatic-%e1%b9%9bk-s/){rel="bookmark"} {#sneha-snowstorms-the-sun-and-the-moon-in-enigmatic-ऋक-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 6, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/12/06/sneha-snowstorms-the-sun-and-the-moon-in-enigmatic-%e1%b9%9bk-s/ "5:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

That the Indo-European homeland was a cold place with snow is evidenced by widespread survival of two hon-homologous words for snow. Recently, a discussion on one of these words, sparked by some linguist, landed on my timeline on Twitter. From that, it seemed that people were unaware of the attestation of its cognate in Old Indo-Aryan. This, in turn, reminded me of a discussion we had with our friend in college on the very same issue. Hence, we thought it worthwhile to put down this discussion --- this apposite since the winter solstice is approaching, and as we write this note, it is a new moon with a solar eclipse in Antarctica.

The two words for snow that can be traced back to early Indo-European, probably even PIE, are attested in Sanskrit as sneha and himá/ह्íमा. The उदात्त is represented by the acute sign and differs between the masculine form that is usually used for snow and the feminine for winter.

Sneha is represented by many cognates that include snow itself; Old Iranian has snaēža = to snow; Baltic: (Lithuanian) sniẽgas; Germanic: (Old English): स्नीwअन्. There are the 0-graded forms in: Greek: niphás = snowflake; Latin: nivis; Celtic (Welsh): nyf. Thus, it is a solid IE word. Interestingly, it has also been transferred to Sumerian in a form that suggests either early Indo-European or the Satem clade (Balto-Slavo-Aryan) as the possible sources. This might point to a potential trade contact between Sumerians and the Indo-Europeans in their colder lands to the north of the former. Coming to Indo-Aryan, while etymologists recognize the ancestral form as being Sanskrit sneha, they usually only cite Prakritic siṇeha or सिण्हा as cognates bypassing Sanskrit as though it is not attested in it. However, as we shall see below, this is not true. In New Indo-Aryan languages that are still familiar with snow we have descendants of the Prakritic forms as the primary word for snow, e.g., Kashmiri स्हीन (pronounced these days with terminal schwa loss).

The second word hima, was probably paralogously polysemous right from the early stages of PIE, meaning both winter and snow. It is clearly inherited from PIE in the Indo-Hittite sense as we have Hittite gimmanza = winter. We have Baltic (Lithuanian): žiemà = winter; Greek: kheima = snowstorm/cold; khion = snow; Armenian: jiun = snow; Latin: hiems= winter; Celtic (Old Welsh) gaem = winter. In Sanskrit we see all the senses being attested. The usage शत-हिमा is literally a 100 winters, meaning hundred years. A similar usage is seen in Latin, e.g., बीमुस् from \*bihimos --- lasting two years. The form hemanta again denotes a season marking the end of winter. On the other hand, the form himavant means a mountain \[covered] with snow. Similarly, in the ऋग्वेद hima can be directly used to indicate snow, for example:

[हिमेनाग्निं घ्रंसम् अवारयेथाम्]{style="color:#0000ff;"}\
[पितुमतीम् ऊर्जम् अस्मा अधत्तम् ।]{style="color:#0000ff;"}\
[ऋबीसे अत्रिम् अश्विनावनीतम्]{style="color:#0000ff;"}\
[उन् निन्यथुः सर्वगणं स्वस्ति ॥]{style="color:#0000ff;"} RV 1.116.8

With snow, you two averted the scorching fire,\
you two bestowed nourishing food for him \[Atri],\
You two अश्विन्-s, led out Atri from the fuming crater,\
into which he been led, and all the troop to weal.

This ऋक् attributed to कक्षिवन्त् Dairghatamasa \[Footnote 2] is an allusion to the famous deed of the अश्विन्-s that is repeatedly alluded to in the RV, where they saved Atri from a fuming crater. The simple reading of the verse would imply that Atri was led into it along with his troops. They were cooled with snow and then brought up. The phrase सर्वगणं svasti occurs only one other time in the RV, in a सूक्त of the Atri-s; however, there it refers to the गण-s of the god बृहस्पति. Nevertheless, one wonders if there is a subtler, undiscovered connection furnished by this cognate phrase from these relatively late सूक्त-s of the RV. Finally, one may comment that this tale hints at the possibility of Atri and his men having fallen into one of the geothermal craters in the Caspian-Black Sea region close to the IE homeland.

Returning to sneha-, we tabulate below the occurrence of this word in some old Vedic texts relative to hima- (while we count both senses of hima- we do not count the season hemanta in the below table).

  - ---------------------- ------- ------
  Text                    sneha   hima
  ऋग्वेद                    2       11
  Atharvaveda (vulgate)   1       16
  Atharvaveda (पैप्पलाद)    0       35
  तैत्तिरीय संहिता           0       10

  - ---------------------- ------- ------

Thus, it is clear that sneha, while attested in the oldest surviving Indo-Aryan text (the single AV-vulgate instance is identical to the ऋक् found in the RV), it is absent in the successor texts though hima- remains as common or is more frequently used. However, we should also add that a related form स्नीहिति (snowstorm) occurs once in the RV and once in the तैत्तिरीय आरण्यक. In the case of sneha, a semantic shift occurred in Sanskrit, where it came to denote a wide range of things, including fluidity, smoothness, oil/fat and love. We will next examine the two occurrences in the RV and suggest that they are semantically aligned with the Middle and New Indo-Aryan forms. Both occur in enigmatic सूक्त-s/ऋक्-s that need further discussion.

The first occurrence is in a long सूक्त of तिरश्ची आङ्गिरस on the many glorious acts of Indra, which needs to be described to give some context. The first three ऋक्-s (1-3) describe the might of Indra and his vajra and also allude to his act of piercing the  $3 \times 7$  mountains with his arrow. This motif is reused by वाल्मीकि in the रामायण when राम pierces the seven trees to prove his might (Indra-hood displacing that of the mighty वालिन्) to the ape सुग्रीव. The following triad (4-6) describes how Indra, in his cosmogonic form as the generator of all beings, slew Ahi with his vajra, even as the mountains shrieked. The Marut-s seeing his valor, approached him for an alliance, like ब्राह्मण-s reciting mantra-s to praise him. The next तृच (7-9) describes a Marut-centric version of the वृत्र myth. When वृत्र violently sallied forth, the other gods, who were the companions of Indra, retreated, leaving him alone in the battle. However, the 360 Marut-s (an unusual count associated with the days in an year) singing the praises of Indra joined him in battle, asking for a share of the ritual offerings. They urged Indra to scatter the anti-deva asura-s with his vajra and cakra, assisted by their battle formation fronted by their sharp spears.

In the next तृच (10-12), the आङ्गिरस calls on his fellow ritualists to send forth their chants to Indra. In the तृच (13-15), which includes the ऋक् of specific interest to us, the narration moves to the battles fought by Indra against the enemies of the gods in alliance with बृहस्पति. This triad is soaked in astronomical allegory, with the god बृहस्पति himself likely being represented in the sky by the planet Jupiter. The main feature of this triad is the mention of a black (shrouding like a cloud) drop, कृष्ण drapsa, which is said to wander to the solstitial colure along a sinuous river अंसुमति. We interpret this river as the lunar ecliptic. The whole myth seems to encode a solar eclipse close to the winter solstice (The old Aryan New Year), in which context the snowstorm is mentioned. The next तृच (16-18) describes the following acts of Indra: 1. Right when he was born, he became the foe of the seven unrivaled ones (not entirely clear who they are). 2. He discovered Dyaus and पृथिवि, which were hidden. 3. He set into motion the wide-ranging worlds. 4. He smashed the unrivaled one (वृत्र) with his vajra. 5. He slew शुश्न. 5. He discovered the hidden cows. 6. He demolished the fortifications. 7. He released the frozen rivers and slew the demoness (an allusion to दानु) lording over the waters. The final तृच (19-21) praises Indra as वृत्रहन् and the lord of the ऋभु-s, calling on him for the soma offering.

[अव द्रप्सो अंशुमतीम् अतिष्ठद्]{style="color:#0000ff;"}\
[इयानः कृष्णो दशभिः सहस्रैः]{style="color:#0000ff;"}\
[आवत् तम् इन्द्रः शच्या धमन्तम्]{style="color:#0000ff;"}\
[अप स्नेहितीर् नृमणा अधत्त ॥]{style="color:#0000ff;"} RV 8.96.13\
The drop stood at the अंशुमती,\
the black \[drop] wanders with the ten thousand.\
Indra helped it \[the drop] blowing along with his skill.\
The manly-minded \[Indra] repulsed the snowstorm.

[द्रप्सम् अपश्यं विषुणे चरन्तम्]{style="color:#0000ff;"}\
[उपह्वरे नद्यो अंशुमत्याः ।]{style="color:#0000ff;"}\
[नभो न कृष्णम् अवतस्थिवांसम्]{style="color:#0000ff;"}\
[इष्यामि वो वृषणो युध्यताजौ ॥]{style="color:#0000ff;"} RV 8.96.14\
I saw the drop wandering at the solstice,\
in the sinuous path of the River अंशुमती,\
going down like a black cloud,\
I impel you, bulls, to fight in the battle.

[अध द्रप्सो अंशुमत्या उपस्थे]{style="color:#0000ff;"}\
[।अधारयत् तन्वं तित्विषाणः ।]{style="color:#0000ff;"}\
[विशो अदेवीर् अभ्य् आचरन्तीर्]{style="color:#0000ff;"}\
[बृहस्पतिना युजेन्द्रः ससाहे ॥]{style="color:#0000ff;"} RV 8.96.14\
Then, the drop, in the lap of the अंशुमती,\
bore the body sparkling with light.\
As the deva-less folks moved forth \[to attack],\
united with बृहस्पति, Indra conquered \[them].

**Commentary:** The black drop (drapsa, an old IE word) is interpreted as the new Moon. The drop is associated most commonly with soma (with a lunar equivalence in several cases; on rare occasions it is used for Venus). Normally, the soma is silvery --- ऋजीशिन् (like earlier in this सूक्त) --- reinforcing the lunar connection. However, here the drop is explicitly and atypically described as black, suggesting that the new Moon is alluded to. The first ऋक् mentions the drop wandering with a ten thousand: we take this large number to denote the stars. The drop is seen as blowing along: we consider this an early allusion to the primitive Hindu astronomical theory (shared with the Greeks and likely of old IE provenance) of celestial bodies being blown on their paths by cosmic winds. In the second ऋक्, the composer states that he sees the black drop which is likened to a cloud --- suggesting its shrouding nature --- wandering near a colure. He also mentions the sinuous course of the river अंशुमती. Given that the word अंशु is used for soma (or the soma stalks before extraction of the juice) and metaphorically connects the soma plant and the Moon, अंशुमती would mean the riverine path with soma/the Moon. Hence, we take this to be an allusion to the lunar path/ecliptic \[Footnote 3].

We take the colure (विषुण) to be solstitial. Winter is the season that corresponds to the battle between the gods, led by Indra, and the demons. The verb, ava-स्था = going own, suggests the nether point of the ecliptic path analogized to a river. Hence, we hold that the solstice referred to is specifically the winter solstice. Thus, it is not any new moon but likely the new Moon closest to the winter solstice, corresponding to the old Aryan New Year. This, in turn, supports the idea that the snehiti which Indra averts is indeed a snowstorm. The final ऋक् of this triad mentions the black drop in the lap of अंशुमती, where it paradoxically takes on a body sparkling with light. We take this to allude to a solar eclipse happening close to the solstice. The at the point of emergence of the sun from the total eclipse or an annular eclipse would indeed give the impression of the black drop (the Moon) taking on a glittering body. Thus, this is a variant of the famous स्वर्भानु eclipse myth of the Atri-s but probably referring to a specific eclipse near the solstice. In this context, the attack अदेवी folks should be taken as a purposeful conflation of the earthly enemies with the asura-s causing the eclipse as in the स्वर्भानु myth. Moreover, given the overall celestial setting, the specific involvement of बृहस्पति, as a companion of Indra, in this conflict suggests the potential presence of Jupiter in the vicinity during this event (Or perhaps in the नक्षत्र of तिष्या).

We may also point out that the deployment of snow or other "weather weapons" is a feature of the battles of Indra with the दानव-s elsewhere in the RV. For example, हिरण्यस्तूप आङ्गिरस gives an account of the battle between Indra and Ahi, when the latter had frozen the rivers and corralled the cows. Here, Ahi, first tries to pierce Indra with his spear, but Indra evades him by becoming the tail of a horse. Having evaded his strike, Indra conquered the cows and the soma and released the waters. Then he closed in for combat with Ahi:

[नास्मै विद्युन् न तन्यतुः सिषेध]{style="color:#0000ff;"}\
[न याम् मिहम् अकिरद् ध्रादुनिं च ।]{style="color:#0000ff;"}\
[इन्द्रश् च यद् युयुधाते अहिश् च]{style="color:#0000ff;"}\
[उतापरीभ्यो मघवा वि जिग्ये ॥]{style="color:#0000ff;"} RV 1.32.13\
Neither the lightning nor the thunder scared away \[Indra] for him,\
neither the snow (/mist) nor the hail that he \[Ahi] spread out.\
When Indra and Ahi fought each other,\
Maghavan triumphed, \[then] and also for the time that came.

Here, Ahi deploys various "weather weapons", reminiscent of the steppe "rain-stone" magic of the Turkic and Mongolic world, but they fail to scare away Indra. The first two, lightning and thunder are unambiguous, and so is the final one, hail (ह्रादुनि). The word miha could mean snow or mist. In either case, it supports the deployment of such weather weapons, consistent with the interpretation of sneha- as snow, i.e., in a snowstorm.

Strikingly, the second occurrence of sneha- is again in the context of the same eclipse myth. This account occurs in the monster सूक्त of मण्डल 9, which agglomerates shorter सूक्त-s of various वसिष्ठ-s and Kutsa आङ्गिरस. We shall consider the whole तृच with this reference below:

[अया पवा पवस्वैना वसूनि]{style="color:#0000ff;"}\
[मांश्चत्व इन्दो सरसि प्र धन्व ।]{style="color:#0000ff;"}\
[ब्रध्नश् चिद् अत्र वातो न जूतः]{style="color:#0000ff;"}\
[पुरुमेधश् चित् तकवे नरं दात् ॥]{style="color:#0000ff;"} RV 9.97.52\
Bring (addressed to soma), by purifying yourself with this filtering, riches.\
At the hiding of the Moon, O drop (moon/soma), run forth into the lake.\
The yellowish (sun) is also here as if impelled by the wind.\
the wise one (Soma) has indeed given us the man (Indra) for the sally.

[उत न एना पवया पवस्व]{style="color:#0000ff;"}\
[अधि श्रुते श्रवाय्यस्य तीर्थे ।]{style="color:#0000ff;"}\
[षष्टिं सहस्रा नैगुतो वसूनि]{style="color:#0000ff;"}\
[वृक्षं न पक्वं धूनवद् रणाय ॥]{style="color:#0000ff;"} RV 9.97.52\
Also with this filtering purify yourself,\
at the front of the famous ford of celebration.\
Sixty thousand treasures the destroyer of rivals,\
like a tree with ripe fruits, will shake down for triumph.

[महीमे अस्य वृषनाम शूषे]{style="color:#0000ff;"}\
[मांश्चत्वे वा पृशने वा वधत्रे ।]{style="color:#0000ff;"}\
[अस्वापयन् निगुतः स्नेहयच् च]{style="color:#0000ff;"}\
[अपामित्रां अपाचितो अचेतः ॥]{style="color:#0000ff;"}\
The bull is his name \[Indra], great and fierce, are his two,\
deadly weapons, in the hiding of the Moon or in the touching.\
He put to sleep the rivals and snowed down on them.\
Repulse the enemies, repulse the senseless ones.

**Commentary:** Composite सूक्त-s, like the one in which these ऋक्-s of Kutsa आङ्गिरस occur, are typical of the final part of मण्डल 9. Except for Kutsa, who is the author of the last 4 तृच-s and the terminal ऋक् with the classic Kutsa refrain, all the other authors are वासिष्ठ-s. However, throughout the long सूक्त (longest in the RV) we find several allusions to the finding of the sun's path, the holding of the sun, and soma as the Moon. Thus, it is not out of place to furnish an astronomical explanation for these ऋक्-s. Key to the interpretation of these ऋक्-s is a rare word मांश्चतु/मांश्चत्व whose meaning has puzzled students over the ages. It occurs only thrice in the RV, and its meaning was already obscure to यास्क, who groups it with the words for horses in the निघण्टु. Two of the occurrences are in this तृच, and one is in RV 7.44.3 by वसिष्ठ मैत्रावरुणि. Thus, this word perhaps links the वासिष्ठ-s to Kutsa. It can be etymologized as मंस्+catu. Catu can be derived from the root cat- = to hide or vanish. मांस् (with a pure अनुनासिक) is taken to mean the Moon, an older variant of मास्, closely related to the form in early Indo-European. This form is supported by other Indo-European cognates like: Baltic (Latvian): mēnesis; Latin: mensis. Thus, the word is taken to mean the vanishing of the Moon. To their credit, some white indologists have correctly etymologized this word using comparisons across IE. However, they failed to understand its actual meaning. Notably, on the only occasions it occurs in RV, it is coupled with bradhna --- the yellowish or reddish sun. This is also seen in the case of the verse of वसिष्ठ:

[दधिक्रावाणम् बुबुधानो अग्निम्]{style="color:#0000ff;"}\
[उप ब्रुव उषसं सूर्यं गाम् ।]{style="color:#0000ff;"}\
[ब्रध्नम् मांश्चतोर् वरुणस्य बभ्रुं]{style="color:#0000ff;"}\
[ते विश्वास्मद् दुरिता यावयन्तु ॥]{style="color:#0000ff;"}\
Ever having awakened, to दधिक्रावन् and Agni\
I speak; to उषस्, and the sun, the cow.\
The yellowish one from the hiding of the Moon, \[becomes] वरुण's brown one,\
let them drive away all the bad things from us.

Notably, the first ऋक् of Kutsa, explicitly states that the sun (bradhna) is also at at the same place as the hiding of the Moon. The sun is said to be impelled to that place by the wind --- again, note an allusion to the old hypothesis of the cosmic winds moving the celestial bodies (c.f. the above ऋक् of तिरश्ची आङ्गिरस). This conjunction corresponds to अमावास्य or the sun and moon "dwelling together", resulting in the new Moon (or the hiding/vanishing of the Moon). Hence, मांश्चत्व should be understood as the "hiding of the moon" at new Moon in all its occurrences. However, there are indications that it is a new moon with a solar eclipse. The final ऋक् talks of two events, the मांश्चत्व and the पृशन, i.e., the touching. We take this touching as the "contact" of the sun and Moon implying an eclipse. Moreover, the ऋक् of वसिष्ठ, states that the bradhna (usually yellow or red) is वरुण's brown one from the मांश्चतु. This suggests that the sun's darkening, indicating a solar eclipse at the new moon \[Footnote 1].

We also encounter the cryptic statements parallel to the ऋक्-s of तिरश्ची आङ्गिरस, such as the drop (Moon) running into the lake. This is also called the famous ford (तीर्थ) --- you can cross over to the "other side" there. We take these as allusions to the winter solstitial point on the ecliptic. Thus, we believe both Kutsa and तिरश्ची are talking about the same or a similar eclipse close to the winter solstice. However, notably, in this case, it is Indra who showers snow on the enemies, putting them to "sleep" --- again reminding us of the use of rain/snow stones in the Altaic warfare. Thus, the two occurrences of sneha+ and the one occurrence of स्नीह्- in the RV indicate the use of this ancient IE word in the sense of snow. The interesting early polymorphism in the form of sneh- and स्नीह्- suggests that the ancestral state of this word, perhaps even its form in the original dialect in which the RV was composed, was close to the ancestral Baltic version (at least the first syllable). Thus, we are probably seeing a fossil of the dialect diversity in early Indo-Aryan or Indo-Iranian itself.

To conclude, we may note, an eclipse at the solstice is a relatively rare event. However, if we give a leeway of about 5 days on either side of the winter solstice, one may get more of such events. Below is a list of such events that have happened or will happen over 3 centuries from 1801-2100 CE anywhere in the world grouped by their  $\approx 19$  year lunar cycle from the catalog of Eclipse Predictions by Fred Espenak (provided by NASA):\
21 Dec 1805; 20 Dec 1824; 21 Dec 1843; 21 Dec 1862\
22 Dec 1870; 22 Dec 1889; 23 Dec 1908\
24 Dec 1916; 24 Dec 1927\
25 Dec 1935; 25 Dec 1954; 24 Dec 1973; 24 Dec 1992\
25 Dec 2000; 26 Dec 2019; 26 Dec 2038; 26 Dec 2057\
27 Dec 2065; 27 Dec 2084\
17 Dec 2066

One can see that between 1900-2100 there have been/there are no events on the solstice day. However, in 1870 CE we had a very close pass within 12 hours of the solstice. In general, the 1800s saw several close events, but the following two centuries did not. Due to the 19-year clusters, we cannot be sure that the ones recorded in the RV were the same event. However, their relative rarity and clustering would mean that they might have been dramatic enough to leave a memory in the text.

  - -----------------------------------------------------------------------

Footnote 1: This occurs in the context of दधिक्रावन्, who is typically invoked in the dawn ritual. We posit that दधिक्रावन् represents a heliacally rising old Vedic constellation, although its identity still remains uncertain.

Foonote 2: While its style is similar to that of the old Gotama founders like कक्षिवन्त् and his father, the सूक्त itself mentions कक्षिवन्त् in the third person and talks of the later Gotama-s. This suggests that it was appended later to the कक्षिवन्त् collection by one of his successors.

Foonote 3: In later Hindu tradition, the lunar and solar paths are often depicted on temple roofs as sinuous snakes.

