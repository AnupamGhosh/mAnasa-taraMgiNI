
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The रामायण in numbers: meters, sarga- and काण्ड- structure](https://manasataramgini.wordpress.com/2021/12/16/the-ramaya%e1%b9%87a-in-numbers-meters-sarga-and-ka%e1%b9%87%e1%b8%8da-structure/){rel="bookmark"} {#the-रमयण-in-numbers-meters-sarga--and-कणड--structure .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 16, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/12/16/the-ramaya%e1%b9%87a-in-numbers-meters-sarga-and-ka%e1%b9%87%e1%b8%8da-structure/ "6:01 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In the extant Indo-European textual corpus, only in the Hindu collection do we find two complete early epics to complement the श्रुति. The Iranian epics come from a much later age than the core Avestan corpus, and in the Greek and Celtic cases, the श्रुति-equivalents have been mostly or entirely lost. As they have come down to us, the Hindu epics postdate much of the Vedic corpus but are still in a distinct language register that largely predates the classical Sanskrit. Thus, the numerical study of the epics gives us essential information regarding the evolution of the Old Indo-Aryan language and compositional technique, with general implications for earlier branching events within Indo-European. Contrary to the deeply flawed mainstream white indological opinion (and its imitators), and in line with Hindu tradition, we hold that the original रामायण was composed prior to the महाभारत. However, it is also clear that both epics were at some point "held" by the same expositors and redactors, resulting in some convergences. We had [earlier presented some key details](https://manasataramgini.wordpress.com/2017/02/12/the-ramaya%e1%b9%87a-and-a-para-ramaya%e1%b9%87a-in-numbers-i-epic-as-religion/) about the structure of रामायण and the earliest para-रामायण (the रामोपाख्यान) via numerical analysis and pointed out how the काण्ड-s show both a certain unity and divergence relating to the compositional history of the epic. Here, we extend that analysis further and draw some inferences regarding the above-stated issues.

The so-called Baroda "critical edition" is available in an electronic format and forms the basis of the below analysis. We have corrected several errors in that text; however, we cannot rule out that some errors remain, affecting some of the below numbers. Nevertheless, these will not affect any of the basic inferences presented below. The रामायण is mostly a metrical text with 17810 verses having 2 hemistiches each. There are 79 verses with 1 hemistich, i.e., standalone  $\tfrac{1}{2}$  verses; 576 verses with 3 hemistiches:  $1\tfrac{1}{2}$  verses; 5 with 4 hemistiches which are essentially agglomerations of 2 complete verses. It is unclear if some of the 1 and 3 hemistich verses were originally complete verses that lost one hemistich. However, many of these odd-hemistich verses are genuine "capping" verses that occur at the ends of sarga-s. The 17810 "properly formed verses" fall into the below metrical classes (Table 1)

**Table 1**

  Syllables     Frequency Meter

  - ---------- ----------- --------------------
  32                16949 अनुष्टुभ् (श्लोक)
  44                  476 त्रिष्टुभ् (उपेन्द्रवज्रा)
  48                  285 Jagati (वंशस्थ)
  50                   26 पुष्पिताग्रा
  45                   22 
  47                   20 
  46                   16 अपरवक्त्रा etc.
  33                    8 
  52                    7 रुचिरा

The primary meters are given in the third column with major, specific subtypes in brackets. It should be noted that the type in the bracket is just a widespread version and not the sole version found in the epic. For example, we have त्रिष्टुभ्-s of other types like जाया, buddhi, कीर्ति, etc. in addition to the common upendravajra. Likewise, with the jagati-s we have versions like कुमारी in addition to the prevalent वंशस्थ. The dominant meter is, of course, the श्लोक अनुष्टुभ्. Now, some verses do not match any meter. Since we did not individually check all of them, some may be errors in the preparation of the electronic text --- indeed, we corrected several of these. However, some of the 33s are genuine hypermetrical अनुष्टुभ्-s, like the famous ancient statement in the second hemistich that is hypermetrical:

[इदं भुङ्क्ष्व महाराज प्रीतो यद् अशना वयम् ।]{style="color:#0000ff;"}\
[यद् अन्नः पुरुषो भवति तद् अन्नास् तस्य देवताः ॥]{style="color:#0000ff;"}\
O great king, be pleased and partake this, such food as we \[eat],\
for the gods are offered the \[same] as what food the man takes.

The 45 syllabled verses seem to be hypermetrical त्रिष्टुभ्-s, and the 47 syllabled ones seem to be primarily hypometrical jagati-s. Thus, there appears to be a total of about 50 hypo/hyper-metrical verses among the "properly formed" verses  $\approx .28\%$ . The 46 syllabled verse is a bit of a mystery. Many of these can be shown to be अपरवक्त्रा-s; however, several do not match the अपरवक्त्रा properly. It is not clear if these were variant अपरवक्त्रा-s that are no longer in vogue or an error of transcription or something else.

This pattern of strong metricality is in contrast to the Veda. Looking at the most metrical of the Vedic texts, the ऋग्वेद, we find below distribution (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/12/rv_syl1.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad Frequency of verses of a given syllable count in the ऋग्वेद.
```{=latex}
\end{center}
```


The RV widely uses the गायत्री meter (2nd most common) that fell out of vogue in the later Sanskrit tradition. However, the other widely used meters अनुष्टुभ् (4th most common), त्रिष्टुभ् (most common) and Jagati (3rd most common), are shared with the epic tradition. We also have internal evidence from the श्रुति that their syllable count was precisely as in the later dialect, like in the epic. Hence, it is striking to note that, unlike in the epic, the meter is far more loosely maintained in the RV, with a dominance of hypometrical verses. This suggests that whereas the रामायण was composed more or less in the same dialect as it has come down to us, the RV was likely originally composed in an older dialect closer to the PI-Ir state, with a distinct system of संधि-s than in later Sanskrit. The language in which it has come down to us has shifted in register closer to later Sanskrit, with the new संधि-s resulting in losses of syllables from the old language. In a smaller number of cases, this shift in register has also resulted in the likely resolution of old संधि-s and consequent hypermetricality. This shall be separately discussed in the future in the context of the Veda.

We shall next look at the distribution of the different meters in each काण्ड per 1000 proper verses in Table 2.

**Table 2**

![ram_Table2](https://manasataramgini.wordpress.com/wp-content/uploads/2021/12/ram_table2.png){width="75%"}

Based on this distribution we can compute the Euclidean distance between काण्ड-s and construct an unrooted single linkage tree (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/12/ram_kanda_tree.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad Relationship between काण्ड-s based on distribution of meters.
```{=latex}
\end{center}
```


To better understand the above groupings, we next go down to the sarga level and compute two metrics for each sarga in a काण्ड: (1) metrical heterogeneity, i.e., the mean syllable count per sarga and (2) length of a sarga in number of verses (as previously discussed). The metrical heterogeneity measures how "pure" a sarga is in terms of the meter. For example, a sarga composed entirely of अनुष्टुभ्-s will have metrical heterogeneity of 32. We show the plots of these metrics in Figure 3.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/12/ramayana_sarga.png){width="75%"}
```{=latex}
\end{center}
```

\
Figure 3. Sarga heterogeneity metrics by काण्ड.

Here, we can see that the काण्ड-s 1 and 7 are dominated by sarga-s with pure अनुष्टुभ्-s of similar mean length, explaining their grouping in the tree. The काण्ड-s 2, 3, and 4 are somewhat more heterogeneous in terms of their metrical structure and have similar mean lengths consistent with their grouping. Finally, काण्ड-s 5 and 6 are metrically the most heterogeneous with on an average significantly longer sarga-s. This structure and grouping throw some light on the history of the text. काण्ड 1 (बाल) states that वाल्मीकि composed the epic in 6 काण्ड-s along with an "uttara" or addendum: [तथा सर्ग-शतान् पञ्च षट् काण्डानि तथोत्तरम् ॥ (]{style="color:#0000ff;"}From Vulgate; absent in "Critical"). This hints that there was a memory of the uttara-काण्ड (7) as an addendum to the core 6. This is apparent from the nature of several parts of काण्ड-7, which fill in the narrative gaps in the core काण्ड-s or provide explanatory commentary. The same feature is evident in काण्ड 1 (including the above statement). Their grouping, together with an अनुष्टुभ्-rich structural uniformity reminiscent of the पुराणिक verses, suggests that they are likely entirely (7) or partly (1) the product of a later redactional effort to fill in parts of the epic that were either lost or needed further explanation/augmentation. Even the supposed names of the sons of राम, कुश and Lava, appear to be derived from an old term for a minstrel, the कुशीलव (the twins are mentioned as such in the beginning of 1 and end of 7), suggesting the emergence of these parts within the oral tradition of such minstrels, which used the relatively-easy-to-compose अनुष्टुभ्-s uniformly. काण्ड 1 also hints that the original epic had two subsections to it:

[काव्यं रामायणं कृत्स्नं सीतायाश् चरितं महत् ।]{style="color:#0000ff;"}\
[पौलस्त्य वधम् इत्य् एवं चकार चरितव्रतः ॥]{style="color:#0000ff;"}\
He (वाल्मीकि) composed the great poem, the रामायण, the story of सीता.\
Even so, he of firm vows composed that known as the slaying of the Paulastya.

We could interpret this as implying two larger sections of the narrative centered on the tale of सीता (i.e., her birth and marriage to राम, etc.) and the killing of रावण. Thus, we suspect the two structurally unified parts the काण्ड-s 2-4 (probably with parts of the ancestral 1) formed the first of these sections, and काण्ड-s 5-6, which are again structurally similar, and organically related to the killing of रावण, formed the second. काण्ड 5 (Sundara), which shows maximal metrical and length heterogeneity, was likely composed thus on purpose ([as we noted before](https://manasataramgini.wordpress.com/2017/02/12/the-ramaya%e1%b9%87a-and-a-para-ramaya%e1%b9%87a-in-numbers-i-epic-as-religion/)). This काण्ड foreshadows the tendency in later classical काव्य, where the kavi-s set aside specific sections of their work, to showcase their virtuosity in terms of composing in a diverse array of meters or displaying various अलंकार-s, including चित्रकाव्य. We do not see the much longer and complex metrical expressions of classical काव्य nor the constraint-based composition using techniques of चित्रकाव्य in the सुन्दरकाण्ड. Yet, it is clear that काण्ड 5 elegantly intersperses diverse meters on top of the basic अनुष्टुभ् background to bring about pleasing changes of cadence. Like later काव्य-s, it also has entire sarga-s in long meter-s.

Next, we study the structure of the sarga-s by themselves and see if we can discern: (1) specific structural classes of sarga-s; (2) whether the sarga class has a relationship to the काण्ड it comes from. To do this, we first construct a matrix where every row corresponds to a sarga. The first 9 columns correspond to the fractions of the sarga in verses of a particular number of syllables (32, 33, 44 etc.). Column 10 corresponds to the length of the sarga in number of verses normalized by the longest sarga (5.1). We then use this matrix for unsupervised classification of the sarga-s using the random forest predictor as implemented by Breiman and Cutler. Briefly, this is a machine-learning method that uses an ensemble of individual classification tree predictors (i.e., decision trees to classify the given data). The decision process specified by an individual tree uses each observation to vote for one "class" and the forest of such trees is used to choose the class with the plurality of votes. For the classification process, the number of randomly selected variables that are searched for deciding the best split at each node in the tree is taken to be  $\left\lfloor\sqrt{n}\right\rfloor$ , where  $n$  is the total number of variables. The unsupervised mode works by making the RF predictor discriminate the observed (i.e., the above sarga matrix in our case) from synthetically produced data. The synthetic data is made by randomly sampling from the product of marginal distributions of the variables from our input matrix. As a result, one can obtain a proximity matrix between the input observations (i.e., sarga-s in our case). This proximity matrix can be converted to a distance matrix and used as the input for multidimensional scaling (MDS), representing the observations as points in an Euclidean space of  $n$  dimensions, with the Euclidean space distances between these points approximately equal to the distances in the distance matrix. By choosing the first two dimensions in this Euclidean space and plotting them, we can reduce dimensionality and obtain visual clusters or classes of the observations. Figure 4 shows the first two dimensions of the MDS plot for our data following unsupervised random forest classification (655 trees and minimal terminal node size of 90).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/12/rama_sarga_class_in.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad
```{=latex}
\end{center}
```


We see 4 broadly delineated clusters, although their "smearing" indicates a degree of a continuum. Examining each cluster individually, we see that they provide a meaningful classification of the sarga-s: (1) The first class (ellipse to the left) is composed of sarga-s that are pure अनुष्टुभ्-s. (2) The second class (top right ellipse) comprises of sarga-s that have अनुष्टुभ्-s combined with त्रिष्टुभ्-s. The core of this class is defined by a very characteristic form of the sarga that contains a त्रिष्टुभ् as the capping (final) verse. (3) The third class (bottom right ellipse) consists of sarga-s combining अनुष्टुभ्-s with jagati-s. The core of this class uses a jagati, usually of the वंशस्थ type, as the capping verse. (4) Finally, the central ellipse contains a group of sarga-s typified by interspersing of different meters on an अनुष्टुभ् background or those with irregular (hypo/hyper-metrical) verses.

![rAmAyaNa_rf_cls](https://manasataramgini.wordpress.com/wp-content/uploads/2021/12/ramayana_rf_cls.png){width="75%"}Figure 5

One can see from the color-coding of the sarga-s by काण्ड in Figure 4 that there might be distinct patterns --- e.g., class 1 appears enriched in काण्ड-s 1 and 7, which are rare in the other classes. Hence, we next examined if each of the above classes differ significantly in terms of the काण्ड-s from which their sarga-s are drawn (Figure 5). This confirms that the काण्ड-s 1 and 7 dominate class 1  $(\chi^2, p=2.87 \times 10^{-16})$ . The pattern is inverted for class 2 with त्रिष्टुभ् capping verses. This is in keeping with the above proposal of काण्ड-s 1 and 7 having a distinct compositional pattern and history. Another notable feature that emerges is the enrichment of काण्ड 2 in the class with jagati capping verses  $(\chi^2, p=5.62 \times 10^{-15})$ . In conclusion, this suggests that the older ऐतिहासिक काव्य tradition had a style of capping a long run of अनुष्टुभ्-s with a त्रिष्टुभ् or a jagati to mark the end of a section. This practice appears to have given way to the purely अनुष्टुभ् composition, probably among the कुशीलव-s who subsequently preserved the itihasa-s and the emerging पुराण-s as an oral tradition.

