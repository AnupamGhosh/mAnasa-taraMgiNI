
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some biographical reflections on visualizing irrationals](https://manasataramgini.wordpress.com/2021/10/26/some-biographical-reflections-on-visualizing-irrationals/){rel="bookmark"} {#some-biographical-reflections-on-visualizing-irrationals .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 26, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/10/26/some-biographical-reflections-on-visualizing-irrationals/ "6:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

In our childhood, our father informed us that, though the school told us that  $\pi = \tfrac{22}{7}$ , it was not valid. However, he added that for "small fractions" \[Footnote 1] it was a great approximation. Moreover, the numerical problems, which we would encounter in the tests, were designed with radii, etc., as "round" multiples of 7; hence, it was alright for व्यवहार. He also told us that in reality,  $\pi$  was irrational and that fractions like  $\tfrac{22}{7}$  were only approximations thereof. We asked why  $\tfrac{22}{7}$  was the best of the "small fraction" approximations. He asked us to figure that out for ourselves but told us that we could get better approximations with bigger fractions:  $\tfrac{333}{106}$  and  $\tfrac{355}{113}$ .

Armed with this information, not being mathematically too endowed, we had to think long about it before arriving at an algorithm to understand this. It went thus (written using post facto terminology):

 1.  Given that we had a lot of sheets of graph- and ruled- paper, we first sought to draw a line with slope equal to  $\pi$ , i.e.,  $y=\pi x$ . This would ideally need the squaring of a circle. However, at that point, we did not know of Ramanujan's constructions and accurately re-doing the yavana constructions with curves like the spiral and kampyle lay in the future. However, we had those approximations with the "larger fractions" that our father had given us, and with a hand calculator, we could verify that they provided more than the accuracy we needed for testing out  $\tfrac{22}{7}$ . Accordingly, we counted out 113 and 355 small squares on the X- and Y-axes respectively on the graph paper and carefully drew the line  $y=\tfrac{355}{113} x$  with a pencil that had an expensive, thin graphite refill.

 2.  By definition, the line passed through (0,0). After that, we tracked every time the line cut the horizontal (red crosses) or vertical (green circles) grid lines of the lattice (Figure 1).

 3.  We saw that the line first cut the horizontal grid lines at 1, 2, 3 and the vertical grid line for the first time at 1 after cutting the horizontal line 3. The horizontal cut at 3 and the vertical cut at 1 come close to each other (Figure 2). This immediately told us the obvious:  $\pi \approx 3$ , a reasonable first-order approximation, seen in several ancient traditions, like the ऋग्वेद, where it is embodied by the god Trita. Going along, we saw an even closer approach between the vertical cut at 6 and the horizontal cut at 19 (Figure 1). This yielded  $\pi \approx 3.1\overline{6}$ , which again reminds one of the kinds of values seen in old traditions like the construction for squaring the circle of the मैत्रायनीय-s or the popular value of  $\sqrt{10}$  seen in some old Indian texts or the value in the Rhind papyrus of the Egyptians. At  $\tfrac{22}{7}$ , we saw a near-perfect matching of the vertical cut at 7 and the horizontal cut at 22. Further ahead, we saw another close approach between the horizontal cut at 25 and the vertical cut at 8, but this was worse than the approach at  $\tfrac{22}{7}$ . It only got worse from there till the higher fraction  $\tfrac{333}{106}$ , though there was a fairly good approach at  $\tfrac{113}{36}$  (still worse than  $\tfrac{22}{7}$ ). Thus, we had seen for ourselves that  $\tfrac{22}{7}$  was indeed the best approximation among the "small fractions". This childhood experiment was to leave us with a lifelong interest in irrationals and their fractional approximations, mirroring the fascination for things like the चक्रवाल seen in our intellectual tradition.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/10/irrationals_fig1.png){width="75%"}
```{=latex}
\end{center}
```



**Figure 1.** The cuts corresponding to  $\pi$ 

The only problem with using such a method for finding rational approximations for numbers like  $\pi$  or  $e$  is that you should know their value to a high degree of accuracy a priori or be able to construct them using special curves. For example, if you can construct a line of slope  $e$  you can determine  $e \approx \tfrac{19}{7}$  --- not as good as the approximation for  $\pi$  but has a nice symmetry with it telling us that  $\tfrac{\pi}{e} \approx \tfrac{22}{19}$ . However, for irrationals amenable to Platonic construction, e.g.,  $\sqrt{2}$  this method can be used effectively to find small rational approximations, if any. Thus, we can arrive at  $\tfrac{17}{12}$  as a rough and ready approximation for  $\sqrt{2}$  (Figure 2).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/10/irrationals_fig2.png){width="75%"}
```{=latex}
\end{center}
```

**Figure 2.** The cuts corresponding to  $\sqrt{2}$ 

A few years later, these experiments led us (independently of any literature on the topic) to the idea that every irrational number can be represented as an infinite string of 0s and 1. We take a line of the form  $y=mx$ , where  $m$  is an irrational number. Based on the above approach, we write 1 if the said line cuts a horizontal grid-line and 0 if it cuts the vertical grid-line of the lattice. Since the line  $y=mx$  will pass through a lattice point only at (0,0), we start the sequence by writing 01. Thus, by moving along the line, we get an infinite sequence representing  $m$  as a string of 1s and 0s. One can see that as the number of elements in this sequence  $n \to \infty$  the ratio of 1s to 0s tends to  $m$ . Thus, for  $\sqrt{2}$ , the first 50 elements of this sequence are:

**01101011010110101011010110101011010110101101010110101**

We also represented the same as a "string of pearls" diagram (Figure 3). This kind of representation reminds one of the differentiated cells in linear multicellular arrays in biology, for example, the occurrence of heterocysts in a filament of *Anabaena*. This led us to realize that the irrationals have a deep structure to them that is not apparent in the seemingly random distribution of digits in their decimal representation.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/10/irrationals_fig3.png){width="75%"}
```{=latex}
\end{center}
```



**Figure 3.** The "string of pearls" representation of various irrationals.

As we have seen before on these pages, this structure can manifest variously. One place where we found this structure as manifesting was in strange non-chaotic attractors, which were first described by Grebogi et al. in the mid-1980s. These attractors are "strange" because they manifest a fractal structure (see below) but do not exhibit chaos, i.e., the attractor has the same structure irrespective of the values with which the map is initiated. The map which we found is distinct from that of Grebogi et al. but produces similar behavior. It is of the form:

 $$\theta_{n+1}= 2l\sin(\theta_n)\cos(x_n)$$ 

 $$x_{n+1}=(x_n+2\pi m) \mod (2\pi)$$ 

Here  $l$  is a constant for which we can choose some value  $l \ne 0$ , but we set it to 1.03 simply for the aesthetics of the "irrational spectrum" (see below) and the manifestation of its fractal structure (Figure 4). The other parameter  $m$  is the irrational number under consideration. Irrespective of the initial conditions (as long as both  $x_0, \theta_0 \ne 0$ ), the attractor takes the same form (Figure 4) that is determined only by the irrational  $m$ , as long as  $l$  remains the same. We term this the "spectrum" of the irrational. While the attractors appear superficially like curves symmetric about the  $x$ -axis with positive and negative limbs, we can show that they are actually fractal. The iterates do not proceed along the curve but jump from lower limb to upper limb and vice versa, and between any two arbitrarily close points on a given limb, one can find a finer structure of points jumping between the limbs, thereby establishing its fractal structure. Indeed, as  $l$  increases, the attractor acquires a structure reminiscent of the classic fractal structure seen in maps like the logistic map. The spectrum itself depends on the fractional part of the irrational  $m$  (Figure 4). This can be established by comparing the spectra for  $m=\phi, \tfrac{1}{\phi}$  (the Golden ratio), which have the same fractional part --- they are identical. In contrast, the spectra of  $\sqrt{2}, \tfrac{1}{\sqrt{2}}$  are not the same, keeping with the differences in their fractional parts.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/10/irrationals_fig4.png){width="75%"}
```{=latex}
\end{center}
```

**Figure 4.** The spectra of various irrationals initiated with  $\theta_0=0.001, x_0=0$ 

While this type of "spectrum" captures the structure hidden in the fractional part, we also designed a second type of "spectrum" that captures the structure relating to the rational approximations we saw in the opening discussion. This is defined as the below summatory sequence:

 $\displaystyle f= \sum_{j=1}^{n} -1^{\lfloor j \cdot m \rfloor}$ ,

Here  $m$  is the irrational number under consideration. Figure 5 shows a step plot of  $f$  for  $m=\pi$  and  $j=1..300$ . We observe that it has a modular structure that builds up into a larger "wave". At the lowest level, we have an up-down step of size 1. Then we see repeats of such steps with a transition between repeats at multiples of 7 (red lines). This continues until the "trough" of that wave is reached at 106 (green line). Then an ascent begins at 113, which is complete after a further 113 steps (violet line). Note that the numbers 7, 106 and 113 are the denominators of the successive best rational approximations of  $\pi$ . Thus, these sequences have a fractal structure with larger and larger cycles whose size is determined by the denominators of the successive rational approximations.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/10/irrationals_fig5.png){width="75%"}
```{=latex}
\end{center}
```



**Figure 5**. Step plot of the summatory spectrum of  $\pi$  till  $n=300$ .

Hence, those irrationals with multiple small rational convergents tend to show more "overtones" resulting in a more complex structure (e.g.,  $\gamma$  or  $\sqrt{2}$ ; Figure 6)
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/10/irrationals_fig6.png){width="75%"}
```{=latex}
\end{center}
```

**Figure 6.** Step plot of the summatory spectrum  $f$  of various irrationals till  $n=300$ .

From the above plots, it becomes apparent that while  $f$  for all irrationals will be fractal, at least small scales some show greater "complexity" than others, e.g., compare the plots for  $\pi$  or  $\zeta(3)$  with those for  $\phi$  or  $\sqrt{2}$ . We wondered if there might be a way to define this visual impression quantitatively? We did so by defining sequences  $f_1$ , the sum of the terms of  $f$  in a sliding window of size  $l$ . For each irrational, we considered all  $f_1$  computed for  $l=1..25$ . Then for each of these summatory sequences  $f_1$  for a given irrational, we counted the number of times the curve changes direction, i.e., changes from going down to going up and vice versa. We then normalized the direction-change counts with each  $l$  for a given irrational by the maximum number of direction changes seen for that irrational. The mean of this normalized value gives a measure of the complexity, i.e., the "wriggliness" of the curve. This is shown as  $c$  in Figure 7 along with a plot of  $f_1$  for  $l=12$  and shows that the measure  $c$  indeed matches the visual impression from Figure 6.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/10/irrationals_fig7.png){width="75%"}
```{=latex}
\end{center}
```



**Figure 7.** Step plot of the sliding window sequence  $f_1$  of various irrationals with  $l=12$ 

  - -----------------------------------------------------------------------

Footnote 1: Fractions with small numerators and denominators

