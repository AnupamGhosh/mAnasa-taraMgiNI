
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Matters of religion: वरुणावसिष्णवम्, अग्नावसिष्णवम् and the व्यहृति-s](https://manasataramgini.wordpress.com/2021/06/21/matters-of-religion-varu%e1%b9%87avasi%e1%b9%a3%e1%b9%87avam-agnavasi%e1%b9%a3%e1%b9%87avam-and-the-vyah%e1%b9%9bti-s/){rel="bookmark"} {#matters-of-religion-वरणवसषणवम-अगनवसषणवम-and-the-वयहत-s .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 21, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/06/21/matters-of-religion-varu%e1%b9%87avasi%e1%b9%a3%e1%b9%87avam-agnavasi%e1%b9%a3%e1%b9%87avam-and-the-vyah%e1%b9%9bti-s/ "6:04 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Like the clouds lifting after the monsoonal deluge to unveil the short-lived comforts of early autumn, the metaphorical pall over the nation cast by the [engineer's virus](https://manasataramgini.wordpress.com/2014/05/06/the-engineer-the-dead-fish-and-the-bag-of-earth/) was lifting. Somakhya and Lootika were at the former's parents' house, relieved that they had survived and overcome the tumultuous events. Somakhya's parents asked them to offer the वरुणावसिष्णव and associated oblations as ordained by the भृगु-s and आङ्गिरस-s of yore. Vrishchika and Indrasena were also present as observers of the rite. Somakhya donned his turban and identified himself with the god Indra to initiate the rite, for indeed the श्रुति has said: [तद् वा एतद् अथर्वणो रूपं यद् उष्णीषी ब्रह्मा ।]{style="color:#0000ff;"} --- that ब्राह्मण who is turbaned is indeed of the form of the Atharvan. He explained to Indrasena that the श्रुति holds the Indra took the shape of the Atharvaveda in his turbaned form to protect the ritual of the gods from the दानव-s. Indrasena: "Indeed, even the primordial श्रुति records that form of Indra in the ऋक् of the काण्व-s:

[यज्ञ इन्द्रम् अवर्धयद् यद् भूमिं व्य् अवर्तयत् । चक्राण ओपशं दिवि ॥]{style="color:#0000ff;"}\
The ritual magnified Indra \[with praise] when he made the earth rotate, making \[himself] a turban in (= of) heaven.\
One may note play on the word ओपश; by taking it as neuter one could also interpret is as the pillar or the axis of heaven."

Then, Somakhya and Lootika took their seat before the fire on the hide of a reddish brown ox strewn with darbha grass. Thereafter, they performed an आचमन and प्रोक्षण with the incantations: [अपां पुष्पं मूर्तिर् आकाशं पवित्रम् उत्तमम् । इन्द्र जीव सूर्य जीव देवा जीवा जीव्यासम् अहम् । सर्वम् आयुर् जीव्यासम् ॥]{style="color:#0000ff;"} \[The flower is the form of the waters, the empty space \[and] that which the most pure. Enliven, o Indra; Enliven o सूर्य. Enliven, o gods. May I live. May I complete my term of life].

Thereafter, Somakhya meditated on the special connection of the founder of his race to god वरुण and uttered the incantation establishing his connection to the founder of his lineage, great भृगु: [तद् भृगोर् भृगुत्वम्। भृगुर् इव वै स सर्वेषु लोकेषु भाति य एवं वेद ॥]{style="color:#0000ff;"} \[That \[connection with वरुण] is the भृगु-ness of भृगु. He who knows thus shines in all the worlds like भृगु]. He recited the formula: [ॐ सर्वैर् एतैर् अथर्वभिश् चाथर्वणैश् च कुर्वीय॥]{style="color:#0000ff;"} [ॐ May I perform \[this rite] by means of all these incantations of Atharvan and the आथर्वण-s]. [ॐ मन्त्राश् च माम् अभिमुखीभवेयुः]{style="color:#0000ff;"} [।]{style="color:#0000ff;"}[ॐ may the \[AV] mantra-s face me \[favorably]]. Somakhya then explained to Indrasena and Vrishchika: "The श्रुति holds that like a mother can be killed by the fetus she bears, the mantra-s can kill the holder if he improperly applied them or has not been diligent in their study. Hence, he must utter this incantation beginning with ॐ. The प्रणव indeed protects him from such backfiring."

He muttered the सावित्र incantation-s to the god सवितृ as per the teaching of the great ब्राह्मण श्वेतकेतु, the son of उद्दाकक आरुणि:\
[ॐ भुर् भुवः स्वः तत् सवितुर्... प्रछोदयात् ॥]{style="color:#0000ff;"}: This first cycle is done with the 3 महाव्याहृति-s.\
[ॐ भूर् जनत् तत् सवितुर्... प्रछोदयात् ॥]{style="color:#0000ff;"}: Somakhya touched Lootika with a darbha-bunch and she made an oblation as is appropriate for the sacrificer's wife in the fire at the utterance of [स्वाहा]{style="color:#0000ff;"} ([इदं न मम ॥]{style="color:#0000ff;"})\
[ॐ भुवो जनत् तत् सवितुर्... प्रछोदयात् ॥]{style="color:#0000ff;"}: Somakhya's parents stepped forward and made an offering with a silent स्वधा call and touched water.\
[ॐ स्वर् जनत् तत् सवितुर्... प्रछोदयात् ॥]{style="color:#0000ff;"}: Somakhya made an oblation with a [स्वाहा]{style="color:#0000ff;"} ([इदं न मम ॥]{style="color:#0000ff;"})\
[ॐ भूर् भुवः स्वर् जनद् ओम् तत् सवितुर्... परो रजसे 'सावदो३म्॥]{style="color:#0000ff;"}: Somakhya made an oblation with a [वौषट्]{style="color:#0000ff;"} uttered loudly ([इदं न मम ॥]{style="color:#0000ff;"})

Then he proceeded to the main oblations:\
[श्रौषड्]{style="color:#0000ff;"}\
[ययोर् ओजसा स्कभिता रजांसि]{style="color:#0000ff;"}\
[यौ वीर्यैर् वीरतमा शविष्ठा ।]{style="color:#0000ff;"}\
[यौ पत्येते अप्रतीतौ सहोभिर्]{style="color:#0000ff;"}\
[विष्णुम् अगन् वरुणं पूर्वहूतिः ॥]{style="color:#0000ff;"}

By whose power the domains of space were stabilized,\
by whose energy, the most energetic and mightiest,\
who lord it unopposed by their powers,\
to \[that] विष्णु and वरुण have gone the first offerings.

[यस्येदं प्रदिशि यद् विरोचते]{style="color:#0000ff;"}\
[प्र चानति वि च चष्टे शचीभिः ।]{style="color:#0000ff;"}\
[पुरा देवस्य धर्मणा सहोभिर्]{style="color:#0000ff;"}\
[विष्णुम् अगन् वरुणं पूर्वहूतिः ॥]{style="color:#0000ff;"}\
[वौषट् + इदं वरुणाविष्णूभ्यां न मम ॥]{style="color:#0000ff;"}

In whose direction is that which shines forth,\
\[whatever] that vibrates and observes with power\
from ancient times by the god's law with might,\
to \[that] विष्णु and वरुण have gone the first offerings.

[ॐ भूह् प्र तद् विष्णु स्तवते वीर्याणि]{style="color:#0000ff;"}\
[ॐ भुवो मृगो न भीमः कुचरो गिरिष्ठाः ।]{style="color:#0000ff;"}\
[ॐ स्वः परावत आ जगम्यात् परस्याः ॥]{style="color:#0000ff;"}\
[ॐ भूर् भुवः स्वर् जनद् वृधत् करद् रुहन् महत् तच् छम् ॐ विष्णवे स्वाहा + इदं विष्णवे न मम ॥]{style="color:#0000ff;"}

Thus, he praises forth his heroic deeds, विष्णु is\
like a dreadful lion wandering, stationed in the mountains\
From the distant realm may he come close.

[श्रौषड्]{style="color:#0000ff;"}\
[अग्नाविष्णू महि तद् वां महित्वम्]{style="color:#0000ff;"}\
[पाथो घृतस्य गुह्यस्य नाम ।]{style="color:#0000ff;"}\
[दमे-दमे सप्त रत्ना दधानौ]{style="color:#0000ff;"}\
[प्रति वां जिह्वा घृतम् आ चरण्यात् ॥]{style="color:#0000ff;"}

O Agni and विष्णु mighty is your might;\
you two drink from name of the ghee's secret.\
In home after home you two place the seven gems.\
may your tongue move here to meet the ghee.

[अग्नाविष्णू महि धाम प्रियम् वां]{style="color:#0000ff;"}\
[वीथो घृतस्य गुह्या जुषाणौ ।]{style="color:#0000ff;"}\
[दमे-दमे सुष्टुत्या वावृधानौ]{style="color:#0000ff;"}\
[प्रति वां जिह्वा घृतम् उच् चरण्यात् ॥]{style="color:#0000ff;"}\
[वौषट् + इदं अग्नाविष्णूभ्यां न मम ॥]{style="color:#0000ff;"}

O Agni and विष्णु mighty is your dear domain;\
may you two savor the secret enjoyment of the ghee\
In home after home you two are magnified by good praise-chants.\
may your tongue flicker upward to meet the ghee.

[ॐ भूह् यस्योरुषु त्रिषु विक्रमनेष्व् अधिक्षियन्ति भुवनानि विश्वा ।]{style="color:#0000ff;"}\
[ॐ भुव उरु विष्णो वि क्रमस्वोरु क्षयाय नस् कृधि ।]{style="color:#0000ff;"}\
[ॐ स्वः घृतम् घृतयोने पिब प्र-प्र यज्ञपतिं तिर ॥]{style="color:#0000ff;"}\
[ॐ भूर् भुवः स्वर् जनद् वृधत् करद् रुहन् महत् तच् छम् ॐ विष्णवे स्वाहा + इदं विष्णवे न मम ॥]{style="color:#0000ff;"}

In whose wide three strides all the worlds are laid down;\
stride widely O विष्णु for wide lordship; make \[that lordship] for us;\
Drink the ghee, O source of ghee; prolong the lord of the ritual over and over!

[मम देवा विहवे सन्तु सर्व]{style="color:#0000ff;"}\
[इन्द्रवन्तो मरुतो विष्णुर् अग्निः ।]{style="color:#0000ff;"}\
[ममान्तरिक्षम् उरुलोकम् अस्तु]{style="color:#0000ff;"}\
[मह्यं वातः पवतां कामायास्मै ॥]{style="color:#0000ff;"}\
[ॐ भूर् भुवः स्वर् जनद् वृधत् करद् रुहन् महत् तच् छम् ओम् इन्द्रवन्तः स्वाहा ॥]{style="color:#0000ff;"}

May all the gods be at my ritual invocation;\
The Marut-s with Indra, विष्णु and Agni.\
Let the broad realm of the atmosphere be mine.\
May वात blow for favoring this wish of mine.

[यो नः स्वो यो अरणः सजात उत निष्ट्यो यो अस्मां अभिदासति ।]{style="color:#0000ff;"}\
[रुद्रः शरव्ययैतान् ममामित्रान् वि विध्यतु ॥]{style="color:#0000ff;"}

Whether one of ours or one who is in a truce, a kinsman or an alien, whosoever attacks us\
may Rudra releasing a shower of arrows pierce those enemies of mine.

[यः सपत्नो यो 'सपत्नो यश् च द्विषन् छपाति नः ।]{style="color:#0000ff;"}\
[देवास् तं सर्वे धूर्वन्तु ब्रह्म वर्म ममान्तरम् ॥]{style="color:#0000ff;"}

Whichever competitor or whichever non-competitor and whichever hater curses us,\
the gods shall injure him. The incantation is my inner armor.

![decoration1](https://manasataramgini.wordpress.com/wp-content/uploads/2021/06/decoration1.png){width="75%"}

Having concluded the after-rites Somakhya, Lootika, Indrasena and Vrischika left to savor the fresh air and the natural world, and engage in some ब्रह्मवाद on the hills beyond the late medieval temple of the awful चण्डिका. They stopped at the quadrangle in the low ground facing the stairs leading to the temple on the hill before a towering bastard poon tree. Vrishchika: "There used to be an old woman with a goat who used sit in the vicinity of this skunk tree. We used to feed her goat as a representative of the god कुमार. She has likely passed into the realm of विवस्वान्'s son along with her aja. Hope Rudra was kind to her when her time came. Indrasena, sometimes, thinking about you, as though seized by Skanda or विष्णु, I used to feed her goat hoping that Skanda might be kind to me." Indrasena: "O गौतमी, after all the meanderings, it seems, Skanda has brought you to your destination as he did to the काण्व and his goat." Lootika: "I also recall that Somakhya's family observes a कौमार rite on the आश्विन fullmoon, where they make a rare dish from payasya (curdled milk cheese; Iranic: पय्नीर्). They would offer some of that to the woman with the goat getting the leaf in which the dish was wrapped" Indrasena: "Is that a folk Atharvan rite?". Somakhya: "Yes, the folk Atharvan tradition holds that Skanda is the teacher of पैप्पलाद, one of the promulgators of the AV संहित-s, and the पौर्णमास्य rite is held in the honor of the enlightenment of पैप्पलाद."

It was a quiet time of the day with just a light stream of votaries and gawkers on the stairway to the temple. The four made their way up the steps, mostly in silent thought, to pay their respects to the enshrined परिवार-देवता-s and the wife of Rudra at the main shrine. Even as they were about to exit the circumambulatory path to resume their climb beyond the stair way further up the crag to the plateau beyond, Lootika was approached by a woman who wanted to fall at her feet. Lootika prevented her from doing so and she began pouring out a litany of medical troubles. Lootika signaled to her sister: "This lady seems to have mistaken me to be you." Vrishchika: "Stepped in and having briefly heard her out gave her some reassuring words and asked her to attend to her father's clinic." Somakhya and Indrasena instinctively felt their concealed guns and knives for a dasyu could always be lurking in the shades. Having reached their favored vantage point, the site of an old megalithic stone circle, they looked on at their city below. There seemed to be some hesitancy in returning to the old normal; hence, the air seemed cleaner and the horizon clearer. The, नक्षत्र of the day, the eye of Mitra and वरुण, had mounted the vault of the cloudless southern sky. Looking into the distance they saw that the fires in the yonder cemetery were far fewer than when Somakhya and Lootika had looked on from the same place during the height of the conflict. Lootika: "The lull between the storms." Vrishchika: "You think it is not yet over?" L: "The clash with the राक्षस-mata-s is like the fight between the Daitya-s and the Deva-s --- the bigger disease from the mleccha-s is that of the mind --- it will play out next with much spilling of blood --- but then our people could end in a whisper too."

Indrasena: "Coming to the rite of morning, I'd like understand more about the AV व्याहृति-s --- both the combination of the महाव्याहृतिस् with the incantations as also the connection of the व्याहृति-s to Maruta इन्द्रवन्तः."\
Vrishchika: "Could we please also have a broader discussion of role the महाव्याहृति-s and their transcendence by other व्याहृति-s across the Vaidika collections?"

Somakhya: "Alright, Vrishchika, let us lay the groundwork for the exploration desired by Indrasena by first addressing the ब्रह्मवाद on the व्याहृति-s in the श्रुति-s other than those of the Atharvan-s. Let us begin this discussion with testing your knowledge of the traditions regarding the व्याहृति-s in the traditions you are familiar with. Why don't you tell us what you know regarding the three महा-व्याहृति-s?"\
V: "The श्रुति of the Aitareya-s holds that the 3 व्याहृति-s are like connective tissue that holds together the three disjunct parts of the श्रुति in the form of the ऋक्-s, यजुष्-es and सामन्-s --- thus they are compared to procedures akin to reducing dislocations of joints or sewing up cut skin. Indeed, this analogy of the Aitareya-s provides early evidence for these medical procedures among the आर्य-s, which parallel those surgical and bone-setting procedures explicitly mentioned in the Veda of the Atharvan-s and having echoes among other Indo-Europeans, like in the Merseburg spell of the शूलपुरुष-s:\
[(भूर् भुवः स्वर्) ... एतानि ह वै वेदानाम् अन्तः श्लेषणानि यद् एता व्याहृतयस् । तद् यथात्मनात्मानं संदध्याद् । यथा पर्वणा पर्व यथा श्लेष्मणा चर्मण्यं वान्यद् वा विश्लिष्टम् संश्लेषयेद् । एवम् एवैताभिर् यज्ङस्य विश्लिष्टं संदधाति ॥]{style="color:#0000ff;"}

These are verily the internal bindings of the Veda-s, these व्याहृति-s. Even as one joins the one individual thing other separated thing; like setting one joint with another joint; like suturing with a cord, skin with another torn one. Even so, verily with these one joins the disjunct parts of the the ritual.

Thus, the suturing role of the व्याहृति-s is critical for the terminal स्विष्टकृत्-s for fixing the errors in the ritual.

S: "That is good. So, what do you know of the thesis of the transcendence of 3 महाव्याहृति-s?"\
V: "Well, the उपनिषत् of the तैत्तिरीयक-s holds that there are three primal or महाव्याहृति-s, भूर्, bhuvas and suvar; however, the sage माहाचमस्य held that there is a fourth, i.e. mahas. In his teaching mahas is privileged over the remaining three. He establishes four homologies between them and other entities. Those are the following: 1) He sees the three primary one भूर्, bhuvas and suvar as corresponding to the earth, the atmosphere and the space beyond. The fourth, mahas, is seen as the आदित्य, the sun, which causes the world material worlds to take form --- perhaps in more than one way --- by supplying the matter to make them and also the light by which their existence is perceived. 2) The next homology is to the first 3 and the sources of light --- the fire, the wind (he implies lightning here) and the sun. The reflected light of the moon is homologized to mahas --- here again we might see it as the ambient light that makes perception possible even the source themselves are invisible. 3) He also homologizes the first 3 with the 3 categories of incantations in the श्रुति, the ऋक्-s, the यजुष्-es and the सामन्-s, and mahas with the brahman, which is to be understood here as the प्रणव. 4) The next homology is between the व्याहृति-s and the inhalation, exhalation, and retention in the प्राणायाम cycle. Specifically, in that context mahas may be understood as the air. However, I hold that from the श्रुति we may infer that what was meant was more general --- the physiological process of nutrient uptake, export of unwanted and secreted compounds and the anabolic processes. The free-energy-providing material in this process, i.e. the nutrients, is the fourth, mahas. Thus, as there are four homologies in each set with a total of 4 sets, the व्यहृति-s are seen as being 16-fold. The summary of these linkages is presented as the understanding that the first three are the limbs of the physical body and mahas corresponds to the consciousness. Thus, mahas in different domains is equated respectively with the link substance, the work-generating substance, the diffuse or reflected light that pervades the universe and the mantra essence --- all of these are seen as analogies for the nature of consciousness with respect to matter."

Somakhya: "Excellent upa-गौतमी. Dear Lootika is there something you might want to add to what your sister has just expounded from other Vedic traditions?"\
Lootika: "Sure. I actually learnt of the multiple expressions of व्याहृत्युत्पत्ति in the scriptural readings I did with your mother. This theory of माहाचमस्य, introducing the fourth व्याहृति perhaps led up to the theory of multiple व्याहृति-s in both Atharvan and यजुष् traditions. This is clearly a departure from the triple व्याहृति system expounded in the ब्राह्मण of the वाजसनेयिन्-s, that of the Aitareya-s and the उपनिषद् ब्राह्मण of the Jaimini singers. There explicitly प्रजापति is described as generating only 3 व्याहृति-s."

Indrasena: "Indeed. However, each of those accounts have notable points --- one may see a gradual build up of concepts within the 3 व्याहृति system that led to the emergence of the fourth. In the श्रुति of the वाजसनेयिन्-s, we have an account that might be seen as retaining the archaic form which the thesis of माहाचमस्य eventually emerged. That account describes the heat (tapas) of प्रजापति as the basis for the emanation of the 3 worlds. Since these worlds were heated by his tapas they emanated the same 3 primary sources of light (the deities Agni, वायु and आदित्य) mentioned by the TU. That tapas causing those lights to radiate heat spawned the collections of the 3 types of mantra-s of the श्रुति. His tapas then caused those mantra collections to radiate heat from which प्रजापति extracted three generative substances (शुक्र-s) that are the व्याहृति-s. We might trace the origins of the two other traditions, which Lootika just mentioned, from such a foundation --- one present in the सामवैदिक tradition of the जैमिनीय-s and the other in the Aitareya-ब्राह्मण.

In the former, प्रजापति is not presented in a protogonic context, but is competing with the other gods, probably reflecting the tension between the surging प्राजापत्य religion among our people and the older आर्य-deva-dharma. प्रजापति conquered the triple-world with the 3-fold mantra collection. Fearing that the other gods might see the same and take over his conquest, he extracted the essence of the ऋक्-s uttering भूः. That became the earth and its essence streamed forth as Agni. From the यजुष्-es he extracted the essence with भुवः and that formed the atmosphere and streamed forth as वायु. The सामन्-s were distilled with the सुवः call and they formed the the heaven, from which the essence streamed forth as the आदित्य. However, there was one अक्षर he could not distill into an essence, namely the प्रणव. That remained by itself and became वाच्.

In the Aitareya text, we have a cosmogony closer to that the of वाजसनेयिन्-s, wherein प्रजापति's heat generated the triple-world. As in the former account, by his heating of those, the 3 luminaries emerged and by heating those the 3 mantra-s collection were generated. By heating those again the generative essences (शुक्र-s), which are the 3 primary व्याहृति-s emerged. But in this account those were heated further to generate 3 phonemes: A, U, M, which प्रजापति got together to generate the प्रणव, ॐ. Thus, here too, as in the जैमिनीय-उपनिषद्-ब्राह्मण we see that there is something beyond the 3 basic व्याहृति-s, namely the प्रणव. It is the प्रणव that माहाचमस्य equated to the fourth व्याहृति mahas in his thesis and that which appears as the final व्याहृति of the Atharvan-s. Thus, we see an evolutionary process within the श्रुति which paved the way for the व्याहृति-s beyond the primary three via the concept of the प्रणव."

Lootika: "There is no mention of the triple व्याहृइति-s in the oldest layer of our tradition, the ऋक्-संहिता. Now, that could be because they are specialized calls that don't fit into the metrical incantations. However, in all the accounts of व्याहृत्युत्पत्ति, which we have discussed so far, we see the central role of the protogonic god प्रजापति. Is that the व्याहृइति incantations arose within a प्राजापत्य milieu? As Indrasena pointed out, one of the narratives might hint at प्रजापति competing with the gods of the old religion. Moreover, in specifying the deities of the व्याहृइति-s, शौनक mentions in the बृहद्देवता that, whereas प्रजापति is the god of all the 3 व्याहृति-s as a group, individually they have Agni, वायु and सूर्य as their deities. Likewise, for ॐ, शौनक mentions वाच् and Ka प्रजापति as in the ब्राह्मण narratives, but also Indra and the gods in general as its deity. This is indeed supported by the fundamental teaching of the उपनिषत्:

[यश् छन्दसाम् ऋषभो विश्वरूपश्]{style="color:#0000ff;"}\
[छन्दोभ्यश् चन्दाँस्य् आविवेश ।]{style="color:#0000ff;"}\
[सताँ शिक्यः प्रोवाचोपनिषदिन्द्रो]{style="color:#0000ff;"}\
[ज्येष्ठ इन्द्रियाय ऋषिभ्यो]{style="color:#0000ff;"}\
[नमो देवेभ्यः स्वधा पितृभ्यो]{style="color:#0000ff;"}\
[भूर् भुवः सुवश् छन्द ओम् ॥]{style="color:#0000ff;"}

After all even in its declining days our tradition was quite unanimous about this teaching and held that: "[स प्रणव-स्वरूपी परमैश्वर्य-युक्त परमात्मा इन्द्र उपनिषत् प्रतिपाद्यो भूत्व व्याहृति-त्रयात्मा ओंकारः ।]{style="color:#808000;"}" Indra, in the form of the व्याहृति-s and the प्रणव, is indeed is the first causal link in the "bringing together" (उपनिषत्) of the sambandha-s for our ancestors --- fitting well with the Aitareya statement on the व्याहृति as the internal bonds of the श्रुति. These hint at a pre-प्राजापत्य origin for the व्याहृति-s and the प्रणव within the old religion. So, can we find evidence for the ritual deployment of the व्याहृति-s in the pre-प्राजापत्य layer of the religion?"

S: "A superficial student could indeed reach the conclusion that the व्याहृइति-concept emerged as part of the linking of the cosmogonic role of प्रजापति with the cosmic origin of the mantra-s, like in the accounts that Indrasena just expounded. However, as your sister noted there is an association of the व्याहृति-s with the स्विष्टकृत् rite for setting right the errors of ritual. Indeed, as you know well, such व्याहृति oblations and calls are a general feature of most गृह्य rites and also श्रौत rituals such the सामिधेनी incantations where they make up the syllables corresponding to the rest of the year beyond the 365. I would say this pervasive use is an indication of their ancient and pre-प्राजापत्य ritual roles, substantiating their conception as the internal fastenings of the Veda. An unambiguous case for their pre-प्राजापत्य role is made by their central role in the silent incantations that are inaudibly recited as as part of various शस्त्र-s, as also the similar incantations used in the morning and evening offerings of the Agnihotra. Thus, the श्रुति of the Aitareya-s states that one concludes the आज्य and Praüga recitation with [भूर् अग्निर् ज्योतिर् ज्योतिर् अग्निः ॥]{style="color:#0000ff;"}; the निष्केवल्य and मरुत्वतीय recitations with [इन्द्रो ज्योतिर् भुवो ज्योतिर् इन्द्रः ॥]{style="color:#0000ff;"} and the आग्निमारुत and वैश्वदेव recitations with [सूर्यो ज्योतिर् ज्योतिः स्वः सूर्यः ॥]{style="color:#0000ff;"} as the inaudible incantations. This give us a glimpse of their ancient use in a श्रौत context going back to pre-प्राजापत्य times. But we see pervasive signs of their place in even more routine rites that reinforce the proposal that their common place use was of pre-प्राजापत्य provenance --- I guess you might agree Indrasena?"

I: "I was just about to interject in this regard. To build up the context, we may first note the व्याहृति-kalpa from the गृह्य appendix of the बोधायन-s, which teaches the 12000x japa of the महाव्याहृति-s for the attainment of specific goals and purification. It was this tradition of the japa of the व्याहृति-s that one hand was incorporated into nitya and naimittika rituals focused on Deva सवितृ --- like the very fact that we use either the three महाव्याहृति-s or the seven व्याहृति-s: [Bःऊः Bःऊव्आः षूव्आः ंआःआः ञाणाः टाप्आः षाट्य़ां]{style="color:#0000ff;"} coupled the सावित्री in our routine daily japa. The same applies to the coupling of the महाव्याहृति-s with the सावित्री and the त्रिसुपर्ण in the mahat (the great) rite taught by उद्दालक आरुणि to याज्ञ्वल्क्य वाजसनेय, the founders of the शुक्ल tradition. Indeed, this coupling of the महाव्याहृति-s with the सावित्री is extended in tradition of the जैमिनीय singers, wherein two additional व्याहृति-s are added to the list to make it a total of five, just as the seven in the other traditions. These are SATYAM and पुरुष, after which comes the गायत्र सामन् composed on the सावित्री. On the other hand the pure महा-व्याहृति japa also developed into the song of व्याहृति-s in the tradition of the राणायनीय and Kauthuma singers. In that song, the musically rendered व्याहृति-s are sandwiched between two musical प्रणव-s and interspersed with 3 repetitions of the magical stobha-s with the concluding bhakti-s of the suvar-ज्योतिः, reminiscent of both the शस्त्र incantation you mentioned and the ब्रह्मशिरस् that closes the सावित्री with the ten प्रणव-s ([ॐ भुः । ॐ भुवः । ओँ सुवः । ॐ महः । ॐ जनः । ॐ तपः। ओँ सत्यम् । ॐ तत् सवितुर् वरेण्यम् । भर्गो देवस्य धिमहि । धियो यो नः प्रछोदयात् ॥ ओम् आपो ज्योती रसो अमृतम् ब्रह्म भूर्-भुव-स्वरोम् ॥]{style="color:#0000ff;"}). Similar to this connection to the सावित्री, the व्याहृति-s are also incorporated into the important व्याहृति-homa-s of the यजुष् tradition ([भूर् अन्नम् अग्नये स्वाहा... इत्यादि]{style="color:#0000ff;"}) which culminate in the incantation of Indra as the bull among the meters that your wife just mentioned. Notably, the last of the व्याहृति-homa incantations include the fourth व्याहृति, mahas. All these not only indicate the pervasive presence of the महाव्याहृति-s but also the other व्याहृति-s in a range of prayoga-s, like say the offerings to महाराज in the आरण्यक of the तैत्तिरीयक-s. These strongly favor the idea that the प्राजापत्य-s were merely incorporating a widely use mantra-tradition into their philosophizing."

S: "Good. In this regard I would note that the special व्याहृति, पुरुष, of the singers is also used in other recitations. One such is during the expiatory singing of the वामदेव्य Stotra based on RV 4.31.1-3 composed by the illustrious ancestor of our wives. The last of these ऋक्-s is short by 3 syllables from the गायत्री; hence, he should insert the the 3 syllables of the व्याहृति पुरुष and recite this ऋक् as a proper गायत्री before the वामदेव्य Stotra is sung. Thus, we have:\
[ॐ अभी षु णः सखीनाम् पु अविता जरितॄणां रु । शतम् भवास्य् ऊतिभिः षः ॥]{style="color:#0000ff;"}

Similarly, during the recitation of the राजन incantation in the nocturnal ritual of the winter solstice we insert a पुरुष into the intertwining of the ऋक् of my ancient clansman बृहद्दिव आथर्वण and that of Priyamedha आङ्गिरस:\
[तद् इद् आस भुवनेषु ज्येष्ठम् पु]{style="color:#0000ff;"}\
[नदं व ओदतीनां ।]{style="color:#0000ff;"}\
[यतो जज्ञ उग्रस् त्वेषनृम्णो रु]{style="color:#0000ff;"}\
[नदं योयुवतीनो३म् ।]{style="color:#0000ff;"}\
[सद्यो जज्ञानो नि रिणाति शत्रून्]{style="color:#0000ff;"}\
[पतिं वो अघ्न्यानां ।]{style="color:#0000ff;"}\
[अनु यं विश्वे मदन्त्य् ऊमाः षो]{style="color:#0000ff;"}\
[धेनूनाम् इषुध्यसो३म् ॥]{style="color:#0000ff;"}

बृहद्दिव आथर्वण:\
He indeed was the foremost in the universes,\
who was born with fierce, mighty manliness\
Simultaneously, with his birth, he melts down the enemies\
as all his friends [विष्णु, वायु and the Marut-s] cheer him on.

Priyamedha आङ्गिरस:\
At the roaring bull among the eager females,\
at the roaring bull among the coy young ladies,\
at the lord of your milk-giving cows,\
shoot your arrow \[in the form of the chant]

As you can see, the intertwining couples a mantra indicating the manliness of Indra with one indicating him as the bull among the females; thus, the व्याहृति पुरुष here becomes the seed that is infused into the incantation."

I: "Somakhya, having gone so far into the realm of the व्याहृति-s and their prayoga-s, let us return to my original question regarding the roots of the व्याहृति-s of the Atharvan-s. Where all are they found and what are their prayoga-s?"\
S: "प्राजापत्य ब्रह्मवाद for the AV व्याहृति-s given in the Gopatha ब्राह्मण is closely but briefly paralleled by the जैमिनीय ब्राह्मण, which states that the local worlds was generated in fluid from the three महाव्याहृति-s. In contrast, the higher realms of space are said to have been generated from व्याहृति-s KARAT, JANAT, वृधत् and SATYAM. This indicates that there was a wider knowledge and tradition of most of the AV व्याहृति-s. In terms of ritual, we can see from the Gopatha ब्राह्मण itself that the prayoga is in the context of the older Aindra religion, in the offering to the Marut-s and other deities who are in the company of Indra (the Indravant-s). Even as the व्याहृति पुरुष is deployed within the incantations that we just discussed, one could make a case for a similar wider presence for most of the AV व्याहृति-s. You noted the emergence of equivalents of MAHAT and JANAD among other vaidika traditions. To that I would add the use other AV व्याहृति-s in various traditions. For instance, the वाराह-गृह्य-सूत्र of the मैत्रायणीय-s specifies the use of the triad karat, janat and बृहत् (unique to that tradition, and semantically mirroring mahat or mahat) in the गर्भधान ritual. Coming to the तैत्तिरीयक-s, we have इड recitations laid down by my ancestor Jamadagni भार्गव for the drawing of the milk offerings in the Agnihotra as taught by आपस्तम्ब:[Bःऊऋ ईडा BःऊVआ ईड्आ षूVआऋ ईडा ःआऋआड् ईडा पृट्ःईङ् ईडा ॥]{style="color:#0000ff;"} In this tradition, the first three महाव्याहृति-s are, as usual, associated with the Agni on earth, वायु in the atmosphere and सूर्य in the heavens. Of the two further व्याहृति-s, karat, matching the AV tradition, corresponds to the moon moving against the backdrop of the नक्षत्र-s and the YV-specific पृथिक् corresponding to the medicinal herbs discovered by Jamadagni भार्गव. The आश्वलायन-s have only four drawings of milk, and use वृधत् instead of karat, again matching one the AV व्याहृति-s. Interestingly, the मैत्रायणीय-s instead use janat in this context. Thus, these व्याहृति-s are individually used in the other traditions but come together as a whole in the AV tradition and the स्वाहा offerings with them are specified in the कौशिक-सूत्र-s.

This brings us to TAT and शम्. I'd posit that those arose from the ancient opening of the शम्युवाक incantation which is repeatedly mentioned throughout the श्रुति. Its ancient use is suggested by opening of the शम्युवाक being sought from Rudra in the ऋक् of कण्व, the son of Ghora अङ्गिरस in the RV. In regard to TAT, one might also note that it might have a link to its use in the यजुष् incantation of the supreme वायु: [ॐ तद् ब्रह्म । ॐ तद् वायुः । ॐ तद् आत्मा । ॐ तत् सत्यम् । ॐ तत् सर्वम् । ॐ तत् पुरोर् नमः ॥]{style="color:#0000ff;"} Finally, coming to the प्रणव as an AV व्याहृति, it seems to be a natural inclusion given the intimate link the महाव्याहृति-s share with it मीमांसा and prayoga. We see that, for example, in the उपनिषत् statement that identifies them with Indra as the bull among the Chandas, which Lootika just mentioned. Finally, I should also mention one of the homologies that the Atharvan tradition recognizes between these व्याहृति-s and the wider horizon of texts. Thus, it has janat as equivalent to the compilation of the आङ्गिरस-s --- the आङ्गिरस-veda; similarly we have वृधत् and Sarpaveda, karat and पिशाछवेद, ruhat and Asuraveda, mahat and the इतिहास-s, and tat and the पुराण-s. This perhaps reflects both the growing corpus of texts and awareness of texts of other traditions --- like the Asuraveda --- it could be some kind of memory of the Iranian texts."

V: "What are some of the meditations one should be mindful of when performing a japa or contemplation on the progression of व्याहृति-s?"\
I: "The most important one is the japa of the threefold महाव्याहृति-s preceded by a प्रणव. During this, as in the वैश्वदेव songs of the Chandoga-s, one meditates on the Vasu-s associating them with BHUR; them one meditates on the Rudra-s associating them with the utterance of BHUVAS; then one mediates on the आदित्य-s while uttering SUVAR. With the preceding ॐ, one meditates on Indra or विश्वेदेव-s, i.e. the entire pantheon. While moving from one महाव्याहृति to another one perceives the connector deities: SUVAR and भुः are connected by द्यावा-पृथिवी; BHUR and BHUVAS by अग्नी-षोमा; BHUVAS and SUVAR by वाता-पर्जन्या. When uttering the five व्याहृति-s, i.e., महाव्याहृति-s + TAPAS and SATYAM one additionally meditates on the primal heat from which all arose and the very nature of existence. May be Somakhya could add more while returning to our starting point of the AV व्याहृति-s?"

L: "Also, before rounding up this discussion it would be worthwhile if you could touch upon some of the मीमांसा-s on the different sets of व्याहृति-s that are not widely aired by the extant ब्रह्मवादिन्-s focused on प्राजापत्य and उत्तरमीमांसा traditions."\
S: "Sure. Their connection to the सावित्री and the god सवितृ is the most apparent one. The श्रुति holds that the inviolable laws of सवितृ, like the probabilities of the draws of the विभीदक nuts from the hole, are the ones which run the universe: [देव इव सविता सत्यधर्मा]{style="color:#0000ff;"}: like the laws of the god सवितृ that hold true. The महाव्याहृति-s illustrate their most apparent domain of action: the near realm, the mid-region and the realm of the sun. The more expanded set of seven व्याहृति-s yoked to the सावित्री indicate their broader sphere of action --- Mahas: the wider space. Then we move into the temporal axis: janas: the origin of space itself. What drives its emergence? tapas: heat. Finally, the very fact that something exists: satyam: also expressing the inviolable or true nature of the laws of सवितृ, the ऋत. Indeed, कृष्ण आङ्गिरस states:\
[ऋतेन देवः सविता शमायत]{style="color:#0000ff;"}\
[ऋतस्य शृङ्गम् उर्विया वि पप्रथे ।]{style="color:#0000ff;"}\
By the natural law the god सवितृ exerts himself,\
\[by that] the antler of the natural law has spread widely.

The Samaveda adds the व्याहृति, पुरुष, after SATYAM. This may be seen as the root of the concept that was later expanded in सांख्या --- the पुरुष as consciousness. In placing the final पुरुष, the singers posited a system in which the laws and existence itself might be objects in the conscious experience of the sole reality, the पुरुष. The next notable मीमांसा of the व्याहृति-s pertains to the way we deployed the महाव्याहृति triad with the three feet of the AV mantra-s to विष्णु. This connection is declared by the जैमिनीय-s, who state that the व्याहृति-s were offered to विष्णु. The three feet of the deployed mantra-s indeed correspond to the three steps of विष्णु, who appeared as a dwarf and suddenly grew to a gigantic size to conquer the worlds from the दानव-s with his famed triple strides: [बृहच्छरीरो विमिमान ऋक्वभिर् युवाकुमारः प्रत्य् एत्य् आहवम् ॥]{style="color:#0000ff;"} As the founder of your race, O गौतमी-s, states in the primal श्रुति, that gargantuan form of विष्णु is said to measure out the worlds with the ऋक्-s --- those are the corresponding AV ऋक्-s we deploy conjoined to the व्याहृति-s. The काण्व-s further add that those steps were the ones with gathered the atoms ---[समूढम् अस्य पंसुरे ॥]{style="color:#0000ff;"} --- from which the universe condenses. Hence, while uttering that incantation the ritualist meditates on the great विष्णु stamping out the Asura-s and likewise calls on him to exclude his rivals from his space. This association with the व्याहृति-s also extends to विष्णु's wife in the incantation seen in the महानारायणोपनिषत् of the late AV tradition that you all know very well:\
[ॐ भूर् लक्ष्मी भुवर् लक्ष्मीः सुवः कालकर्णी तन् नो महालक्ष्मीः प्रचोदयात् ॥]{style="color:#0000ff;"}\
This incantation is notable in placing कालकर्णी in suvar. This is from her association with visible time in the form of the apparent movement of the sun in the sky. As you all know well from your आगमिक practice she is none other than the gigantic, dreadful, fanged, death-dealing eponymous goddess, armed with a bow, arrows, axe, sword, cakra, trident and a cleaver, emitted by रुद्राणी from her mouth to terrorize the gods for their support of प्रजापति दक्ष. The fourth व्याहृति mahat is subliminally hinted by her name महालक्ष्मी, encompassing the wider space.

Coming the the AV व्याहृति-s, the coupling [ऒं Bःऊऋ ञाणाट् ॥]{style="color:#0000ff;"} expiates ऋक् errors and is offered in the गार्हपत्य; [ऒं BःऊVऒ ञाणाट् ॥]{style="color:#0000ff;"} expiates यजुष् errors and is offered in the दक्षिण; [ऒं ष्Vआऋ ञाणाट् ॥]{style="color:#0000ff;"} expiates सामन् errors and is offered in the आहवनीय; [ऒं Bःऊऋ Bःऊव्आः ष्Vआऋ ञाणाड् ऒं ॥]{style="color:#0000ff;"} expiates Atharvan errors and is also offered in the आहवनीय. Here the JANAT is seen as regenerating the flawed incantations. Vrishchika, it may interest you that the AV tradition uses a metallurgical analogy of fusing metals for the welding role of these व्याहृति-s, unlike the medical analogy of the Aitareya that you noted. The AV also holds that the same combination of व्याहृति-s are the incantations uttered by the brahman before he asks the उद्गातृ to sing the stoma to the god बृहस्पति in the सोमयाग. Likewise he utters the entire gamut of व्याहृति-s [ऒं Bःऊऋ Bःऊव्आः ष्Vआऋ ञाणाड् वृड्ःआट् ःआऋआड् ऋऊःआण् ंआःआट् टाC Cःआं ऒं]{style="color:#0000ff;"} when urging the उद्गातृ to sing the song of the Indravant-s derived from the famous एवयामरुत् ऋक् to the Marut-s and विष्णु that was composed by your illustrious ancestor, O Indrasena ([प्र वो महे मतयो यन्तु विष्णवे मरुत्वते गिरिजा एवयामरुत् । प्र शर्धाय प्र यज्यवे सुखादये तवसे भन्ददिष्टये धुनिव्रताय शवसे॥]{style="color:#0000ff;"}). As an aside that mantra-s is notable in more than one way but you may note the phrase गिरिजा --- विष्णु emerging from the mountain --- a mythologem that presages his emergence from the pillar in the later नृसिंह cycle --- but here he emerges with the marching troop of the Marut-s to head for battle, evidently to join Indra in the battle against the दानव-s.

In the purely AV performance, as we did earlier today (or in the muttered incantation of the brahman), it is deployed with the Indravant ऋक् which illustrates the connections to various व्याहृति-s. With Agni we are connected to भूर्, with वात (वायु) Bhuvas, with विष्णु, the realm of the आदित्य-s. All the special व्याहृति-s can be seen as having deep connections with Indra and the Indravant-s. As we saw before the two प्रणव-s at the beginning and the end are the mark of Indra. Janat indicates the emergence of विष्णु and the Marut-s from the mountain, which is a metaphor for the world axis --- thus on one hand it represent the origin of time that विष्णु manifests as. On the other birth of the Marut-s that made the universe manifest. That manifestation and the growth of the universe, which is how the sons of Rudra manifest is indicated by वृधत्. Karat is action of filling the universe, as the ancient भार्गव, उशनस् काव्य, is quoted by Agastya मैत्रावरुणि:[करत् तिस्रो मघवा दानुचित्रा]{style="color:#0000ff;"} : Maghavan made three realms fill with glistening droplets. Ruhat, stands for the ascendance of the gods, manifesting as the rising sun in which they are worshiped. Mahat, as we saw before represent the great expanse of the universe. Finally Tat and शम् are the bliss that one attains from the gods upon the success of the ritual."

