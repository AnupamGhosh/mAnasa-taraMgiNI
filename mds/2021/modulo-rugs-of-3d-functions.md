
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Modulo rugs of 3D functions](https://manasataramgini.wordpress.com/2021/02/22/modulo-rugs-of-3d-functions/){rel="bookmark"} {#modulo-rugs-of-3d-functions .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 22, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/02/22/modulo-rugs-of-3d-functions/ "4:10 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Consider a 3D function  $z=f(x,y)$ . Now evaluate it at each point of a  $n \times n$  integer lattice grid. Compute  $z \mod n$  corresponding to each point and plot it as a color defined by some palette that suits your aesthetic. The consequence is a what we term the "modulo rug".\
For example, below is a plot of  $z=x^2+y^2$ .

![matrixmod01_318](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/matrixmod01_318.png){width="75%"}Figure 1:  $z=x^2+y^2, n=318$ 

We get a pattern of circles around a central circular system reminiscent of ogdoadic arrangements in various Hindu मण्डल-s. From the aesthetic viewpoint, the best modulo rugs are obtained with symmetric functions higher even powers --- this translates into some pleasing symmetry in the rug. Several examples of such are shown below.

![matrixmod06_318](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/matrixmod06_318.png){width="75%"}Figure 2:  $z=x^4-x^2-y^2+y^4, n=318$ 

![matrixmod08_315](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/matrixmod08_315.png){width="75%"}Figure 3:  $z=x^4-x^2-y^2+y^4, n=315$ 

![matrixmod13_309](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/matrixmod13_309.png){width="75%"}Figure 4:  $z= x^6-x^4-y^4+y^6, n=309$ 

![matrixmod12_318](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/matrixmod12_318.png){width="75%"}Figure 5:  $z=x^6-x^2-y^2+y^6, n=318$ 

![matrixmod07_312](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/matrixmod07_312.png){width="75%"}Figure 6:  $z=x^4-x^2+y^2-y^4, n=310$ 

All the above  $n$  are composite numbers. Accordingly, there is some repetitiveness in the structure. However, if  $n$  is a prime then we have the greatest complexity in the rug. One example of such is plotted below.

![matrixmod14_311](https://manasataramgini.wordpress.com/wp-content/uploads/2021/02/matrixmod14_311.png){width="75%"}Figure 7:  $z=x^6-x^4+x^2+y^2-y^4+y^6, n=311$ 

See also: 1) [Sine rugs;](https://manasataramgini.wordpress.com/2017/05/27/sine-rugs/) 2) [Creating patterns through matrix expansion.](https://manasataramgini.wordpress.com/2019/01/27/creating-patterns-through-matrix-expansion/)

