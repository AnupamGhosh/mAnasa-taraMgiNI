
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The shape of dinosaur eggs](https://manasataramgini.wordpress.com/2021/09/08/the-shape-of-dinosaur-eggs/){rel="bookmark"} {#the-shape-of-dinosaur-eggs .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 8, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/09/08/the-shape-of-dinosaur-eggs/ "6:52 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

Readers of these pages will know that we have a special interest in the geometry of [ovals](https://manasataramgini.wordpress.com/2016/04/10/a-biographical-journey-from-conics-to-ovals/). One of the long-standing problems in this regard is: what is the curve that best describes the shape of a dinosaurian egg? While all archosauromorphs hatch from eggs outside their mother's body, the form of their eggs is rather variable; crocodylians and turtles may lay either leathery or hard-shelled eggs. The dinosaurs almost always lay hard-shelled eggs that tend to be rather uniform in shape in the wild. Being hard-shelled, the shape of a dinosaurian egg can be described by the characteristic curve of its maximal (area) cross-section. The egg itself will be the solid of rotation of this curve around its longest axis. Using this definition, the noted morphometrician and student of Aristotelian zoology, D'arcy Thompson, classified bird eggs into various forms in his famous book "On growth and form". More recently, this was revisited by Nishiyama, who named 4 shape groups for the eggs of modern (avian) dinosaurs: (1) circular; (2) elliptical; (3) oval; and (4) pyriform. However, an examination of a large data set of eggs from around 1400 extant birds by Mahadevan and colleagues shows no strict boundaries between the shape classes. Hence, in principle, they should all be describable by a single equation of shape with varying parameters. If postmortem distortion and the effects of fossilization have been properly accounted for, the Mesozoic dinosaur eggs featured additional diversity. For example, the eggs of theropods, like the tyrannosaur Tarbosaurus, would not be described appropriately by any of the 4 purported classes. It would rather be a generalized higher-order elliptical egg (see below). Hence, ideally, the equation should not just cover extant dinosaurs but also the extinct ones.

Indeed, there has been a long-standing interest in obtaining that single equation that describes eggs' shapes, starting with extant birds. One such early attempt was that of the Swiss geometer Jakob Steiner who proposed the oval of Descartes (defined by the bipolar equation:  $r+mr'=c$ , where  $m$  is the ratio parameter and  $c$  the constant sum) as the general equation for avian eggs. However, D'arcy Thomas had pointed out that various avian eggs do not fit this curve. Subsequent explorations of this question have offered a range of solutions. In light of recent work presenting a new potential solution, we consider and compare some notable attempts, including the latest.

 $\bullet$  Maxwellian ovals: The great JC Maxwell, while still in his early teens, generalized the ellipses to describe families of ovals. Of these, one class of ovals, the "trifocal ellipse" can be described using three functions in  $x, y$ :

 $$f_1(x,y)=x^2+y^2; f_2(x,y)=(x-a)^2+y^2; f_3(x,y)=(x-b)^2+y^2$$ 

Then the Maxwellian oval is described by the equation:

 $\sqrt{f_1(x,y)}+\sqrt{f_2(x,y)}+\sqrt{f_3(x,y)}=c$ , where  $a, b, c$  are parameters.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/09/egg_dinosaurs01.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad A Maxwellian trifocal oval.
```{=latex}
\end{center}
```


We too independently arrived at this curve in our teens but, unlike Maxwell, did not achieve any deep understanding of the physics of these curves. This curve is constructed using the same principle as an ellipse, viz., the locus of points whose distances from the foci add up to a constant, but it has 3 colinear foci instead of 2 of the regular ellipse (Figure 1). For  $a=1, b=0.2, c=2.2$  and close values, we get a curve that even a casual observer will note as approximating the shape of common avian eggs (Figure 1). Indeed, in 1957, such a curve had been used by a certain E. Ehrhart as a possible fit for the avian egg following statistical analysis of real specimens. Unaware of Ehrhart's work, in course of our own early experiments with this curve, we too considered this as a possible description of the shape of the most common type of avian egg prototyped by those of several galloanseran birds. This has now been borne out by the large dataset of Mahadevan and coworkers, in which the most frequently occurring morphology is close to this curve. However, it is hardly a universal equation of shape as there are several egg shapes lying outside its scope.

 $\bullet$  Hügelschäffer's equation: The the 1940s, a German engineer Fritz Hügelschäffer, derived the equation of an oval that he felt was a good fit for the shape of avian eggs. As we have noted before on these pages, we independently arrived at the [construction of this oval](https://manasataramgini.wordpress.com/2016/04/10/a-biographical-journey-from-conics-to-ovals/) and derived its equation while in junior college. Hügelschäffer expressed the equation of this curve in the following form:

 $$y=\pm\dfrac{B}{2}\sqrt{\dfrac{L^{2}-4x^{2}}{L^{2}+8wx+4w^{2}}}$$ 

Here,  $L$  is the major axis or length of the egg with its center placed at origin  $O=(0,0)$ .  $B$  is the minor axis or maximal breadth of the egg.  $w$  is the distance between  $O$  and the point of intersection of the segments  $L$  and  $B$ . By taking  $L=1$  this effectively yields a 2 parameter  $(B, w)$  shape curve, that accounts for more egg morphologies than the Maxwellian trifocal oval: with  $L=1$ , we obtain the circular  $(B=1; w=0)$ , elliptical  $(0 \le B < 1); w=0)$  and oval  $(B \ne 1; w> 0)$  shape classes of extant avian eggs. Moreover, its parameters are entirely intuitive and can be measured easily from real specimens. However, it does not account for the so-called pyriform class (common in shorebirds) or the Mesozoic dinosaurian eggs like those of Tarbosaurus, the caenagnathid Beibeilong, the troodontids, or the ceratopsian Protoceratops.

 $\bullet$  Preston's 4 parameter oval: Incited by D'arcy Thompson's failure to give a  general equation for the shape of eggs, in the 1950s, Frank Preston, a versatile English engineer (invented a glass-melting furnace and a device to measure avian egg shapes), marksman and naturalist, derived the 4 parameter oval to describe the shape of all bird eggs. He used the following logic: the most symmetric class is the circular class for which one can easily write down the equation:  $y=\pm \sqrt{1-x^2}$ . By multiplying this by the parameter  $a \le 1$ , the aspect ratio (ratio of minor to major axis) of an ellipse, we get the equation  $y= \pm a\sqrt{1-x^2}$ , which can account for both the circular and elliptical classes. Then Preston accounted for the remaining classes using a polynomial function  $f(x)=1+bx+cx^2+dx^3$ , where  $-1 \le b, c, d \le 1$ , thus yielding the final equation of a generalized oval:

 $$y=\pm a(1+bx+cx^2+dx^3)\sqrt{1-x^2}$$ 

```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/09/egg_dinosaurs03.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 2\quad Eggs of selected extant and extinct dinosaurs modeled using Preston's equation.
```{=latex}
\end{center}
```


You can try out various fits with above parameters on a collection of real eggs [here](http://desmos.com/calculator/pasi1dmuz8)

As one can see from Figure 2, Preston's 4 parameter oval accounts for all dinosaur egg shapes extant and extinct. The egg of the Ural owl is of the elliptical class coming close to the circular class. The emu is nearly a classical elliptical with a very small  $c$  parameter that adjusts it to a more generalized ellipse. The song thrush and osprey are very nearly ovals with a pronounced  $b$  parameter and slight adjustments, again with a very small  $c$  parameter. The guillemot and great snipe are clear pyriforms with both pronounced  $b$  and  $c$  parameters. The extinct dinosaur Troodon has an egg quite distinct from any extant bird but can still be modeled by Preston's oval with positive  $c, d$  terms. The egg of Tarbosaurs (and others like it, e.g., Beibeilong or Protoceratops) is a generalized ellipse that is again well-modeled by Preston's equation with just a negative  $c$  term with other  $x$  powers in the polynomial having 0 coefficients. Preston's equation can model most extant bird eggs using just the linear and square terms with negative coefficients. The cubic term is only needed for unusual eggs, like in this case, that of Troodon. Since Preston, several researchers have tried to duplicate his approach by using other functions in place of his cubic polynomial (see below). However, recent numerical analysis using a dataset of 132 real eggs from various modern species by Biggins et al. has shown that Preston's curve outperforms all these other attempts in having the least and an impressively small error. Thus, the Preston 4 parameter oval can be considered a valid, universal description of the shape of the dinosaurian egg.

 $\bullet$  In light of the success of the Preston oval, we were a bit surprised when we saw a recent work by Narushin et al. claiming to introduce a universal formula for the egg shape. They acknowledge the success of Preston's work but state that the parameters in his equation are neither intuitive nor readily determined. The former is indeed a potential criticism; however, the latter is less of any issue with modern graphing software, so long as one has good photographs. Hence, they decided to start with Hügelschäffer's formula and applied a series of modifications to arrive at a complicated formula for a general oval:

 $$y= \dfrac{B}{2}\sqrt{ \dfrac{L^{2}-4x^{2}}{L^{2}+8wx+4w^{2}}} (1- k f(x))$$ 

 $$k = \dfrac{\sqrt{\dfrac{11}{2}L^{2}+११ळ्व्+4w^{2}}\left(\sqrt{3}BL-2D\sqrt{L^{2}+२व्ळ्+4w^{2}}\right)}{\sqrt{3}BL\left(\sqrt{\dfrac{११ळ्^{2}}{2}+११ळ्व्+4w^{2}}-2\sqrt{L^{2}+२व्ळ्+4w^{2}}\right)}$$ 

 $$f(x) = 1-\sqrt{\dfrac{L\left(L^{2}+8wx+4w^{2}\right)}{2\left(L-2w\right)x^{2}+\left(L^{2}+८ळ्व्-4w^{2}\right)x+२ळ्व्^{2}+L^{2}w+L^{3}}}$$ 

Here, as in Hügelschäffer's equation,  $L$  is the major axis or length of the egg;  $B$  is its minor axis or greatest breadth;  $D$  is the breadth of the egg at the point halfway from the center at  $(0,0)$  to the narrow end of the egg (Figure 3). However,  $w$  is not the same as in the Hügelschäffer equation but is defined as:

 $w=\dfrac{L-B}{2n}$ , where  $n$  is a positive number.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/09/egg_dinosaurs02.png){width="75%"}
```{=latex}
\end{center}
```

The landmarks of Narushin et al's equation for a dinosaurian egg.

One advantage of this equation is that  $L, B, D$  can be directly measured relatively simply with Vernier's calipers and a ruler. The  $w$  parameter can be empirically calculated from  $L, B$  by adjusting  $n$ . While the authors state this equation can account for all extant bird eggs, we found that, unlike Preston's equation, it could not account for special cases of extinct dinosaur eggs, like those of Troodon and Tarbosaurus, assuming that their reconstruction is accurate. However, we rectified that by using an "inversion" flag that takes 3 values: N, Y, and H. It appears that for all extant birds (at least those considered by Narushin et al.), this flag is N; these can be modeled using their equation as is. For Troodon and related eggs, the flag is Y; here,  $x$  has to be substituted by  $-x$ . For Tarbosaurus and related eggs, the flag is H; here for  $-\tfrac{L}{2}\le x \le 0$  we use the equation as is and for  $0 < x \le \tfrac{L}{2}$ , we substitute  $x$  with  $-x$ . This accounts for all dinosaur egg shapes comparable to Preston's equation (Figure 4). If we normalize it by taking  $L=1$ , it effectively leaves us with a maximum of 4 parameters as in the case of Preston's equation.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/09/egg_dinosaurs04.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 4\quad Eggs of selected extant and extinct dinosaurs modeled as Narushin et al's oval with the inversion flag modification.
```{=latex}
\end{center}
```


You can try out various fits with above parameters on a collection of real eggs [here](http://desmos.com/calculator/rpqe77idfu).

The relative merits of this equation need to be compared to that of Preston's using real specimens. Unfortunately, this requires additional work as Mahadevan and colleagues' large dataset does not have all the necessary measurements for such a comparison. They instead used the oval of Baker, an attempt to duplicate Preston's work, which significantly falls short of the latter in terms of accuracy while providing a simple 2 parameter space. It is defined by the equation:

 $$y=\pm a\left(1-x\right)^{\frac{1}{1+b}}\left(1+x\right)^{\frac{b}{1+b}}$$ 

Here  $0 \le a \le 1$  is the aspect ratio of the ellipse as in Preston's equation or the equivalent of  $B | L=1$  in Hügelschäffer's equation, while  $1 \le b \le 2$  is an asymmetry parameter similar to  $w$  in Hügelschäffer's equation. This curve has 3 successively tangent lobes with points of tangency at  $(-1,0)$  and  $(1,0)$ . For  $x \le -1$  and  $x \ge 1$  the two lobes have a divergent hyperbola-like form. For  $-1 \le x \le 1$ , the curve takes the form of the oval that approximates the shape of a dinosaurian egg. When  $b=1$ , the curve becomes an ellipse.

A biologist can easily conceive each of the parameters from Preston's or Narushin et al.'s curves as being controlled by a genetic factor, with changes in it leading to a change in the parameter. Thus, one can easily account for the diversity of egg morphologies observed among dinosaurs through genetic changes. The parameter  $\leftrightarrow$  gene mapping feeds directly into the question of what are the selective forces acting on egg shape. Several of these have been proposed and debated since Darwin. Irrespective of the correctness of some of these, one key point emerging from the suggestions made by Birkhead is that the ovoid morphology is a clear sign of a comprise solution resulting from balancing selection. The compromises themselves might involve very different factors. One such relates to spherical morphology having the smallest surface area for a given volume. Hence, it is ideal for not losing heat quickly. However, eggs also need to be externally warmed, either by direct contact with the mother or exposure to solar or geothermal heat (e.g., in titanosaurs). Here, a less spherical shape would afford a greater surface area to allow quicker external heating. Similarly, a hard-shelled egg would have the greatest strength against external force if spherical. This would be selected for better protection or bearing the weight of the brooding mother or insulating material deposited atop it. On the other hand, it should also be easy enough for the chick to break out. These opposing forces would lead to compromise solutions in the form of deviations from circularity. Mahadevan and colleagues also observed that increased flight performance is often associated with smaller aspect ratios and more asymmetric eggs. Here again, a compromise of sorts might be in play --- higher flight performance selects for bigger eggs on one hand and a more streamlined body on the other. Hence, the compromise is achieved by having longer or more asymmetric eggs. A similar effect, albeit unrelated to flight, but body morphology, might have also been at play in the Mesozoic dinosaurs with long eggs. Of course, the shape diversity beyond a simple ellipse suggests that other selective forces beyond the above are also at play.

However, a morphometrician of the bent of D'arcy Thompson would still object that these equations need to be derived ground up from physics --- in fact, he voices precisely that problem in his account of bird egg shapes --- they need to be derived from an equation which accounts for fluid pressure in a bounded membrane. This was keeping with his wider skepticism towards one of the foundations of biology (natural selection) while emphasizing the other (geometry). Mahadevan and colleagues presented such a derivation a few years back; however, it is not clear if it can actually recapitulate the entire range of ovals seen in real-life dinosaurian eggs.

  - -----------------------------------------------------------------------

Further reading:

Avian egg shape: Form, function, and evolution by Stoddard, Yong, Akkaynak, Sheard, Tobias, and Mahadevan

Egg and math: introducing a universal formula for egg shape by Narushin, Romanov, and Griffin

Accurately quantifying the shape of birds' eggs by Biggins, Thompson, and Birkhead

