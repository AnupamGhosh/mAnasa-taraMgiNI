
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Agni as the divine commander in the Veda and the पुराण](https://manasataramgini.wordpress.com/2021/11/20/agni-as-the-divine-commander-in-the-veda-and-the-pura%e1%b9%87a/){rel="bookmark"} {#agni-as-the-divine-commander-in-the-veda-and-the-परण .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 20, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/11/20/agni-as-the-divine-commander-in-the-veda-and-the-pura%e1%b9%87a/ "10:32 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

With the साकमेध-parvan with the oblations to Agni अनीकवत् having just passed, we present a brief note on Agni as the general of the deva army. Agni is presented as the commander of the deva-s in the ब्राह्मण literature. For example, the शतपथ-ब्राह्मण of the शुक्लयजुर्वेदिन्-s makes the below statement in regard to the ratnin oblations, which are made in houses of the various functionaries of the old Indo-Aryan state. With regard to the oblation to अग्नी अनीकवत् we hear:

[अरण्योर् अग्नी समारोह्य सेनान्यो गृहान् परेत्या + अग्नये ।अनीकवते अष्ठा-कपालम् पुरोडाशं निर्वपति । अग्निर् वै देवतानाम् अनीकं सेनाया वै सेनानीर् अनीकं तस्माद् अग्नये 'नीकवते । एतद् वा अस्यैकं रत्नं यत् सेनानीस् तस्मा एवैतेन सूयते । तं स्वम्-अनपक्रमिणं कुरुते ।]{style="color:#0000ff;"} (in माध्यंदिन SB 5.3.1  $\approx$  in काण्व SB 7.1.4)\
Having taken up the two fires (गार्हपत्य and आहवनीय) on the two kindling-sticks, having gone to the house of the commander of the army, he prepares a cake on eight potsherds for Agni अनीकवत्. Agni is indeed the leader (अनीक) of the gods, and the commander is the head of the army: hence, for Agni अनीकवत्. He, the commander, is verily one of his (the king's) gems. Therefore, for him \[the king], he \[the commander] is thus consecrated. He \[the king] makes him \[the commander] his own follower.

Again, in the Gopatha-ब्राह्मण we have the following statement regarding the offering to Agni अनीकवत् in the context of the Sakamedha oblations:

[ऐन्द्रो वा एष यज्ञ-क्रतुर् यत् साकमेधाः । तद् यथा महाराजः पुरस्तात् सेनानीकानि व्युह्याभयं पन्थानम् अन्वियाद् एवम् एवैतत् पुरस्ताद् देवता यजति । तद् यथैवादः सोमस्य महाव्रतम् एवम् एवैतद् इष्टि-महाव्रतम् । अथ यद् अग्निम् अनीकवन्तं प्रथमं देवतानां यजति । अग्निर् वै देवानां मुखम् । मुखत एव तद् देवान् प्रीणाति ।]{style="color:#0000ff;"} (in GB 2.1.23)\
These साकमेध-s are verily that of Indra. Just as the emperor placing the commanders in the head of his army-formations advances unchecked on his path, so also, he (the ritualist) sacrifices placing the deities to the front. Just as there is the महाव्रत of the soma sacrifices, this is the \[equivalent of the] महाव्रत for the इष्टि-s. Now, in that, he sacrifices to Agni अनीकवत् (the commander), the first of the deities. This Agni is indeed the mouth of the gods. Thus, he pleases the gods through their mouth.

As an aside, we may note that the Atharvavedic tradition sees the साकमेध oblations as the equivalent of the महाव्रत-s for the इष्टि cycle. The महाव्रत is performed at the winter solstice. The साकमेध on the कार्त्तिक full moon is the last full moon in autumn before the winter solstice. Hence, the two are seen as being equivalent. Now, this role of Agni as the commander of the gods is already hinted at by multiple incantations in the ऋग्वेद (reproduced in the Atharvaveda) itself. For example, we have:

[अग्निर् इव मन्यो त्विषितः सहस्व सेनानीर् नः सहुरे हूत एधि ।]{style="color:#0000ff;"} RV 10.84.2a\
Blazing like Agni, o battle fury, conquer! Our commander, the conqueror when you are invoked at the kindling.

Here Manyu (the battle fury) is implied to lead the forces, like commander Agni. We may also consider one of the Agni Anikavat incantations Atri-s:

[उत स्वानासो दिवि षन्त्व् अग्नेस्]{style="color:#0000ff;"}\
[तिग्मायुधा रक्षसे हन्तवा उ ।]{style="color:#0000ff;"}\
[मदे चिद् अस्य प्र रुजन्ति भामा]{style="color:#0000ff;"}\
[न वरन्ते परिबाधो अदेवीः ॥]{style="color:#0000ff;"} RV 5.2.10\
Also, in heaven, let there be the roars of Agni\
with \[his] sharp weapons for the smiting of रक्षस्-es.\
Indeed, in his exhilaration, his fury smashes forth,\
the defense of the ungodly do not contain him.

Finally, we also have the famous रक्षोहा Agni incantations of वामदेव Gautama (RV 4.4.1-5), which present the most exalted account of Agni's war-like nature in the entire श्रुति \[Footnote 1]:

[कृणुष्व पाजः प्रसितिं न पृथ्वीं]{style="color:#0000ff;"}\
[याहि राजेवामवां इभेन ।]{style="color:#0000ff;"}\
[तृष्वीम् अनु प्रसितिं द्रूणानो ।]{style="color:#0000ff;"}\
[अस्तासि विध्य रक्षसस् तपिष्ठैः ॥]{style="color:#0000ff;"} 1\
Make your charge like a broad front.\
Move forth like a mighty king with his troops,\
thirsting to charge forth slaying,\
You are an archer: pierce the रक्षस्-es with the hottest missiles.

[तव भ्रमास आशुया पतन्त्य्]{style="color:#0000ff;"}\
[अनु स्पृश धृषता शोशुचानः ।]{style="color:#0000ff;"}\
[तपूंष्य् अग्ने जुह्वा पतङ्गान्]{style="color:#0000ff;"}\
[असंदितो वि सृज विष्वग् उल्काः ॥]{style="color:#0000ff;"} 2\
Your swirling \[weapons: cakra-s implied] fly swiftly;\
touch down on \[the foes] impetuously blazing.\
O Agni, with your tongue \[hurl] blasts of heat, flying \[sparks]\
unstopped hurl forth firebrands all around.

[प्रति स्पशो वि सृज तूर्णितमो]{style="color:#0000ff;"}\
[भवा पायुर् विशो अस्या अदब्धः ।]{style="color:#0000ff;"}\
[yo no दूरे अघशंसो yo anty]{style="color:#0000ff;"}\
[अग्ने माकिष् टे व्यथिर् आ दधर्षीत् ॥]{style="color:#0000ff;"} 3\
Send out spies against (the foes). He is the fastest.\
Become the uncheated protector of these people.\
Whoever wishes us evil from a distance, whoever from nearby,\
O Agni, may no one (enemy) evade your meandering course.

[उद् अग्ने तिष्ठ प्रत्य् आ तनुष्व]{style="color:#0000ff;"}\
[न्य् अमित्रां ओषतात् तिग्महेते ।]{style="color:#0000ff;"}\
[यो नो अरातिं समिधान चक्रे]{style="color:#0000ff;"}\
[नीचा तं धक्ष्य् अतसं न शुष्कम् ॥]{style="color:#0000ff;"} 4\
O Agni stand up, stretch your bow against \[our enemies],\
Burn down the foes, o one with a sharp weapon.\
Whoever makes hostile moves at us, o kindled one,\
burn him down like dry shrubs.

[ऊर्ध्वो भव प्रति विध्याध्य्]{style="color:#0000ff;"}\
[अस्मद् आविष् कृणुष्व दैव्यान्य् अग्ने ।]{style="color:#0000ff;"}\
[अव स्थिरा तनुहि यातुजूनां]{style="color:#0000ff;"}\
[जामिम् अजामिम् प्र मृणीहि शत्रून् ॥]{style="color:#0000ff;"} 5\
Rising upwards, jabbing against \[the foes, pushing them]\
away from us; make your divine powers apparent.\
Slacken the taut \[bows] of those incited by यातु-s,\
be they kin or non-kin slay forth the enemies.

In the transition between the Vedic and Epic phases of the Hindu literary activity, the role of Agni as the commander of the gods was transferred to his son कुमार, effectively also the son of Agni's dual Rudra. The stage for this is set deep in the श्रुति itself. As noted before, the कौमार mythology is closely tied to the famous सूक्त of the Atri-s we mentioned above (RV 5.2). Already in the fragmentary Vedic tradition of the भाल्लवि-s, we see the hint of this connection in the deployment of the ऋक्-s from RV 5.2 by वृश जान for the इक्ष्वाकु ruler त्र्यरुण. The शतपथ-ब्राह्मण, explaining the duality of Agni and Rudra, explains that the 8 forms of Rudra culminating in the supreme ईशान, the lord of all, are the 8 transformations of Agni, with कुमार as the 9th, evidently alluding to the very same सूक्त of the Atri-s:

[तान्य् एतान्य् अष्टाव् अग्नि-रूपाणि । कुमारो नवमः सैवाग्निस् त्रिवृत्ता ॥]{style="color:#0000ff;"} माध्यंदिन SB 6.1.3.18\
These then are the eight forms of Agni (Rudra, शर्व, पशुपति, Ugra, अशनि, Bhava, महादेव, ईशान). कुमार (the boy) is the ninth: that is Agni's threefold state (i.e., 3  $\times$  3).

This sets the stage for the final ninth form as the son of Rudra-Agni. In this process, कुमार also inherited to connection to the old equinoctial connection to the constellation of Agni, i.e., कृत्तिका-s -- he is also their son, कार्त्तिकेय. Also contributing to the identity of the para-Vedic कुमार are the aspects of the Vedic sons of Rudra, the Marut-s, who are also seen as leaders of the deva army. For instance, in the Apratiratha Aindra सूक्त we have:

[देवसेनानाम् अभिभञ्जतीनां जयन्तीनाम् मरुतो यन्त्व् अग्रम् ॥]{style="color:#0000ff;"} RV 10.103.8c\
May the Maruts go at the forefront of the shattering, conquering armies of the gods.

Notably, the Maruts are repeatedly referred to as Agni-s (e.g., [अग्नयो न शुशुचाना ऋजीषिणो भृमिं धमन्तो अप गा अवृण्वत ।]{style="color:#0000ff;"} RV 2.34.1c; [न येषाम् इरी सधस्थ ईष्ट आं अग्नयो न स्वविद्युतः प्र स्यन्द्रासो धुनीनाम् ।]{style="color:#0000ff;"} RV 5.87.3c; [ते रुद्रासः सुमखा अग्नयो यथा तुविद्युम्ना अवन्त्व् एवयामरुत् ।]{style="color:#0000ff;"}RV 5.87.7a;[ये अग्नयो न शोशुचन्न् इधाना द्विर् यत् त्रिर् मरुतो वावृधन्त ।]{style="color:#0000ff;"} RV 6.66.2a). This completes the circle of the connection between Agni and the sons of Rudra, who are the spear-wielding heroes of the deva army.

However, there are rare instances in the इतिहास-पुराण corpus that furnish descriptions of the martial Agni mirroring his role as commander of the divine army in the श्रुति. We provide snippets of such accounts below from the third section of the हरिवंश, the भविष्य-parvan (sometimes called Appendix I). The first snippet is from the narration of Agni leading the gods in the battle against the daitya Balin, who was subsequently trampled by विष्णु:

[लोहितो लोहितग्रीवो हर्ता दाता हविः कविः ।]{style="color:#0000ff;"}\
[पावको विश्वभुग् देवः सर्व-देवाननः प्रभुः ॥]{style="color:#0000ff;"}\
[सुब्रह्मात्मा सुवर्चस्कः सहस्रार्चिर् विभावसुः ।]{style="color:#0000ff;"}\
[कृष्णवर्त्मा चित्रभानुर् देवाग्र्यश् चित्र एकराट् ॥]{style="color:#0000ff;"}\
[लोकसाक्षी द्विजहुतो वषट्कार-प्रियो 'र्चिमान् ।]{style="color:#0000ff;"}\
[हव्यभक्षः शमीगर्भः स्वयोनिः सर्वकर्मकृत् ॥]{style="color:#0000ff;"}\
[पावनः सर्वभूतानां त्रिदशानां तपोनिधिः ।]{style="color:#0000ff;"}\
[शमनः सर्वपापानां लेलिहानस् तपोमयः ॥]{style="color:#0000ff;"}\
[प्रदक्षिणावर्त-शिखः शुचिलोमा मखाकृतिः ।]{style="color:#0000ff;"}\
[हव्यभुग् भूतभव्येशो हव्यभागहरो हरिः ॥]{style="color:#0000ff;"}\
[सोमपः सुमहातेजा भूतेशः सर्वभूतहा ।]{style="color:#0000ff;"}\
[अधृष्यः पावको भूतिर् भूतात्मा वै स्वधाधिपः ॥]{style="color:#0000ff;"}\
[स्वाहापतिः सामगीतः सोमपूताशनो 'द्रिधृक् ।]{style="color:#0000ff;"}\
[देवदेवो महाक्रोधो रुद्रात्मा ब्रह्मसंभवः ॥]{style="color:#0000ff;"}\
[लोहिताश्वं वायुचक्रं रथम् आस्थाय भूतकृत् ।]{style="color:#0000ff;"}\
[धूमकेतुर् धूमशिखो नीलवासाः सुरोत्तमः ॥]{style="color:#0000ff;"}\
[उद्यम्य दिव्यम् आग्नेयम् अस्त्रं देवो रणे महत् ।]{style="color:#0000ff;"}\
[दानवाणां सहस्राणि प्रयुतान्य् अर्बुदानि च ॥]{style="color:#0000ff;"}\
[ददाह भगवान् वह्निः संक्रुद्धः प्रलये यथा ।]{style="color:#0000ff;"}\
[प्राणो यः सर्वभूतानां हृदि तिष्ठति पञ्चधा ॥]{style="color:#0000ff;"}\
Red, red-necked, the destroyer, the giver, the oblation, the poet;\
The purifier, all-consuming, the god, the mouth of the gods, the lord;\
The soul of mantras, brilliant, thousand-rayed, full of light;\
With black tracks, with wonderful rays, the leader of the gods, beautiful, the sole ruler;\
The witness of the worlds, invoked by twice-born, delighting in the वषट् call, bright;\
The oblation-eater, dwelling in wood, self-reproducing, doer of all acts;\
The purifier of all beings, the wealth of the gods' tapas;\
The suppressor of sins, with a flickering tongue, full of heat;\
With helical swirling flames, bright haired, of the form of ritual;\
The oblation-eater, lord of past and future, the partaker of the ritual share, yellow;\
The soma-drinker, of good great luster, the lord of beings, the slayer of all beings;\
The unassailable one, the purifier, the power, verily the core of beings, स्वधा's husband;\
स्वाहा's husband, the Saman song, the filtered-soma consumer, holding the \[soma]-pounding stone;\
The god of gods, of great wrath, of nature of Rudra, born of incantations;\
Having mounted the chariot drawn by red horses with wind-wheels, the maker of beings,\
the smoke-bannered, the smoke-tufted, blue-clothed one, foremost of the gods,\
having raised the divine आग्नेय missile in battle, the great god\
burnt down thousands, millions and tens of millions of दानव-s,\
like in the cosmic dissolution, the enraged lord of fire;\
He, who is the metabolism of all organisms, situate five-fold in their hearts.

The second short snippet alludes to Agni's weapons and the manifestation of the other gods with their weapons. This comes from the narration of the fruits of the tapasya of the deities and presents the appearance of Agni within the frame of the old Vedic ritual of the churning out of the fire in the manner of the Atharvan-s of yore:

[अथ दीक्षां समास्थाय सर्वे विष्णुमया गणाः ।]{style="color:#0000ff;"}\
[पुष्कराद् अग्निम् उद्धत्य प्रणीय च यथाविधि ॥]{style="color:#0000ff;"}\
[जुहुवुर् मन्त्रविधिना ब्राह्मणा मन्त्रचोदिताः ।]{style="color:#0000ff;"}\
[हविषा मन्त्रपूतेन यथा vai vidhir eva ca ॥]{style="color:#0000ff;"}\
[स चाग्निर् विधिवत् तत्र वर्धते ब्रह्मतेजसा ।]{style="color:#0000ff;"}\
[तेजोभिर् बहुलीभूतः प्रभुः पुरुषविग्रहः ॥]{style="color:#0000ff;"}\
[ब्रह्म-दण्ड इति ख्यातो वपुषा निर्दहन्न् इव ।]{style="color:#0000ff;"}\
[दिव्य-रूप-प्रहरणो ह्य् असि-चर्म-धनुर्धरः ॥]{style="color:#0000ff;"}\
[गदी च लाङ्गली चक्री शरी चर्मी परश्वधी ।]{style="color:#0000ff;"}\
[शूली वज्री खड्गपाणिः शक्तिमान् वरकार्मुकी ॥]{style="color:#0000ff;"}\
[विष्णुश् चक्रधरः खड्गी मुसली लाङ्गलायुधः ।]{style="color:#0000ff;"}\
[नरो लाङ्गलम् आलम्ब्य मुसलं च महाबलः ॥]{style="color:#0000ff;"}\
[वज्रम् इन्द्रस् तपोयोगाच् छतपर्वाणम् आक्षिपत् ।]{style="color:#0000ff;"}\
[रुद्रः शूलं पिनाकं च तपसा-धारयत् प्रभुः ॥]{style="color:#0000ff;"}\
[मृत्युर् दण्डं पाशम् आपः स्कन्दः शक्तिम् अगृह्णत ।]{style="color:#0000ff;"}\
[जग्राह परशुं त्वष्टा कुबेरश् च परश्वधम् ॥]{style="color:#0000ff;"}\
Having taken the दीक्षा for ritual all the troops \[of V1s] imbued with विष्णु, churned out Agni from the \[lotus/] pond \[Footnote 2] and led him forth as per the injunctions \[Footnote 3]. Invoked by the ब्राह्मणा-s as per the Vedic instructions, and inspired by mantra incantations, and \[fed with] offerings purified with mantra-s verily as per the injunctions, he, Agni, as per tradition, blazed forth there with brahman luster. The lord, having become manifold with rays, became anthropomorphic. He is known as the rod of brahman, appearing as though burning \[all] with his body. **With weapons of divine form, indeed holding a sword, shield and bow. With a mace, plowshare, discus, arrow, shield, battle-pickax, trident, thunderbolt, he is sword-armed and wields a lance and an excellent bow.** विष्णु is armed with a discus, sword, pestle and a plowshare. Nara of great might is armed with a plowshare and a pestle \[Footnote 4]. United with tapas, Indra strikes with the thunderbolt of a hundred edges. The lord Rudra by tapas has taken up the trident and पिनाक \[Footnote 5]. Yama took the rod, वरुण (literally waters), the lasso and Guha the lance \[Footnote 6]. त्वष्टृ took up an ax and Kubera a battle-pickax.

While the पौराणिक tradition presents accounts of several of the ancient battles between the deva-s and the daitya-s, all surviving versions aim to downgrade the pantheon as represented in the old Aryan layer of the religion for magnifying their sectarian deities. Nevertheless, we believe that the पौराणिक tradition preserves relatively unmodified fragments from an older layer of narratives. This is supported by the sharing of phrases with the old tradition (e.g., वज्रः शतपर्वणः or कृष्णवर्त्मन्) and the fact that above snippets are replete with Vedic allusions and metaphors. Both these snippets present Agni in his old martial form; the first might be seen as mirroring and augmenting the presentation of Agni in the famous कृणुष्व पाज iti पञ्च incantations, while the second mirrors his emergence, upon being churned out, presented in the above-mentioned सूक्त of the Atri-s. Thus, even with all the religious turnover, some of the primal imagery from the old Aryan past continued relatively unchanged in the पौराणिक tradition.

  - -----------------------------------------------------------------------

Footnote 1: Notice the Vedic device of ring-linking in structuring सूक्त-s with old IE roots. The words कृणुष्व...vidhya from the first are repeated in reverse order in the fifth of the कृणुष्व पाज iti पञ्च as विध्याध्य्...कृणुष्व to complete the classic ring. Then an intricate network is formed by other linkages; for example: in the first ऋक्, the two hemistiches are linked by prasitim. In the second ऋक्, they are linked by the root patan+. Then 1 and 2 are linked by the root tap+; prati links 3, 4, 5; 4 and 5 are linked by the root tan+ and so on. For a complete graph, see Figure 1. Such intricate weaving is especially typical of magical incantations, c.f. the Apratiratha Aindra.
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/11/krinushva_paja.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Footnote 2: This is modeled after the action of the primordial Atharvan, which is mentioned to in the श्रुति:  [त्वाम् अग्ने पुष्कराद् अध्य् अथर्वा निर् अमन्थत ।]{style="color:#0000ff;"} RV 6.16.13a. This is a likely allusion to the fire within water found in the regions closer to the homeland of the early Indo-Europeans.

Footnote 3: The Vedic ritual of Agni-प्रणयन with the recitation of the होतृ and the carrying forth of the wooden sword along with the fire to the महावेदि.

Footnote 4: The coupling of Nara and विष्णु situates this narration with the Nara-नारायण tradition of the epic वैष्णव religion where it is often juxtaposed with the more prevalent सात्वत tradition. This verse hints at their "merger" by furnishing Nara with the iconography of the सात्वत संकर्षण.

Footnote 5: The पिनाक should be correctly understood as the bow of Rudra.

Footnote 6: This hemistich has many readings. e.g., The गीता press text reads:  [मृत्युर् दण्डं पाशम् आपः कालः शक्तिमगृह्णत ।]{style="color:#0000ff;"}; The Pune reading from Parashuram Lakshman Vaidya reads: [मृत्युर् दण्डं सपाशं च कालः शक्तिम् अगृह्णत ।]{style="color:#0000ff;"} We take an uncommon southern reading which more congruent with regard to the weapons and the gods.

