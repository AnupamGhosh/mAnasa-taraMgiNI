
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Two exceedingly simple sums related to triangular numbers](https://manasataramgini.wordpress.com/2021/05/26/two-exceedingly-simple-sums-related-to-triangular-numbers/){rel="bookmark"} {#two-exceedingly-simple-sums-related-to-triangular-numbers .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[May 26, 2021]{.entry-date}](https://manasataramgini.wordpress.com/2021/05/26/two-exceedingly-simple-sums-related-to-triangular-numbers/ "5:24 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note records some elementary arithmetic pertaining to triangular numbers for बालबोधन. In our youth we found that having a flexible attitude was good thing while obtaining closed forms for simple sums: for some sums geometry (using methods of proofs pioneered by आर्यभट which continued down to नीलकण्ठ सोमयाजिन्) was the best way to go; for others algebra was better. The intuition was in choosing the right approach for a given sum. We illustrate that with two such sums.

**Sum 1** Obtain a closed form for the sum:  $\displaystyle \sum_{j=1}^{n} (2j-1)^3$ 

These sums define a sequence: **1, 28, 153, 496, 1225...**\
Given that we can mostly only visually operate in 3 spatial dimensions, our intuition suggested that a cubic sum as this is best tackled with brute-force algebra with the formulae for individual terms derived by आर्यभट and his commentators. Thus we have:

 $$\displaystyle \sum_{j=1}^{n} (2j-1)^3 = \sum_{j=1}^{n} 8 j^3 - 12 j^2 + 6 j- 1$$ 

 $$= 2n^2(n+1)^2-2n(n+1)(2n+1)+3n(n+1)-n= \dfrac{(2n^2-1)2n^2}{2}$$ 

The reason we wrote out the final solution in this unsimplified form is to illustrate that the above sums will always be a triangular number of the form:

 $$\displaystyle \sum_{j=1}^{2j^2-1} j$ , i.e sums from 1 to **1, 7, 17, 31, 49...** or triangular numbers  $T_1, T_7, T_{17}, T_{31}\cdots$$ 

Thus, the  $n$ th terms of sequence of sums would be triangular number  $T_m$ , where  $m=2j^2-1, j=1, 2, 3...$ . From the above, one can also see that the difference of successive terms of our original sequence of sums will be **27, 125, 343, 729...**, i.e., they are perfect cubes of the form  $(2k+1)^3$  (odd numbers 3, 5, 7, 9...). These cubes are thus the interstitial sums of the indices  $j$  of the triangular numbers  $T_j$  up to the index  $m$  corresponding to the triangular number  $T_m$  that is a term of our original sequence. Thus:\
 $j\mapsto$  **1**, 2, 3, 4, 5, 6, **7**, 8, 9, 10, 11, 12, 13, 14, 15, 16, **17**, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, **31**...\
then, 2+3+4...+7=**27**; 8+9+10...+17=**125**; 18+19+20+21+22...+31 =**343** and so on.

Another interesting feature of the original sequence is its decadal cycle in the terms of the numbers in the last 2 places (written in anti-Hindu, i.e. modern order). They will always end in the following sequence of 10 numbers:\
**0, 1, 28, 53, 6, 25, 6, 53, 28, 1**\
Similarly, the index  $m$  of the triangular numbers  $T_m$  the define our sequence also shows a pentadic cycle in the last place of the form:\
**1, 7, 7, 1, 9**

A comparable pattern is seen if we generate a sequence that is the sum of successive terms of our original sequence: **1, 29, 181, 649, 1721, 3781, 7309, 12881...** The last place has a pentadic cycle of the form: 1,9,1,9,1. The last 2 places has a cycle of length 25: 01, 29, 81, 49, 21, 81, 09, 81, 69, 41, 61, 89, 81, 89, 61, 41, 69, 81, 09, 81, 21, 49, 81, 29, 01. Both are palindromic cycles.

Finally, the sum of the reciprocals of the original sequence converges to a constant: 1.04607799646... We suspect there is a closed form for this constant but have not been able to identify it.

**Sum 2** Obtain a closed form for the sum of alternating negative and positive perfect squares: -1+4-9+16... i.e.

 $$\displaystyle \sum_{j=1}\^n (-1)\^j j^2$$ 

With the sum involving just square terms it is possible to use a wordless geometric proof along the lines of that proposed by आर्यभट (Figure 1).
```{=latex}
\begin{center}
```

![](https://manasataramgini.wordpress.com/wp-content/uploads/2021/05/sum_neg_pos_squares.png){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

Figure 1\quad
```{=latex}
\end{center}
```


Thus, we get the above sum as  $-1\^n T_n$ , where  $T_n$  is the  $n$ th triangular number.

