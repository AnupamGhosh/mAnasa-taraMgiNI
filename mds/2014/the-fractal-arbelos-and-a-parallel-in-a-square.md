
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The fractal Arbelos and a parallel in a square](https://manasataramgini.wordpress.com/2014/02/25/the-fractal-arbelos-and-a-parallel-in-a-square/){rel="bookmark"} {#the-fractal-arbelos-and-a-parallel-in-a-square .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 25, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/02/25/the-fractal-arbelos-and-a-parallel-in-a-square/ "5:34 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-FXjTPn09z6w/Uwwp4NeFYJI/AAAAAAAAC3w/q5fu2QVqtwY/s800/Arbelos.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-C2eTFVx2LoM/Uwwp4SgYI9I/AAAAAAAAC30/SPXWYoaqF-8/s800/Square_lattice.jpg){width="75%"}
```{=latex}
\end{center}
```



