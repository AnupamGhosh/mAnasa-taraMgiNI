
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A ज़रथुष्त्रिअन् exit at a Hindu funeral](https://manasataramgini.wordpress.com/2014/07/24/a-zarathushtrian-exit-at-a-hindu-funeral/){rel="bookmark"} {#a-ज़रथषतरअन-exit-at-a-hindu-funeral .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 24, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/07/24/a-zarathushtrian-exit-at-a-hindu-funeral/ "6:39 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

This note continues with the examples we have been providing on these pages illustrating the role of the kavi as a naturalist or an observer of nature. The verse below is a macabre one composed by one of  the greatest men among our people, पाणिनि. It might not be an exaggeration to say that we owe the language that was the purveyor Hindu culture and the lingua franca of the Indosphere to him. This verse, like several others that are in circulation, might have come from his lost novel. It describes a scene at the cemetery where a corpse undergoing cremation is appropriated by vultures. ज़रथुष्त्र, the reformulator of the old Iranian religion (which was originally close to the Indo-Aryan version), rebelled against the old Indo-European funeral custom of cremation and went for a less-widely used practice of disposal of the corpse by vultures -- evidently because he felt Atar, the cognate of the holy agni would be defiled by a corpse (especially given the extreme Iranian necrophobia). Indeed, ironically here पानिनि calls the fire hutabhuj or the eater of oblations of the gods, the very fire which ज़रथुष्त्र feared to "contaminate". But in this scene described by पाणिनि the wake of vultures boldly venture to take over the burning corpse from a pyre and compete to feed on it. Given our limitations it took us sometime to work out some aspects of the great man's word-craft. In particular the word धाम्नः puzzled us before we arrived at the solution presented below.

[चञ्चत् पक्षाभिघात ग्लपित-हुतभुजः प्रौढ-धाम्नश् चितायाः]{style="color:#99cc00;"}\
[क्रोडाद् आकृष्ट मूर्तेर् अहम्-अहमिकया चण्ड-चञ्चु-ग्रहेण ।]{style="color:#99cc00;"}\
[सद्यस् तप्तं शवस्य ज्वलद् इव पिशितं भूरि जग्ध्वाऽर्ध-दग्धं]{style="color:#99cc00;"}\
[पश्यान्तः प्लुष्यमाणः प्रविशति सलिलं सत्वरं गृध्र-संघः ॥]{style="color:#99cc00;"}

चञ्चत्= flapping; पक्ष= wing; अभिघात= striking back; glapita= weakened; हुतभुजः= fire (ablative); प्रौढ= violent; धाम्नश्= radiance (ablative); चितायाः= pyre's (genitive)\
क्रोडाद्= lap (ablative); आकृष्ट= dragged out; मूर्तेर्= corpse (ablative); aham-अहमिकया= with one-another; चण्ड= fierce; चञ्चु-ग्रहेण= by the pecks of beaks |\
sadyas= right now; तप्तं= burning; shavasya= corpse (genitive); jvalad= burning; iva= like; पिशितं= flesh; भूरि= abundantly; जग्ध्वा= having eaten; ardha= half; दग्धं= burnt;\
pashya= see (imperative); अन्तः= innards; प्लुष्यमाणः= scorching (present passive participle); pravishati= resort to (present, parasmai); सलिलं= lake (water); सत्वरं= speedily; गृध्र-संघः= vulture-kettle ||

With flapping wings striking the fire to weaken its violent radiance,\
from the middle of the pyre they have dragged out a corpse,\
each one contending with another with pecks of their fierce beaks,\
having abundantly feasted on the freshly roasted flesh of the corpse,\
which's as if glowing and half in flames; See! with scorching innards,\
to the lake the kettle of vultures speedily resorts!


