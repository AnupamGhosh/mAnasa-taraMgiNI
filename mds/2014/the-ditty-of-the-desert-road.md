
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The ditty of the desert road](https://manasataramgini.wordpress.com/2014/12/10/the-ditty-of-the-desert-road/){rel="bookmark"} {#the-ditty-of-the-desert-road .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[December 10, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/12/10/the-ditty-of-the-desert-road/ "7:59 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

He who eats once is a त्यागिन्;\
He who eats twice is a yogin;\
He who eats thrice is a bhogin;\
And he who eats four times a day\
is verily destined to be a rogin!

Some find rest by going home,\
Some find rest on the bed,\
Some find rest in a woman,\
But for some there is no rest,\
except that which death brings.

Eight are the cremation grounds, the महाश्मशान-s:\
चण्डोग्र in the eastern reaches of the वङ्ग-s,\
यमज्वाल where the sea laps the द्रंइड shores,\
वरुणकपाल where the आनर्त-s have their drinks,\
kuberabhairava where one learns महालीलादेवी's teachings,\
श्रीनायक, outside which the andhra-s flock to brothels,\
अट्टहास, wherein चेरिका-s are possessed by भूत-s,\
घोरान्धकार, from beneath which हिङ्गुला prances,\
And किलिकिलारव where संकर्षण slew the ape of gargantuan proportions.

He marched forth seize the ball and horseshoe of शक्तिगोल;\
He was bold in and ruthless in his advance like bhagadatta;\
But he came face-to-face with the उत्क्रान्तिद of the vaivasvata.\
He had all the power of this world but now he is all gone,\
there beneath the yonder stone he lies whitening to bone.

He had attained total mastery of the horseshoe that lay under water,\
much like our old ancestors gained mastery of the fire under water,\
but as he fought great the mleccha lord he met yama-राज्ञ,\
now food for bacteria, the ऋभु-s of life at the श्मशान.

We may ask: were these brought to their final rest by citragupta ?\
Nay, like पारिक्षित-s of yore they sit weaving at the place of yama.\
But some say verily such a क्षत्रिय-s end is no cause of sorrow.


