
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Attenuated fractality- cube and octagon : Dodecahedron and decagon](https://manasataramgini.wordpress.com/2014/07/01/attenuated-fractality-cube-and-octagon-dodecahedron-and-decagon/){rel="bookmark"} {#attenuated-fractality--cube-and-octagon-dodecahedron-and-decagon .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[July 1, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/07/01/attenuated-fractality-cube-and-octagon-dodecahedron-and-decagon/ "6:54 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-qUjhnkxhI9U/U7JYnY8rs4I/AAAAAAAAC8Y/7b-TwrGpguw/s800/smooth_cube.jpg){width="75%"}
```{=latex}
\end{center}
```

```{=latex}
\begin{center}
```

![](https://lh3.googleusercontent.com/-V4G-xKNyEBo/U7JapFnAssI/AAAAAAAAC84/QtnK1SJIGPg/s800/smooth_dodecahedron.jpg){width="75%"}
```{=latex}
\end{center}
```



