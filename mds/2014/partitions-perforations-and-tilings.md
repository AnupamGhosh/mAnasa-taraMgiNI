
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Partitions, perforations and tilings](https://manasataramgini.wordpress.com/2014/10/19/partitions-perforations-and-tilings/){rel="bookmark"} {#partitions-perforations-and-tilings .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 19, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/10/19/partitions-perforations-and-tilings/ "7:22 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We are "geometric" in our thinking -- perhaps, we are hence a little more Greek or the old type Arya than the later Hindu (who is more algebraic) in mentality. Long back in college we were fascinated by implicit trigonometric relations but were utterly defeated by the difficulty in visualizing. Without visualization, of what use are these relations to one who is inclined towards visible geometry? Then we wrote a bit of code that allowed us to visualize the same. Today far more efficient programs exist to do the same and we tend to revisit these relations, perhaps as a reenactment of our childish fancies.

During those explorations of ours we realized that certain class of trigonometric relations exist which produce the following:

 1.  Partitions: These might be defined as divisions of the x-y plane into continuous zones of 2 colors.

 2.  Perforations: These can be defined as completely bounded zones which can be visualized as perforations of the x-y plane.\
Now, there might be relations which generate both partitions and perforations at the same time.

 3.  Depressions and bosses: They might be visualized as incomplete precursors of perforations in the x-y plane with the plane being deformed in either direction by vectors perpendicular to it.

 4.  Tilings: These completely tile the x-y plane by means of bounded tiles that share edges. There might be tiles which bear perforations or depressions and bosses on them.

 5.  Loopings: These might be defined as adjacent perforations that become contiguous via intersections involving a single point.

To illustrate these let us consider the below relationship:

 $$cot(cos^{3} (x-y)+sin^{2} (x+y))> |sin(x+y)-cos(x-y)|$$ 

It produces the below figure below which includes both partitions and perforations (the rhombuses with concave sides).
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-OkzDlBDxfi4/VENhygDC6fI/AAAAAAAADI4/CLoQoPxRdTY/s800/Rhomboid_perforation.jpg){width="75%"}
```{=latex}
\end{center}
```



Some of these can have highly twisted partitions, in this case combined with depressions and bosses, that superficially make it look like a tiling (the figure to the right) as produced by:

 $$tan(13cos^{3}(x-y)+17sin^{3}(x+y))> |sin(x+y)-cos(x-y)|$$ 

![](https://lh6.googleusercontent.com/-6IYJAc_rzCM/VENhzqr0zfI/AAAAAAAADI8/1QBDccwdh0E/s800/partition_rhomboid.jpg){width="75%"}

Now let us consider the relation:

 $$sin(9sin^{2}(x+y) \pm 12cos^{2}(x-y))> |sin(x+y)-cos(x-y)|$$ 

It produces perforations that show some complexity\
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-HPuPu96rc7w/VENhynCqodI/AAAAAAAADI0/DYDjD_P4BMQ/s800/Complex_perforation.jpg){width="75%"}
```{=latex}
\end{center}
```



Now let us consider the relation:

 $$cot(cos(x-y)+sin(x+y))> |sin(x+y)-cos(x-y)|$$ 

This produces a tiling but the tiles respectively have depressions or bosses.\
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-81iTyvf2Qd8/VENhycnU9cI/AAAAAAAADIo/qTR9nzgKLQY/s800/Tiling_boss_depre.jpg){width="75%"}
```{=latex}
\end{center}
```



The introduction of an irrational constant like  $\phi$  adds a new wrinkle to these relations. For example let us consider:

 $$sec(9sin^{8}(x+\phi y)+4cos^{8}(\phi x-y))> |sin(x+y)-cos(x-y)|$$ 

The Golden ratio  $\phi=1.61803398875$ 
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-cuuJRaIa1jY/VEQRbltLixI/AAAAAAAADJM/DAqVGT2BiZ0/s800/beads.jpg){width="75%"}
```{=latex}
\end{center}
```



The irrationality of the constant manifests in the resultant partitions, perforations, depressions/bosses in form an endless diversity of patterns -- i.e. you do not get the same thing again even though they all look similar. This discovery greatly pleased us when we first stumbled upon it.

We shall conclude with a foray into the realm of personal symbolism. Below is what we term the gaze of indra intertwined with the gaze of Odin; produced by:

 $$cot(4sin^{2}(x+y)+13cos^{2}(x-y))> |sin(x+y)-cos(x-y)|$$ 

```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-rvmOAf1IRLU/VESaqPjQc0I/AAAAAAAADJk/M-ShYKaX4Xk/s800/Odin_वरुण.jpg){width="75%"}
```{=latex}
\end{center}
```



Why do we call it so? In Hindu tradition indra is said to have a 1000 eyes with which he watches over all. Hence, those many rows of eyes gazing at us are those of indra. Now in Germanic lore Odin is said to have sacrificed an eye of his at the well of Mimir. While he was left with just one eye, the drink at the well made him all knowing. Hence, we depict the second set of rows as representing the all-knowing gaze of Odin with only one eye being function and the other blind. The ocular anomaly is something he shares with his Hindu cognate rudra. While rudra is typically known as three-eyed, he is also described as being one-eyed in the rudra rahasya-गान of the सामवेद. This ocular trait is inherited by a subset of the ectypes of rudra known as the vidyeshvara-s who form part of the सैद्धान्तिक shaiva pantheon. As an aside one may also note the "black grid" optical illusion produced by it.

The next one is termed the gaze of bhaga and aryaman and is generated by:

 $$cot(3sin^{2}(x+y)+cos^{3}(x-y))> |sin(x+y)-cos(x-y)|$$ 

```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-3P01Fxc2oJ4/VESap0RN_gI/AAAAAAAADJg/E67SXeCgsXc/s800/bhaga_aryaman.jpg){width="75%"}
```{=latex}
\end{center}
```



Here there are two sets of quasi-symmetric face-like figures with eyes and a mouth. Of these aryaman is the far-seeing one with normal eyes, while bhaga is the one with blind eyes. Hindu tradition going back to the shruti records a legend of bhaga losing his eyes when rudra attacked the यज्ञ or prajApati. In the महाभारत the heroes are cast in the image of the ancient gods. In this matrix some of them are very obvious in their resemblance to the gods. Among the non-obvious ones are vidura who is cast in the image of the all-seeing aryaman, who is also the regent of the homestead, and the blind धृतराष्ट्र who is cast in the image of bhaga. Among the Aditya-s the first and the most prominent pair are mitra and वरुण, they are followed by the pair aryaman and bhaga. The former pair is potentially partitioned between पाण्डु and युधिष्ठिर in the महाभारत.

