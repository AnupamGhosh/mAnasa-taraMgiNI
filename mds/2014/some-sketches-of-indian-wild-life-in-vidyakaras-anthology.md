
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some sketches of Indian wild life in विद्याकर's anthology](https://manasataramgini.wordpress.com/2014/03/21/some-sketches-of-indian-wild-life-in-vidyakaras-anthology/){rel="bookmark"} {#some-sketches-of-indian-wild-life-in-वदयकरs-anthology .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 21, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/03/21/some-sketches-of-indian-wild-life-in-vidyakaras-anthology/ "6:35 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[सान्द्र-स्थूल-नलोपरोध-विषमाः शक्यावताराः]{style="color:#99cc00;"}\
[पुरस् तोयोत्तीर्ण-निवृत्त-नक्र-जठर-क्षुण्ण-स्थली-वालुकाः ।]{style="color:#99cc00;"}\
[व्यक्त-व्याघ्र-पदाङ्क-पङ्क्ति-निचितोन्मुद्रार्द्र-पङ्कोदराः]{style="color:#99cc00;"}\
[संत्रासं जनयन्ति कुञ्ज-सरितः काचाभ-नीलोदकाः ॥]{style="color:#99cc00;"}

sAndra: thick; स्थूल: large; nala: reed; uparodha: barrier; विषमाः: rough; shakya: possible; अवताराः: descents; पुरः: first; toya: water; उत्तीर्ण: descended; निवृत्त: climbed back; nakra: crocodile; जठर: belly; क्षुण्ण: roughed up; स्थली: bank; वालुकाः: sands;

vyakta: adorned; व्याघ्र: tiger; pada: foot; अङ्क: print; पङ्क्ति: track; nichita: full of; unmudra: opened; Ardra: wet; पङ्क: mud; उदराः: cavities; saMtrAsaM: terror; janayanti: evoke (plural); कुञ्ज: forest; सरितः: streams; काचाभ: glistening like glass; नील: dark blue; उदकाः: waters;

The forest streams with dark blue waters glistening like glass,\
with thick, large reed-barriers, accessible via rough descents,\
with sand banks roughed up by the bellies of crocodiles,\
that have descended forth into the water and climbed back,\
entirely covered with tracks of tiger footprints,\
where sinkholes have opened in the wet mud,\
evoke terror! \[perhaps by abhinanda]

[वराहान् आक्षेप्तुं कलम-कवल-प्रीत्य्-अभिमुखान्]{style="color:#99cc00;"}\
[इदानीं सीमानः प्रतिविहित-मञ्चाः स्वपतिभिः ।]{style="color:#99cc00;"}\
[कपोतैः पोतार्थं कृत-निबिड-नीडा विटपिनः]{style="color:#99cc00;"}\
[शिखाभिर् वल्मीकाः खर-नखर-खातोदर-मृदः ॥]{style="color:#99cc00;"}

varAhAn: boars (accusative) ; आक्षेप्तुं: to drive away; kalama: rice ; kavala: mouthful; प्रीति: desire; अभिमुखान्: coming; इदानीं: now ; सीमानः: boundaries; prativihita: guarding against; मञ्चाः: platforms ; स्वपतिभिः: landowners (instrumental);

कपोतैः: doves (instrumental); पोतार्थं: for rearing chicks; कृत: make; निबिड: abundant; नीडा: nests; विटपिनः: fig trees; शिखाभिर्: tops; वल्मीकाः: termite mounds; khara: jackal; nakhara: claw; खात: dug up; udara: hole; मृदः: mud (plural);

To drive away boars coming with desire\
of having their fill of the rice crop,\
now landowners set up watch-platforms\
at the boundaries of their fields;\
the fig-treetops have abundant nests,\
of doves wishing to rear their chicks,\
and mud of termite mounds have holes,\
dug up by the claws of jackals. \[by शतानन्द]

[तोयान्तर्-लीन-मीन-प्रचय-विचय-नव्यापृत-त्रोटि-कोटि]{style="color:#99cc00;"}\
[प्राग्-भाग-प्रह्व-कङ्कावलि-धवल-रुचः पर्यटत्-खञ्जरीटाः ।]{style="color:#99cc00;"}\
[कूजत्-कादम्ब-राजी-पिहित-परिसराः शारदीनां नदीनां तीरान्ता]{style="color:#99cc00;"}\
[मञ्जु-गुञ्जन्-मदकल कुरब-श्रेणयः प्रीणयन्ति ॥]{style="color:#99cc00;"}

तोयान्तर्: within water ; lIna: lurking; mIna: fish;-prachaya: increasing; vichaya: examination; navya: fresh; आपृत: engaged; त्रोटि: beak; कोटि: extremity; प्राक्: front; bhAga: part; prahva: stooping; कङ्कावलि: flock of herons; dhavala: white; रुचः: shining; पर्यटत्: wandering around; खञ्जरीटाः: wagtails;

कूजत्: cooing; कादम्ब: duck; राजी: rows; pihita: covered; परिसराः: environs; शारदीनां: of autumnal; नदीनां: rivers; तीरान्ता: banks; मञ्जु: pleasantly; गुञ्जन्: buzzing; madakala: intoxicated; kuraba: Barleria cristata; श्रेणयः: arrays; प्रीणयन्ति: please (causative plural)

Banks of autumnal rivers, with their line of brilliant white herons,\
with their necks stooping forward, currently engaged in probing,\
with the tips of their beaks, a school of fish lurking within the waters,\
with wagtails wandering around, and their environs covered\
by rows of quacking ducks, with arrays of Barleria cristata\
pleasantly buzzing with intoxicated bees, are the cause of much pleasure!

These poetic sketches belong to that large body of काव्य which we have repeatedly alluded to on these pages -- sketches which bring out the kavI as a naturalist.\
Some earlier examples might be revisited here:

  -  [The crows and the parasitic koel by kavi वल्लण](https://manasataramgini.wordpress.com/2013/08/13/the-crows-and-the-parasitic-koel-a-kavis-take/)

  -  [भवभूति's avifauna and flora](https://manasataramgini.wordpress.com/2011/11/02/bhavabhutis-avifauna-and-flora/)

  -  [The kingfisher by वाक्पति-rAja](https://manasataramgini.wordpress.com/2010/07/30/the-kingfisher/)

  -  [The great master कालिदास](https://manasataramgini.wordpress.com/2012/12/24/vignettes-from-the-kumara-sambhava/)

We thought of our own tumultuous life and realized that we experienced some of these sights as of few years ago. More than two decades ago we saw the crocodiles. As of just over an year ago we saw the bumble bees enter the *Barleria cristata* flowers. From the earliest days from when we started observing the dinosaurs of the current era, aided by the man from the पुरंदर fort, we noted the herons probing the autumnal streams for guppies and doves building their chaotic tangles of nests. We have seen many a towering termite mound throughout our existence. But three sights are no longer the share of the साधारण मानव of भारतवर्ष: The tiger, or even is spoor, the boar or the jackal. For these one needs to stray far from the overgrown urban sprawls and journey to those inaccessible refugia that may be counted on ones fingers. This indeed marks the change from the bhArata of the kavI-s to that of today's महानागरिन्-s.


