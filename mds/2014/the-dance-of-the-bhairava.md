
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The dance of the bhairava](https://manasataramgini.wordpress.com/2014/08/05/the-dance-of-the-bhairava/){rel="bookmark"} {#the-dance-of-the-bhairava .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[August 5, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/08/05/the-dance-of-the-bhairava/ "2:27 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-A-QkUzDQ3ds/U906IaaTHFI/AAAAAAAADDA/Gk3E9H-pZzg/s800/bhairava5_nepAl.jpg){width="75%"}
```{=latex}
\end{center}
```



*bhairava on a cadaver from Nepal*

[नेत्राकुञ्चन-सारण-क्रम-कृत-प्रव्यक्त-नक्तं दिनो]{style="color:#99cc00;"}\
[दिक्-छक्रान्त-विसर्पि-सल्लरि-सटा-भारावरुद्धाम्बरः ।]{style="color:#99cc00;"}\
[हस्त-न्यस्त-कपाल-कन्दर-दरी-मुक्ताभ्र-धाराः पिबन्न्]{style="color:#99cc00;"}\
[उन्मुक्त-ध्वनि-भिन्न-कर्ण-कुहरः क्रव्याद् अयं नृत्यति ॥]{style="color:#99cc00;"}

netra= eye; आकुञ्चन= closing; सारण= opening; krama= periodic; कृत= act; pravyakta= manifest; नक्तं= night; dinaH= day; dik= direction; chakra= circle; anta= ends; [दिक्चक्रान्त= horizon] visarpi= spreading; sallari=lashing (onomatopoeic) ; सटा= bristles; bhAra= mass; avaruddha= covered; अम्बरः= sky

hasta= hand; nyasta= held; कपाल= skull; kandara= bowl; darI= fissure; mukta= release; abhra= cloud; धाराः= streams; pibann= drinking; unmukta= let out; dhvani= noise; bhinna= split; कर्ण-कुहरः= ear-drum; क्रव्याद्= corpse-eater; अयं= this; नृत्यति= dances.

By the periodic closing and opening of his eyes he has made day and night manifest;\
By spreading his lashing, bristly mass of hair, he has covered the sky till the horizons;\
Drinking cloud-streams released from the fissures of the skull bowl held in his hands,\
having shattered ear-drums with the roar he has let out, this corpse-eater dances!

The bhairava ectype of rudra became central to the shaiva traditions related to the कापालिक branch of the old पाशुपत form of the shaiva-शासन. Some साधक-s of these traditions engage in निःशङ्काचर, which involves practices such as consumption of human flesh in cremation grounds. This is indeed the practice alluded to here. This might have deep roots earlier Hindu thought. In the shruti, the deva rudra is closely associated with agni -- to the point that they are explicitly identified with each other right from the ऋग्वेद (e.g. [त्वम् अग्ने रुद्रो असुरो महोदिवः ।]{style="color:#99cc00;"}). The deva agni is said to come in two earthly forms: 1) हव्यवाह -- one who carries the oblations to the gods; 2) क्रव्याद -- one who devours the corpse on the cremation pile and carries oblations to the deceased ancestors. The former is auspicious and the later is inauspicious [(क्रव्यादम् अग्निं प्र हिणोमि दूरं ।]{style="color:#99cc00;"} i.e. I send afar the corpse-eating agni). This dichotomy in agni is also reflected in rudra: on one hand he is shiva (auspicious) and daivyo भिषक् (the divine physician) or जलाष-भेषज (one with anti-fever drugs) on the other hand he is the terrifying rudra who is described as goghna (cow-killing) and पूरुषघ्न (man-killing), traits shared with his Greek cognate Apollon.

Given these Vedic antecedents, in the later days of the shaiva-शासन the bhairava was conceived after the image of agni क्रव्याद: His common iconography (as seen above at the Durbar Square, Kathmandu) depicts him as standing upon a corpse with flaming hair. Thus, he appears to be a personification of agni क्रव्याद, as he is called in the above verse. However, it is clear from the verse that the bhairava here is the supreme deity of the shaiva शासन with whom the साधक attains unity in his ritual performance in the श्मशान. It is probably in this context he utters such verses describing the universal nature of the bhairava: The closings and opening of his eyes are said to be the nights and days. This theme is common in shaiva lore where the universe is said to have been engulfed in darkness when उमा closed rudra's eyes at the time of the creation of andhaka. His bristly hair is said to fill the universe. In the shaiva lore this is captured by the name of shiva as vyomakesha. For example, in the उज्जयिनी copper plate of the परमार rAjpUt yashovarma-deva, a descendent of the great king bhoja-deva we find the following:

[जयति व्योमकेशो ऽसौ यः सर्गाय बिभर्ति तां]{style="color:#99cc00;"}\
[ऐन्दवी शिरसा लेखां जगद् बीजाङ्कुराकृतिम् ॥]{style="color:#99cc00;"}\
Victorious is he whose hair fills the space (vyomakesha), who for the emitted worlds (sarga) bears with his head the lunar crescent which is like the embryo in the seed which is the universe.

For a modern the metaphor of the hair of the bhairava filling space would, not surprisingly, evoke the imagery of the lines of force fields filling the universe. The verse then talks of the bhairava dancing on the cremation group drinking water from the fissures in the skull bowl. This metaphor has two elements to it. In the celestial plane the clouds pouring rain are identified with the कपाल held by the bhairava. On the other hand the water flowing down from the fissure of the skull is a metaphor for the concept of अमृत-स्रवण (streaming of nectar) from within the skull, which a yogin is supposed to experience as part of practices associated with भैरवाचर (e.g. खेचरी mudra). Finally, the verse talks about the ear-splitting noise, which relates to the beating of the Damaru (the hour-glass drum) accompanying the dance of the bhairava. While filling the air with noise, tradition also records that the noises from drum of rudra form the basis of the grammar of पाणिनि (i.e. the माहेश्वर सूत्राणि).


