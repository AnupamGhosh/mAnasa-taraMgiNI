
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The Mandel-diamond: crystals emerge from an amorphous background](https://manasataramgini.wordpress.com/2014/10/09/the-mandel-diamond-crystals-emerge-from-an-amorphous-background/){rel="bookmark"} {#the-mandel-diamond-crystals-emerge-from-an-amorphous-background .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 9, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/10/09/the-mandel-diamond-crystals-emerge-from-an-amorphous-background/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-3eblxZAP_5I/VDYt3_NAzMI/AAAAAAAADII/sM0bopBo8K8/s800/mandel_diamond.jpg){width="75%"}
```{=latex}
\end{center}
```



**A closer look at realms within this "Terra Mysterium"**

*Crystals by the shore-line:*\
```{=latex}
\begin{center}
```

![](https://lh5.googleusercontent.com/-c5lzM7FbUYM/VDYt52nvGrI/AAAAAAAADHk/luuW0GMmqYA/s800/mandel_diamond_magified2.jpg){width="75%"}
```{=latex}
\end{center}
```



*Crystals in the crevice -- realm of craters:*\
```{=latex}
\begin{center}
```

![](https://lh4.googleusercontent.com/-jCrlj1niNWI/VDYt5wUKJtI/AAAAAAAADHo/9u9w1b8jen4/s800/mandel_diamond_magified.jpg){width="75%"}
```{=latex}
\end{center}
```



