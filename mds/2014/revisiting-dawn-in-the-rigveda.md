
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Revisiting dawn in the ऋग्वेद](https://manasataramgini.wordpress.com/2014/06/16/revisiting-dawn-in-the-rigveda/){rel="bookmark"} {#revisiting-dawn-in-the-ऋगवद .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 16, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/06/16/revisiting-dawn-in-the-rigveda/ "7:00 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

One of the most physically and mentally demanding performances in the agnichayana and other soma rituals based on that model is the अतिरात्र or the overnight rite. The highpoint of it is the great ritual to the gods agni and the ashvin-s and the goddess उषस्. The sAman songs to these deities from the rahasya-गान section are sung at the beginning of this ritual. These songs collectively go by the name संधिस्तोत्र indicating that they are sung at the junction between night and the daybreak, i.e., the dawn. That they are linked with the dawn presided by the goddess उषस् is clear from one set of these songs known as the उषाणि, which are thus deployed by the सामवेदिc singers:\
[हिं ।]{style="color:#0000ff;"}\
[प्रस्ताव: प्राति वो दर्श्यायतो३ वा ।]{style="color:#0000ff;"}\
[उद्गीथ: ओ३ च्छन्ती दुहिता दिवो३ पो३ महि वृणुते चक्षुषा तमः ।]{style="color:#0000ff;"}\
[प्रतिहार: ज्यो३तीः कृणोति ।]{style="color:#0000ff;"}\
[उपद्रव: इ ळा ओ३ नारी ओ३ वा हा वु वा ।]{style="color:#0000ff;"}\
[ nidhana: As ||]{style="color:#0000ff;"}

[प्रस्ताव: ज्यो३तो३ वा ।]{style="color:#0000ff;"}\
[उद्गीथ: ओ३ णो३ति सूनरी उदुस्रियास् सृजते३ सूरियास् सचा ।]{style="color:#0000ff;"}\
[प्रतिहार: उद्यन् नाक्षत्रां ।]{style="color:#0000ff;"}\
[उपद्रव: इ ळा ओ३र्छा यि वा दो३ वा हा वु वा ।]{style="color:#0000ff;"}\
[ nidhana: As |]{style="color:#0000ff;"}

[प्रस्ताव: उद्यो३ वा ।]{style="color:#0000ff;"}\
[उद्गीथ: ओ३ क्षत्रम् अर्चिवत् तवे३द् उषो३ व्युषि सूरियास्य च ।]{style="color:#0000ff;"}\
[प्रतिहार: सं भक्ते३ना ग ।]{style="color:#0000ff;"}\
[ upadrava: i LA o3 mahi o3 वा hA vu वा |]{style="color:#0000ff;"}\
[ nidhana: As |]{style="color:#0000ff;"}

The प्रस्ताव is sung by the प्रस्तोता, the उद्गीथ by the उद्गाता, the प्रतिहार by the प्रतिहर्ता, the upadrava by the उद्गाता, and the nidhana by all three.

The lyrics of this rahasya गान are based on the mantra-s of the मैत्रावरुणि वसिष्ठ:\
[प्रत्य् उ अदर्श्य् आयत्य् उच्छन्ती दुहिता दिवः ।]{style="color:#0000ff;"}\
[अपो महि व्ययति चक्षसे तमो ज्योतिष् कृणोति सूनरी ॥]{style="color:#0000ff;"}\
The daughter of dyaus has appeared now, shining as she nears;\
The noble lady rolls up great darkness and makes light for us to see.

[उद् उस्रियाः सृजते सूर्यः सचां उद्यन् नक्षत्रम् अर्चिवत् ।]{style="color:#0000ff;"}\
[तवेद् उषो व्युषि सूर्यस्य च सम् भक्तेन गमेमहि ॥]{style="color:#0000ff;"}\
The sun frees the red cows while he the radiant star rises high;\
May we acquire our share \[of wealth] when you O उषस् and the sun's light come forth! (RV 7.81.1-2)

This is followed by the recitation of the thousand mantra-s of the Ashvina shastra by the hotr\^i, which is a great feat of memory for they are recalled from the ऋग्वेद in a rather jumbled order, i.e., mantra-s from different मण्डल come in different places in the recitations. This rite to the ashvin-s (evidently in its earlier form) is alluded to within the ऋग्वेद itself by मैत्रावरुणि वसिष्ठ. This reference not only supports its antiquity but also suggests it was performed at dawn:

[अशोच्य् अग्निः समिधानो अस्मे उपो अदृश्रन् तमसश् चिद् अन्ताः ।]{style="color:#0000ff;"}\
[अचेति केतुर् उषसः पुरस्ताच् छ्रिये दिवो दुहितुर् जायमानः ॥]{style="color:#0000ff;"}\
*agni has shone brightly when kindled (using samidh-s) by us;*\
*boundaries of darkness were visible.*\
*Eastward is seen the banner of उषस्,*\
*one born to \[indicate] the glory of dyaus' daughter.*

[अभि वां नूनम् अश्विना सुहोता स्तोमैः सिषक्ति नासत्या विवक्वान् ।]{style="color:#0000ff;"}\
[पूर्वीभिर् यातम् पथ्याभिर् अर्वाक् स्वर्विदा वसुमता रथेन ॥]{style="color:#0000ff;"}\
*The good होतृ faces you, O ashvin-s,*\
*now the eloquent chanter attends you with the recitation, O नासत्य-s!*\
*Come by the paths you have formerly traveled by,*\
*by the chariot that finds the light, laden with treasure.* (RV 7.67.2-3)

The great Hindu patriot Lokamanya Tilak, based on the nature of the Vedic references to dawns, postulated that the composers of the veda lived in, or were familiar with, extreme northern latitudes. In particular, the case Ashvina-shastra impressed him the most. He realized that the whole long recitation of 1000 mantra-s was completed before sunrise, which implied a long dawn, much longer than the ones we see in India. Based on this and other allusions he outlined this hypothesis of his in the "Arctic Home in the Vedas." The title and the form he presented the hypothesis was somewhat unfortunate because it gave rise to the impression that the ancestors of the Indo-Aryans lived inside the Arctic circle (i.e. 66.5°N latitude). While that is unlikely, it is clear from several lines of evidence in the ऋग्वेद, chiefly, familiarity and fear of snow/cold winters, centrality of the fire cult, and the natural habitat of Ephedra (soma), that the ancestors of the Indo-Aryans, at some point temporally close to the composition of the RV, lived at a fairly northern latitude in Eurasia. Moreover the evidence from the "fire in water legend" places them in the vicinity of the Caspian or Black Seas, which have the characteristic geological setting with fossil hydrocarbon-powered mud volcanoes that could inspire this legend. On the other hand, linguistic evidence for contacts with Uralic place them even further north, perhaps in the zone just north of the above lakes closer to the Urals (\~47-50°N latitude). This contact with Uralic is also consistent with the specific evolutionary connections between Baltic and Slavic one hand and Indo-Iranian on the other. This suggests that early Indo-Iranian was positioned in proximity to these northerly branches of IE. In light of these points, we are sympathetic to Tilak's idea that it is likely that the Vedic references point to long dawns. Indeed, the cognate goddess presiding over the dawn was an important one in several branches of Indo-European, who appears to have faded away in several branches in subsequent times. Thus, cognates of Indo-Aryan उषस् include, Iranian उषा (as in the Avestan incantation उषाहिन् गाः; also vî-daêvô-dAta 19.28 and yasna haptanghaiti 50.10), the Greek Eos, Latin Aurora and Lithuanian Aushrine. Aushrine seems to have been conflated with Venus in Lithuanian legend after the destruction of their ancestral religion (cf. indraja becoming Lithuanian name of Jupiter). All these cognate deities are described using poetic phrases similar to those used for the उषस् in the veda. Thus, it appears that dawns were a prominent phenomenon in the Indo-European homeland, just as implied by the Vedic memory of the same.

Returning to Tilak's interpretation of the Vedic dawn, there is one mantra in the RV relevant to his hypothesis, which has been difficult to understand. Tilak himself took it as evidence in support of his contention while other interpreters of the RV have tended to look at it differently. We revisit this mantra and see what it might mean.

[शश्वत् पुरोषा व्य् उवास देव्य् अथो अद्येदं व्य् आवो मघोनी ।]{style="color:#0000ff;"}\
[अथो व्य् उच्छाद् उत्तरां अनु द्यून् अजरामृता चरति स्वधाभिः ॥]{style="color:#0000ff;"}RV 1.113.13 by kutsa आञ्गिरस

shashvat= continually; पुरा= formerly उषाः= उषस्, nominative singular; vi= verb prefix; उवास= had shone, perfect, third person of verb vas (class 6); देवी= goddess; atha= moreover; u= emphatic particle; adya= today; इदं= this, 3rd person singular neuter; vi= verb prefix; आवः= shone (see below for discussion) मघोनी= munificent lady.

atha= now ; u= emphatic particle; vi= verb prefix; उच्छात्= may light (see below); ut .अतरान्= future, the terminal nasal is pronounced as pure अनुनासिक in recitation; anu= along; द्यून्= days, masculine accusative plural; ajara= undecaying; अमृता= immortal; charati= moves, present, 3rd person singular; स्वधाभिः= by her own powers, feminine instrumental plural.

Thus with the above analysis one may translate the mantra thus:\
*Formerly, the goddess उषस् had continually shone forth;*\
*now then the munificent lady shined this \[light] today.*\
*Now then, may she light up the days that will come along;*\
*immortal and undecaying she moves on by her own powers.*

Now this mantra poses the following problems of note:

 1.  The first पाद has been interpreted by most as meaning from the former times the goddess उषस् has been continually shining forth. This is taken as a symmetric contrast to the future expectation expressed in the 3rd पाद for the days that will come along. But the point to note is that the verb vi-vas is used in the perfect tense (vi-उवास; पाणिनि liT): it is a regular active perfect of the class 6 verb vas. Now traditionally liT is used to convey परोक्ष-भूत or a distant past that has not been witnessed directly by the speaker. Indeed such a narrative tense is shared between Sanskrit and Greek and commonly used in both traditions while recounting AkhyAna-s or epic events. Hence, in the strictly grammatically sense the पाद would mean: "Formerly, the goddess उषस् had continually shone forth" just as the great commentator सायण and Tilak had interpreted it. Thus, Tilak's idea that the mantra-composer was familiar with a former time when there was a continuous dawn is not technically incorrect. This might imply that the author of this mantra himself was not in the region with the long or the continually shining dawn but his ancestors were and thus he had heard of the same.


 2.  vi-आवः is a peculiar verb form that does not appear in the regular conjugation of the class 6 verb vi-vas. It is found only in the older parts of the veda. सायण informs us in different places in his commentary that it is derived from either लङ् (imperfect past) or लुङ् (aorist past) via luk, i.e. elision of the विकरण or the conjugational affix inserted between the root and the termination, followed by further Vedic-specific transformations (lopa of hal, i.e. पाणिनि's designation for terminal consonant). Of the past tenses, लङ् is termed anadyatana-भूत (used for a past that has not taken place today). While लङ् is often used in conjunction with liT (परोक्ष-भूत), in this context the adverb adya is used suggesting it is a past proximal to the present events. Hence, it should be taken as a लुङ्, i.e. the aorist which can express adyatana-भूत (past continuous with the present).

This interpretation of vi-आवः is consistent with another occurrence of it in a mantra of दीर्घतमस् औचाथ्य in a similar context relating to the dawn Ashvina ritual:

[अबोध्य् अग्निर् ज्म उद् एति सूर्यो व्य् उषाश् चन्द्रा मह्य् आवो अर्चिषा ।]{style="color:#0000ff;"}\
[आयुक्षाताम् अश्विना यातवे रथं प्रासावीद् देवः सविता जगत् पृथक् ॥]{style="color:#0000ff;"} (RV 1.157.1)\
The verbs herein are: abodhi= was awakened, 3rd person singular atmanepada aorist of class 1 verb budh; ud-eti= rises up, 3rd person singular present of class 2 verb i; vi-आवः= shone forth, like above ऋक् in the context of उषस्; आयुक्षाताम्= was yoked, 3rd person dual atmanepada aorist of class 7 verb yuj, technically अयुक्षाताम् but the first syllable is metrically elongated; pra-असावीत्= 3rd person singular aorist of class 6 verb सू;

Thus the above mantra has 5 verbs of which 4 are indicate past actions that precede the rising of the sun indicated in present tense. Of these 3 are clear लुङ्, i.e. aorists, indicating past actions just prior of the rising of the sun. Thus, the 4th of this group आवः is also likely to follow them in being a specialized लुङ् derivative occurring only in the veda. In his commentary on this mantra, सायण indicates it is exactly that -- a लुङ्, which has been modified in Chandas.


 3.  vi-उच्छात् occurring the 3rd पाद of RV 1.113.13 is another unusual verb form. सायण tells us that it is a conjugational form of vi-vas just like आवः and the perfect form उवास. He further informs us that it is a veda-specific conjugational form, leT. This form is taken to imply आशीः, i.e. the hope that something will happen (generally taken to imply a mood stronger than opative, लिङ्, and weaker than imperative, loT); the subjunctive mood of the white indologists.

One may ask if there are any other definitive indicators in support of Tilak's contention that the Vedic dawns were long. We do think that the very prominence of the dawns, the mention of multiple changing colors from purple to "white" are suggestive of this. Moreover, we also think the legend of indra smashing the cart of उषस् is another point supporting this. On multiple occasions indra is described as setting free or giving rise to the उषस् and सूrya (e.g. "[यः सूर्यं य उषसं जजान]{style="color:#0000ff;"}" in RV 2.12.7). On the other hand he is described in 4 different सूक्त-s as smashing the cart of उषस् with his vajra: RV 2.15.6; RV 4.30.8-11; RV 10.73.6; RV 10.138.5. The cart of उषस् is described as being slow ([अजवसः]{style="color:#0000ff;"}in RV 2.15.6) as compared to the fast chariot of indra. It is also explicitly described as being an anas, the cognate of Latin onus -- a slow burden-bearing vehicle. This suggests that the legend arose from the dawn being drawn out as alluded to in RV 1.113.13. While the beauty of the dawn is celebrated in the deity उषस्, on the flip side the long dawn delays actual day break. Hence, she is described as causing trouble (RV 4.30.8) while expanding her glory (RV 4.30.9). Thus, indra is seen as clearing the way for the sun to rise by causing उषस् to flee (ending dawn) by destroying her cart with a strike of his bolt. Hence, this legend, which was seen as a mysterious one by the white indologist Oldenburg, is in our opinion a fairly clear memory of long dawns in the ऋग्वेद.

In conclusion, while it may be a far stretch to place the home of the Indo-Aryans inside the Arctic circle, we do feel Tilak's observation regarding the long dawns in the veda is valid and consistent with the idea of the ancient home of the Indo-Aryans north of the Caucasus.


