
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A stotra to shiva and विष्णु with a few allusions to कुमार thrown in](https://manasataramgini.wordpress.com/2014/06/19/a-stotra-to-shiva-and-vishnu-with-a-few-allusions-to-kumara-thrown-in/){rel="bookmark"} {#a-stotra-to-shiva-and-वषण-with-a-few-allusions-to-कमर-thrown-in .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 19, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/06/19/a-stotra-to-shiva-and-vishnu-with-a-few-allusions-to-kumara-thrown-in/ "6:55 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The stotra is attributed to the ऋषि agastya and is embedded in the वामन पुराण:\
```{=latex}
\begin{center}
```

![](https://manasataramgini.files.wordpress.com/2014/06/shivavishnu2.jpg){width="75%"}
```{=latex}
\end{center}
```



