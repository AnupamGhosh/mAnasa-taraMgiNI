
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Polycentrism, the many-one problem and the roots of yoga](https://manasataramgini.wordpress.com/2014/11/08/polycentrism-the-many-one-problem-and-the-roots-of-yoga/){rel="bookmark"} {#polycentrism-the-many-one-problem-and-the-roots-of-yoga .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[November 8, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/11/08/polycentrism-the-many-one-problem-and-the-roots-of-yoga/ "7:50 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

An updated version of our earlier ramblings first published at: [http://www.indiafacts.co.in/polycentrism-many-one-problem-roots-yoga/](http://www.indiafacts.co.in/polycentrism-many-one-problem-roots-yoga/){rel="nofollow"}

One of the central problems Hindus face on the intellectual battlefield is articulating their position vis-a-vis the stances of their Abrahamistic foes. An important aspect of this problem is the difficulty faced by many modern Hindus in understanding the contrast between their system and the exclusivity, belligerence and universalism of Mohammedanism and cult of Jesus. Thus, we see numerous Hindus taking a rather apologetic stance with respect to their polytheistic system. They might even try to deny it by claiming that in reality they are monotheistic, just as the followers of Mohammed and Jesus. We also see this tendency in daily life: many Hindus use terminology like "God", in a sense similar to that used by the Abrahamists, instead of using their own rich language full of nuance with terms such as आत्मन्, brahman, ईश्वर, or the specific names of deities from their pantheon. In our opinion the lack of clarity on these issues directly contributes to their flaccid response to the aggression of the Abrahamistic foes. Consequently, the use of terminology in an Abrahamistic sense can be seen as a memetic Trojan horse, left behind by the Mohammedans and Christians, after their physical conquests were nullified to an extent by the Hindu fight-back culminating in independence. Hence, we speculate that the adoption of Abrahamistic terminology in this regard could actually "soften" the Hindus who do so, and facilitate their fall to the rampant evangelism of the two Abrahamistic cults in India.

As the great scholar of Indic tradition, श्री Lokesh Chandra explained in an interview \[Footnote 1], it is important that India retain and celebrate its "polycentrism" rather than monotheism, as the former is the defining feature of our civilization. Indeed, the great sage उद्दालक आरुणि states in the veda:

[एकम् एव अद्वितीयम् । तद् ऐक्षत बहु स्याम् प्रजायेय ।]{style="color:#99cc00;"} छन्दोग्योपनिषद् in section 6.2\
It was only one without a second. It willed: "I shall multiply and reproduce as many."

Thus, in the in the Hindu system the one is seen as becoming many. Here the arrow of causation accepts and emphasizes the emergence of multiplicity as being the natural order. This is contrary to the Abrahamistic system of monotheism where the unitary deity is defined by exclusion of all others. Thus, the multiplicity, which was the natural state in the pre-Abrahamistic Afro-Asiatic heathen systems of West Asia, was force-fitted to a single standard. The multiplicity inherent in the Hindu system is consonant with the nature of existence itself: At the origin of the universe, in the earliest period, the unitary force manifested as the four forces of nature. Likewise, unitary matter manifested as particles with a multiplicity of masses, charges, 'colors', spins and other properties. Analogously, emergence of multiplicity characterizes life, where all its diversity, with manifold organisms including ourselves, has emerged from a single ancestral organism through the process of reproduction ("[बहु स्याम् प्रजायेय]{style="color:#99cc00;"}").

Hence, the ऋषि गृत्समद शौनहोत्र states:\
[यो अप्स्व् आ शुचिना दैव्येन ऋतावाजस्र उर्विया विभाति ।]{style="color:#99cc00;"}\
[वया इद् अन्या भुवनान्य् अस्य प्र जायन्ते वीरुधश् च प्रजाभिः ॥]{style="color:#99cc00;"} ऋग्वेद 2.35.8\
All other beings are, as it were, branches of him, the plants, with their progeny, are born (of him), who, imbued with the natural law (ऋत), eternal, and widely-spreading, shines amid the waters with pure and divine (radiance).

Similarly, the veda states:\
[अजायमानो बहुधा विजायते ।]{style="color:#99cc00;"} तैत्तिरीय+आरण्यक in 3.13.1\
The unborn is multiply born (i.e. the unmanifest manifests multiply).

Thus, for the Hindu the multiplicity of forms and the unmanifest are not a contradiction. Rather the direction of causation again emphasizes the emergence of a multiplicity of forms from what is unmanifest or not undergoing birth. This is again in contradiction to Abrahamistic thought, which rails and rants against form, leading to iconoclastic urges that seek to destroy all heathen depictions of divinities in diverse forms. Here again, the Hindu system is consonant with nature: from the incredibly dense singularity (i.e. the unmanifest) the universe with all its forms originated. From unmanifest instructions in genes and proteins present in a single-celled zygote a whole organism with a multiplicity of cell types can emerge via repeated reproduction. Thus, via its unitary emphasis and proscription of the manifestation of multiplicity, Abrahamistic thought is like a still-born embryo where the zygote never proceeded beyond the single-celled stage.

The heathens have had a long and sophisticated history of analyzing the basic philosophical question in this regard. The core of this question tackled both by Hindu and early Greek philosophies was what can be termed "the many-one problem". One way of stating it is that there are many identical, similar or congruent entities, that just appear to be multiple manifestations of one single prototype. So how do we deal with the multiplicity while recognizing the unifying principle within them? In Sanskrit grammar we can reduce the various expressions of language to a relatively small set of prototypical rules -- this was the means by which the great पाणिनि formulated his monumental grammar. Similarly, we can classify the diversity of organisms to a few prototypes based on the principle of homology. Thus, many can be explained as few. Next, one might logically ask if this can be taken to the minimal prototype (may be just one), which is the foundational reality, but at the same time also explain the multiplicity logically rather than deny, exclude or proscribe it as is typical of Abrahamistic monotheism. To this end the Hindus applied themselves diligently.

The analysis of this question lies at the origin of one of the pillars of Hindu thought and religion -- सांख्य-yoga. सांख्य emerges in "thought-matrix" of the उपनिषद्-s: in section 6.13 of the श्वेताश्वतर+उपनिषद् we hear that सांख्य is the basis of मोक्ष. i.e., the soteriological conclusion of the realization of the unifying principle. The essential premise of the सांख्य-class of explanations for the many-one problem derives from a concept termed माया, which goes back to the ancient Indo-Aryan past. Knowing माया is knowing the ways of the deva-s! The deva-s exhibit माया: thus, their one true or prototypic form appears in many diverse forms. This is true of indra in the ऋग्वेद. There the ऋषि विश्वामित्र says:

[रूपम्-रूपं मघवा बोभवीति मायाह् कृन्वानस् तनुवं परि स्वां ।]{style="color:#99cc00;"}(RV 3.53.8).\
maghavan (indra) transforms into form after form, effecting the display of माया around his own body.

In the इतिहास-s, विष्णु and rudra put forth their माया to assume many forms. In the पुराण corpus this माया is the great goddess माहाकाली also called yoga-माया. Thus, right from the beginning the term माया appears to have implied the means by which the deva or दानव creates many forms which "veil" the underlying primal form. Thus, one Hindu approach to the "many-one" problem appears to have been a logical extension of this idea to describe the universe itself. Hence, in सांख्य we have the one पुरुष associating with प्रकृति, who acting like the principle of माया creates a multiplicity that is seen as the universe. This connection between the माया of deva-s and सांख्य theory is very palpable in an explanation of universe offered in an exposition of सांख्य in the महाभारत:

[अपां फेनोपमं लोकं विष्णोर् माया शतैर् वृतम् ।]{style="color:#99cc00;"}\
[चित्त-भित्ति प्रतीकाशं नल सारम् अनर्थकम् ॥]{style="color:#99cc00;"} (Mbh-"critical" 12.290.57)\
The universe is like the foam of water enveloped by hundreds of माया-s of विष्णु, like an illusory wall and ephemeral as sap in a \[hollow] reed.

The use of माया by deva-s leads to yoga, which was originally seen as a system of praxis involving the direct application of सांख्य principles. Evidence for this is abundant in the इतिहास-s and पुराण-s. One of the most famous expressions of this, known to most Hindus, is the statement of कृष्ण in the bhagavad गीता:

[नाहं प्रकाशः सर्वस्य योग-माया-समावृतः ।]{style="color:#99cc00;"}\
[मूढो'यं नाभिजानाति लोको माम्-अजम्-अव्ययं ॥]{style="color:#99cc00;"} (BG7.25)\
I am not manifest to all, veiled by the माया of my yoga. This deluded world \[i.e. entrapped by the display of माया] knows not me, unborn and unlimited.

A more palpable application of माया displayed via yoga is described in the महाभारत, when at sunset on the fourteenth day of the Great War, देवकी-putra turning to arjuna says: "arjuna, I have obscured the sun by the means of my yoga and the kaurava-s think it has set. Kill jayadratha!"

Another poignant expression of the fact that the application of yoga is essentially the same as the display of माया is suggested by the story of the ancient भार्गव sage उशनस् काव्य given in महाभारत 12.278 ("critical edition"). काव्य was enraged with the deva-s because विष्णु had beheaded his mother, who was a partisan of the asura-s. काय्व used his yoga to enter into kubera, the यक्ष who was the treasurer of the deva-s and stole his wealth. Furious, kubera went to rudra and told him that काव्य used his yoga to enter his \[kubera]'s body and having robbed him of his wealth came out of it and escaped. rudra, himself of supreme yoga power, raised his dreaded trident and sought काव्य with the intention of striking him. काव्य realized from a distance the intention of rudra of superior yoga powers and wondered whether he should flee or try some other trick. Then using his mighty yoga, उशनस् काव्य, the prefect and physician of the daitya-s, became small and went and sat on the tip of rudra's trident. Unable to use his weapon he bent it with his arms to make it into the पिनाक bow! At this point उशनस् fell into शिव's hand, who promptly swallowed him and returned to perform his meditative yoga. The भार्गव wandered endlessly in rudra's stomach and was absorbed into his body. As शिव had shut all his outlets in practice of yoga, he was unable to find an exit. Unable to escape he repeatedly worshiped the terrible महादेव, who asked him to emerge from his semen. Thus, did the भृगु drop out. When rudra saw him he raised his trident to kill him. But उमा intervened and asked him to spare the ब्राह्मण's life.

The point of note here is that the famous भार्गव magic of परकाय-प्रवेश is effected by means of yoga, which is parallel to the ability of deva-s to exhibit माया transforming their bodies into many forms. This is further clarified in the great इतिहास in course of the description of yoga:

[ब्रह्माणम्-ईशम् वरदं च विष्णुम्]{style="color:#99cc00;"}\
[भवं च धर्मं च षडाननं च]{style="color:#99cc00;"}\
[सो ब्रह्मपुत्रांश्-च महानुभावान् ॥ ५८]{style="color:#99cc00;"}\
[तमश्-च कष्टं सुमहद्-रजश्-च]{style="color:#99cc00;"}\
[सत्त्वं च शुद्धं प्रकृतिं परां च]{style="color:#99cc00;"}\
[सिद्धिं च देवीं वरुणस्य पत्नीं]{style="color:#99cc00;"}\
[तेजश्च कृत्स्नं सुमहच्-च धैर्यं ॥ ५९]{style="color:#99cc00;"}\
[नराधिपं वै विमलं सतारं]{style="color:#99cc00;"}\
[विश्वांश्-च देवान् उरगान् पितॄंश् च]{style="color:#99cc00;"}\
[शैलांश्-च कृत्स्नानुदधींश्-च घोरान्]{style="color:#99cc00;"}\
[नदीश्-च सर्वाः सवनन् घनांश्-च ॥ ६०]{style="color:#99cc00;"}\
[नागान्-नगान्-यक्ष-गणान्-दिशश्-च]{style="color:#99cc00;"}\
[गन्धर्व-सङ्घान्-पुरुषान्-स्त्रियश्-च ।]{style="color:#99cc00;"}\
[परस्परं प्राप्य महान्-महात्मा]{style="color:#99cc00;"}\
[विशेत योगी नचिराद्विमुक्तः ॥]{style="color:#99cc00;"} 61 ंBह्(12.289.58-61)\
The high-souled yogin filled with greatness, at will, can enter into and come out of, ब्रह्मा the lord of all, the boon-giving विष्णु, bhava, dharma, the six-faced कुमार, the sons of ब्रह्मा, tamas that results in trouble, rajas, sattva, the mahat and the pure, primordial प्रकृती, the goddess siddhi -- the wife of वरुण, the all-encompassing energy, courage, the king, the sky with the stars, the universe, celestial snakes, the ancestor-spirits, mountains, all terrible oceans, all rivers, thick forests, serpents, plants, यक्ष bands, the directions gandharva bands, and both males and females.

Thus it is clear the yoga was precisely the practical means of "effecting माया" --- the yogin could literally enter into all possible रूप-s. We also see that it was described as a means of entering प्रकृति, which is (the cause of) माया.

As the great scholar Ganganath Jha remarked साम्ख्य and yoga appear to be two faces of the same दर्शन. The former stands for the सिद्धान्त or the theoretical framework and the latter for the practical means of achieving it. The earliest notable discoveries in terms of the practical means that characterize yoga were made by the Indo-Aryans probably in the late ब्राह्मण period of the Vedic age. One discovery was that there was something unique about the problem of consciousness and that it could be tackled in a special way by transcending the subject-object distinction. The second discovery was that this issue of consciousness could be understood only by some special physiologically alterations connected to the nervous system. We have no clear evidence whether these discoveries were known before or not -- while some of the methods could go back to human antiquity, at least some of the background knowledge probably derived from the medical investigations of the atharvaveda. The second of these discoveries led to the primitive "कुण्डलिनि system" and a precursor of what later came to be known as the खेचरी mudra. The earliest traces to these system in the late ब्राह्मण period are seen in the ideas of the भार्गव प्राचिनयोग्य in the तैत्तिरीय श्रुति and याज्ञवल्क्य in the वाजसनेयि श्रुति. It was further developed in the भृगु स्मृति \[Footnote 2]. A subsequent preliminary synthesis of the different themes under an overarching framework was attempted by पतञ्जलि. However, his system was mainly an attempt to present yoga as an independent दर्शन by incorporating a slightly modified form of सांख्य as its internal सिद्धान्त. The real task of a grand synthesis, which brought together various themes into a comprehensive system of practice was seen in the tantra-s. Here, in addition to the multifaceted development of the traditions of physical practice, the "कुण्डलिनि" and "खेचरी mudra" systems were brought together with the other ancient Hindu tradition of the mantra-शास्त्र. Credit in this regard goes to the great matsyendra, the fisherman-teacher, who wove the different strands into the great tantric synthesis. This was indeed one of the greatest achievements of Hindu thought, where the many and one are seamlessly integrated, something which is much neglected by many modern theoretical students of Hindu thought.

Footnote 1: [https://www.youtube.com/watch?v=7_t-ग्C७३आती](https://www.youtube.com/watch?v=7_t-ग्C७३आती){rel="nofollow"}

Footnote 2: We offer an approximate, partial translation of the भृगु स्मृति, an important document of Hindu thought here: [https://app.box.com/s/5ws9garzbulhxvpeg0ka](https://app.box.com/s/5ws9garzbulhxvpeg0ka){rel="nofollow"}


