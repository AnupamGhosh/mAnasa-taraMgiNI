
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [A mysterious verse of a siddha](https://manasataramgini.wordpress.com/2014/02/18/a-mysterious-verse-of-a-siddha/){rel="bookmark"} {#a-mysterious-verse-of-a-siddha .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 18, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/02/18/a-mysterious-verse-of-a-siddha/ "7:20 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The siddha [तिरुमूलर् or सुन्दर्नाथ](https://manasataramgini.wordpress.com/2006/08/25/sundaranatha-tirumulars-lineage/ "सुन्दरनाथ (तिरुमूलर्)’s lineage") is supposed to have journeyed from Kashmir to the Tamil country to teach a distinctive flavor of the shaiva मन्त्रमार्ग, which is encapsulated in his famous but obscure [tirumantiram](https://manasataramgini.wordpress.com/2006/08/25/the-mythology-of-tirumular/ "The mythology of तिरुमूलर्"). While the tirumantiram is widely known among the practicing ब्राह्मण-s of the Tamil country it is not understood by most of them due to: 1) the संध्या भाषा typical of तान्त्रिक encodings; 2) its allusions to traditions from an expert insider viewpoint; 3) loss of the direct context of the sub-tradition to which तिरुमूलर् belonged: a hybrid of the सैद्धान्तिक and श्रीकुल doctrines \[such a tendency is mentioned, albeit disapprovingly, by भट्ट रामकण्ठ-II the Kashmirian सैद्धान्तिक commentator of the मतङ्गपारमेश्वर tantra].

The paNDita who had taught us elements of vaidika issues brought to our attention one of the few surviving verses of सुन्दरनाथ in संस्कृत. Apparently, it was recorded by the great kaula मन्त्रवादिन् of chidambaram, महेश्वरानन्दनाथ, without any surviving explanation, at the directive of his female teacher who gave him the famous instruction in mahArAShTrI प्राकृत. It displays the same enigmatic nature of some of the Tamil verses in the tirumantiram, which ironically a vaiShNava brought to our father's attention.

The verse goes thus:\
[आनन्द-ताण्डव-पुरे द्रविडस्य गेहे]{style="color:#0000ff;"}\
[चित्रं वसिष्ठ-वनिता-समम् आज्यपात्रम् ।]{style="color:#0000ff;"}\
[विद्युल्-लतेव परि-नृत्यति तत्र दर्वी]{style="color:#0000ff;"}\
[धारां विलोकयति योग-बलेन सिद्धः ॥]{style="color:#0000ff;"}

In the city of the joyous ताण्डव, in the Tamil's home,\
there is a beautiful ghee-cup like वसिष्ठ's lady;\
there a ladle dances around like the trace of lightning;\
the stream \[of ghee from it] the siddha sees with his yoga power.

Certain elements can be interpreted in relatively straight forward way:\
The city of the Ananda-ताण्डव is Chidambaram as is clearly mentioned in the chidambara माहात्म्य. This is also confirmed by the mention of the Tamil's house -- indicating it is indeed referring to the place in the Tamil country. Beyond this obvious part the rest of the verse requires a non-trivial explanation.
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-39BqPYlDf6E/UwMC73mnWxI/AAAAAAAAC24/wMzyoCvx4wk/s800/Ursamajor.jpg){width="75%"}
```{=latex}
\end{center}
```



The beautiful ghee-cup that is likened to वसिष्ठ's lady: why should a woman be likened to ghee cup? We suggest that वसिष्ठ's wife arundhati is the name of Alcor (80 Ursae Majoris) the binary companion of Zeta Ursae Majoris (वसिष्ठ). This association is an old and undisputed one. The Adiparvan of the महाभारत states:

[सुव्रताऽपि हि कल्याणी सर्व-लोक-परिश्रुता ।]{style="color:#0000ff;"}\
[अरुन्धती पर्यशङ्कद् वसिष्ठम् ऋषि-सत्तमम् ॥]{style="color:#0000ff;"}\
Even though arundhati was well-known in the world as being well-mannered and auspicious she had doubted वसिष्ठ, the great ऋषि.

[विशुद्ध-भावम् अत्यन्तं सदा प्रियहिते रतम् ।]{style="color:#0000ff;"}\
[सप्तर्षि-मध्यगं वीरम् अवमेने च तं मुनिम् ॥]{style="color:#0000ff;"}

Though [वसिष्ठ was] of highest purity in conduct and and delighting in pleasing his wife, she insulted him, the muni who stood in the midst of the seven ऋषि-s.

[अपध्यानेन सा तेन धूमअरुण-समप्रभा ।]{style="color:#0000ff;"}\
[लक्ष्यालक्ष्या नाभिरूपा निमित्तम् इव लक्ष्यते ॥]{style="color:#0000ff;"} ("Critical" 1.224.27-29)

Due to her jealousy \[towards वसिष्ठ] her luminosity became low (like smoke obscuring light) \[Footnote 1]: sometimes visible and sometimes not, like and inauspicious omen \[Footnote 2].

In Hindu tradition planets and stars have been called cups (typically soma cups) from which the gods and manes are supposed to drink \[Footnote 3]. Hence, we suspect that वसिष्ठ's lady in this verse is actually the star Alcor. But what is the ladle dancing around and the ghee dripping from it?. A distinct possibility is that it means the rest of Ursa Major (the constellation of the seven seers), which is shaped like a ladle and has been termed so in vaidika tradition (the heavenly chamasa, sruch or darvi). Indeed, such an allusion of Ursa Major as as darvi is found in the atharvaveda:

[अदितेर् हस्तां स्रुचम् एतां द्वितीयां सप्तऋषयो भूतकृतो याम् अकृण्वन् ।]{style="color:#0000ff;"}\
[सा गात्राणि विदुष्य् ओदनस्य दर्विर् वेद्याम् अध्य् एनं चिनोतु ॥]{style="color:#0000ff;"} AV-vulgate 11.1.24

aditi's hand, this second ritual ladle, which the seven seers, the makers of beings, made; may that ladle, knowing the limbs of the rice-offering, gather it on the altar.

Here the darvi (ladle) is described as being made \[up of] by the seven seers, i.e. the 7 stars of the Ursa Major.

In another place the atharvaveda describes the seven seers constituting the ritual ladle (chamasa) that contains within it the glory of all types:

[तिर्यग् बिलश् चमस ऊर्ध्व-बुध्नस् तस्मिन् यशो निहितं विश्वरूपम् ।]{style="color:#0000ff;"}\
[तद् आसत ऋषयः सप्त साकं ये अस्य गोपा महतो बभूवुः ॥]{style="color:#0000ff;"} AV-vulgate 10.8.9

A ritual ladle with slanting opening and bottom-side up, in it is placed glory of all forms; there sit the seven seers all together; these have become the guardians of the great one.

Interestingly, this mantra comes, along with several other such riddle-mantras, in the mysterious riddle or brahmodaya सूक्त of the atharvaveda, which has a tone similar to the siddha's riddle-speech. A version of this again offered by the बृहदारण्यक उपनिषद् of the shukla yajurveda and expanded further offering a clue for what the siddha meant:

[तद् एष श्लोको भवति --]{style="color:#0000ff;"}\
Regarding this there is the following verse:

[अर्वाग्बिलश् चमस ऊर्ध्व-बुध्नस् तस्मिन् यशो निहितं विश्वरूपम् ।]{style="color:#0000ff;"}\
[तस्यासत ऋषयः सप्त तीरे वाग् अष्टमी ब्रह्मणा संविदान॥ इति ।]{style="color:#0000ff;"}

"There is a ritual vessel which has its mouth below and bottom-side up. In it is placed glory of all forms [शंकराचर्य clarifies: "यथा somash chamase" as soma in the chamasa vessel]; on its rim sit the seven seers; and speech, the eight is speech apprehending the brahman"

[अर्वाग्बिलश् चमस ऊर्ध्वबुध्न इति । इदं तच् छिर एष ह्य् अर्वाग्बिलश् चमस ऊर्ध्वबुध्नः ।]{style="color:#0000ff;"}\
"There is a ritual vessel which has its mouth below and bottom-side up": is this head, for it is a vessel which has its mouth below and with bottom-side up.

[तस्मिन् यशो निहितं विश्वरूपम् इति । प्राणा वै यशो विश्वरूपम् । प्राणान् एतद् आह ।]{style="color:#0000ff;"}\
"In it is placed glory of all forms": the life functions (prANa-s) are indeed the manifold glory. Hence, this statement means the life functions.

[तस्यासत ऋषयः सप्त तीर इति ।प्राणा वा ऋषयः । प्राणाण् एतद् आह ।]{style="color:#0000ff;"}

"On its rim sit the seven seers": the seers are indeed the life functions; this statement means the life functions i.e. the 7 prANa-s.

[वाग् अष्टमी ब्रह्मणा संविदानेति । वाग् घ्य् अष्टमी ब्रह्मणा संवित्ते ॥]{style="color:#0000ff;"} BU-काण्व 2.2.3

"the eighth is speech apprehending the brahman": speech indeed is the eight which perceives the brahman.

From the version in the atharvaveda it is clear the external Ursa Major is being meant with the term तिर्यग्बिलः being proper for the constellation. The भृहदारण्यक has made a few adjustments to suit the internalization of the external visualization of the constellation via homologizing with bodily functions. The speech here is the one which results in mantra-s being articulated and mantra-s are from the beginning of Vedic tradition seen as an embodiment of the "brahman power". So speech is seen as being in constant apprehension of the brahman.

Coming back to the siddha's verse we propose that it uses a similar set of metaphors as these Vedic mantra-s. Thus, we sum up the interpretation of the verse:

  -  The siddha first locates himself in the Tamil country in Chidambaram -- here he specifies the location of his साधन.

  -  Next he uses the metaphor of वसिष्ठ's lady (arundhati) to give a clue regarding how his next metaphor must be interpreted.

  -  Then he talks of the ladle and gives a further hint that it dances around and tracing a path like a lighting streak (indicating it is a luminous object).

  -  Finally, he states that the ghee-stream from this ladle is perceived by the siddha by means of his yogic powers. We suggest that as in the उपनिषद् the siddha has internalized the celestial ladle, Ursa Major. This ladle is now in his internal sky that in that in the तान्त्रिक parlance is the चिदाकाश. The stream of ghee pouring from this ladle is consistent with a well-known metaphor of the stream of nectar which the तान्त्रिक drinks during the performance of [खेचरी](https://manasataramgini.wordpress.com/2009/07/02/some-notes-on-khechari-mudra/ "Some notes on खेचरी mudra") or from his head as he meditates of on a deity in the head or the conjunction of the deities in the सहस्रार chakra in the cranial crown (in the kaula traditions). Hence, it appears that as in the उपनिषद्, the internalized ladle is situated in the head and pours the ghee-stream which is equivalent to the अमृत-स्रवण of the yogin.

**A excursus on वसिष्ठ and अरुन्धती:**\
```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-kAGhK-KhErg/UwMDNlV0GmI/AAAAAAAAC3A/6x17EJXpP-Y/s800/Alcor.gif){width="75%"}
```{=latex}
\end{center}
```

\
In addition to the two stars वसिष्ठ and अरुन्धती forming one of widest binaries \[Footnote 4] there is the notorious non-associated star Sidus Ludoviciana which forms a triangle with them. But its magnitude is 7.6 and beyond typical naked eye visibility. However, there have been reports of seeing up to magnitude 8 in the darkest of sites. Perhaps in the darkest premodern skies mag 8 could have been attainable and Sidus Ludoviciana sighted -- a fitting feat for a siddha's eyesight -- the yoga-चक्षु indeed! There is Mizar B the closer optical binary companion of Mizar at magnitude \~4. But then this is too close to resolve with naked eye at 15 arcseconds (human eye resolves \~2 arcminutes). The period of the वसिष्ठ-arundhati binary would be too long to have been observed during human existence to date: currently estimated at approximately 750,000 years. The Mizar A-B system is estimated at approximately few thousands of years but then it should not have been visible to premodern viewers. Yet, mysteriously, the महाभरत makes an allusion, though entirely anachronistic, of arundhati's motion relative to वसिष्ठ:

[या चैषा विश्रुता राजंस् त्रैलोक्ये साधु-संमता ।]{style="color:#0000ff;"}\
[अरुन्धती तयाप्य् एष वसिष्ठः पृष्ठतः कृतः ॥]{style="color:#0000ff;"}

She, O king, who is well-known in the three worlds and respected by the good, even that अरुन्धती has made वसिष्ठ stand to her rear!

Interesting as this is, it might not necessarily imply knowledge of the revolution of arundhati and वसिष्ठ -- alternative explanations could be the change in brightness of arundhati \[Footnote 1] or an ancient memory of a time when due to precession of the earth's axis arundhati might risen before वसिष्ठ. In any case it is anachronistic in the context of the many unconnected omens narrated at the beginning of the great war.\
Footnote 1: We were for long puzzled by this legend and wondered if it meant that at some point in the historical past Alcor underwent a loss of brightness. Interestingly, in 2001 a paper was published Polcaro and Viotti in which they suggested that the Sumerian/Babylonian records suggest that Alcor was probably brighter than what it is now around 2000 BCE, perhaps being as bright as Mizar itself. Their main reason to suggest this is because the Sumerian/Babylonian records place Alcor as a separate constellation apart from the rest of the core Ursa Major, which is termed the constellation of the great wain. So they reason that the Sumerians might have done this only if Alcor was much brighter then. The Hindu legend is, to our knowledge, the most explicit reference to Alcor having become fainter in human memory. Another point of interest is the connection between Alcor and the so called lost Pleiad in multiple traditions. In several traditions there is the legend of one of the 7 Pleiades being lost and the number becoming 6. Of these, in Mongol, Greek and Hindu tradition there is the legend that the lost one was/became Alcor. In Mongol tradition the 7 stars of Ursa Major are considered 7 thieves who kidnapped one of the 7 maidens, the lost Pleiad, and kept her with them, where she is the Cold star, Huitung Ot. This raises the question if this connection is merely a consequence of numerology and shape (Pleiades and Ursa Major) or it the variability of Alcor and the lost Pleiad happened roughly around the same time.

Footnote 2: In Shinto omenlogy too there is an inauspicious omen associated with the non-visibility of Alcor: If a person fails to see Alcor, he is said to die by the end of that year. Given the presence of a portent in both the Indian and Japanese world it is conceivable that there was some omen associated with star going back to prehistoric times.

Footnote 3: Interestingly, the Canadian Miqmak tribe also has a legend which takes Alcor to be a vessel.

Footnote 4: The widest double (more correctly triple) known to date is Fomalhaut the brightest star in Pisces Austrinus. This system, in addition to the widely separated companions which were only recently discovered, also has an exoplanet and a ring around Fomalhaut A. Fomalhaut B is the variable star TW Piscis Austrini which is a BY Draconis variable varying due to large star spots and rotation.


