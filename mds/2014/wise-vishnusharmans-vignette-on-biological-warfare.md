
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Wise विष्णुशर्मन्'s vignette on biological warfare](https://manasataramgini.wordpress.com/2014/04/13/wise-vishnusharmans-vignette-on-biological-warfare/){rel="bookmark"} {#wise-वषणशरमनs-vignette-on-biological-warfare .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[April 13, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/04/13/wise-vishnusharmans-vignette-on-biological-warfare/ "7:22 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[ब्रह्मा रुद्रः कुमारो हरि-वरुण-यमा वह्निर् इन्द्रः कुबेरश्]{style="color:#0000ff;"}\
[चन्द्रादित्यौ सरस्वत्य्-उदधि-युग-नगा वायुर् उर्वी-भुजङ्गाः ।]{style="color:#0000ff;"}\
[सिद्धा नद्यो ऽश्विनौ श्रीर् दितिर् अदिति-सुता मातरश् चण्डिकाद्या]{style="color:#0000ff;"}\
[वेदास् तीर्थानि यक्षा गण-वसु-मुनयः पान्तु नित्यं ग्रहाश् च ॥]{style="color:#0000ff;"}


Tennyson had put it: "Nature, red in tooth and claw" and indeed biological conflict is our biggest teacher. It has lessons so profound that some of the foundations of biology itself emerge from its understanding. And we being a microcosmic part thereof learn much about how to conduct ourselves in our conflicts from the उपाय-s of other life around us. It is perhaps this understanding that made विष्णुशर्मन् and his wise predecessors situate their lessons on human interaction in the natural world that was there for all to see in old India. It is for this reason after invoking the pantheon as above he says:

[इश्वराणाम् इदं तन्त्रं प्रायेणौत्सुक्यम् आवहेत् ।]{style="color:#0000ff;"}\
[यतस् तिरश्चां चरितैर् नीतिमार्गः प्रदर्श्यते ॥]{style="color:#0000ff;"}

This text of the gods might appear puzzling due to its teachings; however, it intends illustrating by actions of animals the path of right conduct/politics. In the midst of the wide wisdom he offers he also gives a brief lesson biological warfare. This comes in the tale of the bull and the lion where the jackals attendants strive to create a conflict between the two. In justifying the case for the lion to abjure his friendship with the bull the jackal damanka warns him of the danger of biological warfare waged by the bull:

[सिंह आह: "स तावच् छष्पभुग् वयं पिशितभुजस् तत् किम् असौ ममापकर्तुं समर्थः ?"]{style="color:#0000ff;"}\
The lion said: After all this guy is a plant-eater; whereas we are a flesh-eater; so how is he capable to do me harm?

[दमनक आह: "एवम् एतत् । स शष्पभुग् देवपादाः पिशितभुजः । सो ऽन्न-भूतो देवपादा भोक्तृ-भूताः । तथा ऽप्य् असौ यदि स्वयम् अनर्थं न करिष्यति ततो ऽन्यस्माद् उत्पादयिष्यति ।"]{style="color:#0000ff;"}\
damanaka said: It is indeed so that he is a plant-eater and your Highness is a flesh-eater; he is food and your Highness is the food-eater. Even if he cannot himself cause harm, however, he will get some else to do it for him.

[सिंह आह: "का शक्तिर् अस्य स्वतो ऽपकर्तुं परतो ऽपकर्तुं वा?"]{style="color:#0000ff;"}\
The lion said: What power does he have to cause me harm by himself or by others?

[सो ऽब्रवीत् (दमनकः): "त्वम् तावद् अजस्रम् अनेक मत्त-गज गवय महिष वराह शार्दूल चित्रक युद्धेषु नख-दन्त-संनिपात-कृत-व्रण-शबल-तनुः । अयं पुनः सदा त्वत् समीप-वासी प्रकीर्ण विण्-मूत्रः । तद् अनुषङ्गाच् च कृमयः संभविष्यन्ति । ते युष्मच्-छरीर-सामीप्यात् क्षत विवरानुसारिणो ऽन्तः प्रवेक्ष्यन्ति । तथा त्वं विनष्ट एव ।"]{style="color:#0000ff;"}\
He said (damanaka): you are incessantly in combat with elephants in musth, Gayals, buffaloes, boars, tigers and cheetahs, during which your body is dotted with wounds caused by the strikes of their claws and fangs. Now again (संजीवक the bull) always stays beside you splattering feces and urine. And by consequence of that parasites will begin breeding. As your body is close by, they will invade it making their way in via the perforations from the wounds. By this means he (the bull) will cause your destruction.

Thus, विष्णुशर्मन्'s account is perhaps one of the earliest textual references of excreta  having a major role in parasite-transmission as part of predator-herbivore interactions. Of course modern studies suggest that parasites transmitted by carnivorans (example the apicomplexan *Sarcocystis*) can infect herbivores and weaken their muscles/other organs, thereby making them easier prey and furthering parasite transmission. It is not clear if herbivoran parasites can have a reciprocal fitness effect on carnivorans. The practice of excreting in latrines (an ancient trait in the mammalian line first recorded in dicynodonts of the Triassic) suggests that mammals try to limit parasite-transmission by localizing excreta. Such latrines of herbivores could in principle allow transmission of certain debilitating parasites to carnivorans. Potential parasites transmitted via ungulate excretion to felids include the apicomplexan *Cryptosporidium* and the worm nematode *Trichuris*. Such transmission could be exacerbated by a behavior of carnivorans, namely rolling in ungulate dung and licking themselves. Another possibility is the nematode worm *Spirocerca*. This worm is transmitted by dung beetles to felids -- such dung beetles could indeed breed in larger numbers in the presence of bovid dung and could be available for transmission of the parasite to felids.


