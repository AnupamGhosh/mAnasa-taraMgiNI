
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The legend of king हयग्रीव](https://manasataramgini.wordpress.com/2014/09/11/the-legend-of-king-hayagriva/){rel="bookmark"} {#the-legend-of-king-हयगरव .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[September 11, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/09/11/the-legend-of-king-hayagriva/ "12:58 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

First published at [ईन्दिअFअcत्स्](http://www.indiafacts.co.in/legend-king-hayagriva/?utm_source=rss&utm_medium=rss&utm_campaign=legend-king-hayagriva)\
After the great war at कुरुक्षेत्र and the conquest of हस्तिनापुर, युधिष्ठिर went through a phase of deep despondency. Given his pacific nature, he was immensely troubled by the violence he had inflicted by killing his kinsmen, relatives, teachers and other great heroes. This came in the way of this ability to rule effectively and he apparently had thoughts of renouncing the world and leading the life of an ascetic. At this juncture he was shored up by some brilliant advice from his brothers and wife. To cap their advice, the sage व्यास कृष्ण-द्वैपायन himself arrived at his court, confirmed the views of द्रौपदी and the other पाण्डव-s, and taught युधिष्ठिर the right path. In course of this teaching, he narrated to him a small, ancient legend of a king known as हयग्रीव (महाभारत "Critical edition" 12.25.22-12.25.33 \[Footnote 1]). Most of the narrative is in the त्रिष्टुभ् meter, an ancient Sanskrit meter with 11 syllables in each of its 4 units. The first two, the introductory ones, are in अनुष्टुभ्, a meter with 8 syllables in each of its 4 units (the most common meter used in both the इतिहास-s).

[अत्र ते राज-शार्दूल वर्तयिष्ये कथाम् इमाम् ।]{style="color:#0000ff;"}\
[यद् वृत्तं पूर्व राजर्षेर् हयग्रीवस्य पार्थिव ॥]{style="color:#0000ff;"}\
I shall narrate to you, tiger among kings, this legend of what happened in the past in regard to the royal sage हयग्रीव.

[शत्रून् हत्वा हतस्य+आजौ शूरस्य+अक्लिष्ट-कर्मणः ।]{style="color:#0000ff;"}\
[असहायस्य धीरस्य निर्जितस्य युधिष्ठिर ॥]{style="color:#0000ff;"}\
\[The is the story of him] O युधिष्ठिर, who was heroic, intelligent, of unfailing acts, who after slaughtering his foes without assistance, was overcome himself and slain in the midst of battle (आजौ= in battle is a distinct epic usage).

[यत् कर्म वै निग्रहे शात्रवाणां; योगश् च+अग्र्यः पालने मानवानाम् ।]{style="color:#0000ff;"}\
[कृत्वा कर्म प्राप्य कीर्तिं सुयुद्धे; वाजिग्रीवो मोदते देवलोके ॥]{style="color:#0000ff;"}\
Indeed, having performed those deeds for the suppression of enemies, adopting the foremost measures for the protection of people, having performed his duties in battle हयग्रीव attained glory, and is rejoicing in the deva-world.

[संत्यक्त+आत्मा समरेष्व् आततायी; शस्त्रैश् छिन्नो दस्युभिर् अर्द्यमानः ।]{style="color:#0000ff;"}\
[अश्वग्रीवः कर्मशीलो महात्मा; संसिद्धात्मा मोदते देवलोके ॥]{style="color:#0000ff;"}\
Having sacrificed his life in the midst of battle with bow drawn, slashed by weapons while attacked by evil-doers, firm in his actions, the great soul, having attained his objectives, is rejoicing in the deva-world.

[धनुर् यूपो रशना ज्या शरः स्रुक्; स्रुवः खड्गो रुधिरं यत्र चाज्यम् ।]{style="color:#0000ff;"}\
[रथो वेदी कामगो युद्धम् अग्निश्; चातुर्होत्रं चतुरो वाजिमुख्याः ॥]{style="color:#0000ff;"}\
\[At this sacrifice] his bow was the sacrificial stake, his bowstring was the rope \[for tying the animal victim], his arrows the offering ladle, his sword the scooping ladle, and the blood \[spilled in it] the ghee. His chariot was the altar, his unrestrained movement in battle the \[ritual] fire, and his four foremost horses his four Vedic ritualists.

[हुत्वा तस्मिन् यज्ञ-वह्नाव् अथारीन्; पापान् मुक्तो राजसिंहस् तरस्वी ।]{style="color:#0000ff;"}\
[प्राणान् हुत्वा चावभृथे रणे स; वाजिग्रीवो मोदते देवलोके ॥]{style="color:#0000ff;"}\
Having offered on that ritual fire his foes as oblations, free from sins that energetic lion among kings, finally offered his own life-breaths as though it was the offering at the bath at the termination of the Vedic ritual; हयग्रीव now rejoices in the deva-world.

[राष्ट्रं रक्षन् बुद्धि-पूर्वं नयेन; संत्यक्तात्मा यज्ञशीलो महात्मा ।]{style="color:#0000ff;"}\
[सर्वांल् लोकान् व्याप्य कीर्त्या मनस्वी; वाजिग्रीवो मोदते देवलोके ॥]{style="color:#0000ff;"}\
Protecting his nation intelligently and with good policy, the great soul, firm in Vedic rituals sacrificed his life. All realms have been encompassed by the fame of this intelligent king हयग्रीव, who rejoices in the deva-world.

[दैवीं सिद्धिं मानुषीं दण्डनीतिं; योग-न्यायैः पालयित्वा महीं च ।]{style="color:#0000ff;"}\
[तस्माद् राजा धर्मशीलो महात्मा; हयग्रीवो मोदते स्वर्गलोके ॥]{style="color:#0000ff;"}\
Accomplished in matters of divinities, human affairs and justice, and having protected his land with with the devices of law, that king, the great soul, firm in dharma, हयग्रीव rejoices in heaven.

[विद्वांस् त्यागी श्रद्दधानः कृइतज्ञस्; त्यक्त्वा लोकं मानुषं कर्म कृत्वा ।]{style="color:#0000ff;"}\
[मेधाविनां विदुषां संमतानां; तनु-त्यजां लोकम् आक्रम्य राजा ॥]{style="color:#0000ff;"}\
Learned, renunciatory, with good conviction and grateful, having performed his duties, he abandoned this world of men and attained that region reserved for the intelligent, scholarly and esteemed upon leaving his body.

[सम्यग् वेदान् प्राप्य शास्त्राण्य् अधीत्य; सम्यग् राष्ट्रं पालयित्वा महात्मा ।]{style="color:#0000ff;"}\
[चातुर्वर्ण्यं स्थापयित्वा स्वधर्मे; वाजिग्रीवो मोदते देवलोके ॥]{style="color:#0000ff;"}\
Having obtained all the veda-s and having studied the शास्त्र-s, having protected his entire nation, having installed the four वर्ण-s in their respective duties, the great soul, हयग्रीव, rejoices in the deva-world.

[जित्वा संग्रामान् पालयित्वा प्रजाश् च; सोमं पीत्वा तर्पयित्वा द्विजाग्र्यान् ।]{style="color:#0000ff;"}\
[युक्त्या दण्डं धारयित्वा प्रजानां; युद्धे क्षीणो मोदते देवलोके ॥]{style="color:#0000ff;"}\
Having won battles and having protected his people, having drunk soma having pleased the ब्राह्मण-s, and having held the rod of justice with appropriate measures for this people, slain in battle, he rejoices in the deva-world.

[वृत्तं यस्य श्लाघनीयं मनुष्याः; सन्तो विद्वांसश् चार्हयन्त्य् अर्हणीयाः ।]{style="color:#0000ff;"}\
[स्वर्गं जित्वा वीरलोकांश् च गत्वा; सिद्धिं प्राप्तः पुण्यकीर्तिर् महात्मा ॥]{style="color:#0000ff;"}\
His conduct was laudable, learned and good men deservedly praise his worthy behavior. Having won heaven, and having gone to the realm of heroes, the great soul of pure fame attained perfect success.

**Commentary**\
This short narrative, at the face of it, might seem unremarkable in body of the great epic bristling with all kinds of interesting legends. However, it embodies within it a succinct collection of notable ideals held by the ancient Hindus. The persistent motif in it is the ascent of king हयग्रीव to the deva-world upon his death in battle. This motif is an ancient one which is seen in other sister cultures of the Indo-European world: It is rather pronounced in the Germanic world where the slain warriors go to rejoice in the divine realm of Valhalla. In the Greek world there is a memory of a realm called Elysium, which was attained by great heroes upon their death. While this theme is made obvious by the phrase modate devaloke (rejoices in the deva-world), it is also clear that व्यास uses it as didactic platform. He wishes to instruct युधिष्ठिर regarding the duties of an ideal ruler, including when he needs to commit violence and sacrifice his own life if needed.

The key points that are emphasized by the narrative are:

  -  It is necessary for a king to commit violence when he is confronted by evil-doers (dasyu-s) and slay them in battle. In particular, if this is committed to protect the nation (राष्ट्र) and his people (प्रजा) then it is meritorious deed.

  -  Death in such a battle is implied to be the gateway to the realm of the deva-s, where the ruler, who has died thus, is said to rejoice. The narrative also implies that the king should make fighting such a battle, even if outnumbered, his primary duty.

  -  Indeed, the performance of this act is hence equated to a Vedic ritual. In generating this equivalence the narrative establishes many संबन्ध-s(connections) between the Vedic ritual and the battle fought against the evil-doers. Specifically, it is compared to the Vedic animal sacrifice (e.g. नीरूढ-पशुबन्ध), along with the use of technical terms from ritual literature: the sacrificial stake (यूप), the rope for tying the animal victim (रशन), the offering ladle (sruk), the scooping ladle (sruva), ghee offered in the fire, the altar (vedi), the ritual fire and four Vedic ritual specialists, namely the होतृ belonging to the ऋग्वेद, the adhvaryu belonging to the yajurveda, the उद्गातृ belonging to the सामवेद and the brahman usually assigned to the atharvaveda. The final sacrifice of the king in such a battle is equated to the bath which takes places at the end of the Vedic ritual (the अवभृथ). In Hindu tradition the performance of a ritual (karman) is said to yield a result (karmaphala), which, among other things, might be a place in the deva-realm. By equating the battle for the protection of the nation and people to a Vedic ritual the narrative offers a justification of the fruit attained by the king via his sacrifice. Through this equivalence व्यास brings home to युधिष्ठिर that such a battle is not a sinful act but a meritorious and holy act, just like the cognate Vedic ritual.

  -  The narrative also touches upon the duties of such a king with respect to his state. He is described as being intelligent and cultivating scholasticism through the study of texts and patronage to ब्राह्मण-s, who were generators of knowledge. He is described as being diligent in applying law to bring justice among his people (दण्ड or the rod of chastisement) and upholding the stability of society. He is said to apply his intelligence and sound policy for the defense of his nation (राष्ट्रं रक्षन् buddhi-पूर्वं nayena). He is also particular about the performance of Vedic rituals, such as the great soma rituals. Thus, व्यास within a short narrative conveys to युधिष्ठिर the essentials of the Hindu ideal of a ruler. He emphasizes that the upholder of dharma, who commits violence or perishes in the defense of his nation and people, is performing a holy act. Hence, there one should not be afflicted by doubt or grief over such actions.

When we look at pre-modern India, we find several rulers who lived up to this ideal, but one name which fits the bill in its entirety is the great परमार ruler bhojadeva. He lived the life of a scholar, stood as a wall against the Islamic invasions of the Ghaznavids in the 1020s and again in 1043 CE, finally dying in battle defending his kingdom. This ideal widely permeated the collective क्षत्रिय mind and was kept alive by the overtures of the ब्राह्मण-s. A good example of this seen in course of the invasion of Northwestern India by Macedonian Alexander. He met some of the fiercest resistance in his entire career from the क्षत्रिय-s of those regions because they were inspired by the ideal of dying in the defense of their nation by ब्राह्मण-s \[Footnote 2]. Alexander then targeted the ब्राह्मण-s who also sacrificed themselves following a similar ideal \[Footnote 3]. When Alexander was about to hang a ब्राह्मण he asked him what had made him instigate the क्षत्रिय-s to attack the Macedonians. The ब्राह्मण bluntly replied: "I wish to live with honor or die with honor". In essence, he was going by the ideal taught by the example of the ancient king हयग्रीव. The consequence of these sacrifices was that the Macedonian forces were completely eliminated in Northwestern India within 10 years of Alexander's retreat from India. In the medieval period it was the same ideal, which inspired the local warriors of whom the only memories are वीर-kal-s dotting peninsular India. Again, it was the same ideal which inspired the medieval and early modern Hindu warriors in the struggle against their mortal enemies in the form of the Mohammedan and Christian invaders.

In conclusion, one may note in passing that the Hindu self-sacrifice in a dharma-yuddha is a very distinct concept from that of martyrdom shared by Abrahamistic religions. Whereas the former is for the protection of dharma, the latter is for the imposition of a delusion that has come upon a mentally diseased "prophet" on an otherwise normal population.

Footnotes\
Footnote 1: The Critical Edition or the Pune edition of the महाभारत is not a reconstruction of the ancestral text via textual criticism in the real sense. Rather, it is more of a strict consensus edition.

Footnote 2: For a complete narrative of these events one might refer to the book "Alexander of Macedon, 356-323 B.C.: A Historical Biography" by Peter Green; page 425.

Footnote 3: Just prior to this narrative in the महाभारत, arjuna the third पाण्डव provides a teaching wherein he clarifies that though it is not the primary duty of a ब्राह्मण, when the need arises he too needs to engage actively in combat to slay evil-doers and, if need be, die in the process.


