
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The erotic, the warlike and the imperial in the tantra age](https://manasataramgini.wordpress.com/2014/02/14/the-erotic-the-warlike-and-the-imperial-in-the-tantra-age/){rel="bookmark"} {#the-erotic-the-warlike-and-the-imperial-in-the-tantra-age .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 14, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/02/14/the-erotic-the-warlike-and-the-imperial-in-the-tantra-age/ "7:28 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The primary पौराणिक text of the श्री-kula tradition is the famed ललितोपाख्यानम्, which is tagged to the ब्रह्माण्ड पुराणम्. The text describes the exploits of the central deity of the श्री-kula system, त्रिपुरसुन्दरी, who is simultaneously praised as simultaneously erotic:\
[कामेश-ज्ञात-सौभाग्य-मार्दवोरु-द्वयान्विता]{style="color:#0000ff;"} -- she with a pair of graceful, smooth thighs which are enjoyed by कामेश (her bhairava consort).

[कामेश्वर-प्रेम-रत्न-मणि-प्रतिपण-स्तनी]{style="color:#0000ff;"} -- she who paid back the love of कामेश by giving her breasts which are like jewel \[pots]

Warlike:\
[भण्डासुर-वधोद्युक्त-शक्तिसेना-समन्विता]{style="color:#0000ff;"} -- she who is surrounded by an army of goddesses ready to slay भण्डासुर

[भण्डासुरेन्द्र-निर्मुक्त-शस्त्र-प्रत्यस्त्र-वर्षिणी]{style="color:#0000ff;"}-- she who showers missiles to counter the weapons hurled by भण्डासुर

[कामेश्वरास्त्र-निर्दग्ध-सभण्डासुर-शून्यका]{style="color:#0000ff;"} -- she who annihilated भण्डासुर by burning him with the कामेश्वर missile

And imperial:\
[श्रीमहाराज्ञी]{style="color:#0000ff;"} -- the empress\
[श्रीमत्-सिंहासनेश्वरी]{style="color:#0000ff;"} -- the goddess seated on the imperial throne\
[राज-राजेश्वरी]{style="color:#0000ff;"} -- the imperial goddess

This triad of metaphors, while reaching a literary culmination in the ललितोपाख्यानम्, is a theme which is present in some form in other kaula traditions such as those of the पूर्वांनाय (the trika goddess and bhairava-s), उत्तरांनाय (काली), पस्चिमांनाय (कुब्जिका and नवात्मन्). The kaula tradition appears to have gained considerable prominence with the teachings of fisherman-teacher matsyendra and his successors. He is named as the primary teacher or नाथ of this yuga (mentioned by his प्राकृतिc name as मcछन्द vibhu (नाथ) by the great Kashmirian तान्त्रिक abhinavagupta) and is said to have authored the kaula-ज्ञान-निर्णय, a definitive योगिनी-kaula work. The kula tradition also influenced the emerging गाणपत्य sect, giving rise to a kaula version of it, whose founder is said to have been heramba-suta. A version of this tradition transplanted to China and Japan acquired considerable popularity there and survives in the latter country to date, even if in a diluted form. Eventually, within the core Astika tradition, we find streams of स्मार्त-s attributing the origin of the kula system, in a somewhat "domesticated" form, to the advaita teacher शंकराचर्य. Thus, a notable text of this tradition, the saundaryalahari, which presents the कुब्जिका and त्रिपुरा kula systems, is attributed to शंकराचार्य. Reflexes of these expressions also thoroughly permeate the bauddha yoga and योगिनी tantra-s. Versions such as the bauddha [साधना of the विलासिनी-s](https://manasataramgini.wordpress.com/2008/05/25/upasana-of-the-1112-vilasini-s/ "upAsana of the 11/12 विलासिनी-s") and padma-नृत्येश्वर closely imitate the श्रीकुल tradition. [The bauddha तान्त्रिक tradition ultimately culminated in their grand synthesis, which again parallels the shaiva kaula tradition, in the form of the कालचक्र-tantra](https://manasataramgini.wordpress.com/2009/02/16/nastika-notes-2/ "nAstika notes-2"), which teaches the system centered on deities, the 4-headed, 24-armed कालचक्र and his 4-headed and 8-armed consort विश्वमाता. Such lateral influences emanating from the core shaiva kula tradition are also seen in the jaina bhairava tantra-s. Thus, it is clear that the kula "movement" was a pervasive one, which impacted not only the source religion but also it paratypes both in the sub-continent of जंबुद्वीप and the greater Indosphere in Tibet and the east.

If we look closely, this triad of metaphors is also seen in the language of royal discourse as what might be termed the ["tantra age"](https://manasataramgini.wordpress.com/2009/04/02/geopolitics-of-the-tantra-age-an-attempt-at-a-blunt-assessment/ "Geopolitics of the tantra age: an attempt at a blunt assessment") unfolded in India. As an example let us consider the royal praise supposed to have been composed by the noted lexicographer amara-सिंह to praise the great monarch विक्रमादित्य chandragupta-II:

[अभयम् अभयं देव ब्रूमस् तवासि-लता-वधूः कुवलय-दल-श्यामा शत्रोर् उरः स्थल-शायिनी ।]{style="color:#0000ff;"}\
[समय-सुलभां कीर्तिं भव्याम् असूत सुताम् असाव् अपि रमयितुं रागअन्धेव भ्रमत्य् अखिलं जगत् ॥]{style="color:#0000ff;"}

abhayam= guarantee of protection (in this context); deva= lord; ब्रूमः= we say; tava=your; asi= sword; lata=creeper, here a flammard's blade (i.e. like the kris which survives in the Malay archipelago); वधुः= bride; kuvalaya=blue water-lily; dala= petal; श्यामा= dark; शत्रोः= enemy (genitive); उरः= chest; sthala= place; शायिनी= rested upon.

samaya= in due course; सुलभां= effectively/easily; कीर्तिं= fame; भव्याम्= beautiful; asuta= conceived; सुतां= daughter; asau=who; api= now; ramayitum= to cause pleasure; रागान्धा= passionate woman; iva= like; bhramati= wanders; अखिलं= entire; jagat= world.

Give us the guarantee our protection, O lord, when we say that your flammard-blade-bride, dark as the petal of a blue water-lily \[Footnote 1], has laid herself on your enemy's chest; there, she in due course easily conceived a beautiful daughter कीर्ति (fame) who now roams about the entire world like a passionate woman to bring pleasure to all!

This trend is also apparent in the royal inscriptions from the "tantra age". For example, we may consider the famous inscription of  the jaina poet रविकीर्ति describing the exploits of the चालुक्यn emperors:\
[नाना हेति शताभिघात-पतित-भ्राताश्व-पत्ति-द्वीपे नृत्याद् भीम-कबन्ध-खड्ग-किरण-ज्वाला-सहस्रे रणे ।]{style="color:#0000ff;"}\
[लक्श्मीर्-भावित-चापलापि च कृत शौर्येण येन्[अ]आत्मसाद् राजा[आ]सीज् जयसिंह-वल्लभ इति ख्यातश् चालुक्यान्वयः ॥]{style="color:#0000ff;"}

There was, of the चालुक्य dynasty, the king named जयसिंह-vallabha, who in battle -- where horses, infantry and elephants, bewildered, fell down under the strokes of many hundreds of weapons, and where thousands of frightful headless trunks and of flashes of rays of swords were leaping to and fro -- by his valor made fortune his own \[woman], even though she is suspected of fickleness \[Translation adapted from Epigraphia Indica Vol. 6].

Or this verse with a simultaneous heroic and erotic cadence of describing the conquest of the emperor मङ्गलेश:\
[स्फुरन्-मयूखैर्-असि-दिपिका-शतैर् व्युदस्य मातङ्ग-तमिस्र-सञ्चयम् ।]{style="color:#0000ff;"}\
[अवाप्तवान्यो रण-रङ्ग-मन्दिरे कटच्चुरि-श्री-ललना परिग्रहम् ॥]{style="color:#0000ff;"}

Who in that fortress, which was the drama-theater of the battle-field, took in marriage the damsel,\
the fortune of the कटच्चुरिस् \[form of the haihaya clan kalachuri-s of later history], having scattered the gathering gloom, i.e. the array of elephants (of the adversary), with hundreds of bright-rayed lamps, i.e. the swords (of his army) \[Translation adapted from Epigraphia Indica Vol. 6].

It is not as if the sexual element was not associated with the royal in earlier Hindu tradition. Indeed, this is an inheritance from the Indo-European past of the Hindus. It was expressed in their early tradition within the grand culmination of all shrauta rites, the ashvamedha, performed by the supreme monarch upon subjugation of all rival kings. Here, [the sacrifice of the horse is followed by the central sexual ritual](https://manasataramgini.wordpress.com/2004/11/21/the-sacrifice-of-the-horse-in-the-ashvamedha/ "The sacrifice of the Horse in the ashvamedha") along with the [sexual banter between the ब्राह्मण ritualists and the royal ladies](https://manasataramgini.wordpress.com/2004/11/21/the-obscene-formulae-in-the-ashvamedha/ "The obscene formulae in the ashvamedha"). However, we see this as being primarily a fertility ritual -- as fecundity of the people and other animals directly translated into armies and power of the राष्ट्र -- a fact made clear in the yajus and atharvan incantations used in the ashvamedha. This is paralleled in the Roman and Celtic versions of the horse sacrifice \[the only account of the Germanic one indicates a purely fertility ritual with no royal dimension], suggesting that the link goes pretty deep in the Indo-European past. However, we believe that the expressions we see in the "tantra age" are somewhat distinct -- they embody what is termed the शृङ्गार rasa (eroticism) in संस्कृत aesthetics rather than a primarily fertility element (though शृङ्गार could merely represent an aesthetic metaphor for the latter).

[The explicit emphasis on rasa-s was first seen the Hindu theater as described in its earliest surviving text, the नाट्यशास्त्र of bharata](https://manasataramgini.wordpress.com/2011/06/12/why-rasa-where-did-arya-and-yavana-nataka-diverge/ "Why rasa? where did Arya and yavana nATaka diverge?"). By contrast with the poetry of the इतिहास-s, we can see that this form of poetic expression was probably originally exclusively used in the theater as opposed to the royal courts. From here it appears to have merged with and then taken over the conventions of court poetry, which were originally represented by the इतिहास-s. Indeed the exalted ashvamedha ritual involved the participation of two kinds of performers: The सूत who composed and recited इतिहास -- the recitation of which is explicitly mentioned as being done on the pariplava nights in the ashvamedha rite. The rite also involved sessions where शैलूष-s (actors) and raibhin-s/नाराशंसिन्-s (associated poets) put up their performances. Thus, such old royal rituals themselves might have provided the setting for the beginnings of the hybridization of different poetic conventions that then crystallized as the classical संस्कृत काव्य. With the expression of rasa and the associated theory of dhvani becoming the mainstay of काव्य, the old fertility element in royal power might have been re-configured s the expression शृङ्गार rasa.

Given the parallel expression of these metaphors in both royal discourse and kula tantra-s, we suggest that the two were intimately linked. However, we posit that the above motifs in classical काव्य were not a secondary consequence of this linkage, rather काव्य was itself the initial catalyst for it. Under this scenario we posit that the emergence of classical काव्य as the main mode of poetic expression in the royal court \[Footnote 2] brought with it the need to emphasize the rasa-s even as in the theater. Simultaneously, the reworkings of the पुराण-s into neo-पुराण-s allowed this form of expression to also enter what was once the exclusive domain of the religious सूत poetry (originally similar to the इतिहास poetry under the category इतिहास-पुराण). Thus, we see expressions in an extant shiva पुराण (Venkateshvara Steam Press edition) that are paralleled in the कुमारसंभव of कालिदास. These developments were also paralleled in space and time by the kaula reconfiguration of the sexual ritual of the older shaiva tantra. In those older tantra-s like the सिद्धयोगेश्वरी-mata and pichu-mata or brahma-यामल (which teaches the system of the deities कपालीश bhairava and his consort रक्तचामुण्डा) the sexual ritual is primarily for production of substance (कुण्डगोल), which also used as offering to the deities. In the kaula reconfiguration the ritual was not just for the old purpose but also for the act of pleasing the deities residing in the various parts of the body via the highest pleasurable experience, namely ratisukham. Indeed, it is accompanied by other pleasurable experiences (bhoga) such as perfumes (gandha/lepa), food (bhojana) and drinks (pAnaka), which similarly please the deities of the various organs. Now the aesthetic theory of classical काव्य fitted well into this kaula paradigm because काव्य also produces what was termed originally in the theatrical theory as the [स्थायिभाव](https://manasataramgini.wordpress.com/2011/06/12/why-rasa-where-did-arya-and-yavana-nataka-diverge/ "Why rasa? where did Arya and yavana nATaka diverge?"). The experience of स्थायिभाव-s was an ultimately pleasurable experience -- the reason for enjoyment of poetry or theater. Thus, काव्य too could be used to please the deities of the organs, with the शृङ्गारं, वीरं and अद्भुतं being the most pleasurable and deeply moving (thus most pleasing to the deities) of the rasa-s. Thus, it appears that the aesthetics of काव्य was incorporated as a part of the kaula tradition. We see this plainly manifested in the saundaryalahari or the commentaries of abhinavagupta on the नाट्यशास्त्र and the lochana on ध्वन्यालोक of Anandavardhana.

In the subsequent phase, [paralleling the rise of the saiddhantika shaiva आचार्य-s as major teachers of most major royal courts of bhArata and the greater Indosphere there was also an epigraphically less visible network of kaula and other bhairava-srotas teachers and their bauddha imitators](https://manasataramgini.wordpress.com/2007/02/15/the-greatest-temple-of-shiva/ "The greatest temple of shiva"). They were reinforced by charismatic siddha-s, successors of the original kaula tradition of matsyendra. We posit that the increasing influence of these in royal discourse resulted in a strong juxtaposition of the triad of motifs, the erotic, the warlike and the imperial. Of course this was also in part adopted by the other shaiva and vaiShNava तान्त्रिक teachers resulting in further amplification. For example, the polymath सैद्धान्तिक from the Tamil country, [aghorashiva deshika](https://manasataramgini.wordpress.com/2010/09/03/a-brief-hagiography-of-aghorashiva-deshika/ "A brief hagiography of aghorashiva deshika"), was among other things also a noted exponent of काव्य and the theater.

We conclude with a praise by a forgotten poet of king bhojadeva परमार who lay towards the end of this period and was often compared to emperor विक्रमादित्य who lay closer to its beginnings:

[स्वस्ति क्षीराब्धि-मध्यान् निज-दयित-भुजाभ्यन्तर-स्थाब्ज-हस्ता क्ष्मायाम्-अक्षाम-कीर्तिं कुशलयति महाभू-भुजं भोजदेवम् ।]{style="color:#0000ff;"}\
[क्षेमं मे ऽन्यद् युगान्तावधि तपतु भवान् यद् यशो घोषणाभिर् देवो निद्रा-दरिद्रः सफलयति हरिर् यौवन-र्द्धिं ममेति ॥]{style="color:#0000ff;"}

svasti= blessing; क्षीराब्धि= milk ocean; मध्यान्= midst; nija-dayita= own husband; भुजाभ्यन्तर-stha= enclosed within the embrace of the arms; abja-hastA= lotus hands; क्ष्मायाम्= on the earth; अक्षाम-कीर्तिं= undamaged fame; महाभू-भुजं= emperor; bhojadevam.

क्शेमं= conferring security; me= my; anyad= moreover; युगान्तावधि= period encompassed by the ends of yuga-s; tapatu= blaze (third person imperative); भवान्=you; yad= who; यशः= glory; घोषणाभिर्= by the loud announcements; देवः= god; निद्रा= sleep; दरिद्रः= deprive; saphalayati= fulfills; हरिः= विष्णु; yauvana= youthful; ऋद्धिं= endowments; mama= my ; iti= thus\
"Blessings! by my conferring of security may you blaze forth for the whole period between two yuga-endings". Thus, in the midst of the ocean of milk, enclosed in the embrace of her own husband's arms the lotus-handed [लक्ष्मी] blesses emperor bhojadeva, whose fame is undamaged on the earth; the god hari, deprived of his sleep by the loud announcements of whose (bhojadeva's) glory, fulfills the promise of my [लक्ष्मी's] youthful endowments.

Thus, herein the erotic fulfillment of लक्ष्मी's endowments by विष्णु is linked to the glory of the king bhojadeva -- the same metaphor now lie in a vaiShNava context.

Footnote 1: The dark colored water-lily sword-blade appears to be prevalent metaphor from the "tantra age" of India. For example we encounter it in the Bhagalpur inscription of नारायणपाल, the king of Bengal, in punning verse:\
[भयाद् अरातिभिर् यस्य रण-मूर्धनि विस्फुरन्]{style="color:#0000ff;"}\
[असिर् इन्दीवर-श्यामो ददृशे पीत-लोहितः ॥]{style="color:#0000ff;"}\
From fear of it, while it was flashing on the battle-front, his dark water-lily sword-blade looked as though it was yellow-red-hot \[pun: as though it had drunk blood] to his enemies.

Footnote 2: We may note that Tamil anthologies pura-नानूरु and aka-नानूरु, most of whose poems were in place by the first 400 years of the common era, celebrate the vIra and the शृङ्गार rasa respectively. These poems appear to have been mostly composed under the patronage of the Tamil warlords and kings. Thus, by this period the influence of काव्य was spreading beyond the Indo-Aryan languages and inspiring parallel expressions even in Tamil. This suggests that it was a pan-India phenomenon by that time.


