
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [Some comments of on the vyapohana stava and its pantheon](https://manasataramgini.wordpress.com/2014/01/02/comments-of-on-the-vyapohana-stava/){rel="bookmark"} {#some-comments-of-on-the-vyapohana-stava-and-its-pantheon .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 2, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/01/02/comments-of-on-the-vyapohana-stava/ "9:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

The vyapohana stava is an important सैद्धान्तिक purificatory incantation. It may be compared with other works composed by deshika-s of the Urdhvasrotas such as: 1) the व्योमवापिस्तव of the Kashmirian commentator भट्ट रामकाण्ठ-II; 2) the shiva-पूजा-stava of ज्ञानशम्भु, a scholar from the Tamil country who worked in Kashi; 3) the पञ्चावर्ण-stava by the illustrious aghorashiva of the Tamil country; 4) the ananta-vijaya-मण्डल-stava of rAjA bhojadeva परमार. While those stava-s are of clearly human composition, the vyapohana presents itself as a scriptural text of divine origin only retold by human agents.

सूत उवाच\
vyapohana स्तवं वक्ष्ये sarva-siddhi-प्रदं shubham\
nandinash cha मुखाच्-छ्रुत्वा कुमारेण महात्मना || 1\
व्यासाय कथितं तस्माद् बहुमानेन vai mayA

The सूत said: I will now narrate the auspicious vyapohana-stava which confers all success. And having heard it from the mouth of nandin it was expounded by the great कुमार to व्यास. From him indeed it was respectfully received by me \[the सूत].

\[The सूत here is transmitting information from व्यास as is typical of पुराण-s. However, as the post-vaidika shaiva tradition was expanding it gradually began absorbing the कौमार tradition within it in large part in a subordinate position (However, in south India, especially the Tamil country, the कौमार tradition while colored by the सैद्धान्तिक shaiva tradition still retained much independence). In course of the absorption of the कौमार-mata कुमार was often placed as a intermediary who transmitted the shaiva शास्त्र-s. In the early skanda-पुराण, which is primarily a shaiva text, shiva mostly does not act by himself, rather using his agents to act on his behalf. In this कुमार is an agent of rudra who acts repeatedly on his behalf. The other notable agent of shiva is his famous गण nandin who is also often shown acting on his behalf. Importantly, he is also presented as a major transmitter of shaiva शास्त्र-s. Thus, कुमार and nandin, the major transmitters of shaiva lore, are here placed before व्यास and the सूत stressing the point that this inheres from the core shaiva tradition.

सदाशिवः:\
नमः शिवाय शुद्धाय निर्मलाय yashasvine |\
दुष्टान्तकाय शर्वाय भवाय परमात्मने ||\
पञ्च-vaktro dasha-bhujo hy अक्ष-पञ्च-dashair-युतः |\
shuddha-स्फटिक-संकाशः सर्वाभरण-भूषितः ||\
सर्वज्ञः सर्वगः शान्तः sarvopari सुसंस्थितः |\
पद्मासनस्थः सोमेशः पापम् Ashu vyapohatu ||

\[Like several सैद्धान्तिक shaiva आवरण stava-s, this one too begins with the worship of the innermost मण्डल and proceeds outwards. The innermost deity here is सदाशिव with 5 heads, 10 arms and 15 eyes. As is typical of the सैद्धान्तिक tradition, he is seated in padmAsana on top of all. A superb representation of such a सदाशिव from the Vanga country, as expounded in the सैद्धान्तिक tantra-s, might be seen in the University Museum of U Penn. Despite the progressive establishment of many levels of rudra ectypes in shaiva tradition, passing through the early पाशुपत-s, lakula-s, कालामुख-s and finally to the सैद्धान्तिक-s, his organic link to the old vaidika rudra remains clear by the use of the ancient epithets bhava and sharva. Indeed, this is made clear by the deshika-s, ज्ञानशम्भु and the great-great grandson of trilochanashiva, where they term the preferred form of सदाशिव worshiped by them as रुद्रसदाशिव. Tradition informs us that in this form he is visualized as holding the following in the ten hands: 1) त्रिशूल (trident); 2) खत्वाङ्ग (skull-topped brand); 3) shakti (spear); 4) varada-mudra; 5) abhayamudra; 6) इन्दीवर (lotus); 7) ahi (snake); 8) Damaru (dumbbell drum); 9) pomegranate; 10) अक्षसूत्र (rosary). His praise here begins with the ancient पञ्चाक्षरी mantra, which also has its beginnings in the शतरुद्रीय of the yajurveda. The worship of each deity in this stava concludes with the verb vyapoh in imperative mood applied to pApa, meaning remove the sins of the worshiper]

पञ्च-मूर्तयः:\
ईशानः पुरुषश्-chaiva अघोरः sadya eva cha |\
वामदेवश् cha भगवान् पापम् Ashu vyapohatu ||

\[The पञ्चब्रह्म-s or the five face mantra-s, as a group, come from the early layer of the shaiva tradition recorded in the late vaidika period and used by shaiva-s ever since. They are central to the पाशुपत tradition practice and the five sections of the पाशुपत sUtra-s have been identified with these mantra-s. They stand for the faces of सदाशिव or the rudra repesenting each of the five faces. The सैद्धान्तिक tradition holds that their tantra-s were narrated by the upward face or the ईशान face, hence their appellation Urdhavasrotas. Keeping with this, we have the list here begin with ईशान and then go around east-south-west-north. This is an interesting departure from the common ब्रह्मभङ्गि sequence of the सैद्धान्तिक texts (e.g पराख्य tantra) of इशान-tatpuruSha-aghora-वामदेव-सद्योजात or its reverse seen in the veda and tantra-s like किरण and मृगेन्द्र. In the सैद्धान्तिक tradition these five faces are shown as ईशान (quartz ), सद्योजात (conch-white), वाम्देव (red), aghora (black), and tatpuruSha (yellow). If they are visualized as faces of सदाशिव then that of ईशान is shown as a boy, that of tatpuruSha as a young man, that of aghora as a fierce one with a prominent mustache, that of वामदेव as a seductive woman, and that of सद्योजात as a great king. If they are visualized as separate rudra-s then several distinct prescriptions are given: In the मतङ्गपारमेश्वर tantra they are visualized with 4 heads and 4 arms during mantra-practice. If idols are made then they have 4 heads and 8 arms with gems embedded in them. In the Chera country, the deshika ravi in his प्रयोगमञ्जरी recommends visualizing the 4 directional rudra-s thus -- सद्योजात 4 heads, 4 arms; वामदेव as a goddess with 4 heads and 4 hands; aghora with 4 heads with fangs and 8 arms; tatpuruSha as yogin with 1 head and 4 arms; Ishana just like सदाशिव with 5 heads and 10 arms. In the bhairava-srotas (स्वच्छन्द tantra) they are all visualized with 5 heads and 10 arms around स्वच्छन्द-bhairava with 5 heads and 18 hands. The manual from the Kashmira country by क्षेमराज, student of abhinavagupta states that these rudra-s should be depicted as holding varada, abhaya, a sword, shield, a lasso, a hook, arrows, a पिनाक bow,a severed head and the खट्वाङ्ग.]

विद्येश्वराः:\
अनन्तः sarva-विद्येशः सर्वज्ञः सर्वदः प्रभुः |\
shiva-ध्यानैक-सम्पन्नः sa me पापं vyapohatu ||\
सूक्ष्मः सुरासुरेशानो vishvesho गण-पूजितः |\
shiva-ध्यानैक-सम्पन्नः sa me पापं vyapohatu ||\
shivottamo महापूज्यः shiva-dhyAna-परायणः |\
सर्वगः सर्वदः शान्तः sa me पापं vyapohatu ||\
एकाक्षो भगवान् ईशः शिवार्चन-परायणः |\
shiva-ध्यानैक-सम्पन्नः sa me पापं vyapohatu ||\
\[ekarudro महातेजो sarva-भूत-सुपूजितः |\
shiva-ध्यानैक-सम्पन्नः sa me पापं vyapohatu ||]\
त्रिमूर्तिर् भगवान् ईशः shiva-bhakti-प्रबोधकः |\
shiva-ध्यानैक-सम्पन्नः sa me पापं vyapohatu ||\
श्रीकण्ठः श्रीपतिः श्रीमाञ् shiva-dhyAna-रतः सदा |\
शिवार्चन-रतः श्रीमान् sa me पापं vyapohatu ||\
[शिखण्डी शूल-दृक् धीमान् शक्तीशो गणपूजितः |\
शिवार्चनरतः श्रीमान् sa me पापं vyapohatu ||]

\[The vidyeshvara-s are rudra-s who represent the apotheosis of the shaiva mantra-s. In a sense this directly represents a key feature of the तान्त्रिक mantra-मार्ग inherited from a stream of the मीमांस tradition where there and no extra-natural deities. Rather, the deities are the mantra-s themselves. Hence, these विद्या-s have been deified as 8 distinct deities, the vidyeshvara-s, who are depicted thus: ananta (golden); सूक्ष्म (fiery); shivottama (black); ekanetra (bee-colored); ekarudra (white); त्रिमूर्ति (grey); श्रीकण्ठ (red); शिखण्डिन् (light pink). In this system they occupy the circuit of the मण्डल around the पञ्चब्रह्म-s in the 8 cardinal directions. The vidyeshvara-s are depicted as 2 armed, 8 armed or 10 armed. The सैद्धान्तिक manual from Nepal प्रतिष्ठ-लक्षण-sAra-samuchchaya states that the 8 armed vidyesha-s are shown with 4 heads and if two armed they grip a spear and a trident in the crooks of their elbows. The commonly preserved readings appear to drop the names in brackets of ekarudra and शिखण्डिन् probably because of the unfamiliarity of the later पौराणिक transmitters with the intricacies of सैद्धान्तिक tradition.]

महादेवी:

trailokya-नमिता देवी सोल्काकारा पुरातनी || 13\
दाक्षायणी महादेवी गौरी haimavatI शुभा\
एकपर्णाग्रजा saumyA तथा vai चैकपाटला || 14\
अपर्णा वरदा देवी वरदानैकतत्परा\
उमा सुरहरा साक्षात् कौशिकी वा कपर्दिनी || 15\
खट्वाङ्गधारिणी दिव्या कराग्र-taru-पल्लवा\
नैगमेयादिभिर् divyaish चतुर्भिः putrakair-वृता || 16\
मेनाया नन्दिनी देवी वारिजा वारिजेक्षणा\
अंबाया वीतशोकस्य nandinash cha महात्मनः || 17\
शुभावत्याः सखी शान्ता पञ्चचूडा वरप्रदा\
सृष्ट्यर्थं सर्वभूतानां प्रकृति त्वं गताव्यया || 18\
त्रयोविंशतिभिस् tattvair महदाद्यैर् विजृम्भिता\
लक्ष्म्यादि-shaktibhir नित्यं नमिता nanda-नन्दिनी || 19\
मनोन्मनी महादेवी मायावी मण्डनप्रिया\
मायया yA jagat-सर्वं ब्रह्माद्यं सचराचरम् || 20\
क्षोभिणी मोहिनी नित्यं योगिनां हृदि संस्थिता\
एकानेकस्थिता loke इन्दीवरनिभेक्षणा || 21\
भक्त्या परमया नित्यं sarva-devair-अभिष्टुता\
गणेन्द्राम्भोज-garbhendra-yama-vittesha-पूर्वकैः || 22\
संस्तुता जननी तेषां sarvopadrava-नाशिनी\
bhaktA-नामार्तिहा भव्या bhava-bhAva-विनाशनी || 23\
bhukti-mukti-प्रदा दिव्या भक्तानाम्-अप्रयत्नतः\
sA me साक्षान्-महादेवी पापम् Ashu vyapohatu || 24

\[It seems that in the iconography of the earlier सैद्धान्तिक tantra-s सदाशिव was an एकवीर depicted without the accompanying shakti. But in the पञ्चावर्ण stava of aghorashiva we find the shakti very much in place. However, her visualization is not described there as it seem the unity of the shakti with सदाशिव is emphasized this way. Interestingly, in this stava she receives a longer praise than सदाशिव. This, points to the parallel सैद्धान्तिक stream where she played a major role, reminiscent of what was seen in the development of the bhairava- and वाम- स्रोतांसि. She is praised with her usual epithets from पौराणिक legend and also described as the mother of the four fold कुमार-s starting with naigameya. This rather archaic feature is reiterated in this text (see below). Moreover, she is also described as the mother of nandin, गणेश and the deva-s. She is identified with प्रकृति and its 23 evolutes as described in the सांख्य tradition. This is in opposition to the सैद्धान्तिक situation where 36 tattva-s are believed to exist. Thus, this seems to represent a पौराणिक "स्मार्तिज़तिओन्" of the सैद्धान्तिक shaiva position.]

चण्डेश्वरः:\
चण्डः सर्वगणेशानो मुखाच्-छंभोर्-विनिर्गतः\
शिवार्चनरतः श्रीमान् sa me पापं vyapohatu || 25

नन्दिकेश्वरः:\
शालङ्कायन-putrastu hala-मार्गोत्थितः प्रभुः\
जामाता मरुतां देवः sarva-भूत-महेश्वरः || 26\
सर्वगः सर्वदृक् शर्वः sarvesha-सदृशः प्रभुः\
sa-नारायणकैर् देवैः sendra-chandra-दिवाकरैः || 27\
siddhaish cha यक्ष-gandharvair भूतैर्-भूत-विधायकैः\
uragair ऋषिभिश् chaiva ब्रह्मणा cha महात्मना || 28\
stutas-trailokya-नाथस्तु munir-अन्तः पुरं स्थितः\
सर्वदा पूजितः sarvair nandI पापं vyapohatu || 29

गणपतिः:\
महाकायो महातेजा महादेव इवापरः\
शिवार्चनरतः श्रीमान् sa me पापं vyapohatu || 30\
meru-मन्दार-कैलास- तट-कूट-प्रभेदनः\
ऐरावतादिभिर् divyair dig-gajaish cha सुपूजितः || 31\
sapta-पातालपादश् cha sapta-द्वीपोरुजङ्घकः\
सप्तार्णवाङ्कुशश्-chaiva sarva-तीर्थोदरः शिवः || 32\
आकाश-deho dig-बाहुः soma-सूर्याग्नि-लोचनः\
हतासुर-महावृक्षो brahma-विद्या-महोत्कटः || 33\
ब्रह्माद्याधोरणैर् divyair yoga-pAsha-समन्वितैः\
baddho हृत्-पुण्डरीकाख्ये स्तंभे वृत्तिं nirudhya cha || 34\
नागेन्द्र-vaktro yaH साक्षाद् गण-कोटि-shatair-वृतः\
shiva-ध्यानैक-सम्पन्नः sa me पापं vyapohatu || 35

भृङ्गी:\
भृङ्गीशः पिङ्गलाक्षो .asau भसिताशस्तु dehayuk\
शिवार्चनरतः श्रीमान् sa me पापं vyapohatu || 36

कुमारः:\
chaturbhis-tanubhir नित्यं सर्वासुर-निबर्हणः\
स्कन्दः शक्तिधरः शान्तः सेनानीः शिखिवाहनः || 37\
देवसेनापतिः श्रीमान् sa me पापं vyapohatu

अष्ट-मूर्तयः:\
भवः sharvas-तथेशानो रुद्रः pashupatis तथा || 38\
ugro भीमो महादेवः शिवार्चनरतः सदा\
एताः पापं vyapohantu मूर्तयः परमेष्ठिनः || 39

एकादश रुद्राः:\
महादेवः shivo रुद्रः शङ्करो नीललोहितः\
ईशानो vijayo भीमो devadevo भवोद्भवः || 40\
कपालीशश् cha विज्ञेयो रुद्रा रुद्रांश-संभवाः\
shiva-प्रणाम-सम्पन्ना vyapohantu मलं mama || 41

द्वादशादित्याः:\
vikartano विवस्वांश् cha मार्तण्डो भास्करो रविः\
लोकप्रकाशकश्चैव लोकसाक्षीत्रिविक्रमः || 42\
Adityash cha तथा सूर्यश् चांशुमांश् cha दिवाकरः\
ete vai द्वादशादित्या vyapohantu मलं mama || 43

तन्मात्राः:\
गगनं स्पर्शनं tejo rasash cha पृथिवी तथा\
चन्द्रः सूर्यस्-तथात्मा cha तनवः shiva-भाषिताः || 44\
ete पापं vyapohantu भयं निर्णाशयन्तु me

लोकपालाः:\
वासवः पावकश्चैव yamo निरृतिरेव cha || 45\
वरुणो वायुसोमौ cha ईशानो भगवान् हरिः\
पितामहश् cha भगवान् शिवध्यानपरायणः || 46\
ete पापं vyapohantu मनसा कर्मणा कृतम्

मरुतः:\
नभस्वान् sparshano वायुर् anilo मारुतस् तथा || 47\
प्राणः प्राणेश-जीवेशौ मारुतः शिवभाषिताः\
शिवार्चनरताः sarve vyapohantu मलं mama || 48

चारणाः:\
खेचरी वसुचारी cha brahmesho brahma-ब्रह्मधीः\
सुषेणः शाश्वतः पृष्टः सुपुष्टश् cha महाबलः || 49\
ete vai चारणाः शंभोः पूजयातीव भाविताः\
vyapohantu मलं सर्वं पापं chaiva mayA कृतम् || 50

सिद्धाः:\
मन्त्रज्ञो mantravit प्राज्ञो मन्त्रराट् सिद्धपूजितः\
siddhavat-परमः सिद्धः सर्वसिद्धिप्रदायिनः || 51\
vyapohantu मलं sarve सिद्धाः शिवपदार्चकाः

कुबेरादि यक्षगणाः:\
यक्षो यक्षेश dhanado जृम्भको मणिभद्रकः || 52\
पूर्णभद्रेश्वरो mAlI shiti-कुण्डलिर्-eva cha\
narendrashchaiva यक्षेशा vyapohantu मलं mama || 53

सर्पाः:\
अनन्तः kulikashchaiva वासुकिस्तक्षकस्तथा\
कर्कोटको महापद्मः शङ्खपालो महाबलः || 54\
शिवप्रणामसम्पन्नाः शिवदेहप्रभूषणाः\
mama पापं vyapohantu विषं स्थावरजङ्गमम् || 55

किन्नराः:\
वीणाज्ञः kinnarashchaiva सुरसेनः प्रमर्दनः\
अतीशयः sa प्रयोगी गीतज्ञश्चैव किन्नराः || 56\
शिवप्रणामसम्पन्ना vyapohantu मलं mama

विद्याधराः:\
विद्याधरश् cha vibudho विद्याराशिर्विदां वरः || 57\
vibuddho विबुधः श्रीमान् कृतज्ञश् cha महायशाः\
ete विद्याधराः sarve शिवध्यानपरायणाः || 58\
vyapohantu मलं घोरं महादेवप्रसादतः

असुराः:\
वामदेवी महाजम्भः कालनेमिर्महाबलः || 59\
सुग्रीवो mardakashchaiva पिङ्गलो देवमर्दनः\
प्रह्रादश् चाप्य् अनुह्रादः संह्रादः kila बाष्कलौ || 60\
जम्भः कुंभश् cha मायावी कार्तवीर्यः कृतंजयः\
ete .असुरा महात्मानो महादेवपरायणाः || 61\
vyapohantu भयं ghoram आसुरं भावमेव cha

गरुडाः:\
गरुत्मान् khagatishchaiva पक्षिराट् नागमर्दनः || 62\
नागशत्रुर् हिरण्याङ्गो वैनतेयः प्रभञ्जनः\
नागाशीर्विषनाशश् cha विष्णुवाहन eva cha || 63\
ete हिरण्यवर्णाभा garuDA विष्णुवाहनाः\
नानाभरणसम्पन्ना vyapohantu मलं mama || 64

ऋषयः:\
agastyash cha वसिष्ठश् cha अङ्गिरा भृगुरेव cha\
काश्यपो नारदश्चैव दधीचश्च्यवनस् तथा || 65\
उपमन्युस्तथान्ये cha ऋषयः शिवभाविताः\
शिवार्चनरताः sarve vyapohantu मलं mama || 66

pitaraH:\
pitaraH पितामहाश् cha tathaiva प्रपितामहाः\
अग्निष्वात्ता बर्हिषदस् तथा मातामहादयः || 67\
vyapohantu भयं पापं शिवध्यानपरायणाः

मातरः:\
लक्ष्मीश् cha dharaNI chaiva gAyatrI cha सरस्वती || 68\
दुर्गा उषा शची ज्येष्ठा मातरः सुरपूजिताः\
देवानां मातरश् chaiva गणानां मातरस् तथा || 69\
भूतानां मातरः सर्वा yatra yA गण-मातरः\
प्रसादाद् devadevasya vyapohantu मलं mama || 70

अप्सरसः:\
उर्वशी मेनका chaiva रंभा rati तिलोत्तमाः\
सुमुखी दुर्मुखी chaiva कामुकी कामवर्धनी || 71\
तथान्याः सर्वलोकेषु दिव्याश्चाप्सरसस् तथा\
शिवाय ताण्डवं नित्यं kurvantyo .अतीव भाविताः || 72\
देव्यः शिवार्चनरता vyapohantu मलं mama

ग्रहाः:\
अर्कः somo .अङ्गारकश् cha budhashchaiva बृहस्पतिः || 73\
शुक्रः shanaishcharashchaiva राहुः ketustathaiva cha\
vyapohantu भयं घोरं ग्रहपीडां शिवार्चकाः || 74

राशयः:\
मेषो वृषो .atha mithunas तथा कर्कटकः शुभः\
सिंहश् cha कन्या विपुला tulA vai वृश्चिकस् तथा || 75\
dhanush cha makarashchaiva कुंभो मीनस्तथैव cha\
राशयो द्वादश hyete shiva-पूजा-परायणाः || 76\
vyapohantu भयं पापं प्रसादात्-परमेष्ठिनः

नक्षत्राणि:\
अश्विनी भरणी chaiva कृत्तिका रोहिणी तथा || 77\
श्रीमन्मृगशिरश्चार्द्रा पुनर्वसुपुष्यसार्पकाः\
मघा vai पूर्वफाल्गुन्य उत्तराफाल्गुनी तथा || 78\
हस्तचित्रा तथा स्वाती विशाखा चानुराधिका\
ज्येष्ठा मूलं महाभागा पूर्वाषाढा tathaiva cha || 79\
उत्तराषाढिका chaiva श्रवणं cha श्रविष्ठिका\
शतभिषक् पूर्वभद्रा तथा प्रोष्ठपदा तथा || 80\
पौष्णं cha देव्यः सततं vyapohantu मलं mama

pramatha-गनाः:\
ज्वरः kumbhodarash-chaiva शङ्कुकर्णो महाबलः || 81\
महाकर्णः प्रभातश् cha महाभूतप्रमर्दनः\
श्येनजिच्छिवदूतश् cha प्रमथाः प्रीतिवर्धनाः || 82\
कोटिकोटिशतैश्चैव भूतानां मातरः सदा\
vyapohantu भयं पापं महादेवप्रसादतः || 83

पर्वताः:\
शिवध्यानैकसम्पन्नो हिमराड् अंबुसन्निभः\
कुन्देन्दुसदृशाकारः कुंभकुन्देन्दुभूषणः || 84\
वडवानलशत्रुर् yo वडवामुखभेदनः\
चतुष्पादसमायुक्तः क्षीरोद iva पाण्डुरः || 85\
rudraloke sthito नित्यं रुद्रैः सार्धं गणेश्वरैः\
वृषेन्द्रो विश्वधृग् devo vishvasya जगतः pitA || 86\
वृतो नन्दादिभिर् नित्यं मातृभिर् मखमर्दनः\
शिवार्चनरतो नित्यं sa me पापं vyapohatu || 87

गङ्गा:\
गङ्गा माता जगन्माता rudraloke व्यवस्थिता\
शिवभक्ता tu yA nandA sA me पापं vyapohatu || 88

भद्रा:\
भद्रा भद्रपदा देवी shivaloke व्यवस्थिता\
माता गवां महाभागा sA me पापं vyapohatu || 89

सुरभिः:\
सुरभिः सर्वतोभद्रा सर्वपापप्रणाशनी\
रुद्रपूजारता नित्यं sA me पापं vyapohatu || 90

सुशीला:\
सुशीला शीलसम्पन्ना श्रीप्रदा शिवभाविता\
shivaloke स्थिता नित्यं sA me पापं vyapohatu || 91

brahmA-विष्णु-indra-यमः:\
वेदशास्त्रार्थतत्त्वज्ञः सर्वकार्याभिचिन्तकः\
samasta-guNa-सम्पन्नः sarva-देवेश्वरात्मजः || 92\
ज्येष्ठः सर्वेश्वरः saumyo महाविष्णु-तनुः svayam\
आर्यः सेनापतिः साक्षाद् gahano मखमर्दनः || 93\
ऐरावत-गजारूढः कृष्ण-कुञ्चित-मूर्धजः\
कृष्णाङ्गो रक्तनयनः shashi-pannaga-भूषणः || 94\
भूतैः प्रेतैः पिशाचैश् cha कूष्माण्डैश् cha समावृतः\
शिवार्चनरतः साक्षात् sa me पापं vyapohatu || 95

अष्ट-मातृकाः cha योगिन्यः:\
brahmANI chaiva माहेशी कौमारी वैष्णवी तथा\
वाराही chaiva माहेन्द्री चामुण्डाग्नेयिका तथा || 96\
etA vai मातरः सर्वाः सर्वलोकप्रपूजिताः\
योगिनीभिर् महापापं vyapohantu समाहिताः || 97

वीरभद्रः:\
वीरभद्रो महातेजा hima-kundendu-सन्निभः\
rudrasya tanayo रौद्रः शूलासक्तमहाकरः || 98\
सहस्रबाहुः सर्वज्ञः सर्वायुधधरः svayam\
त्रेताग्नि-nayano devas त्रैलोक्याभयदः प्रभुः || 99\
मातॄणां रक्षको नित्यं महावृषभवाहनः\
त्रैलोक्यनमितः श्रीमान् शिवपादार्चने रतः || 100\
यज्ञस्य cha शिरश्छेत्ता पूष्णो danta-विनाशनः\
vahner हस्तहरः साक्षाद् bhaga-netra-निपातनः || 101\
पादाङ्गुष्ठेन सोमाङ्ग-पेषकः प्रभुसंज्ञकः\
उपेन्द्रेन्द्रयमादीनां देवानाम् अङ्गरक्षकः || 102\
सरस्वत्या महादेव्या नासिकोष्ठावकर्तनः\
गणेश्वरो yaH सेनानीः sa me पापं vyapohatu || 103

महालक्श्मी:\
ज्येष्ठा वरिष्ठा वरदा वराभरणभूषिता\
महालक्ष्मीर्जगन्माता sA me पापं vyapohatu || 104\
महामोहा महाभागा महाभूतगणैर्वृता\
शिवार्चनरता नित्यं sA me पापं vyapohatu || 105

लक्ष्मी:\
लक्ष्मीः सर्वगुणोपेता सर्वलक्षणसंयुता\
सर्वदा सर्वगा देवी sA me पापं vyapohatu || 106

महिषमर्दिनी:\
सिंहारूढा महादेवी पार्वत्यास्तनयाव्यया\
विष्णोर्निद्रा महामाया वैष्णवी सुरपूजिता || 107\
त्रिनेत्रा वरदा देवी महिषासुरमर्दिनी\
शिवार्चनरता दुर्गा sA me पापं vyapohatu || 108

रुद्राः:\
ब्रह्माण्डधारका रुद्राः सर्वलोकप्रपूजिताः\
सत्याश् cha मानसाः sarve vyapohantu भयं mama || 109

भूतगणेश्वराः:\
भूताः प्रेताः पिशाचाश् cha कूष्माण्डगणनायकाः\
कूष्माण्डकाश् cha te पापं vyapohantu समाहिताः || 110

phalashruti:\
anena देवं स्तुत्वा tu चान्ते सर्वं समापयेत्\
प्रणम्य शिरसा भूमौ प्रतिमासे द्विजोत्तमाः || 111\
व्यपोहनस्तवं दिव्यं yaH पठेच्छृणुयादपि\
विधूय सर्वपापानि rudraloke महीयते || 112\
कन्यार्थी labhate कन्यां जयकामो जयं labhet\
अर्थकामो लभेदर्थं पुत्रकामो बहून् सुतान् || 113\
विद्यार्थी labhate विद्यां भोगार्थी भोगमाप्नुयात्\
यान्यान्प्रार्थयते कामान् मानवः श्रवणादिह || 114\
तान्सर्वान् शीघ्रमाप्नोति देवानां cha priyo bhavet\
पठ्यमानमिदं पुण्यं yamuddishya tu पठ्यते || 115\
tasya रोगा na बाधन्ते वातपित्तादिसंभवाः\
नाकाले मरणं tasya na sarpairapi dashyate || 116\
यत्पुण्यं chaiva तीर्थानां यज्ञानां chaiva yatphalam\
दानानां chaiva यत्पुण्यं व्रतानां cha विशेषतः || 117\
तत्पुण्यं कोटिगुणितं जप्त्वा चाप्नोति मानवः\
goghnashchaiva कृतघ्नश् cha वीरहा ब्रह्महा bhavet || 118\
शरणागतघाती cha मित्रविश्वासघातकः\
दुष्टः पापसमाचारो मातृहा पितृहा तथा || 119\
vyapohya सर्वपापानि shivaloke महीयते ||120


