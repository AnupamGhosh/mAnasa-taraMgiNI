
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The teaching of the यमोघण्ट](https://manasataramgini.wordpress.com/2014/01/25/the-teaching-of-the-yamoghanta/){rel="bookmark"} {#the-teaching-of-the-यमघणट .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[January 25, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/01/25/the-teaching-of-the-yamoghanta/ "5:23 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

She said that she was शोणितमेखला, the barmaid from the glorious ओड्डियान. We excitedly asked her teaching of the amaraugha. She said: [सोमाख्यः पथेन गच्छति । हालापीवी देवदत्तं पथेन गमयति । पथं वृणीष्व ॥]{style="color:#99cc00;"} (सोमाख्य goes by the path; हालापीविन् makes devadatta go by the path; choose a path for yourself.) Then she then became silent, showed the sign of the three fingers, and went away stating that customers were waiting at the counter.

Then we proceeded to the cluster of caves in कृष्णगिरि in the country of the राष्ट्रक-s. As we were about to enter the chosen cave सुत्वचा the fair daughter of the ब्राह्मण crossed our path. She asked: "Did you understand the teachings of शोणितमेखला". We said we were still seeking their meaning. She went away giggling. There in the cave where the Iranian lord had performed a homa to rudra sat the यमोघण्ट named अन्तकभृत्य. He asked pointed to a rocky seat and said: "About two thousand years ago the Iranian lord sat there; he is long dead and mostly forgotten today but take that seat. We shall now study some poetry." Then he went on to expound three verses from fifty known as the vakrokti पञ्चाशिक of Kashmirian kavi भट्ट रत्नाकर from the court of राजन् avantivarman:

[त्वं हालाहल-भृत् करोषि मनसो मूर्चां ममालिङ्गितो]{style="color:#99cc00;"}\
[हालां नैव बिभर्मि नैव छ हलं मुग्धे कथं हालिकः ।]{style="color:#99cc00;"}\
[सत्यं हालिकतैव ते समुछितासक्तस्य गोवाहने]{style="color:#99cc00;"}\
[वक्रोक्त्येति जितो हिमाद्रि-सुतया स्मेरो हरह् पातु वः ॥]{style="color:#99cc00;"} 2\
When embraced by me you make my mind dizzy -- \with the \[noxious] हालाहल you hold.\by the beer and plow (water) you hold.\\
Silly lady! I hold neither beer nor a plow, how am I a plowman?\
Truth to be told the state of a plowman is appropriate for your attachment -- \to the bovine vehicle\to plying the bovine.\
May hara smiling while thus defeated via use of vakrokti (ambiguous speech/punning words) by the daughter of the Himalayas protect you.

The pun here creates an equivalence between rudra the holder of the हालाहल and the संकर्षण of the पाञ्चरात्रिक-s who holds the hala and hAlA. Indeed, the महाभरत mentions that balarAma, the earthly manifestation of the संकर्षण, is pacified when a bowl of beer is placed before him by the fair hands of his wife रेवती. The poet भास makes a word play on this in the open verse of his play the स्वप्नवासवदत्त thus:\
[उदयनवेन्दुसवर्णावासवदत्ताबलौ बलस्य त्वाम् ।]{style="color:#99cc00;"}\
[पद्मावतीर्णपूर्णौ वसन्तक्रमौ भुजौ पाताम् ॥]{style="color:#99cc00;"}

May bala\[rAma]'s arms protect you, whose color is like the newly arisen moon, imparted vigor by beer, enhanced by पद्मावती (supposed to mean रेवती here), like the pleasant progression of spring. It also plays on the names of the menage-a-trois in the drama comprised of king udayana, his wife वासवदत्ता and the princess पद्मावती

But this beer-swilling संकर्षण in the पाञ्चरात्रिक tradition is indeed an equivalent of rudra and performs the same functions as him. Hence, the word play in this verse indicates the homology between the deities is indicated. Moreover, rudra denying his association with beer indicates his domesticated state. This is different from his wild state as a manifestation of the violent principle inherent in beer -- a point alluded to in the shruti (e.g. [shatapatha ब्राह्मण](https://manasataramgini.wordpress.com/2011/04/28/notes-on-the-vishnu-virachita-rudra-stotram/ "Notes on the विष्णु-virachita rudra stotram")) in the context of the सौत्रामणि ritual. It also distinguishes him from the state where accompanied by the many female deities, the one in which alcoholic beverage is a sacrament as in the tantra-s of the bhairava-srotas.

[त्वं मेनाभिमतो [मे न-अभिमतो\मेना-अभिमतो] भवामि सुतनु श्वश्र्वा अवश्यं मतः]{style="color:#99cc00;"}\
[साधूक्तं भवता न मे रुछित [नमेरु-छित] इत्य् अत्र ब्रुवे ।अहं पुनः ।]{style="color:#99cc00;"}\
[मुग्धे नास्मि नमेरुणा ननु चितः प्रेक्षस्व मां पातु वो]{style="color:#99cc00;"}\
[वक्रोक्त्येति हरो हिमाछल-भुवं स्मेराननां मूकयन् ॥]{style="color:#99cc00;"} 3

\You are not loved by me \मेना likes you\; Indeed, my mother-in-law likes me, O pretty-bodied one.\
Well played (said) by you; \[but] I am saying it again:\you are not liked by me\you are a रुद्राक्ष tree (have रुद्राक्ष-s piled on you).\\
But I'm not piled up with रुद्राक्ष-s, silly lady, look at me!\
May hara silencing with his punning words the smiling faced one born of the Himalayas protect you.

Here rudra is in his domesticated form stated above; hence, he is not piled up with रुद्राक्ष-s.

[नो संध्याहित-मत्सरा [संध्य्-आहित\संध्या-आहित] तव तनौ वत्स्याम्य् अहं संधिना]{style="color:#99cc00;"}\
[ na प्रीतासि varoru chet kathaya tat prastaumi kiM vigraham |]{style="color:#99cc00;"}\
[कार्यं तेन न किं चिद् अस्ति शठ मे वीनां ग्रहेणेति वो]{style="color:#99cc00;"}\
[दिश्यासुः प्रतिबद्ध-केलि शिवयोः श्रेयांसि वक्रोक्तयः ॥]{style="color:#99cc00;"} 4

\I resent our truce\I resent \[your] flirting with संध्या\ and no more in your body I will stay.\
Pretty-thighs! If you are not happy with the truce simply say that. Should I \[then] call declare \hostilities?\a hunt for catching birds?\\
You rascal! Of what use is this business of catching birds to me.\
May the punning words of shiva and शिवा contesting in this game bring you to felicities.

The catching of birds (vi-graha) is an allusion to rudra being called a पुञ्जिष्ठ in the shruti. But you must note that here उमा has called rudra a rascal and clearly told him of her lack of love for him and expressed her intention not to stay in his body. Hence, he has to wander on his own.

Most worship महादेव along with his shakti. This is the साधारण-patha. The ecstatic and high kulapatha is that of rudra as vIra in the midst of his kula with the योगिनी-s, डाकिनी-s and शाकिनी-s. But now that sati has left him he is an एकवीर. The one who follows that path shall explore the mysteries of the अकुलवीर.

We said: "But then they say shiva without शिवा is but a shava. So how could one tread the path of the अकुलवीर".\
The यमोघण्ठ: "Close your eyes."\
We found ourselves in the द्रंइड country near where the land meets the sea. There was an image of the old goddess पत्नी, who had been worshiped by the drAviDa-s in the long past days even as the Iranian was performing his homa to maheshvara. It was a grim and inauspicious day. Yet, everyone around us was laughing and talking in loud tones trying to make themselves heard over the din. Tired from the exertions of the long day we laid ourselves near a large image of कुमार to get some rest. Suddenly, a band of शाकिनी-s arrived and tore us to shreds eating us up entirely.\
We opened our eyes and asked: "Are we dead?"\
The यमोघण्ठ: "Close your eyes again."\
We were in the holy city of जालन्धर in the पञ्चनद country. There were three corpses in the biers which were laid beside us. All of a sudden one of them started moving and then getting up made a loud and terrible noise. We were reminded of क्षेमेन्द्र's words "ghora-फेरावहुंकृतं". We opened our eyes and asked: "Why did that one who was dead move?"\
The यमोघण्ठ: "It was animated by a वेताल captured by शोणितमेखला."\
We: "You are indeed a यमोघण्ठ without the chitra-paTa-s (painted scrolls)".\
The यमोघण्ठ: "Go the mouth the cave and gaze at the yonder sky."\
We: "A flock of egrets in flight."\
The यमोघण्ठ: "the old भास had said:\
विदूषक- [ही ही, शरत् काल निर्मले ऽन्तरिक्षे प्रसारित-बलदेव-बाहु-दर्शनीय सारस-पङ्क्ति यावत् समाहितं गच्छन्ती प्रेक्षतां तावद् भवान् ।]{style="color:#99cc00;"}\
The king- [वयस्य, पश्याम्य् एनाम् ।]{style="color:#99cc00;"}\
[ ऋज्वायतां cha विरलां cha नतोन्नतां cha]{style="color:#99cc00;"}\
[सप्तर्षि-वंश-कुटिलां च निवर्तनेषु ।]{style="color:#99cc00;"}\
[निर्मुछ्यमान-भुजगोदर-निर्मलस्य]{style="color:#99cc00;"}\
[सीमाम् इवअम्बर-तलस्य विभज्यमानाम् ॥]{style="color:#99cc00;"}\
The jester: Hey hey, may your highness see that flock of cranes flying in the clear autumnal welkin like the outstretched arm of baladeva.\
The king: friend, I see these.\
Sometimes straight, sometime dispersed wide apart, sometimes in a V-shaped configuration,\
taking the form the constellation of the seven seers (Ursa Major) while turning,\
like the white belly of a snake which has sloughed its skin,\
\[the flock] is like a dividing contour on the surface of the sky.

This old भास must have been a bit of a पाञ्चरात्रिक. The संकर्षण indeed stirs things up, much like the atmospheric dance of the crane flock. That is what one might see in the simile which likens his fair hand to them. Then there is also the welkin in you which is the चिदाकाश. These birds are tracing paths on that. Those are the movements of prANa that we term the अजपा gAyatrI, the mantra, [सो।अहं हंसः ।]{style="color:#99cc00;"}

It is that mantra you now go forth and try to master. It must be done in solitude. As you leave the fair daughter of the ब्राह्मण will come you path. But you have to walk past her for no use will come of coitus with her. But three ladies whose names will have the initial consonants p, v, and b will keep you company -- but remember their company will be even like that of हत्या who gave the bhairava company till he drank विष्णु's blood. You might die on the path sooner or later and might become the dwelling of a वेताल. The end only you will know"

We were not sure if we were dead or alive.


