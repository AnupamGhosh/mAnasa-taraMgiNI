
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The इन्द्रजाल-सूक्तं and the atharvan military ritual](https://manasataramgini.wordpress.com/2014/06/28/the-indrajala-suktam-and-the-atharvan-military-ritual/){rel="bookmark"} {#the-इनदरजल-सकत-and-the-atharvan-military-ritual .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 28, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/06/28/the-indrajala-suktam-and-the-atharvan-military-ritual/ "7:33 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

An atharvan military ritual for kShatriya-s headed to war involves deployment of AV-vulgate 8.8 or its cognate AV-P 16.29. The ritual is specified by the kaushika sUtra 16.9-20 and some details are provided by the atharvan commentators दारिल and keshava.

The atharvan purohita uses a fire drill with an ashvattha (Pipal) plank and a badhaka drill-bit (One tradition holds that this plant is Taxus baccata) and generates fire via friction using the below churning mantra:

[इन्द्रो मन्थतु मन्थिता शक्रः शूरः पुरंदरः ।]{style="color:#000080;"}\
[यथा हनाम सेना अमित्राणां सहस्रशः ॥१॥]{style="color:#000080;"}\
indra the shaker, shake up the enemies, the mighty hero, the breaker of forts, so that we might slaughter the armies of our enemies by the thousands.

When he sees the sparks he places an old rope with the incantation (in place of अमूम् he may use the name of the enemy army throughout this deployment):\
[पूतिरज्जुर् उपध्मानी पूतिं सेनां कृणोत्व् अमूम् ।]{style="color:#000080;"}

When he sees the rope smoking he utters the incantation:\
[धूमम् परादृश्या ऽमित्रा हृत्सु आ दधतां भयम् ।]{style="color:#000080;"}

When the rope catches fire he recites the mantra:\
[अग्निम् परादृश्या ऽमित्रा हृत्सु आ दधतां भयम् ।]{style="color:#000080;"}

These are derived from the ऋक्\
[पूतिरज्जुर् उपध्मानी पूतिं सेनां कृणोत्व् अमूम् ।]{style="color:#000080;"}\
[धूमम् अग्निम् परादृश्या ऽमित्रा हृत्स्व् आ दधतां भयम् ॥२॥]{style="color:#000080;"}\
Let the putrid rope, blowing forth make that army putrid; let fear be in our enemies hearts seeing the smoke and fire from afar.

Then he places samidh-s of the ashvattha, badhaka, ताजद्भङ्ग (castor), Ahva (पलाश= Butea monosperma), khadira (Acacia) and shara (arrow-grass) with the below mantras:

[अमून् अश्वत्थ निः शृणीहि खादामून् खदिराजिरम् ।]{style="color:#000080;"}\
[ताजद्भङ्ग इव भजन्तां हन्त्व् एनान् वधको वधैः ॥३॥]{style="color:#000080;"}\
O pipal, pound those forces, devour them O khadira, break them O castor plant, kill them with weapons, O vadhaka!

[परुषान् अमून् परुषाह्वः कृणोतु हन्त्व् एनान् वधको वधैः ।]{style="color:#000080;"}\
[क्षिप्रं शर इव भजन्तां बृहज्जालेन संदिताः ॥४॥]{style="color:#000080;"}\
O piercing plant make those of the enemy army pierced, may the vadhaka plant slay them weapons! Quickly like a reed may they be broken and entrapped in the great snare.

Then the objects of war invoked in the rest of the सूक्तं are serially offered dual offerings by the atharvan ritualist: with his right hand he makes a ghee oblation with his darvi ladle ending with स्वाहा for objects of war of his side and to gods for his side; e.g. [जालाय स्वाहा । जालदण्डाय स्वाहा । इन्द्राय स्वाहा । इदम् इन्द्राय न मम ।]{style="color:#000080;"} etc\
With his left hand he makes oblations of इङ्गिड oil to the same fire on the badhaka wood with a deprecation for this foes; e.[ग्। दस्यूनां सेनायै दुराहा । न मम इदम् ।]{style="color:#000080;"}

[अन्तरिक्षं जालम् आसीज् जालदण्डा दिशो महीः ।]{style="color:#000080;"}\
[तेनाभिधाय दस्यूनां शक्रः सेनाम् अपावपत् ॥५॥]{style="color:#000080;"}\
The atmosphere was the net, the great directions were were the net-rods; entrapping them therewith indra uprooted the army of the dasyu-s.

[बृहद् धि जालं बृहतः शक्रस्य वाजिनीवतः ।]{style="color:#000080;"}\
[तेन शत्रून् अभि सर्वान् न्य् उब्ज यथा न मुच्यातै कतमश् चनैषाम् ॥६॥]{style="color:#000080;"}\
Great indeed is the web of the mighty indra, of the vigorous one; therewith may you swoop upon all enemies, such that no one of them may escape.

[बृहत् ते जालं बृहत इन्द्र शूर सहस्रार्घस्य शतवीर्यस्य ।]{style="color:#000080;"}\
[तेन शतं सहस्रम् अयुतं न्यर्बुदं जघान शक्रो दस्यूनाम् अभिधाय सेनया ॥७॥]{style="color:#000080;"}\
Great, O indra the valiant, is the web of yours, which is great worth a thousand, with hundred-fold powers; therewith entrapping the army of the dasyu-s, indra slew a hundred, a thousand, a ten thousand and a hundred million!

[अयं लोको जालम् आसीच् छक्रस्य महतो महान् ।]{style="color:#000080;"}\
[तेनाहम् इन्द्रजालेनामूंस् तमसाभि दधामि सर्वान् ॥८॥]{style="color:#000080;"}\
This great world itself was the web of the great indra; with that web I deliver the entire enemy army into darkness!

[सेदिर् उग्रा व्यृद्धिर् आर्तिश् चानपवाचना ।]{style="color:#000080;"}\
[श्रमस् तन्द्रीश् च मोहश् च तैर् अमून् अभि दधामि सर्वान् ॥९॥]{style="color:#000080;"}\
Illness, fierce misfortune, mishaps which cannot be countered by their mantras, toil, lassitude, and delusion with these I entrap the enemy army.

[मृत्यवे ऽमून् प्र यछामि मृत्युपाशैर् अमी सिताः ।]{style="color:#000080;"}\
[मृत्योर् ये अघला दूतास् तेभ्य एनान् प्रति नयामि बद्ध्वा ॥१०॥]{style="color:#000080;"}\
To death I deliver the enemy army, with the nooses of death the enemy army is bound, having bound them I take them to meet the funereal messengers of death!

[नयतामून् मृत्युदूता यमदूता अपोम्भत ।]{style="color:#000080;"}\
[परः सहस्रा हन्यन्तां तृणेढ्व् एनान् मत्यं भवस्य ॥११॥]{style="color:#000080;"}\
May you lead them O messengers of death; O agents of yama strangle them, may they be killed in numbers greater than thousands; let the hammer of rudra smash them!

[साध्या एकं जालदण्डम् उद्यत्य यन्त्य् ओजसा ।]{style="color:#000080;"}\
[रुद्रा एकं वसव एकम् आदित्यैर् एक उद्यतः ॥१२॥]{style="color:#000080;"}\
The साध्य-s go lifting with the mighty one of the net-rods; the rudra-s one, the vasu-s one, by the aditya-s one is lifted.

[विश्वे देवाः उपरिष्टाद् उब्जन्तो यन्त्व् ओजसा ।]{style="color:#000080;"}\
[मध्येन घ्नन्तो यन्तु सेनाम् अङ्गिरसो महीम् ॥१३॥]{style="color:#000080;"}\
Let all the gods from the sky arrive swooping with might; let the अङ्गिरस-s go midway slaying the mighty army.

[वनस्पतीन् वानस्पत्यान् ओषधीर् उत वीरुधः ।]{style="color:#000080;"}\
[द्विपाच् चतुष्पाद् इष्णामि यथा सेनाम् अमूं हनन् ॥१४॥]{style="color:#000080;"}\
The trees, substances from trees, herbs and plants, what ever is bipedal and quadrupedal I send that they may kill the enemy army.

[गन्धर्वाप्सरसः सर्पान् देवान् पुण्यजनान् पितॄन् ।]{style="color:#000080;"}\
[दृष्टान् अदृष्टान् इष्णामि यथा सेनाम् अमूं हनन् ॥१५॥]{style="color:#000080;"}\
The gandharva-s the apsaras-es, the snakes, the deva-s, the holy ones, the ancestors, those seen and those unseen I dispatch to kill the enemy army.

[इम उप्ता मृत्युपाशा यान् आक्रम्य न मुच्यसे ।]{style="color:#000080;"}\
[अमुष्या हन्तु सेनाया इदं कूटं सहस्रशः ॥१६॥]{style="color:#000080;"}\
Here are spread out the nooses of death, if you step into which you are not released; let this spike kill the enemy army by the thousands.

[घर्मः समिद्धो अग्निनायं होमः सहस्रहः ।]{style="color:#000080;"}\
[भवश् च पृश्नि-बाहुश् च शर्व सेनाम् अमूं हतम् ॥१७॥]{style="color:#000080;"}\
The gharma is offered via the kindled fire as this thousand-slaying oblation; bhava, one having पृष्नि in his arms, sharva (all names of rudra) slay that enemy army!

[मृत्योर् आषम् आ पद्यन्तां क्षुधं सेदिं वधम् भयम् ।]{style="color:#000080;"}\
[इन्द्रश् चाक्षु-जालाभ्यां शर्व सेनाम् अमूं हतम् ॥१८॥]{style="color:#000080;"}\
May the enemy be death's food, may they fall into hunger, illness, the deadly weapon and fear; by the trap and web O indra and rudra, the archer, slay the enemy army!

[पराजिताः प्र त्रसतामित्रा नुत्ता धावत ब्रह्मणा ।]{style="color:#000080;"}\
[बृहस्पति प्रनुत्तानां माम् ईषां मोचि कश् चन ॥१९॥]{style="color:#000080;"}\
Being defeated, being harassed, O enemies, run being expelled by brahman power; expelled by बृहस्पति may none of the enemy ever be freed!

[अव पद्यन्ताम् एषाम् आयुधानि मा शकन् प्रतिधाम् इषुम् ।]{style="color:#000080;"}\
[अथैषां बहु बिभ्यताम् इषवः घ्नन्तु मर्मणि ॥२०॥]{style="color:#000080;"}\
Let their weapons fall down; let them not be able aim their arrow; then as they are in much fear let the arrows strike their marman points.

[सं क्रोशताम् एनान् द्यावापृथिवी सम् अन्तरिक्षं सह देवताभिः ।]{style="color:#000080;"}\
[मा ज्ञातारं मा प्रतिष्ठां विदन्त मिथो विघ्नाना उप यन्तु मृत्युम् ॥२१॥]{style="color:#000080;"}\
The heaven and earth roar at them together; let the atmosphere together with the deities roar at them; let them not have a knowledgeable one in their midst, let them not find a foundation; mutually striking each other may they go unto death!

[दिशश् चतस्रो ऽश्वतर्यो देव-रथस्य पुरोदाशाः शफा अन्तरिक्षम् उद्धिः ।]{style="color:#000080;"}\
[द्यावापृथिवी पक्षसी ऋतवो ऽभीशवो ऽन्तर्देशाः किम्करा वाक् परिरथ्यम् ॥२२॥]{style="color:#000080;"}\
The 4 directions are the mules of the god-chariot; the ritual cakes are hoofs; the atmosphere the seat; heaven and earth the two sides; the seasons the reins; the intermediate directions the attendants and speech the chariot armor.

[संवत्सरो रथः परिवत्सरो रथोपस्थो विराड् ईषाग्नी रथमुखम् ।]{style="color:#000080;"}\
[इन्द्रः सव्यष्ठाश् चन्द्रमाः सारथिः ॥२३॥]{style="color:#000080;"}\
The year is the chariot; the parivatsara year the chariot base; विराट् the pole; agni the mouth of the chariot; indra the one standing to the left on the chariot; moon the charioteer.

[इतो जयेतो वि जय सं जय जय स्वाहा ।]{style="color:#000080;"}\
Conquer here, conquer the foes here, conquer completely, conquer; hail!

[इमे जयन्तु परामी जयन्तां स्वाहैभ्यो दुराहामीभ्यः ।]{style="color:#000080;"}\
[नीललोहितेनामून् अभ्यवतनोमि ॥२४॥]{style="color:#000080;"}\
Let those enemies be conquered; hail to these; wail to the enemies; with the aid of the blue-red rudra (nilalohita) I stretch the web out upon those enemies!

After the last स्वाहा and दुराहा he takes a red shoot of the ashvattha tree and entwines it with blue and red threads. Then he sticks it into the north of the fire and uttering the incantation nilalohitena... he pulls out the shoot towards the right with the threads.

While the ritual itself is symbolic, the traps and the web alluded to in the सूक्तं is hardly symbolic and represent real snares that are likely to be poisoned with castor. This is also likely to be combined with fire and smoke.


