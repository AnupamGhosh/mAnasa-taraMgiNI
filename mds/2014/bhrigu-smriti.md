
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [भृगु स्मृति](https://manasataramgini.wordpress.com/2014/03/08/bhrigu-smriti/){rel="bookmark"} {#भग-समत .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[March 8, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/03/08/bhrigu-smriti/ "9:22 PM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

We provide below a translation of the proto-scientific section of भृगु स्मृति along with a comparative analysis vis-a-vis Miletian school of Greeks:\
Translation of the [भृगु स्मृति 1-4](https://manasataramgini.files.wordpress.com/2014/03/bhrigu_smriti_hs-1.pdf)

We are not entirely happy with this translation done sometime back: Despite trying to be as literal as possible it is difficult to achieve that exactness for a text like this one. Yet it might be good enough for those interested in the comparative history of early philosophy and science in India and Greece


