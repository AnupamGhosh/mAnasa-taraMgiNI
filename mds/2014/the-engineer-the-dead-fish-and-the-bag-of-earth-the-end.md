
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The engineer, the dead fish and the bag of earth-the End](https://manasataramgini.wordpress.com/2014/06/02/the-engineer-the-dead-fish-and-the-bag-of-earth-the-end/){rel="bookmark"} {#the-engineer-the-dead-fish-and-the-bag-of-earth-the-end .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[June 2, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/06/02/the-engineer-the-dead-fish-and-the-bag-of-earth-the-end/ "5:49 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

[From Part-V](https://manasataramgini.wordpress.com/2014/05/29/the-engineer-the-dead-fish-and-the-bag-of-earth-v/ "The engineer, the dead fish and the bag of earth-V")\
kathA-पुच्छं\
Viruses have been around since the beginning of life and locked in conflict with cells and other viruses since then. In course of this epic conflict they have given genes to cells and taken genes from them. These exchanged genes usually code for weaponry used in these battles fought from times immemorial. The fact is without this weaponry, life as we know it would not exist and we ourselves owe our very existence to such. So indeed such is the story of life in a nutshell -- there is no life for the unarmed. Hence, the vipra who knows this becomes a seer who has seen and recites the following brahma-वाक्य-s of life:\
[सर्वो वै मेन्या जिवति । जिवो वै मेन्यां प्रतिष्ठितः । मेनेर् वै जीवस्य नाना रुपाणि संभवन्ति ॥]{style="color:#000080;"}

Time had passed by and some had become oblations in the great इष्टि to the buffalo-riding son of विवस्वान्. Others were waiting for their turn to become samidh-s in that offering from which none escapes. Vidrum, Sharvamanyu, Somakhya and Indrasena were thankful that they had not yet been offered. It was late one night they were seated at the table after dinner and reminiscing of the past. Somakhya: "Vidrum, there is something in the story that perhaps only you might be able to throw light on." Vidrum: "It is all very painful but then this is what I believe transpired. It appears Vrishchika tipped off the IB agent who was active on this matter. He had obtained intelligence that the म्लेच्छ-s had to passed the viruses on their close Islamic ally Saudi Arabia to use on Iran. Faqih ibn al-Ass was here to study how it was being deployed so that his agents could do the same in Iran. He had come under the guise of learning some technical aspects relating to a bat-borne virus in his country. Thus, he had managed to get biological material into the country. Acting on this intelligence, the IB agent asked me if I had anything to say about Tom, Harry and Faqih ibn al-Ass. I told him all that I knew and mentioned that Tom and Harry had fallen out with me and were to leave shortly. He kept a close watch on them. Just before leaving the agent caught Harry admonishing ibn al-Ass as to why he had brought units of the vaccine with him into bhArata and asked him to destroy them immediately. To this he answered that he had merely got them as a precaution. However, our IB agent had the intelligence that in reality he intended to pass them over to the Mujahideen al-Hind in course of a meeting he was to have with their leader in the auspices of the Saudi Arabian embassy. The IB agent saw the opportunity to get hold of the vaccine in the process. I was seeking revenge for Meghana's murder and suggested to him we not just get hold of the vaccine but also retaliate. So I obtained the virus you guys designed from Vrishchika and handed it over to the IB agent. In the mean time the two म्लेच्छ-s wanted some virus samples from here for their study. But their प्रणिधि-s had informed them that if they tried to sneak them out the might be caught. Hence, they decided to use their ally Faqih ibn al-Ass who had already obtained the paperwork for taking some biological material back. He came to me asking if he could have some samples. While I was holding him in conversation the IB agent got to the freezer where he kept stuff and replaced it with our engineered virus and took away his vaccine samples. In the mean time I too gave him the samples that in reality contained our engineered virus. I believe you may know more of the rest of the story."

Somakhya: "Ah! That's how it played out. I believe Faqih ibn al-Ass was given a royal entrance into the म्लेच्छ precincts for they believed they knew all about what he was carrying. He gave the spiked stuff to Dick Shuman with whom he got playing with them in cell-culture and animal experiments. I believe it was the sloppiness of al-Ass which ultimately proved the game-changer. Things got out of hand in course of his experimental training and the rest as they say is history. Little did I know when I first read of Varoli's work that it would spark such events. But then let's not forget that Varoli was not the first to invent these, her namesake (e.g. Cotesia) had stumbled on this strategy with polydnaviruses and nudiviruses to overpower कंबल-कीटक-s millions of years ago!"


