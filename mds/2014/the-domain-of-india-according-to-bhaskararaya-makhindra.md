
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [The domain of India according to भास्करराय-मखीन्द्र](https://manasataramgini.wordpress.com/2014/10/26/the-domain-of-india-according-to-bhaskararaya-makhindra/){rel="bookmark"} {#the-domain-of-india-according-to-भसकररय-मखनदर .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[October 26, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/10/26/the-domain-of-india-according-to-bhaskararaya-makhindra/ "7:38 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

भास्करराय was one of the greatest मन्त्रवादिन्-s of all times, who perhaps was only rivaled by the illustrious abhinavagupta or अघोरशिव देशिक, who in times closer to our own was verily like how a ब्राह्मण sage of the time of the veda might have been. He shone like a luminary on the north bank of the Kaveri illuminating many शास्त्र-s including the ऋक् and अथर्वण श्रुति-s, नवन्याय outside the वङ्ग country and above all the श्रीकुल. In the introduction to his श्रीकुल work वरिवस्या-rahasya he says:

[आ प्राचः कामरूपाद् द्रुहिण-सुत-नद-प्लाविताद् आ प्रतीचो]{style="color:#0000ff;"}\
[गान्धारात् सिन्धु-सान्द्राद्-रघुवर-चरिताद् आ च सेतोर् अवाचः ।]{style="color:#0000ff;"}\
[आ केदाराद् उदीचस् तुहिन-गहनतः सन्ति विद्वत्-समाजा]{style="color:#0000ff;"}\
[ये ये तान् एष यत्नः सुखयतु समजान् कश्च मत् कर्तुम् ईष्टे ॥]{style="color:#0000ff;"}

The learned assemblies, which indeed are from कामरूप in the east which is flooded by the riverine son of द्रुहिण (i.e. ब्रह्मा) to गान्धार in the west which is moistened by the Indus system, from the bridge in the south, by which the foremost of the raghu-s traveled, to the केदार in the north laden heavy with snow, may this effort gratify them; who would wish to please the lay-folk by this work of mine?

Indeed this clearly expresses the extent of India as the domain of the सनातन-dharma. The मराठा patrons of भास्करराय had placed the saffron Hindu flag on the fort of Attock. He is said to have himself witnessed closely the मराठा campaigns against the Portuguese in the Konkan when he was writing his magnum opus the setu-bandha. Thus, there was a brief period of hope that indeed the idea of बाजीराव्-I, of the complete liberation of भारतवर्ष, might become reality. It was perhaps the vision engendered by this hope that inspired the words of भास्करराय. Sadly, the मराठा-s stumbled and the extent of भारत today has contracted and threatens to contract even more. Not surprisingly more than half the treatises written by भास्करराय have been lost. But lest Hindus forget, they must keep in mind that this was the भारतवर्ष that their people had in mind even as of 250 years ago and if its civilizational memory is lost all hopes of regaining it may also be seen as lost.


