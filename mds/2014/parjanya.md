
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...

## [parjanya](https://manasataramgini.wordpress.com/2014/02/24/parjanya/){rel="bookmark"} {#parjanya .entry-title}

[Posted on]{.meta-prep .meta-prep-author} [[February 24, 2014]{.entry-date}](https://manasataramgini.wordpress.com/2014/02/24/parjanya/ "6:25 AM"){rel="bookmark"} [by]{.meta-sep} [[मानस-तरंगिणी](https://manasataramgini.wordpress.com/author/manasataramgini/ "View all posts by मानस-तरंगिणी"){.url .fn .n}]{.author .vcard}

```{=latex}
\begin{center}
```

![](https://lh6.googleusercontent.com/-9nUIkf9NqRs/Uwrk2lqVoHI/AAAAAAAAC3c/VCXHu7HdOpY/s640/parjanya_cloud_small.jpg){width="75%"}
```{=latex}
\end{center}
```



