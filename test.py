import requests
from bs4 import BeautifulSoup, NavigableString
import os
import subprocess
from nltk.corpus import words
import regex
from aksharamukha import transliterate
from itertools import compress

dic = list(map(lambda s: s.lower(), words.words()))
#Indic words in the dictionary to be transliterated and removed from the dictionary
indic_list = ['vishnu', 'brahmana', 'kala', 'varuna', 'devata', 'mandala', 'puja', 'lakshmi', 'raja', 'chalukya', 'chola', 'varna', 'vayu', 'shastra', 'rupa', 'vidya', 'maya', 'pinda', 'lata', 'purana', 'brahmanda', 'kali', 'mala', 'yadava', 'pari', 'rani', 'pandita', 'dravida', 'ganapati', 'shastri', 'brahmanaspati' ]
for i in indic_list: dic.remove(i)

blacklist = ['GPT', 'AI', 'TikZ', 'rmFar', 'rtAd', 'ySe', 'itDyn', 'icSl', 'ScV-', 'FeLV', 'eportA', 'ormFac','eoGebr', 'CE', 'BCE', 'Qasing', 'Rashīd', 'dīn', 'Sk', 'IE', 'CoV', 'mRNA', '$start', '$end', 'Mt', 'ä', 'ü', 'ß', 'ö', 'f', 'UMi', "Mc", 'lue', "mT", "Si", "MidJourney", "ATP", "GTP", "RN", "PhD" ]
whitelist = ['samiddho', 'bhavati', 'mitro', 'śataṃ', 'viśvā', 'yajño','dyāva', 'īśānaś', 'marut', 'gavya', 'atithigva']
chosen_transliteration = "Devanagari"

# Function to fix some issues with transliteration of /ʃ/
sh_wrong = str(transliterate.process("IAST", chosen_transliteration, "sh"))
sh_right = str(transliterate.process("IAST", chosen_transliteration, "ś"))

# Fix for om-kAras
om_wrong = 'ꣲ'
om_right = " ँ" if chosen_transliteration == "Devanagari" else str(transliterate.process("Devanagari", chosen_transliteration, " ँ")).strip()

def mkdir(dir):
    if not os.path.exists(dir):
        os.mkdir(dir)

mkdir('err')
def clean_output(output):
    return regex.sub(sh_wrong, sh_right,output,flags=regex.U|regex.I)

def convert_url(url):
# Fetch the HTML content from the URL
    print(url)
    response = requests.get(url)
    html_content = response.text

    soup = BeautifulSoup(html_content, 'html.parser')

    content_div = soup.find('div', {'id': 'content', 'role': 'main'})
    for div_class in ['nav-previous', 'nav-next', 'entry-utility']:
        unwanted_div = content_div.find('div', {'class': div_class})
        if unwanted_div:
            unwanted_div.extract()

    unwanted_div = content_div.find('div', {'id': 'jp-post-flair'})
    if unwanted_div:
        unwanted_div.extract()

    unwanted_div = content_div.find('div', {'id': 'nav-below'})
    if unwanted_div:
        unwanted_div.extract()

    latex_images = content_div.find_all('img', class_="latex")
    for img in latex_images:
        latex_equation = img['alt'].replace("\\latex", "\\LaTeX").replace("<", "< ").replace(">", "> ")
        latex_equation = regex.sub(r"\\*\[", r"\\lbrack ", latex_equation)
        latex_equation = regex.sub(r"\\*\]", r"\\rbrack ", latex_equation)

        if latex_equation.find('tabular') == -1 and latex_equation.find('\\LaTeX') == -1:
            latex_equation = latex_equation.replace("\n", "")
            math_equation = f'$start${latex_equation}$end$'.strip()
        else: math_equation = latex_equation
        try:
            img.replace_with(BeautifulSoup(math_equation, 'html.parser'))
        except Exception as e:
            print(f"error replacing math: {math_equation}")

    skt_text = []
    for s in content_div.find_all('span', style=lambda x: x is not None and x.find('color') != -1):
        if s.descendants is not None:
            for d in s.descendants:
                skt_text.append(d)
        else:
            skt_text.append(s)

    for skt in skt_text:
        if skt.string is not None and skt.string.strip() != "":
            text = skt.string.strip()
            list_total = regex.split(r"\b", text,flags=regex.U)
            list_invalid = list(compress(list_total, [s.lower() in dic for s in list_total]))
            list_valid = list(compress(list_total, [(((regex.match(r'\w[a-zA-Z0-9_~\^]*[A-Z][a-zA-Z0-9_~\^]*', s) and not regex.match(r'^[A-Z0-9_]*s*$', s)) or regex.match(r'[\w]*[āĀīĪūŪṛṚṝṜḷḶḹḸṃṂḥḤṭṬḍḍḌṅṅṄñÑṇṆśŚṣṢḻḺ][\w]*', s))) for s in list_total]))
            check_for_invalid_translit = (not regex.match("(.)*(" + ")|(".join(blacklist) + ")(.)*", text) and text.find('z') == -1 and (len(list_invalid) < 0.2 * len(list_total) or len(list_valid) > 0.2 * len(list_total))) or bool(regex.match("(.)*(" + ")|(".join(whitelist) + ")(.)*", text)) and text.find('$start') == -1 and text.find('end$') == -1
            print(text, check_for_invalid_translit )
            if not check_for_invalid_translit: continue
            text = text.replace('RV', str(transliterate.process('ITRANS', chosen_transliteration, 'R^igveda'))).replace('AV', str(transliterate.process('ITRANS', chosen_transliteration, 'atharvaveda')))
            replaced_text = str(transliterate.process('autodetect', chosen_transliteration, text))
            # fix for om-kara-s
            replaced_text = regex.sub(r"\s*" + om_wrong, om_right, replaced_text, flags=regex.U)

            try:
                skt.replace_with(replaced_text)
            except Exception as e:
                print(f"error replacing sanskrit: {text}")

    # filter out ITRANS/IAST words and add a Devanagari transliteration
    indic_words = set(filter(lambda s: s.lower() not in dic and (((regex.match(r'[a-zA-Z0-9~\^]+[A-Z][a-zA-Z0-9~\^]*', s) or regex.match(r"(.)*(~[Nn])|(R\^[iI])|(R\\\^[iI])(.)*", s)) and not regex.match(r'^[A-Z0-9_\-]*s*$', s)) or regex.match(r'\w*[āĀīĪūŪṛṚṝṜḷḶḹḸṃṂḥḤṭṬḍḍḌṅṅṄñÑṇṆśŚṣṢḻḺ]\w*', s, flags=regex.U)) and all([s.find(x) == -1 for x in blacklist])  , regex.split(r'(?<=(?<![\p{L}\p{N}_~\^])(?=[\p{L}\p{N}_~\^])|(?<=[\p{L}\p{N}_~\^])(?![\p{L}\p{N}_~\^]))', ''.join(content_div.findAll(string = True)), flags=regex.U|regex.I)))
#    print(indic_words)
    indic_words = list(sorted(indic_words, key=len, reverse=True))
    output = dict()
    filtered_html = str(content_div)
    for w in indic_words:
        transliteration_method = "autodetect"
        if regex.match(r'\w*[āĀīĪūŪṛṚṝṜḷḶḹḸṃṂḥḤṭṬḍḍḌṅṅṄñÑṇṆśŚṣṢḻḺ]\w*', w, flags=regex.U): transliteration_method = "IAST"
        else: transliteration_method = "ITRANS"
        w = w.replace(r"R\^","R^")
        replaced_word = str(transliterate.process(transliteration_method, chosen_transliteration, w))
        if filtered_html.find(w) == -1: print(f"Weird bug in replacing {w} to {replaced_word}")
        filtered_html = filtered_html.replace(w, replaced_word) if not regex.match(r"(.)*[◌ॆऄऎॊऎ](.)*", replaced_word, flags=regex.U) else filtered_html
        output[w] = replaced_word

    print(output)

    base_name = url.rpartition('/')[0].rpartition('/')[2]
    base_name = base_name.replace('/', '_')
    result = base_name + ".pdf"

    pandoc_cmd = ['pandoc', "--wrap=none", '-f', 'html', '-t', 'markdown',]
    input_markdown = regex.sub(r"\\([<>`*_\[\]#\^_\.])(?![a-zA-Z])", r'\1', subprocess.run(pandoc_cmd, input=filtered_html, text=True, capture_output=True).stdout.replace("\\\\", "\\").replace(r"\|", r"|")).replace(r"\'", "'")

    # Fix Figures appearing on newline
    input_markdown = regex.sub(r"(?<=(\n\[!\[.*\](.*)\]\(.*\)))[\s\n]*[fF]igure\s*(\d+)[\.:]*(.*)", r'```{=latex}\n\\begin{center}\n```\nFigure \3\\quad\4\n```{=latex}\n\\end{center}\n```', input_markdown)

    # un-linkify images
    input_markdown = regex.sub(r"\n\[!\[.*\](.*)\]\(.*\)", r"\n```{=latex}\n\\begin{center}\n```\n![]\1\n```{=latex}\n\\end{center}\n```\n", input_markdown)
    # Make lists look nicer
    input_markdown = regex.sub(r'(?<=(\n))\s*[-\*●](?!(.*\*.*))', r'\n  - ', input_markdown, flags=regex.U)
    input_markdown = regex.sub(r'(?<=(\n\s*))(\d*)\\*\)(?=(\s*\w))', r'\n \2. ', input_markdown)
    # Fix for latex equations/environment
    input_markdown = regex.sub(r"\n*\s*```{=latex}\s*(?!(\n))", "\n```{=latex}\n", input_markdown)
    input_markdown = regex.sub(r"(?<!(^[\n]))\s*```(?!({=latex}))", "\n```\n", input_markdown)

    # New-line equations
    input_markdown = regex.sub(r"\n+\s*\\\$start\\\$\s*(?=(.*\$end\\\$[\s\\]*\n+))", "\n\n $$", input_markdown)
    input_markdown = regex.sub(r"(?<=(\$\$.*))\s*\\\$end\\\$[\s\\]*\n+", "$$ \n\n", input_markdown)

    # In-line equations
    input_markdown = regex.sub(r"\\\$start\\\$\s*", " $", input_markdown)
    input_markdown = regex.sub(r"\s*\\\$end\\\$", "$ ", input_markdown)

    # Fix image resolutions
    input_markdown = regex.sub(r"\?w=(\d)*(&h=(\d)*)*", "", input_markdown)
#    input_markdown = regex.sub(r'width\s*=\s*"\d*"', 'width="75%"', input_markdown)
#    input_markdown = regex.sub(r'height\s*=\s*"\d*"', '', input_markdown)

    #clean markdown output
    input_markdown = regex.sub(r":::.*\n", "", input_markdown)
    input_markdown = regex.sub(r"(?<=(\n\[*!\[.*)){.*}", '{width="75%"}', input_markdown)
    input_markdown = regex.sub(r"\\\n\n", r"\n\n", input_markdown)

    date = input_markdown.partition("[Posted on]{.meta-prep .meta-prep-author}")[2].partition("[by]{.meta-sep}")[0].partition("[[")[2].partition("]")[0].replace("\n", " ").strip()
    year = str(regex.search(r"20\d\d", date).group())
#     print(f"Year: {year}")

    mkdir(f"mds/{year}")
    mkdir(f"pdfs/{year}")

    input_markdown =  r'''
---
documentclass: extarticle
fontsize: 14pt
papersize: a4
mainfont: Adobe Devanagari
mathfont: Libertinus Math
geometry:
- top=30mm
- bottom=30mm
- right=20mm
- left=20mm
- heightrounded
header-includes:
    \setmainfont[Script=Devanagari]{Adobe Devanagari}
output:
    pdf_document:
        fig_caption: false
...
'''+ input_markdown

    with open(f'mds/{year}/{base_name}.md', 'w') as f: f.write(input_markdown); f.close();

    pandoc_cmd = ['pandoc', '--verbose', '--dpi=300',
                  "--pdf-engine=xelatex", '-f', 'markdown+implicit_figures', '-t', 'pdf', '-o' , f'pdfs/{year}/{result}']
    print(' '.join(pandoc_cmd))
    cmd = subprocess.run(pandoc_cmd, input=input_markdown, text=True, capture_output=True)
    if cmd.returncode != 0:
        print(cmd.stderr)
        with open(f"err/{url.rpartition('/')[0].rpartition('/')[2]}", "w") as f:
            f.write(str(cmd.stderr))



if __name__=='__main__':
    with open('url.txt', 'r') as f:
        for x in f.readlines():
            try:
              convert_url(x.strip()) if x.strip() != "" else None
            except Exception as e:
                print("Error!")
                with open(f"err/{x.rpartition('/')[0].rpartition('/')[2]}", "a") as f:
                    f.write(str(e))
